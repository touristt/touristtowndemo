<?php
require_once 'include/config.inc.php';
require_once 'include/public-site-functions.inc.php';

$_SESSION['CATEGORY'] = 0;
$SEOtitle = $REGION['R_SEO_Title'];
$SEOdescription = $REGION['R_SEO_Description'];
$SEOkeywords = $REGION['R_SEO_Keywords'];

$sql_slider = "SELECT RTP_Title, RTP_Description, RTP_Photo, RTP_Video_Link FROM tbl_Region_Themes_Photos WHERE RTP_RT_RID ='" . encode_strings($REGION['R_ID'], $db) . "' ORDER BY RTP_Order";
$result_slider = mysql_query($sql_slider, $db) or die("Invalid query: $sql_slider -- " . mysql_error());
$slider_count = mysql_num_rows($result_slider);
//Homepage Painting
$sqlHomepagePainting = "SELECT * FROM tbl_Homepage_Paintings WHERE HP_R_ID ='" . encode_strings($REGION['R_ID'], $db) . "'";
$resultHomepagePainting = mysql_query($sqlHomepagePainting, $db) or die("Invalid query: $sqlHomepagePainting -- " . mysql_error());
$rowHomepagePainting = mysql_fetch_array($resultHomepagePainting);

$sql_feature = "SELECT S_ID, S_Category, S_Title, S_Feature_Image, S_Description, C_Parent from tbl_Story LEFT JOIN tbl_Story_Homepage_Category ON S_ID = SHC_S_ID
                LEFT JOIN tbl_Category ON S_Category = C_ID
                $StorySeasons_JOIN
                WHERE SHC_R_ID ='" . encode_strings($REGION['R_ID'], $db) . "' 
                AND SHC_Homepage = 1 AND SHC_Feature = 1 AND S_Active = 1 
                $StorySeasons_WHERE GROUP BY S_ID ORDER BY SHC_Order ASC";
$result_feature = mysql_query($sql_feature, $db) or die("Invalid query: $sql_feature -- " . mysql_error());
$feature_count = mysql_num_rows($result_feature);
$sql_secondary = "SELECT S_ID, S_Category, S_Title, S_Thumbnail, C_Name, C_Parent, RC_Name from tbl_Story LEFT JOIN tbl_Story_Homepage_Category ON S_ID = SHC_S_ID
                  LEFT JOIN tbl_Category ON S_Category = C_ID
                  LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID
                  $StorySeasons_JOIN
                  WHERE SHC_R_ID ='" . encode_strings($REGION['R_ID'], $db) . "' 
                  AND SHC_Homepage = 1 AND SHC_Secondary = 1 AND S_Active = 1 
                  $StorySeasons_WHERE GROUP BY S_ID ORDER BY SHC_Order ASC";
$result_secondary = mysql_query($sql_secondary, $db) or die("Invalid query: $sql_secondary -- " . mysql_error());
$secondary_count = mysql_num_rows($result_secondary);
?>
<?php require_once 'include/public/header.php'; ?>
<!--Slider Start-->
<section class="main_slider">
    <div class="slider_wrapper">
        <div class="slider_wrapper_video">
            <div class="sliderCycle" data-cycle-loader="wait" data-cycle-fx=fade data-cycle-timeout=0 data-cycle-prev="#prev" data-cycle-next="#next">
                <?php
                $first_img = true;
                $i = 1;
                if ($slider_count > 0) {
                    while ($row_slider = mysql_fetch_array($result_slider)) {
                        $VIDEOID = $row_slider['RTP_Video_Link'];
                        $pos = strpos($VIDEOID, '=');
                        if ($pos == false) {
                            $video_link = end(explode('/', $VIDEOID));
                        } else {
                            $video_link = end(explode("=", $VIDEOID));
                        }
                        if ($row_slider['RTP_Photo'] != '') {
                            if ($first_img) {
                                $image_slider_style = '';
                                $first_img = false;
                            } else {
                                $image_slider_style = 'display:none;';
                            }
                            ?>
                            <div class="slide-images" style="<?php echo $image_slider_style ?>">
                                <?php if ($VIDEOID != "") { ?>
                                    <div class="<?php echo"player" . $i . "_wrapper"; ?>" style="position:absolute;opacity: 0;top:0;">
                                        <input type="hidden" id="video-link-<?php echo $i; ?>" value="<?php echo $video_link ?>">
                                        <div  id="<?php echo "player" . $i; ?>"></div>
                                    </div>
                                <?php } ?>
                                <div class="slider-text">
                                    <h1 class="show-slider-display"><?php echo $row_slider['RTP_Title'] ?></h1>
                                    <h3 class="show-slider-display"><?php echo $row_slider['RTP_Description'] ?></h3>
                                    <?php if ($VIDEOID != "") { ?>
                                        <div class="watch-video play-button show-slider-display">
                                            <div class="slider-button">
                                                <img class="video_icon" src="images/videoicon.png" alt="Play">
                                                <a onclick="play_video(<?php echo $i; ?>, '<?php echo"player" . $i; ?>')">Watch full video</a>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <img class="slider_image" class="show-slider-display" src="<?php echo IMG_LOC_REL . $row_slider['RTP_Photo'] ?>" alt="<?php echo ($row_slider['RTP_Title'] != '') ? $row_slider['RTP_Title'] : $REGION['R_Name']; ?>">
                            </div>
                            <?php
                        }
                        $i++;
                    }
                } else {
                    ?>
                    <div class="slide-images" style="<?php echo $image_slider_style ?>">
                        <img class="slider_image" class="show-slider-display" src="<?php echo IMG_LOC_REL . $default_header_image; ?>" alt="<?php echo $row_slider['RTP_Title']; ?>">
                    </div> 
                    <?php
                }
                ?>
            </div>
            <input type="hidden" id="total_players" value="<?php echo $i; ?>">
            <div class=center>
                <span id=prev></span>
                <span id=next></span>
            </div>
            <?php if ($THEME['TO_Slider_Overlay'] != "") { ?>
                <div class="image_overlay_img">
                    <div class="image_overlay">
                        <img src="<?php echo IMG_ICON_REL . $THEME['TO_Slider_Overlay']; ?>" alt="Slider Overlay">
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<!--Slider End-->
<!--Description Start-->
<section class="description theme-description">
    <div class="description-wrapper">
        <div class="description-inner">
            <h1 class="heading-text"><?php echo (($REGION['R_Homepage_Title']) != "" ? $REGION['R_Homepage_Title'] : "Ready to Explore?") ?></h1>
            <div class="site-text ckeditor-anchors">
                <?PHP if ($REGION['R_Description']) { ?>
                    <p><?PHP echo ($REGION['R_Description']); ?></p>
                <?PHP } ?>
            </div>
        </div>
    </div>
</section>
<!--Description End-->
<!--Homepage Painting-->
<?php
if ($REGION['R_Show_Hide_Homepage_Image'] == 1 && $rowHomepagePainting['HP_Homepage_Painting'] != "") {
    $imageHomepagePainting = $rowHomepagePainting['HP_Homepage_Painting'];
    ?>
    <section class="main_slider">
        <div class="slider_wrapper">
            <div class="slider_wrapper_video">
                <div class="homepage-painting">
                    <?php
                    if ($rowHomepagePainting['HP_Link'] != '') {
                        $linkHomepagePainting = preg_replace('/^www\./', '', $rowHomepagePainting['HP_Link']);
                        $linkHomepagePainting = 'http://' . str_replace(array('http://', 'https://'), '', $linkHomepagePainting);
                        echo '<a class="" href="' . $linkHomepagePainting . '">';
                    }
                    ?>
                    <div class="slide-images">
                        <?php if ($rowHomepagePainting['HP_Title'] != '' || $rowHomepagePainting['HP_Description'] != '') { ?>
                            <div class="slider-text">
                                <?php if ($rowHomepagePainting['HP_Title'] != '') { ?>
                                    <h1 class="show-slider-display"><?php echo $rowHomepagePainting['HP_Title'] ?></h1>
                                <?php } if ($rowHomepagePainting['HP_Description'] != '') { ?>
                                    <h3 class="show-slider-display"><?php echo $rowHomepagePainting['HP_Description']; ?></h3>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <img class="slider_image" class="show-slider-display" src="<?php echo IMG_LOC_REL . $imageHomepagePainting ?>" alt="Painting">
                    </div>
                    <?php
                    if ($rowHomepagePainting['HP_Link'] != '') {
                        echo '</a>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<!--Homepage Painting-->
<!-- Feature Stories-->
<?php if ($feature_count > 0 && $REGION['R_Stories'] == 1 && $blog['RC_Status'] == 0) { ?>
    <section class="section_stories" class="padding-bottom-none border-none">
        <div class="grid-wrapper">
            <div class="grid-inner padding-bottom-none slider">
                <div class="feature-stories">
                    <?php
                    while ($row_feature = mysql_fetch_array($result_feature)) {
                        if ($row_feature['S_Feature_Image'] != '') {
                            $feature_image = $row_feature['S_Feature_Image'];
                        } else {
                            $feature_image = $default_header_image;
                        }
                        ?>
                        <div class="feature-story">
                            <a href="/<?php echo $storyCatSEORow['C_Name_SEO'] ?>/<?php echo clean($row_feature['S_Title']) ?>/<?php echo isset($row_feature['S_ID']) ? $row_feature['S_ID'] : ''; ?>/">
                                <img src="<?php echo IMG_LOC_REL . $feature_image; ?>" width="100%" height="auto" alt="<?php echo $row_feature['S_Title'] ?>" />
                            </a>
                            <a href="/<?php echo $storyCatSEORow['C_Name_SEO'] ?>/<?php echo clean($row_feature['S_Title']) ?>/<?php echo isset($row_feature['S_ID']) ? $row_feature['S_ID'] : ''; ?>/">
                                <h1 align="center" class="heading-text"><?php echo $row_feature['S_Title'] ?></h1>
                            </a>
                            <?php
                            if ($row_feature['S_Description'] != '') {
                                $string = preg_replace('/(.*)<\/p[^>]*>/i', '$1', $row_feature['S_Description']);
                                ?>
                                <div class="story-description">
                                    <?php echo $string ?>
                                    <a href="/<?php echo $storyCatSEORow['C_Name_SEO'] ?>/<?php echo clean($row_feature['S_Title']) ?>/<?php echo isset($row_feature['S_ID']) ? $row_feature['S_ID'] : ''; ?>/">
                                        Read More
                                    </a>
                                    </p>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<!-- Feature Stories end -->
<!-- Secondary Stories-->
<?php if ($secondary_count > 0 && $REGION['R_Stories'] == 1 && $blog['RC_Status'] == 0) { ?>
    <section class="section_stories">
        <div class="grid-wrapper">
            <div class="grid-inner">
                <h1 class="heading-text" align="center"><?php echo ($REGION['R_Homepage_Stories_Title'] != '') ? $REGION['R_Homepage_Stories_Title'] : 'Stories' ?></h1>
                <?php
                $i = 0;
                while ($row_secondary = mysql_fetch_array($result_secondary)) {
                    $i++;
                    if ($row_secondary['S_Thumbnail'] != '') {
                        $secondary_image = $row_secondary['S_Thumbnail'];
                    } else {
                        $secondary_image = $default_thumbnail_image;
                    }
                    if ($i == 1) {
                        echo "<div class='thumbnails static-thumbs'>";
                    }
                    ?>
                    <div class="thumb-item title">
                        <a href="/<?php echo $storyCatSEORow['C_Name_SEO'] ?>/<?php echo clean($row_secondary['S_Title']) ?>/<?php echo isset($row_secondary['S_ID']) ? $row_secondary['S_ID'] : ''; ?>/">
                            <img src="<?php echo IMG_LOC_REL . $secondary_image ?>" alt="<?php echo $row_secondary['S_Title'] ?>">
                            <h3 class="thumbnail-heading"><?php echo ($row_secondary['RC_Name'] != '') ? $row_secondary['RC_Name'] : $row_secondary['C_Name'] ?></h3>
                            <h3 class="thumbnail-desc"><?php echo $row_secondary['S_Title'] ?></h3>
                        </a>
                    </div>
                    <?php
                    if ($i == 3) {
                        echo '</div>';
                        $i = 0;
                    }
                }
                ?>
            </div>
        </div>
    </section>
<?php } ?>
<!-- Secondary Stories end-->
<!--Thumbnail Grid-->
<section class="thumbnail-grid slider-grid">
    <div class="grid-wrapper">
        <?php
        //Getting Active Categories for region
        if (isset($REGION['R_Homepage_Carousals']) && $REGION['R_Homepage_Carousals'] == 1) {
            $getCategory = "SELECT RC1.RC_Image_Icon, RC1.RC_Name, C1.C_Name, C1.C_ID, C1.C_Name_SEO as subSEO, C2.C_Name_SEO as mainSEO FROM tbl_Region_Category RC1
                            LEFT JOIN tbl_Category C1 ON RC1.RC_C_ID = C1.C_ID AND RC1.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                            LEFT JOIN tbl_Category C2 ON C1.C_Parent = C2.C_ID
                            LEFT JOIN tbl_Region_Category RC2 ON RC2.RC_C_ID = C2.C_ID AND RC2.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                            WHERE C1.C_Parent != 0 AND C1.C_Parent != 8 AND C2.C_Is_Blog != 1 AND RC1.RC_Status = 0 AND RC2.RC_Status = 0 ORDER BY RC1.RC_Order ASC";
            $categoryCheck = "BLC_C_ID";
        } else {
            $getCategory = "SELECT RC_Image_Icon, RC_Name, C_Name, C_ID, C_Name_SEO as mainSEO FROM tbl_Category 
                            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                            WHERE C_Parent = 0 AND RC_Status=0 AND C_ID != 8 AND C_Is_Blog != 1 ORDER BY RC_Order ASC";
            $categoryCheck = "BLC_M_C_ID";
        }
        $catRes = mysql_query($getCategory);
        $totalCat = 1;
        $DBTotalCat = mysql_num_rows($catRes);
        $lastChild = "";
        $show_homepage_check = '';
        while ($homeCat = mysql_fetch_assoc($catRes)) {
            if ($totalCat == $DBTotalCat) {
                $lastChild = "grid-last-child";
            }
            $totalCat++;
            if ($REGION['R_Type'] == 4) {
                //County Regions
                $show_homepage_check = ' AND BLCR_Home_status=1';
            }
            if ($REGION['R_Type'] == 1) {
                //Parent Regions
                $getSubCat = " SELECT BL_ID, BL_Name_SEO, BLP_Photo, BL_Listing_Title, RC_Name, C_Name FROM tbl_Business_Listing
                                LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
                                INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                                LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                                INNER JOIN tbl_Region_Category ON RC_C_ID = BLC_C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                                LEFT JOIN tbl_Category ON RC_C_ID = C_ID
                                LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
                                LEFT JOIN tbl_Business_Listing_Order ON BL_ID = BLO_BL_ID AND BLO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
                                AND BLO_S_C_ID = BLC_C_ID
                                $SEASONS_JOIN
                                WHERE " . $categoryCheck . " = '" . encode_strings($homeCat['C_ID'], $db) . "' 
                                $show_homepage_check
                                AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing='1' AND RC_Status = 0
                                AND BLCR_BLC_R_ID IN (SELECT R_ID from tbl_Region LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID where RM_Parent = '" . encode_strings($REGION['R_ID'], $db) . "')
                                $SEASONS_WHERE
                                $include_free_listings
                                GROUP BY BL_Listing_Title
                                $order_listings";
            } else {
                //Child Regions
                $getSubCat = " SELECT BL_ID, BL_Name_SEO, BLP_Photo, BL_Listing_Title, RC_Name, C_Name FROM tbl_Business_Listing
                                LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
                                INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                                LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID=BL_ID
                                INNER JOIN tbl_Region_Category ON RC_C_ID = BLC_C_ID AND RC_R_ID='" . encode_strings($REGION['R_ID'], $db) . "'
                                LEFT JOIN tbl_Category ON RC_C_ID = C_ID
                                LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
                                LEFT JOIN tbl_Business_Listing_Order ON BL_ID = BLO_BL_ID AND BLO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
                                AND BLO_S_C_ID = BLC_C_ID
                                $SEASONS_JOIN
                                WHERE " . $categoryCheck . " = '" . encode_strings($homeCat['C_ID'], $db) . "'
                                $show_homepage_check
                                AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing='1' AND RC_Status = 0
                                AND BLCR_BLC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' AND BLP_R_ID = '" . $REGION['R_ID'] . "'
                                $SEASONS_WHERE
                                $include_free_listings
                                GROUP BY BL_Listing_Title
                                $order_listings";
            }
            if ($REGION['R_Homepage_Listings'] > 0) {
                $getSubCat .= " LIMIT " . encode_strings($REGION['R_Homepage_Listings'], $db);
            } else {
                $getSubCat .= " LIMIT 10";
            }
            $subCatRes = mysql_query($getSubCat, $db) or die(mysql_error() . ' - ' . $getSubCat);
            $count_homepage = mysql_num_rows($subCatRes);
            if ($count_homepage > 0) {
                ?>
                <div class="grid-inner <?php echo $lastChild; ?>">
                    <div class="grid-titles">
                        <div class="grid-left-title heading-text">
                            <img class="grid-title-icon" src="<?php echo (($homeCat['RC_Image_Icon'] != "") ? IMG_LOC_REL . $homeCat['RC_Image_Icon'] : 'images/story-icon.png') ?>" alt="Category Icon"/>
                            <h3><?php echo ($homeCat['RC_Name'] != '') ? $homeCat['RC_Name'] : $homeCat['C_Name'] ?></h3>
                        </div>
                        <div class="grid-left-view heading-text">
                            <h3><a href="/<?php echo $homeCat['mainSEO'] ?>/<?php echo (isset($homeCat['subSEO'])) ? $homeCat['subSEO'] : '' ?>">View All</a></h3>
                        </div>
                    </div>
                    <div class="thumbnails">
                        <div class="thumbnail-slider">
                            <div class="inner">
                                <ul class="bxslider">
                                    <?PHP
                                    while ($homeSubCat = mysql_fetch_assoc($subCatRes)) {
                                        if ($homeSubCat['BLP_Photo'] != '') {
                                            $listing_image = $homeSubCat['BLP_Photo'];
                                        } else {
                                            $listing_image = $default_thumbnail_image;
                                        }
                                        ?>
                                        <li>
                                            <a href="/profile/<?php echo $homeSubCat['BL_Name_SEO'] ?>/<?php echo $homeSubCat['BL_ID'] ?>/">
                                                <img src="<?php echo IMG_LOC_REL . $listing_image ?>" alt="<?php echo $homeSubCat['BL_Listing_Title']; ?>" />
                                                <h3 class="thumbnail-heading"><?php echo (($homeSubCat['RC_Name'] != "") ? $homeSubCat['RC_Name'] : $homeSubCat['C_Name']) ?></h3>
                                                <h3 class="thumbnail-desc"><?php echo $homeSubCat['BL_Listing_Title'] ?></h3>
                                            </a>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
</section>
<!--Thumbnail Grid-->
<!--upto date start-->
<?php if ($row_icons['RS_FB_Link'] != "" || $row_icons['RS_I_Link'] != "" || $row_icons['RS_S_Link'] != "" || $row_icons['RS_T_Link'] != "") { ?>
    <section class="description keep-upto-date">
        <div class="description-wrapper">
            <div class="description-inner">
                <h1 class="heading-text"><?php echo ($REGION['R_Homepage_Text_Above_Events'] != '') ? $REGION['R_Homepage_Text_Above_Events'] : 'Keep Up to Date with Us'; ?></h1>
                <div class="main-page-s-icons">
                    <?php
                    if ($row_icons['RS_FB_Link'] != "") {
                        $facebook_link = preg_replace('/^www\./', '', $row_icons['RS_FB_Link']);
                        ?>
                        <a target="_blank" href="http://<?php echo str_replace(array('http://', 'https://'), '', $facebook_link) ?>"><img src="/images/<?php echo ($row_icons['RS_FB_Icon'] != "") ? 'DB/' . $row_icons['RS_FB_Icon'] : 'facebook.png' ?>" alt="Facebook"></a>
                        <?php
                    } if ($row_icons['RS_I_Link'] != "") {
                        $rs_i_link = preg_replace('/^www\./', '', $row_icons['RS_I_Link']);
                        ?>
                        <a target="_blank" href="http://<?php echo str_replace(array('http://', 'https://'), '', $rs_i_link) ?>"><img src="/images/<?php echo ($row_icons['RS_I_Icon'] != "") ? 'DB/' . $row_icons['RS_I_Icon'] : 'instagram.png' ?>" alt="Instagram"></a>
                        <?php
                    } if ($row_icons['RS_S_Link'] != "") {
                        $rs_s_link = preg_replace('/^www\./', '', $row_icons['RS_S_Link']);
                        ?>
                        <a target="_blank" href="http://<?php echo str_replace(array('http://', 'https://'), '', $rs_s_link) ?>"><img src="/images/<?php echo ($row_icons['RS_S_Icon'] != "") ? 'DB/' . $row_icons['RS_S_Icon'] : 'snapchat.png' ?>" alt="Snapchat"></a>
                        <?php
                    } if ($row_icons['RS_T_Link'] != "") {
                        $rs_t_link = preg_replace('/^www\./', '', $row_icons['RS_T_Link']);
                        ?>
                        <a target="_blank" href="http://<?php echo str_replace(array('http://', 'https://'), '', $rs_t_link) ?>"><img src="/images/<?php echo ($row_icons['RS_T_Icon'] != "") ? 'DB/' . $row_icons['RS_T_Icon'] : 'twitter.png' ?>" alt="Twitter"></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<!--upto date end-->
<!--bar texture start-->
<div class="bar-texture"></div>
<!--bar texture end-->
<!--secondary slider start-->
<?php if ($REGION['R_Show_Hide_Event'] == 1) { ?>
    <div class="secondary-slider">
        <?php
        $getEventImage = "SELECT RC_Image, RC_Description FROM tbl_Region_Category WHERE RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' AND RC_C_ID = 8 LIMIT 1";
        $eventImgRes = mysql_query($getEventImage, $db) or die(mysql_error());
        $eventIMG = mysql_fetch_assoc($eventImgRes);
        if ($eventIMG['RC_Image'] != "") {
            ?>
            <img src="<?php echo IMG_LOC_REL . $eventIMG['RC_Image']; ?>" alt="Event Main Image"/>
            <?php if ($THEME['TO_Secondary_Slider_Overlay'] != "") { ?>
                <div class="secondary-overlay-img">
                    <div class="secondary-overlay">
                        <img src="<?php echo IMG_ICON_REL . $THEME['TO_Secondary_Slider_Overlay'] ?>" alt="Secondary Slider Overlay"/>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
    <!--secondary slider end-->
    <!--Whats on around town.-->
    <section class="description whats-on">
        <div class="description-wrapper">
            <div class="description-inner">
                <h1 class="heading-text"><?php echo $REGION['R_Homepage_Events_Title'] != '' ? $REGION['R_Homepage_Events_Title'] : 'What&#39;s on around town.' ?></h1>
                <div class="site-text ckeditor-anchors">
                    <p>
                        <?php echo $eventIMG['RC_Description']; ?>
                    </p>
                </div>
            </div>
            <a class="heading-text" href="/whats-on/?submitEventAgree=1"><?php echo ($REGION['R_Homepage_Event_Register_Text'] != '') ? $REGION['R_Homepage_Event_Register_Text'] : 'Register your event here'; ?></a>
            <div class="events-wrapper">
                <div class="events">
                    <div class="event-title">
                        <img src="<?php echo $THEME['TO_Homepage_Event_Icon'] != '' ? IMG_ICON_REL . $THEME['TO_Homepage_Event_Icon'] : 'images/events.png' ?>" alt="Events">
                        <h1 class="heading-text">EVENTS</h1>
                    </div>
                    <?php
                    $EP = "SELECT RM_Child FROM `tbl_Region_Multiple` LEFT JOIN tbl_Region ON R_ID = RM_Parent WHERE R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
                    $resultEP = mysql_query($EP, $db) or die("Invalid query: $EP -- " . mysql_error());
                    $srchRegion = "";
                    if ($REGION['R_Parent'] == 0 && $REGION['R_Type'] != 4) {
                        $event_counter = 0;
                        while ($rowEP = mysql_fetch_assoc($resultEP)) {
                            if ($event_counter == 0) {
                                $operator = " AND (";
                            } else {
                                $operator = " OR";
                            }
                            $srchRegion .= " $operator FIND_IN_SET (" . $rowEP['RM_Child'] . ", E_Region_ID)";
                            $event_counter++;
                        }
                        $srchRegion .= " $operator FIND_IN_SET (" . $REGION['R_ID'] . ", E_Region_ID) )";
                    } else {
                        $srchRegion = " AND FIND_IN_SET (" . $REGION['R_ID'] . ", E_Region_ID)";
                    }
                    $sql = "SELECT LocationID, OrganizationID, EventType, EventDateStart, EventDateEnd, Pending, EventID, Title, E_Name_SEO, 
                        ShortDesc FROM `Events_master` 
                        LEFT JOIN Events_Location ON EL_ID = LocationID 
                        LEFT JOIN Events_Organization ON EO_ID = OrganizationID 
                        WHERE ((`EventType` = 1 AND `EventDateStart` >= CURDATE()) OR (`EventDateEnd` >= CURDATE()))
                        AND Pending = 0 $srchRegion
                        ORDER BY EventDateStart LIMIT 6";
                    $result = mysql_query($sql, $db);
                    $l = 0;
                    while ($row = mysql_fetch_assoc($result)) {
                        $l++;
                        if ($l == 1) {
                            ?>
                            <div class="event-column-outer">
                            <?php } ?>
                            <div class="event-column">
                                <div class="event-date">
                                    <?php
                                    echo date('M j, Y', strtotime($row['EventDateStart']));
                                    if ($row['EventDateStart'] != $row['EventDateEnd'] && $row['EventDateEnd'] != '0000-00-00') {
                                        echo ' - ' . date('M j, Y', strtotime($row['EventDateEnd']));
                                    }
                                    ?>
                                </div>
                                <div class="event-title"><a href="/events/<?php echo $row['E_Name_SEO'] ?>/<?php echo $row['EventID'] ?>"><?php echo $row['Title'] ?></a></div>
                                <div class="event-desc"><?php echo ($row['ShortDesc'] != '') ? strip_tags($row['ShortDesc']) : ""; ?></div>
                                <div class="event-more"><a href="/events/<?php echo $row['E_Name_SEO'] ?>/<?php echo $row['EventID'] ?>">More...</a></div>
                            </div>
                            <?php if ($l == 2) { ?>
                            </div> <?php
                            $l = 0;
                        }
                    }
                    ?>
                    <div class="upcoming-events">
                        <div class="upcoming-events-wrapper">
                            <a href="whats-on/upcoming-events/"><?php echo ($REGION['R_Homepage_Event_See_All_Text'] != '') ? $REGION['R_Homepage_Event_See_All_Text'] : 'See All Upcoming Events'; ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<!--Whats on around town. end-->
<!--Map section-->
<section class="description maps map-page margin-bottom-none">
    <?php
    require_once 'include/public/map_filters.php';
    if ($REGION['R_Parent'] == 0) {
        $REG = '';
    } else {
        $REG = "AND BLP_R_ID = " . $REGION['R_ID'];
    }
    $sql = "SELECT  DISTINCTROW BLP_Photo, BL_Listing_Title, BL_Lat, BL_Long, BL_ID, BL_Name_SEO, BL_Street, BL_Town, 
            RC.RC_Name, RC.RC_Map_Icon as subcat_icon, RC1.RC_Map_Icon as cat_icon FROM tbl_Business_Listing 
            LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
            INNER JOIN tbl_Business_Listing_Category_Region ON  BLCR_BL_ID = BL_ID 
            INNER JOIN tbl_Region_Category RC ON RC.RC_C_ID = BLC_C_ID AND RC.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            INNER JOIN tbl_Region_Category RC1 ON RC1.RC_C_ID = BLC_M_C_ID AND RC1.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
            LEFT JOIN tbl_Business_Listing_Order ON BL_ID = BLO_BL_ID AND BLO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
            AND BLO_S_C_ID = BLC_C_ID
            $SEASONS_JOIN
            WHERE BLCR_BLC_R_ID " . encode_strings(($REGION['R_Type'] == 1 ? "IN " . $regionList : "= " . $REGION['R_ID']), $db) . " 
            AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing='1' 
            AND RC.RC_Status = 0 AND RC1.RC_Status = 0 $REG $SEASONS_WHERE $include_free_listings_on_map";
    $sql .= " GROUP BY BL_ID $order_listings";
    // generate icons for all listings on map regardless of page
    $result_map = mysql_query($sql, $db);
    $markers = array();
    while ($list = mysql_fetch_array($result_map, MYSQL_ASSOC)) {
        if ($list['BLP_Photo'] != '') {
            $image = '<div class="thumbnail"><img src="' . (IMG_LOC_REL . $list['BLP_Photo']) . '" alt="' . htmlspecialchars(trim($list['BL_Listing_Title']), ENT_QUOTES) . '" /></div>';
        } else {
            $image = '<div class="thumbnail"><img src="' . (IMG_LOC_REL . $default_thumbnail_image) . '" alt="' . htmlspecialchars(trim($list['BL_Listing_Title']), ENT_QUOTES) . '" /></div>';
        }
        if ($list['subcat_icon'] != '') {
            $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $list['subcat_icon'];
        } elseif ($list['cat_icon'] != '') {
            $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $list['cat_icon'];
        } else {
            $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . 'default.png';
        }
        if ($list['BL_Lat'] && $list['BL_Long']) {
            $markers[] = array(
                'id' => $list['BL_ID'],
                'lat' => $list['BL_Lat'],
                'lon' => $list['BL_Long'],
                'name' => $list['BL_Listing_Title'],
                'path' => '/profile/' . $list['BL_Name_SEO'] . '/' . $list['BL_ID'] . '/',
                'icon' => $icon,
                'main_photo' => $image,
                'address' => htmlspecialchars(trim($list['BL_Street']), ENT_QUOTES),
                'town' => htmlspecialchars(trim($list['BL_Town']), ENT_QUOTES)
            );
        }
    }
    $markers = json_encode($markers);
    ?>
    <div class="map_wrapper">
        <div id="map_canvas">&nbsp;</div>
    </div>
    <?php
    $map_center['latitude'] = $REGION['R_Lat'];
    $map_center['longitude'] = $REGION['R_Long'];
    $map_center['zoom'] = $REGION['R_Zoom'];
    $kml_json = json_encode(array());
    require_once 'map_script.php';
    ?>
</section>
<!--Map section end-->
<?php require_once 'include/public/footer.php'; ?>
