<?php
require_once '../include/config.inc.php';
require_once '../include/public-site-functions.inc.php';
require_once '../update-impression-advert.php';
require_once '../include/track-data-entry.php';

$url_parse = parse_url($_SERVER['REQUEST_URI']);
$data = substr($url_parse['path'], 1);
if (substr($data, -1) == '/') {
    $data = substr($data, 0, -1);
}
$url = explode('/', $data);
if (isset($url[2]) && $url[2] > 0) {
    if($REGION['R_Parent'] == 0){
       $sql = "SELECT * FROM tbl_Business_Listing_Photo
        WHERE BLP_BL_ID  = '" . encode_strings($url['2'], $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $row = mysql_fetch_assoc($result);
       $REG = $row['BLP_R_ID'];
    }else{
        $REG = $REGION['R_ID'];
    }
    
    $sql = "SELECT * FROM tbl_Business_Listing 
        LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID AND BLP_R_ID  = '" . encode_strings($REG, $db) . "'
        WHERE BL_ID  = '" . encode_strings($url['2'], $db) . "'";
    $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($resultTMP);
    //check to pass mobile users to mobile version
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
        if (isset($url[3]) && $url[3] != '') {
            $url3 = $url[3];
        }else{
            $url3 = '';
        }
        header('Location: http://' . $REGION['R_Domain'] . DOMAIN_MOBILE_REL . 'profile/' . $row['BL_Name_SEO'] . '/' . $row['BL_ID'] . '/' . $url3);
        exit();
    }
}
$activeBiz = $row;
//SEO Tags
$SEOtitle = $activeBiz['BL_SEO_Title'] ? $activeBiz['BL_SEO_Title'] : $activeBiz['BL_Listing_Title'];
$SEOdescription = $activeBiz['BL_SEO_Description'];
$SEOkeywords = $activeBiz['BL_SEO_Keywords'];
//OG Tags for FB Share
$OG_URL = curPageURL();
$OG_Type = 'article';
$OG_Title = $activeBiz['BL_SEO_Title'] ? $activeBiz['BL_SEO_Title'] : $activeBiz['BL_Listing_Title'];
$OG_Description = $activeBiz['BL_SEO_Description'];
$OG_Image = ($activeBiz['BLP_Header_Image'] != "") ? 'http://'. DOMAIN . IMG_LOC_REL . $activeBiz['BLP_Header_Image'] : 'http://'. DOMAIN . IMG_LOC_REL . 'Default-Header.jpg';
$OG_Image_Width = 1600;
$OG_Image_Height = 640;

if (isset($url[3]) && $url[3] == 'gallery.php') {
    require_once 'gallery.php';
} else {
    require_once 'home.php';
    ?>
    <div class="listing-footer">
        <?php
        require '../include/public/footer.php';
        ?>
    </div>
    <?php
}
?>