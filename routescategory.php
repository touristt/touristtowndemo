<?php
require_once 'include/config.inc.php';
require_once 'update-impression-advert.php';
require_once 'include/public-site-functions.inc.php';
if ($REGION['R_My_Route'] == 0) {
    header("Location: /404.php");
    exit();
}
if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $routeCatID = $_REQUEST['id'];
} else {
    header("Location: /404.php");
    exit();
}

$sqlRouteCat = "SELECT * FROM tbl_Route_Category WHERE RC_ID = '" . encode_strings($routeCatID, $db) . "'";
$resRouteCat = mysql_query($sqlRouteCat, $db) or die("Invalid query: $sqlRouteCat -- " . mysql_error());
$RouteCat = mysql_fetch_assoc($resRouteCat);
$SEOtitle = $RouteCat['RC_SEO_Title'];
$SEOdescription = $RouteCat['RC_SEO_Description'];
$SEOkeywords = $RouteCat['RC_SEO_Keywords'];

$sqlRoute = "SELECT IR_ID, IR_Title, IR_KML_Path, IR_Feature_Photo, IR_Start_Point, IR_End_Point, IR_Distance, IR_Surface, IR_Introduction_Text  FROM tbl_Individual_Route 
            INNER JOIN tbl_Individual_Route_Category ON IR_ID = IRC_IR_ID 
            WHERE IRC_RC_ID = '" . encode_strings($RouteCat['RC_ID'], $db) . "'
            GROUP BY IR_ID
            ORDER BY IRC_Order ASC";
$resRoute = mysql_query($sqlRoute, $db) or die("Invalid query: $sqlRoute -- " . mysql_error());

//check to pass mobile users to mobile version
$useragent = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
    header('Location: http://' . $REGION['R_Domain'] . DOMAIN_MOBILE_REL . 'routes/' . clean($RouteCat['RC_Name']) . '/' . $RouteCat['RC_ID']);
    exit;
}
?>
<?php require_once 'include/public/header.php'; ?>
<!--Slider Start-->
<section class="main_slider">
    <div class="slider_wrapper">
        <div class="slider_wrapper_video">
            <div class="sliderCycle" data-cycle-loader="wait" data-cycle-fx=fade data-cycle-timeout=0 data-cycle-prev="#prev" data-cycle-next="#next">
                <?php
                $i = 1;
                $show_default = true; //if no image, show default
                $VIDEOID = $RouteCat['RC_Video'];
                $pos = strpos($VIDEOID, '=');
                if ($pos == false) {
                    $video_link = end(explode('/', $VIDEOID));
                } else {
                    $video_link = end(explode("=", $VIDEOID));
                }
                if ($RouteCat['RC_Photo'] != '') {
                    $show_default = false;
                    ?>
                    <div class="slide-images">
                        <?php if ($VIDEOID != "") { ?>
                            <div class="<?php echo"player" . $i . "_wrapper"; ?>" style="position:absolute;opacity: 0;top:0;">
                                <input type="hidden" id="video-link-<?php echo $i; ?>" value="<?php echo $video_link ?>">
                                <div  id="<?php echo "player" . $i; ?>"></div>
                            </div>
                        <?php } ?>
                        <div class="slider-text">
                            <h1 class="show-slider-display"><?php echo $RouteCat['RC_Slider_Title'] ?></h1>
                            <h3 class="show-slider-display"><?php echo $RouteCat['RC_Slider_Description'] ?></h3>
                            <?php if ($VIDEOID != "") { ?>
                                <div class="watch-video play-button show-slider-display">
                                    <div class="slider-button">
                                        <img class="video_icon" src="http://<?php echo DOMAIN ?>/images/videoicon.png" alt="Play">
                                        <a onclick="play_video(<?php echo $i; ?>, '<?php echo"player" . $i; ?>')">Watch full video</a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <img class="slider_image" class="show-slider-display" src="<?php echo IMG_LOC_REL . $RouteCat['RC_Photo'] ?>" alt="<?php echo ($RouteCat['RC_Alt_Tag'] != '') ? $RouteCat['RC_Alt_Tag'] : $RouteCat['RC_Slider_Title']; ?>">
                    </div>
                    <?php
                }
                //show default image if no image available
                if ($show_default) {
                    ?>
                    <div class="slide-images" style="<?php echo $image_slider_style ?>">
                        <img class="slider_image" class="show-slider-display" src="<?php echo IMG_LOC_REL . $default_header_image; ?>" alt="<?php echo ($RouteCat['RC_Alt_Tag'] != '') ? $RouteCat['RC_Alt_Tag'] : $RouteCat['RC_Slider_Title']; ?>">
                    </div>
                    <?php
                }
                $i++;
                ?>
            </div>
            <input type="hidden" id="total_players" value="<?php echo $i; ?>">
            <div class=center>
                <span id=prev></span>
                <span id=next></span>
            </div>
            <?php if ($THEME['TO_Slider_Overlay'] != "") { ?>
                <div class="image_overlay_img">
                    <div class="image_overlay">
                        <img src="<?php echo IMG_ICON_REL . $THEME['TO_Slider_Overlay']; ?>" alt="Slider Overlay">
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<!--Slider End-->
<!--Description Start-->
<section class="description theme-description">
    <div class="description-wrapper">
        <div class="description-inner">
            <h1 class="heading-text"><?php echo (($RouteCat['RC_Name']) != '' ? $RouteCat['RC_Name'] : '') ?></h1>
            <div class="site-text ckeditor-anchors">
                <?PHP if ($RouteCat['RC_Description']) { ?>
                    <p><?PHP echo ($RouteCat['RC_Description']); ?></p>
                <?PHP } ?>
            </div>
        </div>
    </div>
</section>
<!--Description End-->
<!-- Routes Categories -->
<section class="section_stories" class="padding-bottom-none border-none">
    <div class="grid-wrapper">
        <div class="grid-inner padding-bottom-none slider">
            <div class="feature-stories">
                <?php
                $kml_array = array();
                while ($rowRoute = mysql_fetch_array($resRoute)) {
                    if ($rowRoute['IR_Feature_Photo'] != '') {
                        $individual_route_image = $rowRoute['IR_Feature_Photo'];
                    } else {
                        $individual_route_image = $default_header_image;
                    }
                    $kml_array[] = array(
                        'name' => str_replace(' ', '', $rowRoute['IR_Title']),
                        'url' => 'http://' . DOMAIN . MAP_LOC_REL . $rowRoute['IR_KML_Path']
                    );
                    ?>
                    <div class="feature-story">
                        <a href="/route/<?php echo clean($rowRoute['IR_Title']) ?>/<?php echo $rowRoute['IR_ID']; ?>/">
                            <img src="<?php echo IMG_LOC_REL . $individual_route_image ?>" width="100%" height="auto" alt="<?php echo $rowRoute['IR_Title'] ?>" />
                        </a>
                        <a href="/route/<?php echo clean($rowRoute['IR_Title']) ?>/<?php echo $rowRoute['IR_ID']; ?>/">
                            <h1 align="center" class="heading-text"><?php echo $rowRoute['IR_Title'] ?></h1>
                        </a>
                        <div class="story-description">
                            <div class="row">
                                <?php if ($rowRoute['IR_Start_Point'] != '') { ?>
                                    <div class="half-row">
                                        <div class="row-title">Start Point:</div>
                                        <div class="row-data"><?php echo $rowRoute['IR_Start_Point'] ?></div>
                                    </div>
                                    <?php
                                }
                                if ($rowRoute['IR_End_Point'] != '') {
                                    ?>
                                    <div class="half-row">
                                        <div class="row-title">End Point:</div>
                                        <div class="row-data"><?php echo $rowRoute['IR_End_Point'] ?></div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="row">
                                <div class="row-title">Distance:</div>
                                <div class="row-data"><?php echo $rowRoute['IR_Distance'] ?></div>
                            </div>
                            <div class="row">
                                <div class="row-title">Surface:</div>
                                <div class="row-data"><?php echo $rowRoute['IR_Surface'] ?></div>
                            </div>
                        </div>
                        <?php
                        if ($rowRoute['IR_Introduction_Text'] != '') {
                            $string = preg_replace('/(.*)<\/p[^>]*>/i', '$1', $rowRoute['IR_Introduction_Text']);
                            ?>
                            <div class="story-description">
                                <?php echo $string ?>
                                <a href="/route/<?php echo clean($rowRoute['IR_Title']) ?>/<?php echo $rowRoute['IR_ID']; ?>/">
                                    Read More
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>  
    </div>
</section>
<!-- Routes Categories end -->
<!--Map section-->
<section class="description theme-description margin-bottom-none maps">
    <?php require_once 'include/public/map_filters.php'; ?>
    <div class="map_wrapper">
        <?PHP
        if ($REGION['R_Parent'] == 0) {
            $REG = '';
        } else {
            $REG = "AND BLP_R_ID = " . $REGION['R_ID'];
        }
        $sqlMap = "SELECT DISTINCTROW BL_ID, BL_Listing_Title, BL_Name_SEO, BL_Lat, BL_Long, BL_Street, BL_Town, BLP_Photo, BL_Photo_Alt, 
                    RC.RC_Name, RC.RC_Map_Icon as subcat_icon, RC1.RC_Map_Icon as cat_icon FROM tbl_Business_Listing 
                    LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
                    LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
                    INNER JOIN tbl_Region_Category RC ON RC.RC_C_ID = BLC_C_ID AND RC.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                    INNER JOIN tbl_Region_Category RC1 ON RC1.RC_C_ID = BLC_M_C_ID AND RC1.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                    INNER JOIN tbl_Individual_Route_Map_Listings ON IRML_BL_ID = BL_ID 
                    INNER JOIN tbl_Individual_Route ON IR_ID = IRML_IR_ID
                    INNER JOIN tbl_Individual_Route_Category ON IRC_IR_ID = IR_ID
                    $SEASONS_JOIN
                    WHERE IRC_RC_ID = '" . encode_strings($RouteCat['RC_ID'], $db) . "' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing='1' AND RC.RC_Status = 0 AND RC1.RC_Status = 0
                    $REG $SEASONS_WHERE $include_free_listings_on_map
                    ORDER BY IRML_Order DESC";
        // generate icons for all listings on map regardless of page
        $resMap = mysql_query($sqlMap, $db) or die(mysql_error() . ' - ' . $sqlMap);
        $markers = array();
        while ($listMap = mysql_fetch_assoc($resMap)) {
            if ($listMap['BLP_Photo'] != '') {
                $image = '<div class="thumbnail"><img src="' . (IMG_LOC_REL . $listMap['BLP_Photo']) . '" alt="' . htmlspecialchars(trim($listMap['BL_Photo_Alt']), ENT_QUOTES) . '" /></div>';
            } else {
                $image = '<div class="thumbnail"><img src="' . (IMG_LOC_REL . $default_thumbnail_image) . '" alt="' . htmlspecialchars(trim($listMap['BL_Listing_Title']), ENT_QUOTES) . '" /></div>';
            }
            if ($listMap['subcat_icon'] != '') {
                $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $listMap['subcat_icon'];
            } elseif ($listMap['cat_icon'] != '') {
                $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $listMap['cat_icon'];
            } else {
                $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . 'default.png';
            }
            if ($listMap['BL_Lat'] && $listMap['BL_Long']) {
                $markers[] = array(
                    'id' => $listMap['BL_ID'],
                    'lat' => $listMap['BL_Lat'],
                    'lon' => $listMap['BL_Long'],
                    'name' => $listMap['BL_Listing_Title'],
                    'path' => '/profile/' . $listMap['BL_Name_SEO'] . '/' . $listMap['BL_ID'] . '/',
                    'icon' => $icon,
                    'main_photo' => $image,
                    'address' => htmlspecialchars(trim($listMap['BL_Street']), ENT_QUOTES),
                    'town' => htmlspecialchars(trim($listMap['BL_Town']), ENT_QUOTES)
                );
            }
        }
        $markers = json_encode($markers);
        ?>
        <div id="map_canvas">&nbsp;</div>
        <?php
        $map_center['latitude'] = $RouteCat['RC_Lat'];
        $map_center['longitude'] = $RouteCat['RC_Long'];
        $map_center['zoom'] = $RouteCat['RC_Zoom'];
        $kml_json = json_encode($kml_array);
        require_once 'map_script.php';
        ?>
    </div>
</section>
<!--Map section end-->
<?php require_once 'include/public/footer.php'; ?>