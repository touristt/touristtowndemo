<?php
require_once 'include/config.inc.php';
require_once 'include/public-site-functions.inc.php';

$routeID = $_REQUEST['id'];
$sqlGallery = "SELECT IRG_IR_ID, IRG_Order, IRG_Photo, IRG_Title FROM tbl_Individual_Route_Gallery WHERE IRG_IR_ID = '" . encode_strings($routeID, $db) . "' ORDER BY IRG_Order";
$resGallery = mysql_query($sqlGallery, $db) or die("Invalid query: $sqlGallery -- " . mysql_error());
$startingSlide = 0;
if (isset($_REQUEST['order']) && $_REQUEST['order'] != '') {
  $startingSlide = $_REQUEST['order'] - 1;
}
?>
<script type="text/javascript">
  $(document).ready(function () {
    $('.galleryCycle').cycle({
      fx: 'scrollHorz',
      next: '#next',
      prev: '#prev',
      width: 1000,
      height: 500,
      fit: 1,
      startingSlide: <?php echo $startingSlide ?>
    });
  });
</script>
<div id="fb-root"></div>

<script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id))
      return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=162672920596261&version=v2.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>
<!-- Jssor Slider Begin -->
<!-- You can move inline styles to css file or css block. -->

<div class="galleryCycle">
  <?php
  while ($rowGallery = mysql_fetch_assoc($resGallery)) {
    ?>
    <div class="gallery-slide-images">
      <?php if ($rowGallery['IRG_Photo'] != "") { ?>
        <img class="gallery_slider_image" class="show-slider-display" src="<?php echo IMG_LOC_REL . $rowGallery['IRG_Photo'] ?>" alt="<?php echo $rowGallery['IRG_Title'] ?>">
      <?php } else { ?>
        <img class="gallery_slider_image" class="show-slider-display" src="<?php echo IMG_LOC_REL . $rowGallery['IRG_Photo'] ?>" alt="<?php echo $rowGallery['IRG_Title'] ?>">
      <?php } ?>
      <div class="gallery-slider-text">
        <div class="fb-like" data-href="<?php echo IMG_LOC_REL . $rowGallery['IRG_Photo']; ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
        <div class="slider-caption"><?php echo $rowGallery['IRG_Title']; ?></div>
      </div>
    </div>
    <?php
  }
  ?>
</div>
<div class=center>
  <span id=prev></span>
  <span id=next></span>
</div>