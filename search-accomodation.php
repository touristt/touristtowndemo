<?php
require_once 'include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
require 'include/public/header.php';

$_SESSION['CATEGORY'] = 4;
$showAccomodationSearch = true;

if ($REGION['R_Parent'] == 0) {
    $regionsSQL = "";
    if ($regionsSQL == "") {
        $sql = "SELECT R_ID, R_Parent FROM tbl_Region WHERE R_Parent = '" . encode_strings($REGION['R_ID'], $db) . "'";
        $resultRtmp = mysql_query($sql, $db)
                or die("Invalid query: $sql -- " . mysql_error());
        while ($rowRtmp = mysql_fetch_assoc($resultRtmp)) {
            $regionsSQL .= $regionsSQL ? ',' : '';
            $regionsSQL .= $rowRtmp['R_ID'];
        }
        $regionsSQL = "IN (" . $regionsSQL . ")";
    }
} else {
    $regionsSQL = "= '" . encode_strings($REGION['R_ID'], $db) . "'";
}

$sql = "SELECT RC_Image, RC_Alt, RC_Description
        FROM tbl_Category as C1 
        LEFT JOIN tbl_Category as C2 ON C1.C_Parent = C2.C_ID 
        LEFT JOIN tbl_Region_Category ON RC_C_ID = C1.C_ID AND RC_R_ID = " . encode_strings($REGION['R_ID'], $db) . " 
        WHERE C1.C_ID = '" . encode_strings(4, $db) . "' 
        ORDER BY C1.C_Name LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeCat = mysql_fetch_assoc($result);
?>
<div class="main-content">
    <div class="home">
        <div id="headingParent" style="padding:0;margin:0;">
            <div id="videoContent" style="display:none; height:425px;width:759px;"></div>
            <?PHP if ($activeCat['RC_Image']) { ?>
                <img id="subsection-header" onclick="BCL.addPlayer()" class="subsection-header temp-width-header" src="<?php echo IMG_LOC_REL . $activeCat['RC_Image'] ?>" alt="<?php echo $activeCat['RC_Alt'] ?>" longdesc="<?php echo $activeCat['RC_Alt'] ?>" />
            <?PHP } ?>
        </div>
        <?PHP if ($activeCat['RC_Description']) { ?>
            <p  class="region-desc"><?PHP echo nl2br($activeCat['RC_Description']); ?></p>
        <?PHP } ?>
        <div class="font-size-cat main-content-heading">Accomodation Search</div>
    </div>
    <div class="content-right">
        <div class="wide-list destinations searchPagination">
            <ul class="thumbnails" style="border-top: 1px dotted #c2c2c2;">
                <?php
                $regionList = '';
                if ($REGION['R_Parent'] == 0) {
                    $sql = "SELECT R_ID, R_Parent FROM tbl_Region WHERE R_Parent = '" . encode_strings($REGION['R_ID'], $db) . "'";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    $first = true;
                    $regionList .= "(";
                    while ($row = mysql_fetch_assoc($result)) {
                        if ($first) {
                            $first = false;
                        } else {
                            $regionList .= ",";
                        }
                        $regionList .= $row['R_ID'];
                    }
                    $regionList .= ")";
                }
                $rid = $REGION['R_ID'];

                if (is_array($_GET['locations'])) {
                    if (count($_GET['locations']) > 1) {
                        $regionList = "(";
                        $first = true;
                        foreach ($_REQUEST['locations'] as $val) {
                            if ($first) {
                                $first = false;
                            } else {
                                $regionList .= ",";
                            }
                            $regionList .= $val;
                        }
                        $regionList .= ")";
                    } elseif (count($_GET['locations']) == 1) {
                        $rid = $_GET['locations'][0];
                        $regionList = '';
                    }
                }

                $sql = "SELECT DISTINCTROW BL_ID, BL_Listing_Title, BL_Description, BL_Name_SEO, BL_Photo, BL_Photo_Alt, BL_Street, BL_Town, BL_Phone, 
                        BL_Email, BL_Lat, BL_Long, BL_Province, BL_PostalCode, BL_ChamberMember, BLC_M_C_ID, LT_ID
                        FROM tbl_Business_Listing 
                        LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                        LEFT JOIN tbl_Business_Listing_Category_Region ON  BLC_BL_ID = BLCR_BL_ID 
                        LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
                        WHERE BL_C_ID = 4 AND BLCR_BLC_R_ID " . encode_strings(($regionList ? "IN " . $regionList : "= " . $rid), $db) . " ";
                if (is_array($_REQUEST['ammenities'])) {
                    foreach ($_REQUEST['ammenities'] as $val) {
                        if ($val > 0) {
                            $sql .= "AND '" . encode_strings($val, $db) .
                                    "' IN (SELECT BLA_BA_ID FROM tbl_Business_Listing_Ammenity WHERE BLA_BL_ID = tbl_Business_Listing.BL_ID) ";
                        }
                    }
                }
                if (is_array($_REQUEST['rooms'])) {
                    if (count($_REQUEST['rooms'])) {
                        $sql .= " AND (";
                        $rmCount = 0;
                        foreach ($_REQUEST['rooms'] as $val) {

                            if ($val > 0) {
                                if ($rmCount)
                                    $sql .= 'OR ';
                                $sql .= "'" . encode_strings($val, $db) .
                                        "' IN (SELECT BLA_BA_ID FROM tbl_Business_Listing_Ammenity WHERE BLA_BL_ID = tbl_Business_Listing.BL_ID) ";
                            }
                            $rmCount++;
                        }
                        $sql .= ")";
                    }
                }
                if ($_REQUEST['subCategory'] > 0) {
                    $sql .= "AND '" . encode_strings($_REQUEST['subCategory'], $db) .
                            "' = BLC_C_ID ";
                }
                $sql .= "ORDER BY BL_Points DESC, LT_Order DESC, BL_Listing_Title";

                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                while ($row = mysql_fetch_assoc($result)) {
                    ?>
                    <li class="pager">
                        <div class="thumbnail padding-sub-category listings-list">
                            <?php if ($row['LT_ID'] <> 1) { ?>
                                <a href="/profile/<?php echo $row['BL_Name_SEO'] ?>/<?php echo $row['BL_ID'] ?>/"> 
                                <?PHP } ?>
                                <img src="<?php echo ($row['LT_ID'] == 1 || $row['BL_Photo'] == '') ? '/images/Listing-NoPhoto.jpg' : (IMG_LOC_REL . $row['BL_Photo']) ?>" alt="<?php echo $row['BL_Photo_Alt'] ?>" longdesc="<?php echo $row['BL_Listing_Title'] ?>" />
                                <?php if ($row['LT_ID'] <> 1) { ?>
                                </a> 
                            <?PHP } ?>
                            <div class="copy thumbnails-top-10">
                                <h5>
                                    <?php
                                    if ($row['LT_ID'] <> 1) {
                                        echo '<a href="/profile/' . $row['BL_Name_SEO'] . '/' . $row['BL_ID'] . '/">';
                                    }
                                    echo $row['BL_Listing_Title'];
                                    if ($row['LT_ID'] <> 1) {
                                        echo '</a>';
                                    }
                                    ?>
                                </h5>
                                <?PHP
                                if ($row['LT_ID'] == 1 && $row['BL_Street']) { // !FREE
                                    ?>
                                    <p style="margin-top: 10px;"><?php echo $row['BL_Street'] ?>, <?php echo $row['BL_Town'] ?></p>
                                    <?PHP
                                } else {
                                    $text = limit_text($row['BL_Description'], 100);
                                    ?>
                                    <p style="margin-top: 10px;"><?php echo $text; //REPLACE                         ?>
                                        <?PHP
                                        if ($text <> $row['BL_Description']) {
                                            ?>
                                            <a href="/profile/<?php echo $row['BL_Name_SEO'] ?>/">More details...</a>
                                            <?PHP
                                        }
                                        ?>
                                    </p>
                                    <?PHP
                                }
                                ?>
                                <p>
                                    <?PHP
                                    if ($row['BL_Phone'] != '') {
                                        echo 'Phone. ' . $row['BL_Phone'] . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp';
                                    }
                                    if ($row['BL_Email'] != '') {
                                        echo '<a href="mailto:' . $row['BL_Email'] . '">Email Now</a>';
                                    }
                                    ?>
                                </p>
                            </div>
                            <div class="tt-actions">
                                <?PHP
                                if ($row['LT_ID'] <> 1) { // !FREE
                                    ?>
                                    <ul class="unstyled inline" style="margin-top:8px;">
                                        <?PHP
                                        if ($row['LT_ID'] > 2) { // Silver or Gold
                                            ?>
                                            <li>
                                                <div class="add-item-tripplanner"><a href="#" class="planner-link" id="<?php echo $row['BLC_M_C_ID'] . '_' . $row['BL_ID'] ?>"><span class="tripplanner-plus"></span><div class="margin-top">&nbsp;add to my trip</div></a></a>
                                            </li>
                                            <?PHP
                                        }
                                        if ($row['LT_ID'] == 4 && $row['BL_Lat'] && $row['BL_Long']) { // Gold
                                            ?>
                                            <li>
                                                <?PHP if ($row['BL_Lat'] && $row['BL_Long']) { ?>
                                                    <a class="map-link various fancybox.iframe"  href="http://maps.google.ca/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<?php echo $row['BL_Lat'] ?>,<?php echo $row['BL_Long'] ?>+(<?php echo $row['BL_Listing_Title'] ?>)&amp;aq=&amp;sll=<?php echo $row['BL_Lat'] ?>,<?php echo $row['BL_Long'] ?>&amp;sspn=0.027224,0.077162&amp;ie=UTF8&amp;t=m&amp;spn=0.022178,0.025663&amp;z=14&amp;iwloc=A&amp;output=embed"><span class="showonmap-icon"></span><div>&nbsp;show on map</div></a>
                                                <?PHP } elseif ($row['BL_Street']) { ?>
                                                    <a class="map-link various fancybox.iframe"  href="http://maps.google.ca/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<?php echo $row['BL_Street'] ?>,<?php echo $row['BL_Town'] ?>+<?php echo $row['BL_Province'] ?> <?php echo $row['BL_PostalCode'] ?>&amp;aq=&amp;sspn=0.027224,0.077162&amp;ie=UTF8&amp;t=m&amp;spn=0.022178,0.025663&amp;z=14&amp;iwloc=A&amp;output=embed"><span class="showonmap-icon"></span><div>&nbsp;show on map</div></a>
                                                <?PHP } ?>		</li>
                                            <?PHP
                                        }
                                        ?>
                                        <div class="aminities-li">
                                            <li class="amminities-inside">
                                                <?PHP
                                                $sql = "SELECT AI_Image, AI_Name FROM tbl_Business_Listing_Ammenity LEFT JOIN tbl_Ammenity_Icons ON AI_ID = BLA_BA_ID 
                                                        WHERE BLA_BL_ID = '" . encode_strings($row['BL_ID'], $db) . "' AND AI_Image <> ''";
                                                $resultAI = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                                $iCount = mysql_num_rows($resultAI);
                                                $i = 0;
                                                if ($row['BL_ChamberMember']) {
                                                    $i = 1;
                                                    ?>
                                                    <img style="height: 25px; width: 25px;" src="/images/cc-icon.gif" alt="Chamber of Commerce" width="25" height="25" />
                                                    <?PHP
                                                }
                                                while (($rowAI = mysql_fetch_assoc($resultAI)) && $i < 8) {
                                                    ?>
                                                    <img style="height: 25px; width: 25px;" src="<?php echo IMG_LOC_REL ?>ammenity-icons/<?php echo $rowAI['AI_Image'] ?>" alt="<?php echo $rowAI['AI_Name'] ?>" width="25" height="25" />
                                                    <?PHP
                                                    $i++;
                                                }
                                                ?>
                                            </li>
                                            <?PHP
                                            if ($iCount > 8) {
                                                ?>
                                                <li class="margin-top amminities-inside-see">
                                                    <a href="/profile/<?php echo $row['BL_Name_SEO'] ?>/" style = "color: #0D78C6">see more...</a>
                                                </li>
                                                <?PHP
                                            }
                                            if ($i == 0 && $row['LT_ID'] >= 4) {
                                                echo '<img style="border: 0px; box-shadow:0px;height: 25px; width: 25px;" src="/images/spacer.gif">';
                                            }
                                            ?>
                                        </div>
                                    </ul>
                                    <?PHP
                                }
                                ?>
                            </div>
                        </div>
                    </li>
                <?PHP } ?>
            </ul>

        </div> <!-- .destination-list -->
        <?PHP
        if ($REGION['R_Parent'] == 1) {
            ?>
            <p><a href="http://visitsaugeenshores.ca/search-accomodation.php?<?php echo $_SERVER["QUERY_STRING"] ?>">View All Listings</a></p>
            <?PHP
        }
        ?>
    </div> <!-- .content-right -->
    <div class="right-advertise">
        <?php
        require 'nav-left.php';
        include 'advertise.php';
        ?>
    </div>
</div> <!-- .content-right-outer -->

<?php require 'include/public/footer.php'; ?>
