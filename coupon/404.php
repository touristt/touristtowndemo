<?php
require_once '../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
require_once 'include/public/header.php';

$sql_tbl_region_404 = "SELECT R_ID, R_Name, R_Type, RP_ID, RP_Photo, RP_Photo_Title, RP_Title, RP_Description FROM tbl_Region LEFT JOIN tbl_Region_404 ON R_ID = RP_RID 
            WHERE R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' LIMIT 1";
$sql_tbl_region_404_result = mysql_query($sql_tbl_region_404) or die("Invalid query: $sql_tbl_region_404 -- " . mysql_error());
$num_of_region_404 = mysql_fetch_array($sql_tbl_region_404_result);

$sql_slider_404 = "SELECT BFC_ID, BFC_Thumbnail, BFC_Title, BL_SEO_Title, BL_Name_SEO, BFC_Description, tbl_Business_Listing.BL_ID, tbl_Business_Listing.BL_Listing_Title 
FROM tbl_Business_Feature_Coupon 
LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID 
LEFT JOIN tbl_Category ON C_ID = BFCCM_C_ID 
LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_C_ID ='' 
LEFT JOIN tbl_Business_Listing  ON BL_ID = BFC_BL_ID
INNER JOIN tbl_404_Listing  ON tbl_404_Listing.BL_ID = tbl_Business_Listing.BL_ID 
WHERE BFC_Status = 1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date =0000-00-00) AND R_ID = 27 GROUP BY BL_ID ORDER BY BL_Listing_Title";
$sql_slider_404_result = mysql_query($sql_slider_404, $db) or die("Invalid query: $sql_slider_404 -- " . mysql_error());
?>
<div class="content-wrapper error-page">
    <div class="content-wrapper-inner error-page">
        <!-- <div class="top-bar">Ooops. The page you want can't be found.</div> -->
        <!--Slider Start-->
        <section class="main_slider">
            <div class="slider_wrapper">
                <div class="slider_wrapper_inside">
                    <div class="sliderCycle" data-cycle-loader="wait" data-cycle-fx=fade data-cycle-timeout=0 data-cycle-prev="#prev" data-cycle-next="#next">
                        <?php
                        if ($num_of_region_404['RP_Photo'] != '') {
                            ?>
                            <div class="slide-images" style="<?php echo $image_slider_style ?>"> 
                                <div class="slider-text">
                                    <h1 class="show-slider-display"><?php echo $row_slider['RP_Photo_Title'] ?></h1>
                                </div>
                                <img class="slider_image" class="show-slider-display" src="http://<?php echo DOMAIN . IMG_LOC_REL . $row_slider['RP_Photo'] ?>" alt="">
                            </div>
                            <?php
                        } else {
                            echo '<div class="slide-images"><img class="slider_image" class="show-slider-display" src="http://' . DOMAIN . IMG_LOC_REL . $default_header_image . '" alt="" /></div>';
                        }
                        ?>
                    </div>
                </div>
            </div>

        </section>
        <!--Slider End-->
        <div class="page-content error-page">
            <!-- 111 -->
            <!--Thumbnail Grid-->
            <section class="thumbnail-grid sub-cat-margin border-none error-page">
                <div class="grid-wrapper error-page">
                    <div class="grid-inner border-none padding-bottom-none error-page">
                        <?php
                        $count_row_404_region = mysql_num_rows($sql_slider_404_result);
                        $i = 0;
                        if ($count_row_404_region > 0) {
                            while ($show_row_404_region = mysql_fetch_array($sql_slider_404_result)) {
                                $i++;
                                if ($show_row_404_region['BFC_Thumbnail'] != '') {
                                    $listing_image = $show_row_404_region['BFC_Thumbnail'];
                                } else {
                                    $listing_image = $default_thumbnail_image;
                                }
                                if ($i == 1) {
                                    echo "<div class='thumbnails static-thumbs'>";
                                }
                                ?>
                                <div class="thumb-item">
                                    <a href="/profile/<?php echo (($show_row_404_region['BL_SEO_Title'] != "") ? clean($show_row_404_region['BL_SEO_Title']) : $show_row_404_region['BL_Name_SEO']) ?>/<?php echo $show_row_404_region['BFC_ID'] ?>/">
                                        <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $listing_image ?>" />
                                        <h3 class="thumbnail-heading"><?php echo ($show_row_404_region['BL_Listing_Title'] != '') ? $show_row_404_region['BL_Listing_Title'] : $show_row_404_region['BL_Listing_Title'] ?></h3>
                                        <h3 class="thumbnail-desc"><?php echo $show_row_404_region['BL_Listing_Title']; ?></h3>
                                    </a>
                                    <a href="/profile/<?php echo (($show_row_404_region['BL_SEO_Title'] != "") ? clean($show_row_404_region['BL_SEO_Title']) : $show_row_404_region['BL_Name_SEO']) ?>/<?php echo $show_row_404_region['BFC_ID'] ?>/">
                                        <h3 class="thumbnail-coupon"><?php echo $show_row_404_region['BL_SEO_Title'] ?></h3>
                                    </a> 
                                </div>
                                <?php
                                if ($i == 3) {
                                    echo '</div>';
                                    $i = 0;
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<?php require_once 'include/public/footer.php'; ?>