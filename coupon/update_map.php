<?php

require_once '../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
//whether coming from map page or not
$is_map = $_REQUEST['is_map'];

$where = "";
//For categories filter
if (isset($_REQUEST['sub_category'])) {
    $i = 0;
    $where .= "AND (";
    foreach ($_REQUEST['sub_category'] as $subcategory) {
        foreach ($subcategory as $subcat) {
            if ($i == 0) {
                $where .= "(BFCCM_C_ID = $subcat)";
            } else {
                $where .= " OR (BFCCM_C_ID = $subcat)";
            }
            $i++;
        }
    }
    $where .= ")";
}
if ($REGION['R_Include_Free_Listings_On_Map'] == 1) {
    $include_free_listings_on_map = '';
} else {
    $include_free_listings_on_map = ' AND BL_Listing_Type > 1';
}

//Getting Region Theme
$getTheme = "SELECT TO_Default_Thumbnail_Desktop FROM  `tbl_Theme_Options` WHERE TO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
$themeRegion = mysql_query($getTheme, $db) or die("Invalid query: $getTheme -- " . mysql_error());
$THEME = mysql_fetch_assoc($themeRegion);

//Defualt Thumbnail Images
if ($THEME['TO_Default_Thumbnail_Desktop'] != '') {
    $default_thumbnail_image = $THEME['TO_Default_Thumbnail_Desktop'];
} else {
    $default_thumbnail_image = 'Default-Thumbnail-Desktop.jpg';
}

$sql = "SELECT BL_ID, BL_Listing_Title, BL_Name_SEO, BL_Lat, BL_Long, BL_Street, BL_Town, BFC_ID, BFC_Title, BFC_Thumbnail, BFCCM_C_ID, 
        RC.RC_Map_Icon as subcat_icon, RC1.RC_Map_Icon as cat_icon FROM tbl_Business_Feature_Coupon
        LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
        LEFT JOIN tbl_Category C ON C.C_ID = BFCCM_C_ID
        LEFT JOIN tbl_Category C1 ON C1.C_ID = C.C_Parent
        LEFT JOIN tbl_Region_Category RC ON RC.RC_C_ID = C.C_ID AND RC.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
        LEFT JOIN tbl_Region_Category RC1 ON RC1.RC_C_ID = C1.C_ID AND RC1.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
        LEFT JOIN tbl_Business_Listing ON BL_ID = BFC_BL_ID WHERE  BFC_Status=1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date =0000-00-00) 
        AND hide_show_listing='1' $where $include_free_listings_on_map GROUP BY BFC_ID ORDER BY BFC_ID DESC";
$markers = array();
// generate icons for all listings on map regardless of page
$result_map = mysql_query($sql);
$counter = 0;
$sidebar = '';
while ($list = mysql_fetch_array($result_map, MYSQL_ASSOC)) {
    if ($list['subcat_icon'] != '') {
        $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $list['subcat_icon'];
    } elseif ($list['cat_icon'] != '') {
        $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $list['cat_icon'];
    } else {
        $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . 'default.png';
    }
    if ($list['BL_Lat'] && $list['BL_Long']) {
        // Add tooltip content
        if ($list['BFC_Thumbnail'] != '') {
            $image = '<div class="thumbnail"><img width="130" height="90" src="http://' . DOMAIN . (IMG_LOC_REL . $list['BFC_Thumbnail']) . '" alt="" /></div>';
        } else {
            $image = '<div class="thumbnail"><img width="130" height="90" src="http://' . DOMAIN . IMG_LOC_REL . $default_thumbnail_image . '" alt="" /></div>';
        }

        $markers[] = array(
            'id' => $list['BFC_ID'],
            'lat' => $list['BL_Lat'],
            'lon' => $list['BL_Long'],
            'name' => $list['BFC_Title'],
            'path' => '/profile/' . (($list['BFC_Title'] != "") ? clean($list['BFC_Title']) : $list['BL_Name_SEO']) . '/' . $list['BFC_ID'] . '/',
            'icon' => $icon,
            'main_photo' => $image,
            'address' => htmlspecialchars(trim($list['BL_Street']), ENT_QUOTES),
            'town' => htmlspecialchars(trim($list['BL_Town']), ENT_QUOTES)
        );
    }
    //show only limited records in right panel only on map page 
    if ($is_map == 1) {
        if ($counter < $REGION['R_Map_Listings_Limit']) {
            $photo = ($list['BFC_Thumbnail'] == '') ? 'http://' . DOMAIN . IMG_LOC_REL . $default_thumbnail_image : ('http://' . DOMAIN . IMG_LOC_REL . $list['BFC_Thumbnail']);
            $sidebar .= '<div class="thumbnails static-thumbs">
                      <div class="thumb-item">
                        <a href="/profile/' . (($List['BFC_Title'] != "") ? clean($list['BFC_Title']) : $list['BL_Name_SEO']) . '/' . $list['BFC_ID'] . '/"> 
                          <img src="' . $photo . '" alt="' . $list['BFC_Title'] . '" />
                          <h3 class="thumbnail-heading">' . $list['BFC_Title'] . '</h3>
                          <h3 class="thumbnail-desc">' . $list['BL_Listing_Title'] . '</h3>
                        </a> 
                      </div>
                    </div>';
        }
    }
    $counter++;
}
$return['markers'] = $markers;
$return['sidebar'] = $sidebar;
print json_encode($return);
exit;
?>