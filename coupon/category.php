<?php
require_once '../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
//check to pass mobile users to mobile version
$url_parse = parse_url($_SERVER['REQUEST_URI']);
$data = substr($url_parse['path'], 1);
if (substr($data, -1) == '/') {
    $data = substr($data, 0, -1);
}
$url = explode('/', $data);
$useragent = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
    //get parent category
    $sql_child = "SELECT C_ID, C_Name_SEO, C_Parent FROM tbl_Category WHERE C_ID = " . $_REQUEST['c_id'];
    $res_child = mysql_query($sql_child);
    $child = mysql_fetch_assoc($res_child);
    $sql_p = "SELECT C_ID, C_Name_SEO FROM tbl_Category WHERE C_ID = " . $child['C_Parent'];
    $res_p = mysql_query($sql_p);
    $p = mysql_fetch_assoc($res_p);
    $url_redirect = 'http://' . $REGION['R_Domain'] . DOMAIN_MOBILE_REL . $p['C_Name_SEO'] . '/' . $child['C_Name_SEO'] . '/';
    header('Location: ' . $url_redirect);
    exit();
}
require_once 'include/public/header.php';

//Getting Coupons from the subcategory
$sqlGetCoupons = "SELECT BFC_ID, BFC_Thumbnail, BFC_Title, RC_Name, C_Name, BL_Listing_Title, BL_Name_SEO, BL_ID FROM tbl_Business_Feature_Coupon                   
                    LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
                    LEFT JOIN tbl_Category ON C_ID = BFCCM_C_ID
                    LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                    LEFT JOIN tbl_Business_Listing ON BL_ID = BFC_BL_ID 
                    WHERE BFCCM_C_ID = '" . encode_strings($_REQUEST['c_id'], $db) . "' and BFC_Status=1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date =0000-00-00) AND hide_show_listing='1' ORDER BY BFC_ID DESC";
$resCoupons = mysql_query($sqlGetCoupons);
?>
<!--Thumbnail Grid-->
<section class="thumbnail-grid padding-none border-none">
    <div class="grid-wrapper">
        <div class="grid-inner border-none padding-bottom-none">
            <?php
            $i = 0;
            while ($rowCoupon = mysql_fetch_array($resCoupons)) {
                $i++;
                if ($rowCoupon['BFC_Thumbnail'] != '') {
                        $listing_image = $rowCoupon['BFC_Thumbnail'];
                    } else {
                        $listing_image = $default_thumbnail_image;
                    }
                if ($i == 1) {
                    echo "<div class='thumbnails static-thumbs'>";
                }
                ?>   
                <div class="thumb-item">
                    <a href="/profile/<?php echo (($rowCoupon['BFC_Title'] != "") ? clean($rowCoupon['BFC_Title']) : $rowCoupon['BL_Name_SEO']) ?>/<?php echo $rowCoupon['BFC_ID'] ?>/">
                        <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo $rowCoupon['BFC_Title'] ?>"/>
                        <?php if (isset($REGION['R_Show_Hide_Sub_Category_Title']) && $REGION['R_Show_Hide_Sub_Category_Title'] == 1) { ?>
                            <h3 class="thumbnail-heading coupon-header"><?php echo (($rowCoupon['RC_Name'] != "") ? $rowCoupon['RC_Name'] : $rowCoupon['C_Name']) ?></h3>
                        <?php } ?>
                        <h3 class="thumbnail-desc"><?php echo $rowCoupon['BL_Listing_Title'] ?></h3>
                    </a>
                    <a href="/profile/<?php echo (($rowCoupon['BFC_Title'] != "") ? clean($rowCoupon['BFC_Title']) : $rowCoupon['BL_Name_SEO']) ?>/<?php echo $rowCoupon['BFC_ID'] ?>/">
                        <h3 class="thumbnail-coupon"><?php echo $rowCoupon['BFC_Title'] ?></h3>
                    </a>
                </div>             
                <?php
                if ($i == 3) {
                    echo '</div>';
                    $i = 0;
                }
            }
            ?>
        </div>
    </div>
</section>
<?php require_once 'include/public/footer.php'; ?>