<?php
require_once '../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
require_once 'include/public/header.php';

$sql_slider = "SELECT RTP_Title, RTP_Description, RTP_Photo, RTP_Video_Link FROM tbl_Region_Themes_Photos WHERE RTP_RT_RID ='" . encode_strings($REGION['R_ID'], $db) . "' ORDER BY RTP_Order";
$result_slider = mysql_query($sql_slider, $db) or die("Invalid query: $sql_slider -- " . mysql_error());
//Homepage Painting
$sqlHomepagePainting = "SELECT * FROM tbl_Homepage_Paintings WHERE HP_R_ID ='" . encode_strings($REGION['R_ID'], $db) . "'";
$resultHomepagePainting = mysql_query($sqlHomepagePainting, $db) or die("Invalid query: $sqlHomepagePainting -- " . mysql_error());
$rowHomepagePainting = mysql_fetch_array($resultHomepagePainting);
?>
<!--Slider Start-->
<section class="main_slider">
    <div class="slider_wrapper">
        <div class="slider_wrapper_inside">
            <div class="sliderCycle" data-cycle-loader="wait" data-cycle-fx=fade data-cycle-timeout=0 data-cycle-prev="#prev" data-cycle-next="#next">
                <?php
                $first_img = true;
                $i = 1;
                $count_slider = mysql_num_rows($result_slider);
                if ($count_slider > 0) {
                    while ($row_slider = mysql_fetch_array($result_slider)) {
                        $VIDEOID = $row_slider['RTP_Video_Link'];
                        $pos = strpos($VIDEOID, '=');
                        if ($pos == false) {
                            $video_link = end(explode('/', $VIDEOID));
                        } else {
                            $video_link = end(explode("=", $VIDEOID));
                        }
                        if ($row_slider['RTP_Photo'] != '') {
                            if ($first_img) {
                                $image_slider_style = '';
                                $first_img = false;
                            } else {
                                $image_slider_style = 'display:none;';
                            }
                            ?>
                            <div class="slide-images" style="<?php echo $image_slider_style ?>">
                                <?php if ($VIDEOID != "") { ?>
                                    <div class="<?php echo"player" . $i . "_wrapper"; ?>" style="position:absolute;opacity: 0;top:0;">
                                        <input type="hidden" id="video-link-<?php echo $i; ?>" value="<?php echo $video_link ?>">
                                        <div  id="<?php echo "player" . $i; ?>"></div>
                                    </div>
                                <?php } ?>
                                <div class="slider-text">
                                    <h1 class="show-slider-display"><?php echo $row_slider['RTP_Title'] ?></h1>
                                    <h3 class="show-slider-display"><?php echo $row_slider['RTP_Description'] ?></h3>
                                    <?php if ($VIDEOID != "") { ?>
                                        <div class="watch-video play-button show-slider-display">
                                            <div class="slider-button" style="width: 240px;">
                                                <img class="video_icon" src="<?php echo 'http://' . DOMAIN ?>/images/videoicon.png" alt="Play" style="padding-left: 27px;">
                                                <a onclick="play_video(<?php echo $i; ?>, '<?php echo"player" . $i; ?>')">Watch full video</a>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <img class="slider_image" class="show-slider-display" src="http://<?php echo DOMAIN . IMG_LOC_REL . $row_slider['RTP_Photo'] ?>" alt="<?php echo $row_slider['RTP_Title']; ?>">
                            </div>
                            <?php
                        }
                        $i++;
                    }
                } else {
                    ?>
                    <div class="slide-images" style="<?php echo $image_slider_style ?>">
                        <img class="slider_image" class="show-slider-display" src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $default_header_image; ?>" alt="<?php echo $row_slider['RTP_Title']; ?>">
                    </div> 
                    <?php
                }
                ?>
            </div>
            <div class=center>
                <span id=prev></span>
                <span id=next></span>
            </div>
        </div>
    </div>
    <input type="hidden" id="total_players" value="<?php echo $i; ?>">
</section>
<!--Slider End-->
<!--Homepage Painting-->
<?php
if ($REGION['R_Show_Hide_Homepage_Image'] == 1 && $rowHomepagePainting['HP_Homepage_Painting'] != "") {
    $imageHomepagePainting = $rowHomepagePainting['HP_Homepage_Painting'];
    ?>
    <section class="main_slider" style="margin-top: 55px;">
        <div class="slider_wrapper">
            <div class="slider_wrapper_inside">
                <div class="homepage-painting">
                    <?php
                    if ($rowHomepagePainting['HP_Link'] != '') {
                        $linkHomepagePainting = preg_replace('/^www\./', '', $rowHomepagePainting['HP_Link']);
                        $linkHomepagePainting = 'http://' . str_replace(array('http://', 'https://'), '', $linkHomepagePainting);
                        echo '<a class="" href="' . $linkHomepagePainting . '">';
                    }
                    ?>
                    <div class="slide-images">
                        <?php if ($rowHomepagePainting['HP_Title'] != '' || $rowHomepagePainting['HP_Description'] != '') { ?>
                            <div class="slider-text">
                                <?php if ($rowHomepagePainting['HP_Title'] != '') { ?>
                                    <h1 class="show-slider-display"><?php echo $rowHomepagePainting['HP_Title'] ?></h1>
                                <?php } if ($rowHomepagePainting['HP_Description'] != '') { ?>
                                    <h3 class="show-slider-display"><?php echo $rowHomepagePainting['HP_Description']; ?></h3>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <img class="slider_image" class="show-slider-display" src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $imageHomepagePainting ?>" alt="Painting">
                    </div>
                    <?php
                    if ($rowHomepagePainting['HP_Link'] != '') {
                        echo '</a>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<!--Homepage Painting-->
<!--Thumbnail Grid-->
<section class="thumbnail-grid slider-grid">
    <div class="grid-wrapper">

        <?php
        //Getting Sub categories
        $sqlSubCat = "SELECT C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                        WHERE C_Parent = (SELECT C_ID FROM tbl_Category LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID WHERE C_Is_Product_Web = 1 AND RC_Status = 0 AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "') 
                        AND RC_R_ID > 0 AND RC_Status = 0 ORDER BY RC_Order";
        $resSubCat = mysql_query($sqlSubCat);
        $totalSubCat = mysql_num_rows($resSubCat);
        if ($totalSubCat > 0) {
            while ($rowSubCat = mysql_fetch_array($resSubCat)) {
                //Getting Coupons from the subcategory
                $sqlGetCoupons = "SELECT BFC_ID, BFC_Thumbnail, BFC_Title, RC_Name, C_Name, BL_Listing_Title, BL_Name_SEO, BL_ID FROM tbl_Business_Feature_Coupon
                                    LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
                                    LEFT JOIN tbl_Category ON C_ID = BFCCM_C_ID                                    
                                    LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                                    LEFT JOIN tbl_Business_Listing ON BL_ID = BFC_BL_ID 
                                    WHERE BFCCM_C_ID = '" . encode_strings($rowSubCat['C_ID'], $db) . "' and BFC_Status = 1
                                    AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date =0000-00-00) AND hide_show_listing='1' ORDER BY BFC_ID DESC";
                $resCoupons = mysql_query($sqlGetCoupons);
                $totalCoupon = mysql_num_rows($resCoupons);
                if ($totalCoupon > 0) {
                    ?>
                    <div class="grid-inner">
                        <div class="grid-titles">
                            <div class="grid-left-title heading-text">
                                <h3><?php echo (($rowSubCat['RC_Name'] != "") ? $rowSubCat['RC_Name'] : $rowSubCat['C_Name']) ?></h3>
                            </div>
                            <div class="grid-left-view heading-text">
                                <h3><a href="<?php echo '/coupon/' . $rowSubCat['C_Name_SEO'] . '/' . $rowSubCat['C_ID'] ?>">View All</a></h3>
                            </div>
                        </div>
                        <div class="thumbnails">
                            <div class="thumbnail-slider">
                                <div class="inner">
                                    <ul class="<?php echo ($totalCoupon > 3) ? 'bxslider' : 'non-bxslider'; ?>">
                                        <?PHP
                                        while ($rowCoupon = mysql_fetch_array($resCoupons)) {
                                            if ($rowCoupon['BFC_Thumbnail'] != '') {
                                                $listing_image = $rowCoupon['BFC_Thumbnail'];
                                            } else {
                                                $listing_image = $default_thumbnail_image;
                                            }
                                            ?>
                                            <li>
                                                <a href="/profile/<?php echo (($rowCoupon['BFC_Title'] != "") ? clean($rowCoupon['BFC_Title']) : $rowCoupon['BL_Name_SEO']) ?>/<?php echo $rowCoupon['BFC_ID'] ?>/">
                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo $rowCoupon['BFC_Title'] ?>"/>
                                                    <h3 class="thumbnail-heading coupon-header"><?php echo (($rowSubCat['RC_Name'] != "") ? $rowSubCat['RC_Name'] : $rowSubCat['C_Name']) ?></h3>
                                                    <h3 class="thumbnail-desc"><?php echo $rowCoupon['BL_Listing_Title'] ?></h3>
                                                </a>
                                                <a href="/profile/<?php echo (($rowCoupon['BFC_Title'] != "") ? clean($rowCoupon['BFC_Title']) : $rowCoupon['BL_Name_SEO']) ?>/<?php echo $rowCoupon['BFC_ID'] ?>/">
                                                    <h3 class="thumbnail-coupon"><?php echo $rowCoupon['BFC_Title'] ?></h3>
                                                </a>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                    </ul>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            }
        }
        ?>
    </div>
</section>
<!--Thumbnail Grid-->
<?php require_once 'include/public/footer.php'; ?>
