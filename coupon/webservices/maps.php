<?php
require_once '../../include/config.inc.php';
$R_ID = $_REQUEST['r_id'];
$json = array();

$sqlList = "SELECT BL_ID, BL_Name_SEO, BL_Photo_Alt, BL_Lat, BL_Long, BL_Street, BL_Town, BFC_ID, BFC_Title, BL_Listing_Title, BFC_Main_Image, BFCCM_C_ID, 
            RC.RC_Map_Icon as subcat_icon, RC1.RC_Map_Icon as cat_icon FROM tbl_Business_Feature_Coupon
            LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
            LEFT JOIN tbl_Category C ON C.C_ID = BFCCM_C_ID
            LEFT JOIN tbl_Category C1 ON C1.C_ID = C.C_Parent
            LEFT JOIN tbl_Region_Category RC ON RC.RC_C_ID = C.C_ID AND RC.RC_R_ID = '" . encode_strings($R_ID, $db) . "'
            LEFT JOIN tbl_Region_Category RC1 ON RC1.RC_C_ID = C1.C_ID AND RC1.RC_R_ID = '" . encode_strings($R_ID, $db) . "'
            LEFT JOIN tbl_Business_Listing ON BL_ID = BFC_BL_ID
            WHERE BFC_Status=1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date =0000-00-00) 
            AND hide_show_listing='1' $include_free_listings_on_map GROUP BY BFC_ID ORDER BY BFC_ID DESC";
$resList = mysql_query($sqlList, $db) or die(mysql_error() . ' - ' . $sqlList);
while ($List = mysql_fetch_assoc($resList)) {
    if ($List['BFC_Main_Image'] != '') {
    $List['BFC_Main_Image'] = 'http://touristtowndemo.com/images/DB/'. $List['BFC_Main_Image'];
    }
    $json['map'] = $List;
}

$s['data'] = $json;
print json_encode($s)
?>
