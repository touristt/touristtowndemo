<?php
require_once '../../include/config.inc.php';
$R_ID = $_REQUEST['r_id'];
$json = array();

$sql_theme = "SELECT * FROM tbl_Theme_Options_Mobile WHERE TO_R_ID = '" . encode_strings($R_ID, $db) . "' LIMIT 1";
$result_theme = mysql_query($sql_theme, $db) or die("Invalid query: $sql_theme -- " . mysql_error());
$Region_theme = mysql_fetch_assoc($result_theme);

if ($Region_theme['TO_Slider_Overlay'] != '') {
    $Region_theme['TO_Slider_Overlay'] = 'http://touristtowndemo.com/theme_icons/' . $Region_theme['TO_Slider_Overlay'];
}

if ($Region_theme['TO_Bar_Texture'] != '') {
    $home_des_bg_color = explode(">", $Region_theme['TO_Bar_Texture']);
    $home_des_bg = $home_des_bg_color[0];
    $home_des_color = $home_des_bg_color[1];
    if ($home_des_bg != '' && $home_des_color == '') {
        $Region_theme['TO_Bar_Texture'] = 'http://touristtowndemo.com/theme_icons/' . $home_des_bg . '>';
    } elseif ($home_des_bg == '' && $home_des_color != '') {
        $Region_theme['TO_Bar_Texture'] = '>' . $home_des_color;
    } elseif ($home_des_bg != '' && $home_des_color != '') {
        $Region_theme['TO_Bar_Texture'] = 'http://touristtowndemo.com/theme_icons/' . $Region_theme['TO_Bar_Texture'];
    }
}

if ($Region_theme['TO_Category_Header_BG'] != '') {
    $home_des_bg_color = explode(">", $Region_theme['TO_Category_Header_BG']);
    $home_des_bg = $home_des_bg_color[0];
    $home_des_color = $home_des_bg_color[1];
    if ($home_des_bg != '' && $home_des_color == '') {
        $Region_theme['TO_Category_Header_BG'] = 'http://touristtowndemo.com/theme_icons/' . $home_des_bg . '>';
    } elseif ($home_des_bg == '' && $home_des_color != '') {
        $Region_theme['TO_Category_Header_BG'] = '>' . $home_des_color;
    } elseif ($home_des_bg != '' && $home_des_color != '') {
        $Region_theme['TO_Category_Header_BG'] = 'http://touristtowndemo.com/theme_icons/' . $Region_theme['TO_Category_Header_BG'];
    }
}

if ($Region_theme['TO_Subcat_Texture'] != '') {
    $home_des_bg_color = explode(">", $Region_theme['TO_Subcat_Texture']);
    $home_des_bg = $home_des_bg_color[0];
    $home_des_color = $home_des_bg_color[1];
    if ($home_des_bg != '' && $home_des_color == '') {
        $Region_theme['TO_Subcat_Texture'] = 'http://touristtowndemo.com/theme_icons/' . $home_des_bg . '>';
    } elseif ($home_des_bg == '' && $home_des_color != '') {
        $Region_theme['TO_Subcat_Texture'] = '>' . $home_des_color;
    } elseif ($home_des_bg != '' && $home_des_color != '') {
        $Region_theme['TO_Subcat_Texture'] = 'http://touristtowndemo.com/theme_icons/' . $Region_theme['TO_Subcat_Texture'];
    }
}

if ($Region_theme['TO_Listing_Texture'] != '') {
    $home_des_bg_color = explode(">", $Region_theme['TO_Listing_Texture']);
    $home_des_bg = $home_des_bg_color[0];
    $home_des_color = $home_des_bg_color[1];
    if ($home_des_bg != '' && $home_des_color == '') {
        $Region_theme['TO_Listing_Texture'] = 'http://touristtowndemo.com/theme_icons/' . $home_des_bg . '>';
    } elseif ($home_des_bg == '' && $home_des_color != '') {
        $Region_theme['TO_Listing_Texture'] = '>' . $home_des_color;
    } elseif ($home_des_bg != '' && $home_des_color != '') {
        $Region_theme['TO_Listing_Texture'] = 'http://touristtowndemo.com/theme_icons/' . $Region_theme['TO_Listing_Texture'];
    }
}

if ($Region_theme['TO_Desktop_Logo'] != '') {
    $Region_theme['TO_Desktop_Logo'] = 'http://touristtowndemo.com/images/DB/' . $Region_theme['TO_Desktop_Logo'];
}

if ($Region_theme['TO_Homepage_Description_Background'] != '') {
    $home_des_bg_color = explode(">", $Region_theme['TO_Homepage_Description_Background']);
    $home_des_bg = $home_des_bg_color[0];
    $home_des_color = $home_des_bg_color[1];
    if ($home_des_bg != '' && $home_des_color == '') {
        $Region_theme['TO_Homepage_Description_Background'] = 'http://touristtowndemo.com/theme_icons/' . $home_des_bg . '>';
    } elseif ($home_des_bg == '' && $home_des_color != '') {
        $Region_theme['TO_Homepage_Description_Background'] = '>' . $home_des_color;
    } elseif ($home_des_bg != '' && $home_des_color != '') {
        $Region_theme['TO_Homepage_Description_Background'] = 'http://touristtowndemo.com/theme_icons/' . $Region_theme['TO_Homepage_Description_Background'];
    }
}
$json['theme_options'] = $Region_theme;

$sql_map1 = "SELECT R_Type, R_Homepage_Listings, R_Map_Nav_Title, R_Lat, R_Long, R_Zoom, R_Water_Color, R_Landscape_Color, R_Park_Color, 
        R_Administrative_Color, R_Road_Highway_Color, R_Road_Arterial_Color, R_Road_Local_Color, R_Map_Filters_Background_Color, R_Map_Filters_Text, 
        R_Map_Logo, R_Logo_Name FROM tbl_Region WHERE R_ID = '" . encode_strings($R_ID, $db) . "' LIMIT 1";
$result_map1 = mysql_query($sql_map1, $db) or die("Invalid query: $sql_map1 -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result_map1);
if ($activeRegion['R_Map_Logo'] != '') {
    $activeRegion['R_Map_Logo'] = 'http://touristtowndemo.com/images/DB/' . $activeRegion['R_Map_Logo'];
}
$json['map'] = $activeRegion;

$sql_map2 = "SELECT M_Description FROM tbl_region_map WHERE R_ID = '" . encode_strings($R_ID, $db) . "' LIMIT 1";
$result_map2 = mysql_query($sql_map2, $db) or die("Invalid query: $sql_map2 -- " . mysql_error());
$map_record = mysql_fetch_assoc($result_map2);
$json['map'] += $map_record;

$s['data'] = $json;
print json_encode($s)
?>
