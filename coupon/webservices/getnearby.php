<?php
require_once '../../include/config.inc.php';
include 'include/functions.php';
$region = "SELECT * FROM `tbl_Region` WHERE R_ID =27";
$region_result = mysql_query($region, $db) or die(mysql_error());
$region_value = mysql_fetch_array($region_result); 

///// Radius get
if(isset($_REQUEST['radius']) && $_REQUEST['radius'] !==''){ 
    $radius_val = str_replace('km' , '', $_REQUEST['radius']);
}
///// Lat and lang
if(isset($_REQUEST['lat']) && $_REQUEST['lat'] !=='' && (isset($_REQUEST['lng']) && $_REQUEST['lng'] !=='')){
    $lat_val = $_REQUEST['lat'];
    $lng_val = $_REQUEST['lng']; 
}else{
   $lat_val = $region_value['R_Lat'];
   $lng_val = $region_value['R_Long'];  
}

 $sql= "SELECT tbl_Business_Feature_Coupon.*, BL_ID, BL_Lat, BL_Long,  distance(BL_Location, POINT($lat_val, $lng_val)) AS distance 
       FROM tbl_Business_Feature_Coupon LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID 
       LEFT JOIN tbl_Category ON C_ID = BFCCM_C_ID LEFT JOIN tbl_Region ON R_ID = 27 
       LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = 27
       LEFT JOIN tbl_Business_Listing ON BL_ID = BFC_BL_ID 
       WHERE hide_show_listing = '1' and BFC_Status = 1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date = '0000-00-00') HAVING distance < $radius_val ORDER BY BFC_ID DESC";
$result = mysql_query($sql, $db) or die(mysql_error());
$ids = array();
while ($row = mysql_fetch_assoc($result)) {
  $ids[] = $row['BFC_ID'];
  }
  
$json = array();
if(mysql_num_rows($result) > 0){
    $id  = implode(",",$ids);
    $url = "http://visitcc.touristtowndemo.com/mobile/nearby.php?data=".$id; 
    $json["url"] = $url;
    $json["success"] = "1";
    print(json_encode($json));

}else{
    $json["success"] = "0";
    print(json_encode($json));
}
?>