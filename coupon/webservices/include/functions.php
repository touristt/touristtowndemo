<?php

include"config.php";
include"firebase.php";
include"push.php";

/*
 * Function to send notification with FCM
 */
function sendSyncNotificationFCM($message) {
    
  
   $firebase = new Firebase();
    $push = new Push();
    $push_type = 'topic';
    $push->setPayload($message);
    $push->setTitle(1);
    $json = '';
    $response = '';
    if ($push_type == 'topic') {
        $json = $push->getPush();
        $response = $firebase->sendToTopic('cc', $json);
    } else if ($push_type == 'individual') {
        $json = $push->getPush();
        $regId = isset($_GET['regId']) ? $_GET['regId'] : '';
        $response = $firebase->send($regId, $json);
    }
    return $response;
  

  }
?>