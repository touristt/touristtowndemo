<?php
require_once '../../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';

//search by each word in the phrase except common words
$common_words = array('a', 'an', 'the', 'is', 'are', 'be', 'to', 'of', 'and', 'in', 'that', 'have', 'i', 'it', 'for', 'not', 'on',
    'with', 'he', 'as', 'you', 'do', 'at', 'this', 'but', 'his', 'by', 'from', 'they', 'we', 'her', 'she', 'or',
    'will', 'my', 'one', 'all', 'would', 'there', 'their', 'what', 'so', 'up', 'out', 'if', 'about', 'who', 'get',
    'which', 'go', 'me', 'when', 'make', 'can', 'like', 'no', 'just', 'him', 'know', 'take', 'into', 'your', 'good',
    'some', 'could', 'them', 'other', 'then', 'than', 'now', 'look', 'only', 'come', 'its', 'over', 'think', 'also',
    'back', 'after', 'use', 'two', 'how', 'our', 'first', 'well', 'way', 'even', 'because', 'any', 'these', 'give',
    'most', 'us');
require_once 'include/public/header.php';

$searchstring = array();
if (strlen($_GET['txtSearch']) > 3) {
    //search coupons
    $query = "SELECT BFC_ID, BFC_Main_Image, BFC_Title, RC_Name, C_Name, BL_Listing_Title, BL_Photo_Alt, BL_Name_SEO, BL_ID FROM tbl_Business_Feature_Coupon
                LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
                LEFT JOIN tbl_Category ON C_ID = BFCCM_C_ID
                LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                LEFT JOIN tbl_Business_Listing ON BL_ID = BFC_BL_ID
                WHERE (BFC_Title LIKE '%" . encode_strings($_GET['txtSearch'], $db) . "%'
                OR BL_Listing_Title LIKE '%" . encode_strings($_GET['txtSearch'], $db) . "%'  
                OR BL_Description LIKE '%" . encode_strings($_GET['txtSearch'], $db) . "%'      
                OR BFC_Description LIKE '%" . encode_strings($_GET['txtSearch'], $db) . "%')
                and BFC_Status=1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date =0000-00-00) AND hide_show_listing='1' GROUP BY BFC_ID ORDER BY BFC_ID DESC";

    $resultCoupon = mysql_query($query, $db) or die("Invalid query: $query -- " . mysql_error());
    if(mysql_num_rows($resultCoupon) > 0) {
      print '<section class="thumbnails">';
      while ($rowCoupon = mysql_fetch_assoc($resultCoupon)) {
        ?>
        <div class="thumbnails-inner">
            <div class="thumbnail">
                <a href="<?php echo DOMAIN_MOBILE_REL . 'profile/' . $rowCoupon['BL_Name_SEO'] .'/'. $rowCoupon['BFC_ID'] ?>/">
                    <img src="<?php echo 'http://' . DOMAIN . (($rowCoupon['BFC_Main_Image'] == '') ? '/images/Listing-NoPhoto.jpg' : (IMG_LOC_REL . $rowCoupon['BFC_Main_Image'])); ?>" alt="<?php echo $rowCoupon['BL_Photo_Alt'] ?>" />
                    <div class="title"><p><?php echo $rowCoupon['BFC_Title'] ?></p></div>
                </a>
            </div>
        </div>
        <?php
      }
      print '</section>';
    } else {
      print '<section class="thumbnails">
                <div class="thumbnails-inner">
                  <div class="thumbnail">
                    <p class="listing">Your search request found no results. Please try again.</p>
                  </div>
                </div>
              </section>';
    }
} else {
    print '<section class="thumbnails">
              <div class="thumbnails-inner">
                <div class="thumbnail">
                      <p class="listing">Search Term must be more than three (3) characters.</p>
                  </div>
              </div>
          </section>';
}
require_once 'include/public/footer.php';
?>
