<?php
require_once '../../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
require_once 'include/public/header.php';

$fav = $_REQUEST['data'];
$favorites = trim($fav, '[]');
?>
<!--Main Title Start-->
<section class="main_title">
    <p>Near By</p>
</section>
<!--Thumbnail Grid-->
<section class="thumbnails">
    <?php
    //Getting Coupons on the base of profile ids
    $sqlGetCoupons = "SELECT BFC_ID, BFC_Main_Image, BFC_Title, RC_Name, C_Name, BL_Listing_Title, BL_Photo_Alt, BL_Name_SEO, BL_ID FROM tbl_Business_Feature_Coupon                   
                      LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
                      LEFT JOIN tbl_Category ON C_ID = BFCCM_C_ID
                      LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = 27
                      LEFT JOIN tbl_Business_Listing ON BL_ID = BFC_BL_ID 
                      WHERE BFC_ID IN($favorites) and BFC_Status=1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date =0000-00-00) AND hide_show_listing='1' ORDER BY BFC_ID DESC";
    $resCoupons = mysql_query($sqlGetCoupons);
    while ($rowCoupon = mysql_fetch_assoc($resCoupons)) {
        if ($rowCoupon['BFC_Main_Image'] != '') {
            $listing_image = $rowCoupon['BFC_Main_Image'];
        } else {
            $listing_image = $default_thumbnail_image;
        }
        ?>
        <div class="thumbnails-inner">
            <div class="thumbnail">
                <a href="<?php echo DOMAIN_MOBILE_REL . 'profile/' . $rowCoupon['BL_Name_SEO'] . '/' . $rowCoupon['BFC_ID'] ?>/">
                    <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo $rowCoupon['BFC_Title'] ?>" />
                    <div class="title specialty"><p><?php echo $rowCoupon['BFC_Title']; ?></p></div>
                </a>
            </div>
        </div>
        <?php
    }
    ?>
</section>
<!--Thumbnail Grid-->
<?php
require_once 'include/public/footer.php';
?>