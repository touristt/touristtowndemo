<?php
require_once '../../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';

$SEOtitle = $REGION['R_SEO_Title'];
$SEOdescription = $REGION['R_SEO_Description'];
$SEOkeywords = $REGION['R_SEO_Keywords'];

$sql_slider = "SELECT R_ID, R_Name, R_Type, RP_ID, RP_Photo_Mobile, RP_Photo_Title, RP_Title, RP_Description FROM tbl_Region LEFT JOIN tbl_Region_404 ON R_ID = RP_RID 
            WHERE R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' LIMIT 1";
$result_slider = mysql_query($sql_slider, $db) or die("Invalid query: $sql_slider -- " . mysql_error());
$row_slider = mysql_fetch_array($result_slider);

$sql_slider_404 = "SELECT BFC_ID, BFC_Main_Image, BFC_Title, BL_SEO_Title, BL_Name_SEO, BFC_Description, tbl_Business_Listing.BL_ID, tbl_Business_Listing.BL_Listing_Title 
FROM tbl_Business_Feature_Coupon 
LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID 
LEFT JOIN tbl_Category ON C_ID = BFCCM_C_ID 
LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_C_ID ='' 
LEFT JOIN tbl_Business_Listing  ON BL_ID = BFC_BL_ID
INNER JOIN tbl_404_Listing  ON tbl_404_Listing.BL_ID = tbl_Business_Listing.BL_ID 
WHERE BFC_Status = 1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date =0000-00-00) AND R_ID = 27 GROUP BY BL_ID ORDER BY BL_Listing_Title";
$sql_slider_404_result = mysql_query($sql_slider_404, $db) or die("Invalid query: $sql_slider_404 -- " . mysql_error());
?>
<?php require_once 'include/public/header.php'; ?>
<!--Slider Start-->
<style type="text/css">

</style>
<section class="main_image">
    <div class="title">
        <p><?php echo $row_slider['RP_Photo_Title']; ?></p>
    </div>
    <?php
    if ($row_slider['RP_Photo_Mobile'] != '') {
        ?>
        <img class="slider_image" class="show-slider-display" src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $row_slider['RP_Photo_Mobile'] ?>" alt="<?php echo $row_slider['RTP_Title'] ?>">
        <?php } else {
        ?>
        <img class="slider_image" class="show-slider-display" src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $default_header_image ?>" alt="<?php echo $row_slider['RTP_Title'] ?>">
    <?php }
    ?>    
</section>
<!--Slider End-->
<!--Thumbnail Grid-->
<section class="thumbnails" style="margin-top: 10px;">
    <?php
    $count_row_404_region = mysql_num_rows($sql_slider_404_result);
    if ($count_row_404_region > 0) {
        while ($show_row_404_region = mysql_fetch_array($sql_slider_404_result)) {
            if ($show_row_404_region['BFC_Main_Image'] != '') {
                $listing_image = $show_row_404_region['BFC_Main_Image'];
            } else {
                $listing_image = $default_thumbnail_image;
            }
            ?>
            <div class="thumbnails-inner">
                <div class="thumbnail">
                    <a href="profile/<?php echo (($show_row_404_region['BL_SEO_Title'] != "") ? clean($show_row_404_region['BL_SEO_Title']) : $show_row_404_region['BL_Name_SEO']) ?>/<?php echo $show_row_404_region['BFC_ID'] ?>/">
                        <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $listing_image ?>" />
                        <div class="title index">
                            <p><?php echo ($show_row_404_region['BL_Listing_Title'] != '') ? $show_row_404_region['BL_Listing_Title'] : $show_row_404_region['BL_Listing_Title'] ?></p>
                        </div>
                    </a>
                </div>
            </div>
        <?php } ?>
        <?php
    }
    ?>
</section>
<!--Thumbnail Grid-->
<?php require_once 'include/public/footer.php'; ?>