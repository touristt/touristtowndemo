<?php
require_once '../../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
require_once 'include/PHPMailer/class.phpmailer.php';

function ctime($myTime, $end = false) {
    if ($myTime == '00:00:01' && !$end) {
        return 'Closed';
    } elseif ($myTime == '00:00:02') {
        return 'By Appointment';
    } elseif ($myTime == '00:00:03') {
        return 'Open 24 Hours';
    } else {
        $mySplit = explode(':', $myTime);
        return date('g:ia', mktime($mySplit[0], $mySplit[1], 1, 1, 1));
    }
}

if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $getCouponDetail = "SELECT BFC_ID, BFC_Title, BL_Photo_Alt, BFC_Main_Image, BFC_Description, BFC_Terms_Conditions, BL_ID, BL_Listing_Title, BL_Name_SEO, BL_Town, BL_SEO_Title, 
                    BL_SEO_Description, BL_SEO_Keywords, BL_Long, BL_Lat, BL_Street, BL_Province,BL_Country, BL_PostalCode, BL_Phone , BL_Website, BL_Email,
                    BL_Hours_Disabled, BL_Hours_Appointment, BL_Hour_Mon_From, BL_Hour_Tue_From, BL_Hour_Wed_From, BL_Hour_Thu_From, BL_Hour_Fri_From, 
                    BL_Hour_Sat_From, BL_Hour_Sun_From, BL_Hour_Mon_To, BL_Hour_Tue_To, BL_Hour_Wed_To, BL_Hour_Thu_To, BL_Hour_Fri_To, BL_Hour_Sat_To, 
                    BL_Hour_Sun_To
                    FROM tbl_Business_Listing
                    LEFT JOIN tbl_Business_Feature_Coupon ON BL_ID = BFC_BL_ID 
                    WHERE BFC_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $resCouponDetail = mysql_query($getCouponDetail) or die(mysql_error() . '-' . $getCouponDetail);
    $actCoupon = mysql_fetch_assoc($resCouponDetail);
} else {
    header("Location: " . DOMAIN_MOBILE_REL . "404.php");
    exit();
}

if (isset($_POST) && $_POST['op'] == 'save') {
    $couponNo = sprintf("%04d", $actCoupon['BL_ID']) . '-' . sprintf("%04d", $actCoupon['BFC_ID']);
    $style = "style='display:none;'";
    $padding = "style='padding-top:0;'";
    // send an email
    ob_start();
    include 'include/email-template/coupon-html.php';
    $html = ob_get_contents();
    ob_clean();
    $mail = new PHPMailer();
    $mail->From = COUPON_CONTACT_EMAIL;
    $mail->FromName = COUPON_CONTACT_NAME;
    $mail->IsHTML(true);
    $mail->AddReplyTo(COUPON_CONTACT_EMAIL, COUPON_CONTACT_NAME);
    $mail->AddAddress($_POST['get_coupon']);
    $mail->Subject = 'Coupon Request';
    $mail->MsgHTML($html);
    $mail->CharSet = 'UTF-8';
    $emailSent = $mail->Send();
    if ($emailSent) {
        $getCoupons = "SELECT CU_Counter FROM tbl_Coupon_Usages WHERE CU_BFC_ID = '" . encode_strings($actCoupon['BFC_ID'], $db) . "' ORDER BY CU_ID DESC LIMIT 1";
        $resCoupon = mysql_query($getCoupons);

        $rowCouponCount = mysql_fetch_assoc($resCoupon);
        $couponCount = "";
        if ($rowCouponCount['CU_Counter'] > 0) {
            $couponCount = $rowCouponCount['CU_Counter'];
        } else {
            $couponCount = 0;
        }
        $newCouponValue = $couponCount + 1;
        $insertCoupon = "INSERT tbl_Coupon_Usages SET
                                CU_BFC_ID = '" . encode_strings($actCoupon['BFC_ID'], $db) . "',
                                CU_Email = '" . encode_strings($_POST['get_coupon'], $db) . "',
                                CU_Date = CURDATE(),
                                CU_Counter = '$newCouponValue',
                                CU_Coupon_Code = '$couponNo'";
        mysql_query($insertCoupon);
        $_SESSION['status'] = 1;
    } else {
        $_SESSION['status'] = 0;
    }
}

//SEO Tags
$SEOtitle = $actCoupon['BFC_Title'] ? $actCoupon['BFC_Title'] : $actCoupon['BL_SEO_Title'];
$SEOdescription = $actCoupon['BL_SEO_Description'];
$SEOkeywords = $actCoupon['BL_SEO_Keywords'];

//Getting Region Theme
$getThemeOnListing = "SELECT TO_Default_Header_Mobile FROM tbl_Theme_Options WHERE TO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
$themeRegionOnListing = mysql_query($getThemeOnListing, $db) or die("Invalid query: $getThemeOnListing -- " . mysql_error());
$THEMEOnListing = mysql_fetch_assoc($themeRegionOnListing);

//Defualt Thumbnail Images
if ($THEMEOnListing['TO_Default_Header_Mobile'] != '') {
    $default_header_listing_image = $THEMEOnListing['TO_Default_Header_Mobile'];
} else {
    $default_header_listing_image = 'Default-Main-Mobile.jpg';
}

//OG Tags for FB Share
$OG_URL = curPageURL();
$OG_Type = 'article';
$OG_Title = $actCoupon['BFC_Title'] ? $actCoupon['BFC_Title'] : $actCoupon['BL_SEO_Title'];
$OG_Description = $actCoupon['BL_SEO_Description'];
$OG_Image = ($actCoupon['BFC_Main_Image'] != "") ? 'http://' . DOMAIN . IMG_LOC_REL . $actCoupon['BFC_Main_Image'] : 'http://' . DOMAIN . IMG_LOC_REL . $default_header_listing_image;
$OG_Image_Width = 650;
$OG_Image_Height = 420;
require_once 'include/public/header.php';

//Getting Social Media links
$sqlSM = "SELECT * FROM tbl_Business_Social WHERE BS_BL_ID = " . $actCoupon['BL_ID'] . "";
$resultSM = mysql_query($sqlSM);
$rowSM = mysql_fetch_assoc($resultSM);

//Getting Amenities
$sqlAI = "SELECT *  
        FROM tbl_Business_Listing_Ammenity 
        LEFT JOIN tbl_Ammenity_Icons ON AI_ID = BLA_BA_ID 
        WHERE BLA_BL_ID = '" . encode_strings($actCoupon['BL_ID'], $db) . "' AND AI_Image <> '' ORDER BY AI_Name";
$resultAI = mysql_query($sqlAI, $db);
$AIcount = mysql_num_rows($resultAI);

//Getting files to download
$sqlPDF = "SELECT * FROM tbl_Description_PDF WHERE D_BL_ID = '" . encode_strings($actCoupon['BL_ID'], $db) . "' ORDER BY D_PDF_Order ASC";
$resultPDF = mysql_query($sqlPDF, $db);
$PDFcount = mysql_num_rows($resultPDF);
?>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1356712584397235";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<section class="detail-page">
    <?php
    if (isset($_SESSION['status']) && $_SESSION['status'] == 1) {
        unset($_SESSION['status']);
        ?>
        <div class="get-this-coupon email-sent">
            <div class="get-this-coupon-wrapper">
                <div class="coupon-form-title email-sent-coupon">Your coupon has been sent for <?php echo $actCoupon['BFC_Title']; ?>. If your coupon is not in your email inbox, please check your junk mail or whitelist couponcountry.ca</div>
            </div>
        </div>
        <?php
        $_SESSION['email_sent_coupon'] = 1;
    } else if (isset($_SESSION['status']) && $_SESSION['status'] == 0) {
        unset($_SESSION['status'])
        ?>
        <div class="get-this-coupon email-sent">
            <div class="get-this-coupon-wrapper">
                <div class="coupon-form-title email-sent-coupon">Something went wrong, please try again later.</div>
            </div>
        </div>
    <?php }
    if ($actCoupon['BFC_Main_Image'] != '') {
        $listing_image = $actCoupon['BFC_Main_Image'];
    } else {
        $listing_image = $default_header_image;
    }
    ?>
    <div class="main_image">
        <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo $actCoupon['BL_Photo_Alt'] ?>" longdesc="<?php echo $actCoupon['BL_Listing_Title'] ?>"/>
    </div>
    <div class="listing-main">
        <div class="title">
            <?php echo $actCoupon['BFC_Title'] ?>
        </div>
        <div class="other"><?php echo $actCoupon['BL_Listing_Title'] ?></div>
        <div class="other">
            <?php
            if ($actCoupon['BL_Town'] != '') {
                echo $actCoupon['BL_Town'];
            } if ($actCoupon['BL_Province'] != '') {
                echo ', ' . $actCoupon['BL_Province'];
            }
            if ($actCoupon['BL_Country'] != '') {
                echo ', ' . $actCoupon['BL_Country'];
            }
            ?>
        </div>
        <div class="other">
            <div class="fb-share-button" data-href="<?php print curPageURL(); ?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php print curPageURL(); ?>%2F&amp;src=sdkpreparse">Share</a></div>
            <iframe class="twitter-class"
                    src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=http%3A%2F%2F<?php print curPageURL(); ?>&related=twitterapi%2Ctwitter&text=<?php echo $actCoupon['BFC_Title'] ?> in <?php echo $REGION['R_Name'] ?>"
                    width="70"
                    height="25"
                    style="border: 0; overflow: hidden; float: left;">
            </iframe>
            <div class="get-coupon-btn"><a href="#get_coupon">Get this Coupon</a></div>
        </div>
    </div>
    <div class="listing-description">
        <?php if (trim($actCoupon['BFC_Description']) != '') { ?>
            <div class="description-title">The Deal</div>
            <p><?php echo $actCoupon['BFC_Description']; ?></p>
        <?php } if (trim($actCoupon['BFC_Terms_Conditions']) != '') { ?>
            <div class="description-title">Terms & Conditions</div>
            <p><?php echo $actCoupon['BFC_Terms_Conditions']; ?></p>
        <?php } ?>
        <!-----Get This Coupon Start--->
        <div class="get-this-coupon" id="get_coupon">
            <div class="get-this-coupon-wrapper">
                <div class="coupon-form-title">Get this Coupon</div>
                <div class="coupon-form-text">Simply enter your email address and this coupon will be emailed to you. Be sure to check your promotional or junk mailbox.</div>
                <form action="" method="POST" onsubmit="return validateCouponForm();">
                    <input type="hidden" name="op" value="save"/>
                    <input type="email" name="get_coupon" placeholder="Email Address" value="" required />
                    <input type="submit" class="coupon-button" name="go" value="go"/>
                </form>
            </div>
        </div>
        <!-----Get This Coupon End--->
    </div>
    <?php if ($rowSM['BS_Enabled'] > 0) { ?>
        <div class="social-media">
            <?php
            if (isset($rowSM['BS_FB_Link']) && $rowSM['BS_FB_Link'] != '') {
                $facebook_link = preg_replace('/^www\./', '', $rowSM['BS_FB_Link']);
                ?>
                <a target="_blank" href="<?php echo 'http://' . str_replace(array('http://', 'https://'), '', $facebook_link); ?>">
                    <img src="<?php echo 'http://' . DOMAIN . '/images/facebook.png'; ?>" alt="Facebook" />
                </a>
                <?php
            } if (isset($rowSM['BS_I_Link']) && $rowSM['BS_I_Link'] != '') {
                $rs_i_link = preg_replace('/^www\./', '', $rowSM['BS_I_Link']);
                ?>
                <a target="_blank" href="<?php echo 'http://' . str_replace(array('http://', 'https://'), '', $rs_i_link); ?>">
                    <img src="<?php echo 'http://' . DOMAIN . '/images/instagram.png'; ?>" alt="Instagram" />
                </a>
                <?php
            } if (isset($rowSM['BS_T_Link']) && $rowSM['BS_T_Link'] != '') {
                $rs_t_link = preg_replace('/^www\./', '', $rowSM['BS_T_Link']);
                ?>
                <a target="_blank" href="<?php echo 'http://' . str_replace(array('http://', 'https://'), '', $rs_t_link); ?>">
                    <img src="<?php echo 'http://' . DOMAIN . '/images/twitter.png'; ?>" alt="Twitter" />
                </a>
            <?php } ?>
        </div>
    <?php } ?>
    <div class="listing-address">
        <div class="listing-address-inner">
            <?php if ($actCoupon['BL_Street'] != '' || $actCoupon['BL_Town'] != '' || $actCoupon['BL_Province'] != '' || $actCoupon['BL_PostalCode'] != '') { ?>
                <div class="info">
                    <div class="icon">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_MOBILE_REL . 'location.png'; ?>" alt="location">
                    </div>
                    <div class="address-heading">
                        <?php echo $actCoupon['BL_Street'] . ', ' . $actCoupon['BL_Town'] . ' ' . $actCoupon['BL_Province'] . ' ' . $actCoupon['BL_PostalCode']; ?>
                    </div>          
                </div>
                <?php
            }
//            if ($REGION['R_Show_Hide_Phone'] == 1) {
            if ($actCoupon['BL_Phone'] != "") {
                ?>
                <div class="info">
                    <div class="icon">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_MOBILE_REL . 'Listing-Phone.png'; ?>" alt="Phone">
                    </div>
                    <div class="address-heading"><?php echo $actCoupon['BL_Phone'] ?></div> 
                </div>
                <?php
            }
//            }
            if ($actCoupon['BL_Website'] != '') {
                ?>
                <div class="info">
                    <div class="icon">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_MOBILE_REL . 'Listing-Website.png'; ?>" alt="Website">
                    </div>
                    <div class="address-heading">
                        <a href="<?php echo 'http://' . str_replace(array('http://', 'https://'), '', $actCoupon['BL_Website']); ?>">Visit Website</a>
                    </div> 
                </div>
                <?php
            }
//            if ($REGION['R_Show_Hide_Email'] == 1) {
            if ($actCoupon['BL_Email'] != "") {
                ?>
                <div class="info">
                    <div class="icon">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_MOBILE_REL . 'Listing-Email.png'; ?>" alt="Email">
                    </div>
                    <div class="address-heading">
                        <a href="mailto:<?php echo $actCoupon['BL_Email'] ?>">Send Email</a>
                    </div>
                </div>
                <?php
            }
//            }
            if (!$actCoupon['BL_Hours_Disabled']) {
                if ($actCoupon['BL_Hours_Appointment'] == 1 || $actCoupon['BL_Hour_Mon_From'] != '00:00:00' || $actCoupon['BL_Hour_Tue_From'] != '00:00:00' || $actCoupon['BL_Hour_Wed_From'] != '00:00:00' || $actCoupon['BL_Hour_Thu_From'] != '00:00:00' || $actCoupon['BL_Hour_Fri_From'] != '00:00:00' || $actCoupon['BL_Hour_Sat_From'] != '00:00:00' || $actCoupon['BL_Hour_Sun_From'] != '00:00:00') {
                    ?>
                    <div class="info">
                        <div class="icon">
                            <img src="<?php echo 'http://' . DOMAIN . IMG_MOBILE_REL . 'Listing-Hours.png'; ?>" alt="Hours">
                        </div>
                        <div class="address-heading">
                            <a onclick="show_hide(1)">View Hours</a>
                            <div class="accordion hours">
                                <?php
                                if ($actCoupon['BL_Hours_Appointment']) {
                                    ?>
                                    <div class="accordion-heading">Hours</div>
                                    <div class="accordion-data">By Appointment</div>
                                    <?php
                                } else {
                                    if ($actCoupon['BL_Hour_Mon_From'] != '00:00:00') {
                                        ?>
                                        <div class="accordion-heading">Monday</div>
                                        <div class="accordion-data">
                                            <?php echo ctime($actCoupon['BL_Hour_Mon_From']); ?> <?php echo $actCoupon['BL_Hour_Mon_From'] == '00:00:01' || $actCoupon['BL_Hour_Mon_From'] == '00:00:02' || $actCoupon['BL_Hour_Mon_From'] == '00:00:03' ? '' : '- ' . ctime($actCoupon['BL_Hour_Mon_To'], true); ?>
                                        </div>
                                    <?php } if ($actCoupon['BL_Hour_Tue_From'] != '00:00:00') { ?>
                                        <div class="accordion-heading">Tuesday</div>
                                        <div class="accordion-data">
                                            <?php echo ctime($actCoupon['BL_Hour_Tue_From']); ?> <?php echo $actCoupon['BL_Hour_Tue_From'] == '00:00:01' || $actCoupon['BL_Hour_Tue_From'] == '00:00:02' || $actCoupon['BL_Hour_Tue_From'] == '00:00:03' ? '' : '- ' . ctime($actCoupon['BL_Hour_Tue_To'], true); ?>
                                        </div>
                                    <?php } if ($actCoupon['BL_Hour_Wed_From'] != '00:00:00') { ?>
                                        <div class="accordion-heading">Wednesday</div>
                                        <div class="accordion-data">
                                            <?php echo ctime($actCoupon['BL_Hour_Wed_From']); ?> <?php echo $actCoupon['BL_Hour_Wed_From'] == '00:00:01' || $actCoupon['BL_Hour_Wed_From'] == '00:00:02' || $actCoupon['BL_Hour_Wed_From'] == '00:00:03' ? '' : '- ' . ctime($actCoupon['BL_Hour_Wed_To'], true); ?>
                                        </div>
                                    <?php } if ($actCoupon['BL_Hour_Thu_From'] != '00:00:00') { ?>
                                        <div class="accordion-heading">Thursday</div>
                                        <div class="accordion-data">
                                            <?php echo ctime($actCoupon['BL_Hour_Thu_From']); ?> <?php echo $actCoupon['BL_Hour_Thu_From'] == '00:00:01' || $actCoupon['BL_Hour_Thu_From'] == '00:00:02' || $actCoupon['BL_Hour_Thu_From'] == '00:00:03' ? '' : '- ' . ctime($actCoupon['BL_Hour_Thu_To'], true); ?>
                                        </div>
                                    <?php } if ($actCoupon['BL_Hour_Fri_From'] != '00:00:00') { ?>
                                        <div class="accordion-heading">Friday</div>
                                        <div class="accordion-data">
                                            <?php echo ctime($actCoupon['BL_Hour_Fri_From']); ?> <?php echo $actCoupon['BL_Hour_Fri_From'] == '00:00:01' || $actCoupon['BL_Hour_Fri_From'] == '00:00:02' || $actCoupon['BL_Hour_Fri_From'] == '00:00:03' ? '' : '- ' . ctime($actCoupon['BL_Hour_Fri_To'], true); ?>
                                        </div>
                                    <?php } if ($actCoupon['BL_Hour_Sat_From'] != '00:00:00') { ?>
                                        <div class="accordion-heading">Saturday</div>
                                        <div class="accordion-data">
                                            <?php echo ctime($actCoupon['BL_Hour_Sat_From']); ?> <?php echo $actCoupon['BL_Hour_Sat_From'] == '00:00:01' || $actCoupon['BL_Hour_Sat_From'] == '00:00:02' || $actCoupon['BL_Hour_Sat_From'] == '00:00:03' ? '' : '- ' . ctime($actCoupon['BL_Hour_Sat_To'], true); ?>
                                        </div>
                                    <?php } if ($actCoupon['BL_Hour_Sun_From'] != '00:00:00') { ?>
                                        <div class="accordion-heading">Sunday</div>
                                        <div class="accordion-data">
                                            <?php echo ctime($actCoupon['BL_Hour_Sun_From']); ?> <?php echo $actCoupon['BL_Hour_Sun_From'] == '00:00:01' || $actCoupon['BL_Hour_Sun_From'] == '00:00:02' || $actCoupon['BL_Hour_Sun_From'] == '00:00:03' ? '' : '- ' . ctime($actCoupon['BL_Hour_Sun_To'], true); ?>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>   
                        </div>
                    </div>
                    <?PHP
                }
            }
            if ($AIcount > 0) {
                ?>
                <div class="info">
                    <div class="icon">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_MOBILE_REL . 'Listing-Ammenity.png'; ?>" alt="Ammenity">
                    </div>
                    <div class="address-heading">
                        <a onclick="show_hide(2)">View Amenities</a>
                        <div class="accordion amenities">
                            <?php
                            while ($rowAI = mysql_fetch_array($resultAI)) {
                                ?>
                                <div class="accordion-data"><?php echo $rowAI['AI_Name'] ?></div>
                                <?php
                            }
                            ?>
                        </div>   
                    </div>
                </div>
                <?PHP
            }
            if ($PDFcount > 0) {
                ?>
                <div class="info">
                    <div class="icon">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_MOBILE_REL . 'Listing-Download.png'; ?>" alt="Download">
                    </div>
                    <div class="address-heading">
                        <a onclick="show_hide(3)">View Downloads</a>
                        <div class="accordion downloads">
                            <?php
                            while ($rowPDF = mysql_fetch_array($resultPDF)) {
                                ?>
                                <div class="accordion-data">
                                    <a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $rowPDF['D_PDF']; ?>"><?php echo $rowPDF['D_PDF_Title'] ?></a>
                                </div>
                                <?php
                            }
                            ?>
                        </div>   
                    </div>
                </div>
                <?PHP
            }
            ?>          
        </div>
    </div>
    <?php
    if ($actCoupon['BL_Lat'] != '' && $actCoupon['BL_Long']) {
        $markers = array();
        //add to markers for map
        if ($actCoupon['BFC_Main_Image'] != '') {
            $image = '<div class="thumbnail"><img width="130" height="90" src="http://' . (DOMAIN . IMG_LOC_REL . $actCoupon['BFC_Main_Image']) . '" alt="' . htmlspecialchars(trim($actCoupon['BL_Photo_Alt']), ENT_QUOTES) . '" /></div>';
        } else {
            $image = '<div class="thumbnail"><img src="http://' . DOMAIN . IMG_LOC_REL . $default_thumbnail_image .'" alt="' . htmlspecialchars(trim($actCoupon['BL_Listing_Title']), ENT_QUOTES) . '" /></div>';
        }
        $markers[] = array(
            'id' => $actCoupon['BL_ID'],
            'lat' => $actCoupon['BL_Lat'],
            'lon' => $actCoupon['BL_Long'],
            'name' => $actCoupon['BFC_Title'],
            'path' => '/profile/' . $actCoupon['BL_Name_SEO'] . '/' . $actCoupon['BFC_ID'] . '/',
            'main_photo' => $image,
            'address' => htmlspecialchars(trim($actCoupon['BL_Street']), ENT_QUOTES),
            'town' => htmlspecialchars(trim($actCoupon['BL_Town']), ENT_QUOTES)
        );
        $markers = json_encode($markers);
        ?>
        <div id="map_canvas" style="width:100%;height: 390px;margin-bottom:10px;"></div>
        <?php
        $map_center['latitude'] = $actCoupon['BL_Lat'];
        $map_center['longitude'] = $actCoupon['BL_Long'];
        $map_center['zoom'] = $REGION['R_Zoom'];
        $kml_json = json_encode(array());
        require_once 'map_script.php';
    }
    ?>
</section> 
<?php require 'include/public/footer.php'; ?>
