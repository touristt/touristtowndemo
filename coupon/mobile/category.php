<?php
require_once '../../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
$url_parse = parse_url($_SERVER['REQUEST_URI']);
$data = substr($url_parse['path'], 1);
if (substr($data, -1) == '/') {
    $data = substr($data, 0, -1);
}
$url = explode('/', $data);
//If subcategory page
if (isset($url[2]) && $url[2] <> '') {
    $Parent_query = "SELECT C_ID, C_Parent, C_Name_SEO FROM tbl_Category WHERE C_Name_SEO = '" . encode_strings($url[1], $db) . "'";
    $Perent_result = mysql_query($Parent_query);
    $parent_val = mysql_fetch_assoc($Perent_result);
    $parent = $parent_val['C_ID'];
    $sql = "SELECT C_ID, C_Name, C_Parent, C_Is_Blog, C_Name_SEO, RC_Name, RC_SEO_Title, RC_SEO_Description, RC_SEO_Keywords FROM tbl_Category 
            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            WHERE C_Name_SEO = '" . encode_strings($url[2], $db) . "' AND C_Parent = '" . $parent . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeCat = mysql_fetch_assoc($result);
    $SEOtitle = $activeCat['RC_SEO_Title'];
    $SEOdescription = $activeCat['RC_SEO_Description'];
    $SEOkeywords = $activeCat['RC_SEO_Keywords'];
    require_once 'include/public/header.php';
    require_once 'subcategory.php';
}
else if ($url[1] <> '') {
    $sql = "SELECT * FROM tbl_Category 
            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            WHERE C_Name_SEO = '" . encode_strings($url[1], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeCat = mysql_fetch_assoc($result);
    if ($activeCat['C_ID'] < 1) {
        header("Location: " . DOMAIN_MOBILE_REL . "404.php");
        exit();
    }
    $SEOtitle = $activeCat['RC_SEO_Title'];
    $SEOdescription = $activeCat['RC_SEO_Description'];
    $SEOkeywords = $activeCat['RC_SEO_Keywords'];
    require_once 'include/public/header.php';
    require_once 'maincategory.php';
}
require_once 'include/public/footer.php';
?>
