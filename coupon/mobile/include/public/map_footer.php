<!--Footer Start-->
<footer class="map-footer">
    <div class="map-footer-outer">
      <div class="footer-logo">
        <a href="/mobile">
            <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $REGION['R_Map_Logo']; ?>"  alt="<?php echo isset($REGION['R_Logo_Name']) ? $REGION['R_Logo_Name'] : ''; ?>">
        </a>
      </div>
      <div class="footer-home">
        <a href="/mobile" class="footer-button">Home</a>
      </div>
    </div>
</footer>
</body>
</html>