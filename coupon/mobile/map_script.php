<script type="text/javascript">
  var map;
  var markersmap = [];
  var markers = <?php echo $markers; ?>;
  var kml_json = <?php echo $kml_json ?>;
  var business = <?php echo (isset($actCoupon['BL_ID']) && $actCoupon['BL_ID'] > 0) ? $actCoupon['BL_ID'] : 0; ?>;
  function initialize() {
    var myLatlng = new google.maps.LatLng(<?php echo $map_center['latitude'] ?>,<?php echo $map_center['longitude'] ?>);
    var roadAtlasStyles = [
      {
        "featureType": "water",
        "stylers": [
          {
            "color": "<?php echo (isset($REGION['R_Water_Color'])) ? $REGION['R_Water_Color'] : '#e0d7c5'; ?>"
          }
        ]
      },
      {
        "featureType": "landscape.natural",
        "stylers": [
          {
            "color": "<?php echo (isset($REGION['R_Landscape_Color'])) ? $REGION['R_Landscape_Color'] : '#f1eee5'; ?>"
          }
        ]
      },
      {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "<?php echo (isset($REGION['R_Park_Color'])) ? $REGION['R_Park_Color'] : '#d3c8ac'; ?>"
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [
          {
            "visibility": "off"
            //"color": "<?php echo (isset($REGION['R_Park_Label_Color'])) ? $REGION['R_Park_Label_Color'] : '#766740'; ?>"
          }
        ]
      },
      {
        "featureType": "administrative",
        "elementType": "labels.text",
        "stylers": [
          {
            "color": "<?php echo (isset($REGION['R_Administrative_Color'])) ? $REGION['R_Administrative_Color'] : '#312a1a'; ?>"
          },
          {
            "weight": 0.2
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "<?php echo (isset($REGION['R_Road_Highway_Color'])) ? $REGION['R_Road_Highway_Color'] : '#a08b54'; ?>"
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "<?php echo (isset($REGION['R_Road_Arterial_Color'])) ? $REGION['R_Road_Arterial_Color'] : '#a08b54'; ?>"
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "<?php echo (isset($REGION['R_Road_Local_Color'])) ? $REGION['R_Road_Local_Color'] : '#a08b54'; ?>"
          }
        ]
      },
    ];
    var mapOptions = {
      zoom: <?php echo $map_center['zoom'] ?>,
      center: myLatlng,
      scrollwheel: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
//      styles: styles
    }
    map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
    infowindow = new google.maps.InfoWindow({
      content: "",
      maxWidth: 280
    });
    var styledMapOptions = {
      name: 'Interactive Map'
    };
    var usRoadMapType = new google.maps.StyledMapType(
            roadAtlasStyles, styledMapOptions);

    map.mapTypes.set('usroadatlas', usRoadMapType);
    map.setMapTypeId('usroadatlas');
    $.each(kml_json, function (i, item) {
      if(typeof item.url !== undefined){
        var routeLayer = new google.maps.KmlLayer({
          url: item.url,
          suppressInfoWindows: true,
          preserveViewport: true
        });
        routeLayer.setMap(map);
      }
    });
    $.each(markers, function (index, mvisit) {
      markersmap[index] = new google.maps.Marker({
        position: new google.maps.LatLng(mvisit.lat, mvisit.lon),
        map: map,
        title: mvisit.name,
        icon: mvisit.icon
      });
      if (mvisit.name != 0 && business == 0) {
        google.maps.event.addListener(markersmap[index], 'click', function () {
          infowindow.setContent('<div id="iw-container">' +
                  '<div class="iw-content">' +
                  '<a href="' + mvisit.path + '">' + mvisit.main_photo + '</a>' +
                  '<div class="tooltip-title"><a href="' + mvisit.path + '">' + mvisit.name + '</a></div>' +
                  '</div>' +
                  '</div>');
          infowindow.open(map, this);
        });
      }
    });
    // *
    // START INFOWINDOW CUSTOMIZE.
    // The google.maps.event.addListener() event expects
    // the creation of the infowindow HTML structure 'domready'
    // and before the opening of the infowindow, defined styles are applied.
    // *
    google.maps.event.addListener(infowindow, 'domready', function () {

      // Reference to the DIV that wraps the bottom of infowindow
      var iwOuter = $('.gm-style-iw');

      /* Since this div is in a position prior to .gm-div style-iw.
       * We use jQuery and create a iwBackground variable,
       * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
       */
      var iwBackground = iwOuter.prev();

      // Removes background shadow DIV
      iwBackground.children(':nth-child(2)').css({'display': 'none'});

      // Removes white background DIV
      iwBackground.children(':nth-child(4)').css({'display': 'none'});

      // Reference to the div that groups the close button elements.
      var iwCloseBtn = iwOuter.next();

      // Apply the desired effect to the close button
      iwCloseBtn.css({right: '15px', top: '8px', border: '7px solid #0078be', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9', width: '27px', height: '26px'});
    });
  }
  $(document).ready(function () {
    //have map width/height set on load
    $('.map_wrapper').width($(window).width());
    $('.map_wrapper').height($(window).height());
    $('.map_wrapper #map_canvas').height($(window).height());

    //have map width/height set on window resize
    $(window).resize(function () {
      $('.map_wrapper').width($(window).width());
      $('.map_wrapper').height($(window).height());
      $('.map_wrapper #map_canvas').height($(window).height());
      var center = map.getCenter();
      google.maps.event.trigger(map, "resize");
      map.setCenter(center);
    });
    initialize();
  });
</script>