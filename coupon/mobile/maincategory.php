<!--Main Title Start-->
<section class="main_title">
    <p><?php echo ($activeCat['RC_Name'] != '') ? $activeCat['RC_Name'] : $activeCat['C_Name']; ?></p>
</section>
<!--Main Title End-->
<!--Thumbnail Grid-->
<section class="thumbnails">
    <?PHP
    $sql = "SELECT C_ID, RC_Name, RC_Mobile_Thumbnail, C_Name_SEO, C_Name FROM tbl_Business_Feature_Coupon
            INNER JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
            INNER JOIN tbl_Category ON C_ID = BFCCM_C_ID                                    
            INNER JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            INNER JOIN tbl_Business_Listing ON BL_ID = BFC_BL_ID 
            WHERE C_Parent = '" . encode_strings($activeCat['C_ID'], $db) . "' AND BFC_Status=1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date =0000-00-00) AND hide_show_listing='1'
            GROUP BY C_ID ORDER BY RC_Order";
    $result = mysql_query($sql, $db) or die(mysql_error());
    while ($row = mysql_fetch_assoc($result)) {
        if ($row['RC_Mobile_Thumbnail'] != '') {
            $cat_image = $row['RC_Mobile_Thumbnail'];
        } else {
            $cat_image = $default_thumbnail_image;
        }
        ?>
        <div class="thumbnails-inner">
            <div class="thumbnail">
                <a href="<?php echo DOMAIN_MOBILE_REL . $activeCat['C_Name_SEO'] ?>/<?php echo $row['C_Name_SEO'] ?>/">
                    <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $cat_image; ?>" alt="<?php echo ($row['RC_Name'] != '') ? $row['RC_Name'] : $row['C_Name'] ?>" />
                    <div class="title category"><p><?php echo ($row['RC_Name'] != '') ? $row['RC_Name'] : $row['C_Name'] ?></p></div>
                </a>
            </div>
        </div>
        <?php
    }
    ?> 
</section>
<!--Thumbnail Grid-->