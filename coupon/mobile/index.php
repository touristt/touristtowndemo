<?php
require_once '../../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
require_once 'include/public/header.php';
$SEOtitle = $REGION['R_SEO_Title'];
$SEOdescription = $REGION['R_SEO_Description'];
$SEOkeywords = $REGION['R_SEO_Keywords'];

$sql_slider = "SELECT RTP_Title, RTP_Mobile_Photo FROM tbl_Region_Themes_Photos WHERE RTP_RT_RID ='" . encode_strings($REGION['R_ID'], $db) . "' ORDER BY RTP_Order LIMIT 1";
$result_slider = mysql_query($sql_slider, $db) or die("Invalid query: $sql_slider -- " . mysql_error());
$row_slider = mysql_fetch_array($result_slider);
?>
<?php require_once 'include/public/header.php'; ?>
<!--Slider Start-->
<section class="main_image">
    <div class="title">
        <p><?php echo $row_slider['RTP_Title']; ?></p>
    </div>
    <?php if (isset($row_slider['RTP_Mobile_Photo']) && $row_slider['RTP_Mobile_Photo'] != '') { ?>
        <img class="slider_image" class="show-slider-display" src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $row_slider['RTP_Mobile_Photo'] ?>" alt="<?php echo $row_slider['RTP_Title'] ?>">
    <?php } else { ?>
        <img class="slider_image" class="show-slider-display" src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $default_header_image ?>" alt="<?php echo $row_slider['RTP_Title'] ?>">
    <?php } if ($THEME['TO_Slider_Overlay'] != "") { ?>
        <div class="image_overlay_img"> 
            <div class="image_overlay"> 
                <img src="<?php echo 'http://' . DOMAIN . IMG_ICON_REL . $THEME['TO_Slider_Overlay']; ?>" alt="Slider Overlay">
            </div>
        </div>
    <?php } ?>
</section>
<!--Slider End-->
<!--Description Start-->
<section class="description">
    <?PHP if ($REGION['R_Mobile_Description']) { ?>
        <p><?PHP echo ($REGION['R_Mobile_Description']); ?></p>
    <?PHP } ?>
</section>
<!--Description End-->
<!--Thumbnail Grid-->
<section class="thumbnails">
    <?php
    //Getting Active Categories for region
    $sqlMainCat = "SELECT C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category
                  LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID
                  WHERE C_Is_Product_Web = 1 AND RC_Status = 0  AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
    $resMainCat = mysql_query($sqlMainCat);
    $MainCat = mysql_fetch_assoc($resMainCat);
    $getCategory = "SELECT C_ID, RC_Name, RC_Mobile_Thumbnail, C_Name_SEO, C_Name FROM tbl_Category LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                    WHERE C_Parent = '" . $MainCat['C_ID'] . "' 
                    AND RC_R_ID > 0 AND RC_Status = 0 ORDER BY RC_Order";
    $catRes = mysql_query($getCategory);
    while ($homeCat = mysql_fetch_assoc($catRes)) {
        if ($homeCat['RC_Mobile_Thumbnail'] != '') {
            $listing_image = $homeCat['RC_Mobile_Thumbnail'];
        } else {
            $listing_image = $default_thumbnail_image;
        }
        $getSubCat = "SELECT BFC_ID FROM tbl_Business_Feature_Coupon
                      LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
                      LEFT JOIN tbl_Category ON C_ID = BFCCM_C_ID                                    
                      LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                      LEFT JOIN tbl_Business_Listing ON BL_ID = BFC_BL_ID 
                      WHERE BFCCM_C_ID = '" . encode_strings($homeCat['C_ID'], $db) . "' and BFC_Status=1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date =0000-00-00) AND hide_show_listing='1' ORDER BY BFC_ID DESC";
        $subCatRes = mysql_query($getSubCat, $db) or die(mysql_error());
        $count_homepage = mysql_num_rows($subCatRes);
        if ($count_homepage > 0) {
            ?>
            <div class="thumbnails-inner">
                <div class="thumbnail">
                    <a href="<?php echo DOMAIN_MOBILE_REL . $MainCat['C_Name_SEO'] . "/" . $homeCat['C_Name_SEO'] ?>/">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo $homeCat['RC_Name'] ?>" />
                        <div class="title index">
                            <p><?php echo ($homeCat['RC_Name'] != '') ? $homeCat['RC_Name'] : $homeCat['C_Name'] ?></p>
                        </div>
                    </a>
                </div>
            </div>
        <?php } ?>
        <?php
    }
    ?>
</section>
<!--Thumbnail Grid-->
<?php require_once 'include/public/footer.php'; ?>