<?php
require_once '../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
require_once 'include/PHPMailer/class.phpmailer.php';

function ctime($myTime, $end = false) {
    if ($myTime == '00:00:01' && !$end) {
        return 'Closed';
    } elseif ($myTime == '00:00:02') {
        return 'By Appointment';
    } elseif ($myTime == '00:00:03') {
        return 'Open 24 Hours';
    } else {
        $mySplit = explode(':', $myTime);
        return date('g:ia', mktime($mySplit[0], $mySplit[1], 1, 1, 1));
    }
}

$getCouponDetail = "SELECT BFC_ID, BFC_Title, BFC_Main_Image, BFC_Description, BFC_Terms_Conditions, BL_ID, BL_Listing_Title, BL_Name_SEO, BL_Town, BL_SEO_Title, 
                    BL_SEO_Description, BL_SEO_Keywords, BL_Long, BL_Lat, BL_Street, BL_Province, BL_PostalCode, BL_Phone , BL_Website, BL_Email,
                    BL_Hours_Disabled, BL_Hours_Appointment, BL_Hour_Mon_From, BL_Hour_Tue_From, BL_Hour_Wed_From, BL_Hour_Thu_From, BL_Hour_Fri_From, 
                    BL_Hour_Sat_From, BL_Hour_Sun_From, BL_Hour_Mon_To, BL_Hour_Tue_To, BL_Hour_Wed_To, BL_Hour_Thu_To, BL_Hour_Fri_To, BL_Hour_Sat_To, 
                    BL_Hour_Sun_To
                    FROM tbl_Business_Listing
                    LEFT JOIN tbl_Business_Feature_Coupon ON BL_ID = BFC_BL_ID 
                    WHERE BFC_ID = '" . encode_strings($_REQUEST['co_id'], $db) . "'";
$resCouponDetail = mysql_query($getCouponDetail);
$actCoupon = mysql_fetch_assoc($resCouponDetail);

//check to pass mobile users to mobile version
$useragent = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
    header('Location: http://' . $REGION['R_Domain'] . DOMAIN_MOBILE_REL . 'profile/' . $actCoupon['BL_Name_SEO'] . '/' . $actCoupon['BFC_ID'] . '/');
    exit();
}

//SEO Tags
$SEOtitle = $actCoupon['BFC_Title'] ? $actCoupon['BFC_Title'] : $actCoupon['BL_SEO_Title'];
$SEOdescription = $actCoupon['BL_SEO_Description'];
$SEOkeywords = $actCoupon['BL_SEO_Keywords'];

//Getting Region Theme
$getThemeOnListing = "SELECT TO_Default_Header_Mobile FROM tbl_Theme_Options WHERE TO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
$themeRegionOnListing = mysql_query($getThemeOnListing, $db) or die("Invalid query: $getThemeOnListing -- " . mysql_error());
$THEMEOnListing = mysql_fetch_assoc($themeRegionOnListing);

//Defualt Thumbnail Images
if ($THEMEOnListing['TO_Default_Header_Mobile'] != '') {
    $default_header_listing_image = $THEMEOnListing['TO_Default_Header_Mobile'];
} else {
    $default_header_listing_image = 'Default-Main-Mobile.jpg';
}

//OG Tags for FB Share
$OG_URL = curPageURL();
$OG_Type = 'article';
$OG_Title = $actCoupon['BFC_Title'] ? $actCoupon['BFC_Title'] : $actCoupon['BL_SEO_Title'];
$OG_Description = $actCoupon['BL_SEO_Description'];
$OG_Image = ($actCoupon['BFC_Main_Image'] != "") ? 'http://' . DOMAIN . IMG_LOC_REL . $actCoupon['BFC_Main_Image'] : 'http://' . DOMAIN . IMG_LOC_REL . $default_header_listing_image;
$OG_Image_Width = 650;
$OG_Image_Height = 420;
require_once 'include/public/header.php';
?>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1356712584397235";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<section class="description stories margin-bottom-none">
    <div class="description-wrapper">
        <div class="description-inner padding-none">
            <div class="listing-detail-left">
                <?php
                $style = "style='display:block;'";
                $padding = "";
                if (isset($_POST) && $_POST['op'] == 'save') {
                    $couponNo = sprintf("%04d", $actCoupon['BL_ID']) . '-' . sprintf("%04d", $actCoupon['BFC_ID']);
                    $style = "style='display:none;'";
                    $padding = "style='padding-top:0;'";
                    // send an email
                    ob_start();
                    include 'include/email-template/coupon-html.php';
                    $html = ob_get_contents();
                    ob_clean();
                    $mail = new PHPMailer();
                    $mail->From = COUPON_CONTACT_EMAIL;
                    $mail->FromName = COUPON_CONTACT_NAME;
                    $mail->IsHTML(true);
                    $mail->AddReplyTo(COUPON_CONTACT_EMAIL, COUPON_CONTACT_NAME);
                    $mail->AddAddress($_POST['get_coupon']);
                    //$mail->AddAddress('imtiazork@gmail.com');
                    $mail->Subject = 'Coupon Request';
                    $mail->MsgHTML($html);
                    $mail->CharSet = 'UTF-8';
                    $emailSent = $mail->Send();
                    if ($emailSent) {
                        $getCoupons = "SELECT CU_Counter FROM tbl_Coupon_Usages WHERE CU_BFC_ID = '" . encode_strings($actCoupon['BFC_ID'], $db) . "' ORDER BY CU_ID DESC LIMIT 1";
                        $resCoupon = mysql_query($getCoupons);

                        $rowCouponCount = mysql_fetch_assoc($resCoupon);
                        $couponCount = "";
                        if ($rowCouponCount['CU_Counter'] > 0) {
                            $couponCount = $rowCouponCount['CU_Counter'];
                        } else {
                            $couponCount = 0;
                        }
                        $newCouponValue = $couponCount + 1;
                        $insertCoupon = "INSERT tbl_Coupon_Usages SET
                                        CU_BFC_ID = '" . encode_strings($actCoupon['BFC_ID'], $db) . "',
                                        CU_Email = '" . encode_strings($_POST['get_coupon'], $db) . "',
                                        CU_Date = CURDATE(),
                                        CU_Counter = '$newCouponValue',
                                        CU_Coupon_Code = '$couponNo'";
                        mysql_query($insertCoupon);
                        ?>
                        <div class="get-this-coupon email-sent">
                            <div class="get-this-coupon-wrapper">
                                <div class="coupon-form-title email-sent-coupon">Your coupon has been sent for <?php echo $actCoupon['BFC_Title']; ?></div>
                                <div class="coupon-form-text email-sent-town"><?php echo $actCoupon['BL_Listing_Title']; ?>, <?php echo $actCoupon['BL_Town']; ?></div>
                                <div class="coupon-form-text email-sent-to">to: <?php echo $_POST['get_coupon']; ?></div>
                            </div>
                        </div>
                        <div class="menu-desc">
                            <div class="menu-item-heading-container">
                                <div class="menu-title">Please check your inbox. </div>
                            </div>
                        </div>
                        <div class="menu-desc">
                            <p>If your coupon is not in your email inbox, please check your junk mail or whitelist couponcountry.ca</p>
                        </div>
                        <?php
                        $_SESSION['email_sent_coupon'] = 1;
                    } else {
                        ?>
                        <div class="get-this-coupon email-sent">
                            <div class="get-this-coupon-wrapper">
                                <div class="coupon-form-title email-sent-coupon">Something went wrong, please try again later.</div>
                            </div>
                        </div>
                        <?php
                    }
                }
                if ($actCoupon['BFC_Main_Image'] != '') {
                    $listing_image = $actCoupon['BFC_Main_Image'];
                } else {
                    $listing_image = $default_profile_image;
                }
                ?>
                <div class="listing-title-detail" <?php echo $style; ?>>
                    <img class="coupon-img" src="http://<?php echo DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo $actCoupon['BFC_Title'] ?>" longdesc="" />
                    <div class="listing-title"><?php echo $actCoupon['BFC_Title']; ?></div>
                    <div class="listing-address"><?php echo $actCoupon['BL_Listing_Title']; ?>, <?php echo $actCoupon['BL_Town']; ?></div>
                </div>
                <?php if (trim($actCoupon['BFC_Description']) != '') { ?>
                    <div class="menu-desc" <?php echo $style; ?>>
                        <div class="menu-item-heading-container">
                            <div class="menu-title">The Deal: </div>
                        </div>
                        <p><?php echo $actCoupon['BFC_Description']; ?></p>
                    </div>
                <?php } if (trim($actCoupon['BFC_Terms_Conditions']) != '') { ?>
                    <div class="menu-desc last-child" <?php echo $style; ?>>
                        <div class="menu-item-heading-container">
                            <div class="menu-title">Terms & Conditions</div>
                        </div>
                        <p><?php echo $actCoupon['BFC_Terms_Conditions']; ?></p>
                    </div>
                <?php } ?>
                <!-----Location start--->
                <div id="location-info" class="listing-desc" <?php echo $style; ?>>
                    <div class="location-map">
                        <?php
                        if ($actCoupon['BL_Lat'] && $actCoupon['BL_Long']) {
                            $latitude = $actCoupon['BL_Lat'];
                            $longitude = $actCoupon['BL_Long'];
                            ?>
                            <input type="hidden"  id="geo_code_address" value="" >     
                            <iframe
                                width="620"
                                height="390"
                                frameborder="0" style="border:0"
                                src="http://maps.google.ca/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<?php echo $latitude ?>,<?php echo $longitude ?>&amp;aq=&amp;sll=<?php echo $latitude ?>,<?php echo $longitude ?>&amp;sspn=0.027224,0.077162&amp;ie=UTF8&amp;t=m&amp;spn=0.022178,0.025663&amp;z=14&amp;iwloc=A&amp;output=embed&key=AIzaSyAWIfV5vbR8xmGewU5mvd0K1zAWeK0cNOg;" allowfullscreen>
                            </iframe>
                            <?PHP
                        } else if ($actCoupon['BL_Street'] && $actCoupon['BL_Town'] && $actCoupon['BL_Province']) {
                            $latitude = -34.397;
                            $longitude = 150.644;
                            ?>
                            <!--<div id="map_canvas"></div>-->
                            <iframe
                                width="620"
                                height="390"
                                frameborder="0" style="border:0"
                                src="http://maps.google.ca/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<?php echo $actCoupon['BL_Street'] ?>,<?php echo $actCoupon['BL_Town'] ?> <?php echo $actCoupon['BL_Province'] ?>&amp;aq=&amp;sll=<?php echo $actCoupon['BL_Street'] ?>,<?php echo $actCoupon['BL_Town'] ?> <?php echo $actCoupon['BL_Province'] ?>&amp;sspn=0.027224,0.077162&amp;ie=UTF8&amp;t=m&amp;spn=0.022178,0.025663&amp;z=14&amp;iwloc=A&amp;output=embed&key=AIzaSyAWIfV5vbR8xmGewU5mvd0K1zAWeK0cNOg;" allowfullscreen>
                            </iframe>
                            <input type="hidden"  id="geo_code_address" value="<?php echo $actCoupon['BL_Street'] ?>,<?php echo $actCoupon['BL_Town'] ?> <?php echo $actCoupon['BL_Province'] ?>" > 
                        <?PHP } ?>
                    </div>
                </div>
                <!-----Location End--->
                <!-----Get This Coupon Start--->
                <div class="get-this-coupon" id="get_coupon" <?php echo $style; ?>>
                    <div class="get-this-coupon-wrapper">
                        <div class="coupon-form-title">Get this Coupon</div>
                        <div class="coupon-form-text">Simply enter your email address and this coupon will be emailed to you. Be sure to check your promotional or junk mailbox.</div>
                        <form action="" method="POST">
                            <input type="hidden" name="op" value="save"/>
                            <input type="email" name="get_coupon" placeholder="Email Address" value="" required />
                            <input type="submit" class="coupon-button" name="go" value="go"/>
                        </form>
                    </div>
                </div>
                <!-----Get This Coupon End--->
            </div>
            <div class="listing-detail-right" <?php echo $padding; ?>>
                <div class="listing-detail-address">
                    <div class="get-coupon-btn" <?php echo $style; ?>><a href="#get_coupon">Get this Coupon</a></div>
                    <div class="coupon-listing-info">
                        <div class="get-coupon-listing"><?php echo $actCoupon['BL_Listing_Title']; ?></div>
                        <div class="get-coupon-town"><?php echo $actCoupon['BL_Town']; ?></div>
                    </div>
                    <div class="listing-detail-add-icon">
                        <img src="http://<?php echo DOMAIN ?>/images/locationicon.png" alt="Location">
                    </div>
                    <div class="listing-detail-add-detail">
                        <div class="address-heading"><?php echo $actCoupon['BL_Street']; ?></div>
                        <div class="address-heading"><?php echo $actCoupon['BL_Town']; ?></div>
                        <?php if ($actCoupon['BL_Province'] != '' || $actCoupon['BL_PostalCode'] != '') { ?>
                            <div class="address-heading">
                                <?php
                                echo ($actCoupon['BL_Province'] != '') ? $actCoupon['BL_Province'] . ', ' : '';
                                echo ($actCoupon['BL_PostalCode'] != '') ? $actCoupon['BL_PostalCode'] : '';
                                ?>
                            </div>     
                        <?php } ?>
                        <div class="address-heading">    
                            <a class="view-map" href="#location-info">View Map</a>
                        </div>     
                    </div>
                </div>
                <!--Social buttons-->
                <div class="listing-detail-address border-top-none">
                    <div class="fb-share-button" data-href="<?php print curPageURL(); ?>" data-layout="button_count"  data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php print curPageURL(); ?>%2F&amp;src=sdkpreparse">Share</a></div>
                    <iframe class="twitter-class" data-size="large"
                            src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=http%3A%2F%2F<?php print curPageURL(); ?>&related=twitterapi%2Ctwitter&text=<?php echo $actCoupon['BL_Listing_Title'] ?> in <?php echo $REGION['R_Name'] ?>"
                            width="70"
                            height="35"
                            style="border: 0; overflow: hidden; float: left;">
                    </iframe>
                </div>
                <div class="listing-detail-address border-top-none">
                    <?php if ($actCoupon['BL_Phone']) { ?>
                        <div class="listing-address-container">
                            <div class="listing-detail-add-icon">
                                <img src="http://<?php echo DOMAIN ?>/images/Listing-Phone.png" alt="Phone">
                            </div>
                            <div class="listing-detail-add-detail">
                                <div class="address-heading"><?php echo $actCoupon['BL_Phone']; ?></div>   
                            </div>
                        </div>    
                    <?php } if ($actCoupon['BL_Website']) { ?>
                        <div class="listing-address-container">
                            <div class="listing-detail-add-icon">
                                <img src="http://<?php echo DOMAIN ?>/images/Listing-Website.png" alt="Website">
                            </div>
                            <div class="listing-detail-add-detail">
                                <div class="address-heading listing-leftnav-margin">
                                    <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $actCoupon['BL_Website']) ?>" target="_blank">View Website</a> 
                                </div>   
                            </div>
                        </div>
                    <?php } if ($actCoupon['BL_Email']) { ?>
                        <div class="listing-address-container">
                            <div class="listing-detail-add-icon">
                                <img src="http://<?php echo DOMAIN ?>/images/Listing-Email.png" alt="Email">
                            </div>
                            <div class="listing-detail-add-detail">
                                <div class="address-heading listing-leftnav-margin">
                                    <a href="mailto:<?php echo $actCoupon['BL_Email']; ?>">Send Email</a>
                                </div>   
                            </div>
                        </div>
                        <?php
                    } if (!$actCoupon['BL_Hours_Disabled']) {
                        if ($actCoupon['BL_Hours_Appointment'] || $actCoupon['BL_Hour_Mon_From'] != '00:00:00' || $actCoupon['BL_Hour_Tue_From'] != '00:00:00' || $actCoupon['BL_Hour_Wed_From'] != '00:00:00' || $actCoupon['BL_Hour_Thu_From'] != '00:00:00' || $actCoupon['BL_Hour_Fri_From'] != '00:00:00' || $actCoupon['BL_Hour_Sat_From'] != '00:00:00' || $actCoupon['BL_Hour_Sun_From'] != '00:00:00') {
                            ?>
                            <div class="listing-address-container">
                                <div class="listing-detail-add-icon">
                                    <img src="http://<?php echo DOMAIN ?>/images/Listing-Hours.png" alt="Hours">
                                </div>
                                <div class="listing-detail-add-detail">
                                    <div class="address-heading listing-leftnav-margin accordion view-acc">
                                        <h4>View Hours</h4>
                                        <?php
                                        if ($actCoupon['BL_Hours_Appointment']) {
                                            ?>
                                            <div class="businessListing-timing">
                                                <div class="day-heading">Hours</div>
                                                <div class="day-timing">By Appointment</div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="businessListing-timing">
                                                <?php if ($actCoupon['BL_Hour_Mon_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Monday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($actCoupon['BL_Hour_Mon_From']); ?> <?php echo $actCoupon['BL_Hour_Mon_From'] == '00:00:01' || $actCoupon['BL_Hour_Mon_From'] == '00:00:02' || $actCoupon['BL_Hour_Mon_From'] == '00:00:03' ? '' : '- ' . ctime($actCoupon['BL_Hour_Mon_To'], true); ?>
                                                    </div>
                                                <?php } if ($actCoupon['BL_Hour_Tue_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Tuesday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($actCoupon['BL_Hour_Tue_From']); ?> <?php echo $actCoupon['BL_Hour_Tue_From'] == '00:00:01' || $actCoupon['BL_Hour_Tue_From'] == '00:00:02' || $actCoupon['BL_Hour_Tue_From'] == '00:00:03' ? '' : '- ' . ctime($actCoupon['BL_Hour_Tue_To'], true); ?>
                                                    </div>
                                                <?php } if ($actCoupon['BL_Hour_Wed_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Wednesday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($actCoupon['BL_Hour_Wed_From']); ?> <?php echo $actCoupon['BL_Hour_Wed_From'] == '00:00:01' || $actCoupon['BL_Hour_Wed_From'] == '00:00:02' || $actCoupon['BL_Hour_Wed_From'] == '00:00:03' ? '' : '- ' . ctime($actCoupon['BL_Hour_Wed_To'], true); ?>
                                                    </div>
                                                <?php } if ($actCoupon['BL_Hour_Thu_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Thursday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($actCoupon['BL_Hour_Thu_From']); ?> <?php echo $actCoupon['BL_Hour_Thu_From'] == '00:00:01' || $actCoupon['BL_Hour_Thu_From'] == '00:00:02' || $actCoupon['BL_Hour_Thu_From'] == '00:00:03' ? '' : '- ' . ctime($actCoupon['BL_Hour_Thu_To'], true); ?>
                                                    </div>
                                                <?php } if ($actCoupon['BL_Hour_Fri_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Friday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($actCoupon['BL_Hour_Fri_From']); ?> <?php echo $actCoupon['BL_Hour_Fri_From'] == '00:00:01' || $actCoupon['BL_Hour_Fri_From'] == '00:00:02' || $actCoupon['BL_Hour_Fri_From'] == '00:00:03' ? '' : '- ' . ctime($actCoupon['BL_Hour_Fri_To'], true); ?>
                                                    </div>
                                                <?php } if ($actCoupon['BL_Hour_Sat_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Saturday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($actCoupon['BL_Hour_Sat_From']); ?> <?php echo $actCoupon['BL_Hour_Sat_From'] == '00:00:01' || $actCoupon['BL_Hour_Sat_From'] == '00:00:02' || $actCoupon['BL_Hour_Sat_From'] == '00:00:03' ? '' : '- ' . ctime($actCoupon['BL_Hour_Sat_To'], true); ?>
                                                    </div>
                                                <?php } if ($actCoupon['BL_Hour_Sun_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Sunday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($actCoupon['BL_Hour_Sun_From']); ?> <?php echo $actCoupon['BL_Hour_Sun_From'] == '00:00:01' || $actCoupon['BL_Hour_Sun_From'] == '00:00:02' || $actCoupon['BL_Hour_Sun_From'] == '00:00:03' ? '' : '- ' . ctime($actCoupon['BL_Hour_Sun_To'], true); ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>   
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <?php
                    $sql = "SELECT AI_Name FROM tbl_Business_Listing_Ammenity 
                            LEFT JOIN tbl_Ammenity_Icons ON AI_ID = BLA_BA_ID 
                            WHERE BLA_BL_ID = '" . encode_strings($actCoupon['BL_ID'], $db) . "' AND AI_Image <> '' ORDER BY AI_Name";
                    $resultAI = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    $AIcount = mysql_num_rows($resultAI);
                    if ($AIcount > 0) {
                        ?>
                        <div class="listing-address-container">
                            <div class="listing-detail-add-icon">
                                <img src="http://<?php echo DOMAIN ?>/images/Listing-Ammenity.png" alt="Ammenity">
                            </div>
                            <div class="listing-detail-add-detail">
                                <div class="address-heading listing-leftnav-margin accordion view-acc">                               
                                    <h4>View Ammenities</h4>
                                    <div class="businessListing-timing">
                                        <?php
                                        while ($rowAI = mysql_fetch_assoc($resultAI)) {
                                            ?>
                                            <div class="acc-body">
                                                <?php echo $rowAI['AI_Name']; ?>
                                            </div>    
                                        <?PHP } ?>                                  
                                    </div> 
                                </div>   
                            </div>                      
                        </div> 
                        <?php
                    }
                    $sql_dpdf = "SELECT * FROM tbl_Description_PDF WHERE D_BL_ID='" . encode_strings($actCoupon['BL_ID'], $db) . "' ORDER BY D_PDF_Order ASC";
                    $result_dpdf = mysql_query($sql_dpdf, $db) or die("Invalid query: $sql_dpdf -- " . mysql_error());
                    $pdf_num = mysql_num_rows($result_dpdf);
                    if ($pdf_num > 0) {
                        ?>
                        <div class="listing-address-container">
                            <div class="listing-detail-add-icon">
                                <img src="http://<?php echo DOMAIN ?>/images/Listing-Download.png" alt="Download">
                            </div>
                            <div class="listing-detail-add-detail">
                                <div class="address-heading listing-leftnav-margin accordion view-acc">
                                    <h4>View Downloads</h4>
                                    <div class="businessListing-timing">
                                        <?php
                                        while ($data_dpdf = mysql_fetch_assoc($result_dpdf)) {
                                            ?>
                                            <div class="acc-body theme-downloads"><a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $data_dpdf['D_PDF'] ?>"><?php echo $data_dpdf['D_PDF_Title']; ?></a></div>
                                            <?php
                                        }
                                        ?>
                                    </div>   
                                </div>   
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php
                $social_blld = $actCoupon['BL_ID'];
                $sql_social = "SELECT * FROM  tbl_Business_Social where BS_BL_ID=$social_blld";
                $social_result = mysql_query($sql_social);
                $row_soical = mysql_fetch_assoc($social_result);
                if ($row_soical['BS_FB_Link'] || $row_soical['BS_T_Link'] || $row_soical['BS_Y_Link'] || $row_soical['BS_I_Link']) {
                    ?>
                    <div class="listings-social-icon">
                        <?php
                        if ($row_soical['BS_FB_Link'] != '') {
                            $facebook_link = preg_replace('/^www\./', '', $row_soical['BS_FB_Link']);
                            ?>
                            <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $facebook_link) ?>" target="_blank"><img src="<?php echo 'http://' . DOMAIN . '/images/facebook.png' ?>" alt="facebook"/></a>
                            <?php
                        }
                        if ($row_soical['BS_I_Link'] != '') {
                            $rs_i_link = preg_replace('/^www\./', '', $row_soical['BS_I_Link']);
                            ?> 
                            <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $rs_i_link) ?>" target="_blank"><img src="<?php echo 'http://' . DOMAIN . '/images/instagram.png' ?>" alt="instagram"/></a>
                            <?php
                        }
                        if ($row_soical['BS_T_Link'] != '') {
                            $rs_t_link = preg_replace('/^www\./', '', $row_soical['BS_T_Link']);
                            $twitter = explode("?", $rs_t_link);
                            ?>
                            <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $twitter[0]) ?>" target="_blank"><img src="<?php echo 'http://' . DOMAIN . '/images/twitter.png' ?>" alt="twitter"/></a>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
            <!--right end -->
        </div>
    </div>
</section>
<a href="#" class="scrollToTop"><img src="http://<?php echo DOMAIN ?>/theme_icons/scrollup.png" alt="Scroll To Top"></a>
<?php require_once 'include/public/footer.php'; ?>
