<?php
require_once '../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
require_once 'include/public/map_header.php';
?>
<!--Map section-->
<div class="sidebar_map">
    <?PHP
    //get all listings
    $sqlList = "SELECT BL_ID, BL_Listing_Title, BL_Name_SEO, BL_Lat, BL_Long, BL_Street, BL_Town, BFC_ID, BFC_Title, BFC_Thumbnail, BFCCM_C_ID,
                RC.RC_Map_Icon as subcat_icon, RC1.RC_Map_Icon as cat_icon FROM tbl_Business_Feature_Coupon
                LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
                LEFT JOIN tbl_Category C ON C.C_ID = BFCCM_C_ID
                LEFT JOIN tbl_Category C1 ON C1.C_ID = C.C_Parent
                LEFT JOIN tbl_Region_Category RC ON RC.RC_C_ID = C.C_ID AND RC.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                LEFT JOIN tbl_Region_Category RC1 ON RC1.RC_C_ID = C1.C_ID AND RC1.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                LEFT JOIN tbl_Business_Listing ON BL_ID = BFC_BL_ID
                WHERE BFC_Status = 1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date = 0000-00-00) 
                AND hide_show_listing = '1' $include_free_listings_on_map GROUP BY BFC_ID ORDER BY BFC_ID DESC";
    // generate thumbnails for listings on left side bar and markers for map
    $resList = mysql_query($sqlList, $db) or die(mysql_error() . ' - ' . $sqlList);
    $counter = 0;
    while ($List = mysql_fetch_assoc($resList)) {
        //add to markers for map
        if ($List['BFC_Thumbnail'] != '') {
            $image = '<div class="thumbnail"><img width="130" height="90" src="http://' . (DOMAIN . IMG_LOC_REL . $List['BFC_Thumbnail']) . '" alt="' . htmlspecialchars(trim($List['BFC_Title']), ENT_QUOTES) . '" /></div>';
        } else {
            $image = '<div class="thumbnail"><img src="http://' . DOMAIN . IMG_LOC_REL . $default_thumbnail_image . '" alt="' . htmlspecialchars(trim($List['BFC_Title']), ENT_QUOTES) . '" /></div>';
        }
        if ($List['subcat_icon'] != '') {
            $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $List['subcat_icon'];
        } elseif ($List['cat_icon'] != '') {
            $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $List['cat_icon'];
        } else {
            $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . 'default.png';
        }
        if ($List['BL_Lat'] && $List['BL_Long']) {
            $markers[] = array(
                'id' => $List['BFC_ID'],
                'lat' => $List['BL_Lat'],
                'lon' => $List['BL_Long'],
                'name' => $List['BFC_Title'],
                'path' => '/profile/' . (($list['BFC_Title'] != "") ? clean($List['BFC_Title']) : $List['BL_Name_SEO']) . '/' . $List['BFC_ID'] . '/',
                'icon' => $icon,
                'main_photo' => $image,
                'address' => htmlspecialchars(trim($List['BL_Street']), ENT_QUOTES),
                'town' => htmlspecialchars(trim($List['BL_Town']), ENT_QUOTES)
            );
        }
        //show only limited records in right panel
        if ($counter < $REGION['R_Map_Listings_Limit']) {
            ?>
            <div class='thumbnails static-thumbs'>
                <div class="thumb-item">
                    <a href="/profile/<?php echo (($List['BFC_Title'] != "") ? clean($List['BFC_Title']) : $List['BL_Name_SEO']) ?>/<?php echo $List['BFC_ID'] ?>/"> 
                        <img src="http://<?php echo ($List['BFC_Thumbnail'] == '') ? DOMAIN . IMG_LOC_REL . $default_thumbnail_image : (DOMAIN . IMG_LOC_REL . $List['BFC_Thumbnail']) ?>" alt="<?php echo $List['BFC_Title'] ?>" />
                        <h3 class="thumbnail-heading"><?php echo $List['BFC_Title'] ?></h3>
                        <h3 class="thumbnail-desc"><?php echo $List['BL_Listing_Title']; ?></h3>
                    </a> 
                </div>
            </div>
            <?php
        }
        $counter++;
    }
    $markers = json_encode($markers);
    ?>
</div><!-- .nav-left-outer -->

<div class="map_wrapper">
    <div id="map_canvas">&nbsp;</div>
</div> <!-- .content-right-outer -->
<?php
$map_center['latitude'] = $REGION['R_Lat'];
$map_center['longitude'] = $REGION['R_Long'];
$map_center['zoom'] = $REGION['R_Zoom'];
$kml_json = json_encode(array());
require_once 'map_script.php';
?>