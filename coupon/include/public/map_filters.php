<?php
//apply dynamic css to map filters
$map_filter_text = explode("-", $REGION['R_Map_Filters_Text']);
$map_filter_drop_down_text = explode("-", $REGION['R_Map_Filters_Drop_Down_Text']);
?>
<style type="text/css">
    .header-main-nav.maps {
        background-color: <?php echo $REGION['R_Map_Filters_Background_Color'] ?>;
    }
    .header-main-nav.maps .main-nav-wrapper ul li span.filter-title {
        font-family: <?php echo (isset($THEME_ARRAY[$map_filter_text[0]]) === true && empty($THEME_ARRAY[$map_filter_text[0]]) === false) ? $THEME_ARRAY[$map_filter_text[0]] : ""; ?>;
        font-size: <?php echo $map_filter_text[1] ?>px;
        color: <?php echo $map_filter_text[2] ?>;
    }
    .header-main-nav.maps .main-nav-wrapper ul li span.filter-title:hover {
        background-color: <?php echo $map_filter_text[2] ?>;
        color: <?php echo $REGION['R_Map_Filters_Background_Color'] ?>; 
    }
    .header-main-nav.maps .main-nav-wrapper ul li ul.submenu{
        background-color: <?php echo $REGION['R_Map_Filters_Drop_Down_Background_Color'] ?>;
    }
    .header-main-nav.maps .main-nav-wrapper ul li ul.submenu .dd-inner .map-filter-checkbox .label{
        font-family: <?php echo (isset($THEME_ARRAY[$map_filter_drop_down_text[0]]) === true && empty($THEME_ARRAY[$map_filter_drop_down_text[0]]) === false) ? $THEME_ARRAY[$map_filter_drop_down_text[0]] : ""; ?>;
        color: <?php echo $map_filter_drop_down_text[2] ?>;
    }
    .map-filter-checkbox{
        font-size: <?php echo $map_filter_drop_down_text[1] ?>px;
    }
</style>
<form method="POST" id="map-filters-form" action="">
    <!--hidden field to know if map page or not-->
    <input type="hidden" name="is_map" value="<?php echo isset($is_map) ? $is_map : 0 ?>" />
    <div class="map-bottom-filters">
        <?php
        $sql_nav = "SELECT C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category
                    LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID
                    WHERE C_Is_Product_Web = 1 AND RC_Status = 0 AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
        $result = mysql_query($sql_nav);
        ?>
        <!--Main Navigation End...-->
        <nav class="header-main-nav maps">
            <div class="main-nav-container">
                <div class="main-nav-wrapper">
                    <ul>
                        <li><span class="filter-title no-link">Map Filters</span></li>
                        <?php
                        while ($row = mysql_fetch_array($result)) {
                            if ($row['RC_Name'] != '') {
                                echo '<li><span class="filter-title">' . $row['RC_Name'] . '</span>';
                            } else {
                                echo '<li><span class="filter-title">' . $row['C_Name'] . '</span>';
                            }
                            $c_parent = $row['C_ID'];
                            $sql_nav_sub = "SELECT C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category 
                                            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                                            WHERE C_Parent = '$c_parent' and RC_Status=0 AND RC_R_ID > 0
                                            ORDER BY RC_Order";

                            $result_sub = mysql_query($sql_nav_sub);
                            echo '<ul class="submenu"><li><div class="dd-inner">';
                            ?>
                            <?php
                            while ($rowSub = mysql_fetch_assoc($result_sub)) {
                                $sqlGetCoupons = "SELECT BFC_ID, BFC_Thumbnail, BFC_Title, RC_Name, C_Name, BL_Listing_Title, BL_Name_SEO, BL_ID FROM tbl_Business_Feature_Coupon
                                                  LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
                                                  LEFT JOIN tbl_Category ON C_ID = BFCCM_C_ID                                    
                                                  LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                                                  LEFT JOIN tbl_Business_Listing ON BL_ID = BFC_BL_ID 
                                                  WHERE BFCCM_C_ID = '" . encode_strings($rowSub['C_ID'], $db) . "' AND BFC_Status = 1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date =0000-00-00) AND hide_show_listing='1' ";//  
                                $resCoupons = mysql_query($sqlGetCoupons);
                                $totalCoupon = mysql_num_rows($resCoupons);
                                if ($totalCoupon > 0) {
                                ?>
                                <div class="map-filter-checkbox">
                                    <div class="squaredCheckbox">
                                        <input type="checkbox" id="cid_<?php echo $c_parent['C_ID'] ?>" name="sub_category[<?php echo $c_parent ?>][]" value="<?php echo $rowSub['C_ID'] ?>"/>
                                        <span class="checkboxStyling"></span>
                                    </div>
                                    <label for="cid_<?php echo $rowSub['C_ID'] ?>" class="label">
                                        <?php
                                        if ($rowSub['RC_Name'] != '') {
                                            echo $rowSub['RC_Name'];
                                        } else {
                                            echo $rowSub['C_Name'];
                                        }
                                        ?>
                                    </label>
                                </div>
                                <?php
                                }
                            }
                            echo "</li></ul>";
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</form>