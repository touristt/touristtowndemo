<?php
//check to pass mobile users to mobile version
$useragent = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
    header('Location: http://' . $REGION['R_Domain'] . '/mobile/maps.php');
    exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
    <head>
        <meta charset="utf-8" http-equiv="Content-Type" content="text/html">
            <title><?php echo isset($SEOtitle) ? $SEOtitle : $REGION['R_SEO_Title'] ?></title>
            <meta name="description" content="<?php echo isset($SEOdescription) ? $SEOdescription : $REGION['R_SEO_Description'] ?>" />
            <meta name="keywords" content="<?php echo isset($SEOkeywords) ? $SEOkeywords : $REGION['R_SEO_Keywords'] ?>" />

            <!-- CSS FILES -->
            <link rel="stylesheet" type="text/css" href="/stylesheets/mainscreen.min.css" media="all" />

            <!-- JS FILES -->
            <script src="/include/jquery-1.12.4.js"></script>
            <script src="/include/jquery-ui.js"></script>
            <script type="text/javascript" src="/include/plugins/cycle/jquery.cycle.all.js"></script>
            <script type="text/javascript" src="/include/js/jquery.bxslider.min.js"></script>
            <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo STATIC_MAP_API_KEY ?>" type="text/javascript"></script>
            <script src="/stylesheets/docs/assets/css/jquery.blockUI.js" type="text/javascript"></script>
            <script type="text/javascript" src="/include/public.js"></script>
            <?PHP
            //Getting Region Theme
            $getTheme = "SELECT * FROM  `tbl_Theme_Options` WHERE TO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
            $themeRegion = mysql_query($getTheme, $db) or die("Invalid query: $getTheme -- " . mysql_error());
            $THEME = mysql_fetch_assoc($themeRegion);

            $header_text_value = explode("-", $THEME['TO_Header_Text']);
            $season_text_value = explode("-", $THEME['TO_Season_Text']);
            $sec_nav_value = explode("-", $THEME['TO_Secondary_Navigation']);
            $main_nav_value = explode("-", $THEME['TO_Main_Navigation']);
            $slider_title_value = explode("-", $THEME['TO_Slider_Title']);
            $trip_planner_number_value = explode("-", $THEME['TO_Trip_Planner_Number']);
            $slider_desc_value = explode("-", $THEME['TO_Slider_Description']);
            $main_dd_value = explode("-", $THEME['TO_Drop_Down_Menu']);
            $main_page_title_value = explode("-", $THEME['TO_Main_Page_Title']);
            $main_page_body_value = explode("-", $THEME['TO_Main_Page_Body_Copy']);
            $cat_title_value = explode("-", $THEME['TO_Thumbnail_Category_Tite']);
            $view_all_value = explode("-", $THEME['TO_Thumbnail_View_All']);
            $sub_cat_value = explode("-", $THEME['TO_Thumbnail_Sub_Category_Title']);
            $listing_title_value = explode("-", $THEME['TO_Thumbnail_Listing_Title']);
            $event_main_title_value = explode("-", $THEME['TO_Homepage_Event_Main_Title']);
            $event_date_value = explode("-", $THEME['TO_Homepage_Event_Date']);
            $event_title_value = explode("-", $THEME['TO_Homepage_Event_Title']);
            $event_text_value = explode("-", $THEME['TO_Homepage_Event_Text']);
            $map_title_value = explode("-", $THEME['TO_Map_Filter_Font']);
            $footer_title_value = explode("-", $THEME['TO_Footer_Text_Title']);
            $footer_text_value = explode("-", $THEME['TO_Footer_Text']);
            $footer_links_value = explode("-", $THEME['TO_Footer_Links']);
            $footer_disc_value = explode("-", $THEME['TO_Footer_Disclaimer']);
            $listing_dd_text_value = explode("-", $THEME['TO_Listing_Drop_Down_Text']);
            $menu_text_value = explode("-", $THEME['TO_Menu_Text']);
            $listing_sub_heading_value = explode("-", $THEME['TO_Listing_Sub_Heading']);
            $listing_sub_nav_value = explode("-", $THEME['TO_Listing_Sub_Nav_Text']);
            $listing_day_text_value = explode("-", $THEME['TO_Listing_Day_Text']);
            $listing_hours_text_value = explode("-", $THEME['TO_Listing_Hours_Text']);
            $amenities_text_value = explode("-", $THEME['TO_Amenities_Text']);
            $download_text_value = explode("-", $THEME['TO_Downloads_Text']);
            $whats_near_dd_value = explode("-", $THEME['TO_Whats_Nearby_Drop_Down_Text']);
            $calendar_dd_value = explode("-", $THEME['TO_Calendar_Drop_Down_Text']);
            $calendar_go_text_value = explode("-", $THEME['TO_Calendar_Go_Button_Text']);
            $pagination_number_value = explode("-", $THEME['TO_Pagination_Number_Text']);
            $pagination_text_value = explode("-", $THEME['TO_Pagination_Text']);
            $listing_main_title_value = explode("-", $THEME['TO_Listing_Title_Text']);
            $listing_address_value = explode("-", $THEME['TO_Listing_Location_Text']);
            $listing_nav_value = explode("-", $THEME['TO_Listing_Navigation_Text']);
            $general_paragraph_value = explode("-", $THEME['TO_General_Body_Copy']);

            //CSS With Image variables
            $to_navigation_bar_bg_color = explode(">", $THEME['TO_Navigation_Bar']);
            $home_des_bg_color = explode(">", $THEME['TO_Homepage_Description_Background']);
            $home_event_des_bg_color = explode(">", $THEME['TO_Homepage_Events_Description_Background']);
            $listing_nav_gallary_bg_color = explode(">", $THEME['TO_Listing_Nav_Gallary_Background']);
            $listing_whats_nearby_bg_color = explode(">", $THEME['TO_Listing_Whats_Nearby_Background']);

            //Getting Region Theme Font, Due to our DB structure we are applying new query rather than JOIN
            $getFont = "SELECT * FROM `tbl_Theme_Options_Fonts`";
            $themeFontRegion = mysql_query($getFont, $db) or die("Invalid query: $getFont -- " . mysql_error());
            $THEME_ARRAY = array();
            while ($THEME_FONT = mysql_fetch_assoc($themeFontRegion)) {
                $THEME_ARRAY[$THEME_FONT['TOF_ID']] = $THEME_FONT['TOF_Name'];
            }
            $retrun_to_web_text = explode("-", $REGION['R_Map_Return_To_Web_Text']); ?>
            <style>
                /*!
                * Tourist Town
                * Date: Mon Jan 02 2017 17:00:00 PST
                * http://touristtown.ca
                *
                * Copyright 2017, 2018 TouristTown.
                *
                */

                /* Header
                -----------------------------------------------------------------------------------------------------------*/
                .tt-icons.tt-return a {
                    font-family: <?php echo (isset($THEME_ARRAY[$retrun_to_web_text[0]]) === true && empty($THEME_ARRAY[$retrun_to_web_text[0]]) === false) ? $THEME_ARRAY[$retrun_to_web_text[0]] : ""; ?>;
                    font-size: <?php echo $retrun_to_web_text[1] ?>px;
                    color: <?php echo $retrun_to_web_text[2] ?>!important;
                }
                a {
                    color: <?php echo $THEME['TO_Text_Link'] ?>;
                }
                .theme-paragraph, .theme-paragraph p, .theme-paragraph ul li{
                    font-family: <?php echo (isset($THEME_ARRAY[$general_paragraph_value[0]]) === true && empty($THEME_ARRAY[$general_paragraph_value[0]]) === false) ? $THEME_ARRAY[$general_paragraph_value[0]] : ""; ?>;
                    font-size: <?php echo $general_paragraph_value[1] ?>px;
                    color: <?php echo $general_paragraph_value[2] ?>;
                    line-height: <?php echo $THEME['TO_General_Body_Copy_Line_Spacing'] ?>em;
                }
/*                .inside-wrapper, .tt-icons.tt-return a  {
                    font-family: <?php echo (isset($THEME_ARRAY[$header_text_value[0]]) === true && empty($THEME_ARRAY[$header_text_value[0]]) === false) ? $THEME_ARRAY[$header_text_value[0]] : ""; ?>;
                    font-size: <?php echo $header_text_value[1] ?>px;
                }*/
                .header-top-nav, .inside-wrapper {
                    background-color: <?php echo $THEME['TO_Header_Background_Color'] ?>;
                }
/*                .header-top-nav-inside, .tt-icons.tt-return a {
                    color: <?php echo $header_text_value[2] ?>;
                }*/
                .inside-wrapper .season-dropdown select {
                    font-family: <?php echo (isset($THEME_ARRAY[$season_text_value[0]]) === true && empty($THEME_ARRAY[$season_text_value[0]]) === false) ? $THEME_ARRAY[$season_text_value[0]] : ""; ?>;
                    font-size: <?php echo $season_text_value[1] ?>px;
                    color: <?php echo $season_text_value[2] ?>;
                    background-color: <?php echo $THEME['TO_Season_Drop_Down_Background'] ?>;
                }
                .inside-wrapper .season-dropdown .drop-down-arrow {
                    border-left-color: <?php echo $THEME['TO_Season_Drop_Down_Arrow'] ?>;
                }
                .logo-top-nav {
                    font-family: <?php echo (isset($THEME_ARRAY[$sec_nav_value[0]]) === true && empty($THEME_ARRAY[$sec_nav_value[0]]) === false) ? $THEME_ARRAY[$sec_nav_value[0]] : ""; ?>;
                    font-size: <?php echo $sec_nav_value[1] ?>px;
                }
                .logo-top-nav ul li a {
                    color: <?php echo $sec_nav_value[2] ?>;
                }
                .logo-nav-container {
                    margin-top: <?php echo $THEME['TO_Logo_Space_Above'] ?>px;
                    margin-bottom: <?php echo $THEME['TO_Logo_Space_Below'] ?>px;
                }
                .main-nav-wrapper ul li a {
                    font-family: <?php echo (isset($THEME_ARRAY[$main_nav_value[0]]) === true && empty($THEME_ARRAY[$main_nav_value[0]]) === false) ? $THEME_ARRAY[$main_nav_value[0]] : ""; ?>;
                    font-size: <?php echo $main_nav_value[1] ?>px;
                    color: <?php echo $main_nav_value[2] ?>;
                }
                .main-nav-wrapper ul li a:hover {
                    background-color: <?php echo $main_nav_value[2] ?>;
                }
                .main-nav-wrapper ul li ul.submenu .dd-inner .column .maps-text {
                    color: <?php echo $main_nav_value[2] ?>;
                }
                .main-nav-wrapper ul li ul.submenu {
                    border-color: <?php echo $main_nav_value[2] ?>;
                    background-color: <?php echo $THEME['TO_Drop_Down_Menu_BG'] ?>;
                }
                .main-nav-wrapper ul li ul.submenu .dd-inner .column {
                    background-color: <?php echo $THEME['TO_Drop_Down_Menu_BG'] ?>;
                }
                .main-nav-wrapper ul li ul.submenu .dd-inner .column a {
                    font-family: <?php echo (isset($THEME_ARRAY[$main_dd_value[0]]) === true && empty($THEME_ARRAY[$main_dd_value[0]]) === false) ? $THEME_ARRAY[$main_dd_value[0]] : ""; ?>;
                    font-size: <?php echo $main_dd_value[1] ?>px;
                    color: <?php echo $main_dd_value[2] ?>;
                }
                .navigation-bar {
                    <?php if ($to_navigation_bar_bg_color[0] != "") { ?>
                        background-image: url("<?php echo IMG_ICON_REL . $to_navigation_bar_bg_color[0]; ?>");
                        width: 100%;
                        height: 10px;
                        background-color: transparent;
                    <?php } else { ?>
                        background-color: <?php echo $to_navigation_bar_bg_color[1] ?>;
                    <?php } ?>
                }
                .my-trips-count span {
                    background-color: <?php echo $THEME['TO_Trip_Planner_Number_Background'] ?>;
                    font-family: <?php echo (isset($THEME_ARRAY[$trip_planner_number_value[0]]) === true && empty($THEME_ARRAY[$trip_planner_number_value[0]]) === false) ? $THEME_ARRAY[$trip_planner_number_value[0]] : ""; ?>;
                    font-size: <?php echo $trip_planner_number_value[1] ?>px;
                    color: <?php echo $trip_planner_number_value[2] ?>;
                }
                /* Main Navigation End*/
                /* Header End
                -----------------------------------------------------------------------------------------------------------*/
                /* Thumbnail Grid
                -----------------------------------------------------------------------------------------------------------*/
                .grid-left-title.heading-text {
                    font-family: <?php echo (isset($THEME_ARRAY[$cat_title_value[0]]) === true && empty($THEME_ARRAY[$cat_title_value[0]]) === false) ? $THEME_ARRAY[$cat_title_value[0]] : ""; ?>;
                    font-size: <?php echo $cat_title_value[1] ?>px;
                    color: <?php echo $cat_title_value[2] ?>;
                }
                .grid-left-view h3 a {
                    font-family: <?php echo (isset($THEME_ARRAY[$view_all_value[0]]) === true && empty($THEME_ARRAY[$view_all_value[0]]) === false) ? $THEME_ARRAY[$view_all_value[0]] : ""; ?>;
                    font-size: <?php echo $view_all_value[1] ?>px;
                    color: <?php echo $view_all_value[2] ?>;
                }
                ul.bxslider li a h3.thumbnail-heading, .thumbnails.static-thumbs .thumb-item a h3.thumbnail-heading {
                    font-family: <?php echo (isset($THEME_ARRAY[$sub_cat_value[0]]) === true && empty($THEME_ARRAY[$sub_cat_value[0]]) === false) ? $THEME_ARRAY[$sub_cat_value[0]] : ""; ?>;
                    font-size: <?php echo $sub_cat_value[1] ?>px;
                    color: <?php echo $sub_cat_value[2] ?>;
                }
                ul.bxslider li a h3.thumbnail-desc, .thumbnails.static-thumbs .thumb-item a h3.thumbnail-desc {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_title_value[0]]) === true && empty($THEME_ARRAY[$listing_title_value[0]]) === false) ? $THEME_ARRAY[$listing_title_value[0]] : ""; ?>;
                    font-size: <?php echo $listing_title_value[1] ?>px;
                    color: <?php echo $listing_title_value[2] ?>;
                }
                .thumbnail-grid .grid-wrapper .grid-inner h1.heading-text{
                    font-family: <?php echo (isset($THEME_ARRAY[$main_page_title_value[0]]) === true && empty($THEME_ARRAY[$main_page_title_value[0]]) === false) ? $THEME_ARRAY[$main_page_title_value[0]] : ""; ?>;
                    font-size: <?php echo $main_page_title_value[1] ?>px;
                    color: <?php echo $main_page_title_value[2] ?>;
                }
                /* Thumbnail Grid End
                -----------------------------------------------------------------------------------------------------------*/
                /* Map Filter Start
                -----------------------------------------------------------------------------------------------------------*/
                .map-bottom-filters .map-filter.category {
                    font-family: <?php echo (isset($THEME_ARRAY[$map_title_value[0]]) === true && empty($THEME_ARRAY[$map_title_value[0]]) === false) ? $THEME_ARRAY[$map_title_value[0]] : ""; ?>;
                    font-size: <?php echo $map_title_value[1] ?>px;
                    color: <?php echo $map_title_value[2] ?>;
                }
                /* Map Filter End
                -----------------------------------------------------------------------------------------------------------*/
            </style>
            <script type="text/javascript">

                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', '<?php echo $REGION['R_Tracking_ID'] ?>']);
                _gaq.push(['_trackPageview']);

                (function () {
                    var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(ga, s);
                })();
            </script>
    </head>
    <body>
        <?PHP
        // Include Free listings
        if ($REGION['R_Include_Free_Listings_On_Map'] == 1) {
            $include_free_listings_on_map = '';
        } else {
            $include_free_listings_on_map = ' AND BL_Listing_Type > 1';
        }

        //Defualt Header Images
        if ($THEME['TO_Default_Header_Desktop'] != '') {
            $default_header_image = $THEME['TO_Default_Header_Desktop'];
        } else {
            $default_header_image = 'Default-Main-Desktop.jpg';
        }

        //Defualt Thumbnail Images
        if ($THEME['TO_Default_Thumbnail_Desktop'] != '') {
            $default_thumbnail_image = $THEME['TO_Default_Thumbnail_Desktop'];
        } else {
            $default_thumbnail_image = 'Default-Thumbnail-Desktop.jpg';
        }
        //Getting Regions if it has child regions
        /* Becareful...
          ----------------------------------------------------------------------------------------------------------- */
        /* $regionList is used in multiple querries.
          ----------------------------------------------------------------------------------------------------------- */
        $regionList = '';
        if ($REGION['R_Parent'] == 0) {
            $sql = "SELECT * FROM tbl_Region_Multiple LEFT JOIN tbl_Region ON RM_Child = R_ID WHERE RM_Parent = '" . encode_strings($REGION['R_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $first = true;
            $regionList .= "(";
            while ($row = mysql_fetch_assoc($result)) {
                if ($first) {
                    $first = false;
                } else {
                    $regionList .= ",";
                }
                $regionList .= $row['RM_Child'];
            }
            $regionList .= ")";
        }
        ?>
        <!--Start Header-->
        <header class="main-header maps">
            <!--Top Navigation Official Site...-->
            <div class="header-top-nav">
                <div class="header-top-nav-inside">
                    <div class="inside-wrapper">
                        <div class="site-name">
                            <?php if ($REGION['R_Map_Logo'] != '') { ?>
                                <a href="/">
                                    <img src="http://<?php echo DOMAIN; ?>/images/DB/<?php echo $REGION['R_Map_Logo']; ?>"  alt="<?php echo isset($REGION['R_Logo_Name']) ? $REGION['R_Logo_Name'] : ''; ?>" />
                                </a>
                            <?php } ?>
                        </div>
                        <?php
                        $sql_icons = "SELECT * FROM tbl_Region_Social 
                                  WHERE RS_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
                        $result_icons = mysql_query($sql_icons, $db) or die(mysql_error());
                        $row_icons = mysql_fetch_assoc($result_icons);
                        ?>
                        <div class="social-icons" style="margin-top: 20px;">
                            <div class="icons-wrapper">
                                <!--                                <div class="tt-icons tt-search">
                                                                    <img onClick="displayHeaderForm();" src="http://<?php echo DOMAIN ?>/images/<?php echo ($row_icons['RS_SE_Icon'] != "") ? 'DB/' . $row_icons['RS_SE_Icon'] : 'search.png' ?>" alt="search"/>
                                                                    <form method="GET" class="navbar-search" id="search_form" action="/search.php">
                                                                        <div class="search_header">
                                                                            <div class="search_field">
                                                                                <label for="txtSearch" class="label-hidden">Search</label>
                                                                                <input type="text" id="txtSearch" name="txtSearch" onfocus="clearSearch();" placeholder="Search"/>
                                                                            </div>
                                                                            <div class="search_button"><input type="submit" name="submit" value="Go"/></div>
                                                                        </div>
                                                                    </form>
                                                                </div>-->
                                <?php if ($row_icons['RS_FB_Link'] != "") { ?><div class="tt-icons"><a target="_blank" href="http://<?php echo $row_icons['RS_FB_Link'] ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_FB_Icon'] != "") ? 'DB/' . $row_icons['RS_FB_Icon'] : 'facebook.png' ?>" alt="Facebook"/></a></div><?php } ?>
                                <?php if ($row_icons['RS_I_Link'] != "") { ?><div class="tt-icons"><a target="_blank" href="http://<?php echo $row_icons['RS_I_Link'] ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_I_Icon'] != "") ? 'DB/' . $row_icons['RS_I_Icon'] : 'instagram.png' ?>" alt="Instagram"/></a></div><?php } ?>
                                <?php if ($row_icons['RS_Y_Link'] != "") { ?><div class="tt-icons"><a target="_blank" href="http://<?php echo $row_icons['RS_Y_Link'] ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_Y_Icon'] != "") ? 'DB/' . $row_icons['RS_Y_Icon'] : 'youtube-icon.png' ?>" alt="Youtube"/></a></div><?php } ?>
                                <?php if ($row_icons['RS_T_Link'] != "") { ?><div class="tt-icons"><a target="_blank" href="http://<?php echo $row_icons['RS_T_Link'] ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_T_Icon'] != "") ? 'DB/' . $row_icons['RS_T_Icon'] : 'twitter.png' ?>" alt="Twitter"/></a></div><?php } ?>
                                <?php if ($row_icons['RS_S_Link'] != "") { ?><div class="tt-icons"><a target="_blank" href="http://<?php echo $row_icons['RS_S_Link'] ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_S_Icon'] != "") ? 'DB/' . $row_icons['RS_S_Icon'] : 'snapchat.png' ?>" alt="Snapchat"/></a></div><?php } ?>
                                <?php if ($row_icons['RS_Info_Link'] != "") { ?><div class="tt-icons"><a target="_blank" href="http://<?php echo $row_icons['RS_Info_Link'] ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_Info_Icon'] != "") ? 'DB/' . $row_icons['RS_Info_Icon'] : 'info.png' ?>" alt="Info"/></a></div><?php } ?>
                                <?php if ($row_icons['RS_C_Link'] != "") { ?><div class="tt-icons"><a target="_blank" href="http://<?php echo $row_icons['RS_C_Link'] ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_C_Icon'] != "") ? 'DB/' . $row_icons['RS_C_Icon'] : 'contact.png' ?>" alt="Contact"/></a></div><?php } ?>
                                <?php if (isset($_SESSION['REGION_AUTH_ACT_INACT']) && $_SESSION['REGION_AUTH_ACT_INACT'] == 1) { ?><div class="tt-icons"><a href="http://<?php echo $REGION['R_Domain'] ?>/auth_logout.php">Logout</a></div><?php } ?>
                                <div class="tt-icons tt-return"><a href="/"><?php echo $REGION['R_Map_Return_Link'] ?></a></div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
            <!--Top Navigation Official Site...-->
            <?php if ($REGION['R_Map_Header_Image'] != '') { ?>
                <!--Slider Start-->
                <section class="main_slider">
                    <div class="slider_wrapper map">
                        <div class="slider_wrapper_video map">
                            <div class="sliderCycle" data-cycle-loader="wait" data-cycle-fx=fade data-cycle-timeout=0 data-cycle-prev="#prev" data-cycle-next="#next">
                                <div class="slide-images">
                                    <img class="slider_image" class="show-slider-display" src="http://<?php echo DOMAIN; ?>/images/DB/<?php echo $REGION['R_Map_Header_Image']; ?>" alt="maps">
                                </div>
                            </div>
                            <?php if ($THEME['TO_Slider_Overlay'] != "") { ?>
                                <div class="image_overlay_img">
                                    <div class="image_overlay">
                                        <img src="<?php echo 'http://' . DOMAIN . IMG_ICON_REL . $THEME['TO_Slider_Overlay']; ?>" alt="Slider Overlay">
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </section>
                <!--Slider End-->
            <?php } ?>
            <?php
            $is_map = 1;
            include_once 'map_filters.php';
            ?>
            <!--Main Navigation End...-->
        </header>
        <!--End Header-->