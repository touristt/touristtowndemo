<?php
//check to pass mobile users to mobile version
$useragent = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
    header('Location: http://' . $REGION['R_Domain'] . DOMAIN_MOBILE_REL);
    exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <meta charset="utf-8" http-equiv="Content-Type" content="text/html">
            <title><?php echo($SEOtitle) ? $SEOtitle : $REGION['R_SEO_Title'] ?></title>
            <meta name="description" content="<?php echo isset($SEOdescription) ? $SEOdescription : '' ?>" />
            <meta name="keywords" content="<?php echo isset($SEOkeywords) ? $SEOkeywords : '' ?>" />
            <!-- Open Graph Tags fo FB Share and Like-->
            <meta property="fb:app_id" content="1356712584397235"/>
            <meta property="og:url" content="<?php echo isset($OG_URL) ? $OG_URL : ''; ?>" />
            <meta property="og:type" content="<?php echo isset($OG_Type) ? $OG_Type : ''; ?>" />
            <meta property="og:title" content="<?php echo isset($OG_Title) ? $OG_Title : ''; ?>" />
            <meta property="og:description" content="<?php echo isset($OG_Description) ? $OG_Description : ''; ?>" />
            <meta property="og:image" content="<?php echo isset($OG_Image) ? $OG_Image : ''; ?>" />
            <meta property="og:image:width" content="<?php echo isset($OG_Image_Width) ? $OG_Image_Width : ''; ?>" />
            <meta property="og:image:height" content="<?php echo isset($OG_Image_Height) ? $OG_Image_Height : ''; ?>" />

            <!-- CSS FILES -->
            <link rel="stylesheet" type="text/css" href="/stylesheets/mainscreen.min.css" media="all" />

            <!-- JS FILES -->
            <script src="/include/jquery-1.12.4.js"></script>
            <script src="/include/jquery-ui.js"></script>
            <script type="text/javascript" src="/include/plugins/cycle/jquery.cycle.all.js"></script>
            <script type="text/javascript" src="/include/js/jquery.bxslider.min.js"></script>
            <script type="text/javascript" src="/include/public.js"></script>
            <script type="text/javascript" src="/include/plugins/sweetalert/sweetalert.min.js"></script>
            <?PHP
            //Getting Region Theme
            $getTheme = "SELECT * FROM  `tbl_Theme_Options` WHERE TO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
            $themeRegion = mysql_query($getTheme, $db) or die("Invalid query: $getTheme -- " . mysql_error());
            $THEME = mysql_fetch_assoc($themeRegion);

            $season_text_value = explode("-", $THEME['TO_Season_Text']);
            $main_nav_value = explode("-", $THEME['TO_Main_Navigation']);
            $slider_title_value = explode("-", $THEME['TO_Slider_Title']);
            $trip_planner_number_value = explode("-", $THEME['TO_Trip_Planner_Number']);
            $slider_desc_value = explode("-", $THEME['TO_Slider_Description']);
            $main_dd_value = explode("-", $THEME['TO_Drop_Down_Menu']);
            $cat_title_value = explode("-", $THEME['TO_Thumbnail_Category_Tite']);
            $view_all_value = explode("-", $THEME['TO_Thumbnail_View_All']);
            $sub_cat_value = explode("-", $THEME['TO_Thumbnail_Sub_Category_Title']);
            $listing_title_value = explode("-", $THEME['TO_Thumbnail_Listing_Title']);
            $coupon_title_value = explode("-", $THEME['TO_Thumbnail_Coupon_Title']);
            $coupon_title_background_color = $THEME['TO_Thumbnail_Coupon_Title_Background_Color'];
            $map_title_value = explode("-", $THEME['TO_Map_Filter_Font']);
            $footer_text_value = explode("-", $THEME['TO_Footer_Text']);
            $footer_links_value = explode("-", $THEME['TO_Footer_Links']);
            $footer_disc_value = explode("-", $THEME['TO_Footer_Disclaimer']);
            $listing_sub_nav_value = explode("-", $THEME['TO_Listing_Sub_Nav_Text']);
            $listing_day_text_value = explode("-", $THEME['TO_Listing_Day_Text']);
            $listing_hours_text_value = explode("-", $THEME['TO_Listing_Hours_Text']);
            $amenities_text_value = explode("-", $THEME['TO_Amenities_Text']);
            $download_text_value = explode("-", $THEME['TO_Downloads_Text']);
            $listing_main_title_value = explode("-", $THEME['TO_Listing_Title_Text']);
            $listing_address_value = explode("-", $THEME['TO_Listing_Location_Text']);
            $listing_nav_value = explode("-", $THEME['TO_Listing_Navigation_Text']);
            $general_paragraph_value = explode("-", $THEME['TO_General_Body_Copy']);
            $get_this_coupon_title_value = explode("-", $THEME['TO_Get_This_Coupon_Title_Text']);
            $get_this_coupon_desc_value = explode("-", $THEME['TO_Get_This_Coupon_Description_Text']);
            $text_box_text_inside_search_box_value = explode("-", $THEME['TO_Text_Box_Text_Inside_Box']);

            //CSS With Image variables
            $home_event_des_bg_color = explode(">", $THEME['TO_Homepage_Events_Description_Background']);
            $listing_whats_nearby_bg_color = explode(">", $THEME['TO_Listing_Whats_Nearby_Background']);

            //Getting Region Theme Font, Due to our DB structure we are applying new query rather than JOIN
            $getFont = "SELECT * FROM `tbl_Theme_Options_Fonts`";
            $themeFontRegion = mysql_query($getFont, $db) or die("Invalid query: $getFont -- " . mysql_error());
            $THEME_ARRAY = array();
            while ($THEME_FONT = mysql_fetch_assoc($themeFontRegion)) {
                $THEME_ARRAY[$THEME_FONT['TOF_ID']] = $THEME_FONT['TOF_Name'];
            }
            ?>
            <style>
                a {
                    color: <?php echo (isset($THEME['TO_Text_Link'])) ? $THEME['TO_Text_Link'] : '' ?>;
                }
                .search_header {
                    background-color: <?php echo (isset($THEME['TO_Search_Box_Background_Color'])) ? $THEME['TO_Search_Box_Background_Color'] : ''; ?>;
                }
                input#txtSearch {
                    border-color: <?php echo (isset($THEME['TO_Text_Box_Border_Inside_Search_Box'])) ? $THEME['TO_Text_Box_Border_Inside_Search_Box'] : ''; ?>;
                    font-family: <?php echo (isset($THEME_ARRAY[$text_box_text_inside_search_box_value[0]]) === true && empty($THEME_ARRAY[$text_box_text_inside_search_box_value[0]]) === false) ? $THEME_ARRAY[$text_box_text_inside_search_box_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($text_box_text_inside_search_box_value[1])) ? $text_box_text_inside_search_box_value[1] : '' ?>px;
                    color: <?php echo (isset($text_box_text_inside_search_box_value[2])) ? $text_box_text_inside_search_box_value[2] : '' ?>;
                }
                .header-top-nav{
                    background-color: <?php echo (isset($THEME['TO_Header_Background_Color'])) ? $THEME['TO_Header_Background_Color'] : ''; ?>;
                }
                .logo-nav{
                    background-color: <?php echo (isset($THEME['TO_Logo_Background_Color'])) ? $THEME['TO_Logo_Background_Color'] : ''; ?>;
                }
                .logo-nav-container {
                    margin-top: <?php echo (isset($THEME['TO_Logo_Space_Above'])) ? $THEME['TO_Logo_Space_Above'] : ''; ?>px;
                    margin-bottom: <?php echo (isset($THEME['TO_Logo_Space_Below'])) ? $THEME['TO_Logo_Space_Below'] : ''; ?>px;
                }
                .logo-top-nav ul li a {
                    font-family: <?php echo (isset($THEME_ARRAY[$main_nav_value[0]]) === true && empty($THEME_ARRAY[$main_nav_value[0]]) === false) ? $THEME_ARRAY[$main_nav_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($main_nav_value[1])) ? $main_nav_value[1] : ''; ?>px;
                    color: <?php echo (isset($main_nav_value[2])) ? $main_nav_value[2] : ''; ?>;
                }
                .logo-top-nav ul li ul.submenu .dd-inner .column .maps-text {
                    color: <?php echo (isset($main_nav_value[2])) ? $main_nav_value[2] : ''; ?>;
                }
                .logo-top-nav ul li ul.submenu {
                    border-color: <?php echo (isset($main_nav_value[2])) ? $main_nav_value[2] : ''; ?>;
                    background-color: <?php echo (isset($THEME['TO_Drop_Down_Menu_BG'])) ? $THEME['TO_Drop_Down_Menu_BG'] : ''; ?>;
                }
                .logo-top-nav ul li ul.submenu .dd-inner .column {
                    background-color: <?php echo (isset($THEME['TO_Drop_Down_Menu_BG'])) ? $THEME['TO_Drop_Down_Menu_BG'] : ''; ?>;
                }
                .logo-top-nav ul li ul.submenu .dd-inner .column a {
                    font-family: <?php echo (isset($THEME_ARRAY[$main_dd_value[0]]) === true && empty($THEME_ARRAY[$main_dd_value[0]]) === false) ? $THEME_ARRAY[$main_dd_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($main_dd_value[1])) ? $main_dd_value[1] : ''; ?>px;
                    color: <?php echo (isset($main_dd_value[2])) ? $main_dd_value[2] : ''; ?>;
                }
                /* Cycle Slider Start
                -----------------------------------------------------------------------------------------------------------*/
                .main_slider .slider_wrapper .sliderCycle .slide-images, .main_slider .slider_wrapper .homepage-painting .slide-images {
                    font-family: <?php echo (isset($THEME_ARRAY[$slider_title_value[0]]) === true && empty($THEME_ARRAY[$slider_title_value[0]]) === false) ? $THEME_ARRAY[$slider_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($slider_title_value[1])) ? $slider_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($slider_title_value[2])) ? $slider_title_value[2] : ''; ?>;
                }
                .main_slider .slider_wrapper .sliderCycle .slide-images .slider-text h3, .main_slider .slider_wrapper .homepage-painting .slide-images .slider-text h3{
                    font-family: <?php echo (isset($THEME_ARRAY[$slider_desc_value[0]]) === true && empty($THEME_ARRAY[$slider_desc_value[0]]) === false) ? $THEME_ARRAY[$slider_desc_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($slider_desc_value[1])) ? $slider_desc_value[1] : ''; ?>px;
                    color: <?php echo (isset($slider_desc_value[2])) ? $slider_desc_value[2] : ''; ?>;
                }
                /* Cycle Slider End
                -----------------------------------------------------------------------------------------------------------*/
                /* Thumbnail Grid
                -----------------------------------------------------------------------------------------------------------*/
                .grid-left-title.heading-text {
                    font-family: <?php echo (isset($THEME_ARRAY[$cat_title_value[0]]) === true && empty($THEME_ARRAY[$cat_title_value[0]]) === false) ? $THEME_ARRAY[$cat_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($cat_title_value[1])) ? $cat_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($cat_title_value[2])) ? $cat_title_value[2] : ''; ?>;
                }
                .grid-left-view h3 a {
                    font-family: <?php echo (isset($THEME_ARRAY[$view_all_value[0]]) === true && empty($THEME_ARRAY[$view_all_value[0]]) === false) ? $THEME_ARRAY[$view_all_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($view_all_value[1])) ? $view_all_value[1] : ''; ?>px;
                    color: <?php echo (isset($view_all_value[2])) ? $view_all_value[2] : ''; ?>;
                }
                ul.bxslider li a h3.thumbnail-heading, .thumbnails.static-thumbs .thumb-item a h3.thumbnail-heading {
                    font-family: <?php echo (isset($THEME_ARRAY[$sub_cat_value[0]]) === true && empty($THEME_ARRAY[$sub_cat_value[0]]) === false) ? $THEME_ARRAY[$sub_cat_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($sub_cat_value[1])) ? $sub_cat_value[1] : ''; ?>px;
                    color: <?php echo (isset($sub_cat_value[2])) ? $sub_cat_value[2] : ''; ?>;
                }
                ul.bxslider li a h3.thumbnail-desc, .thumbnails.static-thumbs .thumb-item a h3.thumbnail-desc {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_title_value[0]]) === true && empty($THEME_ARRAY[$listing_title_value[0]]) === false) ? $THEME_ARRAY[$listing_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_title_value[1])) ? $listing_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($listing_title_value[2])) ? $listing_title_value[2] : ''; ?>;
                }
                ul.bxslider li a h3.thumbnail-coupon, .thumbnails.static-thumbs .thumb-item a h3.thumbnail-coupon, .get-coupon-btn a {
                    font-family: <?php echo (isset($THEME_ARRAY[$coupon_title_value[0]]) === true && empty($THEME_ARRAY[$coupon_title_value[0]]) === false) ? $THEME_ARRAY[$coupon_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($coupon_title_value[1])) ? $coupon_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($coupon_title_value[2])) ? $coupon_title_value[2] : ''; ?>;
                }
                .thumbnail-coupon{
                    background-color: <?php echo (isset($coupon_title_background_color)) ? $coupon_title_background_color : ''; ?>;
                }
                /* Thumbnail Grid End
                -----------------------------------------------------------------------------------------------------------*/
                /* Footer
                -----------------------------------------------------------------------------------------------------------*/
                .footer-item li a {
                    font-family: <?php echo (isset($THEME_ARRAY[$footer_text_value[0]]) === true && empty($THEME_ARRAY[$footer_text_value[0]]) === false) ? $THEME_ARRAY[$footer_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($footer_text_value[1])) ? $footer_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($footer_text_value[2])) ? $footer_text_value[2] : ''; ?>;
                }
                .footer-urls ul li {
                    border-color: <?php echo (isset($footer_links_value[2])) ? $footer_links_value[2] : ''; ?> 
                }
                .footer-urls ul li a {
                    font-family: <?php echo (isset($THEME_ARRAY[$footer_links_value[0]]) === true && empty($THEME_ARRAY[$footer_links_value[0]]) === false) ? $THEME_ARRAY[$footer_links_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($footer_links_value[1])) ? $footer_links_value[1] : ''; ?>px;
                    color: <?php echo (isset($footer_links_value[2])) ? $footer_links_value[2] : ''; ?>;
                }
                .footer-copyrights {
                    font-family: <?php echo (isset($THEME_ARRAY[$footer_disc_value[0]]) === true && empty($THEME_ARRAY[$footer_disc_value[0]]) === false) ? $THEME_ARRAY[$footer_disc_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($footer_disc_value[1])) ? $footer_disc_value[1] : ''; ?>px;
                    color: <?php echo (isset($footer_disc_value[2])) ? $footer_disc_value[2] : ''; ?>;
                }
                .footer {
                    background-color: <?php echo (isset($THEME['TO_Footer_Background_Color'])) ? $THEME['TO_Footer_Background_Color'] : ''; ?>;
                }
                .footer .footer-outer .footer-images, .footer .footer-outer .footer-urls {
                    border-color: <?php echo (isset($THEME['TO_Footer_Lines_Color'])) ? $THEME['TO_Footer_Lines_Color'] : ''; ?>;
                }
                /* Footer End
                -----------------------------------------------------------------------------------------------------------*/
                /* Thumbnail slider start
                -----------------------------------------------------------------------------------------------------------*/
                .nbs-flexisel-nav-left, .nbs-flexisel-nav-left.disabled, .bx-wrapper .bx-prev {
                    <?php if (isset($THEME['TO_Thumbnail_Arrow_Left']) && $THEME['TO_Thumbnail_Arrow_Left'] != "") { ?>
                        background-image: url("http://<?php echo DOMAIN . IMG_ICON_REL . $THEME['TO_Thumbnail_Arrow_Left'] ?>");
                    <?php } ?>
                }
                .nbs-flexisel-nav-right, .nbs-flexisel-nav-right.disabled, .bx-wrapper .bx-next {
                    <?php if (isset($THEME['TO_Thumbnail_Arrow_Right']) && $THEME['TO_Thumbnail_Arrow_Right'] != "") { ?>
                        background-image: url("http://<?php echo DOMAIN . IMG_ICON_REL . $THEME['TO_Thumbnail_Arrow_Right'] ?>");
                    <?php } ?>
                }
                /* Thumbnail slider end
                -----------------------------------------------------------------------------------------------------------*/
                /* Listing Page Start
                -----------------------------------------------------------------------------------------------------------*/
                .listing-title-detail .listing-title, .event-wrapper-inner .details .section-header h2 {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_main_title_value[0]]) === true && empty($THEME_ARRAY[$listing_main_title_value[0]]) === false) ? $THEME_ARRAY[$listing_main_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_main_title_value[1])) ? $listing_main_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($listing_main_title_value[2])) ? $listing_main_title_value[2] : ''; ?>;
                }
                .listing-address, .get-coupon-listing, .get-coupon-town {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_address_value[0]]) === true && empty($THEME_ARRAY[$listing_address_value[0]]) === false) ? $THEME_ARRAY[$listing_address_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_address_value[1])) ? $listing_address_value[1] : ''; ?>px;
                    color: <?php echo (isset($listing_address_value[2])) ? $listing_address_value[2] : ''; ?>;
                }
                .menu-description-term{
                    font-family: <?php echo (isset($THEME_ARRAY[$general_paragraph_value[0]]) === true && empty($THEME_ARRAY[$general_paragraph_value[0]]) === false) ? $THEME_ARRAY[$general_paragraph_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($general_paragraph_value[1])) ? $general_paragraph_value[1] : ''; ?>px !important;
                    color: <?php echo (isset($general_paragraph_value[2])) ? $general_paragraph_value[2] : ''; ?> !important;
                    line-height: <?php echo (isset($THEME['TO_General_Body_Copy_Line_Spacing'])) ? $THEME['TO_General_Body_Copy_Line_Spacing'] : ''; ?>em;
                }
                /*                .menu-desc p{
                                    font-family: <?php echo (isset($THEME_ARRAY[$general_paragraph_value[0]]) === true && empty($THEME_ARRAY[$general_paragraph_value[0]]) === false) ? $THEME_ARRAY[$general_paragraph_value[0]] : ""; ?>;
                                    font-size: <?php echo (isset($general_paragraph_value[1])) ? $general_paragraph_value[1] : ''; ?>px;
                                    color: <?php echo (isset($general_paragraph_value[2])) ? $general_paragraph_value[2] : ''; ?>;
                                    line-height: <?php echo (isset($THEME['TO_General_Body_Copy_Line_Spacing'])) ? $THEME['TO_General_Body_Copy_Line_Spacing'] : ''; ?>em;
                                }*/
                .listing-detail-add-detail .ui-accordion .ui-accordion-header, .listing-detail-address.border-top-none .listing-address-container .listing-detail-add-detail a,
                .listing-detail-address .listing-address-container .listing-detail-add-detail .address-heading, .listing-detail-tripplanner .address-heading a, .listing-detail-address .listing-detail-add-detail .address-heading
                ,.listing-detail-address .listing-detail-add-detail .address-heading a
                {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_sub_nav_value[0]]) === true && empty($THEME_ARRAY[$listing_sub_nav_value[0]]) === false) ? $THEME_ARRAY[$listing_sub_nav_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_sub_nav_value[1])) ? $listing_sub_nav_value[1] : ''; ?>px;
                    color: <?php echo (isset($listing_sub_nav_value[2])) ? $listing_sub_nav_value[2] : ''; ?>;
                }
                .day-heading {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_day_text_value[0]]) === true && empty($THEME_ARRAY[$listing_day_text_value[0]]) === false) ? $THEME_ARRAY[$listing_day_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_day_text_value[1])) ? $listing_day_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($listing_day_text_value[2])) ? $listing_day_text_value[2] : ''; ?>;
                }
                .day-timing {
                    <?php $listing_hours_text_index = array_search($listing_hours_text_value[0], $THEME_ARRAY); ?>
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_hours_text_value[0]]) === true && empty($THEME_ARRAY[$listing_hours_text_value[0]]) === false) ? $THEME_ARRAY[$listing_hours_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_hours_text_value[1])) ? $listing_hours_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($listing_hours_text_value[2])) ? $listing_hours_text_value[2] : ''; ?>;
                }
                .businessListing-timing .acc-body {
                    font-family: <?php echo (isset($THEME_ARRAY[$amenities_text_value[0]]) === true && empty($THEME_ARRAY[$amenities_text_value[0]]) === false) ? $THEME_ARRAY[$amenities_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($amenities_text_value[1])) ? $amenities_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($amenities_text_value[2])) ? $amenities_text_value[2] : ''; ?>;
                }
                .listing-detail-address .listing-address-container .listing-detail-add-detail .businessListing-timing .acc-body.theme-downloads a {
                    font-family: <?php echo (isset($THEME_ARRAY[$download_text_value[0]]) === true && empty($THEME_ARRAY[$download_text_value[0]]) === false) ? $THEME_ARRAY[$download_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($download_text_value[1])) ? $download_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($download_text_value[2])) ? $download_text_value[2] : ''; ?>;
                }
                .coupon-form-title{
                    font-family: <?php echo (isset($THEME_ARRAY[$get_this_coupon_title_value[0]]) === true && empty($THEME_ARRAY[$get_this_coupon_title_value[0]]) === false) ? $THEME_ARRAY[$get_this_coupon_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($get_this_coupon_title_value[1])) ? $get_this_coupon_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($get_this_coupon_title_value[2])) ? $get_this_coupon_title_value[2] : ''; ?>;
                }
                .coupon-form-text{
                    font-family: <?php echo (isset($THEME_ARRAY[$get_this_coupon_desc_value[0]]) === true && empty($THEME_ARRAY[$get_this_coupon_desc_value[0]]) === false) ? $THEME_ARRAY[$get_this_coupon_desc_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($get_this_coupon_desc_value[1])) ? $get_this_coupon_desc_value[1] : ''; ?>px;
                    color: <?php echo (isset($get_this_coupon_desc_value[2])) ? $get_this_coupon_desc_value[2] : ''; ?>;
                }
                /* Listing Page End
                -----------------------------------------------------------------------------------------------------------*/
            </style>

            <script type="text/javascript">

                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', '<?= $REGION['R_Tracking_ID'] ?>']);
                _gaq.push(['_trackPageview']);

                (function () {
                    var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(ga, s);
                })();

            </script>
    </head>

    <body>
        <?php
        //Defualt Header Images
        if ($THEME['TO_Default_Header_Desktop'] != '') {
            $default_header_image = $THEME['TO_Default_Header_Desktop'];
        } else {
            $default_header_image = 'Default-Main-Desktop.jpg';
        }

        //Defualt Thumbnail Images
        if ($THEME['TO_Default_Thumbnail_Desktop'] != '') {
            $default_thumbnail_image = $THEME['TO_Default_Thumbnail_Desktop'];
        } else {
            $default_thumbnail_image = 'Default-Thumbnail-Desktop.jpg';
        }
        //Defualt Thumbnail Image for Profile only
        if ($THEME['TO_Default_Header_Mobile'] != '') {
            $default_profile_image = $THEME['TO_Default_Header_Mobile'];
        } else {
            $default_profile_image = 'Default-Main-Mobile.jpg';
        }
        //Getting Regions if it has child regions
        /* Becareful...
          ----------------------------------------------------------------------------------------------------------- */
        /* $regionList is used in multiple querries.
          ----------------------------------------------------------------------------------------------------------- */
        $regionList = '';
        if ($REGION['R_Parent'] == 0) {
            $sql = "SELECT RM_Child FROM tbl_Region_Multiple LEFT JOIN tbl_Region ON RM_Child = R_ID WHERE RM_Parent = '" . encode_strings($REGION['R_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $first = true;
            $regionList .= "(";
            while ($row = mysql_fetch_assoc($result)) {
                if ($first) {
                    $first = false;
                } else {
                    $regionList .= ",";
                }
                $regionList .= $row['RM_Child'];
            }
            $regionList .= ")";
        }
        if ($browserERROR) {
            ?>
            <div class="browserOuter"><div class="browser">Some important features may not work in this version of your browser. Please upgrade to a current browser 
                    <a href="http://windows.microsoft.com/en-US/internet-explorer/downloads/ie" target="_blank">Internet Explorer</a>,
                    <a href="http://www.getfirefox.com" target="_blank">FireFox</a>,
                    <a href="http://www.google.com/chrome" target="_blank">Google Chrome</a></div></div>
            <?PHP
        }
        ?>
        <!--Start Header-->
        <header class="main-header">
            <!--Top Navigation Official Site...-->
            <div class="header-top-nav">
                <div class="header-top-nav-inside">
                    <div class="inside-wrapper">
                        <div class="site-name"></div>
                        <div class="season-dropdown">
                            <?php
                            $sql_icons = "SELECT * FROM tbl_Region_Social 
                                      WHERE RS_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
                            $result_icons = mysql_query($sql_icons, $db) or die(mysql_error());
                            $row_icons = mysql_fetch_assoc($result_icons); //echo "<pre>"; print_r(DOMAIN); exit();
                            ?>
                        </div>
                        <div class="social-icons">

                            <div class="icons-wrapper">
                                <div class="social-icons-right">
                                    <?php
                                    $facebook_link = preg_replace('/^www\./', '', $row_icons['RS_FB_Link']);
                                    if ($row_icons['RS_FB_Link'] != "") {
                                        ?><div class="tt-icons"><a target="_blank" href="http://<?php echo str_replace(array('http://', 'https://'), '', $facebook_link) ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_FB_Icon'] != "") ? 'DB/' . $row_icons['RS_FB_Icon'] : 'facebook.png' ?>" alt="Facebook"/></a></div><?php } ?>
                                    <?php
                                    $rs_i_link = preg_replace('/^www\./', '', $row_icons['RS_I_Link']);
                                    if ($row_icons['RS_I_Link'] != "") {
                                        ?><div class="tt-icons"><a target="_blank" href="http://<?php echo str_replace(array('http://', 'https://'), '', $rs_i_link) ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_I_Icon'] != "") ? 'DB/' . $row_icons['RS_I_Icon'] : 'instagram.png' ?>" alt="Instagram"/></a></div><?php } ?>
                                        <?php
                                        $rs_y_link = preg_replace('/^www\./', '', $row_icons['RS_Y_Link']);
                                        if ($row_icons['RS_Y_Link'] != "") {
                                            ?><div class="tt-icons"><a target="_blank" href="http://<?php echo str_replace(array('http://', 'https://'), '', $rs_y_link) ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_Y_Icon'] != "") ? 'DB/' . $row_icons['RS_Y_Icon'] : 'youtube-icon.png' ?>" alt="Youtube"/></a></div><?php } ?>
                                        <?php
                                        $rs_t_link = preg_replace('/^www\./', '', $row_icons['RS_T_Link']);
                                        if ($row_icons['RS_T_Link'] != "") {
                                            ?><div class="tt-icons"><a target="_blank" href="http://<?php echo str_replace(array('http://', 'https://'), '', $rs_t_link) ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_T_Icon'] != "") ? 'DB/' . $row_icons['RS_T_Icon'] : 'twitter.png' ?>" alt="Twitter"/></a></div><?php } ?>
                                        <?php
                                        $rs_s_link = preg_replace('/^www\./', '', $row_icons['RS_S_Link']);
                                        if ($row_icons['RS_S_Link'] != "") {
                                            ?><div class="tt-icons"><a target="_blank" href="http://<?php echo str_replace(array('http://', 'https://'), '', $rs_s_link) ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_S_Icon'] != "") ? 'DB/' . $row_icons['RS_S_Icon'] : 'snapchat.png' ?>" alt="Snapchat"/></a></div><?php } ?>
                                        <?php
                                        $rs_Info_link = preg_replace('/^www\./', '', $row_icons['RS_Info_Link']);
                                        if ($row_icons['RS_Info_Link'] != "") {
                                            ?><div class="tt-icons"><a target="_blank" href="http://<?php echo str_replace(array('http://', 'https://'), '', $rs_Info_link) ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_Info_Icon'] != "") ? 'DB/' . $row_icons['RS_Info_Icon'] : 'info.png' ?>" alt="Info"/></a></div><?php } ?>
                                        <?php
                                        $rs_c_link = preg_replace('/^www\./', '', $row_icons['RS_C_Link']);
                                        if ($row_icons['RS_C_Link'] != "") {
                                            ?><div class="tt-icons"><a target="_blank" href="http://<?php echo str_replace(array('http://', 'https://'), '', $rs_c_link) ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_C_Icon'] != "") ? 'DB/' . $row_icons['RS_C_Icon'] : 'contact.png' ?>" alt="Contact"/></a></div><?php } ?>
                                    <?php if (isset($_SESSION['REGION_AUTH_ACT_INACT']) && $_SESSION['REGION_AUTH_ACT_INACT'] == 1) { ?><div class="tt-icons"><a href="https://<?php echo $REGION['R_Domain'] ?>/auth_logout.php">Logout</a></div><?php } ?>
                                </div>
                                <?php if ($_SESSION['REGION_AUTH_ACT_INACT'] == 1) { ?><div class="tt-icons"><a href="http://<?php echo $REGION['R_Domain'] ?>/auth_logout.php">Logout</a></div><?php } ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!--Top Navigation Official Site...-->
            <!--Logo Navigation...-->
            <div class="logo-nav">
                <div class="logo-nav-wrapper">
                    <div class="logo-nav-container">
                        <div class="site-logo">
                            <a href="/">
                                <img src="http://<?php echo DOMAIN ?>/images/DB/<?php echo $THEME['TO_Desktop_Logo']; ?>"  alt="<?php echo isset($THEME['TO_Desktop_Logo_Alt']) ? $THEME['TO_Desktop_Logo_Alt'] : 'Coupon Country'; ?>">
                            </a>
                        </div>
                        <div class="logo-top-nav">
                            <ul>
                                <li>
                                    <form method="GET" class="navbar-search" id="search_form" action="/search.php">
                                        <div class="search_header">
                                            <div class="search_field"><input type="text" id="txtSearch" name="txtSearch" placeholder="SEARCH OFFERS"/></div>
                                        </div>
                                    </form>
                                </li>
                                <li><a href="/maps.php"><?php echo ($REGION['R_Map_Nav_Title'] != "") ? $REGION['R_Map_Nav_Title'] : 'Search by map' ?></a></li>
                                <?php
                                $sqlBlog = "SELECT C_ID, RC_Name, RC_Link, C_Name_SEO, C_Name FROM tbl_Category
                                            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID
                                            WHERE C_Is_Product_Web = 1 AND RC_Status = 0  AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
                                $resBlog = mysql_query($sqlBlog);
                                $blog = mysql_fetch_array($resBlog);
                                $c_parent = $blog['C_ID'];
                                if ($blog['RC_Link'] != '') {
                                    $blink = preg_replace('/^www\./', '', $blog['RC_Link']);
                                    $blink = 'http://' . str_replace(array('http://', 'https://'), '', $blink);
                                } else {
                                    $blink = '#';
                                }
                                ?>
                                <li>
                                    <a href="<?php echo $blink; ?>"><?php echo ($blog['RC_Name'] != "") ? $blog['RC_Name'] : $blog['C_Name'] ?></a>
                                    <?php
                                    $sql_nav_sub = "SELECT C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category 
                                                    LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                                                    WHERE C_Parent = '$c_parent' and RC_Status=0 AND RC_R_ID > 0
                                                    ORDER BY RC_Order";
                                    $result_sub = mysql_query($sql_nav_sub);
                                    echo '<ul class="submenu"><li><div class="dd-inner">';
                                    $counter = 0;
                                    $i = 0;
                                    $totalRows = mysql_num_rows($result_sub);
                                    while ($rowSub = mysql_fetch_array($result_sub)) {
                                        $sqlGetCoupons = "SELECT BFC_ID, BFC_Thumbnail, BFC_Title, RC_Name, C_Name, BL_Listing_Title, BL_Name_SEO, BL_ID FROM tbl_Business_Feature_Coupon
                                                            LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
                                                            LEFT JOIN tbl_Category ON C_ID = BFCCM_C_ID                                    
                                                            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                                                            LEFT JOIN tbl_Business_Listing ON BL_ID = BFC_BL_ID 
                                                            WHERE BFCCM_C_ID = '" . encode_strings($rowSub['C_ID'], $db) . "' AND BFC_Status = 1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date =0000-00-00) AND hide_show_listing='1' "; //  
                                        $resCoupons = mysql_query($sqlGetCoupons);
                                        $totalCoupon = mysql_num_rows($resCoupons);
                                        if ($totalCoupon > 0) {
                                            $counter++;
                                            if ($counter == 1) {
                                                echo '<div class="fulsize">';
                                            }

                                            echo '<div class="column">';
                                            echo '<a  href="/' . $blog['C_Name_SEO'] . '/' . $rowSub['C_Name_SEO'] . '/' . $rowSub['C_ID'] . '">' . (($rowSub['RC_Name'] != "") ? $rowSub['RC_Name'] : $rowSub['C_Name']) . ' (' . $totalCoupon . ')</a>';
                                            echo '</div>';
                                            if ($counter == 3) {
                                                echo '</div>';
                                                $counter = 0;
                                            }
                                        }
                                    }
                                    echo "</li></ul>";
                                    ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--Logo Navigation End...-->
            <?php if ($THEME['TO_Scroll_Down_Arrow'] != "") { ?>
                <div class="arrow bounce">
                    <img src="<?php echo ($THEME['TO_Scroll_Down_Arrow'] != "") ? "https://" . DOMAIN . IMG_ICON_REL . $THEME['TO_Scroll_Down_Arrow'] : "" ?>">
                </div>
            <?php } ?>
        </header>
        <script type="text/javascript">
            $(window).scroll(function () {
                if ($(this).scrollTop() > 10)
                {
                    $('.arrow').hide();
                } else
                {
                    $('.arrow').show();
                }
            });
        </script>

        <!--End Header-->