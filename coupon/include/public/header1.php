
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <meta charset="utf-8" http-equiv="Content-Type" content="text/html">
            <title><?php echo($SEOtitle) ? $SEOtitle : $REGION['R_SEO_Title'] ?></title>
            <meta name="description" content="<?php echo isset($SEOdescription) ? $SEOdescription : '' ?>" />
            <meta name="keywords" content="<?php echo isset($SEOkeywords) ? $SEOkeywords : '' ?>" />
            <!-- Open Graph Tags fo FB Share and Like-->
            <meta property="fb:app_id" content="1356712584397235"/>
            <meta property="og:url" content="<?php echo isset($OG_URL) ? $OG_URL : ''; ?>" />
            <meta property="og:type" content="<?php echo isset($OG_Type) ? $OG_Type : ''; ?>" />
            <meta property="og:title" content="<?php echo isset($OG_Title) ? $OG_Title : ''; ?>" />
            <meta property="og:description" content="<?php echo isset($OG_Description) ? $OG_Description : ''; ?>" />
            <meta property="og:image" content="<?php echo isset($OG_Image) ? $OG_Image : ''; ?>" />
            <meta property="og:image:width" content="<?php echo isset($OG_Image_Width) ? $OG_Image_Width : ''; ?>" />
            <meta property="og:image:height" content="<?php echo isset($OG_Image_Height) ? $OG_Image_Height : ''; ?>" />

            <!-- CSS FILES -->
            <link rel="stylesheet" type="text/css" href="/stylesheets/listing.css" media="all" />
            <link rel="stylesheet" type="text/css" href="/stylesheets/mainscreen.css" media="all" />
            <link rel="stylesheet" type="text/css" href="/stylesheets/jquery.bxslider.css" media="all" />
            <link href="/include/plugins/sweetalert/sweetalert.css"  rel="stylesheet" type="text/css"/> 

            <!-- JS FILES -->
            <script src="/include/jquery-1.12.4.js"></script>
            <script src="/include/jquery-ui.js"></script>
            <script type="text/javascript" src="/include/plugins/cycle/jquery.cycle.all.js"></script>
            <script type="text/javascript" src="/include/js/jquery.bxslider.min.js"></script>
            <script type="text/javascript" src="/include/public.js"></script>
            <script type="text/javascript" src="/include/plugins/sweetalert/sweetalert.min.js"></script>
            <?PHP
            //Getting Region Theme
            echo $getTheme = "SELECT * FROM  `tbl_Theme_Options` WHERE TO_R_ID = 27";
            $themeRegion = mysql_query($getTheme, $db) or die("Invalid query: $getTheme -- " . mysql_error());
            $THEME = mysql_fetch_assoc($themeRegion);
            
            $season_text_value = explode("-", $THEME['TO_Season_Text']);
            $main_nav_value = explode("-", $THEME['TO_Main_Navigation']);
            $slider_title_value = explode("-", $THEME['TO_Slider_Title']);
            $trip_planner_number_value = explode("-", $THEME['TO_Trip_Planner_Number']);
            $slider_desc_value = explode("-", $THEME['TO_Slider_Description']);
            $main_dd_value = explode("-", $THEME['TO_Drop_Down_Menu']);
            $cat_title_value = explode("-", $THEME['TO_Thumbnail_Category_Tite']);
            $view_all_value = explode("-", $THEME['TO_Thumbnail_View_All']);
            $sub_cat_value = explode("-", $THEME['TO_Thumbnail_Sub_Category_Title']);
            $listing_title_value = explode("-", $THEME['TO_Thumbnail_Listing_Title']);
            $coupon_title_value = explode("-", $THEME['TO_Thumbnail_Coupon_Title']);
            $coupon_title_background_color = $THEME['TO_Thumbnail_Coupon_Title_Background_Color'];
            $map_title_value = explode("-", $THEME['TO_Map_Filter_Font']);
            $footer_text_value = explode("-", $THEME['TO_Footer_Text']);
            $footer_links_value = explode("-", $THEME['TO_Footer_Links']);
            $footer_disc_value = explode("-", $THEME['TO_Footer_Disclaimer']);
            $listing_sub_nav_value = explode("-", $THEME['TO_Listing_Sub_Nav_Text']);
            $listing_day_text_value = explode("-", $THEME['TO_Listing_Day_Text']);
            $listing_hours_text_value = explode("-", $THEME['TO_Listing_Hours_Text']);
            $amenities_text_value = explode("-", $THEME['TO_Amenities_Text']);
            $download_text_value = explode("-", $THEME['TO_Downloads_Text']);
            $listing_main_title_value = explode("-", $THEME['TO_Listing_Title_Text']);
            $listing_address_value = explode("-", $THEME['TO_Listing_Location_Text']);
            $listing_nav_value = explode("-", $THEME['TO_Listing_Navigation_Text']);
            $general_paragraph_value = explode("-", $THEME['TO_General_Body_Copy']);
            $get_this_coupon_title_value = explode("-", $THEME['TO_Get_This_Coupon_Title_Text']);
            $get_this_coupon_desc_value = explode("-", $THEME['TO_Get_This_Coupon_Description_Text']);

            //CSS With Image variables
            $home_event_des_bg_color = explode(">", $THEME['TO_Homepage_Events_Description_Background']);
            $listing_whats_nearby_bg_color = explode(">", $THEME['TO_Listing_Whats_Nearby_Background']);

            //Getting Region Theme Font, Due to our DB structure we are applying new query rather than JOIN
            $getFont = "SELECT * FROM `tbl_Theme_Options_Fonts`";
            $themeFontRegion = mysql_query($getFont, $db) or die("Invalid query: $getFont -- " . mysql_error());
            $THEME_ARRAY = array();
            while ($THEME_FONT = mysql_fetch_assoc($themeFontRegion)) {
                $THEME_ARRAY[$THEME_FONT['TOF_ID']] = $THEME_FONT['TOF_Name'];
            }
            ?>
            <style>
                a {
                    color: <?php echo $THEME['TO_Text_Link'] ?>;
                }
                .header-top-nav{
                    background-color: <?php echo $THEME['TO_Header_Background_Color'] ?>;
                }
                .logo-nav-container {
                    margin-top: <?php echo $THEME['TO_Logo_Space_Above'] ?>px;
                    margin-bottom: <?php echo $THEME['TO_Logo_Space_Below'] ?>px;
                }
                .logo-top-nav ul li a {
                    font-family: <?php echo (isset($THEME_ARRAY[$main_nav_value[0]]) === true && empty($THEME_ARRAY[$main_nav_value[0]]) === false) ? $THEME_ARRAY[$main_nav_value[0]] : ""; ?>;
                    font-size: <?php echo $main_nav_value[1] ?>px;
                    color: <?php echo $main_nav_value[2] ?>;
                }
                .logo-top-nav ul li ul.submenu .dd-inner .column .maps-text {
                    color: <?php echo $main_nav_value[2] ?>;
                }
                .logo-top-nav ul li ul.submenu {
                    border-color: <?php echo $main_nav_value[2] ?>;
                    background-color: <?php echo $THEME['TO_Drop_Down_Menu_BG'] ?>;
                }
                .logo-top-nav ul li ul.submenu .dd-inner .column {
                    background-color: <?php echo $THEME['TO_Drop_Down_Menu_BG'] ?>;
                }
                .logo-top-nav ul li ul.submenu .dd-inner .column a {
                    font-family: <?php echo (isset($THEME_ARRAY[$main_dd_value[0]]) === true && empty($THEME_ARRAY[$main_dd_value[0]]) === false) ? $THEME_ARRAY[$main_dd_value[0]] : ""; ?>;
                    font-size: <?php echo $main_dd_value[1] ?>px;
                    color: <?php echo $main_dd_value[2] ?>;
                }
                /* Cycle Slider Start
                -----------------------------------------------------------------------------------------------------------*/
                .main_slider .slider_wrapper .sliderCycle .slide-images {
                    font-family: <?php echo (isset($THEME_ARRAY[$slider_title_value[0]]) === true && empty($THEME_ARRAY[$slider_title_value[0]]) === false) ? $THEME_ARRAY[$slider_title_value[0]] : ""; ?>;
                    font-size: <?php echo $slider_title_value[1] ?>px;
                    color: <?php echo $slider_title_value[2] ?>;
                }
                .main_slider .slider_wrapper .sliderCycle .slide-images .slider-text h3{
                    font-family: <?php echo (isset($THEME_ARRAY[$slider_desc_value[0]]) === true && empty($THEME_ARRAY[$slider_desc_value[0]]) === false) ? $THEME_ARRAY[$slider_desc_value[0]] : ""; ?>;
                    font-size: <?php echo $slider_desc_value[1] ?>px;
                    color: <?php echo $slider_desc_value[2] ?>;
                }
                /* Cycle Slider End
                -----------------------------------------------------------------------------------------------------------*/
                /* Thumbnail Grid
                -----------------------------------------------------------------------------------------------------------*/
                .grid-left-title.heading-text {
                    font-family: <?php echo (isset($THEME_ARRAY[$cat_title_value[0]]) === true && empty($THEME_ARRAY[$cat_title_value[0]]) === false) ? $THEME_ARRAY[$cat_title_value[0]] : ""; ?>;
                    font-size: <?php echo $cat_title_value[1] ?>px;
                    color: <?php echo $cat_title_value[2] ?>;
                }
                .grid-left-view h3 a {
                    font-family: <?php echo (isset($THEME_ARRAY[$view_all_value[0]]) === true && empty($THEME_ARRAY[$view_all_value[0]]) === false) ? $THEME_ARRAY[$view_all_value[0]] : ""; ?>;
                    font-size: <?php echo $view_all_value[1] ?>px;
                    color: <?php echo $view_all_value[2] ?>;
                }
                ul.bxslider li a h3.thumbnail-heading, .thumbnails.static-thumbs .thumb-item a h3.thumbnail-heading {
                    font-family: <?php echo (isset($THEME_ARRAY[$sub_cat_value[0]]) === true && empty($THEME_ARRAY[$sub_cat_value[0]]) === false) ? $THEME_ARRAY[$sub_cat_value[0]] : ""; ?>;
                    font-size: <?php echo $sub_cat_value[1] ?>px;
                    color: <?php echo $sub_cat_value[2] ?>;
                }
                ul.bxslider li a h3.thumbnail-desc, .thumbnails.static-thumbs .thumb-item a h3.thumbnail-desc {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_title_value[0]]) === true && empty($THEME_ARRAY[$listing_title_value[0]]) === false) ? $THEME_ARRAY[$listing_title_value[0]] : ""; ?>;
                    font-size: <?php echo $listing_title_value[1] ?>px;
                    color: <?php echo $listing_title_value[2] ?>;
                }
                ul.bxslider li a h3.thumbnail-coupon, .thumbnails.static-thumbs .thumb-item a h3.thumbnail-coupon, .get-coupon-btn a {
                    font-family: <?php echo (isset($THEME_ARRAY[$coupon_title_value[0]]) === true && empty($THEME_ARRAY[$coupon_title_value[0]]) === false) ? $THEME_ARRAY[$coupon_title_value[0]] : ""; ?>;
                    font-size: <?php echo $coupon_title_value[1] ?>px;
                    color: <?php echo $coupon_title_value[2] ?>;
                }
                .thumbnail-coupon{
                    background-color: <?php echo $coupon_title_background_color ?>;
                }
                /* Thumbnail Grid End
                -----------------------------------------------------------------------------------------------------------*/
                /* Footer
                -----------------------------------------------------------------------------------------------------------*/
                .footer-item li a {
                    font-family: <?php echo (isset($THEME_ARRAY[$footer_text_value[0]]) === true && empty($THEME_ARRAY[$footer_text_value[0]]) === false) ? $THEME_ARRAY[$footer_text_value[0]] : ""; ?>;
                    font-size: <?php echo $footer_text_value[1] ?>px;
                    color: <?php echo $footer_text_value[2] ?>;
                }
                .footer-urls ul li {
                    border-color: <?php echo $footer_links_value[2] ?> 
                }
                .footer-urls ul li a {
                    font-family: <?php echo (isset($THEME_ARRAY[$footer_links_value[0]]) === true && empty($THEME_ARRAY[$footer_links_value[0]]) === false) ? $THEME_ARRAY[$footer_links_value[0]] : ""; ?>;
                    font-size: <?php echo $footer_links_value[1] ?>px;
                    color: <?php echo $footer_links_value[2] ?>;
                }
                .footer-copyrights {
                    font-family: <?php echo (isset($THEME_ARRAY[$footer_disc_value[0]]) === true && empty($THEME_ARRAY[$footer_disc_value[0]]) === false) ? $THEME_ARRAY[$footer_disc_value[0]] : ""; ?>;
                    font-size: <?php echo $footer_disc_value[1] ?>px;
                    color: <?php echo $footer_disc_value[2] ?>;
                }
                .footer {
                    background-color: <?php echo $THEME['TO_Footer_Background_Color'] ?>;
                }
                .footer .footer-outer .footer-images, .footer .footer-outer .footer-urls {
                    border-color: <?php echo $THEME['TO_Footer_Lines_Color'] ?>;
                }
                /* Footer End
                -----------------------------------------------------------------------------------------------------------*/
                /* Thumbnail slider start
                -----------------------------------------------------------------------------------------------------------*/
                .nbs-flexisel-nav-left, .nbs-flexisel-nav-left.disabled, .bx-wrapper .bx-prev {
                    <?php if ($THEME['TO_Thumbnail_Arrow_Left'] != "") { ?>
                        background-image: url("http://<?php echo DOMAIN . IMG_ICON_REL . $THEME['TO_Thumbnail_Arrow_Left'] ?>");
                    <?php } ?>
                }
                .nbs-flexisel-nav-right, .nbs-flexisel-nav-right.disabled, .bx-wrapper .bx-next {
                    <?php if ($THEME['TO_Thumbnail_Arrow_Right'] != "") { ?>
                        background-image: url("http://<?php echo DOMAIN . IMG_ICON_REL . $THEME['TO_Thumbnail_Arrow_Right'] ?>");
                    <?php } ?>
                }
                /* Thumbnail slider end
                -----------------------------------------------------------------------------------------------------------*/
                /* Listing Page Start
                -----------------------------------------------------------------------------------------------------------*/
                .listing-title-detail .listing-title, .event-wrapper-inner .details .section-header h2 {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_main_title_value[0]]) === true && empty($THEME_ARRAY[$listing_main_title_value[0]]) === false) ? $THEME_ARRAY[$listing_main_title_value[0]] : ""; ?>;
                    font-size: <?php echo $listing_main_title_value[1] ?>px;
                    color: <?php echo $listing_main_title_value[2] ?>;
                }
                .listing-address, .get-coupon-listing, .get-coupon-town {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_address_value[0]]) === true && empty($THEME_ARRAY[$listing_address_value[0]]) === false) ? $THEME_ARRAY[$listing_address_value[0]] : ""; ?>;
                    font-size: <?php echo $listing_address_value[1] ?>px;
                    color: <?php echo $listing_address_value[2] ?>;
                }
                .menu-desc p{
                    font-family: <?php echo (isset($THEME_ARRAY[$general_paragraph_value[0]]) === true && empty($THEME_ARRAY[$general_paragraph_value[0]]) === false) ? $THEME_ARRAY[$general_paragraph_value[0]] : ""; ?>;
                    font-size: <?php echo $general_paragraph_value[1] ?>px;
                    color: <?php echo $general_paragraph_value[2] ?>;
                    line-height: <?php echo $THEME['TO_General_Body_Copy_Line_Spacing'] ?>em;
                }
                .listing-detail-add-detail .ui-accordion .ui-accordion-header, .listing-detail-address.border-top-none .listing-address-container .listing-detail-add-detail a,
                .listing-detail-address .listing-address-container .listing-detail-add-detail .address-heading, .listing-detail-tripplanner .address-heading a, .listing-detail-address .listing-detail-add-detail .address-heading
                ,.listing-detail-address .listing-detail-add-detail .address-heading a
                {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_sub_nav_value[0]]) === true && empty($THEME_ARRAY[$listing_sub_nav_value[0]]) === false) ? $THEME_ARRAY[$listing_sub_nav_value[0]] : ""; ?>;
                    font-size: <?php echo $listing_sub_nav_value[1] ?>px;
                    color: <?php echo $listing_sub_nav_value[2] ?>;
                }
                .day-heading {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_day_text_value[0]]) === true && empty($THEME_ARRAY[$listing_day_text_value[0]]) === false) ? $THEME_ARRAY[$listing_day_text_value[0]] : ""; ?>;
                    font-size: <?php echo $listing_day_text_value[1] ?>px;
                    color: <?php echo $listing_day_text_value[2] ?>;
                }
                .day-timing {
                    <?php $listing_hours_text_index = array_search($listing_hours_text_value[0], $THEME_ARRAY); ?>
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_hours_text_value[0]]) === true && empty($THEME_ARRAY[$listing_hours_text_value[0]]) === false) ? $THEME_ARRAY[$listing_hours_text_value[0]] : ""; ?>;
                    font-size: <?php echo $listing_hours_text_value[1] ?>px;
                    color: <?php echo $listing_hours_text_value[2] ?>;
                }
                .businessListing-timing .acc-body {
                    font-family: <?php echo (isset($THEME_ARRAY[$amenities_text_value[0]]) === true && empty($THEME_ARRAY[$amenities_text_value[0]]) === false) ? $THEME_ARRAY[$amenities_text_value[0]] : ""; ?>;
                    font-size: <?php echo $amenities_text_value[1] ?>px;
                    color: <?php echo $amenities_text_value[2] ?>;
                }
                .listing-detail-address .listing-address-container .listing-detail-add-detail .businessListing-timing .acc-body.theme-downloads a {
                    font-family: <?php echo (isset($THEME_ARRAY[$download_text_value[0]]) === true && empty($THEME_ARRAY[$download_text_value[0]]) === false) ? $THEME_ARRAY[$download_text_value[0]] : ""; ?>;
                    font-size: <?php echo $download_text_value[1] ?>px;
                    color: <?php echo $download_text_value[2] ?>;
                }
                .coupon-form-title{
                    font-family: <?php echo (isset($THEME_ARRAY[$get_this_coupon_title_value[0]]) === true && empty($THEME_ARRAY[$get_this_coupon_title_value[0]]) === false) ? $THEME_ARRAY[$get_this_coupon_title_value[0]] : ""; ?>;
                    font-size: <?php echo $get_this_coupon_title_value[1] ?>px;
                    color: <?php echo $get_this_coupon_title_value[2] ?>;
                }
                .coupon-form-text{
                    font-family: <?php echo (isset($THEME_ARRAY[$get_this_coupon_desc_value[0]]) === true && empty($THEME_ARRAY[$get_this_coupon_desc_value[0]]) === false) ? $THEME_ARRAY[$get_this_coupon_desc_value[0]] : ""; ?>;
                    font-size: <?php echo $get_this_coupon_desc_value[1] ?>px;
                    color: <?php echo $get_this_coupon_desc_value[2] ?>;
                }
                /* Listing Page End
                -----------------------------------------------------------------------------------------------------------*/
            </style>

            <script type="text/javascript">

                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', '<?= $REGION['R_Tracking_ID'] ?>']);
                _gaq.push(['_trackPageview']);

                (function () {
                    var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(ga, s);
                })();

            </script>
    </head>

    <body>
        <?php
        //Getting Regions if it has child regions
        /* Becareful...
          ----------------------------------------------------------------------------------------------------------- */
        /* $regionList is used in multiple querries.
          ----------------------------------------------------------------------------------------------------------- */
        $regionList = '';
        if ($REGION['R_Parent'] == 0) {
            $sql = "SELECT RM_Child FROM tbl_Region_Multiple LEFT JOIN tbl_Region ON RM_Child = R_ID WHERE RM_Parent = '" . encode_strings($REGION['R_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $first = true;
            $regionList .= "(";
            while ($row = mysql_fetch_assoc($result)) {
                if ($first) {
                    $first = false;
                } else {
                    $regionList .= ",";
                }
                $regionList .= $row['RM_Child'];
            }
            $regionList .= ")";
        }
        if ($browserERROR) {
            ?>
            <div class="browserOuter"><div class="browser">Some important features may not work in this version of your browser. Please upgrade to a current browser 
                    <a href="http://windows.microsoft.com/en-US/internet-explorer/downloads/ie" target="_blank">Internet Explorer</a>,
                    <a href="http://www.getfirefox.com" target="_blank">FireFox</a>,
                    <a href="http://www.google.com/chrome" target="_blank">Google Chrome</a></div></div>
            <?PHP
        }
        ?>
        <!--Start Header-->
        <header class="main-header">
            <!--Top Navigation Official Site...-->
           
            <!--Top Navigation Official Site...-->
            <!--Logo Navigation...-->
           
            <!--Logo Navigation End...-->
            <?php  if($THEME['TO_Scroll_Down_Arrow'] != ""){ ?>
            <div class="arrow bounce">
                <img src="<?php echo ($THEME['TO_Scroll_Down_Arrow'] != "") ? "http://" . DOMAIN . IMG_ICON_REL . $THEME['TO_Scroll_Down_Arrow'] : "" ?>">
            </div>
            <?php } ?>
        </header>
        <script type="text/javascript">
          $(window).scroll(function () {
             if ($(this).scrollTop() > 10)
               {
                $('.arrow').hide();
             } else
              {
              $('.arrow').show();
            }
            });
        </script>

        <!--End Header-->