<footer class="footer">
    <div class="footer-outer">
        <!--    Footer menu-->
        <div class="footer-column">
            <div class="footer-item">
                <a href="/">
                    <img src="/include/coupon-logo.png" width="150" alt=""/>
                </a>
            </div>
            <?php
            $sql_nav = "SELECT C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category
                        LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID
                        WHERE C_Is_Product_Web = 1 AND RC_Status = 0 AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
            $result = mysql_query($sql_nav);
            $row = mysql_fetch_assoc($result);
            $sql_nav_five = "SELECT C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                            WHERE C_Parent = " . $row['C_ID'] . " AND RC_R_ID > 0 AND RC_Status = 0 ORDER BY RC_Order ";
            $result_nav_five = mysql_query($sql_nav_five);
            $counter = 0;
            $i = 0;
            $totalRows = mysql_num_rows($result_nav_five);
            while ($row_sub_five = mysql_fetch_array($result_nav_five)) {
                $sqlGetCoupons = "SELECT BFC_ID, BFC_Thumbnail, BFC_Title, RC_Name, C_Name, BL_Listing_Title, BL_Name_SEO, BL_ID FROM tbl_Business_Feature_Coupon
                                    LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
                                    LEFT JOIN tbl_Category ON C_ID = BFCCM_C_ID                                    
                                    LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                                    LEFT JOIN tbl_Business_Listing ON BL_ID = BFC_BL_ID 
                                    WHERE BFCCM_C_ID = '" . encode_strings($row_sub_five['C_ID'], $db) . "' AND BFC_Status = 1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date =0000-00-00) AND hide_show_listing='1' ";
                $resCoupons = mysql_query($sqlGetCoupons);
                $totalCoupon = mysql_num_rows($resCoupons);
                $i++;
                if($totalCoupon > 0) {
                    if ($counter == 0) {
                        echo '<div class="footer-item"><div class="footer-inside"><ul class="text-style">';
                    }
                    echo '<li><a href="/' . $row['C_Name_SEO'] . '/' . $row_sub_five['C_Name_SEO'] . '/' . $row_sub_five['C_ID'] . '"><span>' . (($row_sub_five['RC_Name'] != "") ? $row_sub_five['RC_Name'] : $row_sub_five['C_Name']) . '</span></a></li>';
                    $counter++;
                }
                if ($counter == 5) {
                    echo '</ul></div></div>';
                    $counter = 0;
                } else if ($totalRows == $i) {
                    echo '</ul></div></div>';
                }
            }
            ?>
            <div class="footer-item">
                <div class="footer-inside">
                    <ul class="text-style">
                        <li><a href="/maps.php"><span>Interactive Map</span></a></li>
                    </ul>
                </div>
            </div>
            <!--Footer logo section 1-->
        </div>
        <?php
        $footer = "SELECT FP_Link, FP_Photo, FP_Alt from tbl_Footer_Photo WHERE FP_Link != '' AND FP_Section = 1 AND FP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' ORDER BY FP_Order ASC";
        $footerResult = mysql_query($footer, $db) or die("Invalid query: $footer -- " . mysql_error());
        $footerCount = mysql_num_rows($footerResult);
        if ($footerCount > 0) {
            ?>
            <!--Footer logo section 1-->
            <div class="footer-images">
                <div class="footer-margin-auto">
                    <div>
                        <?php
                        while ($footerRow = mysql_fetch_array($footerResult)) {
                            $url = $footerRow['FP_Link'];
                            ?>
                            <a href="<?php echo $url ?>"><img src="http://<?php echo DOMAIN . IMG_LOC_REL . $footerRow['FP_Photo'] ?>" alt="<?php echo $footerRow['FP_Alt'] ?>" border="0" /></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php
        $footer2 = "SELECT FP_Link, FP_Photo, FP_Alt from tbl_Footer_Photo WHERE FP_Link != '' AND FP_Section = 2 AND FP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' ORDER BY FP_Order ASC";
        $footerResult2 = mysql_query($footer2, $db) or die("Invalid query: $footer2 -- " . mysql_error());
        $footerCount2 = mysql_num_rows($footerResult2);
        if ($footerCount2 > 0) {
            ?>
            <!--Footer logo section 2-->
            <div class="footer-images border-top-none">
                <div class="footer-margin-auto">
                    <div>
                        <?php
                        while ($footerRow2 = mysql_fetch_array($footerResult2)) {
                            $url2 = $footerRow2['FP_Link'];
                            ?>
                            <a href="<?php echo $url2 ?>"><img src="http://<?php echo DOMAIN . IMG_LOC_REL . $footerRow2['FP_Photo'] ?>" alt="<?php echo $footerRow2['FP_Alt'] ?>" border="0" /></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php
        $footer3 = "SELECT FP_Link, FP_Photo, FP_Alt from tbl_Footer_Photo WHERE FP_Link != '' AND FP_Section = 3 AND FP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' ORDER BY FP_Order ASC";
        $footerResult3 = mysql_query($footer3, $db) or die("Invalid query: $footer3 -- " . mysql_error());
        $footerCount3 = mysql_num_rows($footerResult3);
        if ($footerCount3 > 0) {
            ?>
            <div class="footer-urls">
                <ul>
                    <?php
                    while ($footerRow3 = mysql_fetch_array($footerResult3)) {
                        $url3 = $footerRow3['FP_Link'];
                        ?>
                        <li> <a href="<?php echo $url3 ?>"><?php echo $footerRow3['FP_Alt']; ?></a> </li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>
        <div class="footer-copyrights">  
            <div class="footer-copy-wrapper ckeditor-anchors">
                <p><?php echo $REGION['R_Footer_Text']; ?></p>
            </div>
        </div>
</footer>
</body>
</html>
<?php
mysql_close($db);
?>
<!--Footer End-->
<script>
    function play_video(id, player_id) {
        PlayerReady(id, player_id);
    }
    function get_youtube_videoid(url) {
        var video_id = url.split('v=')[1];
        var ampersandPosition = video_id.indexOf('&');
        if (ampersandPosition != -1) {
            video_id = video_id.substring(0, ampersandPosition);
        }
        return video_id;
    }
    // 2. This code loads the IFrame Player API code asynchronously.
    var tag = document.createElement('script');

    tag.src = "http://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // 3. This function creates an <iframe> (and YouTube player)
    //    after the API code downloads.
    var total_players = $("#total_players").val();
    for (var i = 1; i < total_players; i++) {
        eval("var player" + i);
    }
    var current_player_id;

    function onYouTubePlayerAPIReady() {
        for (var i = 1; i < total_players; i++) {
            eval("player" + i + "= new YT.Player('player" + i + "'," + "{" +
                    "height: '538'," +
                    "width: '940'," +
                    "videoId:'" + $("#video-link-" + i).val() +
                    "',events: {'onStateChange': onPlayerStateChange}});");
        }
    }

    // 4. The API will call this function when the video player is ready.
    function PlayerReady(id, player_id) {
        var width = $(".player" + id + "_wrapper").parent(".slide-images").find('img.slider_image').width();
        var height = $(".player" + id + "_wrapper").parent(".slide-images").find('img.slider_image').height();
        if (width == null) {
            var width = 580;
            var height = 351;
        }
        $(".player" + id + "_wrapper").parent(".slide-images").find('img.slider_image').css('opacity', '0');
        $(".center #prev").css('display', 'none');
        $(".center #next").css('display', 'none');
        // $(".player" + id + "_wrapper").css('top', '0');
        $(".player" + id + "_wrapper").css('opacity', '1');
        $(".player" + id + "_wrapper").css('z-index', '600');
        $('.sliderCycle').cycle('pause');
        this[player_id].setSize(width, height);
        this[player_id].playVideo();
        current_player_id = id;
    }

    // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.ENDED) {
            stopVideo();
        }
    }
    function stopVideo() {
        //    this[player_id].stopVideo();
        $(".player" + current_player_id + "_wrapper").css('opacity', '0');
        $(".player" + current_player_id + "_wrapper").css('z-index', '0');
        $(".player" + current_player_id + "_wrapper").parent(".slide-images").find('img.slider_image').css('opacity', '1');
        $(".center #prev").css('display', 'block');
        $(".center #next").css('display', 'block');
        $('.sliderCycle').cycle('resume');
    }

</script>