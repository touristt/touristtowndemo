/*----------------------------------------------------*/
/*	Date Picker
 /*----------------------------------------------------*/
$(document).ready(function () {
    //Show first image and hide all cycle plugin for listing profile
    $(".sliderCycle").children().hide();// hides all images
    $(".sliderCycle").children(":first").show();// shows the first image
//    var middle = $('.logo-top-nav').width();
//    middle = middle + 40;
//    var width_for_div = 949 - middle;
//    $('.site-logo').width(width_for_div);
    /*----------------------------------------------------*/
    /*	Layer Slider on home, listing detail page
     /*----------------------------------------------------*/
    $('ul.bxslider li').show();
    $('.bxslider').bxSlider({
        auto: true,
        minSlides: 3,
        maxSlides: 3,
        slideWidth: 960,
        slideMargin: 10,
        pager: false,
        infiniteLoop: true,
        moveSlides: 3
    });
});
/*----------------------------------------------------*/
/*	Public Accordions
 /*----------------------------------------------------*/
$(function () {
    $(".accordion").accordion({
        collapsible: true,
        active: false
    });
});

$(function () {
    $('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
    $(window).scroll(function () {
        if ($(this).scrollTop() > 400) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });
    //Click event to scroll to top
    $('.scrollToTop').click(function () {
        $('html, body').animate({scrollTop: 0}, 1000);
        return false;
    });
});

/*----------------------------------------------------*/
/*	JQuery Cycle Slider
 /*----------------------------------------------------*/
$(window).load(function () {
    $('.show-slider-display').show();
    $('#next').show();
    $('#prev').show();
    $('.sliderCycle').cycle({
        fx: 'fade',
        next: '#next',
        prev: '#prev',
        before: onAfter
    });
    function onAfter(curr, next, opts) {
        var caption = ($(next).index() + 1) + '/' + opts.slideCount;
    }
    var slides = $('.sliderCycle').children().length;
    if (slides <= 1) {
        $('#next').css("display", "none");
        $('#prev').css("display", "none");
    } else {
        $('#next').css("display", "visible");
        $('#prev').css("display", "visible");
    }
});

//on window resize
$( window ).resize(function() {
  $('.sliderCycle').height($('.slide-images:visible img.slider_image').height());
});

/*----------------------------------------------------*/
/*	Tracking Advertisements
 /*----------------------------------------------------*/
function advertClicks(A_ID) {
    var pathname = window.location.href;
    var split_path = pathname.split("/");
    var DOMAIN = split_path[2];
    $.post('http://' + DOMAIN + '/coupon/update-clicks-advert.php', {
        AS_A_ID: A_ID
    });
}
function dropdown_ads_impression(C_ID) {
    var ad = 0;
    var total = document.getElementById('total-ads-' + C_ID).value;
    for (var i = 0; i < total; i++) {
        ad = document.getElementById('ad' + i + '-' + C_ID);
        if (ad.value != '') {
            var pathname = window.location.href;
            var split_path = pathname.split("/");
            var DOMAIN = split_path[2];
            $.post('http://' + DOMAIN + '/coupon/dropdown-impressions.php', {
                AS_A_ID: ad.value
            });
        }
    }
}