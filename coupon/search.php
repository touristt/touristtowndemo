<?php
require_once '../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
require_once 'include/public/header.php';
?>
<!--Description Start-->
<section class="description search-results margin-bottom-none">
    <div class="description-wrapper">
        <div class="description-inner padding-bottom-none">
            <h1 class="heading-text">Search Results</h1>
            <form method="GET" action="/search.php">
                <div class="search-form">
                    <div class="search_field"><input type="text" name="txtSearch" placeholder="<?php echo stripslashes($_GET['txtSearch']) ?>"></div>
                    <div class="search_button"><input type="submit" name="submit" value="go"></div>
                </div>
            </form>
        </div>
    </div>
</section>
<!--Thumbnail Grid-->
<section class="thumbnail-grid padding-bottom-none border-none">
    <div class="grid-wrapper">
        <div class="grid-inner">

            <!--mywork -->
            <?PHP
            if (strlen($_GET['txtSearch']) > 3) {
                $exact_phrase = array();
                $exact_phrase_blid = array();
                $not_exact_phrase = array();
                
                $query = "SELECT BFC_ID, BFC_Thumbnail, BFC_Title, RC_Name, C_Name, BL_Listing_Title, BL_Name_SEO, BL_ID FROM tbl_Business_Feature_Coupon
                            LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
                            LEFT JOIN tbl_Category ON C_ID = BFCCM_C_ID
                            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                            LEFT JOIN tbl_Business_Listing ON BL_ID = BFC_BL_ID
                            WHERE (BFC_Title LIKE '%" . encode_strings($_GET['txtSearch'], $db) . "%'
                            OR BL_Listing_Title LIKE '%" . encode_strings($_GET['txtSearch'], $db) . "%'  
                            OR BL_Description LIKE '%" . encode_strings($_GET['txtSearch'], $db) . "%'      
                            OR BFC_Description LIKE '%" . encode_strings($_GET['txtSearch'], $db) . "%')
                            AND BFC_Status = 1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date = 0000-00-00) AND hide_show_listing = '1' GROUP BY BFC_ID ORDER BY BFC_ID DESC";
                
                $resultCoupon = mysql_query($query, $db) or die("Invalid query: $query -- " . mysql_error());
                $i = 0;
                while ($rowCoupon = mysql_fetch_assoc($resultCoupon)) {
                    $i++;
                    if ($rowCoupon['BFC_Thumbnail'] != '') {
                        $listing_image = $rowCoupon['BFC_Thumbnail'];
                    } else {
                        $listing_image = $default_thumbnail_image;
                    }
                    if ($i == 1) {
                        echo "<div class='thumbnails static-thumbs'>";
                    }
                    ?>
                    <div class="thumb-item">
                        <a href="/profile/<?php echo (($rowCoupon['BFC_Title'] != "") ? clean($rowCoupon['BFC_Title']) : $rowCoupon['BL_Name_SEO']) ?>/<?php echo $rowCoupon['BFC_ID'] ?>/">
                            <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo $rowCoupon['BFC_Title'] ?>"/>
                            <h3 class="thumbnail-heading coupon-header"><?php echo (($rowCoupon['RC_Name'] != "") ? $rowCoupon['RC_Name'] : $rowCoupon['C_Name']) ?></h3>
                            <h3 class="thumbnail-desc"><?php echo $rowCoupon['BL_Listing_Title'] ?></h3>
                        </a>
                        <a href="/profile/<?php echo (($rowCoupon['BFC_Title'] != "") ? clean($rowCoupon['BFC_Title']) : $rowCoupon['BL_Name_SEO']) ?>/<?php echo $rowCoupon['BFC_ID'] ?>/"><h3 class="thumbnail-coupon"><?php echo $rowCoupon['BFC_Title'] ?></h3></a>
                    </div>
                    <?php
                    if ($i == 3) {
                        echo '</div>';
                        $i = 0;
                    }
                }
            } else {
                ?>
                <p style="clear: left; text-align: center">Search Term must be more than three (3) characters.</p>
                <?PHP
            }
            ?>


        </div>
    </div></section>
<?php require_once 'include/public/footer.php'; ?>
