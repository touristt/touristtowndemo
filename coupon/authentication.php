<?php
require_once '../include/config.inc.php';
$display = "style='display: none;'";
if (isset($_POST['op']) && $_POST['op'] == 'login') {
    $select = "SELECT U_Username, U_Password FROM tbl_User WHERE U_Username	= '" . encode_strings($_POST['username'], $db) . "' && U_Password = '" . md5(encode_strings($_POST['password'], $db)) . "'";
    $result_log = mysql_query($select, $db) or die("Invalid query: $select -- " . mysql_error());
    if (mysql_num_rows($result_log) > 0) {
        $_SESSION['REGION_AUTH_ACT_INACT'] = 1;
        header("location: http://" . $_SESSION['WEBSITE_LOAD']);
        exit;
    } else {
        $display = "style='display: block;'";
    }
}
?>
<style>
    @font-face {
        font-family: 'news-gothic-std';
        src: url('/stylesheets/docs/assets/font/News Gothic.eot');
        src: url('/stylesheets/docs/assets/font/News Gothic.eot?#iefix') format('embedded-opentype'),
            url('/stylesheets/docs/assets/font/News Gothic.woff') format('woff'),
            url('/stylesheets/docs/assets/font/News Gothic.ttf') format('truetype');
    }
    .auth_logo {
        font-family: 'news-gothic-std';
        text-align: center;
    }
    .authentication_container {
        font-family: 'news-gothic-std';
        text-align: center;
    }
    .auth_message {
        font-size: 20px;
    }
    .auth_message {
        font-size: 20px;
    }
    .auth-form-inside-div {
        margin-top: 100px;
    }
    .auth-form-data {
        padding: 10px 0;
    }
    .auth-form-data input[type="text"],input[type="password"] {
        border: 1px solid;
        padding: 8px 5px;
        width: 40%;
    }
    .auth-form-data input[type="submit"] {
        padding: 10px 15px;
        text-decoration: none;
        background: #6dac29;
        color: #fff7ff;
        font-size: 16px;
        border: none;
        text-shadow: none;
        width: 145px;
    }
    .auth-form-data input[type="submit"] {
        padding: 10px 15px;
        text-decoration: none;
        background: #6dac29;
        color: #fff7ff;
        font-size: 16px;
        border: none;
        text-shadow: none;
        width: 145px;
    }
    .error{
        color: #EC2323;
    }
</style>
<div class="main-content">
    <div class="auth_logo">
        <img src="http://<?= DOMAIN ?>/images/TouristTown-Logo.gif" alt="Tourist Town" width="195" height="43" hspace="20" vspace="20" border="0">
    </div>
    <div class="authentication_container">
        <div class="auth_message">
            <p>
                The website is inactive for maintenance, if you are the admin please login to continue.
            </p>
        </div>
        <div class="auth_message error" <?= $display ?>>
            <p>
                Invalid Login.
            </p>
        </div>
        <div class="auth_form">
            <form action="" method="POST">
                <input type="hidden" name="op" value="login"/>
                <div class="auth-form-inside-div">
                    <div class="auth-form-data">
                        <input type="text" name="username" required/>
                    </div>
                    <div class="auth-form-data">
                        <input type="password" name="password" required/>
                    </div>
                    <div class="auth-form-data">
                        <input type="submit" value="Login Now"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
