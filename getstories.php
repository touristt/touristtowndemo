<?php
require_once 'include/config.inc.php';
require_once 'include/public-site-functions.inc.php';

//Getting Region Theme
$getThemeOnGetStories = "SELECT TO_Default_Thumbnail_Desktop FROM tbl_Theme_Options WHERE TO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
$themeRegionOnGetStories = mysql_query($getThemeOnGetStories, $db) or die("Invalid query: $getThemeOnGetStories -- " . mysql_error());
$THEMEOnGetStories = mysql_fetch_assoc($themeRegionOnGetStories);

//Defualt Thumbnail Images
if ($THEME['TO_Default_Thumbnail_Desktop'] != '') {
    $default_thumbnail_image = $THEME['TO_Default_Thumbnail_Desktop'];
} else {
    $default_thumbnail_image = 'Default-Thumbnail-Desktop.jpg';
}

//Checking if SEASONS SESSIOIN is set or not
$SEASONS_JOIN = "";
$StorySeasons_JOIN = "";
$SEASONS_WHERE = "";
$StorySeasons_WHERE = "";
if (isset($_SESSION['SEASONS']) && $_SESSION['SEASONS'] > 0) {
    $SEASONS_JOIN = " LEFT JOIN tbl_Business_Listing_Season ON BL_ID = BLS_BL_ID";
    $StorySeasons_JOIN = " LEFT JOIN tbl_Story_Season ON S_ID = SS_S_ID";
    $SEASONS_WHERE = " AND BLS_S_ID = '" . encode_strings($_SESSION['SEASONS'], $db) . "'";
    $StorySeasons_WHERE = " AND SS_Season = '" . encode_strings($_SESSION['SEASONS'], $db) . "'";
}
$storyCatSEOOnGetStories = "SELECT C_Name_SEO FROM tbl_Category WHERE C_Is_Blog = 1";
$storyCatSEOResOnGetStories = mysql_query($storyCatSEOOnGetStories);
$storyCatSEORowOnGetStories = mysql_fetch_array($storyCatSEOResOnGetStories);
$stories = "SELECT S_ID, S_Category, S_Active, S_Title, S_Thumbnail, C_Name, C_Parent, RC_Name FROM tbl_Story 
            INNER JOIN tbl_Story_Region ON S_ID = SR_S_ID
            LEFT JOIN tbl_Category ON S_Category = C_ID
            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID
            $StorySeasons_JOIN
            WHERE S_Category = '" . $_POST['StoryCategory'] . "'
            AND SR_R_ID = '" . $_POST['currentRegion'] . "' 
            AND S_ID != '" . $_POST["currentStory"] . "' AND S_Active = 1
            $StorySeasons_WHERE GROUP BY S_ID ORDER BY S_Title ";
//See if this region has any limit
if ($_POST['currentLimit'] > 0) {
    $stories .= " LIMIT " . $_POST['currentLimit'];
} else {
    $stories .= " LIMIT 10";
}
$resStoryTopTen = mysql_query($stories, $db) or die("Invalid query: $stories -- " . mysql_error());
?>
<?php
while ($rowStoryTopTen = mysql_fetch_assoc($resStoryTopTen)) {
    if ($rowStoryTopTen['S_Thumbnail'] != '') {
        $story_image = $rowStoryTopTen['S_Thumbnail'];
    } else {
        $story_image = $default_thumbnail_image;
    }
    ?>
    <div class='thumbnails static-thumbs'>
        <div class="thumb-item">
            <a href="/<?php echo $storyCatSEORowOnGetStories['C_Name_SEO']; ?>/<?php echo clean($rowStoryTopTen['S_Title']) ?>/<?php echo clean($rowStoryTopTen['S_ID']) ?>">
                <img src="<?php echo IMG_LOC_REL . $story_image; ?>" alt="" longdesc="<?php echo $rowStoryTopTen['S_Title'] ?>">
                <h3 class="thumbnail-heading"><?php echo (($rowStoryTopTen['RC_Name'] == "") ? $rowStoryTopTen['C_Name'] : $rowStoryTopTen['RC_Name']) ?></h3>
                <h3 class="thumbnail-desc"><?php echo $rowStoryTopTen['S_Title'] ?></h3>
            </a>
        </div>
    </div>
<?php } ?>