<?php
require_once 'include/config.inc.php';
require_once 'update-impression-advert.php';
require_once 'include/public-site-functions.inc.php';
require_once 'include/public/header.php';

if ($REGION['R_Parent'] == 0) {
    $REG = '';
} else {
    $REG = "AND BLP_R_ID = " . $REGION['R_ID'];
}

$sql_tbl_region_404 = "SELECT RP_Title, RP_Description, RP_Photo, RP_Photo_Title FROM tbl_Region_404 WHERE RP_RID = '" . $REGION['R_ID'] . "'";
$sql_tbl_region_404_result = mysql_query($sql_tbl_region_404) or die("Invalid query: $sql_tbl_region_404 -- " . mysql_error());
$num_of_region_404 = mysql_fetch_array($sql_tbl_region_404_result);

$sql_slider_404 = " SELECT bl.BL_ID, bl.BL_Listing_Title, blp.BLP_Photo, bl.BL_Name_SEO, c.C_Name, rc.RC_Name 
                    FROM tbl_404_Listing l LEFT JOIN tbl_Business_Listing bl ON l.BL_ID = bl.BL_ID 
                    LEFT JOIN tbl_Business_Listing_Photo blp ON bl.BL_ID = blp.BLP_BL_ID
                    LEFT join tbl_Business_Listing_Category_Region blcr ON bl.BL_ID = blcr.BLCR_BL_ID
                    LEFT join tbl_Business_Listing_Category blc ON bl.BL_ID = blc.BLC_BL_ID 
                    LEFT join tbl_Category c ON blc.BLC_C_ID = c.C_ID
                    LEFT join tbl_Region_Category rc ON rc.RC_C_ID = c.C_ID AND rc.RC_R_ID = '" . $REGION['R_ID'] . "'
                    WHERE l.R_ID = '" . $REGION['R_ID'] . "' AND bl.hide_show_listing=1 $REG GROUP BY bl.BL_ID ";

$sql_slider_404_result = mysql_query($sql_slider_404, $db) or die("Invalid query: $sql_slider_404 -- " . mysql_error());
?>
<!--Slider Start-->
<section class="main_slider">
    <div class="slider_wrapper">
        <div class="sliderCycle" data-cycle-loader="wait" data-cycle-fx=fade data-cycle-timeout=0 data-cycle-prev="#prev" data-cycle-next="#next">
            <?php
            if ($num_of_region_404['RP_Photo'] != '') {
                ?>                       
                <div class="slide-images">
                    <div class="slider-text">
                        <h1 class="show-slider-display"><?php echo ucfirst($num_of_region_404['RP_Photo_Title']) ?></h1>
                    </div>
                    <img class="slider_image" class="show-slider-display" src="<?php echo IMG_LOC_REL . $num_of_region_404['RP_Photo'] ?>" alt="">
                </div>
                <?php
            } else {
                echo '<div class="slide-images"><img class="slider_image" class="show-slider-display" src="' . IMG_LOC_REL . $default_header_image . '" alt="" /></div>';
            }
            ?>
        </div>       

    </div>
</section>
<!--Slider End-->
<!--Description Start-->
<section class="description theme-description">
    <div class="description-wrapper">
        <div class="description-inner">
            <h1 class="heading-text"><?php echo ($num_of_region_404['RP_Title'] != '') ? $num_of_region_404['RP_Title'] : ''; ?></h1>
            <div class="site-text ckeditor-anchors">
                <?PHP
                if ($num_of_region_404['RP_Description']) {
                    echo $num_of_region_404['RP_Description'];
                }
                ?>
            </div>
        </div>
    </div>
</section> 
<!--Description End-->
<!-- Feature Stories-->

<!--Thumbnail Grid-->
<section class="thumbnail-grid sub-cat-margin border-none">
    <div class="grid-wrapper">
        <div class="grid-inner border-none padding-bottom-none">
            <?php
            $count_row_404_region = mysql_num_rows($sql_slider_404_result);
            $i = 0;
            if ($count_row_404_region > 0) {
                while ($show_row_404_region = mysql_fetch_array($sql_slider_404_result)) {
                    $i++;
                    if ($show_row_404_region['BLP_Photo'] != '') {
                        $listing_image = $show_row_404_region['BLP_Photo'];
                    } else {
                        $listing_image = $default_thumbnail_image;
                    }
                    if ($i == 1) {
                        echo "<div class='thumbnails static-thumbs'>";
                    }
                    ?>
                    <div class="thumb-item">

                        <a href="/profile/<?php echo $show_row_404_region['BL_Name_SEO'] ?>/<?php echo $show_row_404_region['BL_ID'] ?>/"> 
                            <img src="<?php echo IMG_LOC_REL . $listing_image ?>" alt="<?php echo $show_row_404_region['BL_Photo'] ?>" />
                            <h3 class="thumbnail-heading"><?php echo ($show_row_404_region['RC_Name'] != '') ? $show_row_404_region['RC_Name'] : $show_row_404_region['C_Name'] ?></h3>
                            <h3 class="thumbnail-desc"><?php echo $show_row_404_region['BL_Listing_Title']; ?></h3>
                        </a> 
                    </div>
                    <?php
                    if ($i == 3) {
                        echo '</div>';
                        $i = 0;
                    }
                }
            }
            ?>
        </div>
    </div>
</section>
<?php require_once 'include/public/footer.php'; ?>