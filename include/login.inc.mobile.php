<?PHP
define('SESSIONERROR', 'ERROR');
define('SESSIONINFO', 'info');

$sql_select = "SELECT * FROM tbl_User ";

if (isset($_REQUEST['logop']) && $_REQUEST['logop'] == "login") {
    $sql_log = $sql_select
            . "WHERE U_Username 	= '" . encode_strings($_POST['LOGIN_USER'], $db) . "' "
            . "&& U_Password = '" . md5(encode_strings($_POST['LOGIN_PASS'], $db)) . "' ";
    $result_log = mysql_query($sql_log, $db) or die("Invalid query: $sql_log -- " . mysql_error());
    if (mysql_num_rows($result_log) <> 1) {
        $_SESSION['login_error'] = 1;
        header("Location: " . INPUT_DIR . "mobile/login.php");
        exit();
    } else {
        if ($_POST['remember_me'] == 'on') {
            $cookie_name = 'admin_email_id';
            $cookie_value = $LOGIN_USER;
            setcookie($cookie_name, $cookie_value, time() + (60 * 60 * 24 * 30), "/"); // 86400 = 1 day
            $cookie_pass = 'admin_password';
            $cookie_value_pass = $LOGIN_PASS;
            setcookie($cookie_pass, $cookie_value_pass, time() + (60 * 60 * 24 * 30), "/"); // 86400 = 1 day
        }
        $UID = UpdateUserLog($result_log, $db);
        header("Location: " . INPUT_DIR . "mobile/admin-mobile-menu.php");
        exit;
    }
} elseif (isset($_REQUEST['logop']) && $_REQUEST['logop'] == 'logout') {
    $sql = "UPDATE tbl_User SET U_EXPIRE = '0000-00-00' "
            . "WHERE U_KEY = '" . $_COOKIE['USER_KEY'] . "' ";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    my_session_destroy('admin');
    header("Location: " . INPUT_DIR . "mobile/login.php");
    exit();
} elseif (isset($_SESSION['USER_KEY']) && $_SESSION['USER_KEY'] != '0' && $_SESSION['USER_KEY'] != '') {
    $sql_Check = $sql_select
            . "WHERE U_KEY = '" . encode_strings($_SESSION['USER_KEY'], $db) . "' "
            . "&& U_EXPIRE > NOW() ";
    $result_Check = mysql_query($sql_Check, $db) or die("Invalid query: $sql_Check -- " . mysql_error());
    if (mysql_num_rows($result_Check) <> 1) {
        my_session_destroy('admin');
        header("Location: " . INPUT_DIR . "mobile/login.php");
        exit();
    } else {
        $UID = UpdateUserLog($result_Check, $db, $_SESSION['USER_KEY']);
    }
} else {
    my_session_destroy('admin');
    header("Location: " . INPUT_DIR . "mobile/login.php");
    exit();
}

function UpdateUserLog($result, $db, $key = "0") {
    if ($row = mysql_fetch_assoc($result)) {
        $sql = "UPDATE tbl_User SET U_EXPIRE = DATE_ADD(NOW(), "
                . "INTERVAL 1 HOUR)";
        if ($key == "0") {
            $key = mt_rand(1000, 9999999);
            $sql .= ", U_KEY = '$key'";
            $_SESSION['USER_KEY'] = $key;
            $_SESSION['USER_ID'] = $row['U_ID'];
            $_SESSION['USER_NAME'] = $row['U_Username'];
            $_SESSION['USER_EM'] = $row['U_Email'];
            $_SESSION['USER_SUPER_USER'] = $row['U_Super_User'];
            $_SESSION['USER_ROLES'] = array();
            $sql_role = "select * from tbl_User_Role WHERE UR_U_ID = '" . $_SESSION['USER_ID'] . "'";
            $result_role = mysql_query($sql_role, $db) or die("Invalid query: $sql_role -- " . mysql_error());
            while ($data_role = mysql_fetch_assoc($result_role)) {
                $_SESSION['USER_ROLES'][] = $data_role['UR_R_ID'];
                if ($data_role['UR_R_ID'] == 'bgadmin') {
                    $sqlbg = "select * from tbl_Buying_Group left join tbl_User_Role on BG_ID = UR_Limit WHERE UR_Limit = '" . $data_role['UR_Limit'] . "'";
                    $resultbg = mysql_query($sqlbg, $db) or die("Invalid query: $sqlbg -- " . mysql_error());
                    $rowbg = mysql_fetch_assoc($resultbg);
                    $_SESSION['USER_LIMIT'] = $rowbg['BG_Region'];
                } else {
                    $_SESSION['USER_LIMIT'] = $data_role['UR_Limit'];
                }
            }
            $_SESSION['USER_PERMISSIONS'] = array();
            $sql2 = "SELECT * FROM tbl_User_Permission LEFT JOIN tbl_User_Role on UP_R_ID = UR_R_ID  WHERE UR_U_ID = '" . $row[U_ID] . "'";
            $result = mysql_query($sql2, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($data = mysql_fetch_assoc($result)) {
                $_SESSION['USER_PERMISSIONS'][] = $data['UP_P_ID'];
            }
            $_SESSION['user_online'] = $row['U_F_Name'];
        }
        $sql .= " WHERE U_ID = '$row[U_ID]' ";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    } else {
        header("Location: " . INPUT_DIR . "mobile/login.php");
        exit();
    }
    return $row['U_ID'];
}
?>