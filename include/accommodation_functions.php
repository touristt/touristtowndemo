<?php

function send_activation_and_launch_emails($email_type = 1, $block_id) {
    require 'PHPMailer/class.phpmailer.php';
    $footer_image1 = 'images/Footer-SaugeenShores.jpg';
    $footer_image2 = 'images/Footer-PortElgin.png';
    $footer_image3 = 'images/Footer-Southampton.png';
    $group_blocks_info_query = "SELECT * FROM `acc_group_block_info` WHERE id = $block_id";
    $group_blocks_info = mysql_query($group_blocks_info_query);
    $group_data = mysql_fetch_assoc($group_blocks_info);
    $blocks_data = blocks_inThis_group($group_data['id']);
    $block_info = '';
    $i = 1;
    if ($blocks_data) {
        foreach ($blocks_data as $block) {
            $block_info.='<tr><td style="background-color:#f4f9fc; color: #000000;">' . $i++ . ') ' . $block->start_date . '---' . $block->end_date . '</td></tr>';
        }
    }
    $result = activation_and_launch_emails($email_type);
    $image_region = '';
    if (isset($result->img) && $email_type == 1) {
        $image_region = '<tr>
            <td colspan="2"><div style="width:640px;height: 240px">
      <img src="' . $result->img . '" width="640" height="240"/></div></td>
        </tr>';
    } else if ($email_type == 2) {
        $image_region = '<tr>
            <td colspan="2"><div style="width:640px;height:130px;background-color: #d9eaf5;color:#0475b9;font-size:32px; padding-top:70px;padding-left:10px;">
      ' . strtoupper($group_data['name']) . '<p style="color:#0475b9;font-size:24px;">Accommodation availability is now live<p></div></td>
        </tr>';
    }
//    $emails_Bname_Cname=get_EmailsBnameCname();
    $emails_Bname_Cname = (object) array('0' => (object) array('B_Contact' => ' Brad Smith', 'B_Name' => ' Smith', 'B_Email' => 'brad@canvasstudios.ca'),
                '1' => (object) array('B_Contact' => ' Muhammad Tanweer', 'B_Name' => ' Tanweer', 'B_Email' => 'muhammad.tanweer@app-desk.com'));
    foreach ($emails_Bname_Cname as $send_email_to) {
        //replace name
        $body_text_replace_name = str_replace('[contact name]', $send_email_to->B_Contact, $result->body_txt);
        //replace Group block name
        $body_text_replace_blockName = str_replace('[group block name]', $group_data['name'], $body_text_replace_name);
        //replace Business name
        $body_text_replaced = str_replace('[business name]', $send_email_to->B_Name, $body_text_replace_blockName);
        $body = '<table width="640px" border="0" cellspacing="0" cellpadding="0">
    <tbody>';
        $body.=$image_region;
        $body.='<tr>
            <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td style="width:490px;padding-left:10px;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>' . $body_text_replaced . '</td>
                    </tr>
                </table>
            </td>
            <td style="width:210px;vertical-align: text-top;padding-right:10px;">
                <table width="100%" border="0" cellspacing="0" cellpadding="10">
                    <tr>';
        $body.='<td style="background-color:#0778bc;color:#ffffff">' . strtoupper($group_data['name']) . '</td>
                    </tr>';
        $body.=$block_info;
        $body.='</table>
            </td>
        </tr>
        <tr>
            <td  colspan="2" style="border-bottom:1px solid ##e2e2e2;">&nbsp;</td>
        </tr>
        <tr>
     <td  colspan="2">
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="padding-left:30px;"><img width="150" vspace="10" height="60" border="0" alt="" src="' . $footer_image1 . '"/></td>
                        <td><img width="92" vspace="10" height="62" border="0" alt="" src="' . $footer_image2 . '"/></td>
                        <td><img width="113" vspace="10" height="66" border="0" alt="" src="' . $footer_image3 . '"/></td>
                        </tr>
                </table>     
</td>   
</tr>
        <!--footer-->
        <tr>
            <td  colspan="2"   colspan="2" style="border-top:1px solid ##e2e2e2;">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" style="padding:0px 10px;">' . $result->footer_txt . '
            </td>
        </tr>
    </tbody>
</table>';
        $mail = new PHPMailer();
//    $email_id='muhammad.tanweer@app-desk.com';
        $mail->From = "info@touristtown.com";
        $mail->FromName = "Tourist Town";
        $mail->IsHTML(true);
        $mail->AddAddress($send_email_to->B_Email);
        if ($email_type == 1) {
            $mail->Subject = "Accommodation Availability " . strtoupper($group_data['name']) . " Has been activated";
        } else {
            $mail->Subject = "Accommodation Availability " . strtoupper($group_data['name']) . " is now live";
        }
        //$_SESSION['email' . 'orderid'];
        $mail->MsgHTML($body);
        $mail->Send();
    }
    return 1;
}

function blocks_inThis_group($group_block_id) {
    $start_date = "SELECT start_date,end_date FROM acc_blocks WHERE group_block_id = $group_block_id";
    $result = mysql_query($start_date);
    while ($row = mysql_fetch_object($result)) {
        $results[] = $row;
    }
    if (empty($results)) {
        $results = 0;
    }
    return $results;
}

/* 1 for activation email and 2 for launch email */

function activation_and_launch_emails($email_type = 1) {
    $query = "select * from acc_emails_tpl where activation_launch='" . $email_type . "' LIMIT 1";
    $result = mysql_query($query) or die(mysql_error());
    while ($row = mysql_fetch_object($result)) {
        $record = $row;
    }
    if (empty($record)) {
        $record = 0;
    }
    return $record;
}

function get_EmailsBnameCname() {
    $sql = "SELECT DISTINCT tbl_Business.*, DATEDIFF(B_Renewal_Date, CURDATE()) as timeRemaining FROM tbl_Business 
                                    LEFT JOIN tbl_Business_Listing ON BL_B_ID = B_ID WHERE BL_C_ID =4 ORDER BY B_Name";
    $result = mysql_query($sql);
    while ($row = mysql_fetch_object($result)) {
        $results[] = $row;
    }
    if (empty($results)) {
        $results = 0;
    }
    return $results;
}

function get_listings($cat_id, $reg_id, $isclosed, $filters = '', $search = '') {
    if ($isclosed == 1) {
        $acc_closed = " AND closed != 1";
    }
    $reg_id = region_is_parent($reg_id);
    $hotels_select = "SELECT DISTINCTROW acc_accommodator_capacity.*,tbl_Business_Listing.*, tbl_Listing_Type.*, tbl_Business.* 
                    FROM tbl_Business_Listing_Category_Region
                    LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BLCR_BL_ID AND BLC_C_ID = '" . $cat_id . "'
                    LEFT JOIN tbl_Business_Listing ON BL_ID = BLC_BL_ID 
                    LEFT JOIN acc_accommodator_capacity ON tbl_Business_Listing.BL_ID = acc_accommodator_capacity.accommodator_id 
                    LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
                    LEFT JOIN tbl_Business ON B_ID = BL_B_ID 
                    WHERE BLCR_BLC_R_ID IN ($reg_id) AND B_ID is NOT NULL  AND hide_show_listing='1' AND BL_Show_In_Accommodation = 1 $acc_closed";
    if ($filters != '') {
        $filter_count = 0; // for counting filter
        $hotels_select .= " AND (";
        foreach ($filters as $key => $value) {
//            if($key == 'closed') {
//              continue;
//            }
            if ($filter_count == 0) {
                $hotels_select .= "$key = $value ";
            } else {
                $hotels_select .= " OR $key = $value ";
            }
            $filter_count++;
        }
        $hotels_select .= ")";
    }
    if ($search != '') {
        $hotels_select .= " AND (B_Name LIKE '%$search%' OR BL_Listing_Title LIKE '%$search%')";
    }
    $hotels_select .= " ORDER BY BL_Listing_Title, B_Name";
//    print $hotels_select;
    $result = mysql_query($hotels_select);
    $records_found = '';
    while ($row = mysql_fetch_array($result)) {
        $records_found[] = $row;
    }
    $return['listings'] = $records_found;
    $return['pagination'] = '';
    return $return;
}

//function for the search of listings
function get_search_result($search_word) {
    $search_select = "SELECT DISTINCT
                        tbl_Business_Listing_Category_Region.BLCR_BLC_ID,
                        tbl_Business_Listing_Category_Region.BLCR_BLC_R_ID,
                        tbl_Business.B_ID,
                        tbl_Business.B_Name,
                        tbl_Listing_Type.LT_Name,
                        tbl_Region.R_Name,
                        tbl_Category.C_Name,
                        tbl_Business_Listing.BL_ID,
                        tbl_Business_Listing.BL_Override_Title,
                        tbl_Business_Listing.BL_Listing_Title
                        FROM
                        tbl_Business_Listing_Category_Region
                        LEFT JOIN tbl_Business_Listing_Category ON tbl_Business_Listing_Category.BLC_ID = tbl_Business_Listing_Category_Region.BLCR_BLC_ID
                        INNER JOIN tbl_Business_Listing ON tbl_Business_Listing.BL_ID = BLC_BL_ID AND tbl_Business_Listing.BL_ID = tbl_Business_Listing_Category.BLC_BL_ID
                        LEFT JOIN acc_accommodator_capacity ON tbl_Business_Listing.BL_ID = acc_accommodator_capacity.accommodator_id
                        LEFT JOIN tbl_Listing_Type ON tbl_Listing_Type.LT_ID = BL_Listing_Type AND tbl_Business_Listing.BL_Listing_Type = tbl_Listing_Type.LT_ID
                        LEFT JOIN tbl_Business ON tbl_Business.B_ID = BL_B_ID AND tbl_Business_Listing.BL_B_ID = tbl_Business.B_ID
                        INNER JOIN tbl_Region ON tbl_Business_Listing_Category_Region.BLCR_BLC_R_ID = tbl_Region.R_ID
                        INNER JOIN tbl_Category ON tbl_Category.C_ID = tbl_Business_Listing_Category.BLC_C_ID
                        WHERE";
    if ($search_word != '') {
        $search_select .= " B_Name LIKE '%$search_word%' AND";
    }
    $search_select .= " B_ID is NOT NULL AND hide_show_listing='1' AND tbl_Business_Listing_Category.BLC_C_ID in(29,30,31,45,59)
                        ORDER BY LT_Order DESC,BL_Points DESC, B_Name";
//    print_r($search_select);
    $result = mysql_query($search_select);
    if (mysql_num_rows($result) > 0) {
        return $result;
    }
    return '';
}

function region_is_parent($region_id) {
    $region_details = mysql_fetch_array(mysql_query("Select * from tbl_Region where R_ID = $region_id"));
    if ($region_details['R_Parent'] == 0) {
        $region_details = mysql_fetch_array(mysql_query("Select group_concat(R_ID) from tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = $region_id"));
        $region_id = $region_details[0];
    }
    return $region_id;
}

function get_listings_capacity($listing_id) {
    $capacity_select = "";
}

function group_block_info($group_block_id) {
    $start_date = "SELECT MIN(start_date) AS min_date,MAX(end_date) AS max_date, COUNT(DISTINCT id) as blocks FROM acc_blocks WHERE group_block_id = $group_block_id";
    $result = mysql_query($start_date);
    if ($result) {
        return $result = mysql_fetch_array($result);
    }
    return NULL;
}

function group_block_status($group_block_id) {
    $block_query = "SELECT * FROM `acc_group_block_info` WHERE id = $group_block_id";
    $blocks_data = mysql_query($block_query);
    if (mysql_num_rows($blocks_data) > 0) {
        $block = mysql_fetch_array($blocks_data);
        $status = $block['status'];
        $launch_status = $block['launch_status'];
        $pending_data = group_block_info($group_block_id);        //incase for pending block
        $today = date('Y-m-d');
        if ($launch_status == 0 && $status == 0) {
            return 'PENDING';
        } else if ($launch_status == 1) {
            return 'LIVE';
        } else if ($launch_status == 0 && $status == 1) {
            return 'ACTIVE';
        } else if ($today > date('Y-m-d', strtotime($pending_data['max_date'])) && $pending_data['max_date'] != NULL && !isset($pending_data['max_date'])) {
            return 'ARCHIVED';
        } else
            return '-';
    }
}

function category_name($cat_id) {
    $category_query = "Select * from tbl_Category where C_ID =$cat_id";
    $cat = mysql_query($category_query);
    if ($cat) {
        return $result = mysql_fetch_array($cat);
    }
    return NULL;
}

function block_info($block_id) {
    $block_query = "SELECT ab.*, agb.name AS block_group_name, agb.id AS block_group_id  
    FROM `acc_blocks` ab 
    Join acc_group_block_info agb On agb.id = ab.group_block_id 
    WHERE ab.id = $block_id";
    $blocks_data = mysql_query($block_query) or die(mysql_error());
    if (mysql_num_rows($blocks_data) > 0) {
        $block = mysql_fetch_array($blocks_data);
        return $block;
    } else {
        return 0;
    }
}

function cat_type_header($cat_id, $active_item, $block) {
    $days = '';
    if ($cat_id == 59 || $cat_id == 30) {
        ?>
        <div class = "data-header-rows acc-padding-top-bottom content-header ccommodation-cap-heading-width header-row1-color">
            <div class = "header-row1-col1"><?php echo $active_item ?></div>
            <div class="header-row1-col2">Block:<?php echo $block['start_date'] . ' to ' . $block['end_date'] ?></div>
        </div>
        <div class="data-header-rows accommodation-cap-other-width header-row2-color">
            <div class="header-row2-col1">Weekly Availability</div>
            <div class="header-row2-col2">Bedrooms</div>
            <div class="header-row2-col3 acc-top-button">
                <input type="button" onclick="return save_all_response_quantity();" class="Save-all-btn" name="save_all" value="Save All"/>
            </div>
        </div>
        <div class="data-header-rows  header-row3-color">
            <div class="header-row3-type-name">Name</div>
            <div class="header-row3-cap">&nbsp;</div>
            <div class="header-row3-beds">1</div>
            <div class="header-row3-beds">2</div>
            <div class="header-row3-beds">3</div>
            <div class="header-row3-beds">4</div>
            <div class="header-row3-beds">5</div>
            <div class="header-row3-beds">6</div>
            <div class="header-row3-beds">7</div>
            <div class="header-row3-beds">8</div>
            <div class="header-row3-save">&nbsp;</div>
            <!-- <div class="header-row3-percent">%</div>-->
        </div>  
        <?php
    } else if ($cat_id == 29 || $cat_id == 31 || $cat_id == 45) {
        $bedrooms = 'Bedrooms';
        if ($cat_id == 4) {
            $bedrooms = 'Sites';
        } else if ($cat_id == 5) {
            $bedrooms = 'Rooms';
        } else
            $bedrooms = 'Bedrooms';
        ?>
        <div class="data-header-rows acc-padding-top-bottom content-header ccommodation-cap-heading-width header-row2-color">
            <div class="header-row2-col1"><?php echo $active_item; ?></div>
            <div class="header-row2-col2"><?php echo $bedrooms ?></div>
            <div class="header-row2-col3 acc-top-button"><input  type="button" onclick="return save_all_response_quantity();" class="Save-all-btn" name="save_all" value="Save All"/></div>
        </div>
        <div class="data-header-rows accommodation-cap-other-width header-row2-color">
            <div class="header-row2-col1"><?php
        if ($cat_id != 5) {
            echo 'Nightly Availability';
        }
        ?></div>
            <div class="header-row2-col2 extra"><?php
        $start_date = $block['start_date'];
        $end_date = $block['end_date'];
        while (strtotime($start_date) <= strtotime($end_date)) {
            echo date("m-d", strtotime($start_date)), '&nbsp&nbsp';
            $days[date("D", strtotime($start_date))] = date("D", strtotime($start_date));
            $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));
        }
        ?>
            </div>
        </div>
        <div class="data-header-rows accommodation-cap-other-width header-row3-color">
            <div class="header-row3-type-name">Name</div>
            <div class="header-row3-cap">Cap</div>
            <?php
            foreach ($days as $key => $value) {
                echo "<div class='header-row3-beds'>$value</div>";
            }
            ?>
            <div class="header-row3-save">&nbsp;</div>
            <div class="header-row3-percent">%</div>
        </div>
        <?php
    }
    return $days;
}

function pagination($query, $page, $limit) {
    /*
      Place code to connect to your DB here.
     */
    $tbl_name = "acc_accommodator";  //your table name
// How many adjacent pages should be shown on each side?
    $adjacents = 3;
    /*
      First get total number of rows in data table.
      If you have a WHERE clause in your query, make sure you mirror it here.
     */
    $total_pages = mysql_num_rows(mysql_query($query));
    /* Setup vars for query. */
    $targetpage = "accommodator_capacity.php";  //your file name  (the name of this file)
    if ($page)
        $start = ($page - 1) * $limit;    //first item to display on this page
    else
        $start = 0;        //if no page var is given, set start to 0
    /* Get data. */
    $sql = $query . " LIMIT $start, $limit";
    $result = mysql_query($sql);
    /* Setup page vars for display. */
    if ($page == 0)
        $page = 1;     //if no page var is given, default to 1.
    $prev = $page - 1;       //previous page is page - 1
    $next = $page + 1;       //next page is page + 1
    $lastpage = ceil($total_pages / $limit);  //lastpage is = total pages / items per page, rounded up.
    $lpm1 = $lastpage - 1;      //last page minus 1
    /*
      Now we apply our rules and draw the pagination object.
      We're actually saving the code to a variable in case we want to draw it more than once.
     */
    $pagination = "";
    if ($lastpage > 1) {
        $pagination .= "<div class=\"pagination\">";
        //previous button
        if ($page > 1)
            $pagination.= "<a href=\"$targetpage?page=$prev\">previous</a>";
        else
            $pagination.= "<span class=\"disabled\">previous</span>";
        //pages	
        if ($lastpage < 7 + ($adjacents * 2)) { //not enough pages to bother breaking it up
            for ($counter = 1; $counter <= $lastpage; $counter++) {
                if ($counter == $page)
                    $pagination.= "<span class=\"current\">$counter</span>";
                else
                    $pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";
            }
        }
        elseif ($lastpage > 5 + ($adjacents * 2)) { //enough pages to hide some
            //close to beginning; only hide later pages
            if ($page < 1 + ($adjacents * 2)) {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";
                }
                $pagination.= "...";
                $pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
                $pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";
            }
            //in middle; hide some front and some back
            elseif ($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                $pagination.= "<a href=\"$targetpage?page=1\">1</a>";
                $pagination.= "<a href=\"$targetpage?page=2\">2</a>";
                $pagination.= "...";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";
                }
                $pagination.= "...";
                $pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
                $pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";
            }
            //close to end; only hide early pages
            else {
                $pagination.= "<a href=\"$targetpage?page=1\">1</a>";
                $pagination.= "<a href=\"$targetpage?page=2\">2</a>";
                $pagination.= "...";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";
                }
            }
        }
        //next button
        if ($page < $counter - 1)
            $pagination.= "<a href=\"$targetpage?page=$next\">next</a>";
        else
            $pagination.= "<span class=\"disabled\">next</span>";
        $pagination.= "</div>\n";
    }
    $records_found = '';
    while ($row = mysql_fetch_array($result)) {
        $records_found[] = $row;
    }
    $return['found'] = $records_found;
    $return['pagination'] = $pagination;
    return $return;
}

/*
 * When new block is created, set default availability of listings to their capacity
 */
function set_default_availibility($block_id) {
  $sql_accommodations = "SELECT DISTINCTROW tbl_Business_Listing.BL_ID, tbl_Business_Listing_Category.BLC_C_ID
                    FROM tbl_Business_Listing
                    LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                    WHERE hide_show_listing=1 AND BL_Show_In_Accommodation = 1 AND BLC_C_ID IN (29,30,31,59,45)";
  $res_accommodations = mysql_query($sql_accommodations);
  while($accommodations = mysql_fetch_assoc($res_accommodations)) {
    $sql_capacity = "SELECT * FROM acc_accommodator_capacity WHERE accommodator_id = ". $accommodations['BL_ID'];
    $res_capacity = mysql_query($sql_capacity);
    $capacity = mysql_fetch_assoc($res_capacity);
    //update default capacity
    $sql_default = "INSERT INTO acc_capacity_response (block_id, accommodator_id, bed_1, bed_2, bed_3, bed_4, bed_5, bed_6, bed_7, bed_8, sat, sun, mon, tue, wed, thur, fri)
                  VALUES (". $block_id .", ". $accommodations['BL_ID'] .", ". $capacity['1x1'] .", ". $capacity['2x2'] .", ". $capacity['3x3'] .", ". $capacity['4x4'] .", ". $capacity['5x5'] .", ". $capacity['6x6'] .", ". $capacity['7x7'] .", ". $capacity['8x8'] .", ". $capacity['general'] .", ". $capacity['general'] .", ". $capacity['general'] .", ". $capacity['general'] .", ". $capacity['general'] .", ". $capacity['general'] .", ". $capacity['general'] .")";
    mysql_query($sql_default);
  }
}
?>
