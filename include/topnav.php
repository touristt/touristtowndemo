<div class="header">
    <div class="header-inner">
        <div class="logo">
            <a href="/admin/"><img src="/images/TouristTown-Logo.gif" alt="Tourist Town" width="195" height="43" hspace="20" vspace="20" border="0" />
            </a>
        </div>
        <div class="info">
            <?PHP
            if ($_SESSION['USER_ID']) {
                echo 'Welcome ' . $_SESSION['user_online'] . ' <a href="/admin/?logop=logout">Logout</a>';
            } else {
                echo '&nbsp;';
            }
            ?>
        </div>
    </div>
</div>

<div class="main-nav-admin">
    <div class="main-nav-admin-inner">
        <ul>
            <?PHP
            if ($_SESSION['USER_ID']) {
                ?>
                <li><a href="/admin/">Dashboard</a></li>
                <?PHP if (in_array('categories', $_SESSION['USER_PERMISSIONS']) || in_array('manage-listing-points', $_SESSION['USER_PERMISSIONS']) || in_array('manage-cart-items', $_SESSION['USER_PERMISSIONS']) || in_array('ammenity-icons', $_SESSION['USER_PERMISSIONS']) || in_array('business-listings', $_SESSION['USER_PERMISSIONS']) || in_array('free-listings', $_SESSION['USER_PERMISSIONS']) || (in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS']) && $_SESSION['USER_SHOW_BUSINESSES'] == 1) || in_array('superadmin', $_SESSION['USER_ROLES']) || in_array('county', $_SESSION['USER_ROLES'])) { ?>
                    <li><a href="#">Listings</a>
                        <ul class="first">
                            <?PHP
                            if (in_array('categories', $_SESSION['USER_PERMISSIONS'])) {
                                ?>
                                <li><a href="/admin/categories.php">Categories</a></li>
                                <?PHP
                            }if (in_array('town', $_SESSION['USER_ROLES'])) {
                                if ($_SESSION['USER_SHOW_BUSINESSES'] == 1) {
                                    if (in_array('business-listings', $_SESSION['USER_PERMISSIONS']) || in_array('free-listings', $_SESSION['USER_PERMISSIONS']) || in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) {
                                        ?>
                                        <li>
                                            <a href="/admin/listings.php">Listings</a>
                                        </li>    
                                        <?php
                                    }
                                }
                            } else {
                                if (in_array('business-listings', $_SESSION['USER_PERMISSIONS']) || in_array('free-listings', $_SESSION['USER_PERMISSIONS']) || in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) {
                                    ?>
                                    <li>
                                        <a href="/admin/listings.php">Listings</a>
                                    </li>    
                                    <?php
                                }
                            }
                            ?>
                            <?PHP if (in_array('superadmin', $_SESSION['USER_ROLES'])) { ?>
                                <li><a href="/admin/active-advertisement-stats.php">Campaign</a></li>
                                <li><a href="/admin/active-listings-coupon.php">Coupons</a></li>
                            <?PHP } if (in_array('county', $_SESSION['USER_ROLES'])) { ?>
                                <li><a href="/admin/customer-advertisement-county.php">Campaign</a></li>  
                                <?PHP
                            }
                            ?>
                            <?PHP
                            if (in_array('manage-cart-items', $_SESSION['USER_PERMISSIONS'])) {
                                ?>
                                <li><a href="/admin/store-list.php">Store</a></li>
                            <?PHP } if (in_array('manage-listing-points', $_SESSION['USER_PERMISSIONS'])) { ?>
                                <li><a href="/admin/points_list.php?pc_id=1">Listing Points</a></li>
                            <?php } if (in_array('town', $_SESSION['USER_ROLES']) && $_SESSION['USER_SHOW_BUSINESSES'] == 0) { ?>  
                                <li><a href="/admin/request-change-pending.php">Change Requests</a></li>  
                            <?php } ?>    
                        </ul>
                    </li>
                <?php } ?>
                <?PHP if (in_array('superadmin', $_SESSION['USER_ROLES']) || in_array('image-bank', $_SESSION['USER_PERMISSIONS'])) { ?>
                    <li><a href="#">Images</a>
                        <ul class="first">
                            <li><a href="/admin/image-bank.php">Image Bank</a>
                                <ul class="last">
                                    <li><a href="/admin/pending-image-bank.php">Pending Images</a></li>
                                    <?php if (!in_array('county', $_SESSION['USER_ROLES'])) { ?>
                                        <li><a href="/admin/owner.php">Manage Owner</a></li>
                                        <li><a href="/admin/photographer.php">Manage Photographer</a></li>
                                    <?php } ?>
                                </ul>
                            </li>
                        </ul>
                    </li>
                <?PHP } ?>
                <?PHP if (in_array('manage-support', $_SESSION['USER_PERMISSIONS'])) { ?>
                    <li><a href="#">Support</a>
                        <ul class="first">
                            <li>
                                <a href="/admin/help_text.php?">Help Text</a>
                            </li>
                            <li><a href="/admin/support-list.php">Support</a></li>
                        </ul>
                    </li>
                <?php } ?>
                <?PHP if (in_array('manage-users', $_SESSION['USER_PERMISSIONS']) || in_array('manage-accommodation', $_SESSION['USER_PERMISSIONS']) || in_array('manage-accounting', $_SESSION['USER_PERMISSIONS']) || in_array('division-billing', $_SESSION['USER_PERMISSIONS']) || in_array('manage-events', $_SESSION['USER_PERMISSIONS']) || in_array('superadmin', $_SESSION['USER_ROLES']) || in_array('regions', $_SESSION['USER_PERMISSIONS'])) { ?>
                    <li><a href="#">Admin</a>
                        <ul class="first">
                            <?PHP if (in_array('manage-users', $_SESSION['USER_PERMISSIONS'])) { ?>
                                <li><a href="/admin/users.php">Users</a></li>
                            <?PHP } if (in_array('regions', $_SESSION['USER_PERMISSIONS'])) { ?>
                                <li><a href="/admin/domains.php">Domains</a></li>   
                            <?PHP }if (in_array('regions', $_SESSION['USER_PERMISSIONS'])) { ?>
                                <li><a href="/admin/regions.php">Websites</a></li>
                                <li><a href="/admin/communities.php">Communities</a></li>
                            <?PHP } if (in_array('superadmin', $_SESSION['USER_ROLES'])) { ?>
                                <li><a href="/admin/client-contacts.php">Client Contacts</a></li>
                            <?php } if (in_array('manage-events', $_SESSION['USER_PERMISSIONS'])) { ?>
                                <li><a href="/admin/events.php">Events</a></li>
                            <?php } if (in_array('division-billing', $_SESSION['USER_PERMISSIONS'])) { ?>
                                <li><a href="/admin/division-billing.php">Division Billing</a></li>
                            <?php } if (in_array('superadmin', $_SESSION['USER_ROLES'])) { ?>
                                <li><a href="/admin/seasons.php">Seasons</a></li>
                            <?php } ?>
                            <?PHP if (in_array('manage-accounting', $_SESSION['USER_PERMISSIONS'])) { ?>
                                <li><a href="/admin/accounting.php">Accounting</a></li>
                                <li><a href="/admin/sales_stats.php">Sales Statistics</a></li>
                            <?php } if (in_array('manage-accommodation', $_SESSION['USER_PERMISSIONS'])) { ?>
                                <li><a href="/admin/group_blocks.php">Accommodation</a></li>    
                            <?php } if (in_array('manage-users', $_SESSION['USER_PERMISSIONS']) && in_array('superadmin', $_SESSION['USER_ROLES'])) { ?>
                                <li><a href="/admin/email.php">Email</a></li>   
                            <?php } if (in_array('superadmin', $_SESSION['USER_ROLES'])) { ?>
                                <li><a href="/admin/request-change-pending.php">Change Requests</a></li>  
                            <?PHP } if (in_array('regions', $_SESSION['USER_PERMISSIONS'])) { ?>
                                <li ><a href="/admin/mobile_layout.php">Mobile</a></li>
                            <?php } ?>  
                            <li ><a href="/admin/db_scripts.php">DB Scripts</a></li>
                        </ul>
                    </li>
                <?php }if (in_array('manage-county-region', $_SESSION['USER_PERMISSIONS'])) { ?>
                    <li><a href="/admin/county-region.php?rid=<?php echo $_SESSION['USER_LIMIT']; ?>">Manage Website</a></li>
                <?PHP } if (in_array('superadmin', $_SESSION['USER_ROLES']) || in_array('county', $_SESSION['USER_ROLES']) || (in_array('admin', $_SESSION['USER_ROLES']) && in_array('manage-stories', $_SESSION['USER_PERMISSIONS']))) { ?>
                    <li><a href="/admin/stories.php">Stories</a></li>
                    <?php
                }
            } else {
                echo "&nbsp;";
            }
            ?>
        </ul>
    </div>
</div>