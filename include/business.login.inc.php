<?php

//check to pass mobile users to mobile version
$useragent = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))){        
    if (isset($_REQUEST['con']) && $_REQUEST['con'] != "") {
        $con = 'con=' . $_REQUEST['con'] . '&BL_ID=' . $_REQUEST['BL_ID'];
    }else{
        $con = '/';
    }
    header('Location: http://my.touristtowndemo.com/mobile?' . $con);
    exit;
}
define('SESSIONERROR', 'BUSINESS_ERROR');
define('SESSIONINFO', 'BUSINESS_INFO');

$sql_select = "SELECT B_ID, B_Name, B_Email FROM tbl_Business ";

if (isset($_REQUEST['con']) && $_REQUEST['con'] != "") {
    $freeListing = "SELECT BL_Free_Listing_Status_Hash FROM tbl_Business_Listing WHERE BL_ID = '" . $_REQUEST['BL_ID'] . "' AND BL_Free_Listing_status = 1";
    $freeRes = mysql_query($freeListing);
    $freeRow = mysql_fetch_assoc($freeRes);
    if ($_REQUEST['con'] == $freeRow['BL_Free_Listing_Status_Hash']) {
        $sql_update_hash = "DELETE tbl_Business_Listing, tbl_Business_Listing_Ammenity, 
                            tbl_Business_Listing_Category, tbl_Business_Listing_Category_Region,
                            tbl_Business_Social, tbl_BL_Feature, payment_profiles,
                            tbl_Business_Feature_About, tbl_Business_Feature_About_Photo,
                            tbl_Business_Feature_About_Main_Photo, tbl_Business_Feature_Daily_Specials,
                            tbl_Business_Feature_Daily_Specials_Description, tbl_Business_Feature_Entertainment_Acts,
                            tbl_Business_Feature_Entertainment_Description, tbl_Business_Feature_Guest_Book, tbl_Business_Feature_Guest_Book_Description,
                            tbl_Business_Feature_Menu, tbl_Business_Feature_Photo, tbl_Business_Feature_Product, tbl_Business_Feature_Product_Photo,
                            tbl_Business_Feature_Coupon, tbl_Business_Feature_Coupon_Category_Multiple, tbl_Image_Bank_Usage
                            FROM tbl_Business_Listing 
                            LEFT JOIN tbl_Business_Listing_Ammenity ON BLA_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Listing_Category_Region ON BLCR_BLC_ID = BLC_ID 
                            LEFT JOIN tbl_Business_Social ON BS_BL_ID = BL_ID
                            LEFT JOIN tbl_BL_Feature ON BLF_BL_ID = BL_ID
                            LEFT JOIN payment_profiles ON listing_id = BL_ID
                            LEFT JOIN tbl_Business_Feature_About ON BFA_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_About_Photo ON BFAP_BFA_ID = BFA_ID
                            LEFT JOIN tbl_Business_Feature_About_Main_Photo ON BFAMP_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Daily_Specials ON BFDS_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Daily_Specials_Description ON BFDSD_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Entertainment_Acts ON BFEA_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Entertainment_Description ON BFED_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Guest_Book ON BFGB_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Guest_Book_Description ON tbl_Business_Feature_Guest_Book_Description.BFGB_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Menu ON BFM_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Photo ON BFP_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Product ON tbl_Business_Feature_Product.BFP_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Product_Photo ON BFPP_BFP_ID = tbl_Business_Feature_Product.BFP_ID
                            LEFT JOIN tbl_Business_Feature_Coupon ON tbl_Business_Feature_Coupon.BFC_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
                            LEFT JOIN tbl_Image_Bank_Usage ON IBU_BL_ID = BL_ID
                            WHERE BL_ID = '" . encode_strings($_REQUEST['BL_ID'], $db) . "'";
        $result_hash = mysql_query($sql_update_hash);
        $_SESSION['update_hash_success'] = 1;
    } else {
        $_SESSION['update_hash_error'] = 1;
    }
}

if ($_REQUEST['logop'] == "login") {
    $sql_log = $sql_select
            . "WHERE B_Email 	= '" . encode_strings($_POST['LOGIN_USER'], $db) . "' "
            . "&& B_Password = '" . md5(encode_strings($_POST['LOGIN_PASS'], $db)) . "' ";
    $result_log = mysql_query($sql_log, $db) or die("Invalid query: $sql_log -- " . mysql_error());
    if (mysql_num_rows($result_log) <> 1) {
        $_SESSION['login_error'] = 1;
        header("Location: " . BUSINESS_DIR . "login.php");
        exit();
    } else {
        $row = mysql_fetch_assoc($result_log);
        $sql_Listing = "SELECT BL_ID FROM tbl_Business_Listing WHERE BL_B_ID = " . $row['B_ID'];
        $sql_Listing_Result = mysql_query($sql_Listing, $db) or die("Invalid query: $sql_Listing -- " . mysql_error());
        $sql_Free_Listing = "SELECT BL_ID FROM tbl_Business_Listing WHERE BL_B_ID = " . $row['B_ID'] . " AND BL_Listing_Type = 1";
        $sql_Free_Listing_Result = mysql_query($sql_Free_Listing, $db) or die("Invalid query: $sql_Free_Listing -- " . mysql_error());
        if (mysql_num_rows($sql_Listing_Result) == mysql_num_rows($sql_Free_Listing_Result)) {
            $sql_Free_Listing_Unverified = "SELECT BL_ID FROM tbl_Business_Listing WHERE BL_B_ID = " . $row['B_ID'] . " AND BL_Listing_Type = 1 AND BL_Free_Listing_status = 1";
            $sql_Free_Listing_Unverified_Result = mysql_query($sql_Free_Listing_Unverified, $db) or die("Invalid query: $sql_Free_Listing_Unverified -- " . mysql_error());
            if (mysql_num_rows($sql_Free_Listing_Result) == mysql_num_rows($sql_Free_Listing_Unverified_Result)) {
                $sql_Free_Listing_Unverified_Date = "SELECT BL_ID FROM tbl_Business_Listing WHERE BL_B_ID = " . $row['B_ID'] . " AND BL_Listing_Type = 1 
                                                    AND BL_Free_Listing_status = 1 AND CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)";
                $sql_Free_Listing_Verify_Unverified_Date_Result = mysql_query($sql_Free_Listing_Unverified_Date, $db) or die("Invalid query: $sql_Free_Listing_Unverified_Date -- " . mysql_error());
                if (mysql_num_rows($sql_Free_Listing_Verify_Unverified_Date_Result) < 1) {
                    $_SESSION['login_error'] = 2;
                    header("Location: " . BUSINESS_DIR . "login.php");
                    exit();
                }
            }
        }
        $BID = UpdateUserLog($row, $db);
        if ($_REQUEST['updatecc'] == 1) {
            header("Location: " . BUSINESS_DIR . "customer-credit-card-details.php");
            exit();
        }
    }
} elseif ($_REQUEST['logop'] == 'logout') {
    $sql = "UPDATE tbl_Business SET B_Expiry = '0000-00-00' "
            . "WHERE B_Key = '" . $_SESSION['BUSINESS_USER_KEY'] . "' ";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $_SESSION['BUSINESS_USER_KEY'] = 0;
    $_SESSION['BUSINESS_ID'] = 0;
    header("Location: " . BUSINESS_DIR);
    exit();
} elseif ($_REQUEST['logop'] == "lost_pw") {
    header("Location: " . BUSINESS_DIR . "lost-password.php");
    exit();
} elseif ($_REQUEST['logop'] == "send_pw") {
    if ($_POST[USER_EMAIL] != '') {
        $result_pw = mysql_query("SELECT B_ID, B_Email FROM tbl_Business LEFT JOIN tbl_Business_Listing ON B_ID = BL_B_ID WHERE B_Email = '"
                . encode_strings($_POST['USER_EMAIL'], $db) . "' LIMIT 1", $db);
        $num_row = mysql_num_rows($result_pw);
        if ($num_row == 0) {
            $_SESSION['send_password_error'] = 1;
            header("Location: " . BUSINESS_DIR . "send-password.php");
            exit();
        } elseif ($num_row == 1) {
            $row_pw = mysql_fetch_assoc($result_pw);
            $_SESSION['B_ID'] = $row_pw['B_ID'];
            $_SESSION['B_Email'] = $row_pw['B_Email'];
            $_SESSION['send_password_success'] = 1;
            header("Location: " . BUSINESS_DIR . "send-password.php");
            exit();
        } else {
            $_SESSION['send_password_error'] = 2;
            header("Location: " . BUSINESS_DIR . "send-password.php");
            exit();
        }
    } else {
        header("Location: " . BUSINESS_DIR);
        exit();
    }
} elseif (isset($_SESSION['BUSINESS_USER_KEY']) && $_SESSION['BUSINESS_USER_KEY'] != '0' && $_SESSION['BUSINESS_USER_KEY'] != '') {
    $sql_Check = $sql_select
            . "WHERE B_Key = '" . encode_strings($_SESSION['BUSINESS_USER_KEY'], $db) . "' "
            . "&& B_Expiry > NOW() ";
    $result_Check = mysql_query($sql_Check, $db) or die("Invalid query: $sql_Check -- " . mysql_error());
    if (mysql_num_rows($result_Check) <> 1) {
        my_session_destroy('member');
        header("Location: " . BUSINESS_DIR . "login.php");
        exit();
    } else {
        $row = mysql_fetch_assoc($result_Check);
        $BID = UpdateUserLog($row, $db, $_SESSION['BUSINESS_USER_KEY']);
    }
} elseif ($_REQUEST['logop'] == 'rg_p' && $_REQUEST['token'] != '') {
    $hash_code = $_REQUEST['token'];
    $sql_hash = "SELECT B_ID FROM tbl_Business WHERE B_hash = '" . $hash_code . "'";
    $result_hash = mysql_query($sql_hash);
    $row_hash = mysql_num_rows($result_hash);
    if ($row_hash > 0) {
        $_SESSION['hash_code'] = $hash_code;
        header("Location: " . BUSINESS_DIR . "change-password.php");
        exit();
    } else {
        $_SESSION['token_expired'] = 1;
        header("Location: " . BUSINESS_DIR . "login.php");
        exit();
    }
} elseif ($_REQUEST['logop'] == 'change_password') {
    $hash_code = $_REQUEST['regain_pass'];
    if ($_REQUEST['new_password'] == $_REQUEST['confirm_password']) {
        $sql_pass = "UPDATE tbl_Business SET
                     B_Password='" . md5($_REQUEST['new_password']) . "', B_hash='' WHERE B_hash='" . $_REQUEST['regain_pass'] . "'";
        $reslt_pass = mysql_query($sql_pass) or die(mysql_error());
        if ($reslt_pass) {
            $_SESSION['password_changed'] = 1;
            header("Location: " . BUSINESS_DIR . "login.php");
            exit();
        } else {
            $_SESSION['hash_code'] = $hash_code;
            $_SESSION['error_regain_password'] = 1;
            header("Location: " . BUSINESS_DIR . "change-password.php");
            exit();
        }
    } else {
        $_SESSION['hash_code'] = $hash_code;
        $_SESSION['mismatch_regain_password'] = 1;
        header("Location: " . BUSINESS_DIR . "change-password.php");
        exit();
    }
} elseif ($_REQUEST['logop'] == 'unsubscribe') {
    $sql = "UPDATE tbl_Business SET B_Subscription = '0' WHERE B_ID = '" . $_REQUEST['key'] . "'";
    $reslt = mysql_query($sql) or die(mysql_error());
    $_SESSION['unsubscribed'] = 1;
    header("Location: " . BUSINESS_DIR . "login.php");
    exit();
} else {
    my_session_destroy('member');
    header("Location: " . BUSINESS_DIR . "login.php");
    exit();
}

$sql = "SELECT B_Agreement_Accepted FROM tbl_Business WHERE B_ID = '" . encode_strings($_SESSION['BUSINESS_ID'], $db) . "' ";
$result = mysql_query($sql, $db);
$rowBIZ = mysql_fetch_assoc($result);
if ($_POST['agreementOP'] == 'save') {
    if ($_POST['agreed']) {
        $sql = "UPDATE tbl_Business SET B_Agreement_Accepted = '1', B_Agreement_Accepted_Date = NOW() 
                WHERE B_ID = '" . encode_strings($_SESSION['BUSINESS_ID'], $db) . "' LIMIT 1 ";
        mysql_query($sql, $db);
    } else {
        header("Location: " . BUSINESS_DIR);
        exit();
    }
} elseif (!$rowBIZ['B_Agreement_Accepted'] && $bypass == false) {
    $sign = true;
    require 'agreement.php';
    exit();
}

function UpdateUserLog($row, $db, $key = "0") {
    if ($row) {
        $check_listings = array();
        $sql_check_listings = "SELECT BL_Listing_Type FROM tbl_Business_Listing where BL_B_ID='" . $row['B_ID'] . "'";
        $result_check_listings = mysql_query($sql_check_listings);
        while ($row_check_listings = mysql_fetch_assoc($result_check_listings)) {
            $check_listings[] = $row_check_listings['BL_Listing_Type'];
        }
        $sql = "UPDATE tbl_Business SET B_Expiry = DATE_ADD(NOW(), "
                . "INTERVAL 1 HOUR)";
        if ($key == "0") {
            $key = mt_rand(1000, 9999999);
            $sql .= ", B_Key = '$key'";
            $_SESSION['BUSINESS_USER_KEY'] = $key;
            $_SESSION['BUSINESS_ID'] = $row['B_ID'];
            $_SESSION['BUSINESS_NAME'] = $row['B_Name'];
            $_SESSION['BUSINESS_USER_EM'] = $row['B_Email'];
        }
        $sql .= " WHERE B_ID = '$row[B_ID]' ";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    } else {
        header("Location: " . BUSINESS_DIR);
        exit();
    }
    return $row['B_ID'];
}

?>