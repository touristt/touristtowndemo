<?php
define("DOMAIN", 'touristtowndemo.com');
if ($title == '') {
    $title = COMPANY_NAME . "";
}
$listingCheck = mysql_query("SELECT BL_ID FROM tbl_Business_Listing WHERE BL_B_ID = '" . encode_strings($_SESSION['BUSINESS_ID'], $db) . "'");
$listingsArray = array();
while ($listingsRow = mysql_fetch_array($listingCheck)) {
    $listingsArray[] = $listingsRow['BL_ID'];
}
if (isset($_REQUEST['bl_id']) && !in_array($_REQUEST['bl_id'], $listingsArray)) {
    header("location:/mobile/index.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8">
            <title><?PHP echo $title; ?></title>
            <link href="http://<?php echo DOMAIN ?>/my/mobile/css/responsive.css" rel="stylesheet" type="text/css" />
            <link href="http://<?php echo DOMAIN ?>/my/mobile/css/slicknav.css" rel="stylesheet" type="text/css" />
            <link href="http://<?php echo DOMAIN ?>/include/jquery-ui.css" rel="stylesheet" type="text/css" />
            <link href="http://<?php echo DOMAIN ?>/include/plugins/sweetalert/sweetalert.css"  rel="stylesheet" type="text/css"/> 
            <link href="http://<?php echo DOMAIN ?>/include/token-input.css"  rel="stylesheet" type="text/css"/>

            <script src="http://<?php echo DOMAIN ?>/include/jquery.min.js" type="text/javascript"></script>
            <script src="http://<?php echo DOMAIN ?>/include/jquery-ui.min.js" type="text/javascript"></script>
            <script src="http://<?php echo DOMAIN ?>/include/jquery-1.10.2.js" type="text/javascript"></script>
            <script src="http://<?php echo DOMAIN ?>/include/jquery-ui.js" type="text/javascript"></script>
            <script src="http://<?php echo DOMAIN ?>/include/plugins/sweetalert/sweetalert.min.js" type="text/javascript"></script>
            <script src="http://<?php echo DOMAIN ?>/include/ckeditor/ckeditor.js"></script>
            <script src="http://<?php echo DOMAIN ?>/include/ckeditor/jquery.js"></script>
            <script src="http://<?php echo DOMAIN ?>/include/jquery.tokeninput.js"></script>
            <script src="http://<?php echo DOMAIN ?>/include/jquery.form.min.js"></script>
            <script src="http://<?php echo DOMAIN ?>/my/mobile/js/jquery.slicknav.js" type="text/javascript"></script>
            <script src="http://<?php echo DOMAIN ?>/my/mobile/js/jquery.slicknav.min.js" type="text/javascript"></script>
            <script src="http://<?php echo DOMAIN ?>/include/jquery.tablesorter.min.js" type="text/javascript"></script>
            <script src="http://<?php echo DOMAIN ?>/include/jquery.metadata.js" type="text/javascript"></script>
            <script src="http://<?php echo DOMAIN ?>/include/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
            <script src="/include/maskedinput.js"></script>
            <script src="/include/admin.js" type="text/javascript"></script>
            <?PHP
            $sql_responsive_bg = "SELECT * from tbl_Mobile_Layout";
            $result_responsive_bg = mysql_query($sql_responsive_bg, $db) or die("Invalid query: $sql_responsive_bg -- " . mysql_error());
            $row_responsive_bg = mysql_fetch_assoc($result_responsive_bg);
            ?>
            <style>
                .login-outer-body
                {
                    background: url('http://<?php echo DOMAIN ?>/images/DB/<?php echo $row_responsive_bg['ML_Photo_Name']; ?>') no-repeat;
                    background-size: 100% 100%;
                }
            </style>
            <?PHP
            if (count($js) > 0) {
                foreach ($js as $val) {
                    echo "<script type=\"text/javascript\" src=\"/include/" . $val . "\"></script>\n";
                }
            }
            ?>

            <body onload="<?PHP echo $onload; ?>">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                    <div class="main-wrapper">
                        <?php
                        require_once '../../include/topnav-badmin-mobile.php';
                        ?>
                        <div class="content-wrapper">
                            <div class="content">

                                <?PHP
                                if ($_GET['info'] <> '') {
                                    echo "<p class='info'>" . stripslashes(urldecode($_GET['info'])) . "</p>";
                                }
                                if ($_GET['err'] <> '') {
                                    echo "<p class='error'>" . stripslashes(urldecode($_GET['err'])) . "</p>";
                                }
                                if ($_SESSION['BUSINESS_INFO'] <> '') {
                                    echo "<p class='info'>" . stripslashes(urldecode($_SESSION['BUSINESS_INFO'])) . "</p>";
                                    $_SESSION['BUSINESS_INFO'] = "";
                                }
                                if ($_SESSION['BUSINESS_ERROR'] <> '') {
                                    echo "<p class='error'>" . stripslashes(urldecode($_SESSION['BUSINESS_ERROR'])) . "</p>";
                                    $_SESSION['BUSINESS_ERROR'] = "";
                                }
                                ?>
                            