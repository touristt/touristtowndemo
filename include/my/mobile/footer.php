<?PHP
if ($_SESSION['success'] == 1) {
    print '<script>swal("Data Saved", "Data has been saved successfully.", "success");</script>';
    unset($_SESSION['success']);
}
if ($_SESSION['error'] == 1) {
    print '<script>swal("Error", "Error saving data. Please try again.", "error");</script>';
    unset($_SESSION['error']);
}
if ($_SESSION['delete'] == 1) {
    print '<script>swal("Data Deleted", "Data has been deleted successfully.", "success");</script>';
    unset($_SESSION['delete']);
}
if ($_SESSION['delete_error'] == 1) {
    print '<script>swal("Error", "Error deleting data. Please try again.", "error");</script>';
    unset($_SESSION['delete_error']);
}
if ($_SESSION['wrong_cc_details'] == 1) {
    print '<script>swal("Error", "We were unable to process your credit card. Please carefully review your credit card information and try again.", "error");</script>';
    unset($_SESSION['wrong_cc_details']);
}
if ($_SESSION['WARNING_LOGIN_DETAILS'] == 1) {
    print '<script>sweetAlert("Email already exists.", "Please use another email address for this account!", "error");</script>';
    unset($_SESSION['WARNING_LOGIN_DETAILS']);
}
?>
</div>
</div>
<div class="custom-ajax">Loading...</div>
</div>
</body>
</html>
<?php
mysql_close($db);
?>