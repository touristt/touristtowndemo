<?php
if (isset($_SESSION['success']) && $_SESSION['success'] == 1) {
    print '<script>swal("Data Saved", "Data has been saved successfully.", "success");</script>';
    unset($_SESSION['success']);
}
if ($_SESSION['success'] == 2) {
    print '<script>swal("", "Your request has been sent.", "success");</script>';
    unset($_SESSION['success']);
}
if (isset($_SESSION['error']) && $_SESSION['error'] == 1) {
    print '<script>swal("Error", "Error saving data. Please try again.", "error");</script>';
    unset($_SESSION['error']);
}
if (isset($_SESSION['delete']) && $_SESSION['delete'] == 1) {
    print '<script>swal("Data Deleted", "Data has been deleted successfully.", "success");</script>';
    unset($_SESSION['delete']);
}
if (isset($_SESSION['delete_error']) && $_SESSION['delete_error'] == 1) {
    print '<script>swal("Error", "Error deleting data. Please try again.", "error");</script>';
    unset($_SESSION['delete_error']);
}
if (isset($_SESSION['buy_ad_success']) && $_SESSION['buy_ad_success'] == 1) {
    print '<script>swal("Data Saved", "The Tourist Town team will now create your advertisement. Once your advertisement is created you will receive an email to : ' . $emailBUS . '", "success");</script>';
    unset($_SESSION['buy_ad_success']);
}
if(isset($_SESSION['email_not_correct']) && $_SESSION['email_not_correct'] == 1){
    print '<script>swal("Error", "Email address not correct.", "error");</script>';
    unset($_SESSION['email_not_correct']);
}
if ($_SESSION['WARNING_LOGIN_DETAILS'] == 1) {
    print '<script>sweetAlert("Email already exists.", "Please use another email address for this account!", "error");</script>';
    unset($_SESSION['WARNING_LOGIN_DETAILS']);
}
if (isset($_SESSION['transaction_error']) && $_SESSION['transaction_error'] == 1) {
    print '<script>swal("Transaction Error", "An error has been occur due to: ' . $_SESSION['transaction_msg'] . '.", "error");</script>';
    unset($_SESSION['transaction_error']);
    unset($_SESSION['transaction_msg']);
}
if (isset($_SESSION['WARNING']) && $_SESSION['WARNING'] == 1) {
    echo '<script>
            swal({
                    title: "Credit Card Info",   
                    text: "Please provide credit card info before buying Add On!",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#6dac29",   
                    confirmButtonText: "OK",   
                    closeOnConfirm: false 
                }, function(){   
                    window.location.href="customer-credit-card-details.php" 
                });
          </script>';
    unset($_SESSION['WARNING']);
}
if (isset($_SESSION['addon_activation_error']) && $_SESSION['addon_activation_error'] == 1) {
    echo '<script>
        swal({
                    title: "Add On",   
                    text: "Can not deactivate/activate the Add On before 30 days!",   
                    type: "warning",   
                    showCancelButton: false,   
                    confirmButtonColor: "#6dac29",   
                    confirmButtonText: "OK",   
                    closeOnConfirm: false 
                }, function(){   
                    window.location.href="customer-feature-add-ons.php?bl_id=' . $BL_ID . '" 
                });
          </script>';
    unset($_SESSION['addon_activation_error']);
}
if (isset($_SESSION['addon']) && ($_SESSION['addon'] == 'deleted1' || $_SESSION['addon'] == 'deleted0')) {
    echo '<script>swal("", "Your selected Add on has been '.(($_SESSION['addon'] == 'deleted1') ? "activated" : "deactivated").' successfully.", "success");</script>';
    unset($_SESSION['addon']);
}
if ($_SESSION['credit_card_info_error'] == 1) {
    print '<script>swal("Error", "We were unable to process your credit card. Please carefully review your credit card information and try again.", "error");</script>';
    unset($_SESSION['credit_card_info_error']);
}
//about us. delete it.
if ($_SESSION['file_required'] == 1) {
    print '<script>swal("PDF!", "File Required!", "warning");</script>';
    unset($_SESSION['file_required']);
}
if ($_SESSION['file_required_update'] == 1) {
    print '<script>swal("PDF!", "File Required For Update!", "warning");</script>';
    unset($_SESSION['file_required_update']);
}
//
?>

</div>
</div>
<div class="custom-ajax">Loading...</div>
</div>
</body>
</html>
<?php
mysql_close($db);
?>