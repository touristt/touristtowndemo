<?php
require_once '../include/config.inc.php';

//function to calculate points
function calculate_listing_points($listing_id, $type) {
    if ($type == 'all') {
        $sql_points = "SELECT BL_Points FROM tbl_Business_Listing WHERE BL_ID = $listing_id";
        $res_points = mysql_query($sql_points);
        $points = mysql_result($res_points, 0);
    } else if ($type == 'basic') {
        $sql_points = "SELECT BL_Basic_Points FROM tbl_Business_Listing WHERE BL_ID = $listing_id";
        $res_points = mysql_query($sql_points);
        $points = mysql_result($res_points, 0);
    } else if ($type == 'addon') {
        $sql_points = "SELECT BL_Addon_Points FROM tbl_Business_Listing WHERE BL_ID = $listing_id";
        $res_points = mysql_query($sql_points);
        $points = mysql_result($res_points, 0);
    } else if ($type == 'available') {
        $total_points = calculate_total_points('basic');
        $sql_points = "SELECT BL_Basic_Points FROM tbl_Business_Listing WHERE BL_ID = $listing_id";
        $res_points = mysql_query($sql_points);
        $points = $total_points - mysql_result($res_points, 0);
    } else if ($type == 'available_addon') {
        $total_points = calculate_total_points('addon');
        $sql_points = "SELECT BL_Addon_Points FROM tbl_Business_Listing WHERE BL_ID = $listing_id";
        $res_points = mysql_query($sql_points);
        $points = $total_points - mysql_result($res_points, 0);
    }
    return $points;
}

//function to calculate points
function calculate_total_points($type) {
    if ($type == 'basic') {
        $sql_points = "SELECT SUM(points) as total_points FROM points";
        $res_points = mysql_query($sql_points);
        $total_points = mysql_result($res_points, 0);
    } else if ($type == 'addon') {
        $sql_points = "SELECT SUM(F_Points) as total_points FROM tbl_Feature WHERE F_ID NOT IN (3)";
        $res_points = mysql_query($sql_points);
        $total_points = mysql_result($res_points, 0);
    }
    return $total_points;
}

//function to calculate ranking
function calculate_ranking($listing_id, $type, $param, $region) {
    $listing_rank = array();
    // check if child or parent region
    $regionList = '';
    $sql_region = "SELECT * FROM tbl_Region WHERE R_ID = '$region'";
    $res_region = mysql_query($sql_region);
    $regionObj = mysql_fetch_assoc($res_region);
    if ($regionObj['R_Parent'] == 0) {
        $sql_child = "SELECT RM_Child FROM tbl_Region_Multiple WHERE RM_Parent = '" . $regionObj['R_ID'] . "'";
        $res_child = mysql_query($sql_child);
        $first = true;
        $regionList .= "(";
        while ($child = mysql_fetch_assoc($res_child)) {
            if ($first) {
                $first = false;
            } else {
                $regionList .= ",";
            }
            $regionList .= $child['RM_Child'];
        }
        $regionList .= ")";
    } else {
        $regionList = '(' . $region . ')';
    }
    if ($type == 'region') { 
        $sql_rank = "SELECT BL_ID, BL_Points FROM tbl_Business_Listing 
                    INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                    WHERE BLCR_BLC_R_ID IN $regionList AND hide_show_listing='1'
                    GROUP BY BL_ID
                    ORDER BY BL_Points DESC, BL_Listing_Title";
    } else if ($type == 'category') {
        $sql_rank = "SELECT BL_ID, BL_Points FROM tbl_Business_Listing 
                    LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID= BL_ID
                    INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                    WHERE BLC_M_C_ID = '$param' AND BLCR_BLC_R_ID IN $regionList  AND hide_show_listing='1'
                    GROUP BY BL_ID ORDER BY BL_Points DESC, BL_Listing_Title";
    } else if ($type == 'subcategory') {
        $sql_rank = "SELECT BL_ID, BL_Points FROM tbl_Business_Listing 
                    LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                    INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                    WHERE BLC_C_ID = '$param' AND BLCR_BLC_R_ID IN $regionList  AND hide_show_listing='1'
                    GROUP BY BL_ID
                    ORDER BY BL_Points DESC, BL_Listing_Title";
    } else if ($type == 'positions') {
        
    } 
    $res_rank = mysql_query($sql_rank); 
    $i = 1;
    if ($res_rank) { 
        $listing_rank['total'] = mysql_num_rows($res_rank);
        while ($rank = mysql_fetch_assoc($res_rank)) { 
            if ($rank['BL_ID'] == $listing_id) {
                $listing_rank['rank'] = $i;
                break;
            }
            $i++;
        }
    }
    return $listing_rank;
}

?>