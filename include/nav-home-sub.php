<?php
$filename = basename($_SERVER["SCRIPT_FILENAME"], '.php');
if ($filename == 'email' || $filename == 'archive-email' || $filename == 'email-templates') {
    ?>
    <div class="sub-menu">
        <ul>
            <li><a href="/admin/email.php">Send Email</a></li>
            <li><a href="/admin/archive-email.php">Email Archive</a></li>
            <li><a href="/admin/email-templates.php">Notification Messages</a></li>
            <li><a href="/admin/email_reciptions.php">Email Notification Recipients</a></li>
        </ul>
    </div> 
<?php } elseif ($filename == 'users' || $filename == 'permissions' || $filename == 'permission' || $filename == 'user') { ?>
    <div class="sub-menu">
        <ul>
            <li><a href="/admin/users.php">Manage Users</a></li>
            <li><a href="/admin/permissions.php">Manage Permissions</a></li>
        </ul>
    </div> 
<?php } elseif ($filename == 'email_reciptions' || $filename == 'add_email_reciption') { ?>
    <div class="sub-menu">
        <ul>
            <li><a href="/admin/email.php">Send Email</a></li>
            <li><a href="/admin/archive-email.php">Email Archive</a></li>
            <li><a href="/admin/email-templates.php">Notification Messages</a></li>
            <li><a href="/admin/email_reciptions.php">Email Notification Recipients</a></li>
        </ul>
    </div> 
<?php } elseif ($filename == 'help_text') { ?>
    <div class="sub-menu">
        <ul>
            <li><a href="help_text.php">Help Text</a></li>
            <li><a href="#">Welcome Screens & Help Documents</a></li>
        </ul>
    </div> 
<?php } elseif ($filename == 'division-billing' || $filename == 'division-billing-invoices' || $filename == 'division-billing-invoice') {
    ?>
    <div class="sub-menu">
        <ul>
            <li><a href="division-billing.php">Division Billing</a></li>
            <li><a href="division-billing-invoices.php">Invoices</a></li>
        </ul>
    </div> 
    <?php
}
?>