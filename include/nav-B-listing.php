<div class="sub-menu">
    <ul>
        <?PHP
        // Temporary hardcoding to not show Our Products add on
        $sql = "SELECT * FROM tbl_Feature WHERE F_ID NOT IN (3)";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            $sql1 = "SELECT * FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = " . $row['F_ID'];
            $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
            $row1 = mysql_fetch_assoc($result1);
            if ($row1['BLF_BL_ID'] != '') {
                //About Us
                if ($row['F_Name'] == 'About Us') {
                    $sqlAbt = "SELECT * FROM tbl_Business_Feature_About WHERE BFA_BL_ID = '$BL_ID' ORDER BY BFA_Order";
                    $resultAbt = mysql_query($sqlAbt) or die(mysql_error());
                    $numRowsAbt = mysql_num_rows($resultAbt);
                    if ($numRowsAbt > 0 && $row1['BLF_Active'] == "1") {
                        $class = 'points-com';
                    } else {
                        $class = 'points-uncom';
                    }
                }
                //Photo Gallery
                if ($row['F_Name'] == 'Photo Gallery') {
                    $sqlGal = "SELECT * FROM tbl_Business_Feature_Photo	WHERE BFP_BL_ID = '" . encode_strings($BL_ID, $db) . "' ORDER BY BFP_Order";
                    $resultGal = mysql_query($sqlGal, $db) or die("Invalid query: $sqlGal -- " . mysql_error());
                    $numRowsGal = mysql_num_rows($resultGal);
                    if ($numRowsGal > 0 && $row1['BLF_Active'] == "1") {
                        $class = 'points-com';
                    } else {
                        $class = 'points-uncom';
                    }
                }
                //Our Products
                if ($row['F_Name'] == 'Our Products') {
                    $sqlProd = "SELECT * FROM tbl_Business_Feature_Product WHERE BFP_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                    $resultProd = mysql_query($sqlProd) or die(mysql_error());
                    $numRowsProd = mysql_num_rows($resultProd);
                    if ($numRowsProd > 0 && $row1['BLF_Active'] == "1") {
                        $class = 'points-com';
                    } else {
                        $class = 'points-uncom';
                    }
                }
                //Meet The Team
                if ($row['F_Name'] == 'Meet The Team') {
                    $sqlTeam = "SELECT * FROM tbl_Business_Feature_Team WHERE BFT_BL_ID = '" . encode_strings($BL_ID, $db) . "' ORDER BY BFT_Order";
                    $resultTeam = mysql_query($sqlTeam, $db) or die("Invalid query: $sqlTeam -- " . mysql_error());
                    $numRowsTeam = mysql_num_rows($resultTeam);
                    if ($numRowsTeam > 0 && $row1['BLF_Active'] == "1") {
                        $class = 'points-com';
                    } else {
                        $class = 'points-uncom';
                    }
                }
                //Meet The Chef
                if ($row['F_Name'] == 'Meet The Chef') {
                    $sqlChef = "SELECT * FROM tbl_Business_Feature_Chef WHERE BFC_BL_ID = '" . encode_strings($BL_ID, $db) . "'  ORDER BY BFC_Order";
                    $resultChef = mysql_query($sqlChef, $db) or die("Invalid query: $sqlChef -- " . mysql_error());
                    $numRowsChef = mysql_num_rows($resultChef);
                    if ($numRowsChef > 0 && $row1['BLF_Active'] == "1") {
                        $class = 'points-com';
                    } else {
                        $class = 'points-uncom';
                    }
                }
                //Menu
                if ($row['F_Name'] == 'Menu') {
                    $sqlMenu = "SELECT * FROM tbl_Business_Feature_Menu WHERE BFM_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                    $resultMenu = mysql_query($sqlMenu, $db) or die("Invalid query: $sqlMenu -- " . mysql_error());
                    $numRowsMenu = mysql_num_rows($resultMenu);
                    if ($numRowsMenu > 0 && $row1['BLF_Active'] == "1") {
                        $class = 'points-com';
                    } else {
                        $class = 'points-uncom';
                    }
                }
                //Daily Specials
                if ($row['F_Name'] == 'Daily Features') {
                    $sqlDS = "SELECT * FROM tbl_Business_Feature_Daily_Specials WHERE BFDS_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                    $resultDS = mysql_query($sqlDS, $db) or die("Invalid query: $sqlDS -- " . mysql_error());
                    $numRowsDS = mysql_num_rows($resultDS);
                    if ($numRowsDS > 0 && $row1['BLF_Active'] == "1") {
                        $class = 'points-com';
                    } else {
                        $class = 'points-uncom';
                    }
                }
                //Entertainment
                if ($row['F_Name'] == 'Entertainment') {
                    $sqlEnt = "SELECT * FROM tbl_Business_Feature_Entertainment_Acts WHERE BFEA_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                    $resultEnt = mysql_query($sqlEnt) or die(mysql_error());
                    $numRowsEnt = mysql_num_rows($resultEnt);
                    if ($numRowsEnt > 0 && $row1['BLF_Active'] == "1") {
                        $class = 'points-com';
                    } else {
                        $class = 'points-uncom';
                    }
                }
                //Guest Book
                if ($row['F_Name'] == 'Guest Book') {
                    $sqlGB = "SELECT * FROM tbl_Business_Feature_Guest_Book WHERE BFGB_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BFGB_Status = 1";
                    $resultGB = mysql_query($sqlGB) or die(mysql_error());
                    $numRowsGB = mysql_num_rows($resultGB);
                    if ($numRowsGB > 0 && $row1['BLF_Active'] == "1") {
                        $class = 'points-com';
                    } else {
                        $class = 'points-uncom';
                    }
                }
                //Power Ranking
                if ($row['F_Name'] == 'Power Ranking') {
                    if ($row1['BLF_Active'] == "1") {
                        $class = 'points-com';
                    } else {
                        $class = 'points-uncom';
                    }
                }
                //Agendas & Minutes   
                if ($row['F_Name'] == 'Agendas & Minutes') {
                    $sqlAM = "SELECT * FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                    $resultAM = mysql_query($sqlAM) or die(mysql_error());
                    $numRowsAM = mysql_num_rows($resultAM);
                    if ($numRowsAM > 0 && $row1['BLF_Active'] == "1") {
                        $class = 'points-com';
                    } else {
                        $class = 'points-uncom';
                    }
                }
                //Manage Coupon
                if ($row['F_Name'] == 'Manage Coupon') {
                    $sqlMC = "SELECT * FROM tbl_Business_Feature_Coupon WHERE BFC_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BFC_Status = 1";
                    $resultMC = mysql_query($sqlMC) or die(mysql_error());
                    $numRowsMC = mysql_num_rows($resultMC);
                    if ($numRowsMC > 0 && $row1['BLF_Active'] == "1") {
                        $class = 'points-com';
                    } else {
                        $class = 'points-uncom';
                    }
                }
                ?>
                <li class="<?= $class ?>"><a href="/<?= $row['F_File'] ?>?bl_id=<?= $BL_ID ?>"><?= $row['F_Name'] ?></a></li>

                <?php
            }
            $class = '';
        }
        ?>
    </ul>
</div>