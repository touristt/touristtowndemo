
        function onTemplateLoaded(pPlayer) {
     // Get a reference to the player
     player = bcPlayer.getPlayer(pPlayer);

     // Get references to the player modules
     video = player.getModule(APIModules.VIDEO_PLAYER);
     exp = player.getModule(APIModules.EXPERIENCE);

     // Register event listeners
     exp.addEventListener(BCExperienceEvent.TEMPLATE_READY, onTemplateReady);
     video.addEventListener(BCVideoEvent.VIDEO_COMPLETE, onVideoComplete);
}
        function onTemplateReady(e) {
           // alert("onTemplateReady");
        }

        function onVideoComplete(e) {
            BCL.isPlayerAdded = false;
				// unload the player
				exp.unload();
				// remove the player code
				document.getElementById("videoContent").innerHTML = "";
			
			$("#videoContent").hide();
	
			$('#headingParent').animate({
					height: '310'
				  }, {
					duration: 500,
					specialEasing: {
					  height: 'linear'
					}});
			$("#subsection-header").fadeIn(500);
        }


// namespace to keep the global clear of clutter


// data for our player -- note that it must have ActionScript/JavaScript APIs enabled!!
BCL.playerData = { "playerKey" : "AQ~~,AAABZ2UN5kk~,07c56WniKYRqTreWYMMlcqnoFexjXa6P",
                    "width" : playerWidth,
                    "height" : playerHeight,
                    "videoID" : playerVideoID };
// flag to keep track of whether there is a player
BCL.isPlayerAdded = false;
// template for the player object - will populate it with data using markup()
BCL.playerTemplate = "<div style=\"display:none\"></div><object id=\"myExperience\" class=\"BrightcoveExperience\"><param name=\"autoStart\" value=\"true\" /><param name=\"bgcolor\" value=\"#FFFFFF\" /><param name=\"width\" value=\"{{width}}\" /><param name=\"height\" value=\"{{height}}\" /><param name=\"playerID\" value=\"{{playerID}}\" /><param name=\"playerKey\" value=\"{{playerKey}}\" /><param name=\"isVid\" value=\"true\" /><param name=\"isUI\" value=\"true\" /><param name=\"dynamicStreaming\" value=\"true\" /><param name=\"@videoPlayer\" value=\"{{videoID}}\"; /><param name=\"templateLoadHandler\" value=\"BCL.onTemplateLoaded\"</object>";
BCL.addPlayer = function () {
  // if we don't already have a player
  if (BCL.isPlayerAdded == false) {
    BCL.isPlayerAdded = true;
    var playerHTML = "";
    
    // populate the player object template
    playerHTML = BCL.markup(BCL.playerTemplate, BCL.playerData);
    // inject the player code into the DOM
    $("#subsection-header").fadeOut(500);
	
	$('#headingParent').animate({
                    height: playerHeight
  }, {
    duration: 500,
    specialEasing: {
      height: 'linear'
    }});
	
	
	$("#videoContent").show();
	
	if (BUSLISTINGID > 0) {
		$.post("/include/videoCount.php", { blid: BUSLISTINGID } );
	} else {
		$.post("/include/videoCount.php", { rid: REGIONID, cid: CATEGORYID } );
	}
	
	document.getElementById("videoContent").innerHTML = playerHTML;
    // instantiate the player
    player = brightcove.createExperiences();
  }
 
};


BCL.markup = function (html, data) {
    var m;
    var i = 0;
    var match = html.match(data instanceof Array ? /{{\d+}}/g : /{{\w+}}/g) || [];

    while (m = match[i++]) {
        html = html.replace(m, data[m.substr(2, m.length-4)]);
    }
    return html;
};
