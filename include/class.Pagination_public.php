<?php

/*
  Very basic pagination class, but should allow the ability to
  quickly drop in to pages where querysets return many
  results.

  I have hardcoded this with some default CSS values as obtained
  from Brad.

  usage : create a new instance of the Paginate class, ensureing you
  pass in the number of total query results. Optionally pass
  the number of results per page (defaults to 10).

 */

class Paginate {

    public $totalResults;
    public $totalPages;
    public $page;
    public $opSearch;
    public $txtStartDay;
    public $txtEndDay;
    public $sortRegion;
    public $cat_stories; 
    public $textSearch;
    public $queryString;
    // static value defining max results per page.
    protected $maxResults;
    protected $prev;
    protected $next;
    protected $from;
    
    public function __construct($totalResults, $maxResults = 10) {

        $this->totalResults = $totalResults;
        $this->maxResults = $maxResults;
        $this->page = (!isset($_GET['page'])) ? 1 : $_GET['page'];
        $this->opSearch = (!isset($_GET['opSearch'])) ? 0 : $_GET['opSearch'];
        $this->txtStartDay = (!isset($_GET['txtStartDay'])) ? 0 : $_GET['txtStartDay'];
        $this->txtEndDay = (!isset($_GET['txtEndDay'])) ? 0 : $_GET['txtEndDay'];
        $this->sortRegion = (!isset($_GET['sortRegion'])) ? 0 : $_GET['sortRegion'];
         $this->cat_stories = (!isset($_GET['cat_stories'])) ? 0 : $_GET['cat_stories'];
         $this->textSearch = (!isset($_GET['textSearch'])) ? 0 : $_GET['textSearch'];
        $this->queryString = (!isset($_SERVER['QUERY_STRING'])) ? '' : $_SERVER['QUERY_STRING'];
        $this->queryString2 = (!isset($_SERVER['REQUEST_URI'])) ? '' : $_SERVER['REQUEST_URI'];
        if (strpos($this->queryString2, '&page') !== false) {
            $this->pagepath = explode('&page=', $this->queryString);
        } else {
            $this->pagepath = explode('page=', $this->queryString);
        }
        $this->pagepathno = $this->pagepath[0];
        $this->type = (!isset($_GET['type'])) ? '' : $_GET['type'];
        $this->prev = $this->page - 1;
        $this->next = $this->page + 1;
        $this->from = (($this->page * $this->maxResults) - $this->maxResults);
        $this->totalPages = ceil($this->totalResults / $this->maxResults);
    }

    public function paginate() {
        /* return nothing if we only have a single page */
        if ($this->totalPages <= 1) {
            return;
        }

        $pagination = '<div class="pagination clearfix"><div class="pagination-inside-div">';
        $pagination .= '<div class="pull-left">Page ' . $this->page . ' of ' . $this->totalPages . '</div>';

        $pagination .= '<div class="pull-right">';
        /* generate a previous page link */
        if ($this->page > 1) {

            if ($this->txtStartDay != 0 && $this->txtEndDay != 0) {
                $pagination .= '<a href="?opSearch=1&txtStartDay=' . $this->txtStartDay . '&txtEndDay=' . $this->txtEndDay . '&button=Search&page=' . $this->prev . '" class="page arrows">&lt; Previous</a> ';
            }elseif($this->sortRegion > 0){
                $pagination .= '<a href="?sortRegion=' . $this->sortRegion . '&page=' . $this->prev . '#section_stories" class="page arrows">&lt; Previous</a> ';
            } 
            elseif(($this->textSearch > 0 || $this->textSearch != '') && $this->cat_stories > 0){
                $pagination .= '<a href="?op=searchStories&cat_stories=' . $this->cat_stories . '&textSearch=' . $this->textSearch . '&page=' . $this->prev . '#section_stories" class="page arrows">&lt; Previous</a> ';
            }
            elseif($this->cat_stories > 0){
                $pagination .= '<a href="?op=searchStories&cat_stories=' . $this->cat_stories . '&page=' . $this->prev . '#section_stories" class="page arrows">&lt; Previous</a> ';
            } elseif($this->textSearch > 0 || $this->textSearch != ''){
                $pagination .= '<a href="?op=searchStories&textSearch=' . $this->textSearch . '&page=' . $this->prev . '#section_stories" class="page arrows">&lt; Previous</a> ';
            } 

            elseif ($this->queryString) {
                if (strpos($this->queryString, 'url=') !== false) {
                    $pagination .= '<a href="?page=' . $this->prev . '" class="page arrows">&lt; Previous</a> ';
                } else {
                    if (strpos($this->queryString2, '?page') !== false) {
                        $pagination .= '<a href="?' . $this->pagepathno . 'page=' . $this->prev . '" class="page arrows">&lt; Previous</a> ';
                    }else
                    {
                        $pagination .= '<a href="?' . $this->pagepathno . '&page=' . $this->prev . '" class="page arrows">&lt; Previous</a> ';
                    }
                    
                }
            } else if ($this->type) {
                $pagination .= '<a href="?type=' . $this->type . '&page=' . $this->prev . '" class="page arrows">&lt; Previous</a> ';
            } else {
                $pagination .= '<a href="?page=' . $this->prev . '" class="page arrows">&lt; Previous</a> ';
            }
        }

        /* generate links for _all_ pages unless current page */
        for ($i = 1; $i <= $this->totalPages; $i++) {
            if ($this->page == $i) {
                $pagination .= '<span class="page page-active">' . $i . '</span>';
            } else {
                if ($this->txtStartDay != 0 && $this->txtEndDay != 0) {
                    $pagination .= '<a href="?opSearch=1&txtStartDay=' . $this->txtStartDay . '&txtEndDay=' . $this->txtEndDay . '&page=' . $i . '&button=Search" class="page">' . $i . '</a>';
                }elseif($this->sortRegion > 0){
                $pagination .= '<a href="?sortRegion=' . $this->sortRegion . '&page=' . $i . '#section_stories" class="page arrows">' . $i . '</a> ';
            }
            elseif(($this->textSearch > 0 || $this->textSearch != '') && $this->cat_stories > 0){
                $pagination .= '<a href="?op=searchStories&cat_stories=' . $this->cat_stories . '&textSearch=' . $this->textSearch . '&page=' . $i . '#section_stories" class="page arrows">' . $i . '</a> ';
            }
            elseif($this->cat_stories > 0){
                $pagination .= '<a href="?op=searchStories&cat_stories=' . $this->cat_stories . '&page=' . $i . '#section_stories" class="page arrows">' . $i . '</a> ';
            } elseif($this->textSearch > 0 || $this->textSearch != ''){
                $pagination .= '<a href="?op=searchStories&textSearch=' . $this->textSearch . '&page=' . $i . '#section_stories" class="page arrows">' . $i . '</a> ';
            } 
            elseif ($this->queryString) {
                    if (strpos($this->queryString, 'url=') !== false) {
                        $pagination .= '<a href="?page=' . $i . '" class="page">' . $i . '</a> ';
                    } else {
                        if (strpos($this->queryString2, '?page') !== false) {
                            $pagination .= '<a href="?' . $this->pagepathno . 'page=' . $i . '" class="page">' . $i . '</a>';
                        } else {
                            $pagination .= '<a href="?' . $this->pagepathno . '&page=' . $i . '" class="page">' . $i . '</a>';
                        }
                    }
                } else if ($this->type != '') {
                    $pagination .= '<a href="?type=' . $this->type . '&page=' . $i . '" class="page">' . $i . '</a> ';
                } else {
                    $pagination .= '<a href="?page=' . $i . '" class="page">' . $i . '</a>';
                }
            }
        }

        /* generate the next page link */
        if ($this->page < $this->totalPages) {
            if ($this->txtStartDay != 0 && $this->txtEndDay != 0) {
                $pagination .= '<a href="?opSearch=1&txtStartDay=' . $this->txtStartDay . '&txtEndDay=' . $this->txtEndDay . '&button=Search&page=' . $this->next . '" class="page arrows">Next &gt;</a> ';
            }elseif($this->sortRegion > 0){
                $pagination .= '<a href="?sortRegion=' . $this->sortRegion . '&page=' . $this->next . '#scroll" class="page arrows">Next &gt;</a> ';
            }
            elseif(($this->textSearch > 0 || $this->textSearch != '') && $this->cat_stories > 0){
                $pagination .= '<a href="?op=searchStories&cat_stories=' . $this->cat_stories . '&textSearch=' . $this->textSearch . '&page=' . $this->next . '#section_stories" class="page arrows">Next &gt;</a> ';
            }
            elseif($this->cat_stories > 0){
                $pagination .= '<a href="?op=searchStories&cat_stories=' . $this->cat_stories . '&page=' . $this->next . '#section_stories" class="page arrows">Next &gt;</a> ';
            } elseif($this->textSearch > 0 || $this->textSearch != ''){
                $pagination .= '<a href="?op=searchStories&textSearch=' . $this->textSearch . '&page=' . $this->next . '#section_stories" class="page arrows">Next &gt;</a> ';
            } 
            elseif ($this->queryString) {
                if (strpos($this->queryString, 'url=') !== false) {
                    $pagination .= '<a href="?page=' . $this->next . '" class="page arrows">Next &gt;</a> ';
                } else {
                    if (strpos($this->queryString2, '?page') !== false) {
                        $pagination .= '<a href="?' . $this->pagepathno . 'page=' . $this->next . '" class="page arrows">Next &gt;</a>';
                    } else {
                        $pagination .= '<a href="?' . $this->pagepathno . '&page=' . $this->next . '" class="page arrows">Next &gt;</a>';
                    }
                    
                }
            } else if ($this->type) {
                $pagination .= '<a href="?type=' . $this->type . '&page=' . $this->next . '" class="page arrows">Next &gt;</a> ';
            } else {
                $pagination .= '<a href="?page=' . $this->next . '" class="page arrows">Next &gt;</a>';
            }
        }

        $pagination .= "</div></div></div>";
        return $pagination;
    }

    public function generateSql() {
        // make sure we do not fall off the bottom end. set a default
        // of 1 in the event a bogus value is passed into the page
        // get parameter.
        if ($this->from < 0) {
            return ' LIMIT 1, ' . $this->maxResults;
        }
        return ' LIMIT ' . $this->from . ', ' . $this->maxResults;
    }

}
