<?php

require_once '../include/config.inc.php';

if ($_SESSION['USER_LIMIT'] != '') {
    $limits = explode(',', $_SESSION['USER_LIMIT']);
    $regionLimit = array();
    foreach ($limits as $limit) {
        $sql = "SELECT R_ID, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $region = mysql_fetch_assoc($result);
        if ($region['R_Type'] == 1) {
            $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($region['R_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                if ($row['R_ID'] != '' && !in_array($row['R_ID'], $regionLimit)) {
                    $regionLimit[] = $row['R_ID'];
                }
            }
        } else {
            if ($region['R_ID'] != '' && !in_array($region['R_ID'], $regionLimit)) {
                $regionLimit[] = $region['R_ID'];
            }
        }
    }
}

$regionLimitCommaSeparated = rtrim(implode(',', $regionLimit), ',');
$sql = "SELECT * FROM tbl_Business_Listing LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
        INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID";
if ($_SESSION['USER_LIMIT'] != '') {
    $region_where = " AND BLCR_BLC_R_ID IN (" . encode_strings($regionLimitCommaSeparated, $db) . ")";
} else {
    $region_where = " ";
}
$sql .= " WHERE (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) 
          AND hide_show_listing = 1 " . $region_where . " GROUP BY BL_ID";
$result = mysql_query($sql);
$key_name = '';
echo "[";
$key_name = "";
while ($row = mysql_fetch_array($result)) {
    $title = str_replace('"', ' ', $row['BL_Listing_Title']);
    $key_name .= '{id:"' . $row['BL_ID'] . '", name:"' . $title . '"},';
}
echo $key_name;
echo "]";
?>