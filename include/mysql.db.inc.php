<?PHP

$db = mysql_connect($serverName, $userName, $password)
        or die('I cannot connect to the database because: ' . mysql_error());
mysql_select_db($dbname, $db)
        or die("Cannot select $dbname");

mysql_query("SET NAMES utf8;");
mysql_query("SET CHARACTER_SET utf8;");

function query($sql, $db) {
    $result = mysql_query($sql, $db)
            or die("<p>QUERY ERROR: $sql</p><p>" . mysql_error() . "</p>");
    return $result;
}

function make_options_from_enum($sql, $selected, $db) {
    $str = "";
    $result = query($sql, $db);
    if (mysql_num_rows($result) > 0) {
        $row = mysql_fetch_row($result);
        $options = explode("','", preg_replace("/(enum|set)\('(.+?)'\)/", "\\2", $row[1]));
    }
    foreach ($options as $value) {
        $str .= "<option value='$value'";
        if ($selected == $value)
            $str .= "selected";
        $str .= ">$value</option>";
    }
    return $str;
}

function make_options_from_tbl($sql, $selected, $db) {
    $str = "";
    $result = query($sql, $db);
    while ($row = mysql_fetch_row($result)) {
        $str .= "<option value='$row[0]'";
        if ($selected == $row[0])
            $str .= "selected";
        $str .= ">$row[1]</option>";
    }
    return $str;
}

function encode_strings($str, $db) {
    return mysql_real_escape_string(convert_smart_quotes(stripslashes($str)), $db);
}

function encode_form_vars($str) {
    return htmlspecialchars($str, ENT_QUOTES);
}
?>