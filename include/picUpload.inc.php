<?PHP

//last three parameters are for image bank
function Upload_Pic($i, $var = 'photo', $maxW = 0, $maxH = 0, $strict = false, $location = IMG_LOC_ABS, $maxFileSize = 0, $rename = true, $scaleFor = 0, $type = '', $id = 0) {
    $ext = ".jpg";
    if (!file_exists($_FILES[$var]['tmp_name'][$i])) {
        return 0;
    }

    $imgSize = getimagesize($_FILES[$var]['tmp_name'][$i]);
    $fileSize = filesize($_FILES[$var]['tmp_name'][$i]);
    if (!$imgSize) {
        return 0;
    }

    if ($imgSize[2] == 1) {
        $ext = ".gif";
    } elseif ($imgSize[2] == 2) {
        $ext = ".jpg";
    } elseif ($imgSize[2] == 3) {
        $ext = ".png";
    } elseif ($imgSize[2] == 13) {
        $ext = ".swf";
    } else {
        $_SESSION[SESSIONERROR] = "File Type NOT Allowed: " . $imgSize[2];
        return "0";
    }

    if (($maxH > 0 || $maxW > 0) && in_array($ext, array('.swf', '.jpg', '.jpeg', '.gif', '.png'))) {
        if ($maxW > 0) {

            if ($imgSize[0] <> $maxW && $strict) {
                $_SESSION[SESSIONERROR] = "ERROR: Photo wrong size. Must be " . $maxW . " pixels wide. The Photo was not saved.";
                return "0";
            } elseif ($imgSize[0] > $maxW) {
                $_SESSION[SESSIONERROR] = "ERROR: Photo wrong size. Must be less than " . $maxW . " pixels wide. The Photo was not saved.";
                return "0";
            }
        }

        if ($maxH > 0) {
            if ($imgSize[1] <> $maxH && $strict) {
                $_SESSION[SESSIONERROR] = "ERROR: Photo wrong size. Must be " . $maxH . " pixels high. The Photo was not saved.";
                return "0";
            } elseif ($imgSize[1] > $maxH) {
                $_SESSION[SESSIONERROR] = "ERROR: Photo wrong size. Must be less than " . $maxH . " pixels high. The Photo was not saved.";
                return "0";
            }
        }
    }

    if (($maxH == 0 || $maxW == 0) && in_array($ext, array('.swf', '.jpg', '.jpeg', '.gif', '.png'))) {
        if ($maxFileSize > 0) {
            if ($fileSize > $maxFileSize) {
                $_SESSION[SESSIONERROR] = "ERROR: Photo wrong size. Must be less than 10MB. The Photo was not saved.";
                return "0";
            }
        }
    }

    if (file_exists($_FILES[$var]['tmp_name'][$i]) == true && $_FILES[$var]["error"][$i] == UPLOAD_ERR_OK && is_writable($location)) {
        $scale_mylink = "";
        if ($rename == true) {
            do {
                $myLink = mt_rand(1000, 9999999);
                $scale_mylink = $myLink;
                $file = $_FILES[$var]['tmp_name'][$i];
            } while (file_exists($location . $myLink . $ext) == true);
        } else {
            $myLink = str_ireplace($ext, '', Clean_FileName($_FILES[$var]['name'][$i]));
            $addRand = mt_rand(1000, 9999999);
            $myLink = $addRand . '-' . $myLink;
            $scale_mylink = $addRand;
            $file = Clean_FileName($_FILES[$var]['name'][$i]);
        }
        //Creat image from Imgick
        if ($scaleFor >= 1) {
            $orginal_image = 0;
            $getsizes = "SELECT * FROM tbl_Image_Bank_Sizes WHERE IBS_Scale_For = '" . $scaleFor . "' ORDER BY IBS_ID ASC";
            $sizes = mysql_query($getsizes);
            $scaled_imgs = array();
            while ($image_sizes = mysql_fetch_array($sizes)) {
                $heightWidth = explode("X", $image_sizes['IBS_Sizes']);
                $img = new Imagick($file);
                $orientation = $img->getImageOrientation();
                if ($orientation == 3) {
                    $img->rotateImage('white', 180);
                } elseif ($orientation == 6) {
                    $img->rotateImage('white', 90);
                } elseif ($orientation == 8) {
                    $img->rotateImage('white', -90);
                }
                $img->setImageResolution(72, 72);
                $img->resampleImage(72, 72, imagick::FILTER_UNDEFINED, 1);
                //Creating Original Image in Image Bank
                if ($orginal_image == 0) {
                    $img_bnk_name = $scale_mylink . '-' . Clean_FileName($_FILES[$var]['name'][$i]);
                    $img_bnk_thumb_name = mt_rand(1000, 9999999) . '-' . Clean_FileName($_FILES[$var]['name'][$i]);
                    $img_bnk_detail_name = mt_rand(1000, 9999999) . '-' . Clean_FileName($_FILES[$var]['name'][$i]);
                    if (in_array('county', $_SESSION['USER_ROLES'])) {
                        $sql = "SELECT PO_ID FROM tbl_Photographer_Owner LEFT JOIN tbl_User ON PO_ID = U_Owner where U_ID = '" . $_SESSION['USER_ID'] . "'";
                        $result = mysql_query($sql);
                        $rowCounty = mysql_fetch_assoc($result);
                        $IB_Owner = $rowCounty['PO_ID'];
                    } else {
                        $IB_Owner = 1;
                    }
                    $IMG_BNK = "INSERT tbl_Image_Bank SET IB_Path = '" . $img_bnk_name . "', IB_Thumbnail_Path = '" . $img_bnk_thumb_name . "', IB_Detail_Path = '" . $img_bnk_detail_name . "', IB_Date = CURDATE(), IB_Owner = '" . $IB_Owner . "'";
                    if ($type == 'Region') {
                        $IMG_BNK .= ", IB_Region = '" . $id . "'";
                    }
                    if ($type == 'Listing') {
                        // get regions and categories of this listing
                        $sql_region = "SELECT BL_C_ID, BLCR_BLC_R_ID FROM tbl_Business_Listing
                                        INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID 
                                        INNER JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                                        WHERE BL_ID = '$id'";
                        $res_region = mysql_query($sql_region);
                        $regions = '';
                        $category = 0;
                        $prefix = '';
                        while ($r = mysql_fetch_assoc($res_region)) {
                            if ($r['BLCR_BLC_R_ID'] != '' && !in_array($r['BLCR_BLC_R_ID'], explode($regions))) {
                                $regions .= $prefix . $r['BLCR_BLC_R_ID'];
                            }
                            if ($r['BL_C_ID'] != '' && $r['BL_C_ID'] != $category) {
                                $category = $r['BL_C_ID'];
                            }
                            $prefix = ',';
                        }
                        $IMG_BNK .= ", IB_Region = '" . $regions . "', IB_Category = '" . $category . "', IB_Listings = '" . $id . "'";
                    }
                    if ($type == 'Category') {
                        $IMG_BNK .= ", IB_Category = '" . $id . "'";
                    }
                    if ($type == 'Slider') {
                        $IMG_BNK .= ", IB_Category = '" . $id . "'";
                    }
                    if (in_array('superadmin', $_SESSION['USER_ROLES'])) {
                        $IMG_BNK .= ", IB_Photographer = 20";
                    } else {
                        $IMG_BNK .= ", IB_Photographer = 21";
                    }
                    mysql_query($IMG_BNK);
                    $last_id = mysql_insert_id();
                    //Create thumbnail of image in image bank
                    $thumb = new Imagick($file);
                    if ($orientation == 3) {
                        $thumb->rotateImage('white', 180);
                    } elseif ($orientation == 6) {
                        $thumb->rotateImage('white', 90);
                    } elseif ($orientation == 8) {
                        $thumb->rotateImage('white', -90);
                    }
                    $thumb->setImageResolution(72, 72);
                    $thumb->resampleImage(72, 72, imagick::FILTER_UNDEFINED, 1);
                    $thumb_d = $thumb->getImageGeometry();
                    $thumbsourceHeight = $thumb_d['height'];
                    $thumbsourceWidth = $thumb_d['width'];
                    if ($thumbsourceWidth > 200 || $thumbsourceHeight > 200) {
                        if ($thumbsourceWidth > $thumbsourceHeight) {
                            $thumb->scaleImage(200, 0);
                        } else {
                            $thumb->scaleImage(0, 200);
                        }
                    }
                    //Create detailed page preview image in image bank
                    $detail = new Imagick($file);
                    if ($orientation == 3) {
                        $detail->rotateImage('white', 180);
                    } elseif ($orientation == 6) {
                        $detail->rotateImage('white', 90);
                    } elseif ($orientation == 8) {
                        $detail->rotateImage('white', -90);
                    }
                    $detail->setImageResolution(72, 72);
                    $detail->resampleImage(72, 72, imagick::FILTER_UNDEFINED, 1);
                    $detail_d = $detail->getImageGeometry();
                    $detailsourceHeight = $detail_d['height'];
                    $detailsourceWidth = $detail_d['width'];
                    if ($detailsourceWidth > 504 || $detailsourceHeight > 380) {
                        if ($detailsourceWidth > $detailsourceHeight) {
                            $detail->scaleImage(504, 0);
                        } else {
                            $detail->scaleImage(0, 380);
                        }
                    }
                    if ($ext == '.jpg' || $ext == '.jpeg') {
                        header('Content-type: image/jpeg');
                        $img->writeImage(IMG_BANK_ABS . $img_bnk_name);
                        $thumb->writeImage(IMG_BANK_ABS . $img_bnk_thumb_name);
                        $detail->writeImage(IMG_BANK_ABS . $img_bnk_detail_name);
                    }
                    if ($ext == '.gif') {
                        header('Content-type: image/gif');
                        $img->writeImage(IMG_BANK_ABS . $img_bnk_name);
                        $thumb->writeImage(IMG_BANK_ABS . $img_bnk_thumb_name);
                        $detail->writeImage(IMG_BANK_ABS . $img_bnk_detail_name);
                    }
                    if ($ext == '.png') {
                        header('Content-type: image/png');
                        $img->writeImage(IMG_BANK_ABS . $img_bnk_name);
                        $thumb->writeImage(IMG_BANK_ABS . $img_bnk_thumb_name);
                        $detail->writeImage(IMG_BANK_ABS . $img_bnk_detail_name);
                    }
                }
                $path = str_replace(' ', "%20", 'http://' . DOMAIN . IMG_BANK_REL . $img_bnk_name);
                list($width, $height) = getimagesize($path);
                $dimension = $width . "X" . $height;
                $add_dimension = "UPDATE tbl_Image_Bank SET IB_Dimension = '" . $dimension . "' WHERE IB_ID = '" . $last_id . "'";
                mysql_query($add_dimension);

                $d = $img->getImageGeometry();
                $sourceHeight = $d['height'];
                $sourceWidth = $d['width'];

                $targetHeight = $heightWidth[1];
                $targetWidth = $heightWidth[0];
                if ($image_sizes['IBS_ID'] == 3) {
                    if ($sourceWidth > $sourceHeight) {
                        $img->scaleImage($targetWidth, 0);
                    } else {
                        $img->scaleImage(0, $targetHeight);
                    }
                } else if ($image_sizes['IBS_ID'] == 14 || $image_sizes['IBS_ID'] == 16) {
                    if ($sourceHeight > $targetHeight) {
                        $img->scaleImage(0, $targetHeight);
                    } else if ($sourceWidth > $targetWidth) {
                        $img->scaleImage($targetWidth, 0);
                    }
                } else {
                    $sourceRatio = $sourceWidth / $sourceHeight;
                    $targetRatio = $targetWidth / $targetHeight;
                    if ($sourceRatio < $targetRatio) {
                        $scale = $sourceWidth / $targetWidth;
                    } else {
                        $scale = $sourceHeight / $targetHeight;
                    }
                    $resizeWidth = ($sourceWidth / $scale);
                    $resizeHeight = ($sourceHeight / $scale);
                    $cropLeft = (($resizeWidth - $targetWidth) / 2);
                    $cropTop = (($resizeHeight - $targetHeight) / 2);
                    $img->scaleImage($resizeWidth, $resizeHeight);
                    $img->cropImage($targetWidth, $targetHeight, $cropLeft, $cropTop);
                }
                $imagepath = $location . $scale_mylink . $targetWidth . "X" . $targetHeight . Clean_FileName($_FILES[$var]['name'][$i]);
                //if slider is zero no sizes will creates
                if ($type != "Slider") {
                    $IMG_MULTIPLE = "INSERT tbl_Image_Bank_Multiple_Images 
                        SET IBMI_IBS_ID = '" . $image_sizes['IBS_ID'] . "',
                        IBMI_IB_ID = '" . $last_id . "',
                        IBMI_Path = '" . $scale_mylink . $targetWidth . "X" . $targetHeight . Clean_FileName($_FILES[$var]['name'][$i]) . "'";
                    mysql_query($IMG_MULTIPLE);
                }
                if ($ext == '.jpg' || $ext == '.jpeg') {
                    header('Content-type: image/jpeg');
                    $img->writeImage($imagepath);
                }
                if ($ext == '.gif') {
                    header('Content-type: image/gif');
                    $img->writeImage($imagepath);
                }
                if ($ext == '.png') {
                    header('Content-type: image/png');
                    $img->writeImage($imagepath);
                }
                $orginal_image++;
                $scaled_imgs[] = $scale_mylink . $targetWidth . "X" . $targetHeight . Clean_FileName($_FILES[$var]['name'][$i]);
            }
            header("Content-Type: text/plain");
            return array($scaled_imgs, $last_id);
        } else {
            $img = new Imagick($file);
            $orientation = $img->getImageOrientation();
            $scaled_imgs = array();
            $img_bnk_name = $scale_mylink . '-' . Clean_FileName($_FILES[$var]['name'][$i]);
            $img_bnk_thumb_name = mt_rand(1000, 9999999) . '-' . Clean_FileName($_FILES[$var]['name'][$i]);
            $img_bnk_detail_name = mt_rand(1000, 9999999) . '-' . Clean_FileName($_FILES[$var]['name'][$i]);
            //Create thumbnail of image in image bank
            $thumb = new Imagick($file);
            if ($orientation == 3) {
                $thumb->rotateImage('white', 180);
            } elseif ($orientation == 6) {
                $thumb->rotateImage('white', 90);
            } elseif ($orientation == 8) {
                $thumb->rotateImage('white', -90);
            }
            $thumb->setImageResolution(72, 72);
            $thumb->resampleImage(72, 72, imagick::FILTER_UNDEFINED, 1);
            $thumb_d = $thumb->getImageGeometry();
            $thumbsourceHeight = $thumb_d['height'];
            $thumbsourceWidth = $thumb_d['width'];
            if ($thumbsourceWidth > 200 || $thumbsourceHeight > 200) {
                if ($thumbsourceWidth > $thumbsourceHeight) {
                    $thumb->scaleImage(200, 0);
                } else {
                    $thumb->scaleImage(0, 200);
                }
            }
            //Create detailed page preview image in image bank
            $detail = new Imagick($file);
            if ($orientation == 3) {
                $detail->rotateImage('white', 180);
            } elseif ($orientation == 6) {
                $detail->rotateImage('white', 90);
            } elseif ($orientation == 8) {
                $detail->rotateImage('white', -90);
            }
            $detail->setImageResolution(72, 72);
            $detail->resampleImage(72, 72, imagick::FILTER_UNDEFINED, 1);
            $detail_d = $detail->getImageGeometry();
            $detailsourceHeight = $detail_d['height'];
            $detailsourceWidth = $detail_d['width'];
            if ($detailsourceWidth > 504 || $detailsourceHeight > 380) {
                if ($detailsourceWidth > $detailsourceHeight) {
                    $detail->scaleImage(504, 0);
                } else {
                    $detail->scaleImage(0, 380);
                }
            }
            if ($ext == '.jpg' || $ext == '.jpeg') {
                header('Content-type: image/jpeg');
                $img->writeImage(IMG_BANK_ABS . $img_bnk_name);
                $thumb->writeImage(IMG_BANK_ABS . $img_bnk_thumb_name);
                $detail->writeImage(IMG_BANK_ABS . $img_bnk_detail_name);
            }
            if ($ext == '.gif') {
                header('Content-type: image/gif');
                $img->writeImage(IMG_BANK_ABS . $img_bnk_name);
                $thumb->writeImage(IMG_BANK_ABS . $img_bnk_thumb_name);
                $detail->writeImage(IMG_BANK_ABS . $img_bnk_detail_name);
            }
            if ($ext == '.png') {
                header('Content-type: image/png');
                $img->writeImage(IMG_BANK_ABS . $img_bnk_name);
                $thumb->writeImage(IMG_BANK_ABS . $img_bnk_thumb_name);
                $detail->writeImage(IMG_BANK_ABS . $img_bnk_detail_name);
            }
            $scaled_imgs[] = $img_bnk_name;
            $scaled_imgs[] = $img_bnk_thumb_name;
            $scaled_imgs[] = $img_bnk_detail_name;
            return $scaled_imgs;
        }
    } else {
        return "0";
    }
}

function Upload_Pic_Library($bank_id, $scaleFor) {
    $select = "SELECT * 
                FROM `tbl_Image_Bank_Multiple_Images` 
                LEFT JOIN tbl_Image_Bank ON IB_ID = IBMI_IB_ID 
                LEFT JOIN tbl_Image_Bank_Sizes ON IBMI_IBS_ID = IBS_ID 
                WHERE IB_ID = '" . $bank_id . "' AND IBS_Scale_For = '" . $scaleFor . "' ORDER BY IBS_ID ASC";
    $result = mysql_query($select);
    if (mysql_num_rows($result) > 0) {
        //For Region Category Edit Page.
        $getSizes = "SELECT * FROM  `tbl_Image_Bank_Sizes` 
                        WHERE IBS_Scale_For = '" . $scaleFor . "' ORDER BY IBS_ID ASC";
        $resultSizes = mysql_query($getSizes);
        if (mysql_num_rows($result) == mysql_num_rows($resultSizes)) {
            $scaled_imgs = array();
            while ($row = mysql_fetch_array($result)) {
                $scaled_imgs[] = $row['IBMI_Path'];
            }
            return $scaled_imgs;
        } else {
            $scaled_imgs = array();
            $scale_mylink = mt_rand(1000, 9999999);
            while ($row = mysql_fetch_array($result)) {
                if ($row['IBMI_Path'] != "") {
                    $scaled_imgs[] = $row['IBMI_Path'];
                }
                $file = IMG_BANK_ABS . $row['IB_Path'];
                while ($rowSizes = mysql_fetch_array($resultSizes)) {
                    if ($row['IBMI_IBS_ID'] != $rowSizes['IBS_ID']) {
                        $heightWidth = explode("X", $rowSizes['IBS_Sizes']);
                        $img = new Imagick($file);
                        $img->setImageResolution(72, 72);
                        $img->resampleImage(72, 72, imagick::FILTER_UNDEFINED, 1);
                        $d = $img->getImageGeometry();
                        $sourceHeight = $d['height'];
                        $sourceWidth = $d['width'];
                        $targetHeight = $heightWidth[1];
                        $targetWidth = $heightWidth[0];
                        $sourceRatio = $sourceWidth / $sourceHeight;
                        $targetRatio = $targetWidth / $targetHeight;
                        if ($sourceRatio < $targetRatio) {
                            $scale = $sourceWidth / $targetWidth;
                        } else {
                            $scale = $sourceHeight / $targetHeight;
                        }
                        $resizeWidth = ($sourceWidth / $scale);
                        $resizeHeight = ($sourceHeight / $scale);
                        $cropLeft = (($resizeWidth - $targetWidth) / 2);
                        $cropTop = (($resizeHeight - $targetHeight) / 2);
                        $img->scaleImage($resizeWidth, $resizeHeight);
                        $img->cropImage($targetWidth, $targetHeight, $cropLeft, $cropTop);
                        $imagepath = IMG_LOC_ABS . $scale_mylink . $targetWidth . "X" . $targetHeight . $row['IB_Path'];
                        $IMG_MULTIPLE = "INSERT tbl_Image_Bank_Multiple_Images 
                        SET IBMI_IBS_ID = '" . $rowSizes['IBS_ID'] . "',
                        IBMI_IB_ID = '" . $row['IB_ID'] . "',
                        IBMI_Path = '" . $scale_mylink . $targetWidth . "X" . $targetHeight . $row['IB_Path'] . "'";
                        mysql_query($IMG_MULTIPLE);
                        $ext = explode(".", $row['IB_Path']);
                        $keys = array_keys($ext);
                        $last = end($keys);
                        if (strtolower($ext[$last]) == 'jpg' || strtolower($ext[$last]) == 'jpeg') {
                            header('Content-type: image/jpeg');
                            $img->writeImage($imagepath);
                        }
                        if (strtolower($ext[$last]) == 'gif') {
                            header('Content-type: image/gif');
                            $img->writeImage($imagepath);
                        }
                        if (strtolower($ext[$last]) == 'png') {
                            header('Content-type: image/png');
                            $img->writeImage($imagepath);
                        }
                        $scaled_imgs[] = $scale_mylink . $targetWidth . "X" . $targetHeight . $row['IB_Path'];
                    }
                }
            }
            header("Content-Type: text/plain");
            return $scaled_imgs;
        }
    } else {
        $getsizes = "SELECT * FROM tbl_Image_Bank_Sizes WHERE IBS_Scale_For = '" . $scaleFor . "' ORDER BY IBS_ID ASC";
        $sizes = mysql_query($getsizes);
        $image_bnk = "SELECT * FROM tbl_Image_Bank WHERE IB_ID = '" . $bank_id . "'";
        $bnk_size = mysql_query($image_bnk);
        $img_bnk_size = mysql_fetch_assoc($bnk_size);
        $file = IMG_BANK_ABS . $img_bnk_size['IB_Path'];
        $scaled_imgs = array();
        $scale_mylink = mt_rand(1000, 9999999);
        while ($image_sizes = mysql_fetch_array($sizes)) {
            $heightWidth = explode("X", $image_sizes['IBS_Sizes']);
            $img = new Imagick($file);
            $img->setImageResolution(72, 72);
            $img->resampleImage(72, 72, imagick::FILTER_UNDEFINED, 1);
            $d = $img->getImageGeometry();
            $sourceHeight = $d['height'];
            $sourceWidth = $d['width'];

            $targetHeight = $heightWidth[1];
            $targetWidth = $heightWidth[0];
            if ($image_sizes['IBS_ID'] == 3) {
                if ($sourceWidth > $sourceHeight) {
                    $img->scaleImage($targetWidth, 0);
                } else {
                    $img->scaleImage(0, $targetHeight);
                }
            } else if ($image_sizes['IBS_ID'] == 14 || $image_sizes['IBS_ID'] == 16) {
                if ($sourceHeight > $targetHeight) {
                    $img->scaleImage(0, $targetHeight);
                } else if ($sourceWidth > $targetWidth) {
                    $img->scaleImage($targetWidth, 0);
                }
            } else {
                $sourceRatio = $sourceWidth / $sourceHeight;
                $targetRatio = $targetWidth / $targetHeight;
                if ($sourceRatio < $targetRatio) {
                    $scale = $sourceWidth / $targetWidth;
                } else {
                    $scale = $sourceHeight / $targetHeight;
                }
                $resizeWidth = ($sourceWidth / $scale);
                $resizeHeight = ($sourceHeight / $scale);
                $cropLeft = (($resizeWidth - $targetWidth) / 2);
                $cropTop = (($resizeHeight - $targetHeight) / 2);
                $img->scaleImage($resizeWidth, $resizeHeight);
                $img->cropImage($targetWidth, $targetHeight, $cropLeft, $cropTop);
            }
            $imagepath = IMG_LOC_ABS . $scale_mylink . $targetWidth . "X" . $targetHeight . $img_bnk_size['IB_Path'];
            //if slider is zero no sizes will creates
            if (isset($type) && $type != "Slider") {
                $IMG_MULTIPLE = "INSERT tbl_Image_Bank_Multiple_Images 
                        SET IBMI_IBS_ID = '" . $image_sizes['IBS_ID'] . "',
                        IBMI_IB_ID = '" . $img_bnk_size['IB_ID'] . "',
                        IBMI_Path = '" . $scale_mylink . $targetWidth . "X" . $targetHeight . $img_bnk_size['IB_Path'] . "'";
                mysql_query($IMG_MULTIPLE);
            }
            $ext = explode(".", $img_bnk_size['IB_Path']);
            $keys = array_keys($ext);
            $last = end($keys);
            if (strtolower($ext[$last]) == 'jpg' || strtolower($ext[$last]) == 'jpeg') {
                header('Content-type: image/jpeg');
                $img->writeImage($imagepath);
            }
            if (strtolower($ext[$last]) == 'gif') {
                header('Content-type: image/gif');
                $img->writeImage($imagepath);
            }
            if (strtolower($ext[$last]) == 'png') {
                header('Content-type: image/png');
                $img->writeImage($imagepath);
            }
            $scaled_imgs[] = $scale_mylink . $targetWidth . "X" . $targetHeight . $img_bnk_size['IB_Path'];
        }
        header("Content-Type: text/plain");
        return $scaled_imgs;
    }
}

function Delete_Pic($Pic_Name) {
    if (is_file($Pic_Name)) {
        unlink($Pic_Name);
        return file_exists($Pic_Name);
    } else {
        return false;
    }
}

function Delete_Pic_Library($id) {
    $sql = "SELECT * FROM tbl_Image_Bank WHERE IB_ID = $id";
    $result = mysql_query($sql);
    $row = mysql_fetch_assoc($result);
    if ($row) {
        //Delete everything in Image Bank Usage table against this image
        $sqlUsage = "SELECT * FROM tbl_Image_Bank_Usage WHERE IBU_IB_ID = $id";
        $resultUsage = mysql_query($sqlUsage);
        while ($rowUsage = mysql_fetch_assoc($resultUsage)) {
            if ($rowUsage['IBU_BL_ID'] > 0) {
                if ($rowUsage['IBU_Main_Photo'] != '') {
                    if ($rowUsage['IBU_Main_Photo'] == 'Listing Thumbnail Image') {
                        $COLUMN_NAME = "BL_Photo";
                    } else {
                        $COLUMN_NAME = "BL_Header_Image";
                    }
                    $update = "UPDATE tbl_Business_Listing SET $COLUMN_NAME = '' WHERE BL_ID = '" . $rowUsage['IBU_BL_ID'] . "'";
                    $update = "UPDATE tbl_Business_Listing_Photo SET BLP_Photo = '' , BLP_Mobile_Photo = '' WHERE BLP_BL_ID = '" . $rowUsage['IBU_BL_ID'] . "' AND BLP_R_ID='" . $rowUsage['IBU_R_ID'] . "'";
                }if ($rowUsage['IBU_About_Photo'] != '') {
                    if ($rowUsage['IBU_About_Photo'] == 'About Us Main Image') {
                        $update = "DELETE FROM tbl_Business_Feature_About_Main_Photo WHERE BFAMP_BL_ID = '" . $rowUsage['IBU_BL_ID'] . "'";
                    } else {
                        $update = "DELETE FROM tbl_Business_Feature_About_Photo WHERE BFAP_ID = '" . $rowUsage['IBU_About_Photo'] . "'";
                    }
                }
                if ($rowUsage['IBU_Photo_Gallery'] != '') {
                    $update = "DELETE FROM tbl_Business_Feature_Photo WHERE BFP_ID = '" . $rowUsage['IBU_Photo_Gallery'] . "'";
                }if ($rowUsage['IBU_Our_Product'] != '') {
                    $update = "UPDATE tbl_Business_Feature_Product SET BFP_Photo = '' WHERE BFP_ID = '" . $rowUsage['IBU_Our_Product'] . "'";
                }if ($rowUsage['IBU_Daily_Features'] != '') {
                    $update = "UPDATE tbl_Business_Feature_Daily_Specials SET BFDS_Photo = '' WHERE BFDS_ID = '" . $rowUsage['IBU_Daily_Features'] . "'";
                }if ($rowUsage['IBU_Entertainment'] != '') {
                    $update = "UPDATE tbl_Business_Feature_Entertainment_Acts SET BFEA_Photo = '' WHERE BFEA_ID = '" . $rowUsage['IBU_Entertainment'] . "'";
                }if ($rowUsage['IBU_Agenda'] != '') {
                    $update = "DELETE FROM tbl_Business_Feature_Agendas_Minutes_Main_Photo WHERE BFAMMP_ID = '" . $rowUsage['IBU_Agenda'] . "'";
                }
            }if ($rowUsage['IBU_R_ID'] > 0) {
                if ($rowUsage['IBU_Region_Icon'] != '') {
                    $update = "UPDATE tbl_Region_Icon SET RI_Photo = '' WHERE RI_ID = '" . $rowUsage['IBU_Region_Icon'] . "'";
                }if ($rowUsage['IBU_Region_Theme'] != '') {
                    if ($rowUsage['IBU_Region_Theme'] == 'Desktop Logo') {
                        $update = "UPDATE tbl_Region_Themes SET RT_Desktop_Logo = '' WHERE RT_RID = '" . $rowUsage['IBU_R_ID'] . "'";
                    }
                    if ($rowUsage['IBU_Region_Theme'] != 'Desktop Logo' && $rowUsage['IBU_Region_Theme'] != "Map Icon" && $rowUsage['IBU_Region_Theme'] != "") {
                        $update = "DELETE FROM tbl_Region_Themes_Photos WHERE RTP_ID = '" . $rowUsage['IBU_Region_Theme'] . "'";
                    }
                    if ($rowUsage['IBU_Region_Theme'] == 'Map Icon') {
                        $update = "UPDATE tbl_Region_Themes SET RT_Map_Icon = '' WHERE RT_RID = '" . $rowUsage['IBU_R_ID'] . "'";
                    }
                }if ($rowUsage['IBU_Region_Footer'] != '') {
                    $update = "UPDATE tbl_Footer_Photo SET FP_Photo = '' WHERE FP_ID = '" . $rowUsage['IBU_Region_Footer'] . "'";
                }if ($rowUsage['IBU_Region_Category'] != '') { 
                    if ($rowUsage['IBU_Region_Category'] == 'Mobile Thumbnail Image') {
                        $update = "UPDATE tbl_Region_Category SET RC_Mobile_Thumbnail = '' WHERE RC_R_ID = '" . $rowUsage['IBU_R_ID'] . "' AND RC_C_ID = '" . $rowUsage['IBU_Region_Category_C_ID'] . "'";
                    } else if ($rowUsage['IBU_Region_Category'] == 'Main Image') {
                        $update = "UPDATE tbl_Region_Category SET RC_Image = '' WHERE RC_R_ID = '" . $rowUsage['IBU_R_ID'] . "' AND RC_C_ID = '" . $rowUsage['IBU_Region_Category_C_ID'] . "'";
                    } else if ($rowUsage['IBU_Region_Category'] == 'Thumbnail Image') {
                        $update = "UPDATE tbl_Region_Category SET RC_Thumbnail = '' WHERE RC_R_ID = '" . $rowUsage['IBU_R_ID'] . "' AND RC_C_ID = '" . $rowUsage['IBU_Region_Category_C_ID'] . "'";
                    } else {
                        $update = "UPDATE tbl_Region_Category SET RC_Thumbnail = '',  RC_Image = '',  RC_Thumbnail = ''
                                   WHERE RC_R_ID = '" . $rowUsage['IBU_R_ID'] . "' AND RC_C_ID = '" . $rowUsage['IBU_Region_Category'] . "'";
                    }
                }
            }if ($rowUsage['IBU_C_ID'] > 0) {
                $update = "UPDATE tbl_Category SET C_Icon = '' WHERE C_ID = '" . $rowUsage['IBU_C_ID'] . "'";
            }if ($rowUsage['IBU_F_ID'] > 0) {
                $update = "UPDATE tbl_Feature SET F_Image = '' WHERE F_ID = '" . $rowUsage['IBU_F_ID'] . "'";
            }
            ///// For story
//            if($rowUsage['IBU_S_ID'] != ''){
//                    $update = "UPDATE tbl_Story SET S_Thumbnail = '' WHERE S_ID = '" . $rowUsage['IBU_S_ID'] . "'";
//                }
           if ($rowUsage['IBU_Region_Category'] > 0) { 
                $update1 = "UPDATE tbl_Region_Category SET RC_Image = '' WHERE RC_R_ID= '" . $rowUsage['IBU_R_ID'] . "' AND RC_C_ID = '" . $rowUsage['IBU_Region_Category'] . "'";
                $update = "DELETE FROM tbl_Region_Category_Photos WHERE RCP_R_ID = '" . $rowUsage['IBU_R_ID'] . "' AND RCP_C_ID = '" . $rowUsage['IBU_C_ID'] . "'";
            }
            if ($rowUsage['IBU_S_ID'] > 0) {
                $update = "UPDATE tbl_Support SET S_Thumbnail = '' WHERE S_ID = '" . $rowUsage['IBU_S_ID'] . "'";
            }if ($rowUsage['IBU_Coupon_Photo'] != '') {
                $update = "UPDATE tbl_Business_Feature_Coupon SET BFC_Thumbnail = '', 
                   BFC_Main_Image = '' WHERE BFC_ID = '" . $rowUsage['IBU_Coupon_Photo'] . "'";
            }
            if ($rowUsage['IBU_Route'] > 0) {
                $update = "UPDATE tbl_Route SET R_Photo = '', R_Mobile_Photo='' WHERE R_ID = '" . $rowUsage['IBU_Route'] . "'";
            }
            if ($rowUsage['IBU_Route_Category'] > 0) {
                $update = "UPDATE tbl_Route_Category SET RC_Photo = '', RC_Mobile_Photo='', RC_Feature_Photo='' WHERE RC_ID = '" . $rowUsage['IBU_Route_Category'] . "'";
            }
            if ($rowUsage['IBU_Individual_Route'] > 0) {
                if($rowUsage['IBU_Individual_Route_Gallery'] > 0){
                   $update = "DELETE FROM tbl_Individual_Route_Gallery  WHERE IRG_ID = '" . $rowUsage['IBU_Individual_Route_Gallery'] . "' AND IRG_IR_ID= '".$rowUsage['IBU_Individual_Route']."'"; 
                }else{
                $update = "UPDATE tbl_Individual_Route SET IR_Photo = '', IR_Mobile_Photo='', IR_Feature_Photo='' WHERE IR_ID = '" . $rowUsage['IBU_Individual_Route'] . "'";
                }
                
                }
            if ($rowUsage['IBU_Region_Theme'] == 'Page Not Found') {
                $update = "UPDATE tbl_Region_404 SET RP_Photo = '', RP_Photo_Mobile= '' WHERE RP_RID = '" . $rowUsage['IBU_R_ID'] . "'";
            }
            if ($rowUsage['IBU_Map_Header_Image'] > 0) {
                $update = "UPDATE tbl_Region SET R_Map_Header_Image = '' WHERE R_ID= '" . $rowUsage['IBU_R_ID'] . "'";
            }
            
            if ($rowUsage['IBU_Region_Theme'] > 0) {
                $update = "DELETE FROM tbl_Region_Themes_Photos WHERE RTP_ID = '" . $rowUsage['IBU_Region_Theme'] . "' AND RTP_RT_RID= '" . $rowUsage['IBU_R_ID'] . "'";
            }
            
            //echo $update; exit;
            mysql_query($update);
             mysql_query($update1);
        }
        $deleteUsage = "DELETE FROM tbl_Image_Bank_Usage WHERE IBU_IB_ID = $id";
        mysql_query($deleteUsage);
        //Delet everything from Image Bank Multiple table against this image
        $sqlMultiple = "SELECT * FROM tbl_Image_Bank_Multiple_Images WHERE IBMI_IB_ID = $id";
        $resultMultiple = mysql_query($sqlMultiple);
        while ($rowMultiple = mysql_fetch_assoc($resultMultiple)) {
            Delete_Pic(IMG_LOC_ABS . $rowMultiple['IBMI_Path']);
        }
        $deleteAllMultipleImages = "DELETE FROM tbl_Image_Bank_Multiple_Images WHERE IBMI_IB_ID = $id";
        mysql_query($deleteAllMultipleImages);
        //Delete Everything From Image Bank against this image
        Delete_Pic(IMG_BANK_ABS . $row['IB_Path']);
        Delete_Pic(IMG_BANK_ABS . $row['IB_Thumbnail_Path']);
        Delete_Pic(IMG_BANK_ABS . $row['IB_Detail_Path']);
        $deleteImage = "DELETE FROM tbl_Image_Bank WHERE IB_ID = $id";
        mysql_query($deleteImage);
    }
    return 1;
}

function Clean_FileName($filename) {
    $string = str_replace(' ', '-', $filename); // Replaces all spaces with hyphens.
    return preg_replace('/[^a-zA-Z0-9.]/', '', $string); // Removes special chars.
}

function Update_Image_Bank_Listings($IB_ID, $BL_ID) {
    $sql = "SELECT IB_Listings FROM tbl_Image_Bank WHERE IB_ID = '" . $IB_ID . "'";
    $result = mysql_query($sql);
    $row = mysql_fetch_assoc($result);
    if ($row['IB_Listings'] == "") {
        $listings = $BL_ID;
    } else {
        $list = explode(',', $row['IB_Listings']);
        $listings = $row['IB_Listings'] . "," . $BL_ID;
    }
    if (!in_array($BL_ID, $list)) {
        $sql = "UPDATE tbl_Image_Bank SET IB_Listings = '" . $listings . "' WHERE IB_ID = '" . $IB_ID . "'";
        $result = mysql_query($sql);
    }
}

function Upload_Pic_Normal($i, $var = 'photo', $maxW = 0, $maxH = 0, $strict = false, $location = IMG_LOC_ABS, $maxFileSize = 0, $rename = true, $name = '') {
    $ext = ".jpg";

    $imgSize = getimagesize($_FILES[$var]['tmp_name'][$i]);
    if (!$imgSize) {
        return 0;
    }

    if ($imgSize[2] == 1) {
        $ext = ".gif";
    } elseif ($imgSize[2] == 2) {
        $ext = ".jpg";
    } elseif ($imgSize[2] == 3) {
        $ext = ".png";
    } elseif ($imgSize[2] == 13) {
        $ext = ".swf";
    } else {
        $_SESSION[SESSIONERROR] = "File Type NOT Allowed: " . $imgSize[2];
        return "0";
    }
    if (file_exists($_FILES[$var]['tmp_name'][$i]) == true && $_FILES[$var]["error"][$i] == UPLOAD_ERR_OK && is_writable($location)) {
        $scale_mylink = "";
        if ($rename == true) {
            do {
                $myLink = mt_rand(1000, 9999999);
                $scale_mylink = $myLink;
            } while (file_exists($location . $myLink . $ext) == true);
        } elseif ($rename == false && $name != '') {
            $myLink = $name;
        } else {
            $myLink = str_ireplace($ext, '', Clean_FileName($_FILES[$var]['name'][$i]));
            $addRand = mt_rand(1000, 9999999);
            $myLink = $addRand . '-' . $myLink;
            $scale_mylink = $addRand;
        }
        if (move_uploaded_file($_FILES[$var]['tmp_name'][$i], $location . $myLink . $ext)) {
            return $myLink . $ext;
        } else {
            return '';
        }
    }
    return '';
}

function setImageCompressionQuality($imagePath, $quality) {
//    print_r($imagePath);exit;
    $imagick = new Imagick(IMG_LOC_ABS . $imagePath);
    $imagick->setImageCompressionQuality($quality);
    header('Content-type: image/png');
    $imagick->writeImage(IMG_LOC_ABS . 'compressed.png');
}

?>