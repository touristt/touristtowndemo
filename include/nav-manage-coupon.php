<?php 
$filename = basename($_SERVER["SCRIPT_FILENAME"], '.php');
if($filename == 'active-listings-coupon') {
    $action = 'active-listings-coupon';
} else {
    $action = 'listings-coupon';
}
?>
<div class="sub-menu">
    <ul>
        <?PHP  if (in_array('superadmin', $_SESSION['USER_ROLES'])) { ?>
            <li><a href="/admin/active-listings-coupon.php">Active Coupons</a></li>
            <li><a href="/admin/listings-coupon.php">Pending Coupons</a></li>
            <li><a href="/admin/expired-listings-coupon.php">Expired Coupons</a></li>
            <li><a href="/admin/coupons-stats.php">Coupons Stats</a></li>
            <form id="form1" name="form1" method="GET" action="/admin/<?php echo (isset($action)) ? $action : ''; ?>.php">
                <li class="search-field">
                    <input name="strSearch" placeholder="Search Coupon" type="text" id="strSearch" size="20" />
                </li>
                <li>
                    <input type="submit" name="button" id="button" value="Search" />
                </li>
            </form>
        <?PHP } ?>
    </ul>
</div>