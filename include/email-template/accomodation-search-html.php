<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--
body {
	background-color: #EEE;
}
body p,  body li {
	font-family: Verdana, Geneva, sans-serif;
}
body p,  body li{
	font-size: 11px;
	line-height: 18px;
}
.bold {
	font-weight: bold;
}
-->
</style></head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="50" cellpadding="0">
          <tr>
            <td valign="middle"><img src="http://touristtown.ca/images/eamil-touristtown-logo.gif" width="195" height="43" align="absmiddle" /></td>
            <td align="right" valign="middle"><img src="http://touristtown.ca/images/email-saugeen-shores-logo.png" width="150" height="60" align="absmiddle" /></td>
            </tr>
              <tr>
                <td colspan="2"><p>Dear 
                    <?=$row['B_Contact']?>,</p>
                  <p>
                    
                     The Saugeen Shores Chamber of Commerce has been working with Tourist Town and we are pleased to announce improved features for the membership on Saugeen Shores offical tourism website.</p>
                  <p>
                    
                     Starting February 1st, 2013, visitors to the tourism website will be able to search for accomodation in Saugeen Shores, Port Elgin and Southampton, based on the number of bedrooms and various frequently asked for amenities. We are very excited about this new feature.</p>
                  <p>Please note, your business will only show up in search results if you update your listing. Please take a few minutes to update your listing on the Tourism website.</p>
                  <p>To update your listing</p>
                  <ul><li> Login into your account (<a href="http://my.touristtown.ca" target="_blank">my.touristtown.ca</a>)</li><li> (at top) Click on &quot;Listings&quot; </li><li> (on right) Click &quot;Edit&quot;</li><li> (on left) Click &quot;Amenities&quot;</li><li> Scroll down to the bottom and select the number of bedrooms your business offers. You may select multiple room counts. Also update your amenities and upload a photo of your business while you are there!</li></ul>
                  <p>
                     Please ensure you update your listing so that your business shows up in these new search results. If you have any questions about this new feature, please call Tourist Town 519.832.3322 and speak with Mini Jacques.<br />
                    
                      <br />
                    
                     Enjoy,<br />
                    
                     The Tourist Town Team<br />
                  </p></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>