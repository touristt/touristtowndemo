<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--
body {
	background-color: #EEE;
}
body p,  body li {
	font-family: Verdana, Geneva, sans-serif;
}
body p,  body li{
	font-size: 11px;
	line-height: 18px;
}
.bold {
	font-weight: bold;
}
-->
</style></head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="50" cellpadding="0">
          <tr>
            <td valign="middle"><img src="http://touristtown.ca/images/eamil-touristtown-logo.gif" width="195" height="43" align="absmiddle" /></td>
            <td align="right" valign="middle"><img src="http://touristtown.ca/images/email-saugeen-shores-logo.png" width="150" height="60" align="absmiddle" /></td>
            </tr>
              <tr>
                <td colspan="2"><p>Dear 
                    <?=$row['B_Contact']?>
                    ,</p>
                  <p>
                    
                     A new mapping system has been added to the Saugeen Shores tourism website. The mapping system will allow visitors to find your listing using a map and will make planning their vacation and finding your business much easier. This mapping system uses Google as it's mapping engine. To ensure your listing shows up on the maps, you will need to follow the below steps. Please note your listing WILL NOT show up on the mapping system if you do not follow the below steps.</p>
                  <ol>
                    <li>    Login into your Tourist Town account. (<a href="http://my.touristtown.ca">my.touristtown.ca</a>)</li>
                    <li>Click on &quot;Business Profile&quot;</li>
                    <li>Towards the bottom of the form, you will see a title &quot;GPS&quot;. Next to GPS you will see Latitude and Longitude. This is where you will enter the longitude and latitude of your location from Google maps.</li>
                    <li>Open another browser window and go to <a href="http://www.maps.google.com">www.maps.google.com</a></li>
                    <li>Next to the Google logo, enter your address. (eg. 664 Goderich Street, Port Elgin, Ontario)</li>
                    <li>Click &quot;Search&quot; - on the magify glass icon</li>
                    <li>Google will load a map and show an orange symbol with the letter &quot;A&quot;.</li>
                    <li>Confirm the location is correct. If it is proceed to next step.</li>
                    <li>Right click on the &quot;A&quot;</li>
                    <li>A white box will appear.</li>
                    <li>Click on &quot;What's here?&quot;</li>
                    <li>At the top of the page (next to Google logo) you will see 2 numbers seperated by a comma. The first number will be a positive (Latitude) and the second number a negative (Longitude).</li>
                    <li>Select and copy the first number (eg. 44.437457)</li>
                    <li>Go to your Tourist Town account and paste the number into the Latitude field on your Business Profile</li>
                    <li>Go back to the google browser and copy the second number (eg. -81.388233) </li>
                    <li>Go to your Tourist Town account and paste the number into the Longitude field on your Business Profile</li>
                    <li>Confirm you have pasted the correct number into the correct field. In this example it would read: Latitude 44.437457 Longitude -81.388233</li>
                    <li>Click &quot;Submit&quot;</li>
                  </ol>
                  <p><a href="http://www.touristtown.ca/Marketing/TT-Tutorials-MapSettings.php" target="_blank">Click here to watch a Video Tutorial</a>.</p>
                  <p>If you have any issues with this process, please call Tourist Town 519.832.3322 and we will walk you through the process.</p>
                  <p>
                    
                     Enjoy,<br />
                    
                     The Tourist Town Team<br />
                  </p>
                  </td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>