Hello <?=$row['B_Contact']?>,

You are receiving this email as you have a listing on the Official Tourism Website for Saugeen Shores and you can now update it. To login to your account, please follow the below instructions:

To login, click here: my.touristtown.ca

Username: Is your email address used for your Chamber of Commerce membership

Your new password is: <?=$password? $password: 'Set By User'?>

After you login you can change this password using the "Change my Password" link.

To review your website listing on the Official Tourism Website of Saugeen Shores, click here: http://visitsaugeenshores.ca

You can update your listing as often as you like. If you require any assistance with updating your website, don't hesitate to contact me 519.832.3322

Thank you,
Mini Jacques

Tourist Town General Manager - Saugeen Shores
519.832.3322
