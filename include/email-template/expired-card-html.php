<?php

require_once '../config.inc.php';
require '../PHPMailer/class.phpmailer.php';
$NewDate = date('Y-m-d');
$sql = "SELECT * 
FROM  `payment_profiles` 
LEFT JOIN tbl_Business ON business_id = B_ID
LEFT JOIN tbl_Business_Listing ON BL_B_ID = B_ID
LEFT JOIN tbl_Business_Listing_Category_Region ON BL_ID = BLCR_BL_ID
LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
        WHERE card_expiration = DATE_SUB( CURDATE( ) , INTERVAL 1 DAY ) 
        AND BL_Listing_Type = 4 AND hide_show_listing = 1
GROUP BY B_ID";
$result = mysql_query($sql);

//Get Email Template
$email = "SELECT * FROM tbl_Email_Templates WHERE ET_ID = 7";
$res = mysql_query($email);
$rowEmail = mysql_fetch_assoc($res);
//$stripEmail = explode("<input name=", $rowEmail['ET_Template']);
//$stripEmail2 = explode("/>", $stripEmail[2]);

while ($row = mysql_fetch_array($result)) {
    $sql_region="SELECT BL_ID, BLCR_BLC_R_ID, BL_Points, R_Parent, RM_Parent, RM_Child ,R_Domain 
FROM tbl_Business_Listing
INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
INNER JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID
WHERE BL_ID =".$row['BL_ID']." ORDER BY RM_ID
LIMIT 5";
    $result_region=mysql_query($sql_region);
    $row_region = mysql_fetch_assoc($result_region);
    $no_of_days = strtotime($NewDate);
    $cardExpire = strtotime($row['card_expiration']);
    $total_days = $cardExpire - $no_of_days;
    $days = $total_days / 86400;
    $cardNo = explode("XXXX", $row['card_number']);
    $message_detail=str_replace("(Business Name here)", $row['BL_Contact'], $rowEmail['ET_Template']);
$message_detail =str_replace("(insert website and link)", '<a href="'.$row_region['R_Domain'].'">' . $row_region['R_Domain'] .'</a>', $message_detail);
 
    $message = '    
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Expiring Credit Card Notification</title>
            <body>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center"><table width="600" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td bgcolor="#FFFFFF">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="color: #858585;">
                                            <tr>
                                                <td colspan="2" style="border-bottom: 5px solid #f6f6f6; width: 100%; padding-bottom: 25px; ">
                                                    <span style="float: left;"><img src="http://' . DOMAIN . '/images/TouristTown-Logo.gif" width="195" height="43" /></span>
                                                    <span style="float: right; margin-top: 9px;">Expiring Credit Card Notification</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                '.$message_detail.'
                                                </td>
                                            </tr>
                                            <tr>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="middle" style="border-bottom: 5px solid #f6f6f6; padding-bottom: 25px; "></td>
                                                <td align="right" valign="middle" style="border-bottom: 5px solid #f6f6f6; padding-right: 10px; font-size: 15px;"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="font-size: 12px;">
                                                    <p>' . $rowEmail['ET_Footer'] . '</p>
                                                    <p>&nbsp;</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
    <!--                                <p>All costs are monthly. To update or change the credit card details we have for your account, please login at <a href="my.touristtown.ca" style="color: #6fab24; text-decoration: none;">my.touristtown.ca</a></p>-->
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table></td>
                    </tr>
                </table>
            </body>
    </html>';
//    print_r($message);
    //Send email One Month before 

        $mail = new PHPMailer();
        $mail->From = MAIN_CONTACT_EMAIL;
        $mail->AddReplyTo(MAIN_CONTACT_EMAIL, 'Tourist Town');
        $mail->FromName = MAIN_CONTACT_NAME;
        $mail->IsHTML(true);
        $mail->AddAddress($row['email']);
        $mail->AddBCC('majid.khan@app-desk.com');
        $mail->Subject = $rowEmail['ET_Subject'];
        $mail->MsgHTML($message);
        $mail->Send();
}
?>