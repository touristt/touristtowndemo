<?php
include '../config.inc.php';
$bbid = '2994';
$sql_billing = "SELECT * FROM tbl_Business_Billing 
                LEFT JOIN tbl_Business ON BB_B_ID = B_ID 
                LEFT JOIN tbl_Business_Listing ON BB_BL_ID = BL_ID 
                LEFT JOIN tbl_Payment_Status ON BB_Payment_Status = PS_ID 
                LEFT JOIN tbl_Payment_Type ON BB_Payment_Type = PT_ID 
                LEFT JOIN tbl_User ON BB_Account_Manager = U_ID
                LEFT JOIN tbl_Category ON BB_Category = C_ID
                LEFT JOIN payment_profiles ON business_id=BB_B_ID
                WHERE BB_ID = " . $bbid . " LIMIT 1";
$res_billing = mysql_query($sql_billing);
$bill = mysql_fetch_assoc($res_billing);
$sql_region = " SELECT R_Domain, BL_ID, BL_Name_SEO FROM tbl_Business_Listing
                INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                INNER JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID
                WHERE BL_ID =" . $bill['BL_ID'] . " GROUP BY R_ID ORDER BY RM_ID";
$result_region = mysql_query($sql_region);
$regions = '';
while ($row_region = mysql_fetch_assoc($result_region)) {
    if ($regions == '') {
        $regions .= '<a href="http://' . $row_region['R_Domain'] . '" target="_blank">' . $row_region['R_Domain'] . '</a>';
    } else {
        $regions .= ', <a href="http://' . $row_region['R_Domain'] . '" target="_blank">' . $row_region['R_Domain'] . '</a>';
    }
}
//billing period for which the amount was charged
//monthly billing
if ($bill['BL_Billing_Type'] == 1) {
    $start = strtotime("-1 month", strtotime($bill['BB_Renewal_Date']));
    $invoice_start_date = date("M jS, Y", $start);
    $billing_type = 'monthly';
} else if ($bill['BL_Billing_Type'] == -1) {
    $end = strtotime("-1 year", strtotime($bill['BB_Renewal_Date']));
    $invoice_start_date = date("M jS, Y", $end);
    $billing_type = 'annual';
}
$invoice_end_date = date("M jS, Y", strtotime("-1 day", strtotime($bill['BB_Renewal_Date'])));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Payment Confirmation</title>
        <body>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center"><table width="600" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td bgcolor="#FFFFFF">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left" valign="middle" style="border-bottom: 5px solid #f6f6f6; padding-bottom: 25px; "><img src="<?php echo DOMAIN . IMG_LOC_REL . $payment_region['R_Logo_Icon'] ?>" width="195" height="43" align="absmiddle" /></td>
                                            <td align="right" valign="middle" style="border-bottom: 5px solid #f6f6f6; padding-right: 10px; font-size: 15px;"><?php echo date('F d, Y', strtotime($bill['BB_Date'])) ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <p>&nbsp;</p>
                                                <p>Hi <?php echo $bill['BL_Contact'] ?> (<?php echo $bill['BL_Listing_Title'] ?>),</p>
                                                <p>Your <?php echo $billing_type; ?> purchases have been processed. Please save this order confirmation as a record of your purchase.</p>
                                                <p>You currently have a listing on (<?php echo $regions; ?>).</p>
                                                <p>&nbsp;</p>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border: 1px solid #e0e0e0; padding: 15px 10px;">
                                                    <tr>
                                                        <td colspan="4" style="border-bottom: 2px solid #f68c16;font-size: 14px; font-weight: bolder;padding: 15px 10px; color: #484848;">Invoice Summary</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="padding: 30px 0;">
                                                            <span style="font-weight: bolder; padding: 15px 10px; color: #484848;">Invoiced To</span>
                                                            <span style="display: block; padding: 15px 10px; color: #484848;">
                                                                <?php echo $bill['BL_Listing_Title'] != '' ? trim($bill['BL_Listing_Title']) : ''; ?>
                                                                <?php echo $bill['BL_Street'] != '' ? '<br>' . trim($bill['BL_Street']) : ''; ?>
                                                                <?php
                                                                echo $bill['BL_Town'] != '' ? '<br>' . trim($bill['BL_Town']) : '';
                                                                echo $bill['BL_Province'] != '' ? ', ' . trim($bill['BL_Province']) : '';
                                                                echo $bill['BL_PostalCode'] != '' ? ', ' . trim($bill['BL_PostalCode']) : '';
                                                                ?>
                                                                <br> Canada
                                                            </span>
                                                        </td>
                                                        <td colspan="2" style="padding: 30px 0;">
                                                            <span style="font-weight: bolder; padding: 15px 10px; color: #484848;">Pay To</span>
                                                            <span style="display: block; padding: 15px 10px; color: #484848;">
                                                                <?php echo $payment_region['R_Clientname'] != '' ? trim($payment_region['R_Clientname']) : ''; ?>
                                                                <?php echo $payment_region['R_Street'] != '' ? '<br>' . trim($payment_region['R_Street']) : ''; ?>
                                                                <?php
                                                                echo $payment_region['R_Town'] != '' ? '<br>' . trim($payment_region['R_Town']) : '';
                                                                echo $payment_region['R_Province'] != '' ? ', ' . trim($payment_region['R_Province']) : '';
                                                                echo $payment_region['R_PostalCode'] != '' ? ', ' . trim($payment_region['R_PostalCode']) : '';
                                                                ?>
                                                                <br>Canada
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="border-bottom: 1px solid #ededed"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="15%" style="font-weight: bolder; padding: 15px 10px; color: #484848;">Invoice #</td>
                                                        <td width="35%" style="font-weight: normal; padding: 15px 10px; color: #484848;"><?php echo $bill['BB_Invoice_Num'] ?></td>
                                                        <td width="15%" style="font-weight: bolder; padding: 15px 10px; color: #484848;">Listing #</td>
                                                        <td width="35%" style="font-weight: normal; padding: 15px 10px; color: #484848;"><?php echo str_pad($bill['BB_Listing'], 4, 0, STR_PAD_LEFT) ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="border-bottom: 1px solid #ededed"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight: bolder; padding: 15px 10px; color: #484848;">Category</td>
                                                        <td style="font-weight: normal; padding: 15px 10px; color: #484848;"><?php echo $bill['C_Name'] ?></td>
                                                        <td style="font-weight: bolder; padding: 15px 10px; color: #484848;">Invoice Date</td>
                                                        <td style="font-weight: normal; padding: 15px 10px; color: #484848;"><?php echo $bill['BB_Date'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="border-bottom: 1px solid #ededed"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="font-weight: bolder; padding: 15px 10px; color: #484848;">Credit Card: Last four digits of card</td>
                                                        <td style="font-weight: normal; padding: 15px 10px; color: #484848;">
                                                            <?php
                                                            $cardno = explode("XXXX", $bill['card_number']);
                                                            echo $cardno[1];
                                                            ?>
                                                        </td>
                            <!--                            <td style="font-weight: bolder; padding: 15px 10px; color: #484848;">&nbsp;</td>-->
                                                        <td style="font-weight: normal; padding: 15px 10px; color: #484848;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="border-bottom: 1px solid #ededed"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="border-bottom: 2px solid #f68c16;font-size: 14px; font-weight: bolder;padding: 10px; color: #484848;">Purchased Items</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="font-weight: normal;padding: 15px 10px; color: #484848;">Billing Period:</td>
                                                        <td colspan="2" align="right" style="font-weight: normal;padding: 15px 10px; color: #484848;"><?php echo $invoice_start_date . ' - ' . $invoice_end_date; ?></td>
                                                    </tr>
                                                    <?PHP
                                                    $sql_item = "SELECT * 
                                    FROM tbl_Business_Billing_Lines 
                                    WHERE BBL_BB_ID = " . $bill['BB_ID'];
                                                    $result_item = mysql_query($sql_item);
                                                    while ($lineitem = mysql_fetch_assoc($result_item)) {
                                                        ?>
                                                        <tr>
                                                            <td colspan="3" style="font-weight: normal;padding: 15px 10px; color: #484848;"><?php echo $lineitem['BBL_Title'] ?></td>
                                                            <td align="right" style="font-weight: normal;padding: 15px 10px; color: #484848;">$<?php echo $lineitem['BBL_Price'] ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4" style="border-bottom: 1px solid #ededed"></td>
                                                        </tr>
                                                        <?PHP
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="background: #fafafa; font-weight: bolder;padding: 15px 10px; color: #484848;">Sub-Total 1</td>
                                                        <td align="right" style="background: #fafafa; font-weight: bolder;padding: 15px 10px; color: #484848;">$<?php echo $bill['BB_SubTotal'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="font-weight: bolder;padding: 15px 10px;">Discount</td>
                                                        <td align="right" style="font-weight: bolder;padding: 15px 10px;">$<?php echo $bill['BB_CoC_Dis_2'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="background: #fafafa; font-weight: bolder;padding: 15px 10px;">Sub-Total 2</td>
                                                        <td align="right" style="background: #fafafa; font-weight: bolder;padding: 15px 10px;">$<?php echo $bill['BB_SubTotal3'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="font-weight: bolder;padding: 15px 10px;">HST on Sales (13%)</td>
                                                        <td align="right" style="font-weight: bolder;padding: 15px 10px;">$<?php echo $bill['BB_Tax'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="background: #fff3e5 none repeat scroll 0 0;font-weight: bolder;padding: 15px 10px;">Total</td>
                                                        <td align="right" style="background: #fff3e5 none repeat scroll 0 0;font-weight: bolder;padding: 15px 10px;">$<?php echo $bill['BB_Total'] ?></td>
                                                    </tr>
                                                </table>
                                                <p>All costs are <?php echo $billing_type; ?>. To update or change the credit card details we have for your account, please login at <a href="my.touristtown.ca" style="color: #6fab24; text-decoration: none;">my.touristtown.ca</a></p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
            </table>
        </body>
</html>