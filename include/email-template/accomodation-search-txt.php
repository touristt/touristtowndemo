Dear <?=$row['B_Contact']?>,

The Saugeen Shores Chamber of Commerce has been working with Tourist Town and we are pleased to announce improved features for the membership on Saugeen Shores offical tourism website.

Starting February 1st, 2013, visitors to the tourism website will be able to search for accomodation in Saugeen Shores, Port Elgin and Southampton, based on the number of bedrooms and various frequently asked for amenities. We are very excited about this new feature.

Please note, your business will only show up in search results if you update your listing. Please take a few minutes to update your listing on the Tourism website.

To update your listing
Login into your account (http://my.touristtown.ca)
(at top) Click on "Listings"
(on right) Click "Edit"
(on left) Click "Amenities"
Scroll down to the bottom and select the number of bedrooms your business offers. You may select multiple room counts. Also update your amenities and upload a photo of your business while you are there!

Please ensure you update your listing so that your business shows up in these new search results. If you have any questions about this new feature, please call Tourist Town 519.832.3322 and speak with Mini Jacques.

Enjoy,
The Tourist Town Team