<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Home Team - Member</title>

<link href="http://<?=DOMAIN?>/include/HT.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="600" border="0" cellspacing="0" cellpadding="20">
      <tr>
        <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr>
                <td colspan="5"  bgcolor="#FFFFFF" class="pageheadline">Home Team Order</td>
              </tr>
              <tr>
                <td colspan="5"  bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="56%" align="left" valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="1" cellpadding="0">
                          <tr>
                            <td bgcolor="#FFFFFF"><table width="108%" border="0" cellspacing="5" cellpadding="5">
                                <tr>
                                  <td colspan="2" align="left" valign="top" bgcolor="#CCCCCC" class="bold">Billing Information</td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" class="bold">Order#:</td>
                                  <td align="left" valign="top">
                                    <?='M' . $row['CO_M_ID'] . '-C-' . ($row['CO_MO_ID']>100? '': ($row['CO_MO_ID']>10?'0': '00')) . $row['CO_MO_ID']?>
                                  </td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" class="bold">Member Name: </td>
                                  <td align="left" valign="top"><?=$row['M_Name']?></td>
                                </tr>
                                <tr>
                                  <td width="38%" align="left" valign="top" class="bold">Home Team Member:</td>
                                  <td width="62%" align="left" valign="top">
                                    <?=$row['CO_Placed_By']?>
                                  </td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" class="bold">Date Ordered:</td>
                                  <td align="left" valign="top">
                                            
                                    <?=$row['CO_Order_Date_F']?>
                                          </td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top" class="bold">Ship to: </td>
                                  <td align="left" valign="top">
										    
                                    <?=$row['CO_Ship_To']?>
                                          </td>
                                </tr>
                      </table></td>
                          </tr>
                </table></td>
                    </tr>
            </table></td>
              </tr>
              <tr>
                <td colspan="5"  bgcolor="#FFFFFF" class="bold">&nbsp;</td>
              </tr>
              <tr>
                <td width="15%"  bgcolor="#CCCCCC" class="bold">Category</td>
                <td align="center"  bgcolor="#CCCCCC" class="bold">Thumbnail</td>
                <td align="center"  bgcolor="#CCCCCC" class="bold">Description</td>
                <td width="6%" align="center" bgcolor="#CCCCCC" class="bold">Size</td>
                <td width="9%" align="center" bgcolor="#CCCCCC" class="bold">Qty</td>
              </tr>
              <tr>
                <td align="left" bgcolor="#E6E6E6">
                  <?=$row['COL_Category']?>
                </td>
                <td width="13%" align="center" bgcolor="#E6E6E6"><img src="http://<?=DOMAIN?>/images/<?PHP if ($row['COL_Item_Photo']) {?>pic.php?pw=50&ph=50&pname=<?=$row['COL_Item_Photo']?><?PHP } else { echo 'spacer.gif'; } ?>" alt="" width="50" height="50" hspace="5" align="absmiddle" /></td>
                <td width="32%" align="center" bgcolor="#E6E6E6">
                  <?=$row['COL_Item_Name']?>
                </td>
                <td width="6%" align="center" bgcolor="#E6E6E6">
                  <?=$row['COL_Size_Name']==''? '-': $row['COL_Size_Name']?>
                </td>
                <td width="9%" align="center" bgcolor="#E6E6E6">
                  <?=$row['COL_Quantity']?>
                </td>
              </tr>
<?PHP 
			if ($row['CT_ID'] == 1) {
?>
              <tr>
                <td align="left" bgcolor="#E6E6E6">&nbsp;</td>
                <td colspan="4" align="left" bgcolor="#E6E6E6"><table width="100%" border="0" cellspacing="0" cellpadding="3">
                            <?PHP
	if ($row['C_ID'] == 1 || $row['C_ID'] == 2) {
?>
                            <tr>
                                
                              <td width="24%" align="left" valign="top" bgcolor="#E6E6E6">Name</td>
                              <td width="76%" align="left" valign="top" bgcolor="#E6E6E6">
                                <?=$row['COL_Name']?>
                              </td>
                            </tr>
                              
                            <tr>
                                
                              <td align="left" valign="top" bgcolor="#FFFFFF">Cell</td>
                              <td align="left" valign="top" bgcolor="#FFFFFF">
                                <?=$row['COL_Cell']?>
                              </td>
                            </tr>

                            <tr>
                                
                              <td align="left" valign="top" bgcolor="#E6E6E6">Email</td>
                              <td align="left" valign="top" bgcolor="#E6E6E6">
                                <?=$row['COL_Email']?>
                              </td>
                            </tr>
                            <?PHP
	}
?>
                            <tr>
                                
                              <td align="left" valign="top" bgcolor="#FFFFFF">Address</td>
                              <td align="left" valign="top" bgcolor="#FFFFFF">
                                <?=$row['COL_Address']?>
                              </td>
                            </tr>
                              
                            <tr>
                                
                              <td align="left" valign="top" bgcolor="#E6E6E6">City</td>
                              <td align="left" valign="top" bgcolor="#E6E6E6">
                                <?=$row['COL_City']?>
                              </td>
                            </tr>
                              
                            <tr>
                                
                              <td align="left" valign="top" bgcolor="#FFFFFF">Province</td>
                              <td align="left" valign="top" bgcolor="#FFFFFF">
                                <?=$row['COL_Province']?>
                              </td>
                            </tr>
                              
                            <tr>
                                
                              <td align="left" valign="top" bgcolor="#E6E6E6">P.Code</td>
                              <td align="left" valign="top" bgcolor="#E6E6E6">
                                <?=$row['COL_Postal_Code']?>
                              </td>
                            </tr>
                            <?PHP
	if ($row['C_ID'] <> 4) {
?>
                            <tr>
                                
                              <td align="left" valign="top" bgcolor="#FFFFFF">Phone</td>
                              <td align="left" valign="top" bgcolor="#FFFFFF">
                                <?=$row['COL_Phone']?>
                              </td>
                            </tr>
                              
                            <tr>
                                
                              <td align="left" valign="top" bgcolor="#E6E6E6">Fax</td>
                              <td align="left" valign="top" bgcolor="#E6E6E6">
                                <?=$row['COL_Fax']?>
                              </td>
                            </tr>
                            <?PHP
	}
?>
                            <tr>
                                
                              <td align="left" valign="top" bgcolor="#FFFFFF">Website</td>
                              <td align="left" valign="top" bgcolor="#FFFFFF">
                                <?=$row['COL_Website']?>
                              </td>
                            </tr>
                            <?PHP
	if ($row['C_ID'] == 2) {
?>
                            <tr>
                                
                              <td align="left" valign="top" bgcolor="#E6E6E6">Back Text</td>
                              <td align="left" valign="top" bgcolor="#E6E6E6">
                                <?=nl2br($row['COL_Back'])?>
                              </td>
                            </tr>
                            <?PHP
	}
?>
                            </table></td>
              </tr>
<?PHP
	}
?>
              <tr>
                <td colspan="5" align="left" bgcolor="#FFFFFF">If you have any questions regarding this order please contact Home Team Corporate at 519.376.1177.</td>
              </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
