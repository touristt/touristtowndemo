<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--
body {
	background-color: #EEE;
}
body p,  body li {
	font-family: Verdana, Geneva, sans-serif;
}
body p,  body li{
	font-size: 11px;
	line-height: 18px;
}
.bold {
	font-weight: bold;
}
-->
</style></head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="50" cellpadding="0">
          <tr>
            <td valign="middle"><img src="http://touristtown.ca/images/eamil-touristtown-logo.gif" width="195" height="43" align="absmiddle" /></td>
            <td align="right" valign="middle"><img src="http://touristtown.ca/images/email-saugeen-shores-logo.png" width="150" height="60" align="absmiddle" /></td>
            </tr>
              <tr>
                <td colspan="2"><p>Dear 
                    <?=$row['B_Contact']?>
                    ,</p>
                  <p>
                    
                     As a Saugeen Shores Chamber of Commerce member, you can now add a thumbnail photo to your listing on the Official Tourism website of Saugeen Shores. It is proven that users are more likely to click on a listing if there is a thumbnail photo. Please take the time to add a thumbnail photo to your listing by following the below instructions. If you have any questions how to do this, please contact Tourist Town 519.832.3322 or <a href="mailto:mini@touristtown.ca">mini@touristtown.ca</a>. The the Official Tourism website of Saugeen Shores is powered by a company called Tourist Town Online Solutions. </p>
                  <p>-----------------------------------------------------</p>
                  <p><strong>Add a Thumbnail image to your Listing</strong></p>
                  <p>Before you can upload a thumbnail image to your listing, you will need to have an image ready to upload. You can use a software program, such as Photoshop to resize the image to 170 pixels x 90 pixels, or you can use an online tool provided by Tourist Town.</p>
                  <p><strong>To use Tourist Town resize Tool:</strong></p>
                  <ol>
                    <li>    On the home page of your Tourist Town login ( <a href="http://my.touristtown.ca">my.touristtown.ca</a> ), at the bottom of page click on: &quot;<a href="http://resize.touristtown.ca">resize.touristtown.ca</a>&quot;</li>
                    <li>
                        This will open a browser window: <a href="http://resize.touristtown.ca">http://resize.touristtown.ca</a></li>
                    <li>
                        Click &quot;Browse...&quot; button</li>
                    <li>Select the photo you want to use</li>
                    <li>Click &quot;Upload&quot;</li>
                    <li>You will see your photo load into the &quot;Step 2&quot; section</li>
                    <li>Click on the dropdown menu &quot;Select Cropping Size&quot;.</li>
                    <li>Select &quot;Thumbnail - 170 x 90&quot;</li>
                    <li>You will notice a rectangle will appear with 8 nodes and a dotted line. This is the shape that your image will be saved as. Click on one of the 8 nodes and you can make the cropping area bigger or smaller. If you want to resize at 100% of the original size, click &quot;Set Size&quot;</li>
                    <li>When you are happy - click &quot;Save image Now&quot;.</li>
                    <li>The image will save to your computer using the same file name and dimensions.(e.g. &quot;myphoto-170x90.jpg)</li>
                  </ol>
                  <p>
                    <strong>To upload to your listing:</strong></p>
                  <ol>
                    <li>    Login ( <a href="http://my.touristtown.ca">my.touristtown.ca</a> )</li>
                    <li>
                        Click Listings tab at top of page</li>
                    <li>Click &quot;Edit&quot;</li>
                    <li>On left, click &quot;Main Listing Page&quot; </li>
                    <li>Scroll down to &quot;Thumbnail&quot;.</li>
                    <li>Click &quot;Browse&quot;</li>
                    <li>Browse your compute to find the file you want to upload.</li>
                    <li>Enter a name for the file in &quot;Thumbnail Description&quot;</li>
                    <li>Scroll to bottom of page and click &quot;Submit&quot;</li>
                  </ol>
                  <p><br />
                    
                      <br />
                    
                     Enjoy,<br />
                    
                     The Tourist Town Team<br />
                  </p></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>