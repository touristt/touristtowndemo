Dear <?=$row['B_Contact']?>,

As a Saugeen Shores Chamber of Commerce member, you can now add a thumbnail photo to your listing on the Official Tourism website of Saugeen Shores. It is proven that users are more likely to click on a listing if there is a thumbnail photo. Please take the time to add a thumbnail photo to your listing by following the below instructions. If you have any questions how to do this, please contact Tourist Town 519.832.3322 or mini@touristtown.ca. The the Official Tourism website of Saugeen Shores is powered by a company called Tourist Town Online Solutions. 

-----------------------------------------------------

Add a Thumbnail image to your Listing

Before you can upload a thumbnail image to your listing, you will need to have an image ready to upload. You can use a software program, such as Photoshop to resize the image to 170 pixels x 90 pixels, or you can use an online tool provided by Tourist Town.

To use Tourist Town resize Tool:

1. On the home page of your Tourist Town login ( http://my.touristtown.ca ), at the bottom of page click on: "http://resize.touristtown.ca"
2. This will open a browser window: http://resize.touristtown.ca
3. Click "Browse..." button
4. Select the photo you want to use
5. Click "Upload"
6. You will see your photo load into the "Step 2" section
7. Click on the dropdown menu "Select Cropping Size".
8. Select "Thumbnail - 170 x 90"
9. You will notice a rectangle will appear with 8 nodes and a dotted line. This is the shape that your image will be saved as. Click on one of the 8 nodes and you can make the cropping area bigger or smaller. If you want to resize at 100% of the original size, click "Set Size"
10. When you are happy - click "Save image Now".
11. The image will save to your computer using the same file name and dimensions.(e.g. "myphoto-170x90.jpg)

To upload to your listing:

1. Login ( http://my.touristtown.ca )
2. Click Listings tab at top of page
3. Click "Edit"
4. On left, click "Main Listing Page"
5. Scroll down to "Thumbnail".
6. Click "Browse"
7. Browse your compute to find the file you want to upload.
8. Enter a name for the file in "Thumbnail Description"
9. Scroll to bottom of page and click "Submit"

Enjoy,
The Tourist Town Team