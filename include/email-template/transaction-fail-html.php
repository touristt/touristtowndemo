<?php
if ($A_ID > 0) {
    $sql_billing = "SELECT * 
            FROM tbl_Advertisement
            LEFT JOIN tbl_Business_Listing ON A_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Listing_Category_Region ON BL_ID = BLCR_BL_ID
            LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
            WHERE A_ID = " . $A_ID . "
            GROUP BY A_ID";
} else {
    $sql_billing = "SELECT * 
            FROM tbl_Business_Listing
            LEFT JOIN tbl_Business_Listing_Category_Region ON BL_ID = BLCR_BL_ID
            LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
            WHERE BL_ID = " . $BL_ID . "
            GROUP BY BL_ID";
}
$result_billing = mysql_query($sql_billing);
$row_billing = mysql_fetch_assoc($result_billing);

//Get Email Template
$email = "SELECT * FROM tbl_Email_Templates WHERE ET_ID = 2";
$res_email = mysql_query($email);
$rowEmail = mysql_fetch_assoc($res_email);
$stripEmail = explode("<input name=", $rowEmail['ET_Template']);
$stripEmail2 = explode("/>", $stripEmail[2]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Payment Transaction Fail</title>
        <body>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center"><table width="600" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td bgcolor="#FFFFFF">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="color: #858585;">
                                        <tr>
                                            <td colspan="2" style="border-bottom: 5px solid #f6f6f6; width: 100%; padding-bottom: 25px; ">
                                                <span style="float: left;"><img src="http://<?php echo DOMAIN ?>/images/TouristTown-Logo.gif" width="195" height="43" /></span>
                                                <span style="float: right; margin-top: 9px;">Payment Transaction Fail</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <p>&nbsp;</p>
                                                <p>Hi <?php echo $row_billing['BL_Contact'] ?>,</p>
                                                <p style="font-family:arial,sans-serif;font-size:12.8px;line-height: 1.5em;">
                                                    The credit card for <?php echo (($row_billing['A_Title'] != "") ? $row_billing['A_Title'] : $row_billing['BL_Listing_Title'] ) ?> was unable to be processed and has been declined for the following reason.<br/>
                                                    <br/>
                                                    <div>
                                                        <b><?php echo $error_msg_email ?></b><br/>
                                                    </div>
                                                    <br/>
                                                </p>
                                                <p style="line-height: 1.5em;"><?php echo $stripEmail[0] ?></p>
                                                <p style="margin:5px 0">&nbsp;</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="middle"><a style="background-color: #fe880b; text-decoration: none; padding: 12px 15px;color: #FFF;float: right;margin-right: 5px;" href="http://my.touristtown.ca/customer-credit-card-details.php?update_cc=1">Update My Credit Card Details</a></td>
                                            <td align="right" valign="middle"><a style="background-color: #6dac0f;text-decoration: none;padding: 12px 15px;color: #FFF;float: left;margin-left: 5px;width: 180px;text-align: center;" href="http://<?php echo $row_billing["R_Domain"]?>/profile/<?php echo $row_billing["BL_Name_SEO"]?>/<?php echo $row_billing["BL_ID"]?>">View Your Page</a></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <p style="margin:5px 0;line-height: 1.5em;">&nbsp;</p>
                                                <p><?php echo $stripEmail2[1] ?></p>
                                                <p>&nbsp;</p>
                                                <p>The Tourist Town Team</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="middle" style="border-bottom: 5px solid #f6f6f6; padding-bottom: 25px; "></td>
                                            <td align="right" valign="middle" style="border-bottom: 5px solid #f6f6f6; padding-right: 10px; font-size: 15px;"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="font-size: 12px;">
                                                <p><?php echo $rowEmail['ET_Footer'] ?></p>
                                                <p>&nbsp;</p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
            </table>
        </body>
</html>