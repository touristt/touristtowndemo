
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Payment Confirmation</title>
        <body>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center"><table width="600" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td bgcolor="#FFFFFF">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left" valign="middle" style="border-bottom: 5px solid #f6f6f6; padding-bottom: 25px; "><img src="http://touristtown.ca/images/eamil-touristtown-logo.gif" width="195" height="43" align="absmiddle" /></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <p>&nbsp;</p>
                                                <p>Dear Customer,</p>
                                                <p>Your monthly purchases have been processed. Please save this order confirmation as a record of your purchase</p>
                                                <p>&nbsp;</p>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border: 1px solid #e0e0e0; padding: 15px 10px;">
                                                    <tr>
                                                        <td colspan="8" style="border-bottom: 2px solid #f68c16;font-size: 14px; font-weight: bolder;padding: 10px; color: #484848;">Purchased Ads</td>
                                                    </tr>
                                                    <tr>
                                                                <td colspan="3" style="font-weight: bold;padding: 15px 10px; color: #484848;">Title</td>
                                                                <td align="right" style="font-weight: bold;padding: 15px 10px; color: #484848;">Discount</td>
                                                                <td align="right" style="font-weight: bold;padding: 15px 10px; color: #484848;">Price</td>
                                                                <td align="right" style="font-weight: bold;padding: 15px 10px; color: #484848;">Subtotal</td>
                                                                <td align="right" style="font-weight: bold;padding: 15px 10px; color: #484848;">Tax</td>
                                                                <td align="right" style="font-weight: bold;padding: 15px 10px; color: #484848;">Total</td>
                                                            </tr>
                                                    <tr>
                                                                <td colspan="8" style="border-bottom: 1px solid #ededed"></td>
                                                            </tr>
                                                    <?php
                                                    while ($rowAd = mysql_fetch_assoc($result2)) {
//                                                        $sql3 = "SELECT * FROM tbl_Advertisement_Billing WHERE AB_A_ID = '" . $rowAd['A_ID'] . "' AND AB_Active_Date BETWEEN '$start_date' AND '$end_date'";
//                                                        $result3 = mysql_query($sql3, $db) or die("Invalid query: $sql3 -- " . mysql_error());
//                                                        while ($rowAB = mysql_fetch_assoc($result3)) {
                                                            $discount += $rowAd['A_Discount'];
                                                            $total_ad_cost += $rowAd['AT_Cost'];
                                                            $taxes += $rowAd['A_Tax'];
                                                            $subtotal += $rowAd['A_SubTotal'];
                                                            $total += $rowAd['A_Total'];
                                                            ?>
                                                            <tr>
                                                                <td colspan="3" style="font-weight: normal;padding: 15px 10px; color: #484848;"><?= $rowAd['A_Title'] . " (" . $rowAd['AT_Name'] . ")" ?></td>
                                                                <td align="right" style="font-weight: normal;padding: 15px 10px; color: #484848;">$<?= $rowAd['A_Discount'] ?></td>
                                                                <td align="right" style="font-weight: normal;padding: 15px 10px; color: #484848;">$<?= $rowAd['AT_Cost'] ?></td>
                                                                <td align="right" style="font-weight: normal;padding: 15px 10px; color: #484848;">$<?= $rowAd['A_SubTotal'] ?></td>
                                                                <td align="right" style="font-weight: normal;padding: 15px 10px; color: #484848;">$<?= $rowAd['A_Tax'] ?></td>
                                                                <td align="right" style="font-weight: normal;padding: 15px 10px; color: #484848;">$<?= $rowAd['A_Total'] ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="8" style="border-bottom: 1px solid #ededed"></td>
                                                            </tr>
                                                            <?PHP
//                                                        }
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td colspan="8">&nbsp;</td>
                                                    </tr>
<!--                                                    <tr>
                                                        <td colspan="7" style="font-weight: bolder;padding: 15px 10px;">Discount</td>
                                                        <td align="right" style="font-weight: bolder;padding: 15px 10px;">$<? // = $discount ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" style="background: #fafafa; font-weight: bolder;padding: 15px 10px; color: #484848;">Sub-Total</td>
                                                        <td align="right" style="background: #fafafa; font-weight: bolder;padding: 15px 10px; color: #484848;">$<? // = $subtotal ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="7" style="font-weight: bolder;padding: 15px 10px;">HST on Sales (13%)</td>
                                                        <td align="right" style="font-weight: bolder;padding: 15px 10px;">$<? // = $taxes ?></td>
                                                    </tr>-->
                                                    <tr>
                                                        <td colspan="3" style="background: #fff3e5 none repeat scroll 0 0;font-weight: bolder;padding: 15px 10px;">Total</td>
                                                        <td align="right" style="background: #fff3e5 none repeat scroll 0 0;font-weight: bolder;padding: 15px 10px;">$<?= $discount ?></td>
                                                        <td align="right" style="background: #fff3e5 none repeat scroll 0 0;font-weight: bolder;padding: 15px 10px;">$<?= $total_ad_cost ?></td>
                                                        <td align="right" style="background: #fff3e5 none repeat scroll 0 0;font-weight: bolder;padding: 15px 10px;">$<?= $subtotal  ?></td>
                                                        <td align="right" style="background: #fff3e5 none repeat scroll 0 0;font-weight: bolder;padding: 15px 10px;">$<?= $taxes ?></td>
                                                        <td align="right" style="background: #fff3e5 none repeat scroll 0 0;font-weight: bolder;padding: 15px 10px;">$<?= $total ?></td>
                                                    </tr>
                                                </table>
                                                <p>All costs are monthly. To update or change the credit card details we have for your account, please login at <a href="my.touristtown.ca" style="color: #6fab24; text-decoration: none;">my.touristtown.ca</a></p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
            </table>
        </body>
</html>