<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--
body {
	background-color: #EEE;
}
body p {
	font-family: Verdana, Geneva, sans-serif;
}
body p {
	font-size: 11px;
	line-height: 18px;
}
.bold {
	font-weight: bold;
}
-->
</style></head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="50" cellpadding="0">
          <tr>
            <td valign="middle"><img src="http://touristtown.ca/images/eamil-touristtown-logo.gif" width="195" height="43" align="absmiddle" /></td>
            <td align="right" valign="middle"><img src="http://touristtown.ca/images/email-saugeen-shores-logo.png" width="150" height="60" align="absmiddle" /></td>
            </tr>
          <tr>
            <td colspan="2"><p>Dear 
                    <?=$row['B_Contact']?>
                    ,</p>
                  <p>Your web listing on the Saugeen Shores Official Tourism website is now active. We noticed you have not yet logged in an updated your account. We invite you to login to review and update your account information. To date, the website has received over 100,000 visits. </p>
                  <p>Please save this email for future reference. Your account information:</p>
                  <p><span class="bold">Login URL:</span> <a href="http://my.touristtown.ca">my.touristtown.ca</a><br />
                <span class="bold">Username:</span> 
                    <?=$row['B_Email']?>
                    <br />
                <span class="bold">Password:</span> 
                    <?=$password? $password: 'Set By User'?>
                  </p>
                  <p>You can review how your listing will display in the website(s) at the below URLs:<br />
                    Saugeen Shores: <a href="http://www.visitsaugeenshores.ca">www.visitsaugeenshores.ca</a><br />
                    Port Elgin: <a href="mailto:www.visitportelgin.ca">www.visitportelgin.ca</a><br />
                    Southampton: <a href="mailto:www.visitsouthampton.ca">www.visitsouthampton.ca</a></p>
                  <p>If you have any questions about your account information or upgrading your account package, please contact your Tourist Town consultant, Mini Jacques 519.832.3322.</p>
                  <p><strong>Please note</strong>: The Official Tourism website of Saugeen Shores has been endorsed by The Town of Saugeen Shores and The Saugeen Shores Chamber of Commerce. When you first login you will be asked to accept the terms of use. These terms have been revised and endorsed by The Town of Saugeen Shores legal counsel.</p>
                  <p>
                    
                    Enjoy,<br />
                    The Tourist Town Team</p></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>