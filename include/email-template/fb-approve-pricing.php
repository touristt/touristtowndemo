<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>HT Approve Pricing</title>
<link href="http://<?=DOMAIN?>/include/HT.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="20">
  <tr>
    <td align="center" valign="top"><table width="600" border="0" cellspacing="0" cellpadding="20">
      <tr>

        <td width="167" align="center" bgcolor="#FFFFFF"><img src="http://<?=DOMAIN?>/images/HT-Logo-Email.jpg" width="131" height="53" alt="Home Team Logo" /></td>
        <td width="433" bgcolor="#FFFFFF" class="HeaderTagline">Flyer Builder <br />
          <span class="PageHeading">
            <?=str_pad($row['F_ID'], 3, '0', STR_PAD_LEFT)?>
             Approve Pricing</span></td>
      </tr>
      <tr>
        <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="5">
          <tr>
                <td colspan="2" bgcolor="#FFFFFF">Please see the below details for your upcoming Home Team Flyer .Login to your Home Team account to review the pricing. </td>

            </tr>
          <tr>
            <td bgcolor="#E6E6E6" class="bold">To:</td>
            <td bgcolor="#E6E6E6"><?=$row['M_Contact_Name']?></td>
            </tr>
          <tr>
            <td width="21%" bgcolor="#FFFFFF" class="bold">Flyer #:</td>

            <td width="79%" bgcolor="#FFFFFF"><?=str_pad($row['F_ID'], 3, '0', STR_PAD_LEFT)?></td>
          </tr>
          <tr>
            <td bgcolor="#E6E6E6" class="bold">Due Date:</td>
            <td bgcolor="#E6E6E6"><?=date_format(date_create($row['F_Member_Deadline']),'m/d/Y')?></td>
          </tr>
          <tr>

            <td class="bold">Distribution Date:</td>
            <td><?=date_format(date_create($row['F_Delivered_Deadline']),'m/d/Y')?></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>

            <td colspan="2" bgcolor="#FFFFFF">Please contact us if you have any questions or issue with the Flyer Build process.</td>
            </tr>
          </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
