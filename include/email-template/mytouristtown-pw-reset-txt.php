Good day <?=$row['BL_Contact']?>,
 
Your password to manage your business listing for Tourist Town has been reset.
 
Your new password is: <?=$new_pw?> 
 
Please save this email for future reference. Your account information:
 
Login URL: my.touristtown.ca
Username: <?=$row['B_Email']?> 
Password: <?=$new_pw?> 
 
If you have any questions about your account information or upgrading your account package, please contact us on info@touristtown.ca.
<!--If you have any questions about your account information or upgrading your account package, please contact your Tourist Town consultant, Mini Jacques 519.832.3322.-->
 
Enjoy,
The Tourist Town Team