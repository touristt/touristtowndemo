<html>
    <head>
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Coupon Receipt</title>
        <style>
            /* -------------------------------------
                INLINED WITH htmlemail.io/inline
            ------------------------------------- */
            /* -------------------------------------
                RESPONSIVE AND MOBILE FRIENDLY STYLES
            ------------------------------------- */
            @media only screen and (max-width: 620px) {
                table[class=body] h1 {
                    font-size: 28px !important;
                    margin-bottom: 10px !important;
                }
                table[class=body] p,
                table[class=body] ul,
                table[class=body] ol,
                table[class=body] td,
                table[class=body] span,
                table[class=body] a {
                    font-size: 16px !important;
                }
                table[class=body] .wrapper,
                table[class=body] .article {
                    padding: 10px !important;
                }
                table[class=body] .content {
                    padding: 0 !important;
                }
                table[class=body] .container {
                    padding: 0 !important;
                    width: 100% !important;
                }
                table[class=body] .main {
                    border-left-width: 0 !important;
                    border-radius: 0 !important;
                    border-right-width: 0 !important;
                }
                table[class=body] .btn table {
                    width: 100% !important;
                }
                table[class=body] .btn a {
                    width: 100% !important;
                }
                table[class=body] .img-responsive {
                    height: auto !important;
                    max-width: 100% !important;
                    width: auto !important;
                }
            }
            /*mobile*/
             @media only screen and (max-width: 414px) {
                table[class=body] h1 {
                    font-size: 28px !important;
                    margin-bottom: 10px !important;
                }
                table[class=body] p,
                table[class=body] ul,
                table[class=body] ol,
                table[class=body] td,
                table[class=body] span,
                table[class=body] a {
                    font-size: 16px !important;
                }
                table[class=body] .wrapper,
                table[class=body] .article {
                    padding: 10px !important;
                }
                table[class=body] .content {
                    padding: 0 !important;
                }
                table[class=body] .container {
                    padding: 0 !important;
                    width: 90% !important;
                }
                table[class=body] .main {
                    border-left-width: 0 !important;
                    border-radius: 0 !important;
                    border-right-width: 0 !important;
                }
                table[class=body] .btn table {
                    width: 90% !important;
                }
                table[class=body] .btn a {
                    width: 90% !important;
                }
                table[class=body] .img-responsive {
                    height: auto !important;
                    max-width: 90% !important;
                    width: auto !important;
                }
            }

            /* -------------------------------------
                PRESERVE THESE STYLES IN THE HEAD
            ------------------------------------- */
            @media all {
                .ExternalClass {
                    width: 100%;
                }
                .ExternalClass,
                .ExternalClass p,
                .ExternalClass span,
                .ExternalClass font,
                .ExternalClass td,
                .ExternalClass div {
                    line-height: 100%;
                }
                .apple-link a {
                    color: inherit !important;
                    font-family: inherit !important;
                    font-size: inherit !important;
                    font-weight: inherit !important;
                    line-height: inherit !important;
                    text-decoration: none !important;
                }
                .btn-primary table td:hover {
                    background-color: #34495e !important;
                }
                .btn-primary a:hover {
                    background-color: #34495e !important;
                    border-color: #34495e !important;
                }
            }
            p {
               line-height: 24px;
               font-family: arial,sans-serif;
               text-align: justify;
               padding: 0px;
               margin-left: 11px;
               margin-right: 13px;
               width: 88%;
             }
             img.logo_img {
                width: 192px !important;
             }
             img{
                 width: 100% !important;
             }
        </style>
    </head>
    <body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
        <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
            <tr>
                <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
                <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
                    <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">

                        <!-- START CENTERED WHITE CONTAINER -->
                        <span class="preheader" style="color: transparent;  height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">
                            <img src="http://'<?php echo DOMAIN ?>'/images/eamil-touristtown-logo.gif" width="195" height="43" align="absmiddle" />
                        </span>
                        <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">

                            <!-- START MAIN CONTENT AREA -->
                            <tr>
                                <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                                        <tr>
                                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                                                <p style="font-family: sans-serif; font-family: IdealSans-Light-Pro; font-size: 40px; font-weight: bold; margin: 0; Margin-bottom: 15px; color: #ef4d32;"> </p>
                                                <p style="font-family: sans-serif; font-family: IdealSans-Light-Pro; color: #525252; font-size: 17px; margin: 0; Margin-bottom: 15px;  border-bottom: 5px solid #f6f6f6; padding-bottom: 25px; ">
                                                    <img class="logo_img" src="http://<?php echo DOMAIN ?>/images/eamil-touristtown-logo.gif" width="195" height="43" align="absmiddle" />
                                                </p>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                                                <p style="font-family: sans-serif; font-family: IdealSans-Light-Pro; font-size: 40px; font-weight: bold; margin: 0; Margin-bottom: 15px; color: #ef4d32;"> </p>
                                                <p style="font-family: sans-serif; font-family: IdealSans-Light-Pro; color: #525252; font-size: 17px; margin: 0; Margin-bottom: 15px; margin-right: 10px;">
                                                    <?php
                                                    if (count($stripEmail) == 1) {
                                                        $message .= '<p style="line-height: 1.5em;margin-right: 10px;">' . $row['E_Body'] . '</p>';
                                                    } else {
                                                        $message .= '<p style="line-height: 1.5em; margin-right: 10px;">' . $stripEmail[0] . '</p>';
                                                    }
                                                    
                                                    ?>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                                                <p style="font-family: sans-serif; font-family: IdealSans-Light-Pro; font-size: 40px; font-weight: bold; margin: 0; Margin-bottom: 15px; color: #ef4d32;"> </p>
                                                <p style="font-family: sans-serif; font-family: IdealSans-Light-Pro; color: #525252; font-size: 17px; margin: 0; Margin-bottom: 15px;margin-right: 10px;">
                                                    <?php
                                                    if (count($stripEmail) > 1) {
                                                        $message .= '
                                                                <tr>
                                                                    <td align="left" valign="middle"><a style="background-color: #fe880b; text-decoration: none; padding: 12px 15px;color: #FFF;float: right;margin-right: 10px;" href="http://my.dev.touristtown.ca/customer-credit-card-details.php">Update My Credit Card Details</a></td>
                                                                    <td align="right" valign="middle"><a style="background-color: #6dac0f;text-decoration: none;padding: 12px 15px;color: #FFF;float: left;margin-right: 10px;margin-left: 5px;width: 180px;text-align: center;" href="http://' . $listing_row["R_Domain"] . '/profile/' . $listing_row["BL_Name_SEO"] . '/' . $listing_row["BL_ID"] . '">View Your Page</a></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2">
                                                                        <p style="margin:5px 0;line-height: 1.5em;">&nbsp;</p>
                                                                        <p>' . $stripEmail2[1] . '</p>
                                                                        <p>&nbsp;</p>';
                                                                        $message .= '<p>The TouristTown Team</p>';
                                                                        if ($listing_row['B_Subscription'] == 1) {
                                                                            $message .= '<p style="font-size: 10px; border-bottom: 5px solid #f6f6f6; padding-bottom: 25px;"><a href="http://' . BUSINESS_DOMAIN . '?logop=unsubscribe&key=' . $listing_row['B_ID'] . '">Unsubscribe</a></p>';
                                                                        }

                                                                        ' </td>
                                                                </tr>';
                                                    } else {
                                                        $message .= '<tr>
                                                                            <td colspan="2">
                                                                                <p>&nbsp;</p>';
                                                        $message .= '<p>The TouristTown Team</p>';
                                                        ;
                                                        if ($listing_row['B_Subscription'] == 1) {
                                                            $message .= '<p style="font-size: 10px; border-bottom: 5px solid #f6f6f6; padding-bottom: 25px;"><a href="http://' . BUSINESS_DOMAIN . '?logop=unsubscribe&key=' . $listing_row['B_ID'] . '">Unsubscribe</a></p>';
                                                        }

                                                        '</td>
                                                                        </tr>';
                                                    }
                                                    ?>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><?php echo $message;?></td>
                                            
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <!-- END MAIN CONTENT AREA -->
                        </table>
                    </div>
                </td>
                <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
            </tr>
        </table>
    </body>
</html>