<?php
$sql_billing = "SELECT * 
            FROM tbl_Advertisement
            LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID
            LEFT JOIN tbl_Advertisement_Billing ON AB_A_ID = A_ID
            LEFT JOIN tbl_Business ON A_B_ID = B_ID
            LEFT JOIN tbl_Business_Listing ON A_BL_ID = BL_ID
            LEFT JOIN tbl_Payment_Type ON A_Payment_Type = PT_ID
            LEFT JOIN payment_profiles ON business_id=A_B_ID
            WHERE AB_ID =  ". $AB_ID ." LIMIT 1";
$result2 = mysql_query($sql_billing);
$rowAd = mysql_fetch_assoc($result2);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Payment Confirmation</title>
        <body>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="center"><table width="600" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td bgcolor="#FFFFFF">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="left" valign="middle" style="border-bottom: 5px solid #f6f6f6; padding-bottom: 25px; "><img src="<?= DOMAIN . IMG_LOC_REL . $payment_region['R_Logo_Icon'] ?>" width="195" height="43" align="absmiddle" /></td>
                                            <td align="right" valign="middle" style="border-bottom: 5px solid #f6f6f6; padding-right: 10px; font-size: 15px;"><?= date('F d, Y', strtotime($rowAd['A_Active_Date'])) ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <p>&nbsp;</p>
                                                <p>Hi <?= $rowAd['BL_Contact'] ?> (<?= $rowAd['BL_Listing_Title'] ?>),</p>
                                                <p>You monthly purchases have been processed. Please save this order confirmation as a record of your purchase</p>
                                                <p>&nbsp;</p>
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border: 1px solid #e0e0e0; padding: 15px 10px;">
                                                    <tr>
                                                        <td colspan="4" style="border-bottom: 2px solid #f68c16;font-size: 14px; font-weight: bolder;padding: 15px 10px; color: #484848;">Invoice Summary</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="padding: 30px 0;">
                                                            <span style="font-weight: bolder; padding: 15px 10px; color: #484848;">Invoiced To</span>
                                                            <span style="display: block; padding: 15px 10px; color: #484848;">
                                                                <?php echo $rowAd['BL_Listing_Title'] != '' ? trim($rowAd['BL_Listing_Title']) : ''; ?>
                                                                <?php echo $rowAd['BL_Street'] != '' ? '<br>' . trim($rowAd['BL_Street']) : ''; ?>
                                                                <?php
                                                                echo $rowAd['BL_Town'] != '' ? '<br>' . trim($rowAd['BL_Town']) : '';
                                                                echo $rowAd['BL_Province'] != '' ? ', ' . trim($rowAd['BL_Province']) : '';
                                                                echo $rowAd['BL_PostalCode'] != '' ? ', ' . trim($rowAd['BL_PostalCode']) : '';
                                                                ?>
                                                                <br> Canada
                                                            </span>
                                                        </td>
                                                        <td colspan="2" style="padding: 30px 0;">
                                                            <span style="font-weight: bolder; padding: 15px 10px; color: #484848;">Pay To</span>
                                                            <span style="display: block; padding: 15px 10px; color: #484848;">
                                                                <?php echo $payment_region['R_Clientname'] != '' ? trim($payment_region['R_Clientname']) : ''; ?>
                                                                <?php echo $payment_region['R_Street'] != '' ? '<br>' . trim($payment_region['R_Street']) : ''; ?>
                                                                <?php
                                                                echo $payment_region['R_Town'] != '' ? '<br>' . trim($payment_region['R_Town']) : '';
                                                                echo $payment_region['R_Province'] != '' ? ', ' . trim($payment_region['R_Province']) : '';
                                                                echo $payment_region['R_PostalCode'] != '' ? ', ' . trim($payment_region['R_PostalCode']) : '';
                                                                ?>
                                                                <br>Canada
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="border-bottom: 1px solid #ededed"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="15%" style="font-weight: bolder; padding: 15px 10px; color: #484848;">Invoice #</td>
                                                        <td width="35%" style="font-weight: normal; padding: 15px 10px; color: #484848;"><?= $rowAd['AB_Invoice_Num'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="border-bottom: 1px solid #ededed"></td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight: bolder; padding: 15px 10px; color: #484848;">Invoice Date</td>
                                                        <td style="font-weight: normal; padding: 15px 10px; color: #484848;"><?= $rowAd['AB_Active_Date'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="border-bottom: 1px solid #ededed"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" style="font-weight: bolder; padding: 15px 10px; color: #484848;">Credit Card: Last four digits of card</td>
                                                        <td style="font-weight: normal; padding: 15px 10px; color: #484848;">
                                                            <?php
                                                            $cardno = explode("XXXX", $rowAd['card_number']);
                                                            echo $cardno[1];
                                                            ?>
                                                        </td>
                            <!--                            <td style="font-weight: bolder; padding: 15px 10px; color: #484848;">&nbsp;</td>-->
                                                        <td style="font-weight: normal; padding: 15px 10px; color: #484848;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="border-bottom: 1px solid #ededed"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4" style="border-bottom: 2px solid #f68c16;font-size: 14px; font-weight: bolder;padding: 10px; color: #484848;">Purchased Items</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="4">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="background: #fafafa; font-weight: bolder;padding: 15px 10px; color: #484848;">Advertisement Cost</td>
                                                        <td align="right" style="background: #fafafa; font-weight: bolder;padding: 15px 10px; color: #484848;">$<?= $rowAd['AT_Cost'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="font-weight: bolder;padding: 15px 10px;">Discount</td>
                                                        <td align="right" style="font-weight: bolder;padding: 15px 10px;">$<?= $rowAd['A_Discount'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="background: #fafafa; font-weight: bolder;padding: 15px 10px; color: #484848;">Sub-Total</td>
                                                        <td align="right" style="background: #fafafa; font-weight: bolder;padding: 15px 10px; color: #484848;">$<?= $rowAd['A_SubTotal'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="font-weight: bolder;padding: 15px 10px;">HST on Sales (13%)</td>
                                                        <td align="right" style="font-weight: bolder;padding: 15px 10px;">$<?= $rowAd['A_Tax'] ?></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" style="background: #fff3e5 none repeat scroll 0 0;font-weight: bolder;padding: 15px 10px;">Total</td>
                                                        <td align="right" style="background: #fff3e5 none repeat scroll 0 0;font-weight: bolder;padding: 15px 10px;">$<?= $rowAd['A_Total'] ?></td>
                                                    </tr>
                                                </table>
                                                <p>All costs are monthly. To update or change the credit card details we have for your account, please login at <a href="my.touristtown.ca" style="color: #6fab24; text-decoration: none;">my.touristtown.ca</a></p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table></td>
                </tr>
            </table>
        </body>
</html>