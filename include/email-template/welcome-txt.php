<?=$row['B_Contact']?>,

Thank you for choosing to be a Tourist Town member in the region of Saugeen Shores.

Your Tourist Town account is now active. We invite you to login to review and update your account information. Once you accept the agreement with Tourist Town, you will be able to update your account information.

Please save this email for future reference. Your account information:

Login URL: my.touristtown.ca
Username: <?=$row['B_Email']?>
Password: <?=$password? $password: 'Set By User'?>

If you don't know your password, you can reset it by going to the login page and clicking "Forgot Password?" Please note that your password is case sensitive.

You can review how your listing will display in the website(s) at the below URLs:
Saugeen Shores: www.visitsaugeenshores.ca
Port Elgin: www.visitportelgin.ca
Southampton: www.visitsouthampton.ca

You have been assigned a category (e.g. Things To Do). If you want your business listed in another category, please contact your Tourist Town consultant, Mini Jacques 519.832.3322.

Select the sub-category you would like to be listed in (e.g. Amusements). If you would like to be listed in additional sub-categories within your category, please contact your Tourist Town consultant, Mini Jacques.

As a business in Saugeen Shores you can pick a region (Port Elgin or Southampton) you want your business to be displayed in. You can only be listed in 1 region. Your business will be displayed in 1 regional website (Port Elgin or Southampton) as well as Saugeen Shores website. If you feel you should be listed in both regions please contact your Tourist Town consultant.

If you have any questions about your account information or upgrading your account package, please contact your Tourist Town consultant, Mini Jacques 519.832.3322.


Enjoy,
The Tourist Town Team