<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Home Team - Member</title>
<link href="http://<?=DOMAIN?>/include/HT.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="600" border="0" cellspacing="0" cellpadding="20">
      <tr>
        <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr>
                <td colspan="7"  bgcolor="#FFFFFF" class="pageheadline">Home Team Order (<?='M' . $data['CO_M_ID'] . '-C-' . ($data['CO_MO_ID']>100? '': ($data['CO_MO_ID']>10?'0': '00')) . $data['CO_MO_ID']?>)</td>
              </tr>
              <tr>
                <td colspan="7"  bgcolor="#FFFFFF">&nbsp;
</td>
              </tr>
              
              <tr>
                <td width="15%"  bgcolor="#CCCCCC" class="bold">Category</td>
                <td align="center"  bgcolor="#CCCCCC" class="bold">Thumbnail</td>
                <td align="center"  bgcolor="#CCCCCC" class="bold">Description</td>
                <td width="6%" align="center" bgcolor="#CCCCCC" class="bold">Size</td>
                <td width="9%" align="center" bgcolor="#CCCCCC" class="bold">Qty</td>
                <td width="12%" align="center" bgcolor="#CCCCCC" class="bold">Unit Price</td>
                <td width="13%" align="center" bgcolor="#CCCCCC" class="bold">Item Total</td>
              </tr>
              <?PHP
	$subtotal = 0;
	$sql = "SELECT * FROM tbl_C_Order_Line 
				LEFT JOIN tbl_C ON COL_C_ID = C_ID 
				LEFT JOIN tbl_C_Type ON C_CT_ID = CT_ID 
				LEFT JOIN tbl_C_Size ON COL_Size = S_ID 
				WHERE COL_CO_ID = '" . $data['CO_ID'] . "'
				ORDER BY CT_Name, C_Name";
	$result = mysql_query($sql, $db)
		or die("Invalid query: $sql -- " . mysql_error());
	while ($row = mysql_fetch_assoc( $result )) {
		$bg = ($bg == '#E6E6E6'? '#FFFFFF': '#E6E6E6');
?>
              <tr>
                <td align="left" bgcolor="<?=$bg?>">
                  <?=$row['CT_Name']?>
                </td>
                <td width="13%" align="center" bgcolor="<?=$bg?>"><img src="http://<?=DOMAIN?>/images/<?PHP if ($row['C_Image']) {?>pic.php?pw=50&ph=50&pname=<?=$row['C_Image']?><?PHP } else { echo 'spacer.gif'; } ?>" alt="" width="50" height="50" hspace="5" align="absmiddle" /></td>
                <td align="center" bgcolor="<?=$bg?>">
                  <?=$row['C_Name']?><?PHP if ($row['S_ID']) { echo "<br />(" . $row['S_Gender'] . "s)"; } ?>
                </td>
                <td align="center" bgcolor="<?=$bg?>">
                   <?PHP 
							  	if ($row['CT_ID'] == 2) { 
                                	echo $row['S_Size'];
								} else { echo "NA"; } 
						?>
                </td>
                <td align="center" bgcolor="<?=$bg?>">
                  <?PHP if ($row['CT_ID'] <> 1) { echo $row['COL_Quantity']; } else { 
								if ($row['COL_Quantity']=='1K')
									echo 1000;
								elseif ($row['COL_Quantity']=='2K')
									echo 2000;
								elseif ($row['COL_Quantity']=='3K')
									echo 3000;
								elseif ($row['COL_Quantity']=='4K')
									echo 4000;
								else 
									echo 5000;
								 } ?>
                </td>
                <td width="12%" align="center" bgcolor="<?=$bg?>">
                  $<?PHP if ($row['CT_ID'] <> 1) { echo $row['C_Unit_Price']; } else { echo $row['C_'.$row['COL_Quantity']]; } ?>
                </td>
                <td width="13%" align="center" bgcolor="<?=$bg?>">
                  <?PHP 
								if ($row['CT_ID'] <> 1) { 
									echo "$" . number_format($row['C_Unit_Price'] * $row['COL_Quantity'],2); 
									$subtotal += ($row['C_Unit_Price'] * $row['COL_Quantity']);
								} else { 
									echo "$" . $row['C_'.$row['COL_Quantity']]; 
									$subtotal += $row['C_'.$row['COL_Quantity']];
								} ?>
                </td>
              </tr>
              <?PHP 
			if ($row['CT_ID'] == 1) {
?>
                          
                      
              <tr>
                            
                        
                <td align="left" bgcolor="<?=$bg?>">&nbsp;</td>
                <td align="center" bgcolor="<?=$bg?>">&nbsp;</td>
                <td colspan="5" align="center" bgcolor="<?=$bg?>"><table width="100%" border="0" cellspacing="0" cellpadding="3">
                            
                    <?PHP
	if ($row['C_ID'] == 1 || $row['C_ID'] == 2) {
?>
                            
                    <tr>
                                
                              
                      <td width="24%" align="left" valign="top" bgcolor="#E6E6E6">Name</td>
                      <td width="76%" align="left" valign="top" bgcolor="#E6E6E6">
                                
                        <?=$row['COL_Name']?>
                              </td>
                    </tr>
                              
                            
                    <tr>
                                
                              
                      <td align="left" valign="top" bgcolor="#FFFFFF">Cell</td>
                      <td align="left" valign="top" bgcolor="#FFFFFF">
                                
                        <?=$row['COL_Cell']?>
                              </td>
                    </tr>

                            
                    <tr>
                                
                              
                      <td align="left" valign="top" bgcolor="#E6E6E6">Email</td>
                      <td align="left" valign="top" bgcolor="#E6E6E6">
                                
                        <?=$row['COL_Email']?>
                              </td>
                    </tr>
                            
                    <?PHP
	}
?>
                            
                    <tr>
                                
                              
                      <td align="left" valign="top" bgcolor="#FFFFFF">Address</td>
                      <td align="left" valign="top" bgcolor="#FFFFFF">
                                
                        <?=$row['COL_Address']?>
                              </td>
                    </tr>
                              
                            
                    <tr>
                                
                              
                      <td align="left" valign="top" bgcolor="#E6E6E6">City</td>
                      <td align="left" valign="top" bgcolor="#E6E6E6">
                                
                        <?=$row['COL_City']?>
                              </td>
                    </tr>
                              
                            
                    <tr>
                                
                              
                      <td align="left" valign="top" bgcolor="#FFFFFF">Province</td>
                      <td align="left" valign="top" bgcolor="#FFFFFF">
                                
                        <?=$row['COL_Province']?>
                              </td>
                    </tr>
                              
                            
                    <tr>
                                
                              
                      <td align="left" valign="top" bgcolor="#E6E6E6">P.Code</td>
                      <td align="left" valign="top" bgcolor="#E6E6E6">
                                
                        <?=$row['COL_Postal_Code']?>
                              </td>
                    </tr>
                            
                    <?PHP
	if ($row['C_ID'] <> 4) {
?>
                            
                    <tr>
                                
                              
                      <td align="left" valign="top" bgcolor="#FFFFFF">Phone</td>
                      <td align="left" valign="top" bgcolor="#FFFFFF">
                                
                        <?=$row['COL_Phone']?>
                              </td>
                    </tr>
                              
                            
                    <tr>
                                
                              
                      <td align="left" valign="top" bgcolor="#E6E6E6">Fax</td>
                      <td align="left" valign="top" bgcolor="#E6E6E6">
                                
                        <?=$row['COL_Fax']?>
                              </td>
                    </tr>
                            
                    <?PHP
	}
?>
                            
                    <tr>
                                
                              
                      <td align="left" valign="top" bgcolor="#FFFFFF">Website</td>
                      <td align="left" valign="top" bgcolor="#FFFFFF">
                                
                        <?=$row['COL_Website']?>
                              </td>
                    </tr>
                            
                    <?PHP
	if ($row['C_ID'] == 2) {
?>
                            
                    <tr>
                                
                              
                      <td align="left" valign="top" bgcolor="#E6E6E6">Back Text</td>
                      <td align="left" valign="top" bgcolor="#E6E6E6">
                                
                        <?=nl2br($row['COL_Back'])?>
                              </td>
                    </tr>
                            
                    <?PHP
	}
?>
                            </table></td>
              </tr>
              <?PHP
		}
	}
?>
              <tr>
                <td colspan="7" align="left" bgcolor="#FFFFFF">&nbsp;</td>
              </tr>
            </table>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="56%" align="left" valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="1" cellpadding="0">
                          <tr>
                            <td bgcolor="#FFFFFF"><table width="108%" border="0" cellspacing="5" cellpadding="5">
                                <tr>
                                  <td colspan="2" align="left" valign="top" bgcolor="#CCCCCC" class="bold">Billing Information</td>
                                </tr>
                                <tr>
                                  <td width="38%" align="left" valign="top" class="bold">Order#:</td>
                                  <td width="62%" align="left" valign="top"><?='M' . $data['CO_M_ID'] . '-C-' . ($data['CO_MO_ID']>100? '': ($data['CO_MO_ID']>10?'0': '00')) . $data['CO_MO_ID']?></td>
                                </tr>
                                <tr>
                                          
                                  <td align="left" valign="top" class="bold">Order Placed by:</td>
                                  <td align="left" valign="top">
                                            
                                    <?=$data['CO_Placed_By']?>
                                          </td>
                                </tr>
                                <tr>
                                          
                                  <td align="left" valign="top" class="bold">Date Ordered:</td>
                                  <td align="left" valign="top">
                                            
                                    <?=$data['CO_Order_Date_F']?>
                                          </td>
                                </tr>
                                <tr>
                                          
                                  <td align="left" valign="top" class="bold">Ship to: </td>
                                  <td align="left" valign="top">
										    
                                    <?=$data['CO_Ship_To']?>
                                          </td>
                                </tr>
                      
                      
                    </table></td>
                          </tr>
                </table></td>
                      <td width="44%" align="left" valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="1" cellpadding="0">
                          <tr>
                            <td bgcolor="#FFFFFF"><?PHP
											$tax = round($subtotal * 0.13,2);
											if ($data['CO_Shipping'] > 0) {
												$total = $subtotal + $tax + $data['CO_Shipping'];
												$ship = "$" . $data['CO_Shipping'];
											} else {
												$total = $subtotal + $tax;
												$ship = 'TBD';
											}
										?><table width="100%" border="0" cellspacing="5" cellpadding="5">
                                <tr>
                                  <td colspan="2" bgcolor="#CCCCCC" class="bold">Pricing</td>
                                </tr>
                                <tr>
                                  <td class="bold">Merchandise Sub-Total</td>
                                  <td align="right"> $
                                  <span class="bold">
                                            
                                    <?=number_format($subtotal,2)?>
                                            </span></td>
                                </tr>
                                <tr>
                                  <td class="bold">Taxes (13%)</td>
                                  <td align="right"> $
                                  <span class="bold">
                                            
                                    <?=number_format($tax,2)?>
                                            </span></td>
                                </tr>
                                <tr>
                                  <td class="bold"> Shipping</td>
                                  <td align="right">
                                  <span class="bold">
                                            
                                    <?=$ship?>
                                            </span></td>
                                </tr>
                                <tr>
                                  <td class="bold">Order Total</td>
                                  <td align="right"> $
                                  <span class="bold">
                                            
                                    <?=number_format($total,2)?>
                                            </span></td>
                                </tr>
                                <tr>
                                  <td colspan="2" align="center">&nbsp;</td>
                                </tr>
                    </table></td>
                          </tr>
                </table></td>
                    </tr>
                  </table>
		</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
