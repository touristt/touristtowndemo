<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--
body {
	background-color: #EEE;
}
body p {
	font-family: Verdana, Geneva, sans-serif;
}
body p {
	font-size: 11px;
	line-height: 18px;
}
.bold {
	font-weight: bold;
}
-->
</style></head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="50" cellpadding="0">
          <tr>
            <td valign="middle"><img src="http://touristtown.ca/images/eamil-touristtown-logo.gif" width="195" height="43" align="absmiddle" /></td>
            <td align="right" valign="middle"><img src="http://touristtown.ca/images/email-saugeen-shores-logo.png" width="150" height="60" align="absmiddle" /></td>
            </tr>
          <tr>
            <td colspan="2"><p><?=$row['B_Contact']?>,</p>
                  <p>Thank you for choosing to be a Tourist Town member in the region of Saugeen Shores.</p>
                  <p>Your Tourist Town account is now active. We invite you to login to review and update your account information. Once you accept the agreement with Tourist Town, you will be able to update your account information.</p>
                  <p>Please save this email for future reference. Your account information:</p>
                  <p><span class="bold">Login URL:</span> <a href="http://my.touristtown.ca">my.touristtown.ca</a><br />
                <span class="bold">Username:</span> <?=$row['B_Email']?><br />
                <span class="bold">Password:</span> <?=$password? $password: 'Set By User'?></p>
                  <p>If you don't know your password, you can reset it by going to the login page and clicking &quot;Forgot Password?&quot; Please note that your password is case sensitive.</p>
                  <p>You can review how your listing will display in the website(s) at the below URLs:<br />
                    Saugeen Shores: <a href="http://www.visitsaugeenshores.ca">www.visitsaugeenshores.ca</a><br />
                    Port Elgin: <a href="mailto:www.visitportelgin.ca">www.visitportelgin.ca</a><br />
                    Southampton: <a href="mailto:www.visitsouthampton.ca">www.visitsouthampton.ca</a></p>
                  <p>You have been assigned a category (e.g. Things To Do). If you want your business listed in another category, please contact your Tourist Town consultant, Mini Jacques 519.832.3322.</p>
                  <p>Select the sub-category you would like to be listed in (e.g. Amusements). If you would like to be listed in additional sub-categories within your category, please contact your Tourist Town consultant, Mini Jacques.</p>
                  <p>As a business in Saugeen Shores you can pick a region (Port Elgin or Southampton) you want your business to be displayed in. You can only be listed in 1 region. Your business will be displayed in 1 regional website (Port Elgin or Southampton) as well as Saugeen Shores website. If you feel you should be listed in both regions please contact your Tourist Town consultant.</p>
                  <p>If you have any questions about your account information or upgrading your account package, please contact your Tourist Town consultant, Mini Jacques 519.832.3322.</p>
                  <p><br />
                    Enjoy,<br />
                    The Tourist Town Team</p></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>