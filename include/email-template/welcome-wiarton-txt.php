<?=$row['B_Contact']?>,

Your web listing on the Wiarton Official Tourism website is now active. We noticed you have not yet logged in and updated your account. We invite you to login to review and update your account information. 

Please save this email for future reference. Your account information:

Login URL: my.touristtown.ca
Username: <?=$row['B_Email']?>
Password: <?=$password? $password: 'Set By User'?>

You can review how your listing will display in the website at the below URL:
Wairton: http://www.visitwiarton.ca

If you have any questions about your account information or upgrading your account package, please contact your Tourist Town consultant, Brad Smith 519.832.3322.

Please note: The Official Tourism website of Wiarton has been endorsed by The Town of South Bruce Peninsula and The Wiarton Chamber of Commerce. When you first login you will be asked to accept the terms of use.

Enjoy,
The Tourist Town Team