<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Official Tourism Website of Saugeen Shores</title>
<style type="text/css">
<!--
body {
	background-color: #EEE;
}
body p {
	font-family: Verdana, Geneva, sans-serif;
}
body p {
	font-size: 11px;
	line-height: 18px;
}
.bold {
	font-weight: bold;
}
-->
</style></head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="50" cellpadding="0">
          <tr>
            <td valign="middle"><img src="http://touristtown.ca/images/eamil-touristtown-logo.gif" width="195" height="43" align="absmiddle" /></td>
            <td align="right" valign="middle"><img src="http://touristtown.ca/images/email-saugeen-shores-logo.png" width="150" height="60" align="absmiddle" /></td>
            </tr>
          <tr>
            <td colspan="2"><p>Hello <?=$row['B_Contact']?>,</p>
                  <p>You are receiving this email as you have a listing on the Official Tourism Website for Saugeen Shores and you can now update it. To login to your account, please follow the below instructions:</p>
                  <p>To login, click here: <a href="http://my.touristtown.ca" target="_blank">my.touristtown.ca</a></p>
                  <p>Username: Is your email address used for your Chamber of Commerce membership</p>
                  <p>Your new password is: <?=$password? $password: 'Set By User'?></p>
                  <p>After you login you can change this password using the &quot;Change my Password&quot; link.</p>
                  <p>To review your website listing on the Official Tourism Website of Saugeen Shores, click here: http://visitsaugeenshores.ca</p>
                  <p>You can update your listing as often as you like. If you require any assistance with updating your website, don't hesitate to contact me 519.832.3322</p>
                  <p>Thank you,<br />
                    Mini Jacques</p>
                  <p>Tourist Town General Manager - Saugeen Shores<br />
                    519.832.3322</p></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>