Dear <?=$row['B_Contact']?>,

A new mapping system has been added to the Saugeen Shores tourism website. The mapping system will allow visitors to find your listing using a map and will make planning their vacation and finding your business much easier. This mapping system uses Google as it's mapping engine. To ensure your listing shows up on the maps, you will need to follow the below steps. Please note your listing WILL NOT show up on the mapping system if you do not follow the below steps.

1. Login into your Tourist Town account. (http://my.touristtown.ca)
2. Click on "Business Profile"
3. Towards the bottom of the form, you will see a title "GPS". Next to GPS you will see Latitude and Longitude. This is where you will enter the longitude and latitude of your location from Google maps.
4. Open another browser window and go to http://www.maps.google.com
5. Next to the Google logo, enter your address. (eg. 664 Goderich Street, Port Elgin, Ontario)
6. Click "Search" - on the magify glass icon
7. Google will load a map and show an orange symbol with the letter "A".
8. Confirm the location is correct. If it is proceed to next step.
9. Right click on the "A"
10. A white box will appear.
11. Click on "What's here?"
12. At the top of the page (next to Google logo) you will see 2 numbers seperated by a comma. The first number will be a positive (Latitude) and the second number a negative (Longitude).
13. Select and copy the first number (eg. 44.437457)
14. Go to your Tourist Town account and paste the number into the Latitude field on your Business Profile
15. Go back to the google browser and copy the second number (eg. -81.388233) 
16. Go to your Tourist Town account and paste the number into the Longitude field on your Business Profile
17. Confirm you have pasted the correct number into the correct field. In this example it would read: Latitude 44.437457 Longitude -81.388233
18. Click "Submit"

Watch a Video Tutorial: http://www.touristtown.ca/Marketing/TT-Tutorials-MapSettings.php

If you have any issues with this process, please call Tourist Town 519.832.3322 and we will walk you through the process.

Enjoy,
The Tourist Town Team