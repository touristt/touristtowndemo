Dear <?=$row['B_Contact']?>,

Your web listing on the Saugeen Shores Official Tourism website is now active. We noticed you have not yet logged in an updated your account. We invite you to login to review and update your account information. To date, the website has received over 100,000 visits. 

Please save this email for future reference. Your account information:

Login URL: my.touristtown.ca
Username: <?=$row['B_Email']?>
Password: <?=$password? $password: 'Set By User'?>

You can review how your listing will display in the website(s) at the below URLs:
Saugeen Shores: www.visitsaugeenshores.ca
Port Elgin: www.visitportelgin.ca
Southampton: www.visitsouthampton.ca

If you have any questions about your account information or upgrading your account package, please contact your Tourist Town consultant, Mini Jacques 519.832.3322.

Please note: The Official Tourism website of Saugeen Shores has been endorsed by The Town of Saugeen Shores and The Saugeen Shores Chamber of Commerce. When you first login you will be asked to accept the terms of use. These terms have been revised and endorsed by The Town of Saugeen Shores legal counsel.
 
Enjoy,
The Tourist Town Team