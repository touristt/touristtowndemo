<?php
require_once '../config.inc.php';
require '../PHPMailer/class.phpmailer.php';

$NewDate = date('Y-m-d');
$sql = "SELECT BL_ID, BL_Contact, BL_Payment_Type, BL_Listing_Title, BL_Renewal_Date, B_Email
        FROM tbl_Business_Listing
        LEFT JOIN tbl_Business ON B_ID = BL_B_ID
        WHERE BL_Renewal_Date <= DATE_ADD( CURDATE( ) , INTERVAL 1 MONTH )
        AND (BL_Payment_Type = 4 OR BL_Payment_Type = 3)
        AND BL_Listing_Type = 4 
        AND BL_Renewal_Date !=  '0000-00-00'
        AND BL_Billing_Type = -1
        AND BL_Total > 0
        AND hide_show_listing = 1";
$result = mysql_query($sql);

while ($row = mysql_fetch_array($result)) {
    //Get Email Template
    if ($row['BL_Payment_Type'] == 4) {
        $email = "SELECT * FROM tbl_Email_Templates WHERE ET_ID = 3";
        $res = mysql_query($email);
        $rowEmail = mysql_fetch_assoc($res);
    }
    if ($row['BL_Payment_Type'] == 3) {
        $email = "SELECT * FROM tbl_Email_Templates WHERE ET_ID = 6";
        $res = mysql_query($email);
        $rowEmail = mysql_fetch_assoc($res);
    }
    $sql_region = "SELECT BL_ID, BLCR_BLC_R_ID, BL_Points, R_Parent, RM_Parent, RM_Child ,R_Domain 
                    FROM tbl_Business_Listing
                    INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                    INNER JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                    LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID
                    WHERE BL_ID =" . $row['BL_ID'] . " ORDER BY RM_ID";
    $result_region = mysql_query($sql_region);
    $row_region = mysql_fetch_assoc($result_region);
    $no_of_days = strtotime($NewDate);
    $cardExpire = strtotime($row['BL_Renewal_Date']);
    $total_days = $cardExpire - $no_of_days;
    $days = $total_days / 86400;

    $message_detail = str_replace("(Business Name here)", $row['BL_Contact'], $rowEmail['ET_Template']);
    $message_detail = str_replace("(insert website and link)", '<a href="' . $row_region['R_Domain'] . '">' . $row_region['R_Domain'] . '</a>', $message_detail);

    $message = '    
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Expiring Credit Card Notification</title>
            <body>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center"><table width="600" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td bgcolor="#FFFFFF">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="color: #858585;">
                                            <tr>
                                                <td colspan="2" style="border-bottom: 5px solid #f6f6f6; width: 100%; padding-bottom: 25px; ">
                                                    <span style="float: left;"><img src="http://' . DOMAIN . '/images/TouristTown-Logo.gif" width="195" height="43" /></span>
                                                    <span style="float: right; margin-top: 9px;">Annual Billing Reminder</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                '.$message_detail.'
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="middle" style="border-bottom: 5px solid #f6f6f6; padding-bottom: 25px; "></td>
                                                <td align="right" valign="middle" style="border-bottom: 5px solid #f6f6f6; padding-right: 10px; font-size: 15px;"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="font-size: 12px;">
                                                    <p>' . $rowEmail['ET_Footer'] . '</p>
                                                    <p>&nbsp;</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table></td>
                    </tr>
                </table>
            </body>
    </html>';
    //Send email One Month before 
    if ($days == 30) {
        $mail = new PHPMailer();
        $mail->From = MAIN_CONTACT_EMAIL;
        $mail->AddReplyTo(MAIN_CONTACT_EMAIL, 'Tourist Town');
        $mail->FromName = MAIN_CONTACT_NAME;
        $mail->IsHTML(true);
        $mail->AddAddress($row['B_Email']);
//        $mail->Subject = "Your credit card ending $cardNo[1] is expiring in next month";
        $mail->Subject = $rowEmail['ET_Subject'];
        $mail->MsgHTML($message);
        $mail->Send();
    }
    if ($days == 15) {
        $mail = new PHPMailer();
        $mail->From = MAIN_CONTACT_EMAIL;
        $mail->AddReplyTo(MAIN_CONTACT_EMAIL, 'Tourist Town');
        $mail->FromName = MAIN_CONTACT_NAME;
        $mail->IsHTML(true);
        $mail->AddAddress($row['B_Email']);
//        $mail->Subject = "Your credit card ending $cardNo[1] is expiring in next two weeks ";
        $mail->Subject = $rowEmail['ET_Subject'];
        $mail->MsgHTML($message);
        $mail->Send();
        }
    }
?>