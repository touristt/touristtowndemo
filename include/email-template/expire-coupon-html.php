<?php
require_once '../config.inc.php';
require_once '../PHPMailer/class.phpmailer.php';

$NewDate = date('Y-m-d');

$sql = "SELECT BL_ID, BFC_ID, BL_Contact, B_Email, BFC_Expiry_Date, BFC_Title, BL_Email, BL_Listing_Title FROM tbl_Business_Feature_Coupon
        LEFT JOIN tbl_Business_Listing ON BFC_BL_ID = BL_ID
        LEFT JOIN tbl_Business ON BL_B_ID = B_ID
        WHERE BFC_Status = 1 AND BFC_Expiry_Date > CURDATE() AND BFC_Expiry_Date != '0000-00-00'";
$result = mysql_query($sql);
while ($row = mysql_fetch_array($result)) {
    $no_of_days = strtotime($NewDate);
    $cardExpire = strtotime($row['BFC_Expiry_Date']);
    $total_days = $cardExpire - $no_of_days;
    $days = $total_days / 86400;
    ob_start();
    include 'email-inlined-expire-coupon.php';
    $html = ob_get_contents();
    ob_clean();
    $emailTo = $row['B_Email'];
    if ($days == 3) {
        $mail = new PHPMailer();
        $mail->From = MAIN_CONTACT_EMAIL;
        $mail->AddReplyTo(MAIN_CONTACT_EMAIL, 'Tourist Town');
        $mail->FromName = MAIN_CONTACT_NAME;
        $mail->IsHTML(true);
        $mail->CharSet = 'UTF-8';
        $mail->AddAddress($emailTo);
        $mail->Subject = 'Coupon Expiry Notification';
        $mail->MsgHTML($html);
       $mail->Send();
    }
}
?>