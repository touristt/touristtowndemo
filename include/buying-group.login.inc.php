<?PHP
	define ('SESSIONERROR', 'BG_ERROR');
	define ('SESSIONINFO', 'BG_INFO');
	
	$sql_select = "SELECT * FROM tbl_Buying_Group_User LEFT JOIN tbl_Buying_Group ON BGU_Buying_Group_ID = BG_ID ";

if($_REQUEST['logop'] == "login"){
	$sql_log = $sql_select 
		. "WHERE BGU_Email 	= '" . encode_strings($_POST['LOGIN_USER'], $db) . "' "
		. "&& BGU_Password = '" . md5(encode_strings($_POST['LOGIN_PASS'], $db)) . "' ";
	$result_log = mysql_query($sql_log, $db)
		or die("Invalid query: $sql_log -- " . mysql_error());
	if (mysql_num_rows($result_log) <> 1) {
		loginScreen( $db );
		login_exit();
	} else {
		$BGUID = UpdateUserLog($result_log, $db);
	}
} elseif($_REQUEST['logop'] == 'logout') {
	$sql = "UPDATE tbl_Buying_Group_User SET BGU_EXPIRE = '0' " 
		."WHERE BGU_KEY = '" . $_SESSION['BG_USER_KEY'] . "' ";
	$result = mysql_query($sql, $db)
		or die("Invalid query: $sql -- " . mysql_error());
	$_SESSION['BG_USER_KEY']	= 0;
	$_SESSION['BG_ID'] 			= 0;
	$_SESSION['BG_NAME'] 		= '';
	$_SESSION['BG_USER_ID'] 	= '';
	$_SESSION['BG_USER_EM'] 	= '';
	$_SESSION['BG_user_online']		= '';
	header( "Location: " . BUYING_GROUP_DIR );
	exit();
} elseif($_REQUEST['logop']== "lost_pw" ){
	 my_session_destroy('member');
	 lost_PW( $db );
	 login_exit();
} elseif($_REQUEST['logop'] == "send_pw" ){
	if( $_POST[USER_EMAIL] != ''){
		$result_pw = mysql_query( "SELECT * FROM tbl_Buying_Group_User WHERE BGU_Email = '"
			.encode_strings($_POST[USER_EMAIL], $db) . "' ", $db );
		$num_row = mysql_num_rows( $result_pw );
		if( $num_row == 0 ){
			error_PW( 0, $db );
		}
		elseif( $num_row == 1 ){
			$row_pw = mysql_fetch_assoc( $result_pw );
			send_PW( $row_pw, $db );
		}
		else{
			error_PW( 2, $db );
		}
	}
	else{
		header( "Location: " . BUYING_GROUP_DIR );
	}
	login_exit();
} elseif($_REQUEST['logop'] == 'login_scr') {
	if ($_POST['js_test'] && $_POST['cookie_test']) {
		loginScreen( $db );
	} else {
		browserTestFailed($_POST['js_test'], $_POST['cookie_test'], $db);
	}
	login_exit();
} elseif($_SESSION['BG_USER_KEY'] != '0' && $_SESSION['BG_USER_KEY'] != ''){
	$sql_Check = $sql_select
		."WHERE BGU_KEY = '" . encode_strings($_SESSION['BG_USER_KEY'], $db) . "' "
		."&& BGU_EXPIRE > NOW() ";
	$result_Check = mysql_query($sql_Check, $db)
		or die("Invalid query: $sql_Check -- " . mysql_error());
	if(mysql_num_rows($result_Check) <> 1){
		my_session_destroy('member');
		loginScreen( $db );
		login_exit();
	}
	else{
		$BGUID = UpdateUserLog($result_Check, $db, $_SESSION['BG_USER_KEY']);
	}

} else{
	my_session_destroy('member');
	make_browser_test($db);
	login_exit();
}

if ($_REQUEST['logop'] == 'chpw') {
	if( $_POST[op] == "change" ){
		$err = '';
		if( $_POST[old_pw] != '' ){ 
			$sql = "SELECT * FROM tbl_Buying_Group_User WHERE BGU_ID = " . $MUID 
				. " && BGU_Password = '" . md5( $_POST[old_pw] ) . "' ";
			$result =  mysql_query( $sql, $db );
			if( $result ){
				if( mysql_num_rows( $result ) == 1 ){
					if( $_POST[new_pw] == $_POST[new_pw_confirm] ){
						if( strlen( $_POST[new_pw] ) > 4 ){
							$sql_pw = "UPDATE tbl_Buying_Group_User SET BGU_Password = '"
								. md5( $_POST[new_pw] ) . "' WHERE BGU_ID = "
								. $MUID . " LIMIT 1 ";
							if( mysql_query( $sql_pw, $db ) ){
								$err = '';
							}
							else{
								$err = 'Query Invalid';
							}
						}
						else{
							$err = 'Password must be at least 5 characters.';
						}
					}
					else{
						$err = 'New Passwords don\'t match';
					}
				}
				else{
					$err = 'Old password did\'t match';
				}
			}
			else{
				$err = 'Query Invalid';
			}
		}
		else{
			$err = 'Old Password was blank';
		}
		
		
		if( $err == '' ){
			ch_complete();
		}
		else{
			header( "Location: " . BUYING_GROUP_DIR . "?logop=chpw&err=" . urlencode( $err ) );
			exit();
		}
	}
	else{
		ch_form();
	}
	exit();
}

function UpdateUserLog($result, $db, $key = "0"){
	if($row = mysql_fetch_assoc($result)){
		$sql = "UPDATE tbl_Buying_Group_User SET BGU_EXPIRE = DATE_ADD('" . date("Y-m-d H:i:s") ."', "
			."INTERVAL 1 HOUR)";
		if($key == "0"){
			$key =  mt_rand(1000, 9999999);
			$sql .= ", BGU_KEY = '$key'";
			$_SESSION['BG_USER_KEY']	= $key;
			$_SESSION['BG_ID'] 			= $row[BG_ID];
			$_SESSION['BG_NAME'] 		= $row[BG_Name];
			$_SESSION['BG_BUSINESS_IN'] = $row[BG_Business_In];
			$_SESSION['BG_USER_ID'] 	= $row[BGU_ID];
			$_SESSION['BG_USER_EM'] 	= $row[BGU_Email];
			$_SESSION['BG_user_online']		= $row[BGU_F_Name] . " " . $row[BGU_L_Name];
			$_SESSION['BG_SUPER_USER'] 	= $row[BGU_Super_User];
		}
		$sql .= " WHERE BGU_ID = '$row[BGU_ID]' ";
		$result = mysql_query($sql, $db)
			or die("Invalid query: $sql -- " . mysql_error());
	}
	else{
		InvalidUser();
		login_exit();
	}
	return $row[BGU_ID];
}

function InvalidUser(){
	header("Location: " . BUYING_GROUP_DIR);
	login_exit();
}

function login_exit()
{
	exit();
}


function loginScreen( $db ){
	logon_function_header("Login Screen");
	echo '<table width="940" border="0" cellpadding="0" cellspacing="0" bgcolor="#676767" align="center">
              
        <tr>
                
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    
              <tr>
                      
                <td class="SectionHeader">Chamber of Commerce Login</td>
              </tr>
            </table></td>
        </tr>
              
        </table><table width="940" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
      
          <tr>
            <td bgcolor="#FFFFFF"><table width="800" border="0" cellspacing="20" cellpadding="0">
              <tr>
                <td>' . "<form name='form1' method='post' action='http://" . DOMAIN . BUYING_GROUP_DIR . "'>
		<input name='logop' type='hidden' id='logop' value='login'>
		<table width='500' align='center' border='0' cellpadding='0' cellspacing='15' >
                    <tr>
                      <td width='133' align='left' valign='top' ><strong>Username</strong></td>
                      <td width='322' align='left' valign='top'><input name='LOGIN_USER' id='LOGIN_USER' type='text' size='35' /></td>
                    </tr>
                    <tr>
                      <td align='left' valign='top'><strong>Password</strong></td>
                      <td align='left' valign='top'><input name='LOGIN_PASS' id='LOGIN_PASS' type='password' size='35' /></td>
                    </tr>
                    <tr>
                      <td align='left' valign='top'>&nbsp;</td>
                      <td align='left' valign='top'>
                        <input name='Submit' type='submit' value='Login Now' /></td>
                    </tr>
                    

                    
                  </table>
		
		
		</form>" . '</td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table>';
	logon_function_footer();
}
function lost_PW( $db ){
	logon_function_header("Password Reset");
	echo "<form name='form1' method='post' action='" . BUYING_GROUP_DIR . "'>
	<input name='logop' type='hidden' id='logop' value='send_pw'>
		<table width='500' border='0' align='center' cellpadding='5' cellspacing='0'>
		  <tr>
			<td>Email Address: </td>
			<td><input name='USER_EMAIL' type='text' id='USER_EMAIL'></td>
		  </tr>
		  <tr><td colspan='2'><div align='center'>
	  	<input type='submit' name='Submit' value='Submit'>
	  	&nbsp;
	  	<input type='reset' name='Submit2' value='Reset'>
		</div></td>
 		</tr>
		</table>
		<p align=\"center\"><a href=\"" . BUYING_GROUP_DIR . "\">Back to Admin Home</a> | <a href='http://" . DOMAIN . "'>Public 
  Site </a></p>
		</form>";
	logon_function_footer();
}
function send_PW( $row, $db ){
	
	$headers = make_mail_headers(MAIN_CONTACT_EMAIL, COMPANY_NAME);
	
	$new_pw = makeRandomPassword();
	
	$sub = BUYING_GROUP_DIR . " Password Reset";
	
	$msg = "Good day,\n\n"
		. "Your password for " . DOMAIN . BUYING_GROUP_DIR . " has been reset.\n\n"
		. "Your new password is: " . $new_pw . "\n\n"
		. "After you login please change this password using the \"Change my Password\" "
		. "on the Main Maintenance Page.\n\n"
		. "If you didn't request this please notify the office immediately.\n\n"
		. "Thank you,\n" . COMPANY_NAME . " \n\n";
	
	
	
	$sql = "UPDATE tbl_Buying_Group_User SET User_Password = '" . md5( $new_pw ) . "' "
		. "WHERE User_ID = '" . $row[User_ID] . "' LIMIT 1 ";
	
	mysql_query( $sql, $db );
	
	logon_function_header("Password Reset");
	echo "<table width='500' border='0' align='center' cellpadding='0' cellspacing='0'>
		  <tr>
			<td align='center'>";
		if( mail($row[User_Email], $sub, $msg, $headers, '-f'.MAIN_CONTACT_EMAIL)){
			echo "Your new password has been emailed to you.";
		}
		else{
			echo "There was an Error sending you new password contact the office.";
		}
		echo "</td>
		  </tr>
		  
		</table>";
		logon_function_footer();
}

function error_PW( $err, $db ){
	
	logon_function_header("Login Error");
	
	echo "
		<table width='500' border='0' align='center' cellpadding='0' cellspacing='0'>
		  <tr>
			<td><strong>Your request produced an error, please contact the office.</strong></td>
		  </tr>
		</table>";
	logon_function_footer();
}

function make_browser_test($db)
{
	logon_function_header("Browser Test", "my_onload_funtion();");
?>


<script language="JavaScript" type="text/JavaScript">

function browserCheck()
{
	document.getElementById('js_test').value = 1;
	if( cookiesAreEnabled() )
	{
		document.getElementById('cookie_test').value = 1
	}
	document.forms[0].submit();
	return true;
}

function cookiesAreEnabled() {
  SetCookie( "foo", "bar" );
  if ( GetCookie( "foo" ) ) {
    DeleteCookie( "foo" );
    return true;
  } else {
    return false;
  }
}

function GetCookie( name ) {
  var arg = name + "=";
  var alen = arg.length;
  var clen = document.cookie.length;
  var i = 0;
  while ( i < clen ) {
    var j = i + alen;
    if ( document.cookie.substring(i, j) == arg ) return getCookieVal(j);
    i = document.cookie.indexOf( " ", i ) + 1;
    if ( i == 0 ) break;
  }
  return null;
}

function DeleteCookie( name, path, domain ) {
  if ( GetCookie( name ) ) {
    document.cookie = name + "=" +
    ( ( path ) ? "; path=" + path : "" ) +
    ( ( domain ) ? "; domain=" + domain : "" ) +
    "; expires=Thu, 01-Jan-70 00:00:01 GMT";
  }
}

function SetCookie( name, value, expires, path, domain, secure ) {
  document.cookie = name + "=" + escape (value) +
  ( ( expires ) ? "; expires=" + expires.toGMTString() : "" ) +
  ( ( path ) ? "; path=" + path : "" ) +
  ( ( domain ) ? "; domain=" + domain : "" ) +
  ( ( secure ) ? "; secure" : "" );
}

/**
 *   Helper function for GetCookie()
 */
function getCookieVal( offset ) {
  var endstr = document.cookie.indexOf ( ";", offset );
  if ( endstr == -1 ) endstr = document.cookie.length;
  return unescape( document.cookie.substring( offset, endstr ) );
}


function my_onload_funtion() {
	setTimeout('browserCheck()',500);
}
  

</script>

<table width="940" border="0" cellpadding="0" cellspacing="0" bgcolor="#676767" align="center">
              
        <tr>
                
          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    
              <tr>
                      
                <td class="SectionHeader">Browser Test</td>
              </tr>
            </table></td>
        </tr>
              
        </table>
		<table width="940" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
      
          <tr>
            <td bgcolor="#FFFFFF"><table width="800" border="0" cellspacing="20" cellpadding="0">
              <tr>
                <td> 

<table width="500" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr> 
    <td valign="top" >
      <form name='form1' method='post' action='<?PHP echo $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING']; ?>'>
        <input type="hidden" id="logop" name="logop" value="login_scr" >
        <input type="hidden" id="js_test" name="js_test" value="0" >
        <input type="hidden" id="cookie_test" name="cookie_test" value="0" >
        <blockquote>
          <p>This page is performing a test on your browser to determine if it 
            meets the requirements to use this portion of the website. You should 
            be automatically redirected in 0.5 seconds. If you are not, please click 
            proceed.</p>
          <p>Required components:</p>
          <blockquote>
            <p>JavaScript</p>
            <p>Cookies</p>
          </blockquote>
          <p>These are required to ensure ease of use of this system.</p>
          <p>Thanks,</p>
        </blockquote>
        <p align="center"> 
<?PHP
	echo "<input type='submit' name='Submit' value='Submit'>";
?>
        </p>
      </form></td>
  </tr></table></td>
              </tr>
            </table></td>
          </tr>
        </table></td>
      </tr>
    </table>

<?PHP
	logon_function_footer();
}

function browserTestFailed($js_test, $cookie_test, $db)
{
	logon_function_header("Browser Test Failed");

	if (!$js_test) {
?>
	<p>Failed Javascript Test.</p>
<?PHP
	}
	if (!$cookie_test) {
?>
	<p>Failed Cookie Test.</p>
<?PHP
	}
	logon_function_footer();

}

function logon_function_header($title, $onload = '')
{
global $mainNav, $js, $docHome, $db, $config;
if ($title == '') {
	$title = COMPANY_NAME . "";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?PHP echo $title; ?></title>
<link href="/include/tt.css" rel="stylesheet" type="text/css" />
<?PHP
	echo '<link href="/css/ui-lightness/jquery-ui-custom.css" rel="stylesheet" type="text/css" />
		<script src="/include/jquery.min.js" type="text/javascript"></script>
		<script src="/include/jquery-ui.min.js" type="text/javascript"></script>
		<script src="/include/jquery.tablesorter.min.js" type="text/javascript"></script>
		<script src="/include/jquery.metadata.js" type="text/javascript"></script>
		<script src="/include/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
		<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" type="text/css" media="all" />
';
?>

<script src="/include/admin.js" type="text/javascript"></script>
<?PHP
	
	if (count($js) > 0) {
		foreach ($js as $val) {
			echo "<script type=\"text/javascript\" src=\"/include/" . $val . "\"></script>\n";
		}
	}
?>
<body onload="<?PHP echo $onload; ?>">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#FFFFFF"><?php require_once('topnav-bgadmin.php'); ?></td>
  </tr>
<?PHP
	if ($mainNav) {
?>
  <tr>
    <td><?php require_once($mainNav); ?></td>
  </tr>
<?PHP
	}
?>
  <tr>
    <td>
		
<?PHP
	if ($_GET['info'] <> '') {
		echo "<p class='info'>" . stripslashes(urldecode($_GET['info'])) . "</p>";
	}
	if ($_GET['err'] <> '') {
		echo "<p class='error'>" . stripslashes(urldecode($_GET['err'])) . "</p>";
	}
	if ($_SESSION['BG_INFO'] <> '') {
		echo "<p class='info'>" . stripslashes(urldecode($_SESSION['BG_INFO'])) . "</p>";
		$_SESSION['BG_INFO'] = "";
	}
	if ($_SESSION['BG_ERROR'] <> '') {
		echo "<p class='error'>" . stripslashes(urldecode($_SESSION['BG_ERROR'])) . "</p>";
		$_SESSION['BG_ERROR'] = "";
	}
}

function logon_function_footer()
{
?>

                      </td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
  </tr>
</table>
</body>
</html>



<?PHP
}

function ch_complete(){
	logon_function_header("Change Password");
	echo "
<p align=\"center\"><strong><font color=\"#00CC00\" size=\"+2\">Password Changed!</font></strong></p>";
logon_function_footer();

}
function ch_form(){
	logon_function_header("Change Password");
	echo "\n<form name=\"form1\" method=\"post\" action=\"" . BUYING_GROUP_DIR . "\">
<input type=\"hidden\" name=\"logop\" value=\"chpw\">
<input type=\"hidden\" name=\"op\" value=\"change\">
  <table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"5\" cellspacing=\"5\">
    <tr> 
      <td colspan='2'><h1>Change Password</h1></td>
    </tr>
	<tr> 
      <td width='50%'><div align=\"right\">Old Password</div></td>
      <td width='50%'> <input type=\"password\" name=\"old_pw\" > </td>
    </tr>
    <tr> 
      <td><div align=\"right\">New Password</div></td>
      <td><input type=\"password\" name=\"new_pw\"></td>
    </tr>
    <tr> 
      <td><div align=\"right\">Confirm New Password</div></td>
      <td><input type=\"password\" name=\"new_pw_confirm\"></td>
    </tr>
    <tr> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr align=\"center\"> 
      <td colspan=\"2\"><input type=\"submit\" name=\"Submit\" value=\"Submit\"></td>
    </tr>
  </table>
</form>";
logon_function_footer();
}


?>