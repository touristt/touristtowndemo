<div class="main-nav-image-admin">
    <div class="main-nav-image-search-admin">
        <div class="main-nav-image-search-text">
            <form onSubmit="return search_func()" id="auto_complete_form_submit">
                <div class="main-nav-image-search-image">
                    <input class="submit-autocomplete" type="submit" name="submit" value="submit">
                </div>
                <div class="main-nav-image-search-text-field">
                    <input type="text" id="autocomplete">    
                    <input type="hidden" id="keywords-searched" name="search-image" value="">    
                </div>
                <div class="main-nav-image-search-total-image">
                    <?php
                    if (isset($count)) {
                        if ($count > 1) {
                            echo $count . ' images';
                        } else {
                            echo $count . ' image';
                        }
                    }
                    ?> 
                </div>
            </form>
                <select name="img_sort" id="img_sort" style="width: 150px; margin-left: 20px; margin-top: 16px;" onchange="search_func();">
                    <option value="DESC" <?php echo ($order == 'DESC') ? 'selected' : ''; ?>>Newest First</option>
                    <option value="ASC" <?php echo ($order == 'ASC') ? 'selected' : ''; ?>>Oldest First</option>
                </select>
            </div>
    </div>
</div>

<script>
    function search_func() {
        var keywords = $('#keywords-searched').val();
        var img_sort; 
        var img_sort = $('#img_sort option:selected').val();
        $.ajax({
            url: "pending-image-bank-ajax.php",
            type: "GET",
            data: {
                img_sort: img_sort,
                search_image: keywords
            },
            success: function (html) {
                history.pushState({}, null, 'http://touristtowndemo.com/admin/pending-image-bank.php');
                var response = html.split("@");
                $(".image-bank-container").empty();
                $(".img_order_show_hide").hide();
                $(".image-bank-container").html(response[0]);
                $(".main-nav-image-search-total-image").text("");
                $(".main-nav-image-search-total-image").text((response[1] > 1) ? response[1] + " images" : response[1] + "");
            }
        });
        return false;
    }
    function maintain_selection() {
        
    }
</script>
