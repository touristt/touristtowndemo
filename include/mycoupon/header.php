<?php
define("DOMAIN", 'touristtowndemo.com');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">       
        <head>
            <meta charset="utf-8">
            <link href="css/admin-responsive.css" rel="stylesheet" type="text/css" />
            <link href="http://<?php echo DOMAIN ?>/include/tt-coupon.css" rel="stylesheet" type="text/css" />
            <link href="http://<?php echo DOMAIN ?>/include/jquery-ui.css" rel="stylesheet" type="text/css" />
            <link href="http://<?php echo DOMAIN ?>/include/plugins/sweetalert/sweetalert.css"  rel="stylesheet" type="text/css"/> 
            <link href="http://<?php echo DOMAIN ?>/include/token-input.css"  rel="stylesheet" type="text/css"/> 
    		<script src="http://<?php echo DOMAIN ?>/include/jquery.min.js" type="text/javascript"></script>
		<script src="http://<?php echo DOMAIN ?>/include/jquery-ui.min.js" type="text/javascript"></script>
                <script src="http://<?php echo DOMAIN ?>/include/jquery-1.10.2.js" type="text/javascript"></script>
		<script src="http://<?php echo DOMAIN ?>/include/jquery-ui.js" type="text/javascript"></script>
                <script src="http://<?php echo DOMAIN ?>/include/plugins/sweetalert/sweetalert.min.js" type="text/javascript"></script>
                <script type="text/javascript" src="http://<?php echo DOMAIN ?>/include/plugins/ckeditor/ckeditor.js"></script>
                <script src="http://<?php echo DOMAIN ?>/include/ckeditor/jquery.js"></script>
                <script src="http://<?php echo DOMAIN ?>/include/jquery.tokeninput.js"></script>
                <script src="/include/maskedinput.js"></script>

      <script src="/include/mycoupon.js" type="text/javascript"></script>
       <?PHP
         // PREVIEW RELATED DATA STARTS OVER HERE 
         if( isset($BL_ID) && $BL_ID > 0 && isset($preview_page) && $preview_page > 0){
             ?>
       
        <?php // getting the defualt region for the current listing
        $getRegionId = "SELECT BLCR_BLC_R_ID FROM  tbl_Business_Listing_Category_Region WHERE BLCR_BL_ID = $BL_ID LIMIT 1";
        $RegId = mysql_query($getRegionId, $db) or die("Invalid query: $getRegionId -- " . mysql_error());
        $REG = mysql_fetch_assoc($RegId);
        /// for coupon preview chack
        if(isset($coupon_preview) && $coupon_preview == 1){
            $getRegionData = "SELECT * FROM  `tbl_Region` WHERE R_ID = '27'";
            $RegData = mysql_query($getRegionData, $db) or die("Invalid query: $getRegionData -- " . mysql_error());
            $REGION = mysql_fetch_assoc($RegData);
        }else{
        $getRegionData = "SELECT * FROM  `tbl_Region` WHERE R_ID = '" . encode_strings($REG['BLCR_BLC_R_ID'], $db) . "'";
        $RegData = mysql_query($getRegionData, $db) or die("Invalid query: $getRegionData -- " . mysql_error());
        $REGION = mysql_fetch_assoc($RegData);
        
        }
        //Getting Region Theme
        $getTheme = "SELECT * FROM  `tbl_Theme_Options` WHERE TO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
        $themeRegion = mysql_query($getTheme, $db) or die("Invalid query: $getTheme -- " . mysql_error());
        $THEME = mysql_fetch_assoc($themeRegion);
        $header_text_value = explode("-", $THEME['TO_Header_Text']);
        $slider_title_value = explode("-", $THEME['TO_Slider_Title']);
        $slider_desc_value = explode("-", $THEME['TO_Slider_Description']);
        $listing_dd_text_value = explode("-", $THEME['TO_Listing_Drop_Down_Text']);
        $menu_text_value = explode("-", $THEME['TO_Menu_Text']);
        $listing_sub_heading_value = explode("-", $THEME['TO_Listing_Sub_Heading']);
        $listing_sub_nav_value = explode("-", $THEME['TO_Listing_Sub_Nav_Text']);
        $listing_day_text_value = explode("-", $THEME['TO_Listing_Day_Text']);
        $listing_hours_text_value = explode("-", $THEME['TO_Listing_Hours_Text']);
        $amenities_text_value = explode("-", $THEME['TO_Amenities_Text']);
        $download_text_value = explode("-", $THEME['TO_Downloads_Text']);
        $whats_near_dd_value = explode("-", $THEME['TO_Whats_Nearby_Drop_Down_Text']);
        $listing_main_title_value = explode("-", $THEME['TO_Listing_Title_Text']);
        $listing_address_value = explode("-", $THEME['TO_Listing_Location_Text']);
        $listing_nav_value = explode("-", $THEME['TO_Listing_Navigation_Text']);
        $general_paragraph_value = explode("-", $THEME['TO_General_Body_Copy']);

        //CSS With Image variables
        $listing_nav_gallary_bg_color = explode(">", $THEME['TO_Listing_Nav_Gallary_Background']);
        $listing_whats_nearby_bg_color = explode(">", $THEME['TO_Listing_Whats_Nearby_Background']);

        //Getting Region Theme Font, Due to our DB structure we are applying new query rather than JOIN
        $getFont = "SELECT * FROM `tbl_Theme_Options_Fonts`";
        $themeFontRegion = mysql_query($getFont, $db) or die("Invalid query: $getFont -- " . mysql_error());
        $THEME_ARRAY = array();
        while ($THEME_FONT = mysql_fetch_assoc($themeFontRegion)) {
            $THEME_ARRAY[$THEME_FONT['TOF_ID']] = $THEME_FONT['TOF_Name'];
        }
         
        ?>
        <style>
            a {
                color: <?php echo $THEME['TO_Text_Link'] ?>;
            }
            .theme-paragraph, .theme-paragraph p, .theme-paragraph ul li{
                font-family: <?php echo (isset($THEME_ARRAY[$general_paragraph_value[0]]) === true && empty($THEME_ARRAY[$general_paragraph_value[0]]) === false) ? $THEME_ARRAY[$general_paragraph_value[0]] : ""; ?>;
                font-size: <?php echo $general_paragraph_value[1] ?>px;
                color: <?php echo $general_paragraph_value[2] ?>;
                line-height: <?php echo $THEME['TO_General_Body_Copy_Line_Spacing'] ?>em;
            }

            /* IR-> .theme-paragraph p, removed this from the uppper css 13 july, 2017 , and added the below css*/
            .listing-detail-left .theme-paragraph{
                font-family: <?php echo (isset($THEME_ARRAY[$general_paragraph_value[0]]) === true && empty($THEME_ARRAY[$general_paragraph_value[0]]) === false) ? $THEME_ARRAY[$general_paragraph_value[0]] : ""; ?>;
                font-size: <?php echo $general_paragraph_value[1] ?>px;
                color: <?php echo $general_paragraph_value[2] ?>;
                line-height: <?php echo $THEME['TO_General_Body_Copy_Line_Spacing'] ?>em;
            }
            .inside-wrapper {
                font-family: <?php echo (isset($THEME_ARRAY[$header_text_value[0]]) === true && empty($THEME_ARRAY[$header_text_value[0]]) === false) ? $THEME_ARRAY[$header_text_value[0]] : ""; ?>;
                font-size: <?php echo $header_text_value[1] ?>px;
                color: <?php echo $header_text_value[2] ?>;
            }
            /* Cycle Slider Start
            -----------------------------------------------------------------------------------------------------------*/
            .main_slider .slider_wrapper .sliderCycle .slide-images {
                font-family: <?php echo (isset($THEME_ARRAY[$slider_title_value[0]]) === true && empty($THEME_ARRAY[$slider_title_value[0]]) === false) ? $THEME_ARRAY[$slider_title_value[0]] : ""; ?>;
                font-size: <?php echo $slider_title_value[1] ?>px;
                color: <?php echo $slider_title_value[2] ?>;
            }
            .main_slider .slider_wrapper .sliderCycle .slide-images .slider-text h3{
                font-family: <?php echo (isset($THEME_ARRAY[$slider_desc_value[0]]) === true && empty($THEME_ARRAY[$slider_desc_value[0]]) === false) ? $THEME_ARRAY[$slider_desc_value[0]] : ""; ?>;
                font-size: <?php echo $slider_desc_value[1] ?>px;
                color: <?php echo $slider_desc_value[2] ?>;
            }
            /* Cycle Slider End
            -----------------------------------------------------------------------------------------------------------*/
            /* Description after slider
            -----------------------------------------------------------------------------------------------------------*/           
            /* Thumbnail slider start
            -----------------------------------------------------------------------------------------------------------*/
            .nbs-flexisel-nav-left, .nbs-flexisel-nav-left.disabled, .bx-wrapper .bx-prev {
                <?php if ($THEME['TO_Thumbnail_Arrow_Left'] != "") { ?>
                    background-image: url("http://<?php echo DOMAIN . IMG_ICON_REL . $THEME['TO_Thumbnail_Arrow_Left'] ?>");
                <?php } ?>
            }
            .nbs-flexisel-nav-right, .nbs-flexisel-nav-right.disabled, .bx-wrapper .bx-next {
                <?php if ($THEME['TO_Thumbnail_Arrow_Right'] != "") { ?>
                    background-image: url("http://<?php echo DOMAIN . IMG_ICON_REL . $THEME['TO_Thumbnail_Arrow_Right'] ?>");
                <?php } ?>
            }
            /* Thumbnail slider end
            -----------------------------------------------------------------------------------------------------------*/
          
            /* Listing Page Start
            -----------------------------------------------------------------------------------------------------------*/
            .description.listing-home-des {
                <?php if ($listing_nav_gallary_bg_color[0] != "") { ?>
                    background-image: url("<?php echo IMG_ICON_REL . $listing_nav_gallary_bg_color[0] ?>");
                <?php } else { ?>
                    background-color: <?php echo $listing_nav_gallary_bg_color[1]; ?>
                <?php } ?>
            }
            .listing-title-detail .listing-title1, .event-wrapper-inner .details .section-header h2 {
                font-family: <?php echo (isset($THEME_ARRAY[$listing_main_title_value[0]]) === true && empty($THEME_ARRAY[$listing_main_title_value[0]]) === false) ? $THEME_ARRAY[$listing_main_title_value[0]] : ""; ?>;
                font-size: <?php echo $listing_main_title_value[1] ?>px;
                color: <?php echo $listing_main_title_value[2] ?>;
            }
            .listing-address, .get-coupon-listing,  .get-coupon-town {
                font-family: <?php echo (isset($THEME_ARRAY[$listing_address_value[0]]) === true && empty($THEME_ARRAY[$listing_address_value[0]]) === false) ? $THEME_ARRAY[$listing_address_value[0]] : ""; ?>;
                font-size: <?php echo $listing_address_value[1] ?>px;
                color: <?php echo $listing_address_value[2] ?>;
            }
            .listing-desc .menu-Button select {
                font-family: <?php echo (isset($THEME_ARRAY[$listing_dd_text_value[0]]) === true && empty($THEME_ARRAY[$listing_dd_text_value[0]]) === false) ? $THEME_ARRAY[$listing_dd_text_value[0]] : ""; ?>;
                font-size: <?php echo $listing_dd_text_value[1] ?>px;
                color: <?php echo $listing_dd_text_value[2] ?>;
                background-color: <?php echo $THEME['TO_Listing_Drop_Down_Background'] ?>;
            }
            .menu-header-container .drop-down-arrow {
                border-left-color: <?php echo $THEME['TO_Listing_Drop_Down_Arrow'] ?>;
            }
            .menu-item-heading-container .menu-title {
                font-family: <?php echo (isset($THEME_ARRAY[$menu_text_value[0]]) === true && empty($THEME_ARRAY[$menu_text_value[0]]) === false) ? $THEME_ARRAY[$menu_text_value[0]] : ""; ?>;
                font-size: <?php echo $menu_text_value[1] ?>px;
                color: <?php echo $menu_text_value[2] ?>;
            }
            .listing-desc .menu-heading, .listing-review .review-heading, .story-title { 
                font-family: <?php echo (isset($THEME_ARRAY[$listing_sub_heading_value[0]]) === true && empty($THEME_ARRAY[$listing_sub_heading_value[0]]) === false) ? $THEME_ARRAY[$listing_sub_heading_value[0]] : ""; ?>;
                font-size: <?php echo $listing_sub_heading_value[1] ?>px;
                color: <?php echo $listing_sub_heading_value[2] ?>;
            }
            .listing-detail-add-detail .ui-accordion .ui-accordion-header, .listing-detail-address.border-top-none .listing-address-container .listing-detail-add-detail a,
            .listing-detail-address .listing-address-container .listing-detail-add-detail .address-heading, .listing-detail-tripplanner .address-heading a, .listing-detail-address .listing-detail-add-detail .address-heading
            ,.listing-detail-address .listing-detail-add-detail .address-heading a
            {
                font-family: <?php echo (isset($THEME_ARRAY[$listing_sub_nav_value[0]]) === true && empty($THEME_ARRAY[$listing_sub_nav_value[0]]) === false) ? $THEME_ARRAY[$listing_sub_nav_value[0]] : ""; ?>;
                font-size: <?php echo $listing_sub_nav_value[1] ?>px;
                color: <?php echo $listing_sub_nav_value[2] ?>;
            }
             .listing-title-detail .listing-title, .event-wrapper-inner .details .section-header h2 {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_main_title_value[0]]) === true && empty($THEME_ARRAY[$listing_main_title_value[0]]) === false) ? $THEME_ARRAY[$listing_main_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_main_title_value[1])) ? $listing_main_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($listing_main_title_value[2])) ? $listing_main_title_value[2] : ''; ?>;
                }
            .listing-address, .get-coupon-listing, .get-coupon-town {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_address_value[0]]) === true && empty($THEME_ARRAY[$listing_address_value[0]]) === false) ? $THEME_ARRAY[$listing_address_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_address_value[1])) ? $listing_address_value[1] : ''; ?>px;
                    color: <?php echo (isset($listing_address_value[2])) ? $listing_address_value[2] : ''; ?> !important;
                    line-height:39px;
                }
               .menu-description-term{
                    font-family: <?php echo (isset($THEME_ARRAY[$general_paragraph_value[0]]) === true && empty($THEME_ARRAY[$general_paragraph_value[0]]) === false) ? $THEME_ARRAY[$general_paragraph_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($general_paragraph_value[1])) ? $general_paragraph_value[1] : ''; ?>px !important;
                    color: <?php echo (isset($general_paragraph_value[2])) ? $general_paragraph_value[2] : ''; ?> !important;
                    line-height: <?php echo (isset($THEME['TO_General_Body_Copy_Line_Spacing'])) ? $THEME['TO_General_Body_Copy_Line_Spacing'] : ''; ?>em;
                }
            .day-heading {
                font-family: <?php echo (isset($THEME_ARRAY[$listing_day_text_value[0]]) === true && empty($THEME_ARRAY[$listing_day_text_value[0]]) === false) ? $THEME_ARRAY[$listing_day_text_value[0]] : ""; ?>;
                font-size: <?php echo $listing_day_text_value[1] ?>px;
                color: <?php echo $listing_day_text_value[2] ?>;
            }
            .day-timing {
                <?php $listing_hours_text_index = array_search($listing_hours_text_value[0], $THEME_ARRAY); ?>
                font-family: <?php echo (isset($THEME_ARRAY[$listing_hours_text_value[0]]) === true && empty($THEME_ARRAY[$listing_hours_text_value[0]]) === false) ? $THEME_ARRAY[$listing_hours_text_value[0]] : ""; ?>;
                font-size: <?php echo $listing_hours_text_value[1] ?>px;
                color: <?php echo $listing_hours_text_value[2] ?>;
            }
            .businessListing-timing .acc-body {
                font-family: <?php echo (isset($THEME_ARRAY[$amenities_text_value[0]]) === true && empty($THEME_ARRAY[$amenities_text_value[0]]) === false) ? $THEME_ARRAY[$amenities_text_value[0]] : ""; ?>;
                font-size: <?php echo $amenities_text_value[1] ?>px;
                color: <?php echo $amenities_text_value[2] ?>;
            }
            .listing-detail-address .listing-address-container .listing-detail-add-detail .businessListing-timing .acc-body.theme-downloads a {
                font-family: <?php echo (isset($THEME_ARRAY[$download_text_value[0]]) === true && empty($THEME_ARRAY[$download_text_value[0]]) === false) ? $THEME_ARRAY[$download_text_value[0]] : ""; ?>;
                font-size: <?php echo $download_text_value[1] ?>px;
                color: <?php echo $download_text_value[2] ?>;
            }
            .withinkm .drop-down-arrow {
                border-left-color: <?php echo $THEME['TO_Whats_Nearby_Drop_Down_Arrow'] ?>;
            } 
            .fancybox-overlay { 
                <?php if ($THEME['TO_Photo_Gallery_Background_Colour'] != "") { ?>
                    background: none;
                    background-color: <?php echo $THEME['TO_Photo_Gallery_Background_Colour']; ?>
                <?php } ?>
            }
        </style>
        <?php } 
        // PREVIEW RELATED DATA ENDS OVER HERE
        ?>
        </head> 
        <body>
            <!--       main div -->
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <div class="main-wapper">
                <div class="logo-container">
                    <div class="logo">
                        <a href="/">
                            <img src="http://<?php echo DOMAIN ?>/images/logo.png" alt="Tourist Town" />
                        </a>
                    </div>
                </div>