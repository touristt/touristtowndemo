
<?php
define("DOMAIN", 'touristtowndemo.com');
if ($title == '') {
    $title = COMPANY_NAME . " Admin";
}
$pagename = basename($_SERVER['PHP_SELF']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?PHP echo $title; ?></title>
        <link href="css/admin-responsive.css" rel="stylesheet" type="text/css" />
        <link href="http://<?php echo DOMAIN ?>/include/jquery-ui.css" rel="stylesheet" type="text/css" />
        <link href="http://<?php echo DOMAIN ?>/include/plugins/sweetalert/sweetalert.css"  rel="stylesheet" type="text/css"/> 
        <link href="http://<?php echo DOMAIN ?>/include/token-input.css"  rel="stylesheet" type="text/css"/> 
        <script src="http://<?php echo DOMAIN ?>/include/jquery.min.js" type="text/javascript"></script>
        <script src="http://<?php echo DOMAIN ?>/include/jquery-ui.min.js" type="text/javascript"></script>
        <script src="http://<?php echo DOMAIN ?>/include/jquery-1.10.2.js" type="text/javascript"></script>
        <script src="http://<?php echo DOMAIN ?>/include/jquery-ui.js" type="text/javascript"></script>
        <script src="http://<?php echo DOMAIN ?>/include/plugins/sweetalert/sweetalert.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="http://<?php echo DOMAIN ?>/include/plugins/ckeditor/ckeditor.js"></script>        
        <script src="http://<?php echo DOMAIN ?>/include/ckeditor/jquery.js"></script>
        <script src="http://<?php echo DOMAIN ?>/include/jquery.tokeninput.js"></script>
        <script src="/include/maskedinput.js"></script>
        <script src="http://<?php echo DOMAIN ?>/include/js-webshim/minified/polyfiller.js"></script>
        <script>
            webshim.activeLang("en");
            webshims.polyfill("forms");
            webshims.cfg.no$Switch = true;
        </script>
        
        <script src="/include/mycoupon.js" type="text/javascript"></script>

    </head>
    <?PHP
    $sql_responsive_bg = "SELECT * from tbl_Mobile_Layout";
    $result_responsive_bg = mysql_query($sql_responsive_bg, $db) or die("Invalid query: $sql_responsive_bg -- " . mysql_error());
    $row_responsive_bg = mysql_fetch_assoc($result_responsive_bg);
    ?>
    <body >
        <meta name="viewport" content="width=device-width, initial-scale=1">
            <div class="main-wapper">

                <div class="header">
                    <div class="header-inner">
                        <div class="logo">
                            <a href="/mobile/admin-mobile-menu.php"><img src="http://<?php echo DOMAIN ?>/images/logo.png" alt="Tourist Town" width="150" height="70" hspace="20" vspace="20" border="0" />
                            </a>
                        </div>
                        <?php if ($_SESSION['BUSINESS_ID'] && $pagename != 'admin-mobile-menu.php') { ?>
                            <?php
                            if ((isset($_REQUEST['bl_id']) && $_REQUEST['bl_id'] != '') || (isset($_REQUEST['id']) && $_REQUEST['id'] != '')) {
                                if ($_REQUEST['bl_id'] != '') {
                                    $BL_ID = $_REQUEST['bl_id'];
                                }
                                if ($pagename == 'listing-menu.php' || $pagename == 'login-details.php') {
                                    ?>
                                    <div class="back-home-button">
                                        <a href="/mobile/admin-mobile-menu.php">BACK HOME</a>
                                    </div>
                                <?php } else {
                                    ?>
                                    <div class="back-home-button">
                                        <a href="/mobile/listing-menu.php?bl_id=<?= $BL_ID ?>">BACK HOME</a>
                                    </div>
                                    <?php
                                }
                            } else {
                                ?>
                                <div class="back-home-button">
                                    <a href="/mobile/admin-mobile-menu.php">BACK HOME</a>
                                </div>
                                <?php
                            }
                        } else if ($_SESSION['BUSINESS_ID']) {
                            ?>
                            <div class="back-home-button">
                                <a href="/mobile/index.php?logop=logout">Logout</a>
                            </div>
                        <?php }
                        ?>
                    </div>
                </div>

                <?php
                if (isset($_SESSION['success']) && $_SESSION['success'] == 1) {
                    print '<script>swal("Data Saved", "Data has been saved successfully.", "success");</script>';
                    unset($_SESSION['success']);
                }
                if (isset($_SESSION['error']) && $_SESSION['error'] == 1) {
                    print '<script>swal("Error", "Error saving data. Please try again.", "error");</script>';
                    unset($_SESSION['error']);
                }
                if (isset($_SESSION['delete']) && $_SESSION['delete'] == 1) {
                    print '<script>swal("Data Deleted", "Data has been deleted successfully.", "success");</script>';
                    unset($_SESSION['delete']);
                }
                if (isset($_SESSION['delete_error']) && $_SESSION['delete_error'] == 1) {
                    print '<script>swal("Error", "Error deleting data. Please try again.", "error");</script>';
                    unset($_SESSION['delete_error']);
                }
                ?>




