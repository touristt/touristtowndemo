function obtainCount() {

    $.ajax({
    type: "POST",
    url: "/include/tripplanner.inc.php",
    data: { action: "count"},
    success: function(data) {
        $("#trip-count").html(data.count)
    }
   });  

};

$(document).ready(function () {
    
    obtainCount();

    $(".remove-item-tripplanner a").click(function() { 

        var toSplit = (this.id).split("_");
        var categoryID = toSplit[0];
        var businessID = toSplit[1];
        var bl_ID = toSplit[2];
        var total_listings=$('.my-trips-count span').text();
        var total_trip=total_listings-1;
        $('.my-trips-count span').text(total_trip);
        $("#" + this.id).replaceWith('<span class="planner-link" style="text-transform:uppercase;  font-size: 11px; font-weight: bold; float:left;width: 200px; color: #0E9BDB;">Removed thanks</span>');
        $.ajax({
        type: "POST",
        url: "/include/tripplanner.inc.php",
        data: { businessID: businessID, categoryID: categoryID, bl_ID:bl_ID, action: "delete"},
        success: function(data) {

            $("#trip-container-" + categoryID + "_" + businessID).hide("slow",  function() {$(this).remove();});
            obtainCount();

        }
        });  

        return false
    });

    $(".add-item-tripplanner a").click(function() {

        var toSplit = (this.id).split("_");
        var categoryID = toSplit[0];
        var businessID = toSplit[1];
        var bl_ID = toSplit[2];
        var total_listings=parseInt($('.my-trips-count span').text());
        var total_trip= total_listings + 1;
        $('.my-trips-count span').text(total_trip);
        $("#" + this.id).replaceWith('<span class="planner-link" style="text-transform:uppercase;  font-size: 11px; font-weight: bold; float:left;width: 200px; color: #0E9BDB;">added thanks</span>');

        $.ajax({
        type: "POST",
        url: "/include/tripplanner.inc.php",
        data: { action: "add", businessID: businessID, bl_ID:bl_ID, categoryID: categoryID},
        success: function(data) {
            obtainCount();
        }
        });  
        return false
    });
});
