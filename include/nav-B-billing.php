<div class="sub-menu">
    <ul>
        <li><a class="bold" href="customer-billing.php?bl_id=<?php echo $BL_ID; ?>">Invoice History</a></li>
        <?PHP
        $listing_invoices_nav = array();
        $division_invoices_nav = array();
        $all_invoices_nav = array();
        $sql = "SELECT DISTINCT YEAR(BB_Date) FROM tbl_Business_Billing WHERE BB_BL_ID = '" . encode_strings($BL_ID, $db) . "' 
                AND BB_Deleted = 0 AND BB_Date >= '2015-06-12' ORDER BY YEAR(BB_Date) DESC";
        $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($resultTMP)) {
            $listing_invoices_nav[] = $row;
        }

        $sql = "SELECT DISTINCT YEAR(DB_Date) FROM tbl_Division_Billing WHERE DB_BL_ID = '" . encode_strings($BL_ID, $db) . "' 
                AND DB_Deleted = 0 ORDER BY YEAR(DB_Date) DESC";
        $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($resultTMP)) {
            $division_invoices_nav[] = $row;
        }

        $all_invoices_nav = array_merge($listing_invoices_nav, $division_invoices_nav);

        function date_sort_nav($a, $b) {
            $date = (isset($a['YEAR(BB_Date)'])) ? $a['YEAR(BB_Date)'] : $a['YEAR(DB_Date)'];
            $date2 = (isset($b['YEAR(BB_Date)'])) ? $b['YEAR(BB_Date)'] : $b['YEAR(DB_Date)'];
            return strtotime($date2) - strtotime($date);
        }

        usort($all_invoices_nav, "date_sort_nav");

        $i = 0;
        foreach ($all_invoices_nav as $invoice_nav) {
            if (isset($invoice_nav['YEAR(BB_Date)'])) {
                if ($i == 0) {
                    $Recent_Year = $invoice_nav['YEAR(BB_Date)'];
                }
                if (!in_array($invoice_nav['YEAR(BB_Date)'], $yrs)) {
                    ?>
                    <li class="support-nav-b-li"><a class="support-nav-b" href="customer-billing.php?bl_id=<?php echo $BL_ID; ?>&yr=<?php echo $invoice_nav['YEAR(BB_Date)'] ?>"><?php echo $invoice_nav['YEAR(BB_Date)'] ?></a></li>
                    <?php
                    $yrs[] = $invoice_nav['YEAR(BB_Date)'];
                }
            } else if (isset($invoice_nav['YEAR(DB_Date)'])) {
                if ($i == 0) {
                    $Recent_Year = $invoice_nav['YEAR(DB_Date)'];
                }
                if (!in_array($invoice_nav['YEAR(DB_Date)'], $yrs)) {
                    ?>
                    <li class="support-nav-b-li"><a class="support-nav-b" href="customer-billing.php?bl_id=<?php echo $BL_ID; ?>&yr=<?php echo $invoice_nav['YEAR(DB_Date)'] ?>"><?php echo $invoice_nav['YEAR(DB_Date)'] ?></a></li>
                    <?php
                    $yrs[] = $invoice_nav['YEAR(DB_Date)'];
                }
            }
            $i++;
        }
        ?>
        <li><a class="bold" href="customer-billing-old.php?bl_id=<?php echo $BL_ID; ?>">Old Invoice</a></li>
    </ul>
</div>