<div class="sub-menu">
    <ul>
        <?PHP if (in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) { ?>
            <li><a href="/admin/request-change-completed.php">Completed Request</a></li>
            <li><a href="/admin/request-change-pending.php">Pending Request</a></li>   
        <?PHP } if (!in_array('superadmin', $_SESSION['USER_ROLES']) && in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) { ?>
            <li><a href="/admin/request_content_change.php">+Add Request</a></li>
        <?PHP } ?>
    </ul>
</div>