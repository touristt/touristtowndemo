<!--Footer Start-->
<?php
$nav = array();
$nav1 = array();
$nav2 = array();
$totalRows = 0;
$sql_nav = "SELECT C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category 
            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            INNER JOIN tbl_Business_Listing_Category ON BLC_M_C_ID = C_ID    
            INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID AND BLCR_BLC_R_ID  " . encode_strings((($REGION['R_Parent'] == 0 && $REGION['R_Type'] != 4) ? "IN " . $regionList : "= " . $REGION['R_ID']), $db) . "
            INNER JOIN tbl_Business_Listing ON BL_ID = BLC_BL_ID
            WHERE C_Parent = 0 AND RC_Status = 0 AND C_Is_Blog = 0 AND C_ID != 8 AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = '1' $include_free_listings GROUP BY C_ID ORDER BY RC_Order ASC";
$result = mysql_query($sql_nav);
while ($row = mysql_fetch_assoc($result)) {
    $nav1[] = $row;
}
if ($REGION['R_Show_Hide_Event'] == 1) {
    $sql_nav = "SELECT C_ID, RC_C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category 
                            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                            WHERE C_ID = 8 AND RC_Status = 0 AND C_Is_Blog = 0 ORDER BY RC_Order ASC";
    $result = mysql_query($sql_nav);
    while ($row = mysql_fetch_assoc($result)) {
        $nav2[] = $row;
    }
}
$nav = array_merge($nav1, $nav2);
foreach ($nav as $row) {
    $c_parent = $row['C_ID'];
    if ($c_parent != 8) {
        $sql_nav_sub = "SELECT C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category 
                        LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = " . encode_strings($REGION['R_ID'], $db) . "
                        INNER JOIN tbl_Business_Listing_Category ON BLC_C_ID = C_ID    
                        INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID AND BLCR_BLC_R_ID  " . encode_strings((($REGION['R_Parent'] == 0 && $REGION['R_Type'] != 4) ? "IN " . $regionList : "= " . $REGION['R_ID']), $db) . "
                        INNER JOIN tbl_Business_Listing ON BL_ID = BLC_BL_ID
                        WHERE C_Parent = '$c_parent' and RC_Status = 0 AND RC_R_ID > 0 AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = '1' $include_free_listings
                        GROUP BY C_ID ORDER BY RC_Order ASC";
    } else {
        $sql_nav_sub = "SELECT C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category 
                                                    LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                                                    WHERE C_Parent = '$c_parent' and RC_Status = 0 AND RC_R_ID > 0
                                                    ORDER BY RC_Order ASC";
    }
    $result_sub = mysql_query($sql_nav_sub);
    $totalRows += mysql_num_rows($result_sub);
}
$total11 = $totalRows / 4;
?>

<footer class="footer">
    <div class="footer-outer">
        <?php if ($REGION['R_Show_Hide_Footer_Menu'] == 1) { ?>
            <!--    Footer menu-->
            <div class="footer-column">
                <style>
                    .submenu-left-side-inner {
                        width: 100%;
                        float: left;
                        /*margin-top: 15px;*/
                    }
                    li.footer-heading-color {
                        width: 221px;
                        /*float: left;*/
                    }
                    .footer-item {
                        float: left;
                        width: 100%;
                    }
                    .submenu-left-side11 {
                        width: 100%;
                        margin-top: 20px;
                        float: left;
                    }
                    .column {
                        margin-top: 3px;
                        margin-bottom: 3px;
                        width: 220px;
                        float: left;
                    }
                    .footer-heading-dynamic {
                        width: 100%;
                        float: left;
                        margin-top: 6px;
                        margin-bottom: 5px;
                    }
                    .text-style {
                        float: left;
                        width: 100%;
                        margin-left: 0;
                    }
                    .text-style.footer-item.dynamic.map {
                        margin-top: 7px;
                    }
                </style>
                <div class="text-style footer-item dynamic">
                    <ul class="text-style">
                        <?php
                        $nav = array();
                        $nav1 = array();
                        $nav2 = array();

                        $sql_nav = "SELECT C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category 
                        LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                        INNER JOIN tbl_Business_Listing_Category ON BLC_M_C_ID = C_ID    
                        INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID AND BLCR_BLC_R_ID  " . encode_strings((($REGION['R_Parent'] == 0 && $REGION['R_Type'] != 4) ? "IN " . $regionList : "= " . $REGION['R_ID']), $db) . "
                        INNER JOIN tbl_Business_Listing ON BL_ID = BLC_BL_ID
                        WHERE C_Parent = 0 AND RC_Status = 0 AND C_Is_Blog = 0 AND C_ID != 8 AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = '1' $include_free_listings GROUP BY C_ID ORDER BY RC_Order ASC";
                        $result = mysql_query($sql_nav);
                        while ($row = mysql_fetch_assoc($result)) {
                            $nav1[] = $row;
                        }
                        if ($REGION['R_Show_Hide_Event'] == 1) {
                            $sql_nav = "SELECT C_ID, RC_C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category 
                            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                            WHERE C_ID = 8 AND RC_Status = 0 AND C_Is_Blog = 0 ORDER BY RC_Order ASC";
                            $result = mysql_query($sql_nav);
                            while ($row = mysql_fetch_assoc($result)) {
                                $nav2[] = $row;
                            }
                        }
                        $nav = array_merge($nav1, $nav2);
                        $check = 0;
                        foreach ($nav as $row) {
                            echo '<div class="footer-heading-dynamic">';
                            if ($row['RC_Name'] != '') {
                                echo '<li class="footer-heading-color">
                                
                                <a class="footer-heading-color" href="/' . $row['C_Name_SEO'] . '/" onmouseover="dropdown_ads_impression(' . $row['C_ID'] . ');">' . $row['RC_Name'] . '</a></li>';
                            } else {
                                echo '<li class="footer-heading-color"><a class="footer-heading-color" href="/' . $row['C_Name_SEO'] . '/" onmouseover="dropdown_ads_impression(' . $row['C_ID'] . ');">' . $row['C_Name'] . '</a></li>';
                            }
                            $c_parent = $row['C_ID'];
                            if ($c_parent != 8) {
                                $sql_nav_sub = "SELECT C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category 
                                                    LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = " . encode_strings($REGION['R_ID'], $db) . "
                                                    INNER JOIN tbl_Business_Listing_Category ON BLC_C_ID = C_ID    
                                                    INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID AND BLCR_BLC_R_ID  " . encode_strings((($REGION['R_Parent'] == 0 && $REGION['R_Type'] != 4) ? "IN " . $regionList : "= " . $REGION['R_ID']), $db) . "
                                                    INNER JOIN tbl_Business_Listing ON BL_ID = BLC_BL_ID
                                                    WHERE C_Parent = '$c_parent' and RC_Status = 0 AND RC_R_ID > 0 AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = '1' $include_free_listings
                                                    GROUP BY C_ID ORDER BY RC_Order ASC";
                            } else {
                                $sql_nav_sub = "SELECT C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category 
                                                    LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                                                    WHERE C_Parent = '$c_parent' and RC_Status = 0 AND RC_R_ID > 0
                                                    ORDER BY RC_Order ASC";
                            }
                            $result_sub = mysql_query($sql_nav_sub);
                            $totalRows = mysql_num_rows($result_sub);
                            ?>
                            <div  class="submenu-left-side-inner">
                                <?php
                                while ($rowSub = mysql_fetch_array($result_sub)) {
                                    $check++;
                                    if ($totalRows % 2 == 0) {
                                        echo '<div class="column">';
                                        $class = 'first';
                                        if ($rowSub['RC_Name'] != '') {
                                            echo '<a class="' . $class . '" href="/' . $row['C_Name_SEO'] . '/' . $rowSub['C_Name_SEO'] . '/">' . $rowSub['RC_Name'] . '</a>';
                                        } else {
                                            echo '<a class="' . $class . '" href="/' . $row['C_Name_SEO'] . '/' . $rowSub['C_Name_SEO'] . '/">' . $rowSub['C_Name'] . '</a>';
                                        }
                                        echo '</div>';
                                    } else {
                                        echo '<div class="column">';
                                        $class = 'first';
                                        if ($rowSub['RC_Name'] != '') {
                                            echo '<a class="' . $class . '" href="/' . $row['C_Name_SEO'] . '/' . $rowSub['C_Name_SEO'] . '/">' . $rowSub['RC_Name'] . '</a>';
                                        } else {
                                            echo '<a class="' . $class . '" href="/' . $row['C_Name_SEO'] . '/' . $rowSub['C_Name_SEO'] . '/">' . $rowSub['C_Name'] . '</a>';
                                        }

                                        echo '</div>';
                                    }
                                }
                                if ($REGION['R_Day_Trip'] == 1 && $rowcount != 0 && ($row['C_ID'] == 5 || $row['RC_C_ID'] == 5)) {
                                    echo '<div class="column">';
                                    echo '<a class="maps-text first" href="/things-to-do/day-trip">Day Trips</a>';
                                    echo '</div>';
                                }
                                ?></div></div><?php
                }
                ?>
                <div class="text-style footer-item dynamic map">
                    <li class="footer-heading-color"><a href="/maps.php">Map</a></li> 
                    <div class="submenu-left-side-inner">
                        <div class="column">
                            <a href="/maps.php">Interactive Map</a> 
                        </div>
                    </div>
                </div>
                </ul>
            </div> 
        </div>
        <?php
    }
    $footer = "SELECT FP_Link, FP_Photo, FP_Alt from tbl_Footer_Photo WHERE  FP_Section = 1 AND FP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' ORDER BY FP_Order ASC";
    $footerResult = mysql_query($footer, $db) or die("Invalid query: $footer -- " . mysql_error());
    $footerCount = mysql_num_rows($footerResult);

    if ($footerCount > 0) {
        ?>
        <!--Footer logo section 1-->
        <div class="footer-images">
            <div class="footer-margin-auto">
                <div>
                    <?php
                    while ($footerRow = mysql_fetch_array($footerResult)) {
                        $url = preg_replace('/^www\./', '', $footerRow['FP_Link']);
                        if ($footerRow['FP_Photo'] !== "") {
                            if (isset($footerRow['FP_Link']) && $footerRow['FP_Link'] !== "") {
                                ?>
                                <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $url) ?>"><img src="http://<?php echo DOMAIN . IMG_LOC_REL . $footerRow['FP_Photo'] ?>" alt="<?php echo $footerRow['FP_Alt'] ?>" border="0" /></a>
                                <?php
                            } else {
                                ?>
                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $footerRow['FP_Photo'] ?>" alt="<?php echo $footerRow['FP_Alt'] ?>" border="0" />
                                <?php
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php
    $footer2 = "SELECT FP_Link, FP_Photo, FP_Alt from tbl_Footer_Photo WHERE  FP_Section = 2 AND FP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' ORDER BY FP_Order ASC";
    $footerResult2 = mysql_query($footer2, $db) or die("Invalid query: $footer2 -- " . mysql_error());
    $footerCount2 = mysql_num_rows($footerResult2);
    if ($footerCount2 > 0) {
        ?>
        <!--Footer logo section 2-->
        <div class="footer-images border-top-none">
            <div class="footer-margin-auto">
                <div>
                    <?php
                    while ($footerRow2 = mysql_fetch_array($footerResult2)) {
                        $url2 = preg_replace('/^www\./', '', $footerRow2['FP_Link']);
                        if ($footerRow2['FP_Photo'] !== "") {
                            if (isset($footerRow2['FP_Link']) && $footerRow2['FP_Link'] !== "") {
                                ?>
                                <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $url2) ?>"><img src="http://<?php echo DOMAIN . IMG_LOC_REL . $footerRow2['FP_Photo'] ?>" alt="<?php echo $footerRow2['FP_Alt'] ?>" border="0" /></a>
                                <?php
                            } else {
                                ?>
                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $footerRow2['FP_Photo'] ?>" alt="<?php echo $footerRow2['FP_Alt'] ?>" border="0" />
                                <?php
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php
    $footer4 = "SELECT FP_Link, FP_Photo, FP_Alt from tbl_Footer_Photo WHERE  FP_Section = 4 AND FP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' ORDER BY FP_Order ASC";
    $footerResult4 = mysql_query($footer4, $db) or die("Invalid query: $footer4 -- " . mysql_error());
    $footerCount4 = mysql_num_rows($footerResult4);
    if ($footerCount4 > 0) {
        ?>
        <!--Footer logo section 3-->
        <div class="footer-images border-top-none">
            <div class="footer-margin-auto">
                <div>
                    <?php
                    while ($footerRow4 = mysql_fetch_array($footerResult4)) {
                        $url4 = preg_replace('/^www\./', '', $footerRow4['FP_Link']);
                        if ($footerRow4['FP_Photo'] !== "") {
                            if (isset($footerRow4['FP_Link']) && $footerRow4['FP_Link'] != "") {
                                ?>
                                <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $url4) ?>"><img src="http://<?php echo DOMAIN . IMG_LOC_REL . $footerRow4['FP_Photo'] ?>" alt="<?php echo $footerRow4['FP_Alt'] ?>" border="0" /></a>
                                <?php
                            } else {
                                ?>
                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $footerRow4['FP_Photo'] ?>" alt="<?php echo $footerRow4['FP_Alt'] ?>" border="0" />
                                <?php
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php
    $footer5 = "SELECT FP_Link, FP_Photo, FP_Alt from tbl_Footer_Photo WHERE  FP_Section = 5 AND FP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' ORDER BY FP_Order ASC";
    $footerResult5 = mysql_query($footer5, $db) or die("Invalid query: $footer5 -- " . mysql_error());
    $footerCount5 = mysql_num_rows($footerResult5);
    if ($footerCount5 > 0) {
        ?>
        <!--Footer logo section 3-->
        <div class="footer-images border-top-none">
            <div class="footer-margin-auto">
                <div>
                    <?php
                    while ($footerRow5 = mysql_fetch_array($footerResult5)) {
                        $url5 = preg_replace('/^www\./', '', $footerRow5['FP_Link']);
                        if ($footerRow5['FP_Photo'] !== "") {
                            if (isset($footerRow5['FP_Link']) && $footerRow5['FP_Link'] != "") {
                                ?>
                                <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $url5) ?>"><img src="http://<?php echo DOMAIN . IMG_LOC_REL . $footerRow5['FP_Photo'] ?>" alt="<?php echo $footerRow5['FP_Alt'] ?>" border="0" /></a>
                                <?php
                            } else {
                                ?>
                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $footerRow5['FP_Photo'] ?>" alt="<?php echo $footerRow5['FP_Alt'] ?>" border="0" />    
                                <?php
                            }
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    <?php } ?>
    <?php
    $footer3 = "SELECT FP_Link, FP_Photo, FP_Alt from tbl_Footer_Photo WHERE FP_Link != '' AND FP_Section = 3 AND FP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' ORDER BY FP_Order ASC";
    $footerResult3 = mysql_query($footer3, $db) or die("Invalid query: $footer3 -- " . mysql_error());
    $footerCount3 = mysql_num_rows($footerResult3);
    if ($footerCount3 > 0) {
        ?>
        <div class="footer-urls">
            <ul>
                <?php
                while ($footerRow3 = mysql_fetch_array($footerResult3)) {
                    $url3 = $footerRow3['FP_Link'];
                    if (filter_var($url3, FILTER_VALIDATE_URL)) {
                        $url3 = preg_replace('/^www\./', '', $footerRow3['FP_Link']);
                        ?>
                        <li> <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $url3) ?>"><?php echo $footerRow3['FP_Alt']; ?></a> </li>
                    <?php } else {
                        ?>
                        <li> <a href="<?php echo str_replace(array('http://', 'https://'), '', $url3) ?>"><?php echo $footerRow3['FP_Alt']; ?></a> </li> 
                        <?php
                    }
                }
                ?>
            </ul>
        </div>
    <?php } ?>
    <div class="footer-copyrights">  
        <div class="footer-copy-wrapper ckeditor-anchors">
            <p><?php echo $REGION['R_Footer_Text']; ?></p>
        </div>
    </div>


</div>
</footer>        
<!--Footer End-->
<script>
    function play_video(id, player_id) {
        PlayerReady(id, player_id);
    }
    function get_youtube_videoid(url) {
        var video_id = url.split('v=')[1];
        var ampersandPosition = video_id.indexOf('&');
        if (ampersandPosition != -1) {
            video_id = video_id.substring(0, ampersandPosition);
        }
        return video_id;
    }
    // 2. This code loads the IFrame Player API code asynchronously.
    var tag = document.createElement('script');

    tag.src = "http://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // 3. This function creates an <iframe> (and YouTube player)
    //    after the API code downloads.
    var total_players = $("#total_players").val();
    for (var i = 1; i < total_players; i++) {
        eval("var player" + i);
    }
    var current_player_id;

    function onYouTubePlayerAPIReady() {
        for (var i = 1; i < total_players; i++) {
            eval("player" + i + "= new YT.Player('player" + i + "'," + "{" +
                    "height: '538'," +
                    "width: '940'," +
                    "videoId:'" + $("#video-link-" + i).val() +
                    "',events: {'onStateChange': onPlayerStateChange}});");
        }
    }

    // 4. The API will call this function when the video player is ready.
    function PlayerReady(id, player_id) {
        var width = $(".player" + id + "_wrapper").parent(".slide-images").find('img.slider_image').width();
        var height = $(".player" + id + "_wrapper").parent(".slide-images").find('img.slider_image').height();
        if (width == null) {
            var width = 580;
            var height = 351;
        }
        $(".player" + id + "_wrapper").parent(".slide-images").find('img.slider_image').css('opacity', '0');
        $(".center #prev").css('display', 'none');
        $(".center #next").css('display', 'none');
        // $(".player" + id + "_wrapper").css('top', '0');
        $(".player" + id + "_wrapper").css('opacity', '1');
        $(".player" + id + "_wrapper").css('z-index', '600');
        $('.sliderCycle').cycle('pause');
        this[player_id].setSize(width, height);
        this[player_id].playVideo();
        current_player_id = id;
    }

    // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.ENDED) {
            stopVideo();
        }
    }
    function stopVideo() {
        //    this[player_id].stopVideo();
        $(".player" + current_player_id + "_wrapper").css('opacity', '0');
        $(".player" + current_player_id + "_wrapper").css('z-index', '0');
        $(".player" + current_player_id + "_wrapper").parent(".slide-images").find('img.slider_image').css('opacity', '1');
        if (total_players > 2) {
            $(".center #prev").css('display', 'block');
            $(".center #next").css('display', 'block');
        }
        $('.sliderCycle').cycle('resume');
    }

</script>
<script src="/include/tripplanner.js" type="text/javascript"></script>
</body>
</html>
<?php
mysql_close($db);
?>