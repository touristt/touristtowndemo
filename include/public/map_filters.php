<?php
//apply dynamic css to map filters
$map_filter_text = explode("-", $REGION['R_Map_Filters_Text']);
$map_filter_drop_down_text = explode("-", $REGION['R_Map_Filters_Drop_Down_Text']);
?>
<style type="text/css">
    .header-main-nav.maps {
        background-color: <?php echo $REGION['R_Map_Filters_Background_Color'] ?>;
    }
    .header-main-nav.maps .main-nav-wrapper ul li span.filter-title {
        font-family: <?php echo (isset($THEME_ARRAY[$map_filter_text[0]]) === true && empty($THEME_ARRAY[$map_filter_text[0]]) === false) ? $THEME_ARRAY[$map_filter_text[0]] : ""; ?>;
        font-size: <?php echo $map_filter_text[1] ?>px;
        color: <?php echo $map_filter_text[2] ?>;
    }
    .header-main-nav.maps .main-nav-wrapper ul li span.filter-title:hover {
        background-color: <?php echo $map_filter_text[2] ?>;
        color: <?php echo $REGION['R_Map_Filters_Background_Color'] ?>; 
    }
    .header-main-nav.maps .main-nav-wrapper ul li ul.submenu{
        background-color: <?php echo $REGION['R_Map_Filters_Drop_Down_Background_Color'] ?>;
    }
    .header-main-nav.maps .main-nav-wrapper ul li ul.submenu .dd-inner .map-filter-checkbox .label{
        font-family: <?php echo (isset($THEME_ARRAY[$map_filter_drop_down_text[0]]) === true && empty($THEME_ARRAY[$map_filter_drop_down_text[0]]) === false) ? $THEME_ARRAY[$map_filter_drop_down_text[0]] : ""; ?>;
        color: <?php echo $map_filter_drop_down_text[2] ?>;
    }
    .map-filter-checkbox{
        font-size: <?php echo $map_filter_drop_down_text[1] ?>px;
    }
</style>
<form method="POST" id="map-filters-form" action="">
    <!--hidden field to know if map page or not-->
    <input type="hidden" name="is_map" value="<?php echo isset($is_map) ? $is_map : 0 ?>" />
    <?php
    if (isset($routeCatID) && $routeCatID > 0) {
        //hidden field to know its route page
        echo '<input type="hidden" name="is_route" value="1" />';
        echo '<div class="description-wrapper">
            <div class="description-inner">
              <h1 class="heading-text">' . $RouteCat['RC_Name'] . '</h1>
                <div class="map-top-filters">';
        $sqlRoute = "SELECT IR_ID, IR_Title FROM tbl_Individual_Route 
                INNER JOIN tbl_Individual_Route_Category ON IR_ID = IRC_IR_ID 
                WHERE IRC_RC_ID = '" . encode_strings($routeCatID, $db) . "'
                GROUP BY IR_ID
                ORDER BY IRC_Order ASC";
        $resRoute = mysql_query($sqlRoute, $db) or die("Invalid query: $sqlRoute -- " . mysql_error());
        while ($Route = mysql_fetch_assoc($resRoute)) {
            ?>
            <div class="map-filter-checkbox">
                <div class="squaredCheckbox">
                    <input type="checkbox" id="irid_<?php echo $Route['IR_ID'] ?>" name="route[]" value="<?php echo $Route['IR_ID'] ?>"/>
                    <span class="checkboxStyling"></span>
                </div>
                <label for="irid_<?php echo $Route['IR_ID'] ?>" class="label"><?php echo $Route['IR_Title']; ?></label>
            </div>
            <?php
        }
        echo '    </div>
            </div>
          </div>';
        //add hidden field for route category
        echo '<input type="hidden" name="route_category" value="' . $routeCatID . '"';
    } else if (isset($routeID) && $routeID > 0) {
        //hidden field to know its route page
        echo '<input type="hidden" name="is_route" value="1" />';
        //In case coming from individual route page
        echo '<input type="hidden" name="route[]" value="' . $routeID . '"';
    }
    ?>
    <div class="map-bottom-filters">
        <?php
        $sql_nav = "SELECT C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category 
                    LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                    INNER JOIN tbl_Business_Listing_Category ON BLC_M_C_ID = C_ID    
                    INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID AND BLCR_BLC_R_ID " . encode_strings(($REGION['R_Type'] == 1 ? "IN " . $regionList : "= " . $REGION['R_ID']), $db) . "
                    INNER JOIN tbl_Business_Listing ON BL_ID = BLC_BL_ID
                    WHERE C_Parent = 0 AND RC_Status = 0 AND C_Is_Blog = 0 AND C_ID != 8 $include_free_listings_on_map GROUP BY C_ID ORDER BY RC_Order ASC";
        $result = mysql_query($sql_nav);
        ?>
        <!--Main Navigation End...-->
        <nav class="header-main-nav maps">
            <div class="main-nav-container">
                <div class="main-nav-wrapper">
                    <ul>
                        <li><span class="filter-title no-link">Map Filters</span></li>
                        <?php
                        while ($row = mysql_fetch_array($result)) {
                            if ($row['RC_Name'] != '') {
                                echo '<li><span class="filter-title">' . $row['RC_Name'] . '</span>';
                            } else {
                                echo '<li><span class="filter-title">' . $row['C_Name'] . '</span>';
                            }
                            $c_parent = $row['C_ID'];
                            $sql_nav_sub = "SELECT C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category 
                                            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = " . encode_strings($REGION['R_ID'], $db) . "
                                            INNER JOIN tbl_Business_Listing_Category ON BLC_C_ID = C_ID    
                                            INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID
                                            INNER JOIN tbl_Business_Listing ON BL_ID = BLC_BL_ID AND BLCR_BLC_R_ID " . encode_strings(($REGION['R_Type'] == 1 ? "IN " . $regionList : "= " . $REGION['R_ID']), $db) . "  
                                            WHERE C_Parent = '$c_parent' and RC_Status=0 AND RC_R_ID > 0 AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing='1' $include_free_listings_on_map
                                            GROUP BY C_ID ORDER BY C_Order";

                            $result_sub = mysql_query($sql_nav_sub);
                            echo '<ul class="submenu"><li><div class="dd-inner">';
                            ?>
                            <?php
                            while ($rowSub = mysql_fetch_assoc($result_sub)) {
                                ?>
                                <div class="map-filter-checkbox">
                                    <div class="squaredCheckbox">
                                        <input type="checkbox" id="cid_<?php echo $c_parent ?>" name="sub_category[<?php echo $c_parent ?>][]" value="<?php echo $rowSub['C_ID'] ?>"/>
                                        <span class="checkboxStyling"></span>
                                    </div>
                                    <label for="cid_<?php echo $rowSub['C_ID'] ?>" class="label">
                                        <?php
                                        if ($rowSub['RC_Name'] != '') {
                                            echo $rowSub['RC_Name'];
                                        } else {
                                            echo $rowSub['C_Name'];
                                        }
                                        ?>
                                    </label>
                                </div>
                                <?php
                            }
                            echo "</li></ul>";
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</form>