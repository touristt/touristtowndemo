<div class="header">
    <div class="header-inner">
        <div class="logo">
            <a href="/">
                <img src="http://<?= DOMAIN ?>/images/TouristTown-Logo.gif" alt="Tourist Town" width="195" height="43" hspace="20" vspace="20" border="0" />
            </a>
        </div>
        <div class="info">
            <?PHP
            if ($_SESSION['BUSINESS_ID']) {
                echo  $_SESSION['BUSINESS_NAME'] . '     <a href="' . BUSINESS_DIR . '?logop=logout">Logout</a>';
           } else {
                echo '&nbsp;';
            }
            ?>
        </div>
    </div>
</div>
