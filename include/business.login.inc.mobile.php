<?PHP

define('SESSIONERROR', 'BUSINESS_ERROR');
define('SESSIONINFO', 'BUSINESS_INFO');

$sql_select = "SELECT B_ID, B_Name, B_Email FROM tbl_Business ";

if (isset($_REQUEST['con']) && $_REQUEST['con'] != "") {
    $freeListing = "SELECT BL_Free_Listing_Status_Hash FROM tbl_Business_Listing WHERE BL_ID = '" . $_REQUEST['BL_ID'] . "' AND BL_Free_Listing_status = 1";
    $freeRes = mysql_query($freeListing);
    $freeRow = mysql_fetch_assoc($freeRes);
    if ($_REQUEST['con'] == $freeRow['BL_Free_Listing_Status_Hash']) {
        $sql_update_hash = "DELETE tbl_Business_Listing, tbl_Business_Listing_Ammenity, 
                            tbl_Business_Listing_Category, tbl_Business_Listing_Category_Region,
                            tbl_Business_Social, tbl_BL_Feature, payment_profiles,
                            tbl_Business_Feature_About, tbl_Business_Feature_About_Photo,
                            tbl_Business_Feature_About_Main_Photo, tbl_Business_Feature_Daily_Specials,
                            tbl_Business_Feature_Daily_Specials_Description, tbl_Business_Feature_Entertainment_Acts,
                            tbl_Business_Feature_Entertainment_Description, tbl_Business_Feature_Guest_Book, tbl_Business_Feature_Guest_Book_Description,
                            tbl_Business_Feature_Menu, tbl_Business_Feature_Photo, tbl_Business_Feature_Product, tbl_Business_Feature_Product_Photo,
                            tbl_Business_Feature_Coupon, tbl_Business_Feature_Coupon_Category_Multiple, tbl_Image_Bank_Usage
                            FROM tbl_Business_Listing 
                            LEFT JOIN tbl_Business_Listing_Ammenity ON BLA_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Listing_Category_Region ON BLCR_BLC_ID = BLC_ID 
                            LEFT JOIN tbl_Business_Social ON BS_BL_ID = BL_ID
                            LEFT JOIN tbl_BL_Feature ON BLF_BL_ID = BL_ID
                            LEFT JOIN payment_profiles ON listing_id = BL_ID
                            LEFT JOIN tbl_Business_Feature_About ON BFA_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_About_Photo ON BFAP_BFA_ID = BFA_ID
                            LEFT JOIN tbl_Business_Feature_About_Main_Photo ON BFAMP_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Daily_Specials ON BFDS_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Daily_Specials_Description ON BFDSD_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Entertainment_Acts ON BFEA_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Entertainment_Description ON BFED_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Guest_Book ON BFGB_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Guest_Book_Description ON tbl_Business_Feature_Guest_Book_Description.BFGB_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Menu ON BFM_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Photo ON BFP_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Product ON tbl_Business_Feature_Product.BFP_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Product_Photo ON BFPP_BFP_ID = tbl_Business_Feature_Product.BFP_ID
                            LEFT JOIN tbl_Business_Feature_Coupon ON tbl_Business_Feature_Coupon.BFC_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
                            LEFT JOIN tbl_Image_Bank_Usage ON IBU_BL_ID = BL_ID
                            WHERE BL_ID = '" . encode_strings($_REQUEST['BL_ID'], $db) . "'";
        $result_hash = mysql_query($sql_update_hash);
        $_SESSION['update_hash_success'] = 1;
    } else {
        $_SESSION['update_hash_error'] = 1;
    }
}

if ($_REQUEST['logop'] == "login") {
    $sql_log = $sql_select
            . "WHERE B_Email 	= '" . encode_strings($_POST['LOGIN_USER'], $db) . "' "
            . "&& B_Password = '" . md5(encode_strings($_POST['LOGIN_PASS'], $db)) . "' ";
    $result_log = mysql_query($sql_log, $db) or die("Invalid query: $sql_log -- " . mysql_error());
    if (mysql_num_rows($result_log) <> 1) {
        $_SESSION['login_error'] = 1;
        header("Location: " . BUSINESS_DIR . "mobile/login.php");
        exit();
    } else {
        if ($_POST['remember_me'] == 'on') {
            $cookie_name = 'user_email_id';
            $cookie_value = $LOGIN_USER;
            setcookie($cookie_name, $cookie_value, time() + (60 * 60 * 24 * 30), "/"); // 86400 = 1 day
            $cookie_pass = 'user_password';
            $cookie_value_pass = $LOGIN_PASS;
            setcookie($cookie_pass, $cookie_value_pass, time() + (60 * 60 * 24 * 30), "/"); // 86400 = 1 day
        }
        $row = mysql_fetch_assoc($result_log);
        $sql_Listing = "SELECT BL_ID FROM tbl_Business_Listing WHERE BL_B_ID = " . $row['B_ID'];
        $sql_Listing_Result = mysql_query($sql_Listing, $db) or die("Invalid query: $sql_Listing -- " . mysql_error());
        $sql_Free_Listing = "SELECT BL_ID FROM tbl_Business_Listing WHERE BL_B_ID = " . $row['B_ID'] . " AND BL_Listing_Type = 1";
        $sql_Free_Listing_Result = mysql_query($sql_Free_Listing, $db) or die("Invalid query: $sql_Free_Listing -- " . mysql_error());
        if (mysql_num_rows($sql_Listing_Result) == mysql_num_rows($sql_Free_Listing_Result)) {
            $sql_Free_Listing_Unverified = "SELECT BL_ID FROM tbl_Business_Listing WHERE BL_B_ID = " . $row['B_ID'] . " AND BL_Listing_Type = 1 AND BL_Free_Listing_status = 1";
            $sql_Free_Listing_Unverified_Result = mysql_query($sql_Free_Listing_Unverified, $db) or die("Invalid query: $sql_Free_Listing_Unverified -- " . mysql_error());
            if (mysql_num_rows($sql_Free_Listing_Result) == mysql_num_rows($sql_Free_Listing_Unverified_Result)) {
                $sql_Free_Listing_Unverified_Date = "SELECT BL_ID FROM tbl_Business_Listing WHERE BL_B_ID = " . $row['B_ID'] . " AND BL_Listing_Type = 1 
                                                    AND BL_Free_Listing_status = 1 AND CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)";
                $sql_Free_Listing_Verify_Unverified_Date_Result = mysql_query($sql_Free_Listing_Unverified_Date, $db) or die("Invalid query: $sql_Free_Listing_Unverified_Date -- " . mysql_error());
                if (mysql_num_rows($sql_Free_Listing_Verify_Unverified_Date_Result) < 1) {
                    $_SESSION['login_error'] = 2;
                    header("Location: " . BUSINESS_DIR . "mobile/login.php");
                    exit();
                }
            }
        }
        $BID = UpdateUserLog($row, $db);
        if ($_REQUEST['updatecc'] == 1) {
            header("Location: " . BUSINESS_DIR . "customer-credit-card-details.php");
            exit();
        }
    }
} elseif ($_REQUEST['logop'] == 'logout') {
    $sql = "UPDATE tbl_Business SET B_Expiry = '0000-00-00' "
            . "WHERE B_Key = '" . $_SESSION['BUSINESS_USER_KEY'] . "' ";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $_SESSION['BUSINESS_USER_KEY'] = 0;
    $_SESSION['BUSINESS_ID'] = 0;
    header("Location: " . BUSINESS_DIR . "mobile");
    exit();
} elseif (isset($_SESSION['BUSINESS_USER_KEY']) && $_SESSION['BUSINESS_USER_KEY'] != '0' && $_SESSION['BUSINESS_USER_KEY'] != '') {
    $sql_Check = $sql_select
            . "WHERE B_Key = '" . encode_strings($_SESSION['BUSINESS_USER_KEY'], $db) . "' "
            . "&& B_Expiry > NOW() ";
    $result_Check = mysql_query($sql_Check, $db) or die("Invalid query: $sql_Check -- " . mysql_error());
    if (mysql_num_rows($result_Check) <> 1) {
        my_session_destroy('member');
        header("Location: " . BUSINESS_DIR . "mobile/login.php");
        exit();
    } else {
        $row = mysql_fetch_assoc($result_Check);
        $BID = UpdateUserLog($row, $db, $_SESSION['BUSINESS_USER_KEY']);
    }
} else {
    my_session_destroy('member');
    header("Location: " . BUSINESS_DIR . "mobile/login.php");
    exit();
}

$sql = "SELECT B_Agreement_Accepted FROM tbl_Business WHERE B_ID = '" . encode_strings($_SESSION['BUSINESS_ID'], $db) . "' ";
$result = mysql_query($sql, $db);
$rowBIZ = mysql_fetch_assoc($result);
if ($_POST['agreementOP'] == 'save') {
    if ($_POST['agreed']) {
        $sql = "UPDATE tbl_Business SET B_Agreement_Accepted = '1', B_Agreement_Accepted_Date = NOW() 
                WHERE B_ID = '" . encode_strings($_SESSION['BUSINESS_ID'], $db) . "' LIMIT 1 ";
        mysql_query($sql, $db);
    } else {
        header("Location: " . BUSINESS_DIR . "mobile");
        exit();
    }
} elseif (!$rowBIZ['B_Agreement_Accepted'] && $bypass == false) {
    $sign = true;
    require 'agreement.php';
    exit();
}

function UpdateUserLog($row, $db, $key = "0") {
    if ($row) {
        $sql = "UPDATE tbl_Business SET B_Expiry = DATE_ADD(NOW(), "
                . "INTERVAL 1 HOUR)";
        if ($key == "0") {
            $key = mt_rand(1000, 9999999);
            $sql .= ", B_Key = '$key'";
            $_SESSION['BUSINESS_USER_KEY'] = $key;
            $_SESSION['BUSINESS_ID'] = $row['B_ID'];
            $_SESSION['BUSINESS_NAME'] = $row['B_Name'];
            $_SESSION['BUSINESS_USER_EM'] = $row['B_Email'];
        }
        $sql .= " WHERE B_ID = '$row[B_ID]' ";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    } else {
        header("Location: " . BUSINESS_DIR . "mobile");
        exit();
    }
    return $row['B_ID'];
}

?>