<div class="main-nav-image-admin" style="width:100%">
    <div class="main-nav-image-search-admin">
        <div class="main-nav-image-search-text">
            <form onSubmit="return search_func()" id="auto_complete_form_submit">
                <div class="main-nav-image-search-image">
                    <input class="submit-autocomplete" type="submit" name="submit" value="submit">
                </div>
                <div class="main-nav-image-search-text-field">
                    <input type="text" id="autocomplete">    
                    <input type="hidden" id="keywords-searched" name="search-image" value="">    
                </div>
                <?php $multiple = array(1, 101); ?>
                <div  id="btn_upload_from_pc_library" class="main-nav-two-buttons">
                    <div id="upload_from_pc"><label style="font-family:Verdana,Arial,sans-serif;font-size:13px;" for="photo<?php echo ((isset($_REQUEST['iteration']) && $_REQUEST['iteration'] > 0) ? $_REQUEST['iteration'] : $_REQUEST['image_id']) ?>" class="daily_browse">Upload a new photo</label>
                        <input  accept="image/*" id="photo<?php echo ((isset($_REQUEST['iteration']) && $_REQUEST['iteration'] > 0) ? $_REQUEST['iteration'] : $_REQUEST['image_id']) ?>" type="file" name="pic[]" style="display: none;" <?php echo ((in_array($_REQUEST['image_id'], $multiple)) ? 'multiple' : '') ?>/>
                    </div>
                    <div id="upload_from_library"><button style="font-family:Verdana,Arial,sans-serif;font-size:13px;background: #6dac29;border: none;color: #fff7ff;" onclick="select_one()">Upload</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<div class="main-nav-image-admin-inner">
    <ul>
        <li class="first-padding-li">
            Refine :
        </li>
        <?php if (isset($_SESSION['USER_ROLES']) && !in_array('county', $_SESSION['USER_ROLES'])) { ?>
            <li>
                <a>Website</a>
                <?php
                $sql = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Domain != '' ORDER BY R_Name ASC";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                ?>     
                <ul class="region-image-bank">
                    <?php
                    $i = 0;
                    $tempReg = isset($tempregionFilter) ? explode(',', $tempregionFilter) : array();
                    while ($region = mysql_fetch_assoc($result)) {
                        ?>
                        <div class="main-nav-image-admin-inner-sub-menu">
                            <li>
                                <div class="squaredOne">
                                    <input name="regionfilter" type="checkbox" id="<?php echo $region['R_Name'] ?>_<?php echo $region['R_ID'] ?>" value="<?php echo $region['R_ID'] ?>" <?php echo (in_array($region['R_ID'], $tempReg)) ? 'checked' : ''; ?> onchange="search_func();">
                                    <label for="<?php echo $region['R_Name'] ?>_<?php echo $region['R_ID'] ?>"></label>
                                </div>
                                <span><?php echo $region['R_Name'] ?></span>
                            </li> 
                        </div>          
                        <?php
                    }
                    ?>
                </ul>
            </li>
        <?php } ?>
        <li>
            <a>Season</a>
            <?PHP
            $sql = "SELECT * FROM tbl_Image_Bank_Season ORDER BY IBS_ID";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            ?>
            <ul>
                <?php
                $i = 0;
                $tempSeason = isset($seasonFilter) ? explode(',', $seasonFilter) : array();
                while ($season = mysql_fetch_assoc($result)) {
                    if ($i == 0) {
                        ?>
                        <div class="main-nav-image-admin-inner-sub-menu">
                            <?php
                        }
                        ?>
                        <li>
                            <div class="squaredOne">
                                <input name="seasonfilter" type="checkbox" id="<?php echo $season['IBS_Name'] ?>_<?php echo $season['IBS_ID'] ?>" value="<?php echo $season['IBS_ID'] ?>" <?php echo (in_array($season['IBS_ID'], $tempSeason)) ? 'checked' : ''; ?> onchange="search_func();">
                                <label for="<?php echo $season['IBS_Name'] ?>_<?php echo $season['IBS_ID'] ?>"></label>
                            </div>
                            <span><?php echo $season['IBS_Name'] ?></span>
                        </li>
                        <?php
                        $i++;
                        if ($i == 4) {
                            $i = 0;
                            ?> 
                        </div>          
                        <?php
                    }
                }
                ?>
            </ul>          
        </li>
        <li>
            <a>Category</a>
            <?PHP
            $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = 0 ORDER BY C_Order ASC";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            ?>
            <ul>
                <?php
                $i = 0;
                $tempCat = isset($catFilter) ? explode(',', $catFilter) : array();
                while ($cat = mysql_fetch_assoc($result)) {
                    if ($i == 0) {
                        ?>
                        <div class="main-nav-image-admin-inner-sub-menu">
                            <?php
                        }
                        ?>
                        <li>
                            <div class="squaredOne">
                                <input name="catfilter" type="checkbox" id="<?php echo $cat['C_Name'] ?>_<?php echo $cat['C_ID'] ?>" value="<?php echo $cat['C_ID'] ?>" <?php echo (in_array($cat['C_ID'], $tempCat)) ? 'checked' : ''; ?> onchange="search_func();">
                                <label for="<?php echo $cat['C_Name'] ?>_<?php echo $cat['C_ID'] ?>"></label>
                            </div>
                            <span><?php echo $cat['C_Name'] ?></span>
                        </li>
                        <?php
                        $i++;
                        if ($i == 4) {
                            $i = 0;
                            ?> 
                        </div>          
                        <?php
                    }
                }
                ?>
            </ul>             
        </li>
        <li>
            <a>People</a>
            <?PHP
            $sql = "SELECT * FROM tbl_Image_Bank_People ORDER BY IBP_ID";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            ?>
            <ul>
                <?php
                $i = 0;
                $tempPeople = isset($peopleFilter) ? explode(',', $peopleFilter) : array();
                while ($people = mysql_fetch_assoc($result)) {
                    if ($i == 0) {
                        ?>
                        <div class="main-nav-image-admin-inner-sub-menu">
                            <?php
                        }
                        ?>
                        <li>
                            <div class="squaredOne">
                                <input name="peoplefilter" type="checkbox" id="<?php echo $people['IBP_Name'] ?>_<?php echo $people['IBP_ID'] ?>" value="<?php echo $people['IBP_ID'] ?>" <?php echo (in_array($people['IBP_ID'], $tempPeople)) ? 'checked' : ''; ?> onchange="search_func();">
                                <label for="<?php echo $people['IBP_Name'] ?>_<?php echo $people['IBP_ID'] ?>"></label>
                            </div>
                            <span><?php echo $people['IBP_Name'] ?></span>
                        </li>
                        <?php
                        $i++;
                        if ($i == 4) {
                            $i = 0;
                            ?> 
                        </div>          
                        <?php
                    }
                }
                ?>
            </ul>
        </li>
        <?php if (isset($_SESSION['USER_ROLES']) && !in_array('county', $_SESSION['USER_ROLES'])) { ?>
            <li>
                <a>Owner</a>
                <?PHP
                $sql = "SELECT * FROM tbl_Photographer_Owner ORDER BY PO_ID";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                ?>
                <ul>
                    <?php
                    $i = 0;
                    $tempOwner = isset($ownerFilter) ? explode(',', $ownerFilter) : array();
                    while ($owner = mysql_fetch_assoc($result)) {
                        if ($i == 0) {
                            ?>
                            <div class="main-nav-image-admin-inner-sub-menu">
                                <?php
                            }
                            ?>
                            <li>
                                <div class="squaredOne">
                                    <input name="ownerfilter" type="checkbox" id="<?php echo $owner['PO_Name'] ?>_<?php echo $owner['PO_ID'] ?>" value="<?php echo $owner['PO_ID'] ?>" <?php echo (in_array($owner['PO_ID'], $tempOwner)) ? 'checked' : ''; ?> onchange="search_func();">
                                    <label for="<?php echo $owner['PO_Name'] ?>_<?php echo $owner['PO_ID'] ?>"></label>
                                </div>
                                <span><?php echo $owner['PO_Name'] ?></span>
                            </li>
                            <?php
                            $i++;
                            if ($i == 4) {
                                $i = 0;
                                ?> 
                            </div>          
                            <?php
                        }
                    }
                    ?>
                </ul>
            </li>
        <?php } ?>
        <li>
            <a>Campaign</a>
            <?PHP
            $sql = "SELECT * FROM tbl_Image_Bank_Campaign ORDER BY IBC_ID";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            ?>
            <ul>
                <?php
                $i = 0;
                $tempCampaign = isset($campaignFilter) ? explode(',', $campaignFilter) : array();
                while ($campaign = mysql_fetch_assoc($result)) {
                    if ($i == 0) {
                        ?>
                        <div class="main-nav-image-admin-inner-sub-menu">
                            <?php
                        }
                        ?>
                        <li>
                            <div class="squaredOne">
                                <input name="campaignfilter" type="checkbox" id="<?php echo $campaign['IBC_Name'] ?>_<?php echo $campaign['IBC_ID'] ?>" value="<?php echo $campaign['IBC_ID'] ?>" <?php echo (in_array($campaign['IBC_ID'], $tempCampaign)) ? 'checked' : ''; ?> onchange="search_func();">
                                <label for="<?php echo $campaign['IBC_Name'] ?>_<?php echo $campaign['IBC_ID'] ?>"></label>
                            </div>
                            <span><?php echo $campaign['IBC_Name'] ?></span>
                        </li>
                        <?php
                        $i++;
                        if ($i == 4) {
                            $i = 0;
                            ?> 
                        </div>          
                        <?php
                    }
                }
                ?>
            </ul>
        </li>
    </ul>
</div>
</div>

<script>
    var IMG_URL = '<?php echo "http://" . DOMAIN . IMG_BANK_REL ?>';
    $('#photo' +<?php echo ((isset($_REQUEST['iteration']) && $_REQUEST['iteration'] > 0) ? $_REQUEST['iteration'] : $_REQUEST['image_id']) ?>).change(function () {
        if ($('#photo' +<?php echo ((isset($_REQUEST['iteration']) && $_REQUEST['iteration'] > 0) ? $_REQUEST['iteration'] : $_REQUEST['image_id']) ?>).val() !== '') {
            select_new_image($('#photo' +<?php echo ((isset($_REQUEST['iteration']) && $_REQUEST['iteration'] > 0) ? $_REQUEST['iteration'] : $_REQUEST['image_id']) ?>).val(), <?php echo (isset($_REQUEST['image_id']) ? $_REQUEST['image_id'] : "") ?>, <?php echo $_REQUEST['iteration'] ?>);
        }
    });

    // this will enable double click selection in other than photo gallery pages
    var focus = '<?php echo (isset($_REQUEST['image_id']) ? $_REQUEST['image_id'] : "") ?>';
    if (focus != 101 && focus != 1) {
        $('.image-hover-bank').attr('ondblclick', 'select_one()');
    }

    function select_one() {
        var idsArray = ids.split(",");
        if (idsArray[0] > 0 && idsArray.length > 1) {
            select_multiple_image(ids, <?php echo (isset($_REQUEST['image_id']) ? $_REQUEST['image_id'] : "") ?>, <?php echo $_REQUEST['iteration'] ?>);
        } else {
            select_multiple_image(ids, <?php echo (isset($_REQUEST['image_id']) ? $_REQUEST['image_id'] : "") ?>, <?php echo $_REQUEST['iteration'] ?>, image_name, IMG_URL);
        }
    }

    function remove(id) {
        $("#" + id).remove();
        // removing the removed value from selected ids array
        var temp = 0;
        var idsArray = ids.split(",");
        if (idsArray.length == 1 && (focus == 101 || focus == 1)) {
            $('#upload_from_library').css({'display': 'none'});
        }
        for (var i = 0; i < idsArray.length; i++) {
            if (id != idsArray[i]) {
                if (temp == 0) {
                    temp = idsArray[i];
                } else {
                    temp += "," + idsArray[i];
                }
            }
        }
        // if ids is empty then put empty instead of 0
        if (temp == 0) {
            ids = '';
        } else {
            ids = temp;
        }
    }

    var ids = 0;
    var image_name;
    var limit = 30 - '<?php echo $_REQUEST['limit'] ?>';

    function select_the_image(imageName, id) {
        image_name = imageName;
        $('#upload_from_library').css({'display': 'block'});
        if (image_name !== '' && id !== '') {
            var check = 0;
            if (ids == 0) {
                ids = id;
                check = 1;
            } else if (focus == 101 || focus == 1) {
                var idsArray = ids.split(",");
                var check_photo_gallery = 1;
                for (var i = 0; i < idsArray.length; i++) {
                    if (id == idsArray[i]) {
                        $('#bgcolor_' + id).css({'background-color': '#e8eeee'});
                        remove(id);
                        check_photo_gallery = 0;
                        break;
                    }
                }
                if (idsArray.length >= limit && check_photo_gallery == 1) {
                    swal("Limit Exceeded!", "You can not upload more than 30 photos in photo gallery.", "warning");
                    check_photo_gallery = 0;
                }
                if (check_photo_gallery == 1) {
                    ids += "," + id;
                    check = 1;
                }
            } else {
                var idsArray = ids.split(",");
                for (var i = 0; i < idsArray.length; i++) {
                    $('#bgcolor_' + idsArray[i]).css({'background-color': '#e8eeee'});
                    remove(idsArray[i]);
                }
                ids = id;
                check = 1;
            }
            if (check == 1) {
                $('#bgcolor_' + id).css({'background-color': '#ebdabe'});
            }
        }
        return ids;
    }

    function maintain_selection() {
        if (ids != 'undefined' && ids != '') {
            var idsArray = ids.split(",");
            for (var i = 0; i < idsArray.length; i++) {
                $('#bgcolor_' + idsArray[i]).css({'background-color': '#ebdabe'});
            }
        }
    }
    
    function Delete_Images(id){
        if (confirm('Are you sure you want to delete this image?')) {
        $.post('image-bank-multiple-photo-delete.php', {
            ids: id
        }, function (done) {
                location.reload();
        }); 
        }
        return false;
    }
    
    function search_func() {
        var bl_id = '<?php echo (isset($BL_ID)) ? $BL_ID : 0; ?>';
        var image_id = '<?php echo $_REQUEST['image_id']; ?>';
        var iteration = '<?php echo $_REQUEST['iteration']; ?>';
        var keywords = $('#keywords-searched').val();
        var region = new Array();
        var season = new Array();
        var owner = new Array();
        var cat = new Array();
        var people = new Array();
        var campaign = new Array();
        $("input[name=regionfilter]:checked").each(function () {
            region.push($(this).val());
        });
        $("input[name=seasonfilter]:checked").each(function () {
            season.push($(this).val());
        });
        $("input[name=catfilter]:checked").each(function () {
            cat.push($(this).val());
        });
        $("input[name=peoplefilter]:checked").each(function () {
            people.push($(this).val());
        });
        $("input[name=ownerfilter]:checked").each(function () {
            owner.push($(this).val());
        });
        $("input[name=campaignfilter]:checked").each(function () {
            campaign.push($(this).val());
        });
        $.ajax({
            url: "show_image_bank_ajax.php",
            type: "GET",
            data: {
                bl_id: bl_id,
                region: region.join(','),
                season: season.join(','),
                cat: cat.join(','),
                people: people.join(','),
                owner: owner.join(','),
                campaign: campaign.join(','),
                search_image: keywords,
                image_id: image_id,
                iteration: iteration
            },
            success: function (html) {
                $(".image-bank-container").empty();
                $(".image-bank-container").html(html);
            }
        });
        return false;
    }
</script>
