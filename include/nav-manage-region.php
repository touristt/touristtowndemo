<div class="sub-menu" style="margin-bottom: 25px;">
    <ul>
        <?php if (in_array('regions', $_SESSION['USER_PERMISSIONS'])) { ?>
            <li><a href="region.php?rid=<?php echo $regionID ?>">Manage Website</a></li>
            <li><a href="theme-options.php?rid=<?php echo $regionID ?>">Theme Options</a></li>
            <li><a href="theme-options-mobile.php?rid=<?php echo $regionID ?>">Theme Options Mobile</a></li>
            <li><a href="map-color-options.php?rid=<?php echo $regionID ?>">Manage Map </a></li>
            <li><a href="region-slider.php?rid=<?php echo $regionID ?>">Manage Home Slider</a></li>
            <li><a href="region-footer.php?rid=<?php echo $regionID ?>">Manage Footer</a></li>
            <li><a href="region-social-media.php?rid=<?php echo $regionID ?>">Manage Social Media</a></li>
            <li><a href="homepage-paintings.php?rid=<?php echo $regionID ?>">Manage Homepage Image</a></li>
            <li><a href="regions-category.php?rid=<?php echo $regionID ?>">Manage Categories</a></li>
            <li><a href="main-navigation.php?rid=<?php echo $regionID ?>">Manage Main Navigation</a></li>
            <li><a href="region-404.php?rid=<?php echo $regionID ?>">Manage 404 page</a></li>
            <li><a href="listings-404.php?rid=<?php echo $regionID ?>">Manage 404 Listings</a></li>
            <li><a href="customer-region-statistics.php?rid=<?= $regionID ?>">Statistics</a></li>
        <?php } if (in_array('manage-routes', $_SESSION['USER_PERMISSIONS']) && $activeRegion['R_Type'] != 7) { ?>
            <li id="nav_manage_routes"><a href="routes-main.php?rid=<?php echo $regionID ?>">Manage Routes</a></li>
        <?php } if (in_array('manage-county-region', $_SESSION['USER_PERMISSIONS'])) { ?>
            <li><a href="county-region.php?rid=<?php echo $regionID ?>">Manage Homepage</a></li>
            <li><a href="regions-category.php?rid=<?php echo $regionID ?>">Manage Categories</a></li>
        <?php } if (in_array('manage-stories', $_SESSION['USER_PERMISSIONS'])) { ?>
            <li id="nav_manage_stories"><a href="region-stories.php?rid=<?php echo $regionID ?>">Manage Stories</a></li>
            <li id="nav_homepage_stories"><a href="region-home-stories.php?rid=<?php echo $regionID ?>">Homepage Stories</a></li>
            <li id="nav_map_stories"><a href="region-map-stories.php?rid=<?php echo $regionID ?>">Map Stories</a></li>
        <?php } if ($activeRegion['R_Type'] == 4) { ?>
            <li ><a href="region-home-listings.php?rid=<?php echo $regionID ?>">Homepage Listings</a></li>
        <?php } if (in_array('regions', $_SESSION['USER_PERMISSIONS']) && $activeRegion['R_Type'] != 7) { ?>
            <li><a href="region-listings-seasons.php?rid=<?php echo $regionID ?>">Listings Seasons</a></li>
        <?php } if (isset($activeRegion['R_Type']) && $activeRegion['R_Type'] == 4) { ?>
            <li><a href="region-events.php?rid=<?php echo $regionID ?>">Select Events</a></li>
        <?php } ?>
        <li id="day_trip_menu"><a href="day-trips.php?rid=<?php echo $regionID ?>">Day Trips Listings</a></li>
    </ul>
</div>