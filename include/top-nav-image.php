<div class="main-nav-image-admin">
    <div class="main-nav-image-search-admin">
        <div class="main-nav-image-search-text">
            <form onSubmit="return search_func()" id="auto_complete_form_submit">
                <div class="main-nav-image-search-image">
                    <input class="submit-autocomplete" type="submit" name="submit" value="submit">
                </div>
                <div class="main-nav-image-search-text-field">
                    <input type="text" id="autocomplete">    
                    <input type="hidden" id="keywords-searched" name="search-image" value="">    
                </div>
                <div class="main-nav-image-search-total-image">
                    <?php
                    if (isset($count)) {
                        if ($count > 1) {
                            echo $count . ' images';
                        } else {
                            echo $count . ' image';
                        }
                    }
                    ?> 
                </div>
            </form>
            <div class="main-nav-image-search-button">    
                <a href="add-image-bank.php">+ Add photo</a>  
            </div>
        </div>
    </div>
    <div class="main-nav-image-admin-inner">
        <ul>
            <?php if (basename($_SERVER['PHP_SELF']) != 'image-bank-photo-detail.php') { ?>
                <li class="first-padding-li">
                    Refine :
                </li>
                <?php if (!in_array('county', $_SESSION['USER_ROLES'])) { ?>
                    <li>
                        <a>Website</a>
                        <?php
                        $sql = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Domain != '' ORDER BY R_Name ASC";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        ?>     
                        <ul class="region-image-bank">
                            <?php
                            $i = 0;
                            $tempReg = explode(',', $tempregionFilter);
                            while ($region = mysql_fetch_assoc($result)) {
                                ?>
                                <div class="main-nav-image-admin-inner-sub-menu">
                                    <li>
                                        <div class="squaredOne">
                                            <input name="regionfilter" type="checkbox" id="<?php echo $region['R_Name'] ?>_<?php echo $region['R_ID'] ?>" value="<?php echo $region['R_ID'] ?>" <?php echo (in_array($region['R_ID'], $tempReg)) ? 'checked' : ''; ?> onchange="search_func();">
                                            <label for="<?php echo $region['R_Name'] ?>_<?php echo $region['R_ID'] ?>"></label>
                                        </div>
                                        <span><?php echo $region['R_Name'] ?></span>
                                    </li> 
                                </div>          
                                <?php
                            }
                            ?>
                        </ul>
                    </li>
                <?php } ?>
                <li>
                    <a>Season</a>
                    <?PHP
                    $sql = "SELECT * FROM tbl_Image_Bank_Season ORDER BY IBS_ID";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    ?>
                    <ul>
                        <?php
                        $i = 0;
                        $tempSeason = explode(',', $seasonFilter);
                        while ($season = mysql_fetch_assoc($result)) {
                            if ($i == 0) {
                                ?>
                                <div class="main-nav-image-admin-inner-sub-menu">
                                    <?php
                                }
                                ?>
                                <li>
                                    <div class="squaredOne">
                                        <input name="seasonfilter" type="checkbox" id="<?php echo $season['IBS_Name'] ?>_<?php echo $season['IBS_ID'] ?>" value="<?php echo $season['IBS_ID'] ?>" <?php echo (in_array($season['IBS_ID'], $tempSeason)) ? 'checked' : ''; ?> onchange="search_func();">
                                        <label for="<?php echo $season['IBS_Name'] ?>_<?php echo $season['IBS_ID'] ?>"></label>
                                    </div>
                                    <span><?php echo $season['IBS_Name'] ?></span>
                                </li>
                                <?php
                                $i++;
                                if ($i == 4) {
                                    $i = 0;
                                    ?> 
                                </div>          
                                <?php
                            }
                        }
                        ?>
                    </ul>          
                </li>
                <li>
                    <a>Category</a>
                    <?PHP
                    $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = 0 ORDER BY C_Order ASC";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    ?>
                    <ul>
                        <?php
                        $i = 0;
                        $tempCat = explode(',', $catFilter);
                        while ($cat = mysql_fetch_assoc($result)) {
                            if ($i == 0) {
                                ?>
                                <div class="main-nav-image-admin-inner-sub-menu">
                                    <?php
                                }
                                ?>
                                <li>
                                    <div class="squaredOne">
                                        <input name="catfilter" type="checkbox" id="<?php echo $cat['C_Name'] ?>_<?php echo $cat['C_ID'] ?>" value="<?php echo $cat['C_ID'] ?>" <?php echo (in_array($cat['C_ID'], $tempCat)) ? 'checked' : ''; ?> onchange="search_func();">
                                        <label for="<?php echo $cat['C_Name'] ?>_<?php echo $cat['C_ID'] ?>"></label>
                                    </div>
                                    <span><?php echo $cat['C_Name'] ?></span>
                                </li>
                                <?php
                                $i++;
                                if ($i == 4) {
                                    $i = 0;
                                    ?> 
                                </div>          
                                <?php
                            }
                        }
                        ?>
                    </ul>             
                </li>
                <li>
                    <a>People</a>
                    <?PHP
                    $sql = "SELECT * FROM tbl_Image_Bank_People ORDER BY IBP_ID";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    ?>
                    <ul>
                        <?php
                        $i = 0;
                        $tempPeople = explode(',', $peopleFilter);
                        while ($people = mysql_fetch_assoc($result)) {
                            if ($i == 0) {
                                ?>
                                <div class="main-nav-image-admin-inner-sub-menu">
                                    <?php
                                }
                                ?>
                                <li>
                                    <div class="squaredOne">
                                        <input name="peoplefilter" type="checkbox" id="<?php echo $people['IBP_Name'] ?>_<?php echo $people['IBP_ID'] ?>" value="<?php echo $people['IBP_ID'] ?>" <?php echo (in_array($people['IBP_ID'], $tempPeople)) ? 'checked' : ''; ?> onchange="search_func();">
                                        <label for="<?php echo $people['IBP_Name'] ?>_<?php echo $people['IBP_ID'] ?>"></label>
                                    </div>
                                    <span><?php echo $people['IBP_Name'] ?></span>
                                </li>
                                <?php
                                $i++;
                                if ($i == 4) {
                                    $i = 0;
                                    ?> 
                                </div>          
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </li>
                <?php if (!in_array('county', $_SESSION['USER_ROLES'])) { ?>
                    <li>
                        <a>Owner</a>
                        <?PHP
                        $sql = "SELECT * FROM tbl_Photographer_Owner ORDER BY PO_ID";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        ?>
                        <ul>
                            <?php
                            $i = 0;
                            $tempOwner = explode(',', $ownerFilter);
                            while ($owner = mysql_fetch_assoc($result)) {
                                if ($i == 0) {
                                    ?>
                                    <div class="main-nav-image-admin-inner-sub-menu">
                                        <?php
                                    }
                                    ?>
                                    <li>
                                        <div class="squaredOne">
                                            <input name="ownerfilter" type="checkbox" id="<?php echo $owner['PO_Name'] ?>_<?php echo $owner['PO_ID'] ?>" value="<?php echo $owner['PO_ID'] ?>" <?php echo (in_array($owner['PO_ID'], $tempOwner)) ? 'checked' : ''; ?> onchange="search_func();">
                                            <label for="<?php echo $owner['PO_Name'] ?>_<?php echo $owner['PO_ID'] ?>"></label>
                                        </div>
                                        <span><?php echo $owner['PO_Name'] ?></span>
                                    </li>
                                    <?php
                                    $i++;
                                    if ($i == 4) {
                                        $i = 0;
                                        ?> 
                                    </div>          
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </li>
                <?php } ?>
                <li>
                    <a>Campaign</a>
                    <?PHP
                    $sql = "SELECT * FROM tbl_Image_Bank_Campaign ORDER BY IBC_ID";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    ?>
                    <ul>
                        <?php
                        $i = 0;
                        $tempCampaign = explode(',', $campaignFilter);
                        while ($campaign = mysql_fetch_assoc($result)) {
                            if ($i == 0) {
                                ?>
                                <div class="main-nav-image-admin-inner-sub-menu">
                                    <?php
                                }
                                ?>
                                <li>
                                    <div class="squaredOne">
                                        <input name="campaignfilter" type="checkbox" id="<?php echo $campaign['IBC_Name'] ?>_<?php echo $campaign['IBC_ID'] ?>" value="<?php echo $campaign['IBC_ID'] ?>" <?php echo (in_array($campaign['IBC_ID'], $tempCampaign)) ? 'checked' : ''; ?> onchange="search_func();">
                                        <label for="<?php echo $campaign['IBC_Name'] ?>_<?php echo $campaign['IBC_ID'] ?>"></label>
                                    </div>
                                    <span><?php echo $campaign['IBC_Name'] ?></span>
                                </li>
                                <?php
                                $i++;
                                if ($i == 4) {
                                    $i = 0;
                                    ?> 
                                </div>          
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </li>
                <li class="img_order_show_hide">
                    <select name="img_sort" id="img_sort" style="width: 150px; margin-left: 20px;" onchange="search_func();">
                        <option value="DESC" <?php echo ($order == 'DESC') ? 'selected' : ''; ?>>Newest First</option>
                        <option value="ASC" <?php echo ($order == 'ASC') ? 'selected' : ''; ?>>Oldest First</option>
                    </select>
                </li>

                <li class="img_order_show_hide" style="margin-bottom: 10px;margin-right: 82px;float: right;">
                    <select name="img_filter" id="img_filter" style="width: 150px; margin-left: 20px;" onchange="search_func();">
                        <option value="">Select Limit </option>
                        <option value="1" <?php echo ($limit == 1) ? 'selected' : ''; ?>>Less than 500x500 </option>
                        <option value="2" <?php echo ($limit == 2) ? 'selected' : ''; ?>>Between 500x500 & 1500x1500 </option>
                        <option value="3" <?php echo ($limit == 3) ? 'selected' : ''; ?>>Between 1500x1500 & 2500x2500 </option>
                        <option value="4" <?php echo ($limit == 4) ? 'selected' : ''; ?>>Greater than 2500 </option>
                    </select>
                </li>
            <?php } if (basename($_SERVER['PHP_SELF']) != 'image-bank.php') { ?>
                <li class="last-padding-li-go-back-img-bnk">
                    <a href="image-bank.php<?php echo ($pagepathno != '') ? '?' . $pagepathno : ''; ?>">Go back to image Bank</a>
                </li>
            <?php } ?>
        </ul>
    </div>
    <div style="display:none;top: 260px;" id="floater" onclick="return Delete_Images()">DELETE ALL (<span id="total"></span>)</div>
</div>

<script>
    var ids = 0;    // Initializing ids with zero in the start
    var counter = 0;  // counter for selected images
    var url_current = '<?php echo basename($_SERVER['PHP_SELF']) ?>';
    if (url_current != 'image-bank-photo-detail.php') {
        var ids_after_realod = '<?php echo $_REQUEST['ids'] ?>'; // getting the selected ids if exists
    }


    if (ids_after_realod != 'undefined' && ids_after_realod != '' && ids_after_realod != null) {
        ids = ids_after_realod;
        var idsArray = ids.split(",");                          //split ids on the basis of ,
        counter = idsArray.length;                              //get and assign ids' length to counter,
        maintain_selection();                                   //retain the previous selection 
        document.getElementById("total").innerHTML = counter;   // assign the counter to the span with id = total 
    }

    // This will allow the DELETE button to float on left along with scrolling of the current page
    $(window).scroll(function () {
        var winScrollTop = $(window).scrollTop();
        var winHeight = $(window).height();
        var floaterHeight = $('#floater').outerHeight(true);
        //true so the function takes margins into account
        var fromBottom = 320;
        var top = winScrollTop + winHeight - floaterHeight - fromBottom;
        $('#floater').css({'top': top + 'px'});
    });

    // function to remove id, when the user click the image again to unselect it
    function remove(id) {
        $("#" + id).remove();
        // removing the removed value from selected ids array
        var temp = 0;
        var idsArray = ids.split(",");
        for (var i = 0; i < idsArray.length; i++) {
            if (id != idsArray[i]) {
                if (temp == 0) {
                    temp = idsArray[i];
                } else {
                    temp += "," + idsArray[i];
                }
            }
        }
        // if ids is empty then put empty instead of 0
        if (temp == 0) {
            ids = '';
        } else {
            ids = temp;
        }
    }

    var image_name;
    // function for image selection
    function select_the_image(imageName, id) {
        image_name = imageName;
        $("#floater").css("display", "block");      // this will show the floater DELETE button on the left of the page when user will select any image
        if (image_name != '' && id != '') {           // check to confirm that there exist some id
            var check = 0;
            if (ids == 0) {                           // this will work when user will click the first image
                ids = id;
                check = 1;
            } else {                                  // for more than 1 images this check will work with the relevant conditions
                var idsArray = ids.split(",");      // splitting the existing ids on the base of ,
                var check_id = 1;
                for (var i = 0; i < idsArray.length; i++) {   // loop to confirm whether the id is already selected or not, if it does then it will remove that id, as clicking again the image removed the selection
                    if (id == idsArray[i]) {                  // checking if id exists in the selected ids
                        if (idsArray.length == 1) {           // if length of ids is 1 and the image is selected again that means floater DELETE button shouldn't be show
                            $("#floater").css("display", "none");
                        }
                        $('#bgcolor_' + id).css({'background-color': '#e8eeee'});   // changing the background color to default, as the image is not more selected
                        remove(id);                                                 // removig the clicked image's id
                        counter--;                                                  // decrementing the image counter
                        document.getElementById("total").innerHTML = counter;
                        check_id = 0;
                        break;
                    }
                }
                if (check_id == 1) {      // Check for adding new image id in the ids list
                    ids += "," + id;
                    check = 1;
                }
            }
            if (check == 1) {             // Final check to add the image id in the ids list and change the background to show selection
                counter++;
                $('#bgcolor_' + id).css({'background-color': '#ebdabe'});
                document.getElementById("total").innerHTML = counter;
            }
        }
        return ids;
    }

    // function to retain ids in the url of A tags in pagination when the user will go to next or previous page
    function retain_ids() {
        if (ids != 0) {
            $("a.page").each(function () {
                var $this = $(this);
                var _href = $this.attr("href");     // getting the page url of class page
                if (_href.indexOf("&") > 0 && _href.indexOf("region") < 0) {      // check to replace the recent ids with the previously selected ids
                    var output = _href.substring(0, _href.lastIndexOf('&'));
                } else {
                    var output = _href;
                }
                var rids = '&ids=' + ids;
                var rplc = output + rids;
                $this.attr("href", rplc);
            });
        }
    }

    // this function will maintain the background colors of selected images
    function maintain_selection() {
        if (ids != 'undefined' && ids != '') {
            $("#floater").css("display", "block");
            var idsArray = ids.split(",");
            for (var i = 0; i < idsArray.length; i++) {
                $('#bgcolor_' + idsArray[i]).css({'background-color': '#ebdabe'});
            }
        }
    }

    // funtion to delete the selcted images, it will be called when user will click on the DELETE ALL button
    function Delete_Images() {
        // In case if there is filter selected then this will maintain the filters
        var redirect, redirect1;
        var url = window.location.href;
        var _href = $("a.page").attr("href");     // getting the page url of class page
        if (_href.indexOf("&page") > 0 && _href.indexOf("region") > 0 && url.indexOf("region") < 0) {      // check to replace the recent ids with the previously selected ids
            redirect = _href.substring(0, _href.lastIndexOf('&page'));
        }

        if (url.indexOf("&ids") > 0) {      // check to replace the recent ids with the previously selected ids
            redirect1 = url.substring(0, url.lastIndexOf('&ids'));
        }

//        alert(redirect1);
        var ids_all = ids;
        if (confirm('Are you sure you want to delete this image?')) {
            $.post('image-bank-multiple-photo-delete.php', {
                ids: ids_all
            }, function (done) {
                if (redirect != '' && redirect != null && redirect != undefined) {
                    window.location.replace("image-bank.php" + redirect);
                } else if (redirect1 != '' && redirect1 != null && redirect1 != undefined) {
                    window.location.replace(redirect1);
                } else {
                    location.reload();
                }
            });
        }
        return false;
    }

    function search_func() {
        var pathname = window.location.pathname;
        var keywords = $('#keywords-searched').val();
        if (pathname == '/admin/image-bank-photo-detail.php') {
            window.location.href = 'http://touristtowndemo.com/admin/image-bank.php?search_image=' + keywords;
            return false;
        }
        var region = new Array();
        var season = new Array();
        var owner = new Array();
        var cat = new Array();
        var people = new Array();
        var campaign = new Array();
        var img_sort;
        var img_filter;
        $("input[name=regionfilter]:checked").each(function () {
            region.push($(this).val());
        });
        $("input[name=seasonfilter]:checked").each(function () {
            season.push($(this).val());
        });
        $("input[name=catfilter]:checked").each(function () {
            cat.push($(this).val());
        });
        $("input[name=peoplefilter]:checked").each(function () {
            people.push($(this).val());
        });
        $("input[name=ownerfilter]:checked").each(function () {
            owner.push($(this).val());
        });
        $("input[name=campaignfilter]:checked").each(function () {
            campaign.push($(this).val());
        });
        var img_sort = $('#img_sort option:selected').val();
        var img_filter = $('#img_filter option:selected').val();
        $.ajax({
            url: "image-bank-ajax.php",
            type: "GET",
            data: {
                region: region.join(','),
                season: season.join(','),
                cat: cat.join(','),
                people: people.join(','),
                owner: owner.join(','),
                campaign: campaign.join(','),
                img_sort: img_sort,
                img_filter: img_filter,
                search_image: keywords
            },
            success: function (html) {
                history.pushState({}, null, 'http://touristtowndemo.com/admin/image-bank.php');
                var response = html.split("@");
                $(".image-bank-container").empty();
                $(".image-bank-container").html(response[0]);
                $(".main-nav-image-search-total-image").text("");
                $(".main-nav-image-search-total-image").text((response[1] > 1) ? response[1] + " images" : "");
                $("a.page").each(function () {
                    var $this = $(this);
                    $this.attr('onclick', 'retain_ids();');
                });
            }
        });
        return false;
    }
</script>
