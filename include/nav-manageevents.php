<?php
$filename = basename($_SERVER["SCRIPT_FILENAME"], '.php');
if ($filename == 'events') {
    $action = 'events';
} else if ($filename == 'events-pending') {
    $action = 'events-pending';
} else if ($filename == 'events-delete') {
    $action = 'events-delete';
} else {
    $action = 'events';
}
?>
<div class="sub-menu">
    <ul>
        <?php if (in_array('superadmin', $_SESSION['USER_ROLES'])) { ?>
            <li><a href="/admin/events-locations.php">Manage Locations</a></li>
            <li><a href="/admin/events-organizations.php">Manage Organizations</a></li>
        <?PHP } if (in_array('manage-events', $_SESSION['USER_PERMISSIONS'])) { ?>
            <li><a href="/admin/events.php">Active Events</a></li>
            <li><a href="/admin/events-pending.php">Pending Events</a></li>
            <li><a href="/admin/events-delete.php">Expired Events</a></li>
            <li><a href="/admin/event.php">+Add Events</a></li>
            <form id="form1" name="form1" method="GET" action="/admin/<?php echo $action ?>.php">
                <li class="search-field">

                    <input name="strSearch" placeholder="search event" type="text" id="strSearch" size="20" />
                </li>
                <li>
                    <input type="submit" name="button" id="button" value="Search" />
                </li>
            </form>
        <?PHP } ?>
    </ul>
</div>


