<?php
$pagename = basename($_SERVER['PHP_SELF']);
if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $sql = "SELECT * FROM tbl_Business_Billing 
			LEFT JOIN tbl_Business ON BB_B_ID = B_ID 
			LEFT JOIN tbl_Payment_Status ON BB_Payment_Status = PS_ID 
			LEFT JOIN tbl_Payment_Type ON BB_Payment_Type = PT_ID 
			LEFT JOIN tbl_User ON BB_Account_Manager = U_ID
			LEFT JOIN tbl_Category ON BB_Category = C_ID
			WHERE BB_ID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db)
            or die("Invalid query: $sql -- " . mysql_error());
    $rowBus = mysql_fetch_assoc($result);
    $region = $rowBus['BB_R_ID'];
    $sql_region = "SELECT * FROM tbl_Region WHERE R_ID = $region LIMIT 1";
    $res_region = mysql_query($sql_region);
    $rowreg = mysql_fetch_assoc($res_region);
}
?>
<div class="header">
    <div class="header-inner">
        <div class="logo">
            <a href="/admin/mobile/admin-mobile-menu.php"><img src="/images/TouristTown-Logo.gif" alt="Tourist Town" width="150" height="35" hspace="20" vspace="20" border="0" />
            </a>
        </div>
        <?php if ($_SESSION['USER_ID'] && $pagename != 'admin-mobile-menu.php') { ?>
            <?php
            if ((isset($_REQUEST['bl_id']) && $_REQUEST['bl_id'] != '') || (isset($_REQUEST['id']) && $_REQUEST['id'] != '')) {
                if ($_REQUEST['bl_id'] != '') {
                    $BL_ID = $_REQUEST['bl_id'];
                }
                if ($pagename == 'customer-listing-mobile-menu.php' || $pagename == 'login-details.php') {
                    ?>
                    <div class="back-home-button">
                        <a href="/admin/mobile/admin-mobile-menu.php">BACK HOME</a>
                    </div>
                <?php } else {
                    ?>
                    <div class="back-home-button">
                        <a href="/admin/mobile/customer-listing-mobile-menu.php?bl_id=<?= $BL_ID ?>">BACK HOME</a>
                    </div>
                    <?php
                }
            } else {
                ?>
                <div class="back-home-button">
                    <a href="/admin/mobile/admin-mobile-menu.php">BACK HOME</a>
                </div>
                <?php
            }
        } else if ($_SESSION['USER_ID']) {
            ?>
            <div class="back-home-button">
                <a href="/admin/mobile/?logop=logout">Logout</a>
            </div>
        <?php }
        ?>
    </div>
</div>

<div class="main-nav-admin">
    <div class="main-nav-admin-inner">
        <ul>
            <?PHP
            if ($_SESSION['USER_ID']) {
                ?>
                <li><a href="/admin/">Home</a></li>
                <li><a href="#">Listings</a>
                    <ul class="first">
                        <?PHP
                        // }
                        if (in_array('categories', $_SESSION['USER_PERMISSIONS'])) {
                            ?>
                            <li><a href="/admin/category.php">Categories</a></li>
                        <?PHP } if (in_array('customers', $_SESSION['USER_PERMISSIONS']) || in_array('free-listings', $_SESSION['USER_PERMISSIONS']) || in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) { ?>
                            <li><a href="/admin/listings.php">Listings</a></li>
                        <?PHP } if (in_array('ammenity-icons', $_SESSION['USER_PERMISSIONS'])) { ?>
                            <li><a href="/admin/ammenity-icons.php">Amenity Icons</a></li>
                        <?PHP } if (in_array('manage-cart-items', $_SESSION['USER_PERMISSIONS'])) { ?>
                            <li><a href="/admin/store-list.php">Store</a></li>
                        <?PHP } if (in_array('manage-listing-points', $_SESSION['USER_PERMISSIONS'])) { ?>
                            <li><a href="/admin/points_list.php?pc_id=1">Listing Points</a></li>
                        <?php } ?>    
                    </ul>
                </li>
                <?PHP if (in_array('superadmin', $_SESSION['USER_ROLES'])) { ?>
                    <li><a href="#">Images</a>
                        <ul class="first">
                            <li><a href="/admin/image-bank.php">Image Bank</a>
                                <ul class="last">
                                    <li><a href="/admin/pending-image-bank.php">Pending Images</a></li>
                                    <li><a href="/admin/owner.php">Manage Owner</a></li>
                                    <li><a href="/admin/photographer.php">Manage Photographer</a></li>
                                </ul></li>
                            <li><a href="/admin/active-advertisement-stats.php">Advertisement</a></li>
                        </ul>
                    </li>
                <?PHP } ?>
                <?PHP if (in_array('superadmin', $_SESSION['USER_ROLES'])) { ?>
                    <li><a href="#">Support</a>
                        <ul class="first">
                            <li><a href="/admin/help_text.php?">Help Text</a></li>
                            <li><a href="/admin/support-list.php">Support</a></li>
                        </ul>
                    </li>
                <?php } ?>
                <?PHP //if (!in_array('town', $_SESSION['USER_ROLES'])) {   ?>
                <li><a href="#">Admin</a>
                    <ul class="first">
                        <?PHP if (in_array('manage-users', $_SESSION['USER_PERMISSIONS']) && in_array('superadmin', $_SESSION['USER_ROLES'])) { ?>
                            <li><a href="/admin/users.php">Users</a></li>
                        <?PHP } if (in_array('regions', $_SESSION['USER_PERMISSIONS'])) { ?>
                            <li><a href="/admin/domains.php">Domains</a></li>   
                        <?PHP }if (in_array('regions', $_SESSION['USER_PERMISSIONS'])) { ?>
                            <li><a href="/admin/regions.php">Regions</a></li>
                        <?PHP } if (in_array('manage-events', $_SESSION['USER_PERMISSIONS'])) { ?>
                            <li><a href="/admin/events.php">Events</a></li>
                        <?php } ?>   
                        <?PHP /* if (in_array('regions', $_SESSION['USER_PERMISSIONS']) || in_array('superadmin', $_SESSION['USER_ROLES'])) { ?>
                          <li ><a href="/admin/product_websites.php">Product Websites</a></li>
                          <?php } */ ?>
                        <?PHP if (in_array('manage-accounting', $_SESSION['USER_PERMISSIONS'])) { ?>
                            <li><a href="/admin/accounting.php">Accounting</a></li>
                        <?php } ?>
                        <li><a href="/admin/group_blocks.php">Accommodation</a></li>    
                        <?php if (in_array('manage-users', $_SESSION['USER_PERMISSIONS']) && in_array('superadmin', $_SESSION['USER_ROLES'])) { ?>
                            <li><a href="/admin/email-templates.php">Email Templates</a></li>   
                        <?php } ?>
                        <li ><a href="/admin/mobile_layout.php">Mobile</a></li>   
                    </ul>
                </li>
                <?php //} ?>
                <?PHP
            } else {
                echo "&nbsp;";
            }
            ?>
        </ul>
    </div>
</div>