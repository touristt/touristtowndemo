<?php
if (isset($_SESSION['success']) && $_SESSION['success'] == 1) {
    print '<script>swal("Data Saved", "Data has been saved successfully.", "success");</script>';
    unset($_SESSION['success']);
}
if (isset($_SESSION['enhanced_success']) && $_SESSION['enhanced_success'] == 1) {
    print '<script>swal("Data Saved", "Data has been saved successfully. Please update billing information.", "success");</script>';
    unset($_SESSION['enhanced_success']);
}
if (isset($_SESSION['error']) && $_SESSION['error'] == 1) {
    print '<script>swal("Error", "Error saving data. Please try again.", "error");</script>';
    unset($_SESSION['error']);
}
if (isset($_SESSION['delete']) && $_SESSION['delete'] == 1) {
    print '<script>swal("Data Deleted", "Data has been deleted successfully.", "success");</script>';
    unset($_SESSION['delete']);
}
if (isset($_SESSION['delete_error']) && $_SESSION['delete_error'] == 1) {
    print '<script>swal("Error", "Error deleting data. Please try again.", "error");</script>';
    unset($_SESSION['delete_error']);
}
if ($_SESSION['WARNING_LOGIN_DETAILS'] == 1) {
    print '<script>sweetAlert("Email already exists.", "Please use another email address for this account!", "error");</script>';
    unset($_SESSION['WARNING_LOGIN_DETAILS']);
}
if ($_SESSION['transaction_processing_error'] == 1) {
    print '<script>swal("Error", "We were unable to process your credit card. Please carefully review your credit card information and try again.", "error");</script>';
    unset($_SESSION['transaction_processing_error']);
}
?>

</div>
</div>
</div>
<div class="custom-ajax">Loading...</div>
</body>
</html>
<?php
mysql_close($db);
?>