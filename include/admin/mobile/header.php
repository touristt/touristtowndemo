<?php
define("DOMAIN", 'touristtowndemo.com');
if ($title == '') {
    $title = COMPANY_NAME . " Admin";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <title><?PHP echo $title; ?></title>
        <link href="css/admin-responsive.css" rel="stylesheet" type="text/css" />
        <link href="/include/separate.css"  rel="stylesheet" type="text/css"/>  
        <link href="/include/acc_cap.css"   rel="stylesheet" type="text/css"/>      
        <link href="/include/accordion.css" rel="stylesheet" type="text/css"/> 
        <link href="/include/popup.css" rel="stylesheet" type="text/css"/> 
        <link href="/include/jquery-ui.css" rel="stylesheet" type="text/css" />
        <link href="/include/colpick.css" rel="stylesheet" type="text/css" />
        <link href="/include/plugins/sweetalert/sweetalert.css"  rel="stylesheet" type="text/css"/> 
        <link href="/include/token-input.css"  rel="stylesheet" type="text/css"/> 
        
        <script src="/include/jquery.min.js" type="text/javascript"></script>
        <script src="/include/NoSleep.min.js" type="text/javascript"></script>
        <script src="/include/jquery-ui.min.js" type="text/javascript"></script>
        <script src="/include/jquery.tablesorter.min.js" type="text/javascript"></script>
        <script src="/include/jquery.metadata.js" type="text/javascript"></script>
        <script src="/include/jquery-1.10.2.js" type="text/javascript"></script>
        <script src="/include/jquery-ui.js" type="text/javascript"></script>
        <script src="/include/colpick.js" type="text/javascript"></script>
        <script src="/include/ckeditor/jquery.cropit.js" type="text/javascript"></script>
        <script src="/include/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
        <script type="text/javascript" src="/include/plugins/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="/include/plugins/sweetalert/sweetalert.min.js"></script>
        <script src="/include/ckeditor/ckeditor.js"></script>
        <script src="/include/ckeditor/jquery.js"></script>
        <script src="/include/jquery.tokeninput.js"></script>
        <script src="/include/jquery.form.min.js"></script>
        <script src="/include/jquery.maskedinput.min.js"></script>

        <script src="/include/js-webshim/minified/polyfiller.js"></script>
        <script> 
            webshim.activeLang("en");
            webshims.polyfill("forms");
            webshims.cfg.no$Switch = true;
        </script>

        <script src="/include/admin.js" type="text/javascript"></script>
        <?PHP
        $sql_responsive_bg = "SELECT * from tbl_Mobile_Layout";
        $result_responsive_bg = mysql_query($sql_responsive_bg, $db) or die("Invalid query: $sql_responsive_bg -- " . mysql_error());
        $row_responsive_bg = mysql_fetch_assoc($result_responsive_bg);
        ?>
        <style>
            .login-outer-body
            {
                background: url('http://<?= DOMAIN ?>/images/DB/<?php echo $row_responsive_bg['ML_Photo_Name']; ?>') no-repeat;
                background-size: 100% 100%;
            }
        </style>
        <?PHP
        if (count($js) > 0) {
            foreach ($js as $val) {
                echo "<script type=\"text/javascript\" src=\"/include/" . $val . "\"></script>\n";
            }
        }
        ?>
        <body onload="<?PHP echo $onload; ?>">
            <meta name="viewport" content="width=device-width, initial-scale=1">
                <div class="main-wrapper">
                    <?php require_once('../../include/topnav-mobile.php'); ?>
                    <?PHP
                    if ($mainNav) {
                        ?>
                        <div class="heading-wrapper"><?php require_once($mainNav); ?></div>
                        <?PHP
                    }
                    ?>
                    <div class="content-wrapper">
                        <div class="content">

                            <?PHP
                            if (isset($_GET['info']) && $_GET['info'] <> '') {
                                echo "<p class='info'>" . stripslashes(urldecode($_GET['info'])) . "</p>";
                            }
                            if (isset($_GET['err']) && $_GET['err'] <> '') {
                                echo "<p class='error'>" . stripslashes(urldecode($_GET['err'])) . "</p>";
                            }
                            if (isset($_SESSION['info']) && $_SESSION['info'] <> '') {
                                echo "<p class='info'>" . stripslashes(urldecode($_SESSION['info'])) . "</p>";
                                $_SESSION['info'] = "";
                            }
                            if (isset($_SESSION['ERROR']) && $_SESSION['ERROR'] <> '') {
                                echo "<p class='error'>" . stripslashes(urldecode($_SESSION['ERROR'])) . "</p>";
                                $_SESSION['ERROR'] = "";
                            }