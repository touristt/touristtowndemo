<?php
//to show message on ad payments page
if ((isset($_SESSION['success']) && $_SESSION['success'] == 1) && isset($_SESSION['discount_is_greater']) && $_SESSION['discount_is_greater'] == 1) {
    print '<script>swal("Data Saved", "Data has been saved successfully. Discounts of some ads are greater than the ad payment. Please update discounts of those ads to update their total payments.", "warning");</script>';
    unset($_SESSION['success']);
    unset($_SESSION['discount_is_greater']);
}
if ((isset($_SESSION['success']) && $_SESSION['success'] == 1) && isset($_SESSION['discount_updated']) && $_SESSION['discount_updated'] == 1) {
    print '<script>swal("Data Saved", "Data has been saved successfully. Discount value has been updated to 0 because of change of Billing Type. Please re-adjust discount.", "success");</script>';
    unset($_SESSION['success']);
    unset($_SESSION['discount_updated']);
}
if (isset($_SESSION['success']) && $_SESSION['success'] == 1) {
    print '<script>swal("Data Saved", "Data has been saved successfully.", "success");</script>';
    unset($_SESSION['success']);
}
if (isset($_SESSION['enhanced_success']) && $_SESSION['enhanced_success'] == 1) {
    print '<script>swal("Data Saved", "Data has been saved successfully. Please update billing information.", "success");</script>';
    unset($_SESSION['enhanced_success']);
}
if (isset($_SESSION['error']) && $_SESSION['error'] == 1) {
    print '<script>swal("Error", "Error saving data. Please try again.", "error");</script>';
    unset($_SESSION['error']);
}
if ($_SESSION['error'] == 2) {
    print '<script>swal("Error", "Discount must be less than Sub-Total 1!", "error");</script>';
    unset($_SESSION['error']);
}
if (isset($_SESSION['error_cat_exist']) && $_SESSION['error_cat_exist'] == 1) {
    print '<script>swal("Error", "Error saving data. SEO Name already exist.", "error");</script>';
    unset($_SESSION['error_cat_exist']);
}
if (isset($_SESSION['delete']) && $_SESSION['delete'] == 1) {
    print '<script>swal("Data Deleted", "Data has been deleted successfully.", "success");</script>';
    unset($_SESSION['delete']);
}
if (isset($_SESSION['delete_error']) && $_SESSION['delete_error'] == 1) {
    print '<script>swal("Error", "Error deleting data. Please try again.", "error");</script>';
    unset($_SESSION['delete_error']);
}
if(isset($_SESSION['email_existence_error']) && $_SESSION['email_existence_error'] == 1){
    print '<script>swal("Error", "Email does not exists.", "error");</script>';
    unset($_SESSION['email_existence_error']);
}
if(isset($_SESSION['email_not_correct']) && $_SESSION['email_not_correct'] == 1){
    print '<script>swal("Error", "Email address not correct.", "error");</script>';
    unset($_SESSION['email_not_correct']);
}
if (isset($_SESSION['email_already_exist']) && $_SESSION['email_already_exist'] == 1) {
    print '<script>swal("Error", "Email already exist. Please try another Email.", "error");</script>';
    unset($_SESSION['email_already_exist']);
}
if (isset($_SESSION['WARNING']) && $_SESSION['WARNING'] == 1) {
    print '<script>swal("PDF!", "File Required!", "warning");</script>';
    unset($_SESSION['WARNING']);
}
if ($_SESSION['WARNING_LOGIN_DETAILS'] == 1) {
    print '<script>sweetAlert("Email already exists.", "Please use another email address for this account!", "error");</script>';
    unset($_SESSION['WARNING_LOGIN_DETAILS']);
}
if (isset($_SESSION['WARNING_update']) && $_SESSION['WARNING_update'] == 1) {
    print '<script>swal("PDF!", "File Required For Update!", "warning");</script>';
    unset($_SESSION['WARNING_update']);
}
if ($_SESSION['transaction_success'] == 1) {
    print '<script>swal("Thank You", "Your payment has been received.", "success");</script>';
    unset($_SESSION['transaction_success']);
}
if ($_SESSION['transaction_success'] == 2) {
    print '<script>swal("Thank You", "Payment has been refunded.", "success");</script>';
    unset($_SESSION['transaction_success']);
}
if ($_SESSION['transaction_success_ad'] == 1) {
    print '<script>swal("Thank You", "Your transaction has been completed.", "success");</script>';
    unset($_SESSION['transaction_success_ad']);
}
if ($_SESSION['transaction_error'] == 1) {
    print '<script>swal("Transaction Error", "An error has been occur due to: ' . $_SESSION['transaction_msg'] . '.", "error");</script>';
    unset($_SESSION['transaction_error']);
    unset($_SESSION['transaction_msg']);
}
if (isset($_SESSION['story_success']) && $_SESSION['story_success'] == 1) {
    print '<script>swal("Story Added", "Story has been added successfully.", "success");</script>';
    unset($_SESSION['story_success']);
}
if (isset($_SESSION['story_delete']) && $_SESSION['story_delete'] == 1) {
    print '<script>swal("Story Deleted", "Story has been deleted successfully.", "success");</script>';
    unset($_SESSION['story_delete']);
}
if (isset($_SESSION['listing_success']) && $_SESSION['listing_success'] == 1) {
    print '<script>swal("Listing Added", "Listing has been added successfully.", "success");</script>';
    unset($_SESSION['listing_success']);
}
if (isset($_SESSION['listing_delete']) && $_SESSION['listing_delete'] == 1) {
    print '<script>swal("Listing Deleted", "Listing has been deleted successfully.", "success");</script>';
    unset($_SESSION['listing_delete']);
}

if (isset($_SESSION['event_success']) && $_SESSION['event_success'] == 1) {
    print '<script>swal("Event Added", "Event has been added successfully.", "success");;</script>';
    unset($_SESSION['event_success']);
}
if (isset($_SESSION['event_delete']) && $_SESSION['event_delete'] == 1) {
    print '<script>swal("Event Deleted", "Event has been deleted successfully.", "success");;</script>';
    unset($_SESSION['event_delete']);
}

if ($_SESSION['buy_ad_success'] == 1) {
    print '<script>swal("Data Saved", "The Tourist Town team will now create your advertisement. Once your advertisement is created you will receive an email to : ' . $emailBUS . '", "success");</script>';
    unset($_SESSION['buy_ad_success']);
}
if ($_SESSION['advertisement_created'] == 1) {
    print '<script>swal("Thank You", "Your advertisement has been created successfully.", "success");</script>';
    unset($_SESSION['advertisement_created']);
}
if ($_SESSION['request_sent'] == 1) {
    print '<script>swal("", "Your request has been sent.", "success");</script>';
    unset($_SESSION['request_sent']);
}
if ($_SESSION['transaction_processing_error'] == 1) {
    print '<script>swal("Error", "We were unable to process your credit card. Please carefully review your credit card information and try again.", "error");</script>';
    unset($_SESSION['transaction_processing_error']);
}
if ($_SESSION['provide_card_info'] == 1) {
    print '<script>swal("Credit Card Info", "Please provide credit card info before buying Advertisement!", "warning");</script>';
    unset($_SESSION['provide_card_info']);
}
if (isset($_SESSION['addon']) && ($_SESSION['addon'] == 'deleted1' || $_SESSION['addon'] == 'deleted0')) {
    print '<script>swal("", "Your selected Add on has been ' . (($_SESSION['addon'] == 'deleted1') ? "activated" : "deactivated") . ' successfully.", "success");</script>';
    unset($_SESSION['addon']);
}
if ($_SESSION['note_warning'] == 1) {
    print '<script>sweetAlert("note_warning", "Note not long enough!", "error");</script>';
    unset($_SESSION['note_warning']);
}
if ($_SESSION['email_sent'] == 1) {
    print '<script>swal("Email Sent", "Email has been sent successfully.", "success");</script>';
    unset($_SESSION['email_sent']);
}
if ($_SESSION['email_sent_error'] == 1) {
    print '<script>swal("Email Sent Error", "Error while sending email.", "error");</script>';
    unset($_SESSION['email_sent_error']);
}
if ($_SESSION['email_saved_as_draft'] == 1) {
    print '<script>swal("Email Saved As Draft", "Email has been savaed as draft successfully.", "success");</script>';
    unset($_SESSION['email_saved_as_draft']);
}
if (isset($_SESSION['listing_disabled_success']) && $_SESSION['listing_disabled_success'] == 1) {
    print '<script>swal("Data Saved", "Listing has been saved successfully.", "success");</script>';
    unset($_SESSION['listing_disabled_success']);
}
if (isset($_SESSION['listing_disabled_delete']) && $_SESSION['listing_disabled_delete'] == 1) {
    print '<script>swal("Data Deleted", "Data has been deleted successfully.", "success");</script>';
    unset($_SESSION['listing_disabled_delete']);
}
//user.php
if ($_SESSION['email_username_error'] == 1) {
    echo '<script>sweetAlert("Email and Username already exists.", "Please use another email address and username to create account!", "error");</script>';
    unset($_SESSION['email_username_error']);
}
if ($_SESSION['username_error'] == 1) {
    echo '<script>sweetAlert("Username already exists.", "Please use another username to create account!", "error");</script>';
    unset($_SESSION['username_error']);
}
if ($_SESSION['email_error'] == 1) {
    echo '<script>sweetAlert("Email already exists.", "Please use another email address to create account!", "error");</script>';
    unset($_SESSION['email_error']);
}
if ($_SESSION['result_not_found'] == 1) {
    echo '<script>sweetAlert("Data error.", "Result not found.", "error");</script>';
    unset($_SESSION['result_not_found']);
}
if ($_SESSION['email_not_found'] == 1) {
    echo '<script>sweetAlert("Data Error", "Email Not Found.", "error");</script>';
    unset($_SESSION['email_not_found']);
}
if (isset($_SESSION['advert_discount_error']) && $_SESSION['advert_discount_error'] > 0) {
    print '<script>swal("Error", "Discount must be less than advertisement cost!", "error");</script>';
    unset($_SESSION['advert_discount_error']);
}
if (isset($_SESSION['pdferror']) && $_SESSION['pdferror'] == 1) {
    echo '<script>alert("Only pdf formatted files can be uploaded")</script>';
    unset($_SESSION['pdferror']);
}
if (isset($_SESSION['cc_delete']) && $_SESSION['cc_delete'] == 1) {
    echo '<script>swal("Card Deleted", "Credit Card has been deleted.", "success");</script>';
    unset($_SESSION['cc_delete']);
}
if (isset($_SESSION['cc_delete_error']) && $_SESSION['cc_delete_error'] == 1) {
    echo '<script>swal("Delete Error", "Error deleting Credit Card.", "error");</script>';
    unset($_SESSION['cc_delete_error']);
}
if (isset($_SESSION['update_photo']) && $_SESSION['update_photo'] == 1) {
    echo '<script>swal("Success", "Data has been saved successfully.", "success");</script>';
    unset($_SESSION['update_photo']);
}
//Listing trasnferred messages... customer-listings.php
if (isset($_SESSION['listing_transferred_success']) && $_SESSION['listing_transferred_success'] == 1) {
    print '<script>swal("Listing Transferred", "Listing has been transferred to the selected account successfully.", "success");</script>';
    unset($_SESSION['listing_transferred_success']);
}
if (isset($_SESSION['listing_transferred_error']) && $_SESSION['listing_transferred_error'] == 1) {
    print '<script>swal("Error", "Account does not exist. Please try another email address.", "error");</script>';
    unset($_SESSION['listing_transferred_error']);
}
?>

</div>
</div>
</div>
<!-- <div class="custom-ajax">Loading...</div> -->
<!-- <div class="custom-ajax"><img width="160" height="160" src="../images/ajaxloader1.gif"></div> -->
<div class="ajaxSpinnerImage"><img width="160" height="160" src="../images/ajaxloader1.gif"></div>
</body>
</html>
<?php
mysql_close($db);
?>