<div class="sub-menu border-left-orgn">
    <ul>
        <?php
        //Temporary hardcoding to hide Recommendations
        $sql = "SELECT PC_Name, PC_Points, PC_File_Path FROM tbl_Points_Category WHERE PC_ID != 14";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($rowCategories = mysql_fetch_array($result)) {
            //Contact Details
            if ($rowCategories['PC_Name'] == 'Contact Details') {
                if ($rowListing['BL_Listing_Title'] != '' && $rowListing['BL_Contact'] != '' && $rowListing['BL_Phone'] != '' && $rowListing['BL_Website'] != '' && $rowListing['BL_Toll_Free'] != '' && $rowListing['BL_Email'] != '') {
                    $class = 'points-com';
                } else {
                    $class = 'points-uncom';
                }
                $total_cd_points = $rowCategories['PC_Points'];
            }
            //Mapping
            if ($rowCategories['PC_Name'] == 'Location & Mapping') {
                if ($rowListing['BL_Street'] != '' && $rowListing['BL_Town'] != '' && $rowListing['BL_Province'] != '' && $rowListing['BL_PostalCode'] != '' && $rowListing['BL_Lat'] != '' && $rowListing['BL_Long'] != '') {
                    $class = 'points-com';
                } else {
                    $class = 'points-uncom';
                }
                $total_mapping_points = $rowCategories['PC_Points'];
            }
            //Trip Advisor
            if ($rowCategories['PC_Name'] == 'Trip Advisor') {
                if ($rowListing['BL_Trip_Advisor'] != '') {
                    $class = 'points-com';
                } else {
                    $class = 'points-uncom';
                }
                $total_trip_points = $rowCategories['PC_Points'];
            }
            //Description Text
            if ($rowCategories['PC_Name'] == 'Description Text') {
                if ($rowListing['BL_Description'] != '') {
                    $class = 'points-com';
                } else {
                    $class = 'points-uncom';
                }
                $total_desc_points = $rowCategories['PC_Points'];
            }
            //Main Photos
            if ($rowCategories['PC_Name'] == 'Main Photos') {
                if ($rowListing['BL_Header_Image'] != '' && $rowListing['BL_Photo'] != '') {
                    $class = 'points-com';
                } else {
                    $class = 'points-uncom';
                }
                $total_mp_points = $rowCategories['PC_Points'];
            }
            //Search Engine
            if ($rowCategories['PC_Name'] == 'Search Engine') {
                if ($rowListing['BL_SEO_Title'] != '' && $rowListing['BL_SEO_Description'] != '' && $rowListing['BL_SEO_Keywords'] != '' && $rowListing['BL_Search_Words'] != '') {
                    $class = 'points-com';
                } else {
                    $class = 'points-uncom';
                }
                $total_seo_points = $rowCategories['PC_Points'];
            }
            //Amenities
            if ($rowCategories['PC_Name'] == 'Amenities') {
                $amm = "SELECT * FROM tbl_Business_Listing_Ammenity WHERE BLA_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                $amm_result = mysql_query($amm, $db) or die("Invalid query: $amm -- " . mysql_error());
                $amm_count = mysql_num_rows($amm_result);
                if ($amm_count > 0) {
                    $class = 'points-com';
                } else {
                    $class = 'points-uncom';
                }
                $total_amenities_points = $rowCategories['PC_Points'];
            }
            //Hours

            if ($rowCategories['PC_Name'] == 'Hours') {
                $sqlHours = "SELECT * FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
                $resultHours = mysql_query($sqlHours) or die(mysql_error());
                $rowsHours = mysql_fetch_array($resultHours);
                if (!$rowsHours['BL_Hours_Disabled'] && ($rowsHours['BL_Hour_Mon_From'] != '00:00:00' || $rowsHours['BL_Hour_Tue_From'] != '00:00:00' || $rowsHours['BL_Hour_Wed_From'] != '00:00:00' || $rowsHours['BL_Hour_Thu_From'] != '00:00:00' || $rowsHours['BL_Hour_Fri_From'] != '00:00:00' || $rowsHours['BL_Hour_Sat_From'] != '00:00:00' || $rowsHours['BL_Hour_Sun_From'] != '00:00:00')) {
                    $class = 'points-com';
                } else {
                    $class = 'points-uncom';
                }
                $total_hours_points = $rowCategories['PC_Points'];
            }
            //Social Media
            if ($rowCategories['PC_Name'] == 'Social Media') {
                $sqlSM = "SELECT * FROM tbl_Business_Social WHERE BS_BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
                $resultSM = mysql_query($sqlSM) or die(mysql_error());
                $countSM = mysql_num_rows($resultSM);
                $rowsSM = mysql_fetch_array($resultSM);
                if ($rowsSM['BS_FB_Link'] != '' || $rowsSM['BS_T_Link'] != '' || $rowsSM['BS_Y_Link'] || $rowsSM['BS_I_Link']) {
                    $class = 'points-com';
                } else {
                    $class = 'points-uncom';
                }
                $total_sm_points = $rowCategories['PC_Points'];
            }
            //Recommendations
            if ($rowCategories['PC_Name'] == 'Recommendations') {
                $recommends = " SELECT BL_ID, BL_Listing_Type, BL_Name_SEO, BL_Listing_Title, R_ID, R_Text FROM tbl_Business_Listing
                                LEFT JOIN tbl_Recommendations ON R_Recommended = BL_ID 
                                WHERE R_Recommends = '" . encode_strings($BL_ID, $db) . "'";
                $recommended = "SELECT BL_ID, BL_Listing_Type, BL_Name_SEO, BL_Listing_Title, R_ID, R_Text FROM tbl_Business_Listing  
                                LEFT JOIN tbl_Recommendations ON R_Recommends = BL_ID 
                                WHERE R_Recommended = '" . encode_strings($BL_ID, $db) . "'";
                $recommends_result = mysql_query($recommends, $db) or die("Invalid query: $recommends -- " . mysql_error());
                $recommends_count = mysql_num_rows($recommends_result);
                $recommended_result = mysql_query($recommended, $db) or die("Invalid query: $recommended -- " . mysql_error());
                $recommended_count = mysql_num_rows($recommended_result);
                if ($recommends_count > 0) {
                    $class = 'points-com';
                } else {
                    $class = 'points-uncom';
                }
                $total_recom_points = $rowCategories['PC_Points'];
            }
            $filename = basename($_SERVER["SCRIPT_FILENAME"], '.php');
            if ($filename == 'customer-listing-contact-details') {
                $activeclass1 = 'active-class';
            } else {
                $activeclass1 = 'not-active';
            }
            if ($filename == 'customer-listing-mapping') {
                $activeclass2 = 'active-class';
            } else {
                $activeclass2 = 'not-active';
            }
            if ($filename == 'customer-listing-category') {
                $activeclass3 = 'active-class';
            } else {
                $activeclass3 = 'not-active';
            }
            if ($filename == 'customer-listing-description-text') {
                $activeclass4 = 'active-class';
            } else {
                $activeclass4 = 'not-active';
            }
            if ($filename == 'customer-listing-main-photo') {
                $activeclass5 = 'active-class';
            } else {
                $activeclass5 = 'not-active';
            }
            if ($filename == 'customer-listing-seo') {
                $activeclass6 = 'active-class';
            } else {
                $activeclass6 = 'not-active';
            }
            if ($filename == 'customer-listing-ammenities') {
                $activeclass7 = 'active-class';
            } else {
                $activeclass7 = 'not-active';
            }
            if ($filename == 'customer-rankings') {
                $activeclass8 = 'active-class';
            } else {
                $activeclass8 = 'not-active';
            }
            if ($filename == 'customer-listing-statistics') {
                $activeclass9 = 'active-class';
            } else {
                $activeclass9 = 'not-active';
            }
            if ($filename == 'customer-listing-recommendations') {
                $activeclass10 = 'active-class';
            } else {
                $activeclass10 = 'not-active';
            }
            if ($filename == 'customer-listing-upload-pdf') {
                $activeclass11 = 'active-class';
            } else {
                $activeclass11 = 'not-active';
            }
            if ($filename == 'customer-listing-trip-advisor') {
                $activeclass12 = 'active-class';
            } else {
                $activeclass12 = 'not-active';
            }
            if ($filename == 'customer-listing-third-party') {
                $activeclass13 = 'active-class';
            } else {
                $activeclass13 = 'not-active';
            }
            if ($filename == 'customer-listing-seasons') {
                $activeclass14 = 'active-class';
            } else {
                $activeclass14 = 'not-active';
            }
            if ($filename == 'customer-hours') {
                $activeclass16 = 'active-class';
            } else {
                $activeclass16 = 'not-active';
            }
            if ($filename == 'customer-social-media') {
                $activeclass17 = 'active-class';
            } else {
                $activeclass17 = 'not-active';
            }
            ?>

            <li class="<?php
            echo $class;
            if ($rowCategories['PC_Name'] == 'Contact Details') {
                echo ' ';
                echo $activeclass1;
            }
            if ($rowCategories['PC_Name'] == 'Location & Mapping') {
                echo ' ';
                echo $activeclass2;
            }
            if ($rowCategories['PC_Name'] == 'Category') {
                echo $activeclass3;
            }
            if ($rowCategories['PC_Name'] == 'Description Text') {
                echo ' ';
                echo $activeclass4;
            }
            if ($rowCategories['PC_Name'] == 'Main Photos') {
                echo ' ';
                echo $activeclass5;
            }
            if ($rowCategories['PC_Name'] == 'Search Engine') {
                echo ' ';
                echo $activeclass6;
            }
            if ($rowCategories['PC_Name'] == 'Amenities') {
                echo ' ';
                echo $activeclass7;
            }
            if ($rowCategories['PC_Name'] == 'Rankings') {
                echo $activeclass8;
            }
            if ($rowCategories['PC_Name'] == 'Statistics') {
                echo $activeclass9;
            }
            if ($rowCategories['PC_Name'] == 'Recommendations') {
                echo ' ';
                echo $activeclass10;
            }
            if ($rowCategories['PC_Name'] == 'Upload a PDF') {
                echo ' ';
                echo $activeclass11;
            }
            if ($rowCategories['PC_Name'] == 'Trip Advisor') {
                echo ' ';
                echo $activeclass12;
            }
            if ($rowCategories['PC_Name'] == 'Third Party') {
                echo ' ';
                echo $activeclass13;
            }
            if ($rowCategories['PC_Name'] == 'Seasons') {
                echo ' ';
                echo $activeclass14;
            }
            if ($rowCategories['PC_Name'] == 'Hours') {
                echo ' ';
                echo $activeclass16;
            }
            if ($rowCategories['PC_Name'] == 'Social Media') {
                echo ' ';
                echo $activeclass17;
            }
            ?>">
                    <?php
                    if ($rowCategories['PC_Name'] == 'Statistics') {
                        
                    } else {
                        if ($rowCategories['PC_Name'] == 'Main Photos') {
                            ?>
                        <a href="/<?php echo $rowCategories['PC_File_Path'] ?>?bl_id=<?php echo $BL_ID ?>"><?php echo $rowCategories['PC_Name'] . ' / Video' ?></a></li>
                    <?php
                } else {
                    ?>  
                    <a href="/<?php echo $rowCategories['PC_File_Path'] ?>?bl_id=<?php echo $BL_ID ?>"><?php echo $rowCategories['PC_Name'] ?></a></li>
                    <?php
                    $class = '';
                }
            }
        }
        ?>
    </ul>
</div>
