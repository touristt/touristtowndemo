<?PHP

// function to update the points
function update_pointsin_business_tbl($new_listing) {
    $pointsUpdatedDate = date("Y-m-d H:i:s");
    //select all points from points table for My Page
    $pointsquery = "SELECT name, points FROM points";
    $pointsresult = mysql_query($pointsquery) or die(mysql_error());
    $pointsarray = array();
    while ($pointsrow = mysql_fetch_assoc($pointsresult)) {
        $pointsarray[] = $pointsrow;
    }
    //select all points from tbl_Feature table for My Add-ons
    $featurequery = "SELECT F_ID, F_Name, F_Points FROM tbl_Feature WHERE F_ID NOT IN (3)";
    $featureresult = mysql_query($featurequery) or die(mysql_error());
    while ($featurerow = mysql_fetch_assoc($featureresult)) {
        $pointsarray[] = $featurerow;
    }
    $each_record_points = array();
    //calculate the points and insurt in the tbl_business_listing
    if ($new_listing == 0) {
        $sql = "SELECT BL_ID, BL_Listing_Title, BL_Contact, BL_Phone, BL_Website, BL_Email, BL_Toll_Free, BL_Street, BL_Town, BL_Province, BL_PostalCode, 
                BL_Lat, BL_Long, BL_Description, BLP_Photo, BLP_Header_Image, BL_SEO_Title, BL_SEO_Description, BL_SEO_Keywords, BL_Search_Words, 
                BL_Hours_Disabled, BL_Hour_Mon_From, BL_Hour_Tue_From, BL_Hour_Wed_From, BL_Hour_Thu_From, BL_Hour_Fri_From, BL_Hour_Sat_From, 
                BL_Hour_Sun_From, BL_Trip_Advisor FROM tbl_Business_Listing LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID WHERE BL_Listing_Type != '1' GROUP BY BL_ID";
    } else {
        $sql = "SELECT BL_ID, BL_Listing_Title, BL_Contact, BL_Phone, BL_Website, BL_Email, BL_Toll_Free, BL_Street, BL_Town, BL_Province, BL_PostalCode, 
                BL_Lat, BL_Long, BL_Description, BLP_Photo, BLP_Header_Image, BL_SEO_Title, BL_SEO_Description, BL_SEO_Keywords, BL_Search_Words, 
                BL_Hours_Disabled, BL_Hour_Mon_From, BL_Hour_Tue_From, BL_Hour_Wed_From, BL_Hour_Thu_From, BL_Hour_Fri_From, BL_Hour_Sat_From, 
                BL_Hour_Sun_From, BL_Trip_Advisor FROM tbl_Business_Listing LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID WHERE BL_ID = '$new_listing' AND BL_Listing_Type != '1' GROUP BY BL_ID";
    }
    $result = mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    while ($row = mysql_fetch_array($result)) {
        $basic_points = 0;
        $addon_points = 0;
        $total_points = 0;
        foreach ($pointsarray as $points) {
            //Points for My Page
            //listing title
            if (isset($points['name']) && $points['name'] == 'Listing Title') {
                if ($row['BL_Listing_Title'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //contact name
            if (isset($points['name']) && $points['name'] == 'Contact Name') {
                if ($row['BL_Contact'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //phone
            if (isset($points['name']) && $points['name'] == 'Phone') {
                if ($row['BL_Phone'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            // cell
            if (isset($points['name']) && $points['name'] == 'Website') {
                if ($row['BL_Website'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            // cell
            if (isset($points['name']) && $points['name'] == 'Email') {
                if ($row['BL_Email'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            // cell
            if (isset($points['name']) && $points['name'] == 'Toll Free') {
                if ($row['BL_Toll_Free'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //street address
            if (isset($points['name']) && $points['name'] == 'Street Address') {
                if ($row['BL_Street'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //town
            if (isset($points['name']) && $points['name'] == 'Town') {
                if ($row['BL_Town'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //province
            if (isset($points['name']) && $points['name'] == 'Province') {
                if ($row['BL_Province'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //Postal Code
            if (isset($points['name']) && $points['name'] == 'Postal Code') {
                if ($row['BL_PostalCode'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //Latitude
            if (isset($points['name']) && $points['name'] == 'Latitude') {
                if ($row['BL_Lat'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //Longitude
            if (isset($points['name']) && $points['name'] == 'Longitude') {
                if ($row['BL_Long'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //Description
            if (isset($points['name']) && $points['name'] == 'Description') {
                if ($row['BL_Description'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //thumbnail photo
            if (isset($points['name']) && $points['name'] == 'Thumbnail') {
                if ($row['BLP_Photo'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //Main photo
            if (isset($points['name']) && $points['name'] == 'Main Image') {
                if ($row['BLP_Header_Image'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //Title Tag
            if (isset($points['name']) && $points['name'] == 'Title Tag') {
                if ($row['BL_SEO_Title'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //S.E.O Description
            if (isset($points['name']) && $points['name'] == 'S.E.O Description') {
                if ($row['BL_SEO_Description'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //Keywords
            if (isset($points['name']) && $points['name'] == 'Keywords') {
                if ($row['BL_SEO_Keywords'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //Search Words
            if (isset($points['name']) && $points['name'] == 'Search Words') {
                if ($row['BL_Search_Words'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //Amenity
            if (isset($points['name']) && $points['name'] == 'Amenities') {
                $sqlAmmenity = "SELECT BLA_BA_ID FROM tbl_Business_Listing_Ammenity LEFT JOIN tbl_Ammenity_Icons ON AI_ID = BLA_BA_ID 
                                WHERE BLA_BL_ID = '" . $row['BL_ID'] . "' AND AI_Image!=''";
                $resultAI = mysql_query($sqlAmmenity) or die("Invalid query: $sqlAmmenity -- " . mysql_error());
                $iCount = mysql_num_rows($resultAI);
                if ($iCount > 0) {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //hours
            if (isset($points['name']) && $points['name'] == 'Hours') {
                if (!$row['BL_Hours_Disabled'] && ($row['BL_Hour_Mon_From'] != '00:00:00' || $row['BL_Hour_Tue_From'] != '00:00:00' || $row['BL_Hour_Wed_From'] != '00:00:00' || $row['BL_Hour_Thu_From'] != '00:00:00' || $row['BL_Hour_Fri_From'] != '00:00:00' || $row['BL_Hour_Sat_From'] != '00:00:00' || $row['BL_Hour_Sun_From'] != '00:00:00')) {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //Social Media Enabled
            if (isset($points['name']) && $points['name'] == 'Social Media') {
                $sqlSM = "SELECT * FROM tbl_Business_Social WHERE BS_BL_ID = '" . $row['BL_ID'] . "' LIMIT 1";
                $resultSM = mysql_query($sqlSM) or die("Invalid query: $sqlSM -- " . mysql_error());
                $RowsSM = mysql_fetch_array($resultSM);
                if ($RowsSM['BS_FB_Link'] != '' || $RowsSM['BS_T_Link'] != '' || $RowsSM['BS_I_Link'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }
            //Recommendations
            if (isset($points['name']) && $points['name'] == 'Recommendations') {
                $recommends = " SELECT R_ID FROM tbl_Recommendations WHERE R_Recommends = '" . $row['BL_ID'] . "'";
                $recommended = "SELECT R_ID FROM tbl_Recommendations WHERE R_Recommended = '" . $row['BL_ID'] . "'";
                $recommends_result = mysql_query($recommends) or die("Invalid query: $recommends -- " . mysql_error());
                $recommends_count = mysql_num_rows($recommends_result);
                $recommended_result = mysql_query($recommended) or die("Invalid query: $recommended -- " . mysql_error());
                $recommended_count = mysql_num_rows($recommended_result);
                if ($recommends_count > 0) {
                    $recommends_count--;
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                    $total_points += $recommends_count;
                }
                if ($recommended_count > 0) {
                    $recommended_count = $recommended_count * 3;
                    // calculate basic, addon and total points
                    $total_points += $recommended_count;
                }
            }
            //Trip Advisor
            if (isset($points['name']) && $points['name'] == 'Trip Advisor') {
                if ($row['BL_Trip_Advisor'] != '') {
                    // calculate basic, addon and total points
                    $basic_points += $points['points'];
                    $total_points += $points['points'];
                }
            }

            //Points for My Add-ons
            //About Us
            if (isset($points['F_Name']) && $points['F_Name'] == 'About Us') {
                // calculate basic, addon and total points
                $Pur_Add_on = "SELECT BLF_BL_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = '" . $row['BL_ID'] . "' AND BLF_F_ID = '" . $points['F_ID'] . "' AND BLF_Active = 1";
                $resultPur_Add_on = mysql_query($Pur_Add_on) or die("Invalid query: $Pur_Add_on -- " . mysql_error());
                $rowPur_Add_on = mysql_fetch_assoc($resultPur_Add_on);
                if ($rowPur_Add_on['BLF_BL_ID'] != '') {
                    $Add_on = "SELECT BFA_ID FROM tbl_Business_Feature_About WHERE BFA_BL_ID = '" . $row['BL_ID'] . "'";
                    $resultAdd_on = mysql_query($Add_on) or die(mysql_error());
                    $numRowsAdd_on = mysql_num_rows($resultAdd_on);
                    if ($numRowsAdd_on > 0) {
                        $addon_points += $points['F_Points'];
                        $total_points += $points['F_Points'];
                    }
                }
            }
            //Photo Gallery
            if (isset($points['F_Name']) && $points['F_Name'] == 'Photo Gallery') {
                // calculate basic, addon and total points
                $Pur_Add_on1 = "SELECT BLF_BL_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = '" . $row['BL_ID'] . "' AND BLF_F_ID = '" . $points['F_ID'] . "' AND BLF_Active = 1";
                $resultPur_Add_on1 = mysql_query($Pur_Add_on1) or die("Invalid query: $Pur_Add_on1 -- " . mysql_error());
                $rowPur_Add_on1 = mysql_fetch_assoc($resultPur_Add_on1);
                if ($rowPur_Add_on1['BLF_BL_ID'] != '') {
                    $Add_on1 = "SELECT BFP_ID FROM tbl_Business_Feature_Photo WHERE BFP_BL_ID = '" . $row['BL_ID'] . "'";
                    $resultAdd_on1 = mysql_query($Add_on1) or die(mysql_error());
                    $numRowsAdd_on1 = mysql_num_rows($resultAdd_on1);
                    if ($numRowsAdd_on1 > 0) {
                        $addon_points += $points['F_Points'];
                        $total_points += $points['F_Points'];
                    }
                }
            }
            //Our Products
            if (isset($points['F_Name']) && $points['F_Name'] == 'Our Products') {
                // calculate basic, addon and total points
                $Pur_Add_on2 = "SELECT BLF_BL_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = '" . $row['BL_ID'] . "' AND BLF_F_ID = '" . $points['F_ID'] . "' AND BLF_Active = 1";
                $resultPur_Add_on2 = mysql_query($Pur_Add_on2) or die("Invalid query: $Pur_Add_on2 -- " . mysql_error());
                $rowPur_Add_on2 = mysql_fetch_assoc($resultPur_Add_on2);
                if ($rowPur_Add_on2['BLF_BL_ID'] != '') {
                    $Add_on2 = "SELECT BFP_ID FROM tbl_Business_Feature_Product WHERE BFP_BL_ID = '" . $row['BL_ID'] . "'";
                    $resultAdd_on2 = mysql_query($Add_on2) or die(mysql_error());
                    $numRowsAdd_on2 = mysql_num_rows($resultAdd_on2);
                    if ($numRowsAdd_on2 > 0) {
                        $addon_points += $points['F_Points'];
                        $total_points += $points['F_Points'];
                    }
                }
            }
            //Menu
            if (isset($points['F_Name']) && $points['F_Name'] == 'Menu') {
                // calculate basic, addon and total points
                $Pur_Add_on5 = "SELECT BLF_BL_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = '" . $row['BL_ID'] . "' AND BLF_F_ID = '" . $points['F_ID'] . "' AND BLF_Active = 1";
                $resultPur_Add_on5 = mysql_query($Pur_Add_on5) or die("Invalid query: $Pur_Add_on5 -- " . mysql_error());
                $rowPur_Add_on5 = mysql_fetch_assoc($resultPur_Add_on5);
                if ($rowPur_Add_on5['BLF_BL_ID'] != '') {
                    $Add_on5 = "SELECT BFM_ID FROM tbl_Business_Feature_Menu WHERE BFM_BL_ID = '" . $row['BL_ID'] . "'";
                    $resultAdd_on5 = mysql_query($Add_on5) or die(mysql_error());
                    $numRowsAdd_on5 = mysql_num_rows($resultAdd_on5);
                    if ($numRowsAdd_on5 > 0) {
                        $addon_points += $points['F_Points'];
                        $total_points += $points['F_Points'];
                    }
                }
            }
            //Daily Specials
            if (isset($points['F_Name']) && $points['F_Name'] == 'Daily Features') {
                // calculate basic, addon and total points
                $Pur_Add_on6 = "SELECT BLF_BL_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = '" . $row['BL_ID'] . "' AND BLF_F_ID = '" . $points['F_ID'] . "' AND BLF_Active = 1";
                $resultPur_Add_on6 = mysql_query($Pur_Add_on6) or die("Invalid query: $Pur_Add_on6 -- " . mysql_error());
                $rowPur_Add_on6 = mysql_fetch_assoc($resultPur_Add_on6);
                if ($rowPur_Add_on6['BLF_BL_ID'] != '') {
                    $Add_on6 = "SELECT BFDS_ID FROM tbl_Business_Feature_Daily_Specials WHERE BFDS_BL_ID = '" . $row['BL_ID'] . "'";
                    $resultAdd_on6 = mysql_query($Add_on6) or die(mysql_error());
                    $numRowsAdd_on6 = mysql_num_rows($resultAdd_on6);
                    if ($numRowsAdd_on6 > 0) {
                        $addon_points += $points['F_Points'];
                        $total_points += $points['F_Points'];
                    }
                }
            }
            //Entertainment
            if (isset($points['F_Name']) && $points['F_Name'] == 'Entertainment') {
                // calculate basic, addon and total points
                $Pur_Add_on7 = "SELECT BLF_BL_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = '" . $row['BL_ID'] . "' AND BLF_F_ID = '" . $points['F_ID'] . "' AND BLF_Active = 1";
                $resultPur_Add_on7 = mysql_query($Pur_Add_on7) or die("Invalid query: $Pur_Add_on7 -- " . mysql_error());
                $rowPur_Add_on7 = mysql_fetch_assoc($resultPur_Add_on7);
                if ($rowPur_Add_on7['BLF_BL_ID'] != '') {
                    $Add_on7 = "SELECT BFEA_ID FROM tbl_Business_Feature_Entertainment_Acts WHERE BFEA_BL_ID = '" . $row['BL_ID'] . "'";
                    $resultAdd_on7 = mysql_query($Add_on7) or die(mysql_error());
                    $numRowsAdd_on7 = mysql_num_rows($resultAdd_on7);
                    if ($numRowsAdd_on7 > 0) {
                        $addon_points += $points['F_Points'];
                        $total_points += $points['F_Points'];
                    }
                }
            }
            //Guest Book
            if (isset($points['F_Name']) && $points['F_Name'] == 'Guest Book') {
                // calculate basic, addon and total points
                $Pur_Add_on8 = "SELECT BLF_BL_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = '" . $row['BL_ID'] . "' AND BLF_F_ID = '" . $points['F_ID'] . "' AND BLF_Active = 1";
                $resultPur_Add_on8 = mysql_query($Pur_Add_on8) or die("Invalid query: $Pur_Add_on8 -- " . mysql_error());
                $rowPur_Add_on8 = mysql_fetch_assoc($resultPur_Add_on8);
                if ($rowPur_Add_on8['BLF_BL_ID'] != '') {
                    $Add_on8 = "SELECT BFGB_ID FROM tbl_Business_Feature_Guest_Book WHERE BFGB_BL_ID = '" . $row['BL_ID'] . "' AND BFGB_Status = 1";
                    $resultAdd_on8 = mysql_query($Add_on8) or die(mysql_error());
                    $numRowsAdd_on8 = mysql_num_rows($resultAdd_on8);
                    if ($numRowsAdd_on8 > 0) {
                        $addon_points += $points['F_Points'];
                        $total_points += $points['F_Points'];
                    }
                }
            }
            //Power Ranking
            if (isset($points['F_Name']) && $points['F_Name'] == 'Power Ranking') {
                // calculate basic, addon and total points
                $Pur_Add_on9 = "SELECT * FROM tbl_BL_Feature WHERE BLF_BL_ID = '" . $row['BL_ID'] . "' AND BLF_F_ID = '" . $points['F_ID'] . "' AND BLF_Active = 1";
                $resultPur_Add_on9 = mysql_query($Pur_Add_on9) or die("Invalid query: $Pur_Add_on9 -- " . mysql_error());
                $rowPur_Add_on9 = mysql_fetch_assoc($resultPur_Add_on9);
                if ($rowPur_Add_on9['BLF_BL_ID'] != '') {
                    $addon_points += $points['F_Points'];
                    $total_points += $points['F_Points'];
                }
            }
            //Agenda & Minute
            if (isset($points['F_Name']) && $points['F_Name'] == 'Agendas & Minutes') {
                // calculate basic, addon and total points
                $Pur_Add_on10 = "SELECT BLF_BL_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = '" . $row['BL_ID'] . "' AND BLF_F_ID = '" . $points['F_ID'] . "' AND BLF_Active = 1";
                $resultPur_Add_on10 = mysql_query($Pur_Add_on10) or die("Invalid query: $Pur_Add_on10 -- " . mysql_error());
                $rowPur_Add_on10 = mysql_fetch_assoc($resultPur_Add_on10);
                if ($rowPur_Add_on10['BLF_BL_ID'] != '') {
                    $Add_on10 = "SELECT BFAM_ID FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . $row['BL_ID'] . "'";
                    $resultAdd_on10 = mysql_query($Add_on10) or die(mysql_error());
                    $numRowsAdd_on10 = mysql_num_rows($resultAdd_on10);
                    if ($numRowsAdd_on10 > 0) {
                        $addon_points += $points['F_Points'];
                        $total_points += $points['F_Points'];
                    }
                }
            }
            //Manage Coupon
            if (isset($points['F_Name']) && $points['F_Name'] == 'Manage Coupon') {
                // calculate basic, addon and total points
                $Pur_Add_on11 = "SELECT BLF_BL_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = '" . $row['BL_ID'] . "' AND BLF_F_ID = '" . $points['F_ID'] . "' AND BLF_Active = 1";
                $resultPur_Add_on11 = mysql_query($Pur_Add_on11) or die("Invalid query: $Pur_Add_on11 -- " . mysql_error());
                $rowPur_Add_on11 = mysql_fetch_assoc($resultPur_Add_on11);
                if ($rowPur_Add_on11['BLF_BL_ID'] != '') {
                    $Add_on11 = "SELECT BFC_ID FROM tbl_Business_Feature_Coupon WHERE BFC_BL_ID = '" . $row['BL_ID'] . "' AND BFC_Status = 1";
                    $resultAdd_on11 = mysql_query($Add_on11) or die(mysql_error());
                    $numRowsAdd_on11 = mysql_num_rows($resultAdd_on11);
                    if ($numRowsAdd_on11 > 0) {
                        $addon_points += $points['F_Points'];
                        $total_points += $points['F_Points'];
                    }
                }
            }
        }//end foreach
        $each_record_points[$row['BL_ID']]['basic_points'] = $basic_points;
        $each_record_points[$row['BL_ID']]['addon_points'] = $addon_points;
        $each_record_points[$row['BL_ID']]['total_points'] = $total_points;
    } //end while loop
//Now update the tbl_Business_Listing table
    foreach ($each_record_points as $eachid => $value) {
        $update_points_query = "UPDATE tbl_Business_Listing SET BL_Basic_Points = '" . $value['basic_points'] . "', 
                                BL_Addon_Points = '" . $value['addon_points'] . "', BL_Points = '" . $value['total_points'] . "', 
                                Points_updated_date = '$pointsUpdatedDate' WHERE BL_ID = '$eachid'";
        mysql_query($update_points_query) or die(mysql_error());
    }
}

//Function to show points of fields under My Page menu 
function show_field_points($field_name) {
    $sql = "SELECT points FROM points WHERE name = '$field_name'";
    $result = mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_array($result);
    $points = $row['points'];
    return $points;
}

//Function to show Addons points under My Add Ons
function show_addon_points($id) {
    $sql = "SELECT F_Points FROM tbl_Feature WHERE F_ID = '$id'";
    $result = mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_array($result);
    $points = $row['F_Points'];
    return $points;
}

//Function to show points Help Text on user admin
function show_help_text($field_name) {
    $sql = "SELECT HT_Description FROM tbl_Help_Text WHERE HT_Name = '$field_name'";
    $result = mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_array($result);
    $help_text = $row['HT_Description'];
    return $help_text;
}

//function to insert billing data against listing
function listing_billing($billing_type, $BL_ID, $discount, $title, $price) {
    $sql = "SELECT BL_ID, BL_Listing_Type, BL_Billing_Type FROM tbl_Business_Listing WHERE BL_ID = '$BL_ID' LIMIT 1";
    $result = mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $subtotal = 0;
    // see if yearly or monthy and add multiplier accordingly
    if ($billing_type != $rowListing['BL_Billing_Type']) {
        $discount = 0;
        $_SESSION['discount_updated'] = 1;
    }
    if ($billing_type == -1) {
        $multiplier = 12;
    } else if($billing_type == 1) {
        $multiplier = 1;
    }else if($billing_type == 2) {
        $multiplier = 2;
    }
    $sql_bl = "SELECT ($multiplier * LT_Cost) FROM tbl_Listing_Type WHERE LT_ID = " . $rowListing['BL_Listing_Type']; // listing price
    $listing_price = mysql_result(mysql_query($sql_bl), 0);

    $sql_lf = "SELECT SUM($multiplier * F_Price) as feature_price FROM `tbl_BL_Feature` LEFT JOIN `tbl_Feature` ON BLF_F_ID = F_ID WHERE BLF_BL_ID = " . $rowListing['BL_ID'] . " AND BLF_Active = 1"; // tools price
    $feature_price = mysql_result(mysql_query($sql_lf), 0);
    $subtotal = $listing_price + $feature_price + $price;
    $subtotal2 = $subtotal - $discount;
    $tax = round($subtotal2 * .13, 2);
    $total = $subtotal2 + $tax;
    if ($discount > $subtotal) {
        $_SESSION['error'] = 2;
    } else {
        $billing_type == '' ? $billing_type = 0 : '';
        $subtotal == '' ? $subtotal = 0 : '';
        $discount == '' ? $discount = 0 : '';
        $subtotal2 == '' ? $subtotal2 = 0 : '';
        $tax == '' ? $tax = 0 : '';
        $total == '' ? $total = 0 : '';
        $title == '' ? $title = 0 : '';
        $price == '' ? $price = 0 : '';
        $sql = "UPDATE tbl_Business_Listing SET BL_Billing_Type = '$billing_type', ";
        $sql .= "BL_SubTotal = '$subtotal', 
                BL_CoC_Dis_2 = '$discount', 
                BL_SubTotal3 = '$subtotal2', 
                BL_Tax = '$tax', 
                BL_Total = '$total',
                BL_Line_Item_Title = '$title', 
                BL_Line_Item_Price = '$price'
                WHERE BL_ID = '" . $rowListing['BL_ID'] . "'";
        $result = mysql_query($sql);
        if ($result) {
            $_SESSION['success'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
    }
}

//function to count total number and revenue of active ads against region and Ad type for super admin
function show_ads_stats($R_ID, $AT_ID) {
    $sql = "SELECT A_ID, A_Total FROM tbl_Advertisement WHERE A_Website = '$R_ID' AND A_AT_ID = '$AT_ID' AND A_Status = 3 AND A_Is_Deleted = 0
            AND (A_End_Date >= CURDATE() OR A_End_Date = 0000-00-00)";
    $result = mysql_query($sql);
    $count = mysql_num_rows($result);
    $Revenue = 0;
    $stats = array();
    while ($row = mysql_fetch_array($result)) {
        $Revenue += $row['A_Total'];
    }
    $stats['count'] = $count;
    $stats['revenue'] = $Revenue;
    return $stats;
}

//function to count total impressions and clicks of active ads against category and Ad type for super admin
function show_ads_report($R_ID, $C_ID, $AT_ID, $month, $year) {
    $whereclause = '';
    if ($R_ID != '') {
        $whereclause .= " AND A_Website = '$R_ID'";
    }
    if ($month != '') {
        $whereclause .= " AND MONTH(AS_Date) = '$month'";
    }
    if ($year != '') {
        $whereclause .= " AND YEAR(AS_Date) = '$year'";
    }
    $sql = "SELECT SUM(AS_Impression) AS impressions, SUM(AS_Clicks) AS clicks
            FROM tbl_Advertisement_Statistics
            LEFT JOIN tbl_Advertisement ON A_ID = AS_A_ID
            WHERE A_C_ID = '$C_ID' AND A_AT_ID = '$AT_ID' AND A_Status = 3 AND A_Is_Deleted = 0
            AND (A_End_Date >= CURDATE() OR A_End_Date = 0000-00-00) $whereclause";
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    $reports['impressions'] = $row['impressions'];
    $reports['clicks'] = $row['clicks'];
    return $reports;
}

?>