<?php
require_once "config.inc.php";
require_once "public-site-functions.inc.php";

/*
 |  acts as a dispatcher for trip-planner related 
 |  ajax/XHR calls
*/


/*
    Obtains the current "tripplanner" cookie, or sets a value,
    should it not currently exist. Meaning the old cookie expired
    or this was a first time user.
*/
function get_or_set_uniq()
{
    if (!isset($_COOKIE["tripplanner"]))
    {
        $uniqId = uniqid("anontrip-", true);
        setcookie("tripplanner", $uniqId, time()+(60*60*24*60), "/");
        $_COOKIE["tripplanner"] =  $uniqId;
    }

    return $_COOKIE["tripplanner"];
}
/*
    Yields the current count of items in the trip-planner bag
    for the given PHPSESSIONID
*/
function trip_count($db)
{
    $sql = "SELECT count(*)
              FROM tbl_Session_Tripplanner
             WHERE ST_Session_ID = '" . encode_strings(get_or_set_uniq(), $db) . "'";

    $result = mysql_query($sql) or die ("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_row($result);

    return array("count"=> $row[0]);

}

/*
    Adds a row to the tbl_Session_Tripplanner table for the given 
    PHPSESSION. 

    success : returns array
    fail : returns HTTP error code 500
*/
function trip_add($businessID, $categoryID,$bl_ID, $db)
{

    // ensure that the listing does exist
    $result = mysql_query("SELECT COUNT(*) 
                             FROM tbl_Business_Listing
                            WHERE BL_ID = " . encode_strings($bl_ID, $db));

    if (! $result)
    {
        echo mysql_error;
    }

    $row = mysql_fetch_row($result);

    // unable to locate business in db, return failure.
    if (! $row  >= 1)
    {
        header("HTTP/1.1 500 Internal Server Error");
        die();
    }
    // insert new business id  into trip session store        
    $sql = "INSERT INTO tbl_Session_Tripplanner
                          (ST_Session_ID, ST_Type_ID, ST_Bus_ID,ST_BL_ID)
                   VALUES ('" . encode_strings(get_or_set_uniq(), $db) . "', ".
                             encode_strings($categoryID, $db) . ", " . 
                             encode_strings($businessID, $db) . ",".
                             encode_strings($bl_ID, $db) . ")";
//                     print_r($sql);
//                     exit;

    mysql_query($sql) or header("HTTP/1.1 500 Internal Server Error");

    return array("status"=>"ok");

}

function trip_delete($businessID, $categoryID,$bl_ID, $db)
{
    $sql = "DELETE FROM tbl_Session_Tripplanner 
             WHERE ST_Session_ID = '" . encode_strings(get_or_set_uniq(), $db) . "'
               AND ST_Bus_ID = " . encode_strings($businessID, $db) . "
               AND ST_BL_ID = " . encode_strings($bl_ID, $db) . "
               AND ST_Type_ID = " . encode_strings($categoryID, $db);

    mysql_query($sql) or header("HTTP/1.1 500 Internal Server Error");
     
}


if(isset($_POST["action"]) && isset($_COOKIE["PHPSESSID"]))
{
    if ($_POST["action"] != "")
    {
        $action = $_POST["action"];

        if (isset($_POST["businessID"]) && isset($_POST["categoryID"]))
        {
            $businessID = $_POST["businessID"];
            $categoryID = $_POST["categoryID"];
            $bl_ID = $_POST["bl_ID"];
        }
        else
        {
            $businessID = False;
            $categoryID = False;
            $bl_ID = False;
        }


        switch ($action)
        {
            case "add":
                $status = trip_add($businessID, $categoryID,$bl_ID, $db);
                break;
            case "delete":
                $status = trip_delete($businessID, $categoryID,$bl_ID, $db);
                break;
            case "count";
                $status = trip_count($db);
                break;
            case "status":
                $status = array("status"=>"Don\"t Believe the Hype");
                break;
            default:
                header("HTTP/1.1 500 Internal Server Error");
                break;
        }

        // return our json object to the calling jquery item.
        header('Content-Type: application/json');
        echo json_encode($status);
        die();
    }
}
?>
