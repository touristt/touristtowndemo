<?php
//check to pass mobile users to mobile version
$useragent = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))){
    header('Location: http://touristtowndemo.com/admin/mobile/');
}
?>
<?PHP
define("DOMAIN", 'touristtowndemo.com');
define('SESSIONERROR', 'ERROR');
define('SESSIONINFO', 'info');
$sql_select = "SELECT * FROM tbl_User ";
if (isset($_REQUEST['logop']) && $_REQUEST['logop'] == "login") {
    $sql_log = $sql_select
            . "WHERE U_Username 	= '" . encode_strings($_POST['LOGIN_USER'], $db) . "' "
            . "&& U_Password = '" . md5(encode_strings($_POST['LOGIN_PASS'], $db)) . "' ";
    $result_log = mysql_query($sql_log, $db) or die("Invalid query: $sql_log -- " . mysql_error());
    if (mysql_num_rows($result_log) <> 1) {
        $_SESSION['login_error'] = 1;
        header("Location: " . INPUT_DIR . "login.php");
        exit();
    } else {
        $UID = UpdateUserLog($result_log, $db);
    }
} elseif (isset($_REQUEST['logop']) && $_REQUEST['logop'] == 'logout') {
    $sql = "UPDATE tbl_User SET U_EXPIRE = '0000-00-00' "
            . "WHERE U_KEY = '" . $_COOKIE['USER_KEY'] . "' ";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    my_session_destroy('admin');
    header("Location: " . INPUT_DIR);
    exit();
} elseif (isset($_REQUEST['logop']) && $_REQUEST['logop'] == "lost_pw") {
    my_session_destroy('admin');
    header("Location: " . INPUT_DIR . "lost-password.php");
    exit();
} elseif (isset($_REQUEST['logop']) && $_REQUEST['logop'] == "send_pw") {
    if ($_POST[USER_EMAIL] != '') {
        $result_pw = mysql_query("SELECT * FROM tbl_User WHERE U_Email = '"
                . encode_strings($_POST[USER_EMAIL], $db) . "' ", $db);
        $num_row = mysql_num_rows($result_pw);
        if ($num_row == 0) {
            $_SESSION['error_PW'] = 1;
        } elseif ($num_row == 1) {
            $row_pw = mysql_fetch_assoc($result_pw);
            $_SESSION['send_password_uid'] = $row_pw['User_ID'];
            $_SESSION['send_password_email'] = $row_pw['User_Email'];
            header("Location: " . INPUT_DIR . "send-password.php");
            exit();
        } else {
            $_SESSION['error_PW'] = 1;
        }
    } else {
        header("Location: " . INPUT_DIR);
    }
    login_exit();
} elseif (isset($_SESSION['USER_KEY']) && $_SESSION['USER_KEY'] != '0' && $_SESSION['USER_KEY'] != '') {
    $sql_Check = $sql_select
            . "WHERE U_KEY = '" . encode_strings($_SESSION['USER_KEY'], $db) . "' "
            . "&& U_EXPIRE > NOW() ";
    $result_Check = mysql_query($sql_Check, $db) or die("Invalid query: $sql_Check -- " . mysql_error());
    if (mysql_num_rows($result_Check) <> 1) {
        my_session_destroy('admin');
        header("Location: " . INPUT_DIR . "login.php");
        exit();
    } else {
        $UID = UpdateUserLog($result_Check, $db, $_SESSION['USER_KEY']);
    }
} elseif (isset($_REQUEST['logop']) && $_REQUEST['logop'] == 'chpw') {
    if ($_POST[op] == "change") {
        $err = '';
        if ($_POST[old_pw] != '') {
            $sql = "SELECT * FROM tbl_User WHERE U_ID = " . $UID
                    . " && U_Password = '" . md5($_POST[old_pw]) . "' ";
            $result = mysql_query($sql, $db);
            if ($result) {
                if (mysql_num_rows($result) == 1) {
                    if ($_POST[new_pw] == $_POST[new_pw_confirm]) {
                        if (strlen($_POST[new_pw]) > 4) {
                            $sql_pw = "UPDATE tbl_User SET U_Password = '"
                                    . md5($_POST[new_pw]) . "' WHERE U_ID = "
                                    . $UID . " LIMIT 1 ";
                            if (mysql_query($sql_pw, $db)) {
                                $err = '';
                            } else {
                                $err = 'Query Invalid';
                            }
                        } else {
                            $err = 'Password must be at least 5 characters.';
                        }
                    } else {
                        $err = 'New Passwords don\'t match';
                    }
                } else {
                    $err = 'Old password did\'t match';
                }
            } else {
                $err = 'Query Invalid';
            }
        } else {
            $err = 'Old Password was blank';
        }


        if ($err == '') {
            $_SESSION['error_CW'] = '<p align="center"><strong><font color="#006600" size="+2">Password Changed!</font></strong></p>';
        } else {
            $_SESSION['error_CW'] = $err;
        }
    }
    header("Location: " . INPUT_DIR . "change-password.php");
    exit();
} else {
    my_session_destroy('admin');
    header("Location: " . INPUT_DIR . "login.php");
    exit();
}

function UpdateUserLog($result, $db, $key = "0") {
    if ($row = mysql_fetch_assoc($result)) {
        $sql = "UPDATE tbl_User SET U_EXPIRE = DATE_ADD(NOW(), INTERVAL 1 HOUR)";
        if (isset($key) && $key == "0") {
            $key = mt_rand(1000, 9999999);
            $sql .= ", U_KEY = '$key'";
            $_SESSION['USER_KEY'] = $key;
            $_SESSION['USER_ID'] = $row['U_ID'];
            $_SESSION['USER_NAME'] = $row['U_Username'];
            $_SESSION['USER_EM'] = $row['U_Email'];
            $_SESSION['USER_SUPER_USER'] = $row['U_Super_User'];
            $_SESSION['USER_SHOW_BUSINESSES'] = $row['U_Show_Businesses'];
            $_SESSION['USER_ROLES'] = array();
            $sql_role = "select * from tbl_User_Role WHERE UR_U_ID = '" . $_SESSION['USER_ID'] . "'";
            $result_role = mysql_query($sql_role, $db) or die("Invalid query: $sql_role -- " . mysql_error());
            while ($data_role = mysql_fetch_assoc($result_role)) {
                $_SESSION['USER_ROLES'][] = $data_role['UR_R_ID'];
                if ($data_role['UR_R_ID'] == 'bgadmin') {
                    $sqlbg = "select * from tbl_Buying_Group left join tbl_User_Role on BG_ID = UR_Limit WHERE UR_Limit = '" . $data_role['UR_Limit'] . "'";
                    $resultbg = mysql_query($sqlbg, $db) or die("Invalid query: $sqlbg -- " . mysql_error());
                    $rowbg = mysql_fetch_assoc($resultbg);
                    $_SESSION['USER_LIMIT'] = $rowbg['BG_Region'];
                } else {
                    $_SESSION['USER_LIMIT'] = $data_role['UR_Limit'];
                }
            }
            $_SESSION['USER_PERMISSIONS'] = array();
            $sql2 = "SELECT * FROM tbl_User_Permission LEFT JOIN tbl_User_Role on UP_R_ID = UR_R_ID  WHERE UR_U_ID = '" . $row[U_ID] . "'";
            $result = mysql_query($sql2, $db) or die("Invalid query: $sql2 -- " . mysql_error());
            while ($data = mysql_fetch_assoc($result)) {
                $_SESSION['USER_PERMISSIONS'][] = $data['UP_P_ID'];
            }
            $_SESSION['user_online'] = $row['U_F_Name'];
        }
        $sql .= " WHERE U_ID = '$row[U_ID]' ";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    } else {
        InvalidUser();
        login_exit();
    }
    return $row['U_ID'];
}

?>