/*----------------------------------------------------*/
/*	Date Picker
 /*----------------------------------------------------*/
$(document).ready(function () {
    //Show first image and hide all cycle plugin for listing profile
    $(".sliderCycle").children().hide();// hides all images
    $(".sliderCycle").children(":first").show();// shows the first image
    $(".datepicker").datepicker({
        firstDay: 1,
        dateFormat: 'yy-mm-dd'
    });
    $(".previous-date-not-allowed").datepicker({
        minDate: 0,
        firstDay: 1,
        dateFormat: 'yy-mm-dd',
        prevText: "",
        nextText: ""
    });
    $('.timepicker').timepicker({});
//    var middle = $('.logo-top-nav').width();
//    middle = middle + 30;
//    var width_for_div = 949 - middle;
//    $('.site-logo').width(width_for_div);
    /*----------------------------------------------------*/
    /*	Layer Slider on home, listing detail page
     /*----------------------------------------------------*/
    $('ul.bxslider li').show();
    $('.bxslider').bxSlider({
        minSlides: 3,
        maxSlides: 3,
        slideWidth: 960,
        slideMargin: 10,
        pager: false,
        infiniteLoop: false,
        moveSlides: 3
    });
    $(".phone_number").mask("999-999-9999");
});

/*----------------------------------------------------*/
/*	Display Search Box in Header
 /*----------------------------------------------------*/
function displayHeaderForm() {
    $("#search_form").toggle();
}
/*----------------------------------------------------*/
/*	JQuery Cycle Slider
 /*----------------------------------------------------*/
$(window).load(function () {
    $('.show-slider-display').show();
    $('#next').show();
    $('#prev').show();
    $('.sliderCycle').cycle({
        fx: 'fade',
        next: '#next',
        prev: '#prev',
        before: onAfter
    });
    function onAfter(curr, next, opts) {
        var caption = ($(next).index() + 1) + '/' + opts.slideCount;
    }
    var slides = $('.sliderCycle').children().length;
    if (slides <= 1) {
        $('#next').css("display", "none");
        $('#prev').css("display", "none");
    } else {
        $('#next').css("display", "visible");
        $('#prev').css("display", "visible");
    }
});

//on window resize
$(window).resize(function () {
    $('.sliderCycle').height($('.slide-images:visible img.slider_image').height());
});

/*----------------------------------------------------*/
/*	Clear Text Boxes
 /*----------------------------------------------------*/
function clearOrg() {
    if ($('#organization').val() == 'Alternate Organization') {
        $('#organization').val('');
    }
}

function clearLoc() {
    if ($('#location').val() == 'Alternate Location') {
        $('#location').val('');
    }
}

function clearSearch() {
    if ($('#txtSearch').val() == 'Search') {
        $('#txtSearch').val('');
    }
}
function clearEventSearch() {
    if ($('#etxtSearch').val() == 'Search') {
        $('#etxtSearch').val('');
    }
}

function limitTextArea(myID, myCount)
{
    if ($("#" + myID).val().length > myCount) {
        $("#" + myID).val($("#" + myID).val().substr(0, myCount));
    }
}
/*----------------------------------------------------*/
/*	Tracking Advertisements
 /*----------------------------------------------------*/
function advertClicks(A_ID) {
    var pathname = window.location.href;
    var split_path = pathname.split("/");
    var DOMAIN = split_path[2];
    $.post('http://' + DOMAIN + '/update-clicks-advert.php', {
        AS_A_ID: A_ID
    });
}
function dropdown_ads_impression(C_ID) {
    var ad = 0;
    var total = document.getElementById('total-ads-' + C_ID).value;
    for (var i = 0; i < total; i++) {
        ad = document.getElementById('ad' + i + '-' + C_ID);
        if (ad.value != '') {
            var pathname = window.location.href;
            var split_path = pathname.split("/");
            var DOMAIN = split_path[2];
            $.post('http://' + DOMAIN + '/dropdown-impressions.php', {
                AS_A_ID: ad.value
            });
        }
    }
}
/*------------------------------------------------------------------------------------------------*/
/*	Get AJAX Stories on stories page, Load Listing Functions & Get Seasons Listings
 /*------------------------------------------------------------------------------------------------*/
$(document).ready(function () {
    show_menu(); //Load Menu on Listing Page
    show_aboutus(); // //Load About Us on Listing Page
    show_entertainment(); // //Load entertainment on Listing Page
    show_agenda(); // //Load agenda on Listing Page
    var pathname = window.location.href;
    var split_path = pathname.split("/");
    var DOMAIN = split_path[2];
    //Get Stories on explore category
    $('#cat_stories').on('change', function () {
        var StoryCategory = this.value;
        var currentStory = $('#currentStory').val();
        var currentRegion = $('#currentRegion').val();
        var currentLimit = $('#currentLimit').val();
        $.post('http://' + DOMAIN + '/getstories.php', {
            StoryCategory: StoryCategory,
            currentRegion: currentRegion,
            currentStory: currentStory,
            currentLimit: currentLimit
        }, function (result) {
            $("#stories-right").empty();
            $("#stories-right").html(result);
        });
    });
    //Get Stories on explore category
    $('#change_season').on('change', function () {
        var SeasonCategory = this.value;
        $.post('http://' + DOMAIN + '/getseasons.php', {
            SeasonCategory: SeasonCategory
        }, function () {
            location.reload();
        });
    });
});
/*----------------------------------------------------*/
/*	Listing Home page
 /*----------------------------------------------------*/
$(function () {
    $(".accordion").accordion({
        collapsible: true,
        heightStyle: "content",
        active: false
    });
});
function show_menu()
{
    var menu_key = $("#menu_key").val();
    $(".meun-hide-show").fadeOut(500);
    $(".menu-" + menu_key).fadeIn(1000);
}
function show_aboutus()
{
    var aboutus = $("#aboutus_id").val();
    $(".aboutus-body-detail").fadeOut(500);
    $(".aboutus-" + aboutus).fadeIn(1000);
}
function show_entertainment()
{
    var entertainment = $("#entert_id").val();
    $(".entert-hide-show").fadeOut(500);
    $(".entert-" + entertainment).fadeIn(1000);
}
function show_agenda()
{
    var entertainment = $("#agenda_id").val();
    $(".agenda-min-download").fadeOut(500);
    $(".agenda-" + entertainment).fadeIn(1000);
}
function search_word()
{
    var keyvalue = $("#search-agenda-key").val();
    var numItems = $(".agenda-" + keyvalue).length;
    if (numItems > 0)
    {
        $("#search-heading").text("Search result for: " + keyvalue);
        $(".agenda-show-hide").hide();
        $(".agenda-min-download-search").fadeIn(500);
        $(".agenda-" + keyvalue).fadeIn(500);
    } else {
        $("#search-heading").text("Search result for: " + keyvalue);
        $(".agenda-min-download-search").show();
        $(".agenda-show-hide").hide();
        $(".agenda-no-result").fadeIn(500);
    }
}
/*----------------------------------------------------*/
/*	
 /*----------------------------------------------------*/
/*Validate Event form*/
function validateEventForm() {
    var pathname = window.location.href;
    var split_path = pathname.split("/");
    var DOMAIN = split_path[2];
    if ($("#location").val() == '' && $("#setLocationID").val() == 0) {
        swal("Error", "Location Or Alternate Location Required", "error");
        $("#location").focus();
        return false;
    }
    if ($("#organization").val() == '' && $("#setOrganizationID").val() == 0) {
        swal("Error", "Organization Or Alternate Organization Required", "error");
        $("#organization").focus();
        return false;
    }
    if ($("#g-recaptcha-response").val() == '') {
        swal("Error", "Please click on the reCAPTCHA box.", "error");
        return false;
    } else {
        var gRecaptchaResponse = $("#g-recaptcha-response").val();
        $.post('http://' + DOMAIN + '/get_recaptcha_response.php', {
            gRecaptchaResponse: gRecaptchaResponse
        }, function (result) {
            if (result == 0) {
                swal("Error", "Robot verification failed, please try again.", "error");
                return false;
            }
        });
    }
    $("#button2").attr('disabled', 'disabled');
}
  
$(function () {
    var pathname = window.location.href;
    var split_path = pathname.split("/");
    var DOMAIN = split_path[2];
    $(".weekday").click(function () {
        if ($(this).prop('checked') == true) {
            $(this).val(1);
        } else if ($(this).prop('checked') == false) {
            $(this).val(0);
        }
    });
    $("#all_day").click(function () {
        if ($(this).prop('checked') == true) {
            $(this).val(1);
            $('#starttime').hide();
            $('#starttime').attr('required', false);
            $('#endtime').hide();
            $('#endtime').attr('required', false);
        } else if ($(this).prop('checked') == false) {
            $(this).val(0);
            $('#starttime').show();
            $('#starttime').attr('required', true);
            $('#endtime').show();
            $('#endtime').attr('required', true);
        }
    });
    $(".community_select").change(function () {
        var childRegion = $(this).val();
        $.post('http://' + DOMAIN + '/get_organizations_ajax.php', {
            childRegion: childRegion,
            organization: '1'
        }, function (result) {
            $("#setOrganizationID").empty();
            $("#setOrganizationID").html(result);
        });
        $.post('http://' + DOMAIN + '/get_organizations_ajax.php', {
            childRegion: childRegion,
            location: '1'
        }, function (result) {
            $("#setLocationID").empty();
            $("#setLocationID").html(result);
        });
    });
});

function validateGuestBookForm() {
    if ($("#g-recaptcha-response").val() == '') {
        swal("Error", "Please click on the reCAPTCHA box.", "error");
        return false;
    }
}

/*CUSTOM SCRIPT(include/js/custom-script.js)*/

(function($) {

    $.fn.quickPager = function(options) {
        var defaults = {
            pageSize: 10,
            currentPage: 1,
            holder: null,
            pagerLocation: "after"
        };
        var options = $.extend(defaults, options);
        return this.each(function() {
            var selector = $(this);
            var pageCounter = 1;
            selector.wrap("<div class='simplePagerContainer'></div>");
            selector.parents(".simplePagerContainer").find("ul.simplePagerNav").remove();
            selector.children().each(function(i) {
                if (i < pageCounter * options.pageSize && i >= (pageCounter - 1) * options.pageSize) {
                    $(this).addClass("simplePagerPage" + pageCounter);
                }
                else {
                    $(this).addClass("simplePagerPage" + (pageCounter + 1));
                    pageCounter++;
                }
            });
            // show/hide the appropriate regions 
            selector.children().hide();
            selector.children(".simplePagerPage" + options.currentPage).show();
            if (pageCounter <= 1) {
                return;
            }
            //Build pager navigation
            var pageNav = "<div class='pagination clearfix'><div class='pull-left'>Page " + options.currentPage + " of " + pageCounter + "</div><div class='pull-right'><ul style='list-style-type:none' class='simplePagerNav'>";
            for (i = 1; i <= pageCounter; i++) {
                if (i == options.currentPage) {
                    pageNav += "<li class='currentPage simplePageNav" + i + "' style='float:left;margin-left:5px;'><span class=' page-active page'><a rel='" + i + "' href='#'>" + i + "</a></span></li>";
                }
                else {
                    pageNav += "<li class='simplePageNav" + i + "' style='float:left;margin-left:5px;'><span class='page'><a rel='" + i + "' href='#'>" + i + "</a></span></li>";
                }
            }
            pageNav += "</ul></div></div>";
            if (!options.holder) {
                switch (options.pagerLocation)
                {
                    case "before":
                        selector.before(pageNav);
                        break;
                    case "both":
                        selector.before(pageNav);
                        selector.after(pageNav);
                        break;
                    default:
                        selector.after(pageNav);
                }
            }
            else {
                $(options.holder).append(pageNav);
            }
            //pager navigation behaviour
            selector.parent().find(".simplePagerNav a").click(function() {
                //grab the REL attribute 
                var clickedLink = $(this).attr("rel");
                options.currentPage = clickedLink;
                if (options.holder) {
                    $(this).parent("li").parent("ul").parent(options.holder).find("li.currentPage").removeClass("currentPage");
                    $(this).parent("li").parent("ul").parent(options.holder).find("a[rel='" + clickedLink + "']").parent("li").addClass("currentPage");
                }
                else {
                    //remove current current (!) page
                    $(this).parent("li").parent("ul").parent(".simplePagerContainer").find("li.currentPage").removeClass("currentPage");
                    //Add current page highlighting
                    $(this).parent("li").parent("ul").parent(".simplePagerContainer").find("a[rel='" + clickedLink + "']").parent("li").addClass("currentPage");
                }
                //hide and show relevant links
                selector.children().hide();
                selector.find(".simplePagerPage" + clickedLink).show();
                $('.pull-left').empty();
                $('.pull-left').html('Page ' + options.currentPage + ' of ' + pageCounter + '');
                return false;
            });
        });
    }
})(jQuery);


jQuery(document).ready(function($) {
    $('.view-gallery').show();
    // Show gallery in popup fancybox
    $(".view-gallery").click(function(e) {
        $.fancybox.open({
            src: this.href,
            type: 'ajax',
            opts: {
              infobar : false,
              buttons : false,

              // What buttons should appear in the toolbar
              slideShow  : false,
              fullScreen : false,
              thumbs     : false
            }
        });
        e.preventDefault();
    });
});
$(document).ready(function() {
    $("ul.data").quickPager({
        pageSize: "10"
    });
});
        
/*CUSTOM SCRIPT(include/js/custom-script.js)*/