<div class="sub-menu">
    <ul>
        <li><a href="/admin/customer-listings.php?id=<?php echo $BID ?>">My Listings</a></li>
        <li><a href="/admin/login-details.php?id=<?php echo $BID ?>">Login Detail</a></li>
        <li><a href="/admin/customer-advertisment-myaccount.php?id=<?php echo $BID ?>">My Campaigns</a></li>
        <?php if (in_array('billing-history', $_SESSION['USER_PERMISSIONS'])) { ?>
            <li><a href="/admin/customer-credit-card-details.php?id=<?php echo $BID ?>">Credit Card Details</a></li>
        <?php } if (in_array('customers', $_SESSION['USER_PERMISSIONS'])) { ?>
            <li><a href="/admin/customer-notes.php?id=<?php echo $BID ?>">Notes</a></li>
        <?php } ?>
    </ul>
</div>