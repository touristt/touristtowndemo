<?php
$filename = basename($_SERVER["SCRIPT_FILENAME"], '.php');
            if ($filename == 'coupon-listing-contact-details') {
                $activeclass1 = 'active-class';
            } else {
                $activeclass1 = 'not-active';
            }
            if ($filename == 'manage-coupon') {
                $activeclass2 = 'active-class';
            } else {
                $activeclass2 = 'not-active';
            }
            if ($filename == 'coupon-listing-mapping') {
                $activeclass3 = 'active-class';
            } else {
                $activeclass3 = 'not-active';
            }
            if ($filename == 'coupon-hour') {
                $activeclass4 = 'active-class';
            } else {
                $activeclass4 = 'not-active';
            }
            if ($filename == 'coupon-social-media') {
                $activeclass5 = 'active-class';
            } else {
                $activeclass5 = 'not-active';
            }
?>

<div class="left-nav-coupon">
    <ul>
        <li class="<?php echo $activeclass2 ?>"><a href="manage-coupon.php?bl_id=<?php echo $BL_ID ?>">Manage Coupons</a></li>
        <li class="<?php echo $activeclass1 ?>"><a href="coupon-listing-contact-details.php?bl_id=<?php echo $BL_ID ?>">Contact Details</a></li>
        <li class="<?php echo $activeclass3 ?>"><a href="coupon-listing-mapping.php?bl_id=<?php echo $BL_ID ?>">Mapping</a></li>
        <li class="<?php echo $activeclass4 ?>"><a href="coupon-hour.php?bl_id=<?php echo $BL_ID ?>">Hours</a></li>
        <li class="<?php echo $activeclass5 ?>"><a href="coupon-social-media.php?bl_id=<?php echo $BL_ID ?>">Social Media</a></li>
        <li class="not-active"><a href="index.php">Home</a></li>
        <li class="not-active"><a href=<?php echo "http://" . COUPON_DOMAIN . BUSINESS_DIR . "?logop=logout" ?>>Log Out</a></li>
    </ul>
</div>
