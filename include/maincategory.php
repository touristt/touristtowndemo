<?php
categoryTrack($REGION['R_ID'], $_SESSION['CATEGORY'], 1, 0);

$sql = "SELECT * FROM tbl_Category 
				LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
				WHERE C_ID = '" . encode_strings($_SESSION['CATEGORY'], $db) . "' ORDER BY C_Name LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeCat = mysql_fetch_assoc($result);
$VIDEOID = $activeCat['RC_Video'];
$video_link = explode("=", $VIDEOID);
$sql_category_slider = "SELECT * FROM tbl_Region_Category_Photos
            WHERE RCP_C_ID = '" . encode_strings($activeCat['C_ID'], $db) . "' AND RCP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' order by RCP_Order";
$result_category_slider = mysql_query($sql_category_slider, $db) or die("Invalid query: $sql_category_slider -- " . mysql_error());
?>

<div class="main-content">
    <div class="home">
        <script src="http://www.youtube.com/player_api"></script>
        <input  type="hidden" id="video-link" value="<?= $video_link[1] ?>">
        <div id="headingParent" style="padding:0;margin:0;<?= $activeCat['RC_Video'] ? 'cursor:pointer;' : ''; ?>">
            <div id="player" style="display:none"></div>
            <!--                <div id="videoContent" class="videoContent" style="display:none; height:400px;width:930px;"></div>-->
            <?php
            // Select Random advertisments
            $adQueryCH = "SELECT * FROM tbl_Advertisement
                    LEFT JOIN tbl_Business_Listing ON A_BL_ID = BL_ID
                    WHERE A_C_ID = '" . encode_strings($_SESSION['CATEGORY'], $db) . "' AND A_AT_ID = 1 AND A_Status = 3 AND A_Website = '" . encode_strings($REGION['R_ID'], $db) . "' 
                    AND A_Is_Deleted = 0
                    GROUP BY A_ID
                    ORDER BY RAND() LIMIT 4";
            $adResultCH = mysql_query($adQueryCH) or die("Invalid query: $adQueryCH -- " . mysql_error());
            $num_of_adv = mysql_num_rows($adResultCH);
            ?>
            <div class=center>
                <span id=prev></span>
                <span id=next></span>
            </div>
            <div class="sliderCycle" data-cycle-fx=scrollLeft data-cycle-timeout=0 data-cycle-prev="#prev" data-cycle-next="#next" style="width: 930px !important">
                <?php
                $first_img = true;
                if ($Firstslider['RC_Image'] != "") {
                    ?>
                    <img  id="subsection-header"  class="subsection-header temp-width-header" src="<?= IMG_LOC_REL . $Firstslider['RC_Image'] ?>" />
                    <?php
                } if ($num_of_adv > 0) {
                    $active_category_slider = mysql_fetch_assoc($result_category_slider);
                    if ((isset($active_category_slider['RCP_Image']) && $active_category_slider['RCP_Image'] != '')) {
                        if ($Firstslider['RC_Image'] == "") {
                            if ($first_img) {
                                $image_slider_style = '';
                                $first_img = false;
                            } else {
                                $image_slider_style = 'display:none;';
                            }
                        } else {
                            $image_slider_style = 'display:none;';
                        }
                        if ($Firstslider['RC_Image'] == "") {
                            ?>
                            <img  id="subsection-header"  class="subsection-header temp-width-header" style="<?= $image_slider_style ?>" src="<?= IMG_LOC_REL . $active_category_slider['RCP_Image'] ?>" alt="<?= $activeCat['RC_Alt'] ?>" longdesc="<?= $activeCat['RC_Alt'] ?>" />
                            <?php
                        }
                    }
                } else {
                    while ($active_category_slider = mysql_fetch_assoc($result_category_slider)) {
                        if ((isset($active_category_slider['RCP_Image']) && $active_category_slider['RCP_Image'] != '')) {
                            if ($Firstslider['RC_Image'] == "") {
                                if ($first_img) {
                                    $image_slider_style = '';
                                    $first_img = false;
                                } else {
                                    $image_slider_style = 'display:none;';
                                }
                            } else {
                                $image_slider_style = 'display:none;';
                            }
                            ?>
                            <img  id="subsection-header"  class="subsection-header temp-width-header" style="width:100%;<?= $image_slider_style ?>" src="<?= IMG_LOC_REL . $active_category_slider['RCP_Image'] ?>" alt="<?= $activeCat['RC_Alt'] ?>" longdesc="<?= $activeCat['RC_Alt'] ?>" />
                            <?php
                        }
                    }
                }
                if ($num_of_adv > 0) {
                    while ($adRowCH = mysql_fetch_array($adResultCH)) {
                        if ($adRowCH['A_Third_Party'] != "") {
                            echo '<a style="width: 930px !important; display:none" onclick = "advertClicks(' . $adRowCH['A_ID'] . ')" target="_blank" href="' . $adRowCH['A_Third_Party'] . '"><img  style="width: 930px !important;" src="' . IMG_LOC_REL . $adRowCH['A_Approved_Logo'] . '"></a>';
                        } else {
                            echo '<a style="width: 930px !important; display:none" onclick = "advertClicks(' . $adRowCH['A_ID'] . ')" href="/profile/' . $adRowCH['BL_Name_SEO'] . '/' . $adRowCH['BL_ID'] . '/"><img style="width: 930px !important;" src="' . IMG_LOC_REL . $adRowCH['A_Approved_Logo'] . '"></a>';
                        }
                        $AS_A_ID_CH = $adRowCH['A_ID'];
                        ADVERT_IMPRESSION($AS_A_ID_CH);
                    }
                }
                ?>
            </div>
            <?php
            if ($video_link[1] != '') {
                ?>
                <div class="play-button">
                    <a onclick="add_video()">Play Video</a>
                </div>
            <?php } ?>
        </div>
        <div class="font-size-cat main-content-heading" style="float: left;margin-bottom: 10px;"><?= $activeCat['C_Name'] ?> in <?= $REGION['R_Name'] ?></div>
        <div class="cat-description-space" >
            <?PHP if ($activeCat['RC_Description']) { ?>
                <?PHP echo convert(str_replace('<p>', '', str_replace('</p>', '', $activeCat['RC_Description']))); ?>
            <?PHP } ?>
        </div>
    </div>
    <div class="eat" style="clear: left;">

        <?PHP
        $LiveEnt = true;
        if ($_REQUEST['submitEvent']) {
            $LiveEnt = false;
            require 'submitEventRules.php';
        } elseif ($_REQUEST['submitEventAgree']) {
            $LiveEnt = false;
            require 'submitEvent.php';
        } else {
            ?>

            <ul class="thumbnails tt-thumbnails">
                <?PHP
                $sql = "SELECT * FROM tbl_Category 
			LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
			WHERE C_Parent = '" . encode_strings($_SESSION['CATEGORY'], $db) . "' and RC_Status=0 AND RC_R_ID > 0
			ORDER BY C_Order";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                $countThumb = 0;
                while ($row = mysql_fetch_assoc($result)) {
                    $countThumb++;
                    //check if we have specific thumbnail for the category for the region.
                    if ($row['RC_Thumbnail'] != '') {
                        $icon = $row['RC_Thumbnail'];
                    } else {
                        $icon = $row['C_Icon'];
                    }
                    if ($countThumb == 4) {
                        ?>
                        <li class="last-child-home">
                            <a href="/<?= $activeCat['C_Name_SEO'] ?>/<?= $row['C_Name_SEO'] ?>/" class="thumbnail">
                                <img src="<?= IMG_LOC_REL . $icon ?>" alt="<?= $row['C_Name'] ?>" longdesc="<?= $row['C_Name'] ?>">
                                <p></p>
                                <span><?= $row['C_Name'] ?></span>
                            </a>
                        </li>
                        <?php
                        $countThumb = 0;
                    } else {
                        ?>
                        <li class="">
                            <a href="/<?= $activeCat['C_Name_SEO'] ?>/<?= $row['C_Name_SEO'] ?>/" class="thumbnail">
                                <img src="<?= IMG_LOC_REL . $icon ?>" alt="<?= $row['C_Name'] ?>" longdesc="<?= $row['C_Name'] ?>">
                                <p></p>
                                <span><?= $row['C_Name'] ?></span>
                            </a>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>
        <?php } ?>

        <?PHP
//            if ($_SESSION['CATEGORY'] == 6) { // menu
//                if ($REGION['R_Menu'] == 1) {
//                    require 'include/menuFeatures.inc.php';
//                }
//                
        ?>
        <!--            <div style=" clear: both; height:25px">&nbsp;</div> -->
        <?PHP
//                if ($REGION['R_At_A_Glance'] == 1) {
//                    require 'include/memeberEvents.inc.php';
//                }
//            } else
        if (($_SESSION['CATEGORY'] == 5 || $_SESSION['CATEGORY'] == 8 || $_SESSION['CATEGORY'] == 6) && $LiveEnt) { //things to do
            if ($REGION['R_At_A_Glance'] == 1) {
                require 'include/memeberEvents.inc.php';
            }
        }
        ?>
    </div><!-- .content-right -->
</div><!-- .content-right-outer -->

<?php
if ($_SESSION['CATEGORY'] != 8) {
    ?>
    <!--Top ten ranked listings-->
    <div class="main-content">
        <?php
        $c_parent = $_SESSION['CATEGORY'];
        $sql_top_ten = "Select * from tbl_Region_Category 
                    where RC_R_ID='" . encode_strings($REGION['R_ID'], $db) . "'
                    And RC_C_ID='$c_parent'";
        $res_top_ten = mysql_query($sql_top_ten);
        $top_ten = mysql_fetch_assoc($res_top_ten);
        if ($top_ten['RC_Show_Hide'] == 1) {
            ?>
            <div class="content-right padding-none">
                <div class="wide-list destinations">
                    <div class="main-content-heading">
                        Top 10 Ranked <?= $activeCat['C_Name'] ?> for <?php echo $REGION['R_Name']; ?>
                    </div>
                    <?php
                    $c_parent = $_SESSION['CATEGORY'];
                    if ($REGION['R_Parent'] == 0) {
                        $sql_top = "SELECT * FROM tbl_Business 
                LEFT JOIN tbl_Business_Listing ON BL_B_ID = B_ID
                INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID=BL_ID
                INNER JOIN tbl_Region_Category ON RC_C_ID = BLC_M_C_ID AND RC_R_ID='" . encode_strings($REGION['R_ID'], $db) . "'
                AND RC_Show_Hide =1
                LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
                LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                WHERE BLC_M_C_ID = '$c_parent' 
                AND BL_Top_Ten_hide=1
                AND BLCR_BLC_R_ID IN (SELECT R_ID from tbl_Region LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID where RM_Parent = '" . encode_strings($REGION['R_ID'], $db) . "')
                GROUP BY BL_Listing_Title
                ORDER BY BL_Points DESC, LT_Order DESC, BL_Listing_Title
                LIMIT 10";
                    } else {
                        $sql_top = "SELECT * FROM tbl_Business 
                LEFT JOIN tbl_Business_Listing ON BL_B_ID = B_ID
                INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID=BL_ID
                INNER JOIN tbl_Region_Category ON RC_C_ID = BLC_M_C_ID AND RC_R_ID='" . encode_strings($REGION['R_ID'], $db) . "'
                AND RC_Show_Hide =1
                LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
                LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                WHERE BLC_M_C_ID = '$c_parent'
                AND BL_Top_Ten_hide=1
                AND BLCR_BLC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
                GROUP BY BL_Listing_Title
                ORDER BY BL_Points DESC, LT_Order DESC, BL_Listing_Title
                LIMIT 10";
                    }
                    $res_top = mysql_query($sql_top);
                    echo '<div class="ranked-business">';
                    echo '<ul class="thumbnails data">';
                    while ($top = mysql_fetch_assoc($res_top)) {
                        listingTrack($top['BL_ID'], 1, 0, 0, $REGION['R_ID']);
                        ?> 
                        <li class="pager">
                            <div class="thumbnail listings-list">
                                <?php if ($top['LT_ID'] <> 1) { ?>
                                    <a href="/profile/<?= $top['BL_Name_SEO'] ?>/<?= $top['BL_ID'] ?>/"> 
                                    <?PHP } ?>
                                    <img src="<?= ($top['LT_ID'] == 1 || $top['BL_Photo'] == '') ? '/images/Listing-NoPhoto.jpg' : (IMG_LOC_REL . $top['BL_Photo']) ?>" alt="<?= $top['BL_Photo_Alt'] ?>" longdesc="<?= $top['BL_Listing_Title'] ?>"/>
                                    <?php if ($top['LT_ID'] <> 1) { ?>
                                    </a>
                                <?PHP } ?>
                                <div class="copy thumbnails-top-10">
                                    <h5>
                                        <?php
                                        if ($top['LT_ID'] <> 1) {
                                            echo '<a href="/profile/' . $top['BL_Name_SEO'] . '/' . $top['BL_ID'] . '/">';
                                        }
                                        echo $top['BL_Listing_Title'];
                                        if ($top['LT_ID'] <> 1) {
                                            echo '</a>';
                                        }
                                        ?>
                                    </h5>
                                    <?PHP
                                    if ($top['LT_ID'] == 1) { // !FREE
                                        ?>
                                        <p style="margin-top: 10px;"><?= $top['BL_Street'] ?>, <?= $top['BL_Town'] ?></p>
                                        <?PHP
                                    } else {
//                                        $text = limit_text($top['BL_Description'], 130);
                                        ?>
                                        <p style="margin-top: 10px;"><?php echo $top['BL_Short_Description'] . "..."; //REPLACE                                                                                                                                                                                                                                                                                                ?>
                                            <?PHP
                                            if ($top['BL_Short_Description'] <> $top['BL_Description']) {
                                                ?>
                                                <a href="/profile/<?= $top['BL_Name_SEO'] ?>/?cid=<?= $top['BL_C_ID'] ?>">More Details</a>
                                                <?PHP
                                            }
                                            ?>
                                        </p>
                                        <?PHP
                                    }
                                    ?>
                                    <p>
                                        <?PHP
                                        if ($top['BL_Phone'] != '' && $top['BL_Listing_Type'] != 1) {
                                            print 'Phone. ' . $top['BL_Phone'] . '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp';
                                        }
                                        if ($top['BL_Email'] != '' && $top['BL_Listing_Type'] != 1) {
                                            echo '<a href="mailto:' . $top['BL_Email'] . '">Email Now</a>';
                                        }
                                        ?>
                                    </p>
                                </div>
                                <div class="tt-actions margin-action">
                                    <?PHP
                                    if ($top['LT_ID'] <> 1) { // !FREE
                                        ?>
                                        <ul class="unstyled inline" style="margin-top:8px;">
                                            <?PHP
                                            if ($top['LT_ID'] >= 2) { // Silver or Gold
                                                $sql_remove_trip = "SELECT * FROM  tbl_Session_Tripplanner where ST_Session_ID = '" . encode_strings($_COOKIE["tripplanner"], $db) . "'
                                                   AND ST_Bus_ID = " . encode_strings($top['B_ID'], $db) . "
                                                   AND ST_BL_ID = " . encode_strings($top['BL_ID'], $db) . "
                                                   AND ST_Type_ID = " . encode_strings($top['BL_C_ID'], $db) . "";
                                                $result_remove_trip = mysql_query($sql_remove_trip);
                                                $count_remove_trip = mysql_num_rows($result_remove_trip);
                                                if ($count_remove_trip > 0) {
                                                    ?>
                                                    <li>
                                                        <div class="remove-item-tripplanner">
                                                            <a href="#" style="line-height: 25px;" class="planner-link" id="<?php echo $top["BL_C_ID"] . "_" . $top["B_ID"] . "_" . $top["BL_ID"] ?>"><div class="remove-minus-sign"><img src="<?= IMG_LOC_REL ?>minus.png"></div><div class="margin-top">remove from my trip</div></a>
                                                        </div>
                                                    </li>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <li>
                                                        <div class="add-item-tripplanner"><a href="#" class="planner-link" id="<?php echo $top['BL_C_ID'] . '_' . $top['B_ID'] . "_" . $top["BL_ID"] ?>"><div class="plus-sign"><img src="<?= IMG_LOC_REL ?>plus.png"></div><div class="margin-top">&nbsp;add to my trip</div></a></div>
                                                    </li>
                                                    <?PHP
                                                }
                                            }
                                            if ($top['LT_ID'] >= 4) { // Gold
                                                ?>
                                                <li>
                                                    <?PHP if ($top['BL_Lat'] && $top['BL_Long']) { ?>
                                                        <a class="map-link various fancybox.iframe"  href="http://maps.google.ca/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<?= $top['BL_Lat'] ?>,<?= $top['BL_Long'] ?>+(<?= str_replace('&', ' ', $top['BL_Listing_Title']) ?>)&amp;aq=&amp;sll=<?= $top['BL_Lat'] ?>,<?= $top['BL_Long'] ?>&amp;sspn=0.027224,0.077162&amp;ie=UTF8&amp;t=m&amp;spn=0.022178,0.025663&amp;z=14&amp;iwloc=A&amp;output=embed"><span class="showonmap-icon"></span><div class="margin-top">&nbsp;show on map</div></a>
                                                    <?PHP } elseif ($top['BL_Street']) { ?>
                                                        <a class="map-link various fancybox.iframe"  href="http://maps.google.ca/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<?= $top['BL_Street'] ?>,<?= $top['BL_Town'] ?>+<?= $top['BL_Province'] ?> <?= $top['BL_PostalCode'] ?>&amp;aq=&amp;sspn=0.027224,0.077162&amp;ie=UTF8&amp;t=m&amp;spn=0.022178,0.025663&amp;z=14&amp;iwloc=A&amp;output=embed"><span class="showonmap-icon"></span><div class="margin-top">&nbsp;show on map</div></a>
                                                    <?PHP } ?>
                                                </li>
                                                <?PHP
                                            }
                                            ?>
                                            <div class="aminities-li">
                                                <li class="amminities-inside">
                                                    <?PHP
                                                    $sql = "SELECT *  
				FROM tbl_Business_Listing_Ammenity 
				LEFT JOIN tbl_Ammenity_Icons ON AI_ID = BLA_BA_ID 
				WHERE BLA_BL_ID = '" . encode_strings($top['BL_ID'], $db) . "' AND AI_Image <> ''";
                                                    $resultAI = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                                    $iCount = mysql_num_rows($resultAI);
                                                    $i = 0;
                                                    if ($top['BL_ChamberMember']) {
                                                        $i = 1;
                                                        ?>
                                                        <img style="height: 25px; width: 25px;" src="/images/cc-icon.gif" alt="Chamber of Commerce" width="25" height="25" />
                                                        <?PHP
                                                    }
                                                    while (($rowAI = mysql_fetch_assoc($resultAI)) && $i < 8) {
                                                        ?><img style="height: 25px; width: 25px;" src="<?= IMG_LOC_REL ?>ammenity-icons/<?= $rowAI['AI_Image'] ?>" alt="<?= $rowAI['AI_Name'] ?>" width="25" height="25" /><?PHP
                                                        $i++;
                                                    }
                                                    ?>
                                                </li>
                                                <?PHP
                                                if ($iCount > 8) {
                                                    ?>
                                                    <li class="margin-top amminities-inside-see">
                                                        <a href="/profile/<?= $top['BL_Name_SEO'] ?>/<?= $top['BL_ID'] ?>/" style = "color: #0D78C6">see more...</a>
                                                    </li>
                                                    <?PHP
                                                }
                                                if ($i == 0 && $top['LT_ID'] >= 4) {
                                                    echo '<img style="border: 0px; box-shadow:0px;height: 25px; width: 25px;" src="/images/spacer.gif">';
                                                }
                                                ?>
                                            </div>
                                        </ul>
                                        <?PHP
                                    }
                                    ?>
                                </div>
                            </div>
                        <?php }
                        ?>
                    </li>
                    <?php
                    echo '</ul>';
                    echo '</div>';
                    ?>
                </div>
            </div>
        <?php } ?>
        <div class="right-advertise padding-top">
            <?php include 'advertise.php'; ?>
        </div>  
    </div>
    <?php
}
?>