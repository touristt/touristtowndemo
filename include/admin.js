// JavaScript Document
$(document).ready(function () {
    showPage(1); // pagination
    //Ajax Loader
    $(document).ajaxStart(function () {
        $('.ajaxSpinnerImage').show();
    })
            .ajaxStop(function () {
                $('.ajaxSpinnerImage').hide();
            });

    $("form").submit(function () {
        var check_submit = 0;
        /// Checked textbox is required
        $('form').find('input').each(function () {
            if ($(this).prop('required')) {
                if ($(this).val() == '') {
                    check_submit = 1;
                }
            }
        });
        /// Checked select option is required
        $('form').find('select').each(function () {
            if ($(this).prop('required')) {
                if ($(this).val() == '') {
                    check_submit = 1;
                }
            }
        });

        if ($(".community_select").length > 0) {
            var checked_box = $('input:checkbox:checked.community_select').map(function () {
                return this.value;
            }).get();
            if (checked_box == "") {
                check_submit = 1;
            }
        }
        if ($(".image_bank").length > 0) {
            if ($("input[name=image_bank]").val() == '') {
                check_submit = 1;
            }
        }
        /// mapping Lat and Long
        var latitudeVal = $("#latitude").val();
        var longitudeVal = $("#longitude").val(); 
        if (latitudeVal != '' && longitudeVal == '') { 
           check_submit = 1;
           return false;
        }
        if (latitudeVal == '' && longitudeVal != '') {
           check_submit = 1;
           return false;
        }
        if (check_submit == 0) {
            $('.ajaxSpinnerImage').show();
            setTimeout(function () {
                $('.ajaxSpinnerImage').hide();
            }, 2000);
        }
    });
    $(".datepicker").datepicker({
        firstDay: 1,
        dateFormat: 'yy-mm-dd'
    });

    $(".previous-date-not-allowed").datepicker({
        minDate: 0,
        firstDay: 1,
        dateFormat: 'yy-mm-dd',
        prevText: "",
        nextText: ""
    });
    $(".phone_number").mask("999-999-9999");
    $("#phone").mask("999-999-9999");
    $(".phone_toll").mask("9-999-999-9999");
    $("#entertainment_act_desc").keyup(function () {
        update_chars_left(300, $('#entertainment_act_desc')[0], $('#character_count'));
    });
    $("input#latitude").blur(function (event) {
        var regex = /^-?([0-8]?[0-9]|90)\.[0-9]{1,6}$/;
        var lat = $(this).val();
        if (lat != '' && !regex.test(lat)) {
            alert("Invalid Latitude");
            $(this).val('');
        }
    });
    $("input#longitude").blur(function (event) {
        var regex = /^-?((1?[0-7]?|[0-9]?)[0-9]|180)\.[0-9]{1,6}$/;
        var lon = $(this).val();
        if (lon != '' && !regex.test(lon)) {
            alert("Invalid Longitude");
            $(this).val('');
        }
    });
    //accommodation
    $('.cap, .beds-btn, .cap-btn, .capacity-txt, .closed-checkbox').click(function (event) {
        event.stopPropagation(); // this is the magic
    });
    //Add Inactive Class To All Accordion Headers
    $('.accordion-header').toggleClass('inactive-header');
    $('.accordion li[defaultactive="true"]').index();
    //Set The Accordion Content Width
    var contentwidth = $('.accordion-header').width();
    $('.accordion-content').css({
        'width': contentwidth
    });
    // The Accordion Effect
    $('.accordion-header').click(function () {
        if ($(this).is('.inactive-header')) {
            $('.active-header').toggleClass('active-header').toggleClass('inactive-header').next().slideToggle().toggleClass('open-content');
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        } else {
            $(this).toggleClass('active-header').toggleClass('inactive-header');
            $(this).next().slideToggle().toggleClass('open-content');
        }
    });

    $('.inactive-header.first').toggleClass('inactive-header first').toggleClass('active-header first').next().slideToggle().toggleClass('open-content');
    //    $('.active-header').toggleClass('active-header').toggleClass('inactive-header');
    $('.active-header.first').toggleClass('.active-header first').toggleClass('.active-header');
    return false;
    // to remove 0 from field
    $('.beds-txt').focus(function () {
        if ($(this).val() == 0) {
            $(this).val('');
        }
    });
    $('.timepicker').timepicker({});

});
function filter_region()
{
    $('#region-filter').submit();
}
function update_chars_left(max_len, target_input, display_element) {
    var text_len = target_input.value.length;
    if (text_len >= max_len) {
        target_input.value = target_input.value.substring(0, max_len); // truncate
        display_element.html("0");
    } else {
        display_element.html((max_len - text_len) + " characters left");
    }
}
function limitTextArea(myID, myCount)
{
    if ($("#" + myID).val().length > myCount) {
        $("#" + myID).val($("#" + myID).val().substr(0, myCount));
    }
}
function validatePassword(password, optional, password1)
{
    if (optional == true && password.length == 0 && password1.length == 0) {
        return true;
    } else {
        if (password.length < 6) {
            alert("Password too short! At least 6 chacters.");
            return false;
        }
        if (!password.match("[0-9]")) {
            alert("Password must contain at least one digit!");
            return false;
        }
        if (!password.match("[A-Z]")) {
            alert("Password must contain at least one uppercase letter!");
            return false;
        }
        if (!password.match("[a-z]")) {
            alert("Password must contain at least one lowercase letter!");
            return false;
        }
        if (password != password1) {
            alert("Password does not match!");
            return false;
        }
    }
}
function isClosed(myVar, myValue) {
    if (myValue == '00:00:01' || myValue == "00:00:02" || myValue == "00:00:03") {
        $("#" + myVar).hide();
        $("#" + myVar + " option:selected").prop("selected", false);
    } else {
        $("#" + myVar).show();
    }
}
function deleteProductType(myValue) {
    $('.rowPT' + myValue).remove();
    return false;
}
function deleteProductImage(myValue, myID, rowID) {
    if (confirm('Are you sure?')) {
        $('#file' + rowID).remove();
        $.post('/include/removeFile.php', {
            id: myID,
            type: 'products',
            file: myValue
        });
    }
    return false;
}
function deleteContentImage(myValue, myID, rowID) {
    if (confirm('Are you sure?')) {
        $('#file' + rowID).remove();
        $.post('/include/removeFile.php', {
            id: myID,
            type: 'fb',
            file: myValue
        });
    }
    return false;
}
function changeFavouriteRecipeVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        alert('Yes');
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'FavouriteRecipe',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'FavouriteRecipe',
            status: 0
        });
    }
    return false;
}
function changeProductVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'product',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'product',
            status: 0
        });
    }
    return false;
}
function changeLearningVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'learn',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'learn',
            status: 0
        });
    }
    return false;
}
function changeGalleryVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'gallery',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'gallery',
            status: 0
        });
    }
    return false;
}
function changeShowroomVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'showroom',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'showroom',
            status: 0
        });
    }
    return false;
}
function changeTestimonialVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'testimonial',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'testimonial',
            status: 0
        });
    }
    return false;
}
function deleteServiceImage(myValue, myID, rowID) {
    if (confirm('Are you sure?')) {
        $('#file' + rowID).remove();
        $.post('/include/removeFile.php', {
            id: myID,
            type: 'services',
            file: myValue
        });
    }
    return false;
}
function changeServiceVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'service',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'service',
            status: 0
        });
    }
    return false;
}
function deleteCustomImage(myValue, myID, rowID) {
    if (confirm('Are you sure?')) {
        $('#file' + rowID).remove();
        $.post('/include/removeFile.php', {
            id: myID,
            type: 'custom',
            file: myValue
        });
    }
    return false;
}
function deleteGroupHPImage(myValue, myID, rowID) {
    if (confirm('Are you sure?')) {
        $('#file' + rowID).remove();
        $.post('/include/removeFile.php', {
            id: myID,
            type: 'grouphp',
            file: myValue
        });
    }
    return false;
}
function changeCustomVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'custom',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'custom',
            status: 0
        });
    }
    return false;
}
function changeBrandVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'brand',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'brand',
            status: 0
        });
    }
    return false;
}
function changeInstallProcedureVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'installpro',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'installpro',
            status: 0
        });
    }
    return false;
}
function changeServiceProcedureVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'servicepro',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'servicepro',
            status: 0
        });
    }
    return false;
}
function changeCollateralVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'collateral',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'collateral',
            status: 0
        });
    }
    return false;
}
function changeCollateralVendorVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'collateralVendor',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'collateralVendor',
            status: 0
        });
    }
    return false;
}
function changeCollateralSizeVisible(myID, pID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            cid: pID,
            type: 'collateralSize',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            cid: pID,
            type: 'collateralSize',
            status: 0
        });
    }
    return false;
}
function changeflyerdate(myID, mydate, fID) {
    $.post('/include/changeDate.php', {
        id: myID,
        update: mydate,
        updateid: fID
    });
    location.reload();
    return false;
}
function MM_preloadImages() { //v3.0
    var d = document;
    if (d.images) {
        if (!d.MM_p)
            d.MM_p = new Array();
        var i, j = d.MM_p.length, a = MM_preloadImages.arguments;
        for (i = 0; i < a.length; i++)
            if (a[i].indexOf("#") != 0) {
                d.MM_p[j] = new Image;
                d.MM_p[j++].src = a[i];
            }
    }
}
function MM_swapImgRestore() { //v3.0
    var i, x, a = document.MM_sr;
    for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++)
        x.src = x.oSrc;
}
function MM_findObj(n, d) { //v4.01
    var p, i, x;
    if (!d)
        d = document;
    if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
        d = parent.frames[n.substring(p + 1)].document;
        n = n.substring(0, p);
    }
    if (!(x = d[n]) && d.all)
        x = d.all[n];
    for (i = 0; !x && i < d.forms.length; i++)
        x = d.forms[i][n];
    for (i = 0; !x && d.layers && i < d.layers.length; i++)
        x = MM_findObj(n, d.layers[i].document);
    if (!x && d.getElementById)
        x = d.getElementById(n);
    return x;
}
function MM_swapImage() { //v3.0
    var i, j = 0, x, a = MM_swapImage.arguments;
    document.MM_sr = new Array;
    for (i = 0; i < (a.length - 2); i += 3)
        if ((x = MM_findObj(a[i])) != null) {
            document.MM_sr[j++] = x;
            if (!x.oSrc)
                x.oSrc = x.src;
            x.src = a[i + 2];
        }
}
function viewVideo(data) {
    var recipe = window.open('', 'VideoWindow', 'width=640,height=480');
    var html = '<html><head><title>View Video</title></head><body><div id="video">' + data + '</div></body></html>';
    recipe.document.open();
    recipe.document.write(html);
    recipe.document.close();
    return false;
}
/* function for sending ajax call for deleting photos	*/
function deletephoto(tablename, columnname, matchingcolumns, businesslistid)
{
    if (confirm("Are you sure you want to delete?"))
    {
        $.ajax({
            type: "POST",
            url: 'deletephoto.php',
            data: "tablename=" + tablename + "&fields=" + columnname + "&blid=" + businesslistid + "&matchingcolumn=" + matchingcolumns,
            success: function (response) {
                $('#' + columnname).remove();
                console.log(response);
            }
        });
    }
    return false;
}
//accommodation
pageSize = 20;
function showPage(page) {
    $(".accordion-section").hide();
    $(".accordion-section").each(function (n) {
        if (n >= pageSize * (page - 1) && n < pageSize * page)
            $(this).show('slow');
    });
}
//showPage(1);
$(document).ready(function () {
    $("#pagin li span").click(function () {
        $("#pagin li span").removeClass("current");
        $("#pagin li span").removeAttr("id");
        $(this).addClass("current");
        $(this).attr("id", "current");
        showPage(parseInt($(this).text()))
    });
});
/*
 * save all availability
 */
function save_all_response_quantity() {
    var acc_ids = document.getElementById('acc_ids').innerHTML;
    if (document.getElementById('current')) {
        var page = document.getElementById('current').innerHTML;
    } else {
        var page = 1;
    }
    var forms_data = [];
    var acc_ids_array = acc_ids.split(',')
    var current_index = (page - 1) * 10;
    var items = 0;
    for (var acc_id = current_index; acc_id < (acc_ids_array.length - 1); acc_id++) {
        var obj = [];
        var block = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).block_id.value;
        var accom_id = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).accom_id.value;
        /*                 <!--beds-->*/
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).bed_1) {
            var bed_1 = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).bed_1.value;
        } else {
            bed_1 = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).bed_2) {
            var bed_2 = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).bed_2.value;
        } else {
            bed_2 = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).bed_3) {
            var bed_3 = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).bed_3.value;
        } else {
            bed_3 = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).bed_4) {
            var bed_4 = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).bed_4.value;
        } else {
            bed_4 = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).bed_5) {
            var bed_5 = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).bed_5.value;
        } else {
            bed_5 = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).bed_6) {
            var bed_6 = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).bed_6.value;
        } else {
            bed_6 = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).bed_7) {
            var bed_7 = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).bed_7.value;
        } else {
            bed_7 = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).bed_8) {
            var bed_8 = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).bed_8.value;
        } else {
            bed_8 = 0;
        }
        /*days*/
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).sun) {
            var sun = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).sun.value;
        } else {
            sun = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).mon) {
            var mon = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).mon.value;
        } else {
            mon = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).tue) {
            var tue = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).tue.value;
        } else {
            tue = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).wed) {
            var wed = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).wed.value;
        } else {
            wed = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).thur) {
            var thur = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).thur.value;
        } else {
            thur = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).fri) {
            var fri = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).fri.value;
        } else {
            fri = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).sat) {
            var sat = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).sat.value;
        } else {
            sat = 0;
        }
        obj = {
            block_id: block,
            accom_id: accom_id,
            bed_1: bed_1,
            bed_2: bed_2,
            bed_3: bed_3,
            bed_4: bed_4,
            bed_5: bed_5,
            bed_6: bed_6,
            bed_7: bed_7,
            bed_8: bed_8,
            sun: sun,
            mon: mon,
            tue: tue,
            wed: wed,
            thur: thur,
            fri: fri,
            sat: sat
        }
        forms_data.push(obj);
        items++;
        if (items == 20) {
            break;
        } else {
            continue;
        }
    }
    $.ajax({
        /*this is the php file that processes the data and send mail */
        url: "save_all_response_quantity.php", /*GET method is used */
        type: "POST", /*pass the data	*/
        data: {
            forms_data: forms_data
        }, /*Do not cache the page*/
        cache: false, /*success*/
        success: function (html) {
            $("#dialog").html(html);
            $("#dialog").dialog({
                open: function (event, ui) {
                    var obj = $(this);
                    window.setTimeout(function () {
                        obj.dialog('close');
                    },
                            3000);
                    window.location.reload();
                }
            });
        }
    });
    return false;
}
function block_capacity_update(form, counter)
{
    var data = $(form).serialize();
    $.ajax({
        /*this is the php file that processes the data and send mail */
        url: "save_response_quantity.php", /*GET method is used */
        type: "GET", /*pass the data	*/
        data: data, /*Do not cache the page */
        cache: false, /*success*/
        success: function (html) {
            $("#dialog").html(html);
            $("#cap-percent-" + counter).html(html + "%");
            $("#dialog").dialog({
                open: function (event, ui) {
                    var obj = $(this);
                    window.setTimeout(function () {
                        obj.dialog('close');
                    },
                            3000);
                }
            });
        }
    });
    return false;
}
function block_capacity_update_notes(form) {
    var data = $(form).serialize();
    $.ajax({/*this is the php file that processes the data and send mail*/
        url: "save_accomm_notes.php", /*GET method is used */
        type: "GET", /*pass the data*/
        data: data, /*Do not cache the page*/
        cache: false, /*success*/
        success: function (html) {
            $("#dialog").html(html);
            $("#dialog").dialog({
                open: function (event, ui) {
                    var obj = $(this);
                    window.setTimeout(function () {
                        obj.dialog('close');
                    },
                            3000);
                }
            });
        }
    });
    return false;
}
function accomm_cap_update(form) {
    var data = $(form).serialize();
    $.ajax({/*this is the php file that processes the data and send mail*/
        url: "update_capacity.php",
        /*GET method is used*/
        type: "POST", /*pass the data*/
        data: data, /*Do not cache the page*/
        cache: false, /*success*/
        success: function (html) {
            $("#dialog").html(html);
            $("#dialog").dialog({
                open: function (event, ui) {
                    var obj = $(this);
                    window.setTimeout(function () {
                        obj.dialog('close');
                    },
                            3000);
                }
            });
        }
    });
    return false;
}
/*
 * save all availability
 */
function accomm_cap_update_all() {
    var acc_ids = document.getElementById('acc_ids').innerHTML;
    if (document.getElementById('current')) {
        var page = document.getElementById('current').innerHTML;
    } else {
        var page = 1;
    }
    var forms_data = [];
    var acc_ids_array = acc_ids.split(',')
    var current_index = (page - 1) * 10;
    var items = 0;
    for (var acc_id = current_index; acc_id < (acc_ids_array.length - 1); acc_id++) {
        var obj = [];
        //    var block=document.getElementById("ajax_group_accom_cap_"+acc_ids_array[acc_id]).block_id.value;
        var accom_id = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).accom_id.value;
        /*                 <!--general capacity-->*/
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).closed.checked) {
            var closed = 1;
        } else {
            closed = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).capacity) {
            var capacity = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).capacity.value;
        } else {
            capacity = 0;
        }
        /*                 <!--beds-->*/
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).accom_1x1) {
            var accom_1x1 = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).accom_1x1.value;
        } else {
            accom_1x1 = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).accom_2x2) {
            var accom_2x2 = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).accom_2x2.value;
        } else {
            accom_2x2 = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).accom_3x3) {
            var accom_3x3 = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).accom_3x3.value;
        } else {
            accom_3x3 = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).accom_4x4) {
            var accom_4x4 = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).accom_4x4.value;
        } else {
            accom_4x4 = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).accom_5x5) {
            var accom_5x5 = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).accom_5x5.value;
        } else {
            accom_5x5 = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).accom_6x6) {
            var accom_6x6 = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).accom_6x6.value;
        } else {
            accom_6x6 = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).accom_7x7) {
            var accom_7x7 = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).accom_7x7.value;
        } else {
            accom_7x7 = 0;
        }
        if (document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).accom_8x8) {
            var accom_8x8 = document.getElementById("ajax_group_accom_cap_" + acc_ids_array[acc_id]).accom_8x8.value;
        } else {
            accom_8x8 = 0;
        }
        obj = {
            accom_id: accom_id,
            closed: closed,
            capacity: capacity,
            accom_1x1: accom_1x1,
            accom_2x2: accom_2x2,
            accom_3x3: accom_3x3,
            accom_4x4: accom_4x4,
            accom_5x5: accom_5x5,
            accom_6x6: accom_6x6,
            accom_7x7: accom_7x7,
            accom_8x8: accom_8x8
        }
        forms_data.push(obj);
        items++;
        if (items == 20) {
            break;
        } else {
            continue;
        }
    }
    $.ajax({
        /*this is the php file that processes the data and send mail */
        url: "update_all_capacity.php",
        type: "POST", /*GET method is used */
        data: {/*pass the data	*/
            forms_data: forms_data
        },
        cache: false, /*Do not cache the page*/
        success: function (html) {           /*success*/
            $("#dialog").html(html);
            $("#dialog").dialog({
                open: function (event, ui) {
                    var obj = $(this);
                    window.setTimeout(function () {
                        obj.dialog('close');
                    },
                            3000);
                }
            });
        }
    });
    return false;
}
function block_activate(block_id, name) {
    $("#dialog-confirm").dialog({
        resizable: false,
        height: 140,
        modal: true,
        buttons: {
            "Delete all items": function () {
                $(this).dialog("close");
            },
            Cancel: function () {
                $(this).dialog("close");
            }
        }
    });
}
var link_type;
var block_id;
function openPopUp(id, type)
{
    $("#block_id").val(id);
    $("#link_type").val(type);
    $("#confirm_pass").attr("title", "Enter your Password.").dialog({
        modal: true,
        draggable: false,
        resizable: false,
        width: 500
    });
}
function confirm_user() {
    var u_name = $("#u_name").val();
    var u_pass = $("#u_pass").val();
    var block_id = $("#block_id").val();
    var link_type = $("#link_type").val();
    if (link_type == 'status') {
        //        document.getElementById('ajax-loader').style.display='block';
        $.ajax({/*this is the php file that processes the data and send mail*/
            url: "group_block_status_update.php",
            /*GET method is used*/
            type: "GET", /*pass the data*/
            data: {
                id: block_id,
                status: 1,
                u_name: u_name,
                u_pass: u_pass
            },
            cache: false, /*success*/
            success: function (html) {
                window.location.reload();
            }
        });
    } else if (link_type == 'launch') {
        //        document.getElementById('ajax-loader').style.display='block';
        $.ajax({/*this is the php file that processes the data and send mail*/
            url: "group_block_launch_update.php",
            /*GET method is used*/
            type: "GET", /*pass the data*/
            data: {
                id: block_id,
                launch: 1,
                u_name: u_name,
                u_pass: u_pass
            },
            cache: false, /*success*/
            success: function (html) {
                closePopUp();
                $("#dialog").html(html);
                $("#dialog").dialog({
                    open: function (event, ui) {
                        var obj = $(this);
                        window.setTimeout(function () {
                            obj.dialog('close');
                            window.location.reload();
                        },
                                3000);
                    }
                });
            }
        });
    }
    return false;
}
function closePopUp()
{
    document.getElementById('ajax-loader').style.display = 'none';
    element = document.getElementById("popup");
    element.style.display = "none";
    element = document.getElementById("background");
    element.style.display = "none";
}
//Recomendation Page
function get_subcat() {
    var cat_id = document.getElementById('recommendation-cat').value;
    $.ajax({
        type: "GET",
        url: "get-customer-listing-subcat.php",
        data: {
            cat_id: cat_id
        }
    })
            .done(function (msg) {
                document.getElementById('recommendation-subcats').style.visibility = 'visible';
                document.getElementById('recommendation-subcats').innerHTML = msg;
                $('#form').submit();
            });
}
//Modal on My pages to transfer listing to another account 
function transfer_listing(bl_id) {
    $("#transfer-listing-form" + bl_id).attr("title", "Transfer Listing").dialog({
        modal: true,
        draggable: false,
        resizable: false,
        width: 500
    });
}
function show_textbox(bl_id) {
    $("#recommendation-form" + bl_id).attr("title", "Add Recommendation").dialog({
        modal: true,
        draggable: false,
        resizable: false,
        width: 500
    });
}
function show_textbox_existing(rid) {
    $("#existing-recommendation-form" + rid).attr("title", "Edit Recommendation").dialog({
        modal: true,
        draggable: false,
        resizable: false,
        width: 500
    });
}

function view_recommendation(rid) {
    $("#view-my-recommendation" + rid).attr("title", "View Recommendation").dialog({
        modal: true,
        draggable: false,
        resizable: false,
        width: 500
    });
}
function show_description(bfp_id) {
    $("#gallery-description-form" + bfp_id).attr("title", "Edit Description").dialog({
        modal: true,
        draggable: false,
        resizable: false,
        width: 500
    });
}
$(document).ready(function () {
    function setHeight() {
        windowHeight = $(window).innerHeight();
        $('.login-outer-body').css('min-height', windowHeight);
    }
    setHeight();
    $(window).resize(function () {
        setHeight();
    });
});
function show_main_image(image_id) {
    $(".image-pop-up" + image_id).attr("title", "Image").dialog({
        width: 500,
        modal: true,
        draggable: false,
        resizable: false
    });
}
$(document).ready(function () {
    CKEDITOR.disableAutoInline = true; // Use CKEDITOR.replace() if element is <textarea>.
    $('#description-listing').ckeditor({
        customConfig: 'http://touristtowndemo.com/include/plugins/ckeditor/config.js'
    });
    $('.tt-description').ckeditor({
        customConfig: 'http://touristtowndemo.com/include/plugins/ckeditor/config.js'
    });
    $('.tt-ckeditor').ckeditor({
        customConfig: 'http://touristtowndemo.com/include/plugins/ckeditor/config.js'
    });
    $('#special_case').ckeditor({
        customConfig: 'http://touristtowndemo.com/include/plugins/ckeditor/ckeditor.config.js'
    });
    $('#email-template').ckeditor({
        customConfig: 'http://touristtowndemo.com/include/plugins/ckeditor/email.config.js'
    });
});
var dialog_box = "";
function choose_file(dialog_id) {
    dialog_box = dialog_id;
    jQuery('.image_bank_input_file').val('');
    $(".dialog" + dialog_id).attr("title", "Choose Photo From").dialog({
        width: 500,
        height: 200,
        modal: true,
        draggable: false,
        resizable: false,
        open: function () {
            jQuery('.ui-widget-overlay').bind('click', function () {
                jQuery('.dialog' + dialog_id).dialog('close');
            });
        }
    });
}
function show_image_library(image_id, iteration, limit) {
    var itr;
    if (iteration > 0) {
        itr = iteration;
    } else {
        itr = 0;
    }
    $.ajax({
        url: "show_image_bank.php",
        type: "GET",
        data: {
            image_id: image_id,
            iteration: itr,
            limit: limit
        },
        cache: false,
        success: function (html) {
            $("#image-library").html(html);
        }
    });
    var dWidth = $(window).width() * 0.95;
    var dHeight = $(window).height() * 0.95;
    jQuery('.dialog' + dialog_box).dialog('close');
    $("#image-library").attr("title", "Library").dialog({
        width: dWidth,
        height: dHeight,
        modal: true,
        draggable: false,
        resizable: false,
        open: function () {
            $("body").css("overflow", "hidden");
            jQuery('.ui-widget-overlay').bind('click', function () {
                jQuery('#image-library').dialog('close');
            });
        },
        close: function () {
            $('body').css('overflow', 'auto');
        }
    });
}
$(document).ready(function () {
    $('.dialog-close').on("click", function () {
        $(this).dialog("close");
    });
    //Getting name of the file when upload
    $('.setting-name').on('change', function (event, files, label) {
        var file_name = this.value.replace(/\\/g, '/').replace(/.*\//, '');
        $('#file-upload-name').text('');
        $('#file-upload-name').show();
        $('#file-upload-name').text(file_name);
    });
});
function select_image(id, image_id, img_bnk_name, iteration, IMG_URL) {
    jQuery('#image-library').dialog('close');
    var itr;
    if (iteration > 0) {
        itr = iteration;
    } else {
        itr = image_id;
    }
    //Empty browse and give library an image id
    $('#image_bank' + itr).val(id);
    $('#photo' + itr).val('');
    //show file name
    $('#image_name_bank' + itr).val('');
    $('#image_name_bank' + itr).show();
    $('#image_name_bank' + itr).val(img_bnk_name);
    // Show image preview
    $('#uploadFile1').hide();
    $('#preview').show();
    $('.existing_imgs' + itr).hide();
    $('#photo' + itr).hide();
    $('.preview-img-script' + itr).show();
    $('.preview-img-script' + itr).attr('src', IMG_URL + img_bnk_name);
    $('.preview-img-script' + itr).css('transform', '');
    $('.preview-img-script' + itr).css('max-width', '');
    $('.preview-img-script' + itr).css('max-height', '');
    $('.main-preview .preview-img-script' + itr).css('max-width', '');
    $('.main-preview .preview-img-script' + itr).css('max-height', '');
}

function select_multiple_image(id, image_id, iteration, img_bnk_name, IMG_URL) {
    jQuery('#image-library').dialog('close');
    var itr, idsArray;
    if (iteration > 0) {
        itr = iteration;
    } else {
        itr = image_id;
    }
    //Empty browse and give library an image id
    $('#image_bank' + itr).val(id);
    idsArray = id.split(",");
    if (idsArray.length > 1) {
        $('#uploadFile1').show();
        $('#preview').hide();
        document.getElementById("uploadFile1").value = "Multiple Images Selected";
    } else {
        $('#photo' + itr).val('');
        //show file name
        $('#image_name_bank' + itr).val('');
        $('#image_name_bank' + itr).show();
        $('#image_name_bank' + itr).val(img_bnk_name);
        // Show image preview
        $('#uploadFile1').hide();
        $('#preview').show();
        $('.existing_imgs' + itr).hide();
        $('#photo' + itr).hide();
        $('.preview-img-script' + itr).show();
        $('.preview-img-script' + itr).attr('src', IMG_URL + img_bnk_name);
        $('.preview-img-script' + itr).css('transform', '');
        $('.preview-img-script' + itr).css('max-width', '');
        $('.preview-img-script' + itr).css('max-height', '');
        $('.main-preview .preview-img-script' + itr).css('max-width', '');
        $('.main-preview .preview-img-script' + itr).css('max-height', '');
    }
}

function select_new_image(id, image_id, iteration) {
    jQuery('#image-library').dialog('close');
    var itr;
    if (iteration > 0) {
        itr = iteration;
    } else {
        itr = image_id;
    }
    $('#preview').hide();
    $('#uploadFile1').show();
    var files = document.getElementById('photo' + itr).files;
    var inp = document.getElementById('photo' + itr);
    var imageVal = inp.files.length;
    if (imageVal == 1) {
        document.getElementById("uploadFile1").value = files[0].name;
    } else
    {
        document.getElementById("uploadFile1").value = "Multiple Images Selected";
    }
    //Empty browse and give library an image id
    $('#photo' + itr).val(id);
    $('#image_bank' + itr).val('');

}

function show_file_name(image_id, input, iteration) {
    //Get dimensions of image
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
    if (regex.test(input.value.toLowerCase())) {
        //Check whether HTML5 is supported.
        if (typeof (input.files) != "undefined") {
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(input.files[0]);
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();

                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;

                //Validate the File Height and Width.
                image.onload = function () {
                    var height = this.height;
                    var width = this.width;
                    EXIF.getData(input.files[0], function () {
                        if (this.exifdata.Orientation == 3) {
                            $('.preview-img-script' + id).css('transform', 'rotate(180deg)');
                            $('.preview-img-script' + id).css('max-width', '190px');
                            $('.preview-img-script' + id).css('max-height', '127px');
                            $('.main-preview .preview-img-script' + id).css('max-width', '545px');
                            $('.main-preview .preview-img-script' + id).css('max-height', '335px');
                        } else if (this.exifdata.Orientation == 6) {
                            $('.preview-img-script' + id).css('transform', 'rotate(90deg)');
                            if (width > height) {
                                $('.preview-img-script' + id).css('max-width', '127px');
                                $('.preview-img-script' + id).css('max-height', '190px');
                                $('.main-preview .preview-img-script' + id).css('max-width', '335px');
                                $('.main-preview .preview-img-script' + id).css('max-height', '545px');
                            }
                        } else if (this.exifdata.Orientation == 8) {
                            $('.preview-img-script' + id).css('transform', 'rotate(-90deg)');
                            if (width > height) {
                                $('.preview-img-script' + id).css('max-width', '127px');
                                $('.preview-img-script' + id).css('max-height', '190px');
                                $('.main-preview .preview-img-script' + id).css('max-width', '335px');
                                $('.main-preview .preview-img-script' + id).css('max-height', '545px');
                            }
                        } else {
                            $('.preview-img-script' + id).css('transform', '');
                            $('.preview-img-script' + id).css('max-width', '');
                            $('.preview-img-script' + id).css('max-height', '');
                            $('.main-preview .preview-img-script' + id).css('max-width', '');
                            $('.main-preview .preview-img-script' + id).css('max-height', '');
                        }
                    });
                };
            }
        }
    }

    var file_name = input.value.replace(/\\/g, '/').replace(/.*\//, '');
    var id;
    if (iteration > 0) {
        id = iteration;
    } else {
        id = image_id;
    }
    if (file_name.length > 0) {
        //Empty library 
        $('#image_bank' + image_id).val('');
        //show file name
        $('#image_name_bank' + id).val('');
        $('#image_name_bank' + id).show();
        $('#image_name_bank' + id).val(file_name);
    }
    // Show image preview
    $('.existing_imgs' + id).hide();
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        $('#photo' + id).hide();
        $('.preview-img-script' + id).show();
        reader.onload = function (e) {
            $('.preview-img-script' + id).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function getPhotographer(photographer_class) {
    $.ajax({
        url: "getPhotographer.php",
        type: "GET",
        data: {
            photographer_class: photographer_class
        },
        cache: false,
        success: function (html) {
            $("#photographer").empty();
            $("#photographer").html(html);
        }
    });
}
//Progress Bar
function progress_bar(FILE_ID) {
    var progressbox = $('#progressbox');
    var progress_conti = $('.progress-container-main');
    var progressbar = $('#progressbar');
    var statustxt = $('#statustxt');
    var completed = '0%';
    var href = "";
    var options = {
        beforeSubmit: beforeSubmit, // pre-submit callback 
        uploadProgress: OnProgress,
        success: afterSuccess, // post-submit callback 
        resetForm: true        // reset the form after successful submit 
    };
    // when agenda page form submited
    var required_field = $(".required_fields" + FILE_ID).val();
    if (required_field != '' && typeof required_field != 'undefined') {
        href = window.location.href;
        var agenda_size = "";
        var minute_size = "";
        var agenda_pdf_file = $('.agenda_file_ext' + FILE_ID).val();
        if (agenda_pdf_file != '') {
            var agenda_ext = agenda_pdf_file.split(".");
            var extesion_agenda = agenda_ext[agenda_ext.length - 1];
            var ext_check_agenda = extesion_agenda.toLowerCase();
            if (ext_check_agenda == 'pdf') {
                if (($(".agenda_file_ext" + FILE_ID))[0].files.length > 0) {
                    agenda_size = ($(".agenda_file_ext" + FILE_ID))[0].files[0].size;
                    if (agenda_size > 5342523) {
                        swal("PDF!", "File Size must be less then 5MB!", "warning");
                        $('.agenda_file_ext' + FILE_ID).val('');
                        $('.agenda_pdf_new' + FILE_ID).text('');
                        return false;
                    }
                }
            } else {
                swal("PDF!", "File Must be PDF!", "warning");
                $('.agenda_file_ext' + FILE_ID).val('');
                $('.agenda_pdf_new' + FILE_ID).text('');
                return false;
            }
        }
        var minute_pdf_file = $('.minute_file_ext' + FILE_ID).val();
        if (minute_pdf_file != '') {
            var minute_ext = minute_pdf_file.split(".");
            var extesion_minute = minute_ext[minute_ext.length - 1];
            var ext_check_minute = extesion_minute.toLowerCase();
            if (ext_check_minute == 'pdf') {
                if (($(".minute_file_ext" + FILE_ID))[0].files.length > 0) {
                    minute_size = ($(".minute_file_ext" + FILE_ID))[0].files[0].size;
                    if (minute_size > 5342523) {
                        swal("PDF!", "File Size must be less then 5MB!", "warning");
                        $('.minute_file_ext' + FILE_ID).val('');
                        $('.minutes_pdf_new' + FILE_ID).text('');
                        return false;
                    }
                }
            } else {
                swal("PDF!", "File Must be PDF!", "warning");
                $('.minute_file_ext' + FILE_ID).val('');
                $('.minutes_pdf_new' + FILE_ID).text('');
                return false;
            }
        }
        $('#pdf-update' + FILE_ID).submit(function () {
            $(this).ajaxSubmit(options);
            $('.custom-ajax-show').removeClass("custom-ajax-show").addClass('custom-ajax');
        });
    } else if ($('.agenda_page').val() == 1) {
        $('.required_fields' + FILE_ID).val('');
        return false;
    }
    // when upload pdf page form submited
    var required_title = $(".required_title" + FILE_ID).val();
    if (required_title != '' && typeof required_title != 'undefined') {
        href = window.location.href;
        var pdf_size = "";
        var pdf_file = $('.file_ext' + FILE_ID).val();
        if (pdf_file != '') {
            var file_ext = pdf_file.split(".");
            var extesion_file = file_ext[file_ext.length - 1];
            var ext_check_file = extesion_file.toLowerCase();
            if (ext_check_file == 'pdf') {
                if (($(".file_ext" + FILE_ID))[0].files.length > 0) {
                    pdf_size = ($(".file_ext" + FILE_ID))[0].files[0].size;
                    if (pdf_size > 5342523) {
                        swal("PDF!", "File Size must be less then 5MB!", "warning");
                        $('.file_ext' + FILE_ID).val('');
                        return false;
                    }
                }
            } else {
                swal("PDF!", "File Must be PDF!", "warning");
                $('.agenda_file_ext' + FILE_ID).val('');
                $('.agenda_pdf_new' + FILE_ID).text('');
                return false;
            }
        } else if (FILE_ID == 0) {
            swal("PDF!", "File Not Selected", "warning");
            return false;
        }
        $('#pdf-update' + FILE_ID).submit(function () {
            $(this).ajaxSubmit(options);
            $('.custom-ajax-show').removeClass("custom-ajax-show").addClass('custom-ajax');
        });
    } else if ($('.upload_pdf_page').val() == 1) {
        $('.required_title' + FILE_ID).val('');
        return false;
    }
    if ($('#image_page').val() == 1) {
        var image_page_mobile = $('#image_page_mobile').val();
        var pendVal = $('#pendVal').val();
        if (image_page_mobile == 1)
        {
            href = "admin-mobile-menu.php";
        } else
        {
            if (pendVal != '') {
                href = "pending-image-bank.php";
            } else {
                href = "image-bank.php";
            }
        }
        var image_size = "";
        if ($('#image_title').val() != "" && $('#image_owner').val() != ""  && $('#image_season').val() != "" && $('#campaign_image').val() != "" && $('#image_people').val() != "" ) {
            var image_file = $('.setting-name').val(); 
            if (image_file != '') {
//                var image_ext = image_file.split(".");
                var extesion_image = image_file.substr(image_file.lastIndexOf('.') + 1);
                var ext_check_image = extesion_image.toLowerCase();
                if (ext_check_image == 'jpg' || ext_check_image == 'jpeg' || ext_check_image == 'png' || ext_check_image == 'gif') {
                    if (($(".setting-name"))[0].files.length > 0) {
                        image_size = ($(".setting-name"))[0].files[0].size;
                        if (image_size > 15728640) {
                            swal("Image!", "File Size must be less then 15MB!", "warning");
                            $('.setting-name').val('');
                            $('#file-upload-name').text('');
                            $("#uploadFile").attr("src", "");
                            document.getElementById("uploadFile1").value = "";
                            return false;
                        }
                    }
                } else {
                    swal("Image!", "File Must be JPG, PNG, GIF!", "warning");
                    $('.setting-name').val('');
                    $('#file-upload-name').text('');
                    $("#uploadFile").attr("src", "");
                    return false;
                }
            } else if (FILE_ID == 0) {
                swal("Image!", "Please select image!", "warning");
                $('.setting-name').val('');
                $('#file-upload-name').text('');
                $("#uploadFile").attr("src", "");
                $('#uploadFile1').hide();
                $(".uploadFileName").val('');
                $(".uploadFileName").hide();
                return false;
            }
        } else if (FILE_ID == 0) {
            return false;
        }
        $('#MyUploadForm').submit(function () {
            $(this).ajaxSubmit(options);
            $('.custom-ajax-show').removeClass("custom-ajax-show").addClass('custom-ajax');
        });
    }
    if ($('#image_page_mobile_redirect').val() == 1) {
        href = "customer-listing-mobile-menu.php?bl_id=" + $('#bl_id').val();
        var image_size = "";
        var image_file = $('.setting-name').val();
        if (image_file != '') {
            var extesion_image = image_file.substr(image_file.lastIndexOf('.') + 1);
            var ext_check_image = extesion_image.toLowerCase();
            if (ext_check_image == 'jpg' || ext_check_image == 'jpeg' || ext_check_image == 'png' || ext_check_image == 'gif') {
                if (($(".setting-name"))[0].files.length > 0) {
                    image_size = ($(".setting-name"))[0].files[0].size;
                    if (image_size > 15728640) {
                        swal("Image!", "File Size must be less then 15MB!", "warning");
                        $('.setting-name').val('');
                        $('#file-upload-name').text('');
                        $("#uploadFile").attr("src", "");
                        document.getElementById("uploadFile1").value = "";
                        return false;
                    }
                }
            } else {
                swal("Image!", "File Must be JPG, PNG, GIF!", "warning");
                $('.setting-name').val('');
                $('#file-upload-name').text('');
                $("#uploadFile").attr("src", "");
                return false;
            }
        } else if (FILE_ID == 0) {
            return false;
        }
        $('#MyUploadForm').submit(function () {
            $(this).ajaxSubmit(options);
            $('.custom-ajax-show').removeClass("custom-ajax-show").addClass('custom-ajax');
        });
    }
    //when upload progresses	
    function OnProgress(event, position, total, percentComplete)
    {
        //Progress bar
        progressbar.width(percentComplete + '%') //update progressbar percent complete
        statustxt.html(percentComplete + '%'); //update status text
        if (percentComplete > 50)
        {
            statustxt.css('color', '#fff'); //change status text to white after 50%
        }
    }
    //after succesful upload
    function afterSuccess()
    {
        window.location.href = href;

    }
    //function to check file size before uploading.
    function beforeSubmit() {
        //check whether browser fully supports all File API
        if (window.File && window.FileReader && window.FileList && window.Blob)
        {
            //Progress bar
            progressbox.show(); //show progressbar
            progress_conti.show(); //show progress container
            progressbar.width(completed); //initial value 0% of progressbar
            statustxt.html(completed); //set status text
            statustxt.css('color', '#000'); //initial color of status text
        }
    }
}
$(document).ready(function () {
    $('input[type="file"]').on('change', function () {
        var agenda_file_name = this.value.replace(/\\/g, '/').replace(/.*\//, '');
        var set_name = $(this).attr('id');
        $('.agenda_file_name').show();
        $('.agenda_file_name').css('float', 'left');
        $('.agenda_file_name').css('margin', '8px 0px 0px 10px');
        $('.agenda_file_name').css('width', '100px');
        $('.' + set_name).text('');
        $('.' + set_name).text(agenda_file_name);
    });
});
//Check IMG Size of Image for gallery add-On
function check_img_size(image_field, image_size) {
    if ($("#title" + image_field).val() == "") {
        swal("Required!", "Please fill out title field!", "warning");
        return false;
    }
    var update = $('#update_' + image_field).val();
    var image_file = $("#photo" + image_field).val();
    if (image_file != '') {
        var extesion_image = image_file.substr(image_file.lastIndexOf('.') + 1);
        var ext_check_image = extesion_image.toLowerCase();
        if (ext_check_image != 'jpg' && ext_check_image != 'jpeg' && ext_check_image != 'png' && ext_check_image != 'gif') {
            swal("Image!", "File Must be JPG, PNG, GIF!", "warning");
            $("#photo" + image_field).val('');
            $("#image_name_bank" + image_field).hide('');
            return false;
        }
    }
    if (($("#photo" + image_field))[0].files.length > 0) {
        var img_size = ($("#photo" + image_field))[0].files[0].size;
        if (img_size > image_size) {
            if (image_size == 3000000) {
                swal("Image!", "File Size must be less then 3MB!", "warning");
            } else {
                swal("Image!", "File Size must be less then 10MB!", "warning");
            }
            $("#photo" + image_field).val('');
            $("#image_name_bank" + image_field).hide('');
            return false;
        }
    } else if (($("#photo" + image_field))[0].files.length == 0) {
        if ($('#image_bank' + image_field).val() == "") {
            if (update != 1) {
                swal("Image!", "Please select image!", "warning");
                $("#photo" + image_field).val('');
                $("#image_name_bank" + image_field).hide('');
                return false;
            }
        }
    }
}
function pagination(lowerLimit, upperLimit, currentPage, image_id, iteration, AJAXPage) {

    if (AJAXPage == "show_image_bank_ajax") {

        var keywords = $('#keywords-searched').val();
        var region = new Array();
        var season = new Array();
        var owner = new Array();
        var cat = new Array();
        var people = new Array();
        var campaign = new Array();
        $("input[name=regionfilter]:checked").each(function () {
            region.push($(this).val());
        });
        $("input[name=seasonfilter]:checked").each(function () {
            season.push($(this).val());
        });
        $("input[name=catfilter]:checked").each(function () {
            cat.push($(this).val());
        });
        $("input[name=peoplefilter]:checked").each(function () {
            people.push($(this).val());
        });
        $("input[name=ownerfilter]:checked").each(function () {
            owner.push($(this).val());
        });
        $("input[name=campaignfilter]:checked").each(function () {
            campaign.push($(this).val());
        });
        $.ajax({
            url: "show_image_bank_ajax.php",
            type: "GET",
            data: {
                region: region.join(','),
                season: season.join(','),
                cat: cat.join(','),
                people: people.join(','),
                owner: owner.join(','),
                campaign: campaign.join(','),
                search_image: keywords,
                pages: 1,
                lowLimit: lowerLimit,
                upLimit: upperLimit,
                current: currentPage,
                image_id: image_id,
                iteration: iteration
            },
            success: function (html) {
                $('#image-bank-container').html('');
                $('#image-bank-container').remove();
                $('#image-library').append('<div class="image-bank-container image-bank-container-on-dialog" id="image-bank-container"></div>');
                $('#image-bank-container').html(html);
                maintain_selection();
            }
        });
    } else {

        $('#script_auto').html('');
        $.post("show_image_bank.php", {
            pages: 1,
            lowLimit: lowerLimit,
            upLimit: upperLimit,
            current: currentPage,
            image_id: image_id,
            iteration: iteration
        }, function (done) {
            $('#image-bank-container').html('');
            $('#image-bank-container').remove();
            $('#script_auto').html('');
            $('#script_auto').remove();
            $('.main-nav-image-search-text-field ul').first().remove();
            $('#image-library').append('<div id="image-bank-container-dynamic"></div>');
            $('#image-bank-container-dynamic').html(done);
            maintain_selection();
        })
    }
}
/*Validate Event form*/
function validateEventForm() {
    var checked_box = $('input:checkbox:checked.community_select').map(function () {
        return this.value;
    }).get();
    if (checked_box == "") {
        alert('Please select atleast one community');
        $(".community_select").focus();
        return false;
    }
    if ($("#location").val() == '' && $("#setLocationID").val() == 0) {
        alert('Location Or Alternate Location Required');
        $("#location").focus();
        return false;
    }
    if ($("#organization").val() == '' && $("#setOrganizationID").val() == 0) {
        alert('Organization Or Alternate Organization Required');
        $("#organization").focus();
        return false;
    }
    $("#button2").attr('disabled', 'disabled');
}
$(function () {
    $(".weekday").click(function () {
        if ($(this).prop('checked') == true) {
            $(this).val(1);
        } else if ($(this).prop('checked') == false) {
            $(this).val(0);
        }
    });
    $("#all_day").click(function () {
        if ($(this).prop('checked') == true) {
            $(this).val(1);
            $('#starttime').hide();
            $('#starttime').attr('required', false);
            $('#endtime').hide();
            $('#endtime').attr('required', false);
        } else if ($(this).prop('checked') == false) {
            $(this).val(0);
            $('#starttime').show();
            $('#starttime').attr('required', true);
            $('#endtime').show();
            $('#endtime').attr('required', true);
        }
    });
    $(".community_select").click(function () {
        var childRegion = $('input:checkbox:checked.community_select').map(function () {
            return this.value;
        }).get();
        $.post("get_organizations_ajax.php", {
            childRegion: childRegion,
            organization: '1'
        }, function (result) {
            $("#ajax-organization").empty();
            $("#ajax-organization").html(result);
        });
        $.post("get_organizations_ajax.php", {
            childRegion: childRegion,
            location: '1'
        }, function (result) {
            $("#ajax-location").empty();
            $("#ajax-location").html(result);
        });
    });
});