<p>
    <?php if (in_array('business-listings', $_SESSION['USER_PERMISSIONS'])) { ?>
        <a href="/admin/customer-listing-overview.php?id=<?= $BL_ID ?>">Overview</a><br />
    <?php } if (in_array('business-listings', $_SESSION['USER_PERMISSIONS'])) { ?>
        <a href="/admin/customer-listing-category.php?id=<?= $BL_ID ?>">Categories</a><br />
    <?php } if (in_array('business-listings', $_SESSION['USER_PERMISSIONS'])) { ?>
        <a href="/admin/customer-listing-regions.php?id=<?= $BL_ID ?>">Regions</a><br />
    <?php } ?>
    <?php if (in_array('listing-tools', $_SESSION['USER_PERMISSIONS'])) { ?>
        <a href="/admin/customer-listing-features.php?id=<?= $BL_ID ?>">Listing Tools</a><br />
    <?php } if (in_array('business-listings', $_SESSION['USER_PERMISSIONS'])) { ?>
        <a href="/admin/customer-listing-main-page.php?id=<?= $BL_ID ?>">Main Listing Page</a><br />
    <?php } if (in_array('business-listings', $_SESSION['USER_PERMISSIONS'])) { ?>
        <a href="/admin/customer-listing-ammenities.php?id=<?= $BL_ID ?>">Amenities</a><br />
    <?php } if (in_array('billing-history', $_SESSION['USER_PERMISSIONS'])) { ?>
        <a href="/admin/customer-listing-billing.php?id=<?= $BL_ID ?>">Billing</a>
    <?php } ?>
</p>

<p><strong><em>Listing Tools</em></strong><br>
    <?PHP
    if (in_array('listing-tools', $_SESSION['USER_PERMISSIONS'])) {
        $sql = "SELECT * FROM tbl_Feature";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            $sql1 = "SELECT * FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = " . $row['F_ID'];
            $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
            $row1 = mysql_fetch_assoc($result1);
            if ($row1['BLF_BL_ID'] != '') {
                if ($row['F_Name'] == 'About Us') {
                    ?>
                    <a href="/admin/customer-feature-about-us.php?id=<?= $BL_ID ?>">About Us</a><br>
                <?PHP } if ($row['F_Name'] == 'Photo Gallery') { ?>
                    <a href="/admin/customer-feature-gallery.php?id=<?= $BL_ID ?>">Photo Gallery</a><br>
                <?PHP } if ($row['F_Name'] == 'Our Products') { ?>
                    <a href="/admin/customer-feature-product.php?id=<?= $BL_ID ?>">Products</a><br>
                <?PHP } if ($row['F_Name'] == 'Menu') { ?>
                    <a href="/admin/customer-feature-menu.php?id=<?= $BL_ID ?>">Menu</a><br>
                <?PHP } if ($row['F_Name'] == 'Daily Features') { ?>
                    <a href="/admin/customer-feature-daily-specials.php?id=<?= $BL_ID ?>">Daily Features</a><br>
                <?PHP } if ($row['F_Name'] == 'Entertainment') { ?>
                    <a href="/admin/customer-feature-entertainment.php?id=<?= $BL_ID ?>">Entertainment</a><br>
                <?PHP } if ($row['F_Name'] == 'Hours') { ?>
                    <a href="/admin/customer-hours.php?id=<?= $BL_ID ?>">Hours</a><br>
                <?PHP } if ($row['F_Name'] == 'Social Media') { ?>
                    <a href="/admin/customer-social-media.php?id=<?= $BL_ID ?>">Social Media</a><br>
                <?php } ?>
                <?php
            }
        }
    }
    ?>
</p>