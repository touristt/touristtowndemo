<?PHP
require_once '../include/config.inc.php';
require_once '../include/coupon.login.inc.php';
require_once '../include/image-bank-usage-function.php';

$BL_ID=$_REQUEST['bl_id'];
if ($BL_ID > 0) {
    $sql = "SELECT * FROM tbl_Business_Listing 
	    WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db)
            or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $delCouponImg = "DELETE FROM tbl_Business_Feature_Coupon WHERE BFC_ID = '" . encode_strings($_REQUEST['bfc_id'], $db) . "' AND BFC_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $resDelCouponImg = mysql_query($delCouponImg, $db) or die("Invalid query: $delCouponImg -- " . mysql_error());
    if ($resDelCouponImg) {
        $_SESSION['delete'] = 1;
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Coupon_Photo', $_REQUEST['bfc_id']);
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location:manage-coupon.php?bl_id=" . $BL_ID);
    exit();
}
logon_function_header($title);
?>
    <div class="main-container">
        <div class="coupon-listing-title">
            <?php echo $row['BL_Listing_Title'] ?>
        </div>
        <div class="main-container-body">
        <div class="left">
            <?PHP
        require '../include/nav-B-mypage-coupon.php';
        ?>
        </div>
        <div class="right">
            <div class='data-container'>
            <div class='data-header'>
                <div class='data-header-coupon-title'>Coupon Name</div>
                <div class='data-header-coupon-option-no'>#Sent</div>
                <div class='data-header-coupon-option'>Edit</div>
                <div class='data-header-coupon-option'>Delete</div>
            </div>
              <?PHP
            $sql = "SELECT * FROM tbl_Business_Feature_Coupon WHERE BFC_BL_ID = '$BL_ID'";
            $resultTMP = mysql_query($sql, $db)
                    or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($resultTMP)) {
                ?>
                <div class="data-content">
                    <div class="data-content-coupon-title">
                        <?php echo $row['BFC_Title'] ?>
                    </div>
                    <div class="data-content-coupon-option-no">
                        <?php
                        $sql_sent="SELECT * FROM  tbl_Coupon_Usages where CU_BFC_ID='" . encode_strings($row['BFC_ID'], $db) . "' order by CU_Counter desc";
                        $result_count=mysql_query($sql_sent);
                        $row_count=mysql_fetch_assoc($result_count);
                        echo ($row_count['CU_Counter']!='' ? $row_count['CU_Counter'] : '0');
                        ?>
                    </div>
                    <div class="data-content-coupon-option"><a href="add-manage-coupon.php?bl_id=<?php echo $BL_ID ?>&bfc_id=<?php echo $row['BFC_ID'] ?>">Edit</a></div>
                    <div class="data-content-coupon-option"><a onClick="return confirm('Are you sure?');" href="manage-coupon.php?bl_id=<?php echo $BL_ID ?>&bfc_id=<?php echo $row['BFC_ID'] ?>&op=del">Delete</a></div>
                </div>
            <?PHP }
            ?>
                <div class='link'><a href='add-manage-coupon.php?bl_id=<?php echo $BL_ID ?>'>+ Add Coupon</a></div>
        
            </div>
            
        </div>
       </div>
    </div>

<?PHP
if ($_SESSION['success'] == 1) {
    print '<script>swal("Data Saved", "Data has been saved successfully.", "success");</script>';
    unset($_SESSION['success']);
}
if ($_SESSION['error'] == 1) {
    print '<script>swal("Error", "Error saving data. Please try again.", "error");</script>';
    unset($_SESSION['error']);
}
if ($_SESSION['delete'] == 1) {
    print '<script>swal("Data Deleted", "Data has been deleted successfully.", "success");</script>';
    unset($_SESSION['delete']);
}
if ($_SESSION['delete_error'] == 1) {
    print '<script>swal("Error", "Error deleting data. Please try again.", "error");</script>';
    unset($_SESSION['delete_error']);
}
logon_function_footer();
?>