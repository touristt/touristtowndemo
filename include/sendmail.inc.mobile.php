<?PHP
require '../../include/PHPMailer/class.phpmailer.php';

function send_mail($msg, $email, $sub, $plaintext = false, $bcc = '', $from = array('name' => MAIN_CONTACT_NAME, 'email' => MAIN_CONTACT_EMAIL)) {

  $mail = new PHPMailer();
  $mail->From = $from['email'];
  $mail->FromName = $from['name'];
  $mail->IsHTML(true);

  $mail->AddAddress($email);

  $mail->Subject = $sub;
  $mail->MsgHTML($msg);

  if ($mail->Send()) {
    $msg = true;
  } else {
    $msg = false;
  }
  return $msg;
//  if (stristr($_SERVER['SCRIPT_FILENAME'], ABS_ROOT) !== FALSE) {
//    $headers = "Return-path: <" . $from[email] . ">\n";
//    $headers .= "From: \"" . $from[name] . "\" <" . $from[email] . ">\n";
//    $headers .= "Reply-To: " . $from[email] . "\n";
//    $headers .= "X-Sender: <" . $from[email] . ">\n";
//    $headers .= "X-Mailer: PHP\n"; //mailer
//    $headers .= "X-Priority: 3\n"; //1 UrgentMessage, 3 Normal
//    $headers .= "X-MSMail-Priority: Normal\n";
//
//    if ($bcc != '') {
//      $headers .= "Bcc: $bcc\n";
//    }
//    //Uncomment this to send html format
//    $headers .= "MIME-Version: 1.0";
//    if ($plaintext == false) {
//      $headers .= "\nContent-Type: text/html; charset=\"ISO-8859-1\"";
//    }
//
//    $done = 'no';
//    $counter = 0;
//    while ($done != 'yes' && $counter < 3) {
//      if (mail($email, $sub, $msg, $headers)) {
//        $done = 'yes';
//      }
//      $counter++;
//      sleep(2);
//    }
//    if ($done == 'yes')
//      $msg = true;
//    else
//      $msg = false;
//    return $msg;
//  }
//  else {
//    exit();
//  }
}

function add_msg_to_queue($msg, $sub, $html, $bcc, $fn, $fe, $e, $plaintext = '', $priority = false) {
  global $db;

  if (checkEmail($e)) {
    $sql = "INSERT tbl_Mail_Queue SET 
				MQ_To 			= '" . encode_strings($e, $db) . "',
				MQ_Subject		= '" . encode_strings($sub, $db) . "',
				MQ_Message		= '" . encode_strings(str_replace('##e##', $e, $msg), $db) . "',
				MQ_Plaintext	= '" . encode_strings(str_replace('##e##', $e, $plaintext), $db) . "',
				MQ_HTML 		= '" . encode_strings($html, $db) . "',
				MQ_From_Name 	= '" . encode_strings($fn, $db) . "',
				MQ_From_Email 	= '" . encode_strings($fe, $db) . "',
				MQ_BCC 			= '" . encode_strings($bcc, $db) . "'";
    if (!$priority) {
      $sql .= ", MQ_Time 		= NOW()";
    }
    return query($sql, $db);
  } else {
    return false;
  }
}

function run_mail_queue() {
  global $config, $db, $max_emails;
  require_once "Mail.php";
  require_once "Mail/mime.php";

//	$max_emails = 1;

  $sql = "SELECT * FROM tbl_Mail_Queue 
				ORDER BY MQ_Time ASC LIMIT " . $max_emails;
  $result = query($sql, $db);


  $mail = Mail::factory('smtp', array('host' => 'mail.touristtown.ca',
              'auth' => true,
              'username' => 'info@touristtown.ca',
              'port' => 26,
              'password' => 'JgvHkT9sW-RX'));

  while ($row = mysql_fetch_assoc($result)) {

    $to = $row['MQ_To'];
    //$to = 'rbell@lucidgs.com';

    $recipients = $to;

    $headers['From'] = $row['MQ_From_Name'] . " <" . $row['MQ_From_Email'] . ">";
    $headers['To'] = $to;
    $headers['Subject'] = $row['MQ_Subject'];
    $headers['Content-Type'] = "text/html; charset=\"iso-8859-1\"";
    $headers['Reply-To'] = $row['MQ_From_Name'] . " <" . $row['MQ_From_Email'] . ">";


    $text = $row['MQ_Plaintext'];
    $html = $row['MQ_Message'];
    $crlf = "\n";
    $hdrs = array(
        'From' => $row['MQ_From_Name'] . " <" . $row['MQ_From_Email'] . ">",
        'Subject' => $row['MQ_Subject'],
        'Reply-To' => $row['MQ_From_Name'] . " <" . $row['MQ_From_Email'] . ">"
    );

    $mime = new Mail_mime($crlf);

    $mime->setTXTBody($text);
    $mime->setHTMLBody($html);

    //do not ever try to call these lines in reverse order
    $body = $mime->get();
    $headers = $mime->headers($hdrs);


    $send = $mail->send($recipients, $headers, $body);

    if (PEAR::isError($send)) {
      echo("<p>" . $send->getMessage() . "</p>");
    } else {
      $sql = "DELETE FROM tbl_Mail_Queue WHERE MQ_ID = " . $row['MQ_ID'] . " LIMIT 1";
      query($sql, $db);
      echo("<p>Message successfully sent - $recipients!</p>");
    }
  }
}

function sendAttachment($msg, $email, $sub, $fileVar, $bcc = '', $from = array('name' => MAIN_CONTACT_NAME, 'email' => MAIN_CONTACT_EMAIL)) {

  $to = $email;
//define the subject of the email
  $subject = $sub;
//create a boundary string. It must be unique
//so we use the MD5 algorithm to generate a random hash
  $random_hash = md5(date('r', time()));

  $headers = "Return-path: <" . $from[email] . ">\n";
  $headers .= "From: \"" . $from[name] . "\" <" . $from[email] . ">\n";
  $headers .= "Reply-To: " . $from[email] . "\n";
  $headers .= "X-Sender: <" . $from[email] . ">\n";
  $headers .= "X-Mailer: PHP\n"; //mailer
  $headers .= "X-Priority: 3\n"; //1 UrgentMessage, 3 Normal
  $headers .= "X-MSMail-Priority: Normal\n";

  if ($bcc != '') {
    $headers .= "Bcc: $bcc\n";
  }
//add boundary string and mime type specification
  $headers .= "Content-Type: multipart/mixed; boundary=\"PHP-mixed-" . $random_hash . "\"";
//read the atachment file contents into a string,
//encode it with MIME base64,
//and split it into smaller chunks
  $attachment = chunk_split(base64_encode(file_get_contents($_FILES[$fileVar]['tmp_name'])));
//define the body of the message.
  ob_start(); //Turn on output buffering
  ?>
  --PHP-mixed-<?php echo $random_hash; ?> 
  Content-Type: multipart/alternative; boundary="PHP-alt-<?php echo $random_hash; ?>"

  --PHP-alt-<?php echo $random_hash; ?> 
  Content-Type: text/plain; charset="iso-8859-1"
  Content-Transfer-Encoding: 7bit

  <?php echo strip_tags($msg); ?>

  --PHP-alt-<?php echo $random_hash; ?> 
  Content-Type: text/html; charset="iso-8859-1"
  Content-Transfer-Encoding: 7bit

  <?php echo $msg; ?>

  --PHP-alt-<?php echo $random_hash; ?>--

  --PHP-mixed-<?php echo $random_hash; ?> 
  Content-Type: application/pdf; name="<?= $_FILES[$fileVar]['name'] ?>" 
  Content-Transfer-Encoding: base64 
  Content-Disposition: attachment filename="<?= $_FILES[$fileVar]['name'] ?>"

  <?php echo $attachment; ?>

  --PHP-mixed-<?php echo $random_hash; ?>--

  <?php
//copy current buffer contents into $message variable and delete current output buffer
  $message = ob_get_clean();
//send the email
  $mail_sent = @mail($to, $subject, $message, $headers);
//if the message is sent successfully print "Mail sent". Otherwise print "Mail failed"
  return $mail_sent;
}
?>