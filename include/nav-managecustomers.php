<?php 
$filename = basename($_SERVER["SCRIPT_FILENAME"], '.php');
if($filename == 'deactivate-listings') {
    $action = 'deactivate-listings';
    $email_action = 'deactivate-listings';
} else if($filename == 'inactivated_free_listings') {
    $action = 'inactivated_free_listings';
    $email_action = 'inactivated_free_listings';
}else {
    $action = 'listings';
    $email_action = 'customers';
}
?>
<div class="sub-menu">
    <ul>
        <?PHP if (in_array('free-listings', $_SESSION['USER_PERMISSIONS'])) { ?>
            <li><a href="/admin/listings.php?type=Free">Free Listings</a></li>
        <?PHP } if (in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) { ?>
            <li><a href="/admin/listings.php?type=Town Asset">Town Assets</a></li>
        <?PHP }if (in_array('business-listings', $_SESSION['USER_PERMISSIONS'])) { ?>
            <li><a href="/admin/listings.php?type=Enhanced">Enhanced Listings</a></li>
        <?PHP } if (in_array('customers', $_SESSION['USER_PERMISSIONS'])) { ?>
            <li><a href="/admin/customers-stats.php">Listings Stats</a></li>
            <li><a href="/admin/login-details.php">+Add Customer</a></li>
            <li><a href="/admin/add-ons-stats.php">Add Ons stats</a></li>
        <?PHP }
        ?>
    </ul>
</div>
<form id = "form1" name = "form1" method = "GET" action = "/admin/<?php echo (isset($action)) ? $action : ''; ?>.php">
    <label>
        <strong>Search by Contact</strong><br><br>
        <input name = "strSearch_contact" type = "text" id = "strSearch" size = "20" />
    </label>
    <br><br>
    <label>
        <input type = "submit" name = "button" id = "button" value = "Search" />
    </label>
</form>
<form id = "form1" name = "form1" method = "GET" action = "/admin/<?php echo (isset($action)) ? $action : ''; ?>.php" style="margin-top: 20px;">
    <strong>Search by Listing</strong><br><br>
    <label>
        <?php if (isset($_REQUEST['type'])) { ?>
            <input type="hidden" value="<?php echo $_REQUEST['type'] ?>" name="type">
        <?php } ?>
        <input name = "strSearch" type = "text" id = "strSearch" size = "20" />
    </label>
    <br><br>
    <label>
        <input type = "submit" name = "button" id = "button" value = "Search" />
    </label>
</form>
<form name = "form" method = "GET" action = "/admin/<?php echo (isset($email_action)) ? $email_action : ''; ?>.php" style="margin-top: 20px;">
    <label>
        <strong>Search by Email</strong><br><br>
        <input name="email" type="email" id = "strSearch" required/>
    </label>
    <br><br>
    <label>
        <input type="submit" name="button" value="Search" />
    </label>
</form>
