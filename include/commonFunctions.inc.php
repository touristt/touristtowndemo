<?PHP
function makeRandomPassword($j = 7) {
    $salt = "ABCDEFGHJKLMNPQRSTUVWXYabchefghjkmnpqrstuvwxyz23456789";
    srand((double) microtime() * 1000000);
    $i = 0;
    while ($i <= $j) {
        $num = rand() % 33;
        $tmp = substr($salt, $num, 1);
        $pass = $pass . $tmp;
        $i++;
    }
    return $pass;
}

function getHeight($ow, $oh, $Nw) {
    $pro = (float) $ow / $Nw;
    return round($oh / $pro, 0);
}

function getWidth($ow, $oh, $Nh) {
    $pro = (float) $oh / $Nh;
    return round($ow / $pro, 0);
}

function encodeHTML($sHTML) {
    return htmlentities($sHTML, ENT_QUOTES);
}

function my_session_destroy($type = 0) {
    if ($type == 'member') {
        $_SESSION['MEMBER_ID'] = 0;
        $_SESSION['MEMBER_NAME'] = '';
        $_SESSION['MEMBER_USER_KEY'] = 0;
        $_SESSION['MEMBER_USER_ID'] = 0;
        $_SESSION['MEMBER_USER_EM'] = '';
        $_SESSION['member_user_online'] = '';
        $_SESSION['MEMBER_SUPER_USER'] = 0;
        $_SESSION['MEMBER_PERMISSIONS'] = array();
        $_SESSION['MEMBER_ERROR'] = '';
        $_SESSION['MEMBER_INFO'] = '';
    } elseif ($type == 'admin') {
        $_SESSION['USER_KEY'] = 0;
        $_SESSION['USER_ID'] = 0;
        $_SESSION['USER_SUPER_USER'] = 0;
        $_SESSION['USER_PERMISSIONS'] = array();
        $_SESSION['USER_EM'] = '';
        $_SESSION['user_online'] = '';
        $_SESSION['ERROR'] = '';
        $_SESSION['info'] = '';
        $_SESSION['previousPage'] = '';
    } elseif ($type == 'printer') {
        $_SESSION['PRINT_KEY'] = 0;
        $_SESSION['PRINT_ID'] = 0;
        $_SESSION['PRINT_EM'] = '';
        $_SESSION['PRINT_online'] = '';
        $_SESSION['PRINT_ERROR'] = '';
        $_SESSION['PRINT_INFO'] = '';
    } else {
        $_SESSION = array();
        if (isset($_COOKIE[session_name()])) {
            setcookie(session_name(), '', time() - 42000, '/');
        }
        session_destroy();
    }
}

function checkEmail($e) {
    return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,5})$", $e);
}

function dateInput($y = '00', $m = '00', $d = '00') {
    $text .= '<select class="form" name="month">';
    $text .= '<option value="00"';
    if ($m == '00') {
        $text .= ' selected="selected"';
    }
    $text .= '>Month</option>';
    for ($i = 1; $i < 13; $i++) {
        $text .= '<option value="' . ($i < 10 ? '0' . $i : $i) . '"';
        if ($m == $i || ($m == '' && $i == date('m'))) {
            $text .= ' selected="selected"';
        }
        $text .= '>' . date('F', mktime(0, 0, 0, $i, 1)) . '</option>';
    }
    $text .= '</select>';
    $text .= '<select class="form" name="day">';
    $text .= '<option value="00"';
    if ($m == '00') {
        $text .= ' selected="selected"';
    }
    $text .= '>Day</option>';
    for ($i = 1; $i < 32; $i++) {
        $text .= '<option value="' . ($i < 10 ? '0' . $i : $i) . '"';
        if ($d == $i || ($d == '' && $i == date('d'))) {
            $text .= ' selected="selected"';
        }
        $text .= '>' . $i . '</option>';
    }
    $text .= '</select>';
    $text .= '<select class="form" name="year">';
    $text .= '<option value="0000"';
    if ($m == '0000') {
        $text .= ' selected="selected"';
    }
    $text .= '>Year</option>';
    for ($i = date('Y'); $i < date('Y') + 5; $i++) {
        $text .= '<option value="' . $i . '"';
        if ($y == $i || ($y == '' && $i == date('Y'))) {
            $text .= ' selected="selected"';
        }
        $text .= '>' . $i . '</option>';
    }
    $text .= '</select>';
    return $text;
}

function timeInput($t) {
    $t = explode(':', $t);
    $h = $t[0];
    $m = $t[1];

    if ($h > 12) {
        $h = $h - 12;
        $PM = true;
    }
    $text .= '<select name="hour">';
    $text .= '<option value="00"';
    if ($m == '00') {
        $text .= ' selected="selected"';
    }
    $text .= '>Hour</option>';
    for ($i = 1; $i < 13; $i++) {
        $text .= '<option value="' . ($i < 10 ? '0' . $i : $i) . '"';
        if ($h == $i) {
            $text .= ' selected="selected"';
        }
        $text .= '>' . $i . '</option>';
    }
    $text .= '</select>';
    $text .= ':';
    $text .= '<select name="minute">';
    $text .= '<option value="00"';
    if ($m == '00') {
        $text .= ' selected="selected"';
    }
    $text .= '>Minute</option>';
    for ($i = 0; $i < 4; $i++) {
        $output = $i * 15;
        $output = $output < 10 ? '0' . $output : $output;
        $text .= '<option value="' . $output . '"';
        if ($m == $output) {
            $text .= ' selected="selected"';
        }
        $text .= '>' . $output . '</option>';
    }
    $text .= '</select>';

    $text .= '<select name="meridiem">';
    $text .= '<option value="00"';
    if (!$PM) {
        $text .= ' selected="selected"';
    }
    $text .= '>AM</option>';
    $text .= '<option value="12"';
    if ($PM) {
        $text .= ' selected="selected"';
    }
    $text .= '>PM</option>';
    $text .= '</select>';
    return $text;
}

function convert_smart_quotes($string) {
    $search = array(chr(145),
        chr(146),
        chr(147),
        chr(148),
        chr(151));

    $replace = array("'",
        "'",
        '"',
        '"',
        '-');

    return str_replace($search, $replace, $string);
}

function encodeTextInput($string) {
    return htmlspecialchars($string, ENT_QUOTES);
}

function limit_text($text, $limit) {
    $text = strip_tags($text);
    if (strlen($text) > $limit) {
        $pos = strpos($text, ' ', $limit);
        $text = substr($text, 0, $pos);
    }
    return $text;
}

function getFileExtension($name) {
    $filename = strtolower($name);
    $exts = split("[/\\.]", $filename);
    $n = count($exts) - 1;
    return $exts[$n];
}

function replaceLinks($input, $cid, $client) {

    $regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";

    if (preg_match_all("/$regexp/siU", $input, $matches, PREG_SET_ORDER)) {
        foreach ($matches as $match) {
            $input = str_replace($match[2], "http://" . DOMAIN
                    . "/newsletter-link.php?client=" . $client . "&amp;cid=" . $cid . "&amp;link=" . urlencode($match[2]), $input);
        }
    }
    return $input;
}

function fixBusinessSEOname($name) {
    $arrSearch = array("'", '"', '&', '\\', '/', ',');
    $seoName = str_replace($arrSearch, '', strtolower($name));
    $seoName = str_replace('  ', ' ', $seoName);
    $seoName = str_replace(' ', '-', $seoName);
    return $seoName;
}

?>