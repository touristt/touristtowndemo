<?php
$regionList = '';
if ($REGION['R_Parent'] == 0) {
    $sql = "SELECT * FROM tbl_Region_Multiple LEFT JOIN tbl_Region ON RM_Child = R_ID WHERE RM_Parent = '" . encode_strings($REGION['R_ID'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $first = true;
    $regionList .= "(";
    while ($row = mysql_fetch_assoc($result)) {
        if ($first) {
            $first = false;
        } else {
            $regionList .= ",";
        }
        $regionList .= $row['RM_Parent'];
    }
    $regionList .= ")";
}

$sql = "SELECT *, 
        DATE_FORMAT(BFEA_Time, '%c/%e/%Y') as myDate, 
        LOWER(DATE_FORMAT(BFEA_Time, '%l:%i%p')) as myTime 
        FROM tbl_Business_Feature_Entertainment_Acts 
        LEFT JOIN tbl_Business_Listing ON BFEA_BL_ID = BL_ID 
        LEFT JOIN tbl_Listing_Type ON BL_Listing_Type = LT_ID
        LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
        LEFT JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID 

        WHERE BFEA_Time <= DATE_ADD(CURDATE(), INTERVAL 10 DAY) AND 
        BLCR_BLC_R_ID " . encode_strings(($REGION['R_Parent'] == 0 ? "IN " . $regionList : "= " . $REGION['R_ID']), $db) . "
        AND BFEA_Time != '0000-00-00 00:00:00' AND BFEA_Time >= DATE(CURDATE())
        ORDER BY BFEA_Time";

$result1 = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$totalResult = mysql_num_rows($result1);
$arrDisplay = array();
if ($totalResult > 0) {
    ?>
    <h3 class="main-content-heading"><a name="atAGlance"></a><?php echo 'Things To Do - At A Glance'; ?></h3>

    <div class="wide-list entertainment glance" style=" margin-right: 25px;">

        <ul class="thumbnails">
            <?php
            while ($data = mysql_fetch_assoc($result1)) {
                if (!in_array($data['BFEA_ID'], $arrDisplay)) {
                    $arrDisplay[] = $data['BFEA_ID'];
                    ?>
                    <li class="" style="overflow:hidden; display: block;clear: both;">
                        <div class="thumbnail" style="overflow:hidden; display: block;clear: both;">
                            <div class="date-box">

                                <div class="mo-name">&bull;&nbsp;<?php echo date("M", strtotime($data['BFEA_Time'])); //REPLACE        ?>&nbsp;&bull;</div>
                                <div class="date-num"><?php echo date("d", strtotime($data['BFEA_Time'])); //REPLACE        ?></div>
                                <div class="day-name"><?php echo date("D", strtotime($data['BFEA_Time'])); //REPLACE        ?></div>
                                <div class="time"><?php echo $data['myTime']; //REPLACE        ?></div>
                            </div>
                            <?php if (!empty($data['BFEA_Photo'])) { ?>
                                <img src="<?php echo IMG_LOC_REL . $data['BFEA_Photo']; ?>" alt="<?php echo $data['BFEA_Name']; //REPLACE        ?>"	longdesc="<?php echo '/'; //REPLACE        ?>" />
                            <?php } ?>
                            <div class="copy">

                                <h5>
                                    <?php echo $data['BFEA_Name']; //REPLACE ?>
                                </h5>
                                <p>At: <a href="/profile/<?= $data['BL_Name_SEO'] ?>/<?= $data['BL_ID'] ?>/"><?php echo $data["BL_Listing_Title"]; ?></a></p>
                                <?php echo substr(trim($data['BFEA_Description']), 0, 3) == '<p>' ? $data['BFEA_Description'] : ("<p>" . preg_replace("~\n~m", "<br>", $data['BFEA_Description']) . "</p>"); ?>
                                <?PHP /* <div class="tt-actions">				<a class="planner-link" href="<?php echo '/';//REPLACE ?>"><!--<span class="tripplanner-plus"></span><div>&nbsp;add to trip planner</div>--></a></div> */ ?>
                            </div>
                        </div>
                    </li>
                    <?php
                }
            }
            ?>
        </ul>



    </div><!-- .destination-right -->
<?php } ?>
