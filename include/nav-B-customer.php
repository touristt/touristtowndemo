<?PHP
//Checking if listing is active or inactive
$listingStatus = "";
if ($BL_ID > 0) {
    $sqlADL = "SELECT BL_B_ID, hide_show_listing FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $resultADL = mysql_query($sqlADL, $db) or die("Invalid query: $sqlADL -- " . mysql_error());
    $rowADL = mysql_fetch_assoc($resultADL);
    if ($rowADL['hide_show_listing'] == 0) {
        $listingStatus = "This listing is inactive.";
    }
}
$sqlNav = "SELECT SS_ID FROM tbl_Support_Section LIMIT 1";
$resultNav = mysql_query($sqlNav, $db) or die("Invalid query: $sqlNav -- " . mysql_error());
$rowNav = mysql_fetch_assoc($resultNav);
$sql = "SELECT BL_Listing_Type FROM tbl_Business_Listing WHERE BL_ID = '$BL_ID'";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$row = mysql_fetch_assoc($result);
?>
<div class="main-menu main-menu-customer">
    <ul>
        <li><a href="/index.php">Home</a></li>
        <li><a href="/customer-listing-overview.php?bl_id=<?php echo $BL_ID; ?>">Overview</a></li>
        <li><a href="/customer-listing-contact-details.php?bl_id=<?php echo $BL_ID; ?>">My Page</a></li>
        <li><a href="/customer-feature-add-ons.php?bl_id=<?php echo $BL_ID ?>">My Add Ons</a></li>
        <li><a href="/customer-feature-store.php?bl_id=<?php echo $BL_ID ?>">Store</a></li>
        <?php if ($row['BL_Listing_Type'] != 5) { ?>
            <li><a href="customer-billing.php?bl_id=<?php echo $BL_ID ?>">Invoices</a></li>
        <?php } ?>
        <li><a href="customer-support.php?bl_id=<?php echo $BL_ID ?>&ss_id=<?php echo $rowNav['SS_ID'] ?>">Support</a></li>
        <li><a href="listings-preview.php?bl_id=<?php echo $BL_ID ?>">Preview</a></li>
    </ul>
</div>
