<?php
$filename = basename($_SERVER["SCRIPT_FILENAME"], '.php');
$status = $_REQUEST['status'];
if ($status == 1) {
    $activeclass1 = 'active-class';
} else {
    $activeclass1 = 'not-active';
}
if ($status == 2) {
    $activeclass2 = 'active-class';
} else {
    $activeclass2 = 'not-active';
}
/*if ($status == 3) {
    $activeclass3 = 'active-class';
} else {
    $activeclass3 = 'not-active';
}*/
if ($status == 3  || $filename == 'active-advertisement-stats') {
    $activeclass3 = 'active-class';
} else {
    $activeclass3 = 'not-active';
}
if ($filename == 'inactive_advertisement') {
    $activeclass8 = 'active-class';
} else {
    $activeclass8 = 'not-active';
}
if ($filename == 'active-advertisement-reports') {
    $activeclass4 = 'active-class';
} else {
    $activeclass4 = 'not-active';
}
if ($filename == 'customer-advertisement-townasset') {
    $activeclass5 = 'active-class';
} else {
    $activeclass5 = 'not-active';
}
if ($filename == 'customer-advertisement-createad') {
    $activeclass6 = 'active-class';
} else {
    $activeclass6 = 'not-active';
}
if ($filename == 'customer-advertisement-payments') {
    $activeclass7 = 'active-class';
} else {
    $activeclass7 = 'not-active';
}
if ($filename == 'customer-advertisement-county') {
    $activeclass8 = 'active-class';
} else {
    $activeclass8 = 'not-active';
}
if ($filename == 'inactive_advertisement') {
    $activeclass9 = 'active-class';
} else {
    $activeclass9 = 'not-active';
}
if ($filename == 'delete_advertisement') {
    $activeclass11 = 'active-class';
} else {
    $activeclass11 = 'not-active';
}
if (basename($_SERVER['PHP_SELF']) == 'advertisement-expired.php') {
    $activeclass10 = 'active-class';
} else {
    $activeclass10 = 'not-active';
}
?>
<div class="sub-menu ">
    <ul>
        <?php if (in_array('superadmin', $_SESSION['USER_ROLES'])) { ?>
        <li class="<?php echo $activeclass3 ?>"><a href="/admin/advertisement.php?status=3">Active Campaigns</a></li>
        <li class="<?php echo $activeclass1 ?>" ><a href="/admin/advertisement.php?status=1">Requested Campaigns</a></li>
        <li class="<?php echo $activeclass2 ?>" ><a href="/admin/advertisement.php?status=2">Pending Clients Approval</a></li>
        <li class="<?php echo $activeclass9 ?>" ><a href="/admin/inactive_advertisement.php">Inactive Campaigns</a></li>
        <li class="<?php echo $activeclass11 ?>" ><a href="/admin/delete_advertisement.php">Deleted Campaigns</a></li>
        <li class="<?php echo $activeclass10 ?>" ><a href="/admin/advertisement-expired.php">Expired Campaigns</a></li>
        <li class="<?php echo $activeclass4 ?>" ><a href="/admin/active-advertisement-reports.php">Campaign Statistics</a></li>
        <li class="<?php echo $activeclass5 ?>" ><a href="/admin/customer-advertisement-townasset.php">Create Town Asset Campaign</a></li>
        <?php } if (in_array('county', $_SESSION['USER_ROLES'])) { ?>
        <li class="<?php echo $activeclass8 ?>" ><a href="/admin/customer-advertisement-county.php">Create Campaign</a></li>
        <?php } if (in_array('superadmin', $_SESSION['USER_ROLES'])) {  ?>
        <li class="<?php echo $activeclass6 ?>" ><a href="/admin/customer-advertisement-createad.php">Create Campaign</a></li>
        <li class="<?php echo $activeclass7 ?>" ><a href="/admin/customer-advertisement-payments.php">Campaign Payments</a></li>
        <?php } ?>
    </ul>
</div>
