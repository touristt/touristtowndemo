<?php
require_once 'include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
require_once 'include/class.Pagination.php';
require_once 'include/public/header.php';
?>
<section class="main_slider">
    <div class="slider_wrapper">
        <div class="slider_wrapper_video">
            <div class="sliderCycle" data-cycle-loader="wait" data-cycle-fx="fade" data-cycle-timeout="0" data-cycle-prev="#prev" data-cycle-next="#next">
                <div class="slide-images" style="display: block;">
                    <div class="slider-text slider-listing-text">
                        <div class="watch-video play-button">
                            <div class="slider-button slider-listing-button">
                            </div>
                        </div>
                    </div>                  
                    <img class="slider_image" src="/images/DB/57817251600X640DSC54942.jpg" alt="" longdesc="">                    
                </div>
            </div>
        </div>
    </div>
</section>

<section class="description listing-home-des margin-bottom-none">
    <div class="description-wrapper">
        <div class="event-filter">
            <div class="listing-title-detail">
                <div class="listing-title">Frequently Asked Questions </div>
            </div>
        </div>
    </div>
</section>

<section class="description stories margin-bottom-none">
    <div class="description-wrapper">
        <div class="description-inner padding-none">
            <div class="listing-detail-left" style="width:100%;">
                <div class="listing-desc theme-paragraph ckeditor-anchor-listing"  style="width:100%;">
                    <p>We've collected the top 10 frequently asked questions and have provided the answers below:</p>
                    
                    <p><strong>How do I book the Chi-Cheemaun?</strong></p>
                    <p>To book the Chi-Cheemaun you have to either call 1-800-265-3163 or visit 
                        <a href="https://www.ontarioferries.com/en/home/">ontarioferries.com</a>. Just make sure your booking for the Chi-Cheemaun and 
                        not the other ferry in Ontario. </p>
                    <p>Please note that it is required that you have a reservation if you are taking a vehicle across. With only 140 spaces, they fill up 
                        quick!! If you are not taking a vehicle across and are crossing as a passenger you do not need a reservation, but its always good 
                        to have one just in case.</p>
                    
                    <p><strong>What is this timed parking for the Grotto? How will it affect my visit?</strong></p>
                    <p>The Grotto is a very busy place in the summer, with its crystal blue turquoise waters and gorgeous landscape there's no doubt as to 
                        why. This year Parks Canada has implemented timed parking for Cyprus Lake to hopefully help with the long lines and provide people 
                        with more certainty as to if they will be able to find a spot. 
                        The time<a class="T2f3x7U25Yd3S iwGUvJB8" href="#29013724" title="Click to Continue > by Counterflix"> 
                        slots</a> will be given out on a first come first serve 
                        basis starting at 7am. The slots will only be valid for the day they are picked up - so not preordering, and within the first 2 
                        hours of the time<a class="T2f3x7U25Yd3S iwGUvJB8" href="#76959894" title="Click to Continue > by Counterflix"> 
                        slot</a>. The time slots are:</p>
                    <ul>
                        <li><strong>Time slot 1</strong>:  7:00 a.m. - 11:00 a.m.</li>
                        <li><strong>Time slot 2</strong>: 12:00 p.m. - 4:00 p.m.</li>
                        <li><strong>Time slot 3:</strong> 5:00 p.m. - 9:00 p.m.</li>
                    </ul>
                    <p>For more information on the time<a class="T2f3x7U25Yd3S iwGUvJB8" href="#87036951" title="Click to Continue > by Counterflix"> 
                       slots</a>, please contact Parks Canada or visit:
                       <a target="_blank" href="https://www.pc.gc.ca/en/pn-np/on/bruce/activ/experiences/grotto/parking"> Parks Canada: Grotto Parking</a></p>
                    
                    <p><strong>The Grotto is full! What can I do now?</strong></p>
                    <p>During the summer months it is very likely that you will run into this problem! Parks Canada has begun using time slots to allow 
                        for more visitors to be able to pass through the gates and work their way down to the Grotto. </p>
                    <p>If the Grotto is full and you need something to do to fill up either your entire day or the just the time until your
                       <a class="T2f3x7U25Yd3S iwGUvJB8" href="#21745253" title="Click to Continue > by Counterflix"> 
                       slot</a> comes up, see below for some fantastic ideas!</p>
                    <ul>
                        <li><a href="http://explorethebruce.com/explore/boating-and-watersports-in-bruce-county/91/">Boating and Watersports</a></li>
                        <li><a href="http://explorethebruce.com/explore/boat-tours-and-day-trips/104/">Boat Tours and Day Trips</a></li>
                        <li><a href="http://explorethebruce.com/explore/bruce-trail/76/">The Bruce Trail</a></li>
                        <li><a href="http://explorethebruce.com/explore/glass-bottom-boat-tours-to-flowerpot-island/9/">Glass Bottom Boat Tour</a> to 
                            <a href="http://explorethebruce.com/explore/your-flowerpot-island-experience/67/">Flowerpot Island </a></li>
                        <li><a href="http://explorethebruce.com/explore/lighthouse-touring/62/">Lighthouse Touring</a></li>
                        <li><a href="http://explorethebruce.com/explore/mountain-bike-the-bruce/24/">Mountain Biking</a></li>
                        <li><a href="http://explorethebruce.com/explore/ms-chi-cheemaun/64/">MS Chi-Cheemaun</a></li>
                        <li><a href="http://explorethebruce.com/explore/scuba-diving-and-snorkeling/59/">Scuba Diving and Snorkeling</a></li>
                        <li><a href="http://explorethebruce.com/explore/top-10-things-to-do-on-the-way-to-tobermory/77/">Top things to do on the way to 
                                Tobermory</a> (or while your waiting)</li>
                    </ul>
                    
                    <p><strong>Which beaches are dog friendly?</strong></p>
                    <p>We have five dog friendly beaches. Please remember to keep the dog on a leash and to clean up after it. </p>
                    <ul>
                        <li>Lions Head Beach</li>
                        <li>MacGregor Point Provincial Park</li>
                        <li>Inverhuron Provincial Park</li>
                        <li>Kincardine Station Beach</li>
                        <li>Point Clark Beach</li>
                    </ul>
                    
                    <p><strong>I want to hike for the day on the Bruce Trail - Where should I go?</strong></p>
                    <p>Bruce County is home to over 250km of Bruce Trail, with almost 100kms of that being side trails that create loops and alternate ways 
                        for you to hike the trail!! There are quite a few different hikes you can take that vary in length and difficulty, for example there 
                        is the Wiarton Loop that is 5.6km of easy hiking in the Spirit Rock Conservation Area or Boundary Bluffs that is 5.3km of moderate 
                        to strenuous hiking by Sydney Bluff. For the Top 10 Bruce Trail hikes visit here: 
                        <a href="http://explorethebruce.com/things-to-do/bruce-trail/">Bruce Trail</a>, but there are around 25 loops that can be done on 
                        the Peninsula! For maps on each of these loops you will have to purchase the Bruce Trail Guide from the Bruce Trail Conservancy 
                        online or at one of the locations below. </p>
                    
                    <p><strong>How do I get a Bruce Trail Guidebook and Maps?</strong></p>
                    <p>There are a few places on the Peninsula that you can get your copy of the Bruce Trail Map from the Bruce Trail Conservancy. If you 
                        would like individual maps instead of the whole book you can purchase them from the Bruce Trail Conservancy on your tablet or phone.</p>
                    <ul>
                        <li>McKenszie Pharmacy - 608 Berford Street, Wiarton - 519.534.0230</li>
                        <li>Spirit Rock Outport - 877 Berford Street, Wiarton - 519.534.5168</li>
                        <li>Suntrail Outfitters - 100 Spencer Street, Hepworth - 519.935.2478</li>
                        <li>McIvor House B&B - 952 Purple Valley Road, Cape Croker - 519.534.1769</li>
                        <li>Cape Croker Park - 112 Park Road, Cape Croker - 519.534.0571</li>
                        <li>Cedarholme B&B - 108 Beech Street, Hope Bay - 519.534.3705</li>
                        <li>Ferndale Tourist Centre - 2928 Highway 6, Ferndale - 519.793.5474</li>
                        <li>Pensinsula Pharmacy - 76 Main Street, Lion's Head - 519.793.3200</li>
                        <li>Marydale's Restaurant - 76 Main Street, Lion's Head - 519.793.4224</li>
                        <li>Taylor-Made B&B - 31 Bryon Street, Lion's Head - 519.793.4853</li>
                        <li>Miller Family Camp - 108 Miller Lake Shore Road, Miller Lake - 519.795.7750</li>
                        <li>Summer House Park - 197 Miller Lake Shore Road, Miller Lake - 519.795.7712</li>
                        <li>Much More Camping - 7405 Highway 6, Tobermory - 519.596.2000</li>
                        <li>Thorncrest Outfitters - 7441 Highway 6, Tobermory - 519.596.8908</li>
                        <li>Verna's Gift Shop - 11 Bay Street, Tobermory - 519.596.2351</li>
                        <li>Mariner Chart Shop - 17 Bay Street, Tobermory - 519.596.2999</li>
                        <li>National Park Visitor Centre - 7374 Highway 6, Tobermory - 519.596.8181 / 519.596.2233</li>
                        <li>Reader's Haven Book Store - 10 Bay Street, Tobermory - 519.596.2359</li>
                    </ul>
                    
                    <p><strong>Is there transportation available from Toronto to Tobermory?</strong></p>
                    <p>Parkbus does run a bus from Toronto to the Bruce Peninsula National Park on a regular basis throughout the summer, for availability and 
                        rates you can visit them at <a target="_blank" href="https://parkbus.ca/bruce">Parkbus.ca</a>. Alternatively, 
                        <a target="_blank" href="https://www.greyhound.ca/">Grey Hound</a> runs a bus from Toronto to Owen Sound and then First Student 
                        runs from Owen Sound to Tobermory - with a few stops along the way, for availability and rates please contact them at 519.376.5712. </p>
                    
                    <p><strong>How do I visit Chantry Lighthouse?</strong></p>
                    <p><a href="http://explorethebruce.com/profile/chantry-island-lighthouse-tour/855/">Chantry Lighthouse</a> is located off the coast of 
                        Southampton on a bird sanctuary island and is only available for visiting via a 15-minute boat ride from the Marine Heritage Society. 
                        This is a max 12 person - 2-hour tour of the lighthouse. The cost is $30 and it is highly recommended that you call ahead and book your 
                        tour as this is only run 3 times a day during the busy season.</p>
                    
                    <p><strong>Can I camp on Flowerpot Island?</strong></p>
                    <p>The only spot you can camp in the Fathom Five National Marine Park is on Flowerpot Island, there are a total of 6 tent camping spots on 
                        the island with a wooden tent platform. The only way onto the island is via boat either your own, kayak, or via private boat tour from 
                        Tobermory. Camping permits are required. Please check the Parks Canada website for more information: 
                        <a href="http://www.pc.gc.ca/eng/amnc-nmca/on/fathomfive/activ/activ1.aspx">Parks Canada</a>.</p>
                    
                    <p><strong>Where can I go Mountain Biking?</strong></p>
                    <p>Bruce County is the go to destination for Mountain Biking in Ontario. With 4 different tracts filled with trails running over rock, 
                        gravel, through trees and testing the limits, it'll be a fun and crazy ride! For more information on Mountain Biking and the various 
                        locations please visit: <a href="http://explorethebruce.com/explore/mountain-bike-the-bruce/24/">Bruce County Mountain Biking</a>. 
                        <strong>Remember to always ride within your own level of ability, with a helmet on and at your own risk.</strong></p>                                                        </div>
            </div>
        </div>
    </div>
</section>


<?php require_once 'include/public/footer.php'; ?>
