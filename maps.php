<?php
require_once 'include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
require_once 'include/class.Pagination.php';
require_once 'include/public/map_header.php';
?>
<!--Map section-->
<div class="sidebar_map">
    <?PHP
    $sql_stories = "SELECT S_ID, S_Title, S_Thumbnail, S_Category, C_Name, C_Parent, RC_Name from tbl_Story LEFT JOIN tbl_Story_Homepage_Category ON S_ID = SHC_S_ID
                  LEFT JOIN tbl_Category ON S_Category = C_ID
                  LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID
                  WHERE SHC_R_ID ='" . $REGION['R_ID'] . "' 
                  AND SHC_Map = 1 AND S_Active = 1 GROUP BY S_ID ORDER BY SHC_Order ASC";
    $result_stories = mysql_query($sql_stories, $db) or die("Invalid query: $sql_stories -- " . mysql_error());
    $stories_count = mysql_num_rows($result_stories);
    if ($stories_count > 0 && $REGION['R_Stories'] == 1) {
        while ($stories = mysql_fetch_assoc($result_stories)) {
            if ($stories['S_Thumbnail'] != '') {
                $story_image = $stories['S_Thumbnail'];
            } else {
                $story_image = $default_thumbnail_image;
            }
            ?>
            <div class="thumbnails static-thumbs">
                <div class="thumb-item title">
                    <a href="/<?php echo $storyCatSEORow['C_Name_SEO'] ?>/<?php echo clean($stories['S_Title']) ?>/<?php echo isset($stories['S_ID']) ? $stories['S_ID'] : ''; ?>/">
                        <img src="<?php echo IMG_LOC_REL .$story_image ?>" alt="<?php echo $stories['S_Title'] ?>">
                        <h3 class="thumbnail-heading"><?php echo ($stories['RC_Name'] != '') ? $stories['RC_Name'] : $stories['C_Name'] ?></h3>
                        <h3 class="thumbnail-desc"><?php echo $stories['S_Title'] ?></h3>
                    </a>
                </div>
            </div>
            <?php
        }
    }
    if ($REGION['R_Parent'] == 0) {
        $REG = '';
    } else {
        $REG = "AND BLP_R_ID = " . $REGION['R_ID'];
    }
    //get all listings
    $sqlList = "SELECT DISTINCTROW BL_ID, BL_Listing_Title, BL_Name_SEO, BL_Lat, BL_Long, BL_Street, BL_Town, BLP_Photo, BL_Photo_Alt, BLC_M_C_ID, 
                RC.RC_Name, RC.RC_Map_Icon as subcat_icon, RC1.RC_Map_Icon as cat_icon, C_Name
                FROM tbl_Business_Listing 
                LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
                LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
                INNER JOIN tbl_Business_Listing_Category_Region ON  BLCR_BL_ID = BL_ID 
                INNER JOIN tbl_Region_Category RC ON RC.RC_C_ID = BLC_C_ID AND RC.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                INNER JOIN tbl_Region_Category RC1 ON RC1.RC_C_ID = BLC_M_C_ID AND RC1.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                LEFT JOIN tbl_Category ON RC.RC_C_ID = C_ID
                LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
                LEFT JOIN tbl_Business_Listing_Order ON BL_ID = BLO_BL_ID AND BLO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
                AND BLO_S_C_ID = BLC_C_ID
                $SEASONS_JOIN
                WHERE BLCR_BLC_R_ID " . encode_strings((($REGION['R_Parent'] == 0 && $REGION['R_Type'] != 4 ) ? "IN " . $regionList : "= " . $REGION['R_ID']), $db) . " 
                AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing='1' AND RC.RC_Status = 0 AND RC1.RC_Status = 0 $REG $SEASONS_WHERE";
    if (isset($_GET['cid']) && $_GET['cid'] > 0) {
        $cid = $_GET['cid'];
        $sqlList .= " AND BLC_M_C_ID = '" . encode_strings($cid, $db) . "'";
    }
    if (isset($_GET['subCategory'])) {
        $cid = $_GET['subCategory'];
        $sqlList .= " AND BLC_C_ID = '" . encode_strings($cid, $db) . "'";
    }
    $sqlList .= " $include_free_listings_on_map GROUP BY BL_ID $order_listings";
    // generate thumbnails for listings on left side bar and markers for map
    $resList = mysql_query($sqlList, $db) or die(mysql_error() . ' - ' . $sqlList);
    $counter = 0;
    while ($List = mysql_fetch_assoc($resList)) {
        //add to markers for map
        if ($List['BLP_Photo'] != '') {
            $image = '<div class="thumbnail"><img width="130" height="90" src="' . (IMG_LOC_REL . $List['BLP_Photo']) . '" alt="' . htmlspecialchars(trim($List['BL_Photo_Alt']), ENT_QUOTES) . '" /></div>';
        } else {
            $image = '<div class="thumbnail"><img src="' . (IMG_LOC_REL . $default_thumbnail_image) . '" alt="' . htmlspecialchars(trim($List['BL_Listing_Title']), ENT_QUOTES) . '" /></div>';
        }
        if ($List['subcat_icon'] != '') {
            $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $List['subcat_icon'];
        } elseif ($List['cat_icon'] != '') {
            $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $List['cat_icon'];
        } else {
            $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . 'default.png';
        }
        if ($List['BL_Lat'] && $List['BL_Long']) {
            $markers[] = array(
                'id' => $List['BL_ID'],
                'lat' => $List['BL_Lat'],
                'lon' => $List['BL_Long'],
                'name' => $List['BL_Listing_Title'],
                'path' => '/profile/' . $List['BL_Name_SEO'] . '/' . $List['BL_ID'] . '/',
                'icon' => $icon,
                'main_photo' => $image,
                'address' => htmlspecialchars(trim($List['BL_Street']), ENT_QUOTES),
                'town' => htmlspecialchars(trim($List['BL_Town']), ENT_QUOTES)
            );
        }
        //show only limited records in right panel
        if ($counter < $REGION['R_Map_Listings_Limit'] && ($stories_count == 0 || $REGION['R_Stories'] == 0)) {
            ?>
            <div class='thumbnails static-thumbs'>
                <div class="thumb-item">
                    <a href="/profile/<?php echo $List['BL_Name_SEO'] ?>/<?php echo $List['BL_ID'] ?>/"> 
                        <img src="<?php echo ($List['BLP_Photo'] == '') ? (IMG_LOC_REL . $default_thumbnail_image) : (IMG_LOC_REL . $List['BLP_Photo']) ?>" alt="<?php echo $List['BL_Photo_Alt'] ?>" />
                        <h3 class="thumbnail-heading"><?php echo ($List['RC_Name'] != '') ? $List['RC_Name'] : $List['C_Name'] ?></h3>
                        <h3 class="thumbnail-desc"><?php echo $List['BL_Listing_Title']; ?></h3>
                    </a> 
                </div>
            </div>
            <?php
        }
        $counter++;
    }
    $markers = json_encode($markers);
    ?>
</div><!-- .nav-left-outer -->

<div class="map_wrapper">
    <div id="map_canvas">&nbsp;</div>
</div> <!-- .content-right-outer -->
<?php
$map_center['latitude'] = $REGION['R_Lat'];
$map_center['longitude'] = $REGION['R_Long'];
$map_center['zoom'] = $REGION['R_Zoom'];
$kml_json = json_encode(array());
require_once 'map_script.php';
?>
