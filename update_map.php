<?php

require_once 'include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
require_once 'include/class.Pagination.php';

//whether coming from map page or not
$is_map = $_REQUEST['is_map'];

//if coming from route pages
$is_route = (isset($_REQUEST['is_route'])) ? $_REQUEST['is_route'] : 0;

$where = "";
$ROUTE_JOIN = "";
$markers = array();
$kml_array = array();
$regionList = '';
if ($REGION['R_Parent'] == 0) {
    $sql = "SELECT RM_Child FROM tbl_Region_Multiple LEFT JOIN tbl_Region ON RM_Child = R_ID WHERE RM_Parent = '" . encode_strings($REGION['R_ID'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $first = true;
    $regionList .= "(";
    while ($row = mysql_fetch_assoc($result)) {
        if ($first) {
            $first = false;
        } else {
            $regionList .= ",";
        }
        $regionList .= $row['RM_Child'];
    }
    $regionList .= ")";
}

//For categories filter
if (isset($_REQUEST['category'])) {
    $category = true;
    $scroll = true; // to scroll page to filters area if a filter is applied
    $where .= " AND (";
    $i = 0;
    $j = 0;
    $subcats = array();
    foreach ($_REQUEST['category'] as $category) {
        $k = 0;
        foreach ($_REQUEST['sub_category'][$category] as $subcategory) {
            $subcats[] = $subcategory;
            if ($i == 0 && $j == 0) {
                $where .= " (BLC_M_C_ID = $category AND BLC_C_ID = $subcategory)";
            } else {
                $where .= " OR (BLC_M_C_ID = $category AND BLC_C_ID = $subcategory)";
            }
            $i++;
            $k++;
        }
        if ($k == 0) {
            if ($j == 0) {
                $where .= " (BLC_M_C_ID = $category)";
            } else {
                $where .= " OR (BLC_M_C_ID = $category)";
            }
        }
        $j++;
    }
    foreach ($_REQUEST['sub_category'] as $subcategory) {
        foreach ($subcategory as $subcat) {
            if (!in_array($subcat, $subcats)) {
                $where .= " OR (BLC_C_ID = $subcat)";
            }
        }
    }
    $where .= ")";
} elseif (isset($_REQUEST['sub_category']) && !isset($_REQUEST['category'])) {
    $i = 0;
    $where .= " AND (";
    foreach ($_REQUEST['sub_category'] as $subcategory) {
        foreach ($subcategory as $subcat) {
            if ($i == 0) {
                $where .= " (BLC_C_ID = $subcat)";
            } else {
                $where .= " OR (BLC_C_ID = $subcat)";
            }
            $i++;
        }
    }
    $where .= ")";
}
//Routes filter
$routes = array();
if (isset($_REQUEST['route'])) {
    $i = 0;
    $ROUTE_JOIN = " INNER JOIN tbl_Individual_Route_Map_Listings ON IRML_BL_ID = BL_ID 
                    INNER JOIN tbl_Individual_Route ON IR_ID = IRML_IR_ID";
    $where .= " AND (";
    foreach ($_REQUEST['route'] as $route) {
        $routes[] = $route;
        if ($i == 0) {
            $where .= " IR_ID = $route";
        } else {
            $where .= " OR IR_ID = $route";
        }
        $i++;
    }
    $where .= ")";
    // load only specific kml files for the selected routes
    $sqlKML = "SELECT IR_Title, IR_KML_Path FROM tbl_Individual_Route WHERE IR_KML_Path != '' AND IR_ID IN (" . implode(',', $routes) . ")";
    $resKML = mysql_query($sqlKML);
    while ($KML = mysql_fetch_assoc($resKML)) {
        $kml_array[] = array(
            'name' => str_replace(' ', '', $KML['IR_Title']),
            'url' => 'http://' . DOMAIN . MAP_LOC_REL . $KML['IR_KML_Path']
        );
    }
} else if (isset($_REQUEST['route_category'])) {
    $route_category = $_REQUEST['route_category'];
    $ROUTE_JOIN = " INNER JOIN tbl_Individual_Route_Map_Listings ON IRML_BL_ID = BL_ID 
                    INNER JOIN tbl_Individual_Route ON IR_ID = IRML_IR_ID
                    INNER JOIN tbl_Individual_Route_Category ON IRC_IR_ID = IR_ID";
    $where .= " AND IRC_RC_ID = $route_category";

    //Load kml files
    //get routes of this category
    $sqlKML = "SELECT IR_Title, IR_KML_Path FROM tbl_Individual_Route 
                INNER JOIN tbl_Individual_Route_Category ON IR_ID = IRC_IR_ID 
                WHERE IRC_RC_ID = $route_category AND IR_KML_Path != ''
                GROUP BY IR_ID";
    $resKML = mysql_query($sqlKML);
    while ($KML = mysql_fetch_assoc($resKML)) {
        $kml_array[] = array(
            'name' => str_replace(' ', '', $KML['IR_Title']),
            'url' => 'http://' . DOMAIN . MAP_LOC_REL . $KML['IR_KML_Path']
        );
    }
}

$SEASONS_JOIN = "";
$StorySeasons_JOIN = "";
$SEASONS_WHERE = "";
$StorySeasons_WHERE = "";
if (isset($_SESSION['SEASONS']) && $_SESSION['SEASONS'] > 0) {
    $SEASONS_JOIN = " LEFT JOIN tbl_Business_Listing_Season ON BL_ID = BLS_BL_ID";
    $StorySeasons_JOIN = " LEFT JOIN tbl_Story_Season ON S_ID = SS_S_ID";
    $SEASONS_WHERE = " AND BLS_S_ID = '" . encode_strings($_SESSION['SEASONS'], $db) . "'";
    $StorySeasons_WHERE = " AND SS_Season = '" . encode_strings($_SESSION['SEASONS'], $db) . "'";
}
if ($REGION['R_Include_Free_Listings_On_Map'] == 1) {
    $include_free_listings_on_map = '';
} else {
    $include_free_listings_on_map = ' AND BL_Listing_Type > 1';
}
if ($REGION['R_Order_Listings_Manually'] == 1) {
    $order_listings = 'ORDER BY BLO_Order ASC, LT_Order DESC, BL_Listing_Title';
} else {
    $order_listings = 'ORDER BY BL_Points DESC, LT_Order DESC, BL_Listing_Title';
}
if ($REGION['R_Parent'] == 0) {
    $REG = '';
} else {
    $REG = "AND BLP_R_ID = " . $REGION['R_ID'];
}

//Getting Region Theme
$getTheme = "SELECT TO_Default_Thumbnail_Desktop FROM  `tbl_Theme_Options` WHERE TO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
$themeRegion = mysql_query($getTheme, $db) or die("Invalid query: $getTheme -- " . mysql_error());
$THEME = mysql_fetch_assoc($themeRegion);

//Defualt Thumbnail Images
if ($THEME['TO_Default_Thumbnail_Desktop'] != '') {
    $default_thumbnail_image = $THEME['TO_Default_Thumbnail_Desktop'];
} else {
    $default_thumbnail_image = 'Default-Thumbnail-Desktop.jpg';
}

//if from pages other than map
if ($is_route == 1) {
    $sql = "SELECT DISTINCTROW BL_ID, BL_Listing_Title, BL_Name_SEO, BL_Lat, BL_Long, BL_Street, BL_Town, BLP_Photo, BL_Photo_Alt, 
            RC.RC_Map_Icon as subcat_icon, RC1.RC_Map_Icon as cat_icon FROM tbl_Business_Listing 
            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
            LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
            INNER JOIN tbl_Individual_Route_Map_Listings ON IRML_BL_ID = BL_ID 
            INNER JOIN tbl_Individual_Route ON IR_ID = IRML_IR_ID
            INNER JOIN tbl_Individual_Route_Category ON IRC_IR_ID = IR_ID
            INNER JOIN tbl_Region_Category RC ON RC.RC_C_ID = BLC_C_ID AND RC.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            INNER JOIN tbl_Region_Category RC1 ON RC1.RC_C_ID = BLC_M_C_ID AND RC1.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            $SEASONS_JOIN
            WHERE (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing='1' AND RC.RC_Status = 0 AND RC1.RC_Status = 0 $where $REG
            $SEASONS_WHERE $include_free_listings_on_map
            ORDER BY IRML_Order DESC";
} else if ($is_map != 1) {
    $sql = "SELECT DISTINCTROW BL_ID, BL_Listing_Title, BL_Name_SEO, BL_Lat, BL_Long, BL_Street, BL_Town, BLP_Photo, BL_Photo_Alt, 
            RC.RC_Map_Icon as subcat_icon, RC1.RC_Map_Icon as cat_icon FROM tbl_Business_Listing 
            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
            LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
            INNER JOIN tbl_Business_Listing_Category_Region ON  BLCR_BL_ID = BL_ID 
            INNER JOIN tbl_Region_Category RC ON RC.RC_C_ID = BLC_C_ID AND RC.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            INNER JOIN tbl_Region_Category RC1 ON RC1.RC_C_ID = BLC_M_C_ID AND RC1.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
            LEFT JOIN tbl_Business_Listing_Order ON BL_ID = BLO_BL_ID AND BLO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
            AND BLO_S_C_ID = BLC_C_ID
            $ROUTE_JOIN
            $SEASONS_JOIN
            WHERE BLCR_BLC_R_ID " . encode_strings((($REGION['R_Parent'] == 0 && $REGION['R_Type'] != 4 ) ? "IN " . $regionList : "= " . $REGION['R_ID']), $db) . " $where 
            AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing='1' AND RC.RC_Status = 0 AND RC1.RC_Status = 0 $SEASONS_WHERE $include_free_listings_on_map $REG
            GROUP BY BL_ID $order_listings";
} else {
    $sql = "SELECT DISTINCTROW BL_ID, BL_Listing_Title, BL_Name_SEO, BL_Lat, BL_Long, BL_Street, BL_Town, BLP_Photo, BL_Photo_Alt, 
            RC.RC_Name, RC.RC_Map_Icon as subcat_icon, RC1.RC_Map_Icon as cat_icon, C_Name FROM tbl_Business_Listing 
            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
            LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
            INNER JOIN tbl_Business_Listing_Category_Region ON  BLCR_BL_ID = BL_ID
            INNER JOIN tbl_Region_Category RC ON RC.RC_C_ID = BLC_C_ID AND RC.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            INNER JOIN tbl_Region_Category RC1 ON RC1.RC_C_ID = BLC_M_C_ID AND RC1.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            LEFT JOIN tbl_Category ON RC.RC_C_ID = C_ID
            LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
            LEFT JOIN tbl_Business_Listing_Order ON BL_ID = BLO_BL_ID AND BLO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
            AND BLO_S_C_ID = BLC_C_ID
            $ROUTE_JOIN
            $SEASONS_JOIN
            WHERE BLCR_BLC_R_ID " . encode_strings((($REGION['R_Parent'] == 0 && $REGION['R_Type'] != 4 ) ? "IN " . $regionList : "= " . $REGION['R_ID']), $db) . " $where 
            AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing='1' AND RC.RC_Status = 0 AND RC1.RC_Status = 0 $SEASONS_WHERE $include_free_listings_on_map $REG
            GROUP BY BL_ID $order_listings";
}
// generate icons for all listings on map regardless of page
$result_map = mysql_query($sql) or die(mysql_error() . ' - ' . $sql);
$counter = 0;
$sidebar = '';
while ($list = mysql_fetch_array($result_map, MYSQL_ASSOC)) {
    if ($list['subcat_icon'] != '') {
        $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $list['subcat_icon'];
    } elseif ($list['cat_icon'] != '') {
        $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $list['cat_icon'];
    } else {
        $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . 'default.png';
    }
    if ($list['BL_Lat'] && $list['BL_Long']) {
        // Add tooltip content
        if ($list['BLP_Photo'] != '') {
            $image = '<div class="thumbnail"><img width="130" height="90" src="' . (IMG_LOC_REL . $list['BLP_Photo']) . '" alt="' . htmlspecialchars(trim($list['BL_Photo_Alt']), ENT_QUOTES) . '" /></div>';
        } else {
            $image = '<div class="thumbnail"><img src="' . (IMG_LOC_REL . $default_thumbnail_image) . '" alt="' . htmlspecialchars(trim($list['BL_Listing_Title']), ENT_QUOTES) . '" /></div>';
        }
        $markers[] = array(
            'id' => $list['BL_ID'],
            'lat' => $list['BL_Lat'],
            'lon' => $list['BL_Long'],
            'name' => $list['BL_Listing_Title'],
            'path' => '/profile/' . trim($list['BL_Name_SEO']) . '/' . $list['BL_ID'] . '/',
            'icon' => $icon,
            'main_photo' => $image,
            'address' => htmlspecialchars(trim($list['BL_Street']), ENT_QUOTES),
            'town' => htmlspecialchars(trim($list['BL_Town']), ENT_QUOTES)
        );
    }
    //show only limited records in right panel only on map page 
    if ($is_map == 1) {
        if ($counter < $REGION['R_Map_Listings_Limit']) {
            $photo = ($list['BLP_Photo'] == '') ? (IMG_LOC_REL . $default_thumbnail_image) : (IMG_LOC_REL . $list['BLP_Photo']);
            $catname = ($list['RC_Name'] != '') ? $list['RC_Name'] : $list['C_Name'];
            $sidebar .= '<div class="thumbnails static-thumbs">
                    <div class="thumb-item">
                      <a href="/profile/' . $list['BL_Name_SEO'] . '/' . $list['BL_ID'] . '/"> 
                        <img src="' . $photo . '" alt="' . $list['BL_Photo_Alt'] . '" />
                        <h3 class="thumbnail-heading">' . $catname . '</h3>
                        <h3 class="thumbnail-desc">' . $list['BL_Listing_Title'] . '</h3>
                      </a> 
                    </div>
                  </div>';
        }
    }
    $counter++;
}
$return['markers'] = $markers;
$return['kml'] = $kml_array;
$return['sidebar'] = $sidebar;
print json_encode($return);
exit;
?>