<?PHP
	require_once 'config.inc.php';
	if ($_POST['op'] == 'upld') {
		require_once 'upload.inc.php';
		$orgFilename = $_FILES['file']['name'][0];
		$file = Upload(0);
		 
		$imgSize = getimagesize(FILE_LOC . $file);
		$picW = 1000;
		if ($imgSize[0] > $picW) {
			if ($imgSize[2] == 1) {
				$img_r = imagecreatefromgif(FILE_LOC . $file);
			} elseif ($imgSize[2] == 2) {
				$img_r = imagecreatefromjpeg(FILE_LOC . $file);
			} elseif ($imgSize[2] == 3) {
				$img_r = imagecreatefrompng(FILE_LOC . $file);
			}
			$picH = getHeight($imgSize[0], $imgSize[1], $picW);
			
			$im = imagecreatetruecolor ($picW, $picH)
      				or die ("Cannot Initialize new GD image stream");
					
			imagecopyresampled($im, $img_r, 0, 0, 0, 0, $picW, $picH, imagesx($img_r), imagesy($img_r));
			
			if ($imgSize[2] == 1) {
				imagegif ($im, FILE_LOC . $file);
			} elseif ($imgSize[2] == 2) {
				imagejpeg ($im, FILE_LOC . $file, 82);
			} elseif ($imgSize[2] == 3) {
				imagepng ($im, FILE_LOC . $file, 82);
			}
			
		 }
	} elseif ($_POST['op'] == 'crop') {
		$filename = FILE_LOC . $_POST['img'];
		
		$imgSize = getimagesize($filename);
		
		if ($imgSize[2] == 1) {
			$img_r = imagecreatefromgif($filename);
		} elseif ($imgSize[2] == 2) {
			$img_r = imagecreatefromjpeg($filename);
		} elseif ($imgSize[2] == 3) {
			$img_r = imagecreatefrompng($filename);
		} else {
			exit();
		}
		
		$dst_r = ImageCreateTrueColor( $_POST['targ_w'], $_POST['targ_h'] );
	
		imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],
			$_POST['targ_w'],$_POST['targ_h'],$_POST['w'],$_POST['h']);
		
		
		
		if(ini_get('zlib.output_compression')) {
		  ini_set('zlib.output_compression', 'Off');
		}
		
		$file_extension = getFileExtension ($_POST['imgORG']);
		$output_filename = str_replace(".".$file_extension, '', $_POST['imgORG']);
		
		if( $filename == "" ) {
		  echo "<html><title>" . COMPANY_NAME . "</title><body>ERROR: download file NOT SPECIFIED.</body></html>";
		  exit;
		} elseif ( ! file_exists( $filename ) ){
		  echo "<html><title>" . COMPANY_NAME . "</title><body>ERROR: File not found.</body></html>";
		  exit();
		}
		
		$ctype="application/force-download";
		
		header("Pragma: public"); // required
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: private",false); // required for certain browsers 
		header("Content-Type: $ctype");
		
		header("Content-Disposition: attachment; filename=\"" . $output_filename . "-resize-" . $_POST['targ_w'] . "x" . $_POST['targ_h'] . ".jpg\";" );
		header("Content-Transfer-Encoding: binary");
		imagejpeg($dst_r,null,90);
		exit();
	}


?><!DOCTYPE HTML>

<html lang="en" class="no-js">
<head>
<meta charset="utf-8">
<title>Tourist Town - Cropping Tool</title>
<link rel="stylesheet" href="/cs.css">

<link rel="stylesheet" href="/jquery.jcrop.css">
<script src="/jquery.js"></script>
<script src="/jquery.jcrop.js"></script>
<?PHP
	if ($file) {
		echo "<script>var jcrop_api;
		
		$(window).load(function(){

		
		jcrop_api = $.Jcrop('#mainImage');
		jcrop_api.setOptions({
			onChange: showCoords,
			onSelect: showCoords,
			aspectRatio: 1
		});

		});
var myArrSizing=new Array(); 
myArrSizing[0]=['680','310']; 
myArrSizing[1]=['170','90'];
myArrSizing[2]=['460','300'];
myArrSizing[3]=['460','300'];
myArrSizing[4]=['200','260'];
myArrSizing[5]=['160','190'];
myArrSizing[6]=['490','250'];
myArrSizing[7]=['160','100'];


function showCoords(c)
{
	$('#x').val(c.x);
	$('#y').val(c.y);
	$('#w').val(c.w);
	$('#h').val(c.h);
};

function changeDim (myValue) {
	$('#targ_w').val(myArrSizing[myValue][0]);
	$('#targ_h').val(myArrSizing[myValue][1]);
	jcrop_api.setOptions({
				aspectRatio: $('#targ_w').val() / $('#targ_h').val()
				
			});
	jcrop_api.animateTo( [ 0, 0, $('#targ_w').val(), $('#targ_h').val() ] );
}</script>";
	}
?>
<script>

</script>
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top"><table width="800" border="0" cellspacing="0" cellpadding="40">
      <tr>
        <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr bgcolor="#FFFFFF">
            <td><table width="100%" border="0" cellspacing="0" cellpadding="10">
              <tr bgcolor="#FFFFFF">
                <td class="text-grey"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="text-green-Heading">Image Resizing Tool</td>
                    <td align="right"><img src="images/tt-logo.gif" width="236" height="56" alt="Canvas Studios" /></td>
                    </tr>
                  <tr>
                    <td colspan="2">&nbsp;</td>
                    </tr>
                  <tr>
                    <td colspan="2"><table width="800" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td colspan="2"><img src="images/greyline.gif" alt="" width="100%" height="5" vspace="20" /></td>
                        </tr>
                      <tr>
                        <td colspan="2"><p><span class="text-green-bold">Step 1</span><br /> 
                          To resize your image for your Tourist Town listing,  select the photo you want to resize. <br />
                          </p>
                          <ol>
                            <li>Click &quot;Browse...&quot; button</li>
                            <li>Select the photo you want to use</li>
                            <li> Click &quot;Upload&quot;</li>
                          </ol></td>
                        </tr>
                      <tr>
                        <td colspan="2">&nbsp;</td>
                        </tr>
                      <tr>
                                  <td width="800" colspan="2"><form action="/" method="POST" enctype="multipart/form-data"><span class="text-green-bold">1. Select Image To Crop
                          </span>
                                    
					  	              <input type="hidden" name="op" value="upld">
                                      <input name="file[0]" type="file" id="file[0]">
                                      <input type="submit" name="Submit" value="Upload">
      
                                    </form></td>
                        </tr>
                      <tr>
                        <td colspan="2"><img src="images/greyline.gif" alt="" width="100%" height="5" vspace="20" /></td>
                        </tr>
						</table>
						      <form name="formSave" id="formSave" method="post" action="/">
						
                    <input type="hidden" name="op" value="crop">
						
                    <input type="hidden" name="img" id="img" value="<?=$file?>">
						
                    <input type="hidden" name="imgORG" id="imgORG" value="<?=$orgFilename?>">
						
                    <input type="hidden" id="x" name="x" />
						
                    <input type="hidden" id="y" name="y" />
						
                    <input type="hidden" id="w" name="w" />
						
                    <input type="hidden" id="h" name="h" />
					<input type="hidden" name="targ_w" id="targ_w"  value="150" />
					<input type="hidden" name="targ_h" id="targ_h"  value="150" />
                                <table width="800" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td colspan="2"><p><span class="text-green-bold">Step 2</span> <br />
                          Once your image is uploaded you can now crop the image and resize it to fit on your Tourist Town listing.<br />
                        </p>
                          <ol>
                            <li>Click on the dropdown menu &quot;Select Cropping Size&quot;. </li>
                            <li>Select the type of image you want to save (e.g. Products - 200 x 260)</li>
                            <li>You will notice a rectangle will appear with 8 nodes and a dotted line. This is the shape that your image will be saved as. Click on one of the 8 nodes and you can make the cropping area bigger or smaller.</li>
                            <li>When you are happy - click &quot;Save image Now&quot;.</li>
                            <li>The image will save to your computer using the same file name and dimensions.(e.g. &quot;myphoto-170x90.jpg)</li>
                          </ol></td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                  </tr>
                                  <tr>
                                    <td colspan="2"><span class="text-green-bold">2.</span>
                          <select name="sizeList" id="sizeList" onChange="changeDim(this.value);" <?=$file?'':'disabled'?>>
                                        <option selected="selected">Select Cropping Size</option>
                                        <option value="0">Home Main Photo - 680 x 310</option>
                                        <option value="1">Thumbnail - 170 x 90</option>
                                        <option value="2">About Us - 460 x 300</option>
                                        <option value="3">Photo Gallery - 460 x 300</option>
                                        <option value="4">Products - 200 x 260</option>
                                        <option value="5">Meet The Team - 160 x 190</option>
                                        <option value="6">Meet The Chef - 490 x 250</option>
										<option value="7">Entertainment - 160 x 100</option>
                            </select>
                                      <input type="button" name="Submit2" value="Set Size" onClick="changeDim($('#sizeList').val());">
                          <span class="text-green-bold">
                                      <input name="button3" type="submit" id="button3" value="Save Image Now" <?=$file?'':'disabled'?> />
                            </span></td>
                                  </tr>
                                  <tr>
                                    <td colspan="2">&nbsp;</td>
                                  </tr>
								  </table>
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
								  <tr>
                                    <td colspan="2"><?PHP if ($file) { ?><img id="mainImage" src="<?=FILE_URL?><?=$file?>" alt="" /><?PHP } else { echo '&nbsp;'; } ?></td>
                                  </tr>
                                </table>
                                                              </form>    </td>
                    </tr>
                  </table></td>
                </tr>
              <tr>
                <td align="left" class="text-grey"><?PHP if ($file) { ?><span class="text-red">Uploaded File: </span><?=$orgFilename?><?PHP } else { echo '&nbsp;'; } ?></td>
                </tr>
              </table></td>
          </tr>
          </table></td>
      </tr>
    </table></td>
  </tr>
</table>

</body>
</html>
