// JavaScript Document
$(function () {
  
  'use strict';

    // Initialize the jQuery File Upload widget:
    
	$('#fileupload').bind('fileuploaddone', function (e, data) {
			$.each(data.result, function (index, file) {
              $('#mainImageName').html(file.name);
			  $('#imgORG').val(file.name);
			   	$('#mainImage').attr("src", '/resize/upld/example/files/'+file.name);
			    

            });

		
			
});

	$('#fileupload').fileupload();
	
	$('#mainImage').Jcrop({
					aspectRatio: 1,
					onSelect: updateCoords
				});
	
	
	$('#fileupload').fileupload(
    'option',
    'autoUpload',
    'true'
);
  // Load existing files:
    $.getJSON($('#fileupload form').prop('action'), function (files) {
        var fu = $('#fileupload').data('fileupload');
        fu._adjustMaxNumberOfFiles(-files.length);
        fu._renderDownload(files)
            .appendTo($('#fileupload .files'))
            .fadeIn(function () {
                // Fix for IE7 and lower:
                $(this).show();
            });
    });

    // Open download dialogs via iframes,
    // to prevent aborting current uploads:
    $('#fileupload .files a:not([target^=_blank])').live('click', function (e) {
        e.preventDefault();
        $('<iframe style="display:none;"></iframe>')
            .prop('src', this.href)
            .appendTo('body');
    });
});
