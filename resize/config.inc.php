<?PHP
	
	error_reporting(E_ERROR | E_WARNING | E_PARSE);
	ini_set('display_errors', 'on');  	
	$max_emails						= 10;
	
	define("ABS_ROOT", 				"/home/touristt/public_html/resize/");
	define("WEB_ROOT", 				ABS_ROOT . "");
	define("FILE_LOC",				WEB_ROOT . "DB/");
	define("FILE_URL",				"/DB/");
	define("DOMAIN", 				"resize.touristtown.ca/"); 
	define("COMPANY_NAME", 			"Tourist Town");
	define("MAIN_CONTACT_NAME", 	"Tourist Town");
	define("MAIN_CONTACT_EMAIL",	"info@touristtown.ca");
	define("GOOGLE_API_KEY", 		"");
	
	
	
	
	ini_set("session.gc_maxlifetime", 65*60);
	ini_set("session.cookie_lifetime", 12*60*60);
	//ini_set("session.save_path", ABS_ROOT . "tmp/");  
	
	session_start();
	
	if ($_SESSION['USER_KEY'] == '') {
		$_SESSION['USER_KEY']			= 0;
		$_SESSION['USER_ID'] 		  	= 0;
		$_SESSION['USER_SUPER_USER'] 	= 0;
		$_SESSION['USER_PERMISSIONS'] 	= array();
		$_SESSION['USER_EM'] 			= '';
		$_SESSION['user_online']		= '';
		$_SESSION['ERROR']				= '';
		$_SESSION['info']				= '';
		$_SESSION['previousPage']		= '';
		
	} 
function getFileExtension ($name)
{
	$filename = strtolower($name) ;
	$exts = split("[/\\.]", $filename) ;
	$n = count($exts)-1;
	return $exts[$n];
}
function getHeight($ow, $oh, $Nw)
{
	$pro = (float) $ow / $Nw;
	return round($oh/$pro, 0);
}

function getWidth($ow, $oh, $Nh)
{
	$pro = (float) $oh / $Nh;
	return round($ow/$pro, 0);
}
?>
