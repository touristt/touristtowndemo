<?PHP
if (is_array($_GET['ammenities'])) {
    $listAmm = $_GET['ammenities'];
} else {
    $listAmm = array();
}
if (is_array($_GET['rooms'])) {
    $listRooms = $_GET['rooms'];
} else {
    $listRooms = array();
}
if (is_array($_GET['locations'])) {
    $listLoc = $_GET['locations'];
} else {
    $listLoc = array();
}
if (count($listLoc) == 0) {
    $listAll = true;
}
?>
<div class="event-search" style="margin-bottom: 10px;width:170px;">
    <form action="/search-accomodation.php">
        <h2>Search</h2>
        <h5>Accomodation Type</h5>
        <p>
            <select name="subCategory" class="AmmenityText" id="select" style="width: 159px;">
                <option selected="selected">Select Type</option>
                <?PHP
                $sql = "SELECT C_ID, C_Parent, C_Name FROM tbl_Category LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID
                        WHERE C_Parent = '4' AND RC_R_ID = '" . $REGION['R_ID'] . "' ORDER BY C_Name";
                $resultSubs = mysql_query($sql, $db)
                        or die("Invalid query: $sql -- " . mysql_error());
                while ($rowSub = mysql_fetch_assoc($resultSubs)) {
                    ?>                                          
                    <option value="<?php echo $rowSub['C_ID'] ?>"<?php echo ($_GET['subCategory'] == $rowSub['C_ID']) ? 'selected' : '' ?>><?php echo $rowSub['C_Name'] ?></option>
                    <?PHP
                }
                ?>

            </select>
        </p>
        <h5>Bedrooms</h5>
        <p>
            <?PHP
            $sql = "SELECT AI_ID, AI_Name FROM tbl_Ammenity_Icons WHERE AI_Name LIKE '%bedroom%' ORDER BY AI_Name";
            $resultRooms = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($rowRooms = mysql_fetch_assoc($resultRooms)) {
                $str = explode(' ', $rowRooms['AI_Name']);
                $rooms = $str[0];
                ?>
                <span class="AmmenityText">
                    <input type="checkbox" name="rooms[]" id="checkbox" <?php echo (in_array($rowRooms['AI_ID'], $listRooms)) ? 'checked' : '' ?> value="<?php echo $rowRooms['AI_ID'] ?>" />
                    <?php echo $rooms ?>
                </span>
                <?PHP
            }
            ?>
        </p>

        <h5>Amenities</h5>
        <p>
            <?PHP
            $sql = "SELECT AI_ID, AI_Name, AI_Search FROM tbl_Ammenity_Icons WHERE  AI_Search > 0 ORDER BY AI_Search";
            $resultAmmenities = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($rowAmmenity = mysql_fetch_assoc($resultAmmenities)) {
                ?>
                <span class="AmmenityText full-width">
                    <input type="checkbox" name="ammenities[]" id="checkbox" <?php echo in_array($rowAmmenity['AI_ID'], $listAmm) ? 'checked' : '' ?> value="<?php echo $rowAmmenity['AI_ID'] ?>" />
                    <?php echo $rowAmmenity['AI_Name'] ?>
                </span>
                <?PHP
            }
            ?>
        </p>
        <?PHP
        if ($REGION['R_Parent'] == 0) {
            ?>
            <h5>Locations</h5>
            <p>
                <?PHP
                $sql = "SELECT * FROM tbl_Region_Multiple LEFT JOIN tbl_Region ON RM_Child = R_ID WHERE RM_Parent = '" . encode_strings($REGION['R_ID'], $db) . "'";
                $resultRtmp = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                while ($rowRtmp = mysql_fetch_assoc($resultRtmp)) {
                    ?>
                    <span class="AmmenityText full-width">
                        <input type="checkbox" name="locations[]" id="checkbox" <?php echo ($listAll || in_array($rowRtmp['R_ID'], $listLoc)) ? 'checked' : '' ?> value="<?php echo $rowRtmp['R_ID'] ?>" />
                        <?php echo $rowRtmp['R_Name'] ?>
                    </span>
                <?PHP }
                ?>
            </p>
            <?php
        }
        ?>
        <p><input type="submit" name="button" id="button" value="Search Now" style="padding:12px 39px;"/></p>
    </form>
</div>
