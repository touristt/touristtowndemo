<?php
require_once 'include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
require_once 'include/public/header.php';
?>
<!--Thumbnail Grid-->
<section class="thumbnail-grid padding-bottom-none border-none">
  <div class="grid-wrapper">
    <div class="grid-inner border-none">
      <!-- Begin MailChimp Signup Form -->
      <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
      <style type="text/css">
        #mc_embed_signup
        {background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
        /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
        We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
      </style>
      <div id="mc_embed_signup">
        <form action="//explorethebruce.us1.list-manage.com/subscribe/post?u=cf11b2d04bf40d80b1a4f59cf&id=4484c0624d" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
          <div id="mc_embed_signup_scroll">
            <h2>Subscribe to our mailing list</h2>
            <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
            <div class="mc-field-group">
              <label for="mce-EMAIL">Email Address <span class="asterisk">*</span>
              </label>
              <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
            </div>
            <div class="mc-field-group">
              <label for="mce-FNAME">First Name </label>
              <input type="text" value="" name="FNAME" class="" id="mce-FNAME">
            </div>
            <div class="mc-field-group">
              <label for="mce-LNAME">Last Name </label>
              <input type="text" value="" name="LNAME" class="" id="mce-LNAME">
            </div>
            <div class="mc-field-group input-group">
              <strong>Most Interested in </strong>
              <ul><li><input type="checkbox" value="1" name="group51" id="mce-group5-5-0"><label for="mce-group5-5-0">Adventure Passport - Scavenger Hunt Across Bruce County</label></li>
                <li><input type="checkbox" value="1024" name="group51024" id="mce-group5-5-1"><label for="mce-group5-5-1">Bruce County Events</label></li>
                <li><input type="checkbox" value="512" name="group5512" id="mce-group5-5-2"><label for="mce-group5-5-2">Birding</label></li>
                <li><input type="checkbox" value="2" name="group52" id="mce-group5-5-3"><label for="mce-group5-5-3">Boat Tours</label></li>
                <li><input type="checkbox" value="4" name="group54" id="mce-group5-5-4"><label for="mce-group5-5-4">Bruce Trail</label></li>
                <li><input type="checkbox" value="16" name="group516" id="mce-group5-5-5"><label for="mce-group5-5-5">Cross County Skiing</label></li>
                <li><input type="checkbox" value="32" name="group532" id="mce-group5-5-6"><label for="mce-group5-5-6">Cycling</label></li>
                <li><input type="checkbox" value="64" name="group564" id="mce-group5-5-7"><label for="mce-group5-5-7">Diving/Snorkling</label></li>
                <li><input type="checkbox" value="128" name="group5128" id="mce-group5-5-8"><label for="mce-group5-5-8">Hiking</label></li>
                <li><input type="checkbox" value="256" name="group5256" id="mce-group5-5-9"><label for="mce-group5-5-9">Lighthouses</label></li>
                <li><input type="checkbox" value="2048" name="group52048" id="mce-group5-5-10"><label for="mce-group5-5-10">Motorcycling</label></li>
                <li><input type="checkbox" value="2097152" name="group52097152" id="mce-group5-5-11"><label for="mce-group5-5-11">Mountain Biking</label></li>
                <li><input type="checkbox" value="4194304" name="group54194304" id="mce-group5-5-12"><label for="mce-group5-5-12">National Parks</label></li>
                <li><input type="checkbox" value="8388608" name="group58388608" id="mce-group5-5-13"><label for="mce-group5-5-13">Paddling</label></li>
                <li><input type="checkbox" value="16777216" name="group516777216" id="mce-group5-5-14"><label for="mce-group5-5-14">Provincial Parks</label></li>
                <li><input type="checkbox" value="33554432" name="group533554432" id="mce-group5-5-15"><label for="mce-group5-5-15">Snowmobiling</label></li>
                <li><input type="checkbox" value="67108864" name="group567108864" id="mce-group5-5-16"><label for="mce-group5-5-16">Snowshoeing</label></li>
                <li><input type="checkbox" value="134217728" name="group5134217728" id="mce-group5-5-17"><label for="mce-group5-5-17">Swimming/Beaches</label></li>
                <li><input type="checkbox" value="268435456" name="group5268435456" id="mce-group5-5-18"><label for="mce-group5-5-18">Other</label></li>
              </ul>
            </div>
            <div class="mc-field-group input-group">
              <strong>Communities of Interest </strong>
              <ul><li><input type="checkbox" value="4096" name="group94096" id="mce-group9-9-0"><label for="mce-group9-9-0">Tobermory</label></li>
                <li><input type="checkbox" value="16384" name="group916384" id="mce-group9-9-1"><label for="mce-group9-9-1">Lion's Head</label></li>
                <li><input type="checkbox" value="8192" name="group98192" id="mce-group9-9-2"><label for="mce-group9-9-2">Wiarton</label></li>
                <li><input type="checkbox" value="32768" name="group932768" id="mce-group9-9-3"><label for="mce-group9-9-3">Southampton</label></li>
                <li><input type="checkbox" value="65536" name="group965536" id="mce-group9-9-4"><label for="mce-group9-9-4">Port Elgin</label></li>
                <li><input type="checkbox" value="1048576" name="group91048576" id="mce-group9-9-5"><label for="mce-group9-9-5">Kincardine</label></li>
                <li><input type="checkbox" value="131072" name="group9131072" id="mce-group9-9-6"><label for="mce-group9-9-6">Walkerton & Area</label></li>
                <li><input type="checkbox" value="262144" name="group9262144" id="mce-group9-9-7"><label for="mce-group9-9-7">Ripley, Lucknow & Area</label></li>
                <li><input type="checkbox" value="524288" name="group9524288" id="mce-group9-9-8"><label for="mce-group9-9-8">Pailsey & Area</label></li>
              </ul>
            </div>
            <div id="mce-responses" class="clear">
              <div class="response" id="mce-error-response" style="display:none"></div>
              <div class="response" id="mce-success-response" style="display:none"></div>
            </div> <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_cf11b2d04bf40d80b1a4f59cf_4484c0624d" tabindex="-1" value=""></div>
            <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
          </div>
        </form>
      </div>
      <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function ($)
        {
          window.fnames = new Array();
          window.ftypes = new Array();
          fnames[0] = 'EMAIL';
          ftypes[0] = 'email';
          fnames[1] = 'FNAME';
          ftypes[1] = 'text';
          fnames[2] = 'LNAME';
          ftypes[2] = 'text';
          fnames[4] = 'MMERGE4';
          ftypes[4] = 'text';
        }
        (jQuery));
        var $mcj = jQuery.noConflict(true);</script>
      <!--End mc_embed_signup-->
    </div>
  </div>
</section>
<?php
require_once 'include/public/footer.php';
?>
