<?php
$sql = "SELECT C_ID, C_Name, RC_Name, RC_Description, RC_Category_Page_Title, RC_At_A_Glance_Text
        FROM tbl_Category 
        LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
        WHERE C_ID = '" . encode_strings($subCat, $db) . "' 
        ORDER BY C_Name LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeCat = mysql_fetch_assoc($result);
$slider = array();
$i = 0;
// Select Random advertisments
$adQueryCH = "SELECT A_ID, BL_ID, A_C_ID, A_SC_ID, A_AT_ID, A_Status, A_Website, A_Is_Deleted, A_Third_Party, A_Approved_Logo, BL_Name_SEO FROM tbl_Advertisement
                LEFT JOIN tbl_Business_Listing ON A_BL_ID = BL_ID $include_free_listings
                WHERE A_C_ID = '" . encode_strings($_SESSION['CATEGORY'], $db) . "' 
                AND A_SC_ID = '" . encode_strings($subCat, $db) . "'
                AND A_AT_ID = 1 AND A_Status = 3 
                AND A_Website = '" . encode_strings($REGION['R_ID'], $db) . "' 
                AND A_Is_Deleted = 0
                GROUP BY A_ID
                ORDER BY RAND() LIMIT 4";
$adResultCH = mysql_query($adQueryCH) or die("Invalid query: $adQueryCH -- " . mysql_error());
$num_of_subcatadvert = mysql_num_rows($adResultCH);
//Create array of all images except ads
if ($Firstslider['RC_Image'] != "") {
    $slider[$i]['title'] = $Firstslider['RC_Slider_Title'];
    $slider[$i]['desc'] = $Firstslider['RC_Slider_Des'];
    $slider[$i]['image'] = $Firstslider['RC_Image'];
    $slider[$i]['video'] = $Firstslider['RC_Video'];
    $slider[$i]['alt'] = $Firstslider['RC_Alt'];
    $i++;
}
while ($active_category_slider = mysql_fetch_assoc($result_category_slider)) {
    if ($active_category_slider['RCP_Image'] != '') {
        $slider[$i]['title'] = $active_category_slider['RCP_Slider_Title'];
        $slider[$i]['desc'] = $active_category_slider['RCP_Slider_Description'];
        $slider[$i]['image'] = $active_category_slider['RCP_Image'];
        $slider[$i]['video'] = $active_category_slider['RCP_Video'];
        $slider[$i]['alt'] = $active_category_slider['RCP_Slider_Title'];
        $i++;
    }
}
$sql_feature = "SELECT S_ID, S_Category, S_Title, S_Feature_Image, S_Description, C_Parent from tbl_Story LEFT JOIN tbl_Story_Homepage_Category ON S_ID = SHC_S_ID
                LEFT JOIN tbl_Category ON S_Category = C_ID
                $StorySeasons_JOIN
                WHERE SHC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
                AND SHC_Category = '" . encode_strings($subCat, $db) . "' AND SHC_Feature = 1 AND S_Active = 1 $StorySeasons_WHERE GROUP BY S_ID ORDER BY SHC_Order ASC";
$result_feature = mysql_query($sql_feature, $db) or die("Invalid query: $sql_feature -- " . mysql_error());
$feature_count = mysql_num_rows($result_feature);
?>
<!--Slider Start-->
<section class="main_slider">
    <div class="slider_wrapper">
        <div class="sliderCycle" data-cycle-loader="wait" data-cycle-fx=fade data-cycle-timeout=0 data-cycle-prev="#prev" data-cycle-next="#next">
            <?php
            $first_img = true;
            $j = 1;
            $show_default = true; //if no image, show default
            foreach ($slider as $slide) {
                $VIDEOID = $slide['video'];
                $pos = strpos($VIDEOID, '=');
                if ($pos == false) {
                    $video_link = end(explode('/', $VIDEOID));
                } else {
                    $video_link = end(explode("=", $VIDEOID));
                }
                if ($slide['image'] != '') {
                    $show_default = false;
                    if ($first_img) {
                        $image_slider_style = '';
                        $first_img = false;
                    } else {
                        $image_slider_style = 'display:none;';
                    }
                    ?>                       
                    <div class="slide-images"  style="<?php echo $image_slider_style ?>">
                        <?php if ($VIDEOID != "") { ?>
                            <div class="<?php echo"player" . $j . "_wrapper"; ?>" style="position:absolute;opacity: 0;top:0;">
                                <input type="hidden" id="video-link-<?php echo $j; ?>" value="<?php echo $video_link ?>">
                                <div  id="<?php echo "player" . $j; ?>"></div>
                            </div>
                        <?php } ?>
                        <div class="slider-text">
                            <h1 class="show-slider-display"><?php echo $slide['title'] ?></h1>
                            <h3 class="show-slider-display"><?php echo $slide['desc'] ?></h3>
                            <?php if ($VIDEOID != "") { ?>
                                <div class="watch-video play-button show-slider-display">
                                    <div class="slider-button">
                                        <img class="video_icon" src="<?php echo 'http://' . DOMAIN . '/images/videoicon.png'; ?>" alt="Play">
                                        <a onclick="play_video(<?php echo $j; ?>, '<?php echo"player" . $j; ?>')">Watch full video</a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <img class="slider_image" class="show-slider-display" src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $slide['image'] ?>" alt="<?php echo ($slide['alt'] != '') ? $slide['alt'] : $activeCat['C_Name']; ?>">
                    </div>
                    <?php
                }
                $j++;
            }

            if ($num_of_subcatadvert > 0) {
                while ($adRowCH = mysql_fetch_array($adResultCH)) {
                    $show_default = false;
                    if ($adRowCH['A_Third_Party'] != "") {
                        echo '<div class="slide-images"><a class="show-slider-display" style="display:none" onclick = "advertClicks(' . $adRowCH['A_ID'] . ')" target="_blank" href="http://' . str_replace(array('http://', 'https://'), '', $adRowCH['A_Third_Party']) . '"><img  src="http://' . DOMAIN . IMG_LOC_REL . $adRowCH['A_Approved_Logo'] . '"></a></div>';
                    } else {
                        echo '<div class="slide-images"><a class="show-slider-display" style="display:none" onclick = "advertClicks(' . $adRowCH['A_ID'] . ')" href="/profile/' . $adRowCH['BL_Name_SEO'] . '/' . $adRowCH['BL_ID'] . '/"><img  src="http://' . DOMAIN . IMG_LOC_REL . $adRowCH['A_Approved_Logo'] . '"></a></div>';
                    }
                    $AS_A_ID_CH = $adRowCH['A_ID'];
                    ADVERT_IMPRESSION($AS_A_ID_CH);
                }
            }
            //show default image if no image available
            if ($show_default) {
                echo '<div class="slide-images"><img class="slider_image" src="http://' . DOMAIN . IMG_LOC_REL . $default_header_image . '" alt="' . (($activeCat['RC_Name'] != '') ? $activeCat['RC_Name'] : $activeCat['C_Name']) . '" /></div>';
            }
            ?>
        </div>       
        <input type="hidden" id="total_players" value="<?php echo $j; ?>">
        <div class=center>
            <span id=prev></span>
            <span id=next></span>
        </div>
        <?php if ($THEME['TO_Slider_Overlay'] != "") { ?>
            <div class="image_overlay_img">
                <div class="image_overlay">
                    <img src="<?php echo 'http://' . DOMAIN . IMG_ICON_REL . $THEME['TO_Slider_Overlay']; ?>" alt="Slider Overlay">
                </div>
            </div>
        <?php } ?>
    </div>
</section>
<!--Slider End-->
<!--Description Start-->
<section class="description theme-description">
    <div class="description-wrapper">
        <div class="description-inner">
            <h1 class="heading-text">
                <?php
                if ($activeCat['RC_Category_Page_Title'] != '') {
                    echo $activeCat['RC_Category_Page_Title'];
                } else {
                    echo ($activeCat['RC_Name'] != '') ? $activeCat['RC_Name'] : $activeCat['C_Name'];
                    if ($REGION['R_Website_Title'] == 1) {
                        echo " in " . $REGION['R_Name'];
                    }
                }
                ?>
            </h1>
            <div class="site-text ckeditor-anchors">
                <?PHP
                if ($activeCat['RC_Description']) {
                    echo $activeCat['RC_Description'];
                }
                ?>
            </div>
        </div>
    </div>
</section>
<!--Description End-->
<!-- Feature Stories-->
<?php if ($feature_count > 0 && $REGION['R_Stories'] == 1 && $blog['RC_Status'] == 0) { ?>
    <section class="section_stories" class="padding-bottom-none border-none">
        <div class="grid-wrapper">
            <div class="grid-inner padding-bottom-none slider">
                <div class="feature-stories">
                    <?php
                    while ($row_feature = mysql_fetch_array($result_feature)) {
                        if ($row_feature['S_Feature_Image'] != '') {
                            $feature_image = $row_feature['S_Feature_Image'];
                        } else {
                            $feature_image = $default_header_image;
                        }
                        ?>
                        <div class="feature-story">
                            <a href="/<?php echo $storyCatSEORow['C_Name_SEO'] ?>/<?php echo clean($row_feature['S_Title']) ?>/<?php echo isset($row_feature['S_ID']) ? $row_feature['S_ID'] : ''; ?>/">
                                <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $feature_image; ?>" width="100%" height="auto" alt="<?php echo $row_feature['S_Title']; ?>" />
                            </a>
                            <a href="/<?php echo $storyCatSEORow['C_Name_SEO'] ?>/<?php echo clean($row_feature['S_Title']) ?>/<?php echo isset($row_feature['S_ID']) ? $row_feature['S_ID'] : ''; ?>/">
                                <h1 align="center" class="heading-text"><?php echo $row_feature['S_Title'] ?></h1>
                            </a>
                            <?php
                            if ($row_feature['S_Description'] != '') {
                                $string = preg_replace('/(.*)<\/p[^>]*>/i', '$1', $row_feature['S_Description']);
                                ?>
                                <div class="story-description">
                                    <?php echo $string ?>
                                    <a href="/<?php echo $storyCatSEORow['C_Name_SEO'] ?>/<?php echo clean($row_feature['S_Title']) ?>/<?php echo isset($row_feature['S_ID']) ? $row_feature['S_ID'] : ''; ?>/">
                                        Read More
                                    </a>
                                    </p>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>  
        </div>
    </section>
<?php } ?>
<!-- Feature Stories end -->
<!--Thumbnail Grid-->
<?php
if ($activeCat['C_ID'] == '87' || $activeCat['C_ID'] == '94' || $activeCat['C_ID'] == '102' || $activeCat['C_ID'] == '88' || $activeCat['C_ID'] == '34' || $activeCat['C_ID'] == '35' || $activeCat['C_ID'] == '50' || $activeCat['C_ID'] == '120') {
    require_once 'events.php';
} else {
    ?>
    <section class="thumbnail-grid sub-cat-margin border-none">
        <div class="grid-wrapper">
            <div class="grid-inner border-none padding-bottom-none">
                <?php if ($REGION['R_Listings_Title'] != '') { ?>
                    <h1 class="heading-text" align="center"><?php echo $REGION['R_Listings_Title']; ?></h1>
                    <?php
                }
                $sql = "SELECT BL_ID, BL_Name_SEO, LT_ID, BLP_Photo, BL_Photo_Alt, BL_Listing_Title, R.R_Name as permanent_name, R1.R_Name as secondary_name
                    $distance_select FROM tbl_Business_Listing
                    LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
                    LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                    INNER JOIN tbl_Region_Category ON RC_C_ID = BLC_C_ID AND RC_R_ID='" . encode_strings($REGION['R_ID'], $db) . "'
                    LEFT JOIN tbl_Category ON RC_C_ID = C_ID
                    LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
                    INNER JOIN tbl_Business_Listing_Category_Region BLCR ON BL_ID = BLCR.BLCR_BL_ID AND BLCR.BLCR_BLC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                    $TOWN_JOIN
                    LEFT JOIN tbl_Region R ON BLCR.BLCR_BLC_R_ID = R.R_ID
                    LEFT JOIN tbl_Region R1 ON BLCR1.BLCR_BLC_R_ID = R1.R_ID
                    LEFT JOIN tbl_Business_Listing_Order ON BL_ID = BLO_BL_ID AND BLO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
                    AND BLO_S_C_ID = BLC_C_ID
                    WHERE BLC_C_ID = '" . encode_strings($activeCat['C_ID'], $db) . "' AND BLP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                    AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = '1' 
                    $include_free_listings
                    GROUP BY BL_Listing_Title $distance_having
                    $order_listings";
                $result = mysql_query($sql, $db);
                $pages = new Paginate(mysql_num_rows($result), 15);
                $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
                $i = 0;
                while ($row = mysql_fetch_assoc($result)) {
                    $i++;
                    if ($row['BLP_Photo'] != '') {
                        $listing_image = $row['BLP_Photo'];
                    } else {
                        $listing_image = $default_thumbnail_image;
                    }
                    if ($i == 1) {
                        echo "<div class='thumbnails static-thumbs'>";
                    }
                    ?>
                    <div class="thumb-item">
                        <a href="/profile/<?php echo $row['BL_Name_SEO'] ?>/<?php echo $row['BL_ID'] ?>/"> 
                            <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo $row['BL_Listing_Title'] ?>" />
                            <h3 class="thumbnail-heading"><?php echo ($row['secondary_name'] != '') ? $row['secondary_name'] : $row['permanent_name']; ?></h3>
                            <h3 class="thumbnail-desc"><?php echo $row['BL_Listing_Title']; ?></h3>
                        </a> 
                    </div>
                    <?php
                    if ($i == 3) {
                        echo '</div>';
                        $i = 0;
                    }
                }
                ?>
            </div>
            <?php
            // display our pagination footer if set.
            if (isset($pages)) {
                echo $pages->paginate();
            }
            ?>
        </div>
    </section>
    <!--bar texture start-->
    <div class="bar-texture"></div>
    <!--bar texture end-->
    <!--Map section-->
    <section class="description maps">
        <div class="description-wrapper">
            <div class="description-inner">
                <h1 class="heading-text">
                    <?php
                    if ($activeCat['RC_At_A_Glance_Text'] != '') {
                        echo $activeCat['RC_At_A_Glance_Text'];
                    } else {
                        echo (($activeCat['RC_Name'] != '') ? $activeCat['RC_Name'] : $activeCat['C_Name']) . ' at a glance...';
                    }
                    ?>
                </h1>
            </div>
        </div>
        <div class="map_wrapper">
            <?PHP
            $sql = "SELECT DISTINCTROW BL_ID, BL_Listing_Title, BL_Name_SEO, BLP_Photo, BL_Photo_Alt, BL_Lat, BL_Long, BL_Street, BL_Town, hide_show_listing, 
                RC.RC_Map_Icon as subcat_icon, RC1.RC_Map_Icon as cat_icon $distance_select
                FROM tbl_Business_Listing 
                LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
                LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                INNER JOIN tbl_Region_Category RC ON RC.RC_C_ID = BLC_C_ID AND RC.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                INNER JOIN tbl_Region_Category RC1 ON RC1.RC_C_ID = BLC_M_C_ID AND RC1.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                INNER JOIN tbl_Business_Listing_Category_Region BLCR ON BL_ID = BLCR.BLCR_BL_ID AND BLCR.BLCR_BLC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
                LEFT JOIN tbl_Business_Listing_Order ON BL_ID = BLO_BL_ID AND BLO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
                AND BLO_S_C_ID = BLC_C_ID
                $TOWN_JOIN
                WHERE BLC_C_ID = '" . encode_strings($activeCat['C_ID'], $db) . "' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing='1' AND RC.RC_Status = 0 AND RC1.RC_Status = 0 AND BLP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                $include_free_listings_on_map
                GROUP BY BL_ID $distance_having $order_listings";
            // generate icons for all listings on map regardless of page
            $result_map = mysql_query($sql, $db);
            $markers = array();
            while ($list = mysql_fetch_array($result_map, MYSQL_ASSOC)) {
                if ($list['BLP_Photo'] != '') {
                    $image = '<div class="thumbnail"><img src="http://' . DOMAIN . IMG_LOC_REL . $list['BLP_Photo'] . '" alt="' . htmlspecialchars(trim($list['BL_Listing_Title']), ENT_QUOTES) . '" /></div>';
                } else {
                    $image = '<div class="thumbnail"><img src="http://' . DOMAIN . IMG_LOC_REL . $default_thumbnail_image . '" alt="' . htmlspecialchars(trim($list['BL_Listing_Title']), ENT_QUOTES) . '" /></div>';
                }
                if ($list['subcat_icon'] != '') {
                    $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $list['subcat_icon'];
                } elseif ($list['cat_icon'] != '') {
                    $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $list['cat_icon'];
                } else {
                    $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . 'default.png';
                }
                if ($list['BL_Lat'] && $list['BL_Long']) {
                    $markers[] = array(
                        'id' => $list['BL_ID'],
                        'lat' => $list['BL_Lat'],
                        'lon' => $list['BL_Long'],
                        'name' => $list['BL_Listing_Title'],
                        'path' => '/profile/' . $list['BL_Name_SEO'] . '/' . $list['BL_ID'] . '/',
                        'icon' => $icon,
                        'main_photo' => $image,
                        'address' => htmlspecialchars(trim($list['BL_Street']), ENT_QUOTES),
                        'town' => htmlspecialchars(trim($list['BL_Town']), ENT_QUOTES)
                    );
                }
            }
            $markers = json_encode($markers);
            ?>
            <div id="map_canvas">&nbsp;</div>
            <?php
            $map_center['latitude'] = $REGION['R_Lat'];
            $map_center['longitude'] = $REGION['R_Long'];
            $map_center['zoom'] = $REGION['R_Zoom'];
            $kml_json = json_encode(array());
            require_once 'map_script.php';
            ?>
        </div>
    </section>
    <!--Map section end-->
<?php } ?>
