<?php
require_once 'include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
$useragent = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
    $url_redirect = 'http://' . $REGION['R_Domain'] . '/mobile/search.php?txtSearch=' . $_GET['txtSearch'] . '&submit=Go';
    header('Location: ' . $url_redirect);
    exit();
}
//search by each word in the phrase except common words
$common_words = array('a', 'an', 'the', 'is', 'are', 'be', 'to', 'of', 'and', 'in', 'that', 'have', 'i', 'it', 'for', 'not', 'on',
    'with', 'he', 'as', 'you', 'do', 'at', 'this', 'but', 'his', 'by', 'from', 'they', 'we', 'her', 'she', 'or',
    'will', 'my', 'one', 'all', 'would', 'there', 'their', 'what', 'so', 'up', 'out', 'if', 'about', 'who', 'get',
    'which', 'go', 'me', 'when', 'make', 'can', 'like', 'no', 'just', 'him', 'know', 'take', 'into', 'your', 'good',
    'some', 'could', 'them', 'other', 'then', 'than', 'now', 'look', 'only', 'come', 'its', 'over', 'think', 'also',
    'back', 'after', 'use', 'two', 'how', 'our', 'first', 'well', 'way', 'even', 'because', 'any', 'these', 'give',
    'most', 'us');
require_once 'include/public/header.php';
?>
<!--Description Start-->
<section class="description search-results margin-bottom-none">
    <div class="description-wrapper">
        <div class="description-inner padding-bottom-none">
            <h1 class="heading-text">Search Results</h1>
            <form method="GET" action="/search.php">
                <div class="search-form">
                    <div class="search_field"><input type="text" name="txtSearch" placeholder="<?php echo stripslashes($_GET['txtSearch']) ?>"></div>
                    <div class="search_button"><input type="submit" name="submit" value="go"></div>
                </div>
            </form>
        </div>
    </div>
</section>
<!--Thumbnail Grid for stories-->
<?PHP
$searchstring = array();
if (strlen($_GET['txtSearch']) > 3) {
    //search stories
    $searchtext = explode(' ', $_GET['txtSearch']);
    foreach ($searchtext as $char) {
        if (!in_array($char, $common_words)) {
            $searchstring[] = $char;
        }
    }
    $Count = count($searchstring);
    foreach ($searchstring as $char) {
        $temp++;
        if ($temp == 1) {
            $sql = "SELECT DISTINCTROW S_ID, S_Title, S_Thumbnail, C_Name, C_Parent, RC_Name from tbl_Story 
                    LEFT JOIN tbl_Story_Region ON SR_S_ID = S_ID 
                    LEFT JOIN tbl_Content_Piece ON CP_S_ID = S_ID
                    LEFT JOIN tbl_Category ON S_Category = C_ID
                    LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID
                    WHERE SR_R_ID = " . $REGION['R_ID'] . " AND S_Active = 1 
                    AND (S_Title LIKE '%" . encode_strings($char, $db) . "%' OR
                    S_Description LIKE '%" . encode_strings($char, $db) . "%' OR
                    CP_Title LIKE '%" . encode_strings($char, $db) . "%' OR
                    CP_Description LIKE '%" . encode_strings($char, $db) . "%'";
        } else {
            $sql .= "OR S_Title LIKE '%" . encode_strings($char, $db) . "%' OR
                      S_Description LIKE '%" . encode_strings($char, $db) . "%' OR
                      CP_Title LIKE '%" . encode_strings($char, $db) . "%' OR
                      CP_Description LIKE '%" . encode_strings($char, $db) . "%'";
        }
        if ($temp == $Count) {
            $sql .= ") GROUP BY S_ID ORDER BY S_ID DESC";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        }
    }
    $i = 0;
    if ($Count > 0 && mysql_num_rows($result) > 0) {
        print '<section class="thumbnail-grid padding-bottom-none border-none">
              <div class="grid-wrapper">
                <div class="grid-inner border-none">';
        while ($row = mysql_fetch_assoc($result)) {
            $i++;
            if ($row['S_Thumbnail'] != '') {
                $story_image = $row['S_Thumbnail'];
            } else {
                $story_image = $default_thumbnail_image;
            }
            if ($i == 1) {
                echo "<div class='thumbnails static-thumbs'>";
            }
            ?>
            <div class="thumb-item">
                <a href="/<?php echo $storyCatSEORow['C_Name_SEO'] ?>/<?php echo clean($row['S_Title']) ?>/<?php echo isset($row['S_ID']) ? $row['S_ID'] : ''; ?>/">
                    <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $story_image; ?>" alt="<?php echo $row['S_Title'] ?>" />
                    <h3 class="thumbnail-heading"><?php echo ($row['RC_Name'] != '') ? $row['RC_Name'] : $row['C_Name'] ?></h3>
                    <h3 class="thumbnail-desc"><?php echo $row['S_Title'] ?></h3>
                </a> 
            </div>
            <?php
            if ($i == 3) {
                echo '</div>';
                $i = 0;
            }
        }
        print '</div>
                </div>
                </section>';
    }

    //search listings
    $exact_phrase = array();
    $exact_phrase_blid = array();
    $not_exact_phrase = array();
    //exact phrase search
    $sql = "SELECT DISTINCTROW BL_ID, BL_Name_SEO, BLP_Photo, BL_Photo_Alt, BL_Listing_Title, R.R_Name as permanent_name, R1.R_Name as secondary_name
            $distance_select FROM tbl_Business_Listing 
            LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
            LEFT JOIN tbl_Region_Category ON RC_C_ID = BLC_C_ID AND RC_R_ID= '" . encode_strings($REGION['R_ID'], $db) . "'
            INNER JOIN tbl_Business_Listing_Category_Region BLCR ON BL_ID = BLCR.BLCR_BL_ID AND BLCR.BLCR_BLC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            $TOWN_JOIN
            LEFT JOIN tbl_Region R ON BLCR.BLCR_BLC_R_ID = R.R_ID
            LEFT JOIN tbl_Region R1 ON BLCR1.BLCR_BLC_R_ID = R1.R_ID
            WHERE BL_Listing_Title = '" . encode_strings($_GET['txtSearch'], $db) . "'
            AND hide_show_listing = '1' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND RC_Status = 0 AND BLP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            $include_free_listings
            GROUP BY BL_ID $distance_having";
    $resultListing = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($row = mysql_fetch_assoc($resultListing)) {
        $exact_phrase[] = $row;
        $exact_phrase_blid[] = $row['BL_ID'];
    }

    $temp = 0;
    foreach ($searchstring as $char) {
        $temp++;
        if ($temp == 1) {
            $sql = "SELECT DISTINCTROW BL_ID, BL_Name_SEO, BLP_Photo, BL_Photo_Alt, BL_Listing_Title, R.R_Name as permanent_name, R1.R_Name as secondary_name
                    $distance_select FROM tbl_Business_Listing 
                    LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
                    LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
                    LEFT JOIN tbl_Region_Category ON RC_C_ID = BLC_C_ID AND RC_R_ID= '" . encode_strings($REGION['R_ID'], $db) . "'
                    LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
                    INNER JOIN tbl_Business_Listing_Category_Region BLCR ON BL_ID = BLCR.BLCR_BL_ID AND BLCR.BLCR_BLC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                    $TOWN_JOIN
                    LEFT JOIN tbl_Region R ON BLCR.BLCR_BLC_R_ID = R.R_ID
                    LEFT JOIN tbl_Region R1 ON BLCR1.BLCR_BLC_R_ID = R1.R_ID
                    WHERE BLP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' AND (BL_Listing_Title LIKE '%" . encode_strings($char, $db) . "%' OR
                    BL_Description LIKE '%" . encode_strings($char, $db) . "%' OR
                    BL_Search_Words LIKE '%" . encode_strings($char, $db) . "%' ";
        } else {
            $sql .= "OR BL_Listing_Title LIKE '%" . encode_strings($char, $db) . "%' OR
                    BL_Description LIKE '%" . encode_strings($char, $db) . "%' OR
                    BL_Search_Words LIKE '%" . encode_strings($char, $db) . "%' ";
        }
        if ($temp == $Count) {
            $sql .= ") AND hide_show_listing='1' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND RC_Status = 0
                     $include_free_listings
                     GROUP BY BL_ID $distance_having ORDER BY ";
        }
    }
    $temp = 0;
    foreach ($searchstring as $char) {
        $temp++;
        if ($temp != 1) {
            $sql .= "+";
        }
        $sql .= "IF(BL_Listing_Title LIKE '%" . encode_strings($char, $db) . "%',1,0)+
                    IF(BL_Description LIKE '%" . encode_strings($char, $db) . "%',1,0)+
                    IF(BL_Search_Words LIKE '%" . encode_strings($char, $db) . "%',1,0)";
        if ($temp == $Count) {
            $sql .= " DESC, BL_Points DESC, LT_Order DESC";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        }
    }
    while ($row = mysql_fetch_assoc($result)) {
        if (!in_array($row['BL_ID'], $exact_phrase_blid)) {
            $not_exact_phrase[] = $row;
        }
    }
    $listings = array_merge($exact_phrase, $not_exact_phrase);
    $i = 0;
    if (count($listings) > 0) {
        print '<section class="thumbnail-grid padding-bottom-none border-none">
                <div class="grid-wrapper">
                <div class="grid-inner border-none">';
        foreach ($listings as $row) {
            $i++;
            if ($row['BLP_Photo'] != '') {
                $listing_image = $row['BLP_Photo'];
            } else {
                $listing_image = $default_thumbnail_image;
            }
            if ($i == 1) {
                echo "<div class='thumbnails static-thumbs'>";
            }
            ?>
            <div class="thumb-item">
                <a href="/profile/<?php echo $row['BL_Name_SEO'] ?>/<?php echo $row['BL_ID'] ?>/">
                    <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo $row['BL_Photo_Alt'] ?>" longdesc="<?php echo $row['BL_Listing_Title'] ?>"/>
                    <h3 class="thumbnail-heading"><?php echo ($row['secondary_name'] != '') ? $row['secondary_name'] : $row['permanent_name']; ?></h3>
                    <h3 class="thumbnail-desc"><?php echo $row['BL_Listing_Title'] ?></h3>
                </a> 
            </div>
            <?php
            if ($i == 3) {
                echo '</div>';
                $i = 0;
            }
        }
        print '</div>
                </div>
                </section>';
    } else {
        print '<section class="thumbnail-grid padding-bottom-none border-none">
                <div class="grid-wrapper">
                    <div class="grid-inner border-none">
                        <p style="clear: left; text-align: center">Your search request found no results. Please try again.</p>
                    </div>
                </div>
                </section>';
    }
} else {
    print '<section class="thumbnail-grid padding-bottom-none border-none">
              <div class="grid-wrapper">
                  <div class="grid-inner border-none">
                      <p style="clear: left; text-align: center">Search Term must be more than three (3) characters.</p>
                  </div>
              </div>
          </section>';
}
require_once 'include/public/footer.php';
?>
