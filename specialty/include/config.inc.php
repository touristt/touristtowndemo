<?PHP
session_start();
//echo 'The website is under active maintenance. We ll be back shortly';exit;
$serverName = "localhost";
$userName = "demotour_db";
$password = "Rg)aeDJ(^M4K";
$dbname = "demotour_db";

#error_reporting(E_ERROR | E_WARNING | E_PARSE);
error_reporting(E_ALL);
ini_set('display_errors', 'off');
ini_set("log_errors", 1);
ini_set("error_log", "/home/demotourist/tmp/php-error.log");
#ini_set('date.timezone', 'America/New_York'); 	
date_default_timezone_set('America/New_York');
$max_emails = 10;

define("ABS_ROOT", "/home/demotourist/public_html/");
define("WEB_ROOT", ABS_ROOT . "");
define("IMG_LOC_ABS", WEB_ROOT . "images/DB/");
define("IMG_LOC_REL", "/images/DB/");
define("IMG_ICON_MAP_ABS", WEB_ROOT . "images/map-icons/");
define("IMG_ICON_MAP_REL", "/images/map-icons/");
define("IMG_BANK_ABS", WEB_ROOT . "image_bank/");
define("IMG_BANK_REL", "/image_bank/");
define("IMG_ICON_ABS", WEB_ROOT . "theme_icons/");
define("IMG_ICON_REL", "/theme_icons/");
define("IMG_MOBILE_ABS", WEB_ROOT . "mobile/img/");
define("IMG_MOBILE_REL", "/mobile/img/");
define("PDF_LOC_ABS", WEB_ROOT . "pdf/");
define("PDF_LOC_REL", "/pdf/");
define("MAP_LOC_ABS", WEB_ROOT . "map/");
define("MAP_LOC_REL", "/map/");
define("IMG_USER_REL", "/images/usr");
define("FILE_LOC", WEB_ROOT . "DB/");
define("FILE_URL", "/DB/");
define("IMG_USER_ABS", WEB_ROOT . "images/usr");
define("SPECIALTY_DOMAIN", 'touristtowndemo.com/specialty');
define("DOMAIN_MOBILE", 'touristtowndemo.com/mobile');
define("DOMAIN_MOBILE_REL", '/mobile/');
define("COMPANY_NAME", "Tourist Town");
define("MAIN_CONTACT_NAME", "Tourist Town");
define("MAIN_CONTACT_EMAIL", "info@touristtowndemo.com");
define("COUPON_CONTACT_NAME", "Coupon Country");
define("COUPON_CONTACT_EMAIL", "info@couponcountry.ca");
define("GOOGLE_API_KEY", "");
define("STATIC_MAP_API_KEY", "AIzaSyAg90IEnrw4-EJTQlN-3EGH3u-MbGMpU08");


define("INPUT_DIR", "/admin/");
define("BUSINESS_DOMAIN", "my.touristtowndemo.com");
define("COUPON_DOMAIN", "mycoupon.touristtowndemo.com");
define("BUSINESS_DIR", "/");
define("BUYING_GROUP_DIR", "/bgadmin/");
define("PIC_W_MAX", 210);
define("PIC_H_MAX", 210);

require_once WEB_ROOT . "include/mysql.db.inc.php";
require_once WEB_ROOT . "include/commonFunctions.inc.php";

if (isset($isPublic) && $isPublic) {
    ini_set("session.gc_maxlifetime", 60 * 60 * 24 * 365);
    ini_set("session.cookie_lifetime", 60 * 60 * 24 * 365);
} else {
    ini_set("session.gc_maxlifetime", 65 * 60);
    ini_set("session.cookie_lifetime", 12 * 60 * 60);
    //ini_set("session.save_path", ABS_ROOT . "tmp/");  
}

if (isset($_SESSION['PUBLIC_KEY']) && $_SESSION['PUBLIC_KEY'] == '') {
    $_SESSION['PUBLIC_KEY'] = 0;
    $_SESSION['CATEGORY'] = 0;
}
if (isset($_SESSION['USER_KEY']) && $_SESSION['USER_KEY'] == '') {
    $_SESSION['USER_KEY'] = 0;
    $_SESSION['USER_ID'] = 0;
    $_SESSION['USER_SUPER_USER'] = 0;
    $_SESSION['USER_PERMISSIONS'] = array();
    $_SESSION['USER_EM'] = '';
    $_SESSION['user_online'] = '';
    $_SESSION['ERROR'] = '';
    $_SESSION['info'] = '';
    $_SESSION['previousPage'] = '';
}

if (isset($_SESSION['BUSINESS_USER_KEY']) && $_SESSION['BUSINESS_USER_KEY'] == '') {
    $_SESSION['BUSINESS_ID'] = 0;
    $_SESSION['BUSINESS_NAME'] = '';
    $_SESSION['BUSINESS_USER_KEY'] = 0;
    $_SESSION['BUSINESS_USER_ID'] = 0;
    $_SESSION['BUSINESS_USER_EM'] = '';
    $_SESSION['BUSINESS_user_online'] = '';
    $_SESSION['BUSINESS_ERROR'] = '';
    $_SESSION['BUSINESS_INFO'] = '';
}

if (isset($_SESSION['BG_USER_KEY']) && $_SESSION['BG_USER_KEY'] == '') {
    $_SESSION['BG_ID'] = 0;
    $_SESSION['BG_NAME'] = '';
    $_SESSION['BG_USER_KEY'] = 0;
    $_SESSION['BG_USER_ID'] = 0;
    $_SESSION['BG_USER_EM'] = '';
    $_SESSION['BG_user_online'] = '';
    $_SESSION['BG_ERROR'] = '';
    $_SESSION['BG_INFO'] = '';
    $_SESSION['BG_SUPER_USER'] = false;
    $_SESSION['BG_BUSINESS_IN'] = '';
}


$alphabet = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
    'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
$days = array(//ISO-8601 numeric representation of the day of the week
    1 => 'Monday',
    2 => 'Tuesday',
    3 => 'Wednesday',
    4 => 'Thursday',
    5 => 'Friday',
    6 => 'Saturday',
    7 => 'Sunday');

$menuSections = array(// dataField FROM tbl_Business_Feature_Data 
    'B' => array('title' => 'Breakfast', 'dataField' => 'Menu_Breakfast'),
    'L' => array('title' => 'Lunch', 'dataField' => 'Menu_Lunch'),
    'D' => array('title' => 'Dinner', 'dataField' => 'Menu_Dinner'),
    'A' => array('title' => 'Appetizers', 'dataField' => 'Menu_Appetizers'),
    'S' => array('title' => 'Salads', 'dataField' => 'Menu_Salads'),
    'SW' => array('title' => 'Sandwiches', 'dataField' => 'Menu_Sandwiches'),
    'MC' => array('title' => 'Main Course', 'dataField' => 'Menu_Main_Course'),
    'CM' => array('title' => 'Childrens Menu', 'dataField' => 'Menu_Children'),
    'DS' => array('title' => 'Desserts', 'dataField' => 'Menu_Desserts'),
    'DR' => array('title' => 'Drinks', 'dataField' => 'Menu_Drinks'),
    'CA' => array('title' => 'Catering', 'dataField' => 'Menu_Catering'),
    'PZ' => array('title' => 'Pizza', 'dataField' => 'Menu_Pizza'),
    'PA' => array('title' => 'Pasta', 'dataField' => 'Menu_Pasta'),
    'BG' => array('title' => 'Baked Goods', 'dataField' => 'Menu_Baked_Goods'));

$bg_colours = array("#CCCCCC", "#E3E3E3");
$default_bg = $bg_colours[1];

$config = array();
$sql = "SELECT * FROM tbl_Config";
$result = query($sql, $db);
while ($row = mysql_fetch_assoc($result)) {
    $config[$row['ID']] = $row['Value'];
}
?>