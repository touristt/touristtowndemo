/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */


CKEDITOR.editorConfig = function( config )
{
	config.toolbar = 'MyToolbar';
        config.toolbar_MyToolbar =
	[
        { name: 'document', items : [ 'Source','-'] },
	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','-','SpellChecker', 'Scayt','-','Undo','Redo'] },
        { name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','SpecialChar'] },
        
        { name: 'insert', items : [ 'Bold','Italic'] },
        { name: 'html', items : [ 'Button' ] },
        '/',
	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'] },
	{ name: 'links', items : [ 'Link','Unlink', 'Anchor'] },
	
	{ name: 'styles', items : ['Format'] },
	{ name: 'tools', items : ['Maximize'] }
	];
config.filebrowserUploadUrl = 'http://touristtown.ca/include/plugins/ckeditor/upload/upload.php';
};