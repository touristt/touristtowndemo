<?PHP
$sql = "SELECT * FROM tbl_Region WHERE REPLACE(LCASE(R_Domain), 'www.', '') = LCASE('" . preg_replace('^(www\.)^', '', $_SERVER["HTTP_HOST"]) . "') OR 
        R_Domain_Alternates LIKE concat( '%', REPLACE( LCASE( '" . $_SERVER["HTTP_HOST"] . "' ) , 'www.', '' ) , '%' )";
$result = query($sql, $db);
$REGION = mysql_fetch_assoc($result);
if ($REGION['R_ID'] < 1) {
    header('Location: http://www.touristtown.ca/Marketing/TT-Tutorials-AddingPhotos.php');
    exit();
}
if ($REGION['R_Status'] == 0 && $_SESSION['REGION_AUTH_ACT_INACT'] != 1) {
    $_SESSION['WEBSITE_LOAD'] = $REGION['R_Domain'];
    header("Location: http://" . $REGION['R_Domain'] . "/authentication.php");
    exit();
}

define("DOMAIN", 'touristtowndemo.com');

require_once 'browser_detection.php';

if (( browser_detection('browser_working') == 'ie' ) && ( browser_detection('browser_number') < 8 ) ||
        ( browser_detection('browser_working') == 'moz' ) && ( browser_detection('browser_number') < 8 ) ||
        ( browser_detection('browser_working') == 'webkit' ) && ( browser_detection('browser_number') < 531 )) {
    $browserERROR = true;
} else {
    $browserERROR = false;
}

function clean($string) {
    $string = strtolower(str_replace(' ', '-', $string)); // Replaces all spaces with hyphens.
    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}

//get full url of current page
function curPageURL() {
    $pageURL = 'http';
    if ($_SERVER["HTTPS"] == "on") {
        $pageURL .= "s";
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

?>