<?php
//check to pass mobile users to mobile version
$useragent = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
    header('Location: http://' . $REGION['R_Domain'] . DOMAIN_MOBILE_REL);
    exit;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <meta charset="utf-8" http-equiv="Content-Type" content="text/html">
            <title><?php echo isset($SEOtitle) ? $SEOtitle : $REGION['R_SEO_Title'] ?></title>
            <meta name="description" content="<?php echo isset($SEOdescription) ? $SEOdescription : $REGION['R_SEO_Description'] ?>" />
            <meta name="keywords" content="<?php echo isset($SEOkeywords) ? $SEOkeywords : $REGION['R_SEO_Keywords'] ?>" />
            <!-- Open Graph Tags fo FB Share and Like-->
            <meta property="fb:app_id" content="1356712584397235"/>
            <meta property="og:url" content="<?php echo isset($OG_URL) ? $OG_URL : ''; ?>" />
            <meta property="og:type" content="<?php echo isset($OG_Type) ? $OG_Type : ''; ?>" />
            <meta property="og:title" content="<?php echo isset($OG_Title) ? $OG_Title : ''; ?>" />
            <meta property="og:description" content="<?php echo isset($OG_Description) ? $OG_Description : ''; ?>" />
            <meta property="og:image" content="<?php echo isset($OG_Image) ? $OG_Image : ''; ?>" />
            <meta property="og:image:width" content="<?php echo isset($OG_Image_Width) ? $OG_Image_Width : ''; ?>" />
            <meta property="og:image:height" content="<?php echo isset($OG_Image_Height) ? $OG_Image_Height : ''; ?>" />

            <!-- CSS FILES -->
            <link rel="stylesheet" type="text/css" href="http://<?php echo $REGION['R_Domain'] ?>/stylesheets/mainscreen.min.css" media="all" />
            <!-- JS FILES -->
            <script src="http://<?php echo $REGION['R_Domain'] ?>/include/jquery-1.12.4.js"></script>
            <script src="http://<?php echo $REGION['R_Domain'] ?>/include/jquery-ui.js"></script>
            <script src="http://<?php echo $REGION['R_Domain'] ?>/include/jquery.bxslider.min.js"></script>
            <script type="text/javascript" src="http://<?php echo $REGION['R_Domain'] ?>/include/plugins/sweetalert/sweetalert.min.js"></script>
            <script type="text/javascript" src="http://<?php echo $REGION['R_Domain'] ?>/include/js/fancybox/jquery.fancybox.min.js"></script>
            <script type="text/javascript" src="http://<?php echo $REGION['R_Domain'] ?>/include/js/custom-script.js"></script>
            <script type="text/javascript" src="http://<?php echo $REGION['R_Domain'] ?>/include/jquery-ui-timepicker-addon.js"></script>
            <script type="text/javascript" src="http://<?php echo $REGION['R_Domain'] ?>/include/plugins/cycle/jquery.cycle.all.js"></script>
            <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo STATIC_MAP_API_KEY ?>" type="text/javascript"></script>
            <script src="http://<?php echo $REGION['R_Domain'] ?>/stylesheets/docs/assets/css/jquery.blockUI.js" type="text/javascript"></script>
            <script type="text/javascript" src="http://<?php echo $REGION['R_Domain'] ?>/include/jquery.maskedinput.min.js"></script>
            <script type="text/javascript" src="http://<?php echo $REGION['R_Domain'] ?>/include/public.js"></script>
            <script src='https://www.google.com/recaptcha/api.js'></script>
            <script type="text/javascript" src="http://<?php echo $REGION['R_Domain'] ?>/include/plugins/ckeditor/ckeditor.js"></script>
            <script src="http://<?php echo $REGION['R_Domain'] ?>/include/ckeditor/jquery.js"></script> 

            <?PHP
            //Getting Region Theme
            $getTheme = "SELECT * FROM  `tbl_Theme_Options` WHERE TO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
            $themeRegion = mysql_query($getTheme, $db) or die("Invalid query: $getTheme -- " . mysql_error());
            $THEME = mysql_fetch_assoc($themeRegion);

            $header_text_value = explode("-", $THEME['TO_Header_Text']);
            $season_text_value = explode("-", $THEME['TO_Season_Text']);
            $sec_nav_value = explode("-", $THEME['TO_Secondary_Navigation']);
            $main_nav_value = explode("-", $THEME['TO_Main_Navigation']);
            $slider_title_value = explode("-", $THEME['TO_Slider_Title']);
            $trip_planner_number_value = explode("-", $THEME['TO_Trip_Planner_Number']);
            $slider_desc_value = explode("-", $THEME['TO_Slider_Description']);
            $main_dd_value = explode("-", $THEME['TO_Drop_Down_Menu']);
            $main_page_title_value = explode("-", $THEME['TO_Main_Page_Title']);
            $main_page_body_value = explode("-", $THEME['TO_Main_Page_Body_Copy']);
            $cat_title_value = explode("-", $THEME['TO_Thumbnail_Category_Tite']);
            $view_all_value = explode("-", $THEME['TO_Thumbnail_View_All']);
            $sub_cat_value = explode("-", $THEME['TO_Thumbnail_Sub_Category_Title']);
            $listing_title_value = explode("-", $THEME['TO_Thumbnail_Listing_Title']);
            //
            $homepage_register_event_value = explode("-", $THEME['TO_Homepage_Event_Register_Event_Text']);
            //
            $map_title_value = explode("-", $THEME['TO_Map_Filter_Font']);
            $event_main_title_value = explode("-", $THEME['TO_Homepage_Event_Main_Title']);
            $event_date_value = explode("-", $THEME['TO_Homepage_Event_Date']);
            $event_title_value = explode("-", $THEME['TO_Homepage_Event_Title']);
            $event_text_value = explode("-", $THEME['TO_Homepage_Event_Text']);
            //
            $homepage_event_more_text_value = explode("-", $THEME['TO_Homepage_Event_More_Text']);
            $homepage_event_see_all_text_value = explode("-", $THEME['TO_Homepage_Event_See_All_Text']);
            //
            $footer_title_value = explode("-", $THEME['TO_Footer_Text_Title']);
            $footer_text_value = explode("-", $THEME['TO_Footer_Text']);
            $footer_links_value = explode("-", $THEME['TO_Footer_Links']);
            $footer_disc_value = explode("-", $THEME['TO_Footer_Disclaimer']);
            $listing_dd_text_value = explode("-", $THEME['TO_Listing_Drop_Down_Text']);
            $menu_text_value = explode("-", $THEME['TO_Menu_Text']);
            $listing_sub_heading_value = explode("-", $THEME['TO_Listing_Sub_Heading']);
            $listing_sub_nav_value = explode("-", $THEME['TO_Listing_Sub_Nav_Text']);
            $listing_day_text_value = explode("-", $THEME['TO_Listing_Day_Text']);
            $listing_hours_text_value = explode("-", $THEME['TO_Listing_Hours_Text']);
            $amenities_text_value = explode("-", $THEME['TO_Amenities_Text']);
            $download_text_value = explode("-", $THEME['TO_Downloads_Text']);
            $whats_near_dd_value = explode("-", $THEME['TO_Whats_Nearby_Drop_Down_Text']);
            $calendar_dd_value = explode("-", $THEME['TO_Calendar_Drop_Down_Text']);
            $calendar_go_text_value = explode("-", $THEME['TO_Calendar_Go_Button_Text']); //
            $detail_page_event_main_title_value = explode("-", $THEME['TO_Event_Page_Main_Title']);
            $detail_page_event_date_value = explode("-", $THEME['TO_Event_Page_Date']);
            $detail_page_event_data_label_value = explode("-", $THEME['TO_Event_Page_Data_Label']);
            $detail_page_event_data_content_value = explode("-", $THEME['TO_Event_Page_Data_Content']);
            $story_main_title_value = explode("-", $THEME['TO_Story_Page_Main_Title']);
            $story_share_text_value = explode("-", $THEME['TO_Story_Share_Text']);
            $story_content_title_value = explode("-", $THEME['TO_Story_Page_Content_Title']);
            $story_drop_down_text_value = explode("-", $THEME['TO_Story_Page_Drop_Down_Text']);
            $story_see_all_text_value = explode("-", $THEME['TO_Story_See_All_Text']);
            $route_main_title_value = explode("-", $THEME['TO_Route_Page_Main_Title']);
            $route_data_label_value = explode("-", $THEME['TO_Route_Page_Data_Label']);
            $route_data_content_value = explode("-", $THEME['TO_Route_Page_Data_Content']);
            //
            $pagination_number_value = explode("-", $THEME['TO_Pagination_Number_Text']);
            $pagination_text_value = explode("-", $THEME['TO_Pagination_Text']);
            $listing_main_title_value = explode("-", $THEME['TO_Listing_Title_Text']);
            $listing_address_value = explode("-", $THEME['TO_Listing_Location_Text']);
            $listing_nav_value = explode("-", $THEME['TO_Listing_Navigation_Text']);
            $general_paragraph_value = explode("-", $THEME['TO_General_Body_Copy']);
            $search_box_button_text_value = explode("-", $THEME['TO_Search_Box_Button_Text']);
            $text_box_text_inside_search_box_value = explode("-", $THEME['TO_Text_Box_Text_Inside_Box']);

            //CSS With Image variables
            $to_navigation_bar_bg_color = explode(">", $THEME['TO_Navigation_Bar']);
            $home_des_bg_color = explode(">", $THEME['TO_Homepage_Description_Background']);
            $home_event_des_bg_color = explode(">", $THEME['TO_Homepage_Events_Description_Background']);
            $listing_nav_gallary_bg_color = explode(">", $THEME['TO_Listing_Nav_Gallary_Background']);
            $listing_whats_nearby_bg_color = explode(">", $THEME['TO_Listing_Whats_Nearby_Background']);

            //Getting Region Theme Font, Due to our DB structure we are applying new query rather than JOIN
            $getFont = "SELECT * FROM `tbl_Theme_Options_Fonts`";
            $themeFontRegion = mysql_query($getFont, $db) or die("Invalid query: $getFont -- " . mysql_error());
            $THEME_ARRAY = array();
            while ($THEME_FONT = mysql_fetch_assoc($themeFontRegion)) {
                $THEME_ARRAY[$THEME_FONT['TOF_ID']] = $THEME_FONT['TOF_Name'];
            }
            ?>
            <style>
                /*!
                * Tourist Town
                * Date: Mon Jan 02 2017 17:00:00 PST
                * http://touristtown.ca
                *
                * Copyright 2017, 2018 TouristTown.
                *
                */
                /* Header
                -----------------------------------------------------------------------------------------------------------*/
                /* IR-> CSS FOR BOUNCE ANIMATION ON THE SLIDER OF LISTING PROFILES*/
                .arrow {
                    position: fixed;
                    bottom: 40px;
                    right: 5%;
                    z-index: 10; 
                }
                .bounce {
                    -webkit-animation: bounce 2s infinite;
                    animation: bounce 2s infinite;
                }
                /* Scroll down indicator (bouncing) */
                @-webkit-keyframes bounce {
                    0%, 20%, 50%, 80%, 100% {
                        -webkit-transform: translateY(0); }
                    40% {
                        -webkit-transform: translateY(-30px); }
                    60% {
                        -webkit-transform: translateY(-15px); } }
                @-moz-keyframes bounce {
                    0%, 20%, 50%, 80%, 100% {
                        -moz-transform: translateY(0); }
                    40% {
                        -moz-transform: translateY(-30px); }
                    60% {
                        -moz-transform: translateY(-15px); } }
                @keyframes bounce {
                    0%, 20%, 50%, 80%, 100% {
                        -webkit-transform: translateY(0);
                        -moz-transform: translateY(0);
                        -ms-transform: translateY(0);
                        -o-transform: translateY(0);
                        transform: translateY(0); }
                    40% {
                        -webkit-transform: translateY(-30px);
                        -moz-transform: translateY(-30px);
                        -ms-transform: translateY(-30px);
                        -o-transform: translateY(-30px);
                        transform: translateY(-30px); }
                    60% {
                        -webkit-transform: translateY(-15px);
                        -moz-transform: translateY(-15px);
                        -ms-transform: translateY(-15px);
                        -o-transform: translateY(-15px);
                        transform: translateY(-15px); } }
                /*END ARROW ANIMATION OF SLIDER....*/
                a {
                    color: <?php echo (isset($THEME['TO_Text_Link'])) ? $THEME['TO_Text_Link'] : ''; ?>;
                }
                .search_header {
                    background-color: <?php echo (isset($THEME['TO_Search_Box_Background_Color'])) ? $THEME['TO_Search_Box_Background_Color'] : ''; ?>;
                }
                input#txtSearch {
                    border-color: <?php echo (isset($THEME['TO_Text_Box_Border_Inside_Search_Box'])) ? $THEME['TO_Text_Box_Border_Inside_Search_Box'] : ''; ?>;
                    font-family: <?php echo (isset($THEME_ARRAY[$text_box_text_inside_search_box_value[0]]) === true && empty($THEME_ARRAY[$text_box_text_inside_search_box_value[0]]) === false) ? $THEME_ARRAY[$text_box_text_inside_search_box_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($text_box_text_inside_search_box_value[1])) ? $text_box_text_inside_search_box_value[1] : '' ?>px;
                    color: <?php echo (isset($text_box_text_inside_search_box_value[2])) ? $text_box_text_inside_search_box_value[2] : '' ?>;
                }
                .search_header .search_button input {
                    background-color: <?php echo (isset($THEME['TO_Search_Box_Background_Button_Color'])) ? $THEME['TO_Search_Box_Background_Button_Color'] : ''; ?>;
                    font-family: <?php echo (isset($THEME_ARRAY[$search_box_button_text_value[0]]) === true && empty($THEME_ARRAY[$search_box_button_text_value[0]]) === false) ? $THEME_ARRAY[$search_box_button_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($search_box_button_text_value[1])) ? $search_box_button_text_value[1] : '' ?>px;
                    color: <?php echo (isset($search_box_button_text_value[2])) ? $search_box_button_text_value[2] : '' ?>;
                }
                .theme-paragraph, .theme-paragraph p, .theme-paragraph ul li{
                    font-family: <?php echo (isset($THEME_ARRAY[$general_paragraph_value[0]]) === true && empty($THEME_ARRAY[$general_paragraph_value[0]]) === false) ? $THEME_ARRAY[$general_paragraph_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($general_paragraph_value[1])) ? $general_paragraph_value[1] : ''; ?>px;
                    color: <?php echo (isset($general_paragraph_value[2])) ? $general_paragraph_value[2] : ''; ?>;
                    line-height: <?php echo (isset($THEME['TO_General_Body_Copy_Line_Spacing'])) ? $THEME['TO_General_Body_Copy_Line_Spacing'] : ''; ?>em;
                }

                /* IR-> .theme-paragraph p, removed this from the uppper css 13 july, 2017 , and added the below css*/
                .listing-detail-left .theme-paragraph{
                    font-family: <?php echo (isset($THEME_ARRAY[$general_paragraph_value[0]]) === true && empty($THEME_ARRAY[$general_paragraph_value[0]]) === false) ? $THEME_ARRAY[$general_paragraph_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($general_paragraph_value[1])) ? $general_paragraph_value[1] : ''; ?>px;
                    color: <?php echo (isset($general_paragraph_value[2])) ? $general_paragraph_value[2] : ''; ?>;
                    line-height: <?php echo (isset($THEME['TO_General_Body_Copy_Line_Spacing'])) ? $THEME['TO_General_Body_Copy_Line_Spacing'] : ''; ?>em;
                }
                .inside-wrapper {
                    font-family: <?php echo (isset($THEME_ARRAY[$header_text_value[0]]) === true && empty($THEME_ARRAY[$header_text_value[0]]) === false) ? $THEME_ARRAY[$header_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($header_text_value[1])) ? $header_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($header_text_value[2])) ? $header_text_value[2] : ''; ?>;
                }
                .header-top-nav {
                    background-color: <?php echo (isset($THEME['TO_Header_Background_Color'])) ? $THEME['TO_Header_Background_Color'] : ''; ?>;
                }
                .logo-nav, .header-main-nav {
                    background-color: <?php echo (isset($THEME['TO_Logo_Background_Color'])) ? $THEME['TO_Logo_Background_Color'] : ''; ?>;
                }
                .inside-wrapper .dropdown .header-dropdown {
                    background-color: <?php echo (isset($THEME['TO_Season_Drop_Down_Background'])) ? $THEME['TO_Season_Drop_Down_Background'] : ''; ?>;
                }
                .inside-wrapper .dropdown .header-dropdown select {
                    font-family: <?php echo (isset($THEME_ARRAY[$season_text_value[0]]) === true && empty($THEME_ARRAY[$season_text_value[0]]) === false) ? $THEME_ARRAY[$season_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($season_text_value[1])) ? $season_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($season_text_value[2])) ? $season_text_value[2] : ''; ?>;
                    background-color: <?php echo (isset($THEME['TO_Season_Drop_Down_Background'])) ? $THEME['TO_Season_Drop_Down_Background'] : ''; ?>;
                }
                .inside-wrapper .dropdown .header-dropdown #dr .drop-down-arrow {
                    border-left-color: <?php echo (isset($THEME['TO_Season_Drop_Down_Arrow'])) ? $THEME['TO_Season_Drop_Down_Arrow'] : ''; ?>;
                }
                .logo-nav-container {
                    margin-top: <?php echo (isset($THEME['TO_Logo_Space_Above'])) ? $THEME['TO_Logo_Space_Above'] : ''; ?>px;
                    margin-bottom: <?php echo (isset($THEME['TO_Logo_Space_Below'])) ? $THEME['TO_Logo_Space_Below'] : ''; ?>px;
                }
                .main-nav-wrapper ul li a {
                    font-family: <?php echo (isset($THEME_ARRAY[$main_nav_value[0]]) === true && empty($THEME_ARRAY[$main_nav_value[0]]) === false) ? $THEME_ARRAY[$main_nav_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($main_nav_value[1])) ? $main_nav_value[1] : ''; ?>px;
                    color: <?php echo (isset($main_nav_value[2])) ? $main_nav_value[2] : ''; ?>;
                }
                .main-nav-wrapper ul li a:hover {
                    background-color: <?php echo (isset($main_nav_value[2])) ? $main_nav_value[2] : ''; ?>;
                }
                .main-nav-wrapper ul li ul.submenu .dd-inner .column .maps-text {
                    color: <?php echo (isset($main_nav_value[2])) ? $main_nav_value[2] : ''; ?>;
                }
                .main-nav-wrapper ul li ul.submenu {
                    border-color: <?php echo (isset($main_nav_value[2])) ? $main_nav_value[2] : ''; ?>;
                    background-color: <?php echo (isset($THEME['TO_Drop_Down_Menu_BG'])) ? $THEME['TO_Drop_Down_Menu_BG'] : ''; ?>;
                }
                .main-nav-wrapper ul li ul.submenu .dd-inner .column {
                    background-color: <?php echo (isset($THEME['TO_Drop_Down_Menu_BG'])) ? $THEME['TO_Drop_Down_Menu_BG'] : ''; ?>;
                }
                .main-nav-wrapper ul li ul.submenu .dd-inner .column a {
                    font-family: <?php echo (isset($THEME_ARRAY[$main_dd_value[0]]) === true && empty($THEME_ARRAY[$main_dd_value[0]]) === false) ? $THEME_ARRAY[$main_dd_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($main_dd_value[1])) ? $main_dd_value[1] : ''; ?>px;
                    color: <?php echo (isset($main_dd_value[2])) ? $main_dd_value[2] : ''; ?>;
                }
                .navigation-bar {
                    <?php if (isset($to_navigation_bar_bg_color[0]) && $to_navigation_bar_bg_color[0] != "") { ?>
                        background-image: url("http://<?php echo DOMAIN . IMG_ICON_REL . $to_navigation_bar_bg_color[0]; ?>");
                        width: 100%;
                        height: 10px;
                        background-color: transparent;
                    <?php } else { ?>
                        background-color: <?php echo $to_navigation_bar_bg_color[1] ?>;
                    <?php } ?>
                }
                .my-trips-count span {
                    background-color: <?php echo $THEME['TO_Trip_Planner_Number_Background'] ?>;
                    font-family: <?php echo (isset($THEME_ARRAY[$trip_planner_number_value[0]]) === true && empty($THEME_ARRAY[$trip_planner_number_value[0]]) === false) ? $THEME_ARRAY[$trip_planner_number_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($trip_planner_number_value[1])) ? $trip_planner_number_value[1] : ''; ?>px;
                    color: <?php echo (isset($trip_planner_number_value[2])) ? $trip_planner_number_value[2] : ''; ?>;
                }
                /* Main Navigation End*/
                /* Header End
                -----------------------------------------------------------------------------------------------------------*/
                /* Cycle Slider Start
                -----------------------------------------------------------------------------------------------------------*/
                .main_slider .slider_wrapper .sliderCycle .slide-images, .main_slider .slider_wrapper .homepage-painting .slide-images {
                    font-family: <?php echo (isset($THEME_ARRAY[$slider_title_value[0]]) === true && empty($THEME_ARRAY[$slider_title_value[0]]) === false) ? $THEME_ARRAY[$slider_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($slider_title_value[1])) ? $slider_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($slider_title_value[2])) ? $slider_title_value[2] : ''; ?>;
                }
                .main_slider .slider_wrapper .sliderCycle .slide-images .slider-text h3, .main_slider .slider_wrapper .homepage-painting .slide-images .slider-text h3{
                    font-family: <?php echo (isset($THEME_ARRAY[$slider_desc_value[0]]) === true && empty($THEME_ARRAY[$slider_desc_value[0]]) === false) ? $THEME_ARRAY[$slider_desc_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($slider_desc_value[1])) ? $slider_desc_value[1] : ''; ?>px;
                    color: <?php echo (isset($slider_desc_value[2])) ? $slider_desc_value[2] : ''; ?>;
                }
                /* Cycle Slider End
                -----------------------------------------------------------------------------------------------------------*/
                /* Description after slider
                -----------------------------------------------------------------------------------------------------------*/
                .description.theme-description {
                    <?php if (isset($home_des_bg_color[0]) && $home_des_bg_color[0] != "") { ?>
                        background-image: url("http://<?php echo DOMAIN . IMG_ICON_REL . $home_des_bg_color[0] ?>");
                    <?php } else { ?>
                        background-color: <?php echo $home_des_bg_color[1]; ?>
                    <?php } ?>
                }
                .description.theme-description .description-wrapper .description-inner .site-text.ckeditor-anchors p {
                    line-height: <?php echo (isset($THEME['TO_Homepage_Description_Space'])) ? $THEME['TO_Homepage_Description_Space'] : ''; ?>em;
                }
                .description .description-wrapper .description-inner h1.heading-text , .nearby-heading .heading-text {
                    font-family: <?php echo (isset($THEME_ARRAY[$main_page_title_value[0]]) === true && empty($THEME_ARRAY[$main_page_title_value[0]]) === false) ? $THEME_ARRAY[$main_page_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($main_page_title_value[1])) ? $main_page_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($main_page_title_value[2])) ? $main_page_title_value[2] : ''; ?>;
                }
                .description .description-wrapper .description-inner .site-text {
                    font-family: <?php echo (isset($THEME_ARRAY[$main_page_body_value[0]]) === true && empty($THEME_ARRAY[$main_page_body_value[0]]) === false) ? $THEME_ARRAY[$main_page_body_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($main_page_body_value[1])) ? $main_page_body_value[1] : ''; ?>px;
                    color: <?php echo (isset($main_page_body_value[2])) ? $main_page_body_value[2] : ''; ?>;
                }
                /* Description after slider End
                -----------------------------------------------------------------------------------------------------------*/
                /* Feature Stories
                -----------------------------------------------------------------------------------------------------------*/
                .section_stories .grid-wrapper .grid-inner h1.heading-text {
                    font-family: <?php echo (isset($THEME_ARRAY[$main_page_title_value[0]]) === true && empty($THEME_ARRAY[$main_page_title_value[0]]) === false) ? $THEME_ARRAY[$main_page_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($main_page_title_value[1])) ? $main_page_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($main_page_title_value[2])) ? $main_page_title_value[2] : ''; ?>;
                }
                .section_stories .grid-wrapper .grid-inner .feature-stories .feature-story .story-description {
                    font-family: <?php echo (isset($THEME_ARRAY[$general_paragraph_value[0]]) === true && empty($THEME_ARRAY[$general_paragraph_value[0]]) === false) ? $THEME_ARRAY[$general_paragraph_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($general_paragraph_value[1])) ? $general_paragraph_value[1] : ''; ?>px;
                    color: <?php echo (isset($general_paragraph_value[2])) ? $general_paragraph_value[2] : ''; ?>;
                    line-height: <?php echo (isset($THEME['TO_General_Body_Copy_Line_Spacing'])) ? $THEME['TO_General_Body_Copy_Line_Spacing'] : ''; ?>em;
                }
                /* Feature Stories End
                -----------------------------------------------------------------------------------------------------------*/
                /* Thumbnail Grid
                -----------------------------------------------------------------------------------------------------------*/
                .grid-left-title.heading-text {
                    font-family: <?php echo (isset($THEME_ARRAY[$cat_title_value[0]]) === true && empty($THEME_ARRAY[$cat_title_value[0]]) === false) ? $THEME_ARRAY[$cat_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($cat_title_value[1])) ? $cat_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($cat_title_value[2])) ? $cat_title_value[2] : ''; ?>;
                }
                .grid-left-view h3 a {
                    font-family: <?php echo (isset($THEME_ARRAY[$view_all_value[0]]) === true && empty($THEME_ARRAY[$view_all_value[0]]) === false) ? $THEME_ARRAY[$view_all_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($view_all_value[1])) ? $view_all_value[1] : ''; ?>px;
                    color: <?php echo (isset($view_all_value[2])) ? $view_all_value[2] : ''; ?>;
                }
                ul.bxslider li a h3.thumbnail-heading, .thumbnails.static-thumbs .thumb-item a h3.thumbnail-heading {
                    font-family: <?php echo (isset($THEME_ARRAY[$sub_cat_value[0]]) === true && empty($THEME_ARRAY[$sub_cat_value[0]]) === false) ? $THEME_ARRAY[$sub_cat_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($sub_cat_value[1])) ? $sub_cat_value[1] : ''; ?>px;
                    color: <?php echo (isset($sub_cat_value[2])) ? $sub_cat_value[2] : ''; ?>;
                }
                ul.bxslider li a h3.thumbnail-desc, .thumbnails.static-thumbs .thumb-item a h3.thumbnail-desc {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_title_value[0]]) === true && empty($THEME_ARRAY[$listing_title_value[0]]) === false) ? $THEME_ARRAY[$listing_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_title_value[1])) ? $listing_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($listing_title_value[2])) ? $listing_title_value[2] : ''; ?>;
                }
                .thumbnail-grid .grid-wrapper .grid-inner h1.heading-text{
                    font-family: <?php echo (isset($THEME_ARRAY[$main_page_title_value[0]]) === true && empty($THEME_ARRAY[$main_page_title_value[0]]) === false) ? $THEME_ARRAY[$main_page_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($main_page_title_value[1])) ? $main_page_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($main_page_title_value[2])) ? $main_page_title_value[2] : ''; ?>;
                }
                /* Thumbnail Grid End
                -----------------------------------------------------------------------------------------------------------*/
                /* Whats on around town
            -----------------------------------------------------------------------------------------------------------*/
                .description.whats-on {
                    <?php if (isset($home_event_des_bg_color[0]) && $home_event_des_bg_color[0] != "") { ?>
                        background-image: url("<?php echo IMG_ICON_REL . $home_event_des_bg_color[0] ?>");
                    <?php } else { ?>
                        background-color: <?php echo $home_event_des_bg_color[1]; ?>
                    <?php } ?>
                }            
                .whats-on.description .description-wrapper a{
                    font-family: <?php echo (isset($THEME_ARRAY[$homepage_register_event_value[0]]) === true && empty($THEME_ARRAY[$homepage_register_event_value[0]]) === false) ? $THEME_ARRAY[$homepage_register_event_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($homepage_register_event_value[1])) ? $homepage_register_event_value[1] : ''; ?>px;
                    color: <?php echo (isset($homepage_register_event_value[2])) ? $homepage_register_event_value[2] : ''; ?>;
                }
                .whats-on.description .description-wrapper .events-wrapper .events .event-title .heading-text {
                    font-family: <?php echo (isset($THEME_ARRAY[$event_main_title_value[0]]) === true && empty($THEME_ARRAY[$event_main_title_value[0]]) === false) ? $THEME_ARRAY[$event_main_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($event_main_title_value[1])) ? $event_main_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($event_main_title_value[2])) ? $event_main_title_value[2] : ''; ?>;
                }
                .whats-on.description .description-wrapper .events-wrapper .events .event-column .event-date {
                    font-family: <?php echo (isset($THEME_ARRAY[$event_date_value[0]]) === true && empty($THEME_ARRAY[$event_date_value[0]]) === false) ? $THEME_ARRAY[$event_date_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($event_date_value[1])) ? $event_date_value[1] : ''; ?>px;
                    color: <?php echo (isset($event_date_value[2])) ? $event_date_value[2] : ''; ?>;
                    text-transform: capitalize;
                }
                .whats-on.description .description-wrapper .events-wrapper .events .event-title a {
                    font-family: <?php echo (isset($THEME_ARRAY[$event_title_value[0]]) === true && empty($THEME_ARRAY[$event_title_value[0]]) === false) ? $THEME_ARRAY[$event_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($event_title_value[1])) ? $event_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($event_title_value[2])) ? $event_title_value[2] : ''; ?>;
                }
                .whats-on.description .description-wrapper .events-wrapper .events .event-column .event-desc {
                    font-family: <?php echo (isset($THEME_ARRAY[$event_text_value[0]]) === true && empty($THEME_ARRAY[$event_text_value[0]]) === false) ? $THEME_ARRAY[$event_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($event_text_value[1])) ? $event_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($event_text_value[2])) ? $event_text_value[2] : ''; ?>;
                }
                .whats-on.description .description-wrapper .events-wrapper .events .event-column .event-more a {
                    font-family: <?php echo (isset($THEME_ARRAY[$homepage_event_more_text_value[0]]) === true && empty($THEME_ARRAY[$homepage_event_more_text_value[0]]) === false) ? $THEME_ARRAY[$homepage_event_more_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($homepage_event_more_text_value[1])) ? $homepage_event_more_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($homepage_event_more_text_value[2])) ? $homepage_event_more_text_value[2] : ''; ?>;
                }
                .whats-on.description .description-wrapper .events-wrapper .events .upcoming-events .upcoming-events-wrapper a {
                    font-family: <?php echo (isset($THEME_ARRAY[$homepage_event_see_all_text_value[0]]) === true && empty($THEME_ARRAY[$homepage_event_see_all_text_value[0]]) === false) ? $THEME_ARRAY[$homepage_event_see_all_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($homepage_event_see_all_text_value[1])) ? $homepage_event_see_all_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($homepage_event_see_all_text_value[2])) ? $homepage_event_see_all_text_value[2] : ''; ?>;
                }
                /* Whats on around town End
                -----------------------------------------------------------------------------------------------------------*/
                /* Map Filter Start
                -----------------------------------------------------------------------------------------------------------*/
                .map-bottom-filters .map-filter.category {
                    font-family: <?php echo (isset($THEME_ARRAY[$map_title_value[0]]) === true && empty($THEME_ARRAY[$map_title_value[0]]) === false) ? $THEME_ARRAY[$map_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($map_title_value[1])) ? $map_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($map_title_value[2])) ? $map_title_value[2] : ''; ?>;
                }
                /* Map Filter End
                -----------------------------------------------------------------------------------------------------------*/
                /* Thumbnail slider start
                -----------------------------------------------------------------------------------------------------------*/
                .nbs-flexisel-nav-left, .nbs-flexisel-nav-left.disabled, .bx-wrapper .bx-prev {
                    <?php if (isset($THEME['TO_Thumbnail_Arrow_Left']) && $THEME['TO_Thumbnail_Arrow_Left'] != "") { ?>
                        background-image: url("http://<?php echo DOMAIN . IMG_ICON_REL . $THEME['TO_Thumbnail_Arrow_Left'] ?>");
                    <?php } ?>
                }
                .nbs-flexisel-nav-right, .nbs-flexisel-nav-right.disabled, .bx-wrapper .bx-next {
                    <?php if (isset($THEME['TO_Thumbnail_Arrow_Right']) && $THEME['TO_Thumbnail_Arrow_Right'] != "") { ?>
                        background-image: url("http://<?php echo DOMAIN . IMG_ICON_REL . $THEME['TO_Thumbnail_Arrow_Right'] ?>");
                    <?php } ?>
                }
                /* Thumbnail slider end
                -----------------------------------------------------------------------------------------------------------*/
                /* Footer
                -----------------------------------------------------------------------------------------------------------*/
                .footer-urls ul li {
                    border-color: <?php echo (isset($footer_links_value[2])) ? $footer_links_value[2] : ''; ?> 
                }
                .footer-urls ul li a {
                    font-family: <?php echo (isset($THEME_ARRAY[$footer_links_value[0]]) === true && empty($THEME_ARRAY[$footer_links_value[0]]) === false) ? $THEME_ARRAY[$footer_links_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($footer_links_value[1])) ? $footer_links_value[1] : ''; ?>px;
                    color: <?php echo (isset($footer_links_value[2])) ? $footer_links_value[2] : ''; ?>;
                }
                .footer-copyrights {
                    font-family: <?php echo (isset($THEME_ARRAY[$footer_disc_value[0]]) === true && empty($THEME_ARRAY[$footer_disc_value[0]]) === false) ? $THEME_ARRAY[$footer_disc_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($footer_disc_value[1])) ? $footer_disc_value[1] : ''; ?>px;
                    color: <?php echo (isset($footer_disc_value[2])) ? $footer_disc_value[2] : ''; ?>;
                }
                .footer {
                    background-color: <?php echo (isset($THEME['TO_Footer_Background_Color'])) ? $THEME['TO_Footer_Background_Color'] : ''; ?>;
                }
                .footer .footer-outer .footer-images, .footer .footer-outer .footer-urls {
                    border-color: <?php echo (isset($THEME['TO_Footer_Lines_Color'])) ? $THEME['TO_Footer_Lines_Color'] : ''; ?>;
                }
                /* Footer End
                -----------------------------------------------------------------------------------------------------------*/
                /* Bar Texture Start
                -----------------------------------------------------------------------------------------------------------*/
                .bar-texture {
                    <?php
                    $to_bar_texture = explode(">", $THEME['TO_Bar_Texture']);
                    if (isset($to_bar_texture[0]) && $to_bar_texture[0] != "") {
                        ?>
                        background-image: url("http://<?php echo DOMAIN . IMG_ICON_REL . $to_bar_texture[0] ?>");
                    <?php } else { ?>
                        background-color: <?php echo $to_bar_texture[1]; ?>;
                        background-image: none;
                    <?php } ?>
                }
                /* Bar Texture End
                -----------------------------------------------------------------------------------------------------------*/
                /* Listing Page Start
                -----------------------------------------------------------------------------------------------------------*/
                .description.listing-home-des {
                    <?php if (isset($listing_nav_gallary_bg_color[0]) && $listing_nav_gallary_bg_color[0] != "") { ?>
                        background-image: url("http://<?php echo DOMAIN . IMG_ICON_REL . $listing_nav_gallary_bg_color[0] ?>");
                    <?php } else { ?>
                        background-color: <?php echo $listing_nav_gallary_bg_color[1]; ?>
                    <?php } ?>
                }
                .listing-title-detail .listing-title {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_main_title_value[0]]) === true && empty($THEME_ARRAY[$listing_main_title_value[0]]) === false) ? $THEME_ARRAY[$listing_main_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_main_title_value[1])) ? $listing_main_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($listing_main_title_value[2])) ? $listing_main_title_value[2] : ''; ?>;
                }
                .listing-address {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_address_value[0]]) === true && empty($THEME_ARRAY[$listing_address_value[0]]) === false) ? $THEME_ARRAY[$listing_address_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_address_value[1])) ? $listing_address_value[1] : ''; ?>px;
                    color: <?php echo (isset($listing_address_value[2])) ? $listing_address_value[2] : ''; ?>;
                }
                .filter-inner.listing-nav ul li a
                {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_nav_value[0]]) === true && empty($THEME_ARRAY[$listing_nav_value[0]]) === false) ? $THEME_ARRAY[$listing_nav_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_nav_value[1])) ? $listing_nav_value[1] : ''; ?>px;
                    color: <?php echo (isset($listing_nav_value[2])) ? $listing_nav_value[2] : ''; ?>;
                }
                .listing-desc .menu-Button select {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_dd_text_value[0]]) === true && empty($THEME_ARRAY[$listing_dd_text_value[0]]) === false) ? $THEME_ARRAY[$listing_dd_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_dd_text_value[1])) ? $listing_dd_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($listing_dd_text_value[2])) ? $listing_dd_text_value[2] : ''; ?>;
                    background-color: <?php echo (isset($THEME['TO_Listing_Drop_Down_Background'])) ? $THEME['TO_Listing_Drop_Down_Background'] : ''; ?>;
                }
                .menu-header-container .drop-down-arrow {
                    border-left-color: <?php echo (isset($THEME['TO_Listing_Drop_Down_Arrow'])) ? $THEME['TO_Listing_Drop_Down_Arrow'] : ''; ?>;
                }
                .menu-item-heading-container .menu-title {
                    font-family: <?php echo (isset($THEME_ARRAY[$menu_text_value[0]]) === true && empty($THEME_ARRAY[$menu_text_value[0]]) === false) ? $THEME_ARRAY[$menu_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($menu_text_value[1])) ? $menu_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($menu_text_value[2])) ? $menu_text_value[2] : ''; ?>;
                }
                .listing-desc .menu-heading, .listing-review .review-heading { 
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_sub_heading_value[0]]) === true && empty($THEME_ARRAY[$listing_sub_heading_value[0]]) === false) ? $THEME_ARRAY[$listing_sub_heading_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_sub_heading_value[1])) ? $listing_sub_heading_value[1] : ''; ?>px;
                    color: <?php echo (isset($listing_sub_heading_value[2])) ? $listing_sub_heading_value[2] : ''; ?>;
                }
                .listing-detail-add-detail .ui-accordion .ui-accordion-header, .listing-detail-address.border-top-none .listing-address-container .listing-detail-add-detail a,
                .listing-detail-address .listing-address-container .listing-detail-add-detail .address-heading, .listing-detail-tripplanner .address-heading a, .listing-detail-address .listing-detail-add-detail .address-heading
                ,.listing-detail-address .listing-detail-add-detail .address-heading a
                {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_sub_nav_value[0]]) === true && empty($THEME_ARRAY[$listing_sub_nav_value[0]]) === false) ? $THEME_ARRAY[$listing_sub_nav_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_sub_nav_value[1])) ? $listing_sub_nav_value[1] : ''; ?>px;
                    color: <?php echo (isset($listing_sub_nav_value[2])) ? $listing_sub_nav_value[2] : ''; ?>;
                }
                .day-heading {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_day_text_value[0]]) === true && empty($THEME_ARRAY[$listing_day_text_value[0]]) === false) ? $THEME_ARRAY[$listing_day_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_day_text_value[1])) ? $listing_day_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($listing_day_text_value[2])) ? $listing_day_text_value[2] : ''; ?>;
                }
                .day-timing {
                    <?php $listing_hours_text_index = array_search($listing_hours_text_value[0], $THEME_ARRAY); ?>
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_hours_text_value[0]]) === true && empty($THEME_ARRAY[$listing_hours_text_value[0]]) === false) ? $THEME_ARRAY[$listing_hours_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_hours_text_value[1])) ? $listing_hours_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($listing_hours_text_value[2])) ? $listing_hours_text_value[2] : ''; ?>;
                }
                .businessListing-timing .acc-body {
                    font-family: <?php echo (isset($THEME_ARRAY[$amenities_text_value[0]]) === true && empty($THEME_ARRAY[$amenities_text_value[0]]) === false) ? $THEME_ARRAY[$amenities_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($amenities_text_value[1])) ? $amenities_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($amenities_text_value[2])) ? $amenities_text_value[2] : ''; ?>;
                }
                .listing-detail-address .listing-address-container .listing-detail-add-detail .businessListing-timing .acc-body.theme-downloads a {
                    font-family: <?php echo (isset($THEME_ARRAY[$download_text_value[0]]) === true && empty($THEME_ARRAY[$download_text_value[0]]) === false) ? $THEME_ARRAY[$download_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($download_text_value[1])) ? $download_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($download_text_value[2])) ? $download_text_value[2] : ''; ?>;
                }
                .fancybox-overlay { 
                    <?php if (isset($THEME['TO_Photo_Gallery_Background_Colour']) && $THEME['TO_Photo_Gallery_Background_Colour'] != "") { ?>
                        background: none;
                        background-color: <?php echo $THEME['TO_Photo_Gallery_Background_Colour']; ?>
                    <?php } ?>
                }
                /* Listing Page End
                -----------------------------------------------------------------------------------------------------------*/
                /* Stories Page Start
            -----------------------------------------------------------------------------------------------------------*/
                .description.stories .story-left h1.heading-text{
                    font-family: <?php echo (isset($THEME_ARRAY[$story_main_title_value[0]]) === true && empty($THEME_ARRAY[$story_main_title_value[0]]) === false) ? $THEME_ARRAY[$story_main_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($story_main_title_value[1])) ? $story_main_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($story_main_title_value[2])) ? $story_main_title_value[2] : ''; ?>;
                }
                .story-share .story-share-wrapper .story-share-text {
                    font-family: <?php echo (isset($THEME_ARRAY[$story_share_text_value[0]]) === true && empty($THEME_ARRAY[$story_share_text_value[0]]) === false) ? $THEME_ARRAY[$story_share_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($story_share_text_value[1])) ? $story_share_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($story_share_text_value[2])) ? $story_share_text_value[2] : ''; ?>;
                }
                .story-title { 
                    font-family: <?php echo (isset($THEME_ARRAY[$story_content_title_value[0]]) === true && empty($THEME_ARRAY[$story_content_title_value[0]]) === false) ? $THEME_ARRAY[$story_content_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($story_content_title_value[1])) ? $story_content_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($story_content_title_value[2])) ? $story_content_title_value[2] : ''; ?>;
                }
                .description.stories .story-right .upcoming-events .upcoming-events-wrapper a {
                    font-family: <?php echo (isset($THEME_ARRAY[$story_see_all_text_value[0]]) === true && empty($THEME_ARRAY[$story_see_all_text_value[0]]) === false) ? $THEME_ARRAY[$story_see_all_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($story_see_all_text_value[1])) ? $story_see_all_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($story_see_all_text_value[2])) ? $story_see_all_text_value[2] : ''; ?>;
                }
                .description.stories .story-right .story-filter #dr1{
                    font-family: <?php echo (isset($THEME_ARRAY[$story_drop_down_text_value[0]]) === true && empty($THEME_ARRAY[$story_drop_down_text_value[0]]) === false) ? $THEME_ARRAY[$story_drop_down_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($story_drop_down_text_value[1])) ? $story_drop_down_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($story_drop_down_text_value[2])) ? $story_drop_down_text_value[2] : ''; ?>;
                    background-color: <?php echo (isset($THEME['TO_Story_Page_Drop_Down_Background'])) ? $THEME['TO_Story_Page_Drop_Down_Background'] . ' !important' : ''; ?>;  
                }
                .description.stories .story-right .story-filter select{
                    font-family: <?php echo (isset($THEME_ARRAY[$story_drop_down_text_value[0]]) === true && empty($THEME_ARRAY[$story_drop_down_text_value[0]]) === false) ? $THEME_ARRAY[$story_drop_down_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($story_drop_down_text_value[1])) ? $story_drop_down_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($story_drop_down_text_value[2])) ? $story_drop_down_text_value[2] : ''; ?>;
                    background-color: <?php echo (isset($THEME['TO_Story_Page_Drop_Down_Background'])) ? $THEME['TO_Story_Page_Drop_Down_Background'] : ''; ?>;     
                }
                .story-filter .drop-down-arrow1 {
                    border-left-color: <?php echo (isset($THEME['TO_Story_Page_Drop_Down_Arrow'])) ? $THEME['TO_Story_Page_Drop_Down_Arrow'] : ''; ?>;
                }
                /* Stories Page End
                -----------------------------------------------------------------------------------------------------------*/
                /* Route Page End
                    -----------------------------------------------------------------------------------------------------------*/
                .listing-title-detail .route-title{
                    font-family: <?php echo (isset($THEME_ARRAY[$route_main_title_value[0]]) === true && empty($THEME_ARRAY[$route_main_title_value[0]]) === false) ? $THEME_ARRAY[$route_main_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($route_main_title_value[1])) ? $route_main_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($route_main_title_value[2])) ? $route_main_title_value[2] : ''; ?>;
                }
                .listing-detail-right .listing-detail-routes .address-heading{
                    font-family: <?php echo (isset($THEME_ARRAY[$route_data_label_value[0]]) === true && empty($THEME_ARRAY[$route_data_label_value[0]]) === false) ? $THEME_ARRAY[$route_data_label_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($route_data_label_value[1])) ? $route_data_label_value[1] : ''; ?>px;
                    color: <?php echo (isset($route_data_label_value[2])) ? $route_data_label_value[2] : ''; ?>;
                }
                .listing-detail-right .listing-detail-routes .address-data{
                    font-family: <?php echo (isset($THEME_ARRAY[$route_data_content_value[0]]) === true && empty($THEME_ARRAY[$route_data_content_value[0]]) === false) ? $THEME_ARRAY[$route_data_content_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($route_data_content_value[1])) ? $route_data_content_value[1] : ''; ?>px;
                    color: <?php echo (isset($route_data_content_value[2])) ? $route_data_content_value[2] : ''; ?>;
                }
                /* Route Page Start
                -----------------------------------------------------------------------------------------------------------*/
                /* Event Page Start
                -----------------------------------------------------------------------------------------------------------*/
                .first-filter input {
                    font-family: <?php echo (isset($THEME_ARRAY[$calendar_dd_value[0]]) === true && empty($THEME_ARRAY[$calendar_dd_value[0]]) === false) ? $THEME_ARRAY[$calendar_dd_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($calendar_dd_value[1])) ? $calendar_dd_value[1] : ''; ?>px;
                    color: <?php echo (isset($calendar_dd_value[2])) ? $calendar_dd_value[2] : ''; ?>;
                    background-color: <?php echo (isset($THEME['TO_Calendar_Drop_Down_Background'])) ? $THEME['TO_Calendar_Drop_Down_Background'] : ''; ?>;
                    border-color: <?php echo (isset($THEME['TO_Calendar_Drop_Down_Outline'])) ? $THEME['TO_Calendar_Drop_Down_Outline'] : ''; ?>;
                }
                .event-button { 
                    background-color: <?php echo (isset($THEME['TO_Calendar_Go_Button_Background'])) ? $THEME['TO_Calendar_Go_Button_Background'] : ''; ?>;
                    font-family: <?php echo (isset($THEME_ARRAY[$calendar_go_text_value[0]]) === true && empty($THEME_ARRAY[$calendar_go_text_value[0]]) === false) ? $THEME_ARRAY[$calendar_go_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($calendar_go_text_value[1])) ? $calendar_go_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($calendar_go_text_value[2])) ? $calendar_go_text_value[2] : ''; ?>;
                }
                .event-wrapper-inner .details .section-header h2{
                    font-family: <?php echo (isset($THEME_ARRAY[$detail_page_event_main_title_value[0]]) === true && empty($THEME_ARRAY[$detail_page_event_main_title_value[0]]) === false) ? $THEME_ARRAY[$detail_page_event_main_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($detail_page_event_main_title_value[1])) ? $detail_page_event_main_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($detail_page_event_main_title_value[2])) ? $detail_page_event_main_title_value[2] : ''; ?>;
                }
                .event-wrapper-inner .details .section-header .eventDate{
                    font-family: <?php echo (isset($THEME_ARRAY[$detail_page_event_date_value[0]]) === true && empty($THEME_ARRAY[$detail_page_event_date_value[0]]) === false) ? $THEME_ARRAY[$detail_page_event_date_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($detail_page_event_date_value[1])) ? $detail_page_event_date_value[1] : ''; ?>px;
                    color: <?php echo (isset($detail_page_event_date_value[2])) ? $detail_page_event_date_value[2] : ''; ?>;
                }
                .event-wrapper-inner .details .businessListing .event_container_outside .event_title_dt{
                    font-family: <?php echo (isset($THEME_ARRAY[$detail_page_event_data_label_value[0]]) === true && empty($THEME_ARRAY[$detail_page_event_data_label_value[0]]) === false) ? $THEME_ARRAY[$detail_page_event_data_label_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($detail_page_event_data_label_value[1])) ? $detail_page_event_data_label_value[1] : ''; ?>px;
                    color: <?php echo (isset($detail_page_event_data_label_value[2])) ? $detail_page_event_data_label_value[2] : ''; ?>;
                }
                .event-wrapper-inner .details .businessListing .event_container_outside .event_detail_dd{
                    font-family: <?php echo (isset($THEME_ARRAY[$detail_page_event_data_content_value[0]]) === true && empty($THEME_ARRAY[$detail_page_event_data_content_value[0]]) === false) ? $THEME_ARRAY[$detail_page_event_data_content_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($detail_page_event_data_content_value[1])) ? $detail_page_event_data_content_value[1] : ''; ?>px;
                    color: <?php echo (isset($detail_page_event_data_content_value[2])) ? $detail_page_event_data_content_value[2] : ''; ?>;
                }
                .event-wrapper-inner .details .businessListing .event_container_outside, .event-wrapper-inner .details .desc {
                    border-bottom-color: <?php echo (isset($THEME['TO_Event_Page_Data_Border_Color'])) ? $THEME['TO_Event_Page_Data_Border_Color'] : ''; ?>;
                }
                /* Event Page End
                    -----------------------------------------------------------------------------------------------------------*/
                /* Pagination Start
                -----------------------------------------------------------------------------------------------------------*/
                .pagination {
                    border-color: <?php echo (isset($THEME['TO_Pagination_Line'])) ? $THEME['TO_Pagination_Line'] : ''; ?>;
                }
                .page {
                    background-color: <?php echo (isset($THEME['TO_Pagination_Number_Background'])) ? $THEME['TO_Pagination_Number_Background'] : ''; ?>;
                    font-family: <?php echo (isset($THEME_ARRAY[$pagination_number_value[0]]) === true && empty($THEME_ARRAY[$pagination_number_value[0]]) === false) ? $THEME_ARRAY[$pagination_number_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($pagination_number_value[1])) ? $pagination_number_value[1] : ''; ?>px;
                    color: <?php echo (isset($pagination_number_value[2])) ? $pagination_number_value[2] : ''; ?>;
                }
                .pull-left {
                    font-family: <?php echo (isset($THEME_ARRAY[$pagination_text_value[0]]) === true && empty($THEME_ARRAY[$pagination_text_value[0]]) === false) ? $THEME_ARRAY[$pagination_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($pagination_text_value[1])) ? $pagination_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($pagination_text_value[2])) ? $pagination_text_value[2] : ''; ?>;
                }
                .page.arrows {
                    font-family: <?php echo (isset($THEME_ARRAY[$pagination_text_value[0]]) === true && empty($THEME_ARRAY[$pagination_text_value[0]]) === false) ? $THEME_ARRAY[$pagination_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($pagination_text_value[1])) ? $pagination_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($pagination_text_value[2])) ? $pagination_text_value[2] : ''; ?>;
                }
                .event-pager .pagination a.page.arrows, .event-pager .pagination a.page {
                    font-family: <?php echo (isset($THEME_ARRAY[$pagination_text_value[0]]) === true && empty($THEME_ARRAY[$pagination_text_value[0]]) === false) ? $THEME_ARRAY[$pagination_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($pagination_text_value[1])) ? $pagination_text_value[1] : ''; ?>px;
                    color: <?php echo (isset($pagination_text_value[2])) ? $pagination_text_value[2] : ''; ?>;
                }
                .page-active {
                    background-color: <?php echo (isset($THEME['TO_Pagination_Selected_Number_Background'])) ? $THEME['TO_Pagination_Selected_Number_Background'] : ''; ?>;
                }
                /* Pagination End
                -----------------------------------------------------------------------------------------------------------*/
            </style>
            <script type="text/javascript">

                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', '<?php echo $REGION['R_Tracking_ID'] ?>']);
                _gaq.push(['_trackPageview']);

                (function () {
                    var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(ga, s);
                })();

                // IR-> SCROLL DOWN CONTROL FOR DOWN ARROW
                $(window).scroll(function () {
                    if ($(this).scrollTop() > 10)
                    {
                        $('.arrow').hide();
                    } else
                    {
                        $('.arrow').show();
                    }
                });
                //
            </script>
    </head>

    <body>

        <?php
        //Stories SEO Name
        $storyCatSEO = "SELECT C_Name_SEO FROM tbl_Category WHERE C_Is_Blog = 1";
        $storyCatSEORes = mysql_query($storyCatSEO);
        $storyCatSEORow = mysql_fetch_array($storyCatSEORes);

        //Defualt Header Images
        if ($THEME['TO_Default_Header_Desktop'] != '') {
            $default_header_image = $THEME['TO_Default_Header_Desktop'];
        } else {
            $default_header_image = 'Default-Main-Desktop.jpg';
        }

        //Defualt Thumbnail Images
        if ($THEME['TO_Default_Thumbnail_Desktop'] != '') {
            $default_thumbnail_image = $THEME['TO_Default_Thumbnail_Desktop'];
        } else {
            $default_thumbnail_image = 'Default-Thumbnail-Desktop.jpg';
        }

        $TOWN_JOIN = "";
        $distance_select = '';
        $distance_having = '';
        $disable_distance = 'disabled';
        $include_free_listings = '';
        if (isset($_SESSION['TOWN']) && $_SESSION['TOWN'] > 0) {
            $disable_distance = '';
            $TOWN_JOIN = "INNER JOIN tbl_Business_Listing_Category_Region BLCR1 ON BL_ID = BLCR1.BLCR_BL_ID AND BLCR1.BLCR_BLC_R_ID != BLCR.BLCR_BLC_R_ID AND BLCR1.BLCR_BLC_R_ID = '" . encode_strings($_SESSION['TOWN'], $db) . "'";
        } else {
            $TOWN_JOIN = "LEFT JOIN tbl_Business_Listing_Category_Region BLCR1 ON BL_ID = BLCR1.BLCR_BL_ID AND BLCR1.BLCR_BLC_R_ID != BLCR.BLCR_BLC_R_ID";
            unset($_SESSION['DISTANCE']);
        }
        if ($_SESSION['DISTANCE'] && $_SESSION['DISTANCE'] > 0 && $_SESSION['TOWN'] && $_SESSION['TOWN'] > 0) {
            $getReg = " SELECT R_Lat, R_Long FROM tbl_Region WHERE R_ID = '" . encode_strings($_SESSION['TOWN'], $db) . "'";
            $regRes = mysql_query($getReg, $db) or die(mysql_error());
            $regRow = mysql_fetch_array($regRes);
            if (isset($regRow['R_Lat']) && $regRow['R_Lat'] != '') {
                $latitude = $regRow['R_Lat'];
            } else {
                $latitude = 0;
            }

            if (isset($regRow['R_Long']) && $regRow['R_Long'] != '') {
                $longitude = $regRow['R_Long'];
            } else {
                $longitude = 0;
            }
            $distance_select = ", ( 6371 * acos( cos( radians($latitude) ) * cos( radians( BL_Lat ) ) * cos( radians( BL_Long ) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians( BL_Lat ) ) ) ) AS distance";
            $distance_having = "having distance <= " . $_SESSION['DISTANCE'];
        }
        if ($REGION['R_Include_Free_Listings'] == 1) {
            $include_free_listings = '';
        } else {
            $include_free_listings = ' AND BL_Listing_Type > 1';
        }
        if ($REGION['R_Include_Free_Listings_On_Map'] == 1) {
            $include_free_listings_on_map = '';
        } else {
            $include_free_listings_on_map = ' AND BL_Listing_Type > 1';
        }
        if ($REGION['R_Order_Listings_Manually'] == 1) {
            $order_listings = 'ORDER BY BLO_Order ASC, LT_Order DESC, BL_Listing_Title';
        } else {
            $order_listings = 'ORDER BY BL_Points DESC, LT_Order DESC, BL_Listing_Title';
        }
        if ($browserERROR) {
            ?>
            <div class="browserOuter"><div class="browser">Some important features may not work in this version of your browser. Please upgrade to a current browser 
                    <a href="http://windows.microsoft.com/en-US/internet-explorer/downloads/ie" target="_blank">Internet Explorer</a>,
                    <a href="http://www.getfirefox.com" target="_blank">FireFox</a>,
                    <a href="http://www.google.com/chrome" target="_blank">Google Chrome</a></div></div>
            <?PHP
        }
        ?>
        <!--Start Header-->
        <header class="main-header">
            <!--Top Navigation Official Site...-->
            <div class="header-top-nav">
                <div class="header-top-nav-inside">
                    <div class="inside-wrapper">
                        <?php if (isset($REGION['R_Show_Hide_Town']) && $REGION['R_Show_Hide_Town'] == 1) { ?>
                            <div class="dropdown">
                                <form>
                                    <div class="header-dropdown">
                                        <div id="dr"><div class="drop-down-arrow"></div></div>
                                        <label for="change_town" class="label-hidden">Select Your Town</label>
                                        <select id="change_town">
                                            <option value="">Select Your Town</option>
                                            <?php
                                            $getTown = "SELECT R_ID, R_Name FROM tbl_Region 
                                                        INNER JOIN tbl_Business_Listing_Category_Region BLCR ON R_ID = BLCR.BLCR_BLC_R_ID 
                                                        INNER JOIN tbl_Business_Listing ON BLCR_BL_ID = BL_ID 
                                                        INNER JOIN tbl_Business_Listing_Category_Region BLCR1 ON BLCR1.BLCR_BL_ID = BL_ID 
                                                        AND BLCR1.BLCR_BLC_R_ID = " . $REGION['R_ID'] . " 
                                                        WHERE R_Type = 2 AND hide_show_listing = '1' GROUP BY R_ID ORDER BY R_Name";
                                            $resTown = mysql_query($getTown, $db) or die("Invalid query: $getTown -- " . mysql_error());
                                            while ($towns = mysql_fetch_array($resTown)) {
                                                ?>
                                                <option value="<?php echo $towns['R_ID'] ?>" <?php echo ((isset($_SESSION['TOWN']) && $_SESSION['TOWN'] == $towns['R_ID']) ? "selected" : "") ?>><?php echo $towns['R_Name'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="header-dropdown">
                                        <div id="dr"><div class="drop-down-arrow"></div></div>
                                        <label for="change_distance" class="label-hidden">Select Distance</label>
                                        <select id="change_distance" <?php echo $disable_distance; ?>>
                                            <option value="">Select Distance</option>
                                            <option value="10" <?php echo ($_SESSION['DISTANCE'] == 10 && $_SESSION['TOWN'] > 0) ? 'selected' : '' ?>>10 km</option>
                                            <option value="25" <?php echo ($_SESSION['DISTANCE'] == 25 && $_SESSION['TOWN'] > 0) ? 'selected' : '' ?>>25 km</option>
                                            <option value="50" <?php echo ($_SESSION['DISTANCE'] == 50 && $_SESSION['TOWN'] > 0) ? 'selected' : '' ?>>50 km</option>
                                            <option value="75" <?php echo ($_SESSION['DISTANCE'] == 75 && $_SESSION['TOWN'] > 0) ? 'selected' : '' ?>>75 km</option>
                                        </select>
                                    </div>
                                </form>
                            </div>
                            <?php
                        } elseif (isset($REGION['R_Topnav_Title']) && $REGION['R_Topnav_Title'] != "") {
                            ?>
                            <div class="site-name">
                                <p><?php echo $REGION['R_Topnav_Title']; ?></p>
                            </div>
                            <?php
                        }
                        $imgSE = '';
                        $sql_icons = "SELECT * FROM tbl_Region_Social WHERE RS_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
                        $result_icons = mysql_query($sql_icons, $db) or die(mysql_error());
                        $row_icons = mysql_fetch_assoc($result_icons);
                        if ($row_icons['RS_SE_Icon'] != "") {
                            $imgSE = 'DB/' . $row_icons['RS_SE_Icon'];
                        } else {
                            $imgSE = 'gb-search.png';
                        }
                        ?>
                        <?php
                        $sql_icons = "SELECT * FROM tbl_Region_Social 
                                      WHERE RS_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
                        $result_icons = mysql_query($sql_icons, $db) or die(mysql_error());
                        $row_icons = mysql_fetch_assoc($result_icons); //echo "<pre>"; print_r($row_icons); exit();
                        ?>
                        <div class="social-icons">
                            <div class="icons-wrapper">
                                <div class="tt-icons tt-search">
                                    <img onClick="displayHeaderForm();" src="<?php echo 'http://' . DOMAIN . '/images/' . $imgSE; ?>" alt="search"/>
                                    <form method="GET" class="navbar-search" id="search_form" action="/search.php">
                                        <div class="search_header">
                                            <div class="search_field">
                                                <label for="txtSearch" class="label-hidden">Search</label>
                                                <input type="text" id="txtSearch" name="txtSearch" onfocus="clearSearch();" placeholder="Search"/>
                                            </div>
                                            <div class="search_button"><input type="submit" name="submit" value="Go"/></div>
                                        </div>
                                    </form>
                                </div>
                                <?php
                                $facebook_link = preg_replace('/^www\./', '', $row_icons['RS_FB_Link']);
                                if ($row_icons['RS_FB_Link'] != "") {
                                    ?><div class="tt-icons"><a target="_blank" href="http://<?php echo str_replace(array('http://', 'https://'), '', $facebook_link) ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_FB_Icon'] != "") ? 'DB/' . $row_icons['RS_FB_Icon'] : 'facebook.png' ?>" alt="Facebook"/></a></div><?php } ?>
                                <?php
                                $rs_i_link = preg_replace('/^www\./', '', $row_icons['RS_I_Link']);
                                if ($row_icons['RS_I_Link'] != "") {
                                    ?><div class="tt-icons"><a target="_blank" href="http://<?php echo str_replace(array('http://', 'https://'), '', $rs_i_link) ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_I_Icon'] != "") ? 'DB/' . $row_icons['RS_I_Icon'] : 'instagram.png' ?>" alt="Instagram"/></a></div><?php } ?>
                                    <?php
                                    $rs_y_link = preg_replace('/^www\./', '', $row_icons['RS_Y_Link']);
                                    if ($row_icons['RS_Y_Link'] != "") {
                                        ?><div class="tt-icons"><a target="_blank" href="http://<?php echo str_replace(array('http://', 'https://'), '', $rs_y_link) ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_Y_Icon'] != "") ? 'DB/' . $row_icons['RS_Y_Icon'] : 'youtube-icon.png' ?>" alt="Youtube"/></a></div><?php } ?>
                                    <?php
                                    $rs_t_link = preg_replace('/^www\./', '', $row_icons['RS_T_Link']);
                                    if ($row_icons['RS_T_Link'] != "") {
                                        ?><div class="tt-icons"><a target="_blank" href="http://<?php echo str_replace(array('http://', 'https://'), '', $rs_t_link) ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_T_Icon'] != "") ? 'DB/' . $row_icons['RS_T_Icon'] : 'twitter.png' ?>" alt="Twitter"/></a></div><?php } ?>
                                    <?php
                                    $rs_s_link = preg_replace('/^www\./', '', $row_icons['RS_S_Link']);
                                    if ($row_icons['RS_S_Link'] != "") {
                                        ?><div class="tt-icons"><a target="_blank" href="http://<?php echo str_replace(array('http://', 'https://'), '', $rs_s_link) ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_S_Icon'] != "") ? 'DB/' . $row_icons['RS_S_Icon'] : 'snapchat.png' ?>" alt="Snapchat"/></a></div><?php } ?>
                                    <?php
                                    $rs_Info_link = preg_replace('/^www\./', '', $row_icons['RS_Info_Link']);
                                    if ($row_icons['RS_Info_Link'] != "") {
                                        ?><div class="tt-icons"><a target="_blank" href="http://<?php echo str_replace(array('http://', 'https://'), '', $rs_Info_link) ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_Info_Icon'] != "") ? 'DB/' . $row_icons['RS_Info_Icon'] : 'info.png' ?>" alt="Info"/></a></div><?php } ?>
                                    <?php
                                    $rs_c_link = preg_replace('/^www\./', '', $row_icons['RS_C_Link']);
                                    if ($row_icons['RS_C_Link'] != "") {
                                        ?><div class="tt-icons"><a target="_blank" href="http://<?php echo str_replace(array('http://', 'https://'), '', $rs_c_link) ?>"><img src="<?php echo 'http://' . DOMAIN ?>/images/<?php echo ($row_icons['RS_C_Icon'] != "") ? 'DB/' . $row_icons['RS_C_Icon'] : 'contact.png' ?>" alt="Contact"/></a></div><?php } ?>
                                <?php if (isset($_SESSION['REGION_AUTH_ACT_INACT']) && $_SESSION['REGION_AUTH_ACT_INACT'] == 1) { ?><div class="tt-icons"><a href="http://<?php echo $REGION['R_Domain'] ?>/auth_logout.php">Logout</a></div><?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Top Navigation Official Site...-->
            <!--Logo Navigation...-->
            <div class="logo-nav">
                <div class="logo-nav-wrapper">
                    <div class="logo-nav-container">
                        <div class="site-logo <?php echo (!isset($REGION['R_Article_Map_Links']) || $REGION['R_Article_Map_Links'] == 0) ? 'top-nav-show' : '' ?>">
                            <a href="/">
                                <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $THEME['TO_Desktop_Logo']; ?>"  alt="<?php echo isset($THEME['TO_Desktop_Logo_Alt']) ? $THEME['TO_Desktop_Logo_Alt'] : ''; ?>">
                            </a>
                        </div>
                        <div class="logo-top-nav">
                            <ul>
                                <?php
                                $nav_top = array();
                                $nav2 = array();
                                $nav3 = array();

                                $sql_nav = "SELECT C_ID, RC_C_ID, RC_Link, C_Name_SEO, C_Name, MN_ID, RC_Name, MN_Static_Links, RC_Status, MN_Order,C_Is_Blog FROM tbl_Main_Navigation 
                                LEFT JOIN tbl_Region_Category ON RC_C_ID = MN_C_ID AND RC_R_ID = MN_R_ID
                                LEFT JOIN tbl_Category ON C_ID = MN_C_ID AND C_Parent = 0     
                               WHERE MN_R_ID = '" . $REGION['R_ID'] . "' AND C_Is_Blog = 1 GROUP BY MN_ID ORDER BY MN_Order ASC";
                                $result = mysql_query($sql_nav);
                                while ($row = mysql_fetch_assoc($result)) {
                                    $nav2[] = $row;
                                }

                                $sql_nav1 = "SELECT MN_ID, MN_Static_Links, MN_Order FROM tbl_Main_Navigation   
                                WHERE MN_R_ID = '" . $REGION['R_ID'] . "' AND (MN_Static_Links =1 || MN_Static_Links = 2) GROUP BY MN_ID ORDER BY MN_Order ASC";
                                $result1 = mysql_query($sql_nav1);
                                while ($row1 = mysql_fetch_assoc($result1)) {
                                    $nav3[] = $row1;
                                }

                                $nav_top = array_merge($nav2, $nav3);
                                array_multisort(array_column($nav_top, "MN_Order"), SORT_DESC, $nav_top);

                                foreach ($nav_top as $row) {
                                    ?>
                                    <!--Show Articles and Map links in top if selected from admin-->
                                    <?php if ((!isset($REGION['R_Article_Map_Links']) || $REGION['R_Article_Map_Links'] == 0) && $row['MN_Static_Links'] == 1) { ?>
                                        <li><a  href="/maps.php"><?php echo ($REGION['R_Map_Nav_Title'] != "") ? $REGION['R_Map_Nav_Title'] : 'Maps' ?></a></li>
                                    <?php }
                                    ?>

                                    <?php if ((!isset($REGION['R_Article_Map_Links']) || $REGION['R_Article_Map_Links'] == 0) && $row['C_ID'] == 121) { ?>
                                        <?php
                                        $sqlBlog = "SELECT RC_Name, RC_Link, RC_Status, C_Is_Blog, C_Name_SEO, C_Name FROM tbl_Category
                                                LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID
                                                WHERE C_Is_Blog = 1  AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
                                        $resBlog = mysql_query($sqlBlog);
                                        $blog = mysql_fetch_array($resBlog);
                                        if ($blog['C_Is_Blog'] == 1 && $blog['RC_Status'] == 0 && (isset($REGION['R_Stories']) && $REGION['R_Stories'] == 1)) {
                                            if ($blog['RC_Link'] != '') {
                                                $blink = preg_replace('/^www\./', '', $blog['RC_Link']);
                                                $blink = 'http://' . str_replace(array('http://', 'https://'), '', $blink);
                                            } else {
                                                $blink = '/' . $blog['C_Name_SEO'] . '/';
                                            }
                                            ?>
                                            <li>
                                                <a   href="<?php echo $blink ?>"><?php echo ($blog['RC_Name'] != "") ? $blog['RC_Name'] : $blog['C_Name'] ?></a>
                                            </li>
                                            <?php
                                        }
                                    }
                                }
                                ?>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--Logo Navigation End...-->
            <!--Main Navigation End...-->
            <nav class="header-main-nav">
                <div class="main-nav-container">
                    <div class="main-nav-wrapper">
                        <ul>
                            <!--Main Category for specialty website-->
                            <?php
                            $nav = array();
                            $nav1 = array();
                            $nav2 = array();
                            $nav3 = array();
                            $nav4 = array();

                            $sql = "SELECT C_ID, RC_C_ID, RC_Link, C_Name_SEO, C_Name, MN_ID, RC_Name, RC_Status, MN_Order,C_Is_Blog FROM tbl_Main_Navigation
                            LEFT JOIN tbl_Region_Category ON RC_C_ID = MN_C_ID AND RC_R_ID = MN_R_ID
                            LEFT JOIN tbl_Category ON C_ID = MN_C_ID AND C_Parent = 0 
                            INNER JOIN tbl_Business_Listing_Category ON BLC_M_C_ID = MN_C_ID    
                            INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID AND BLCR_BLC_R_ID  " . encode_strings((($REGION['R_Parent'] == 0 && $REGION['R_Type'] != 4) ? "IN " . $regionList : "= " . $REGION['R_ID']), $db) . "
                            INNER JOIN tbl_Business_Listing ON BL_ID = BLC_BL_ID
                            WHERE MN_R_ID = '" . $REGION['R_ID'] . "' AND C_ID != 8 AND  MN_Static_Links = 0 AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = '1' $include_free_listings GROUP BY MN_ID ORDER BY MN_Order ASC";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($row = mysql_fetch_assoc($result)) {
                                $nav1[] = $row;
                            }
                            $sql_nav = "SELECT C_ID, RC_C_ID, RC_Link, C_Name_SEO, C_Name, MN_ID, RC_Name, MN_Static_Links, RC_Status, MN_Order,C_Is_Blog FROM tbl_Main_Navigation 
                            LEFT JOIN tbl_Region_Category ON RC_C_ID = MN_C_ID AND RC_R_ID = MN_R_ID
                            LEFT JOIN tbl_Category ON C_ID = MN_C_ID AND C_Parent = 0     
                           WHERE MN_R_ID = '" . $REGION['R_ID'] . "' AND C_Is_Blog = 1 GROUP BY MN_ID ORDER BY MN_Order ASC";
                            $result = mysql_query($sql_nav);
                            while ($row = mysql_fetch_assoc($result)) {
                                $nav2[] = $row;
                            }

                            $sql_nav1 = "SELECT MN_ID, MN_Static_Links, MN_Order FROM tbl_Main_Navigation   
                            WHERE MN_R_ID = '" . $REGION['R_ID'] . "' AND (MN_Static_Links =1 || MN_Static_Links = 2) GROUP BY MN_ID ORDER BY MN_Order ASC";
                            $result1 = mysql_query($sql_nav1);
                            while ($row1 = mysql_fetch_assoc($result1)) {
                                $nav3[] = $row1;
                            }
                            if ($REGION['R_Show_Hide_Event'] == 1) {
                                $sql_nav_event = "SELECT C_ID, RC_C_ID, RC_Link, C_Name_SEO, C_Name, MN_ID, RC_Name, MN_Static_Links, RC_Status, MN_Order,C_Is_Blog FROM tbl_Main_Navigation 
                            LEFT JOIN tbl_Region_Category ON RC_C_ID = MN_C_ID AND RC_R_ID = MN_R_ID
                            LEFT JOIN tbl_Category ON C_ID = MN_C_ID AND C_Parent = 0     
                           WHERE MN_R_ID = '" . $REGION['R_ID'] . "' AND C_ID = 8 AND C_Is_Blog = 0 GROUP BY MN_ID ORDER BY MN_Order ASC";
                                $result_event = mysql_query($sql_nav_event);
                                while ($row = mysql_fetch_assoc($result_event)) {
                                    $nav4[] = $row;
                                }
                            }

                            $nav = array_merge($nav1, $nav2, $nav3, $nav4);
                            array_multisort(array_column($nav, "MN_Order"), SORT_ASC, $nav);

                            foreach ($nav as $row) {
                                if (isset($REGION['R_Article_Map_Links']) && $REGION['R_Article_Map_Links'] == 1 && ($row['MN_Static_Links'] == 1 || $row['MN_Static_Links'] == 2 || $row['C_ID'] == 121)) {
                                    if ($row['C_Is_Blog'] == 1 && $row['RC_Status'] == 0 && (isset($REGION['R_Stories']) && $REGION['R_Stories'] == 1)) {
                                        if ($row['RC_Link'] != '') {
                                            $blink = preg_replace('/^www\./', '', $row['RC_Link']);
                                            $blink = 'http://' . str_replace(array('http://', 'https://'), '', $blink);
                                        } else {
                                            $blink = '/' . $row['C_Name_SEO'] . '/';
                                        }
                                        ?>
                                        <li><a href="<?php echo $blink ?>"><?php echo ($row['RC_Name'] != "") ? $row['RC_Name'] : $row['C_Name'] ?></a></li>
                                        <?php
                                    }
                                    if ($row['MN_Static_Links'] == 1) {
                                        ?>
                                        <li><a href="/maps.php"><?php echo ($REGION['R_Map_Nav_Title'] != "") ? $REGION['R_Map_Nav_Title'] : 'Search by Map' ?></a></li>
                                        <?php
                                    }
                                } else if ($row['RC_Status'] == 0 && ($row['MN_Static_Links'] != 1 && $row['MN_Static_Links'] != 2 && $row['C_ID'] != 121)) {
                                    if ($row['RC_Link'] != '') {
                                        $link = preg_replace('/^www\./', '', $row['RC_Link']);
                                        $link = 'http://' . str_replace(array('http://', 'https://'), '', $link);
                                    } else {
                                        $link = '/' . $row['C_Name_SEO'] . '/';
                                    }
                                    ?>
                                    <li><a href="<?php echo $link; ?>" onmouseover="dropdown_ads_impression(<?php echo $row['C_ID']; ?>);"><?php echo ($row['RC_Name'] != '') ? $row['RC_Name'] : $row['C_Name']; ?></a>
                                        <?php
                                        if ($row['C_ID'] != 8) {
                                            $sql_nav_sub = "SELECT C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category
                                                        LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                                                        INNER JOIN tbl_Business_Listing_Category ON BLC_C_ID = C_ID
                                                        INNER JOIN tbl_Business_Listing ON BL_ID = BLC_BL_ID
                                                        INNER JOIN tbl_Business_Listing_Category_Region BLCR ON BL_ID = BLCR.BLCR_BL_ID AND BLCR.BLCR_BLC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                                                        $TOWN_JOIN
                                                        WHERE C_Parent = " . $row['C_ID'] . " AND RC_Status = 0 AND RC_R_ID > 0 AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = '1' $include_free_listings
                                                        GROUP BY C_ID ORDER BY RC_Order";
                                        } else {
                                            $sql_nav_sub = "SELECT C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category 
                                                        LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                                                        WHERE C_Parent = " . $row['C_ID'] . " AND RC_Status = 0 AND RC_R_ID > 0
                                                        ORDER BY RC_Order ASC";
                                        }
                                        $result_sub = mysql_query($sql_nav_sub);
                                        echo '<ul class="submenu"><li><div class="dd-inner">';
                                        ?>
                                        <div  class="submenu-left-side">
                                            <?php
                                            while ($rowSub = mysql_fetch_array($result_sub)) {
                                                echo '<div class="column">';
                                                $class = 'first';
                                                if ($rowSub['RC_Name'] != '') {
                                                    echo '<a class="' . $class . '" href="/' . $row['C_Name_SEO'] . '/' . $rowSub['C_Name_SEO'] . '/">' . $rowSub['RC_Name'] . '</a>';
                                                } else {
                                                    echo '<a class="' . $class . '" href="/' . $row['C_Name_SEO'] . '/' . $rowSub['C_Name_SEO'] . '/">' . $rowSub['C_Name'] . '</a>';
                                                }
                                                echo '</div>';
                                            }
                                            ?>
                                        </div>
                                        <?php
                                        // Select Random advertisments
                                        $adQuery = "SELECT A_ID, BL_ID, A_C_ID, A_SC_ID, A_AT_ID, A_Title, A_Status, A_Website, A_Is_Deleted, A_Third_Party, 
                                                A_Approved_Logo, BL_Name_SEO FROM tbl_Advertisement 
                                                LEFT JOIN tbl_Business_Listing ON A_BL_ID = BL_ID $include_free_listings
                                                WHERE A_C_ID = '" . encode_strings($row['C_ID'], $db) . "' 
                                                AND A_AT_ID = 2 AND A_Status = 3 
                                                AND A_Website = '" . encode_strings($REGION['R_ID'], $db) . "' 
                                                AND A_Is_Deleted = 0 AND (A_End_Date >= CURDATE() OR A_End_Date = 0000-00-00)
                                                GROUP BY A_ID
                                                ORDER BY RAND() LIMIT 3";
                                        $adResult = mysql_query($adQuery) or die("Invalid query: $adQuery -- " . mysql_error());
                                        $count = mysql_num_rows($adResult);
                                        echo '<input type="hidden" value="' . $count . '" id="total-ads-' . $row['C_ID'] . '"/>';
                                        $adCounter = 0;
                                        while ($adRow = mysql_fetch_array($adResult)) {
                                            echo '<input type="hidden" value="' . $adRow['A_ID'] . '" id="ad' . $adCounter . '-' . $row['C_ID'] . '"/>';
                                            if ($adCounter == 0) {
                                                if ($adRow['A_Third_Party'] != "") {
                                                    echo '<div class="column-img"><a class="advertise-header" onclick = "advertClicks(' . $adRow['A_ID'] . ')" target="_blank" href="http://' . str_replace(array('http://', 'https://'), '', $adRow['A_Third_Party']) . '"><img class="col-first-img" src="http://' . DOMAIN . IMG_LOC_REL . $adRow['A_Approved_Logo'] . '" alt="' . $adRow['A_Title'] . '" /></a>';
                                                } else {
                                                    echo '<div class="column-img"><a class="advertise-header" onclick = "advertClicks(' . $adRow['A_ID'] . ')" href="/profile/' . $adRow['BL_Name_SEO'] . '/' . $adRow['BL_ID'] . '/"><img class="col-first-img" src="http://' . DOMAIN . IMG_LOC_REL . $adRow['A_Approved_Logo'] . '" alt="' . $adRow['A_Title'] . '" /></a>';
                                                }
                                            } else {
                                                if ($adRow['A_Third_Party'] != "") {
                                                    echo '<a class="advertise-header" onclick = "advertClicks(' . $adRow['A_ID'] . ')" target="_blank" href="http://' . str_replace(array('http://', 'https://'), '', $adRow['A_Third_Party']) . '"><img src="' . 'http://' . DOMAIN . IMG_LOC_REL . $adRow['A_Approved_Logo'] . '" alt="' . $adRow['A_Title'] . '" /></a>';
                                                } else {
                                                    echo '<a class="advertise-header" onclick = "advertClicks(' . $adRow['A_ID'] . ')" href="/profile/' . $adRow['BL_Name_SEO'] . '/' . $adRow['BL_ID'] . '/"><img src="http://' . DOMAIN . IMG_LOC_REL . $adRow['A_Approved_Logo'] . '" alt="' . $adRow['A_Title'] . '" /></a>';
                                                }
                                            }
                                            if ($adCounter == 2) {
                                                echo "</div></div>";
                                            }
                                            $adCounter++;
                                        }
                                        echo "</li></ul>";
                                    }
                                }
                                ?>
                        </ul>
                    </div>
                </div>
                <div class="navigation-bar"></div>
            </nav>
            <?php if ($THEME['TO_Scroll_Down_Arrow'] != "") { ?>
                <div class="arrow bounce">
                    <img src="<?php echo ($THEME['TO_Scroll_Down_Arrow'] != "") ? "http://" . DOMAIN . IMG_ICON_REL . $THEME['TO_Scroll_Down_Arrow'] : "" ?>" alt="Scroll Arrow">
                </div>
            <?php } ?>
        </header>
        <!--End Header-->