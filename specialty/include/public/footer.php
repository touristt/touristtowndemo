<!--bar texture start-->
<div class="bar-texture"></div>
<!--bar texture end-->
<!--Footer Start-->
<footer class="footer">
    <div class="footer-outer">
        <?php
        $footer = "SELECT FP_Link, FP_Photo, FP_Alt from tbl_Footer_Photo WHERE FP_Section = 1 AND FP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' ORDER BY FP_Order ASC";
        $footerResult = mysql_query($footer, $db) or die("Invalid query: $footer -- " . mysql_error());
        $footerCount = mysql_num_rows($footerResult);
        if ($footerCount > 0) {
            ?>
            <!--Footer logo section 1-->
            <div class="footer-images">
                <div class="footer-margin-auto">
                    <div>
                        <?php
                        while ($footerRow = mysql_fetch_array($footerResult)) {
                            $url = preg_replace('/^www\./', '', $footerRow['FP_Link']);
                            if ($footerRow['FP_Photo'] != "") {
                                if (isset($footerRow['FP_Link']) && $footerRow['FP_Link'] != "") {
                                    ?>
                                    <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $url) ?>"><img src="http://<?php echo DOMAIN . IMG_LOC_REL . $footerRow['FP_Photo'] ?>" alt="<?php echo $footerRow['FP_Alt'] ?>" border="0" /></a>
                                    <?php
                                } else {
                                    ?>
                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $footerRow['FP_Photo'] ?>" alt="<?php echo $footerRow['FP_Alt'] ?>" border="0" />    
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php
        $footer2 = "SELECT FP_Link, FP_Photo, FP_Alt from tbl_Footer_Photo WHERE FP_Section = 2 AND FP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' ORDER BY FP_Order ASC";
        $footerResult2 = mysql_query($footer2, $db) or die("Invalid query: $footer2 -- " . mysql_error());
        $footerCount2 = mysql_num_rows($footerResult2);
        if ($footerCount2 > 0) {
            ?>
            <!--Footer logo section 2-->
            <div class="footer-images border-top-none">
                <div class="footer-margin-auto">
                    <div>
                        <?php
                        while ($footerRow2 = mysql_fetch_array($footerResult2)) {
                            $url2 = preg_replace('/^www\./', '', $footerRow2['FP_Link']);
                            if ($footerRow2['FP_Photo'] != "") {
                                if (isset($footerRow2['FP_Link']) && $footerRow2['FP_Link'] != "") {
                                    ?>
                                    <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $url2) ?>"><img src="http://<?php echo DOMAIN . IMG_LOC_REL . $footerRow2['FP_Photo'] ?>" alt="<?php echo $footerRow2['FP_Alt'] ?>" border="0" /></a>
                                    <?php
                                } else {
                                    ?>
                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $footerRow2['FP_Photo'] ?>" alt="<?php echo $footerRow2['FP_Alt'] ?>" border="0" />    
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php
        $footer4 = "SELECT FP_Link, FP_Photo, FP_Alt from tbl_Footer_Photo WHERE  FP_Section = 4 AND FP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' ORDER BY FP_Order ASC";
        $footerResult4 = mysql_query($footer4, $db) or die("Invalid query: $footer4 -- " . mysql_error());
        $footerCount4 = mysql_num_rows($footerResult4);
        if ($footerCount4 > 0) {
            ?>
            <!--Footer logo section 2-->
            <div class="footer-images border-top-none">
                <div class="footer-margin-auto">
                    <div>
                        <?php
                        while ($footerRow4 = mysql_fetch_array($footerResult4)) {
                            $url4 = preg_replace('/^www\./', '', $footerRow4['FP_Link']);
                            if ($footerRow4['FP_Photo'] != "") {
                                if (isset($footerRow4['FP_Link']) && $footerRow4['FP_Link'] != "") {
                                    ?>
                                    <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $url4) ?>"><img src="http://<?php echo DOMAIN . IMG_LOC_REL . $footerRow4['FP_Photo'] ?>" alt="<?php echo $footerRow4['FP_Alt'] ?>" border="0" /></a>
                                    <?php
                                } else {
                                    ?>
                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $footerRow4['FP_Photo'] ?>" alt="<?php echo $footerRow4['FP_Alt'] ?>" border="0" />  
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php
        $footer5 = "SELECT FP_Link, FP_Photo, FP_Alt from tbl_Footer_Photo WHERE  FP_Section = 5 AND FP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' ORDER BY FP_Order ASC";
        $footerResult5 = mysql_query($footer5, $db) or die("Invalid query: $footer5 -- " . mysql_error());
        $footerCount5 = mysql_num_rows($footerResult5);
        if ($footerCount5 > 0) {
            ?>
            <!--Footer logo section 3-->
            <div class="footer-images border-top-none">
                <div class="footer-margin-auto">
                    <div>
                        <?php
                        while ($footerRow5 = mysql_fetch_array($footerResult5)) {
                            $url5 = preg_replace('/^www\./', '', $footerRow5['FP_Link']);
                            if ($footerRow5['FP_Photo'] != "") {
                                if (isset($footerRow5['FP_Link']) && $footerRow5['FP_Link'] != "") {
                                    ?>
                                    <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $url5) ?>"><img src="http://<?php echo DOMAIN . IMG_LOC_REL . $footerRow5['FP_Photo'] ?>" alt="<?php echo $footerRow5['FP_Alt'] ?>" border="0" /></a>
                                    <?php
                                } else {
                                    ?>
                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $footerRow5['FP_Photo'] ?>" alt="<?php echo $footerRow5['FP_Alt'] ?>" border="0" />     
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php
        $footer3 = "SELECT FP_Link, FP_Alt from tbl_Footer_Photo WHERE FP_Link != '' AND FP_Section = 3 AND FP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' ORDER BY FP_Order ASC";
        $footerResult3 = mysql_query($footer3, $db) or die("Invalid query: $footer3 -- " . mysql_error());
        $footerCount3 = mysql_num_rows($footerResult3);
        if ($footerCount3 > 0) {
            ?>
            <div class="footer-urls">
                <ul>
                    <?php
                    while ($footerRow3 = mysql_fetch_array($footerResult3)) {
                        $url3 = $footerRow3['FP_Link'];
                        if (filter_var($url3, FILTER_VALIDATE_URL)) {
                            $url3 = preg_replace('/^www\./', '', $footerRow3['FP_Link']);
                            ?>
                            <li> <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $url3) ?>"><?php echo $footerRow3['FP_Alt']; ?></a> </li>
                            <?php
                        } else {
                            ?>
                            <li> <a href="<?php echo str_replace(array('http://', 'https://'), '', $url3) ?>"><?php echo $footerRow3['FP_Alt']; ?></a> </li> 
                            <?php
                        }
                    }
                    ?>
                </ul>
            </div>
        <?php } ?>
        <div class="footer-copyrights">  
            <div class="footer-copy-wrapper ckeditor-anchors">
                <p><?php echo $REGION['R_Footer_Text']; ?></p>
            </div>
        </div>
    </div>
</footer>        
<!--Footer End-->
<script>
    function play_video(id, player_id) {
        PlayerReady(id, player_id);
    }
    function get_youtube_videoid(url) {
        var video_id = url.split('v=')[1];
        var ampersandPosition = video_id.indexOf('&');
        if (ampersandPosition != -1) {
            video_id = video_id.substring(0, ampersandPosition);
        }
        return video_id;
    }
    // 2. This code loads the IFrame Player API code asynchronously.
    var tag = document.createElement('script');

    tag.src = "http://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // 3. This function creates an <iframe> (and YouTube player)
    //    after the API code downloads.
    var total_players = $("#total_players").val();
    for (var i = 1; i < total_players; i++) {
        eval("var player" + i);
    }
    var current_player_id;

    function onYouTubePlayerAPIReady() {
        for (var i = 1; i < total_players; i++) {
            eval("player" + i + "= new YT.Player('player" + i + "'," + "{" +
                    "height: '538'," +
                    "width: '940'," +
                    "videoId:'" + $("#video-link-" + i).val() +
                    "',events: {'onStateChange': onPlayerStateChange}});");
        }
    }

    // 4. The API will call this function when the video player is ready.
    function PlayerReady(id, player_id) {
        var width = $(".player" + id + "_wrapper").parent(".slide-images").find('img.slider_image').width();
        var height = $(".player" + id + "_wrapper").parent(".slide-images").find('img.slider_image').height();
        $(".player" + id + "_wrapper").parent(".slide-images").find('img.slider_image').css('opacity', '0');
        $(".center #prev").css('display', 'none');
        $(".center #next").css('display', 'none');
        //    alert(top);
        $(".player" + id + "_wrapper").css('top', '0');
        $(".player" + id + "_wrapper").css('opacity', '1');
        $(".player" + id + "_wrapper").css('z-index', '600');
        $('.sliderCycle').cycle('pause');
        this[player_id].setSize(width, height);
        this[player_id].playVideo();
        current_player_id = id;
    }

    // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.ENDED) {
            stopVideo();
        }
    }
    function stopVideo() {
        //    this[player_id].stopVideo();
        $(".player" + current_player_id + "_wrapper").css('opacity', '0');
        $(".player" + current_player_id + "_wrapper").css('z-index', '0');
        $(".player" + current_player_id + "_wrapper").parent(".slide-images").find('img.slider_image').css('opacity', '1');
        if (total_players > 2) {
            $(".center #prev").css('display', 'block');
            $(".center #next").css('display', 'block');
        }
        $('.sliderCycle').cycle('resume');
    }
</script>
</body>
</html>
<?php
mysql_close($db);
?>
