<?php
require_once '../include/config.inc.php';
require_once '../include/public-site-functions.inc.php';
require_once '../update-impression-advert.php';

$sql = "SELECT * FROM tbl_Business_Feature_Photo WHERE BFP_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' ORDER BY BFP_Order";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$startingSlide = 0;
if (isset($_REQUEST['order']) && $_REQUEST['order'] != '') {
    $startingSlide = $_REQUEST['order'] - 1;
}

//Getting Region Theme
$getTheme = "SELECT TO_Photo_Gallery_Description_Text, TO_Photo_Gallery_Background_Colour FROM  `tbl_Theme_Options` WHERE TO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
$themeRegion = mysql_query($getTheme, $db) or die("Invalid query: $getTheme -- " . mysql_error());
$THEME = mysql_fetch_assoc($themeRegion);
$description_text_value = explode("-", $THEME['TO_Photo_Gallery_Description_Text']);

//Getting Region Theme Font, Due to our DB structure we are applying new query rather than JOIN
$getFont = "SELECT * FROM `tbl_Theme_Options_Fonts`";
$themeFontRegion = mysql_query($getFont, $db) or die("Invalid query: $getFont -- " . mysql_error());
$THEME_ARRAY = array();
while ($THEME_FONT = mysql_fetch_assoc($themeFontRegion)) {
    $THEME_ARRAY[$THEME_FONT['TOF_ID']] = $THEME_FONT['TOF_Name'];
}
?>

<style type="text/css">
    .center #prev {
        background-image: url('/stylesheets/images/sliderarrows.png');
        background-repeat: no-repeat;
        position: absolute;
        left: 0;
        top: 50%;
        height: 47px;
        width: 50px;
        z-index: 111;
        cursor: pointer;
        display: block;
    }
    .center #next {
        background-image: url('/stylesheets/images/sliderarrows.png');
        background-repeat: no-repeat;
        position: absolute;
        right: 0;
        top: 50%;
        height: 47px;
        width: 50px;
        background-position: 100% 0;
        z-index: 111;
        cursor: pointer;
        display: block;
    }
    .gallery-slide-images {
        overflow: hidden;
    }
    .gallery-slider-text {
        text-align: center;
        width: 100%;
        position: absolute;
        top: 0;
        background: rgba(0,0,0,0.5);
        color: #ffffff;
        padding: 1% 5% 1% 3%;
        font-family: 'newsGothicMT';
    }
    .gallery-slider-text .fb-like {
        float: left;
    }
    .gallery-slider-text .slider-caption {
        float: right;
        font-family: <?php echo (isset($THEME_ARRAY[$description_text_value[0]]) === true && empty($THEME_ARRAY[$description_text_value[0]]) === false) ? $THEME_ARRAY[$description_text_value[0]] : ""; ?>;
        font-size: <?php echo $description_text_value[1] ?>px;
        color: <?php echo $description_text_value[2] ?>;
    }
    .fancybox-bg {
        background-color: <?php echo $THEME['TO_Photo_Gallery_Background_Colour'] ?>;
    }
    .fancybox-slide>* {
        padding: 0;
    }
    .fancybox-close-small:focus:after{
        outline:none;
    }
    .fancybox-slide > *{
        text-align: center;
    }
    .slider-caption {
        margin-right: 81px;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        $('.galleryCycle').cycle({
            fx: 'scrollHorz',
            next: '#next',
            prev: '#prev',
            width: 1000,
            height: 500,
            fit: 1,
            startingSlide: <?php echo $startingSlide ?>
        });
        var slides = $('.galleryCycle').children().length;
        if (slides <= 1) {
            $('#next').css("display", "none");
            $('#prev').css("display", "none");
        } else {
            $('#next').css("display", "visible");
            $('#prev').css("display", "visible");
        }
    });
</script>
<div id="fb-root"></div>
<script>(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id))
        return;
    js = d.createElement(s);
    js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1356712584397235";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Jssor Slider Begin -->

<!-- You can move inline styles to css file or css block. -->

<div class="galleryCycle">
    <?php
    while ($row_slider = mysql_fetch_assoc($result)) {
        ?>
        <div class="gallery-slide-images">
            <?php if ($row_slider['BFP_Photo_1000X600'] != "") { ?>
                <img class="gallery_slider_image" class="show-slider-display" src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $row_slider['BFP_Photo_1000X600'] ?>" alt="<?php echo $row_slider['BFP_Title'] ?>">
            <?php } else { ?>
                <img class="gallery_slider_image" class="show-slider-display" src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $row_slider['BFP_Photo'] ?>" alt="<?php echo $row_slider['BFP_Title'] ?>">
            <?php } ?>
            <div class="gallery-slider-text">
                <div class="fb-like" data-href="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $row_slider['BFP_Photo']; ?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
                <div class="slider-caption"><?php echo $row_slider['BFP_Title']; ?></div>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<div class=center>
    <span id=prev></span>
    <span id=next></span>
</div>