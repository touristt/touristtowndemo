<?php
require '../include/public/header.php';

if (isset($_POST['confirmlisting'])) {
    $sql_update_hash = "UPDATE tbl_Business_Listing SET BL_Free_Listing_status = 0, BL_Free_Listing_Status_Date = '0000-00-00'
                        WHERE BL_ID = " . $activeBiz['BL_ID'];
    mysql_query($sql_update_hash);
    $_SESSION['update_hash'] = 1;
}

if (isset($_POST['reviewsubmit'])) {
    $response = $_POST['g-recaptcha-response'];
    $secret = '6Lf4_U0UAAAAAMTOrhaXHofRv-J4HzmQ-8ris_K0';
//get verify response data
    $verify = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $response);
    $responseData = json_decode($verify);
    if ($responseData->success == true) {
        $name = $_POST['name'];
        $title = $_POST['title'];
        $rate = $_POST['rating'];
        $desc = $_POST['description'];
        $date = date("Y-m-d");
        $review = " INSERT tbl_Business_Feature_Guest_Book SET
                    BFGB_Name = '$name',
                    BFGB_Title = '$title',
                    BFGB_BL_ID = '" . $activeBiz['BL_ID'] . "',
                    BFGB_Rating = '$rate',
                    BFGB_Desc = '" . encode_strings($desc, $db) . "',
                    BFGB_Date = '$date',
                    BFGB_Status = '0',
                    BFGB_Seen = '0'";
        mysql_query($review, $db) or die("Invalid query: $review -- " . mysql_error());
        $_SESSION['success'] = 1;
//Do you stuff
    } else {
        $name = $_POST['name'];
        $title = $_POST['title'];
        $rate = $_POST['rating'];
        $desc = $_POST['description'];
        $_SESSION['error'] = 1;
    }
}
//If the listing is free we have to show this snippet code
if ($activeBiz['hide_show_listing'] == 0) {
    $check_hide_status = (isset($url[3])) ? $url[3] : "";
    if ($check_hide_status == "") {
        ?>
        <section class="thumbnail-grid sub-cat-margin border-none">
            <div class="grid-wrapper" style="width: 310px;">
                <div class="grid-inner border-none padding-bottom-none" style="width: 310px;">
                    <div class="thumbnails static-thumbs" style="width: 310px;">                    
                        <div class="thumb-item">
                            <div class="message">
                                This listing has been disabled.
                            </div>
                        </div>            
                    </div>
                </div>
            </div>
        </section>   
        <div class="listing-footer">
            <?php
            require '../include/public/footer.php';
            ?>
        </div>
        <?php
        exit();
    }
    $startTime = date("h:i", strtotime('+5 minutes', $check_hide_status));
    $expire_time_check = date("h:i");
    if ($expire_time_check < $startTime) {
        
    } else {
        ?>
        <section class="thumbnail-grid sub-cat-margin border-none">
            <div class="grid-wrapper" style="width: 310px;">
                <div class="grid-inner border-none padding-bottom-none" style="width: 310px;">
                    <div class="thumbnails static-thumbs" style="width: 310px;">                    
                        <div class="thumb-item">
                            <div class="message">
                                This listing has been disabled.
                            </div>
                        </div>            
                    </div>
                </div>
            </div>
        </section> 
        <div class="listing-footer">
            <?php
            require '../include/public/footer.php';
            ?>
        </div>
        <?php
        exit();
    }
}
if ($activeBiz['BL_Listing_Type'] == 1) {
    ?>
    <!--Thumbnail Grid-->
    <section class="thumbnail-grid sub-cat-margin border-none">
        <div class="grid-wrapper" style="width: 310px;">
            <div class="grid-inner border-none padding-bottom-none" style="width: 310px;">
                <div class="thumbnails static-thumbs" style="width: 310px;">                    
                    <div class="thumb-item" style="height: auto;">
                        <img src="<?php echo 'https://' . DOMAIN . (($activeBiz['BL_Photo'] == '') ? (IMG_LOC_REL . $default_thumbnail_image) : (IMG_LOC_REL . $activeBiz['BL_Photo'])); ?>" alt="<?php echo $activeBiz['BL_Listing_Title'] ?>" longdesc="<?php echo $activeBiz['BL_Listing_Title'] ?>"/>
                        <?php
                        $freeListing = "SELECT RC_Name, C_Name, BL_Free_Listing_Status, BL_Free_Listing_Status_Hash
                                            FROM tbl_Business_Listing
                                            LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID
                                            LEFT JOIN tbl_Business_Listing_Category_Region ON BL_ID = BLCR_BL_ID
                                            LEFT JOIN tbl_Category ON BLC_C_ID = C_ID
                                            LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID
                                            AND RC_R_ID ='" . encode_strings($REGION['R_ID'], $db) . "'
                                            WHERE BL_ID ='" . encode_strings($activeBiz['BL_ID'], $db) . "'";
                        $freeRes = mysql_query($freeListing);
                        $freeRow = mysql_fetch_assoc($freeRes);
                        ?>
                        <h3 class="thumbnail-heading"><?php echo (($freeRow['RC_Name'] != "") ? $freeRow['RC_Name'] : $freeRow['C_Name']) ?></h3>
                        <h3 class="thumbnail-desc"><?php echo $activeBiz['BL_Listing_Title'] ?></h3>
                        <?php if ($REGION['R_Show_Hide_Phone'] == 1) { ?>
                            <?php if ($activeBiz['BL_Phone'] != "") { ?><h3 class="thumbnail-desc"><?php echo $activeBiz['BL_Phone'] ?></h3> <?php } ?>
                        <?php } if ($REGION['R_Show_Hide_Email'] == 1) { ?>
                            <?php if ($activeBiz['BL_Email'] != "") { ?><h3 class="thumbnail-desc"><a class="free_listing_email" href="mailto:<?php echo $activeBiz['BL_Email'] ?>"><?php echo $activeBiz['BL_Email'] ?></a></h3> <?php } ?>
                        <?php } ?>
                        <h3 class="thumbnail-desc"><?php echo $activeBiz['BL_Street']; ?></h3>
                        <h3 class="thumbnail-desc"><?php echo $activeBiz['BL_Town']; ?></h3>
                        <h3 class="thumbnail-desc"><?php echo ($activeBiz['BL_Province'] != '' && $activeBiz['BL_PostalCode'] != '' ? $activeBiz['BL_Province'] . ', ' : $activeBiz['BL_Province']) . $activeBiz['BL_PostalCode']; ?></h3>              
                        <!--Getting BL_Free_Listing_Status_Hash again in var $freeRow because after updation we have to get its latest value-->
                        <?php if (isset($url[3]) && $url[3] == $freeRow['BL_Free_Listing_Status_Hash'] && $freeRow['BL_Free_Listing_Status'] == 1) { ?>
                            <form action="" method ="post">
                                <input type="hidden" name="con" value="<?php echo $url[3]; ?>">
                                <h3 class="thumbnail-desc">
                                    <input type="submit" name="confirmlisting" value="Confirm Listing">
                                </h3>  
                            </form>
                            <?php
                        } elseif (isset($url[3]) && $url[3] == $freeRow['BL_Free_Listing_Status_Hash'] && $freeRow['BL_Free_Listing_Status'] == 0) {
                            print '<script>swal("Thank you", "You have already confirmed your Listing.", "success");</script>';
                        }
                        ?>
                    </div>

                </div>            
            </div>
        </div>
    </section> 
    <div class="listing-footer">
        <?php
        if (isset($_SESSION['update_hash']) && $_SESSION['update_hash'] == 1) {
            print '<script>swal("Thank you", "Your Listing has been confirmed.", "success");</script>';
            unset($_SESSION['update_hash']);
        }
        require '../include/public/footer.php';
        ?>
    </div>
    <!--Thumbnail Grid End-->
    <?php
    exit();
}

$VIDEOID = $activeBiz['BLP_Video'];
$pos = strpos($VIDEOID, '=');
if ($pos == false) {
    $video_link = end(explode('/', $VIDEOID));
} else {
    $video_link = end(explode("=", $VIDEOID));
}

function ctime($myTime, $end = false) {
    if ($myTime == '00:00:01' && !$end) {
        return 'Closed';
    } elseif ($myTime == '00:00:02') {
        return 'By Appointment';
    } elseif ($myTime == '00:00:03') {
        return 'Open 24 Hours';
    } else {
        $mySplit = explode(':', $myTime);
        return date('g:ia', mktime($mySplit[0], $mySplit[1], 1, 1, 1));
    }
}

//Getting Gallery Images if active
$count_gallery = 0;
$sqlFG = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 2 AND BLF_Active = 1";
$resultFG = mysql_query($sqlFG, $db) or die("Invalid query: $sqlFG -- " . mysql_error());
if (mysql_num_rows($resultFG) > 0) {
    $sqlG = "SELECT BFP_Title, BFP_Photo, BFP_Photo_195X195, BFP_Order FROM tbl_Business_Feature_Photo WHERE BFP_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' ORDER BY BFP_Order";
    $resultG = mysql_query($sqlG, $db) or die("Invalid query: $sqlG -- " . mysql_error());
    $count_gallery = mysql_num_rows($resultG);
}

//Getting About us if active
$count_about = 0;
$sqlFA = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 1 AND BLF_Active = 1";
$resultFA = mysql_query($sqlFA, $db) or die("Invalid query: $sqlFA -- " . mysql_error());
if (mysql_num_rows($resultFA) > 0) {
    $sql = "SELECT BFA_ID FROM tbl_Business_Feature_About WHERE BFA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'";
    $resultAbt = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $count_about = mysql_num_rows($resultAbt);
}

//Getting Menu if active
$count_menu = 0;
$sqlFM = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 6 AND BLF_Active = 1";
$resultFM = mysql_query($sqlFM, $db) or die("Invalid query: $sqlFM -- " . mysql_error());
if (mysql_num_rows($resultFM) > 0) {
    $sql = "SELECT BFM_ID FROM tbl_Business_Feature_Menu WHERE BFM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'";
    $resultMenu = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $count_menu = mysql_num_rows($resultMenu);
}

//Getting Entertainment if active
$count_Ent = 0;
$sqlFE = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 8 AND BLF_Active = 1";
$resultFE = mysql_query($sqlFE, $db) or die("Invalid query: $sqlFE -- " . mysql_error());
if (mysql_num_rows($resultFE) > 0) {
    $sql = "SELECT BFEA_ID FROM tbl_Business_Feature_Entertainment_Acts WHERE BFEA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND BFEA_Time >= CURDATE()";
    $resultEnt = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $count_Ent = mysql_num_rows($resultEnt);
}

//Getting Daily Feature if active
$count_DS = 0;
$sqlFDS = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 7 AND BLF_Active = 1";
$resultFDS = mysql_query($sqlFDS, $db) or die("Invalid query: $sqlFDS -- " . mysql_error());
if (mysql_num_rows($resultFDS) > 0) {
    $sql = "SELECT BFDS_ID FROM tbl_Business_Feature_Daily_Specials WHERE BFDS_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'";
    $resultDS = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $count_DS = mysql_num_rows($resultDS);
}
//Getting Agenda Minutes if active
$count_AM = 0;
$sqlFAM = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 13 AND BLF_Active = 1";
$resultFAM = mysql_query($sqlFAM, $db) or die("Invalid query: $sqlFAM -- " . mysql_error());
if (mysql_num_rows($resultFAM) > 0) {
    $sql = "SELECT BFAM_ID FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'";
    $resultAM = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $count_AM = mysql_num_rows($resultAM);
}
//Getting Guest Book if active
$sqlFGB = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 12 AND BLF_Active = 1";
$resultFGB = mysql_query($sqlFGB, $db) or die("Invalid query: $sqlFGB -- " . mysql_error());

//Getting Recommendations
$recommended = "SELECT * FROM tbl_Business_Listing 
LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
LEFT JOIN tbl_Recommendations ON R_Recommends = BL_ID 
WHERE R_Recommended = '" . encode_strings($activeBiz['BL_ID'], $db) . "'";
$recommended_result = mysql_query($recommended, $db) or die("Invalid query: $recommended -- " . mysql_error());
$recommended_count = mysql_num_rows($recommended_result);

if ($_REQUEST['nearby_select'] != '') {
    $default_radius = $_REQUEST['nearby_select'];
    $classForNearby = "nearby";
} else {
    $default_radius = $REGION['R_Radius'];
    $classForNearby = "";
}
?>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1356712584397235";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!--Slider start-->
<section class="main_slider">
    <div class="slider_wrapper">
        <div class="slider_wrapper_video">
            <div class="sliderCycle" data-cycle-loader="wait" data-cycle-fx=fade data-cycle-timeout=0 data-cycle-prev="#prev" data-cycle-next="#next">
                <?php if ($activeBiz['BLP_Header_Image'] != "") { ?>
                    <div class="slide-images">
                        <?php if ($VIDEOID != "") { ?>
                            <div class="player1_wrapper" style="position:absolute;opacity: 0;top:0;">
                                <input type="hidden" id="video-link-1" value="<?php echo $video_link ?>">
                                <div id="player1"></div>
                            </div>
                        <?php } ?>
                        <div class="slider-text slider-listing-text">
                            <div class="watch-video play-button">
                                <div class="slider-button slider-listing-button">
                                    <?php if ($VIDEOID != "") { ?>
                                        <a class="watch-full-video-listing" onclick="play_video(1, 'player1')">Watch video</a>
                                    <?php } if ($count_gallery > 0) { ?>
                                        <a class="view_all_photo view-gallery" href="/profile/<?php echo $activeBiz['BL_Name_SEO'] ?>/<?php echo $activeBiz['BL_ID'] ?>/gallery.php">VIEW ALL PHOTOS</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>                  
                        <img class="slider_image" src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $activeBiz['BLP_Header_Image'] ?>" alt="<?php echo $activeBiz['BLP_Header_Image_Alt'] ?>" longdesc="<?php echo $activeBiz['BLP_Header_Image_Alt'] ?>">                    
                    </div>
                <?php } else {
                    ?>
                    <div class="slide-images">
                        <?php if ($VIDEOID != "") { ?>
                            <div class="player1_wrapper" style="position:absolute;opacity: 0;top:0;">
                                <input type="hidden" id="video-link-1" value="<?php echo $video_link ?>">
                                <div  id="player1"></div>
                            </div>
                        <?php } ?>
                        <div class="slider-text slider-listing-text">

                            <div class="watch-video play-button">
                                <div class="slider-button slider-listing-button">
                                    <?php if ($VIDEOID != "") { ?>
                                        <img class="video_icon" src="<?php echo 'http://' . DOMAIN; ?>/images/videoicon.png" alt="">
                                        <a onclick="play_video(1, 'player1')">Watch full video</a>
                                    <?php } if ($count_gallery > 0) { ?>
                                        <a class="view_all_photo view-gallery" href="/profile/<?php echo $activeBiz['BL_Name_SEO'] ?>/<?php echo $activeBiz['BL_ID'] ?>/gallery.php">VIEW ALL PHOTOS</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>                  
                        <img class="slider_image" src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $default_header_image ?>" alt="<?php echo $activeBiz['BLP_Header_Image_Alt'] ?>" longdesc="<?php echo $activeBiz['BLP_Header_Image_Alt'] ?>">                    
                    </div>
                    <?php
                }
                ?>
            </div>
            <input type="hidden" id="total_players" value="2">
            <?php if ($THEME['TO_Slider_Overlay'] != "") { ?>
                <div class="image_overlay_img">
                    <div class="image_overlay">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_ICON_REL . $THEME['TO_Slider_Overlay']; ?>" alt="Slider Overlay">
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<!--Slider End-->
<section class="description listing-home-des margin-bottom-none">
    <div class="description-wrapper">
        <div class="event-filter">
            <div class="filter-inner listing-nav">
                <ul>
                    <li><a href="#overview">Overview</a></li>
                    <?php $check_more = 1; ?>
                    <?php if ($activeBiz['BL_Trip_Advisor']) { ?>
                        <li><a href="#reviews">Reviews</a></li>
                        <?php
                        $check_more++;
                    } if ($count_menu > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 6)) {
                        ?>
                        <li><a href="#menu-info">Menu</a></li>
                        <?php
                        $check_more++;
                    } if ($count_about > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 1)) {
                        ?>
                        <li><a href="#about-info">About Us</a></li>
                        <?php
                        $check_more++;
                    }
                    if ($count_DS > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 8)) {
                        if ($check_more == 4) {
                            ?>
                            <li><a class="more">More</a>
                                <ul class="more">
                                    <?php
                                }
                                ?>
                                <li><a href="#daily-s-check">Daily Features</a></li>
                                <?php
                                $check_more++;
                            }
                            if ($count_Ent > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 7)) {
                                if ($check_more == 4) {
                                    ?>
                                    <li><a class="more">More</a>
                                        <ul class="more">
                                            <?php
                                        }
                                        ?>
                                        <li><a href="#Entertainment-check">Entertainment</a></li>
                                        <?php
                                        $check_more++;
                                    }
                                    if ($count_AM > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 13)) {
                                        if ($check_more == 4) {
                                            ?>
                                            <li><a class="more">More</a>
                                                <ul class="more">
                                                    <?php
                                                }
                                                ?>
                                                <li><a href="#Agenda-and-Minutes">Agenda & Minutes</a></li>
                                                <?php
                                                $check_more++;
                                            }
                                            if (($activeBiz['BL_Lat'] && $activeBiz['BL_Long']) || ($activeBiz['BL_Street'])) {
                                                if ($check_more == 4) {
                                                    ?>
                                                    <li><a class="more">More</a>
                                                        <ul class="more">
                                                            <?php
                                                        }
                                                        ?>
                                                        <li><a href="#location-info">Location</a></li>
                                                        <?php
                                                        if ($check_more > 4) {
                                                            ?>
                                                        </ul>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                        </div>
                                        <div class="listing-title-detail">
                                            <div class="listing-title"><?php echo ($activeBiz['BL_Listing_Title']); ?></div>
                                            <div class="listing-address"><?php echo ($activeBiz['BL_Town'] != '' ? $activeBiz['BL_Town'] : '' ) . ($activeBiz['BL_Province'] != '' ? ', ' . $activeBiz['BL_Province'] : '') . ($activeBiz['BL_Country'] != '' ? ', ' . $activeBiz['BL_Country'] : ''); ?></div>
                                        </div>
                                        <div class="thumbnails listing-home-gallary">
                                            <div class="thumbnail-slider">
                                                <div class="inner">
                                                    <?PHP
//Gal_feature_count query is shifted top of the page.
                                                    if ($count_gallery > 0) {
                                                        ?>
                                                        <ul class="bxslider">
                                                            <?PHP
                                                            while ($rowG = mysql_fetch_assoc($resultG)) {
                                                                if ($rowG['BFP_Photo_195X195'] != "") {
                                                                    ?>
                                                                    <li><a href="/profile/<?php echo $activeBiz['BL_Name_SEO'] ?>/<?php echo $activeBiz['BL_ID'] ?>/gallery.php?order=<?php echo $rowG['BFP_Order'] ?>" class="view-gallery"><img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $rowG['BFP_Photo_195X195']; ?>" alt="<?php echo $rowG['BFP_Title']; ?>"/></a></li>
                                                                <?php } else if ($rowG['BFP_Photo'] != "") { ?>
                                                                    <li><a href="/profile/<?php echo $activeBiz['BL_Name_SEO'] ?>/<?php echo $activeBiz['BL_ID'] ?>/gallery.php?order=<?php echo $rowG['BFP_Order'] ?>" class="view-gallery"><img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $rowG['BFP_Photo']; ?>" alt="<?php echo $rowG['BFP_Title']; ?>"/></a></li>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </ul>    
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                        </section>
                                        <section class="description stories margin-bottom-none">
                                            <div class="description-wrapper">
                                                <div class="description-inner padding-none">
                                                    <div class="listing-detail-left">
                                                        <div id="overview" class="listing-desc theme-paragraph ckeditor-anchor-listing">
                                                            <?php
                                                            echo $activeBiz["BL_Description"];
                                                            ?>
                                                        </div>
                                                        <?php if ($activeBiz['BL_Trip_Advisor']) { ?>
                                                            <div id="reviews" class="listing-review">
                                                                <div class="review-heading">Reviews</div>
                                                                <div class="review-body"><?php echo $activeBiz['BL_Trip_Advisor'] ?></div>
                                                            </div>
                                                        <?php } ?>
                                                        <!-----MENU START--->
                                                        <?php if ($count_menu > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 6)) { ?>
                                                            <div id="menu-info" class="listing-desc theme-paragraph">
                                                                <div class="menu-header-container">
                                                                    <div class="menu-heading">Menu</div>
                                                                    <div class="menu-Button">
                                                                        <div class="drop-down-arrow listing"></div>
                                                                        <select id="menu_key" onchange="show_menu()">
                                                                            <?php
                                                                            foreach ($menuSections as $key => $val) {
                                                                                $sql3 = "SELECT BFM_ID FROM tbl_Business_Feature_Menu 
                                        WHERE BFM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND 
					BFM_Type = '" . encode_strings($key, $db) . "'";
                                                                                $result3 = mysql_query($sql3, $db) or die("Invalid query: $sql3 -- " . mysql_error());
                                                                                $count = mysql_num_rows($result3);
                                                                                if ($count > 0) {
                                                                                    ?>
                                                                                    <option value="<?php echo $key ?>"><?php echo $val['title'] ?></option>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                $sql_des = "Select BFMD_Description, BFMD_Type from tbl_Business_Feature_Menu_Description where  BFMD_BL_ID= '" . encode_strings($activeBiz['BL_ID'], $db) . "'";
                                                                $result_des = mysql_query($sql_des);
                                                                while ($row_des = mysql_fetch_assoc($result_des)) {
                                                                    if ($row_des['BFMD_Description'] != '') {
                                                                        ?>
                                                                        <div class="menu-desc theme-paragraph meun-hide-show menu-<?php echo $row_des['BFMD_Type']; ?>">
                                                                            <?php echo $row_des['BFMD_Description']; ?>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                }
                                                                $sql = "SELECT BFM_Title, BFM_Description, BFM_Price, BFM_Type FROM tbl_Business_Feature_Menu 
				WHERE BFM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' 
				ORDER BY BFM_Order";
                                                                $result1 = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                                                $title_type = '';
                                                                while ($data = mysql_fetch_assoc($result1)) {
                                                                    if ($title_type != $data['BFM_Type']) {
                                                                        if ($title_type == "") {
                                                                            
                                                                        } else if ($title_type != $data['BFM_Type']) {
                                                                            echo '</div>';
                                                                        }
                                                                        ?>
                                                                        <div class="type-child">
                                                                            <?php
                                                                        }
                                                                        ?>                   
                                                                        <div class="menu-desc meun-hide-show menu-<?php echo $data['BFM_Type']; ?>">
                                                                            <div class="menu-item-heading-container">
                                                                                <div class="menu-title"><?php echo $data["BFM_Title"]; ?></div>
                                                                                <div class="menu-price">$<?php echo $data["BFM_Price"]; //REPLACE                                                                                                                                                                                          ?></div>
                                                                            </div>
                                                                            <?php echo $data["BFM_Description"]; ?>
                                                                        </div>
                                                                        <?php
                                                                        $title_type = $data['BFM_Type'];
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <!-----MENU End--->
                                                            <!-----About Us start--->
                                                        <?php } if ($count_about > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 1)) { ?>
                                                            <div id="about-info" class="listing-desc">
                                                                <div class="menu-header-container">
                                                                    <div class="menu-heading">About Us</div>
                                                                    <div class="menu-Button">
                                                                        <div class="drop-down-arrow listing"></div>
                                                                        <select id="aboutus_id" onchange="show_aboutus()">
                                                                            <?php
                                                                            $sql2 = " SELECT BFA_ID, BFA_Title FROM tbl_Business_Feature_About
                                            WHERE BFA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' 
                                            ORDER BY BFA_Order";
                                                                            $result2 = mysql_query($sql2, $db) or die("Invalid query: $sql1 -- " . mysql_error());
                                                                            while ($data2 = mysql_fetch_assoc($result2)) {
                                                                                ?>
                                                                                <option value="<?php echo $data2['BFA_ID'] ?>"><?php echo $data2['BFA_Title'] ?></option> 
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                $sql = "SELECT BFA_ID, BFA_Description FROM tbl_Business_Feature_About
				LEFT JOIN tbl_Business_Listing ON BL_ID = BFA_BL_ID
				WHERE BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' ORDER BY BFA_Order";
                                                                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                                                while ($row = mysql_fetch_assoc($result)) {
                                                                    ?>
                                                                    <div class="aboutus-body-detail aboutus-<?php echo $row['BFA_ID'] ?>">
                                                                        <div class="aboutus-desc theme-paragraph">
                                                                            <?php echo $row["BFA_Description"]; ?>
                                                                        </div>

                                                                        <?php
                                                                        $sql_photo = " SELECT BFAP_Title, BFAP_Photo, BFAP_Photo_470X320 FROM tbl_Business_Feature_About_Photo
				WHERE BFAP_BFA_ID = '" . encode_strings($row['BFA_ID'], $db) . "' 
				ORDER BY BFAP_Order";
                                                                        $result_photo = mysql_query($sql_photo, $db) or die("Invalid query: $sql -- " . mysql_error());
                                                                        $count_photo = mysql_num_rows($result_photo);
                                                                        if ($count_photo > 0) {
                                                                            ?>
                                                                            <div class="about-image-container">
                                                                                <?php
                                                                                while ($data = mysql_fetch_assoc($result_photo)) {
                                                                                    if ($data['BFAP_Photo_470X320'] != "") {
                                                                                        ?>
                                                                                        <img class="about-img" src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $data['BFAP_Photo_470X320']; ?>" alt="<?php echo $data['BFAP_Title']; ?>" longdesc="<?php echo '/'; //REPLACE                                                                                                                                                                                       ?>" />
                                                                                    <?php } else if ($data['BFAP_Photo'] != "") { ?>
                                                                                        <img class="about-img" src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $data['BFAP_Photo']; ?>" alt="<?php echo $data['BFAP_Title']; ?>" longdesc="<?php echo '/'; //REPLACE                                                                                                                                                                                      ?>" />
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </div>
                                                                            <?php
                                                                        }
                                                                        ?>                                                                       
                                                                    </div>    
                                                                <?php } ?>
                                                            </div>
                                                            <!-----About Us End--->
                                                            <!-----Daily Features start--->
                                                        <?PHP } if ($count_DS > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 7)) { ?>
                                                            <div id="daily-s-check" class="listing-desc">
                                                                <div class="menu-header-container">
                                                                    <div class="menu-heading">Daily Features</div>
                                                                </div>
                                                                <?php
                                                                $features_bl_id = $activeBiz['BL_ID'];
                                                                $daily = "SELECT BFDSD_Description FROM `tbl_Business_Feature_Daily_Specials_Description` WHERE BFDSD_BL_ID = '$features_bl_id'";
                                                                $result1Daily = mysql_query($daily, $db) or die("Invalid query: $daily -- " . mysql_error());
                                                                $dataresult1Daily = mysql_fetch_assoc($result1Daily);
                                                                ?>
                                                                <div class="daily-description theme-paragraph"><?php echo $dataresult1Daily['BFDSD_Description']; ?></div>
                                                                <?php
                                                                foreach ($days as $key => $val) {
                                                                    $sql = "SELECT BFDS_Title, BFDS_Description, BFDS_Photo FROM tbl_Business_Feature_Daily_Specials 
				WHERE BFDS_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND 
					BFDS_Day = '" . encode_strings($key, $db) . "' LIMIT 1";
                                                                    $result1 = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                                                    $data = mysql_fetch_assoc($result1);
                                                                    if ($data) {
                                                                        ?>
                                                                        <div class="thumbnail">
                                                                            <?php
                                                                            if ($data['BFDS_Photo'] != "") {
                                                                                $dailyFeaturePhoto = $data['BFDS_Photo'];
                                                                                ?>
                                                                                <div class="feature-img-outer">
                                                                                    <img class="temp-width-feature" src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $dailyFeaturePhoto; ?>" alt="<?php echo $data['BFDS_Title']; //REPLACE                                                                                                                             ?>"	longdesc="<?php echo '/'; //REPLACE                                                                                                                             ?>" />
                                                                                </div>
                                                                                <?php
                                                                            }
                                                                            ?>        
                                                                            <div class="feature-body">
                                                                                <h5><?php echo $val; ?></h5>
                                                                                <div class="feature-title"><?php echo $data['BFDS_Title']; //REPLACE                                                                                                                           ?></div>
                                                                                <div class="feature-des">
                                                                                    <?php echo $data['BFDS_Description']; //REPLACE             ?>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </div>
                                                            <!-----Daily Features End--->
                                                            <!-----Entertainment start--->
                                                        <?PHP } if ($count_Ent > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 8)) { ?>               
                                                            <div id="Entertainment-check" class="listing-desc entert-padding-bottom">
                                                                <div class="menu-header-container">
                                                                    <div class="menu-heading">Entertainment</div>
                                                                    <div class="menu-Button">
                                                                        <div class="drop-down-arrow"></div>
                                                                        <select id="entert_id" onchange="show_entertainment()">
                                                                            <?php
                                                                            $sql = "SELECT BFEA_Time, DATE_FORMAT( BFEA_Time,  '%M' ) AS MONTH  FROM tbl_Business_Feature_Entertainment_Acts 
                                            WHERE BFEA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'  AND BFEA_Time >= CURDATE()
                                            Group by MONTH ORDER BY BFEA_Time ";
                                                                            $result1 = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                                                            while ($data2 = mysql_fetch_assoc($result1)) {
                                                                                ?>
                                                                                <option value="<?php echo date("M", strtotime($data2['BFEA_Time'])); ?>"><?php echo date("F  Y", strtotime($data2['BFEA_Time'])) ?></option> 
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                $enter_bl_id = $activeBiz['BL_ID'];
                                                                $daily = "SELECT BFED_Description FROM `tbl_Business_Feature_Entertainment_Description` WHERE BFED_BL_ID = '$enter_bl_id'";
                                                                $result1Daily = mysql_query($daily, $db) or die("Invalid query: $daily -- " . mysql_error());
                                                                $dataresult1Daily = mysql_fetch_assoc($result1Daily);
                                                                if ($dataresult1Daily['BFED_Description'] != '') {
                                                                    ?>
                                                                    <div class="entert-desc theme-paragraph"><?php echo $dataresult1Daily['BFED_Description']; ?></div>
                                                                    <?php
                                                                }
                                                                $sql = "SELECT *, LOWER(DATE_FORMAT(BFEA_Time, '%l:%i%p')) as myTime , LOWER(DATE_FORMAT(BFEA_End_Time, '%l:%i%p')) as myendTime FROM tbl_Business_Feature_Entertainment_Acts 
				WHERE BFEA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'  AND BFEA_Time >= CURDATE()
				ORDER BY BFEA_Time";
                                                                $result1 = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                                                while ($data = mysql_fetch_assoc($result1)) {
                                                                    ?>
                                                                    <div class="entert-desc entert-hide-show entert-<?php echo date("M", strtotime($data['BFEA_Time'])); ?>">
                                                                        <div class="entert-date"><?php echo date("F j, Y", strtotime($data['BFEA_Time'])); ?></div>
                                                                        <div class="entert-title"><?php echo $data['BFEA_Name']; ?></div>                           
                                                                        <div class="entert-body-desc theme-paragraph">
                                                                            <?PHP
                                                                            if (substr($data['BFEA_Description'], 0, 3) == '<p>') {
                                                                                echo $data['BFEA_Description'];
                                                                            } else {
                                                                                ?>
                                                                                <p><?php echo preg_replace("~\n~m", "<br>", $data['BFEA_Description']); ?></p>
                                                                            <?PHP } ?></div>                           
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <!-----Entertainment End--->
                                                        <?php } if ($count_AM > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 13)) { ?>
                                                            <!-----Agenda start--->
                                                            <?php
                                                            $agendaDesc_title = "SELECT * FROM tbl_Feature WHERE F_ID = 13";
                                                            $resDesc_title = mysql_query($agendaDesc_title, $db) or die("Invalid query: $agendaDesc_title --" . mysql_error());
                                                            $descAgenda_title = mysql_fetch_assoc($resDesc_title);
                                                            ?>
                                                            <div id="Agenda-and-Minutes" class="listing-desc">
                                                                <div class="menu-header-container">
                                                                    <div class="menu-heading"><?php echo $descAgenda_title['F_Name']; ?></div>
                                                                    <div class="menu-Button">
                                                                        <div class="drop-down-arrow"></div>
                                                                        <select id="agenda_id" onchange="show_agenda()">
                                                                            <?php
                                                                            $agenda = "SELECT YEAR(STR_TO_DATE(BFAM_Date, '%Y-%m-%d')) as Agenda_Year FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' GROUP BY Agenda_Year ORDER BY Agenda_Year ASC";
                                                                            $result_agenda = mysql_query($agenda, $db) or die("Invalid query: $agenda -- " . mysql_error());
                                                                            while ($rowAgenda = mysql_fetch_assoc($result_agenda)) {
                                                                                ?>
                                                                                <option value="<?php echo $rowAgenda['Agenda_Year']; ?>"><?php echo $rowAgenda['Agenda_Year']; ?></option> 
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="agenda-desc theme-paragraph"><?php echo $descAgenda_title['F_Description']; ?></div>
                                                                <form action="" onsubmit="search_word();
                                                                            return false;" method ="post">
                                                                    <div class="agenda-search-from">
                                                                        <div class="form-inside-div border-none padding-top-bottom agenda-form-main">
                                                                            <div class="agenda-form">
                                                                                <input name="searchAgnda" id="search-agenda-key" type="text" placeholder="Search" value="<?php echo $_POST['searchAgnda'] ?>" required>
                                                                                <input type="submit" name="agenda_form"  value="go"/>
                                                                            </div>     
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                                <div class="agenda-min-download-search" style="display: none;">
                                                                    <div id="search-heading" class="pdf-div-heading " ></div>                                    
                                                                    <?php
                                                                    $complete_agenda = "SELECT * FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' ORDER BY BFAM_Date ASC";
                                                                    $res_cmp_ag = mysql_query($complete_agenda, $db) or die("Invalid query: $complete_agenda -- " . mysql_error());
                                                                    if (mysql_num_rows($res_cmp_ag) > 0) {
                                                                        while ($data_agenda = mysql_fetch_assoc($res_cmp_ag)) {
                                                                            if ($data_agenda['BFAM_Minute'] != "" || $data_agenda['BFAM_Agenda'] != "") {
                                                                                $keywords = explode(",", $data_agenda['BFAM_Keywords']);
                                                                                ?>
                                                                                <div class="agenda-minutes agenda-show-hide <?php
                                                                                foreach ($keywords as $keysearch) {
                                                                                    echo 'agenda-' . $keysearch . ' ';
                                                                                }
                                                                                ?>" style="display: none;">
                                                                                    <div class="view-agenda-heading"><?php echo date("F d, Y", strtotime($data_agenda['BFAM_Date'])) ?></div>
                                                                                    <?php if ($data_agenda['BFAM_Agenda'] != "") { ?><div class="view-agenda-minute agenda-pdf"><a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $data_agenda['BFAM_Agenda'] ?>">View Agenda</a></div><?php } ?>
                                                                                    <?php if ($data_agenda['BFAM_Minute'] != "") { ?><div class="view-agenda-minute minuts-pdf"><a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $data_agenda['BFAM_Minute'] ?>">View Minutes</a></div><?php } ?>
                                                                                </div> 
                                                                                <?php
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                    <div class="agenda-minutes agenda-show-hide agenda-no-result " style="display: none;">No Result Found...</div> 
                                                                </div>
                                                                <?php
                                                                $agenda = "SELECT YEAR(STR_TO_DATE(BFAM_Date, '%Y-%m-%d')) as Agenda_Year FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' GROUP BY Agenda_Year ORDER BY Agenda_Year ASC";
                                                                $result_agenda = mysql_query($agenda, $db)
                                                                        or die("Invalid query: $agenda -- " . mysql_error());
                                                                if (mysql_num_rows($result_agenda) > 0) {
                                                                    while ($rowAgenda = mysql_fetch_array($result_agenda)) {
                                                                        ?>
                                                                        <div class="agenda-min-download agenda-<?php echo $rowAgenda['Agenda_Year']; ?>">
                                                                            <div class="pdf-div-heading"><?php echo $rowAgenda['Agenda_Year']; ?></div>                                    
                                                                            <?php
                                                                            $complete_agenda = "SELECT * FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND YEAR(STR_TO_DATE(BFAM_Date, '%Y-%m-%d')) = '" . encode_strings($rowAgenda['Agenda_Year'], $db) . "' ORDER BY BFAM_Date ASC";
                                                                            $res_cmp_ag = mysql_query($complete_agenda, $db)
                                                                                    or die("Invalid query: $complete_agenda -- " . mysql_error());
                                                                            if (mysql_num_rows($res_cmp_ag) > 0) {
                                                                                while ($data_agenda = mysql_fetch_assoc($res_cmp_ag)) {
                                                                                    if ($data_agenda['BFAM_Minute'] != "" || $data_agenda['BFAM_Agenda'] != "") {
                                                                                        ?>
                                                                                        <div class="agenda-minutes">
                                                                                            <div class="view-agenda-heading"><?php echo date("F d, Y", strtotime($data_agenda['BFAM_Date'])) ?></div>
                                                                                            <?php if ($data_agenda['BFAM_Agenda'] != "") { ?><div class="view-agenda-minute"><a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $data_agenda['BFAM_Agenda'] ?>">View Agenda</a></div><?php } ?>
                                                                                            <?php if ($data_agenda['BFAM_Minute'] != "") { ?><div class="view-agenda-minute"><a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $data_agenda['BFAM_Minute'] ?>">View Minutes</a></div><?php } ?>
                                                                                        </div> 
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </div>
                                                            <!-----agenda End--->                   
                                                        <?php } if ($recommended_count > 0) { ?> 
                                                            <!--Recommendations-->
                                                            <div id="recommendations" class="listing-desc" style="display: none;">
                                                                <div class="menu-header-container">
                                                                    <div class="menu-heading">Recommendations</div>
                                                                </div>
                                                                <div class="recommendations-padding">
                                                                    <?php
                                                                    while ($data = mysql_fetch_assoc($recommended_result)) {
                                                                        ?>
                                                                        <div class="recommendation">
                                                                            <div class="theme-paragraph recommendation-title"><strong><?php echo $data['BL_Listing_Title'] ?></strong></div>                                                                       
                                                                            <div class="theme-paragraph recommendation-desc"><?php echo $data['R_Text'] ?></div>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>

                                                        <?php } if (mysql_num_rows($resultFGB) > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 12)) { ?>

                                                            <div id="guest-book" class="listing-desc" style="<?php echo ($_SESSION['error'] == 1) ? '' : 'display:none;'; ?>">
                                                                <div class="menu-header-container">
                                                                    <div class="menu-heading">Guest Book</div>
                                                                    <div class="menu-Button">
                                                                        <a href="#reviewshow" onclick="$('#reviewshow').show();">Add review</a>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                $daily = "SELECT BFGB_Description FROM `tbl_Business_Feature_Guest_Book_Description` WHERE BFGB_BL_ID = '" . $activeBiz['BL_ID'] . "'";
                                                                $result1Daily = mysql_query($daily, $db);
                                                                $dataresult1Daily = mysql_fetch_assoc($result1Daily);
                                                                if ($dataresult1Daily['BFGB_Description']) {
                                                                    ?>
                                                                    <div class="agenda-desc theme-paragraph"><?php echo $dataresult1Daily['BFGB_Description']; ?></div>
                                                                <?php } ?>
                                                                <ul>
                                                                    <?php
                                                                    $sql = "SELECT * FROM tbl_Business_Feature_Guest_Book WHERE BFGB_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND 
                                                                            BFGB_Status = 1";
                                                                    $result1 = mysql_query($sql, $db);
                                                                    while ($data = mysql_fetch_assoc($result1)) {
                                                                        ?>
                                                                        <li class="thumbnail">
                                                                            <div class="guest-name theme-paragraph"><?php echo $data['BFGB_Name'] ?></div>
                                                                            <div class="guest-others theme-paragraph"><?php echo $data['BFGB_Title'] ?></div>
                                                                            <div class="guest-others">
                                                                                <?php
                                                                                $rating = $data['BFGB_Rating'];
                                                                                if ($rating == '1') {
                                                                                    ?>
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                                                    <?php
                                                                                } else if ($rating == '2') {
                                                                                    ?>
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                                                    <?php
                                                                                } else if ($rating == '3') {
                                                                                    ?>
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                                                    <?php
                                                                                } else if ($rating == '4') {
                                                                                    ?>
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                                                    <?php
                                                                                } else if ($rating == '5') {
                                                                                    ?>
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                                                    <?php
                                                                                } else {
                                                                                    ?>
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                                                    <?php
                                                                                }
                                                                                ?></div>
                                                                            <div class="guest-others theme-paragraph"><?php echo date("F d, Y", strtotime($data['BFGB_Date'])); ?></div>
                                                                            <div class="guest-others theme-paragraph"><?php echo $data['BFGB_Desc'] ?></div>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    <li class="thumbnail" id="reviewshow" style="<?php echo ($_SESSION['error'] == 1) ? '' : 'display:none;'; ?>">
                                                                        <form action="" method ="post" onSubmit="return validateGuestBookForm();">
                                                                            <div class="menu-header-container"><div class="menu-heading">Add Review</div></div>
                                                                            <div class="form-inside-div">
                                                                                <div class="form-data guest-book-form">
                                                                                    <label>Name</label>
                                                                                    <input name="name" type="text" value="<?php echo ($_SESSION['error']) ? $name : ''; ?>" required>
                                                                                </div>
                                                                                <div class="form-data guest-book-form">
                                                                                    <label>Title of Feedback</label>
                                                                                    <input name="title" type="text" value="<?php echo ($_SESSION['error']) ? $title : ''; ?>">
                                                                                </div>
                                                                                <div class="form-data guest-book-form">
                                                                                    <label>Rating</label>
                                                                                    <select name="rating" required>
                                                                                        <option value="">Rate review...</option>
                                                                                        <option value="1" <?php echo ($_SESSION['error'] && $rate == 1) ? 'selected' : ''; ?>>1</option>
                                                                                        <option value="2" <?php echo ($_SESSION['error'] && $rate == 2) ? 'selected' : ''; ?>>2</option>
                                                                                        <option value="3" <?php echo ($_SESSION['error'] && $rate == 3) ? 'selected' : ''; ?>>3</option>
                                                                                        <option value="4" <?php echo ($_SESSION['error'] && $rate == 4) ? 'selected' : ''; ?>>4</option>
                                                                                        <option value="5" <?php echo ($_SESSION['error'] && $rate == 5) ? 'selected' : ''; ?>>5</option>
                                                                                    </select> 
                                                                                </div>
                                                                                <div class="form-data guest-book-form">
                                                                                    <label>Description</label>
                                                                                    <textarea name="description" rows="7" cols="36"><?php echo ($_SESSION['error']) ? $desc : ''; ?></textarea>
                                                                                </div>
                                                                                <div class="form-data guest-book-form">
                                                                                    <label></label>
                                                                                    <div class="g-recaptcha" data-sitekey="6Lf4_U0UAAAAACQc7YpqpPkSE3-AQwjFiQwcgoXF"></div>
                                                                                </div>
                                                                                <div class="form-data guest-button-review">
                                                                                    <input type="submit" name="reviewsubmit" value="Submit"/>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        <?php }if (($activeBiz['BL_Lat'] && $activeBiz['BL_Long']) || ($activeBiz['BL_Street'])) {
                                                            ?>
                                                            <!-----Location start--->
                                                            <div id="location-info" class="listing-desc">
                                                                <div class="menu-header-container">
                                                                    <div class="menu-heading margin-top-none">Location</div>
                                                                </div>
                                                                <?php
                                                                if ($activeBiz['BL_Location_Description'] != '') {
                                                                    ?>
                                                                    <div class="location-desc theme-paragraph">
                                                                        <?php echo $activeBiz['BL_Location_Description'] ?>
                                                                    </div>
                                                                    <?PHP
                                                                }
                                                                ?>
                                                                <div class="location-map">
                                                                    <?php
                                                                    $markers = array();
                                                                    if ($activeBiz['BL_Lat'] && $activeBiz['BL_Long']) {
                                                                        $markers[] = array(
                                                                            'id' => $activeBiz['BL_ID'],
                                                                            'lat' => $activeBiz['BL_Lat'],
                                                                            'lon' => $activeBiz['BL_Long'],
                                                                            'name' => $activeBiz['BL_Listing_Title'],
                                                                            'address' => htmlspecialchars(trim($list['BL_Street']), ENT_QUOTES),
                                                                            'town' => htmlspecialchars(trim($list['BL_Town']), ENT_QUOTES)
                                                                        );
                                                                    }
                                                                    $markers = json_encode($markers);
                                                                    ?>
                                                                    <div style="width:620px; height: 390px; float: left; margin-bottom: 20px;" id="map_canvas">&nbsp;</div>
                                                                    <?php
                                                                    $map_center['latitude'] = $activeBiz['BL_Lat'];
                                                                    $map_center['longitude'] = $activeBiz['BL_Long'];
                                                                    $map_center['zoom'] = $REGION['R_Zoom'];
                                                                    $kml_json = json_encode(array());
                                                                    require_once '../map_script.php';
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        <?PHP } ?>
                                                        <!-----Location End--->
                                                    </div>
                                                    <div class="listing-detail-right">
                                                        <!--Social buttons-->
                                                        <div class="listing-detail-address">
                                                            <div class="fb-share-button" data-href="<?php print curPageURL(); ?>" data-layout="button_count"  data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php print curPageURL(); ?>%2F&amp;src=sdkpreparse">Share</a></div>
                                        <!--                    <div class="fb-like" data-href="<?php print curPageURL(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"style="vertical-align: middle;"></div>
                                                            --><iframe class="twitter-class"
                                                                       src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=http%3A%2F%2F<?php print curPageURL(); ?>&related=twitterapi%2Ctwitter&text=<?php echo $activeBiz['BL_Listing_Title'] ?> in <?php echo $REGION['R_Name'] ?>"
                                                                       width="70"
                                                                       height="25"
                                                                       style="border: 0; overflow: hidden; float: left;">
                                                            </iframe>
                                                        </div>
                                                        <?PHP if ($activeBiz['BL_Street']) { ?>
                                                            <div class="listing-detail-address border-top-none">
                                                                <div class="listing-detail-add-icon">
                                                                    <img src="<?php echo 'http://' . DOMAIN; ?>/images/locationicon.png" alt="Location">
                                                                </div>
                                                                <div class="listing-detail-add-detail">

                                                                    <div class="address-heading">
                                                                        <?php echo $activeBiz['BL_Street']; ?> 
                                                                    </div>
                                                                    <div class="address-heading">    
                                                                        <?php
                                                                        echo $activeBiz['BL_Town'];
                                                                        if ($activeBiz['BL_Town'] != '' && $activeBiz['BL_Province'] != '') {
                                                                            echo ', ';
                                                                        }
                                                                        echo $activeBiz['BL_Province'];
                                                                        if (($activeBiz['BL_Town'] != '' || $activeBiz['BL_Province'] != '') && $activeBiz['BL_PostalCode'] != '') {
                                                                            echo ', ';
                                                                        }
                                                                        echo $activeBiz['BL_PostalCode'];
                                                                        ?>
                                                                    </div>    
                                                                    <div class="address-heading">    
                                                                        <a class="view-map" href="#location-info">View Map</a>
                                                                    </div>     
                                                                </div>
                                                            </div>
                                                            <?PHP
                                                        }
                                                        $sql = "SELECT *  
                                                                FROM tbl_Business_Listing_Ammenity 
                                                                LEFT JOIN tbl_Ammenity_Icons ON AI_ID = BLA_BA_ID 
                                                                WHERE BLA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND AI_Image <> '' ORDER BY AI_Name";
                                                        $resultAI = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                                        $AIcount = mysql_num_rows($resultAI);

                                                        $sql_dpdf = "SELECT * FROM tbl_Description_PDF WHERE D_BL_ID='" . encode_strings($activeBiz['BL_ID'], $db) . "' ORDER BY D_PDF_Order ASC";
                                                        $result_dpdf = mysql_query($sql_dpdf, $db)
                                                                or die("Invalid query: $sql_dpdf -- " . mysql_error());
                                                        $pdf_num = mysql_num_rows($result_dpdf);
                                                        if ($activeBiz['BL_Phone'] || $activeBiz['BL_Toll_Free'] || $activeBiz['BL_Toll_Free'] || $AIcount > 0 || $pdf_num > 0 || $activeBiz['BL_Website'] || $activeBiz['BL_Email'] || $activeBiz['BL_Hours_Appointment'] == 1 || $activeBiz['BL_Hour_Mon_From'] != '00:00:00' || $activeBiz['BL_Hour_Tue_From'] != '00:00:00' || $activeBiz['BL_Hour_Wed_From'] != '00:00:00' || $activeBiz['BL_Hour_Thu_From'] != '00:00:00' || $activeBiz['BL_Hour_Fri_From'] != '00:00:00' || $activeBiz['BL_Hour_Sat_From'] != '00:00:00' || $activeBiz['BL_Hour_Sun_From'] != '00:00:00') {
                                                            ?>
                                                            <div class="listing-detail-address border-top-none">
                                                                <?php if ($activeBiz['BL_Phone']) {
                                                                    ?>
                                                                    <div class="listing-address-container">
                                                                        <div class="listing-detail-add-icon">
                                                                            <img src="<?php echo 'http://' . DOMAIN; ?>/images/Listing-Phone.png" alt="Phone">
                                                                        </div>
                                                                        <div class="listing-detail-add-detail">
                                                                            <div class="address-heading">
                                                                                <?php echo $activeBiz['BL_Phone']; ?> 
                                                                            </div>   
                                                                        </div>
                                                                    </div>    
                                                                    <?PHP
                                                                } if ($activeBiz['BL_Toll_Free']) {
                                                                    ?>
                                                                    <div class="listing-address-container">
                                                                        <div class="listing-detail-add-icon">
                                                                            <img src="<?php echo 'http://' . DOMAIN; ?>/images/Listing-Phone.png" alt="Phone">
                                                                        </div>
                                                                        <div class="listing-detail-add-detail">
                                                                            <div class="address-heading">
                                                                                <?php echo $activeBiz['BL_Toll_Free']; ?> 
                                                                            </div>   
                                                                        </div>
                                                                    </div>  
                                                                <?php } if ($activeBiz['BL_Fax']) {
                                                                    ?>
                                                                    <div class="listing-address-container">
                                                                        <div class="listing-detail-add-icon">
                                                                            <img src="<?php echo 'http://' . DOMAIN; ?>/images/fax.png" alt="Fax">
                                                                        </div>
                                                                        <div class="listing-detail-add-detail">
                                                                            <div class="address-heading" style=" margin-top: 3px;">
                                                                                <?php echo $activeBiz['BL_Fax']; ?> 
                                                                            </div>   
                                                                        </div>
                                                                    </div> 
                                                                    <?php
                                                                }
                                                                if ($activeBiz['BL_Cell']) {
                                                                    ?>
                                                                    <div class="listing-address-container">
                                                                        <div class="listing-detail-add-icon">
                                                                            <img src="<?php echo 'http://' . DOMAIN; ?>/images/Mobile.png" alt="Mobile">
                                                                        </div>
                                                                        <div class="listing-detail-add-detail">
                                                                            <div class="address-heading" style=" margin-top: 3px;">
                                                                                <?php echo $activeBiz['BL_Cell']; ?> 
                                                                            </div>   
                                                                        </div>
                                                                    </div> 
                                                                    <?php
                                                                }
                                                                if ($activeBiz['BL_Website']) {
                                                                    ?>
                                                                    <div class="listing-address-container">
                                                                        <div class="listing-detail-add-icon">
                                                                            <img src="<?php echo 'http://' . DOMAIN; ?>/images/Listing-Website.png" alt="Website">
                                                                        </div>
                                                                        <div class="listing-detail-add-detail">
                                                                            <div class="address-heading listing-leftnav-margin">
                                                                                <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $activeBiz['BL_Website']) ?>" target="_blank">View Website</a> 
                                                                            </div>   
                                                                        </div>
                                                                    </div>
                                                                    <?PHP
                                                                }
                                                                if ($activeBiz['BL_Email']) {
                                                                    ?>
                                                                    <div class="listing-address-container">
                                                                        <div class="listing-detail-add-icon">
                                                                            <img src="<?php echo 'http://' . DOMAIN; ?>/images/Listing-Email.png" alt="Email">
                                                                        </div>
                                                                        <div class="listing-detail-add-detail">
                                                                            <div class="address-heading listing-leftnav-margin">
                                                                                <a href="mailto:<?php echo $activeBiz['BL_Email']; ?>"><?php echo $activeBiz['BL_Email']; ?></a>
                                                                            </div>   
                                                                        </div>
                                                                    </div>
                                                                    <?PHP
                                                                }
                                                                if (!$activeBiz['BL_Hours_Disabled']) {
                                                                    if ($activeBiz['BL_Hours_Appointment'] == 1 || $activeBiz['BL_Hour_Mon_From'] != '00:00:00' || $activeBiz['BL_Hour_Tue_From'] != '00:00:00' || $activeBiz['BL_Hour_Wed_From'] != '00:00:00' || $activeBiz['BL_Hour_Thu_From'] != '00:00:00' || $activeBiz['BL_Hour_Fri_From'] != '00:00:00' || $activeBiz['BL_Hour_Sat_From'] != '00:00:00' || $activeBiz['BL_Hour_Sun_From'] != '00:00:00') {
                                                                        ?>
                                                                        <div class="listing-address-container">
                                                                            <div class="listing-detail-add-icon">
                                                                                <img src="<?php echo 'http://' . DOMAIN; ?>/images/Listing-Hours.png" alt="Hours">
                                                                            </div>
                                                                            <div class="listing-detail-add-detail">
                                                                                <div class="address-heading listing-leftnav-margin accordion view-acc">
                                                                                    <h4>View Hours</h4>
                                                                                    <?php
                                                                                    if ($activeBiz['BL_Hours_Appointment']) {
                                                                                        ?>
                                                                                        <div class="businessListing-timing">
                                                                                            <div class="day-heading">Hours</div>
                                                                                            <div class="day-timing">By Appointment</div>
                                                                                        </div>
                                                                                        <?php
                                                                                    } else {
                                                                                        ?>
                                                                                        <div class="businessListing-timing">
                                                                                            <?php
                                                                                            if ($activeBiz['BL_Hour_Mon_From'] != '00:00:00') {
                                                                                                ?>
                                                                                                <div class="day-heading">Monday</div>
                                                                                                <div class="day-timing">
                                                                                                    <?php echo ctime($activeBiz['BL_Hour_Mon_From']); ?> <?php echo $activeBiz['BL_Hour_Mon_From'] == '00:00:01' || $activeBiz['BL_Hour_Mon_From'] == '00:00:02' || $activeBiz['BL_Hour_Mon_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Mon_To'], true); ?>
                                                                                                </div>
                                                                                            <?php } if ($activeBiz['BL_Hour_Tue_From'] != '00:00:00') { ?>
                                                                                                <div class="day-heading">Tuesday</div>
                                                                                                <div class="day-timing">
                                                                                                    <?php echo ctime($activeBiz['BL_Hour_Tue_From']); ?> <?php echo $activeBiz['BL_Hour_Tue_From'] == '00:00:01' || $activeBiz['BL_Hour_Tue_From'] == '00:00:02' || $activeBiz['BL_Hour_Tue_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Tue_To'], true); ?>
                                                                                                </div>
                                                                                            <?php } if ($activeBiz['BL_Hour_Wed_From'] != '00:00:00') { ?>
                                                                                                <div class="day-heading">Wednesday</div>
                                                                                                <div class="day-timing">
                                                                                                    <?php echo ctime($activeBiz['BL_Hour_Wed_From']); ?> <?php echo $activeBiz['BL_Hour_Wed_From'] == '00:00:01' || $activeBiz['BL_Hour_Wed_From'] == '00:00:02' || $activeBiz['BL_Hour_Wed_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Wed_To'], true); ?>
                                                                                                </div>
                                                                                            <?php } if ($activeBiz['BL_Hour_Thu_From'] != '00:00:00') { ?>
                                                                                                <div class="day-heading">Thursday</div>
                                                                                                <div class="day-timing">
                                                                                                    <?php echo ctime($activeBiz['BL_Hour_Thu_From']); ?> <?php echo $activeBiz['BL_Hour_Thu_From'] == '00:00:01' || $activeBiz['BL_Hour_Thu_From'] == '00:00:02' || $activeBiz['BL_Hour_Thu_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Thu_To'], true); ?>
                                                                                                </div>
                                                                                            <?php } if ($activeBiz['BL_Hour_Fri_From'] != '00:00:00') { ?>
                                                                                                <div class="day-heading">Friday</div>
                                                                                                <div class="day-timing">
                                                                                                    <?php echo ctime($activeBiz['BL_Hour_Fri_From']); ?> <?php echo $activeBiz['BL_Hour_Fri_From'] == '00:00:01' || $activeBiz['BL_Hour_Fri_From'] == '00:00:02' || $activeBiz['BL_Hour_Fri_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Fri_To'], true); ?>
                                                                                                </div>
                                                                                            <?php } if ($activeBiz['BL_Hour_Sat_From'] != '00:00:00') { ?>
                                                                                                <div class="day-heading">Saturday</div>
                                                                                                <div class="day-timing">
                                                                                                    <?php echo ctime($activeBiz['BL_Hour_Sat_From']); ?> <?php echo $activeBiz['BL_Hour_Sat_From'] == '00:00:01' || $activeBiz['BL_Hour_Sat_From'] == '00:00:02' || $activeBiz['BL_Hour_Sat_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Sat_To'], true); ?>
                                                                                                </div>
                                                                                            <?php } if ($activeBiz['BL_Hour_Sun_From'] != '00:00:00') { ?>
                                                                                                <div class="day-heading">Sunday</div>
                                                                                                <div class="day-timing">
                                                                                                    <?php echo ctime($activeBiz['BL_Hour_Sun_From']); ?> <?php echo $activeBiz['BL_Hour_Sun_From'] == '00:00:01' || $activeBiz['BL_Hour_Sun_From'] == '00:00:02' || $activeBiz['BL_Hour_Sun_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Sun_To'], true); ?>
                                                                                                </div>
                                                                                            <?php } ?>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                </div>   
                                                                            </div>
                                                                        </div>
                                                                        <?PHP
                                                                    }
                                                                }
                                                                ?>
                                                                <?php
                                                                if ($AIcount > 0) {
                                                                    ?>
                                                                    <div class="listing-address-container">
                                                                        <div class="listing-detail-add-icon">
                                                                            <img src="<?php echo 'http://' . DOMAIN; ?>/images/Listing-Ammenity.png" alt="Ammenity">
                                                                        </div>
                                                                        <div class="listing-detail-add-detail">
                                                                            <div class="address-heading listing-leftnav-margin accordion view-acc">                               
                                                                                <h4>View Amenities</h4>
                                                                                <div class="businessListing-timing">
                                                                                    <?php
                                                                                    while ($rowAI = mysql_fetch_assoc($resultAI)) {
                                                                                        ?>
                                                                                        <div class="acc-body">
                                                                                            <?php echo $rowAI['AI_Name']; ?>
                                                                                        </div>    
                                                                                    <?PHP } ?>
                                                                                </div> 
                                                                            </div>   
                                                                        </div>                      
                                                                    </div>
                                                                    <?php
                                                                }
                                                                if ($pdf_num > 0) {
                                                                    ?>
                                                                    <div class="listing-address-container">
                                                                        <div class="listing-detail-add-icon">
                                                                            <img src="<?php echo 'http://' . DOMAIN; ?>/images/Listing-Download.png" alt="Download">
                                                                        </div>
                                                                        <div class="listing-detail-add-detail">
                                                                            <div class="address-heading listing-leftnav-margin accordion view-acc">
                                                                                <h4>View Downloads</h4>
                                                                                <div class="businessListing-timing">
                                                                                    <?php
                                                                                    while ($data_dpdf = mysql_fetch_assoc($result_dpdf)) {
                                                                                        ?>
                                                                                        <div class="acc-body theme-downloads"><a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $data_dpdf['D_PDF'] ?>"><?php echo $data_dpdf['D_PDF_Title']; ?></a></div>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </div>   
                                                                            </div>   
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                }
                                                                if ($recommended_count > 0) {
                                                                    ?>
                                                                    <div class="listing-address-container">
                                                                        <div class="listing-detail-add-icon">
                                                                            <img src="<?php echo 'http://' . DOMAIN; ?>/images/Listing-Recommendations.png" alt="Recommendations">
                                                                        </div>
                                                                        <div class="listing-detail-add-detail">
                                                                            <div class="address-heading listing-leftnav-margin accordion view-acc">
                                                                                <a href="#recommendations" onclick="$('#recommendations').show();">Recommendations</a>
                                                                            </div>   
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                }
                                                                if (mysql_num_rows($resultFGB) > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 12)) {
                                                                    ?>
                                                                    <div class="listing-address-container">
                                                                        <div class="listing-detail-add-icon">
                                                                            <img src="<?php echo 'http://' . DOMAIN; ?>/images/Listing-Guest-Book.png" alt="Guest Book">
                                                                        </div>
                                                                        <div class="listing-detail-add-detail">
                                                                            <div class="address-heading listing-leftnav-margin accordion view-acc">
                                                                                <a href="#guest-book" onclick="$('#guest-book').show();">Guest Book</a>
                                                                            </div>   
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                            <?php
                                                        }
                                                        $social_blld = $activeBiz['BL_ID'];
                                                        $sql_social = "SELECT * FROM  tbl_Business_Social where BS_BL_ID=$social_blld";
                                                        $social_result = mysql_query($sql_social);
                                                        $row_soical = mysql_fetch_assoc($social_result);
                                                        if ($row_soical['BS_FB_Link'] || $row_soical['BS_T_Link'] || $row_soical['BS_Y_Link'] || $row_soical['BS_I_Link']) {
                                                            ?>
                                                            <div class="listings-social-icon">
                                                                <?php
                                                                if ($row_soical['BS_FB_Link'] != '') {
                                                                    $facebook_link = preg_replace('/^www\./', '', $row_soical['BS_FB_Link']);
                                                                    ?>
                                                                    <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $facebook_link) ?>" target="_blank"><img src="<?php echo 'http://' . DOMAIN . '/images/facebook.png' ?>"/></a>
                                                                    <?php
                                                                }
                                                                if ($row_soical['BS_I_Link'] != '') {
                                                                    $rs_i_link = preg_replace('/^www\./', '', $row_soical['BS_I_Link']);
                                                                    ?> 
                                                                    <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $rs_i_link) ?>" target="_blank"><img src="<?php echo 'http://' . DOMAIN . '/images/instagram.png' ?>"/></a>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <?php
                                                                if ($row_soical['BS_T_Link'] != '') {
                                                                    $rs_t_link = preg_replace('/^www\./', '', $row_soical['BS_T_Link']);
                                                                    $twitter = explode("?", $rs_t_link);
                                                                    ?>
                                                                    <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $twitter[0]) ?>" target="_blank"><img src="<?php echo 'http://' . DOMAIN . '/images/twitter.png' ?>"/></a>
                                                                <?php }
                                                                ?>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                    <!--right end -->
                                                </div>
                                            </div>
                                        </section>
                                        <?php if ($THEME['TO_Top_Page_Icon'] != "") { ?>
                                            <a href="#" class="scrollToTop">
                                                <img src="<?php echo 'http://' . DOMAIN . IMG_ICON_REL . $THEME['TO_Top_Page_Icon']; ?>">
                                            </a>
                                        <?php } ?>
                                        <script>
                                            $(function () {
                                                $('a[href*="#"]:not([href="#"])').click(function () {
                                                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                                                        var target = $(this.hash);
                                                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                                                        if (target.length) {
                                                            $('html, body').animate({
                                                                scrollTop: target.offset().top
                                                            }, 2000);
                                                            return false;
                                                        }
                                                    }
                                                });
                                                $(window).scroll(function () {
                                                    if ($(this).scrollTop() > 400) {
                                                        $('.scrollToTop').fadeIn();
                                                    } else {
                                                        $('.scrollToTop').fadeOut();
                                                    }
                                                });
                                                //Click event to scroll to top
                                                $('.scrollToTop').click(function () {
                                                    $('html, body').animate({scrollTop: 0}, 2000);
                                                    return false;
                                                });
                                                var myhash = window.location.hash.substring(1);
                                                if (myhash != '')
                                                {
                                                    $('html, body').animate({
                                                        scrollTop: $('#section_nearby').offset().top
                                                    }, 2000);
                                                    return false;
                                                }
                                            });

                                        </script>
                                        <?PHP
                                        if ($_SESSION['success'] == 1) {
                                            print '<script>swal("Thank you", "for submitting your feedback!", "success");</script>';
                                            unset($_SESSION['success']);
                                        }
                                        if ($_SESSION['error'] == 1) {
                                            print '<script>swal("Error", "Robot verification failed, please try again.", "error");</script>';
                                            unset($_SESSION['error']);
                                        }
                                        ?>
