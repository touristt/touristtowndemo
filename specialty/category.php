<?php
require_once 'include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
require_once 'include/class.Pagination_public.php';
require_once '../include/PHPMailer/class.phpmailer.php';
require_once 'update-impression-advert.php';

$url_parse = parse_url($_SERVER['REQUEST_URI']);
$data = substr($url_parse['path'], 1);
if (substr($data, -1) == '/') {
    $data = substr($data, 0, -1);
}
$url = explode('/', $data);
if ($url[0] <> '') {
    $sql = "SELECT C_ID, C_Is_Blog, C_Parent, C_Name_SEO FROM tbl_Category WHERE C_Name_SEO = '" . encode_strings($url[0], $db) . "' ORDER BY C_Name LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeCat = mysql_fetch_assoc($result);
    if ($activeCat['C_ID'] > 0) {
        $_SESSION['CATEGORY'] = $activeCat['C_ID'];
    } else {
        $sql = "SELECT C_ID, C_SEO_Name, redirect.CR_ID FROM tbl_Contests_Redirect as redirect
                LEFT JOIN tbl_Contests ON C_ID = redirect.CR_C_ID 
                LEFT JOIN tbl_Contests_Regions as region ON region.CR_C_ID = C_ID
                WHERE region.CR_R_ID = '" . $REGION['R_ID'] . "' AND 
                CR_Page LIKE '" . encode_strings($url[0], $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $redirect = mysql_fetch_assoc($result);
        if ($redirect['C_ID'] > 0) {
            $sql = "UPDATE tbl_Contests_Redirect SET CR_Views = CR_Views + 1 WHERE CR_ID = '" . $redirect['CR_ID'] . "'";
            mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            header("Location: /contests/" . $redirect['C_SEO_Name']);
            exit();
        } else {
            header("Location: /404.php");
            exit();
        }
    }
    //check to pass mobile users to mobile version
    if(!isset($url[1]) || $url[1] == '') {
        $useragent = $_SERVER['HTTP_USER_AGENT'];
        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
            $url_redirect = 'http://' . $REGION['R_Domain'] . DOMAIN_MOBILE_REL . $activeCat['C_Name_SEO'] .'/';
            header('Location: ' . $url_redirect);
            exit();
        }
    }
}
$subCat = 0;
if (isset($url[1]) && $url[1] <> '') {
    if ($activeCat['C_Is_Blog'] == 1) {
        $subCat = 1;
    } else {
        $sql = "SELECT C_ID, C_Parent, C_Name_SEO FROM tbl_Category WHERE C_Name_SEO = '" . encode_strings($url[1], $db) . "' AND 
                C_Parent = '" . encode_strings($activeCat['C_ID'], $db) . "' 
                ORDER BY C_Name LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $activeCat = mysql_fetch_assoc($result);
        if ($activeCat['C_ID'] > 0) {
            $_SESSION['CATEGORY'] = $activeCat['C_Parent'];
            $subCat = $activeCat['C_ID'];
        }
    }
    //check to pass mobile users to mobile version
    $useragent = $_SERVER['HTTP_USER_AGENT'];
    if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
        // If story page
      if(isset($url['2']) && $url['2'] > 0) {
          $getStory = " SELECT S_ID, S_Title FROM tbl_Story WHERE S_ID = '" . encode_strings($url[2], $db) . "' AND S_Active = 1";
            $resStory = mysql_query($getStory, $db) or die("Invalid query: $getStory -- " . mysql_error());
            $activeStory = mysql_fetch_assoc($resStory);
            $url_redirect = 'http://' . $REGION['R_Domain'] . DOMAIN_MOBILE_REL . 'story/'. clean($activeStory['S_Title']) .'/'. $activeStory['S_ID'] .'/';
            header('Location: '. $url_redirect);
            exit();
        }
        // If subcategory page
        else {
            //get parent category
            $sql_parent = "SELECT C_ID, C_Name_SEO FROM tbl_Category WHERE C_ID = ". $_SESSION['CATEGORY'];
            $res_parent = mysql_query($sql_parent);
            $p = mysql_fetch_assoc($res_parent); 
            $url_redirect = 'http://' . $REGION['R_Domain'] . DOMAIN_MOBILE_REL . $p['C_Name_SEO'] . '/' . $activeCat['C_Name_SEO'] .'/';
            header('Location: '. $url_redirect);
            exit();
        }
    }
}

$sql_category_slider = "SELECT RCP_Image, RCP_Video, RCP_Slider_Title, RCP_Slider_Description FROM tbl_Region_Category_Photos WHERE
                        RCP_C_ID = '" . encode_strings($activeCat['C_ID'], $db) . "' AND RCP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                        ORDER BY RCP_Order";
$result_category_slider = mysql_query($sql_category_slider, $db) or die("Invalid query: $sql_category_slider -- " . mysql_error());
$sqlCatMainSlider = "SELECT RC_Image, RC_Video, RC_Slider_Des, RC_Slider_Title FROM tbl_Region_Category WHERE
                     RC_C_ID = '" . encode_strings($activeCat['C_ID'], $db) . "' AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
$resMainSlider = mysql_query($sqlCatMainSlider, $db) or die("Invalid query: $sqlCatMainSlider -- " . mysql_error());
$Firstslider = mysql_fetch_assoc($resMainSlider);

if ($subCat == 0) {
    $sql = "SELECT C_ID, RC_SEO_Title, RC_SEO_Description, RC_SEO_Keywords FROM tbl_Category 
            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            WHERE C_ID = '" . encode_strings($_SESSION['CATEGORY'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $tmpCat = mysql_fetch_assoc($result);
    $SEOtitle = $tmpCat['RC_SEO_Title'];
    $SEOdescription = $tmpCat['RC_SEO_Description'];
    $SEOkeywords = $tmpCat['RC_SEO_Keywords'];
    require_once 'include/public/header.php';
    require_once 'maincategory.php';
    //show from maincategory
} else if ($subCat == 1) {
    $getStory = "SELECT S_ID, S_Title, S_Author, S_Category, S_SEO_Title, S_SEO_Description, S_SEO_Keywords, S_Feature_Image, C_Parent, C_Name, RC_Name FROM tbl_Story 
                LEFT JOIN tbl_Category ON C_ID = S_Category   
                LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                WHERE S_ID = '" . encode_strings($url[2], $db) . "' AND S_Active = 1";
    $resStory = mysql_query($getStory, $db) or die("Invalid query: $getStory -- " . mysql_error());
    $activeStory = mysql_fetch_assoc($resStory);
    //SEO Tags
    $SEOtitle = $activeStory['S_SEO_Title'];
    $SEOdescription = $activeStory['S_SEO_Description'];
    $SEOkeywords = $activeStory['S_SEO_Keywords'];
    //Get first content piece image
    $sqlCP = "SELECT CP_Photo FROM tbl_Content_Piece WHERE CP_S_ID = '" . encode_strings($activeStory['S_ID'], $db) . "' ORDER BY CP_Order ASC LIMIT 1";
    $resCP = mysql_query($sqlCP);
    $CP = mysql_fetch_assoc($resCP);
    //OG Tags for FB Share
    $OG_URL = curPageURL();
    $OG_Type = 'article';
    $OG_Title = $activeStory['S_SEO_Title'] != '' ? $activeStory['S_SEO_Title'] : $activeStory['S_Title'];
    $OG_Description = $activeStory['S_SEO_Description'];
    $OG_Image = 'http://'. DOMAIN . IMG_LOC_REL . $CP['CP_Photo'];
    $OG_Image_Width = 580;
    $OG_Image_Height = 350;
    
    require_once 'include/public/header.php';
    require_once 'stories.php';
} else {
    $sql = "SELECT C_ID, RC_Video, RC_SEO_Title, RC_SEO_Description, RC_SEO_Keywords FROM tbl_Category 
            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            WHERE C_ID = '" . encode_strings($subCat, $db) . "'LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $tmpCat = mysql_fetch_assoc($result);
    $SEOtitle = $tmpCat['RC_SEO_Title'];
    $SEOdescription = $tmpCat['RC_SEO_Description'];
    $SEOkeywords = $tmpCat['RC_SEO_Keywords'];
    require_once 'include/public/header.php';
    require_once 'subcategory.php';
}
?>
<?php require_once 'include/public/footer.php'; ?>
