<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1356712584397235";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<section class="event-detail-page">
    <div class="event-filter event-filter-align-width">
        <div class="event-filter-align">
            <div class="filter-inner event-filter-align-inner">
                <form method="GET" action="/event-search.php">
                    <div class="first-filter">
                        <p class="event-heading">Select Dates</p>
                        <input class="datepicker event-search-textbox" name="txtStartDay" id="txtStartDay" value="<?php echo ($_GET['txtStartDay']) ? $_GET['txtStartDay'] : date('Y-m-d') ?>" size="15" type="text" placeholder="By Start Date">
                        <input class="datepicker event-search-textbox" name="txtEndDay" id="txtEndDay" value="<?php echo ($_GET['txtEndDay']) ? $_GET['txtEndDay'] : date('Y-m-d', strtotime("+1 month")) ?>" size="15" type="text" placeholder="By End Date">
                    </div>
                    <span class="event-heading">or</span>
                    <div class="second-filter">
                        <input class="event-search-textbox" name="etxtSearch" id="etxtSearch" onfocus="clearEventSearch();" size="15" type="text" placeholder="By Name">
                        <input class="event-button" type="submit" value="go">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="event-wrapper-page"> 
        <div class="event-wrapper-inner">
            <div class="details">
                <div class="section-header">
                    <h2 class="main-content-heading"><?php echo $activeEvent['Title'] ?></h2>
                    <p class="eventDate">
                        <?php
                        echo date('M j, Y', strtotime($activeEvent['EventDateStart']));
                        if ($activeEvent['EventDateStart'] != $activeEvent['EventDateEnd'] && $activeEvent['EventDateEnd'] != '0000-00-00') {
                            echo ' - ' . date('M j, Y', strtotime($activeEvent['EventDateEnd']));
                        }
                        ?>
                    </p>
                </div>
                <?php if ($activeEvent['Event_Image']) { ?>
                    <div class="section-header">
                        <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $activeEvent['Event_Image']; ?>" >
                    </div>
                    <?php
                }
                ?>
                <div class="desc theme-paragraph"><?php echo ($activeEvent['Content']); ?></div>
                <div class="businessListing">
                    <?PHP if ($activeEvent['EventStartTime'] <> '00:00:00' || $activeEvent['EventEndTime'] <> '00:00:00') { ?>
                        <div class="event_container_outside">
                            <div class="event_title_dt">Time</div><div class="event_detail_dd">
                                <?php
                                if ($activeEvent['EventTimeAllDay'] == '1') {
                                    echo "All day";
                                } else {
                                    echo $activeEvent['EventStartTime'] == '00:00:01' ? 'Dusk' : ($activeEvent['EventStartTime'] == '00:00:00' ? '' : date('g:i a', strtotime($activeEvent['EventStartTime'])))
                                    ?><?php
                                    echo
                                    $activeEvent['EventEndTime'] <> '00:00:00' ? (' to ' . ($activeEvent['EventEndTime'] == '00:00:01' ? 'Dusk' : date('g:i a', strtotime($activeEvent['EventEndTime'])))) : '';
                                }
                                ?>
                            </div>
                        </div>
                    <?PHP } ?> 
                    <div class="event_container_outside">
                        <div class="event_title_dt">Community</div><div class="event_detail_dd">
                            <?PHP
                            if ($activeEvent['E_Region_ID']) {
                                $getRegionEvent = "SELECT GROUP_CONCAT(R_Name) AS R_Names FROM tbl_Region WHERE R_ID IN (" . $activeEvent['E_Region_ID'] . ")";
                                $rowRegionEvent = mysql_fetch_assoc(mysql_query($getRegionEvent));
                                if ($rowRegionEvent['R_Names'] != "") {
                                    echo $rowRegionEvent['R_Names'];
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <?PHP
                    $recuring = explode(",", $activeEvent['Recuring']);
                    if (in_array(1, $recuring)) {
                        ?>
                        <div class="event_container_outside">
                            <div class="event_title_dt">Recurring</div>
                            <?php
                            $weekdays = array();
                            if ($recuring[0] == 1) {
                                $weekdays[] = "Mon";
                            }
                            if ($recuring[1] == 1) {
                                $weekdays[] = "Tue";
                            }
                            if ($recuring[2] == 1) {
                                $weekdays[] = "Wed";
                            }
                            if ($recuring[3] == 1) {
                                $weekdays[] = "Thu";
                            }
                            if ($recuring[4] == 1) {
                                $weekdays[] = "Fri";
                            }
                            if ($recuring[5] == 1) {
                                $weekdays[] = "Sat";
                            }
                            if ($recuring[6] == 1) {
                                $weekdays[] = "Sun";
                            }
                            echo '<div class="event_detail_dd">';
                            $i = 0;
                            $count = count($weekdays);
                            foreach($weekdays as $day){
                                if($i > 0 && $i != $count){
                                    echo ', ';
                                }
                                echo $day;
                                $i++;
                            }
                            echo '</div>';
                            ?>
                        </div>
                    <?php } ?>
                    <?php
                    if ($activeEvent['Organization'] || $activeEvent['OrganizationID']) {
                        ?>
                        <div class="event_container_outside">
                            <div class="event_title_dt">Organization</div><div class="event_detail_dd">
                                <?php echo $activeEvent['OrganizationID'] ? $activeEvent['EO_Name'] : $activeEvent['Organization']; ?>
                            </div>
                        </div>
                        <?PHP
                    }
                    if ($activeEvent['LocationID']) {
                        ?>
                        <div class="event_container_outside">
                            <div class="event_title_dt">Location</div><div class="event_detail_dd">
                                <?php echo $activeEvent['EL_Name']; ?>
                            </div>
                        </div>
                        <?PHP if ($activeEvent['EL_Street']) { ?>
                            <div class="event_container_outside">
                                <div class="event_title_dt">Address</div><div class="event_detail_dd">
                                    <?php echo $activeEvent['EL_Street']; ?>, <?php echo $activeEvent['EL_Town']; ?>
                                </div>
                            </div>
                            <?PHP
                        }
                    } elseif ($activeEvent['LocationList'] && $activeEvent['LocationList'] <> 'Alternate Location') {
                        if ($activeEvent['LocationList']) {
                            ?>
                            <div class="event_container_outside">
                                <div class="event_title_dt">Location</div><div class="event_detail_dd">
                                    <?php echo $activeEvent['LocationList']; ?>
                                </div>
                            </div>
                        <?PHP } if ($activeEvent['StreetAddress']) { ?>
                            <div class="event_container_outside">
                                <div class="event_title_dt">Address</div><div class="event_detail_dd">
                                    <?php echo $activeEvent['StreetAddress']; ?>
                                </div>
                            </div>
                            <?PHP
                        }
                    }
                    ?>
                    <?PHP if ($activeEvent['ContactName']) { ?>
                        <div class="event_container_outside">
                            <div class="event_title_dt">Contact Person</div><div class="event_detail_dd">
                                <?php echo $activeEvent['ContactName']; ?>
                            </div>
                        </div>
                    <?PHP } if ($activeEvent['ContactPhone']) { ?>
                        <div class="event_container_outside">
                            <div class="event_title_dt">Phone</div><div class="event_detail_dd">
                                <?php echo $activeEvent['ContactPhone']; ?>
                            </div>
                        </div>
                    <?PHP } if ($activeEvent['ContactPhoneTollFree']) { ?>
                        <div class="event_container_outside">
                            <div class="event_title_dt">Additional Phone</div><div class="event_detail_dd">
                                <?php echo $activeEvent['ContactPhoneTollFree']; ?>
                            </div>
                        </div>
                    <?PHP } if ($activeEvent['Email']) { ?>
                        <div class="event_container_outside">
                            <div class="event_title_dt">Email</div><div class="event_detail_dd">
                                <a class="email-to" href="mailto:<?php echo $activeEvent['Email']; ?>"><?php echo $activeEvent['Email']; ?></a>
                            </div>
                        </div>
                    <?PHP } if ($activeEvent['WebSiteLink']) { ?>
                        <div class="event_container_outside">
                            <div class="event_title_dt">Website</div><div class="event_detail_dd">
                                <a href="http://<?php echo str_replace("http://", "", $activeEvent['WebSiteLink']); ?>" target="_blank"><?php echo $activeEvent['WebSiteLink']; ?></a>
                            </div>
                        </div>
                    <?PHP } ?>
                    <?PHP if ($activeEvent['EventPdf']) { ?>
                        <div class="event_container_outside">
                            <div class="event_title_dt">PDF</div><div class="event_detail_dd">
                                <a href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $activeEvent['EventPdf'] ?>" target="_blank">View</a>
                            </div>
                        </div>
                    <?PHP } ?>
                </div>
                <?php if ($activeEvent['LocationID']) { ?>
                    <?PHP if ($activeEvent['EL_Use_GPS']) { ?>
                        <iframe width="615" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.ca/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=(<?php echo $activeEvent['EL_Lat'] ?>,<?php echo $activeEvent['EL_Long'] ?>)(<?php echo $activeEvent['EL_Name'] ?>)&amp;aq=&amp;sll=<?php echo $activeEvent['EL_Lat'] ?>,<?php echo $activeEvent['EL_Long'] ?>&amp;sspn=0.027224,0.077162&amp;ie=UTF8&amp;t=m&amp;spn=0.022178,0.025663&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
                    <?PHP } elseif ($activeEvent['EL_Street']) { ?>
                        <iframe width="615" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.ca/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<?php echo $activeEvent['EL_Street'] ?>,<?php echo $activeEvent['EL_Town'] ?>+<?php echo $activeEvent['EL_Province'] ?> <?php echo $activeEvent['EL_PostalCode'] ?>&amp;aq=&amp;sspn=0.027224,0.077162&amp;ie=UTF8&amp;t=m&amp;spn=0.022178,0.025663&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe>
                    <?PHP } ?>

                    <?PHP if ($activeEvent['EL_Use_GPS']) { ?>
                        <br />
                        <small>
                            <a class="blue-color" target="_blank" href="http://maps.google.ca/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=(<?php echo $activeEvent['EL_Lat'] ?>,<?php echo $activeEvent['EL_Long'] ?>)(<?php echo $activeEvent['EL_Name'] ?>)&amp;aq=&amp;sll=<?php echo $activeEvent['EL_Lat'] ?>,<?php echo $activeEvent['EL_Long'] ?>&amp;sspn=0.027224,0.077162&amp;ie=UTF8&amp;t=m&amp;spn=0.022178,0.025663&amp;z=14&amp;iwloc=A">View Larger Map</a>
                        </small>
                    <?PHP } elseif ($activeEvent['EL_Street']) { ?>
                        <br />
                        <small>
                            <a class="view-larger-map" target="_blank" href="http://maps.google.ca/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<?php echo $activeEvent['EL_Street'] ?>,<?php echo $activeEvent['EL_Town'] ?>+<?php echo $activeEvent['EL_Province'] ?> <?php echo $activeEvent['EL_PostalCode'] ?>&amp;aq=&amp;sspn=0.027224,0.077162&amp;ie=UTF8&amp;t=m&amp;spn=0.022178,0.025663&amp;z=14&amp;iwloc=A">View Larger Map</a>
                        </small>
                    <?PHP } ?>

                <?php }
                ?>
            </div>
            <div class="add-new-event">
                <div class="fb-share-button" data-href="<?php print curPageURL(); ?>" data-layout="button_count"  data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php print curPageURL(); ?>%2F&amp;src=sdkpreparse">Share</a></div>
                <iframe class="twitter-class" data-size="large"
                        src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=http%3A%2F%2F<?php print curPageURL(); ?>&related=twitterapi%2Ctwitter&text=<?php echo $activeEvent['Title'] ?> in <?php echo $REGION['R_Name'] ?>"
                        width="70"
                        height="35"
                        style="border: 0; overflow: hidden; float: left;">
                </iframe>
            </div>
            <div class="advertisement">
                <?php include '../../advertise.php'; ?>
            </div>
        </div>
    </div>
</section>
<!--Event detail end-->
<?php

function ctime($myTime, $end = false) {
    if ($myTime == '00:00:00' && !$end) {
        return 'Closed';
    } elseif ($myTime == '00:00:01') {
        return 'By Appointment';
    } elseif ($myTime == '00:00:02') {
        return 'Open 24 Hours';
    } else {
        $mySplit = explode(':', $myTime);
        return date('g:ia', mktime($mySplit[0], $mySplit[1], 1, 1, 1));
    }
}
?>
