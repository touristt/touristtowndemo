<?php
require_once 'include/config.inc.php';
require_once 'include/public-site-functions.inc.php';

$story_parent_cat = "SELECT C_ID, C_Name_SEO FROM tbl_Category WHERE C_Is_Blog = 1";
$story_parent_catRes = mysql_query($story_parent_cat);
$story_parent_catRow = mysql_fetch_array($story_parent_catRes);
$stories = "SELECT S_ID, S_Category, S_Active, S_Title, S_Thumbnail, C_Name, C_Parent, RC_Name FROM tbl_Story 
            INNER JOIN tbl_Story_Region ON S_ID = SR_S_ID
            LEFT JOIN tbl_Category ON S_Category = C_ID
            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID
            WHERE S_Category = '" . $_POST['StoryCategory'] . "'
            AND SR_R_ID = '" . $_POST['currentRegion'] . "' 
            AND S_ID != '" . $_POST["currentStory"] . "' AND S_Active = 1
            GROUP BY S_ID ORDER BY S_Title ";
//See if this region has any limit
if ($_POST['currentLimit'] > 0) {
    $stories .= " LIMIT " . $_POST['currentLimit'];
} else {
    $stories .= " LIMIT 10";
}
$resStoryTopTen = mysql_query($stories, $db) or die("Invalid query: $stories -- " . mysql_error());
?>
<?php
while ($rowStoryTopTen = mysql_fetch_assoc($resStoryTopTen)) {
    if ($rowStoryTopTen['S_Thumbnail'] != '') {
        $story_image = $rowStoryTopTen['S_Thumbnail'];
    } else {
        $story_image = $default_thumbnail_image;
    }
    ?>
    <div class='thumbnails static-thumbs'>
        <div class="thumb-item">
            <a href="/<?php echo $story_parent_catRow['C_Name_SEO']; ?>/<?php echo clean($rowStoryTopTen['S_Title']) ?>/<?php echo clean($rowStoryTopTen['S_ID']) ?>">
                <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $story_image; ?>" alt="" longdesc="<?php echo $rowStoryTopTen['S_Title'] ?>">
                <h3 class="thumbnail-heading"><?php echo (($rowStoryTopTen['RC_Name'] == "") ? $rowStoryTopTen['C_Name'] : $rowStoryTopTen['RC_Name']) ?></h3>
                <h3 class="thumbnail-desc"><?php echo $rowStoryTopTen['S_Title'] ?></h3>
            </a>
        </div>
    </div>
<?php } ?>