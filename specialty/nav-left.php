<form method="GET" action="/event-search.php">
    <div class="first-filter">
        <p class="event-heading">Select Dates</p>
        <input class="datepicker event-search-textbox" name="txtStartDay" id="txtStartDay"  size="15" type="text" placeholder="By Start Date">
        <input class="datepicker event-search-textbox" name="txtEndDay" id="txtEndDay"  size="15" type="text" placeholder="By End Date">
    </div>
    <span class="event-heading">or</span>
    <div class="second-filter">
        <input class="event-search-textbox" name="etxtSearch" id="etxtSearch" onfocus="clearEventSearch();" size="15" type="text" value="<?php echo ($_GET['etxtSearch']) ? $_GET['etxtSearch'] : '' ?>" placeholder="By Name">
        <input class="event-button" type="submit" value="go">
    </div>
</form>
