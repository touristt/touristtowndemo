<?php
require_once 'include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
require_once 'include/class.Pagination_public.php';
require_once 'include/public/header.php';

$EP = "SELECT RM_Child FROM `tbl_Region_Multiple` LEFT JOIN tbl_Region ON R_ID = RM_Parent WHERE R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
$resultEP = mysql_query($EP, $db) or die("Invalid query: $EP -- " . mysql_error());

$srchRegion = "";
if ($REGION['R_Parent'] == 0 && $REGION['R_Type'] != 4) {
    $event_counter = 0;
    while ($rowEP = mysql_fetch_assoc($resultEP)) {
        if ($event_counter == 0) {
            $operator = " AND (";
        } else {
            $operator = " OR";
        }
        $srchRegion .= " $operator FIND_IN_SET (" . $rowEP['RM_Child'] . ", E_Region_ID)";
        $event_counter++;
    }
    $srchRegion .= " $operator FIND_IN_SET (" . $REGION['R_ID'] . ", E_Region_ID) )";
} else {
    $srchRegion = " AND FIND_IN_SET (" . $REGION['R_ID'] . ", E_Region_ID)";
}
?>

<!--Whats on around town.-->
<section class="description whats-on">
    <div class="description-wrapper">
        <div class="event-filter">
            <div class="filter-inner event-search-padding border-none">
                <?php require_once 'nav-left.php'; ?>
            </div>
        </div>
        <div class="events-wrapper">
            <div class="events">
                <?php
                if ($_GET['pastEvents']) {
                    $sqlEventSearch = "SELECT Title, E_Name_SEO, ShortDesc, LocationID, content, ContactName, Pending, EventDateStart, EventDateEnd, EventID FROM Events_master LEFT JOIN Events_Location ON EL_ID = LocationID
                                        WHERE (Title LIKE '%" . encode_strings($_GET['etxtSearch'], $db) . "%' OR 
                                        Content LIKE '%" . encode_strings($_GET['etxtSearch'], $db) . "%' OR 
                                        ShortDesc LIKE '%" . encode_strings($_GET['etxtSearch'], $db) . "%' OR 
                                        ContactName LIKE '%" . encode_strings($_GET['etxtSearch'], $db) . "%') ";
                    $sqlEventSearch .= " AND Pending = 0 $srchRegion Order BY EventDateStart";
                } else if ($_GET['txtStartDay'] != '' || $_GET['txtEndDay'] != '') {
                    $sqlEventSearch = "SELECT Title, E_Name_SEO, ShortDesc, LocationID, content, ContactName, Pending, EventDateStart, EventDateEnd, EventID FROM Events_master LEFT JOIN Events_Location ON EL_ID = LocationID
                                        WHERE `EventDateStart` BETWEEN '" . encode_strings($_REQUEST['txtStartDay'], $db) . "' AND   '" . encode_strings($_REQUEST['txtEndDay'], $db) . "' 
                                        AND `EventDateEnd`  BETWEEN '" . encode_strings($_REQUEST['txtStartDay'], $db) . "' AND  '" . encode_strings($_REQUEST['txtEndDay'], $db) . "' 
                                        AND (Title LIKE '%" . encode_strings($_GET['etxtSearch'], $db) . "%' OR 
                                        Content LIKE '%" . encode_strings($_GET['etxtSearch'], $db) . "%' OR 
                                        ShortDesc LIKE '%" . encode_strings($_GET['etxtSearch'], $db) . "%' OR 
                                        ContactName LIKE '%" . encode_strings($_GET['etxtSearch'], $db) . "%') ";
                    $sqlEventSearch .= " AND Pending = 0 $srchRegion Order BY EventDateStart";
                } else {
                    $sqlEventSearch = "SELECT Title, E_Name_SEO, ShortDesc, LocationID, content, ContactName, Pending, EventDateStart, EventDateEnd, EventID FROM Events_master LEFT JOIN Events_Location ON EL_ID = LocationID
                                        WHERE (`EventDateStart` >= CURDATE()  OR ( `EventDateEnd` >= CURDATE( ) AND `EventDateStart` <= CURDATE( )))
                                        AND (Title LIKE '%" . encode_strings($_GET['etxtSearch'], $db) . "%' OR 
                                        Content LIKE '%" . encode_strings($_GET['etxtSearch'], $db) . "%' OR 
                                        ShortDesc LIKE '%" . encode_strings($_GET['etxtSearch'], $db) . "%' OR 
                                        ContactName LIKE '%" . encode_strings($_GET['etxtSearch'], $db) . "%') ";
                    $sqlEventSearch .= " AND Pending = 0 $srchRegion Order BY EventDateStart";
                }
                $resultEventSearch = mysql_query($sqlEventSearch, $db) or die("Invalid query: $sqlEventSearch -- " . mysql_error());
                $pages = new Paginate(mysql_num_rows($resultEventSearch), 20);
                $resultEventSearch = mysql_query($sqlEventSearch . $pages->generateSql(), $db) or die("Invalid query: $sqlEventSearch -- " . mysql_error());
                $totalEventsSearch = mysql_num_rows($resultEventSearch);
                if ($totalEventsSearch > 0) {
                    $ii = 1;
                    $jj = 1;
                    while ($rowEventSearch = mysql_fetch_assoc($resultEventSearch)) {
                        if ($ii == 1) {
                            if ($totalEventsSearch == $jj) {
                                ?>
                                <div class="event-column-outer margin-bottom-event">
                                <?php } else { ?>
                                    <div class="event-column-outer">
                                        <?php
                                    }
                                }
                                ?>
                                <div class="event-column">
                                    <div class="event-date">
                                      <?php
                                      echo date('M j, Y', strtotime($rowEventSearch['EventDateStart']));
                                      if ($rowEventSearch['EventDateStart'] != $rowEventSearch['EventDateEnd'] && $rowEventSearch['EventDateEnd'] != '0000-00-00') {
                                        echo ' - '. date('M j, Y', strtotime($rowEventSearch['EventDateEnd']));
                                      }
                                      ?>
                                    </div>
                                    <div class="event-title"><a href="/events/<?php echo $rowEventSearch['E_Name_SEO'] ?>/<?php echo $rowEventSearch['EventID'] ?>"><?php echo $rowEventSearch['Title'] ?></a></div>
                                    <div class="event-desc"><?php echo ($rowEventSearch['ShortDesc'] != '') ? strip_tags($rowEventSearch['ShortDesc']) : ""; ?></div>
                                    <div class="event-more"><a href="/events/<?php echo $rowEventSearch['E_Name_SEO'] ?>/<?php echo $rowEventSearch['EventID'] ?>">More...</a></div>
                                </div>
                                <?PHP
                                if ($ii == 2) {
                                    $ii = 0;
                                    ?>
                                </div>
                                <?
                            }
                            if ($totalEventsSearch == $jj && $ii == 1) {
                                ?>
                            </div>
                            <?
                        }
                        $ii++;
                        $jj++;
                    }
                }
                // display our pagination footer if set.
                if (isset($pages)) {
                    ?>
                    <div class="event-pager">
                        <?php echo $pages->paginate(); ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<!--Whats on around town. end-->
<?php require_once 'include/public/footer.php'; ?>
