<?php

require_once 'include/config.inc.php';
require_once 'include/public-site-functions.inc.php';

//whether coming from map page or not
$is_map = $_REQUEST['is_map'];

$TOWN_JOIN = "";
$TOWN_WHERE = "";
$distance_select = '';
$distance_having = '';
$disable_distance = 'disabled';
if (isset($_SESSION['TOWN']) && $_SESSION['TOWN'] > 0) {
    $disable_distance = '';
    $TOWN_JOIN = "INNER JOIN tbl_Business_Listing_Category_Region BLCR1 ON BL_ID = BLCR1.BLCR_BL_ID AND BLCR1.BLCR_BLC_R_ID != BLCR.BLCR_BLC_R_ID AND BLCR1.BLCR_BLC_R_ID = '" . encode_strings($_SESSION['TOWN'], $db) . "'";
} else {
    $TOWN_JOIN = "LEFT JOIN tbl_Business_Listing_Category_Region BLCR1 ON BL_ID = BLCR1.BLCR_BL_ID AND BLCR1.BLCR_BLC_R_ID != BLCR.BLCR_BLC_R_ID";
    unset($_SESSION['DISTANCE']);
}
if ($_SESSION['DISTANCE'] && $_SESSION['DISTANCE'] > 0 && $_SESSION['TOWN'] && $_SESSION['TOWN'] > 0) {
    $getReg = " SELECT R_Lat, R_Long FROM tbl_Region WHERE R_ID = '" . encode_strings($_SESSION['TOWN'], $db) . "'";
    $regRes = mysql_query($getReg, $db) or die(mysql_error());
    $regRow = mysql_fetch_array($regRes);
    $latitude = $regRow['R_Lat'];
    $longitude = $regRow['R_Long'];
    $distance_select = ", ( 6371 * acos( cos( radians($latitude) ) * cos( radians( BL_Lat ) ) * cos( radians( BL_Long ) - radians($longitude) ) + sin( radians($latitude) ) * sin( radians( BL_Lat ) ) ) ) AS distance";
    $distance_having = "having distance <= " . $_SESSION['DISTANCE'];
}
if ($REGION['R_Include_Free_Listings_On_Map'] == 1) {
    $include_free_listings_on_map = '';
} else {
    $include_free_listings_on_map = ' AND BL_Listing_Type > 1';
}
if ($REGION['R_Order_Listings_Manually'] == 1) {
    $order_listings = 'ORDER BY BLO_Order ASC, LT_Order DESC, BL_Listing_Title';
} else {
    $order_listings = 'ORDER BY BL_Points DESC, LT_Order DESC, BL_Listing_Title';
}

$where = "";
$markers = array();
$kml_array = array();

//For categories filter
if (isset($_REQUEST['sub_category'])) {
    $i = 0;
    $where .= "AND (";
    foreach ($_REQUEST['sub_category'] as $subcategory) {
        foreach ($subcategory as $subcat) {
            if ($i == 0) {
                $where .= "(BLC_C_ID = $subcat)";
            } else {
                $where .= " OR (BLC_C_ID = $subcat)";
            }
            $i++;
        }
    }
    $where .= ")";
}

//Getting Region Theme
$getTheme = "SELECT TO_Default_Thumbnail_Desktop FROM  `tbl_Theme_Options` WHERE TO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
$themeRegion = mysql_query($getTheme, $db) or die("Invalid query: $getTheme -- " . mysql_error());
$THEME = mysql_fetch_assoc($themeRegion);

//Defualt Thumbnail Images
if ($THEME['TO_Default_Thumbnail_Desktop'] != '') {
    $default_thumbnail_image = $THEME['TO_Default_Thumbnail_Desktop'];
} else {
    $default_thumbnail_image = 'Default-Thumbnail-Desktop.jpg';
}

//if from pages other than map
if ($is_map != 1) {
    $sql = "SELECT DISTINCTROW BL_ID, BL_Listing_Title, BL_Name_SEO, BLP_Photo, BL_Photo_Alt, BL_Lat, BL_Long, BL_Street, BL_Town, 
            R.R_Name as permanent_name, R1.R_Name as secondary_name, RC.RC_Map_Icon as subcat_icon, RC1.RC_Map_Icon as cat_icon $distance_select
            FROM tbl_Business_Listing 
            LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
            INNER JOIN tbl_Region_Category RC ON RC.RC_C_ID = BLC_C_ID AND RC.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            INNER JOIN tbl_Region_Category RC1 ON RC1.RC_C_ID = BLC_M_C_ID AND RC1.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            INNER JOIN tbl_Business_Listing_Category_Region BLCR ON BL_ID = BLCR.BLCR_BL_ID AND BLCR.BLCR_BLC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
            LEFT JOIN tbl_Business_Listing_Order ON BL_ID = BLO_BL_ID AND BLO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
            AND BLO_S_C_ID = BLC_C_ID
            $TOWN_JOIN
            LEFT JOIN tbl_Region R ON BLCR.BLCR_BLC_R_ID = R.R_ID
            LEFT JOIN tbl_Region R1 ON BLCR1.BLCR_BLC_R_ID = R1.R_ID
            WHERE (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = '1' AND RC.RC_Status = 0 AND RC1.RC_Status = 0 $where $include_free_listings_on_map AND BLP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
            GROUP BY BL_ID $distance_having $order_listings";
} else {
    $sql = " SELECT DISTINCTROW BL_ID, BL_Listing_Title, BL_Name_SEO, BLP_Photo, BL_Photo_Alt, BL_Lat, BL_Long, BL_Street, BL_Town, 
            R.R_Name as permanent_name, R1.R_Name as secondary_name, RC.RC_Map_Icon as subcat_icon, RC1.RC_Map_Icon as cat_icon $distance_select
            FROM tbl_Business_Listing 
            LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
            INNER JOIN tbl_Region_Category RC ON RC.RC_C_ID = BLC_C_ID AND RC.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            INNER JOIN tbl_Region_Category RC1 ON RC1.RC_C_ID = BLC_M_C_ID AND RC1.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            INNER JOIN tbl_Business_Listing_Category_Region BLCR ON BL_ID = BLCR.BLCR_BL_ID AND BLCR.BLCR_BLC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
            LEFT JOIN tbl_Business_Listing_Order ON BL_ID = BLO_BL_ID AND BLO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
            AND BLO_S_C_ID = BLC_C_ID
            $TOWN_JOIN
            LEFT JOIN tbl_Region R ON BLCR.BLCR_BLC_R_ID = R.R_ID
            LEFT JOIN tbl_Region R1 ON BLCR1.BLCR_BLC_R_ID = R1.R_ID
            WHERE (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing='1' AND RC.RC_Status = 0 AND RC1.RC_Status = 0 $where $include_free_listings_on_map AND BLP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
            GROUP BY BL_ID $distance_having $order_listings";
}
// generate icons for all listings on map regardless of page
$result_map = mysql_query($sql) or die(mysql_error() . ' - ' . $sql);
$counter = 0;
$sidebar = '';
while ($list = mysql_fetch_array($result_map, MYSQL_ASSOC)) {

    if ($list['subcat_icon'] != '') {
        $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $list['subcat_icon'];
    } elseif ($list['cat_icon'] != '') {
        $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $list['cat_icon'];
    } else {
        $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . 'default.png';
    }
    if ($list['BL_Lat'] && $list['BL_Long']) {
        // Add tooltip content
        if ($list['BLP_Photo'] != '') {
            $image = '<div class="thumbnail"><img width="130" height="90" src="http://' . DOMAIN . ((IMG_LOC_REL . $list['BLP_Photo'])) . '" alt="' . htmlspecialchars(trim($list['BL_Photo_Alt']), ENT_QUOTES) . '" /></div>';
        } else {
            $image = '<div class="thumbnail"><img src="http://' . DOMAIN . ((IMG_LOC_REL . $default_thumbnail_image)) . '" alt="' . htmlspecialchars(trim($list['BL_Listing_Title']), ENT_QUOTES) . '" /></div>';
        }
        $markers[] = array(
            'id' => $list['BL_ID'],
            'lat' => $list['BL_Lat'],
            'lon' => $list['BL_Long'],
            'name' => $list['BL_Listing_Title'],
            'path' => '/profile/' . trim($list['BL_Name_SEO']) . '/' . $list['BL_ID'] . '/',
            'icon' => $icon,
            'main_photo' => $image,
            'address' => htmlspecialchars(trim($list['BL_Street']), ENT_QUOTES),
            'town' => htmlspecialchars(trim($list['BL_Town']), ENT_QUOTES)
        );
    }
    //show only limited records in right panel only on map page 
    if ($is_map == 1) {
        if ($counter < $REGION['R_Map_Listings_Limit']) {
            $photo = ($list['BLP_Photo'] == '') ? (IMG_LOC_REL . $default_thumbnail_image) : (IMG_LOC_REL . $list['BLP_Photo']);
            $sidebar .= '<div class="thumbnails static-thumbs">
                    <div class="thumb-item">
                      <a href="/profile/' . $list['BL_Name_SEO'] . '/' . $list['BL_ID'] . '/"> 
                        <img src="http://' . DOMAIN . $photo . '" alt="' . $list['BL_Photo_Alt'] . '" />
                        <h3 class="thumbnail-heading">' . (($list['secondary_name'] != '') ? $list['secondary_name'] : $list['permanent_name']) . '</h3>
                        <h3 class="thumbnail-desc">' . $list['BL_Listing_Title'] . '</h3>
                      </a> 
                    </div>
                  </div>';
        }
    }
    $counter++;
}
$return['markers'] = $markers;
$return['kml'] = $kml_array;
$return['sidebar'] = $sidebar;
print json_encode($return);
exit;
?>