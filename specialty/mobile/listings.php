<?php if ($REGION['R_Listings_Title'] != '') { ?>
    <h1 class="heading-text" align="center"><?php echo $REGION['R_Listings_Title']; ?></h1>
    <?php
}
$sqlListings = "SELECT DISTINCTROW BL_ID, BL_Name_SEO, LT_ID, BLP_Mobile_Photo, BL_Photo_Alt, BL_Listing_Title FROM tbl_Business_Listing
                LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
                INNER JOIN tbl_Business_Listing_Category_Region ON BL_ID = BLCR_BL_ID AND BLCR_BLC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
                LEFT JOIN tbl_Business_Listing_Order ON BL_ID = BLO_BL_ID AND BLO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
                AND BLO_S_C_ID = BLC_C_ID
                WHERE BLC_C_ID = '" . encode_strings($activeCat['C_ID'], $db) . "' AND BLP_R_ID  = '" . encode_strings($REGION['R_ID'], $db) . "'
                AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = '1'
                $include_free_listings
                $order_listings";
$resultListings = mysql_query($sqlListings, $db);
while ($Listings = mysql_fetch_assoc($resultListings)) {
    if ($Listings['BLP_Mobile_Photo'] != '') {
        $listing_image = $Listings['BLP_Mobile_Photo'];
    } else {
        $listing_image = $default_thumbnail_image;
    }
    ?>
    <div class="thumbnails-inner">
        <div class="thumbnail">
            <a href="<?php echo DOMAIN_MOBILE_REL . 'profile/' . $Listings['BL_Name_SEO'] . '/' . $Listings['BL_ID'] ?>/">
                <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo $Listings['BL_Listing_Title'] ?>" />
                <div class="title specialty"><p><?php echo $Listings['BL_Listing_Title']; ?></p></div>
            </a>
        </div>
    </div>
    <?php
}
?>