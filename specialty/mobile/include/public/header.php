<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <meta charset="utf-8" http-equiv="Content-Type" content="text/html">
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <title><?php echo isset($SEOtitle) ? $SEOtitle : $REGION['R_SEO_Title'] ?></title>
            <meta name="description" content="<?php echo isset($SEOdescription) ? $SEOdescription : $REGION['R_SEO_Description'] ?>" />
            <meta name="keywords" content="<?php echo isset($SEOkeywords) ? $SEOkeywords : $REGION['R_SEO_Keywords'] ?>" />
            <!-- Open Graph Tags fo FB Share and Like-->
            <meta property="fb:app_id" content="1356712584397235"/>
            <meta property="og:url" content="<?php echo isset($OG_URL) ? $OG_URL : ''; ?>" />
            <meta property="og:type" content="<?php echo isset($OG_Type) ? $OG_Type : ''; ?>" />
            <meta property="og:title" content="<?php echo isset($OG_Title) ? $OG_Title : ''; ?>" />
            <meta property="og:description" content="<?php echo isset($OG_Description) ? $OG_Description : ''; ?>" />
            <meta property="og:image" content="<?php echo isset($OG_Image) ? $OG_Image : ''; ?>" />
            <meta property="og:image:width" content="<?php echo isset($OG_Image_Width) ? $OG_Image_Width : ''; ?>" />
            <meta property="og:image:height" content="<?php echo isset($OG_Image_Height) ? $OG_Image_Height : ''; ?>" />

            <!-- CSS FILES -->
            <link rel="stylesheet" type="text/css" href="http://<?php echo $REGION['R_Domain'] . DOMAIN_MOBILE_REL ?>stylesheets/mainscreen.min.css" media="all" />
            <!-- JS FILES -->
            <script type="text/javascript" src="http://<?php echo $REGION['R_Domain'] . DOMAIN_MOBILE_REL ?>include/jquery-1.12.4.js"></script>
            <script type="text/javascript" src="http://<?php echo $REGION['R_Domain'] . DOMAIN_MOBILE_REL ?>include/jquery-ui.js"></script>
            <script type="text/javascript" src="http://<?php echo $REGION['R_Domain'] . DOMAIN_MOBILE_REL ?>include/js/fancybox/jquery.fancybox.min.js"></script>
            <script type="text/javascript" src="http://<?php echo $REGION['R_Domain'] . DOMAIN_MOBILE_REL ?>include/js/script.js"></script>
            <script type="text/javascript" src="http://<?php echo $REGION['R_Domain'] . DOMAIN_MOBILE_REL ?>include/plugins/sweetalert/sweetalert.min.js"></script>
            <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo STATIC_MAP_API_KEY ?>" type="text/javascript"></script>
            <!--<script src="http://<?php echo $REGION['R_Domain'] . DOMAIN_MOBILE_REL ?>stylesheets/docs/assets/css/jquery.blockUI.js" type="text/javascript"></script>-->
            <script type="text/javascript">

                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', '<?php echo $REGION['R_Tracking_ID'] ?>']);
                _gaq.push(['_trackPageview']);

                (function () {
                    var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(ga, s);
                })();

            </script>

            <?PHP
            //Getting Region Theme
            $getTheme = "SELECT * FROM  `tbl_Theme_Options_Mobile` WHERE TO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
            $themeRegion = mysql_query($getTheme, $db) or die("Invalid query: $getTheme -- " . mysql_error());
            $THEME = mysql_fetch_assoc($themeRegion);

            $main_page_title = explode("-", $THEME['TO_Main_Page_Title']);
            $main_navigation = explode("-", $THEME['TO_Main_Navigation']);
            $main_navigation_selected = explode("-", $THEME['TO_Main_Navigation_Selected']);
            $category_header_text = explode("-", $THEME['TO_Category_Header_Text']);
            $slider_title = explode("-", $THEME['TO_Slider_Title']);
            $main_page_body = explode("-", $THEME['TO_Main_Page_Body_Copy']);
            $homepage_description_space = $THEME['TO_Homepage_Description_Space'];
            $index_title = explode("-", $THEME['TO_Thumbnail_Category_Tite']);
            $cat_title = explode("-", $THEME['TO_Thumbnail_Sub_Category_Title']);
            $subcat_title = explode("-", $THEME['TO_Thumbnail_Listing_Title']);
            //Listing page
            $hours_heading = explode("-", $THEME['TO_Listing_Day_Text']);
            $hours_data = explode("-", $THEME['TO_Listing_Hours_Text']);
            $amenities_data = explode("-", $THEME['TO_Amenities_Text']);
            $download_data = explode("-", $THEME['TO_Downloads_Text']);
            $listing_main_title = explode("-", $THEME['TO_Listing_Title_Text']);
            $listing_address = explode("-", $THEME['TO_Listing_Location_Text']);
            $general_paragraph = explode("-", $THEME['TO_General_Body_Copy']);
            $listing_general_body_copy_line_spacing = $THEME['TO_General_Body_Copy_Line_Spacing'];
            $listing_sub_nav_value = explode("-", $THEME['TO_Listing_Sub_Nav_Text']);
            //Listing page
            //Stories page
            $story_main_title_value = explode("-", $THEME['TO_Story_Page_Main_Title']);
            $story_share_text_value = explode("-", $THEME['TO_Story_Share_Text']);
            $story_content_title_value = explode("-", $THEME['TO_Story_Page_Content_Title']);
            //Stories page
            //Route Page
            $route_main_title_value = explode("-", $THEME['TO_Route_Page_Main_Title']);
            $route_data_label_value = explode("-", $THEME['TO_Route_Page_Data_Label']);
            $route_data_content_value = explode("-", $THEME['TO_Route_Page_Data_Content']);
            //Route Page
            //Event Page
            $detail_page_event_main_title_value = explode("-", $THEME['TO_Event_Page_Main_Title']);
            $detail_page_event_date_value = explode("-", $THEME['TO_Event_Page_Date']);
            $detail_page_event_data_label_value = explode("-", $THEME['TO_Event_Page_Data_Label']);
            $detail_page_event_data_content_value = explode("-", $THEME['TO_Event_Page_Data_Content']);
            //Event Page
            //Footer
            $footer_links = explode("-", $THEME['TO_Footer_Links']);
            $footer_disclaimer = explode("-", $THEME['TO_Footer_Disclaimer']);
            $footer_lines_color = $THEME['TO_Footer_Lines_Color'];
            $footer_background_color = $THEME['TO_Footer_Background_Color'];
            //Footer
            //Event related things needs to be done later for other websites
            $event_main_title_value = explode("-", $THEME['TO_Homepage_Event_Main_Title']);
            $event_date_value = explode("-", $THEME['TO_Homepage_Event_Date']);
            //CSS With Image variables
            $category_header_bg = explode(">", $THEME['TO_Category_Header_BG']);
            $home_des_bg_color = explode(">", $THEME['TO_Homepage_Description_Background']);
            $index_texture = explode(">", $THEME['TO_Bar_Texture']);
            $cat_texture = explode(">", $THEME['TO_Subcat_Texture']);
            $subcat_texture = explode(">", $THEME['TO_Listing_Texture']);
            $search_box_button_text_value = explode("-", $THEME['TO_Search_Box_Button_Text']);
            $text_box_text_inside_search_box_value = explode("-", $THEME['TO_Text_Box_Text_Inside_Box']);

            //Getting Region Theme Font, Due to our DB structure we are applying new query rather than JOIN
            $getFont = "SELECT * FROM `tbl_Theme_Options_Fonts`";
            $themeFontRegion = mysql_query($getFont, $db) or die("Invalid query: $getFont -- " . mysql_error());
            $THEME_ARRAY = array();
            while ($THEME_FONT = mysql_fetch_assoc($themeFontRegion)) {
                $THEME_ARRAY[$THEME_FONT['TOF_ID']] = $THEME_FONT['TOF_Name'];
            }
            ?>
            <style>
                .search_header {
                    background-color: <?php echo (isset($THEME['TO_Search_Box_Background_Color'])) ? $THEME['TO_Search_Box_Background_Color'] : ''; ?>;
                }
                input#txtSearch {
                    border-color: <?php echo (isset($THEME['TO_Text_Box_Border_Inside_Search_Box'])) ? $THEME['TO_Text_Box_Border_Inside_Search_Box'] : ''; ?>;
                    font-family: <?php echo (isset($THEME_ARRAY[$text_box_text_inside_search_box_value[0]]) === true && empty($THEME_ARRAY[$text_box_text_inside_search_box_value[0]]) === false) ? $THEME_ARRAY[$text_box_text_inside_search_box_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($text_box_text_inside_search_box_value[1])) ? $text_box_text_inside_search_box_value[1] : '' ?>px;
                    color: <?php echo (isset($text_box_text_inside_search_box_value[2])) ? $text_box_text_inside_search_box_value[2] : '' ?>;
                }
                .search_header .search_button input {
                    background-color: <?php echo (isset($THEME['TO_Search_Box_Background_Button_Color'])) ? $THEME['TO_Search_Box_Background_Button_Color'] : ''; ?>;
                    font-family: <?php echo (isset($THEME_ARRAY[$search_box_button_text_value[0]]) === true && empty($THEME_ARRAY[$search_box_button_text_value[0]]) === false) ? $THEME_ARRAY[$search_box_button_text_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($search_box_button_text_value[1])) ? $search_box_button_text_value[1] : '' ?>px;
                    color: <?php echo (isset($search_box_button_text_value[2])) ? $search_box_button_text_value[2] : '' ?>;
                }
                /*Main Navigation*/
                .navigation ul li a{
                    font-family: <?php echo (isset($THEME_ARRAY[$main_navigation[0]]) === true && empty($THEME_ARRAY[$main_navigation[0]]) === false) ? $THEME_ARRAY[$main_navigation[0]] : ""; ?>;
                    font-size: <?php echo (isset($main_navigation[1])) ? $main_navigation[1] : ''; ?>px;
                    color: <?php echo (isset($main_navigation[2])) ? $main_navigation[2] : ''; ?>;
                }
                .navigation ul li a.active{
                    font-family: <?php echo (isset($THEME_ARRAY[$main_navigation_selected[0]]) === true && empty($THEME_ARRAY[$main_navigation_selected[0]]) === false) ? $THEME_ARRAY[$main_navigation_selected[0]] : ""; ?>;
                    font-size: <?php echo (isset($main_navigation_selected[1])) ? $main_navigation_selected[1] : ''; ?>px;
                    color: <?php echo (isset($main_navigation_selected[2])) ? $main_navigation_selected[2] : ''; ?>;
                }
                /*Page Content*/
                .main_image .title p{
                    font-family: <?php echo (isset($THEME_ARRAY[$slider_title[0]]) === true && empty($THEME_ARRAY[$slider_title[0]]) === false) ? $THEME_ARRAY[$slider_title[0]] : ""; ?>;
                    font-size: <?php echo (isset($slider_title[1])) ? $slider_title[1] : ''; ?>px;
                    color: <?php echo (isset($slider_title[2])) ? $slider_title[2] : ''; ?>;
                }
                .main_title{
                    <?php if (isset($category_header_bg[0]) && $category_header_bg[0] != "") { ?>
                        background-image: url("<?php echo 'http://' . DOMAIN . IMG_ICON_REL . $category_header_bg[0]; ?>");
                    <?php } else { ?>
                        background-color: <?php echo $category_header_bg[1]; ?>
                    <?php } ?>
                }
                .main_title p{
                    font-family: <?php echo (isset($THEME_ARRAY[$category_header_text[0]]) === true && empty($THEME_ARRAY[$category_header_text[0]]) === false) ? $THEME_ARRAY[$category_header_text[0]] : ""; ?>;
                    font-size: <?php echo (isset($category_header_text[1])) ? $category_header_text[1] : ''; ?>px;
                    color: <?php echo (isset($category_header_text[2])) ? $category_header_text[2] : ''; ?>;
                }
                .secondary-items .title, .feature-items .feature .feature-title, .thumbnails .heading-text{
                    font-family: <?php echo (isset($THEME_ARRAY[$main_page_title[0]]) === true && empty($THEME_ARRAY[$main_page_title[0]]) === false) ? $THEME_ARRAY[$main_page_title[0]] : ""; ?>;
                    font-size: <?php echo (isset($main_page_title[1])) ? $main_page_title[1] : ''; ?>px;
                    color: <?php echo (isset($main_page_title[2])) ? $main_page_title[2] : ''; ?>;
                }
                .description {
                    <?php if (isset($home_des_bg_color[0]) && $home_des_bg_color[0] != "") { ?>
                        background-image: url("<?php echo 'http://' . DOMAIN . IMG_ICON_REL . $home_des_bg_color[0]; ?>");
                    <?php } else { ?>
                        background-color: <?php echo $home_des_bg_color[1]; ?>
                    <?php } ?>
                }
                .description p, .description ul li{
                    font-family: <?php echo (isset($THEME_ARRAY[$main_page_body[0]]) === true && empty($THEME_ARRAY[$main_page_body[0]]) === false) ? $THEME_ARRAY[$main_page_body[0]] : ""; ?>;
                    font-size: <?php echo (isset($main_page_body[1])) ? $main_page_body[1] : ''; ?>px;
                    color: <?php echo (isset($main_page_body[2])) ? $main_page_body[2] : ''; ?>;
                    line-height: <?php echo (isset($homepage_description_space)) ? $homepage_description_space : ''; ?>em;
                }
                .thumbnails .thumbnails-inner .thumbnail .title.index {
                    <?php if (isset($index_texture[0]) && $index_texture[0] != "") { ?>
                        background-image: url("<?php echo 'http://' . DOMAIN . IMG_ICON_REL . $index_texture[0]; ?>");
                    <?php } else { ?>
                        background-color: <?php echo $index_texture[1]; ?>
                    <?php } ?>
                }
                .thumbnails .thumbnails-inner .thumbnail .title.index p{
                    font-family: <?php echo (isset($THEME_ARRAY[$index_title[0]]) === true && empty($THEME_ARRAY[$index_title[0]]) === false) ? $THEME_ARRAY[$index_title[0]] : ""; ?>;
                    font-size: <?php echo (isset($index_title[1])) ? $index_title[1] : ''; ?>px;
                    color: <?php echo (isset($index_title[2])) ? $index_title[2] : ''; ?>;
                }
                .thumbnails .thumbnails-inner .thumbnail .title.category {
                    <?php if (isset($cat_texture[0]) && $cat_texture[0] != "") { ?>
                        background-image: url("<?php echo 'http://' . DOMAIN . IMG_ICON_REL . $cat_texture[0]; ?>");
                    <?php } else { ?>
                        background-color: <?php echo $cat_texture[1]; ?>
                    <?php } ?>
                }
                .thumbnails .thumbnails-inner .thumbnail .title.category p{
                    font-family: <?php echo (isset($THEME_ARRAY[$cat_title[0]]) === true && empty($THEME_ARRAY[$cat_title[0]]) === false) ? $THEME_ARRAY[$cat_title[0]] : ""; ?>;
                    font-size: <?php echo (isset($cat_title[1])) ? $cat_title[1] : ''; ?>px;
                    color: <?php echo (isset($cat_title[2])) ? $cat_title[2] : ''; ?>;
                }
                .thumbnails .thumbnails-inner .thumbnail .title.specialty {
                    <?php if (isset($subcat_texture[0]) && $subcat_texture[0] != "") { ?>
                        background-image: url("<?php echo 'http://' . DOMAIN . IMG_ICON_REL . $subcat_texture[0]; ?>");
                    <?php } else { ?>
                        background-color: <?php echo $subcat_texture[1]; ?>
                    <?php } ?>
                }
                .thumbnails .thumbnails-inner .thumbnail .title.specialty p{
                    font-family: <?php echo (isset($THEME_ARRAY[$subcat_title[0]]) === true && empty($THEME_ARRAY[$subcat_title[0]]) === false) ? $THEME_ARRAY[$subcat_title[0]] : ""; ?>;
                    font-size: <?php echo (isset($subcat_title[1])) ? $subcat_title[1] : ''; ?>px;
                    color: <?php echo (isset($subcat_title[2])) ? $subcat_title[2] : ''; ?>;
                }
                /*Listing Page*/
                .detail-page .listing-main .title{
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_main_title[0]]) === true && empty($THEME_ARRAY[$listing_main_title[0]]) === false) ? $THEME_ARRAY[$listing_main_title[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_main_title[1])) ? $listing_main_title[1] : ''; ?>px;
                    color: <?php echo (isset($listing_main_title[2])) ? $listing_main_title[2] : ''; ?>;
                }
                .detail-page .listing-main .other{
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_address[0]]) === true && empty($THEME_ARRAY[$listing_address[0]]) === false) ? $THEME_ARRAY[$listing_address[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_address[1])) ? $listing_address[1] : ''; ?>px;
                    color: <?php echo (isset($listing_address[2])) ? $listing_address[2] : ''; ?>;
                }
                .detail-page .listing-description, .detail-page .listing-description p, .detail-page .listing-description ul li, 
                .feature-items .feature .description p, .detail-page .content-description, .detail-page .content-description p {
                    font-family: <?php echo (isset($THEME_ARRAY[$general_paragraph[0]]) === true && empty($THEME_ARRAY[$general_paragraph[0]]) === false) ? $THEME_ARRAY[$general_paragraph[0]] : ""; ?>;
                    font-size: <?php echo (isset($general_paragraph[1])) ? $general_paragraph[1] : ''; ?>px;
                    color: <?php echo (isset($general_paragraph[2])) ? $general_paragraph[2] : ''; ?>;
                    line-height: <?php echo (isset($listing_general_body_copy_line_spacing)) ? $listing_general_body_copy_line_spacing : ''; ?>em;
                }
                .detail-page .listing-address .listing-address-inner .info .address-heading {
                    font-family: <?php echo (isset($THEME_ARRAY[$listing_sub_nav_value[0]]) === true && empty($THEME_ARRAY[$listing_sub_nav_value[0]]) === false) ? $THEME_ARRAY[$listing_sub_nav_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($listing_sub_nav_value[1])) ? $listing_sub_nav_value[1] : ''; ?>px;
                    color: <?php echo (isset($listing_sub_nav_value[2])) ? $listing_sub_nav_value[2] : ''; ?>;
                }
                .detail-page .listing-address .listing-address-inner .info .address-heading a {
                    color: <?php echo (isset($listing_sub_nav_value[2])) ? $listing_sub_nav_value[2] : ''; ?>;
                }
                .detail-page .listing-address .listing-address-inner .info .address-heading .accordion.hours .accordion-heading{
                    font-family: <?php echo (isset($THEME_ARRAY[$hours_heading[0]]) === true && empty($THEME_ARRAY[$hours_heading[0]]) === false) ? $THEME_ARRAY[$hours_heading[0]] : ""; ?>;
                    font-size: <?php echo (isset($hours_heading[1])) ? $hours_heading[1] : ''; ?>px;
                    color: <?php echo (isset($hours_heading[2])) ? $hours_heading[2] : ''; ?>;
                }
                .detail-page .listing-address .listing-address-inner .info .address-heading .accordion.hours .accordion-data{
                    font-family: <?php echo (isset($THEME_ARRAY[$hours_data[0]]) === true && empty($THEME_ARRAY[$hours_data[0]]) === false) ? $THEME_ARRAY[$hours_data[0]] : ""; ?>;
                    font-size: <?php echo (isset($hours_data[1])) ? $hours_data[1] : ''; ?>px;
                    color: <?php echo (isset($hours_data[2])) ? $hours_data[2] : ''; ?>;
                }
                .detail-page .listing-address .listing-address-inner .info .address-heading .accordion.amenities .accordion-data{
                    font-family: <?php echo (isset($THEME_ARRAY[$amenities_data[0]]) === true && empty($THEME_ARRAY[$amenities_data[0]]) === false) ? $THEME_ARRAY[$amenities_data[0]] : ""; ?>;
                    font-size: <?php echo (isset($amenities_data[1])) ? $amenities_data[1] : ''; ?>px;
                    color: <?php echo (isset($amenities_data[2])) ? $amenities_data[2] : ''; ?>;
                }
                .detail-page .listing-address .listing-address-inner .info .address-heading .accordion.downloads .accordion-data a{
                    font-family: <?php echo (isset($THEME_ARRAY[$download_data[0]]) === true && empty($THEME_ARRAY[$download_data[0]]) === false) ? $THEME_ARRAY[$download_data[0]] : ""; ?>;
                    font-size: <?php echo (isset($download_data[1])) ? $download_data[1] : ''; ?>px;
                    color: <?php echo (isset($download_data[2])) ? $download_data[2] : ''; ?>;
                }
                /*Stories Page*/
                .detail-page .story-title .title{
                    font-family: <?php echo (isset($THEME_ARRAY[$story_main_title_value[0]]) === true && empty($THEME_ARRAY[$story_main_title_value[0]]) === false) ? $THEME_ARRAY[$story_main_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($story_main_title_value[1])) ? $story_main_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($story_main_title_value[2])) ? $story_main_title_value[2] : ''; ?>;
                }
                .detail-page .share-this-story{
                    font-family: <?php echo (isset($THEME_ARRAY[$story_share_text_value[0]]) === true && empty($THEME_ARRAY[$story_share_text_value[0]]) === false) ? $THEME_ARRAY[$story_share_text_value[0]] : ""; ?>;
                    font-size: <?php echo isset($story_share_text_value[1]) ? $story_share_text_value[1] : ''; ?>px;
                    color: <?php echo isset($story_share_text_value[2]) ? $story_share_text_value[2] : ''; ?>;
                }
                .detail-page .content-title p{
                    font-family: <?php echo (isset($THEME_ARRAY[$story_content_title_value[0]]) === true && empty($THEME_ARRAY[$story_content_title_value[0]]) === false) ? $THEME_ARRAY[$story_content_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($story_content_title_value[1])) ? $story_content_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($story_content_title_value[2])) ? $story_content_title_value[2] : ''; ?>;
                }
                /*Routes Page*/
                .detail-page .listing-main .title.route-title{
                    font-family: <?php echo (isset($THEME_ARRAY[$route_main_title_value[0]]) === true && empty($THEME_ARRAY[$route_main_title_value[0]]) === false) ? $THEME_ARRAY[$route_main_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($route_main_title_value[1])) ? $route_main_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($route_main_title_value[2])) ? $route_main_title_value[2] : ''; ?>;
                }
                .listing-detail-routes .address-heading{
                    font-family: <?php echo (isset($THEME_ARRAY[$route_data_label_value[0]]) === true && empty($THEME_ARRAY[$route_data_label_value[0]]) === false) ? $THEME_ARRAY[$route_data_label_value[0]] : ""; ?>;
                    font-size: <?php echo isset($route_data_label_value[1]) ? $route_data_label_value[1] : ''; ?>px;
                    color: <?php echo isset($route_data_label_value[2]) ? $route_data_label_value[2] : ''; ?>;
                }
                .listing-detail-routes .address-data{
                    font-family: <?php echo (isset($THEME_ARRAY[$route_data_content_value[0]]) === true && empty($THEME_ARRAY[$route_data_content_value[0]]) === false) ? $THEME_ARRAY[$route_data_content_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($route_data_content_value[1])) ? $route_data_content_value[1] : ''; ?>px;
                    color: <?php echo (isset($route_data_content_value[2])) ? $route_data_content_value[2] : ''; ?>;
                }
                /*Event Page*/
                .detail-page .listing-main .title.event-title{
                    font-family: <?php echo (isset($THEME_ARRAY[$detail_page_event_main_title_value[0]]) === true && empty($THEME_ARRAY[$detail_page_event_main_title_value[0]]) === false) ? $THEME_ARRAY[$detail_page_event_main_title_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($detail_page_event_main_title_value[1])) ? $detail_page_event_main_title_value[1] : ''; ?>px;
                    color: <?php echo (isset($detail_page_event_main_title_value[2])) ? $detail_page_event_main_title_value[2] : ''; ?>;
                }
                .detail-page .listing-main .other.event-other{
                    font-family: <?php echo (isset($THEME_ARRAY[$detail_page_event_date_value[0]]) === true && empty($THEME_ARRAY[$detail_page_event_date_value[0]]) === false) ? $THEME_ARRAY[$detail_page_event_date_value[0]] : ""; ?>;
                    font-size: <?php echo isset($detail_page_event_date_value[1]) ? $detail_page_event_date_value[1] : ''; ?>px;
                    color: <?php echo isset($detail_page_event_date_value[2]) ? $detail_page_event_date_value[2] : ''; ?>;
                }
                .event-detail-heading{
                    font-family: <?php echo (isset($THEME_ARRAY[$detail_page_event_data_label_value[0]]) === true && empty($THEME_ARRAY[$detail_page_event_data_label_value[0]]) === false) ? $THEME_ARRAY[$detail_page_event_data_label_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($detail_page_event_data_label_value[1])) ? $detail_page_event_data_label_value[1] : ''; ?>px;
                    color: <?php echo (isset($detail_page_event_data_label_value[2])) ? $detail_page_event_data_label_value[2] : ''; ?>;
                }
                .event-detail-content{
                    font-family: <?php echo (isset($THEME_ARRAY[$detail_page_event_data_content_value[0]]) === true && empty($THEME_ARRAY[$detail_page_event_data_content_value[0]]) === false) ? $THEME_ARRAY[$detail_page_event_data_content_value[0]] : ""; ?>;
                    font-size: <?php echo (isset($detail_page_event_data_content_value[1])) ? $detail_page_event_data_content_value[1] : ''; ?>px;
                    color: <?php echo (isset($detail_page_event_data_content_value[2])) ? $detail_page_event_data_content_value[2] : ''; ?>;
                }
                .detail-page .listing-description, .event-items-content-container{
                    border-bottom-color: <?php echo (isset($THEME['TO_Event_Page_Data_Border_Color'])) ? $THEME['TO_Event_Page_Data_Border_Color'] : ''; ?>;
                }
                /*Page Content*/
                /*Footer*/
                .footer{
                    background-color: <?php echo (isset($footer_background_color)) ? $footer_background_color : ''; ?>;
                }
                .footer-urls ul li a{
                    font-family: <?php echo (isset($THEME_ARRAY[$footer_links[0]]) === true && empty($THEME_ARRAY[$footer_links[0]]) === false) ? $THEME_ARRAY[$footer_links[0]] : ""; ?>;
                    font-size: <?php echo (isset($footer_links[1])) ? $footer_links[1] : ''; ?>px;
                    color: <?php echo (isset($footer_links[2])) ? $footer_links[2] : ''; ?>;
                }
                .footer-copyrights p{
                    font-family: <?php echo (isset($THEME_ARRAY[$footer_disclaimer[0]]) === true && empty($THEME_ARRAY[$footer_disclaimer[0]]) === false) ? $THEME_ARRAY[$footer_disclaimer[0]] : ""; ?>;
                    font-size: <?php echo (isset($footer_disclaimer[1])) ? $footer_disclaimer[1] : ''; ?>px;
                    color: <?php echo (isset($footer_disclaimer[2])) ? $footer_disclaimer[2] : ''; ?>;
                }
                .footer-copyrights p a{
                    font-family: <?php echo (isset($THEME_ARRAY[$footer_links[0]]) === true && empty($THEME_ARRAY[$footer_links[0]]) === false) ? $THEME_ARRAY[$footer_links[0]] : ""; ?>;
                    font-size: <?php echo (isset($footer_links[1])) ? $footer_links[1] : ''; ?>px;
                    color: <?php echo (isset($footer_links[2])) ? $footer_links[2] : ''; ?>;
                }
                .footer-images, .footer-urls{
                    border-color:  <?php echo (isset($footer_lines_color)) ? $footer_lines_color : ''; ?>;
                }
                /*Footer*/
            </style>
    </head>

    <body>
        <?php
        $url_parse = parse_url($_SERVER['REQUEST_URI']);
        $data = substr($url_parse['path'], 1);
        if (substr($data, -1) == '/') {
            $data = substr($data, 0, -1);
        }
        $url = explode('/', $data);
        
        //Getting Default Images
        $getDefaultImage = "SELECT * FROM  `tbl_Theme_Options` WHERE TO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
        $DefaultImageRegion = mysql_query($getDefaultImage, $db) or die("Invalid query: $getDefaultImage -- " . mysql_error());
        $DefaultImage = mysql_fetch_assoc($DefaultImageRegion);
        //Defualt Header Images
        if ($DefaultImage['TO_Default_Header_Mobile'] != '') {
            $default_header_image = $DefaultImage['TO_Default_Header_Mobile'];
        } else {
            $default_header_image = 'Default-Main-Mobile.jpg';
        }

        //Defualt Thumbnail Images
        if ($DefaultImage['TO_Default_Thumbnail_Mobile'] != '') {
            $default_thumbnail_image = $DefaultImage['TO_Default_Thumbnail_Mobile'];
        } else {
            $default_thumbnail_image = 'Default-Thumbnail-Mobile.jpg';
        }

        $include_free_listings = '';
        if ($REGION['R_Include_Free_Listings'] != 1) {
            $include_free_listings = ' AND BL_Listing_Type > 1';
        }
        if ($REGION['R_Include_Free_Listings_On_Map'] == 1) {
            $include_free_listings_on_map = '';
        } else {
            $include_free_listings_on_map = ' AND BL_Listing_Type > 1';
        }
        if ($REGION['R_Order_Listings_Manually'] == 1) {
            $order_listings = 'ORDER BY BLO_Order ASC, LT_Order DESC, BL_Listing_Title';
        } else {
            $order_listings = 'ORDER BY BL_Points DESC, LT_Order DESC, BL_Listing_Title';
        }
        ?>
        <!--Start Header-->
        <header class="main-header">
            <div class="search-icon-and-logo">
                <!--Top Navigation Official Site...-->
                <div class="search-icon">
                    <?php
                    $sql_icons = "SELECT RS_Mobile_SE_Icon FROM tbl_Region_Social WHERE RS_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
                    $result_icons = mysql_query($sql_icons, $db) or die(mysql_error());
                    $row_icons = mysql_fetch_assoc($result_icons);
                    ?>
                    <img onClick="displayHeaderForm();" src="<?php echo 'http://' . DOMAIN . (($row_icons['RS_Mobile_SE_Icon'] == '') ? '/images/gb-search.png' : (IMG_LOC_REL . $row_icons['RS_Mobile_SE_Icon'])); ?>" alt="search"/>
                    <form method="GET" class="navbar-search" id="search_form" action="<?php echo DOMAIN_MOBILE_REL ?>search.php">
                        <div class="search_header">
                            <div class="search_field">
                                <label for="txtSearch" class="label-hidden">Search</label>
                                <input type="text" id="txtSearch" name="txtSearch" onfocus="clearSearch();" placeholder="Search"/>
                            </div>
                            <div class="search_button"><input type="submit" name="submit" value="Go"/></div>
                        </div>
                    </form>
                </div>
                <!--Top Navigation Official Site...-->
                <!--Logo Navigation...-->
                <div class="logo">
                    <a href="/mobile">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $THEME['TO_Desktop_Logo']; ?>"  alt="<?php echo isset($THEME['TO_Desktop_Logo_Alt']) ? $THEME['TO_Desktop_Logo_Alt'] : ''; ?>">
                    </a>
                </div>
                <!--Logo Navigation End...-->
            </div>
            <!--Main Navigation End...-->
            <nav class="navigation">
                <ul>
                    <!--Main Category for specialty website-->
                    <?php
                    $nav = array();
                    $nav1 = array();
                    $nav2 = array();
                    $nav3 = array();
                    $nav4 = array();

                    $sql = "SELECT C_ID, RC_C_ID, RC_Link, C_Name_SEO, C_Name, MN_ID, RC_Name, RC_Status, MN_Order,C_Is_Blog FROM tbl_Main_Navigation
                    LEFT JOIN tbl_Region_Category ON RC_C_ID = MN_C_ID AND RC_R_ID = MN_R_ID
                    LEFT JOIN tbl_Category ON C_ID = MN_C_ID AND C_Parent = 0 
                    INNER JOIN tbl_Business_Listing_Category ON BLC_M_C_ID = MN_C_ID    
                    INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID AND BLCR_BLC_R_ID  " . encode_strings((($REGION['R_Parent'] == 0 && $REGION['R_Type'] != 4) ? "IN " . $regionList : "= " . $REGION['R_ID']), $db) . "
                    INNER JOIN tbl_Business_Listing ON BL_ID = BLC_BL_ID
                    WHERE MN_R_ID = '" . $REGION['R_ID'] . "' AND C_ID != 8 AND  MN_Static_Links = 0 $include_free_listings GROUP BY MN_ID ORDER BY MN_Order ASC";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    while ($row = mysql_fetch_assoc($result)) {
                        $nav1[] = $row;
                    }
                    $sql_nav = "SELECT C_ID, RC_C_ID, RC_Link, C_Name_SEO, C_Name, MN_ID, RC_Name, MN_Static_Links, RC_Status, MN_Order,C_Is_Blog FROM tbl_Main_Navigation 
                    LEFT JOIN tbl_Region_Category ON RC_C_ID = MN_C_ID AND RC_R_ID = MN_R_ID
                    LEFT JOIN tbl_Category ON C_ID = MN_C_ID AND C_Parent = 0     
                    WHERE MN_R_ID = '" . $REGION['R_ID'] . "' AND C_Is_Blog = 1 GROUP BY MN_ID ORDER BY MN_Order ASC";
                    $result = mysql_query($sql_nav);
                    while ($row = mysql_fetch_assoc($result)) {
                        $nav2[] = $row;
                    }

                    $sql_nav1 = "SELECT MN_ID, MN_Static_Links, MN_Order FROM tbl_Main_Navigation   
                    WHERE MN_R_ID = '" . $REGION['R_ID'] . "' AND (MN_Static_Links =1 || MN_Static_Links = 2) GROUP BY MN_ID ORDER BY MN_Order ASC";
                    $result1 = mysql_query($sql_nav1);
                    while ($row1 = mysql_fetch_assoc($result1)) {
                        $nav3[] = $row1;
                    }
                    if ($REGION['R_Show_Hide_Event'] == 1) {
                        $sql_nav_event = "SELECT C_ID, RC_C_ID, RC_Link, C_Name_SEO, C_Name, MN_ID, RC_Name, MN_Static_Links, RC_Status, MN_Order,C_Is_Blog FROM tbl_Main_Navigation 
                    LEFT JOIN tbl_Region_Category ON RC_C_ID = MN_C_ID AND RC_R_ID = MN_R_ID
                    LEFT JOIN tbl_Category ON C_ID = MN_C_ID AND C_Parent = 0     
                    WHERE MN_R_ID = '" . $REGION['R_ID'] . "' AND C_ID = 8 AND C_Is_Blog = 0 GROUP BY MN_ID ORDER BY MN_Order ASC";
                        $result_event = mysql_query($sql_nav_event);
                        while ($row = mysql_fetch_assoc($result_event)) {
                            $nav4[] = $row;
                        }
                    }
                    $nav = array_merge($nav1, $nav2, $nav3, $nav4);
                    array_multisort(array_column($nav, "MN_Order"), SORT_ASC, $nav);

                    foreach ($nav as $row) {
                        if ($row['MN_Static_Links'] == 1 || $row['MN_Static_Links'] == 2 || $row['C_ID'] == 121) {
                            if ($row['C_Is_Blog'] == 1 && $row['RC_Status'] == 0 && (isset($REGION['R_Stories']) && $REGION['R_Stories'] == 1)) {
                                if ($row['RC_Link'] != '') {
                                    $blink = preg_replace('/^www\./', '', $row['RC_Link']);
                                    $blink = 'http://' . str_replace(array('http://', 'https://'), '', $blink);
                                } else {
                                    $blink = DOMAIN_MOBILE_REL . $row['C_Name_SEO'] . '/';
                                }
                                ?>
                                <li><a <?php echo (isset($url['2']) && $url['2'] == 'explore') ? 'class="active"' : ''; ?> href="<?php echo $blink; ?>"><?php echo (($REGION['R_Stories'] == "") ? " " : "Explore") ?></a></li>
                                <?php
                            }
                            if ($row['MN_Static_Links'] == 1) {
                                ?>
                                <li><a <?php echo (isset($url['1']) && $url['1'] == 'maps.php') ? 'class="active"' : ''; ?> href="<?php echo DOMAIN_MOBILE_REL ?>maps.php"><?php echo ($REGION['R_Map_Nav_Title'] != "") ? $REGION['R_Map_Nav_Title'] : 'Map' ?></a></li>
                                <?php
                            }
                        } else if ($row['RC_Status'] == 0 && ($row['MN_Static_Links'] != 1 && $row['MN_Static_Links'] != 2 && $row['C_ID'] != 121)) {
                            if ($row['RC_Link'] != '') {
                                $link = preg_replace('/^www\./', '', $row['RC_Link']);
                                $link = 'http://' . str_replace(array('http://', 'https://'), '', $link);
                            } else {
                                $link = DOMAIN_MOBILE_REL . $row['C_Name_SEO'] . '/';
                            }
                            ?>
                            <li><a <?php echo (isset($url['1']) && $url['1'] == $row['C_Name_SEO']) ? 'class="active"' : ''; ?> href="<?php echo $link; ?>"><?php echo ($row['RC_Name'] != '') ? $row['RC_Name'] : $row['C_Name']; ?></a></li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            </nav>
        </header>
        <!--End Header-->