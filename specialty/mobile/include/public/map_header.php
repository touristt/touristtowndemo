<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <meta charset="utf-8" http-equiv="Content-Type" content="text/html">
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <title><?php echo isset($SEOtitle) ? $SEOtitle : $REGION['R_SEO_Title'] ?></title>
            <meta name="description" content="<?php echo isset($SEOdescription) ? $SEOdescription : $REGION['R_SEO_Description'] ?>" />
            <meta name="keywords" content="<?php echo isset($SEOkeywords) ? $SEOkeywords : $REGION['R_SEO_Keywords'] ?>" />

            <!-- CSS FILES -->
            <link rel="stylesheet" type="text/css" href="http://<?php echo $REGION['R_Domain'] . DOMAIN_MOBILE_REL ?>stylesheets/mainscreen.min.css" media="all" />
            <!-- JS FILES -->
            <script src="http://<?php echo $REGION['R_Domain'] . DOMAIN_MOBILE_REL ?>include/jquery-1.12.4.js"></script>
            <script type="text/javascript" src="http://<?php echo $REGION['R_Domain'] . DOMAIN_MOBILE_REL ?>include/js/script.js"></script>
            <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo STATIC_MAP_API_KEY ?>" type="text/javascript"></script>
            <script type="text/javascript">

                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', '<?php echo $REGION['R_Tracking_ID'] ?>']);
                _gaq.push(['_trackPageview']);

                (function () {
                    var ga = document.createElement('script');
                    ga.type = 'text/javascript';
                    ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0];
                    s.parentNode.insertBefore(ga, s);
                })();

            </script>
    </head>
    <body class="map_page">
        <?PHP
        //Getting Region Theme
        $getTheme = "SELECT * FROM  `tbl_Theme_Options_Mobile` WHERE TO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
        $themeRegion = mysql_query($getTheme, $db) or die("Invalid query: $getTheme -- " . mysql_error());
        $THEME = mysql_fetch_assoc($themeRegion);
        
        //Getting Default Images
        $getDefaultImage = "SELECT * FROM  `tbl_Theme_Options` WHERE TO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
        $DefaultImageRegion = mysql_query($getDefaultImage, $db) or die("Invalid query: $getDefaultImage -- " . mysql_error());
        $DefaultImage = mysql_fetch_assoc($DefaultImageRegion);
        //Defualt Header Images
        if ($DefaultImage['TO_Default_Header_Mobile'] != '') {
            $default_header_image = $DefaultImage['TO_Default_Header_Mobile'];
        } else {
            $default_header_image = 'Default-Main-Mobile.jpg';
        }

        //Defualt Thumbnail Images
        if ($DefaultImage['TO_Default_Thumbnail_Mobile'] != '') {
            $default_thumbnail_image = $DefaultImage['TO_Default_Thumbnail_Mobile'];
        } else {
            $default_thumbnail_image = 'Default-Thumbnail-Mobile.jpg';
        }

        $include_free_listings = '';
        if ($REGION['R_Include_Free_Listings'] != 1) {
            $include_free_listings = ' AND BL_Listing_Type > 1';
        }
        if ($REGION['R_Include_Free_Listings_On_Map'] == 1) {
            $include_free_listings_on_map = '';
        } else {
            $include_free_listings_on_map = ' AND BL_Listing_Type > 1';
        }
        if ($REGION['R_Order_Listings_Manually'] == 1) {
            $order_listings = 'ORDER BY BLO_Order ASC, LT_Order DESC, BL_Listing_Title';
        } else {
            $order_listings = 'ORDER BY BL_Points DESC, LT_Order DESC, BL_Listing_Title';
        }
        //Getting Regions if it has child regions
        /* Becareful...
          ----------------------------------------------------------------------------------------------------------- */
        /* $regionList is used in multiple querries.
          ----------------------------------------------------------------------------------------------------------- */
        $regionList = '';
        if ($REGION['R_Parent'] == 0) {
            $sql = "SELECT * FROM tbl_Region_Multiple LEFT JOIN tbl_Region ON RM_Child = R_ID WHERE RM_Parent = '" . encode_strings($REGION['R_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $first = true;
            $regionList .= "(";
            while ($row = mysql_fetch_assoc($result)) {
                if ($first) {
                    $first = false;
                } else {
                    $regionList .= ",";
                }
                $regionList .= $row['RM_Child'];
            }
            $regionList .= ")";
        }
        ?>