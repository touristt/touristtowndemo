<?php
require_once '../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
require_once 'include/public/header.php';

$sql_slider = "SELECT RP_Photo_Mobile, RP_Photo_Title, RP_Description FROM tbl_Region_404 WHERE RP_RID = '" . $REGION['R_ID'] . "'";
$result_slider = mysql_query($sql_slider) or die("Invalid query: $sql_slider -- " . mysql_error());
$row_slider = mysql_fetch_array($result_slider);

$sql_slider_404 = " SELECT bl.BL_ID, bl.BL_Listing_Title, blp.BLP_Mobile_Photo, bl.BL_Name_SEO
                    FROM tbl_404_Listing l LEFT JOIN tbl_Business_Listing bl ON l.BL_ID = bl.BL_ID 
                    LEFT JOIN tbl_Business_Listing_Photo blp ON bl.BL_ID = blp.BLP_BL_ID
                    LEFT JOIN tbl_Business_Listing_Category_Region blcr ON bl.BL_ID = blcr.BLCR_BL_ID
                    LEFT JOIN tbl_Business_Listing_Category blc ON bl.BL_ID = blc.BLC_BL_ID 
                    LEFT JOIN tbl_Category c ON blc.BLC_C_ID = c.C_ID
                    LEFT JOIN tbl_Region_Category rc ON rc.RC_C_ID = c.C_ID AND rc.RC_R_ID = '" . $REGION['R_ID'] . "'
                    WHERE l.R_ID = '" . $REGION['R_ID'] . "' AND blp.BLP_R_ID  = '" . $REGION['R_ID'] . "' AND bl.hide_show_listing = 1 GROUP BY bl.BL_ID ";

$sql_slider_404_result = mysql_query($sql_slider_404, $db) or die("Invalid query: $sql_slider_404 -- " . mysql_error());
?>
<!--Slider Start-->
<section class="main_image">
    <div class="title">
        <p><?php echo ucfirst($num_of_region_404['RP_Photo_Title']); ?></p>
    </div>
    <?php if ($row_slider['RP_Photo_Mobile'] != '') { ?>
        <img class="slider_image" class="show-slider-display" src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $row_slider['RP_Photo_Mobile'] ?>">
    <?php } else { ?>
        <img class="slider_image" class="show-slider-display" src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $default_header_image ?>">
    <?php } ?> 
</section>
<!--Slider End-->
<!--Description Start-->
<section class="description">
    <?PHP if ($num_of_region_404['RP_Description']) { ?>
        <p><?PHP echo ($num_of_region_404['RP_Description']); ?></p>
    <?PHP } ?>
</section>
<!--Description End-->
<!--Thumbnail Grid-->
<section class="thumbnails">
    <?php
    $count_row_404_region = mysql_num_rows($sql_slider_404_result);
    if ($count_row_404_region > 0) {
        while ($show_row_404_region = mysql_fetch_array($sql_slider_404_result)) {
            if ($show_row_404_region['BLP_Mobile_Photo'] != '') {
                $listing_image = $show_row_404_region['BLP_Mobile_Photo'];
            } else {
                $listing_image = $default_thumbnail_image;
            }
            ?>
            <div class="thumbnails-inner">
                <div class="thumbnail">
                    <a href="<?php echo DOMAIN_MOBILE_REL . 'profile/' . $show_row_404_region['BL_Name_SEO'] . '/' . $show_row_404_region['BL_ID']; ?>">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo $show_row_404_region['BL_Listing_Title']; ?>"/>
                        <div class="title index">
                            <p><?php echo $show_row_404_region['BL_Listing_Title']; ?></p>
                        </div>
                    </a>
                </div>
            </div>
            <?php
        }
    }
    ?>
</section>
<!--Thumbnail Grid-->
<?php require_once 'include/public/footer.php'; ?>