<?php
require_once '../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';

if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $getStory = "SELECT S_ID, S_Title, S_Author, S_SEO_Title, S_SEO_Description, S_SEO_Keywords FROM tbl_Story
            LEFT JOIN tbl_Story_Region ON S_ID = SR_S_ID
            LEFT JOIN tbl_Region ON R_ID = SR_R_ID   
            LEFT JOIN tbl_Category ON C_ID = S_Category   
            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            WHERE S_ID = '" . encode_strings($_REQUEST['id'], $db) . "' AND S_Active = 1";
    $resStory = mysql_query($getStory, $db) or die("Invalid query: $getStory -- " . mysql_error());
    $activeStory = mysql_fetch_assoc($resStory);
} else {
    header("Location: " . DOMAIN_MOBILE_REL . "404.php");
    exit();
}

$SEOtitle = $activeStory['S_SEO_Title'];
$SEOdescription = $activeStory['S_SEO_Description'];
$SEOkeywords = $activeStory['S_SEO_Keywords'];
//Get first content piece image
$sqlCP = "SELECT CP_Photo_Mobile FROM tbl_Content_Piece WHERE CP_S_ID = '" . encode_strings($activeStory['S_ID'], $db) . "' ORDER BY CP_Order ASC LIMIT 1";
$resCP = mysql_query($sqlCP);
$CP = mysql_fetch_assoc($resCP);
//OG Tags for FB Share
$OG_URL = curPageURL();
$OG_Type = 'article';
$OG_Title = $activeStory['S_SEO_Title'] != '' ? $activeStory['S_SEO_Title'] : $activeStory['S_Title'];
$OG_Description = $activeStory['S_SEO_Description'];
$OG_Image = 'http://' . DOMAIN . IMG_LOC_REL . $CP['CP_Photo_Mobile'];
$OG_Image_Width = 715;
$OG_Image_Height = 400;
require_once 'include/public/header.php';
?>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1356712584397235";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!--Description Start-->
<section class="detail-page">
    <div class="story-title">
        <div class="title">
            <?php echo $activeStory['S_Title'] ?>
        </div>
    </div>
    <?php if (isset($REGION['R_Show_Hide_Story_Author']) && $REGION['R_Show_Hide_Story_Author'] == 1) { ?>
        <div class="content-title margin-none"><p><?php echo $activeStory['S_Author'] ?></p></div> 
    <?php } ?>
    <div class="share-this-story">
        <div class="share-this-story-inner">
            <p><?php echo (isset($REGION['R_Story_Share_Text']) && $REGION['R_Story_Share_Text'] != '') ? $REGION['R_Story_Share_Text'] : 'Share this story...'; ?></p>
            <div class="fb-like" data-href="<?php print curPageURL(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"style="vertical-align: middle;"></div>
            <iframe class="twitter-class"
                    src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=http%3A%2F%2F<?php print curPageURL(); ?>&related=twitterapi%2Ctwitter&text=<?php echo $activeStory['S_Title'] ?> in <?php echo $REGION['R_Name'] ?>"
                    width="70"
                    height="25"
                    style="border: 0; overflow: hidden; float: left;">
            </iframe>
        </div>
    </div>
    <?php
    $getStoryContent = "SELECT CP_Title, CP_Description, CP_Photo_Mobile, CP_Pdf, CP_Pdf_Title FROM tbl_Content_Piece WHERE CP_S_ID = '" . encode_strings($activeStory['S_ID'], $db) . "' ORDER BY CP_Order ASC";
    $resContent = mysql_query($getStoryContent, $db) or die("Invalid query: $getStoryContent -- " . mysql_error());
    while ($activeContent = mysql_fetch_assoc($resContent)) {
        if ($activeContent['CP_Photo_Mobile'] != "") {
            ?>
            <div class="main_image"><img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $activeContent['CP_Photo_Mobile'] ?>" alt="<?php echo $activeContent['CP_Title'] ?>"/></div>
        <?php } ?>
        <div class="content-title"><p><?php echo $activeContent['CP_Title'] ?></p></div>
        <div class="content-description"><?php echo $activeContent['CP_Description'] ?>
            <?php if (isset($activeContent['CP_Pdf']) && $activeContent['CP_Pdf'] !== '') { ?>
                <div class="pdf-story-front-view"><a  href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $activeContent['CP_Pdf'] ?>" target="_blank"><?php echo ($activeContent['CP_Pdf_Title']) ? $activeContent['CP_Pdf_Title'] : "View PDF" ?></a></div>
                <?php } ?>
        </div>

    <?php } ?>
</section>
<!--Description End-->
<?php require_once 'include/public/footer.php'; ?>