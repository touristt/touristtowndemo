<?php
require_once '../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
require_once 'include/public/map_header.php';

$sqlList = "SELECT DISTINCTROW BL_ID, BL_Listing_Title, BL_Name_SEO, BL_Lat, BL_Long, BL_Street, BL_Town, BLP_Mobile_Photo, BL_Photo_Alt, 
            RC.RC_Map_Icon as subcat_icon, RC1.RC_Map_Icon as cat_icon FROM tbl_Business_Listing
            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
            INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID AND BLCR_BLC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            INNER JOIN tbl_Region_Category RC ON RC.RC_C_ID = BLC_C_ID AND RC.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            INNER JOIN tbl_Region_Category RC1 ON RC1.RC_C_ID = BLC_M_C_ID AND RC1.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
            LEFT JOIN tbl_Business_Listing_Order ON BL_ID = BLO_BL_ID AND BLO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
            AND BLO_S_C_ID = BLC_C_ID
            WHERE (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = '1' AND RC.RC_Status = 0 AND RC1.RC_Status = 0 AND BLP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            $include_free_listings_on_map";
if ($_GET['cid'] > 0) {
    $cid = $_GET['cid'];
    $sqlList .= " AND BLC_M_C_ID = '" . encode_strings($cid, $db) . "'";
}
$sqlList .= " GROUP BY BL_ID $order_listings";
// generate thumbnails for listings on left side bar and markers for map
$resList = mysql_query($sqlList, $db) or die(mysql_error() . ' - ' . $sqlList);
while ($List = mysql_fetch_assoc($resList)) {
    //add to markers for map
    if ($List['BLP_Mobile_Photo'] != '') {
        $image = '<div class="thumbnail"><img width="130" height="90" src="http://' . DOMAIN . ((IMG_LOC_REL . $List['BLP_Mobile_Photo'])) . '" alt="' . htmlspecialchars(trim($List['BL_Photo_Alt']), ENT_QUOTES) . '" /></div>';
    } else {
        $image = '<div class="thumbnail"><img src="http://' . DOMAIN . IMG_LOC_REL . $default_thumbnail_image . '" alt="' . htmlspecialchars(trim($List['BL_Listing_Title']), ENT_QUOTES) . '" /></div>';
    }
    if ($List['subcat_icon'] != '') {
        $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $List['subcat_icon'];
    } elseif ($List['cat_icon'] != '') {
        $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $List['cat_icon'];
    } else {
        $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . 'default.png';
    }

    //      $marker = false;
    if ($List['BL_Lat'] && $List['BL_Long']) {
        $markers[] = array(
            'id' => $List['BL_ID'],
            'lat' => $List['BL_Lat'],
            'lon' => $List['BL_Long'],
            'name' => $List['BL_Listing_Title'],
            'path' => '/mobile/profile/' . $List['BL_Name_SEO'] . '/' . $List['BL_ID'] . '/',
            'icon' => $icon,
            'main_photo' => $image,
            'address' => htmlspecialchars(trim($List['BL_Street']), ENT_QUOTES),
            'town' => htmlspecialchars(trim($List['BL_Town']), ENT_QUOTES)
        );
    }
}
$markers = json_encode($markers);
if ($REGION['R_Map_Mobile_Header_Image'] != '') {
    ?>
    <!--Slider Start-->
    <section class="main_image">
        <img class="slider_image" class="show-slider-display" src="http://<?php echo DOMAIN; ?>/images/DB/<?php echo $REGION['R_Map_Mobile_Header_Image']; ?>" alt="Map Header Image">
        <?php if ($THEME['TO_Slider_Overlay'] != "") { ?>
            <div class="image_overlay_img">
                <div class="image_overlay">
                    <img src="<?php echo 'http://' . DOMAIN . IMG_ICON_REL . $THEME['TO_Slider_Overlay']; ?>" alt="Slider Overlay">
                </div>
            </div>
        <?php } ?>
    </section>
    <!--Slider End-->
<?php } ?>
<div class="map_wrapper" style="float:left">
    <div id="map_canvas">&nbsp;</div>
</div> <!-- .content-right-outer -->
<?php
$map_center['latitude'] = $REGION['R_Lat'];
$map_center['longitude'] = $REGION['R_Long'];
$map_center['zoom'] = $REGION['R_Zoom'];
$kml_json = json_encode(array());
require_once 'map_script.php';
?>
<?php require_once 'include/public/map_footer.php'; ?>