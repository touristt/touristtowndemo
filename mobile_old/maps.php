<?php

require 'config.php';
$error = false;
if (isset($_POST['region']) && $_POST['region'] > 0) {
$region_id = $_POST['region'];
} else {
    $error = true;
}
$ACT_REGION = get_regions($region_id);
$sql = "SELECT  DISTINCTROW tbl_Business_Listing.*, tbl_Listing_Type.*, tbl_Business.* 
        FROM tbl_Business_Listing 
        LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
        LEFT JOIN tbl_Business_Listing_Category_Region ON  BLC_ID = BLCR_BLC_ID 
        LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
        LEFT JOIN tbl_Business ON B_ID = BL_B_ID 
        WHERE BLCR_BLC_R_ID IN " . $ACT_REGION['regionList'] . " AND B_ID is NOT NULL AND (B_lat <> '' OR B_long <> '')";
$markers = array();

// generate icons for all listings on map regardless of page
$result_map = mysql_query($sql);
if (!$result_map) {
    $error = true;
}
while ($list = mysql_fetch_array($result_map, MYSQL_ASSOC)) {

    if ($list['BL_C_ID'] > 0) {
        $icon = BASEPATH . "images/map-icons/" . $list['BL_C_ID'] . ".png";
    }
    if ($list['B_Lat'] && $list['B_Long']) {
        // Add tooltip content
        if ($list['BL_Photo'] != '') {
            $image = '<div class="thumbnail"><img width="130" src="' . (BASEPATH . 'images/DB/' . $list['BL_Photo']) . '" alt="" /></div>';
        } else {
            $image = '<div class="thumbnail"><img width="130" src="' . BASEPATH . 'images/Listing-NoPhoto.jpg" alt="" /></div>';
        }

        $markers[] = array(
            'id' => $list['BL_ID'],
            'lat' => $list['B_Lat'],
            'lon' => $list['B_Long'],
            'name' => htmlspecialchars(trim($list['B_Name']), ENT_QUOTES),
            'path' => BASEPATH . 'mobile/listingdetail.html?listing_id=' . $list['BL_ID'],
            'icon' => $icon,
            'main_photo' => $image,
            'address' => htmlspecialchars(trim($list['B_Street']), ENT_QUOTES),
            'town' => htmlspecialchars(trim($list['B_Town']), ENT_QUOTES),
        );
    }
}
$sql_map = "SELECT * FROM tbl_Region WHERE R_ID = " . $ACT_REGION['region']['R_ID'];
$res_map = mysql_query($sql_map);
if (!$res_map) {
    $error = true;
}
$map_options = mysql_fetch_assoc($res_map);

$return['map_options'] = $map_options;
$return['markers'] = $markers;
$return['error'] = $error;
print json_encode($return);
exit;
?>