<?php

include 'config.php';
$error = false;
if ((isset($_POST['region']) && $_POST['region'] > 0) && (isset($_POST['c_id']) && $_POST['c_id'] > 0)) {
    $id = $_POST['c_id'];
    $region_id = $_POST['region'];
} else {
    $error = true;
}
$ACT_REGION = get_regions($region_id);

$subcat = array();

$regionList = '';
if ($$ACT_REGION['region']['R_Parent'] == 0) {
    $sql = "SELECT * FROM tbl_Region_Multiple LEFT JOIN tbl_Region ON RM_Child = R_ID WHERE RM_Parent = '" . $ACT_REGION['region']['R_ID'] . "'";
    $result = mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    $first = true;
    $regionList .= "(";
    while ($row = mysql_fetch_assoc($result)) {
        if ($first) {
            $first = false;
        } else {
            $regionList .= ",";
        }
        $regionList .= $row['RM_Child'];
    }
    $regionList .= ")";
}

$sql_cat = "SELECT C_Name, RC_Name, C_Is_Blog FROM tbl_Category
               LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID ='" . $ACT_REGION['region']['R_ID'] . "'
               WHERE C_ID =$id AND RC_Status =0 AND RC_R_ID >0 ORDER BY C_Order";
$res_cat = mysql_query($sql_cat);
$r_cat = mysql_fetch_array($res_cat);
if ($id != 8 && $r_cat['C_Is_Blog'] != 1) {
    $sql_nav_sub = "SELECT C_ID, RC_Name, C_Name, RC_Mobile_Thumbnail FROM tbl_Category 
                    LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID  " . ($ACT_REGION['region']['R_Parent'] == 0 ? "IN " . $regionList : "= " . $ACT_REGION['region']['R_ID']) . "
                    INNER JOIN tbl_Business_Listing_Category ON BLC_C_ID = C_ID    
                    INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID
                    INNER JOIN tbl_Business_Listing ON BL_ID = BLC_BL_ID AND BLCR_BLC_R_ID  " . ($ACT_REGION['region']['R_Parent'] == 0 ? "IN " . $regionList : "= " . $ACT_REGION['region']['R_ID']) . "  
                    WHERE C_Parent = '$id' and RC_Status=0 AND RC_R_ID > 0 AND BL_Free_Listing_status =0
                    GROUP BY C_ID ORDER BY C_Order";
} else if($id == 8) {
    $sql_nav_sub = "SELECT C_ID, RC_Name, C_Name, RC_Mobile_Thumbnail FROM tbl_Category 
                    LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . $ACT_REGION['region']['R_ID'] . "'
                    WHERE C_Parent = '$id' and RC_Status=0 AND RC_R_ID > 0
                    ORDER BY C_Order";
}
else {
    $sql_nav_sub = "SELECT * FROM tbl_Category 
                  LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . $ACT_REGION['region']['R_ID'] . "'
                  WHERE C_Parent = '$id' and RC_Status=0 AND RC_R_ID > 0
                  ORDER BY C_Order";
}
$res_nav_sub = mysql_query($sql_nav_sub) or die('invalid - '. $sql_nav_sub . mysql_error());
if(!$res_nav_sub){
    $error = true;
}
$sub = array();
while ($r = mysql_fetch_object($res_nav_sub)) {
    $sub[] = $r;
}
$subcat['subcategories'] = $sub;
$subcat['category_name'] = ($r_cat['RC_Name']!='')?$r_cat['RC_Name']:$r_cat['C_Name'];
$subcat['error'] = $error;

echo json_encode($subcat);
?>