<?php

include 'config.php';
$error = false;
if (isset($_POST['region']) && $_POST['region'] != '') {
  $region_id = $_POST['region'];
} else {
  $error = true;
}
$ACT_REGION = get_regions($region_id);
$logo = array();
// get footer message
$sql = "SELECT R_Footer_Text,R_Phone,TO_Desktop_Logo,TO_Desktop_Logo_Alt,R_Name FROM tbl_Region LEFT JOIN tbl_Theme_Options_Mobile ON TO_R_ID = R_ID WHERE R_ID = " . $ACT_REGION['region']['R_ID'];
$res = mysql_query($sql);
$footer = '';
$phone = '';
if (mysql_num_rows($res) > 0) {
  $result = mysql_fetch_assoc($res);
  $phone = $result['R_Phone'];
  $footer = $result['R_Footer_Text'];
  $logo_mobile = $result['TO_Desktop_Logo'];
  $logo_alt = $result['TO_Desktop_Logo_Alt'];
  $R_Name = $result['R_Name'];
}
$logo['footer'] = $footer;
$logo['phone'] = $phone;
$logo['logo'] = $logo_mobile;
$logo['alt'] = $logo_alt;
$logo['R_Name'] = $R_Name;
$logo['AUTH_ADMIN'] = $_SESSION['AUTH_ADMIN'];
$logo['error'] = $error;

$sql_nav = "SELECT C_ID, C_Name, RC_Name FROM tbl_Category 
                            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . $ACT_REGION['region']['R_ID'] . "'
                                    WHERE C_Parent = 0 and RC_Status=0 ORDER BY RC_Order ASC";
//if($ACT_REGION['region']['R_Type']!=4)
//{
//  $sql_routes  .= " AND C_Is_Blog = 0 ";
//}
// $sql_routes .=" ORDER BY RC_Order ASC";
$resNav = mysql_query($sql_nav);
$categories = array();
while ($navigation = mysql_fetch_object($resNav)) {
  $categories[] = $navigation;
}
//Get footer logos
$footer = "SELECT FP_Photo, FP_Link from tbl_Footer_Photo WHERE FP_Link != '' AND FP_Section = 1 AND FP_R_ID = '" . $ACT_REGION['region']['R_ID'] . "' ORDER BY FP_Order ASC";
$footerResult = mysql_query($footer);
$footer_1 = array();
while ($footerRow = mysql_fetch_object($footerResult)) {
  $footer_1[] = $footerRow;
}
//Get footer logos 2
$footer2 = "SELECT FP_Photo, FP_Link from tbl_Footer_Photo WHERE FP_Link != '' AND FP_Section = 2 AND FP_R_ID = '" . $ACT_REGION['region']['R_ID'] . "' ORDER BY FP_Order ASC";
$footerResult2 = mysql_query($footer2);
$footer_2 = array();
while ($footerRow2 = mysql_fetch_object($footerResult2)) {
  $footer_2[] = $footerRow2;
}
//Get footer links
$footer3 = "SELECT FP_Alt, FP_Link from tbl_Footer_Photo WHERE FP_Link != '' AND FP_Section = 3 AND FP_R_ID = '" . $ACT_REGION['region']['R_ID'] . "' ORDER BY FP_Order ASC";
$footerResult3 = mysql_query($footer3);
$footer_3 = array();
while ($footerRow3 = mysql_fetch_object($footerResult3)) {
  $footer_3[] = $footerRow3;
}
// Get search icon
$sql_icons = "SELECT RS_SE_Icon FROM tbl_Region_Social WHERE RS_R_ID = '" . $ACT_REGION['region']['R_ID'] . "'";
$result_icons = mysql_query($sql_icons);
$row_icons = mysql_fetch_assoc($result_icons);
$logo['footer_logos'] = $footer_1;
$logo['footer_logos2'] = $footer_2;
$logo['footer_links'] = $footer_3;
$logo['categories'] = $categories;
$logo['search_icon'] = $row_icons['RS_SE_Icon'];
echo json_encode($logo);
?>