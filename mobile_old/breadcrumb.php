<?php

include 'config.php';
$error = false;
if ((isset($_POST['region']) && $_POST['region'] > 0) && (isset($_POST['id']) && $_POST['id'] > 0)) {
    $cat_id = $_POST['id'];
    $region_id = $_POST['region'];
} else {
    $error = true;
}
$ACT_REGION = get_regions($region_id);
$breadcrumb = array();
$cat = '';
$sub = '';
$sql_cat = "SELECT C_Name, RC_Name FROM tbl_Region_Category LEFT JOIN tbl_Category ON RC_C_ID = C_ID AND RC_R_ID = " . $ACT_REGION['region']['R_ID'] . " WHERE C_ID = '$cat_id'";
$res_cat = mysql_query($sql_cat);
if (!$res_cat) {
    $error = true;
}
$cat_row = mysql_fetch_assoc($res_cat);
if ($cat_row['RC_Name'] != '') {
    $cat = $cat_row['RC_Name'];
} else {
    $cat = $cat_row['C_Name'];
}
if (isset($_POST['sub']) && $_POST['sub'] != 0) {
    $subcat_id = $_POST['sub'];
    $sql_sub_cat = "SELECT C_Name, RC_Name FROM tbl_Region_Category LEFT JOIN tbl_Category ON RC_C_ID = C_ID AND RC_R_ID = " . $ACT_REGION['region']['R_ID'] . " WHERE C_ID = '$subcat_id'";
    $res_sub_cat = mysql_query($sql_sub_cat);
    if (!$res_cat) {
        $error = true;
    }
    $sub_cat_row = mysql_fetch_assoc($res_sub_cat);
    if ($sub_cat_row['RC_Name'] != '') {
        $sub = $sub_cat_row['RC_Name'];
    } else {
        $sub = $sub_cat_row['C_Name'];
    }
}
$breadcrumb['category'] = $cat;
$breadcrumb['subcategory'] = $sub;
$breadcrumb['error'] = $error;
echo json_encode($breadcrumb);
?>