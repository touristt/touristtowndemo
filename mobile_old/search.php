<?php

include 'config.php';
$error = false;
if (isset($_REQUEST['region']) && $_REQUEST['region'] > 0) {
    $region_id = $_REQUEST['region'];
} else {
    $error = true;
}
$search = mysql_real_escape_string($_REQUEST['query']);
$ACT_REGION = get_regions($region_id);
include 'commonFunctions.inc.php';

$query = "SELECT BL_ID, BL_Mobile_Header_Image, BL_Listing_Title 
            FROM tbl_Business_Listing
            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
            LEFT JOIN tbl_Business_Listing_Category_Region ON  BL_ID = BLCR_BL_ID 
            LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
            LEFT JOIN tbl_Business ON B_ID = BL_B_ID 
            WHERE BLCR_BLC_R_ID IN " . $ACT_REGION['regionList'] . " AND B_ID is NOT NULL 
            AND (
                    B_Name LIKE '%" . $search . "%' OR 
                    BL_Search_Words LIKE '%" . $search . "%' OR 
                    BL_Description LIKE '%" . $search . "%' OR 
                    BL_Listing_Title LIKE '%" . $search . "%' 
            )
            AND hide_show_listing='1' AND BL_Free_Listing_status = 0 GROUP BY BL_ID ORDER BY BL_Points DESC, LT_Order DESC";
$detail = mysql_query($query) or die('invalid query: '. $query . mysql_error());
if (!$detail) {
    $error = true;
}
$listing = array();
$list = array();
while ($row = mysql_fetch_assoc($detail)) {
    $list[] = $row;
//    print_r($row);
}
$listing['details'] = $list;
//foreach ($listing['details'] as $val) {
//    listingTrack($val['BL_ID'], 0, 1, 0, $ACT_REGION['region']['R_ID']);
//}
$listing['error'] = $error;
print json_encode($listing);exit;
?>