<?php
include 'config.php';
$error = false;
if (isset($_POST['region']) && $_POST['region'] != '') {
    $region_id = $_POST['region'];
} else {
    $error = true;
}
$THEME_ARRAY = array();
$getTheme = "SELECT TO_Listing_Title_Text,TO_Listing_Location_Text,TO_Listing_Sub_Nav_Text,TO_General_Body_Copy,TO_General_Body_Copy_Line_Spacing,
             TO_Main_Navigation,TO_Main_Navigation_Selected,TO_Main_Page_Body_Copy,TO_Slider_Title,TO_Homepage_Description_Background,TO_Homepage_Description_Space,
             TO_Slider_Overlay,TO_Footer_Background_Color,TO_Bar_Texture,TO_Thumbnail_Category_Tite,TO_Homepage_Event_Title,TO_Homepage_Event_Date,TO_Footer_Links,TO_Footer_Disclaimer, 
             TO_Subcat_Texture,TO_Thumbnail_Sub_Category_Title,TO_Homepage_Event_Main_Title,TO_Category_Header_Text,TO_Category_Header_BG,TO_Thumbnail_Listing_Title,TO_Listing_Texture FROM  `tbl_Theme_Options_Mobile` WHERE TO_R_ID = $region_id";
$themeRegion = mysql_query($getTheme) or die("Invalid query: $getTheme -- " . mysql_error());
$THEME = mysql_fetch_assoc($themeRegion);

$listing_main_title_value = explode("-", $THEME['TO_Listing_Title_Text']);
$listing_location_text_value = explode("-", $THEME['TO_Listing_Location_Text']);
$listing_sub_nav_text = explode("-", $THEME['TO_Listing_Sub_Nav_Text']);
$listing_general_body_copy = explode("-", $THEME['TO_General_Body_Copy']);
$main_navigation = explode("-", $THEME['TO_Main_Navigation']);
$main_navigation_selected = explode("-", $THEME['TO_Main_Navigation_Selected']);
$main_page_body_copy = explode("-", $THEME['TO_Main_Page_Body_Copy']);
$slider_title = explode("-", $THEME['TO_Slider_Title']);
$thumbnail_category_title = explode("-", $THEME['TO_Thumbnail_Category_Tite']);
$category_event_title = explode("-", $THEME['TO_Homepage_Event_Main_Title']);
$category_event_date = explode("-", $THEME['TO_Homepage_Event_Date']);
$footer_links = explode("-", $THEME['TO_Footer_Links']);
$footer_disclaimer = explode("-", $THEME['TO_Footer_Disclaimer']);
$footer_background_color = explode("-", $THEME['TO_Footer_Background_Color']);
$thumbnail_sub_category_title = explode("-", $THEME['TO_Thumbnail_Sub_Category_Title']);
$thumbnail_listing_title = explode("-", $THEME['TO_Thumbnail_Listing_Title']);
$category_header_text = explode("-", $THEME['TO_Category_Header_Text']);
$home_des_bg_color = explode(">", $THEME['TO_Homepage_Description_Background']);
$bar_texture = explode(">", $THEME['TO_Bar_Texture']);
$subcat_Texture = explode(">", $THEME['TO_Subcat_Texture']);
$listing_texture = explode(">", $THEME['TO_Listing_Texture']);
$THEME_ARRAY['footer_links_font']=$footer_links[0];
$THEME_ARRAY['footer_links_size']=$footer_links[1].'px';
$THEME_ARRAY['footer_links_color']=$footer_links[2];
$THEME_ARRAY['footer_disclaimer_font']=$footer_disclaimer[0];
$THEME_ARRAY['footer_disclaimer_size']=$footer_disclaimer[1].'px';
$THEME_ARRAY['footer_disclaimer_color']=$footer_disclaimer[2];
$THEME_ARRAY['category_event_font']=$category_event_title[0];
$THEME_ARRAY['category_event_size']=$category_event_title[1].'px';
$THEME_ARRAY['category_event_color']=$category_event_title[2];
$THEME_ARRAY['category_event_date_font']=$category_event_date[0];
$THEME_ARRAY['category_event_date_size']=$category_event_date[1].'px';
$THEME_ARRAY['category_event_date_color']=$category_event_date[2];
$THEME_ARRAY['listing_title_font']=$listing_main_title_value[0];
$THEME_ARRAY['listing_title_font_size']=$listing_main_title_value[1].'px';
$THEME_ARRAY['listing_title_font_color']=$listing_main_title_value[2];
$THEME_ARRAY['listing_location_text_font']=$listing_location_text_value[0];
$THEME_ARRAY['listing_location_text_size']=$listing_location_text_value[1].'px';
$THEME_ARRAY['listing_location_text_color']=$listing_location_text_value[2];
$THEME_ARRAY['listing_sub_nav_text_font']=$listing_sub_nav_text[0];
$THEME_ARRAY['listing_sub_nav_text_size']=$listing_sub_nav_text[1].'px';
$THEME_ARRAY['listing_sub_nav_text_color']=$listing_sub_nav_text[2];
$THEME_ARRAY['listing_general_body_copy_font']=$listing_general_body_copy[0];
$THEME_ARRAY['listing_general_body_copy_size']=$listing_general_body_copy[1].'px';
$THEME_ARRAY['listing_general_body_copy_color']=$listing_general_body_copy[2];
$THEME_ARRAY['main_navigation_font']=$main_navigation[0];
$THEME_ARRAY['main_navigation_size']=$main_navigation[1].'px';
$THEME_ARRAY['main_navigation_color']=$main_navigation[2];
$THEME_ARRAY['main_navigation_selected_font']=$main_navigation_selected[0];
$THEME_ARRAY['main_navigation_selected_size']=$main_navigation_selected[1].'px';
$THEME_ARRAY['main_navigation_selected_color']=$main_navigation_selected[2];
$THEME_ARRAY['main_page_body_copy_font']=$main_page_body_copy[0];
$THEME_ARRAY['main_page_body_copy_size']=$main_page_body_copy[1].'px';
$THEME_ARRAY['main_page_body_copy_color']=$main_page_body_copy[2];
$THEME_ARRAY['slider_title_font']=$slider_title[0];
$THEME_ARRAY['slider_title_size']=$slider_title[1].'px';
$THEME_ARRAY['slider_title_color']=$slider_title[2];
$THEME_ARRAY['thumbnail_category_title_font']=$thumbnail_category_title[0];
$THEME_ARRAY['thumbnail_category_title_size']=$thumbnail_category_title[1].'px';
$THEME_ARRAY['thumbnail_category_title_color']=$thumbnail_category_title[2];
$THEME_ARRAY['thumbnail_sub_category_font']=$thumbnail_sub_category_title[0];
$THEME_ARRAY['thumbnail_sub_category_size']=$thumbnail_sub_category_title[1].'px';
$THEME_ARRAY['thumbnail_sub_category_color']=$thumbnail_sub_category_title[2];
$THEME_ARRAY['thumbnail_listing_font']=$thumbnail_listing_title[0];
$THEME_ARRAY['thumbnail_listing_size']=$thumbnail_listing_title[1].'px';
$THEME_ARRAY['thumbnail_listing_color']=$thumbnail_listing_title[2];
$THEME_ARRAY['home_des_bg_image']=$home_des_bg_color[0];
$THEME_ARRAY['home_des_bg_color']=$home_des_bg_color[1];
$THEME_ARRAY['slider_overlay']=$THEME['TO_Slider_Overlay'];
$THEME_ARRAY['category_header_font']=$category_header_text[0];
$THEME_ARRAY['category_header_size']=$category_header_text[1].'px';
$THEME_ARRAY['category_header_color']=$category_header_text[2];
$THEME_ARRAY['category_header_bg']=$THEME['TO_Category_Header_BG'];
$THEME_ARRAY['bar_texture_image']=$bar_texture[0];
$THEME_ARRAY['bar_texture_color']=$bar_texture[1];
$THEME_ARRAY['subcat_Texture_image']=$subcat_Texture[0];
$THEME_ARRAY['subcat_Texture_color']=$subcat_Texture[1];
$THEME_ARRAY['listing_texture_image']=$listing_texture[0];
$THEME_ARRAY['listing_texture_color']=$listing_texture[1];
$THEME_ARRAY['homepage_description_space']=$THEME['TO_Homepage_Description_Space'];
$THEME_ARRAY['footer_background_color']=$footer_background_color;
$THEME_ARRAY['listing_general_body_copy_line_spacing']=$THEME['TO_General_Body_Copy_Line_Spacing'];
$THEME_ARRAY['error'] = $error;
$THEME_ARRAY_Font=  array();
$getFont = "SELECT * FROM `tbl_Theme_Options_Fonts`";
        $themeFontRegion = mysql_query($getFont) or die("Invalid query: $getFont -- " . mysql_error());
        
        while ($THEME_FONT = mysql_fetch_object($themeFontRegion)) {
            $THEME_ARRAY_Font[] = $THEME_FONT;
        }
$THEME_ARRAY['Font_Family'] = $THEME_ARRAY_Font;
echo json_encode($THEME_ARRAY);
?>