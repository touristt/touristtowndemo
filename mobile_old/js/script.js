//var basepath = 'http://www.touristtown.ca/';
var basepath = 'http://touristtowndemo.com/';

/*---------------------------------------------------------------------------------------------------------*/
/* Checking the status of user, If region is inactive this block of code will execute
 -----------------------------------------------------------------------------------------------------------*/
//$(document).ready(function)
$(document).ready(function () {
  var auth_region = getParameterByName('region');
  $.post("region_status.php", {
    auth_region: auth_region
  }, function (done) {
    var res = done.split("@");
    if (res[0] > 0 && res[1] != 1) {
      window.location.href = "authentication.php?RID=" + res[0];
    }
  });
//  $(".searchIcon").click(function () {
//    $('.ui-page-active #search_form').toggle();
//    $('.search-text').focus();
//    $('.ui-page-active').append('<div class="search-form-bg" id="searchIcon-bg"></div>');
//  });
//  $('.search-text').blur(function () {
//    $('.ui-page-active #search_form').toggle();
//    $('#searchIcon-bg').remove();
//  });
});

/*---------------------------------------------------------------------------------------------------------*/
/* Function to load Header, Footer Also checking the admin status
 -----------------------------------------------------------------------------------------------------------*/
function loadLogo(region) {
  $.post('getlogo.php', {
    region: region
  }, function (data) {
    if (data.error == false) {
      document.title = data.R_Name;
      var AUTH_ADMIN = data.AUTH_ADMIN;
      $('.ui-page-active .logo').empty();
      $('.ui-page-active .logo').append('<a href="index.html?region=' + region + '" data-role="none"><img id="img" src="/images/DB/' + data.logo + '" alt="' + data.alt + '" /></a>').trigger('create');
      $('.ui-page-active #logout').hide();
      if (AUTH_ADMIN == 1) {
        $('.ui-page-active #logout').show();
        $('.ui-page-active #logout').empty();
        $('.ui-page-active #logout').append("<a onclick='auth_logout(" + region + ")'>Logout</a>");
      }
      //update search icon
      if(data.search_icon && data.search_icon != '') {
        $('.ui-page-active .searchIcon').attr('src', basepath + 'images/DB/' + data.search_icon);
      }
      else {
        $('.ui-page-active .searchIcon').attr('src', basepath + 'mobile/img/search.png');
      }
      $('.ui-page-active .navigation').empty();
      $('.ui-page-active .navigation').append("<ul class='ul-navigation'></ul>");
      var ACTIVE_CAT = getParameterByName('c_id'); // current active category
      $.each(data.categories, function (index, category) {
        var catName = ((category.RC_Name != '') ? category.RC_Name : category.C_Name);
        var activeClass = '';
        if(ACTIVE_CAT == category.C_ID) {
          activeClass = 'active';
        }
        $('.ui-page-active .navigation .ul-navigation').append('<li><a class="'+ activeClass +'" href="category.html?c_id=' + category.C_ID + '&region=' + region + '" data-transition="slide">' + catName + '</a></li>').trigger("create");
      });
      //Footer logos
      $('.ui-page-active .footer-logos').empty();
      $('.ui-page-active .footer-logos').append('<div class="footer-images"><div class="footer-margin-auto"><div class="footer-anchors"></div></div></div>');
      $.each(data.footer_logos, function (index, footer_logo) {
        $('.ui-page-active .footer-anchors').append('<a href="' + footer_logo.FP_Link + '"><img src="' + basepath + 'images/DB/' + footer_logo.FP_Photo + '" alt="" border="0"/></a>').trigger("create");
      });
      //Footer logos2
      if (data.footer_logos2.length != 0)
      {
        $('.ui-page-active .footer-logos2').empty();
        $('.ui-page-active .footer-logos2').append('<div class="footer-images border-top-none"><div class="footer-margin-auto"><div class="footer-anchors2"></div></div></div>');
        $.each(data.footer_logos2, function (index, footer_logo2) {
          $('.ui-page-active .footer-anchors2').append('<a href="' + footer_logo2.FP_Link + '"><img src="' + basepath + 'images/DB/' + footer_logo2.FP_Photo + '" alt="" border="0"/></a>').trigger("create");
        });
      }
      //Footer links
      if (data.footer_links.length != 0)
      {
        $('.ui-page-active .footer-bottom-urls').empty();
        $('.ui-page-active .footer-bottom-urls').append('<div class="footer-urls"><ul class="footer-urls-list"></ul></div>');
        $.each(data.footer_links, function (index, footer_link) {
          $('.ui-page-active .footer-urls-list').append('<li><a href="' + footer_link.FP_Link + '">'+ footer_link.FP_Alt +'</a></li>').trigger("create");
        });
      }
//      $('.ui-page-active .footer-bottom-urls').empty();
//      $('.ui-page-active .footer-bottom-urls').append('<div class="footer-urls"><ul><li><a href="#">About Us</a></li><li><a href="#">Media Requests</a></li><li><a href="#">Industry</a></li><li><a href="#">FAQ</a></li><li><a href="#">Accessible Public Washrooms</a></li><li><a href="#">Operator Login</a></li><li><a href="#">Contact</a></li></ul></div>');
      $('.ui-page-active .footer-message').empty();
      $('.ui-page-active .footer-message').append(data.footer);
      
      //attach event to search field
      $(".searchIcon").click(function () {
        $('.ui-page-active #search_form').toggle();
        $('.search-text').focus();
        $('.ui-page-active').append('<div class="search-form-bg" id="searchIcon-bg"></div>');
      });
      $('.search-text').blur(function () {
        $('.ui-page-active #search_form').toggle();
        $('#searchIcon-bg').remove();
      });
    }
  }, 'json');
}
/*---------------------------------------------------------------------------------------------------------*/
/* Function to Logout admin
 -----------------------------------------------------------------------------------------------------------*/
function auth_logout(region) {
  window.location.href = "auth_logout.php?region=" + region;
}
/*---------------------------------------------------------------------------------------------------------*/
/* Tracking Regions through Google Analytics
 -----------------------------------------------------------------------------------------------------------*/
$(document).delegate('.ui-page', 'pageshow', function () {
  var R_ID = getParameterByName('region');
  var gAccountNo = '';
  $.post("get-tracking-id.php", {
    R_ID: R_ID
  }, function (done) {
    gAccountNo = done;
  })
  try {
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', gAccountNo]);
    _gaq.push(['_trackPageview']);

    (function () {
      var ga = document.createElement('script');
      ga.type = 'text/javascript';
      ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(ga, s);
    })();
  } catch (err) {

  }
});

/*---------------------------------------------------------------------------------------------------------*/
/* Load Index page
 -----------------------------------------------------------------------------------------------------------*/
$(document).ajaxStop($.unblockUI);
$(document).delegate("#index", "pageshow", function () {
  var region = getParameterByName('region');
  $.blockUI({
    message: '<img src="' + basepath + 'mobile/img/ajaxloader.gif" alt="Loading..." />',
    css: {
      border: 'none',
      backgroundColor: 'transparent'
    },
    applyPlatformOpacityRules: false
  });
  loadLogo(region);
  $('.ui-page-active .search-text').val('');
  $.post('getcategories.php?', {
    region: region
  }, function (data) {
    if (data.error == false) {
      $('.ui-page-active #container').empty();
      $('.ui-page-active #container').append('<div class="main_image"><div class="main_title"><p>' + data.title + '</p></div><img width="100%" src="' + basepath + 'images/DB/' + data.image + '" />' + ((data.overlay != null && data.overlay != '') ? '<div class="image_overlay_img"><div class="image_overlay"><img src="' + basepath + 'theme_icons/' + data.overlay + '" alt=""></div></div>' : '') + '</div>');
      $('.ui-page-active #container').append('<div class="description">' + data.desc + '</div>');
      $('.ui-page-active #container').append('<div id="category-items"></div>').trigger('create');
      var catName = '';
      $.each(data.categories, function (index, category) {
        catName = ((category.RC_Name != '') ? category.RC_Name : category.C_Name);
        $('.ui-page-active #category-items').append('<div class="category"><div class="thumbnail"><a  href="category.html?c_id=' + category.C_ID + '&region=' + region + '" data-transition="slide"><img src="' + basepath + 'images/DB/' + category.RC_Mobile_Thumbnail + '" alt="' + catName + '" /></a></div><a class="data-role-style-new" data-role="button" data-icon="carat-r" data-iconpos="right" href="category.html?c_id=' + category.C_ID + '&region=' + region + '" data-transition="slide"><div class="index-category-title"><p>' + catName + '</p></div></div></a>').trigger("create");
      });
    } else {
      $('.ui-page-active #container').empty();
      $('.ui-page-active #container').append('<div id="category-items"></div>').trigger('create');
      $('.ui-page-active #category-items').append('<div id="noRecordFound">An unknown error occurred. Try again later.</div>').trigger('create');
    }
    loadtheme(region);
  }, 'json');
});

/*---------------------------------------------------------------------------------------------------------*/
/* Load Category page to get sub categories
 -----------------------------------------------------------------------------------------------------------*/
$(document).delegate("#category", "pageshow", function () {
  var region = getParameterByName('region');
  $.blockUI({
    message: '<img src="' + basepath + 'mobile/img/ajaxloader.gif" alt="Loading..." />',
    css: {
      border: 'none',
      backgroundColor: 'transparent'
    },
    applyPlatformOpacityRules: false
  });
  loadLogo(region);
  var c_id = getParameterByName('c_id');
  sub_cat(c_id, region);
});
function sub_cat(c_id, region) {
  $('.ui-page-active .search-text').val('');
  $.post('getsubcat.php', {
    c_id: c_id,
    region: region
  }, function (data) {

    if (data.error == false) {
      $('.ui-page-active #category-container').empty();
      $('.ui-page-active #category-container').append('<div id="sub-category-items"></div>').trigger('create');
      $('.ui-page-active #sub-category-items').append('<div class="category">'+
              ' <div>'+
              '   <div class="main-category-title">'+
              '     <p>' + data.category_name + '</p>'+
              '   </div>'+
              ' </div>'+
              '</div>').trigger("create");
      var catName = '';
      $.each(data.subcategories, function (index, subcat) {
        catName = ((subcat.RC_Name && subcat.RC_Name != '') ? subcat.RC_Name : subcat.C_Name);
        $('.ui-page-active #sub-category-items').append('<div class="category">'+
                ' <div>'+
                '   <div class="thumbnail">'+
                '     <a href="listings.html?c_id=' + c_id + '&sub_id=' + subcat.C_ID + '&region=' + region + '" data-transition="slide">'+
                '       <img class="' + ((subcat.RC_Mobile_Thumbnail == "") ? "with-no-img" : "") + '" src="' + basepath + ((subcat.RC_Mobile_Thumbnail && subcat.RC_Mobile_Thumbnail != "") ? "images/DB/" + subcat.RC_Mobile_Thumbnail : "images/Listing-NoPhoto.jpg") + '" alt="' + catName + '" />'+
                '     </a>'+
                '   </div>'+
                '   <a class="data-role-style-new" data-role="button" data-icon="carat-r" data-iconpos="right" href="listings.html?c_id=' + c_id + '&sub_id=' + subcat.C_ID + '&region=' + region + '" data-transition="slide">'+
                '     <div class="category-title">'+
                '       <p>' + catName + '</p>'+
                '     </div>'+
                '   </a>'+
                ' </div>'+
                '</div>').trigger("create");
      });
    } else {
      $('.ui-page-active #category-container').empty();
      $('.ui-page-active #category-container').append('<div id="sub-category-items"></div>').trigger('create');
      $('.ui-page-active #sub-category-items').append('<div id="noRecordFound">An unknown error occurred. Try again later.</div>').trigger('create');
    }
    loadtheme(region);
  }, 'json');
}
/*---------------------------------------------------------------------------------------------------------*/
/* Get Parameters from the QueryString
 -----------------------------------------------------------------------------------------------------------*/
function getParameterByName(name)
{
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.href);
  if (results === null)
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}
/*---------------------------------------------------------------------------------------------------------*/
/* Load Listings of subcategory
 -----------------------------------------------------------------------------------------------------------*/
$(document).delegate("#listings", "pageshow", function () {
  $.blockUI({
    message: '<img src="' + basepath + 'mobile/img/ajaxloader.gif" alt="Loading..." />',
    css: {
      border: 'none',
      backgroundColor: 'transparent'
    },
    applyPlatformOpacityRules: false
  });
  var region = getParameterByName('region');
  loadLogo(region);
  var category = getParameterByName('c_id');
  var sub_id = getParameterByName('sub_id');
  listings(sub_id, category, region);
});
function listings(sub_id, category, region) {
  $.post('listings.php', {
    sub_id: sub_id,
    region: region
  }, function (data) {
    if (data.error == false) {
      if (data.events == true) {
        $('.ui-page-active #listing').empty();
        $('.ui-page-active #listing').append('<div id="listing-items"></div>').trigger('create');
        $('.ui-page-active #listing-items').append('<div class="category"><div><div class="main-category-title"><p>' + data.category_name + '</p></div></div></div>').trigger("create");
        $.each(data.details, function (index, listing) {
          if (listing.startdate != listing.enddate) {
            $('.ui-page-active #listing-items').append('<div class="category">'+
                    ' <div class="event-detail-container">'+
                    '   <div class="event-title">'+
                    '     <a href="listingdetail.html?c_id=' + category + '&sub_id=' + sub_id + '&region=' + region + '&listing_id=' + listing.EventID + '" data-transition="slide">'+
                    '       <p>' + listing.Title + '</p>'+
                    '     </a>'+
                    '   </div>'+
                    '   <div class="event-date">'+
                    '     <p>' + listing.startdate + ' to ' + listing.enddate + '</p>'+
                    '   </div>'+
                    ' </div>'+
                    '</div>').trigger("create");
          }
          else {
            $('.ui-page-active #listing-items').append('<div class="category">'+
                    ' <div class="event-detail-container">'+
                    '   <div class="event-title">'+
                    '     <a href="listingdetail.html?c_id=' + category + '&sub_id=' + sub_id + '&region=' + region + '&listing_id=' + listing.EventID + '" data-transition="slide">'+
                    '       <p>' + listing.Title + '</p>'+
                    '     </a>'+
                    '   </div>'+
                    '   <div class="event-date">'+
                    '     <p>' + listing.startdate + '</p>'+
                    '   </div>'+
                    ' </div>'+
                    '</div>').trigger("create");
          }
        });
        loadtheme(region);
      } else if (data.story == true) {
        $('.ui-page-active #listing').empty();
        $('.ui-page-active #listing').append('<div id="listing-items"></div>').trigger('create');
        $('.ui-page-active #listing-items').append('<div class="category">'+
                ' <div>'+
                '   <div class="main-category-title">'+
                '     <p>' + data.category_name + '</p>'+
                '   </div>'+
                ' </div>'+
                '</div>').trigger("create");
        $.each(data.details, function (index, listing) {
          $('.ui-page-active #listing-items').append('<div class="category">'+
                  ' <div>'+
                  '   <div class="thumbnail">'+
                  '     <a href="listingdetail.html?listing_id=' + listing.S_ID + '&c_id=' + category + '&sub_id=' + sub_id + '&region=' + region + '" data-transition="slide">'+
                  '       <img src="' + basepath + ((listing.S_Thumbnail_Mobile && listing.S_Thumbnail_Mobile != "") ? "images/DB/" + listing.S_Thumbnail_Mobile : "images/Listing-NoPhoto.jpg") + '" alt="' + listing.S_Title + '" />'+
                  '     </a>'+
                  '   </div>'+
                  '   <a class="data-role-style-new" data-role="button" data-icon="carat-r" data-iconpos="right" href="listingdetail.html?listing_id=' + listing.S_ID + '&c_id=' + category + '&sub_id=' + sub_id + '&region=' + region + '" data-transition="slide">'+
                  '     <div class="category-listings-title">'+
                  '       <p>' + listing.S_Title + '</p>'+
                  '     </div>'+
                  '   </a>'+
                  ' </div>'+
                  '</div>').trigger("create");
        });
        loadtheme(region);
      } else {
        $('.ui-page-active #listing').empty();
        $('.ui-page-active #listing').append('<div id="listing-items"></div>').trigger('create');
        $('.ui-page-active #listing-items').append('<div class="category">'+
                ' <div>'+
                '   <div class="main-category-title">'+
                '     <p>' + data.category_name + '</p>'+
                '   </div>'+
                ' </div>'+
                '</div>').trigger("create");
        $.each(data.details, function (index, listing) {
          $('.ui-page-active #listing-items').append('<div class="category">'+
                  ' <div>'+
                  '   <div class="thumbnail">'+
                  '     <a href="listingdetail.html?listing_id=' + listing.BL_ID + '&c_id=' + category + '&sub_id=' + sub_id + '&region=' + region + '" data-transition="slide">'+
                  '       <img src="' + basepath + ((listing.BL_Mobile_Header_Image && listing.BL_Mobile_Header_Image != "") ? "images/DB/" + listing.BL_Mobile_Header_Image : "images/Listing-NoPhoto.jpg") + '" alt="' + listing.BL_Listing_Title + '" />'+
                  '     </a>'+
                  '   </div>'+
                  '   <a class="data-role-style-new" data-role="button" data-icon="carat-r" data-iconpos="right" href="listingdetail.html?listing_id=' + listing.BL_ID + '&c_id=' + category + '&sub_id=' + sub_id + '&region=' + region + '" data-transition="slide">'+
                  '     <div class="category-listings-title">'+
                  '       <p>' + listing.BL_Listing_Title + '</p>'+
                  '     </div>'+
                  '   </a>'+
                  ' </div>'+
                  '</div>').trigger("create");
        });
        loadtheme(region);
      }
    } else {
      $('.ui-page-active #listing').empty();
      $('.ui-page-active #listing').append('<div id="listing-items"></div>').trigger('create');
      $('.ui-page-active #listing-items').append('<div id="noRecordFound">An unknown error occurred. Try again later.</div>').trigger('create');
    }
  }, 'json');
}
/*---------------------------------------------------------------------------------------------------------*/
/* Listing/Event Detail Page
 -----------------------------------------------------------------------------------------------------------*/
$(document).delegate("#listingdetails", "pageshow", function () {
  $.blockUI({
    message: '<img src="' + basepath + 'mobile/img/ajaxloader.gif" alt="Loading..." />',
    css: {
      border: 'none',
      backgroundColor: 'transparent'
    },
    applyPlatformOpacityRules: false
  });
  var region = getParameterByName('region');
  loadLogo(region);
  var sub_id = getParameterByName('sub_id');
  var listing_id = getParameterByName('listing_id');
  listingdetail(listing_id, sub_id, region);
});
function listingdetail(listing_id, sub_id, region) {
  $.post('listingdetail.php', {
    listing_id: listing_id,
    sub_id: sub_id,
    region: region
  }, function (data) {
    if (data.error == false) {
      if (data.events == true)
      {
        $('.ui-page-active #listingdetail').empty();
        $('.ui-page-active #listingdetail').append('<div id="bl-detail-items"></div>').trigger('create');
        var listing = data.details;
        $('.ui-page-active #bl-detail-items').empty();
        if (listing.startdate != listing.enddate)
        {
          $('.ui-page-active #bl-detail-items').append('<div class="listing-title-detail"><div class="listing-title">' + listing.Title + '</div><div class="listing-address">' + listing.startdate + ' to ' + listing.enddate + '</div></div>').trigger("create");
        } else
        {
          $('.ui-page-active #bl-detail-items').append('<div class="listing-title-detail"><div class="listing-title">' + listing.Title + '</div><div class="listing-address">' + listing.startdate + '</div></div>').trigger("create");
        }
        $('.ui-page-active #bl-detail-items').append('<div class="listing-description event-des-style">' + listing.Content + '</div>').trigger("create");
        $('.ui-page-active #bl-detail-items').append('<div id="event-detail-items" class="event-detail-item-container"></div>').trigger('create');
        if (listing.EventStartTime != '00:00:00' || listing.EventEndTime != '00:00:00')
        {
          var event_time_detail = '';
          if (listing.EventTimeAllDay == 1)
          {
            event_time_detail = 'All day ';
          } else
          {
            if (listing.EventStartTime == 'dust')
            {
              event_time_detail = ' Dusk ';
            } else
            {
              if (listing.EventStartTime == '00:00:00')
              {

              } else {
                event_time_detail = listing.EventStartTime;
              }
            }
            if (listing.EventEndTime != '00:00:00')
            {
              event_time_detail = event_time_detail + ' to ';
              if (listing.EventEndTime == 'dust')
              {
                event_time_detail = event_time_detail + ' Dusk ';
              } else {
                event_time_detail = event_time_detail + listing.EventEndTime;
              }
            }
          }
          $('.ui-page-active #event-detail-items').append('<div class="event-items-content-container"><div class="event-detail-heading">Time</div><div class="event-detail-content">' + event_time_detail + '</div></div>').trigger('create');
        }
        $('.ui-page-active #event-detail-items').append('<div class="event-items-content-container"><div class="event-detail-heading">Community</div><div class="event-detail-content">' + data.Event_R_Name + '</div></div>').trigger('create');
        if (data.weekdays != null && data.weekdays != '')
        {
          $('.ui-page-active #event-detail-items').append('<div class="event-items-content-container"><div class="event-detail-heading">Recuring</div><div class="event-detail-content">' + data.weekdays + '</div></div>').trigger('create');
        }
        if (listing.Organization != '' || listing.OrganizationID > 0)
        {
          var Organization_name = '';
          if (listing.OrganizationID > 0)
          {
            Organization_name = listing.EO_Name;
          } else
          {
            Organization_name = listing.Organization;
          }
          $('.ui-page-active #event-detail-items').append('<div class="event-items-content-container"><div class="event-detail-heading">Organization</div><div class="event-detail-content">' + Organization_name + '</div></div>').trigger('create');
        }
        if (listing.LocationID > 0)
        {
          $('.ui-page-active #event-detail-items').append('<div class="event-items-content-container"><div class="event-detail-heading">Location</div><div class="event-detail-content">' + listing.EL_Name + '</div></div>').trigger('create');
          if (listing.EL_Street != '')
          {
            $('.ui-page-active #event-detail-items').append('<div class="event-items-content-container"><div class="event-detail-heading">Address</div><div class="event-detail-content">' + listing.EL_Street + ',' + listing.EL_Town + '</div></div>').trigger('create');
          }
        } else if (listing.LocationList != '' && listing.LocationList != 'Alternate Location')
        {
          if (listing.LocationList != '')
          {
            $('.ui-page-active #event-detail-items').append('<div class="event-items-content-container"><div class="event-detail-heading">Location</div><div class="event-detail-content">' + listing.LocationList + '</div></div>').trigger('create');
          }
          if (listing.StreetAddress != '')
          {
            $('.ui-page-active #event-detail-items').append('<div class="event-items-content-container"><div class="event-detail-heading">Address</div><div class="event-detail-content">' + listing.StreetAddress + '</div></div>').trigger('create');
          }
        }
        if (listing.ContactName != '')
        {
          $('.ui-page-active #event-detail-items').append('<div class="event-items-content-container"><div class="event-detail-heading">Contact Person</div><div class="event-detail-content">' + listing.ContactName + '</div></div>').trigger('create');
        }
        if (listing.ContactPhone != '')
        {
          $('.ui-page-active #event-detail-items').append('<div class="event-items-content-container"><div class="event-detail-heading">Phone</div><div class="event-detail-content">' + listing.ContactPhone + '</div></div>').trigger('create');
        }
        if (listing.ContactPhoneTollFree != '')
        {
          $('.ui-page-active #event-detail-items').append('<div class="event-items-content-container"><div class="event-detail-heading">Additional Phone</div><div class="event-detail-content">' + listing.ContactPhoneTollFree + '</div></div>').trigger('create');
        }
        if (listing.Email != '')
        {
          $('.ui-page-active #event-detail-items').append('<div class="event-items-content-container"><div class="event-detail-heading">Email</div><div class="event-detail-content"><a class="email-to" href="mailto:' + listing.Email + '" ?>' + listing.Email + '</a></div></div>').trigger('create');
        }
        if (listing.WebSiteLink != '')
        {
          $('.ui-page-active #event-detail-items').append('<div class="event-items-content-container"><div class="event-detail-heading">Website</div><div class="event-detail-content"><a class="email-to" href="http://' + listing.WebSiteLink + '" target="_blank"?>' + listing.WebSiteLink + '</a></div></div>').trigger('create');
        }
        loadtheme(region);
      } else if (data.story == true) {
        $('.ui-page-active #listingdetail').empty();
        $('.ui-page-active #listingdetail').append('<div id="bl-detail-items"></div>').trigger('create');
        var listing = data.details;
        $('.ui-page-active #bl-detail-items').empty();
        $('.ui-page-active #bl-detail-items').append('<div class="listing-title-detail"><div class="listing-title">' + listing.S_Title + ' in ' + listing.R_Name + '</div><div class="listing-address">Author: ' + listing.S_Author + '</div></div>').trigger("create");
        $.each(data.storycontent, function (index, storycontent) {
          $('.ui-page-active #bl-detail-items').append('<div class="main_image"><img src="' + basepath + ((storycontent.CP_Photo_Mobile != "") ? "images/DB/" + storycontent.CP_Photo_Mobile : "images/Listing-NoPhoto.jpg") + '" alt="' + storycontent.S_Title + '" /></div>').trigger("create");
          $('.ui-page-active #bl-detail-items').append('<div class="listing-title-detail story-title-detail"><div class="listing-title story-title">' + storycontent.CP_Title + '</div></div>').trigger("create");
          $('.ui-page-active #bl-detail-items').append('<div class="listing-description story-description">' + storycontent.CP_Description + '</div>').trigger("create");
        });
        loadtheme(region);
      } else {
        $('.ui-page-active #listingdetail').empty();
        $('.ui-page-active #listingdetail').append('<div id="bl-detail-items"></div>').trigger('create');
        var downloads = data.downloads;
        var listing = data.details;
        $('.ui-page-active #bl-detail-items').empty();
        $('.ui-page-active #bl-detail-items').append('<div class="main_image"><img src="' + basepath + ((listing.BL_Mobile_Header_Image != "") ? "images/DB/" + listing.BL_Mobile_Header_Image : "images/Listing-NoPhoto.jpg") + '" alt="' + listing.BL_Listing_Title + '" /></div>').trigger("create");
        $('.ui-page-active #bl-detail-items').append('<div class="listing-title-detail"><div class="listing-title">' + listing.BL_Listing_Title + '</div><div class="listing-address">' + (listing.BL_Town != '' ? listing.BL_Town + ', ' : '') + (listing.BL_Province != '' ? listing.BL_Province + ', ' : '') + 'Canada' + '</div></div>').trigger("create");
        $('.ui-page-active #bl-detail-items').append('<div class="listing-description">' + listing.BL_Description + '</div>').trigger("create");
        if (listing.BL_Trip_Advisor) {
          $('.ui-page-active #bl-detail-items').append('<div class="listing-trip-advisor"></div>').trigger("create");
          $('<iframe>', {
            src: basepath + '/mobile/tripadvisor.php?listing_id=' + listing.BL_ID,
            id: 'myFrame',
            frameborder: 0,
            scrolling: 'no',
            width: '100%'
          }).appendTo('.ui-page-active #bl-detail-items .listing-trip-advisor');
        }
        var gallery = data.gallery;
        if (gallery.length > 0) {
          $('.ui-page-active #bl-detail-items').append('<div class="gallery"></div>');
          $.each(gallery, function (index, photo) {
            $('.ui-page-active #bl-detail-items .gallery').append('<img src="' + basepath + 'images/DB/' + photo.BFP_Photo_710X440_Mobile + '" alt="' + photo.BFP_Title + '" width="100%" />');
          });
        }
        //social media
        if (listing.BS_Enabled > 0) {
          $('.ui-page-active #bl-detail-items').append('<div class="social-media"></div>');
          //fb link
          if(listing.BS_FB_Link && listing.BS_FB_Link != '') {
            $('.ui-page-active #bl-detail-items .social-media').append('<a target="_blank" href="http://'+ listing.BS_FB_Link.replace(/http:\/\/|https\/\/:/gi, '') +'"><img src="' + basepath + 'images/facebook.png" alt="Facebook" /></a>').trigger('create');
          }
          //instagram link
          if(listing.BS_I_Link && listing.BS_I_Link != '') {
            $('.ui-page-active #bl-detail-items .social-media').append('<a target="_blank" href="http://'+ listing.BS_I_Link.replace(/http:\/\/|https\/\/:/gi, '') +'"><img src="' + basepath + 'images/instagram.png" alt="Facebook" /></a>').trigger('create');
          }
          //twitter link
          if(listing.BS_T_Link && listing.BS_T_Link != '') {
            $('.ui-page-active #bl-detail-items .social-media').append('<a target="_blank" href="http://'+ listing.BS_T_Link.replace(/http:\/\/|https\/\/:/gi, '') +'"><img src="' + basepath + 'images/twitter.png" alt="Facebook" /></a>').trigger('create');
          }
        }
        $('.ui-page-active #bl-detail-items').append('<div class="listing-detail-address"></div>');
        $('.ui-page-active #bl-detail-items .listing-detail-address').append('<div class="listing-info-wrapper"></div>');
        if (listing.BL_Street || listing.BL_Town || listing.BL_Province || listing.BL_PostalCode) {
          $('.ui-page-active #bl-detail-items .listing-detail-address .listing-info-wrapper').append('<div class="listing-info"><div class="listing-detail-add-icon"><img src="' + basepath + 'mobile/img/location.png" alt=""></div><div class="address-heading">' + listing.BL_Street + ', ' + listing.BL_Town + ' ' + listing.BL_Province + ' ' + listing.BL_PostalCode + '</div>');
        }
        if (listing.BL_Phone) {
          $('.ui-page-active #bl-detail-items .listing-detail-address .listing-info-wrapper').append('<div class="listing-info"><div class="listing-detail-add-icon"><img src="' + basepath + 'mobile/img/Listing-Phone.png" alt=""></div><div class="address-heading">' + listing.BL_Phone + '</div>');
        }
        if (listing.BL_Website) {
          $('.ui-page-active #bl-detail-items .listing-detail-address .listing-info-wrapper').append('<div class="listing-info"><div class="listing-detail-add-icon"><img src="' + basepath + 'mobile/img/Listing-Website.png" alt=""></div><div class="address-heading"><a href="' + listing.BL_Website + '" target="_blank">Visit Website</a></div>');
        }
        if (listing.BL_Email) {
          $('.ui-page-active #bl-detail-items .listing-detail-address .listing-info-wrapper').append('<div class="listing-info"><div class="listing-detail-add-icon"><img src="' + basepath + 'mobile/img/Listing-Email.png" alt=""></div><div class="address-heading"><a href="' + listing.BL_Email + '">Send Email</a></div>');
        }
        if (listing.BL_Hours_Disabled == '0') {
          if (listing.BL_Hours_Appointment || listing.BL_Hour_Mon_From != '00:00:00' || listing.BL_Hour_Tue_From != '00:00:00' || listing.BL_Hour_Wed_From != '00:00:00' || listing.BL_Hour_Thu_From || listing.BL_Hour_Fri_From != '00:00:00' || listing.BL_Hour_Sat_From != '00:00:00' || listing.BL_Hour_Sun_From != '00:00:00') {
            $('.ui-page-active #bl-detail-items .listing-detail-address .listing-info-wrapper').append('<div class="listing-info"><div class="listing-detail-add-icon"><img src="' + basepath + 'mobile/img/Listing-Hours.png" alt=""></div><div class="address-heading view-hours accordion view-acc"><a onclick="show_hide()">View Hours</a></div>');
            $('.ui-page-active #bl-detail-items .listing-detail-address .view-hours').append('<div class="view-hours-timing"></div>');
            if (listing.BL_Hour_Mon_From != "00:00:00") {
              $('.ui-page-active .view-hours-timing').append('<div class="day-heading">Monday</div><div class="day-timing">' + formatTime(listing.BL_Hour_Mon_From, listing.BL_Hour_Mon_To) + '</div>');
            }
            if (listing.BL_Hour_Tue_From != '00:00:00') {
              $('.ui-page-active .view-hours-timing').append('<div class="day-heading">Tuesday</div><div class="day-timing">' + formatTime(listing.BL_Hour_Mon_From, listing.BL_Hour_Mon_To) + '</div>');
            }
            if (listing.BL_Hour_Wed_From != '00:00:00') {
              $('.ui-page-active .view-hours-timing').append('<div class="day-heading">Wednesday</div><div class="day-timing">' + formatTime(listing.BL_Hour_Mon_From, listing.BL_Hour_Mon_To) + '</div>');
            }
            if (listing.BL_Hour_Thu_From != '00:00:00') {
              $('.ui-page-active .view-hours-timing').append('<div class="day-heading">Thursday</div><div class="day-timing">' + formatTime(listing.BL_Hour_Mon_From, listing.BL_Hour_Mon_To) + '</div>');
            }
            if (listing.BL_Hour_Fri_From != '00:00:00') {
              $('.ui-page-active .view-hours-timing').append('<div class="day-heading">Friday</div><div class="day-timing">' + formatTime(listing.BL_Hour_Mon_From, listing.BL_Hour_Mon_To) + '</div>');
            }
            if (listing.BL_Hour_Sat_From != '00:00:00') {
              $('.ui-page-active  .view-hours-timing').append('<div class="day-heading">Saturday</div><div class="day-timing">' + formatTime(listing.BL_Hour_Mon_From, listing.BL_Hour_Mon_To) + '</div>');
            }
            if (listing.BL_Hour_Sun_From != '00:00:00') {
              $('.ui-page-active  .view-hours-timing').append('<div class="day-heading">Sunday</div><div class="day-timing">' + formatTime(listing.BL_Hour_Mon_From, listing.BL_Hour_Mon_To) + '</div>');
            }
          }
        }
        var geo_code_address = '';
        if (listing.BL_Street) {
          geo_code_address = listing.BL_Street;
        }
        if (listing.BL_Town) {
          geo_code_address += ', ' + listing.BL_Town;
        }
        if (listing.BL_Province) {
          geo_code_address += ', ' + listing.BL_Province;
        }
        var lat = -34.397;
        var lon = 150.644;
        var listing_title = listing.BL_Listing_Title;
        if((listing.BL_Lat != '' && listing.BL_Long != '') || geo_code_address != '') {
          lat = listing.BL_Lat;
          lon = listing.BL_Long;
          $('.ui-page-active #bl-detail-items').append('<div id="map_canvas"  style="width:100%;height:300px;margin-bottom:10px;"></div>');
          initialize_map(listing_title, geo_code_address, lat, lon);
        }
        loadtheme(region);
      }
    } else {
      $('.ui-page-active #listingdetail').empty();
      $('.ui-page-active #listingdetail').append('<div id="bl-detail-items"></div>').trigger('create');
      $('.ui-page-active #bl-detail-items').append('<div id="noRecordFound">An unknown error occurred. Try again later.</div>').trigger('create');
    }
  }, 'json');
}
//$(function () {
//    $(".accordion").accordion({
//        collapsible: true,
//        active: false
//    });
//});
/*---------------------------------------------------------------------------------------------------------*/
/* Load Search content
 -----------------------------------------------------------------------------------------------------------*/
$(document).delegate("#searching", "pageshow", function () {
  $.blockUI({
    message: '<img src="' + basepath + 'mobile/img/ajaxloader.gif" alt="Loading..." />',
    css: {
      border: 'none',
      backgroundColor: 'transparent'
    },
    applyPlatformOpacityRules: false
  });
  var query = getParameterByName('query');
  var region = getParameterByName('region');
  loadLogo(region);
  search(query, region);
});
function search(query, region) {
  $('.ui-page-active .search-text').val('');
  $('.ui-page-active .search-text').val(query);
  $('.ui-page-active #search-result').empty();

  $.post('search.php', {
    query: query,
    region: region
  }, function (data) {
//      console.log(data);
    if (data.error == false) {
      if (data != '' && data.details != '') {
        $('.ui-page-active #search-result').empty();
        $('.ui-page-active #search-result').append('<div id="search-items"></div>').trigger('create');
        $.each(data.details, function (index, listing) {
          $('.ui-page-active #search-items').append('<div class="category">' +
                  ' <div>' +
                  '   <div class="thumbnail">' +
                  '     <a href="listingdetail.html?listing_id=' + listing.BL_ID + '&c_id=0&sub_id=0&region=' + region + '&query=' + query + '" data-transition="slide">' +
                  '       <img src="' + basepath + ((listing.BL_Mobile_Header_Image && listing.BL_Mobile_Header_Image != "") ? "images/DB/" + listing.BL_Mobile_Header_Image : "images/Listing-NoPhoto.jpg") + '" alt="' + listing.BL_Listing_Title + '" />' +
                  '     </a>' +
                  '   </div>' +
                  '   <a href="listingdetail.html?listing_id=' + listing.BL_ID + '&c_id=0&sub_id=0&region=' + region + '&query=' + query + '" data-transition="slide">' +
                  '     <div class="category-title">' +
                  '       <p>' + listing.BL_Listing_Title + '</p>' +
                  '     </div>' +
                  '     <div class="arrow-right"><img src="' + basepath + 'mobile/img/arrow-right.png"></div>' +
                  '   </a>' +
                  ' </div>' +
                  '</div>').trigger("create");
        });
      } else {
        $('.ui-page-active #search-result').empty();
        $('.ui-page-active #search-result').append('<div id="search-items"></div>').trigger('create');
        $('.ui-page-active #search-items').append('<div id="noRecordFound">Your search request found no results. Please try again.</div>').trigger('create');
      }
    } else {
      $('.ui-page-active #search-result').empty();
      $('.ui-page-active #search-result').append('<div id="search-items"></div>').trigger('create');
      $('.ui-page-active #search-items').append('<div id="noRecordFound">An unknown error occurred. Try again later.</div>').trigger('create');
    }
  }, 'json');
}
/*---------------------------------------------------------------------------------------------------------*/
/* Map Page does not require
 -----------------------------------------------------------------------------------------------------------*/
var geocoder;
function initialize_map(title, geo_code_address, lat, lon) {
  var address = geo_code_address;
  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(lat, lon);
  var myOptions = {
    zoom: 14,
    center: latlng,
    mapTypeControl: true,
    mapTypeControlOptions: {
      style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
    },
    navigationControl: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

  if (geo_code_address != '') {
    if (geocoder) {
      geocoder.geocode({
        'address': address
      }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
            map.setCenter(results[0].geometry.location);

            var infowindow = new google.maps.InfoWindow(
                    {
                      content:
                              '<b>' + title + '</b><br>' + address,
                      size: new google.maps.Size(150, 50)
                    });

            var marker = new google.maps.Marker({
              position: results[0].geometry.location,
              map: map,
              title: address
            });
            google.maps.event.addListener(marker, 'click', function () {
              infowindow.open(map, marker);
            });

          } else {
            alert("No results found");
          }
        }
        //        else {
        //          alert("Geocode was not successful for the following reason: " + status);
        //        }
      });
    }
  } 
  else {
    var infowindow = new google.maps.InfoWindow(
            {
              content:
                      '<b>' + title + '</b><br>' + address,
              size: new google.maps.Size(150, 50)
            });

    var marker = new google.maps.Marker({
      position: latlng,
      map: map,
      title: address
    });
    google.maps.event.addListener(marker, 'click', function () {
      infowindow.open(map, marker);
    });

  }
}
function validateForm() {
  var region = getParameterByName('region');
  var search = $('.ui-page-active .search-text').val().length;
  if (search < 3) {
    alert("Search must be at least three characters");
    return false;
  } else {
    $('.active_region').val(region);
    return true;
  }
}

/* Load Mobile Theme Start
 -----------------------------------------------------------------------------------------------------------*/
function loadtheme(region) {
  $.post('gettheme.php', {
    region: region
  }, function (data) {
    if (data.error == false) {
      $(".ul-navigation li a").css({"font-size": data.main_navigation_size, "color": data.main_navigation_color});
      $(".ul-navigation li a.active").css({"font-size": data.main_navigation_selected_size, "color": data.main_navigation_selected_color});
      $(".footer-urls ul li a").css({"font-size": data.footer_links_size, "color": data.footer_links_color});
      $(".footer-message p a").css({"font-size": data.footer_links_size, "color": data.footer_links_color});
      $(".footer-message p").css({"font-size": data.footer_disclaimer_size, "color": data.footer_disclaimer_color});
      $(".main_title p").css({"font-size": data.slider_title_size, "color": data.slider_title_color});
      $(".event-title,.event-title a p").css({"font-size": data.category_event_size, "color": data.category_event_color});
      $(".event-date").css({"font-size": data.category_event_date_size, "color": data.category_event_date_color});
      $(".index-category-title p").css({"font-size": data.thumbnail_category_title_size, "color": data.thumbnail_category_title_color});
      $(".main-category-title p").css({"font-size": data.category_header_size, "color": data.category_header_color});
      $(".category-listings-title p").css({"font-size": data.thumbnail_listing_size, "color": data.thumbnail_listing_color});
      $(".category-title p").css({"font-size": data.thumbnail_sub_category_size, "color": data.thumbnail_sub_category_color});
      $(".ui-footer").css({"background-color": data.footer_background_color + "!important", "float": "left"});
      $(".main-category-title").css({"background-color": data.category_header_bg + "!important"});
//            $(".footer-logos2").css({"background-color": data.footer_background_color + "!important" });  
//            $(".footer-bottom-urls").css({"background-color": data.footer_background_color + "!important" });  
//            $(".footer-message").css({"background-color": data.footer_background_color + "!important" });  
      if (data.home_des_bg_image != "")
      {
        $(".description").css({"background-image": 'url(' + basepath + "theme_icons/" + data.home_des_bg_image + ')'});
      } else
      {
        $(".description").css({"background-color": data.home_des_bg_color});
      }
      if (data.bar_texture_image != "")
      {
        $(".index-category-title").css({"background-image": 'url(' + basepath + "theme_icons/" + data.bar_texture_image + ')'});
      } else
      {
        $(".index-category-title").css({"background-color": data.bar_texture_color + "!important"});
      }
      if (data.listing_texture_image != "")
      {
        $(".category-listings-title").css({"background-image": 'url(' + basepath + "theme_icons/" + data.listing_texture_image + ')'});
        $(".event-detail-container").css({"background-image": 'url(' + basepath + "theme_icons/" + data.listing_texture_image + ')'});
      } else
      {
        $(".category-listings-title").css({"background-color": data.listing_texture_color + "!important"});
        $(".event-detail-container").css({"background-color": data.listing_texture_color + "!important"});
      }
      if (data.subcat_Texture_image != "")
      {
        $(".category-title").css({"background-image": 'url(' + basepath + "theme_icons/" + data.subcat_Texture_image + ')'});

      } else
      {
        $(".category-title").css({"background-color": data.subcat_Texture_color + "!important"});

      }
      $(".description p , description ul li").css({"font-size": data.main_page_body_copy_size, "color": data.main_page_body_copy_color, "line-height": data.homepage_description_space});
      $(".listing-title").css({"font-size": data.listing_title_font_size, "color": data.listing_title_font_color});
      $(".listing-address").css({"font-size": data.listing_location_text_size, "color": data.listing_location_text_color});
      $(".address-heading , .event-detail-heading , .event-detail-content").css({"font-size": data.listing_sub_nav_text_size, "color": data.listing_sub_nav_text_color});
      $(".listing-description , .listing-description p , .listing-description ul li").css({"font-size": data.listing_general_body_copy_size, "line-height": data.listing_general_body_copy_line_spacing, "color": data.listing_general_body_copy_color});
      $.each(data.Font_Family, function (index, Font_Name) {
        if (Font_Name.TOF_ID == data.footer_links_font) {
          $(".footer-urls ul li a").css({"font-family": Font_Name.TOF_Name});
          $(".footer-message p a").css({"font-family": Font_Name.TOF_Name});
        }
        if (Font_Name.TOF_ID == data.footer_disclaimer_font) {
          $(".footer-message p").css({"font-family": Font_Name.TOF_Name});
        }
        if (Font_Name.TOF_ID == data.category_event_font) {
          $(".event-title").css({"font-family": Font_Name.TOF_Name});
        }
        if (Font_Name.TOF_ID == data.category_event_date_font) {
          $(".event-date").css({"font-family": Font_Name.TOF_Name});
        }
        if (Font_Name.TOF_ID == data.main_navigation_font) {
          $(".ul-navigation li a").css({"font-family": Font_Name.TOF_Name});
        }
        if (Font_Name.TOF_ID == data.main_navigation_selected_font) {
          $(".ul-navigation li a.active").css({"font-family": Font_Name.TOF_Name});
        }
        if (Font_Name.TOF_ID == data.slider_title_font) {
          $(".main_title p").css({"font-family": Font_Name.TOF_Name});
        }
        if (Font_Name.TOF_ID == data.thumbnail_category_title_font) {
          $(".index-category-title p").css({"font-family": Font_Name.TOF_Name});
        }
        if (Font_Name.TOF_ID == data.category_header_font) {
          $(".main-category-title p").css({"font-family": Font_Name.TOF_Name});
        }
        if (Font_Name.TOF_ID == data.thumbnail_sub_category_font) {
          $(".category-title p").css({"font-family": Font_Name.TOF_Name});
        }
        if (Font_Name.TOF_ID == data.thumbnail_listing_font) {
          $(".category-listings-title p").css({"font-family": Font_Name.TOF_Name});
//                $(".event-title").css({"font-family": Font_Name.TOF_Name });
        }
        if (Font_Name.TOF_ID == data.main_page_body_copy_font) {
          $(".description p , description ul li").css({"font-family": Font_Name.TOF_Name});
        }
        if (Font_Name.TOF_ID == data.listing_title_font) {
          $(".listing-title").css({"font-family": Font_Name.TOF_Name});
        }
        if (Font_Name.TOF_ID == data.listing_location_text_font) {
          $(".listing-address").css({"font-family": Font_Name.TOF_Name});
        }
        if (Font_Name.TOF_ID == data.listing_sub_nav_text_font) {
          $(".address-heading , .event-detail-heading , .event-detail-content").css({"font-family": Font_Name.TOF_Name});
        }
        if (Font_Name.TOF_ID == data.listing_general_body_copy_font) {
          $(".listing-description , .listing-description p , .listing-description ul li").css({"font-family": Font_Name.TOF_Name});
        }
      });
    }
  }, 'json');
}
/* Load Mobile Theme End
 -----------------------------------------------------------------------------------------------------------*/
function formatTime(startTime, endTime) {
  if (startTime == '00:00:00' && endTime == '00:00:00') {
    return 'Closed';
  } else if (startTime == '00:00:01') {
    return 'By Appointment';
  } else if (startTime == '00:00:02') {
    return 'Open 24 Hours';
  } else {
    var open, close;
    var startSplit = startTime.split(':');
    var endSplit = endTime.split(':');
    if (startSplit[0] > 12) {
      open = (startSplit[0] - 12) + ':' + startSplit[1] + ' PM';
    } else {
      open = startSplit[0] + ':' + startSplit[1] + ' AM';
    }
    if (endSplit[0] > 12) {
      close = (endSplit[0] - 12) + ':' + endSplit[1] + ' PM';
    } else {
      close = endSplit[0] + ':' + endSplit[1] + ' AM';
    }

    return open + ' - ' + close;
  }
}
function show_hide()
{
  $('.ui-page-active .view-hours-timing').toggle();
}