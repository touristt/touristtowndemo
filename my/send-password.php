<?php
require_once '../include/config.inc.php';
require '../include/sendmail.inc.php';
$hash = md5(rand(0, 1000));
//////Email template 
$email = "SELECT * FROM tbl_Email_Templates WHERE ET_ID = 12";
$result_data = mysql_query($email);
$rowEmail = mysql_fetch_assoc($result_data);
$email_subject = $rowEmail['ET_Subject'];
$email_heading = $rowEmail['ET_Name'];
$email_body = $rowEmail['ET_Template'];
$email_footer = $rowEmail['ET_Footer'];

$new_Val_hef = '<a href="http://my.touristtowndemo.com/?logop=rg_p&token=' . $hash . '?>" target="_blank">Please click this link to verify the change.</a>';
$old_Val_hef = '<a href="http://touristtowndemo.com/" target="_blank">Please click this link to verify the change.</a>';

$message_body = str_replace($old_Val_hef, $new_Val_hef, $rowEmail['ET_Template']);
$title = "Password Reset";
ob_start();
include '../include/email-template/mytouristtown-pw-reset-html.php';
$html = ob_get_contents();
ob_clean();
if (isset($_SESSION['B_Email']) && $_SESSION['B_Email'] !== '') {
    send_mail($html, $_SESSION['B_Email'], $email_subject, false);
}
$sql = "UPDATE tbl_Business SET B_hash = '" . $hash . "' WHERE B_ID = '" . $_SESSION[B_ID] . "' LIMIT 1 ";
mysql_query($sql, $db);

unset($_SESSION['B_Email']);
unset($_SESSION['B_ID']);


require_once '../include/my/header.php';
?>
<table width="940" border="0" cellpadding="0" cellspacing="0" bgcolor="#676767" align="center">
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="SectionHeader">Reset Password</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table width="940" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
    <tr>
        <td bgcolor="#FFFFFF">
            <table width="800" border="0" cellspacing="20" cellpadding="0" align="center">
                <tr>
                    <td>
                        <table width="570" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <?php
                                if (isset($_SESSION['send_password_error']) && $_SESSION['send_password_error'] == 1) {
                                    ?>
                                    <td>There is no record of the email address that you have entered. Please contact Tourist Town to speak to a support representative. <br>
                                        &nbsp;<br>
                                        Free Phone Support: 519.378.3096<br>
                                        Email: <a href='mailto:info@touristtown.ca'>info@touristtown.ca</a><br>
                                        &nbsp;<br>Tourist Town is the web platform that supports your local Tourism website.
                                    </td>
                                    <?php
                                    unset($_SESSION['send_password_error']);
                                } elseif (isset($_SESSION['send_password_success']) && $_SESSION['send_password_success'] == 1) {
                                    ?>
                                    <td align="center">
                                        You have requested to change your password and will receive an email link shortly. If you do not receive an email, please check your spam box or contact us at <a href="mailto:info@touristtown.ca.">info@touristtown.ca.</a>
                                        <p>Tourist Town Team</p>
                                    </td>
                                    <?php
                                } else {
                                    header("Location: " . BUSINESS_DIR . "login.php");
                                    exit();
                                }
                                unset($_SESSION['send_password_success']);
                                ?>
                            </tr>

                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?php
require_once '../include/my/footer.php';
?>