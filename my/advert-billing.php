<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';

$BID = $_SESSION['BUSINESS_ID'];

if ($BID > 0) {
    
} else {
    header('Location: index.php');
}

require_once '../include/my/header.php';
?>
<div class="content-left advert">

    <?php require_once '../include/nav-B-advertisement.php'; ?>

    <div class="title-link">
        <div class="title">Invoices</div>
    </div>
    <?php
    $sql = "SELECT A_ID, A_Title FROM tbl_Advertisement WHERE A_B_ID = '" . encode_strings($BID, $db) . "' ORDER BY A_ID DESC";
    $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $gtotal = $tax = $discount3 = 0;
    ?>
    <div class="right">
        <?PHP
        while ($row = mysql_fetch_assoc($resultTMP)) {
            $billing = "SELECT AB_ID, AB_Invoice_Num, AB_Active_Date, AB_Total, AB_SubTotal, AB_Tax, AB_CoC_Dis FROM tbl_Advertisement_Billing WHERE AB_A_ID = '" . encode_strings($row['A_ID'], $db) . "' AND AB_Deleted = 0
			ORDER BY AB_ID DESC";
            $resBilling = mysql_query($billing, $db) or die("Invalid query: $billing -- " . mysql_error());
            $total_count = mysql_num_rows($resBilling);
            $billCounterHeader = 0;
            $count = 0;
            $gtotal = 0;
            $subtotal = 0;
            $tax = 0;
            $discount3 = 0;
            while ($rowBill = mysql_fetch_array($resBilling)) {
                if ($billCounterHeader == 0) {
                    ?>
                    <div class="content-sub-header"><?php echo $row['A_Title'] ?></div>
                    <div class="data-header"> 
                        <div class="data-column advert-billing">Invoice #</div>
                        <div class="data-column advert-billing1">Date Paid</div>
                        <div class="data-column advert-billing2">Discount</div>
                        <div class="data-column advert-billing2">Sub Total</div>
                        <div class="data-column advert-billing2">Tax</div>
                        <div class="data-column advert-billing2">Total</div>
                    </div>                             
                    <?php
                }
                $count++;
                $billCounterHeader++;
                $gtotal += $rowBill['AB_Total'];
                $subtotal += $rowBill['AB_SubTotal'];
                $tax += $rowBill['AB_Tax'];
                $discount3 += $rowBill['AB_CoC_Dis'];
                ?>                        
                <div class="data-content">
                    <div class="data-column advert-billing"><a class="support-nav-b" href="advert-bill.php?a_id=<?php echo $rowBill['AB_ID']; ?>">#<?php echo $rowBill['AB_Invoice_Num'] ?> - <?php echo $row['A_Title'] ?></a></div>
                    <div class="data-column advert-billing1"><?php echo $rowBill['AB_Active_Date'] ?></div>
                    <div class="data-column advert-billing2">$<?php echo $rowBill['AB_CoC_Dis'] ?></div>
                    <div class="data-column advert-billing2">$<?php echo $rowBill['AB_SubTotal'] ?></div>
                    <div class="data-column advert-billing2">$<?php echo $rowBill['AB_Tax'] ?></div>
                    <div class="data-column advert-billing2">$<?php echo $rowBill['AB_Total'] ?></div>
                </div>
                <?PHP
            }
        }
        ?>        
    </div>
</div>

<?PHP
require_once '../include/my/footer.php';
?>