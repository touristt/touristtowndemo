<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    
} else {
    header('Location: index.php');
}

require_once '../include/my/header.php';
?>

<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">My Page</div>
        <div class="link">Preview My Page</div>
    </div>

    <div class="left">
        <?PHP
        require '../include/nav-B-statistics.php';
        ?>
    </div>

    <div class="right">
        <div class="content-header">Statistics</div>
        <div class="data">
            <div class="content-sub-header padding-none">
                <div class="data-column">Year</div>
                <div class="data-column">Month</div>
                <div class="data-column">Listed</div>
                <div class="data-column">Searched</div>
                <div class="data-column">Viewed</div>
            </div>
            <?PHP
            $sql = "SELECT BLS_Listing_Display, BLS_Search_Display, BLS_Profile_Views, DATE_FORMAT(BLS_Month,'%Y') as daYear, DATE_FORMAT(BLS_Month,'%M') as daMonth 
                    FROM tbl_Business_Listing_Stats  
                    WHERE BLS_BL_ID = '" . encode_strings($BL_ID, $db) . "' 
                    AND  BLS_R_ID = '" . encode_strings('0', $db) . "' 
                    ORDER BY BLS_Month DESC";
            $resultCount = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($bizCount = mysql_fetch_assoc($resultCount)) {
                ?>
                <div class="data-content">
                    <div class="data-column"><?php echo $bizCount['daYear'] ?></div>
                    <div class="data-column"><?php echo $bizCount['daMonth'] ?></div>
                    <div class="data-column"><?php echo $bizCount['BLS_Listing_Display'] ?></div>
                    <div class="data-column"><?php echo $bizCount['BLS_Search_Display'] ?></div>
                    <div class="data-column"><?php echo $bizCount['BLS_Profile_Views'] ?></div>
                </div>
            <?PHP } ?>
        </div>
    </div>
</div>

<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>