<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/ranking.inc.php';
require_once '../include/adminFunctions.inc.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    $sql = "SELECT BL_ID, BL_Listing_Title, BL_Description, BL_Listing_Type, BL_Contact, BL_Phone, BL_Website, BL_Toll_Free, BL_Email, BL_Street, BL_Town, 
            BL_Province, BL_PostalCode, BL_Lat, BL_Long, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Description, BL_SEO_Keywords, 
            BL_Search_Words FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    // get regions and categories of this listing
    $sql_region = "SELECT BLC_M_C_ID, BLC_C_ID, BLCR_BLC_R_ID, RM_Parent FROM tbl_Business_Listing
                    LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                    LEFT JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID 
                    LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                    LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID
                    WHERE BLC_BL_ID = '$BL_ID'";
    $res_region = mysql_query($sql_region);
    $regions = array();
    $category = array();
    $subcategories = array();
    while ($r = mysql_fetch_assoc($res_region)) {
        // add region parent as well if available
        if ($r['RM_Parent'] != 0 && !in_array($r['RM_Parent'], $regions)) {
            $regions[] = $r['RM_Parent'];
        }
        // add region
        if ($r['BLCR_BLC_R_ID'] != '' && !in_array($r['BLCR_BLC_R_ID'], $regions)) {
            $regions[] = $r['BLCR_BLC_R_ID'];
        }
        if ($r['BLC_M_C_ID'] != '' && !in_array($r['BLC_M_C_ID'], $category)) {
            $category[] = $r['BLC_M_C_ID'];
        }
        if ($r['BLC_C_ID'] != '' && !in_array($r['BLC_C_ID'], $subcategories)) {
            $subcategories[] = $r['BLC_C_ID'];
        }
    }
} else {
    header('Location: index.php');
}

require_once '../include/my/header.php';
?>

<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">My Page</div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-B-mypage.php'; ?>
    </div>

    <div class="right">
        <div class="content-header">Rankings</div>
        <?php
        if ($rowListing['BL_Listing_Type'] == 1) {
            echo '<div class="form-inside-div border-none">There is no Ranking, Points and Notifications for free listings. To get Rankings and Points please upgrade your listing type.</div>';
        } else {
            $help_text = show_help_text('Rankings');
            if ($help_text != '') {
                echo '<div class="form-inside-div border-none">' . $help_text . '</div>';
            }
            ?>
            <div class="form-inside-div border-none">
                <!--Ranking Points-->
                <div class="content-sub-header ranking-header">
                    <div class="title">Ranking Points</div>
                </div>
                <div class="ranking-row">
                    <div class="ranking-title">My Page Points</div>
                    <div class="ranking-points"><?php echo calculate_listing_points($BL_ID, 'basic'); ?></div>
                </div>
                <div class="ranking-row">
                    <div class="ranking-title">Unused My Page Points</div>
                    <div class="ranking-points"><?php echo calculate_listing_points($BL_ID, 'available'); ?></div>
                </div>
                <div class="ranking-row">
                    <div class="ranking-title">My Add-Ons Points</div>
                    <div class="ranking-points"><?php echo calculate_listing_points($BL_ID, 'addon'); ?></div>
                </div>
                <div class="ranking-row">
                    <div class="ranking-title">Available Add-Ons Points</div>
                    <div class="ranking-points"><?php echo calculate_listing_points($BL_ID, 'available_addon'); ?></div>
                </div>
                <div class="ranking-row">
                    <div class="ranking-title">Total Points</div>
                    <div class="ranking-points"><?php echo calculate_listing_points($BL_ID, 'all'); ?></div>
                </div>
                <!--Ranking by Region-->
                <div class="content-sub-header ranking-header">
                    <div class="title">Ranking by Region</div>
                </div>
                <div class="ranking-row">
                    <form name="form" action="customer-rankings.php" method="GET" id="ranking-form">
                        <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>" />
                        <select name="region" onchange="$('#ranking-form').submit();">
                            <?php
                            foreach ($regions as $key => $r) {
                                if ($key == 0) {
                                    $default_region = $r;
                                }
                                $selected = ($_REQUEST['region'] == $r['R_ID']) ? ' selected' : '';
                                $sql = "SELECT R_ID, R_Name, R_Domain FROM tbl_Region WHERE R_ID = '$r' LIMIT 1";
                                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                $region = mysql_fetch_assoc($result);
                                print '<option value="' . $region['R_ID'] . '" ' . $selected . '>' . $region['R_Name'] . ' (' . $region['R_Domain'] . ')</option>';
                            }
                            ?>
                        </select>
                    </form>
                </div>
                <!--Overall Ranking-->
                <div class="content-sub-header ranking-header">
                    <div class="title">Site Ranking</div>
                </div>
                <?php
                if (isset($_REQUEST['region']) && $_REQUEST['region'] > 0) {
                    $ranking_region = $_REQUEST['region'];
                } else {
                    $ranking_region = $default_region;
                }
                $_SESSION['ranking_region'] = $ranking_region; // This session ranking ID to be used in right overview panel
                $region_rank = calculate_ranking($BL_ID, 'region', $ranking_region, $ranking_region);
                ?>
                <div class="ranking-row">
                    <div class="ranking-title">Overall Ranking</div>
                    <div class="ranking-points"><?php echo $region_rank['rank'] . ' of ' . $region_rank['total'] ?></div>
                </div>
                <?php
                $Add_on_check = 0;
                foreach ($category as $cat) {
                    ?>
                    <div class="content-sub-header ranking-header">
                        <div class="title">Category Ranking</div>
                    </div>
                    <?php
                    $sql_category = "SELECT C_Name FROM tbl_Category WHERE C_ID = '$cat'";
                    $res_category = mysql_query($sql_category);
                    $c = mysql_fetch_assoc($res_category);
                    $category_rank = calculate_ranking($BL_ID, 'category', $cat, $ranking_region);
                    ?>
                    <div class="ranking-row">
                        <div class="ranking-title">Category Ranking (<?php echo $c['C_Name'] ?>)</div>
                        <div class="ranking-points"><?php echo $category_rank['rank'] . ' of ' . $category_rank['total'] ?></div>
                    </div>
                    <?php
                    $i = 1;
                    foreach ($subcategories as $key => $subcat) {
                        $sql_subcat = "SELECT C_Name FROM tbl_Category WHERE C_ID = '$subcat' and C_Parent= '$cat'";
                        $res_subcat = mysql_query($sql_subcat);
                        $count_subcat = mysql_num_rows($res_subcat);
                        $s = mysql_fetch_assoc($res_subcat);
                        $subcat_rank = calculate_ranking($BL_ID, 'subcategory', $subcat, $ranking_region);
                        if ($count_subcat > 0) {
                            ?>
                            <div class="ranking-row">
                                <div class="ranking-title">Sub-Category <?php echo $i ?> Ranking (<?php echo $s['C_Name'] ?>)</div>
                                <div class="ranking-points"><?php echo $subcat_rank['rank'] ?> of <?php echo $subcat_rank['total'] ?></div>
                            </div>
                            <div class="ranking-row">
                                <div class="ranking-title">Page you appear within <?php echo $s['C_Name'] ?></div>
                                <div class="ranking-points"><?php echo ceil($subcat_rank['rank'] / 10) ?> of <?php echo ceil($subcat_rank['total'] / 10) ?></div>
                            </div>
                            <?php
                            $i++;
                        }
                    }
                    if ($Add_on_check == 0) {
                        ?>

                        <!--Available Points-->
                        <div class="content-sub-header ranking-header">
                            <div class="title">Available Add Ons Points</div>
                        </div>
                        <?PHP
                        $sql = "SELECT * FROM tbl_Feature";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_assoc($result)) {
                            $sql1 = "SELECT * FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = " . $row['F_ID'];
                            $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
                            $row1 = mysql_fetch_assoc($result1);
                            if ($row1['BLF_BL_ID'] != '') {
                                
                            } else {
                                ?>
                                <div class="ranking-row">
                                    <div class="ranking-title"><?php echo $row['F_Name'] ?> (<?php echo $row['F_Points'] ?> Points)</div>
                                    <div class="ranking-purchase"><a href="customer-feature-listing-store.php?f_id=<?php echo $row['F_ID'] ?>&bl_id=<?php echo $BL_ID ?>">Purchase</a></div>
                                    <div class="ranking-points"><?php echo calculate_listing_points($BL_ID, 'addon'); ?> of <?php echo calculate_total_points('addon'); ?></div>
                                </div>
                                <?php
                            }
                        }
                    }
                    ?>
                    <!--Overall Ranking-->
                    <div class="content-sub-header ranking-header">
                        <div class="title">Top Ranked Businesses in your Category (<?php echo $c['C_Name'] ?>)</div>
                    </div>
                    <?php
                    $get_parent = "SELECT R_Parent FROM tbl_Region WHERE R_ID = '$ranking_region'";
                    $parent_result = mysql_query($get_parent, $db) or die("Invalid query: $get_parent -- " . mysql_error());
                    $parent_row = mysql_fetch_assoc($parent_result);
                    if ($parent_row['R_Parent'] == 0) {
                        $sql_top = "SELECT BL_ID, BL_Listing_Title, BL_Name_SEO, BL_Points, R_Domain FROM tbl_Business_Listing 
                                    LEFT JOIN tbl_Business_Listing_Category on BLC_BL_ID= BL_ID
                                    INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID 
                                    LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                                    WHERE BLC_M_C_ID = '$cat'
                                    AND BLCR_BLC_R_ID IN (SELECT R_ID from tbl_Region LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID where RM_Parent = '" . encode_strings($ranking_region, $db) . "')
                                    GROUP BY BL_Listing_Title
                                    ORDER BY BL_Points DESC, BL_Listing_Title
                                    LIMIT 10";
                    } else {
                        $sql_top = "SELECT BL_ID, BL_Listing_Title, BL_Name_SEO, BL_Points, R_Domain FROM tbl_Business_Listing 
                                    LEFT JOIN tbl_Business_Listing_Category on BLC_BL_ID= BL_ID
                                    INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID 
                                    LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                                    WHERE BLC_M_C_ID = '$cat'
                                    AND BLCR_BLC_R_ID = '" . encode_strings($ranking_region, $db) . "'
                                    GROUP BY BL_Listing_Title
                                    ORDER BY BL_Points DESC, BL_Listing_Title
                                    LIMIT 10";
                    }
                    $res_top = mysql_query($sql_top);
                    $i = 1;
                    while ($top = mysql_fetch_assoc($res_top)) {
                        print '<div class="ranking-row">
                                <div class="ranking-title">
                                <span class="sr_number">' . $i . ')</span>
                                <a href="http://' . $top['R_Domain'] . '/profile/' . $top['BL_Name_SEO'] . '/' . $top['BL_ID'] . '/" target="_blank">' . $top['BL_Listing_Title'] . '</a></div>
                                <div class="ranking-points">' . $top['BL_Points'] . ' Points</div>
                                </div>';
                        $i++;
                    }
                    $Add_on_check++;
                }
                ?>

            </div>
        <?php } ?>
    </div>
</div>
<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>