<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    $sql = "SELECT BL_ID, BL_Listing_Title, BL_Description, BL_Listing_Type, BL_Contact, BL_Phone, BL_Website, BL_Toll_Free, BL_Email, BL_Street, BL_Town, 
            BL_Province, BL_PostalCode, BL_Lat, BL_Long, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Description, BL_SEO_Keywords, 
            BL_Search_Words FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $sqlLS = "SELECT * FROM tbl_Business_Listing_Season WHERE BLS_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $resultLS = mysql_query($sqlLS, $db) or die("Invalid query: $sqlLS -- " . mysql_error());
} else {
    header('Location: index.php');
}
if ($_POST['op'] == 'save') {
    $delete = "DELETE FROM tbl_Business_Listing_Season WHERE BLS_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $res = mysql_query($delete, $db);
    foreach ($_POST['season'] as $value) {
        $sql = "INSERT tbl_Business_Listing_Season SET BLS_S_ID = '$value', 
                BLS_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $res1 = mysql_query($sql, $db);
    }
    if ($res) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Seasons', '', 'Add', 'user admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: customer-listing-seasons.php?bl_id=" . $BL_ID);
    exit();
}
require_once '../include/my/header.php';
?>
<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">My Page</div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div>
        <div class="instruction">
            Fields with this background<span></span>will show on free listings profile.
        </div>
    </div>

    <div class="left">
        <?PHP
        require '../include/nav-B-mypage.php';
        ?>
    </div>

    <div class="right">

        <div class="content-header">
            <div class="title">Seasons</div>
            <div class="link">
            </div>
        </div>

        <form name="form1" method="post" action="">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="op" value="save">
            <?PHP
            $listingSeasons = array();
            while ($rowLS = mysql_fetch_array($resultLS)) {
                $listingSeasons[] = $rowLS['BLS_S_ID'];
            }
            $sqlSeasons = "SELECT * FROM tbl_Seasons";
            $resultSeasons = mysql_query($sqlSeasons, $db) or die("Invalid query: $sqlSeasons -- " . mysql_error());
            while ($rowSeasons = mysql_fetch_array($resultSeasons)) {
                ?>
                <div class="form-inside-div">
                    <input name="season[<?php echo $rowSeasons['S_ID'] ?>]" type="checkbox" value="<?php echo $rowSeasons['S_ID'] ?>" <?php echo in_array($rowSeasons['S_ID'], $listingSeasons) ? 'checked' : '' ?> /><?php echo $rowSeasons['S_Name'] ?>
                </div>
            <?php }
            ?>
            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" value="Save Now">
                </div>
            </div>
        </form>

    </div>
</div>

<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>