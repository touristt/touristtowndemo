<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];
$points_taken = 0;
$Coupons_direction = $_REQUEST['coupons'];
$bfc_id = $_REQUEST['bfc_id'];
if ($BL_ID > 0) {
    $sql = "SELECT BL_ID, BL_Listing_Title, BL_Description, BL_Listing_Type, BL_Contact, BL_Phone, BL_Website, BL_Toll_Free, BL_Email, BL_Street, BL_Town, 
            BL_Province, BL_PostalCode, BL_Lat, BL_Long, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Description, BL_SEO_Keywords, 
            BL_Search_Words FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}


if ($_POST['op'] == 'save') {
    if ($_POST['counter'] > 0) {
        $sql = "UPDATE tbl_Business_Social SET 
                BS_FB_Link = '" . encode_strings($_POST['fb_link'], $db) . "',
                BS_T_Link = '" . encode_strings($_POST['t_link'], $db) . "',
                BS_I_Link = '" . encode_strings($_POST['i_link'], $db) . "'
                WHERE BS_BL_ID = '$BL_ID'";
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Social Media','','Update','user admin');
    } else {
        $sql = "INSERT INTO tbl_Business_Social(BS_BL_ID, BS_FB_Link, BS_T_Link, BS_I_Link) VALUES('" . encode_strings($BL_ID, $db) . "', '" . encode_strings($_POST['fb_link'], $db) . "', '" . encode_strings($_POST['t_link'], $db) . "', '" . encode_strings($_POST['i_link'], $db) . "')";
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Social Media','','Add','user admin');
    }
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }

    //update points only for listing
    update_pointsin_business_tbl($BL_ID);
    if (isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true) {
        header("Location: /listings-preview.php?bl_id=" . $BL_ID);
    } else if (isset($Coupons_direction) && $Coupons_direction == 'editcoupon') {
        header("Location:customer-feature-add-coupons.php?bl_id=" . $BL_ID . "&bfc_id=" . $bfc_id . "&re_dir=1");
    } else {
        header("Location: customer-social-media.php?bl_id=" . $BL_ID);
    }
    exit();
}

require_once '../include/my/header.php';
?>

<div class="content-left">

<?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">Add Ons</div>
        <div class="link">
<?PHP
require_once('preview-link.php');
?>
        </div>
        <div class="instruction">
            Fields with this background<span></span>will show on free listings profile.
        </div>
    </div>

    <div class="left">
<?PHP
require '../include/nav-B-mypage.php';
?>
    </div>

    <div class="right">

        <form name="form1" method="post" action="">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="coupons" value="<?php echo $Coupons_direction; ?>">
            <input type="hidden" name="go_to_preview" value="<?php echo $_REQUEST['go_to_preview'] ?>">
            <input type="hidden" name="bfc_id" value="<?php echo $bfc_id; ?>">
            <div class="content-header">
                <div class="title">Social Media</div>
                <div class="link">
<?php
$SM = show_field_points('Hours');
if ($rowsSM['BS_FB_Link'] != '' || $rowsSM['BS_T_Link'] != '' || $rowsSM['BS_Y_Link'] || $rowsSM['BS_I_Link']) {
    echo '<div class="points-com des-heading-point">' . $SM . ' pts</div>';
    $points_taken = $SM;
} else {
    echo '<div class="points-uncom des-heading-point">' . $SM . ' pts</div>';
}
?>
                </div>
            </div>

<?php
$help_text = show_help_text('Social Media');
if ($help_text != '') {
    echo '<div class="form-inside-div" style="border-bottom:3px solid #eeeeee;">' . $help_text . '</div>';
}
?>

            <div class="form-inside-div">
                <label>Facebook Link</label>
                <div class="form-data">
                    <input name="fb_link" type="text" value="<?php echo $rowsSM['BS_FB_Link'] ?>" size="50" />
                </div>
            </div>

            <div class="form-inside-div">
                <label>Twitter Link</label>
                <div class="form-data">
                    <input name="t_link" type="text"  value="<?php echo $rowsSM['BS_T_Link'] ?>" size="50" />
                </div>
            </div>

            <div class="form-inside-div">
                <label>Instagram Link</label>
                <div class="form-data">
                    <input name="i_link" type="text"  value="<?php echo $rowsSM['BS_I_Link'] ?>" size="50" />
                </div>
            </div>

            <div class="form-inside-div">
                <div class="button">
                    <input type="hidden" name="counter" value="<?php echo $countSM; ?>">
                    <input type="submit" name="button" value="Submit" />
                </div>
            </div>
        </form>

        <div class="form-inside-div listing-ranking border-none">
            Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_sm_points ?> points
        </div>
    </div>
</div>


<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>