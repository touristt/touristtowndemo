<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/business.login.inc.mobile.php';
require '../../include/PHPMailer/class.phpmailer.php';
require_once '../../include/track-data-entry.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];
if (isset($_POST['button2'])) {
    $name = $_POST['name'];
    $email_add = $_POST['email'];
    $subject = $_POST['subject'];
    $Detail = $_POST['Detail'];
    $message = '<html>
<head>
 <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
</head>
<body>
<p>Dear Admin,<br></p>
<div>' . $Detail . '</div>
</body>
</html>';
    $mail = new PHPMailer();
    $mail->From = $email_add;
    $mail->FromName = $name;
    $mail->IsHTML(true);
    $mail->AddAddress('info@touristtown.ca');
    $mail->AddBCC('majid.khan@app-desk.com');
    $mail->Subject = $subject;
    $mail->MsgHTML($message);
    $mail->Send();
    $_SESSION['success'] = 1;
    // TRACK DATA ENTRY
    Track_Data_Entry('Listing',$BL_ID,'Contact Tourist Town','','Contact','user admin mobile');
    header('Location: /mobile/contact-tourist-town.php?bl_id=' . $BL_ID);
    exit;
}
require_once '../../include/my/mobile/header.php';
?>

<div class="content-left"> <div class="left">
    </div>
    <div class="right">
        <form name="form1" method="post" action="">
            <div class="content-header">Contact Tourist Town</div>
            <div class="form-inside-div">
                <label>Name</label>
                <div class="form-data">
                    <input type="text" placeholder="Enter Name" name="name"  required />
                </div>
            </div>
            <div class="form-inside-div">
                <label>Email</label>
                <div class="form-data">
                    <input type="email" placeholder="Enter Email" name="email" required />
                </div>
            </div>
            <div class="form-inside-div">
                <label>Subject</label>
                <div class="form-data">
                    <input type="text" placeholder="Enter Subject" name="subject" required />
                </div>
            </div>
            <div class="form-inside-div">
                <label>Detail</label>
                <div class="form-data">
                    <textarea name="Detail" rows="10" placeholder="Enter Detail" ></textarea>
                </div>
            </div>
            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
                    <input type="submit" name="button2" value="Send"/>
                </div>
            </div>
        </form>
    </div>
</div>

<?PHP
require_once '../../include/my/mobile/footer.php';
?>