<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/adminFunctions.inc.php';
require_once '../../include/business.login.inc.mobile.php';

$BID = $_SESSION['BUSINESS_ID'];
$sql = "SELECT BL_ID, BL_Listing_Title, C_Name FROM tbl_Business_Listing 
        LEFT JOIN tbl_Category ON BL_C_ID = C_ID 
        LEFT JOIN tbl_Listing_Type ON BL_Listing_Type = LT_ID 
        LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
        WHERE BL_B_ID = '$BID' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY))";
$resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$count_listings = mysql_num_rows($resultTMP);
if ($count_listings == '1') {
    $row = mysql_fetch_assoc($resultTMP);
    header('Location: customer-listing-mobile-menu.php?bl_id=' . $row['BL_ID']);
    exit;
}
require_once '../../include/my/mobile/header.php';
?>
<div class="content-left">
    <div class="right">
        <!--My Pages-->
        <div class="container">
            <div class="content-header">
                My Listings
            </div>

            <?php
            $help_text = show_help_text('My Pages');
            if ($help_text != '') {
                echo '<div class="form-inside-div">' . $help_text . '</div>';
            }
            ?>

            <?PHP
            while ($row = mysql_fetch_assoc($resultTMP)) {
                ?>
                <div class="form-inside-div" id="my-pages">
                    <div class="title">
                        <?php echo $row['BL_Listing_Title'] ?>
                    </div>
                    <div class="category">
                        <?php echo $row['C_Name'] ?>
                    </div>


                    <div class="view"><a href="/mobile/customer-listing-mobile-menu.php?bl_id=<?php echo $row['BL_ID'] ?>">View</a></div>
                </div>
                <?PHP
            }
            ?>
        </div>

        <!--Advertising-->
        <div class="container">
            <?php
            $sqlPending = "SELECT A_ID FROM tbl_Advertisement
                            WHERE A_B_ID = '" . encode_strings($BID, $db) . "' AND A_Is_Deleted = 0 AND A_Status = 2";
            $resultPending = mysql_query($sqlPending, $db) or die("Invalid query: $sqlPending -- " . mysql_error());
            $PendingCount = mysql_num_rows($resultPending);
            if ($PendingCount > 0) {
                ?>
                <div class="content-header">
                    My Advertisements
                </div>
                <?php
            }
            while ($rowPending = mysql_fetch_assoc($resultPending)) {
                ?>
                <div class="form-inside-div" id="my-pages">
                    Your Ad is in Pending state <a href="customer-advertisment-detail.php?advert_id=<?php echo $rowPending['A_ID'] ?>">Approve Now</a>
                </div>
                <?PHP
            }
            ?>
        </div>
    </div>
</div>
<?PHP
require_once '../../include/my/mobile/footer.php';
?>