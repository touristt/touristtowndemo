<?php
require_once '../../include/config.inc.php';
require_once '../../include/adminFunctions.inc.php';

$title = "Login Screen";
$updatecc = $_REQUEST['updatecc'];

require_once '../../include/my/mobile/header.php';
?>
<div class="login-outer-body">
    <div class="login-outer-container">
        <div class="login-container">
            <form name="form1" method="post" action="http://<?php echo BUSINESS_DOMAIN . BUSINESS_DIR; ?>mobile/">
                <input name="logop" type="hidden" id="logop" value="login">
                <input name="updatecc" type="hidden" value="<?php echo $updatecc; ?>">
                <div class="login-container-inside-div">
                    <?php
                    $help_text = show_help_text("Login Page");
                    if ($help_text != "") {
                        ?>
                        <div class="login-from-inside-div" >
                            <div class="login_text" colspan="2" >
                                <?php echo $help_text; ?>
                            </div>
                        </div>
                        <?php
                    }
                    if (count($_COOKIE) > 0) {
                        $user_email_id = $_COOKIE["user_email_id"];
                        $user_pas = $_COOKIE["user_password"];
                    }
                    ?>
                    <div class="login-detail-title-responsive">
                        <div class="signin-title-responsive-page">
                            Sign In
                        </div>
                    </div>
                    <div class="login-from-inside-div margin-top-login-from">
                        <div class="from-inside-div-feild login-user-font-size" ><input name="LOGIN_USER" id="LOGIN_USER" placeholder="Email" type="text" size="35" value="<?php echo $user_email_id; ?>"/></div>
                    </div>
                    <div class="login-from-inside-div margin-top-login-from">
                        <div class="from-inside-div-feild login-user-font-size"><input name="LOGIN_PASS" id="LOGIN_PASS" placeholder="Password" type="password" size="35" value="<?php echo $user_pas; ?>"/></div>
                    </div>
                    <div class="login-from-inside-div margin-top-login-from">
                        <div class="from-inside-div-feild"><input class="remember_me" name="remember_me"  type="checkbox" /><span class="remeber_text" >Remember Me</span></div>
                    </div>
                    <?php
                    if (isset($_SESSION['login_error']) && $_SESSION['login_error'] == 1) {
                        ?>
                        <div class="login-from-inside-div margin-top-login-from">
                            <div>Your email or password doesn't match what we have on file. Please try again or contact <a href="mailto:info@touristtown.ca">Tourist Town</a> for help.</div>
                        </div>
                        <?php
                        unset($_SESSION['login_error']);
                    }
                    if (isset($_SESSION['login_error']) && $_SESSION['login_error'] == 2) {
                        ?>
                        <div class="login-from-inside-div margin-top-login-from">
                            <div>Your listing has been deactivated. Please contact <a href="mailto:info@touristtown.ca">info@touristtown.ca</a> to reactivate your listing.<br>
                            </div>
                        </div> 
                        <?php
                        unset($_SESSION['login_error']);
                    }
                    if (isset($_SESSION['update_hash_success']) && $_SESSION['update_hash_success'] == 1) {
                        ?>
                        <div class="login-from-inside-div margin-top-login-from">
                            <div>Your business has been deleted.<br>
                            </div>
                        </div> 
                        <?php
                        unset($_SESSION['update_hash_success']);
                    }
                    if (isset($_SESSION['update_hash_error']) && $_SESSION['update_hash_error'] == 1) {
                        ?>
                        <div class="login-from-inside-div margin-top-login-from">
                            <div>Your business cannot be deleted for some reasons.<br>
                            </div>
                        </div> 
                        <?php
                        unset($_SESSION['update_hash_error']);
                    }
                    ?>
                    <div class="login-from-inside-div">
                        <div class="from-inside-div-button">
                            <input name="Submit" type="submit" value="SIGN IN" /></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require_once '../../include/my/mobile/footer.php';
?>