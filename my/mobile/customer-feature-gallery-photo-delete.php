<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/adminFunctions.inc.php';
require_once '../../include/business.login.inc.php';
require_once '../../include/track-data-entry.php';

if ($_REQUEST['id'] > 0) {
    $sql = "SELECT BFP_Photo, BFP_Order, BFP_BL_ID FROM tbl_Business_Feature_Photo WHERE BFP_ID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
}

if ($_GET['op'] == 'del') {
    
    $sql = "UPDATE tbl_Business_Feature_Photo SET 
            BFP_Order = (BFP_Order - 1) 
            WHERE BFP_Order > '" . encode_strings($row['BFP_Order'], $db) . "' AND 
            BFP_BL_ID = '" . $row['BFP_BL_ID'] . "'";
    mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());

    $sql = "DELETE tbl_Business_Feature_Photo 
            FROM tbl_Business_Feature_Photo 
            WHERE BFP_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $row['BFP_BL_ID'];
        Track_Data_Entry('Listing', $id, 'Photo Gallery', '', 'Delete Image', 'user admin mobile');
        //update points only for listing
    update_pointsin_business_tbl($row['BFP_BL_ID']);
    } else {
        $_SESSION['delete_error'] = 1;
    }

    header("Location: /mobile/customer-feature-gallery.php?bl_id=" . $row['BFP_BL_ID']);
    exit();
}
?>

