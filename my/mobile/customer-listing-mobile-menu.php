<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/business.login.inc.mobile.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];
if ($BL_ID > 0) {
    $sql = "SELECT BLP_Header_Image FROM tbl_Business_Listing 
        LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
        WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
} else {
    header('Location: /mobile/index.php');
}
if(isset($_REQUEST['msg']) && $_REQUEST['msg'] ==1){
    $_SESSION['success'] = 1;
}
require_once '../../include/my/mobile/header.php';
?>
<div class="content-left">
    <div class="right">
        <?php if ($rowListing['BLP_Header_Image'] != '') { ?>
            <img  src="http://<?php echo DOMAIN . IMG_LOC_REL . $rowListing['BLP_Header_Image'] ?>" width="100%">
        <?php } else { ?>
            <img  src="http://<?php echo DOMAIN . IMG_LOC_REL . 'no-perview.jpg' ?>" width="100%">
        <?php } ?>
        <div class="form-inside-div position-on-image">
            <div class="form-inside-image-position-div position-on-image-margin-bottom">
                <div class="inside-position-on-image-div">
                    <a class="main-image-mobile-button float-left position-on-image-button-width"  href="customer-listing-main-photo.php?bl_id=<?php echo $BL_ID ?>">UPDATE MAIN IMAGE / VIDEO</a>
                </div>
            </div> 
            <div class="form-inside-image-position-div">
                <div class="inside-position-on-image-div">
                    <a class="btn-anchor float-left position-on-image-button-width" href="customer-listing-thumbnail-photo.php?bl_id=<?php echo $BL_ID ?>">UPDATE THUMBNAIL IMAGE</a>
                </div>
            </div>
        </div>
        <?php
        $sql = "SELECT * FROM tbl_Feature where F_Name = 'Photo Gallery'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            $sql1 = "SELECT * FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = " . $row['F_ID'];
            $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
            $row1 = mysql_fetch_assoc($result1);
            if ($row1['BLF_BL_ID'] != '') {
                ?>
                <div class="form-inside-div">
                    <div class="menu-item-text-align-inside">
                        <a href="/mobile/customer-feature-gallery.php?bl_id=<?php echo $BL_ID ?>" role="menuitem" tabindex="0">Manage Photo Gallery</a>
                    </div>
                </div>
                <?php
            }
        }
        ?>
        <div class="form-inside-div">
            <div class="menu-item-text-align-inside">
                <a href="/mobile/customer-listing-contact-details.php?bl_id=<?php echo $BL_ID ?>" role="menuitem" tabindex="0">Contact Details</a>
            </div>
        </div>
        <?PHP
        require_once('preview-link.php');
        ?>
        <div class="form-inside-div">
            <div class="menu-item-text-align-inside"> 
                <a href="/mobile/customer-credit-card-details.php?bl_id=<?php echo $BL_ID ?>" role="menuitem" tabindex="0">Credit Card Details</a>
            </div>
        </div>
        <div class="form-inside-div">
            <div class="menu-item-text-align-inside">
                <a href="/mobile/login-details.php?bl_id=<?php echo $BL_ID ?>" role="menuitem" tabindex="0">Manage Login Details</a>
            </div>
        </div>
        <div class="form-inside-div">
            <div class="menu-item-text-align-inside">
                <a href="/mobile/contact-tourist-town.php?bl_id=<?php echo $BL_ID ?>" role="menuitem" tabindex="0">Contact Tourist Town</a>
            </div>
        </div> 
        <div class="form-inside-div">
            <div class="menu-item-text-align-inside">
                <?php if ($_SESSION['BUSINESS_ID']) { ?>
                    <a href="<?php echo BUSINESS_DIR ?>mobile/?logop=logout">Logout</a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?PHP
require_once '../../include/my/mobile/footer.php';
?>