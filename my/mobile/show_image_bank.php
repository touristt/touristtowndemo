<?php
require_once '../../include/config.inc.php';
define("DOMAIN", 'touristtowndemo.com');
$BL_ID = $_GET['bl_id'];
require_once('../../include/top-nav-show-image-bank.php');
$sql = "SELECT * FROM tbl_Image_Bank WHERE FIND_IN_SET($BL_ID, IB_Listings) ORDER BY IB_ID DESC";
$result = mysql_query($sql);
$count = mysql_num_rows($result);
?>

<div class="image-bank-container image-bank-container-on-dialog">
    <?php
    if ($count > 0) {
        while ($row = mysql_fetch_array($result)) {
            ?>
            <div id="bgcolor_<?php echo $row['IB_ID'] ?>" onclick="select_the_image('<?php echo $row['IB_Path'] ?>','<?php echo $row['IB_ID'] ?>')" class="image-hover-bank image-bank-image-section" style="border: 1px solid #fff;cursor: default;">
                <div class="image-section-image-align">
                    <img src="http://<?php echo DOMAIN . IMG_BANK_REL . $row['IB_Thumbnail_Path'] ?>">
                </div>
                <div style="text-align:center;margin-top: 2px;">
                    <?php echo $row['IB_ID'] . ' - ' . $row['IB_Dimension'] ?>
                </div>
            </div>
            <?php
        }
    } else {
        echo '<div class="no-image-found">No images found in image bank.</div>';
    }
    ?>
</div>
<div id="autocomplete_data" style="display: none;"><?php include '../../include/autocomplete-mobile.php' ?></div>
<script>
    $(function() {
        //autocomplete for lisitngs
        var search = $('#autocomplete_data').text();
        var searched_list = "";
        var json_data = [];
        if(search != ""){
            searched_list = search.split("%");
            for (var i = 0; i < searched_list.length-1; i++) {
                json_data.push({"name":searched_list[i]});
            }
        }        
        $("#autocomplete").tokenInput(json_data ,{
            onResult: function (item) {
                if($.isEmptyObject(item)){
                    return [{id:'0',name: $("tester").text()}]
                }else{
                    item.unshift({"name":$("tester").text()});
                    var lookup = {};
                    var result = [];

                    for (var temp, i = 0; temp = item[i++];) {
                        var name = temp.name;

                        if (!(name in lookup)) {
                            lookup[name] = 1;
                            result.push({"name":name});
                        }
                    }
                    return result;
                }

            },
            onAdd: function (item) {
                var value = $('#keywords-searched').val();
                if(value != '') {
                    $('#keywords-searched').val(value+","+ item.name);
                    $('#auto_complete_form_submit').submit();
                }
                else {
                    $('#keywords-searched').val(item.name);
                    $('#auto_complete_form_submit').submit();
                }
            },
            onDelete: function (item) {
                var value = $('#keywords-searched').val().split(",");
                $('#keywords-searched').empty(); 
                var data = "";
                $.each( value, function( key, value ) {
                    if(value != item.name) {
                        if(data != '') {
                            data = data+","+ value;
                            $('#auto_complete_form_submit').submit();
                        } else {
                            data = value;   
                            $('#auto_complete_form_submit').submit();
                        }
                    }
                });
                $('#keywords-searched').val(data);
            },
            resultsLimit: 10
            }
    );
    $('.token-input-dropdown').css("width", "200px");
    });
</script>