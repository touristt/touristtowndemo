<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/adminFunctions.inc.php';
require_once '../../include/business.login.inc.mobile.php';
require_once '../../include/image-bank-usage-function.php';
require_once '../../include/track-data-entry.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];
if ($BL_ID > 0) {
    
} else {
    header('Location: /mobile/index.php');
}
if (isset($_POST['op']) && $_POST['op'] == 'save') {
    require '../../include/picUpload.inc.php';
    if ($_POST['image_bank'] == "") {
        foreach ($_FILES['pic']['name'] as $key => $val) {
            $pic = Upload_Pic($key, 'pic', 0, 0, false, IMG_LOC_ABS, 10000000, true, 1, 'Listing', $BL_ID);
            if (is_array($pic)) {
                $sqlMax = "SELECT MAX(BFP_Order) FROM tbl_Business_Feature_Photo WHERE BFP_BL_ID = '$BL_ID'";
                $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
                $rowMax = mysql_fetch_row($resultMax);
                $sql_gallary = "INSERT tbl_Business_Feature_Photo SET BFP_Title = '" . encode_strings($_REQUEST['title'], $db) . "',
                            BFP_Photo_710X440 = '" . encode_strings($pic['0']['0'], $db) . "',
                            BFP_Photo_195X195 = '" . encode_strings($pic['0']['1'], $db) . "',
                            BFP_Photo_1000X600 = '" . encode_strings($pic['0']['2'], $db) . "',
                            BFP_Photo_285X210 = '" . encode_strings($pic['0']['3'], $db) . "',
                            BFP_Photo_710X440_Mobile = '" . encode_strings($pic['0']['4'], $db) . "',
                            BFP_BL_ID = '$BL_ID', BFP_Order = '" . ($rowMax[0] + 1) . "'";
                $pic_id = $pic['1'];
            }
            $result = mysql_query($sql_gallary, $db) or die("Invalid query: $sql_gallary -- " . mysql_error());
            $id = mysql_insert_id();
            $ids = $pic_id . ',' . $ids;
            if ($result) {
                $_SESSION['success'] = 1;
                if ($pic_id > 0) {
                    //Image usage from image bank
                    imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Photo_Gallery', $id);
                }
            } else {
                $_SESSION['error'] = 1;
            }
        }
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $BL_ID, 'Photo Gallery', $ids, 'Add Image', 'user admin mobile');
    } else {
        $ids = encode_strings($_POST['image_bank'], $db);
        $pic_ids = explode(",", $ids);
        // last @param 1 = Gallery Images
        foreach ($pic_ids as $pic_id) {
            $pic = Upload_Pic_Library($pic_id, 1);
            if (is_array($pic)) {
                $sqlMax = "SELECT MAX(BFP_Order) FROM tbl_Business_Feature_Photo WHERE BFP_BL_ID = '$BL_ID'";
                $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
                $rowMax = mysql_fetch_row($resultMax);
                $sql = "INSERT tbl_Business_Feature_Photo SET BFP_Title = '" . encode_strings($_REQUEST['title'], $db) . "',
                    BFP_Photo_710X440 = '" . encode_strings($pic['0'], $db) . "',
                    BFP_Photo_195X195 = '" . encode_strings($pic['1'], $db) . "',
                    BFP_Photo_1000X600 = '" . encode_strings($pic['2'], $db) . "',
                    BFP_Photo_285X210 = '" . encode_strings($pic['3'], $db) . "',
                    BFP_Photo_710X440_Mobile = '" . encode_strings($pic['0']['4'], $db) . "',
                    BFP_BL_ID = '$BL_ID', BFP_Order = '" . ($rowMax[0] + 1) . "'";
            }
            $result = mysql_query($sql, $db);
            $id = mysql_insert_id();
            if ($result) {
                $_SESSION['success'] = 1;
                if ($pic_id > 0) {
                    //Image usage from image bank
                    imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Photo_Gallery', $id);
                }
            } else {
                $_SESSION['error'] = 1;
            }
        }
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $BL_ID, 'Photo Gallery', $ids, 'Add Image', 'user admin mobile');
    }
    //update points only for listing
    update_pointsin_business_tbl($BL_ID);
    header("Location: /mobile/customer-feature-gallery.php?bl_id=" . $BL_ID);
    exit();
}

if ($_POST['op'] == 'edit') {
    $BL_ID = $_POST['bl_id'];
    $BFP_ID = $_POST['bfp_id'];
    $text = $_POST['description' . $BFP_ID];
    $sql = "UPDATE tbl_Business_Feature_Photo SET BFP_Title = '$text' WHERE BFP_ID = '$BFP_ID'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Photo Gallery', '', 'Edit Description', 'user admin mobile');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /mobile/customer-feature-gallery.php?bl_id=" . $BL_ID);
    exit();
}

require_once '../../include/my/mobile/header.php';
?>
<div class="content-left">
    <div class="left">
        <?php require_once '../../include/nav-B-customer.php'; ?>
        <?PHP require_once '../../include/nav-B-listing.php'; ?>
    </div>
    <style type="text/css">
        .ajaxSpinnerImage{
            display: none;
            z-index: -9999;   
        }
        .custom-ajax-show{ display: none;
                           z-index: -9999; }
        </style>
        <div class="right">
        <form name="form1" id="MyUploadForm" onSubmit="return false"  method="post"  enctype="multipart/form-data" action="" style="display:none;float:left;width:100%;margin-bottom: 10px;">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="image_bank" class="image_bank" id="image_bank1" value="">
            <div class="content-header">Add Photo</div>
            <div class="form-inside-div">
                <label>Title</label>
                <div class="form-data">
                    <input name="title" type="text" id="title" size="50" required/>
                </div>
            </div>

            <div class="form-inside-div">
                <label>Photo</label>
                <div class="form-data form-input-text">
                    <div class="div_image_library">
                        <span id="wakeup" class="daily_browse" onchange="show_file_name(1, this, 0)" onclick="show_image_library(1, <?php echo $BL_ID; ?>, null,<?php echo $numRowsGal ?>)">Select File</span>
                        <input class="uploadFileName" id="image_name_bank1" style="display: none;" disabled>
                        <input class="setting-name" onchange="show_text()" accept="image/*" id="photo1" type="file" name="pic[]" style="display: none;" multiple />
                    </div>
                    <input style="width:170px;" id="uploadFile1" class="uploadFileName" disabled>
                </div>
            </div>
            <div class="form-inside-div">
                <div class="button">
                    <input type="hidden" id="image_page" value="1" />
                    <input type="submit" name="button" onclick="progress_bar(1, <?php echo $BL_ID ?>)" id="button" value="Save Photo" />
                </div>
            </div>

            <div class="form-inside-div border-none">
                <div class="form-data full-width-details">
                    Photo file limit size is 10MBs. If you're files are large they may take 10 seconds to load once you click "Save Photo".
                </div>
            </div>
        </form>

        <div class="content-header">Photo Gallery
            <div class="link">
                <?PHP
                $counter = 1;
                if ($numRowsGal < 30) {
                    echo '<div class="add-link responsive-link"><a onclick="show_form()">+Add Photo</a></div>';
                }
                $Gal = show_addon_points(2);
                if ($numRowsGal > 0) {
                    echo '<div class="points"><div class="points-com">' . $Gal . ' pts</div></div>';
                } else {
                    echo '<div class="points"><div class="points-uncom">' . $Gal . ' pts</div></div>';
                }
                ?>
            </div>
            <div class="instruction">
                Fields with this background<span></span>will show on free listings profile.
            </div>
            <script>
                function show_form() {
                    $('#MyUploadForm').show();
                }
            </script>
        </div>

        <?php
        $help_text = show_help_text('Photo Gallery');
        if ($help_text != '') {
            echo '<div class="form-inside-div responsive-hide-photo-gallery">' . $help_text . '</div>';
        }
        ?>
        <div class="photo-gallery-limit">
            <span>Photos <?php echo((mysql_num_rows($resultGal) == "") ? "0" : mysql_num_rows($resultGal)) ?> of 30</span>
        </div>
        <div class="form-inside-div border-none margin-none" id="listing-gallery">
            <ul class="gallery">
                <?PHP
                while ($data = mysql_fetch_assoc($resultGal)) {
                    ?>
                    <li class="image" id="recordsArray_<?php echo $data['BFP_ID'] ?>">
                        <?php if ($data['BFP_Photo_285X210'] != "") { ?>
                            <div class="listing-image"><img src="http://<?php echo DOMAIN . IMG_LOC_REL . $data['BFP_Photo_285X210'] ?>" alt="" /></div>
                        <?php } else { ?>
                            <div class="listing-image"><img src="http://<?php echo DOMAIN . IMG_LOC_REL . $data['BFP_Photo'] ?>" alt=""/></div>
                        <?php } ?>
                        <div class="count"><?php echo $counter ?></div>
                        <div class="cross"><a onClick="return confirm('Are you sure?');" href="customer-feature-gallery-photo-delete.php?op=del&amp;id=<?php echo $data['BFP_ID'] ?>&amp;bl_id=<?php echo$BL_ID ?>">x</a></div>
                        <div class="desc" onclick="show_description(<?php echo $data['BFP_ID'] ?>);"><?php echo ($data['BFP_Title'] != '') ? $data['BFP_Title'] : '+ Add Description'; ?></div> 

                        <form action="customer-feature-gallery.php" method="post" name="form" id="gallery-description-form<?php echo $data['BFP_ID'] ?>" style="display:none;">
                            <div >
                                <input type="hidden" name="op" value="edit">
                                <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
                                <input type="hidden" name="bfp_id" value="<?php echo $data['BFP_ID'] ?>">
                                <textarea name="description<?php echo $data['BFP_ID'] ?>" style="width:94% !important; margin-left: 1%;" size="50" required><?php echo $data['BFP_Title'] ?></textarea>
                            </div>
                            <div >
                                <div class="form-data" style="text-align: center;">
                                    <input type="submit" name="button" value="Save Now"/>
                                </div>
                            </div> 
                        </form>
                    </li>
                    <?PHP
                    $counter++;
                }
                ?>
            </ul>
        </div>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>
<div class="progress-container-main" style="display:none;">
    <div id="progress-container">
        <p class="progress-title">Upload in Progress</p>
        <p>Please leave this page open while your image is uploading</p>
        <div id="progressbox" style="display:none;">
            <div id="progressbar"></div>
            <div id="statustxt">0%</div>
        </div>
    </div>
</div>

<script>

    // FUNCTION TO ALERT IF MORE THAN 20 PHOTOS ARE SELECTED
    $("input[type='file']").on("change", function () {
        var numFiles = $(this).get(0).files.length;
        var numf = numFiles + <?php echo mysql_num_rows($resultGal) ?>;
        if (numf > 30) {
            swal("", "You can not upload more than 30 photos in photo gallery.", "warning");
            $("#photo1").val([]);
            document.getElementById("uploadFile1").value = "";

        } else if (numFiles > 20) {

            swal("", "You can upload only 20 photos.", "warning");
            $("#photo1").val([]);
            document.getElementById("uploadFile1").value = "";

        }

    });
    function show_text() {
        $('#uploadFile1').show();
        document.getElementById("uploadFile1").value = "Multiple Images Selected";
        $('#image_name_bank1').hide();
        $('#image_bank1').val('');
    }

</script>
<?PHP
require_once '../../include/my/mobile/footer.php';
?>