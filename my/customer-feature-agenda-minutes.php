<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/adminFunctions.inc.php';

$BID = $_SESSION['BUSINESS_ID'];
$points_taken = 0;

$BL_ID = $_REQUEST['bl_id'];
$sql_agendas = "SELECT BFAMMP_Photo FROM tbl_Business_Feature_Agendas_Minutes_Main_Photo WHERE BFAMMP_BL_ID = '$BL_ID'";
$result_agendas = mysql_query($sql_agendas, $db) or die("Invalid query: $sql_agendas -- " . mysql_error());
$row_agendas = mysql_fetch_assoc($result_agendas);
if ($BL_ID > 0) {
    
} else {
    header("Location:customers.php");
    exit();
}
if ($_POST['op'] == 'save_photo') {
    require_once '../include/picUpload.inc.php';
    // last @param 23 = Agenda
    $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 23, 'Listing', $BL_ID);
    $sql_daily_des = mysql_query("SELECT * FROM tbl_Business_Feature_Agendas_Minutes_Main_Photo WHERE BFAMMP_BL_ID= '" . encode_strings($_POST['bl_id'], $db) . "'");
    $daily_des_img = mysql_fetch_assoc($sql_daily_des);
    $count = mysql_num_rows($sql_daily_des);
    if ($_POST['image_bank'] == "") {
        if (is_array($pic)) {
            if ($count > 0) {
                $sql = "UPDATE tbl_Business_Feature_Agendas_Minutes_Main_Photo SET 
                        BFAMMP_Photo = '" . encode_strings($pic['0']['0'], $db) . "'
                        WHERE BFAMMP_BL_ID= '" . encode_strings($_POST['bl_id'], $db) . "'";
                $pic_id = $pic['1'];
            } else {
                $sql = "INSERT tbl_Business_Feature_Agendas_Minutes_Main_Photo SET 
                        BFAMMP_BL_ID= '" . encode_strings($_POST['bl_id'], $db) . "',
                        BFAMMP_Photo = '" . encode_strings($pic['0']['0'], $db) . "'";
            }
        }
    } else {
        $pic_id = $_POST['image_bank'];
        // last @param 23 = About Us Main Image
        $pic = Upload_Pic_Library($pic_id, 23);
        if (is_array($pic)) {
            if ($count > 0) {
                $sql = "UPDATE tbl_Business_Feature_Agendas_Minutes_Main_Photo SET 
                        BFAMMP_Photo = '" . encode_strings($pic['0'], $db) . "'
                        WHERE BFAMMP_BL_ID= '" . encode_strings($_POST['bl_id'], $db) . "'";
            } else {
                $sql = "INSERT tbl_Business_Feature_Agendas_Minutes_Main_Photo SET 
                        BFAMMP_BL_ID= '" . encode_strings($_POST['bl_id'], $db) . "',
                        BFAMMP_Photo = '" . encode_strings($pic['0'], $db) . "'";
            }
        }
    }
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    if ($count > 0) {
        $id = $daily_des_img['BFAMMP_ID'];
    } else {
        $id = mysql_insert_id();
    }
    if ($result) {
        $_SESSION['success'] = 1;
        if ($pic_id > 0) {
            //Image usage from image bank
            imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Agenda', $id);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location:customer-feature-agenda-minutes.php?bl_id=" . $BL_ID);
    exit;
}
if ($_GET['op'] == 'del_photo') {
    $update = "DELETE FROM tbl_Business_Feature_Agendas_Minutes_Main_Photo 
                WHERE BFAMMP_BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['delete'] = 1;
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Agenda', $_REQUEST['bl_id']);
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location:customer-feature-agenda-minutes.php?bl_id=" . $_REQUEST['bl_id']);
    exit;
}
if ($_POST['op_1st'] == 'save') {
    $BL_ID = $_POST['bl_id'];
    $meeting_date = $_POST['meeting_date'];
    $BID = $_POST['bid'];
    $sql_meeting = "INSERT tbl_Business_Feature_Agendas_Minutes SET
                    BFAM_Date = '$meeting_date',
                    BFAM_BL_ID = '$BL_ID',
                    BFAM_B_ID = '$BID'";
    $file_size_agenda = $_FILES['file_agenda']['size'];
    $file_size_minute = $_FILES['file_minute']['size'];
    $max_filesize = 5342523;
    $pdf_agenda = str_replace(' ', '_', $_FILES['file_agenda']['name']);
    $pdf_minute = str_replace(' ', '_', $_FILES['file_minute']['name']);
    if ($pdf_minute != "") {
        $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_minute;
        $filePathMinute = PDF_LOC_ABS . $random_name;
        if ($_FILES["file_minute"]["type"] == "application/pdf") {
            if ($file_size_minute < $max_filesize) {
                move_uploaded_file($_FILES["file_minute"]["tmp_name"], $filePathMinute);
                $sql_meeting .= ", BFAM_Minute = '" . $random_name . "'";
            }
        }
    }
    if ($pdf_agenda != "") {
        $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_agenda;
        $filePathAgenda = PDF_LOC_ABS . $random_name;
        if ($_FILES["file_agenda"]["type"] == "application/pdf") {
            if ($file_size_agenda < $max_filesize) {
                move_uploaded_file($_FILES["file_agenda"]["tmp_name"], $filePathAgenda);
                $sql_meeting .= ", BFAM_Agenda = '" . $random_name . "'";
            }
        }
    }
    $sql_meeting .= ", BFAM_Keywords = '" . $_POST['new_keyword'] . "'";
    $result = mysql_query($sql_meeting);
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
    exit;
}
if ($_POST['op'] == 'save') {
    $select = "SELECT BFAM_Minute, BFAM_Agenda FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_ID = '" . encode_strings($_POST['BFAM_ID'], $db) . "'";
    $selRes = mysql_query($select, $db) or die("Invalid query: $select -- " . mysql_error());
    $rowSel = mysql_fetch_assoc($selRes);
    $sql = "UPDATE tbl_Business_Feature_Agendas_Minutes SET BFAM_Date = '" . encode_strings($_POST['meeting_date'], $db) . "'";
    $file_size_agenda = $_FILES['file_agenda']['size'];
    $file_size_minute = $_FILES['file_minute']['size'];
    $max_filesize = 5342523;
    $pdf_agenda = str_replace(' ', '_', $_FILES['file_agenda']['name']);
    $pdf_minute = str_replace(' ', '_', $_FILES['file_minute']['name']);
    if ($pdf_minute != "") {
        $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_minute;
        $filePathMinute = PDF_LOC_ABS . $random_name;
        if ($_FILES["file_minute"]["type"] == "application/pdf") {
            if ($file_size_minute < $max_filesize) {
                if ($rowSel['BFAM_Minute'] != "") {
                    unlink(PDF_LOC_ABS . $rowSel['BFAM_Minute']);
                }
                move_uploaded_file($_FILES["file_minute"]["tmp_name"], $filePathMinute);
                $sql .= ", BFAM_Minute = '" . $random_name . "'";
            }
        }
    }
    if ($pdf_agenda != "") {
        $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_agenda;
        $filePathAgenda = PDF_LOC_ABS . $random_name;
        if ($_FILES["file_agenda"]["type"] == "application/pdf") {
            if ($file_size_agenda < $max_filesize) {
                if ($rowSel['BFAM_Agenda'] != "") {
                    unlink(PDF_LOC_ABS . $rowSel['BFAM_Agenda']);
                }
                move_uploaded_file($_FILES["file_agenda"]["tmp_name"], $filePathAgenda);
                $sql .= ", BFAM_Agenda = '" . $random_name . "'";
            }
        }
    }
    $sql .= ", BFAM_Keywords = '" . encode_strings($_POST['new_keyword'], $db) . "'
                WHERE BFAM_ID = '" . encode_strings($_POST['BFAM_ID'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
    exit;
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del_pdf') {
    $BL_ID = $_REQUEST['bl_id'];
    $BFAM_ID = ($_REQUEST['del_pdf_age']) ? $_REQUEST['del_pdf_age'] : $_REQUEST['del_pdf_min'];
    $select = "SELECT BFAM_Agenda, BFAM_Minute FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_ID = '" . encode_strings($BFAM_ID, $db) . "'";
    $result = mysql_query($select, $db) or die("Invalid query: $select -- " . mysql_error());
    $PDF_FILE = mysql_fetch_assoc($result);
    $delete = "UPDATE tbl_Business_Feature_Agendas_Minutes SET ";
    if (isset($_REQUEST['del_pdf_age'])) {
        $delete .= "BFAM_Agenda = ''";
        unlink(PDF_LOC_ABS . $PDF_FILE['BFAM_Agenda']);
    } else if (isset($_REQUEST['del_pdf_min'])) {
        $delete .= "BFAM_Minute = ''";
        unlink(PDF_LOC_ABS . $PDF_FILE['BFAM_Minute']);
    }
    $delete .= " WHERE BFAM_ID = '" . encode_strings($BFAM_ID, $db) . "'";
    $result = mysql_query($delete, $db) or die("Invalid query: $delete -- " . mysql_error());
    if ($result) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location:customer-feature-agenda-minutes.php?bl_id=" . $BL_ID);
    exit;
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $BFAM_ID = $_REQUEST['delete'];
    $BL_ID = $_REQUEST['bl_id'];
    $select = "SELECT BFAM_Agenda, BFAM_Minute FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_ID = '" . encode_strings($BFAM_ID, $db) . "'";
    $result = mysql_query($select, $db) or die("Invalid query: $select -- " . mysql_error());
    $PDF_FILE = mysql_fetch_assoc($result);
    if ($PDF_FILE['BFAM_Agenda'] != "") {
        unlink(PDF_LOC_ABS . $PDF_FILE['BFAM_Agenda']);
    }
    if ($PDF_FILE['BFAM_Minute'] != "") {
        unlink(PDF_LOC_ABS . $PDF_FILE['BFAM_Minute']);
    }
    $delete = "DELETE FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_ID = '" . encode_strings($BFAM_ID, $db) . "'";
    $result = mysql_query($delete, $db) or die("Invalid query: $delete -- " . mysql_error());
    if ($result) {
        $_SESSION['delete'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location:customer-feature-agenda-minutes.php?bl_id=" . $BL_ID);
    exit;
}
require_once '../include/my/header.php';
?>
<div class="content-left">
    <?php require_once '../include/nav-B-customer.php'; ?>
    <div class="title-link">
        <div class="title">My Page</div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div> 
    </div>

    <div class="left">
        <?PHP require '../include/nav-B-listing.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">
            <div class="title">Agendas & Minutes</div>
            <div class="link">
                <?php
                $AgendaMinute = show_addon_points(13);
                if ($numRowsAM > 0) {
                    echo '<div class="points-com">' . $AgendaMinute . ' pts</div>';
                    $points_taken = $AgendaMinute;
                } else {
                    echo '<div class="points-uncom">' . $AgendaMinute . ' pts</div>';
                }
                ?>
            </div>
        </div>
        <div class="form-inside-div" style="border:none;">
            <div class="content-sub-header">
                <div class="title">Main Image</div>
                <div class="link">

                </div>
            </div>
        </div>
        <div class="cropped-container1">
            <div class="image-editor1">
                <div class="cropit-image-preview-container my-container-left">
                    <div class="cropit-image-preview main-preview">
                        <img class="preview-img preview-img-script23" style="display: none;" src="">    
                        <?php if ($row_agendas['BFAMMP_Photo'] != "") { ?>
                            <img class="existing-img existing_imgs23" src="http://<?php echo DOMAIN . IMG_LOC_REL . $row_agendas['BFAMMP_Photo'] ?>">
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <form name="form1" method="post" onSubmit="return check_img_size(23, 10000000)"  enctype="multipart/form-data" action="">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="op" value="save_photo">
            <input type="hidden" name="image_bank" class="image_bank" id="image_bank23" value="">
            <div class="form-inside-div margin-none border-none">
                <div class="div_image_library">
                    <span class="daily_browse" onclick="show_image_library(23,<?php echo $BL_ID; ?>)">Select File</span>
                    <input type="file" name="pic[]" onchange="show_file_name(23, this, 0)" id="photo23" style="display: none;">
                    <div class="data-column delete-main-photo1 padding-none">
                        <?php if ($row_agendas['BFAMMP_Photo'] != '') { ?>
                            <a class="deletePhoto margin-left-main" onClick="return confirm('Are you sure?');"  href="customer-feature-agenda-minutes.php?bl_id=<?php echo $BL_ID ?>&op=del_photo">Delete Photo</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" name="button" id="button" value="Save Image" />
                </div>
            </div>
        </form>
        <div class="form-inside-div border-none">
            <?php
            $help_text = show_help_text('Agendas & Minutes');
            if ($help_text != '') {
                echo '<div class="form-inside-div padding-none margin-none">' . $help_text . '</div>';
            }
            ?>
            <form name="new-agenda" onsubmit="return false" id="pdf-update0" method="post"  action="" enctype="multipart/form-data">
                <input type="hidden" name="op_1st" value="save">
                <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
                <input type="hidden" name="bid" value="<?php echo $BID ?>">
                <input type="hidden" class="agenda_page" value="1">
                <div class="addParentCategory agenda_autocomplete">
                    <div class="form-inside-div margin-left-none adenda-minute-main">
                        <label class="form-inside-div-adv">Meeting Date</label>
                        <div class="form-data add-menu-item-field">
                            <input class="previous-date-not-allowed required_fields0" type="text" name="meeting_date" value="" required/>
                        </div>
                        <div class="form-data add-menu-item-field padding-top-bottom minute-agenda-form">
                            <label class="form-inside-div-adv">Agenda</label>
                            <div class="agenda-min-pdf">
                                <label class="form-inside-div-adv" for="agenda_pdf_new0">Browse for PDF</label>
                                <input onchange="show_file_name_agenda(this.files[0].name, 1)" class="agenda_file_ext0" id="agenda_pdf_new0" type="file" accept="application/pdf" name="file_agenda" value="" style="visibility: hidden; position: absolute; z-index: 0">
                                <span class="agenda_pdf_new0 agenda_file_name"></span>
                            </div>
                        </div>
                        <div class="form-data add-menu-item-field padding-top-bottom minute-agenda-form">
                            <label class="form-inside-div-adv">Minutes</label>
                            <div class="agenda-min-pdf">
                                <label class="form-inside-div-adv" for="minutes_pdf_new0">Browse for PDF</label>
                                <input class="minute_file_ext0" onchange="show_file_name_minuts(this.files[0].name, 1)" id="minutes_pdf_new0" type="file" accept="application/pdf" name="file_minute" value="" style="visibility: hidden; position: absolute; z-index: 0">
                                <span class="minutes_pdf_new0 agenda_file_name minuts"></span>
                            </div>                           
                        </div>
                        <input type="hidden" id="listing_search" class="keywords-searched" name="new_keyword" value="">
                        <div class="form-data add-menu-item-field padding-top-bottom">
                            <label class="form-inside-div-adv">Keywords</label>
                            <input type="text" id="autocomplete" size="50"/>
                        </div>
                    </div>
                </div>
                <div class="form-inside-div border-none margin-left-none">
                    <div class="button">
                        <input type="submit" name="add_meeting" onclick="progress_bar(0)"  value="Add Meeting Now" />
                    </div>      
                </div> 
            </form>
            <div class="progress-container-main" style="display:none;">
                <div id="progress-container">
                    <p class="progress-title">Upload in Progress</p>
                    <p>Please leave this page open while your PDF is uploading</p>
                    <div id="progressbox" style="display:none;">
                        <div id="progressbar"></div>
                        <div id="statustxt">0%</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-inside-div border-none margin-top-agenda">
            <div class="content-sub-header manage-agenda-minute-title border-none">
                <div class="title">Manage Agendas & Minutes</div>
            </div>
        </div>
        <script>
            $(function () {
                var searched_items = $('.keywords-searched').val();
                if (searched_items != "") {
                    var myarray = searched_items.split(',');
                    var json = [];
                    for (var i = 0; i < myarray.length; i++)
                    {
                        json.push({"name": myarray[i]});
                    }
                }
                $("#autocomplete").tokenInput(<?php include '../include/agenda_autocomplete.php'; ?>, {
                    onResult: function (item) {
                        if ($.isEmptyObject(item)) {
                            return [{id: '0', name: $("tester").text()}]
                        } else {
                            item.unshift({"name": $("tester").text()});
                            var lookup = {};
                            var result = [];
                            for (var temp, i = 0; temp = item[i++]; ) {
                                var name = temp.name;

                                if (!(name in lookup)) {
                                    lookup[name] = 1;
                                    result.push({"name": name});
                                }
                            }
                            return result;
                        }
                    },
                    onAdd: function (item) {
                        var value = $('.keywords-searched').val();
                        if (value != '') {
                            $('.keywords-searched').val(value + "," + item.name);
                        } else {
                            $('.keywords-searched').val(item.name);
                        }
                    },
                    onDelete: function (item) {
                        var value = $('.keywords-searched').val().split(",");
                        $('.keywords-searched').empty();
                        var data = "";
                        $.each(value, function (key, value) {
                            if (value != item.name) {
                                if (data != '') {
                                    data = data + "," + value;
                                } else {
                                    data = value;
                                }
                            }
                        });
                        $('.keywords-searched').val(data);
                    },
                    resultsLimit: 10,
                    prePopulate: json
                }
                );

            });
        </script>
        <div class="menu-items-accodings">                         
            <div id="accordion">
                <?PHP
                $sql = "SELECT * FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '$BL_ID' ORDER BY BFAM_Date DESC";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                $i = 0;
                while ($data = mysql_fetch_assoc($result)) {
                    ?>
                    <h3 class="accordion-rows" id="<?php echo $data['BFAM_ID'] . ' #@ ' . $data["BFAM_Keywords"] ?>"><span class="accordion-title"><?php echo $data['BFAM_Date'] ?></span><label class="add-item-accordion"><a onClick="return confirm('Are you sure this action can not be undone!');" href="customer-feature-agenda-minutes.php?delete=<?php echo $data['BFAM_ID']; ?>&op=del&bl_id=<?php echo $BL_ID ?>">Delete</a></label></h3> 
                    <div class="sub-accordions accordion-padding">
                        <form method="post" action="" onsubmit="return false" id="pdf-update<?php echo $data['BFAM_ID'] ?>" enctype="multipart/form-data">
                            <input type="hidden" name="op" value="save">
                            <input type="hidden" name="BFAM_ID" value="<?php echo $data['BFAM_ID'] ?>">
                            <input type="hidden" class="agenda_page" value="1">
                            <div class="addParentCategory agenda_autocomplete">
                                <div class="form-inside-div add-product-section border-none padding-none adenda-minute-main">
                                    <div class="form-data add-menu-item-field padding-top-bottom">
                                        <label>Date</label>
                                        <input class="previous-date-not-allowed required_fields<?php echo $data['BFAM_ID'] ?>" type="text" name="meeting_date" value="<?php echo $data['BFAM_Date'] ?>">
                                    </div>
                                    <div class="form-data add-menu-item-field padding-top-bottom minute-agenda-form">
                                        <label>Agenda</label>
                                        <div class="agenda-min-pdf">
                                            <label for="agenda_pdf_<?php echo $data['BFAM_ID'] ?>" class="">Browse for PDF</label>
                                            <span class="agenda_pdf_<?php echo $data['BFAM_ID'] ?> agenda_file_name"></span>
                                            <input onchange="show_file_name_agenda_edit(this.files[0].name, '<?php echo $i ?>')" class="agenda_file_ext<?php echo $data['BFAM_ID'] ?>" id="agenda_pdf_<?php echo $data['BFAM_ID'] ?>" accept="application/pdf" type="file" name="file_agenda" value="" style="visibility: hidden; position: absolute; z-index: 0">
                                            <span class="agenda_edit" id="agenda_edit<?php echo $i++ ?>"></span>
                                        </div>
                                        <?php
                                        if ($data['BFAM_Agenda'] != "") {
                                            header('Content-type: application/pdf');
                                            header('Content-Disposition: attachment; filename="' . $data['BFAM_Agenda'] . '"');
                                            ?>
                                            <div class="agenda-min-pdf view-delete">
                                                <a target="_blank" href="http://<?php echo DOMAIN . PDF_LOC_REL . $data['BFAM_Agenda'] ?>">View PDF</a>
                                            </div>
                                            <div class="agenda-min-pdf view-delete">
                                                <a onClick="return confirm('Are you sure this action can not be undone!');" href="/customer-feature-agenda-minutes.php?del_pdf_age=<?php echo $data['BFAM_ID']; ?>&op=del_pdf&bl_id=<?php echo $BL_ID ?>">Delete PDF</a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="form-data add-menu-item-field padding-top-bottom minute-agenda-form">
                                        <label>Minutes</label>
                                        <div class="agenda-min-pdf">
                                            <label for="minutes_pdf_<?php echo $data['BFAM_ID'] ?>" class="">Browse for PDF</label>
                                            <span class="minutes_pdf_<?php echo $data['BFAM_ID'] ?> agenda_file_name"></span>
                                            <input onchange="show_file_name_minuts_edit(this.files[0].name, '<?php echo $i ?>')" class="minute_file_ext<?php echo $data['BFAM_ID'] ?>" id="minutes_pdf_<?php echo $data['BFAM_ID'] ?>" type="file" accept="application/pdf" name="file_minute" value="" style="visibility: hidden; position: absolute; z-index: 0">
                                            <span class="agenda_edit_minuts" id="agenda_edit_minuts<?php echo $i++ ?>"></span>
                                        </div>
                                        <?php
                                        if ($data['BFAM_Minute'] != "") {
                                            header('Content-type: application/pdf');
                                            header('Content-Disposition: attachment; filename="' . $data['BFAM_Minute'] . '"');
                                            ?>
                                            <div class="agenda-min-pdf view-delete">
                                                <a target="_blank" href="http://<?php echo DOMAIN . PDF_LOC_REL . $data['BFAM_Minute'] ?>">View PDF</a>
                                            </div>
                                            <div class="agenda-min-pdf view-delete">
                                                <a onClick="return confirm('Are you sure this action can not be undone!');" href="/customer-feature-agenda-minutes.php?del_pdf_min=<?php echo $data['BFAM_ID']; ?>&op=del_pdf&bl_id=<?php echo $BL_ID ?>">Delete PDF</a>
                                            </div>
                                        <?php } ?>                             
                                    </div>
                                    <input type="hidden" class="keywords-searched_<?php echo $data['BFAM_ID'] ?>" name="new_keyword" value="<?php echo $data['BFAM_Keywords'] ?>">
                                    <div class="form-data add-menu-item-field padding-top-bottom">
                                        <label>Keywords</label>
                                        <input type="text" class="autocomplete_<?php echo $data['BFAM_ID'] ?>" size="50"/>
                                    </div>
                                </div>
                                <div class="form-inside-div border-none margin-left-none">
                                    <div class="button">
                                        <input type="submit" name="save_now" onclick="progress_bar(<?php echo $data['BFAM_ID'] ?>)" value="Save Now" />
                                    </div>      
                                </div>
                            </div>
                        </form>
                    </div> 
                    <?php
                }
                ?>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $(".accordion-rows").one("click", function () {
                    var values = this.id.split("#@");
                    var BFAM_ID = values[0];
                    var searched_items = values[1];
                    if (searched_items.length >= 1 && searched_items.trim()) {
                        var myarray = searched_items.split(',');
                        var json = [];
                        for (var i = 0; i < myarray.length; i++)
                        {
                            json.push({"name": myarray[i]});
                        }
                    }
                    $(".autocomplete_" + BFAM_ID).tokenInput(<?php include '../include/agenda_autocomplete.php'; ?>, {
                        onResult: function (item) {
                            if ($.isEmptyObject(item)) {
                                return [{id: '0', name: $("tester").text()}]
                            } else {
                                item.unshift({"name": $("tester").text()});
                                var lookup = {};
                                var result = [];
                                for (var temp, i = 0; temp = item[i++]; ) {
                                    var name = temp.name;

                                    if (!(name in lookup)) {
                                        lookup[name] = 1;
                                        result.push({"name": name});
                                    }
                                }
                                return result;
                            }
                        },
                        onAdd: function (item) {
                            var value = $('.keywords-searched_' + BFAM_ID).val();
                            if (value != '') {
                                $('.keywords-searched_' + BFAM_ID).val(value + "," + item.name);
                            } else {
                                $('.keywords-searched_' + BFAM_ID).val(item.name);
                            }
                        },
                        onDelete: function (item) {
                            var value = $('.keywords-searched_' + BFAM_ID).val().split(",");
                            $('.keywords-searched_' + BFAM_ID).empty();
                            var data = "";
                            $.each(value, function (key, value) {
                                if (value.trim() != item.name.trim()) {
                                    if (data != '') {
                                        data = data + "," + value;
                                    } else {
                                        data = value;
                                    }
                                }
                            });
                            $('.keywords-searched_' + BFAM_ID).val(data);
                        },
                        resultsLimit: 10,
                        prePopulate: json
                    });

                });
            });

        </script>
        <div class="form-inside-div listing-ranking border-none margin-top-agenda-points">
            Ranking Points: <?php echo $points_taken ?> points out of <?php echo $AgendaMinute ?> points
        </div>
        <?php
        $sql1 = "SELECT * FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = 13";
        $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
        $row1 = mysql_fetch_assoc($result1);
        if ($row1['BLF_BL_ID'] != '') {
            ?>
            <div class="form-inside-div border-none text-align-center">
                <a href="customer-feature-add-ons.php?id=<?php echo $row1['BLF_ID'] ?>&bl_id=<?php echo $BL_ID ?>"  onclick="return confirm('Are you sure you want to <?php echo $row1['BLF_Active'] == 1 ? "deactivate" : "activate" ?> this Add on?');"><?php echo $row1['BLF_Active'] == 1 ? "Deactivate" : "Activate" ?> This Add on</a>
            </div>
            <?PHP
        }
        ?>
    </div>
    <div id="image-library" style="display:none;"></div>
</div>
<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>
<script>
    $(function () {
        $('.accordion-rows a').click(function (event) {
            if ($(this).parent().parent().hasClass("ui-accordion-header")) {
                event.stopPropagation(); // Stop opening accordion when clicks on delete
            }
        });
        $("#accordion").accordion({
            collapsible: true,
            heightStyle: 'content',
            active: false
        });
        $('.token-input-dropdown').css("width", "320px");
    });
    $(function () {
        $.urlParam = function (name, url) {
            if (!url) {
                url = window.location.href;
            }
            var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(url);
            if (!results) {
                return undefined;
            }
            return results[1] || undefined;
        }
        var existingImageMain = '<?php echo $mainPath ?>';
        var existingPathMain = 'http://<?php echo DOMAIN . IMG_LOC_REL . $mainPath ?>';
        $('.image-editor1').cropit({
            exportZoom: 1.305,
            imageBackground: true,
            imageBackgroundBorderWidth: 20,
            imageState: {
                src: 'http://<?php echo DOMAIN . IMG_LOC_REL . $mainPath ?>'
            }
            ,
            onFileChange: function () {
                $('.main-resize').show();
                $('.main-photo-resize').show();
            },
            onImageLoaded: function () {
                if ($('.image-editor1').cropit('isZoomable')) {
                    $('.main-resize').show();
                } else {
                    $('.main-resize').hide();
                }
            }
        });
        $('.export1').click(function () {
            var bl_id = $.urlParam('bl_id');
            if ($('.export1').cropit('imageSrc') == existingPathMain) {
                $.post("agenda_minutes_uploader.php", {
                    imageDataUpdate: existingImageMain,
                    bl_id: bl_id,
                    UpdateWithExistingImage: 'True'
                }, function (done) {
                    $('.image-editor1').cropit('disable');
                    if (done == 1) {
                        location.reload();
                    }
                });
            } else {
                var imageDataMain = $('.image-editor1').cropit('export');
                if (imageDataMain == null) {
                    alert('Image field is required');
                    return false;
                }
                $.post("agenda_minutes_uploader.php", {
                    imageData: imageDataMain,
                    bl_id: bl_id
                }, function (done) {
                    $('.image-editor1').cropit('disable');
                    if (done == 1) {
                        location.reload();
                    }
                });
            }

        });
    });
    function deletePhoto(BL_ID) {
        if (confirm("Are you sure?")) {
            $.post("agenda-photo-delete.php", {
                BL_ID: BL_ID
            }, function (result) {
                if (result == 1) {
                    location.reload();
                }
            });
        } else {
            return false;
        }
    }
    function show_file_name_agenda(val, count) {
        $(".agenda_pdf_new0.agenda_file_name").show();
    }
    function show_file_name_minuts(val, count) {
        $(".minutes_pdf_new0.agenda_file_name.minuts").show();
    }
    function show_file_name_agenda_edit(val, count) {
        document.getElementById("agenda_edit" + count).innerHTML = val;
    }
    function show_file_name_minuts_edit(vals, counts) {
        document.getElementById("agenda_edit_minuts" + counts).innerHTML = vals;
    }
</script>