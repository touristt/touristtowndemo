<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    
} else {
    header('Location: index.php');
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $delCouponImg = "DELETE tbl_Business_Feature_Coupon, tbl_Business_Feature_Coupon_Category_Multiple FROM tbl_Business_Feature_Coupon LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID WHERE BFC_ID = '" . encode_strings($_REQUEST['bfc_id'], $db) . "' AND BFC_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $resDelCouponImg = mysql_query($delCouponImg, $db) or die("Invalid query: $delCouponImg -- " . mysql_error());
    if ($resDelCouponImg) {
        $_SESSION['delete'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        $BFC_ID = $_REQUEST['bfc_id'];
        Track_Data_Entry('Listing', $id, 'Manage Coupon', $BFC_ID, 'Delete', 'user admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location:customer-feature-manage-coupon.php?bl_id=" . $BL_ID);
    exit();
}
require_once '../include/my/header.php';
?>

<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">Add ons</div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div>
    </div>

    <div class="left">
        <?PHP
        require '../include/nav-B-listing.php';
        ?>
    </div>
    <div class="right">
        <div class="content-header">
            <div class="title">Manage Coupon</div>
            <div class="link">
                <a href='customer-feature-add-coupon.php?bl_id=<?php echo $BL_ID ?>'>+ Add Coupon</a>
            </div>
        </div>

        <div class="form-inside-div border-none padding-bottom-none">
            <div class="content-sub-header">
                <div class="data-pdf-title" style="width: 200px">Coupon Name</div>
                <div class="coupon-options">Preview</div>
                <div class="coupon-options">#Sent</div>
                <div class="coupon-options">Edit</div>
                <div class="coupon-options">Delete</div>
                <div class="coupon-options">Status</div>
            </div>            
        </div>
        <?PHP
        $sql = "SELECT BFC_ID, BFC_Title, BFC_Status FROM tbl_Business_Feature_Coupon WHERE BFC_BL_ID = '$BL_ID'";
        $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($resultTMP)) {
            ?>
            <div class="form-inside-div padding-coupon-items">
                <div class="data-pdf-title" style="width: 200px">
                    <?php echo $row['BFC_Title'] ?>
                </div>
                <div class="coupon-options"><a href="customer-feature-add-coupons.php?bl_id=<?php echo $BL_ID ?>&bfc_id=<?php echo $row['BFC_ID'] ?>">Preview</a></div>
                <div class="coupon-options"><?php
                    $sql_sent = "SELECT CU_Counter FROM  tbl_Coupon_Usages where CU_BFC_ID='" . encode_strings($row['BFC_ID'], $db) . "' order by CU_Counter desc";
                    $result_count = mysql_query($sql_sent);
                    $row_count = mysql_fetch_assoc($result_count);
                    echo ($row_count['CU_Counter'] != '' ? $row_count['CU_Counter'] : '0');
                    ?></div>
                <div class="coupon-options"><a href="customer-feature-add-coupon.php?bl_id=<?php echo $BL_ID ?>&bfc_id=<?php echo $row['BFC_ID'] ?>">Edit</a></div>
                <div class="coupon-options"><a onClick="return confirm('Are you sure?');" href="customer-feature-manage-coupon.php?bl_id=<?php echo $BL_ID ?>&bfc_id=<?php echo $row['BFC_ID'] ?>&op=del">Delete</a></div>
                <div class="coupon-options"><?php echo $row['BFC_Status'] == 1 ? '<span class="approved">Approved</span>' : '<span class="pending">Pending</span>' ?></div>
            </div>
        <?PHP }
        ?>
    </div>
</div>
<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?> 