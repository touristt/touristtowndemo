<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/track-data-entry.php';

$BID = $_SESSION['BUSINESS_ID'];
$AID = $_REQUEST['advert_id'];
$sql = "SELECT * FROM tbl_Business WHERE B_ID = '" . encode_strings($BID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$rowBUS = mysql_fetch_assoc($result);
$emailBUS = $rowBUS['B_Email'];
if ($AID > 0) {
    $sql_advertisements = " SELECT tbl_Advertisement.*, tbl_Advertisement_Type.*, C_Name, RC_Name, R_Name, R_Domain  FROM tbl_Advertisement
                            LEFT JOIN tbl_Region ON A_Website = R_ID 
                            LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID 
                            LEFT JOIN tbl_Category ON A_C_ID = C_ID 
                            LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID AND A_Website = RC_R_ID
                            WHERE A_ID = '" . encode_strings($AID, $db) . "'";
    $result_advertisements = mysql_query($sql_advertisements, $db) or die("Invalid query: $sql_advertisements -- " . mysql_error());
    $rowAdvert = mysql_fetch_assoc($result_advertisements); 
    $title = $rowAdvert['A_Title'];
    $sql_sub_advert = "SELECT * FROM tbl_Category WHERE C_ID = '" . encode_strings($rowAdvert['A_SC_ID'], $db) . "'";
    $result_sub = mysql_query($sql_sub_advert, $db) or die("Invalid query: $sql_sub_advert -- " . mysql_error());
    $rowAdvert_sub = mysql_fetch_assoc($result_sub);
    $Website_id = $rowAdvert['A_Website'];
    $category = $rowAdvert['A_C_ID'];
    $subcategory = $rowAdvert['A_SC_ID']; 
} else {
    header('Location:customer-advertisment-myaccount.php');
}

if (isset($_POST['button_approve'])) {
    $sql_update_req = " UPDATE tbl_Advertisement SET A_Status='3', A_Active_Date = CURDATE() WHERE A_ID = '" . encode_strings($AID, $db) . "' LIMIT 1";
    $result_update_req = mysql_query($sql_update_req, $db) or die("Invalid query: $sql_update_req -- " . mysql_error());

    $_SESSION['success'] = 1;
    // TRACK DATA ENTRY
    Track_Data_Entry('Listing Home', $BID, 'My Campaigns', $AID, 'Approve', 'user admin');
    header("Location: customer-advertisment-detail.php?advert_id=" . $AID);
    exit();
}

if (isset($_POST['button_change_request'])) {
    $sql_update_req = " UPDATE tbl_Advertisement SET A_Status='1' WHERE A_ID = '" . encode_strings($AID, $db) . "' LIMIT 1";
    $result_update_req = mysql_query($sql_update_req, $db) or die("Invalid query: $sql_update_req -- " . mysql_error());
    $sql_req_des = "INSERT tbl_Advertisement_Change_Request SET 
                    ACR_A_AID = '" . encode_strings($AID, $db) . "',      
                    ACR_Description = '" . encode_strings($_POST['request_desc'], $db) . "',
                    ACR_Date = NOW()";
    $result_req_des = mysql_query($sql_req_des, $db) or die("Invalid query: $sql_req_des -- " . mysql_error());
    $_SESSION['success'] = 2;
    // TRACK DATA ENTRY
    Track_Data_Entry('Listing Home', $BID, 'My Campaigns', $AID, 'Change Request', 'user admin');
    header("Location: customer-advertisment-detail.php?advert_id=" . $AID);
    exit();
}

if ($_REQUEST['del'] == 'true') {
    $sql = " UPDATE tbl_Advertisement SET A_Is_Deleted = 1 WHERE A_ID = '" . encode_strings($AID, $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $_SESSION['delete'] = 1;
    // TRACK DATA ENTRY
    Track_Data_Entry('Listing Home', $BID, 'My Campaigns', $AID, 'Delete', 'user admin');
    header("Location:customer-advertisment-myaccount.php");
    exit();
}

require_once '../include/my/header.php';
?>
<div class="content-left advert">
    <?php require_once '../include/nav-B-advertisement.php'; ?>
    <div class="title-link">
        <div class="title">My Campaigns</div>
        <div class="link">
        </div> 
        <div class="title float-right-text text-margin"></div>
    </div>

    <div class="right">
        <div class="content-header">
            <div class="title ad-detail">
                <div class="advert-detail-header" style="margin-bottom: 5px;"><?php echo $rowAdvert['R_Name'] ?></div>
                <div class="advert-detail-header">
                    <?php
                    echo $rowAdvert['AT_Name'];
                    if ($rowAdvert['A_C_ID'] != 0) {
                        if ($rowAdvert['RC_Name'] != '') {
                            echo " / " . $rowAdvert['RC_Name'];
                        } else {
                            echo " / " . $rowAdvert['C_Name'];
                        }
                    }
                    if ($rowAdvert['A_SC_ID'] != 0) {
                        echo " / " . $rowAdvert_sub['C_Name'];
                    }
                    
                    ?>
                </div>
            </div>
             <?php 
              $sql = mysql_query("SELECT A_ID, A_Status,A_End_Date, A_Is_Deleted FROM tbl_Advertisement WHERE A_ID  = '". $_REQUEST['advert_id'] . "' ORDER BY A_ID");
            $vals = mysql_fetch_assoc($sql);
            $stats = $vals['A_Status'];
            $A_Date = $vals['A_End_Date'];
            $delt = $vals['A_Is_Deleted']; 
            if($stats == 4 OR $delt == 1 OR ($vals['A_End_Date'] < date('Y-m-d') AND $vals['A_End_Date'] != '0000-00-00')){
            $disable_value = 1;  
            }else{
                $disable_value = '';
            }


             ?>
            <div class="link ad-detail">
               <form name="form" method="get" action="customer-advertisment-detail.php">
                    <select name="advert_id"  onchange="this.form.submit()">
                        <?php
                        $sql = "SELECT A_ID, A_Title FROM tbl_Advertisement WHERE A_B_ID = '$BID' ORDER BY A_ID";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($allAds = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $allAds['A_ID'] ?>" <?php echo ($AID == $allAds['A_ID']) ? 'selected' : '' ?>><?php echo $allAds['A_Title'] ?></option>
                        <?php } ?>
                    </select>
                </form>
            </div>
        </div>
        <div class="containter-adv">
            <div class="form-inside-div form-inside-div-adv">
                <label>Website</label>
                <div class="form-data from-inside-div-text <?php if($disable_value == 1){ echo "disabled-val"; } ?>">
                    <?php echo $rowAdvert['R_Domain']; ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-adv">
                <label>Campaign Type</label>
                <div class="form-data from-inside-div-text <?php if($disable_value == 1){ echo "disabled-val"; } ?>">
                    <?php echo $rowAdvert['AT_Name']; ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-adv" id="cat" <?php
                    if ($rowAdvert['AT_ID'] == 4) {
                        echo 'style="display :none"';
                    }
                    ?>>
                   
                <input type="hidden" value="<?php echo $Website_id; ?>" id="region-id" >
                <input type="hidden" value="1" id="update-id" >
                <input type="hidden" value="<?php echo $rowAdvert['A_ID']; ?>" id="advert-id" >
                <input type="hidden" value="<?php echo $rowAdvert['AT_ID']; ?>" id="advert-type" >
                <label>Category</label>
                <div class="form-data  id="advert-cats" >
                    <select class="adv-type-options <?php if($disable_value == 1){ echo "disabled-val"; } ?>"  id="advert-cat" name="category" onChange="get_cat_advert_update();" <?php if($disable_value == 1){ ?> disabled <?php } ?>>
                        <?PHP
                        if (isset($category)) { 
                            $sql = "SELECT C_ID, C_Name, RC_Name FROM tbl_Category
                                    LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID
                                    LEFT JOIN tbl_Region ON RC_R_ID = R_ID 
                                    WHERE R_ID = '$Website_id' AND C_Parent = 0 ORDER BY RC_Order ASC";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($rowCat = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo $rowCat['C_ID'] ?>" <?php echo ($rowCat['C_ID'] == $category) ? 'selected' : ''; ?>><?php echo ($rowCat['RC_Name'] != '') ? $rowCat['RC_Name'] : $rowCat['C_Name'] ?></option>
                                <?PHP
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-adv" id="sub-cat" <?php
                        if ($rowAdvert['AT_ID'] == 2 || $rowAdvert['AT_ID'] == 4) {
                            echo 'style="display :none"';
                        }
                        ?>>
                <label>Sub Category</label>
                <div class="form-data" id="advert-subcats">
                    <select class="adv-type-options <?php if($disable_value == 1){ echo "disabled-val"; } ?>" <?php if($disable_value == 1){ ?> disabled<?php } ?> id="advert-subcat" name="subcategory" onChange="get_subcat_advert_update();">
                        <option value="">Select Sub Category</option>
                        <?PHP
                        if (isset($subcategory)) {
                            $sql = "SELECT C_ID, C_Name FROM tbl_Category 
                                    LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID
                                    LEFT JOIN tbl_Region ON RC_R_ID = R_ID 
                                    WHERE R_ID = '$Website_id' AND C_Parent = '$category' ORDER BY C_Name";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($rowSubcat = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo $rowSubcat['C_ID'] ?>" <?php echo ($rowSubcat['C_ID'] == $subcategory) ? 'selected' : ''; ?>><?php echo $rowSubcat['C_Name'] ?></option>
                                <?PHP
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-adv">
                <label>Campaign</label>
                <div class="form-data from-inside-div-text">
                    <?php
                    if ($rowAdvert['A_Campaign'] == 1) {
                        echo 'Yes';
                    } else {
                        echo 'No';
                    }
                    ?>
                </div>
            </div>
            <?php
            if ($rowAdvert['A_Status'] == 3) {
                ?>
                <div class="form-inside-div form-inside-div-adv">
                    <label>View Campaign</label>
                    <div class="form-data from-inside-div-text">
                        <div class="approved-photo-div">
                            <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $rowAdvert['A_Approved_Logo'] ?>">          
                        </div>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-adv">
                    <label>Original Start Date</label>
                    <div class="form-data from-inside-div-text">
                        <?php echo date('F d, Y', strtotime($rowAdvert['A_Date'])); ?>
                    </div>
                </div>
                <?php
                if ($rowAdvert['A_End_Date'] != '0000-00-00') {
                    ?>
                    <div class="form-inside-div form-inside-div-adv">
                        <label>End Date</label>
                        <div class="form-data from-inside-div-text">
                            <?php echo date('F d, Y', strtotime($rowAdvert['A_End_Date'])); ?>
                        </div>
                    </div>
                    <?php
                }
                $sql_Impressions = "SELECT * FROM  tbl_Advertisement_Statistics WHERE AS_A_ID = '" . encode_strings($rowAdvert['A_ID'], $db) . "'";
                $result_Impressions = mysql_query($sql_Impressions, $db) or die("Invalid query: $sql_Impressions -- " . mysql_error());
                $total_impressions = 0;
                $total_clicks = 0;
                while ($rowAdvert_Impressions = mysql_fetch_assoc($result_Impressions)) {
                    $total_impressions += $rowAdvert_Impressions['AS_Impression'];
                    $total_clicks += $rowAdvert_Impressions['AS_Clicks'];
                }
                ?>
                <div class="form-inside-div form-inside-div-adv">
                    <label>Impressions</label>
                    <div class="form-data from-inside-div-text">
                        <?php echo $total_impressions ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-adv">
                    <label>Clicks</label>
                    <div class="form-data from-inside-div-text">
                        <?php echo $total_clicks ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-adv">
                    <label>Monthly Cost</label>
                    <div class="form-data margin-top-11">
                        $<?php echo $rowAdvert['A_Total'] ?>
                    </div>
                    <?php if($disable_value == '') { ?>
                    <a class="side-informtion margin-adver-cancel margin-top-11" onClick="return confirm('Are you sure you want to cancel your ad? This action can not be undone.');"  href="customer-advertisment-detail.php?del=true&advert_id=<?php echo $rowAdvert['A_ID'] ?>">Cancel Advertisement</a>
                <?php } ?>
                </div>
                <?php
            }
            if ($rowAdvert['A_Status'] == 1) {
                ?>
                <div class="form-inside-div border-none">
                    <p> The Tourist Town team will now create your campaign. Once your campaign is created you will receive an email to : <?php echo $emailBUS ?></p>
                </div>
                <?php
            }
            if ($rowAdvert['A_Status'] == 2) {
                ?>
                <div class="form-inside-div border-none">
                    <p> Below is your campaign that will shown as per your request.As soon as you
                        approve the campaign it will be posted to the selected website and billing 
                        will commence.
                    </p>
                </div>
                <div class="form-inside-div border-none">
                    <div class="form-data image-align-advert">
                        <div class="approved-photo-div">
                            <img  src="http://<?php echo DOMAIN . "/" . IMG_LOC_REL . "/" . $rowAdvert['A_Approved_Logo'] ?>"> 
                        </div>
                    </div>
                </div>
                <div class="form-inside-div border-none">
                    <div class="button">
                        <form name="form1" method="post" enctype="multipart/form-data" action="customer-advertisment-detail.php" >
                            <input type="hidden" name="advert_id" value="<?php echo $AID; ?>">

                            <input type="submit" name="button_approve" class="margin-right-15" value="Approve Advertisement">
                            <input type="button" value="Request Change" onclick="show_update_content(<?php echo $rowAdvert['A_ID'] ?>);" >
                        </form>
                        <form action="customer-advertisment-detail.php" method="post" name="form" id="Update-Content-<?php echo $rowAdvert['A_ID']; ?>" style="display:none;">
                            <div style="float:left; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                <input type="hidden" name="advert_id" value="<?php echo $AID; ?>">
                                <textarea required name="request_desc" style="width: 422px;"></textarea>
                            </div>
                            <div style="float:left;text-align: center; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                <div class="form-data"  >
                                    <input type="submit" name="button_change_request" value="Save Now"/>
                                </div>
                            </div> 
                        </form>
                    </div>
                </div>
            <?php } ?>
        </div>

        <!--Statistics-->
        <?php
        if ($rowAdvert['A_Status'] == 3) {
            ?>
            <div class="content-header">
                <div class="title">
                    Campaigning Period
                </div>
            </div>
            <div class="menu-items-accodings">
                <div id="accordion">
                    <?php
                    $sql2 = "SELECT DISTINCT YEAR(AS_Date) as years FROM tbl_Advertisement_Statistics WHERE AS_A_ID = $AID order by YEAR(AS_Date) desc";
                    $result2 = mysql_query($sql2, $db) or die("Invalid query: $sql2 -- " . mysql_error());
                    while ($row1 = mysql_fetch_assoc($result2)) {
                        $rows[] = $row1;
                    }
                    $current_month = date('M', strtotime('+1 month'));
                    $current_year = date('Y');
                    foreach ($rows as $row) {
                        ?>  
                        <h3 class="accordion-rows" id="ddd"><span class="accordion-title"><?php echo $row['years']; ?></span></h3>
                        <div class="sub-accordions accordion-padding">
                            <div class="monthly-statistic-header form-inside-div-width-admin">
                                <div class="data-column adv-listing-title padding-none">Month</div>
                                <div class="data-column cust-listing-other padding-none">Clicks</div>
                                <div class="data-column cust-listing-other padding-none">Impressions</div>
                            </div>
                            <?php
                            $months = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
                            $clicks = 0; $impressions = 0;
                            foreach ($months as $month) {
                                
                                if($row['years'] == $current_year && date('M', strtotime($row['years'] . '-' . $month . '-' . '01')) == $current_month){ break;}
                                
                                $sql2 = "SELECT YEAR(AS_Date),MONTHNAME(AS_Date), SUM(AS_Impression) AS impression, SUM(AS_Clicks) AS clicks FROM tbl_Advertisement_Statistics 
                                WHERE AS_A_ID = $AID AND YEAR(AS_Date) = '" . $row['years'] . "' AND MONTH(AS_Date) = '" . $month . "'";
                                $result2 = mysql_query($sql2, $db) or die("Invalid query: $sql2 -- " . mysql_error());
                                $rowStats = mysql_fetch_array($result2);
                                $clicks += $rowStats['clicks']; 
                                $impressions += $rowStats['impression'];
                                $dateObj = DateTime::createFromFormat('!m', $month);
                                $monthName = $dateObj->format('F');
                                ?>
                                <div class="data-content advert-statistics form-inside-div-width-admin">
                                    <div class="data-column adv-listing-title padding-none"><?php echo $monthName; ?></div>
                                    <div class="data-column cust-listing-other padding-none"><?php echo ($rowStats['impression'] != '') ? $rowStats['clicks'] : '0'; ?></div>
                                    <div class="data-column cust-listing-other padding-none"><?php echo ($rowStats['impression'] != '') ? $rowStats['impression'] : '0'; ?></div>
                                </div>
                                <?php }
                            ?>
                            <div class="data-content advert-statistics form-inside-div-width-admin total">
                                <div class="data-column adv-listing-title padding-none">Total :</div>
                                
                                <div class="data-column cust-listing-other padding-none"><?php echo $clicks ?></div>
                                    <div class="data-column cust-listing-other padding-none"><?php echo $impressions ?></div>
                            </div> 
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>    
<script>
    function show_update_content(a_id){
        $("#Update-Content-"+a_id).attr("title", "Request Change").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 500
        });
    }
  $(function () {

        $('.accordion-rows a').click(function (event) {
            if ($(this).parent().parent().hasClass("ui-accordion-header")) {
                event.stopPropagation(); // this is
            }
        });
        $("#accordion").accordion({
            collapsible: true,
            heightStyle: 'content',
            active: 0
        });
        $('.token-input-dropdown').css("width", "400px");
    });
</script>
<style>
    .menu-items-accodings{width: 825px !important;}
</style>
<?PHP
require_once '../include/my/footer.php';
?>