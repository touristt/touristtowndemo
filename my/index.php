<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/adminFunctions.inc.php';

$BID = $_SESSION['BUSINESS_ID'];

require_once '../include/my/header.php';
?>
<div class="content-left">

    <div class="title-link padding-none">
    </div>

    <div class="left">
        <?php require_once('../include/nav-B-home-sub.php'); ?>
    </div>

    <div class="right">
        <!--My Pages-->
        <div class="container">
            <div class="content-header">
                My Listings
            </div>

            <?php
            $help_text = show_help_text('My Pages');
            if ($help_text != '') {
                echo '<div class="form-inside-div">' . $help_text . '</div>';
            }
            ?>

            <?PHP
            $sql = "SELECT BL_ID, BL_Listing_Title, LT_Name FROM tbl_Business_Listing LEFT JOIN tbl_Category ON BL_C_ID = C_ID LEFT JOIN tbl_Listing_Type ON BL_Listing_Type = LT_ID 
                    LEFT JOIN tbl_Business ON BL_B_ID = B_ID WHERE BL_B_ID = '$BID' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY))";
            $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($resultTMP)) {
                ?>
                <div class="form-inside-div" id="my-pages">
                    <div class="title">
                        <?php echo $row['BL_Listing_Title'] ?>
                    </div>
                    <div class="category">
                        <?php echo $row['LT_Name'] ?>
                    </div>
                    <div class="category-name">
                        <?php
                        $sql_sub_categories = " SELECT C_Name FROM tbl_Business_Listing_Category LEFT JOIN tbl_Category ON C_ID = BLC_M_C_ID
                                                WHERE BLC_BL_ID=" . encode_strings($row['BL_ID'], $db) . " GROUP BY BLC_M_C_ID ";
                        $result_sub_categories = mysql_query($sql_sub_categories, $db) or die("Invalid query: $sql_sub_categories -- " . mysql_error());
                        while ($row_sub_categories = mysql_fetch_array($result_sub_categories)) {
                            ?>
                            <?php
                            echo $row_sub_categories['C_Name'];
                            ?>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="view"><a href="customer-listing-overview.php?bl_id=<?php echo $row['BL_ID'] ?>">View</a></div>
                </div>
            <?PHP }
            ?>
        </div>

        <!--Advertising-->
        <div class="container">
            <?php
            $sqlPending = "SELECT A_ID FROM tbl_Advertisement WHERE A_B_ID = '" . encode_strings($BID, $db) . "' AND A_Is_Deleted = 0 AND A_Status = 2";
            $resultPending = mysql_query($sqlPending, $db) or die("Invalid query: $sqlPending -- " . mysql_error());
            $PendingCount = mysql_num_rows($resultPending);
            if ($PendingCount > 0) {
                ?>
                <div class="content-header">
                    My Advertisements
                </div>
                <?php
            }
            while ($rowPending = mysql_fetch_assoc($resultPending)) {
                ?>
                <div class="form-inside-div" id="my-pages">
                    Your Ad is in Pending state <a href="customer-advertisment-detail.php?advert_id=<?php echo $rowPending['A_ID'] ?>">Approve Now</a>
                </div>
                <?PHP
            }
            ?>
        </div>
    </div>
</div>
<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>