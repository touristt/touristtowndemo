<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];
$Coupons_direction = $_REQUEST['coupons'];
$bfc_id = $_REQUEST['bfc_id'];
if ($BL_ID > 0) {
    $sql = "SELECT BL_ID, BL_Listing_Title, BL_Description, BL_Listing_Type, BL_Contact, BL_Phone, BL_Website, BL_Toll_Free, BL_Email, BL_Street, BL_Town, 
            BL_Province, BL_PostalCode, BL_Lat, BL_Long, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Description, BL_SEO_Keywords, 
            BL_Search_Words FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}

if ($_POST['op'] == 'save') {
    $title = $_POST['pdf_title'];
    $file_size = $_FILES['file']['size'];
    $max_filesize = 5342523;
    $pdf_name = str_replace(' ', '_', $_FILES['file']['name']);
    if ($pdf_name != "") {
        $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_name;
        $filePath = PDF_LOC_ABS . $random_name;
        if ($_FILES["file"]["type"] == "application/pdf") {
            if ($file_size < $max_filesize) {
                move_uploaded_file($_FILES["file"]["tmp_name"], $filePath);
                $sql_pdf = "INSERT INTO tbl_Description_PDF SET D_BL_ID = '$BL_ID', D_PDF = '$random_name', D_PDF_Title = '$title'";
                $sqlMax = "SELECT MAX(D_PDF_Order) FROM tbl_Description_PDF WHERE D_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
                $rowMax = mysql_fetch_row($resultMax);
                $sql_pdf .= ", D_PDF_Order = '" . ($rowMax[0] + 1) . "'";
                $result = mysql_query($sql_pdf);
                if ($result) {
                    $_SESSION['success'] = 1;
                    // TRACK DATA ENTRY
                    $id = $BL_ID;
                    Track_Data_Entry('Listing', $id, 'Upload A PDF', '', 'Add', 'user admin');
                } else {
                    $_SESSION['error'] = 1;
                }
            }
        }
    }
    if (isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true) {
        header("Location: /listings-preview.php?bl_id=" . $BL_ID);
    } else {
        header("Location: customer-listing-upload-pdf.php?bl_id=" . $BL_ID);
    }
    exit;
}
if ($_POST['up'] == 'upload') {
    $title = $_POST['pdf_update'];
    $pdf_name = str_replace(' ', '_', $_FILES['file']['name']);
    $file_size = $_FILES['file']['size'];
    $max_filesize = 5342523;
    $d_id_update = $_POST['d_id_update'];
    if ($pdf_name != "") {
        $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_name;
        $filePath = PDF_LOC_ABS . $random_name;
        if ($_FILES["file"]["type"] == "application/pdf") {
            if ($file_size < $max_filesize) {
                move_uploaded_file($_FILES["file"]["tmp_name"], $filePath);
                $select_pdf = "SELECT D_PDF FROM tbl_Description_PDF WHERE D_id= $d_id_update AND D_BL_ID ='" . $BL_ID . "'";
                $result_pdf = mysql_query($select_pdf);
                $pdf_row = mysql_fetch_assoc($result_pdf);
                if ($pdf_row['D_PDF'] != "") {
                    unlink(PDF_LOC_ABS . $pdf_row['D_PDF']);
                }
                $sql_pdf = "Update tbl_Description_PDF SET D_BL_ID ='" . $BL_ID . "', D_PDF='" . mysql_real_escape_string($random_name) . "', D_PDF_Title = '" . mysql_real_escape_string($title) . "' WHERE D_id= $d_id_update";
                $result = mysql_query($sql_pdf);
                if ($result) {
                    $_SESSION['success'] = 1;
                } else {
                    $_SESSION['error'] = 1;
                }
            }
        }
    } else {
        $sql_pdf = "Update tbl_Description_PDF SET D_PDF_Title='$title' where D_id= $d_id_update";
        $result = mysql_query($sql_pdf);
        if ($result) {
            $_SESSION['success'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
    }
    // TRACK DATA ENTRY
    $id = $BL_ID;
    Track_Data_Entry('Listing', $id, 'Upload A PDF', '', 'Update', 'user admin');
    if (isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true) {
        header("Location: /listings-preview.php?bl_id=" . $BL_ID);
    } else {
        header("Location: customer-listing-upload-pdf.php?bl_id=" . $BL_ID);
    }
    exit;
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $select_pdf = "SELECT D_PDF FROM tbl_Description_PDF WHERE D_id= " . $_REQUEST['delete'] . " AND D_BL_ID ='" . $BL_ID . "'";
    $result_pdf = mysql_query($select_pdf);
    $pdf_row = mysql_fetch_assoc($result_pdf);
    if ($pdf_row['D_PDF'] != "") {
        unlink(PDF_LOC_ABS . $pdf_row['D_PDF']);
    }
    $sql = "Delete FROM tbl_Description_PDF WHERE D_id = '" . $_REQUEST['delete'] . "'";
    $result = mysql_query($sql);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Upload A PDF', '', 'Delete PDF', 'user admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    if (isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true) {
        header("Location: /listings-preview.php?bl_id=" . $BL_ID);
    } else {
        header("Location: customer-listing-upload-pdf.php?bl_id=" . $BL_ID);
    }

    exit;
}
require_once '../include/my/header.php';
?>

<div class="content-left">

<?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">My Page</div>
        <div class="link">
<?PHP
require_once('preview-link.php');
?>
        </div>
        <div class="instruction">
            Fields with this background<span></span>will show on free listings profile.
        </div>
    </div>

    <div class="left">
<?PHP require '../include/nav-B-mypage.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">
            <div class="title">Upload A PDF</div>
            <div class="link">
            </div>
        </div>
<?php
$help_text = show_help_text('Upload a PDF');
if ($help_text != '') {
    echo '<div class="form-inside-div border-none">' . $help_text . '</div>';
}
?>
        <form method="post" onsubmit="" id="pdf-update0" name="form1" enctype="multipart/form-data" action="">
            <input type="hidden" name="op" value="save">
            <input type="hidden"  name="bl_id" value="<?php echo $BL_ID ?>" required>
            <div class="form-inside-div border-none des-pdf padding-top-zero" >
                <label for="description_pdf" class="">Browse for PDF</label>
                <input class="file_ext0" id="description_pdf" onchange="show_file_name(this.files[0].name, 1)" type="file" name="file" accept="application/pdf" value="" style="visibility: hidden; position: absolute; z-index: 0"/>
                <div class="des-input">    
                    <input type="text" class="required_title0" name="pdf_title" placeholder="Enter Title" value="" required/>
                </div>
                <input id="uploadFile_pdf" class="uploadFileName" disabled>
            </div>
            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" name="buttonpdf"  onclick="" value="Save Now"/>
                </div>  
            </div>

        </form>
        <div class="progress-container-main" style="display:none;">
            <div id="progress-container">
                <p class="progress-title">Upload in Progress</p>
                <p>Please leave this page open while your PDF is uploading</p>
                <div id="progressbox" style="display:none;">
                    <div id="progressbar"></div>
                    <div id="statustxt">0%</div>
                </div>
            </div>
        </div>
        <div class="form-inside-div border-none padding-top-des">
            <div class="content-sub-header">
                <div class="title">Manage PDFs</div>
            </div>
        </div>

        <div class="brief">
<?php
$sql_dpdf = "SELECT * from tbl_Description_PDF where D_BL_ID = $BL_ID ORDER BY D_PDF_Order ASC";
$result_dpdf = mysql_query($sql_dpdf, $db) or die("Invalid query: $sql_dpdf -- " . mysql_error());

while ($data_dpdf = mysql_fetch_assoc($result_dpdf)) {
    ?>
                <div class="form-inside-div border-none padding-top-none" id="recordsArray_<?php echo $data_dpdf['D_id'] ?>">
                    <div class="data-pdf-title">
                        <a href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $data_dpdf['D_PDF'] ?>"  target="_blank"><?php echo $data_dpdf['D_PDF_Title']; ?></a> 
                    </div>
                    <div class="data-pdf-edit">
                        <a class="delete-pointer" onclick="Editpdf(<?php echo $data_dpdf['D_id'] ?>)">Edit</a>
                    </div>
                    <div class="data-des-option">
                        <a class="delete-pointer" onClick="return confirm('Are you sure this action can not be undone!');" href="customer-listing-upload-pdf.php?delete=<?php echo $data_dpdf['D_id'] ?>&op=del&bl_id=<?php echo $BL_ID ?>&go_to_preview=<?php echo $_REQUEST['go_to_preview'] ?>">Delete</a>
                    </div>
                    <form enctype="multipart/form-data" onsubmit="" method="post" name="form" id="pdf-update<?php echo $data_dpdf['D_id'] ?>" style="display:none; overflow: hidden">
                        <input type="hidden" name="up" value="upload">
                        <input type="hidden"  name="d_id_update" value="<?php echo $data_dpdf['D_id'] ?>" required>
                        <input type="hidden"  name="update_bl_id" value="<?php echo $BL_ID ?>" required>
                        <div class="pop-des-label">
                            <label for="description_pdf<?php echo $data_dpdf['D_id'] ?>" class="">Browse for PDF</label>
                            <input class="file_ext<?php echo $data_dpdf['D_id'] ?>" id="description_pdf<?php echo $data_dpdf['D_id'] ?>" type="file" name="file" accept="application/pdf" value="" style="visibility: hidden; position: absolute; z-index: 0"/>
                            <div class="popup-des-input">
                                <input type="text" class="required_title<?php echo $data_dpdf['D_id'] ?>" name="pdf_update" placeholder="Enter Title" value="<?php echo $data_dpdf['D_PDF_Title'] ?>" required/>
                            </div>
                        </div>
                        <div class="form-inside-div border-none text-algn-center">
                            <div class="button">
                                <input type="submit" class="first-div" name="update_pdf" onclick="" value="Save Now"/>
                            </div>  
                        </div>
                    </form>
                </div>
    <?php
}
?>
        </div>
    </div>
</div>
<script>
    $(function () {
        $(".brief").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Description_PDF&field=D_PDF_Order&id=D_id';
                $.post("reorder.php", order, function (theResponse) {
                    swal("Pdf Re-ordered", "Pdf Re-ordered successfully.", "success");
                });
            }
        });
    });
    function Editpdf(pdf_id) {
        $("#pdf-update" + pdf_id).dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 500
        });
    }
    function show_file_name(val, count) {
        $("#uploadFile_pdf").show();
        document.getElementById("uploadFile_pdf").value = val;
    }
</script>
<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>