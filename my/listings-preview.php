<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/adminFunctions.inc.php';
$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = isset($_REQUEST['bl_id']) ? $_REQUEST['bl_id'] : 0;
$preview_page = 1;
if ($BL_ID > 0) {
    
} else {
    header('Location: index.php');
}

if (isset($_POST['op']) && $_POST['op'] == 'edit') {
    $BFP_ID = $_POST['bfp_id'];
    $text = $_POST['description' . $BFP_ID];
    $sql = "UPDATE tbl_Business_Feature_Photo SET BFP_Title = '$text' WHERE BFP_ID = '$BFP_ID'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: listings-preview.php?bl_id=" . $BL_ID);
    exit();
}

if ($_GET['op'] == 'del') {
    $BL_ID = $_GET['bl_id'];
    $BFP_ID = $_REQUEST['id'];
    $sql = "DELETE FROM tbl_Business_Feature_Photo 
            WHERE BFP_ID = '" . encode_strings($BFP_ID, $db) . "' AND BFP_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Photo_Gallery', $BFP_ID);
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: listings-preview.php?bl_id=" . $BL_ID);
    exit();
}

require_once '../include/my/header.php';
?>
<div>

    <?php require_once '../include/nav-B-customer.php'; ?>
    <?php
    $sql = "SELECT * FROM tbl_Business_Listing WHERE BL_ID  = $BL_ID";
    $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($resultTMP);
    $activeBiz = $row;

    function ctime($myTime, $end = false) {
        if ($myTime == '00:00:01' && !$end) {
            return 'Closed';
        } elseif ($myTime == '00:00:02') {
            return 'By Appointment';
        } elseif ($myTime == '00:00:03') {
            return 'Open 24 Hours';
        } else {
            $mySplit = explode(':', $myTime);
            return date('g:ia', mktime($mySplit[0], $mySplit[1], 1, 1, 1));
        }
    }

//Getting Gallery Images if active
    $count_gallery = 0;
    $sqlFG = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 2 AND BLF_Active = 1";
    $resultFG = mysql_query($sqlFG, $db) or die("Invalid query: $sqlFG -- " . mysql_error());
    if (mysql_num_rows($resultFG) > 0) {
        $sqlG = "SELECT BFP_ID,BFP_BL_ID, BFP_Title, BFP_Photo, BFP_Photo_195X195, BFP_Order FROM tbl_Business_Feature_Photo WHERE BFP_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' ORDER BY BFP_Order";
        $resultG = mysql_query($sqlG, $db) or die("Invalid query: $sqlG -- " . mysql_error());
        $count_gallery = mysql_num_rows($resultG);
    }

//Getting About us if active
    $count_about = 0;
    $sqlFA = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 1 AND BLF_Active = 1";
    $resultFA = mysql_query($sqlFA, $db) or die("Invalid query: $sqlFA -- " . mysql_error());
    if (mysql_num_rows($resultFA) > 0) {
        $sql = "SELECT BFA_ID FROM tbl_Business_Feature_About WHERE BFA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'";
        $resultAbt = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $count_about = mysql_num_rows($resultAbt);
    }

//Getting Menu if active
    $count_menu = 0;
    $sqlFM = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 6 AND BLF_Active = 1";
    $resultFM = mysql_query($sqlFM, $db) or die("Invalid query: $sqlFM -- " . mysql_error());
    if (mysql_num_rows($resultFM) > 0) {
        $sql = "SELECT BFM_ID FROM tbl_Business_Feature_Menu WHERE BFM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'";
        $resultMenu = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $count_menu = mysql_num_rows($resultMenu);
    }

//Getting Entertainment if active
    $count_Ent = 0;
    $sqlFE = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 8 AND BLF_Active = 1";
    $resultFE = mysql_query($sqlFE, $db) or die("Invalid query: $sqlFE -- " . mysql_error());
    if (mysql_num_rows($resultFE) > 0) {
        $sql = "SELECT BFEA_ID FROM tbl_Business_Feature_Entertainment_Acts WHERE BFEA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND BFEA_Time >= CURDATE()";
        $resultEnt = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $count_Ent = mysql_num_rows($resultEnt);
    }

//Getting Daily Feature if active
    $count_DS = 0;
    $sqlFDS = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 7 AND BLF_Active = 1";
    $resultFDS = mysql_query($sqlFDS, $db) or die("Invalid query: $sqlFDS -- " . mysql_error());
    if (mysql_num_rows($resultFDS) > 0) {
        $sql = "SELECT BFDS_ID FROM tbl_Business_Feature_Daily_Specials WHERE BFDS_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'";
        $resultDS = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $count_DS = mysql_num_rows($resultDS);
    }
//Getting Agenda Minutes if active
    $count_AM = 0;
    $sqlFAM = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 13 AND BLF_Active = 1";
    $resultFAM = mysql_query($sqlFAM, $db) or die("Invalid query: $sqlFAM -- " . mysql_error());
    if (mysql_num_rows($resultFAM) > 0) {
        $sql = "SELECT BFAM_ID FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'";
        $resultAM = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $count_AM = mysql_num_rows($resultAM);
    }
//Getting Guest Book if active
    $sqlFGB = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 12 AND BLF_Active = 1";
    $resultFGB = mysql_query($sqlFGB, $db) or die("Invalid query: $sqlFGB -- " . mysql_error());
    ?>
    <section class="main_slider">
        <div class="slider_wrapper">
            <div class="slider_wrapper_video">
                <div class="sliderCycle1" data-cycle-loader="wait" data-cycle-fx=fade data-cycle-timeout=0 data-cycle-prev="#prev" data-cycle-next="#next">
                    <?php if ($activeBiz['BL_Header_Image'] != "") { ?>
                        <a id="edit_main_image" href="/customer-listing-main-photo.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                            Edit
                        </a>
                        <div class="slide-images">  
                            <img class="preview-img-home1 preview-img-script22" src=""> 
                            <img class="slider_image" src="http://<?php echo DOMAIN . IMG_LOC_REL . $activeBiz['BL_Header_Image'] ?>" alt="<?php echo $activeBiz['BL_Header_Image_Alt'] ?>" longdesc="<?php echo $activeBiz['BL_Header_Image_Alt'] ?>">                    
                        </div>
                    <?php } else {
                        ?>

                        <a id="add_main_image" href="/customer-listing-main-photo.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                            +Add Image
                        </a>
                        <div class="slide-images">
                            <img class="preview-img-home1 preview-img-script22" src="">    
                            <img class="slider_image" src="http://<?php echo DOMAIN . IMG_LOC_REL . "Default-Header.jpg" ?>" alt="<?php echo $activeBiz['BL_Header_Image_Alt'] ?>" longdesc="<?php echo $activeBiz['BL_Header_Image_Alt'] ?>">
                        </div>
                    <?php }
                    ?>
                </div>
                <?php if ($THEME['TO_Slider_Overlay'] != "") { ?>
                    <div class="image_overlay_img">
                        <div class="image_overlay">
                            <img src="http://<?php echo DOMAIN . IMG_ICON_REL . $THEME['TO_Slider_Overlay']; ?>" alt="Slider Overlay">
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <!--Slider End-->
    <section class="description listing-home-des margin-bottom-none" style="background-color:white;">
        <div class="description-wrapper">
            <div class="event-filter">
                <div class="listing-title-detail" >
                    <div class="listing-title1 <?php if ($activeBiz['BL_Listing_Type'] == 1) { ?> listing-title-with-bg" <?php } else { ?> " style="margin-bottom: 20px;" <?php } ?> >
                        <?php if ($activeBiz['BL_Listing_Title'] != '') { ?>
                            <?php echo ($activeBiz['BL_Listing_Title']); ?>
                            <a href="/customer-listing-contact-details.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                            </a>
                        <?php } else { ?>
                            <div class="add-box-preview" >
                                <a href="/customer-listing-contact-details.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                    +Add Title 
                                </a>
                            </div>  
                        <?php } ?>
                    </div>
                    <div class="listing-address <?php if ($activeBiz['BL_Listing_Type'] == 1) { ?> listing-title-with-bg <?php } ?>">
                        <?php if ($activeBiz['BL_Town'] != '' || $activeBiz['BL_Province'] != '' || $activeBiz['BL_Country'] != '') { ?>
                            <?php echo ($activeBiz['BL_Town'] != '' ? $activeBiz['BL_Town'] : '' ) . ($activeBiz['BL_Province'] != '' ? ', ' . $activeBiz['BL_Province'] : '') . ($activeBiz['BL_Country'] != '' ? ', ' . $activeBiz['BL_Country'] : ''); ?>
                            <a href="/customer-listing-mapping.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                            </a>
                        <?php } else { ?>
                            <div class="add-box-preview" >
                                <a href="/customer-listing-mapping.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                    +Add Location & Mapping Details 
                                </a>
                            </div> 
                        <?php } ?>
                    </div>
                </div> 

                <?php if ($count_gallery <= 0) { ?>
                    <div class="listing-title-detail">
                        <div class="add-box-preview" >
                            <a href="/customer-feature-gallery.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                +Add Photo 
                            </a>
                        </div> 
                    </div>
                <?php } else { ?>
                    <div id="display_photo_button" class="content-header1">
                        <div class="link">
                            <div class="add-link"><a href="/customer-feature-gallery.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">+Add Photo</a></div>
                        </div>
                    </div>
                <?php } ?>
                <div id="image-library"></div>            

                     <div class="thumbnails listing-home-gallary">                
                     <div class="thumbnail-slider">
                        <div class="inner">
                            <?PHP
                            if ($count_gallery > 0) {
                                ?>
                                <ul class="bxslider">
                                    <?PHP while ($rowG = mysql_fetch_assoc($resultG)) { ?>
                                        <li>   
                                            <div class="cross1"><a onClick="return confirm('Are you sure?');" href="listings-preview.php?op=del&id=<?php echo $rowG['BFP_ID'] ?>&bl_id=<?php echo $rowG['BFP_BL_ID'] ?>">x</a></div>
                                            <div class="desc" onclick="show_description(<?php echo $rowG['BFP_ID'] ?>);"><?php echo ($rowG['BFP_Title'] != '') ? $rowG['BFP_Title'] : '+ Add Description'; ?></div> 

                                            <form action="" method="post" name="form" id="gallery-description-form<?php echo $rowG['BFP_ID'] ?>" style="display:none;">
                                                <div id="gallery_desc">
                                                    <input type="hidden" name="op" value="edit">
                                                    <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
                                                    <input type="hidden" name="bfp_id" value="<?php echo $rowG['BFP_ID'] ?>">
                                                    <textarea name="description<?php echo $rowG['BFP_ID'] ?>" style="width:420px !important;" size="50" required><?php echo $rowG['BFP_Title'] ?></textarea>
                                                </div>
                                                <div id="gallery_desc_save_btn">
                                                    <div class="form-data">
                                                        <input type="submit" name="button" value="Save Now"/>
                                                    </div>
                                                </div> 
                                            </form>
                                            <a href="/profile/<?php echo $activeBiz['BL_Name_SEO'] ?>/<?php echo $activeBiz['BL_ID'] ?>/gallery.php?order=<?php echo $rowG['BFP_Order'] ?>" class="view-gallery">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $rowG['BFP_Photo_195X195']; ?>" alt="<?php echo $rowG['BFP_Title']; ?>"/>
                                            </a>
                                        </li>
                                        <?php
                                        if ($rowG['BFP_Photo_195X195'] != "") {
                                            ?>
                                        <?php } else if ($rowG['BFP_Photo'] != "") { ?>
                                            <li><a href="/profile/<?php echo $activeBiz['BL_Name_SEO'] ?>/<?php echo $activeBiz['BL_ID'] ?>/gallery.php?order=<?php echo $rowG['BFP_Order'] ?>" class="view-gallery">
                                                    <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $rowG['BFP_Photo']; ?>" alt="<?php echo $rowG['BFP_Title']; ?>"/>
                                                </a></li>
                                        <?php }
                                        ?>

                                        <?php
                                    }
                                    ?>
                                </ul>    
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="description stories margin-bottom-none">
        <div class="description-wrapper">
            <div class="description-inner padding-none">
                <div class="listing-detail-left">
                    <div  id="overview" class="listing-desc theme-paragraph ckeditor-anchor-listing">
                        <div style="clear:both" id="descr">
                            <?php if ($activeBiz['BL_Description'] != '') { ?>
                                <a class="a_edit" href="/customer-listing-description-text.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                    <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                </a>
                                <?php echo $activeBiz["BL_Description"]; ?>

                            <?php } else { ?>
                                <div class="add-box-preview" >
                                    <a href="/customer-listing-description-text.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                        +Add Description Text 
                                    </a>
                                </div> 
                            <?php } ?>
                        </div>
                    </div>
                    <?php if ($activeBiz['BL_Trip_Advisor']) { ?>
                        <div id="reviews" class="listing-review">
                            <div class="review-heading">Reviews</div>
                            <?php // echo $activeBiz['BL_Trip_Advisor']   ?>
                            <div style="float: right">
                                <?php if ($activeBiz['BL_Trip_Advisor'] != '') { ?>
                                    <a class="a_edit" href="/customer-listing-trip-advisor.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                        <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                    </a>
                                    <?php echo $activeBiz["BL_Trip_Advisor"]; ?>

                                <?php } ?>
                            </div>  
                        </div>
                    <?php } else { ?>
                        <div id="menu-info" class="listing-desc theme-paragraph">
                            <div class="menu-header-container heading-margin">
                                <div class="menu-heading">Reviews</div>
                            </div>
                            <div class="add-box-preview" style="clear:both">
                                <a href="/customer-listing-trip-advisor.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                    +Add Trip Advisor 
                                </a>
                            </div> 
                        </div>
                    <?php } ?>
                    <!-----MENU START--->
                    <?php if ($count_menu > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 6)) { ?>
                        <div id="menu-info" class="listing-desc theme-paragraph">
                            <div class="menu-header-container">
                                <div class="menu-heading">Menu</div>
                                <div class="menu-Button">
                                    <div class="drop-down-arrow listing"></div>
                                    <select id="menu_key" onchange="show_menu()">
                                        <?php
                                        foreach ($menuSections as $key => $val) {
                                            $sql3 = "SELECT BFM_ID FROM tbl_Business_Feature_Menu 
                                                WHERE BFM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND 
                                                BFM_Type = '" . encode_strings($key, $db) . "'";
                                            $result3 = mysql_query($sql3, $db) or die("Invalid query: $sql3 -- " . mysql_error());
                                            $count = mysql_num_rows($result3);
                                            if ($count > 0) {
                                                ?>
                                                <option value="<?php echo $key ?>"><?php echo $val['title'] ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <?php
                            $sql_des = "Select BFMD_Description, BFMD_Type from tbl_Business_Feature_Menu_Description where  BFMD_BL_ID= '" . encode_strings($activeBiz['BL_ID'], $db) . "'";
                            $result_des = mysql_query($sql_des);
                            $acco = 0;
                            while ($row_des = mysql_fetch_assoc($result_des)) {
                                ?>
                                <div class="menu-desc theme-paragraph meun-hide-show menu-<?php echo $row_des['BFMD_Type']; ?>">
                                    <?php if ($row_des['BFMD_Description'] != '') { ?>
                                        <a class="a_edit" href="/customer-feature-menu.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true&acc=<?php echo $acco ?>">
                                            <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                        </a>
                                        <?php echo $row_des['BFMD_Description']; ?>

                                    <?php } else { ?>
                                        <div class="add-box-preview" >
                                            <a href="/customer-feature-menu.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true&acc=<?php echo $acco ?>">
                                                +Add Description Text 
                                            </a>
                                        </div> 
                                    <?php } ?>

                                </div>
                                <?php
                                $acco++;
                            }
                            $sql = "SELECT BFM_Title, BFM_Description, BFM_Price, BFM_Type FROM tbl_Business_Feature_Menu 
                                WHERE BFM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' 
                                ORDER BY BFM_Order";
                            $result1 = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            $title_type = '';
                            $acco = 0;
                            while ($data = mysql_fetch_assoc($result1)) {
                                if ($title_type != $data['BFM_Type']) {
                                    if ($title_type == "") {
                                        
                                    } else if ($title_type != $data['BFM_Type']) {
                                        echo '</div>';
                                    }
                                    ?>
                                    <div class="type-child">
                                        <?php
                                    }
                                    ?>                   
                                    <div class="menu-desc meun-hide-show menu-<?php echo $data['BFM_Type']; ?>">
                                        <div class="menu-item-heading-container">
                                            <div class="menu-title"><?php echo $data["BFM_Title"]; ?></div>
                                            <div class="menu-price">$<?php echo $data["BFM_Price"]; //REPLACE                                                                                                                                                                          ?></div>
                                        </div>
                                        <?php if ($data['BFM_Description'] != '') { ?>
                                            <a class="a_edit" href="/customer-feature-menu.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true&acc=<?php echo $acco ?>">
                                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                            </a>
                                            <?php echo $data["BFM_Description"]; ?>

                                        <?php } else { ?>
                                            <div class="add-box-preview" >
                                                <a href="/customer-feature-menu.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true&acc=<?php echo $acco ?>">
                                                    +Add Description 
                                                </a>
                                            </div> 
                                        <?php } ?>

                                    </div>
                                    <?php
                                    $title_type = $data['BFM_Type'];
                                    $acco++;
                                }
                                ?>
                            </div>
                        </div>
                        <!-----MENU End--->
                        <!-----About Us start--->
                    <?php } else { ?>
                        <div id="menu-info" class="listing-desc theme-paragraph">
                            <div class="menu-header-container heading-margin">
                                <div class="menu-heading">Menu</div>
                            </div>
                            <div class="add-box-preview" style="clear:both">
                                <a href="/customer-feature-menu.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                    +Add Menu 
                                </a>
                            </div> 
                        </div>
                        <?php
                    }
                    if ($count_about > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 1)) {
                        ?>
                        <div id="about-info" class="listing-desc">
                            <div class="menu-header-container">
                                <div class="menu-heading">About Us</div>
                                <div class="menu-Button">
                                    <div class="drop-down-arrow listing"></div>
                                    <select id="aboutus_id" onchange="show_aboutus()">
                                        <?php
                                        $sql2 = " SELECT BFA_ID, BFA_Title FROM tbl_Business_Feature_About
                                              WHERE BFA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' 
                                              ORDER BY BFA_Order";
                                        $result2 = mysql_query($sql2, $db) or die("Invalid query: $sql1 -- " . mysql_error());
                                        while ($data2 = mysql_fetch_assoc($result2)) {
                                            ?>
                                            <option value="<?php echo $data2['BFA_ID'] ?>"><?php echo $data2['BFA_Title'] ?></option> 
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <?php
                            $sql = "SELECT BFA_ID, BFA_Description FROM tbl_Business_Feature_About
                                LEFT JOIN tbl_Business_Listing ON BL_ID = BFA_BL_ID
                                WHERE BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' ORDER BY BFA_Order";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            $acco = 0;
                            while ($row = mysql_fetch_assoc($result)) {
                                ?>
                                <div class="aboutus-body-detail aboutus-<?php echo $row['BFA_ID'] ?>">
                                    <div class="aboutus-desc theme-paragraph">
                                        <?php if ($row['BFA_Description'] != '') { ?>
                                            <a class="a_edit" href="/customer-feature-about-us.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true&acc=<?php echo $acco ?>">
                                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                            </a>
                                            <?php echo $row["BFA_Description"]; ?>

                                        <?php } else { ?>
                                            <div class="add-box-preview" >
                                                <a href="/customer-feature-about-us.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true&acc=<?php echo $acco ?>">
                                                    +Add Description 
                                                </a>
                                            </div> 
                                        <?php } ?>

                                    </div>

                                    <?php
                                    $sql_photo = " SELECT BFAP_Title, BFAP_Photo, BFAP_Photo_470X320 FROM tbl_Business_Feature_About_Photo
                                               WHERE BFAP_BFA_ID = '" . encode_strings($row['BFA_ID'], $db) . "' 
                                               ORDER BY BFAP_Order";
                                    $result_photo = mysql_query($sql_photo, $db) or die("Invalid query: $sql -- " . mysql_error());
                                    $count_photo = mysql_num_rows($result_photo);
                                    if ($count_photo > 0) {
                                        ?>
                                        <div class="about-image-container">
                                            <?php
                                            while ($data = mysql_fetch_assoc($result_photo)) {
                                                if ($data['BFAP_Photo_470X320'] != "") {
                                                    ?>
                                                    <img class="about-img" src="http://<?php echo DOMAIN . IMG_LOC_REL . $data['BFAP_Photo_470X320']; ?>" alt="<?php echo $data['BFAP_Title']; ?>" longdesc="<?php echo '/'; //REPLACE                                                                                                                                                                     ?>" />
                                                <?php } else if ($data['BFAP_Photo'] != "") { ?>
                                                    <img class="about-img" src="http://<?php echo DOMAIN . IMG_LOC_REL . $data['BFAP_Photo']; ?>" alt="<?php echo $data['BFAP_Title']; ?>" longdesc="<?php echo '/'; //REPLACE                                                                                                                                                                     ?>" />
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                        <?php
                                    }
                                    ?>                                                                       
                                </div>    
                                <?php
                                $acco++;
                            }
                            ?>
                        </div>

                        <!-----About Us End--->
                        <!-----Daily Features start--->
                    <?PHP } else { ?>
                        <div id="menu-info" class="listing-desc theme-paragraph">
                            <div class="menu-header-container heading-margin">
                                <div class="menu-heading">About Us</div>
                            </div>
                            <div class="add-box-preview" style="clear:both">
                                <a href="/customer-feature-about-us.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                    +Add About Us 
                                </a>
                            </div> 
                        </div>
                        <?php
                    }
                    if ($count_DS > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 7)) {
                        ?>
                        <div id="daily-s-check" class="listing-desc">
                            <div class="menu-header-container">
                                <div class="menu-heading">Daily Features</div>
                            </div>
                            <?php
                            $features_bl_id = $activeBiz['BL_ID'];
                            $daily = "SELECT BFDSD_Description FROM `tbl_Business_Feature_Daily_Specials_Description` WHERE BFDSD_BL_ID = '$features_bl_id'";
                            $result1Daily = mysql_query($daily, $db) or die("Invalid query: $daily -- " . mysql_error());
                            $dataresult1Daily = mysql_fetch_assoc($result1Daily);
                            ?>
                            <div class="daily-description theme-paragraph">
                                <?php if ($dataresult1Daily['BFDSD_Description'] != '') { ?>
                                    <a class="a_edit" href="/customer-feature-daily-specials.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                        <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                    </a>
                                    <?php echo $dataresult1Daily["BFDSD_Description"]; ?>

                                <?php } else { ?>
                                    <div class="add-box-preview" >
                                        <a href="/customer-feature-daily-specials.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                            +Add Description 
                                        </a>
                                    </div> 
                                <?php } ?>

                            </div>
                            <?php
                            foreach ($days as $key => $val) {
                                $sql = "SELECT BFDS_Title, BFDS_Description, BFDS_Photo FROM tbl_Business_Feature_Daily_Specials 
                                    WHERE BFDS_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND 
                                    BFDS_Day = '" . encode_strings($key, $db) . "' LIMIT 1";
                                $result1 = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                $data = mysql_fetch_assoc($result1);
                                if ($data) {
                                    ?>
                                    <div class="thumbnail">
                                        <?php
                                        if ($data['BFDS_Photo'] != "") {
                                            $dailyFeaturePhoto = $data['BFDS_Photo'];
                                            ?>
                                            <div class="feature-img-outer">
                                                <img class="temp-width-feature" src="http://<?php echo DOMAIN . IMG_LOC_REL . $dailyFeaturePhoto; ?>" alt="<?php echo $data['BFDS_Title']; //REPLACE                                                                                                            ?>"	longdesc="<?php echo '/'; //REPLACE                                                                                                            ?>" />
                                            </div>
                                            <?php
                                        }
                                        ?>        
                                        <div class="feature-body">
                                            <h5><?php echo $val; ?></h5>
                                            <div class="feature-title"><?php echo $data['BFDS_Title']; //REPLACE                                                                                                          ?></div>
                                            <div class="feature-des">
                                                <?php echo $data['BFDS_Description']; //REPLACE               ?>
                                            </div>

                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <!-----Daily Features End--->
                        <!-----Entertainment start--->
                    <?PHP } else { ?>
                        <div id="menu-info" class="listing-desc theme-paragraph">
                            <div class="menu-header-container heading-margin">
                                <div class="menu-heading">Daily Features</div>
                            </div>
                            <div class="add-box-preview" style="clear:both">
                                <a href="/customer-feature-daily-specials.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                    +Add Daily Features
                                </a>
                            </div> 
                        </div>
                        <?php
                    }
                    if ($count_Ent > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 8)) {
                        ?>               
                        <div id="Entertainment-check" class="listing-desc entert-padding-bottom">
                            <div class="menu-header-container">
                                <div class="menu-heading">Entertainment</div>                            
                                <div class="menu-Button">
                                    <div class="drop-down-arrow listing"></div>
                                    <select id="entert_id" onchange="show_entertainment()">
                                        <?php
                                        $sql = "SELECT BFEA_Time, DATE_FORMAT( BFEA_Time,  '%M' ) AS MONTH  FROM tbl_Business_Feature_Entertainment_Acts 
                                            WHERE BFEA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'  AND BFEA_Time >= CURDATE()
                                            Group by MONTH ORDER BY BFEA_Time ";
                                        $result1 = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                        while ($data2 = mysql_fetch_assoc($result1)) {
                                            ?>
                                            <option value="<?php echo date("M", strtotime($data2['BFEA_Time'])); ?>"><?php echo date("F  Y", strtotime($data2['BFEA_Time'])) ?></option> 
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <?php
                            $enter_bl_id = $activeBiz['BL_ID'];
                            $daily = "SELECT BFED_Description FROM `tbl_Business_Feature_Entertainment_Description` WHERE BFED_BL_ID = '$enter_bl_id'";
                            $result1Daily = mysql_query($daily, $db) or die("Invalid query: $daily -- " . mysql_error());
                            $dataresult1Daily = mysql_fetch_assoc($result1Daily);
                            ?>
                            <div class="entert-desc theme-paragraph">
                                <?php if ($dataresult1Daily['BFED_Description'] != '') { ?>
                                    <a class="a_edit" href="/customer-feature-entertainment.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                        <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                    </a>
                                    <?php echo $dataresult1Daily["BFED_Description"]; ?>

                                <?php } else { ?>
                                    <div class="add-box-preview" >
                                        <a href="/customer-feature-entertainment.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                            +Add Description 
                                        </a>
                                    </div> 
                                <?php } ?>

                            </div>
                            <?php
                            $sql = "SELECT *, LOWER(DATE_FORMAT(BFEA_Time, '%l:%i%p')) as myTime , LOWER(DATE_FORMAT(BFEA_End_Time, '%l:%i%p')) as myendTime FROM tbl_Business_Feature_Entertainment_Acts 
                                WHERE BFEA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'  AND BFEA_Time >= CURDATE()
                                ORDER BY BFEA_Time";
                            $result1 = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            $acco = 0;
                            while ($data = mysql_fetch_assoc($result1)) {
                                ?>

                                <div class="entert-desc entert-hide-show entert-<?php echo date("M", strtotime($data['BFEA_Time'])); ?>">
                                    <div class="entert-date"><?php echo date("F j, Y", strtotime($data['BFEA_Time'])); ?></div>
                                    <div class="entert-title"><?php echo $data['BFEA_Name']; ?></div>                           
                                    <div class="entert-body-desc theme-paragraph">

                                        <?php if ($data['BFEA_Description'] != '') { ?>
                                            <a class="a_edit" href="/customer-feature-entertainment.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true&acc=<?php echo $acco ?>">
                                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                            </a>
                                            <?PHP
                                            if (substr($data['BFEA_Description'], 0, 3) == '<p>') {
                                                echo $data['BFEA_Description'];
                                            } else {
                                                ?>
                                                <p><?php echo preg_replace("~\n~m", "<br>", $data['BFEA_Description']); ?></p>
                                            <?PHP } ?>

                                        <?php } else { ?>
                                            <div class="add-box-preview" >
                                                <a href="/customer-feature-entertainment.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true&acc=<?php echo $acco ?>">
                                                    +Add Description 
                                                </a>
                                            </div> 
                                        <?php } ?>


                                    </div>                           
                                </div>
                                <?php
                                $acco++;
                            }
                            ?>
                        </div>
                        <!-----Entertainment End--->
                    <?php } else { ?>
                        <div id="menu-info" class="listing-desc theme-paragraph">
                            <div class="menu-header-container heading-margin">
                                <div class="menu-heading">Entertainment</div>
                            </div>
                            <div class="add-box-preview" style="clear:both">
                                <a href="/customer-feature-entertainment.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                    +Add Entertainment
                                </a>
                            </div> 
                        </div>
                        <?php
                    }
                    if (mysql_num_rows($resultFAM) > 0) {
                        if ($count_AM > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 13)) {
                            ?>
                            <!-----Agenda start--->
                            <?php
                            $agendaDesc_title = "SELECT * FROM tbl_Feature WHERE F_ID = 13";
                            $resDesc_title = mysql_query($agendaDesc_title, $db) or die("Invalid query: $agendaDesc_title --" . mysql_error());
                            $descAgenda_title = mysql_fetch_assoc($resDesc_title);
                            ?>
                            <div id="Agenda-and-Minutes" class="listing-desc">
                                <div class="menu-header-container">
                                    <div class="menu-heading"><?php echo $descAgenda_title['F_Name']; ?></div>
                                    <div class="menu-Button">
                                        <div class="drop-down-arrow listing"></div>
                                        <select id="agenda_id" onchange="show_agenda()">
                                            <?php
                                            $agenda = "SELECT YEAR(STR_TO_DATE(BFAM_Date, '%Y-%m-%d')) as Agenda_Year FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' GROUP BY Agenda_Year ORDER BY Agenda_Year ASC";
                                            $result_agenda = mysql_query($agenda, $db) or die("Invalid query: $agenda -- " . mysql_error());
                                            while ($rowAgenda = mysql_fetch_assoc($result_agenda)) {
                                                ?>
                                                <option value="<?php echo $rowAgenda['Agenda_Year']; ?>"><?php echo $rowAgenda['Agenda_Year']; ?></option> 
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="agenda-desc theme-paragraph">
                                    <?php echo $descAgenda_title['F_Description']; ?>                            
                                </div>
                                <form action="" onsubmit="search_word();return false;" method ="post">
                                    <div class="agenda-search-from">
                                        <div class="form-inside-div border-none padding-top-bottom agenda-form-main">
                                            <div class="agenda-form">
                                                <input name="searchAgnda" id="search-agenda-key" type="text" placeholder="Search" value="<?php echo $_POST['searchAgnda'] ?>" required>
                                                <input type="submit" name="agenda_form"  value="go"/>
                                            </div>     
                                        </div>
                                    </div>
                                </form>
                                <div class="agenda-min-download-search" style="display: none;">
                                    <div id="search-heading" class="pdf-div-heading " ></div>                                    
                                    <?php
                                    $complete_agenda = "SELECT * FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' ORDER BY BFAM_Date ASC";
                                    $res_cmp_ag = mysql_query($complete_agenda, $db) or die("Invalid query: $complete_agenda -- " . mysql_error());
                                    if (mysql_num_rows($res_cmp_ag) > 0) {
                                        while ($data_agenda = mysql_fetch_assoc($res_cmp_ag)) {
                                            if ($data_agenda['BFAM_Minute'] != "" || $data_agenda['BFAM_Agenda'] != "") {
                                                $keywords = explode(",", $data_agenda['BFAM_Keywords']);
                                                ?>
                                                <div class="agenda-minutes agenda-show-hide <?php
                                                foreach ($keywords as $keysearch) {
                                                    echo 'agenda-' . $keysearch . ' ';
                                                }
                                                ?>" style="display: none;">
                                                    <div class="view-agenda-heading"><?php echo date("F d, Y", strtotime($data_agenda['BFAM_Date'])) ?></div>
                                                    <?php if ($data_agenda['BFAM_Agenda'] != "") { ?><div class="view-agenda-minute"><a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $data_agenda['BFAM_Agenda'] ?>">View Agenda</a></div><?php } ?>
                                                    <?php if ($data_agenda['BFAM_Minute'] != "") { ?><div class="view-agenda-minute"><a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $data_agenda['BFAM_Minute'] ?>">View Minutes</a></div><?php } ?>
                                                </div> 
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                    <div class="agenda-minutes agenda-show-hide agenda-no-result " style="display: none;">No Result Found...</div> 
                                </div>
                                <?php
                                $agenda = "SELECT YEAR(STR_TO_DATE(BFAM_Date, '%Y-%m-%d')) as Agenda_Year FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' GROUP BY Agenda_Year ORDER BY Agenda_Year ASC";
                                $result_agenda = mysql_query($agenda, $db)
                                        or die("Invalid query: $agenda -- " . mysql_error());
                                if (mysql_num_rows($result_agenda) > 0) {
                                    while ($rowAgenda = mysql_fetch_array($result_agenda)) {
                                        ?>
                                        <div class="agenda-min-download agenda-<?php echo $rowAgenda['Agenda_Year']; ?>">
                                            <div class="pdf-div-heading"><?php echo $rowAgenda['Agenda_Year']; ?></div>                                    
                                            <?php
                                            $complete_agenda = "SELECT * FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND YEAR(STR_TO_DATE(BFAM_Date, '%Y-%m-%d')) = '" . encode_strings($rowAgenda['Agenda_Year'], $db) . "' ORDER BY BFAM_Date ASC";
                                            $res_cmp_ag = mysql_query($complete_agenda, $db)
                                                    or die("Invalid query: $complete_agenda -- " . mysql_error());
                                            if (mysql_num_rows($res_cmp_ag) > 0) {
                                                while ($data_agenda = mysql_fetch_assoc($res_cmp_ag)) {
                                                    if ($data_agenda['BFAM_Minute'] != "" || $data_agenda['BFAM_Agenda'] != "") {
                                                        ?>

                                                        <?php if ($data_agenda['BFAM_Minute'] != "" || $data_agenda['BFAM_Agenda'] != "") { ?>
                                                            <a class="a_edit" href="/customer-feature-agenda-minutes.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                                            </a>                                               

                                                        <?php } else { ?>
                                                            <div class="add-box-preview" >
                                                                <a href="/customer-feature-agenda-minutes.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                                                    +Add Agendas & Minutes 
                                                                </a>
                                                            </div> 
                                                        <?php } ?>

                                                        <div class="agenda-minutes">
                                                            <div class="view-agenda-heading"><?php echo date("F d, Y", strtotime($data_agenda['BFAM_Date'])) ?></div>
                                                            <?php if ($data_agenda['BFAM_Agenda'] != "") { ?><div class="view-agenda-minute agenda-pdf"><a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $data_agenda['BFAM_Agenda'] ?>">View Agenda</a></div><?php } ?>
                                                            <?php if ($data_agenda['BFAM_Minute'] != "") { ?><div class="view-agenda-minute minuts-pdf"><a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $data_agenda['BFAM_Minute'] ?>">View Minutes</a></div><?php } ?>
                                                        </div> 
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <!-----agenda End--->                   
                        <?php } else { ?>
                            <div id="menu-info" class="listing-desc theme-paragraph">
                                <div class="menu-header-container heading-margin">
                                    <div class="menu-heading">Agendas & Minutes</div>
                                </div>
                                <div class="add-box-preview" style="clear:both">
                                    <a href="/customer-feature-agenda-minutes.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                        +Add Agendas & Minutes
                                    </a>
                                </div> 
                            </div>
                            <?php
                        }
                    }
                    if (mysql_num_rows($resultFGB) > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 12)) {
                        ?>
                        <div id="guest-book" class="listing-desc">
                            <div class="menu-header-container">
                                <div class="menu-heading">Guest Book</div>
                            </div>
                            <?php
                            $daily = "SELECT BFGB_Description FROM `tbl_Business_Feature_Guest_Book_Description` WHERE BFGB_BL_ID = '" . $activeBiz['BL_ID'] . "'";
                            $result1Daily = mysql_query($daily, $db);
                            $dataresult1Daily = mysql_fetch_assoc($result1Daily);
                            ?>
                            <div class="agenda-desc theme-paragraph">
                                <?php if ($dataresult1Daily['BFGB_Description'] != '') { ?>
                                    <a class="a_edit" href="/customer-feature-guest-book.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                        <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                    </a>
                                    <?php echo $dataresult1Daily['BFGB_Description']; ?>

                                <?php } else { ?>
                                    <div class="add-box-preview" >
                                        <a href="/customer-feature-guest-book.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                            +Add Description 
                                        </a>
                                    </div> 
                                <?php } ?>

                            </div>
                            <ul>
                                <?php
                                $sql = "SELECT * FROM tbl_Business_Feature_Guest_Book WHERE BFGB_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND 
                                                                            BFGB_Status = 1";
                                $result1 = mysql_query($sql, $db);
                                while ($data = mysql_fetch_assoc($result1)) {
                                    ?>
                                    <li class="thumbnail">
                                        <a class="a_edit" href="/customer-feature-guest-book.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                            <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                        </a>                                               
                                        <div class="guest-name theme-paragraph"><?php echo $data['BFGB_Name'] ?></div>
                                        <div class="guest-others theme-paragraph"><?php echo $data['BFGB_Title'] ?></div>
                                        <div class="guest-others">
                                            <?php
                                            $rating = $data['BFGB_Rating'];
                                            if ($rating == '1') {
                                                ?>
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                <?php
                                            } else if ($rating == '2') {
                                                ?>
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                <?php
                                            } else if ($rating == '3') {
                                                ?>
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                <?php
                                            } else if ($rating == '4') {
                                                ?>
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                <?php
                                            } else if ($rating == '5') {
                                                ?>
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>selectr.png">
                                                <?php
                                            } else {
                                                ?>
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>blank.png">
                                                <?php
                                            }
                                            ?></div>
                                        <div class="guest-others theme-paragraph"><?php echo date("F d, Y", strtotime($data['BFGB_Date'])); ?></div>
                                        <div class="guest-others theme-paragraph"><?php echo $data['BFGB_Desc'] ?></div>
                                    </li>
                                    <?php
                                }
                                ?>                                             
                            </ul>
                        </div>
                    <?php } ?>
                    <!-----Location End--->
                </div>
                <div class="listing-detail-right">
                    <?PHP if ($REGION['R_My_Trip'] == 1) { ?>
                        <div class="listing-detail-tripplanner">
                            <div class="address-heading">
                                <?php
                                $sql_b_id = "SELECT BLC_M_C_ID, B_ID
                                        FROM tbl_Business
                                        LEFT JOIN tbl_Business_Listing ON BL_B_ID = B_ID
                                        INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                                        LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                                        LEFT JOIN tbl_Region_Category ON RC_C_ID = BLC_M_C_ID
                                        WHERE BL_ID='" . encode_strings($activeBiz["BL_ID"], $db) . "'";
                                $result_b_id = mysql_query($sql_b_id);
                                $row_b_id = mysql_fetch_array($result_b_id);
                                $sql_remove_trip = "SELECT * FROM  tbl_Session_Tripplanner where ST_Session_ID = '" . encode_strings($_COOKIE["tripplanner"], $db) . "'
                                                AND ST_Bus_ID = " . encode_strings($row_b_id['B_ID'], $db) . "
                                                AND ST_BL_ID = " . encode_strings($activeBiz['BL_ID'], $db) . "
                                                AND ST_Type_ID = " . encode_strings($row_b_id['BLC_M_C_ID'], $db) . "";
                                $result_remove_trip = mysql_query($sql_remove_trip);
                                $count_remove_trip = mysql_num_rows($result_remove_trip);
                                if ($count_remove_trip > 0) {
                                    ?>
                                    <div class="remove-item-tripplanner">
                                        <a href="#" style="line-height: 25px;" class="planner-link" id="<?php echo $row_b_id["BLC_M_C_ID"] . "_" . $row_b_id["B_ID"] . "_" . $activeBiz["BL_ID"] ?>">- Remove from My Trip</a>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="add-item-tripplanner"><a href="#" class="planner-link" id="<?php echo $row_b_id['BLC_M_C_ID'] . "_" . $row_b_id['B_ID'] . '_' . $activeBiz['BL_ID'] ?>">+ Add to My Trip</a></div>
                                    <?PHP
                                }
                                ?>
                            </div>    
                        </div>
                    <?PHP } ?>
                    <div class="listing-detail-address border-top-none" <?php if ($activeBiz['BL_Listing_Type'] == 1) { ?> style="background:#cccccc" <?php } ?>>
                        <?php if ($activeBiz['BL_Street'] != "" || $activeBiz['BL_Town'] != "" || $activeBiz['BL_Province'] != "" || $activeBiz['BL_PostalCode'] != "") { ?>
                            <a class="a_edit" href="/customer-listing-mapping.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                            </a>                                               

                            <div style="float:left;">
                                <div class="listing-detail-add-icon">
                                    <img src="http://<?php echo DOMAIN ?>/images/locationicon.png" alt="Location">
                                </div>
                                <div class="listing-detail-add-detail">


                                    <div  class="address-heading">
                                        <span id="street"><?php echo $activeBiz['BL_Street']; ?> </span>
                                    </div>
                                    <div class="address-heading">    
                                        <span id="town1"><?php echo $activeBiz['BL_Town']; ?> </span>
                                    </div>     
                                    <div class="address-heading">  
                                        <span id="province1"><?php echo $activeBiz['BL_Province']; ?> </span>
                                    </div>   
                                    <div class="address-heading">  
                                        <span id="postalcode"><?php echo $activeBiz['BL_PostalCode']; ?> </span>
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="add-box-preview" >
                                <a href="/customer-listing-mapping.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                    +Add Location Details 
                                </a>
                            </div> 
                        <?php } ?>
                    </div>
                    <?PHP
                    $sql = "SELECT *  
                        FROM tbl_Business_Listing_Ammenity 
                        LEFT JOIN tbl_Ammenity_Icons ON AI_ID = BLA_BA_ID 
                        WHERE BLA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND AI_Image <> '' ORDER BY AI_Name";
                    $resultAI = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    $AIcount = mysql_num_rows($resultAI);

                    $sql_dpdf = "SELECT * FROM tbl_Description_PDF WHERE D_BL_ID='" . encode_strings($activeBiz['BL_ID'], $db) . "' ORDER BY D_PDF_Order ASC";
                    $result_dpdf = mysql_query($sql_dpdf, $db)
                            or die("Invalid query: $sql_dpdf -- " . mysql_error());
                    $pdf_num = mysql_num_rows($result_dpdf);
                    ?>
                    <div class="listing-detail-address border-top-none">
                        <?php if ($activeBiz['BL_Phone'] != "" || $activeBiz['BL_Toll_Free'] != "" || $activeBiz['BL_Fax'] != "" || $activeBiz['BL_Cell'] != "" || $activeBiz['BL_Website'] != "" || $activeBiz['BL_Email'] != "") { ?>
                            <a class="a_edit" href="/customer-listing-contact-details.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                            </a>  

                            <?php if ($activeBiz['BL_Phone']) {
                                ?>
                                <div class="listing-address-container <?php if ($activeBiz['BL_Listing_Type'] == 1) { ?> listing-title-with-bg1 <?php } ?>" >
                                    <div class="listing-detail-add-icon">
                                        <img src="http://<?php echo DOMAIN ?>/images/Listing-Phone.png" alt="">
                                    </div>
                                    <div class="listing-detail-add-detail">
                                        <div class="address-heading">
                                            <span id="phone1"><?php echo $activeBiz['BL_Phone']; ?> </span> 
                                        </div>   
                                    </div>
                                </div>    
                                <?PHP
                            } if ($activeBiz['BL_Toll_Free']) {
                                ?>
                                <div class="listing-address-container">
                                    <div class="listing-detail-add-icon">
                                        <img src="http://<?php echo DOMAIN ?>/images/Listing-Phone.png" alt="">
                                    </div>
                                    <div class="listing-detail-add-detail">
                                        <div class="address-heading">
                                            <span id="tollfree"><?php echo $activeBiz['BL_Toll_Free']; ?> </span>
                                        </div>   
                                    </div>
                                </div>  
                            <?php } if ($activeBiz['BL_Fax']) {
                                ?>
                                <div class="listing-address-container">
                                    <div class="listing-detail-add-icon">
                                        <img src="http://<?php echo DOMAIN ?>/images/fax.png" alt="Fax">
                                    </div>
                                    <div class="listing-detail-add-detail">
                                        <div class="address-heading" style=" margin-top: 3px;">
                                            <span id="fax"><?php echo $activeBiz['BL_Fax']; ?> </span>
                                        </div>   
                                    </div>
                                </div> 
                                <?php
                            }
                            if ($activeBiz['BL_Cell']) {
                                ?>
                                <div class="listing-address-container">
                                    <div class="listing-detail-add-icon">
                                        <img src="http://<?php echo DOMAIN ?>/images/Mobile.png" alt="Mobile">
                                    </div>
                                    <div class="listing-detail-add-detail">
                                        <div class="address-heading" style=" margin-top: 3px;">
                                            <span id="cell"><?php echo $activeBiz['BL_Cell']; ?> </span>
                                        </div>   
                                    </div>
                                </div> 
                                <?php
                            }
                            if ($activeBiz['BL_Website']) {
                                ?>
                                <div class="listing-address-container">
                                    <div class="listing-detail-add-icon">
                                        <img src="http://<?php echo DOMAIN ?>/images/Listing-Website.png" alt="Website">
                                    </div>
                                    <div class="listing-detail-add-detail">
                                        <div class="address-heading listing-leftnav-margin">
                                            <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $activeBiz['BL_Website']) ?>" target="_blank">View Website</a> 
                                        </div>   
                                    </div>
                                </div>
                                <?PHP
                            }
                            if ($activeBiz['BL_Email']) {
                                ?>
                                <div class="listing-address-container <?php if ($activeBiz['BL_Listing_Type'] == 1) { ?> listing-title-with-bg1 <?php } ?>">
                                    <div class="listing-detail-add-icon">
                                        <img src="http://<?php echo DOMAIN ?>/images/Listing-Email.png" alt="Email">
                                    </div>
                                    <div class="listing-detail-add-detail">
                                        <div class="address-heading listing-leftnav-margin">
                                            <a href="mailto:<?php echo $activeBiz['BL_Email']; ?>"><?php echo $activeBiz['BL_Email']; ?></a>
                                        </div>   
                                    </div>
                                </div>
                                <?PHP
                            }
                        } else {
                            ?>
                            <div class="add-box-preview" >
                                <a href="/customer-listing-contact-details.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                    +Add Contact Details 
                                </a>
                            </div> 
                            <?php
                        }

                        if (!$activeBiz['BL_Hours_Disabled'] && ($activeBiz['BL_Hours_Appointment'] == 1 || $activeBiz['BL_Hour_Mon_From'] != '00:00:00' || $activeBiz['BL_Hour_Tue_From'] != '00:00:00' || $activeBiz['BL_Hour_Wed_From'] != '00:00:00' || $activeBiz['BL_Hour_Thu_From'] != '00:00:00' || $activeBiz['BL_Hour_Fri_From'] != '00:00:00' || $activeBiz['BL_Hour_Sat_From'] != '00:00:00' || $activeBiz['BL_Hour_Sun_From'] != '00:00:00')) {
                            ?>
                            <a class="a_edit" href="/customer-hours.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                            </a>  

                            <div class="listing-address-container">
                                <div class="listing-detail-add-icon">
                                    <img src="http://<?php echo DOMAIN ?>/images/Listing-Hours.png" alt="Hours">
                                </div>
                                <div class="listing-detail-add-detail">
                                    <div class="address-heading listing-leftnav-margin accordion view-acc">
                                        <h4>View Hours</h4>
                                        <?php
                                        if ($activeBiz['BL_Hours_Appointment']) {
                                            ?>
                                            <div class="businessListing-timing">
                                                <div class="day-heading">Hours</div>
                                                <div class="day-timing">By Appointment</div>
                                            </div>
                                            <?php
                                        } else {
                                            ?>
                                            <div class="businessListing-timing">
                                                <?php
                                                if ($activeBiz['BL_Hour_Mon_From'] != '00:00:00') {
                                                    ?>
                                                    <div class="day-heading">Monday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($activeBiz['BL_Hour_Mon_From']); ?> <?php echo $activeBiz['BL_Hour_Mon_From'] == '00:00:01' || $activeBiz['BL_Hour_Mon_From'] == '00:00:02' || $activeBiz['BL_Hour_Mon_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Mon_To'], true); ?>
                                                    </div>
                                                <?php } if ($activeBiz['BL_Hour_Tue_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Tuesday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($activeBiz['BL_Hour_Tue_From']); ?> <?php echo $activeBiz['BL_Hour_Tue_From'] == '00:00:01' || $activeBiz['BL_Hour_Tue_From'] == '00:00:02' || $activeBiz['BL_Hour_Tue_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Tue_To'], true); ?>
                                                    </div>
                                                <?php } if ($activeBiz['BL_Hour_Wed_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Wednesday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($activeBiz['BL_Hour_Wed_From']); ?> <?php echo $activeBiz['BL_Hour_Wed_From'] == '00:00:01' || $activeBiz['BL_Hour_Wed_From'] == '00:00:02' || $activeBiz['BL_Hour_Wed_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Wed_To'], true); ?>
                                                    </div>
                                                <?php } if ($activeBiz['BL_Hour_Thu_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Thursday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($activeBiz['BL_Hour_Thu_From']); ?> <?php echo $activeBiz['BL_Hour_Thu_From'] == '00:00:01' || $activeBiz['BL_Hour_Thu_From'] == '00:00:02' || $activeBiz['BL_Hour_Thu_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Thu_To'], true); ?>
                                                    </div>
                                                <?php } if ($activeBiz['BL_Hour_Fri_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Friday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($activeBiz['BL_Hour_Fri_From']); ?> <?php echo $activeBiz['BL_Hour_Fri_From'] == '00:00:01' || $activeBiz['BL_Hour_Fri_From'] == '00:00:02' || $activeBiz['BL_Hour_Fri_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Fri_To'], true); ?>
                                                    </div>
                                                <?php } if ($activeBiz['BL_Hour_Sat_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Saturday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($activeBiz['BL_Hour_Sat_From']); ?> <?php echo $activeBiz['BL_Hour_Sat_From'] == '00:00:01' || $activeBiz['BL_Hour_Sat_From'] == '00:00:02' || $activeBiz['BL_Hour_Sat_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Sat_To'], true); ?>
                                                    </div>
                                                <?php } if ($activeBiz['BL_Hour_Sun_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Sunday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($activeBiz['BL_Hour_Sun_From']); ?> <?php echo $activeBiz['BL_Hour_Sun_From'] == '00:00:01' || $activeBiz['BL_Hour_Sun_From'] == '00:00:02' || $activeBiz['BL_Hour_Sun_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Sun_To'], true); ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>   
                                </div>
                            </div>
                        <?PHP } else {
                            ?>
                            <div class="listing-address-container">
                                <div class="add-box-preview" >
                                    <a href="/customer-hours.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                        +Add Hours 
                                    </a>
                                </div> 
                            </div>
                        <?php }
                        ?>
                        <?php
                        if ($AIcount > 0) {
                            ?>
                            <a class="a_edit" href="/customer-listing-ammenities.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                            </a>  
                            <div class="listing-address-container">
                                <div class="listing-detail-add-icon">
                                    <img src="http://<?php echo DOMAIN ?>/images/Listing-Ammenity.png" alt="Ammenity">
                                </div>
                                <div class="listing-detail-add-detail">
                                    <div class="address-heading listing-leftnav-margin accordion view-acc">                               
                                        <h4>View Amenities</h4>
                                        <div class="businessListing-timing">
                                            <?php
                                            while ($rowAI = mysql_fetch_assoc($resultAI)) {
                                                ?>
                                                <div class="acc-body">
                                                    <?php echo $rowAI['AI_Name']; ?>
                                                </div>    
                                            <?PHP } ?>
                                        </div> 
                                    </div>   
                                </div>                      
                            </div>
                        <?php } else {
                            ?>
                            <div class="listing-address-container">
                                <div class="add-box-preview" >
                                    <a href="/customer-listing-ammenities.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                        +Add Amenities 
                                    </a>
                                </div> 
                            </div>
                        <?php } ?>
                        <div class="listing-address-container">
                            <?php
                            if ($pdf_num > 0) {
                                ?>
                                <a class="a_edit" href="/customer-listing-upload-pdf.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                    <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                </a> 
                                <div class="listing-detail-add-icon">
                                    <img src="http://<?php echo DOMAIN ?>/images/Listing-Download.png" alt="Download">
                                </div>
                                <div class="listing-detail-add-detail">
                                    <div class="address-heading listing-leftnav-margin accordion view-acc">
                                        <h4>View Downloads</h4>
                                        <div class="businessListing-timing">
                                            <?php
                                            while ($data_dpdf = mysql_fetch_assoc($result_dpdf)) {
                                                ?>
                                                <div class="acc-body theme-downloads"><a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $data_dpdf['D_PDF'] ?>"><?php echo $data_dpdf['D_PDF_Title']; ?></a></div>
                                                <?php
                                            }
                                            ?>
                                        </div>   
                                    </div>   
                                </div>

                            <?php } else { ?>
                                <div class="add-box-preview" >
                                    <a href="/customer-listing-upload-pdf.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                        +Upload PDF 
                                    </a>
                                </div> 
                            <?php } ?> </div>
                    </div>
                    <?php
                    $social_blld = $activeBiz['BL_ID'];
                    $sql_social = "SELECT * FROM  tbl_Business_Social where BS_BL_ID=$social_blld";
                    $social_result = mysql_query($sql_social);
                    $row_soical = mysql_fetch_assoc($social_result);
                    ?>
                    <div class="listings-social-icon">
                        <?php
                        if ($row_soical['BS_FB_Link'] || $row_soical['BS_T_Link'] || $row_soical['BS_Y_Link'] || $row_soical['BS_I_Link']) {
                            ?>                        
                            <a class="a_edit" href="/customer-social-media.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                            </a> 

                            <?php if ($row_soical['BS_FB_Link'] != '') { ?>
                                <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $row_soical['BS_FB_Link']) ?>" target="_blank"><img src="http://<?php echo DOMAIN ?>/images/facebook.png"/></a>
                                <?php
                            }
                            if ($row_soical['BS_I_Link'] != '') {
                                ?> 
                                <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $row_soical['BS_I_Link']) ?>" target="_blank"><img src="http://<?php echo DOMAIN ?>/images/instagram.png"/></a>

                                <?php
                            }
                            ?>                                        
                            <?php
                            if ($row_soical['BS_T_Link'] != '') {
                                $twitter = explode("?", $row_soical['BS_T_Link']);
                                ?>
                                <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $twitter[0]) ?>" target="_blank"><img src="http://<?php echo DOMAIN ?>/images/twitter.png"/></a>

                            <?php }
                            ?>

                        <?php } else { ?>
                            <div class="add-box-preview" >
                                <a href="/customer-social-media.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                    +Add Social Media 
                                </a>
                            </div> 
                        <?php } ?>
                    </div>
                </div>
                <!--right end -->
            </div>
        </div>
    </section>
    <style>
        .ui-accordion .ui-accordion-header{
            padding:0px;
        }
        .ui-accordion .ui-accordion-icons {
            padding-left: 0px; 
        }
    </style>

</div>

<?PHP
require_once '../include/my/footer.php';
?>