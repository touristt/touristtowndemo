<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/ranking.inc.php';

$BID = $_SESSION['BUSINESS_ID'];
$ID = $_REQUEST['a_id'];

if ($BID > 0) {
    
} else {
    header('Location: index.php');
}

if ($ID > 0) {
    $sql = "SELECT BL_Listing_Title, BL_Street, BL_Address, BL_Town, BL_Province, BL_PostalCode, AB_Deleted, AB_Invoice_Num, AB_Active_Date, AB_CoC_Dis, 
            AB_SubTotal, AB_Tax, AB_Total, AB_Renewal_Date, A_Title, BLCR_BLC_R_ID, AT_Name, AT_Cost, card_number FROM tbl_Advertisement_Billing 
            LEFT JOIN tbl_Advertisement ON A_ID = AB_A_ID
            LEFT JOIN tbl_Business_Listing ON BL_ID= A_BL_ID
            LEFT JOIN payment_profiles ON listing_id=A_BL_ID
            LEFT JOIN tbl_Business_Listing_Category  ON BLC_BL_ID = BL_ID
            INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID 
            LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID
            WHERE AB_ID = '" . encode_strings($ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowBus = mysql_fetch_assoc($result);
    $region = $rowBus['BLCR_BLC_R_ID'];
    $sql_region = "SELECT R_Clientname, R_Street, R_Town, R_Province, R_PostalCode FROM tbl_Region WHERE R_ID = $region LIMIT 1";
    $res_region = mysql_query($sql_region);
    $rowreg = mysql_fetch_assoc($res_region);
}

if ($rowBus['AB_Deleted'] == 0 && $ID > 0) {
    
} else {
    header("Location:advert-billing.php");
    exit();
}

require_once '../include/my/header.php';
?>
<div class="content-left advert">

    <?php require_once '../include/nav-B-advertisement.php'; ?>

    <div class="title-link">
        <div class="title">Invoices</div>
    </div>
    <div class="right">
        <div class="page-print-adv">
        <div class="billing-inside-div-tittle">
            <div class="data-content advert-bill-invoice-detail">
                <div class="data-column invoice-heading">Invoice Number : </div>
                <div class="data-column invoice-no"><?php echo $rowBus['AB_Invoice_Num'] ?></div>
                <div class="data-column invoice-date">Date Paid: </div>
                <div class="data-column invoice-date-bb"><?php echo $rowBus['AB_Active_Date'] ?></div>
            </div>
        </div>
        <div class="invoice-address advert-bill-header">
            <div class="data-left  inviced-left">
                <div class="data-header-inv bold inviced-head">Invoiced To</div>
                <div class="data-content-address margine-inviced-bottom">
                    <?php echo $rowBus['BL_Listing_Title'] != '' ? trim($rowBus['BL_Listing_Title']) : ''; ?>
                    <?php echo $rowBus['BL_Street'] != '' ? '<br>' . trim($rowBus['BL_Street']) : ''; ?>
                    <?php echo $rowBus['BL_Address'] != '' ? '<br>' . trim($rowBus['BL_Address']) : ''; ?>
                    <?php
                    echo $rowBus['BL_Town'] != '' ? '<br>' . trim($rowBus['BL_Town']) : '';
                    echo $rowBus['BL_Province'] != '' ? ', ' . trim($rowBus['BL_Province']) : '';
                    echo $rowBus['BL_PostalCode'] != '' ? ', ' . trim($rowBus['BL_PostalCode']) : '';
                    ?>
                    <br> Canada
                </div>
            </div>
            <div class="data-right">
                <div class="data-header-inv bold pay-head">Pay To</div>
                <div class="data-content-address margine-inviced-bottom">
                    <?php
                    echo $rowreg['R_Clientname'] != '' ? trim($rowreg['R_Clientname']) : '';
                    echo $rowreg['R_Street'] != '' ? '<br>' . trim($rowreg['R_Street']) : '';
                    echo $rowreg['R_Town'] != '' ? '<br>' . trim($rowreg['R_Town']) : '';
                    echo $rowreg['R_Province'] != '' ? ', ' . trim($rowreg['R_Province']) : '';
                    echo $rowreg['R_PostalCode'] != '' ? ', ' . trim($rowreg['R_PostalCode']) : '';
                    ?>
                    <br>Canada

                </div>
            </div>
        </div>
        <div class="invoice-pay-main advert-bill-pay-main">
            <div class="data-column bold payment-text type-width">Payment Type :</div>
            <div class="data-column card-name"><?php echo $rowBus['PT_Name'] ?></div>
        </div>
        <div class="card-main">
            <div class="data-column bold last-card-digits">Last four digits of card :</div>
            <?php
            $cardno = explode("XXXX", $rowBus['card_number']);
            ?>
            <div class="data-column last-card-digits-no"><?php echo $cardno[1] ?></div>
        </div>
        
            <div class="data-header bold inviced-detail inviced-order-heading">Order Summary</div>
            <div class="data-content">
                <div class="data-column inviced-tools-margin "><?php echo $rowBus['A_Title'] . " (" . $rowBus['AT_Name'] . ")" ?></div>
                <div class="data-column">$<?php echo $rowBus['AT_Cost'] ?> </div>
            </div>
            <div class="data-content">
                <div class="data-column bold inviced-sub-margin">Discount</div>
                <div class="data-column">$<?php echo $rowBus['AB_CoC_Dis'] ?></div>
            </div>
            <div class="data-header sub-total-color">
                <div class="data-column bold inviced-sub-margin ">Sub-Total</div>
                <div class="data-column sub-total-color">$<?php echo $rowBus['AB_SubTotal'] ?> </div>
            </div>
            <div class="data-content ">
                <div class="data-column bold inviced-sub-margin">Taxes (13%)</div>
                <div class="data-column">$<?php echo $rowBus['AB_Tax'] ?></div>
            </div>
            <div class="data-header total-invice-color ">
                <div class="data-column bold inviced-sub-margin total-invice-color">Total</div>
                <div class="data-column total-invice-color">$<?php echo $rowBus['AB_Total'] ?></div>
            </div>
            <div class="data-content">
                <div class="data-column renewel-date"><?php echo $rowBus['AB_Renewal_Date'] ?></div>
                <div class="data-column bold renewel-text">Your next invoice date is :</div>
            </div>

        </div>
        <div class="data-content print-link">
            <div class="print-button">
                <a href="#" onclick="window.print();">Print Invoice</a>
            </div>
        </div>
    </div>
</div>
</div>

<?PHP
require_once '../include/my/footer.php';
?>