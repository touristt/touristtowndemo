<?php
require_once '../include/config.inc.php';

$title = "Password Reset";

require_once '../include/my/header.php';
?>
<div class="SectionHeader">Password Reset</div>
<div class="login-container">
    <form name="form1" method="post" action="http://<?php echo BUSINESS_DOMAIN . BUSINESS_DIR; ?>">
        <div class="forget-pass-container-inside-div">
            <input name="logop" type="hidden" id="logop" value="send_pw">
            <div class="login-detail-title-responsive">
                <div class="signin-title-responsive-page">
                    Forget Password 
                </div>
            </div>
            <div class="login-from-inside-div">
                <div class="from-inside-div-label" >Email Address: </div>
                <div class="from-inside-div-feild"><input name="USER_EMAIL" required size="35" placeholder="Email Address" type="email" id="USER_EMAIL"></div>
            </div>
            <div class="login-from-inside-div">
                <div class="from-inside-div-label" ></div>
                <div class="from-inside-div-button">
                    <input type="submit" name="Submit" value="Submit">
                </div>
            </div>
            <div class="login-from-inside-div forget_pass_page_links">
                <p class="link-password" align="center">
                    <a class="margin-left" href="http://<?php echo BUSINESS_DOMAIN . BUSINESS_DIR; ?>">Back to Business Login</a> | <a href="http://<? echo DOMAIN ; ?>">Public Site </a>
                </p>
            </div>
        </div>
    </form>
</div>
<?php
require_once '../include/my/footer.php';
?>