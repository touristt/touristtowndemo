<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

if ($_GET['op'] == 'del') {
    require_once '../include/picUpload.inc.php';
    $BL_ID = $_GET['bl_id'];
    $BFP_ID = $_REQUEST['id'];

    $sql = "DELETE tbl_Business_Feature_Photo 
            FROM tbl_Business_Feature_Photo 
            WHERE BFP_ID = '" . encode_strings($BFP_ID, $db) . "' AND BFP_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Photo Gallery', '', 'Delete Image', 'user admin');
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Photo_Gallery', $BFP_ID);
    } else {
        $_SESSION['delete_error'] = 1;
    }
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /listings-preview.php?bl_id=" . $BL_ID);   
    }
   else
   {
   header("Location: customer-feature-gallery.php?bl_id=" . $BL_ID );
   }
//    header("Location: customer-feature-gallery.php?bl_id=" . $BL_ID);
    exit();
}
?>

