<?PHP
ob_start();
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];
$points_taken = '';

if ($BL_ID > 0) {
    $sql = "SELECT BL_ID, BL_Listing_Title, BL_Description, BL_Listing_Type, BL_Contact, BL_Phone, BL_Website, BL_Toll_Free, BL_Email, BL_Street, BL_Town, 
            BL_Province, BL_PostalCode, BL_Lat, BL_Long, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Description, BL_SEO_Keywords, 
            BL_Search_Words, BL_Video FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}

//TO GET THE CORRESPONDING REGIONS OF CURRENT LISTING
if (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
    $regionLimit = array();
    $limits = explode(',', $_SESSION['USER_LIMIT']);
    foreach ($limits as $limit) {
        $sql = "SELECT R_ID, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $region = mysql_fetch_assoc($result);
        if (isset($region['R_Type']) && $region['R_Type'] == 1) {
            $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($region['R_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                if ($row['R_ID'] != '' && !in_array($row['R_ID'], $regionLimit)) {
                    $regionLimit[] = $row['R_ID'];
                }
            }
        } else {
            if ($region['R_ID'] != '' && !in_array($region['R_ID'], $regionLimit)) {
                $regionLimit[] = $region['R_ID'];
            }
        }
    }
}
$regionLimitCommaSeparated = rtrim(implode(',', $regionLimit), ',');

if (isset($BL_ID) && $BL_ID > 0) {
    $sql = "SELECT BLCR_BLC_R_ID, BLCR_ID FROM tbl_Business_Listing_Category_Region  WHERE BLCR_BL_ID = " . encode_strings($BL_ID, $db);
    $sql .= $regionLimitCommaSeparated ? (" AND BLCR_BLC_R_ID IN(" . $regionLimitCommaSeparated . ") ") : "";
    $resultRegion = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($rowRegion = mysql_fetch_assoc($resultRegion)) {
        ?>
        <?php
        $sql = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Type NOT IN(1,6)";
        $sql .= $regionLimitCommaSeparated ? (" AND R_ID IN(" . $regionLimitCommaSeparated . ") ") : "";
        $sql .= " ORDER BY R_Name";
        $resultRegionList = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        ?>                                      
        <?PHP
        while ($rowRList = mysql_fetch_assoc($resultRegionList)) {
            if ($rowRList['R_ID'] == $rowRegion['BLCR_BLC_R_ID']) {
                $regs[] = $rowRList['R_ID'] . ',' . $rowRList['R_Name'];
            }
        }
    }
}
//print_r($regs);die;
if ($_POST['op'] == 'save') {
    
    $BL_ID = $_REQUEST['bl_id'];
    $R_ID = $_REQUEST['r_id'];
    $sql = "SELECT * FROM tbl_Business_Listing_Photo WHERE BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BLP_R_ID = '" . encode_strings($R_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $val = mysql_fetch_assoc($result);
    
    require '../include/picUpload.inc.php';
    //Thumbnail Image
    if ($_POST['thumb_photo'] == "") {        
        // last @param 21 = Listing Thumbnail Image
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 21, 'Listing', $BL_ID);
        if (is_array($pic)) {
            if ($val['BLP_BL_ID'] > 0 && $val['BLP_R_ID'] > 0) {
                $sql = "UPDATE tbl_Business_Listing_Photo SET BLP_Photo = '" . encode_strings($pic['0']['0'], $db) . "', BLP_Mobile_Photo = '" . encode_strings($pic['0']['1'], $db) . "' WHERE BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BLP_R_ID = '" . encode_strings($R_ID, $db) . "'";
                $result = mysql_query($sql, $db);
            } else {
                $sql = "INSERT tbl_Business_Listing_Photo SET BLP_Photo = '" . encode_strings($pic['0']['0'], $db) . "',
                    BLP_Mobile_Photo = '" . encode_strings($pic['0']['1'], $db) . "',
                    BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                    BLP_R_ID = '" . encode_strings($R_ID, $db) . "' ";
                $result = mysql_query($sql, $db);
            }
        }
        $pic_id = $pic['1'];
    } else {
        $pic_id = $_POST['thumb_photo'];
        // last @param 21 = Listing Thumbnail Image
        $pic_response = Upload_Pic_Library($pic_id, 21);
        if ($pic_response) {
            if ($val['BLP_BL_ID'] > 0 && $val['BLP_R_ID'] > 0) {
                $sql = "UPDATE tbl_Business_Listing_Photo SET BLP_Photo = '" . encode_strings($pic_response['0'], $db) . "', BLP_Mobile_Photo = '" . encode_strings($pic_response['1'], $db) . "' WHERE BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BLP_R_ID = '" . encode_strings($R_ID, $db) . "'";
                $result = mysql_query($sql, $db);
            } else {
               $sql = "INSERT tbl_Business_Listing_Photo SET BLP_Photo = '" . encode_strings($pic_response['0'], $db) . "',
                   BLP_Mobile_Photo = '" . encode_strings($pic_response['1'], $db) . "',
                    BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                    BLP_R_ID = '" . encode_strings($R_ID, $db) . "'";
                $result = mysql_query($sql, $db);
            }
        }
    }
    if ($result && $pic_id > 0) {
        //Image usage from image bank.
        imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Main_Photo', 'Listing Thumbnail Image');
    }
    
    $sql = "SELECT * FROM tbl_Business_Listing_Photo WHERE BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BLP_R_ID = '" . encode_strings($R_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $val = mysql_fetch_assoc($result);

    //Main Image
    if ($_POST['main_photo'] == "") {
        // last @param 22 = Listing Main Image
        $pic1 = Upload_Pic('1', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 22, 'Listing', $BL_ID);
        if (is_array($pic1)) {

            if ($val['BLP_BL_ID'] > 0 && $val['BLP_R_ID'] > 0) {
                $sql1 = "UPDATE tbl_Business_Listing_Photo SET BLP_Header_Image = '" . encode_strings($pic1['0']['0'], $db) . "',
                    BLP_Mobile_Header_Image = '" . encode_strings($pic1['0']['1'], $db) . "'
                    WHERE BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BLP_R_ID = '" . encode_strings($R_ID, $db) . "'";
                $result1 = mysql_query($sql1, $db);
            } else {
                $sql1 = "INSERT tbl_Business_Listing_Photo SET BLP_Header_Image = '" . encode_strings($pic1['0']['0'], $db) . "',BLP_Mobile_Header_Image = '" . encode_strings($pic1['0']['1'], $db) . "',BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "',BLP_R_ID = '" . encode_strings($R_ID, $db) . "'";
                $result1 = mysql_query($sql1, $db);
            }
        }
        $pic_id1 = $pic1['1'];
        
    } else {
        $pic_id1 = $_POST['main_photo'];
        // last @param 22 = Listing Main Image
        $pic_response1 = Upload_Pic_Library($pic_id1, 22);
        if ($pic_response1) {
            if ($val['BLP_BL_ID'] > 0 && $val['BLP_R_ID'] > 0) {
                $sql1 = "UPDATE tbl_Business_Listing_Photo SET BLP_Header_Image = '" . encode_strings($pic_response1['0'], $db) . "',
                    BLP_Mobile_Header_Image = '" . encode_strings($pic_response1['1'], $db) . "'
                    WHERE BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "'  AND BLP_R_ID = '" . encode_strings($R_ID, $db) . "'";
                $result1 = mysql_query($sql1, $db);
            } else {
                $sql1 = "INSERT tbl_Business_Listing_Photo SET BLP_Header_Image = '" . encode_strings($pic_response1['0'], $db) . "',
                   BLP_Mobile_Header_Image = '" . encode_strings($pic_response1['1'], $db) . "',
                       BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "',BLP_R_ID = '" . encode_strings($R_ID, $db) . "'";
                $result1 = mysql_query($sql1, $db);
            }
        }
    }
    $sql_video = "UPDATE tbl_Business_Listing_Photo SET BLP_Video='" . encode_strings($_POST['home_video'], $db) . "' WHERE BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "'  AND BLP_R_ID = '" . encode_strings($R_ID, $db) . "'";
    $result_video = mysql_query($sql_video, $db);
    if ($result1 && $pic_id1 > 0) {
        //Image usage from image bank.
        imageBankUsage($pic_id1, 'IBU_BL_ID', $BL_ID, 'IBU_Main_Photo', 'Listing Main Image');
    }
    if ($result || $result1 || $result_video) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Main Photos / Video','','Update','user admin');
    } else {
        $_SESSION['error'] = 1;
    }
    //update points only for listing
    update_pointsin_business_tbl($BL_ID);
    header("Location: customer-listing-main-photo.php?bl_id=" . $BL_ID);
    exit();
} elseif (isset($_GET['op']) && $_GET['op'] == 'del') {  
     require_once '../include/picUpload.inc.php';
    $BL_ID = $_REQUEST['bl_id'];
    $R_ID = $_REQUEST['r_id'];
    
    if ($_REQUEST['type'] == 0) {
        $del = "BLP_Photo = ''";
        $photo_type = 'Listing Thumbnail Image';
    } else {
        
        $del = "BLP_Header_Image = '', BLP_Mobile_Header_Image = ''";
        $photo_type = 'Listing Main Image';
    }
    $sql = "UPDATE tbl_Business_Listing_Photo SET $del WHERE BLP_BL_ID = '$BL_ID' AND BLP_R_ID = '$R_ID'";
    $result = mysql_query($sql);
    if ($result) {
        $_SESSION['delete'] = 1;
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Main_Photo', $photo_type);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Main Photos / Video','','Delete Photo','user admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    //update points only for listing
    update_pointsin_business_tbl($BL_ID);
    header("Location: customer-listing-main-photo.php?bl_id=" . $BL_ID);
    exit();
}

require_once '../include/my/header.php';
?>
<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">My Page</div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div>
        <div class="instruction">
            Fields with this background<span></span>will show on free listings profile.
        </div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-B-mypage.php'; ?>
    </div>
    <div class="right">
        <div class="listing-inside-div-tittle">
        </div>
        
        <?php if (sizeof($regs) == 0) { ?>
            <div class="form-inside-div border-none main-photos">
                <div class="content-sub-header">
                    <div class="title">No Website Selected</div>
                </div>
                <div class="form-inside-div margin-none border-none">Kindly select a website to upload Main Header Photo</div>

            </div> 

            <?php
        } else {
        foreach ($regs as $key => $value) {
            $reg = explode(",", $value);
            ?>
        
        <form action="" onSubmit="return check_img_size(21, 10000000, 22)" method="post" enctype="multipart/form-data" name="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="r_id" value="<?php echo $reg[0] ?>">
            <!--<input type="hidden" id="listing_type" value="<?php echo $rowListing['BL_Listing_Type'] ?>">-->
            <div class="content-header">Main Photos / Video (<?php echo $reg[1] ?>)
            </div>
            <?PHP
                $sql = "SELECT * FROM tbl_Business_Listing_Photo WHERE BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BLP_R_ID = '" . encode_strings($reg[0], $db) . "' LIMIT 1";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                $rowListing = mysql_fetch_assoc($result);
            ?>
            <div class="form-inside-div" style="border:none;">
                <div class="content-sub-header">
                    <div class="title">Thumbnail</div>
                    <div class="link">
                        <?php
                        $thumbnail = show_field_points('Thumbnail');
                        if ($rowListing['BLP_Photo']) {
                            echo '<div class="points-com">' . $thumbnail . ' pts</div>';
                            $points_taken = $thumbnail;
                        } else {
                            echo '<div class="points-uncom">' . $thumbnail . ' pts</div>';
                        }
                        ?>
                    </div>
                </div>

                <div class="form-inside-div margin-none border-none">
                    <div class="div_image_library listing-main-photo">
                        <span class="daily_browse thumbnail-button" onclick="show_image_library(21<?php echo $key ?>, <?php echo $BL_ID; ?>)">Select File</span>
                        <input type="hidden" name="thumb_photo" id="image_bank21<?php echo $key ?>" value="">
                        <input type="file" onchange="show_file_name(21<?php echo $key ?>, this)" name="pic[]" id="photo21<?php echo $key ?>" style="display: none;">
                        <div class="cropped-container">
                            <div class="image-editor">
                                <div class="cropit-image-preview-container margin-left-thumnail">
                                    <div class="cropit-image-preview thumb-photo-perview">
                                        <img class="preview-img thumb-preview preview-img-script21<?php echo $key ?>" style="display: none;" src="">    
                                        <?php if ($rowListing['BLP_Photo'] != "") { ?>
                                            <img class="existing-img existing_imgs21<?php echo $key ?> thumb-preview max-width-thumbnail" src="http://<?php echo DOMAIN . IMG_LOC_REL . $rowListing['BLP_Photo'] ?>">
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if ($rowListing['BLP_Photo'] != "") { ?><div class="data-column delete-main-photo1 padding-none thumbnail-button"><a class="deletePhoto margin-left-main" onClick="return confirm('Are you sure?');"  href="customer-listing-main-photo.php?bl_id=<?php echo $BL_ID ?>&type=0&op=del&r_id=<?php echo $reg[0] ?>">Delete Photo</a></div><?php } ?>
                    </div>
                </div>
            </div>
            
            <div class="form-inside-div" style="border:none;">
                <div class="content-sub-header">
                    <div class="title">Main Image</div>
                    <div class="link">
                        <?php
                        $mainPhoto = show_field_points('Main Image');
                        if ($rowListing['BLP_Header_Image']) {
                            echo '<div class="points-com">' . $mainPhoto . ' pts</div>';
                            $points_taken = $mainPhoto;
                        } else {
                            echo '<div class="points-uncom">' . $mainPhoto . ' pts</div>';
                        }
                        ?>
                    </div>
                </div>
                 <div class="form-inside-div margin-none border-none">
                    <div class="cropped-container1">
                        <div class="cropit-image-preview main-preview">
                            <img class="preview-img preview-img-script22<?php echo $key ?>" style="display: none;" src="">    
                            <?php if ($rowListing['BLP_Header_Image'] != "") { ?>
                                <img class="existing-img existing_imgs22<?php echo $key ?> max-width-thumbnail" src="http://<?php echo DOMAIN . IMG_LOC_REL . $rowListing['BLP_Header_Image'] ?>">
                            <?php } ?>
                        </div>
                    </div>
                    <div class="div_image_library main-image-buttons">
                        <span class="daily_browse" onclick="show_image_library(22<?php echo $key ?>, <?php echo $BL_ID; ?>)">Select File</span>
                        <input type="hidden" name="main_photo" id="image_bank22<?php echo $key ?>" value="">
                        <input type="file" onchange="show_file_name(22<?php echo $key ?>, this)" name="pic[]" id="photo22<?php echo $key ?>" style="display: none;">
                        <?php if ($rowListing['BLP_Header_Image'] != "") { ?><div class="data-column delete-main-photo1 padding-none"><a class="deletePhoto margin-left-main" onClick="return confirm('Are you sure?');"  href="customer-listing-main-photo.php?bl_id=<?php echo $BL_ID ?>&type=1&op=del&r_id=<?php echo $reg[0] ?>">Delete Photo</a></div><?php } ?>
                    </div>
                </div>

            </div>
            <div class="form-inside-div">
                <label>Video</label>
                <div class="form-data">
                    <input name="home_video"  type="text" value="<?php echo $rowListing['BLP_Video'] ?>" size="50" />
                </div>
            </div>

            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" name="button" id="button" value="Save" />
                </div>
            </div>

            
        </form> 
        <?php }
        }
        ?>
        <div class="form-inside-div border-none main-photos">
            <div class="content-sub-header">
                <div class="title">Image Uploading Tips</div>
            </div>
            <?php
            $help_text = show_help_text('Main Photos');
            if ($help_text != '') {
                echo '<div class="form-inside-div margin-none border-none">' . $help_text . '</div>';
            }
            ?>
        </div>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>

<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>