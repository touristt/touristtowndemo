<?php
require_once '../include/config.inc.php';

$title = "Password Reset";
$hash_code = $_SESSION['hash_code'];

require_once '../include/my/header.php';
?>
<div class="SectionHeader">Password Reset</div>
<div class="login-container">
    <form name="form1" method="post" action="http://<?php echo BUSINESS_DOMAIN . BUSINESS_DIR; ?>">
        <div class="forget-pass-container-inside-div">
            <input name="logop" type="hidden" id="logop" value="change_password">
            <input name="regain_pass" type="hidden"  value="<?php echo $hash_code; ?>">
            <div class="login-detail-title-responsive">
                <div class="signin-title-responsive-page">
                    Forget Password
                </div>
            </div>
            <?php
            if (isset($_SESSION['mismatch_regain_password']) && $_SESSION['mismatch_regain_password'] == 1) {
                ?>
                <div class="login-from-inside-div">
                    <div>Password Mismatch<br>
                        </a></div>
                </div>
                <?php
                unset($_SESSION['mismatch_regain_password']);
            }
            if (isset($_SESSION['error_regain_password']) && $_SESSION['error_regain_password'] == 1) {
                ?>
                <div class="login-from-inside-div">
                    <div>Error saving data. Please try again.<br>
                        </a></div>
                </div>
                <?php
                unset($_SESSION['error_regain_password']);
            }
            ?>
            <div class="login-from-inside-div margin-top-pass-from">
                <div class="from-inside-div-label" >Password: </div>
                <div class="from-inside-div-feild"><input name="new_password" required size="35" placeholder="New Password" type="password" ></div>
            </div>
            <div class="login-from-inside-div margin-top-pass-from">
                <div class="from-inside-div-label" >Confirm Password: </div>
                <div class="from-inside-div-feild"><input name="confirm_password" required size="35" placeholder="Confirm Password" type="password" ></div>
            </div>
            <div class="login-from-inside-div">
                <div class="from-inside-div-label" ></div>
                <div class="from-inside-div-button">
                    <input type="submit" name="Submit" value="Submit">
                </div>
            </div>
    </form>
</div>
<?php
require_once '../include/my/footer.php';
?>