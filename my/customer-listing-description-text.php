<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

$error = empty($_SESSION['error']) ? '' : $_SESSION['error'];

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];
$points_taken = 0;

if ($BL_ID > 0) {
    $sql = "SELECT BL_ID, BL_Listing_Title, BL_Description, BL_Listing_Type, BL_Contact, BL_Phone, BL_Website, BL_Toll_Free, BL_Email, BL_Street, BL_Town, 
            BL_Province, BL_PostalCode, BL_Lat, BL_Long, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Description, BL_SEO_Keywords, 
            BL_Search_Words FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}

if ($_POST['op'] == 'save') {
    $sql = "tbl_Business_Listing SET
            BL_Description = '" . encode_strings($_POST['maindescription'], $db) . "'";
    if ($BL_ID > 0) {
        $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Description Text','','Update','user admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing',$id,'Description Text','','Add','user admin');
    }
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /listings-preview.php?bl_id=" . $BL_ID);   
    } else
    {
    header("Location: customer-listing-description-text.php?bl_id=" . $BL_ID);
    }
    
    exit();
}
require_once '../include/my/header.php';
?>
<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">My Page</div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div>
        <div class="instruction">
            Fields with this background<span></span>will show on free listings profile.
        </div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-B-mypage.php'; ?>
    </div>
    <div class="right">
        <form action="" method="post" enctype="multipart/form-data" name="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <div class="content-header">
                <div class="title">Description Text</div>
                <div class="link">
                    <?php
                    $desc = show_field_points('Description');
                    if ($rowListing['BL_Description']) {
                        echo '<div class="points-com des-heading-point">' . $desc . ' pts</div>';
                        $points_taken = $desc;
                    } else {
                        echo '<div class="points-uncom">' . $desc . ' pts</div>';
                    }
                    ?>
                </div>
            </div>
            <div class="form-inside-div border-none bottom-pading-none">
                <div class="form-data">
                    <textarea name="maindescription" cols="55" rows="10" id="description-listing"><?php echo $rowListing['BL_Description'] ?></textarea>                           
                </div>
            </div>
            <script>
                $(document).ready(function () {
                    CKEDITOR.config.width = 555;
                });
            </script>
            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" name="button2" value="Save Now"/>
                </div>  
            </div>

            <div class="form-inside-div listing-ranking border-none">
                Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_desc_points ?> points
            </div>

            <div class="form-inside-div border-none">
                <div class="content-sub-header">
                    <div class="title">Writing Content Tips</div>
                </div>
                <?php
                $help_text = show_help_text('Description');
                if ($help_text != '') {
                    echo '<div class="form-inside-div margin-none border-none">' . $help_text . '</div>';
                }
                ?>
            </div>
        </form>
    </div>
</div>

<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>