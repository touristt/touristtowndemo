<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/adminFunctions.inc.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    $sql = "SELECT BL_Billing_Type, BL_CoC_Dis_2, BL_Line_Item_Title, BL_Line_Item_Price FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}
if ($_REQUEST['id']) {
    $id = $_REQUEST['id'];
    /* check for 30 days */
    $before_limit = "SELECT BLF_Date FROM tbl_BL_Feature WHERE BLF_ID = '" . encode_strings($id, $db) . "'";
    $res_lmt = mysql_query($before_limit, $db) or die("Invalid query: $before_limit -- " . mysql_error());
    $row_date = mysql_fetch_assoc($res_lmt);
    $cur_date = strtotime(date('Y-m-d'));
    $BLF_Date = strtotime($row_date['BLF_Date']);
    $dateDiff = abs($BLF_Date - $cur_date);
    $numberDays = $dateDiff / 86400;
    $resDays = intval($numberDays);
    if ($resDays > 30) {
        $getStatus = "SELECT BLF_Active FROM tbl_BL_Feature WHERE BLF_ID = '" . encode_strings($id, $db) . "'";
        $resultStatus = mysql_query($getStatus, $db) or die("Invalid query: $getStatus -- " . mysql_error());
        $rowStatus = mysql_fetch_assoc($resultStatus);
        $BLF_Active = ($rowStatus['BLF_Active'] == 1) ? "0" : "1";
        $del = "UPDATE tbl_BL_Feature SET BLF_Active = '" . $BLF_Active . "' WHERE BLF_ID = '" . encode_strings($id, $db) . "'";
        $result = mysql_query($del, $db) or die("Invalid query: $del -- " . mysql_error());

        if ($result) {
            $_SESSION['addon'] = 'deleted'.$BLF_Active;
        }

        listing_billing($rowListing['BL_Billing_Type'], $BL_ID, $rowListing['BL_CoC_Dis_2'], $rowListing['BL_Line_Item_Title'], $rowListing['BL_Line_Item_Price']);
//update points only for one listing
        update_pointsin_business_tbl($BL_ID);
        if (isset($_SESSION['success'])) {
            unset($_SESSION['success']);
        }
        header('location: customer-feature-add-ons.php?bl_id=' . $BL_ID);
        exit;
    } else {
        $_SESSION['addon_activation_error'] = 1;
    }
}

require_once '../include/my/header.php';
?>
<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">Add Ons</div>
        <div class="link">
            <?PHP
            require_once ('preview-link.php');
            ?>
        </div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-B-listing.php'; ?>
    </div>
    <div class="right">
        <div class="listing-inside-div-tittle">
        </div>
        <div class="content-header">
            <div class="title">Add ons</div>
        </div>
        <?php
        $help_text = show_help_text('Add Ons');
        if ($help_text != '') {
            echo '<div class="form-inside-div border-none">' . $help_text . '</div>';
        }
        ?>
    </div>
</div>
<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>