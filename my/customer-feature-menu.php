<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];
$go_to_preview = $_REQUEST['go_to_preview'];
if ($BL_ID > 0) {
    
} else {
    header('Location: index.php');
}
if (isset($_POST['button33'])) {
    $menutype = $_REQUEST['menu-type'];
    $menutypeid = $_REQUEST['menu-des-type'];
    $title = $_REQUEST['title'];
    $id = $_REQUEST['bl_id'];
    $des_acc_id = $_REQUEST['acc_id'];
    $descriptionNEW = $_REQUEST['descriptionNEW'];
    $price = ($_REQUEST['priceNEW']) ? $_REQUEST['priceNEW'] : '0.00';
    $sql_check = "Select BFM_ID, BFM_BL_ID from tbl_Business_Feature_Menu where BFM_ID = '$menutypeid' AND BFM_BL_ID = '$id'";
    $result_check = mysql_query($sql_check);
    $count_des = mysql_num_rows($result_check);
    $sql_op = "tbl_Business_Feature_Menu SET                     
            BFM_Title = '$title',
            BFM_Description = '$descriptionNEW',
            BFM_Price = '$price'"; 
    if ($count_des > 0) {
        $sql_op = "UPDATE " . $sql_op . " WHERE BFM_ID ='$menutypeid' AND BFM_BL_ID = '$id'";
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing',$id,'Menu',$des_acc_id,'Update Item','user admin');
    } else {
        $sql_op = "INSERT " . $sql_op . " ,BFM_Type  = '$menutype', BFM_BL_ID = '$id'";
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing',$id,'Menu',$des_acc_id,'Add Item','user admin');
         update_pointsin_business_tbl($id);
    } 
    $result_op = mysql_query($sql_op);

    if ($result_op) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /listings-preview.php?bl_id=" . $BL_ID);   
    }
   else
   {
        header("location: customer-feature-menu.php?bl_id=$id&acc=$des_acc_id");
   }
    
    exit;
 }
if (isset($_REQUEST['save_des'])) { 
    $des_type_id = $_REQUEST['menu-des-type'];
    $des_bl_id = $_REQUEST['menu-des-id'];
    $des_acc_id = $_REQUEST['acc_id'];
    $description = $_REQUEST['descriptionNEW_TYPE'];
    $price = ($_REQUEST['price']) ? $_REQUEST['price'] : '0.00';
    $sql_check = "Select BFMD_ID from tbl_Business_Feature_Menu_Description where BFMD_Type='$des_type_id' AND BFMD_BL_ID= $BL_ID";
    $result_check = mysql_query($sql_check);
    $count_des = mysql_num_rows($result_check);
    $sql_op = "tbl_Business_Feature_Menu_Description SET                        
            BFMD_Description = '$description'";
    if ($count_des > 0) {
        $sql_op = "UPDATE " . $sql_op . " WHERE BFMD_BL_ID = '$des_bl_id' AND BFMD_Type = '$des_type_id'";
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Menu',$des_acc_id,'Update Description','user admin');
    } else {
        $sql_op = "INSERT " . $sql_op . " ,BFMD_BL_ID = '$des_bl_id', 
            BFMD_Type = '$des_type_id' ";
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Menu',$des_acc_id,'Add Description','user admin');
    } 
    $result_op = mysql_query($sql_op);
    if ($result_op) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /listings-preview.php?bl_id=" . $BL_ID);   
    }
   else
   {
        header("location: customer-feature-menu.php?bl_id=$des_bl_id&acc=$des_acc_id");
   }
    
    exit;
}
$activeAccordion = '';
if (isset($_GET['acc']) && $_GET['acc'] != '') {
    $activeAccordion = $_GET['acc'];
} else {
    $activeAccordion = 'false';
}
require_once '../include/my/header.php';
?>
<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">Add Ons</div>
        <div class="link">
            <?PHP
            require_once ('preview-link.php');
            ?>
        </div>
    </div>

    <div class="left">
        <?PHP
        require '../include/nav-B-listing.php';
        ?>
    </div>
    <div class="right">

        <div class="content-header">
            <div class="title">Menu</div>
            <div class="link">
                <?php
                $Menu = show_addon_points(6);
                if ($numRowsMenu > 0) {
                    echo '<div class="points-com">' . $Menu . ' pts</div>';
                } else {
                    echo '<div class="points-uncom">' . $Menu . ' pts</div>';
                }
                ?>
            </div>
        </div>

        <?php
        $help_text = show_help_text('Menu');
        if ($help_text != '') {
            echo '<div class="form-inside-div border-none">' . $help_text . '</div>';
        }
        ?>
        <div class="menu-items-accodings">                         
            <div id="accordion">
                <?PHP
                $acco = 0;
                foreach ($menuSections as $key => $val) {
                    ?>
                    <h3 class="accordion-rows"><?php echo $val['title'] ?><label class="add-item-accordion"><a onclick="show_form('<?php echo $key; ?>')">+ Add Item</a></label></h3> 
                    <div class="sub-accordions accordion-padding">
                        <form name="menu-item" method="post" action=""  style="float:left;">
                            <input type="hidden"  name="menu-des-type" value="<?php echo $key ?>">
                            <input type="hidden"  name="menu-des-id" value="<?php echo $BL_ID ?>">
                             <input type="hidden" name="go_to_preview" value="<?php echo $_REQUEST['go_to_preview'] ?>">
                            <input type="hidden"  name="acc_id" value="<?php echo $acco; ?>">
                            <div class="form-inside-div add-menu-item-new ">
                                <?php
                                $sql_des = "Select BFMD_Description from tbl_Business_Feature_Menu_Description where BFMD_Type='$key' AND BFMD_BL_ID= $BL_ID";
                                $result_des = mysql_query($sql_des);
                                $row_des = mysql_fetch_assoc($result_des);
                                ?>
                                <label>Description</label>
                                <div class="form-data add-menu-item-field wiziwig-menu">
                                    <textarea name="descriptionNEW_TYPE" cols="40" rows="5" wrap="VIRTUAL" class="formtext textarea-width-menu" id="descriptionmain<?php echo $key ?>"><?php echo $row_des['BFMD_Description']; ?></textarea>
                                </div>
                            </div>
                            <div class="form-inside-div">
                                <div class="button menu-new-btn">
                                    <input type="submit" name="save_des" value="Save Item" />
                                </div>
                            </div>   
                        </form>
                        <form name="menu-item" method="post" action="" id="gallery-form<?php echo $key ?>" style="display: none;">
                            <input type="hidden" id="menu-type-<?php echo $key ?>" name="menu-type" value="<?php echo $key ?>">
                            <input type="hidden" id="menu-type-id-<?php echo $BL_ID ?>" name="menu-type-id" value="<?php echo $BL_ID ?>">
                             <input type="hidden" name="go_to_preview" value="<?php echo $_REQUEST['go_to_preview'] ?>">
                            <input type="hidden"  name="acc_id" value="<?php echo $acco; ?>">
                            <div class="form-inside-div add-menu-item-new">
                                <label>Name</label>
                                <div class="form-data add-menu-item-field menu-input-width">
                                    <input name="title" type="text" id="title<?php echo $key ?>" size="50" required/>
                                </div>
                            </div>
                            <div class="form-inside-div add-menu-item-new wiziwig-menu">
                                <label>Description</label>
                                <div class="form-data add-menu-item-field">
                                    <textarea name="descriptionNEW" cols="40" rows="5" wrap="VIRTUAL" class="formtext textarea-width-menu" id="descriptionNEW<?php echo $key ?>"></textarea>
                                </div>
                            </div>
                            <div class="form-inside-div add-menu-item-new">
                                <label>Price<span>$</span></label>
                                <div class="form-data add-menu-item-field menu-input-width">
                                    <input name="priceNEW" type="text" id="priceNEW<?php echo $key ?>" value="" size="5" />
                                </div>
                            </div>
                            <div class="form-inside-div add-menu-item-image margin-bottom-prod margin-top-newp">
                            </div>
                            <div class="form-inside-div add-menu-item-image">
                            </div>
                            <div class="form-inside-div">
                                <div class="button menu-new-btn">
                                    <input type="submit" name="button33" class="button3<?php echo $key ?>" value="Save Item" />
                                </div>
                            </div>   
                        </form>
                        <script>
                            CKEDITOR.disableAutoInline = true;
                            $(document).ready(function () {
                                var keys = '<?php echo $key ?>';
                                $('#descriptionNEW' + keys).ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
                                $('#descriptionmain' + keys).ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
                            });
                        </script>
                        <?PHP
                        $sql = "SELECT * FROM tbl_Business_Feature_Menu WHERE BFM_BL_ID = '$BL_ID' AND 
				BFM_Type= '$key' ORDER BY BFM_Order";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        $counter = 1;
                        $numRows = mysql_num_rows($result);
                        ?>
                        <div id="sortable-container">
                            <ul class="brief menu-order-sortable add-menu-item-container">
                                <?php
                                while ($data = mysql_fetch_assoc($result)) {
                                    ?>                    
                                    <li class="menu-order-sortable" id="recordsArray_<?php echo $data['BFM_ID'] ?>">
                                        <form name="menu-item" method="post" action=""  style="float:left;">
                                            <input type="hidden"  name="menu-des-type" value="<?php echo $data['BFM_ID'] ?>">
                                            <input type="hidden"  name="menu-des-id" value="<?php echo $BL_ID ?>">
                                             <input type="hidden" name="go_to_preview" value="<?php echo $_REQUEST['go_to_preview'] ?>">
                                            <input type="hidden"  name="acc_id" value="<?php echo $acco; ?>">
                                        <div class="form-inside-div add-menu-item-new">
                                            <label>Name</label>
                                                <div class="form-data add-menu-item-field menu-input-width ">
                                                    <input name="title" type="text" id="title-<?php echo $data['BFM_ID'] ?>" value="<?php echo $data['BFM_Title'] ?>" size="25" required/>
                                            </div>
                                        </div>
                                        <div class="form-inside-div add-menu-item-new wiziwig-menu">
                                            <label>Description</label>
                                            <div class="form-data add-menu-item-field">
                                                    <textarea name="descriptionNEW" cols="40" rows="7" wrap="VIRTUAL" class="formtext textarea-width-menu" id="description-<?php echo $data['BFM_ID'] ?>"><?php echo $data['BFM_Description'] ?></textarea>
                                            </div>
                                        </div>
                                        <div class="add-menu-item-container">
                                            <div class="form-inside-div add-menu-item-new">
                                                <label>Price<span>$</span></label>
                                                <div class="form-data add-menu-item-field menu-input-width">
                                                        <input name="priceNEW" type="text" id="price-<?php echo $data['BFM_ID'] ?>" value="<?php echo $data['BFM_Price'] ?>" size="5" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-inside-div add-menu-item-button">
                                            <div class="button product-butn-replace">
                                                    <input type="submit" name="button33" id="button<?php echo $data['BFM_ID'] ?>" class="margin-right-btn button<?php echo $data['BFM_ID'] ?>" value="Save Item" />
                                                <input type="button" onclick="deleteitem(<?php echo $data['BFM_ID'] ?>, <?php echo $acco; ?>)" value="Delete Item"/>
                                            </div>
                                        </div>
                                        <div class="accordion-separator"></div>
                                    </li>
                                    </form>
                                    <script>
                                        function deleteitem(BFM_item_ID, ACTIVE_ACC) {
                                            var meunid = <?php echo $BL_ID ?>;
                                            var go_to_preview = '<?php echo $go_to_preview ?>';
                                            var menutypeid = $('#menu-type-id-' + meunid).val();
                                            if (confirm("Are you sure you want to delete item?")) {
                                                $.post("customer-feature-menu-delete.php", {
                                                    BFM_ID_item: BFM_item_ID,
                                                    BL_ID: meunid,
                                                    menutypeid: menutypeid
                                                }, function (result) {                                                    
                                                    if (result == 1 && (go_to_preview == '' || go_to_preview == null)) {
                                                        window.location.href = "customer-feature-menu.php?bl_id=<?php echo $BL_ID; ?>&acc=" + ACTIVE_ACC;
                                                    }else{
                                                        $(location).attr('href', 'listings-preview.php?bl_id=<?php echo $BL_ID ?>')
                                                    }                                                    
                                                });
                                            } else {
                                                return false;
                                            }
                                        }
                                        CKEDITOR.disableAutoInline = true;
                                        $(document).ready(function () {
                                            var Ck_bfm_id = <?php echo $data['BFM_ID'] ?>;
                                            $('#description-' + Ck_bfm_id).ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
                                        });
                                    </script>
                                    <?PHP
                                    $counter++;
                                }
                                ?>
                            </ul>
                        </div>
                    </div>    
                    <?PHP
                    $acco++;
                }
                ?>
            </div> 
        </div>

        <?php
        $sql1 = "SELECT * FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = 6";
        $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
        $row1 = mysql_fetch_assoc($result1);
        if ($row1['BLF_BL_ID'] != '') {
            ?>
            <div class="form-inside-div border-none text-align-center">
                <a href="customer-feature-add-ons.php?id=<?php echo $row1['BLF_ID'] ?>&bl_id=<?php echo $BL_ID ?>"  onclick="return confirm('Are you sure you want to <?php echo $row1['BLF_Active'] == 1 ? "deactivate" : "activate" ?> this Add on?');"><?php echo $row1['BLF_Active'] == 1 ? "Deactivate" : "Activate" ?> This Add on</a>
            </div>
            <?PHP
        }
        ?>

    </div>
    <div id="dialog-message"></div>
</div>    
<script>
    $(function () {
        $("#accordion").accordion({
            collapsible: true,
            heightStyle: 'content',
            active: <?php echo $activeAccordion; ?>
        });
        $('.add-item-accordion > a').click(function (event) {
            if ($(this).parent().parent().hasClass("ui-accordion-header-active")) {
                event.stopPropagation(); // this is
            }
            event.preventDefault(); // the magic
        });
    });
    function show_form(PS_ID) {
        $('#gallery-form' + PS_ID).show();
    }
    $(function () {
        $("ul.brief").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Business_Feature_Menu&field=BFM_Order&id=BFM_ID';
                $.post("reorder.php", order, function (theResponse) {
                    $("#dialog-message").html("Menu Items Re-Ordered");
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                });
            }
        });
    });
</script>
<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>