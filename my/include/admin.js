// JavaScript Document
$(document).ready(function () {
    //Ajax Loader
    $(document).ajaxStart(function () {
        $('.custom-ajax').removeClass("custom-ajax").addClass('custom-ajax-show');
    }).ajaxStop(function () {
        $('.custom-ajax-show').removeClass("custom-ajax-show").addClass('custom-ajax');
    });

    $(".datepicker").datepicker({
        firstDay: 1,
        dateFormat: 'yy-mm-dd'
    });
    $(".previous-date-not-allowed").datepicker({
        minDate: 0,
        firstDay: 1,
        dateFormat: 'yy-mm-dd',
        prevText: "",
        nextText: ""
    });
     $(".button input[type='submit']").click(function() {
        $('.custom-ajax').removeClass("custom-ajax").addClass('custom-ajax-show');
        setTimeout(function() { 
        $('.custom-ajax-show').removeClass("custom-ajax-show").addClass('custom-ajax');
        }, 2000);
      });
    $(".phone_number").mask("999-999-9999");
    $(".phone_toll").mask("9-999-999-9999");

    $("input#latitude").blur(function (event) {
        var regex = /^-?([0-8]?[0-9]|90)\.[0-9]{1,6}$/;
        var lat = $(this).val();
        if (lat != '' && !regex.test(lat)) {
            alert("Invalid Latitude");
            $(this).val('');
        }
    });
    $("input#longitude").blur(function (event) {
        var regex = /^-?((1?[0-7]?|[0-9]?)[0-9]|180)\.[0-9]{1,6}$/;
        var lon = $(this).val();
        if (lon != '' && !regex.test(lon)) {
            alert("Invalid Longitude");
            $(this).val('');
        }
    });
    /*mask phone number*/
    if ($(".phone").length > 0) {
        $(".phone").mask("(999) 999.9999");
    }
    //ckeditor
    CKEDITOR.disableAutoInline = true;
    //$('#description-listing').ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
    $('#description-listing').ckeditor({ 
        customConfig: 'http://touristtowndemo.com/include/plugins/ckeditor/config.js'
    });
    $('.tt-description').ckeditor({
     customConfig: 'http://touristtowndemo.com/include/plugins/ckeditor/config.js'   
    });
    $('.tt-ckeditor').ckeditor({
     customConfig: 'http://touristtowndemo.com/include/plugins/ckeditor/config.js'   
    });
    $('#special_case').ckeditor({
        customConfig: 'http://touristtowndemo.com/include/plugins/ckeditor/ckeditor.config.js'
    });
    $('#email-template').ckeditor({
        customConfig: 'http://touristtowndemo.com/include/plugins/ckeditor/email.config.js'
    });
});


function limitTextArea(myID, myCount)
{
    if ($("#" + myID).val().length > myCount) {
        $("#" + myID).val($("#" + myID).val().substr(0, myCount));
    }
}

function validatePassword(password, optional, password1)
{
    if (optional == true && password.length == 0 && password1.length == 0) {
        return true;
    } else {
        if (password.length < 6) {
            alert("Password too short! At least 6 chacters.");
            return false;
        }
        if (!password.match("[0-9]")) {
            alert("Password must contain at least one digit!");
            return false;
        }
        if (!password.match("[A-Z]")) {
            alert("Password must contain at least one uppercase letter!");
            return false;
        }
        if (!password.match("[a-z]")) {
            alert("Password must contain at least one lowercase letter!");
            return false;
        }
        if (password != password1) {
            alert("Password does not match!");
            return false;
        }
    }

}

function isClosed(myVar, myValue) {
    if (myValue == '00:00:01' || myValue == "00:00:02" || myValue == "00:00:03") {
        $("#" + myVar).hide();
    } else {
        $("#" + myVar).show();
    }
}
function deleteProductType(myValue) {
    $('.rowPT' + myValue).remove();
    return false;
}

function deleteProductImage(myValue, myID, rowID) {
    if (confirm('Are you sure?')) {
        $('#file' + rowID).remove();
        $.post('/include/removeFile.php', {
            id: myID,
            type: 'products',
            file: myValue
        });
    }
    return false;
}

function deleteContentImage(myValue, myID, rowID) {
    if (confirm('Are you sure?')) {
        $('#file' + rowID).remove();
        $.post('/include/removeFile.php', {
            id: myID,
            type: 'fb',
            file: myValue
        });
    }
    return false;
}

function changeFavouriteRecipeVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        alert('Yes');
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'FavouriteRecipe',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'FavouriteRecipe',
            status: 0
        });
    }
    return false;
}

function changeProductVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'product',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'product',
            status: 0
        });
    }
    return false;
}

function changeLearningVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'learn',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'learn',
            status: 0
        });
    }
    return false;
}
function changeGalleryVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'gallery',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'gallery',
            status: 0
        });
    }
    return false;
}
function changeShowroomVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'showroom',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'showroom',
            status: 0
        });
    }
    return false;
}
function changeTestimonialVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'testimonial',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'testimonial',
            status: 0
        });
    }
    return false;
}

function deleteServiceImage(myValue, myID, rowID) {
    if (confirm('Are you sure?')) {
        $('#file' + rowID).remove();
        $.post('/include/removeFile.php', {
            id: myID,
            type: 'services',
            file: myValue
        });
    }
    return false;
}
function changeServiceVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'service',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'service',
            status: 0
        });
    }
    return false;
}
function deleteCustomImage(myValue, myID, rowID) {
    if (confirm('Are you sure?')) {
        $('#file' + rowID).remove();
        $.post('/include/removeFile.php', {
            id: myID,
            type: 'custom',
            file: myValue
        });
    }
    return false;
}

function deleteGroupHPImage(myValue, myID, rowID) {
    if (confirm('Are you sure?')) {
        $('#file' + rowID).remove();
        $.post('/include/removeFile.php', {
            id: myID,
            type: 'grouphp',
            file: myValue
        });
    }
    return false;
}
function changeCustomVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'custom',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'custom',
            status: 0
        });
    }
    return false;
}

function changeBrandVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'brand',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'brand',
            status: 0
        });
    }
    return false;
}


function changeInstallProcedureVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'installpro',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'installpro',
            status: 0
        });
    }
    return false;
}
function changeServiceProcedureVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'servicepro',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'servicepro',
            status: 0
        });
    }
    return false;
}

function changeCollateralVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'collateral',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'collateral',
            status: 0
        });
    }
    return false;
}

function changeCollateralVendorVisible(myID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'collateralVendor',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            type: 'collateralVendor',
            status: 0
        });
    }
    return false;
}

function changeCollateralSizeVisible(myID, pID) {
    if ($("#visiblity-" + myID).attr("checked") == "checked") {
        $.post('/include/changeVisibility.php', {
            id: myID,
            cid: pID,
            type: 'collateralSize',
            status: 1
        });
    } else {
        $.post('/include/changeVisibility.php', {
            id: myID,
            cid: pID,
            type: 'collateralSize',
            status: 0
        });
    }
    return false;
}

function changeflyerdate(myID, mydate, fID) {

    $.post('/include/changeDate.php', {
        id: myID,
        update: mydate,
        updateid: fID
    });
    location.reload();
    return false;
}

function MM_preloadImages() { //v3.0
    var d = document;
    if (d.images) {
        if (!d.MM_p)
            d.MM_p = new Array();
        var i, j = d.MM_p.length, a = MM_preloadImages.arguments;
        for (i = 0; i < a.length; i++)
            if (a[i].indexOf("#") != 0) {
                d.MM_p[j] = new Image;
                d.MM_p[j++].src = a[i];
            }
    }
}

function MM_swapImgRestore() { //v3.0
    var i, x, a = document.MM_sr;
    for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++)
        x.src = x.oSrc;
}

function MM_findObj(n, d) { //v4.01
    var p, i, x;
    if (!d)
        d = document;
    if ((p = n.indexOf("?")) > 0 && parent.frames.length) {
        d = parent.frames[n.substring(p + 1)].document;
        n = n.substring(0, p);
    }
    if (!(x = d[n]) && d.all)
        x = d.all[n];
    for (i = 0; !x && i < d.forms.length; i++)
        x = d.forms[i][n];
    for (i = 0; !x && d.layers && i < d.layers.length; i++)
        x = MM_findObj(n, d.layers[i].document);
    if (!x && d.getElementById)
        x = d.getElementById(n);
    return x;
}

function MM_swapImage() { //v3.0
    var i, j = 0, x, a = MM_swapImage.arguments;
    document.MM_sr = new Array;
    for (i = 0; i < (a.length - 2); i += 3)
        if ((x = MM_findObj(a[i])) != null) {
            document.MM_sr[j++] = x;
            if (!x.oSrc)
                x.oSrc = x.src;
            x.src = a[i + 2];
        }
}

function viewVideo(data) {
    var recipe = window.open('', 'VideoWindow', 'width=640,height=480');
    var html = '<html><head><title>View Video</title></head><body><div id="video">' + data + '</div></body></html>';
    recipe.document.open();
    recipe.document.write(html);
    recipe.document.close();
    return false;
}

//Recomendation Page
function get_subcat() {
    var cat_id = document.getElementById('recommendation-cat').value;
    $.ajax({
        type: "GET",
        url: "get-customer-listing-subcat.php",
        data: {
            cat_id: cat_id
        }
    })
            .done(function (msg) {
                document.getElementById('recommendation-subcats').style.visibility = 'visible';
                document.getElementById('recommendation-subcats').innerHTML = msg;
                $('#form').submit();
            });
}

function show_textbox(bl_id) {
    $("#recommendation-form" + bl_id).attr("title", "Add Recommendation").dialog({
        modal: true,
        draggable: false,
        resizable: false,
        width: 500
    });
}
function show_textbox_existing(rid) {
    $("#existing-recommendation-form" + rid).attr("title", "Edit Recommendation").dialog({
        modal: true,
        draggable: false,
        resizable: false,
        width: 500
    });
}
function view_recommendation(rid) {
    $("#view-my-recommendation" + rid).attr("title", "View Recommendation").dialog({
        modal: true,
        draggable: false,
        resizable: false,
        width: 500
    });
}

//Photo Gallery Page (Listings)
function show_form() {
    $('#gallery-form').show();
}

function show_description(bfp_id) {
    $("#gallery-description-form" + bfp_id).attr("title", "Edit Description").dialog({
        modal: true,
        draggable: false,
        resizable: false,
        width: 500
    });
}
function show_video() {
    $("#video").dialog({
        modal: true,
        draggable: false,
        resizable: false,
        position: ['center'],
        show: 'blind',
        hide: 'blind',
        width: 700,
        height: 335,
        open: function () {
            jQuery('.ui-widget-overlay').bind('click', function () {
                jQuery('#video').dialog('close');
            })
        }
    });
    $(".ui-dialog-titlebar").hide();
}
function show_form(key) {
    $('#menu-item' + key).show();
}

function get_cat_advert_update() {
    var cat_id = document.getElementById('advert-cat').value;
    var region_id = document.getElementById('region-id').value;
    var advert_id = document.getElementById('advert-id').value;
    var advert_type = document.getElementById('advert-type').value;
    $.ajax({
        type: "GET",
        url: "get-customer-advert-cat-update.php",
        data: {
            cat_id: cat_id,
            website_id: region_id,
            advert_id: advert_id,
            advert_type: advert_type
        }
    }).done(function (msg) {
        if (msg == 1) {
            location.reload();
        } else {
            document.getElementById('advert-subcats').innerHTML = msg;
        }
    });
}
function get_subcat_advert_update() {
    var subcat_id = document.getElementById('advert-subcat-load').value;
    var advert_id = document.getElementById('advert-id').value;
    var cat_id = document.getElementById('advert-cat').value;
    $.ajax({
        type: "GET",
        url: "get-customer-advert-subcat-update.php",
        data: {
            subcat_id: subcat_id,
            advert_id: advert_id,
            cat_id: cat_id
        }
    }).done(function (msg) {
        location.reload();
    });
}
function get_subcat_update() {
    var subcat_id = document.getElementById('advert-subcat').value;
    var advert_id = document.getElementById('advert-id').value;
    var cat_id = document.getElementById('advert-cat').value;
    $.ajax({
        type: "GET",
        url: "get-customer-advert-subcat-update.php",
        data: {
            subcat_id: subcat_id,
            advert_id: advert_id,
            cat_id: cat_id
        }
    }).done(function (msg) {
        location.reload();
    });
}
var dialog_box = "";
function choose_file(dialog_id) {
    dialog_box = dialog_id;
    jQuery('.image_bank_input_file').val('');
    $(".dialog" + dialog_id).attr("title", "Choose Photo From").dialog({
        width: 500,
        height: 200,
        modal: true,
        draggable: false,
        resizable: false,
        open: function () {
            jQuery('.ui-widget-overlay').bind('click', function () {
                jQuery('#dialog' + dialog_id).dialog('close');
            });
        }
    });
}
function show_main_image(image_id) {
    $(".image-pop-up" + image_id).attr("title", "Image").dialog({
        width: 500,
        modal: true,
        draggable: false,
        resizable: false
    });
}
function show_image_library(image_id, bl_id, iteration,limit) {
    var itr;
    if (iteration > 0) {
        itr = iteration;
    } else {
        itr = 0;
    }
    $.ajax({
        url: "show_image_bank.php",
        type: "GET",
        data: {
            image_id: image_id,
            bl_id: bl_id,
            iteration: itr,
            limit:limit
        },
        cache: false,
        success: function (html) {
            $("#image-library").html(html);
        }
    });
    var dWidth = $(window).width() * 0.95;
    var dHeight = $(window).height() * 0.95;
    jQuery('.dialog' + dialog_box).dialog('close');
    $("#image-library").attr("title", "Library").dialog({
        width: dWidth,
        height: dHeight,
        modal: true,
        draggable: false,
        resizable: false,
        open: function () {
            $("body").css("overflow", "hidden");
            jQuery('.ui-widget-overlay').bind('click', function () {
                jQuery('#image-library').dialog('close');
            });
        },
        close: function () {
            $('body').css('overflow', 'auto');
        }
    });
}

$(document).ready(function () {
    $('.dialog-close').on("click", function () {
        $(this).dialog("close");
    });
    //Getting name of the file when upload
    $('.setting-name').on('change', function (event, files, label) {
        var file_name = this.value.replace(/\\/g, '/').replace(/.*\//, '');
        $('#file-upload-name').text('');
        $('#file-upload-name').show();
        $('#file-upload-name').text(file_name);
    });
});
function select_image(id, image_id, img_bnk_name, iteration, IMG_URL) {
    jQuery('#image-library').dialog('close');
    var itr;
    if (iteration > 0) {
        itr = iteration;
    } else {
        itr = image_id;
    }
    //Empty browse and give library an image id
    $('#image_bank' + itr).val(id);
    $('#photo' + itr).val('');
    //show file name
    $('#image_name_bank' + itr).val('');
    $('#image_name_bank' + itr).show();
    $('#image_name_bank' + itr).val(img_bnk_name);

    // Show image preview
    $('#uploadFile1').hide();
    $('#preview').show();
    $('.existing_imgs' + itr).hide();
    $('#photo' + itr).hide();
    $('.preview-img-script' + itr).show();
    $('.preview-img-script' + itr).attr('src', IMG_URL + img_bnk_name);
    $('.preview-img-script' + itr).css('transform', '');
    $('.preview-img-script' + itr).css('max-width', '');
    $('.preview-img-script' + itr).css('max-height', '');
    $('.main-preview .preview-img-script' + itr).css('max-width', '');
    $('.main-preview .preview-img-script' + itr).css('max-height', '');
}

function select_multiple_image(id, image_id, iteration,img_bnk_name, IMG_URL) {
    jQuery('#image-library').dialog('close');
    var itr;
    if (iteration > 0) {
        itr = iteration;
    } else {
        itr = image_id;
    }
    //Empty browse and give library an image id   
    $('#image_bank' + itr).val(id);
    idsArray = id.split(",");
    if(idsArray.length > 1){
        $('#uploadFile1').show();
        $('#preview').hide();
        document.getElementById("uploadFile1").value = "Multiple Images Selected";
    }else{
            //show file name
    $('#photo' + itr).val('');
    //show file name
    $('#image_name_bank' + itr).val('');
    $('#image_name_bank' + itr).show();
    $('#image_name_bank' + itr).val(img_bnk_name);
    // Show image preview
    $('#uploadFile1').hide();
    $('#preview').show();
    $('.existing_imgs' + itr).hide();
    $('#photo' + itr).hide();
    $('.preview-img-script' + itr).show();
    $('.preview-img-script' + itr).attr('src', IMG_URL + img_bnk_name);
    $('.preview-img-script' + itr).css('transform', '');
    $('.preview-img-script' + itr).css('max-width', '');
    $('.preview-img-script' + itr).css('max-height', '');
    $('.main-preview .preview-img-script' + itr).css('max-width', '');
    $('.main-preview .preview-img-script' + itr).css('max-height', '');
    }
}
function select_new_image(id, image_id, iteration) {
    jQuery('#image-library').dialog('close');
    var itr;
    if (iteration > 0) {
        itr = iteration;
    } else {
        itr = image_id;
    }
    $('#preview').hide();
    $('#uploadFile1').show(); 
    var inp = document.getElementById('photo'+itr);
    var imageVal = inp.files.length;
    if (imageVal == 1) {                 
        document.getElementById("uploadFile1").value = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '');                 
    }
    else
    {
        document.getElementById("uploadFile1").value = "Multiple Images Selected";
    }
    //Empty browse and give library an image id
    $('#photo'+itr).val(id);

   $('#image_bank'+itr).val('');

}
function show_file_name(image_id, input, iteration) {
    //Get dimensions of image
    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.gif)$");
    if (regex.test(input.value.toLowerCase())) {
        //Check whether HTML5 is supported.
        if (typeof (input.files) != "undefined") {
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(input.files[0]);
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();

                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;

                //Validate the File Height and Width.
                image.onload = function () {
                    var height = this.height;
                    var width = this.width;
                    EXIF.getData(input.files[0], function () {
                        if (this.exifdata.Orientation == 3) {
                            $('.preview-img-script' + id).css('transform', 'rotate(180deg)');
                            $('.preview-img-script' + id).css('max-width', '190px');
                            $('.preview-img-script' + id).css('max-height', '127px');
                            $('.main-preview .preview-img-script' + id).css('max-width', '545px');
                            $('.main-preview .preview-img-script' + id).css('max-height', '335px');
                        } else if (this.exifdata.Orientation == 6) {
                            $('.preview-img-script' + id).css('transform', 'rotate(90deg)');
                            if (width > height) {
                                $('.preview-img-script' + id).css('max-width', '127px');
                                $('.preview-img-script' + id).css('max-height', '190px');
                                $('.main-preview .preview-img-script' + id).css('max-width', '335px');
                                $('.main-preview .preview-img-script' + id).css('max-height', '545px');
                            }
                        } else if (this.exifdata.Orientation == 8) {
                            $('.preview-img-script' + id).css('transform', 'rotate(-90deg)');
                            if (width > height) {
                                $('.preview-img-script' + id).css('max-width', '127px');
                                $('.preview-img-script' + id).css('max-height', '190px');
                                $('.main-preview .preview-img-script' + id).css('max-width', '335px');
                                $('.main-preview .preview-img-script' + id).css('max-height', '545px');
                            }
                        } else {
                            $('.preview-img-script' + id).css('transform', '');
                            $('.preview-img-script' + id).css('max-width', '');
                            $('.preview-img-script' + id).css('max-height', '');
                            $('.main-preview .preview-img-script' + id).css('max-width', '');
                            $('.main-preview .preview-img-script' + id).css('max-height', '');
                        }
                    });
                };
            }
        }
    }

    var file_name = input.value.replace(/\\/g, '/').replace(/.*\//, '');
    var id;
    if (iteration > 0) {
        id = iteration;
    } else {
        id = image_id;
    }
    if (file_name.length > 0) {
        //Empty library 
        $('#image_bank' + image_id).val('');
        //show file name
        $('#image_name_bank' + id).val('');
        $('#image_name_bank' + id).show();
        $('#image_name_bank' + id).val(file_name);
    }
    // Show image preview
    $('.existing_imgs' + id).hide();
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        $('#photo' + id).hide();
        $('.preview-img-script' + id).show();
        reader.onload = function (e) {
            $('.preview-img-script' + id).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function progress_bar(FILE_ID, BL_ID) {
    var progressbox = $('#progressbox');
    var progress_conti = $('.progress-container-main');
    var progressbar = $('#progressbar');
    var statustxt = $('#statustxt');
    var completed = '0%';
    var pathname = window.location.pathname;
    pathname = pathname.split('/');
    var href = window.location.href;
    var options = {
        beforeSubmit: beforeSubmit, // pre-submit callback 
        uploadProgress: OnProgress,
        success: afterSuccess, // post-submit callback 
        resetForm: true        // reset the form after successful submit 
    };
    //For Mobile user admin
    if ($('#image_page').val() == 1) {
        href = "customer-listing-mobile-menu.php?bl_id=" + BL_ID +'&msg=1';
        var image_size = "";
        var image_file = $('.setting-name').val();
        if (image_file != '') {
            var extesion_image = image_file.substr(image_file.lastIndexOf('.') + 1);
            var ext_check_image = extesion_image.toLowerCase();
            if (ext_check_image == 'jpg' || ext_check_image == 'jpeg' || ext_check_image == 'png' || ext_check_image == 'gif') {
                if (($(".setting-name"))[0].files.length > 0) {
                    image_size = ($(".setting-name"))[0].files[0].size;
                    if (image_size > 10000000) {
                        swal("PDF!", "File Size must be less then 10MB!", "warning");
                        $('.setting-name').val('');
                        $('#image_name_bank' + FILE_ID).val('');
                        $("#image_bank" + FILE_ID).attr("src", "");
                        return false;
                    }
                }
            } else {
                swal("Image!", "File Must be JPG, PNG, GIF!", "warning");
                $('.setting-name').val('');
                $('#image_name_bank' + FILE_ID).val('');
                $("#image_bank" + FILE_ID).attr("src", "");
                return false;
            }
        }
        $('#MyUploadForm').submit(function () {
            $(this).ajaxSubmit(options);
            
        });
         
    }
    // when agenda page form submited
    var required_field = $(".required_fields" + FILE_ID).val();
    if (required_field != '' && typeof required_field != 'undefined') {
        href = window.location.href;
        var agenda_size = "";
        var minute_size = "";
        var agenda_pdf_file = $('.agenda_file_ext' + FILE_ID).val();
        if (agenda_pdf_file != '') {
            var agenda_ext = agenda_pdf_file.split(".");
            var extesion_agenda = agenda_ext[1];
            var ext_check_agenda = extesion_agenda.toLowerCase();
            if (ext_check_agenda == 'pdf') {
                if (($(".agenda_file_ext" + FILE_ID))[0].files.length > 0) {
                    agenda_size = ($(".agenda_file_ext" + FILE_ID))[0].files[0].size;
                    if (agenda_size > 5342523) {
                        swal("PDF!", "File Size must be less then 5MB!", "warning");
                        $('.agenda_file_ext' + FILE_ID).val('');
                        $('.agenda_pdf_new' + FILE_ID).text('');
                        return false;
                    }
                }
            } else {
                swal("PDF!", "File Must be PDF!", "warning");
                $('.agenda_file_ext' + FILE_ID).val('');
                $('.agenda_pdf_new' + FILE_ID).text('');
                return false;
            }
        }
        var minute_pdf_file = $('.minute_file_ext' + FILE_ID).val();
        if (minute_pdf_file != '') {
            var minute_ext = minute_pdf_file.split(".");
            var extesion_minute = minute_ext[1];
            var ext_check_minute = extesion_minute.toLowerCase();
            if (ext_check_minute == 'pdf') {
                if (($(".minute_file_ext" + FILE_ID))[0].files.length > 0) {
                    minute_size = ($(".minute_file_ext" + FILE_ID))[0].files[0].size;
                    if (minute_size > 5342523) {
                        swal("PDF!", "File Size must be less then 5MB!", "warning");
                        $('.minute_file_ext' + FILE_ID).val('');
                        $('.minutes_pdf_new' + FILE_ID).text('');
                        return false;
                    }
                }
            } else {
                swal("PDF!", "File Must be PDF!", "warning");
                $('.minute_file_ext' + FILE_ID).val('');
                $('.minutes_pdf_new' + FILE_ID).text('');
                return false;
            }
        }
        $('#pdf-update' + FILE_ID).submit(function () {
            $(this).ajaxSubmit(options);
            $('.custom-ajax-show').removeClass("custom-ajax-show").addClass('custom-ajax');
        });
    } else if ($('.agenda_page').val() == 1) {
        $('.required_fields' + FILE_ID).val('');
        return false;
    }
    // when upload pdf page form submited
    var required_title = $(".required_title" + FILE_ID).val();
    if (required_title != '' && typeof required_title != 'undefined') {
        href = window.location.href;
        var pdf_size = "";
        var pdf_file = $('.file_ext' + FILE_ID).val();
        if (pdf_file != '') {
            var file_ext = pdf_file.split(".");
            var extesion_file = file_ext[1];
            var ext_check_file = extesion_file.toLowerCase();
            if (ext_check_file == 'pdf') {
                if (($(".file_ext" + FILE_ID))[0].files.length > 0) {
                    pdf_size = ($(".file_ext" + FILE_ID))[0].files[0].size;
                    if (pdf_size > 5342523) {
                        swal("PDF!", "File Size must be less then 5MB!", "warning");
                        $('.file_ext' + FILE_ID).val('');
                        return false;
                    }
                }
            } else {
                swal("PDF!", "File Must be PDF!", "warning");
                $('.agenda_file_ext' + FILE_ID).val('');
                $('.agenda_pdf_new' + FILE_ID).text('');
                return false;
            }
        } else if (FILE_ID == 0) {
            swal("PDF!", "File Not Selected", "warning");
            return false;
        }
        $('#pdf-update' + FILE_ID).submit(function () {
            $(this).ajaxSubmit(options);
            $('.custom-ajax-show').removeClass("custom-ajax-show").addClass('custom-ajax');
        });
    } else if ($('.upload_pdf_page').val() == 1) {
        $('.required_title' + FILE_ID).val('');
        return false;
    }
    //when upload progresses	
    function OnProgress(event, position, total, percentComplete)
    {
        //Progress bar
        progressbar.width(percentComplete + '%') //update progressbar percent complete
        statustxt.html(percentComplete + '%'); //update status text
        if (percentComplete > 50)
        {
            statustxt.css('color', '#fff'); //change status text to white after 50%
        }
    }
    //after succesful upload
    function afterSuccess()
    {
        window.location.href = href;
    }
    //function to check file size before uploading.
    function beforeSubmit() {
        //check whether browser fully supports all File API
        if (window.File && window.FileReader && window.FileList && window.Blob)
        {
            //Progress bar
            progressbox.show(); //show progressbar
            progress_conti.show(); //show progress container
            progressbar.width(completed); //initial value 0% of progressbar
            statustxt.html(completed); //set status text
            statustxt.css('color', '#000'); //initial color of status text
        }
    }
}
$(document).ready(function () {
    $('input[type="file"]').on('change', function () {
        var agenda_file_name = this.value.replace(/\\/g, '/').replace(/.*\//, '');
        var set_name = $(this).attr('id');
        $('.' + set_name).text('');
        $('.' + set_name).text(agenda_file_name);
    });
});
//Check IMG Size of Image for gallery add-On
function check_img_size(image_field, image_size, mainImage) {
    if ($("#title" + image_field).val() == "") {
        swal("Required!", "Please fill out title field!", "warning");
        return false;
    }

    if (image_field == 21 && mainImage == 22) {
        var thumbnailPhoto = $("#photo" + image_field).val();
        var mainPhoto = $("#photo" + mainImage).val();
        if ((typeof thumbnailPhoto != "undefined" && thumbnailPhoto != "") || (typeof mainPhoto != "undefined" && mainPhoto != "")) {
            if ($('#listing_type').val() != 1) {
                if (image_field == 21) {
                    if (thumbnailPhoto != '') {
                        var extesion_image_main = thumbnailPhoto.substr(thumbnailPhoto.lastIndexOf('.') + 1);
                        var ext_check_image_main = extesion_image_main.toLowerCase();
                        if (ext_check_image_main != 'jpg' && ext_check_image_main != 'jpeg' && ext_check_image_main != 'png' && ext_check_image_main != 'gif') {
                            swal("Image!", "File Must be JPG, PNG, GIF!", "warning");
                            $("#photo" + image_field).val('');
                            $("#image_name_bank" + image_field).hide('');
                            return false;
                        }
                    }
                    if (($("#photo" + image_field))[0].files.length > 0) {
                        var img_size_main = ($("#photo" + image_field))[0].files[0].size;
                        if (img_size_main > image_size) {
                            if (image_size == 3000000) {
                                swal("Image!", "File Size must be less then 3MB!", "warning");
                            } else {
                                swal("Image!", "File Size must be less then 10MB!", "warning");
                            }
                            $("#photo" + image_field).val('');
                            $("#image_name_bank" + image_field).hide('');
                            return false;
                        }
                    }
                }
            }
            if (mainImage == 22) {
                if (mainPhoto != '') {
                    var extesion_image_main22 = mainPhoto.substr(mainPhoto.lastIndexOf('.') + 1);
                    var ext_check_image_main22 = extesion_image_main22.toLowerCase();
                    if (ext_check_image_main22 != 'jpg' && ext_check_image_main22 != 'jpeg' && ext_check_image_main22 != 'png' && ext_check_image_main22 != 'gif') {
                        swal("Image!", "File Must be JPG, PNG, GIF!", "warning");
                        $("#photo" + mainImage).val('');
                        $("#image_name_bank" + mainImage).hide('');
                        return false;
                    }
                }
                if (($("#photo" + mainImage))[0].files.length > 0) {
                    var img_size_main22 = ($("#photo" + mainImage))[0].files[0].size;
                    if (img_size_main22 > image_size) {
                        if (image_size == 3000000) {
                            swal("Image!", "File Size must be less then 3MB!", "warning");
                        } else {
                            swal("Image!", "File Size must be less then 10MB!", "warning");
                        }
                        $("#photo" + mainImage).val('');
                        $("#image_name_bank" + mainImage).hide('');
                        return false;
                    }
                }
            }
        } else {
        }
    } else {
        var update = $('#update_' + image_field).val();
        var image_file = $("#photo" + image_field).val();
        if (image_file != '') {
            var extesion_image = image_file.substr(image_file.lastIndexOf('.') + 1);
            var ext_check_image = extesion_image.toLowerCase();
            if (ext_check_image != 'jpg' && ext_check_image != 'jpeg' && ext_check_image != 'png' && ext_check_image != 'gif') {
                swal("Image!", "File Must be JPG, PNG, GIF!", "warning");
                $("#photo" + image_field).val('');
                $("#image_name_bank" + image_field).hide('');
                return false;
            }
        }
        if (($("#photo" + image_field))[0].files.length > 0) {
            var img_size = ($("#photo" + image_field))[0].files[0].size;
            if (img_size > image_size) {
                if (image_size == 3000000) {
                    swal("Image!", "File Size must be less then 3MB!", "warning");
                } else {
                    swal("Image!", "File Size must be less then 10MB!", "warning");
                }
                $("#photo" + image_field).val('');
                $("#image_name_bank" + image_field).hide('');
                return false;
            }
        } else if (($("#photo" + image_field))[0].files.length == 0) {
            if ($('#image_bank' + image_field).val() == "") {
                if (update != 1) {
                    swal("Image!", "Please select image!", "warning");
                    $("#photo" + image_field).val('');
                    $("#image_name_bank" + image_field).hide('');
                    return false;
                }
            }
        }
    }
}

$(document).ready(function () {
    function setHeight() {
        windowHeight = $(window).innerHeight();
        $('.content-wrapper').css('min-height', windowHeight);
    }
    ;
    setHeight();

    $(window).resize(function () {
        setHeight();
    });
});
$(document).ready(function () {
    function setHeight() {
        windowHeight = $(window).innerHeight();
        $('.login-outer-body').css('min-height', windowHeight);
    }
    ;
    setHeight();

    $(window).resize(function () {
        setHeight();
    });
});