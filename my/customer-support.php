<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    
} else {
    header('Location: index.php');
}
if (isset($_GET['ss_id'])) {
    $sqlSupportSection = "SELECT SS_Name FROM tbl_Support_Section WHERE SS_ID = " . $_GET['ss_id'];
    $resultSupportSection = mysql_query($sqlSupportSection, $db) or die("Invalid query: $sqlSupportSection -- " . mysql_error());
    $rowresultSupportSection = mysql_fetch_assoc($resultSupportSection);
    $sqlSupport = "SELECT * FROM tbl_Support WHERE S_SS_ID = " . $_GET['ss_id'];
    $resultSupport = mysql_query($sqlSupport, $db) or die("Invalid query: $sqlSupport -- " . mysql_error());
}
require_once '../include/my/header.php';
?>
<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">Support</div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-B-support.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">
            <div class="title">
                <?php echo $rowresultSupportSection['SS_Name']; ?> - Support
            </div>
        </div>
        <div class="support-sections-container form-inside-div border-none padding-none">
            <?php while ($rowSupport = mysql_fetch_assoc($resultSupport)) { ?>
                <div class="support-item-container">
                    <div class="video-thumbnail">
                        <a href="customer-support-section.php?bl_id=<?php echo $BL_ID; ?>&s_id=<?php echo $rowSupport['S_ID']; ?>">
                            <?php
                            if ($rowSupport['S_Thumbnail'] != "") {
                                ?>
                                <img class="video-icon" src="http://<?php echo DOMAIN . IMG_LOC_REL ?>icon-video.png">
                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $rowSupport['S_Thumbnail']; ?>">
                            <?php } else { ?>
                                <img src="http://<?php echo DOMAIN . IMG_LOC_REL ?>default.png">
                            <?php }
                            ?>
                        </a>
                    </div>
                    <div class="support-section-name"><a href="customer-support-section.php?bl_id=<?php echo $BL_ID; ?>&s_id=<?php echo $rowSupport['S_ID']; ?>"><?php echo $rowSupport['S_Name']; ?></a></div>
                    <div class="support-section-desc">
                        <?php
                        $string = strip_tags($rowSupport['S_Description']);
                        if (strlen($string) > 100) {
                            // truncate string
                            $stringCut = substr($string, 0, 100);
                            // make sure it ends in a word so assassinate doesn't become ass...
                            $string = substr($stringCut, 0, strrpos($stringCut, ' ')) . '... <a href="customer-support-section.php?bl_id=' . $BL_ID . '&s_id=' . $rowSupport['S_ID'] . '">More details</a>';
                        }
                        echo $string;
                        ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>