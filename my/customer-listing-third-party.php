<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    $sql = "SELECT BL_ID, BL_Listing_Title, BL_Description, BL_Listing_Type, BL_Contact, BL_Phone, BL_Website, BL_Toll_Free, BL_Email, BL_Street, BL_Town, 
            BL_Province, BL_PostalCode, BL_Lat, BL_Long, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Description, BL_SEO_Keywords, 
            BL_Search_Words, BL_Active_Addon, BL_Third_Party FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}

if ($_POST['op'] == 'save') {
    $add_on = ($_POST['add_ons'] != "") ? $_POST['add_ons'] : 0; 
    $sql = "tbl_Business_Listing SET
            BL_Third_Party = '" . encode_strings($_POST['third_party'], $db) . "',
            BL_Active_Addon = '" . encode_strings($add_on, $db) . "'";
    if ($BL_ID > 0) {
        $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Third Party', '', 'Update', 'user admin');
    } else {
        $sql = "INSERT " . $sql;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Third Party', '', 'Add', 'user admin');
    }
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: customer-listing-third-party.php?bl_id=" . $BL_ID);
    exit();
}
require_once '../include/my/header.php';
?>
<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">My Page</div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div>
        <div class="instruction">
            Fields with this background<span></span>will show on free listings profile.
        </div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-B-mypage.php'; ?>
    </div>
    <div class="right">
        <form action="" method="post" name="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <div class="content-header">
                <div class="title">Third Party</div>
                <div class="link">
                </div>
            </div>
            <div class="form-inside-div">
                <label>Third Party Code</label>
                <div class="form-data">
                    <textarea name="third_party" class="short-des-text" rows="4" id="third-party-code" onchange="thirdParty();"><?php echo $rowListing['BL_Third_Party'] ?></textarea>                           
                </div>
            </div>
            <div class="form-inside-div">
                <label>Add-Ons</label>
                <div class="form-data">
                    <select name="add_ons" id="add-ons" <?php echo ($rowListing['BL_Third_Party'] != '') ? "required" : ""; ?>>
                        <option value="">Select Add-On</option>
                        <option value="-1" <?php echo (($rowListing['BL_Active_Addon'] == "-1") ? "selected" : ""); ?>>Home</option>
                        <?php
                        $sql_Features = "SELECT F_ID, F_Name FROM tbl_BL_Feature LEFT JOIN tbl_Feature ON BLF_F_ID = F_ID
                                         WHERE BLF_BL_ID = '" . $BL_ID . "' AND BLF_Active = 1 AND BLF_F_ID NOT IN (2)";
                        $res_Features = mysql_query($sql_Features);
                        if (isset($res_Features)) {
                            while ($features = mysql_fetch_array($res_Features)) {
                                ?>
                                <option value="<?php echo $features['F_ID'] ?>" <?php echo (($features['F_ID'] == $rowListing['BL_Active_Addon']) ? "selected" : ""); ?>><?php echo $features['F_Name'] ?></option>
                            <?php
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" name="button2" value="Save Now"/>
                </div>  
            </div>
        </form>
    </div>
</div>

<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>
<script type="text/javascript">
    function thirdParty() {
        if ($("#third-party-code").val() != '') {
            $("#add-ons").attr('required', 'required');
        }else{
            $("#add-ons").removeAttr('required');
        }
    }
</script>