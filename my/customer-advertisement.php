<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require '../include/PHPMailer/class.phpmailer.php';
require_once '../include/track-data-entry.php';

$BID = $_SESSION['BUSINESS_ID'];
if ($_POST['butt'] == 'Submit Campaign for Creation') {
    if (isset($_REQUEST['campaign'])) {
        $campaign = 1;
    } else {
        $campaign = 0;
    }
    $sql = "SELECT BL_Listing_Title, B_Email FROM tbl_Business_Listing LEFT JOIN tbl_Business ON B_ID = BL_B_ID WHERE BL_ID = '" . encode_strings($_REQUEST['listing'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $sql = "tbl_Advertisement SET 
            A_BL_ID = '" . encode_strings($_REQUEST['listing'], $db) . "',
            A_Website = '" . encode_strings($_REQUEST['domain'], $db) . "', 
            A_AT_ID = '" . encode_strings($_REQUEST['advert_type'], $db) . "', 
            A_C_ID = '" . encode_strings($_REQUEST['category'], $db) . "',   
            A_SC_ID = '" . encode_strings(($_REQUEST['subcategory'] != "") ? $_REQUEST['subcategory'] : 0, $db) . "',
            A_End_Date = '" . encode_strings(($_REQUEST['endDate'] != "") ? $_REQUEST['endDate'] : "0000-00-00", $db) . "',
            A_Campaign = '" . encode_strings($campaign, $db) . "'";
    if ($_POST['choose'] == 'existing') {
        $existingAd = "SELECT A_Title, A_Description, A_Notes, A_Approved_Logo FROM tbl_Advertisement 
                        WHERE A_ID = '" . encode_strings($_POST['choose-existing-ad'], $db) . "' LIMIT 1";
        $existingAdResult = mysql_query($existingAd, $db) or die("Invalid query: $existingAd -- " . mysql_error());
        $existingAdRow = mysql_fetch_assoc($existingAdResult);

        $advertType = "SELECT * FROM tbl_Advertisement_Type WHERE AT_ID = '" . encode_strings($_REQUEST['advert_type'], $db) . "'";
        $resAdvertType = mysql_query($advertType, $db) or die("Invalid query: $advertType -- " . mysql_error());
        $rowType = mysql_fetch_assoc($resAdvertType);
        $total = $rowType['AT_Cost'];
        $sql .= ", A_Title = '" . encode_strings($existingAdRow['A_Title'], $db) . "', 
                           A_Description = '" . encode_strings($existingAdRow['A_Description'], $db) . "',      
                           A_Notes = '" . encode_strings($existingAdRow['A_Notes'], $db) . "',
                           A_Approved_Logo = '" . encode_strings($existingAdRow['A_Approved_Logo'], $db) . "',
                           A_Date = CURDATE(),
                           A_Status = '3',
                           A_B_ID  ='" . encode_strings($BID, $db) . "',
                           A_Active_Date = CURDATE(),
                            A_Total = '" . encode_strings($total, $db) . "'";
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        $existingAdPhotos = "SELECT AP_Photo FROM tbl_Advertisement_Photo WHERE AP_A_ID = '" . encode_strings($_POST['choose-existing-ad'], $db) . "'";
        $existingAdPhotosResult = mysql_query($existingAdPhotos, $db) or die("Invalid query: $existingAdPhotos -- " . mysql_error());
        while ($existingAdPhotosRow = mysql_fetch_assoc($existingAdPhotosResult)) {
            $sql = mysql_query("INSERT INTO tbl_Advertisement_Photo(AP_A_ID, AP_Photo) VALUES('$id', '" . $existingAdPhotosRow['AP_Photo'] . "')");
        }
        if ($result) {
            $_SESSION['success'] = 1;
            // TRACK DATA ENTRY
            Track_Data_Entry('Listing Home', $BID, 'Buy A Campaign', $id, 'Buy Campaign', 'user admin');
            header("Location: customer-advertisment-detail.php?advert_id=" . $id);
            exit();
        } else {
            $_SESSION['error'] = 1;
            header("Location: customer-advertisment-myaccount.php");
            exit();
        }
    } else {
        $sql .= ", A_Title = '" . encode_strings($_REQUEST['title'], $db) . "', 
                   A_Description = '" . encode_strings($_REQUEST['desc'], $db) . "',      
                   A_Notes = '" . encode_strings($_REQUEST['notes'], $db) . "',
                   A_Date = CURDATE(),
                   A_Status = '1',
                   A_B_ID  ='" . encode_strings($BID, $db) . "'";
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $id = mysql_insert_id();
        require_once '../include/picUpload.inc.php';
        for ($i = 1; $i < $_POST['total_photos'] + 1; $i++) {
            $pic = Upload_Pic_Normal('0', 'pic' . $i, 0, 0, true, IMG_LOC_ABS, 3145728);
            if ($pic) {
                $sql = mysql_query("INSERT INTO tbl_Advertisement_Photo(AP_A_ID, AP_Photo) VALUES('$id', '$pic')");
            }
        }
        //////Email template 
        $email = "SELECT * FROM tbl_Email_Templates WHERE ET_ID = 14";
        $result_data = mysql_query($email);
        $rowEmail = mysql_fetch_assoc($result_data);
        $email_subject = $rowEmail['ET_Subject'];
        $email_heading = $rowEmail['ET_Name'];
        $email_body = $rowEmail['ET_Template'];
        $email_footer = $rowEmail['ET_Footer'];
        $message_body = str_replace('(listing name)', $rowListing['BL_Listing_Title'], $rowEmail['ET_Template']);
        ob_start();
        include '../include/email-template/create-advertisment-email.php';
        $html = ob_get_contents();
        ob_clean();
        $mail = new PHPMailer();
        $mail->IsMail();
        $mail->IsHTML(true);
        $mail->AddAddress($to);
        $mail->CharSet = 'UTF-8';
        $mail->From = $rowListing['B_Email'];
        $mail->FromName = "TouristTown";
        $mail->IsHTML(true);
        $mail->AddAddress('danielle@touristtown.ca', 'Danielle Mulasmajic');
        //$mail->Subject ="Tourist Town - Ad Request" . $rowListing['BL_Listing_Title'];
        $mail->Subject =$email_subject  .  $rowListing['BL_Listing_Title'];
        $mail->MsgHTML($html);
       // $mail->Send();
        $_SESSION['buy_ad_success'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing Home', $BID, 'Buy A Campaign', $id, 'Buy Campaign', 'user admin');
        header("Location: customer-advertisment-detail.php?advert_id=" . $id);
        exit();
    }
}

require_once '../include/my/header.php';
?>
<script>
    $(function () {
        $(document).tooltip();
    });
</script>
<div class="content-left advert">
    <?php require_once '../include/nav-B-advertisement.php'; ?>
    <div class="title-link">
        <div class="title">Campaigning</div>
        <div class="link">
        </div>
    </div>

    <div class="right">
        <!--Step One-->
        <form id="form" name="form1" method="post" action="customer-advertisement.php" enctype="multipart/form-data">
            <div class="content-header">Buy A Campaign</div>
            <div class="form-inside-div form-inside-div-adv">
                <label>Campaign Link</label>
                <div class="form-data ad-link-width">
                    <select class="adv-type-options" name="listing" id="listing-id" onChange="validate_buy_an_add(1, 0, 0, 0, 0);">
                        <option required value="">Select Listing</option>
                        <?PHP
                        $sql = "SELECT BL_ID, BL_Listing_Title FROM tbl_Business_Listing WHERE BL_B_ID = '$BID' ORDER BY BL_ID";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($rowListing = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $rowListing['BL_ID'] ?>"><?php echo $rowListing['BL_Listing_Title'] ?></option>
                            <?PHP
                        }
                        ?>

                    </select>
                </div>
                <a class="adv-sample" title="Select the listing you would like the ad to link to.">What is this?</a>
            </div>
            <div class="form-inside-div form-inside-div-adv">
                <label>Website</label>
                <div class="form-data" id="regions">
                    <select class="adv-type-options" id="region-id" name="domain" onChange="validate_buy_an_add(0, 1, 0, 0, 0);" required>
                        <option required value="">Select Website</option>
                    </select>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-adv">
                <label>Campaign Type</label>
                <div class="form-data">
                    <select class="adv-type-options" id="advert-type" name="advert_type" onChange="validate_buy_an_add(0, 0, 1, 0, 0);" required>
                        <option value="">Select Campaign Type</option>
                        <?php
                        $sql = "SELECT AT_ID, AT_Name, AT_Cost FROM tbl_Advertisement_Type WHERE AT_ID NOT IN(3,4)";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($rowAT = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $rowAT['AT_ID'] ?>"><?php echo "$" . $rowAT['AT_Cost'] . " " . $rowAT['AT_Name'] ?></option>
                        <?php } ?>

                    </select>
                </div>
                <a class="adv-sample" id="view-sample" onclick="show_image()" style="display:none">View Sample</a>
                <div id="sample-image" style="display :none;">
                    <div id="sample-image-show" >

                    </div>
                </div>

            </div>

            <div class="form-inside-div form-inside-div-adv" id="cat">
                <label>Category</label>
                <div class="form-data" id="advert-cats">
                    <select class="adv-type-options" id="advert-cat" name="category" required="required">

                        <option value="" required>Select Category</option>
                    </select>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-adv" id="sub-cat">
                <label>Sub Category</label>
                <div class="form-data" id="advert-subcats">
                    <select class="adv-type-options" id="advert-subcat" name="subcategory" required="required">

                        <option value="">Select Sub Category</option>
                    </select>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-adv">
                <label>End Date</label>
                <div class="form-data">
                    <input type="text" name="endDate" class="previous-date-not-allowed" id="endDate"/> 
                </div>
            </div>

            <div class="form-inside-div form-inside-div-adv">
                <label>Campaign</label>
                <div class="form-data">
                    <input type="checkbox" name="campaign" id="campaign"/> 
                </div>
            </div>

            <div class="form-inside-div form-inside-div-adv" id="adver-cost" style="display :<?php echo ($AID > 0) ? 'block' : 'none'; ?>">
                <label>Monthly Cost</label>
                <div class="form-data from-inside-div-text" id="advert-cost" >
                </div>
            </div>

            <div class="form-inside-div padding-top-30 border-none" id="choose" style="display:none;">
                <div class="existing-ad">
                    <div class="ad-catorgory">Choose Existing Campaign</div>
                    <div class="ad-catorgory-des">If you have previously created an campaign for Tourist Town and you would like to use it again,
                        click on choose Existing Campaign.
                    </div>
                    <div class="ad-button" id="existing-button">
                        <label class="select-ad-catorgory"><input type="radio" name="choose" value="existing">Choose Existing Campaign</label>
                    </div>
                </div>
                <div class="new-ad">
                    <div class="ad-catorgory">Create New Campaign</div>
                    <div class="ad-catorgory-des">If you would like to created a brand new ad,click "Create New Campaign".</div>
                    <div class="ad-button">
                        <label class="select-ad-catorgory-new"><input type="radio" name="choose" value="new">Create New Campaign</label>

                    </div>
                </div>
            </div>

            <!--Create New Ad-->
            <div class="step2-create-adv" id="new-ad" style="display:none">
                <div class="form-inside-div form-inside-div-adv">
                    <p> You are now ready to create your campaign.Simply fill in the information you
                        would like in your campaign and the Tourist Town team will create it for you.
                    </p>
                    <p>
                        Campaigning Tips
                    <ul class="temp-margin-22">
                        <li>Be direct: Remember the less words the better.</li>
                        <li>Choose a strong photograph.</li>
                    </ul>
                    </p>

                </div>
                <div class="form-inside-div form-inside-div-adv">
                    <label>Title</label>
                    <div class="form-data">
                        <input type="text" name="title" id="title">
                    </div>
                    <a class="adv-sample" title="This is the main message of your ad. Keep it short">What is this?</a>
                </div>
                <div class="form-inside-div form-inside-div-adv">
                    <label>Description</label>
                    <div class="form-data">
                        <textarea name="desc" class="tt-ckeditor" id="desc"></textarea>
                    </div>
                    <a class="adv-sample" title="Outline any details you want included in your ad">What is this?</a>
                </div>
                <div class="form-inside-div form-inside-div-adv">
                    <label>Notes</label>
                    <div class="form-data">
                        <textarea name="notes" class="tt-ckeditor" id="notes"></textarea>
                    </div>
                    <a class="adv-sample" title="Outline how you want your ad to look. Specify the colors and style you would like to see">What is this?</a>
                </div>
                <div id="add-another-photo">
                    <div class="form-inside-div form-inside-div-adv">
                        <label>Photo 1</label>
                        <div class="form-data">
                            <input type="hidden" name="total_photos" id="photo-check" value="1">
                            <div class="inputWrapper adv-photo">Browse<input class="fileInput" type="file" name="pic1[]" id="photo1" onchange="show_file_name(this.files[0].name, 1)"></div>
                            <input id="uploadFile1" class="uploadFileName" disabled>
                        </div>
                        <a class="adv-sample" onclick="add_photo()">Add another</a>
                    </div>
                </div>
            </div>

            <!--Choose Existing Ad-->
            <div class="menu-items-accodings margin-top-12" id="existing-ad" style="display:none">                         

            </div>

            <div class="form-inside-div border-none margin-bottom-26 margin-top-11" id="submit-ad-button" style="display:none;">
                <div class="button">
                    <input type="submit" name="butt" value="Submit Campaign for Creation" id="submitbtn"/>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('input[type=radio][name=choose]').change(function () {
            var region_id = $('#region-id').val();
            var advert_type = $('#advert-type').val();
            var cat_id = $('#advert-cat').val();
            var SubCat = $('#advert-subcat').val();
            if (this.value == 'existing') {
                //Change buttons based on Ad type
                $.post("customer-choose-existing-ad.php", {
                    region_id: region_id,
                    advert_type: advert_type,
                    cat_id: cat_id,
                    SubCat: SubCat
                }, function (done) {
                    var res = done.split("@");
                    if (res[0] == 0) {
                        $("#submit-ad-button").hide();
                        swal("", "You do not have any " + res[1] + " Campaigns in your Library. Please add new one!", "warning");
                    } else {
                        $('#existing-ad').html(res[0]);
                        $("#submit-ad-button").show();
                    }
                });
                $('#existing-ad').show();
                $('#new-ad').hide();
                $("#title").removeAttr('required');
                $("#desc").removeAttr('required');
            } else if (this.value == 'new') {
                $('#new-ad').show();
                $('#existing-ad').hide();
                $("#title").attr('required', 'required');
                $("#desc").attr('required', 'required');
                $('#existing-ad').html('');
                $("#submit-ad-button").show();
            }
        });
    });
    function show_image()
    {
        var check = $.trim($("#sample-image-show").html());
        if (check != '') {
            $("#sample-image").dialog({
                modal: true,
                draggable: false,
                resizable: false,
                width: 'auto',
                open: function () {
                    jQuery('.ui-widget-overlay').bind('click', function () {
                        jQuery('#sample-image').dialog('close');
                    });
                }
            });
        }
        $("#ui-dialog-title-dialog").hide();
        $(".ui-dialog-titlebar").removeClass('ui-widget-header');
    }
    function validate_buy_an_add(L, R, AT, C, S)
    {
        var advert_type = $('#advert-type').val();
        var bl_id = $('#listing-id').val();

        //Onchange Listing
        if (L == 1) {
            $("#region-id").val("");
            $.ajax({
                type: "GET",
                url: "get-customer-advert-region.php",
                data: {
                    bl_id: bl_id
                }
            })
                    .done(function (msg) {
                        document.getElementById('regions').innerHTML = msg;
                    });
            R = 1;
        }
        var region_id = $('#region-id').val();

        //Onchange Region
        if (R == 1) {
            $("#advert-cat").val("");
            $.ajax({
                type: "GET",
                url: "get-customer-advert-cat.php",
                data: {
                    region_id: region_id,
                    advert_type: advert_type
                }
            })
                    .done(function (msg) {
                        document.getElementById('advert-cats').innerHTML = msg;
                    });
            C = 1;
        }

        //Onchange Ad Type
        if (AT == 1) {
            //Get Price based on Ad type
            $.post("get_advert_price.php", {
                advert_type: advert_type
            }, function (done) {
                if (done == 0) {
                    $('#adver-cost').hide();
                    $('#advert-cost').html("");
                } else {
                    $('#adver-cost').show();
                    $('#advert-cost').html('$' + done);
                }
            });
            if (advert_type == '')
            {
                $("#view-sample").hide();
                $('#cat').show();
                $("#advert-cat").attr('required', 'required');
                $("#advert-cat").val("");
                $('#sub-cat').show();
                $("#advert-subcat").attr('required', 'required');
                $("#advert-subcat").val("");
                $("#sample-image-show").empty();
                $('#choose').hide();
                $("input[type=radio][name=choose]").attr("checked", false);
                $('#existing-ad').html("");
                $('#new-ad').hide();
                $("#submit-ad-button").hide();
            }
            if (advert_type == 1)
            {
                $('#cat').show();
                $("#advert-cat").attr('required', 'required');
                $("#advert-cat").val("");
                $('#sub-cat').show();
                $("#advert-subcat").removeAttr('required');
                $("#advert-subcat").val("");
                $("#view-sample").show();
                $("#sample-image-show").empty();
                $("#sample-image-show").append("<img src='<?php echo "http://" . DOMAIN . IMG_LOC_REL . "8489225.jpg" ?>'>");
                $('#choose').hide();
                $("input[type=radio][name=choose]").attr("checked", false);
                $('#existing-ad').html("");
                $('#new-ad').hide();
                $("#submit-ad-button").hide();
            }
            if (advert_type == 2)
            {
                $('#cat').show();
                $("#advert-cat").attr('required', 'required');
                $("#advert-cat").val("");
                $('#sub-cat').hide();
                $("#advert-subcat").removeAttr('required');
                $("#advert-subcat").val("");
                $("#view-sample").show();
                $("#sample-image-show").empty();
                $("#sample-image-show").append("<img src='<?php echo "http://" . DOMAIN . IMG_LOC_REL . "d-ad-3.png" ?>'>");
                $('#choose').hide();
                $("input[type=radio][name=choose]").attr("checked", false);
                $('#existing-ad').html("");
                $('#new-ad').hide();
                $("#submit-ad-button").hide();
            }
            if (advert_type == 3)
            {
                $('#cat').show();
                $("#advert-cat").attr('required', 'required');
                $("#advert-cat").val("");
                $('#sub-cat').show();
                $("#advert-subcat").attr('required', 'required');
                $("#advert-subcat").val("");
                $("#view-sample").show();
                $("#sample-image-show").empty();
                $("#sample-image-show").append("<img src='<?php echo "http://" . DOMAIN . IMG_LOC_REL . "ad-1.png" ?>'>");
                $('#choose').hide();
                $("input[type=radio][name=choose]").attr("checked", false);
                $('#existing-ad').html("");
                $('#new-ad').hide();
                $("#submit-ad-button").hide();
            }
            if (advert_type == 4)
            {
                $('#cat').hide();
                $("#advert-cat").removeAttr('required');
                $("#advert-cat").val("");
                $('#sub-cat').hide();
                $("#advert-subcat").removeAttr('required');
                $("#advert-subcat").val("");
                $("#view-sample").show();
                $("#sample-image-show").empty();
                $("#sample-image-show").append("<img src='<?php echo "http://" . DOMAIN . IMG_LOC_REL . "ad-1.png" ?>'>");
                $('#choose').hide();
                $("input[type=radio][name=choose]").attr("checked", false);
                $('#existing-ad').html("");
                $('#new-ad').hide();
                $("#submit-ad-button").hide();
            }
        }
        var cat_id = $('#advert-cat').val();

        //Onchange Cat
        if (C == 1) {
            $.ajax({
                type: "GET",
                url: "get-customer-advert-subcat.php",
                data: {
                    cat_id: cat_id,
                    website_id: region_id,
                    advert_type: advert_type
                }
            })
                    .done(function (msg) {
                        document.getElementById('advert-subcats').innerHTML = msg;
                        //                $('#form').submit();
                    });
            $('#advert-subcat').val("");
            $("input[type=radio][name=choose]").attr("checked", false);
            $('#existing-ad').html("");
            $('#new-ad').hide();
            $("#submit-ad-button").hide();
        }
        var SubCat = $('#advert-subcat').val();

        //Onchange Sub Cat
        if (S == 1) {
            $("input[type=radio][name=choose]").attr("checked", false);
            $('#existing-ad').html("");
            $('#new-ad').hide();
        }
        if (region_id != '' && advert_type != '') {
            //Dropdown
            if (advert_type == 1 || advert_type == 2) {
                if (cat_id != '') {
                    $('#choose').show();
                } else {
                    $('#choose').hide();
                    $("input[type=radio][name=choose]").attr("checked", false);
                    $('#existing-ad').html("");
                    $('#new-ad').hide();
                    $("#submit-ad-button").hide();
                }
            }
            //Homepage / Sidebanner
            else if (advert_type == 4) {
                $('#choose').show();
            }
            //Other
            else if (advert_type == 3) {
                if (cat_id != '' && SubCat != '') {
                    $('#choose').show();
                } else {
                    $('#choose').hide();
                    $("input[type=radio][name=choose]").attr("checked", false);
                    $('#existing-ad').html("");
                    $('#new-ad').hide();
                    $("#submit-ad-button").hide();
                }
            } else {
                $('#choose').hide();
                $("input[type=radio][name=choose]").attr("checked", false);
                $('#existing-ad').html("");
                $('#new-ad').hide();
                $("#submit-ad-button").hide();
            }
        } else {
            $('#choose').hide();
            $("input[type=radio][name=choose]").attr("checked", false);
            $('#existing-ad').html("");
            $('#new-ad').hide();
            $("#submit-ad-button").hide();
        }
    }
    function add_photo() {
        var count = $("#photo-check").val();
        next_count = ++count;
        if (count < 6) {
            $("#add-another-photo").append(
                    '<div class="form-inside-div form-inside-div-adv"><label>Photo ' + next_count + '</label><div class="form-data"><div class="inputWrapper adv-photo">Browse<input class="fileInput" type="file" name="pic' + next_count + '[]" id="photo' + next_count + '" onchange="show_file_name(this.files[0].name, ' + next_count + ')"></div><input id="uploadFile' + next_count + '" class="uploadFileName" disabled></div></div>'
                    );
            $("#photo-check").val(next_count);
        } else {
            swal("", "You can upload only 5 photos.", "warning");
        }
    }
    function show_file_name(val, count) {
        document.getElementById("uploadFile" + count).value = val;
    }
</script>

<?PHP
require_once '../include/my/footer.php';
?>