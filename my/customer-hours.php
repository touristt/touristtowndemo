<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];
$points_taken = 0;
$Coupons_direction = $_REQUEST['coupons'];
$bfc_id = $_REQUEST['bfc_id'];
if ($BL_ID > 0) {
    $sql = "SELECT BL_ID, BL_Listing_Title, BL_Description, BL_Listing_Type, BL_Contact, BL_Phone, BL_Website, BL_Toll_Free, BL_Email, BL_Street, BL_Town, 
            BL_Province, BL_PostalCode, BL_Lat, BL_Long, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Description, BL_SEO_Keywords, 
            BL_Search_Words, BL_Hours_Disabled, BL_Hour_Mon_From, BL_Hour_Mon_To, BL_Hour_Tue_From, BL_Hour_Tue_To, BL_Hour_Wed_From, BL_Hour_Wed_To,
            BL_Hour_Thu_From, BL_Hour_Thu_To, BL_Hour_Fri_From, BL_Hour_Fri_To, BL_Hour_Sat_From, BL_Hour_Sat_To,BL_Hour_Sun_From, BL_Hour_Sun_To, 
            BL_Hours_Appointment FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}


if ($_POST['op'] == 'save') {
    $hDisabled = $_REQUEST['hDisabled'] != '' ? 1 : 0;
    $happointment = $_REQUEST['happointment'] != '' ? 1 : 0;
    $sql = "tbl_Business_Listing SET  
            BL_Hours_Disabled = '" . encode_strings($hDisabled, $db) . "', 
            BL_Hours_Appointment = '" . encode_strings($happointment, $db) . "', 
            BL_Hour_Mon_From = '" . encode_strings($_REQUEST['monFrom'], $db) . "', 
            BL_Hour_Mon_To = '" . encode_strings($_REQUEST['monTo'], $db) . "', 
            BL_Hour_Tue_From = '" . encode_strings($_REQUEST['tueFrom'], $db) . "', 
            BL_Hour_Tue_To = '" . encode_strings($_REQUEST['tueTo'], $db) . "', 
            BL_Hour_Wed_From = '" . encode_strings($_REQUEST['wedFrom'], $db) . "', 
            BL_Hour_Wed_To = '" . encode_strings($_REQUEST['wedTo'], $db) . "', 
            BL_Hour_Thu_From = '" . encode_strings($_REQUEST['thuFrom'], $db) . "', 
            BL_Hour_Thu_To = '" . encode_strings($_REQUEST['thuTo'], $db) . "', 
            BL_Hour_Fri_From = '" . encode_strings($_REQUEST['friFrom'], $db) . "', 
            BL_Hour_Fri_To = '" . encode_strings($_REQUEST['friTo'], $db) . "', 
            BL_Hour_Sat_From = '" . encode_strings($_REQUEST['satFrom'], $db) . "', 
            BL_Hour_Sat_To = '" . encode_strings($_REQUEST['satTo'], $db) . "', 
            BL_Hour_Sun_From = '" . encode_strings($_REQUEST['sunFrom'], $db) . "', 
            BL_Hour_Sun_To = '" . encode_strings($_REQUEST['sunTo'], $db) . "'";
    $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . $BL_ID . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Hours', '', 'Update', 'user admin');
    } else {
        $_SESSION['error'] = 1;
    }
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /listings-preview.php?bl_id=" . $BL_ID);   
    }
   else if(isset($Coupons_direction) && $Coupons_direction == 'editcoupon'){
        
        header("Location:customer-feature-add-coupons.php?bl_id=" . $BL_ID . "&bfc_id=" . $bfc_id . "&re_dir=1");
        exit();
    } else
    {
    header("Location: customer-hours.php?bl_id=" . $BL_ID);
    exit();
    }
}

require_once '../include/my/header.php';
?>

<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">Add Ons</div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div> 
        <div class="instruction">
            Fields with this background<span></span>will show on free listings profile.
        </div>
    </div>

    <div class="left">
        <?PHP
        require '../include/nav-B-mypage.php';
        ?>
    </div>

    <div class="right">
        <form name="form1" method="post" action="">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="coupons" value="<?php echo $Coupons_direction; ?>">
            <input type="hidden" name="bfc_id" value="<?php echo $bfc_id; ?>">
            <div class="content-header">
                <div class="title">Hours</div>
                <div class="link">
                    <?php
                    $Hours = show_field_points('Hours');
                    if (!$rowsHours['BL_Hours_Disabled'] && ($rowsHours['BL_Hour_Mon_From'] != '00:00:00' || $rowsHours['BL_Hour_Tue_From'] != '00:00:00' || $rowsHours['BL_Hour_Wed_From'] != '00:00:00' || $rowsHours['BL_Hour_Thu_From'] != '00:00:00' || $rowsHours['BL_Hour_Fri_From'] != '00:00:00' || $rowsHours['BL_Hour_Sat_From'] != '00:00:00' || $rowsHours['BL_Hour_Sun_From'] != '00:00:00')) {
                        echo '<div class="points-com des-heading-point">' . $Hours . ' pts</div>';
                        $points_taken = $Hours;
                    } else {
                        echo '<div class="points-uncom">' . $Hours . ' pts</div>';
                    }
                    ?>
                </div>
            </div>

            <?php
            $help_text = show_help_text('Hours');
            if ($help_text != '') {
                echo '<div class="form-inside-div" style="border-bottom:3px solid #eeeeee;">' . $help_text . '</div>';
            }
            ?>

            <div class="form-inside-div hours">
                <label>Monday</label>
                <div class="form-data">
                    <select name="monFrom" id="monFrom" onchange="isClosed('monHide', this.value);">
                        <?PHP fromHours($rowListing['BL_Hour_Mon_From']); ?>
                    </select>
                    <span id="monHide" <?php echo $rowListing['BL_Hour_Mon_From'] == '00:00:01' || $rowListing['BL_Hour_Mon_From'] == '00:00:02' || $rowListing['BL_Hour_Mon_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                        <select name="monTo" id="monTo" onchange="">
                            <?PHP toHours($rowListing['BL_Hour_Mon_To']); ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="form-inside-div hours">
                <label>Tuesday</label>
                <div class="form-data">
                    <select name="tueFrom" id="tueFrom" onchange="isClosed('tueHide', this.value);">
                        <?PHP fromHours($rowListing['BL_Hour_Tue_From']); ?>
                    </select>
                    <span id="tueHide" <?php echo $rowListing['BL_Hour_Tue_From'] == '00:00:01' || $rowListing['BL_Hour_Tue_From'] == '00:00:02' || $rowListing['BL_Hour_Tue_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                        <select name="tueTo" id="tueTo" onchange="">
                            <?PHP toHours($rowListing['BL_Hour_Tue_To']); ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="form-inside-div hours">
                <label>Wednesday</label>
                <div class="form-data">
                    <select name="wedFrom" id="wedFrom" onchange="isClosed('wedHide', this.value);">
                        <?PHP fromHours($rowListing['BL_Hour_Wed_From']); ?>
                    </select>
                    <span id="wedHide" <?php echo $rowListing['BL_Hour_Wed_From'] == '00:00:01' || $rowListing['BL_Hour_Wed_From'] == '00:00:02' || $rowListing['BL_Hour_Wed_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                        <select name="wedTo" id="wedTo" onchange="">
                            <?PHP toHours($rowListing['BL_Hour_Wed_To']); ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="form-inside-div hours">
                <label>Thursday</label>
                <div class="form-data">
                    <select name="thuFrom" id="thuFrom" onchange="isClosed('thuHide', this.value);">
                        <?PHP fromHours($rowListing['BL_Hour_Thu_From']); ?>
                    </select>
                    <span id="thuHide" <?php echo $rowListing['BL_Hour_Thu_From'] == '00:00:01' || $rowListing['BL_Hour_Thu_From'] == '00:00:02' || $rowListing['BL_Hour_Thu_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                        <select name="thuTo" id="thuTo" onchange="">
                            <?PHP toHours($rowListing['BL_Hour_Thu_To']); ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="form-inside-div hours">
                <label>Friday</label>
                <div class="form-data">
                    <select name="friFrom" id="friFrom" onchange="isClosed('friHide', this.value);">
                        <?PHP fromHours($rowListing['BL_Hour_Fri_From']); ?>
                    </select>
                    <span id="friHide" <?php echo $rowListing['BL_Hour_Fri_From'] == '00:00:01' || $rowListing['BL_Hour_Fri_From'] == '00:00:02' || $rowListing['BL_Hour_Fri_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                        <select name="friTo" id="friTo" onchange="">
                            <?PHP toHours($rowListing['BL_Hour_Fri_To']); ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="form-inside-div hours">
                <label>Saturday</label>
                <div class="form-data">
                    <select name="satFrom" id="satFrom" onchange="isClosed('satHide', this.value);">
                        <?PHP fromHours($rowListing['BL_Hour_Sat_From']); ?>
                    </select>
                    <span id="satHide" <?php echo $rowListing['BL_Hour_Sat_From'] == '00:00:01' || $rowListing['BL_Hour_Sat_From'] == '00:00:02' || $rowListing['BL_Hour_Sat_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                        <select name="satTo" id="satTo" onchange="">
                            <?PHP toHours($rowListing['BL_Hour_Sat_To']); ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="form-inside-div hours">
                <label>Sunday</label>
                <div class="form-data">
                    <select name="sunFrom" id="sunFrom" onchange="isClosed('sunHide', this.value);">
                        <?PHP fromHours($rowListing['BL_Hour_Sun_From']); ?>
                    </select>
                    <span id="sunHide" <?php echo $rowListing['BL_Hour_Sun_From'] == '00:00:01' || $rowListing['BL_Hour_Sun_From'] == '00:00:02' || $rowListing['BL_Hour_Sun_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                        <select name="sunTo" id="sunTo">
                            <?PHP toHours($rowListing['BL_Hour_Sun_To']); ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="form-inside-div">
                <label>Disable Hours</label>
                <div class="form-data">
                    <input name="hDisabled" type="checkbox" style="margin-top: 19px;" value="1" <?php echo $rowListing['BL_Hours_Disabled'] ? 'checked' : '' ?>>
                </div>
            </div>
            <div class="form-inside-div">
                <label>By Appointment</label>
                <div class="form-data">
                    <input name="happointment" type="checkbox" style="margin-top: 19px;" value="1" <?php echo $rowListing['BL_Hours_Appointment'] ? 'checked' : '' ?>>
                </div>
            </div>

            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" name="submit" value="Save Now"/>
                </div>
            </div>
            <div class="form-inside-div listing-ranking border-none">
                Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_hours_points ?> points
            </div>
        </form> 
    </div>
</div>


<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';

function fromHours($time) {
    echo "<option value='00:00:00'>Select One</option>";
    for ($i = 1; $i < 23; $i++) {
        if ($i == 0) {
            echo "<option value='" . date('H:00:04', mktime($i)) . "'";
            echo $time == date('H:00:04', mktime($i)) ? ' selected' : '';
            echo ">" . date('g:ia', mktime($i, 0)) . "</option>";
        } else {
            echo "<option value='" . date('H:00:00', mktime($i)) . "'";
            echo $time == date('H:00:00', mktime($i)) ? ' selected' : '';
            echo ">" . date('g:ia', mktime($i, 0)) . "</option>";
        }
        echo "<option value='" . date('H:i:00', mktime($i, 30)) . "'";
        echo $time == date('H:i:00', mktime($i, 30)) ? ' selected' : '';
        echo ">" . date('g:ia', mktime($i, 30)) . "</option>";
    }
    echo "<option value='00:00:04'";
    echo $time == '00:00:04' ? ' selected' : '';
    echo ">12:00am</option>";
    echo "<option value='00:30:00'";
    echo $time == '00:30:00' ? ' selected' : '';
    echo ">12:30am</option>";
    echo "<option value='00:00:01'";
    echo $time == '00:00:01' ? ' selected' : '';
    echo ">Closed</option>";
    echo "<option value='00:00:02'";
    echo $time == '00:00:02' ? ' selected' : '';
    echo ">By Appointment</option>";
    echo "<option value='00:00:03'";
    echo $time == '00:00:03' ? ' selected' : '';
    echo ">open 24 hours</option>";
}

function toHours($time) {
    echo "<option value='00:00:00'>Select One</option>";
    for ($i = 1; $i < 23; $i++) {
        if ($i == 0) {
            echo "<option value='" . date('H:00:01', mktime($i)) . "'";
            echo $time == date('H:00:01', mktime($i)) ? ' selected' : '';
            echo ">" . date('g:ia', mktime($i, 0)) . "</option>";
        } else {
            echo "<option value='" . date('H:00:00', mktime($i)) . "'";
            echo $time == date('H:00:00', mktime($i)) ? ' selected' : '';
            echo ">" . date('g:ia', mktime($i, 0)) . "</option>";
        }
        echo "<option value='" . date('H:i:00', mktime($i, 30)) . "'";
        echo $time == date('H:i:00', mktime($i, 30)) ? ' selected' : '';
        echo ">" . date('g:ia', mktime($i, 30)) . "</option>";
    }
    echo "<option value='00:00:04'";
    echo $time == '00:00:04' ? ' selected' : '';
    echo ">12:00am</option>";
    echo "<option value='00:30:00'";
    echo $time == '00:30:00' ? ' selected' : '';
    echo ">12:30am</option>";
}
?>