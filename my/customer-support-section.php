<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    
} else {
    header('Location: index.php');
}

if (isset($_GET['s_id'])) {
    $sqlSection = "SELECT * FROM tbl_Support WHERE S_ID = " . $_GET['s_id'];
    $resultSection = mysql_query($sqlSection, $db) or die("Invalid query: $sqlSection -- " . mysql_error());
    $rowresultSection = mysql_fetch_assoc($resultSection);
}
require_once '../include/my/header.php';
?>
<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">Support</div>
        <div class="title float-right-text"><?php
            if (isset($_SESSION['successMsg'])) {
                echo $_SESSION['successMsg'];
                unset($_SESSION['successMsg']);
            }
            ?></div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-B-support.php'; ?>
    </div>
    <div class="right">
        <div class="listing-inside-div-tittle">
        </div>
        <form action="" method="post" enctype="multipart/form-data" name="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <div class="content-header">
                <div class="title">
                    <?php
                    echo $rowresultSection['S_Name'];
                    ?> - Support
                </div>
            </div>
            <div class="form-inside-div store margin-none border-none">
                <?php if (filter_var($rowresultSection['S_Video'], FILTER_VALIDATE_URL) === FALSE) { ?>
                <?php } else { ?>
                    <iframe frameborder="0" width="580" height="326" src="<?php echo $rowresultSection['S_Video'] ?>"></iframe>
                <?php } ?>
            </div>
            <div class="form-inside-div store margin-none border-none text-algn-center">
                <?php
                if ($rowresultSection['S_PDF'] != "") {
                    header('Content-type: application/pdf');
                    header('Content-Disposition: attachment; filename="' . $rowresultSection['S_PDF'] . '"');
                    ?>
                    <a target="_blank" href="http://<?php echo DOMAIN . IMG_LOC_REL . $rowresultSection['S_PDF'] ?>" class="instructional-video support-padding margin-none">Download PDF</a>
                    <?php
                } else {
                    $_SESSION['successMsg'] = 'File not found';
                    ?>
                    <a href="customer-support-section.php?bl_id=<?php echo $BL_ID ?>&s_id=<?php echo $rowresultSection['S_ID'] ?>" class="instructional-video support-padding margin-none">Download PDF</a>
                <?php }
                ?>
            </div> 
            <div class="form-inside-div border-none">
                <?php echo $rowresultSection['S_Description']; ?>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>