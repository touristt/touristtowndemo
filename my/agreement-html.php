<?PHP
$sign = $_REQUEST['sign'];

require_once '../include/config.inc.php';
$bypass = true;
require_once '../include/business.login.inc.php';

$sql = "SELECT BL_Contact, DATE_FORMAT(B_Agreement_Accepted_Date,'%D') as Aday, 
        DATE_FORMAT(B_Agreement_Accepted_Date,'%M') as Amonth, DATE_FORMAT(B_Agreement_Accepted_Date,'%Y') as Ayear 
        FROM tbl_Business LEFT JOIN tbl_Business_Listing ON B_ID = BL_B_ID WHERE B_ID = '" . encode_strings($_SESSION['BUSINESS_ID'], $db) . "' 
        ORDER BY BL_ID DESC LIMIT 1";
$result = mysql_query($sql, $db);
$rowBIZ = mysql_fetch_assoc($result);
?>
<html>
    <head>
        <title>Agreement</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="http://<?php echo DOMAIN ?>/include/tt.css" rel="stylesheet" type="text/css" />
    </head>

    <body style="background-color: #FFF;">
        <p>Terms & Conditions/Privacy Statement<br>
            This AGREEMENT (the "Agreement") is made and entered into between 2299701 ONTARIO INC. o/a Tourist Town Online Solutions ("Tourist Town") and you ("Client") (each being referred to individually as a "Party" and collectively as the "Parties"). By registering for an account with Tourist Town you hereby agree to all of the terms and conditions contained in this agreement:</p>
            <!--<p>Services<br>-->
        <p>Tourist Town Services<br>
            Tourist Town agrees to provide Client with services for hosting Client content within the Tourism Website, using the Tourist Town system, on the World Wide Web portion of the Internet so that the website is accessible to third parties as specified herein. Except as expressly provided herein, Client agrees that Tourist Town is responsible only for providing the Tourist Town Services specified in its Pricing & Features Schedule, and Tourist Town is not responsible for providing any services or performing any tasks not specifically set forth in the Pricing & Features Schedule.</p>
        <p>Content<br>
            Client shall be responsible for maintaining its own content via its member login area. By accepting this agreement, Client agrees to give permission to use its content (including, but not limited to images, description, location, contact information, email, website) on the Tourist Town website.</p>
        <p>Availability of Web Site<br>
            The Web Site shall be accessible to third parties via the World Wide Web portion of the Internet twenty-four (24) hours a day, seven (7) days a week, except for scheduled maintenance and required repairs, and except for any loss or interruption of Tourist Town services due to causes beyond the control of Tourist Town or which are not reasonably foreseeable by Tourist Town, including, but not limited to, interruption or failure of telecommunication or digital transmission links and Internet slow-downs or failures. In the event of any loss or interruption of Tourist Town services, Client's sole and exclusive remedy and Tourist Town's sole and exclusive liability for any loss or interruption of Tourist Town Services shall be as follows: for loss or interruption of Tourist Town Services which is due to (i) causes other than scheduled maintenance and required repairs, or (ii) causes beyond the control of Tourist Town, or (iii) causes which are not reasonably foreseeable by Tourist Town, including, but not limited to, interruption or failure of telecommunication or digital transmission links and Internet slow-downs or failures, which loss or interruption of Tourist Town Services exceeds a total period of three (3) hours and thirty-six (36) minutes, Client shall receive a credit against future Tourist Town services up to the fees paid by the client for this month, not exceeding the total amount of Tourist Town Service fees paid by the Client to the Tourist Town in the previous month. Client must request any service credit within seven (7) days after the end of the billing period in which the interruption of Tourist Town services exceeded a total period of three (3) hours and thirty-six (36) minutes.</p>
        <p>Client Content<br>
            Client assumes sole responsibility for: (a) acquiring any authorization(s) necessary for hypertext links to third party websites; (b) the accuracy of materials on the website, including, without limitation, client content, descriptive claims, warranties, guarantees, nature of business, and address where business is conducted; and (c) ensuring that the client content does not infringe or violate any right of any third party. Notwithstanding the foregoing, Tourist Town reserves the right, in its sole discretion, to exclude or remove from the website any hypertext links to third party web sites, any client content on the website, or other content not supplied by Tourist Town which, in Tourist Town's sole reasonable discretion, may violate or infringe any law or third party rights or which otherwise exposes or potentially exposes Tourist Town to civil or criminal liability or public ridicule, provided that such right shall not place an obligation on Tourist Town to monitor or exert editorial control over the website.</p>
        <p>Limitations on Client Content<br>
            Client shall place only content that does not contain any materials which are obscene, threatening, malicious, which infringe on or violate any applicable law or regulation or any proprietary, contract, moral, privacy or other third party right, or which otherwise exposes Tourist Town to civil or criminal liability. Any such materials placed on the website which do not satisfy the foregoing requirements shall be deemed to be a material breach of this Agreement.</p>
        <p>Resource Usage<br>
            Client may not initiate the following on our servers:</p>
        <ul>
            <li>any process that requires more than 8Mb of memory space, more than 30 CPU seconds, or use more than 5% of all available system resources at any time;</li>
            <li>any type of interactive real-time chat applications that require server resources;</li>
            <li>stand-alone, unattended server-side processes at any point in time on the server;</li>
            <li>any software that interfaces with an IRC (Internet Relay Chat) network; and</li>
            <li>remote access to databases located on the Tourist Town's servers without written approval.</li>
        </ul>
        <p>Spam<br>
            Client shall not send bulk email, commonly known as spam, from or through their account. Any use of Client's account to send bulk email shall be a material breach of this agreement and shall be grounds for immediate cancellation of Client's account without notice. Any charges related to spam will be charged to Client.</p>
        <p>Refusal or discontinuation of service<br>
            Tourist Town reserves the right to refuse or discontinue service to anyone at Tourist Town's sole discretion. Tourist Town may deny you access to all or part of the service without notice if you engage in any conduct or activities that Tourist Town in its sole discretion believes violates any of the terms and conditions in this agreement. Tourist Town shall have no responsibility to notify any third-party providers of services, merchandise, or information, nor any responsibility for any consequences resulting from such discontinuance or lack of notification. You agree that Tourist Town has the right to monitor the service electronically from time to time and to disclose any information as necessary to satisfy the law, or to protect itself or its subscribers. Tourist Town reserves the right to refuse to post or to remove any information or materials, in whole or in part, that, in its sole discretion, are unacceptable, undesirable, or in violation of this agreement. Tourist Town also reserves the right to refuse refunds in cases where Tourist Town believes abuse has taken place.</p>
        <p><br>
            Content Guidelines<br>
            Tourist Town reserves the right to remove any content from your account at its discretion. Tourist Town also reserves the right to suspend your account at its discretion. Members posting inappropriate content are subject to having their accounts suspended, or cancelled, without any financial reimbursement. Inappropriate content includes, but is not limited to:</p>
        <ul>
            <li>Text, graphics, sound, or animations that might be viewed as obscene or any illegal activities</li>
            <li>Links to other web sites that might be viewed as obscene or related in any way to any illegal activities</li>
            <li>Satirical impressionistic or cartoon-like graphics</li>
            <li>Invisible text, (i.e., text that is present only when a "Webcrawler" or other Web indexing tool accesses the Web site), or any other type of hidden text, hidden information, hidden graphics, or other hidden materials; or destructive elements or destructive programming of any type.</li>
            <li>Content that: is hateful, threatening, or pornographic; incites violence; or contains nudity or graphic or gratuitous violence</li>
            <li>On-line gambling</li>
            <li>Spam, no unsolicited material</li>
            <li>Copyright infringement</li>
            <li>Defamatory remarks</li>
            <li>Content referring to Municipal Election campaigns</li>
            <li>Content referring to Municipal decisions or issues</li>
        </ul>
        <p>&nbsp;</p>
        <p>Termination and Renewal<br>
            This Agreement shall be effective when user agrees to this agreement. Client assents by virtue of clicking the button at the end of this agreement. The clicking of the button at the end of this agreement shall constitute signing by the Parties and thereafter the agreement shall remain in effect for one (1) year unless earlier terminated as otherwise provided in this Agreement (the "Initial Term"). This Agreement shall automatically be renewed beyond the Initial Term unless Client provides Tourist Town with a written notice of termination at least thirty (30) days prior to the expiration of the Initial Term.<br>
            <br>
            <br>
            Termination and Payment<br>
            Tourist Town may terminate this Agreement at any time and for any reason by providing written notice of termination to Client and refunding a pro rata portion of fees paid to Client for Tourist Town Services not yet rendered on the date of termination. Upon any termination or expiration of this Agreement, Client shall pay all unpaid and outstanding fees through the effective date of termination or expiration of this Agreement.</p>
        <p>&nbsp;</p>
        <p>Indemnification<br>
            <br>
            A. Client<br>
            Client agrees to indemnify, defend, and hold harmless Tourist Town, its directors, officers, employees and agents, and defend any action brought against same with respect to any claim, demand, cause of action, debt or liability, including reasonable legal fees, to the extent that such action is based upon a claim that: (i) if true, would constitute a breach of any of Client's representations, warranties, or agreements hereunder; (ii) arises out of the negligence or willful misconduct of Client; or (iii) any of the Client Content to be provided by Client hereunder or other material on the Web Site infringes or violates any rights of third parties, including without limitation, rights of publicity, rights of privacy, patents, copyrights, trademarks, trade secrets, and/or licenses.<br>
            <br>
            B. Tourist Town<br>
            Tourist Town agrees to indemnify, defend, and hold harmless Client, its directors, officers, employees and agents, and defend any action brought against same with respect to any claim, demand, cause of action, debt or liability, including reasonable legal fees, to the extent that such action arises out of the gross negligence or willful misconduct of Tourist Town.<br>
        </p>
        <p>Notice<br>
            In claiming any indemnification hereunder, the indemnified Party shall promptly provide the indemnifying Party with written notice of any claim which the indemnified Party believes falls within the scope of the foregoing paragraphs. The indemnified Party may, at its own expense, assist in the defense if it so chooses, provided that the indemnifying Party shall control such defense and all negotiations relative to the settlement of any such claim and further provided that any settlement intended to bind the indemnified Party shall not be final without the indemnified Party's written consent, which shall not be unreasonably withheld.<br>
        </p>
        <p>Limitation of Liability<br>
            Tourist Town shall have no liability for unauthorized access to, or alteration, theft or destruction of, the website or client's data files, programs or information through accident, fraudulent means or devices. Tourist Town shall have no liability with respect to tourist town�s obligations under this agreement or otherwise for consequential, exemplary, special, incidental, or punitive damages even if Tourist Town has been advised of the possibility of such damages. In any event, the liability of Tourist Town to Client for any reason and upon any cause of action shall be limited to the amount actually paid to Tourist Town by client under this agreement during the month immediately preceding the date on which such claim accrued. This limitation applies to all causes of action in the aggregate, including, without limitation, to breach of contract, breach of warranty, negligence, strict liability, misrepresentations, and other torts.</p>

        <p><br>
              Privacy</p>
        <p>
            Website Visitors<br>
            Like most website operators, Tourist Town collects non-personally-identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. Tourist Town�s purpose in collecting non-personally identifying information is to better understand how Tourist Town�s visitors use its website. From time to time, Tourist Town may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of its website.<br>
            Tourist Town may also collect potentially personally-identifying information like Internet Protocol (IP) addresses for logged in users. Tourist Town only discloses logged in user and commenter IP addresses under the same circumstances that it uses and discloses personally-identifying information as described below.</p>
        <p>
            Gathering of Personally-Identifying Information<br>
            Certain visitors to Tourist Town�s websites choose to interact with Tourist Town in ways that require Tourist Town to gather personally-identifying information. The amount and type of information that Tourist Town gathers depends on the nature of the interaction. For example, we ask members to provide a username and email address. Those who engage in transactions with Tourist Town are asked to provide additional information, including as necessary the personal and financial information required to process those transactions. In each case, Tourist Town collects such information only insofar as is necessary or appropriate to fulfill the purpose of the visitor�s interaction with Tourist Town. Tourist Town does not disclose personally-identifying information other than as described below. Visitors can always refuse to supply personally-identifying information, with the caveat that it may prevent them from engaging in certain website-related activities</p>
        <p>
            Aggregated Statistics<br>
            Tourist Town may collect statistics about the behaviour of visitors to its websites. Tourist Town may display this information publicly or provide it to others. However, Tourist Town does not disclose personally-identifying information other than as described below.</p>
        <p>
            Protection of Certain Personally-Identifying Information<br>
            Tourist Town discloses potentially personally-identifying and personally-identifying information only to those of its employees, contractors and affiliated organizations that (i) need to know that information in order to process it on Tourist Town�s behalf or to provide services available at Tourist Town�s websites, and (ii) that have agreed not to disclose it to others. Some of those employees, contractors and affiliated organizations may be located outside of Canada; by using Tourist Town�s websites, you consent to the transfer of such information to them. Tourist Town will not rent or sell potentially personally-identifying and personally-identifying information to anyone. Other than to its employees, contractors and affiliated organizations, as described above, Tourist Town discloses potentially personally-identifying and personally-identifying information only in response to a subpoena, court order or other governmental request, or when Tourist Town believes in good faith that disclosure is reasonably necessary to protect the property or rights of Tourist Town, third parties or the public at large. If you are a registered user of a Tourist Town website and have supplied your email address, Tourist Town may occasionally send you an email to tell you about new features, solicit your feedback, or just keep you up to date with what�s going on with Tourist Town and our products. We primarily use our various product blogs to communicate this type of information, so we expect to keep this type of email to a minimum. If you send us a request (for example via a support email or via one of our feedback mechanisms), we reserve the right to publish it in order to help us clarify or respond to your request or to help us support other users. Tourist Town takes all measures reasonably necessary to protect against the unauthorized access, use, alteration or destruction of potentially personally-identifying and personally-identifying information.</p>
        <p>
            Cookies<br>
            A cookie is a string of information that a website stores on a visitor�s computer, and that the visitor�s browser provides to the website each time the visitor returns. Tourist Town uses cookies to help Tourist Town identify and track visitors, their usage of Tourist Town website, and their website access preferences. Tourist Town visitors who do not wish to have cookies placed on their computers should set their browsers to refuse cookies before using Tourist Town�s websites, with the drawback that certain features of Tourist Town�s websites may not function properly without the aid of cookies.</p>
        <p>
            Business Transfers<br>
            If Tourist Town, or substantially all of its assets were acquired, or in the unlikely event that Tourist Town goes out of business or enters bankruptcy, user information would be one of the assets that is transferred or acquired by a third party. Client acknowledges that such transfers may occur, and that any acquirer of Tourist Town may continue to use Client�s personal information as set forth in this policy.</p>
        <p>
            Ads<br>
            Ads appearing on any of our websites may be delivered to users by advertising partners, who may set cookies. These cookies allow the ad server to recognize your computer each time they send you an online advertisement to compile information about you or others who use your computer. This information allows ad networks to, among other things, deliver targeted advertisements that they believe will be of most interest to you. This Privacy Policy covers the use of cookies by Tourist Town and does not cover the use of cookies by any advertisers.</p>
        <p>
            Privacy Policy Changes<br>
            Although most changes are likely to be minor, Tourist Town may change its Privacy Policy from time to time, and in Tourist Town�s sole discretion. Tourist Town encourages visitors to frequently check this page for any changes to its Privacy Policy. Your continued use of this site after any change in this Privacy Policy will constitute your acceptance of such change</p>
        <p>
              Fees<br>
            Tourist Town Services Fees<br>
            Client shall pay Tourist Town all fees for the Tourist Town services in accordance with the applicable fee and payment schedule set forth in the Tourist Town Pricing & Features Schedule. Tourist Town expressly reserves the right to change its rates charged hereunder for the services during any Renewal Term (as defined herein). Service cancellation or change requests must be submitted thirty (30) day prior to the account renewal date.</p>
        <p>Late Payment<br>
            Tourist Town may terminate service when Client's payment is late and shall not be responsible for maintaining any data Client may have uploaded to Tourist Town's server. Tourist Town reserves the right to suspend the Client's services after 72 hours if payment is not received or no other arrangement has been made acceptable to Tourist Town. Immediately upon account suspension, Tourist Town is not responsible in any way for Client's data. Additionally, no account will be re-activated until all outstanding fees owing are paid in full.</p>
        <p>Reactivation Fee<br>
            In the event an account has been terminated or suspected due to late or non-payment, Tourist Town reserves the right to levy a reactivation fee equal to the last month�s service charges paid by the client to cover administrative and processing fees. Exceptions may be made in full at Tourist Town's absolute discretion, if Tourist Town is notified IN ADVANCE of any late payments.</p>
        <p>GENERAL</p>

        <p>Disclaimer of Warranty<br>
            Tourist Town makes no warranties hereunder, and host expressly disclaims all other warranties, express or implied, including, without limitation, warranties of merchantability and fitness for a particular purpose.</p>
        <p>
            Entire Agreement<br>
            This Agreement and Schedules referenced herein constitute the entire agreement between Client and Tourist Town with respect to the subject matter hereof and there are no representations, understandings or agreements which are not fully expressed in this Agreement.</p>
        <p>
            Force Majeure<br>
            Except for the payment of fees by Client, if the performance of any part of this Agreement by either Party is prevented, hindered, delayed or otherwise made impracticable by reason of any flood, riot, fire, judicial or governmental action, labour disputes, act of God or any other causes beyond the control of either Party, that Party shall be excused from such to the extent that it is prevented, hindered or delayed by such causes.</p>
        <p>
            Canadian Law<br>
            This Agreement shall be governed in all respects by the laws of Canada and Client and Tourist Town agree that the sole venue and jurisdiction for disputes arising from this Agreement shall be the appropriate province or national court as selected by Tourist Town. Client and Tourist Town hereby submit to the jurisdiction of such courts.</p>
        <p>
            Assignment<br>
            Client shall not assign, without the prior written consent of Tourist Town, its rights, duties or obligations under this Agreement to any person or entity, in whole or in part, whether by assignment, merger, transfer of assets, sale of stock, operation of law or otherwise, and any attempt to do so shall be deemed a material breach of this Agreement.</p>
        <p>
            Modification and Notice<br>
            Tourist Town has the right to modify this Agreement. Any modification is effective immediately upon either a posting on the support helpdesk, or upon notice by electronic mail, or postal mail. Client's continued use of the Tourist Town's Service following notice of any modification to this Agreement shall be conclusively deemed an acceptance of all such modification(s). Client's only right with respect to any dissatisfaction with any modifications made pursuant to this provision, or any policies or practices of Tourist Town in providing the Services, including, without limitation, (i) any change in the content of the Services, or (ii) any change in the amount or type of Service Fees, is to terminate this agreement by delivering notice to Tourist Town. Such notice will be effective upon receipt by Tourist Town.</p>
        <p>Waiver<br>
            The waiver of failure of either Party to exercise any right in any respect provided for herein shall not be deemed a waiver of any further right hereunder.</p>
        <p>
            Severability<br>
            If any provision of this Agreement is determined to be invalid under any applicable statute or rule of law, it is to that extent to be deemed omitted, and the balance of the Agreement shall remain enforceable.</p>
        <p>
            Counterparts<br>
            This Agreement may be executed in several counterparts, all of which taken together shall constitute the entire agreement between the Parties hereto.</p>
        <p>
            Headings<br>
            The section headings used herein are for reference and convenience only and shall not enter into the interpretation hereof.</p>
        <p>
            Approvals and Similar Actions<br>
            Where agreement, approval, acceptance, consent or similar action by either Party hereto is required by any provision of this Agreement, such action shall not be unreasonably delayed or withheld.</p>
        <p>
            Survival<br>
            All provisions of this Agreement relating to Client warranties, confidentiality, non-disclosure, proprietary rights, limitation of liability, Client indemnification obligations and payment obligations shall survive the termination or expiration of this Agreement.</p>
        <p>
            Agreement Acknowledgement<br>
            This agreement supersedes any written, electronic, or oral communication you may have had with Tourist Town or any agent or representative thereof, and constitutes the complete and total agreement between the parties. Should any provision of this agreement is determined to be invalid or unenforceable, all other provisions shall remain in full force and effect and said provision shall be reformed only to the extent necessary to make it enforceable. By placing and continuing to maintain or place information on Tourist Town's servers you are stating and acknowledging that you have read the aforementioned terms and conditions and that you understand such terms and conditions and agree to be bound by them.</p>

        <p>
            DATED the 
            <?php echo $sign ? date('jS') : $rowBIZ['Aday'] ?>
            day of 
            <?php echo $sign ? date('M') : $rowBIZ['Amonth'] ?>
            , 
            <?php echo $sign ? date('Y') : $rowBIZ['Ayear'] ?>
            .</p>
        <p><br>
            2299701 ONTARIO INC.<br>
            o/a Tourist Town Online Solutions (&quot;Tourist Town&quot;)</p>
        <p><br>
            CLIENT:<br>
            Name: <?php echo $rowBIZ['BL_Contact'] ?></p>
    </body>
</html>
