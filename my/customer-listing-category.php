<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require '../include/PHPMailer/class.phpmailer.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = isset($_REQUEST['bl_id']) ? $_REQUEST['bl_id'] : 0;

if ($BL_ID > 0) {
    $sql = "SELECT BL_ID, BL_Listing_Title, BL_Description, BL_Listing_Type, BL_Contact, BL_Phone, BL_Website, BL_Toll_Free, BL_Email, BL_Street, BL_Town, 
            BL_Province, BL_PostalCode, BL_Lat, BL_Long, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Description, BL_SEO_Keywords, 
            BL_Search_Words, BL_Billing_Type, BL_CoC_Dis_2, BL_Line_Item_Title, BL_Line_Item_Price FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}
if (isset($_POST['op']) && $_POST['op'] == 'save') {

    if (isset($_POST['newCat']) && $_POST['newCat'] > 0) {
        mysql_query("DELETE tbl_Business_Listing_Category FROM tbl_Business_Listing_Category WHERE BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BLC_M_C_ID ='" . encode_strings($_POST['newCat_mutiple_id'], $db) . "'");
    }
    if (isset($_POST['subcat']) && is_array($_POST['subcat'])) {
        foreach ($_POST['subcat'] as $key => $val) {
            if (substr($key, 0, 1) == 'c') {
                $mykey = substr($key, 1);
                $sql = "UPDATE tbl_Business_Listing_Category SET BLC_C_ID = '" . encode_strings($val, $db) . "' WHERE BLC_ID = '" . encode_strings($mykey, $db) . "'";
                $result = mysql_query($sql, $db);
            } else {
                if ($val > 0) {
                    $sql = "INSERT tbl_Business_Listing_Category SET 
                            BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                            BLC_M_C_ID = '" . encode_strings($_POST['category'], $db) . "',
                            BLC_C_ID = '" . encode_strings($val, $db) . "'";
                    $result = mysql_query($sql, $db);
                    if ($result) {
                        $_SESSION['success'] = 1;
                    } else {
                        $_SESSION['error'] = 1;
                    }
                }
            }
        }
    }
    listing_billing($rowListing['BL_Billing_Type'], $BL_ID, $rowListing['BL_CoC_Dis_2'], $rowListing['BL_Line_Item_Title'], $rowListing['BL_Line_Item_Price']);
    // TRACK DATA ENTRY
    $id = $BL_ID;
    Track_Data_Entry('Listing', $id, 'Category', '', 'Update Category', 'user admin');
    header("Location: customer-listing-category.php?bl_id=" . $BL_ID);
    exit();
}
if (isset($_POST['update_cat_details'])) {
    $sql = "INSERT tbl_Request_Content_Change SET 
            RCC_BL_ID = '" . encode_strings($BL_ID, $db) . "',
            RCC_Description = '" . encode_strings($_POST['cat_update_des'], $db) . "',
            RCC_User_ID = '" . encode_strings($_SESSION['BUSINESS_ID'], $db) . "', 
            RCC_Status = '" . encode_strings(0, $db) . "',
            RCC_Created_Date = CURDATE()";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success_cat'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Category', '', 'Update Category Details', 'user admin');
    } else {
        $_SESSION['error_cat'] = 1;
    }
}
if (isset($_POST['op']) && $_POST['op'] == 'save_new') {
    foreach ($_POST['subcat'] as $val) {
        $sql = "INSERT tbl_Business_Listing_Category SET 
                BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                BLC_M_C_ID = '" . encode_strings($_POST['category'], $db) . "',
                BLC_C_ID = '" . encode_strings($val, $db) . "'";
    }
    $res1 = mysql_query($sql, $db);
    if ($res1) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Category', '', 'Add Category', 'user admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: customer-listing-category.php?bl_id=" . $BL_ID);
    exit();
}
if (isset($_POST['op']) && $_POST['op'] == 'save_type') {
    $sql_change_type = "SELECT * FROM tbl_Business_Listing
                        LEFT JOIN tbl_Business on BL_B_ID = B_ID
                        WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $result_change_type = mysql_query($sql_change_type, $db);
    $row_change_type = mysql_fetch_array($result_change_type);
    $sql = "tbl_Business_Listing SET 
            BL_Listing_Type = '" . encode_strings($_POST['type'], $db) . "'";

    if ($_POST['type'] <> $rowListing['BL_Listing_Type']) {
        $sqlTMP = "SELECT * FROM tbl_Listing_Type WHERE LT_ID = '" . encode_strings($_POST['type'], $db) . "'";
        $result = mysql_query($sqlTMP, $db) or die("Invalid query: $sqlTMP -- " . mysql_error());
        $row = mysql_fetch_assoc($result);

        $sql .= ", BL_Price = '" . encode_strings($row['LT_Cost'], $db) . "'";
        if ($_POST['type'] == 1) {
            $sql .= ", BL_Basic_Points = '" . encode_strings(0, $db) . "',
                      BL_Addon_Points = '" . encode_strings(0, $db) . "',
                      BL_Points = '" . encode_strings(0, $db) . "',
                      BL_SubTotal = '" . encode_strings(0.00, $db) . "',
                      BL_CoC_Dis_2 = '" . encode_strings(0.00, $db) . "',
                      BL_SubTotal3 = '" . encode_strings(0.00, $db) . "',
                      BL_Tax = '" . encode_strings(0.00, $db) . "',
                      BL_Total = '" . encode_strings(0.00, $db) . "',
                      BL_Renewal_Date = '" . encode_strings('0000-00-00', $db) . "',
                      BL_Renewal_Date_Last_Update = '" . encode_strings($_SESSION['USER_EM'] . '-' . $_SESSION['user_online'] . '-' . $_SESSION['USER_ID'], $db) . '-' . $date . "'";
        }
    }
    if (isset($_POST['newCat']) && $_POST['newCat'] > 0) {
        $sql .= ", BL_C_ID = '" . encode_strings($_POST['category'], $db) . "'";
        mysql_query("DELETE tbl_Business_Listing_Category FROM tbl_Business_Listing_Category WHERE BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "'");
    }

    $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $res = mysql_query($sql, $db);
    // TRACK DATA ENTRY
    $id = $BL_ID;
    Track_Data_Entry('Listing', $id, 'Category', '', 'Update Type', 'user admin');
    if ($_POST['type'] == 1) {
        $sql = "UPDATE tbl_Advertisement set A_Status = 4 WHERE A_BL_ID = $BL_ID";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    } else {
        $sql = "UPDATE tbl_Advertisement set A_Status = 3 WHERE A_BL_ID = $BL_ID";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $_SESSION['update_photo'] = 1;
    }
    //////Email template 
    $email = "SELECT * FROM tbl_Email_Templates WHERE ET_ID = 11";
    $result_data   = mysql_query($email);
    $rowEmail      = mysql_fetch_assoc($result_data);
    $email_subject = $rowEmail['ET_Subject'];
    $email_heading = $rowEmail['ET_Name'];
    $email_body    = $rowEmail['ET_Template'];
    $email_footer  = $rowEmail['ET_Footer'];
    
    if ($res) {
       if ($row_change_type['BL_Listing_Type'] == 4) {
            $message_body_data = str_replace("(Listing name)", $row_change_type['BL_Listing_Title'], $email_body);
            if ($_POST['type'] == 1) {
                ob_start();
                include '../include/email-template/my-change-request-email.php';
                $message = ob_get_contents();
                $mail = new PHPMailer();
                $mail->From = $row_change_type['B_Email'];
                $mail->FromName = $row_change_type['BL_Listing_Title'];
                $mail->IsHTML(true);
                $mail->AddAddress('muhammad.tanweer@app-desk.com');
                $mail->Subject = $email_subject;
                $mail->MsgHTML($message);
               // $mail->Send();
            }
        }
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }

    if ($_POST['type'] > 1) {
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    }
    header("Location: customer-listing-category.php?bl_id=" . $BL_ID);
    exit();
}
if (isset($_REQUEST['delete']) && $_REQUEST['delete'] == 'true') {
    $sql = "DELETE tbl_Business_Listing_Category FROM tbl_Business_Listing_Category WHERE BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BLC_C_ID = '" . encode_strings($_REQUEST['cid'], $db) . "'";
    $result = mysql_query($sql, $db);
    // TRACK DATA ENTRY
    $id = $BL_ID;
    Track_Data_Entry('Listing', $id, 'Category', '', 'Delete Subcategory', 'user admin');
    if ($result) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
    listing_billing($rowListing['BL_Billing_Type'], $BL_ID, $rowListing['BL_CoC_Dis_2']);
    header("Location: customer-listing-category.php?bl_id=" . $BL_ID);
    exit();
}
if (isset($_REQUEST['del_cat']) && $_REQUEST['del_cat'] > 0) {
    $sql = "DELETE FROM tbl_Business_Listing_Category WHERE BLC_M_C_ID = '" . encode_strings($_REQUEST['del_cat'], $db) . "' AND BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $result = mysql_query($sql, $db);
    // TRACK DATA ENTRY
    $id = $BL_ID;
    Track_Data_Entry('Listing', $id, 'Category', '', 'Delete Category', 'user admin');
    if ($result) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
    listing_billing($rowListing['BL_Billing_Type'], $BL_ID, $rowListing['BL_CoC_Dis_2']);
    header("Location: customer-listing-category.php?bl_id=" . $BL_ID);
    exit();
}

require_once '../include/my/header.php';
?>

<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">My Page</div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div>
        <div class="instruction">
            Fields with this background<span></span>will show on free listings profile.
        </div>
    </div>

    <div class="left">
        <?PHP
        if ($BL_ID) {
            require '../include/nav-B-mypage.php';
        }
        ?>
    </div>

    <div class="right">
        <div class="content-header">
            <div class="title">Category</div>
        </div>
        <form name="frmCategory" id="frmCategory" method="post" action="">
            <?php
            $sql_type = "SELECT BL_Listing_Type FROM tbl_Business_Listing WHERE BL_ID='" . encode_strings($BL_ID, $db) . "'";
            $result_type = mysql_query($sql_type);
            $row_type = mysql_fetch_array($result_type);
            ?>
            <input type="hidden" name="op" value="save_type">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <div class="form-inside-div">
                <div class="content-sub-header" style="margin-bottom:10px;">
                    Listing Type
                </div>
                <label>Listing Type</label>
                <div class="form-data">
                    <?php
                    $sql = "SELECT * FROM tbl_Listing_Type WHERE LT_ID != 5 ORDER BY LT_Order";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    ?>
                    <select name="type" id="select" >

                        <option>Select Listing Type</option>
                        <?PHP
                        if ($row_type['BL_Listing_Type'] == 1) {
                            ?>
                            <option value="1" <?php echo $row_type['BL_Listing_Type'] == 1 ? 'selected' : '' ?>>Free</option>
                            <?php
                        } else {
                            $js = '';
                            while ($row = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo $row['LT_ID'] ?>" <?php echo $row_type['BL_Listing_Type'] == $row['LT_ID'] ? 'selected' : '' ?>><?php echo $row['LT_Name'] ?></option>
                                <?PHP
                            }
                        }
                        ?>
                    </select>

                </div>
            </div>
            <div class="form-inside-div" style="border:none;">
                <div class="button">
                    <input type="submit" name="button2" value="Save" class="cat-button" onclick="return checktype()"/>
                </div>
            </div>
        </form>  
        <?php
        $sql = "SELECT * FROM tbl_Business_Listing_Category
                            LEFT JOIN tbl_Business_Listing ON BL_ID=BLC_BL_ID
                            WHERE BLC_BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "' GROUP BY  BLC_M_C_ID";
        $result_get_details = mysql_query($sql, $db);
        $main_cat_count = 1;
        while ($rowListing = mysql_fetch_assoc($result_get_details)) {
            ?>
            <div class="content-header">
                <?php
                $sql_get_name = "SELECT C_Name FROM tbl_Category WHERE C_ID ='" . encode_strings($rowListing['BLC_M_C_ID'], $db) . "'";
                $result_get_name = mysql_query($sql_get_name, $db) or die("Invalid query: $sql_get_name -- " . mysql_error());
                $row_get_name = mysql_fetch_assoc($result_get_name);
                ?>
                <div class="title"><?php echo $row_get_name['C_Name'] ?></div>
                <div class="link">
                    <div class="add-link"></div>            
                </div>
            </div>
            <form name="frmCategory" id="frmCategory" method="post" action="">
                <input type="hidden" name="op" value="save">
                <input type="hidden" name="bid" value="<?php echo $BID ?>">
                <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
                <input type="hidden" name="newCat" id="newCat" value="0">
                <input type="hidden" name="newSubCat" id="newSubCat" value="0">
                <input type="hidden" name="newCat" id="newCat_<?php echo $main_cat_count ?>" value="0">
                <input type="hidden" name="newCat_mutiple_id"  value="<?php echo $rowListing['BLC_M_C_ID'] ?>">
                <div class="form-inside-div">
                    <div class="content-sub-header" style="margin-bottom:10px;">
                        Main Category
                    </div>
                    <label>Main Category</label>
                    <div class="form-data">
                        <select required name="category" id="category_<?php echo $main_cat_count ?>" onChange="get_sub_category(<?php echo $main_cat_count ?>)">

                            <option value="">Select Main Category</option>
                            <?PHP
                            $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = 0 AND C_Is_Blog=0 AND  C_Is_Product_Web=0 ORDER BY C_Order";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            $js = '';
                            while ($row = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo $row['C_ID'] ?>" <?php echo $rowListing['BLC_M_C_ID'] == $row['C_ID'] ? 'selected' : '' ?>><?php echo $row['C_Name'] ?></option>
                                <?PHP
                            }
                            ?>

                        </select>
                        <script type="text/javascript">
                            function confirmCat()
                            {
                                if (confirm('Are you sure, this can not be changed! All sub categories will be deleted.')) {
                                    newCat();
                                } else {
                                    $('#category option').first().attr('selected', 'selected');
                                    return false;
                                }
                            }
                            function newCat()
                            {
                                $("#newCat").val("1");
                                $('#frmCategory').submit();
                                return false;
                            }
                        </script>  


                    </div>
                </div>

                <?PHP
                if ($rowListing['BLC_M_C_ID']) {
                    $sql = "SELECT *, concat('c',BLC_ID) as arrKey FROM tbl_Business_Listing_Category WHERE BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "' and BLC_M_C_ID= '" . encode_strings($rowListing['BLC_M_C_ID'], $db) . "' ORDER BY BLC_ID";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    $subCount = mysql_num_rows($result);

                    if ($_REQUEST['newSubCat']) {
                        $subCount++;
                    }

                    $subs = array();
                    while ($rowSubs = mysql_fetch_assoc($result)) {
                        $subs[] = $rowSubs;
                    }
                    ?>
                    <div class="form-inside-div">
                        <div class="content-sub-header" style="margin-bottom:10px;">
                            Sub Category
                        </div>
                        <label>Sub-Category 1</label>
                        <div class="form-data listings_subcategory_<?php echo $main_cat_count ?>">
                            <select class="instructional-background" name="subcat[<?php echo $subs[0]['arrKey'] ?>]" id="subcat-<?php echo $subs[0]['arrKey'] ?>">
                                <option>Select Sub Category</option>
                                <?PHP
                                $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '" . encode_strings($rowListing['BLC_M_C_ID'], $db) . "' ORDER BY C_Order";
                                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                while ($row = mysql_fetch_assoc($result)) {
                                    ?>
                                    <option value="<?php echo $row['C_ID'] ?>" <?php echo $subs[0]['BLC_C_ID'] == $row['C_ID'] ? 'selected' : '' ?>><?php echo $row['C_Name'] ?></option>
                                    <?PHP
                                }
                                ?>

                            </select>
                        </div>
                    </div>
                    <div id="sub_category_childs_<?php echo $main_cat_count ?>">
                        <?PHP
                        if ($subCount > 1) {
                            for ($i = 1; $i < $subCount; $i++) {
                                ?>
                                <div class="form-inside-div">
                                    <label>Sub-Category <?php echo $i + 1 ?></label>
                                    <div class="form-data">
                                        <select class="instructional-background" name="subcat[<?php echo $subs[$i]['arrKey'] ?>]" id="subcat-<?php echo $subs[$i]['arrKey'] ?>">
                                            <option>Select Sub Category</option>
                                            <?PHP
                                            $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '" . encode_strings($rowListing['BLC_M_C_ID'], $db) . "' ORDER BY C_Order";
                                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                            while ($row = mysql_fetch_assoc($result)) {
                                                ?>
                                                <option value="<?php echo $row['C_ID'] ?>" <?php echo $subs[$i]['BLC_C_ID'] == $row['C_ID'] ? 'selected' : '' ?>><?php echo $row['C_Name'] ?></option>
                                                <?PHP
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="dlt" id="dltRemove-<?php echo $subs[$i]['arrKey'] ?>">
                                        <a class="dlt-category" href="customer-listing-category.php?bl_id=<?php echo $BL_ID ?>&delete=true&cid=<?php echo $subs[$i]['BLC_C_ID'] ?>" onclick="return confirm('Are you sure you want to delete this subcategory?')">Delete</a>
                                    </div>
                                </div>
                                <?PHP
                            }
                        }
                        ?>
                    </div>
                    <div class="form-inside-div" id="sub_cat_new_<?php echo $main_cat_count ?>" style="display:none;">
                        <label>Sub-Category <?php echo $subCount + 1 ?></label>
                        <div class="form-data">
                            <?php
                            $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '" . encode_strings($rowListing['BLC_M_C_ID'], $db) . "' ORDER BY C_Order";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            ?>
                            <select class="instructional-background" name="subcat[]" id="subcat-<?php echo $subs[0]['arrKey'] ?>" class="listings_subcategory">
                                <option>Select Sub Category</option>
                                <?PHP
                                while ($row = mysql_fetch_assoc($result)) {
                                    ?>
                                    <option value="<?php echo $row['C_ID'] ?>"><?php echo $row['C_Name'] ?></option>
                                    <?PHP
                                }
                                ?>

                            </select>
                        </div>
                    </div>
                    <div class="form-inside-div" style="border:none;">
                        <div class="button">
                            <?php if ($_REQUEST['newSubCat']) { ?>
                                <input type="button" name="button2" value="Save Now" onClick='form_submit()' class="cat-button"/>
                            <?php } else { ?>
                                <input type="submit" name="button2" value="Save Now"/>
                            <?php }
                            ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </form>
            <?PHP
            $main_cat_count++;
        }
        ?>
        <form enctype="multipart/form-data" method="post" name="form" id="update-cat-sub-cat" style="display:none; overflow: hidden">
            <input type="hidden"  name="bl_id" value="<?php echo $BL_ID ?>" required>
            <div class="pop-des-label-cat">
                <label class="">Description</label>
                <div class="popup-des-label-cat-input">
                    <textarea  name="cat_update_des" placeholder="Enter category and sub-category detail"  required/></textarea>
                </div>
            </div>
            <div class="form-inside-div border-none text-algn-center">
                <div class="button">
                    <input type="submit" class="first-div" name="update_cat_details"  value="Save Now"/>
                </div>  
            </div>
        </form>
        <div class="form-inside-div category" style="border:none;">
            <label>Add Sub-Category</label>
            <?php
            $help_text = show_help_text('Category');
            if ($help_text != '') {
                echo '<p>' . str_replace('href="#"', 'class="category-pointer" onclick="show_pop_form()"', $help_text) . '</p>';
            }
            ?>
        </div>


        <script type="text/javascript">
            function newSubCat(id)
            {
                $("#sub_cat_new_" + id).show();
            }
            function form_submit() {
                $('#frmCategory').submit();
            }
            $(function () {
                $("#dltRemove-").remove();
            });
            function get_sub_category(count_id)
            {
                var cat_id = $('#category_' + count_id).val();
                var bl_id = $('#bl_id').val();
                $.ajax({
                    type: "GET",
                    url: "get-customer-listing-subcat-new.php",
                    data: {
                        cat_id: cat_id,
                        bl_id: bl_id
                    }
                })
                        .done(function (msg) {
                            $("#newCat_" + count_id).val("1");
                            $("#sub_category_childs_" + count_id).remove();
                            $("#add_new_sub_cat_" + count_id).remove();
                            $(".listings_subcategory_" + count_id).empty();
                            $(".listings_subcategory_" + count_id).html(msg);
                        });
            }
            function show_form()
            {
                $("#new_main_Category").show();
            }
            function checktype()
            {
                var select_id = $('select').val();
                if (select_id == 1)
                {
                    swal({
                        title: "Enhanced to Free",
                        text: "Are you sure you want to change your listing from Enhanced to Free? Your listing will now only show your Business name and address.",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#6dac29",
                        confirmButtonText: "Change",
                        closeOnConfirm: false
                    }, function () {
                        $("#frmCategory").submit();
                    });
                    return false;
                }
            }
            function show_pop_form() {
                $("#update-cat-sub-cat").dialog({
                    modal: true,
                    draggable: false,
                    resizable: false,
                    width: 500
                });
            }
        </script>

    </div>
</div>

<?PHP
if (isset($_SESSION['success']) && $_SESSION['success'] == 1) {
    print '<script>swal("Data Saved", "Data has been saved successfully.", "success");</script>';
    unset($_SESSION['success']);
}
if (isset($_SESSION['success_cat']) && $_SESSION['success_cat'] == 1) {
    print '<script>swal("Request Sent", "Your request has been sent.", "success");</script>';
    unset($_SESSION['success_cat']);
}
if (isset($_SESSION['error']) && $_SESSION['error'] == 1) {
    print '<script>swal("Error", "Error saving data. Please try again.", "error");</script>';
    unset($_SESSION['error']);
}
if (isset($_SESSION['error_cat']) && $_SESSION['error_cat'] == 1) {
    print '<script>swal("Error", "Error sending request.", "error");</script>';
    unset($_SESSION['error_cat']);
}
if (isset($_SESSION['delete']) && $_SESSION['delete'] == 1) {
    print '<script>swal("Data Deleted", "Data has been deleted successfully.", "success");</script>';
    unset($_SESSION['delete']);
}
if (isset($_SESSION['WARNING']) && $_SESSION['WARNING'] == 1) {
    echo '<script>
            swal({
                    title: "Credit Card Info",   
                    text: "Please provide credit card info before buying Add On!",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#6dac29",   
                    confirmButtonText: "OK",   
                    closeOnConfirm: false 
                }, function(){   
                    window.location.href="customer-listing-credit-card-details.php?bl_id=' . $BL_ID . '" 
                });
          </script>';
    unset($_SESSION['WARNING']);
}
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>