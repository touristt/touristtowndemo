<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];
$F_ID = $_REQUEST['f_id'];

if ($BL_ID > 0) {
    $sql = "SELECT BL_Listing_Type, BL_Billing_Type, BL_CoC_Dis_2, BL_Line_Item_Title, BL_Line_Item_Price FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}

if ($F_ID > 0) {
    $sql = "SELECT * FROM tbl_Feature WHERE F_ID = '" . encode_strings($F_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowFeature = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}

if ($_POST['op'] == 'save') {
    $sql_card = "SELECT card_number, DATE(created_time) AS created_date FROM payment_profiles WHERE business_id = '$BID'";
    $res_card = mysql_query($sql_card);
    $card = mysql_fetch_assoc($res_card);
    if ($card['card_number'] != '' || $rowListing['BL_Listing_Type'] == 5) {
        $sql = "INSERT INTO tbl_BL_Feature(BLF_BL_ID, BLF_F_ID, BLF_Agreement_Status, BLF_Date, BLF_Active) VALUES('" . $BL_ID . "','$F_ID', '1', CURDATE(), 1)";
        $result = mysql_query($sql, $db);
        if ($result) {
            $_SESSION['success'] = 1;
            // TRACK DATA ENTRY
            $id = $BL_ID;
            Track_Data_Entry('Listing', $id, 'Store', $F_ID, 'Add', 'user admin');
            //update points only for one listing
            update_pointsin_business_tbl($BL_ID);
        } else {
            $_SESSION['error'] = 1;
        }

        listing_billing($rowListing['BL_Billing_Type'], $BL_ID, $rowListing['BL_CoC_Dis_2'], $rowListing['BL_Line_Item_Title'], $rowListing['BL_Line_Item_Price']);

        header("Location: customer-feature-store.php?bl_id=" . $BL_ID);
        exit();
    } else {
        $_SESSION['WARNING'] = 1;
    }
}

require_once '../include/my/header.php';
?>
<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">Store</div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-B-listing-feature.php'; ?>
    </div>
    <div class="right">
        <form action="" method="post" enctype="multipart/form-data" name="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="f_id" value="<?php echo $F_ID ?>">
            <div class="content-header">
                <div class="title"><?php echo $rowFeature['F_Name']; ?></div>
                <div class="link">$<?php echo $rowFeature['F_Price']; ?></div>
            </div>
            <div class="form-inside-div store margin-none border-none">
                <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $rowFeature['F_Image'] ?>" alt="" width="580" height="300"/>
            </div>
            <div class="form-inside-div border-none">
                <?php echo $rowFeature['F_Description']; ?>
            </div>

            <div class="form-inside-div store-sub border-none">
                <div class="form-inside-div border-none margin-none">
                    <input type="checkbox" name="agreement" id="check" required>
                    <div id="check-text">I understand that on my next renewal date, my account will change to the selected billing ammount.
                        I also understand that this purchase will be valid for a minimum of 30 days.</div>
                </div>
            </div>


            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" name="button" value="Buy Now"/>
                </div>
            </div> 
        </form>
        <div id="video" style="display:none;padding:0;">
            <iframe frameborder="0" width="700" height="297" src="<?php echo $rowFeature['F_Instruction_Video'] ?>"></iframe>
        </div>
    </div>
</div>
<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>