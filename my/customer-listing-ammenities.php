<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];
$points_taken = 0;
$Coupons_direction = $_REQUEST['coupons'];
$bfc_id = $_REQUEST['bfc_id'];
if ($BL_ID > 0) {
    $sql = "SELECT BL_ID, BL_Listing_Title, BL_Description, BL_Listing_Type, BL_Contact, BL_Phone, BL_Website, BL_Toll_Free, BL_Email, BL_Street, BL_Town, 
            BL_Province, BL_PostalCode, BL_Lat, BL_Long, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Description, BL_SEO_Keywords, 
            BL_Search_Words, C_Name FROM tbl_Business_Listing LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID
            LEFT JOIN tbl_Category ON BLC_M_C_ID = C_ID
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}
if ($_POST['op'] == 'save') {
    $delete = "DELETE FROM tbl_Business_Listing_Ammenity WHERE BLA_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $res = mysql_query($delete, $db);
    foreach ($_POST['AMMid'] as $value) {
        $sql = "INSERT tbl_Business_Listing_Ammenity SET BLA_BA_ID = '$value', 
                BLA_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $res1 = mysql_query($sql, $db);
    }
    if ($res) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Amenities', '', 'Update', 'user admin');
    } else {
        $_SESSION['error'] = 1;
    }
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
             echo '1';  
    }
    //update points only for listing
    update_pointsin_business_tbl($BL_ID);
    exit();
}

if ($rowListing['BL_Listing_Type'] == 1) { // FREE
    $sql = "DELETE tbl_Business_Listing_Ammenity FROM tbl_Business_Listing_Ammenity WHERE BLA_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
}

require_once '../include/my/header.php';
?>
<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">My Page</div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div>
        <div class="instruction">
            Fields with this background<span></span>will show on free listings profile.
        </div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-B-mypage.php'; ?>
    </div>

    <div class="right">

        <div class="content-header">
            <div class="title">Amenities - <?php echo $rowListing['C_Name'] ?></div>
            <div class="link">
                <?PHP
                if ($amm_count > 0) {
                    echo '<div class="points"><div class="points-com">' . $total_amenities_points . ' pts</div></div>';
                    $points_taken += $total_amenities_points;
                } else {
                    echo '<div class="points"><div class="points-uncom">' . $total_amenities_points . ' pts</div></div>';
                }
                ?>
            </div>
        </div>

        <?php
        $help_text = show_help_text('Amenities');
        if ($help_text != '') {
            echo '<div class="form-inside-div border-none">' . $help_text . '</div>';
        }
        ?>

        <?PHP
        if ($rowListing['BL_Listing_Type'] == 1) { // FREE
            echo '<div class="data-content">
                                  Not available for free listings
                                </div>';
        } else {
            $listingAmmenities = array();
            while ($rowBA = mysql_fetch_row($amm_result)) {
                $listingAmmenities[] = $rowBA[0];
            }

            $sql = "SELECT * FROM tbl_Ammenity_Icons ORDER BY AI_Name";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $counter = 0;
            while ($row = mysql_fetch_assoc($result)) {
                if ($counter == 0) {
                    $class = "amenity-column";
                } else {
                    $class = "amenity-column2";
                }
                ?>
                <div class="<?php echo $class ?>">
                    <div class="amenity-image">
                        <input name="ckbxAMM<?php echo $row['AI_ID'] ?>" type="checkbox" id="ckbxAMM<?php echo $row['AI_ID'] ?>" value="<?php echo $row['AI_ID'] ?>" align="absmiddle" <?php echo in_array($row['AI_ID'], $listingAmmenities) ? 'checked' : '' ?> />
                    </div>
                    <div class="amenity-name"><?php echo $row['AI_Name'] ?></div>
                </div>
                <?PHP
                $counter++;
                if ($counter == 2) {
                    $counter = 0;
                }
            }
            ?>
            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" onClick="return updateAMM(<?php echo $BL_ID ?>);" value="Save Now">
                </div>
            </div>
        <?php }
        ?>

        <div class="form-inside-div listing-ranking border-none" style="margin-top: 25px;">
            Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_amenities_points ?> points
        </div>

    </div>
</div>

<script type="text/javascript">
    function updateAMM(bl_id) {
        var checkedValues = $('input:checkbox:checked').map(function() {
            return this.value;
        }).get();
        var go_to_preview = '<?php echo (isset($_REQUEST['go_to_preview']) ? true : '') ?>';
        $.post('customer-listing-ammenities.php', { 
            AMMid: checkedValues,
            bl_id: bl_id,
            op: 'save',
            status: 1,
            go_to_preview: go_to_preview
        }, function(done){
            if(done == 1){
            $(location).attr('href', 'listings-preview.php?bl_id='+bl_id)
            } else {
            location.reload();
            }
            
        });
        return false;
    }

</script>


<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>