<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

$error = empty($_SESSION['error']) ? '' : $_SESSION['error'];

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];
$points_taken = 0;

if ($BL_ID > 0) {
    $sql = "SELECT BL_ID, BL_Listing_Title, BL_Description, BL_Listing_Type, BL_Contact, BL_Phone, BL_Website, BL_Toll_Free, BL_Email, BL_Street, BL_Town, 
            BL_Province, BL_PostalCode, BL_Lat, BL_Long, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Description, BL_SEO_Keywords, 
            BL_Search_Words FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}

if ($_POST['op'] == 'save') {
    $sql = "tbl_Business_Listing SET BL_SEO_Title = '" . encode_strings($_POST['title'], $db) . "', BL_SEO_Description = '" . encode_strings($_POST['description'], $db) . "', 
            BL_SEO_Keywords = '" . encode_strings($_POST['keywords'], $db) . "', BL_Search_Words = '" . encode_strings($_POST['search'], $db) . "'";
    if ($BL_ID > 0) {
        $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Search Engine Optimization','','Update','user admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing',$id,'Search Engine Optimization','','Add','user admin');
    }
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: customer-listing-seo.php?bl_id=" . $BL_ID);
    exit();
}

require_once '../include/my/header.php';
?>
<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">My Page</div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div>
        <div class="instruction">
            Fields with this background<span></span>will show on free listings profile.
        </div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-B-mypage.php'; ?>
    </div>
    <div class="right">
        <div class="listing-inside-div-tittle">
        </div>
        <form action="" method="post" enctype="multipart/form-data" name="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <div class="content-header">Search 
                Engine Optimization
            </div>
            <?php
            if (!empty($error)) {
                echo $error;
                $_SESSION['error'] = '';
                unset($_SESSION['error']);
            }
            $help_text = show_help_text('S.E.O');
            if ($help_text != '') {
                echo '<div class="form-inside-div">' . $help_text . '</div>';
            }
            ?>
            <div class="form-inside-div">
                <label>Title Tag</label>
                <div class="form-data">
                    <input name="title" type="text" size="50" value="<?php echo $rowListing['BL_SEO_Title'] ?>"/> 
                </div>
                <?php
                $title = show_field_points('Title Tag');
                if ($rowListing['BL_SEO_Title']) {
                    echo '<div class="points-com">' . $title . ' pts</div>';
                    $points_taken += $title;
                } else {
                    echo '<div class="points-uncom">' . $title . ' pts</div>';
                }
                ?>
            </div>
            <div class="form-inside-div">
                <label>S.E.O Description</label>
                <div class="form-data">
                    <input name="description" type="text" size="50" value="<?php echo $rowListing['BL_SEO_Description'] ?>"/> 
                </div>
                <?php
                $desc = show_field_points('S.E.O Description');
                if ($rowListing['BL_SEO_Description']) {
                    echo '<div class="points-com">' . $desc . ' pts</div>';
                    $points_taken += $desc;
                } else {
                    echo '<div class="points-uncom">' . $desc . ' pts</div>';
                }
                ?>
            </div>
            <div class="form-inside-div">
                <label>Keywords</label>
                <div class="form-data">
                    <input name="keywords" type="text" size="50" value="<?php echo $rowListing['BL_SEO_Keywords'] ?>"/> 
                </div> 
                <?php
                $keywords = show_field_points('Keywords');
                if ($rowListing['BL_SEO_Keywords']) {
                    echo '<div class="points-com">' . $keywords . ' pts</div>';
                    $points_taken += $keywords;
                } else {
                    echo '<div class="points-uncom">' . $keywords . ' pts</div>';
                }
                ?>
            </div>  
            <div class="form-inside-div border-none">
                <div class="content-sub-header">
                    <div class="title">My Page Searching</div>
                </div>
                <?php
                $help_text1 = show_help_text('My Page Search');
                if ($help_text1 != '') {
                    echo '<div class="form-inside-div margin-none">' . $help_text1 . '</div>';
                }
                ?>
                <div class="form-inside-div margin-none">
                    <label>Search Words</label>
                    <div class="form-data">
                        <input name="search" type="text" size="50" value="<?php echo $rowListing['BL_Search_Words'] ?>" /> 
                    </div>
                    <?php
                    $searchwords = show_field_points('Search Words');
                    if ($rowListing['BL_Search_Words']) {
                        echo '<div class="points-com">' . $searchwords . ' pts</div>';
                        $points_taken += $searchwords;
                    } else {
                        echo '<div class="points-uncom">' . $searchwords . ' pts</div>';
                    }
                    ?>
                </div>
            </div>


            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" name="button2" value="Save Now"/>
                </div>
            </div> 

            <div class="form-inside-div listing-ranking border-none">
                Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_seo_points ?> points
            </div>
        </form>
    </div>
</div>
<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>