<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/adminFunctions.inc.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    
} else {
    header('Location: index.php');
}

require_once '../include/my/header.php';
?>
<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">Store</div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-B-listing-feature.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">
            <div class="title">Store</div>
        </div>
        <?php
        $help_text = show_help_text('Store');
        if ($help_text != '') {
            echo '<div class="form-inside-div">' . $help_text . '</div>';
        }
        ?>
    </div>
</div>
<?PHP
if ($_SESSION['success'] == 1) {
    print '<script>swal("Thank you", "for your purchase, now go to My Add Ons to create your new page", "success");</script>';
    unset($_SESSION['success']);
}
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>