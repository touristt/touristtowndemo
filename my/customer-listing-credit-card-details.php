<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';

if (isset($_REQUEST['bl_id'])) {
    //business id and listing id
    $BID = $_SESSION['BUSINESS_ID'];
    $BL_ID = $_REQUEST['bl_id'];
    $sql = "SELECT BL_ID, BL_Listing_Title, BL_Description, BL_Listing_Type, BL_Contact, BL_Phone, BL_Website, BL_Toll_Free, BL_Email, BL_Street, BL_Town, 
            BL_Province, BL_PostalCode, BL_Lat, BL_Long, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Description, BL_SEO_Keywords, 
            BL_Search_Words, B_ID, B_Email, BLCR_BLC_R_ID FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            LEFT JOIN tbl_Listing_Type ON BL_Listing_Type = LT_ID 
            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
            INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID 
            LEFT JOIN tbl_Category ON BL_C_ID = C_ID 
            WHERE BL_ID = " . $BL_ID . " LIMIT 1";
    $result = mysql_query($sql);
    $rowListing = mysql_fetch_assoc($result);
}

if (isset($_POST['postback'])) {
    $card_number = $_POST['card_number'];
    $exp_date = $_POST['exp_month'] . '/' . $_POST['exp_year'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $item_name = $_POST['item_name'];
    $business_id = $_POST['business_id'];
    $listing_id = $_POST['listing_id'];
    $email = $_POST['email'];
    $region = $_POST['region'];

    // get authorize.net information of the specified region
    $sql_region = "SELECT R_API_Login_ID, R_Transaction_Key FROM tbl_Region WHERE R_ID = $region LIMIT 1";
    $res_region = mysql_query($sql_region);
    $payment_region = mysql_fetch_assoc($res_region);
    require_once 'config_payment.php';

    // Create new customer profile
    $request = new AuthorizeNetCIM;
    $customerProfile = new AuthorizeNetCustomer;
    $customerProfile->description = $item_name;
    $customerProfile->merchantCustomerId = $listing_id . time();
    $customerProfile->email = $email;
    // Add payment profile.
    $paymentProfile = new AuthorizeNetPaymentProfile;
    $paymentProfile->customerType = "individual";
    $paymentProfile->payment->creditCard->cardNumber = $card_number;
    $paymentProfile->payment->creditCard->expirationDate = $exp_date;
    $paymentProfile->billTo->firstName = $first_name;
    $paymentProfile->billTo->lastName = $last_name;
    $customerProfile->paymentProfiles[] = $paymentProfile;
    $profile_response = $request->createCustomerProfile($customerProfile);
    $parsedresponse = simplexml_load_string($profile_response->response, "SimpleXMLElement", LIBXML_NOWARNING);
    $profile_id = $parsedresponse->customerProfileId;
    //save profile if created
    if ($profile_id != '') {
        $customer_profile = $request->getCustomerProfile($profile_id);
        $parsedprofile = simplexml_load_string($customer_profile->response, "SimpleXMLElement", LIBXML_NOWARNING);
        $profile_object = $parsedprofile->profile;
        $payment_profile_id = $profile_object->paymentProfiles->customerPaymentProfileId;
        $card_num = $profile_object->paymentProfiles->payment->creditCard->cardNumber;
        $expiry_date = $profile_object->paymentProfiles->payment->creditCard->expirationDate;
        $profile_email = $profile_object->email;
        $created_date = date('Y-m-d H:i:s');
        // delete existing profiles for this listing
        $sql_delete = sprintf("DELETE FROM payment_profiles WHERE listing_id > 0 AND listing_id = '%d'", $listing_id);
        mysql_query($sql_delete);
        // insert the newly created profile
        $sql_profile = sprintf("INSERT INTO payment_profiles (profile_id, payment_profile_id, email, card_number, expiry_date, first_name, last_name, created_time, listing_id) VALUES('%d', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%d')", $profile_id, $payment_profile_id, $profile_email, $card_num, $expiry_date, $first_name, $last_name, $created_date, $listing_id);
        mysql_query($sql_profile);
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: customer-listing-credit-card-details.php?bl_id=" . $listing_id);
    exit();
}
require_once '../include/my/header.php';
?>

<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">My Page</div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div>
    </div>

    <div class="left">
        <?PHP
        require '../include/nav-B-mypage.php';
        ?>
    </div>

    <div class="right">
        <form name="form1" method="post">
            <div class="content-header">Credit Card Details</div>
            <?php
            $sql_card = "SELECT card_number, DATE(created_time) AS created_date FROM payment_profiles WHERE listing_id = '$BL_ID'";
            $res_card = mysql_query($sql_card);
            $card = mysql_fetch_assoc($res_card);
            if ($card['card_number']) {
                $cardNo = explode("XXXX", $card['card_number']);
                $cardNo = "#### #### #### " . $cardNo[1];
                ?>
                <div class="form-inside-div exiting-card">
                    <label>Card on File</label>
                    <div class="form-data">
                        <?php echo $cardNo ?>
                    </div>
                </div>
                <div class="form-inside-div exiting-card">
                    <label>Created Date</label>
                    <div class="form-data">
                        <?php echo $card['created_date'] ?>
                    </div>
                </div>
                <?php
            }
            ?>
            <div class="form-inside-div">
                <label>Card Number</label>
                <div class="form-data">
                    <input type="text" placeholder="Enter Card Number" name="card_number" id="card_number" required />
                </div>
            </div>

            <div class="form-inside-div">
                <label>First Name</label>
                <div class="form-data">
                    <input type="text" placeholder="Enter First Name" name="first_name" id="first_name" required />
                </div>
            </div>

            <div class="form-inside-div">
                <label>Last Name</label>
                <div class="form-data">
                    <input type="text" placeholder="Enter Last Name" name="last_name" id="last_name" required />
                </div>
            </div>

            <div class="form-inside-div">
                <label>Expiration Date</label>
                <div class="form-data">
                    <select name="exp_month" id="exp_month" required>
                        <option value="">Month</option>
                        <option value="01">Jan</option>
                        <option value="02">Feb</option>
                        <option value="03">Mar</option>
                        <option value="04">Apr</option>
                        <option value="05">May</option>
                        <option value="06">Jun</option>
                        <option value="07">Jul</option>
                        <option value="08">Aug</option>
                        <option value="09">Sep</option>
                        <option value="10">Oct</option>
                        <option value="11">Nov</option>
                        <option value="12">Dec</option>
                    </select>
                    <select name="exp_year" id="exp_year" required>
                        <option value="">Year</option>
                        <option value="2015">2015</option>
                        <option value="2016">2016</option>
                        <option value="2017">2017</option>
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option value="2020">2020</option>
                    </select>
                </div>
            </div>

            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="hidden" name="email" value="<?php echo $rowListing['B_Email'] ?>" />
                    <input type="hidden" name="item_name" value="<?php echo substr($rowListing['BL_Listing_Title'], 0, 125); ?>" />
                    <input type="hidden" name="business_id" value="<?php echo $rowListing['B_ID'] ?>" />
                    <input type="hidden" name="listing_id" value="<?php echo $rowListing['BL_ID'] ?>" />
                    <input type="hidden" name="region" value="<?php echo $rowListing['BLCR_BLC_R_ID'] ?>" />
                    <input type="hidden" name="postback" value="1" />
                    <input type="submit" name="button2" value="Save Now"/>
                </div>
            </div>
        </form>
    </div>
</div>

<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>