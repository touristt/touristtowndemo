<?PHP
require_once '../include/config.inc.php';
require_once '../include/business.login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];
if (isset($_POST['entertainment-description-btn'])) {

    $daily_description = $_POST['entertainment-description'];
    $bl_id_description = $_POST['ent_des_bl_id'];
    $sql_daily_des = mysql_query("SELECT BFED_ID FROM tbl_Business_Feature_Entertainment_Description WHERE BFED_BL_ID = '$bl_id_description'");
    $count = mysql_num_rows($sql_daily_des);
    if ($count > 0) {
        $sql_daily_des_update = "UPDATE tbl_Business_Feature_Entertainment_Description SET BFED_Description = '$daily_description' WHERE BFED_BL_ID = '$bl_id_description'";
        $result = mysql_query($sql_daily_des_update);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Entertainment', '', 'Update Description', 'user admin');
    } else {
        $sql_daily_des_insert = "INSERT tbl_Business_Feature_Entertainment_Description SET BFED_BL_ID = '$bl_id_description',BFED_Description = '$daily_description'";
        $result = mysql_query($sql_daily_des_insert);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Entertainment', '', 'Add Description', 'user admin');
    }
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /listings-preview.php?bl_id=" . $BL_ID);   
    }
   else
   {
        header("Location:customer-feature-entertainment.php?bl_id=" . $bl_id_description);
   }
    
    exit;
}
$sql_description_daily = mysql_query("SELECT * FROM tbl_Business_Feature_Entertainment_Description WHERE BFED_BL_ID = '$BL_ID'");
$data_description = mysql_fetch_assoc($sql_description_daily);

if ($BL_ID > 0) {
    $sql = "SELECT BL_Street, BL_Town, BL_Province, BL_PostalCode, BL_Contact, BL_Phone, BL_Toll_Free, BL_Email, BL_Website FROM tbl_Business_Listing 
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            LEFT JOIN tbl_Business_Listing_Feature_Data ON BL_ID = BLFD_BL_ID
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}

if ($_POST['op'] == 'save') {
    $title = $_REQUEST['title'];
    $description = $_REQUEST['description'];
    $hour = $_REQUEST['hour'];
    $end_hour = $_REQUEST['end_hour'];
    $eventdate = (($_REQUEST['eventdate'] != "") ? $_REQUEST['eventdate'] : "0000-00-00") . " " . $hour;
    $bl_id = $_REQUEST['bl_id'];
    $sql_region_event = "SELECT R_Parent, RM_Child
                        FROM tbl_Business_Listing
                        INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                        INNER JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                        LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID
                        WHERE BL_ID = '$bl_id'
                        GROUP BY RM_Child";
    $result_region_event = mysql_query($sql_region_event);
    $event_regions = array();
    while ($r = mysql_fetch_assoc($result_region_event)) {
        // add region parent as well if available
        if ($r['R_Parent'] != 0 && !in_array($r['R_Parent'], $event_regions)) {
            $event_regions[] = $r['RM_Child'];
        }
    }
    $event_regions = implode(",", $event_regions);
    $location_detail = $row['BL_Street'] . ',' . $row['BL_Town'] . ',' . $row['BL_Province'] . ',' . $row['BL_PostalCode'];
    require_once '../include/picUpload.inc.php';
    if ($_POST['image_bank'] == "") {
        // last @param 20 = entertainment
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 20, 'Listing', $BL_ID);
        if (is_array($pic)) {
            $sql = "INSERT tbl_Business_Feature_Entertainment_Acts SET 
                    BFEA_BL_ID = '" . encode_strings($bl_id, $db) . "', 
                    BFEA_Name = '" . encode_strings($title, $db) . "',
                    BFEA_Photo = '" . encode_strings($pic['0']['0'], $db) . "',
                    BFEA_Description = '" . encode_strings($description, $db) . "',
                    BFEA_Time = '" . encode_strings($eventdate, $db) . "', 
                    BFEA_End_Time = '" . encode_strings($end_hour, $db) . "'";
            $pic_id = $pic['1'];
        } else {
            $sql = "INSERT tbl_Business_Feature_Entertainment_Acts SET 
                    BFEA_BL_ID = '" . encode_strings($bl_id, $db) . "', 
                    BFEA_Name = '" . encode_strings($title, $db) . "',
                    BFEA_Description = '" . encode_strings($description, $db) . "',
                    BFEA_Time = '" . encode_strings($eventdate, $db) . "',
                    BFEA_End_Time = '" . encode_strings($end_hour, $db) . "'";
        }
    } else {
        $pic_id = $_POST['image_bank'];
        $ext = explode(".", $pic);
        $myLink = mt_rand(1000, 9999999);
        // last @param 20 = Entertainment
        $pic = Upload_Pic_Library($pic_id, 20);
        if (is_array($pic)) {
            $sql = "INSERT tbl_Business_Feature_Entertainment_Acts SET 
                    BFEA_BL_ID = '" . encode_strings($bl_id, $db) . "', 
                    BFEA_Name = '" . encode_strings($title, $db) . "',
                    BFEA_Photo = '" . encode_strings($pic['0'], $db) . "',
                    BFEA_Description = '" . encode_strings($description, $db) . "',
                    BFEA_Time = '" . encode_strings($eventdate, $db) . "',
                    BFEA_End_Time = '" . encode_strings($end_hour, $db) . "'";
        }
    }
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $id = mysql_insert_id();
    $sql_main_events = "INSERT Events_master SET 
                        E_Region_ID = '" . encode_strings($event_regions, $db) . "',
                        E_BL_Event_ID = '" . encode_strings($id, $db) . "',
                        Title = '" . encode_strings($title, $db) . "', 
                        EventType = '" . encode_strings(1, $db) . "',
                        ShortDesc = '" . encode_strings(substr($description, 0, 95), $db) . "', 
                        Content = '" . encode_strings(substr($description, 0, 700), $db) . "', 
                        EventDateStart = '" . encode_strings(($_REQUEST['eventdate'] != "") ? $_REQUEST['eventdate'] : "0000-00-00", $db) . "',
                        EventStartTime = '" . encode_strings($hour, $db) . "', 
                        EventEndTime = '" . encode_strings($end_hour, $db) . "', 
                        LocationList = '" . encode_strings($location_detail, $db) . "', 
                        StreetAddress = '" . encode_strings($row['BL_Street'], $db) . "', 
                        ContactName = '" . encode_strings($row['BL_Contact'], $db) . "', 
                        ContactPhone = '" . encode_strings($row['BL_Phone'], $db) . "', 
                        ContactPhoneTollFree = '" . encode_strings($row['BL_Toll_Free'], $db) . "', 
                        Email = '" . encode_strings($row['BL_Email'], $db) . "', 
                        WebSiteLink = '" . encode_strings($row['BL_Website'], $db) . "'";
    $result_event = mysql_query($sql_main_events, $db) or die("Invalid query: $sql_main_events -- " . mysql_error());
    if ($result || $result_event) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        if ($pic_id > 0) {
            //Image usage from image bank
            imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Entertainment', $id);
        }
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Entertainment', '', 'Add Event', 'user admin');
    } else {
        $_SESSION['error'] = 1;
    }
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /listings-preview.php?bl_id=" . $BL_ID);   
    }
   else
   {
        header("Location: customer-feature-entertainment.php?bl_id=" . $bl_id);
   }
    
    exit();
}

if ($_POST['op'] == 'edit') {
    $acc = $_REQUEST['acc'];
    $title = $_REQUEST['title'];
    $description = $_REQUEST['description'];
    $hour = $_REQUEST['hour'];
    $end_hour = $_REQUEST['end_hour'];
    $eventdate = (($_REQUEST['eventdate'] != "") ? $_REQUEST['eventdate'] : "0000-00-00") . " " . $hour;
    $BFEA_ID = $_REQUEST['BFEA_ID'];
    // last @param 20 = Entertainment
    $bl_id = $_REQUEST['bl_id'];
    $sql_region_event = "SELECT R_Parent, RM_Child
                        FROM tbl_Business_Listing
                        INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                        INNER JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                        LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID
                        WHERE BL_ID ='$bl_id'
                        GROUP BY RM_Child";
    $result_region_event = mysql_query($sql_region_event);
    $event_regions = array();
    while ($r = mysql_fetch_assoc($result_region_event)) {
        // add region parent as well if available
        if ($r['R_Parent'] != 0 && !in_array($r['R_Parent'], $event_regions)) {
            $event_regions[] = $r['RM_Child'];
        }
    }
    $event_regions = implode(",", $event_regions);
    require_once '../include/picUpload.inc.php';
    $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 20, 'Listing', $BL_ID);
    if ($_POST['image_bank'] == "") {
        if (is_array($pic)) {
            $sql = "UPDATE tbl_Business_Feature_Entertainment_Acts SET  
                    BFEA_Name = '" . encode_strings($title, $db) . "', 
                    BFEA_Photo = '" . encode_strings($pic['0']['0'], $db) . "', 
                    BFEA_Description = '" . encode_strings($description, $db) . "', 
                    BFEA_Time = '" . encode_strings($eventdate, $db) . "',
                    BFEA_End_Time = '" . encode_strings($end_hour, $db) . "' 
                    WHERE BFEA_ID = '" . encode_strings($BFEA_ID, $db) . "'";
            $pic_id = $pic['1'];
        } else {
            $sql = "UPDATE tbl_Business_Feature_Entertainment_Acts SET  
                    BFEA_Name = '" . encode_strings($title, $db) . "', 
                    BFEA_Description = '" . encode_strings($description, $db) . "', 
                    BFEA_Time = '" . encode_strings($eventdate, $db) . "',
                    BFEA_End_Time = '" . encode_strings($end_hour, $db) . "' 
                    WHERE BFEA_ID = '" . encode_strings($BFEA_ID, $db) . "'";
        }
    } else {
        $pic_id = $_POST['image_bank'];
        $ext = explode(".", $pic);
        $myLink = mt_rand(1000, 9999999);
        // last @param 20 = Entertainment
        $pic = Upload_Pic_Library($pic_id, 20);
        if (is_array($pic)) {
            Update_Image_Bank_Listings($pic_id, $BL_ID);
            $sql = "UPDATE tbl_Business_Feature_Entertainment_Acts SET  
                    BFEA_Name = '" . encode_strings($title, $db) . "', 
                    BFEA_Photo = '" . encode_strings($pic['0'], $db) . "', 
                    BFEA_Description = '" . encode_strings($description, $db) . "', 
                    BFEA_Time = '" . encode_strings($eventdate, $db) . "',
                    BFEA_End_Time = '" . encode_strings($end_hour, $db) . "' 
                    WHERE BFEA_ID = '" . encode_strings($BFEA_ID, $db) . "'";
        }
    }
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $sql_main_events = "UPDATE Events_master SET 
                        E_Region_ID = '" . encode_strings($event_regions, $db) . "',
                        Title = '" . encode_strings($title, $db) . "', 
                        EventType = '" . encode_strings(1, $db) . "',
                        ShortDesc = '" . encode_strings(substr($description, 0, 95), $db) . "', 
                        Content = '" . encode_strings(substr($description, 0, 700), $db) . "', 
                        EventDateStart = '" . encode_strings(($_REQUEST['eventdate'] != "") ? $_REQUEST['eventdate'] : "0000-00-00", $db) . "',
                        EventStartTime = '" . encode_strings($hour, $db) . "', 
                        EventEndTime = '" . encode_strings($end_hour, $db) . "', 
                        LocationList = '" . encode_strings($location_detail, $db) . "', 
                        StreetAddress = '" . encode_strings($row['BL_Street'], $db) . "', 
                        ContactName = '" . encode_strings($row['BL_Contact'], $db) . "', 
                        ContactPhone = '" . encode_strings($row['BL_Phone'], $db) . "', 
                        ContactPhoneTollFree = '" . encode_strings($row['BL_Toll_Free'], $db) . "', 
                        Email = '" . encode_strings($row['BL_Email'], $db) . "', 
                        WebSiteLink = '" . encode_strings($row['BL_Website'], $db) . "'
                        WHERE E_BL_Event_ID = '" . encode_strings($BFEA_ID, $db) . "'";
    $result_event = mysql_query($sql_main_events, $db) or die("Invalid query: $sql_main_events -- " . mysql_error());
    if ($result || $result_event) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        if ($pic_id > 0) {
            $id = $BFEA_ID;
            //Image usage from image bank
            imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Entertainment', $id);
        }
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Entertainment', $acc, 'Update Event', 'user admin');
    } else {
        $_SESSION['error'] = 1;
    }
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /listings-preview.php?bl_id=" . $BL_ID);   
    }
   else
   {
        header("Location: customer-feature-entertainment.php?bl_id=" . $_POST['bl_id'] . "&acc=" . $acc);
   }
    
    exit();
}
if ($_GET['op'] == 'del_photo') {
    $BFEA_ID_Photo = $_REQUEST['bfea_id'];
    $sql = "UPDATE tbl_Business_Feature_Entertainment_Acts SET BFEA_Photo ='' WHERE BFEA_ID = '$BFEA_ID_Photo'";
    $result = mysql_query($sql);
    if ($result) {
        $_SESSION['delete'] = 1;
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Entertainment', $BFEA_ID_Photo);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Entertainment', '', 'Delete Image', 'user admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /listings-preview.php?bl_id=" . $BL_ID);   
    }
   else
   {
        header("Location: customer-feature-entertainment.php?bl_id=" . $_REQUEST['bl_id']);
   }
    
    exit();
}
if (isset($_GET['op']) && $_GET['op'] == 'del_event') {
    $bl_id = $_REQUEST['bl_id'];
    $BFEA_ID = $_REQUEST['bfea_id'];
    $result = mysql_query("DELETE FROM tbl_Business_Feature_Entertainment_Acts WHERE BFEA_ID = '$BFEA_ID'") or die(mysql_error());
    $result_main_events = mysql_query("DELETE FROM Events_master WHERE E_BL_Event_ID = '$BFEA_ID'") or die(mysql_error());
    if ($result || $result_main_events) {
        $_SESSION['delete'] = 1;
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Entertainment', $BFEA_ID);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Entertainment', '', 'Delete Event', 'user admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
//update points only for listing
    update_pointsin_business_tbl($bl_id);
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /listings-preview.php?bl_id=" . $BL_ID);   
    }
   else
   {
        header("Location: customer-feature-entertainment.php?bl_id=" . $_REQUEST['bl_id']);
   }
    
    exit();
}
$activeAccordion = '';
if (isset($_REQUEST['acc']) && $_REQUEST['acc'] != '') {
    $activeAccordion = $_REQUEST['acc'];
} else {
    $activeAccordion = 'false';
}
require_once '../include/my/header.php';
?>

<div class="content-left">

    <?php require_once '../include/nav-B-customer.php'; ?>

    <div class="title-link">
        <div class="title">Add ons</div>
        <div class="link">
            <?PHP require_once('preview-link.php'); ?>
        </div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-B-listing.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">
            <div class="title">Entertainment</div>
            <div class="link">
                <label class="add-link"><a onclick="show_form()">+ Add Event</a></label>
                <?php
                $Ent = show_addon_points(8);
                if ($numRowsEnt > 0) {
                    echo '<div class="points"><div class="points-com">' . $Ent . ' pts</div></div>';
                } else {
                    echo '<div class="points"><div class="points-uncom">' . $Ent . ' pts</div></div>';
                }
                ?>
            </div>
        </div>
        <?php
        $help_text = show_help_text('Entertainment');
        if ($help_text != '') {
            echo '<div class="form-inside-div">' . $help_text . '</div>';
        }
        ?>
        <form name="entertainment-des" method="post"  action="">
            <div class="daily-special-specifivation ent-des-width">
                <div class="form-inside-div add-menu-item Description-width">
                    <input type="hidden" name="ent_des_bl_id" id="daily_bl_id" value="<?php echo $BL_ID ?>">
                    <input type="hidden" name="go_to_preview" value="<?php echo $_REQUEST['go_to_preview'] ?>">
                    <label>Description</label>
                    <div class="form-data add-menu-item-field wiziwig-about">
                        <textarea name="entertainment-description" class="Description-ent-textarea description" id="daily-description"  cols="60" rows="3" ><?php echo $data_description['BFED_Description'] ?></textarea>
                    </div>
                </div>
                <div class="form-inside-div add-menu-item-button">
                    <div class="button">
                        <input type="submit" name="entertainment-description-btn" value="Save" />
                    </div>      
                </div> 
            </div>      
        </form>  
        <form name="menu-item" onSubmit="return check_img_size(<?php echo $BL_ID ?>, 10000000)" enctype="multipart/form-data" method="post" action="" id="enteritment-form" style="display: none; float:left;">
            <div class="form-inside-div add-product-section add-product-section-in-accordion">
                <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
                <input type="hidden" name="op" value="save">
                <input type="hidden" name="go_to_preview" value="<?php echo $_REQUEST['go_to_preview'] ?>">
                <input type="hidden" id="update_<?php echo $BL_ID ?>" value="1">
                <input type="hidden" name="image_bank" class="image_bank" id="image_bank<?php echo $BL_ID ?>" value="">
                <div class="add-menu-item-container margin-right-entr">
                    <div class="form-inside-div add-menu-item">
                        <label class="entertment-margin padding-entr">Name</label>
                        <div class="form-data add-menu-item-field daily-special-width">
                            <input name="title" type="text" id="title" class="title-enter-width" size="50" value="" required/>
                        </div>
                    </div>
                    <div class="form-inside-div add-menu-item">
                        <label class="entertment-margin">Description</label>
                        <div class="form-data add-menu-item-field wizwig_editor_product_items">
                            <textarea name="description" class="description" cols="40" rows="7" id="title-entertainment"></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-inside-div div_image_library add-menu-item-image product-cropit adjust-entert-width">
                    <span class="daily_browse" onclick="show_image_library(20, <?php echo $BL_ID; ?>, <?php echo $BL_ID ?>)">Select File</span>
                    <input type="file" name="pic[]" onchange="show_file_name(20, this, <?php echo $BL_ID ?>)" id="photo<?php echo $BL_ID ?>" style="display: none;">
                    <div class="form-inside-div add-about-us-item-image margin-none">
                        <div class="cropit-image-preview aboutus-photo-perview">
                            <img class="preview-img preview-img-script<?php echo $BL_ID ?>" style="display: none;" src="">    
                        </div>
                    </div>
                </div>
                <div class="form-inside-div add-entr-item">
                    <label class="entertment-margin padding-entr">Start time</label>
                    <div class="form-data add-menu-item-field margin-about-entertainment">
                        <select name="hour" id="hour" class="ententment-select" required>
                            <option value="">Select Time</option>
                            <?PHP
                            for ($i = 1; $i <= 24; $i++) {
                                $j = 0;
                                if ($i == 24) {
                                    $j = 1;
                                    $i = "00";
                                }
                                ?>
                                <option value="<?php echo $i ?>:00:00">
                                    <?php echo $i > 12 ? $i - 12 : $i ?>
                                    :00 
                                    <?php echo $i > 11 ? 'pm' : 'am' ?>
                                </option>
                                <option value="<?php echo $i ?>:15:00">
                                    <?php echo $i > 12 ? $i - 12 : $i ?>
                                    :15 
                                    <?php echo $i > 11 ? 'pm' : 'am' ?>
                                </option>
                                <option value="<?php echo $i ?>:30:00">
                                    <?php echo $i > 12 ? $i - 12 : $i ?>
                                    :30 
                                    <?php echo $i > 11 ? 'pm' : 'am' ?>
                                </option>
                                <option value="<?php echo $i ?>:45:00">
                                    <?php echo $i > 12 ? $i - 12 : $i ?>
                                    :45 
                                    <?php echo $i > 11 ? 'pm' : 'am' ?>
                                </option>
                                <?PHP
                                if ($j == 1) {
                                    $i = 24;
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-inside-div add-entr-item">
                    <label class="entertment-margin padding-entr">End time</label>
                    <div class="form-data add-menu-item-field margin-about-entertainment">
                        <select name="end_hour" id="end_hour" class="ententment-select" required>
                            <option value="">Select Hour</option>
                            <?PHP
                            for ($i = 1; $i <= 24; $i++) {
                                $j = 0;
                                if ($i == 24) {
                                    $j = 1;
                                    $i = "00";
                                }
                                ?>
                                <option value="<?php echo $i ?>:00:00">
                                    <?php echo $i > 12 ? $i - 12 : $i ?>
                                    :00 
                                    <?php echo $i > 11 ? 'pm' : 'am' ?>
                                </option>
                                <option value="<?php echo $i ?>:15:00">
                                    <?php echo $i > 12 ? $i - 12 : $i ?>
                                    :15 
                                    <?php echo $i > 11 ? 'pm' : 'am' ?>
                                </option>
                                <option value="<?php echo $i ?>:30:00">
                                    <?php echo $i > 12 ? $i - 12 : $i ?>
                                    :30 
                                    <?php echo $i > 11 ? 'pm' : 'am' ?>
                                </option>
                                <option value="<?php echo $i ?>:45:00">
                                    <?php echo $i > 12 ? $i - 12 : $i ?>
                                    :45 
                                    <?php echo $i > 11 ? 'pm' : 'am' ?>
                                </option>
                                <?PHP
                                if ($j == 1) {
                                    $i = 24;
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-data add-menu-item-field margin-about-entertainment">
                    <label class="entertment-margin padding-entr">Date</label>
                    <input class="previous-date-not-allowed date-textbox-width" type="text" name="eventdate" value=""/>
                </div>
                <div class="form-inside-div add-menu-item-button">
                    <div class="button entr-butn-new">
                        <input type="submit" name="button-add-event" class="button-add-event" value="Save Event" />
                    </div>   
                </div>   
            </div>   
        </form>
        <div class="menu-items-accodings product-accordion">                         
            <div id="accordion">
                <?PHP
                $sql = "SELECT * , DATE_FORMAT( BFEA_Time, '%Y-%m-%d' ) AS myDate, LOWER(DATE_FORMAT( BFEA_Time, '%T')) AS myHour, 
                        LOWER(DATE_FORMAT( BFEA_End_Time, '%T')) AS endHour FROM tbl_Business_Feature_Entertainment_Acts WHERE BFEA_BL_ID = '$BL_ID' 
                        ORDER BY BFEA_Time";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                $acco = 0;
                while ($data = mysql_fetch_assoc($result)) {
                    $originalDate = $data['myDate'];
                    ?>
                    <h3 class="accordion-rows"><?php echo $originalDate ?>---<?php echo $data['BFEA_Name'] ?></h3> 
                    <div class="sub-accordions accordion-padding">
                        <form name="menu-item" onSubmit="return check_img_size(<?php echo $data['BFEA_ID'] ?>, 10000000)" enctype="multipart/form-data" method="post" action="" id="enteritment-form" style="float:left;">
                            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
                            <input type="hidden" name="op" value="edit">
                            <input type="hidden" name="go_to_preview" value="<?php echo $_REQUEST['go_to_preview'] ?>">
                            <input type="hidden" name="acc" value="<?php echo $acco ?>">
                            <input type="hidden" name="BFEA_ID" value="<?php echo $data['BFEA_ID'] ?>">
                            <input type="hidden" id="update_<?php echo $data['BFEA_ID'] ?>" value="1">
                            <input type="hidden" name="image_bank" class="image_bank" id="image_bank<?php echo $data['BFEA_ID'] ?>" value="">
                            <div class="add-menu-item-container">
                                <div class="form-inside-div add-menu-item">
                                    <label class="entertment-margin ">Name</label>
                                    <div class="form-data add-menu-item-field daily-special-width">
                                        <input name="title" type="text" id="title_<?php echo $data['BFEA_ID']; ?>" size="50" value="<?php echo $data['BFEA_Name'] ?>" required/>
                                    </div>
                                </div>
                                <div class="form-inside-div add-menu-item">
                                    <label class="entertment-margin">Description</label>
                                    <div class="form-data add-menu-item-field wizwig_editor_product_items">
                                        <textarea name="description" class="daily-description" cols="40" rows="7" id="title-entertainment<?php echo $data['BFEA_ID'] ?>"><?php echo $data['BFEA_Description'] ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-inside-div div_image_library add-menu-item-image product-cropit">
                                <span class="daily_browse" onclick="show_image_library(20, <?php echo $BL_ID; ?>, <?php echo $data['BFEA_ID'] ?>)">Select File</span>
                                <input type="file" name="pic[]" onchange="show_file_name(20, this, <?php echo $data['BFEA_ID'] ?>)" id="photo<?php echo $data['BFEA_ID'] ?>" style="display: none;">
                                <div class="cropit-image-preview aboutus-photo-perview">
                                    <img class="preview-img preview-img-script<?php echo $data['BFEA_ID'] ?> max-width-thumbnail" style="display: none;" src="">    
                                    <?php if ($data['BFEA_Photo'] != "") { ?>
                                        <img class="existing-img existing_imgs<?php echo $data['BFEA_ID'] ?> max-width-thumbnail" src="http://<?php echo DOMAIN . IMG_LOC_REL . $data['BFEA_Photo'] ?>">
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-inside-div add-menu-item-image">
                                <div class="data-column daily-del"> 
                                    <a class="deletePhoto" href="customer-feature-entertainment.php?bl_id=<?php echo $BL_ID ?>&bfea_id=<?php echo $data['BFEA_ID'] ?>&op=del_photo&acc=<?php echo $acco; ?>&go_to_preview=<?php echo $_REQUEST['go_to_preview'] ?>" onClick="return confirm('Are you sure you want to delete photo?');">Delete Photo</a>
                                </div>
                            </div>
                            <div class="form-inside-div add-entr-item">
                                <label class="entertment-margin">Start time</label>
                                <div class="form-data add-menu-item-field">
                                    <select name="hour" id="hour<?php echo $data['BFEA_ID'] ?>" class="ententment-select" required>
                                        <option value="">Select Hour</option>
                                        <?PHP
                                        for ($i = 1; $i <= 24; $i++) {
                                            if ($i < 10) {
                                                $i = "0" . $i;
                                            }
                                            $j = 0;
                                            if ($i == 24) {
                                                $j = 1;
                                                $i = "00";
                                            }
                                            ?>
                                            <option value="<?php echo $i ?>:00:00" <?php
                                            if ($data['myHour'] == $i . ":00:00") {
                                                echo "selected";
                                            }
                                            ?> >
                                                        <?php echo $i > 12 ? $i - 12 : $i ?>
                                                :00 
                                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                                            </option>
                                            <option value="<?php echo $i ?>:15:00" <?php
                                            if ($data['myHour'] == $i . ":15:00") {
                                                echo "selected";
                                            }
                                            ?> >
                                                        <?php echo $i > 12 ? $i - 12 : $i ?>
                                                :15 
                                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                                            </option>
                                            <option value="<?php echo $i ?>:30:00" <?php
                                            if ($data['myHour'] == $i . ":30:00") {
                                                echo "selected";
                                            }
                                            ?> >
                                                        <?php echo $i > 12 ? $i - 12 : $i ?>
                                                :30 
                                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                                            </option>
                                            <option value="<?php echo $i ?>:45:00" <?php
                                            if ($data['myHour'] == $i . ":45:00") {
                                                echo "selected";
                                            }
                                            ?>>
                                                        <?php echo $i > 12 ? $i - 12 : $i ?>
                                                :45 
                                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                                            </option>
                                            <?PHP
                                            if ($j == 1) {
                                                $i = 24;
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-inside-div add-entr-item">
                                <label class="entertment-margin">End time</label>
                                <div class="form-data add-menu-item-field">
                                    <select name="end_hour" id="end_hour<?php echo $data['BFEA_ID'] ?>" class="ententment-select" required>
                                        <option value="">Select Hour</option>
                                        <?PHP
                                        for ($i = 1; $i <= 24; $i++) {
                                            if ($i < 10) {
                                                $i = "0" . $i;
                                            }
                                            $j = 0;
                                            if ($i == 24) {
                                                $j = 1;
                                                $i = "00";
                                            }
                                            ?>
                                            <option value="<?php echo $i ?>:00:00" <?php
                                            if ($data['endHour'] == $i . ":00:00") {
                                                echo "selected";
                                            }
                                            ?> >
                                                        <?php echo $i > 12 ? $i - 12 : $i ?>
                                                :00 
                                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                                            </option>
                                            <option value="<?php echo $i ?>:15:00" <?php
                                            if ($data['endHour'] == $i . ":15:00") {
                                                echo "selected";
                                            }
                                            ?>>
                                                        <?php echo $i > 12 ? $i - 12 : $i ?>
                                                :15 
                                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                                            </option>
                                            <option value="<?php echo $i ?>:30:00" <?php
                                            if ($data['endHour'] == $i . ":30:00") {
                                                echo "selected";
                                            }
                                            ?>>
                                                        <?php echo $i > 12 ? $i - 12 : $i ?>
                                                :30 
                                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                                            </option>
                                            <option value="<?php echo $i ?>:45:00" <?php
                                            if ($data['endHour'] == $i . ":45:00") {
                                                echo "selected";
                                            }
                                            ?>>
                                                        <?php echo $i > 12 ? $i - 12 : $i ?>
                                                :45 
                                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                                            </option>
                                            <?PHP
                                            if ($j == 1) {
                                                $i = 24;
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-inside-div add-menu-item">
                                <label class="entertment-margin">Date</label>
                                <div class="form-data add-menu-item-field">
                                    <input class="previous-date-not-allowed date-textbox-width" type="text" name="eventdate" id="eventdate<?php echo $data['BFEA_ID'] ?>" value="<?php echo $data['myDate'] ?>">
                                </div>
                            </div>
                            <div class="form-inside-div border-none button-spaces">
                                <div class="button">
                                    <input type="submit" name="button" class="button<?php echo $data['BFEA_ID']; ?>" value="Save Event" />
                                    <a class="btn-anchor" href="customer-feature-entertainment.php?bl_id=<?php echo $BL_ID ?>&bfea_id=<?php echo $data['BFEA_ID'] ?>&op=del_event&acc=<?php echo $acco; ?>&go_to_preview=<?php echo $_REQUEST['go_to_preview'] ?>" onClick="return confirm('Are you sure you want to delete event?');">Delete Event</a>
                                </div>
                            </div>
                        </form>
                        <script>
                            CKEDITOR.disableAutoInline = true;
                            $(document).ready(function () {
                                var wzwig_BFEA_ID = <?php echo $data['BFEA_ID'] ?>;
                                $('#title-entertainment' + wzwig_BFEA_ID).ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
                            });
                        </script>

                    </div> 

                    <?php
                    $acco++;
                }
                ?>
            </div>
            <div id="image-library" style="display:none;"></div>
        </div>

        <?php
        $sql1 = "SELECT * FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = 8";
        $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
        $row1 = mysql_fetch_assoc($result1);
        if ($row1['BLF_BL_ID'] != '') {
            ?>
            <div class="form-inside-div border-none text-align-center">
                <a href="customer-feature-add-ons.php?id=<?php echo $row1['BLF_ID'] ?>&bl_id=<?php echo $BL_ID ?>" onclick="return confirm('Are you sure you want to <?php echo $row1['BLF_Active'] == 1 ? "deactivate" : "activate" ?> this Add on?');"><?php echo $row1['BLF_Active'] == 1 ? "Deactivate" : "Activate" ?> This Add on</a>
            </div>
            <?PHP
        }
        ?>
    </div>
</div>
<?PHP
require_once('rank-advertise.php');
require_once '../include/my/footer.php';
?>
<script>
    CKEDITOR.disableAutoInline = true;
    $(document).ready(function () {
        $('#daily-description').ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
    });

    $(function () {
        $("#accordion").accordion({
            collapsible: true,
            heightStyle: 'content',
            active: <?php echo $activeAccordion; ?>
        });
    });
    function show_form() {
        $('#enteritment-form').show();
    }
    $(document).ready(function () {
        $('#title-entertainment').ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
    });
</script> 