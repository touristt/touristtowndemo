<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Tourist Town - Small Town Tourism Websites</title>
<link href="tt.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top" bgcolor="#FFFFFF"><?php require_once('TopNav.php'); ?></td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="#BABABA"><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/spacer.gif" width="30" height="30" /></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><table width="960" border="0" cellspacing="30" cellpadding="0">
          <tr>
            <td width="870" colspan="2" align="left" valign="top"><p>
              <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
              <HTML>
              <!-- 		@page { margin: 2cm } 		P { margin-bottom: 0.21cm } 		H1 { margin-bottom: 0.21cm } 		H1.western { font-family: "Times New Roman", serif } 		H1.cjk { font-family: "SimSun" } 		H1.ctl { font-family: "Mangal" } 		H3 { margin-bottom: 0.21cm } 		H3.cjk { font-family: "SimSun" } 		H3.ctl { font-family: "Mangal" } 		A:link { so-language: zxx } 	-->
              <BODY DIR="LTR">
            </p>
              <H1>Tourist Town Terms of Use</H1>
              <H3>Overview:</H3>
              <p>Welcome to Tourist Town! We are a subscription service that is dedicated to creating amazing destination marketing websites for small rural communities. The websites are  easy to use, extremely functional and attractive. They offer a variety of listing options and tools to make your business really stand out. </p>
              <p>These Terms of Use govern the use of our service. As used in these Terms of Use, &quot;Tourist Town,&quot; &quot;our service&quot; or &quot;the service&quot; means the service provided by Tourist Town for the building and maintenance of a customized tourism site. <BR>
                <BR>
              </p>
              <OL>
                <LI>
                  <p><strong>Acceptance 	of Terms of Use.</strong> These 	Terms of Use, which include our Privacy Policy 	(www.touristtown.ca/PrivacyPolicy), End User License Agreement 	(www.touristtown.ca/EULA) (&quot;EULA&quot;), About Cookies and 	Internet Advertising (www.touristtown.ca/cookies), and, to the 	extent you use the social features, our Social Terms 	(www.touristtown.ca/SocialTerms), govern your use of the Tourist 	Town services. By using the Tourist Town service, you accept and 	agree to these Terms of Use. If you do not agree to these Terms of 	Use, do not use the Tourist Town service. </p>
                  <OL TYPE="a">
                  </OL>
                <LI><strong>Changes 	to Terms of Use.</strong> Tourist 	town may, from time to time, change these Terms of Use, including 	the Privacy Policy, EULA and Social Terms. Such revisions shall be 	effective immediately; provided however, for existing members, such 	revisions shall, unless otherwise stated, be effective 30 days after 	posting. We will endeavor to post prior versions of the Terms of 	Use, if any, for the preceding 12-month period. You can see these 	prior versions by visiting our website 	(www.touristtown.ca/priorterms).                <br>
                  <br>
                <LI><strong>Privacy.</strong> Personally identifying information is subject to our Privacy Policy 	(<a href="http://visitsaugeenshores.ca/privacy.php">www.touristtown.ca/PrivacyPolicy</a>), the terms of which are 	incorporated herein. Please review our Privacy Policy to understand 	our practices. <br>
                  <br>
                <LI> <strong>Communication 	Preferences.</strong> By using 	the Tourist Town service, you consent to receiving electronic 	communications from Tourist Town relating to your listing. These 	communications may involve sending emails to your email address 	provided during registration, and will include notices about your 	account (e.g., payment authorizations, change in password or Payment 	Method, confirmation e-mails and other transactional information) 	and are part of your relationship with Tourist Town. You agree that 	any notices, agreements, disclosures or other communications that we 	send to you electronically will satisfy any legal communication 	requirements, including that such communications be in writing. You 	should maintain copies of electronic communications by printing a 	paper copy or saving an electronic copy. You also consent to 	receiving certain other communications from us, such as newsletters 	about new Tourist Town features and content, special offers, 	promotional announcements and customer surveys via email or other 	methods. 
                  <br>
                  <br>
                <LI><strong>Membership, 	Free Listings, Billing and Cancellation</strong>
                  <OL TYPE="a">
                    <LI><strong>Membership</strong>
                      <OL TYPE="i">
                        <LI><strong>Ongoing 			Membership.</strong> Your Tourist Town membership, which may start with 			a free listing, will continue month-to-month unless and until you 			cancel your membership or we terminate it. You must provide us 			with a current, valid, VISA or MasterCard to use the Tourist Town 			service. We will bill the monthly membership fee to the credit 			card provided. You must cancel your membership before it renews 			each month in order to avoid billing of the next month's 			membership fees to your credit card.
                        <LI><strong>Differing 			Memberships.</strong> We offer a number of membership plans, including 			basic listings and listings with different Add-Ons included.  Any 			materially different terms from those described in these Terms of 			Use will be disclosed at your sign-up or in other communications 			made available to you. You can find specific details regarding 			your membership with Tourist Town by visiting our website and and 			logging into your business listing.  We reserve the right to 			modify, terminate or otherwise amend our offered membership plans. 
                      </OL>
                    <LI><strong>Free 		Listings</strong>
                      <OL TYPE="i">
                        <LI>Your 			Tourist Town membership may start with a free listing. The free 			listing period of your membership lasts for as long as you wish, 			or as otherwise specified during sign-up. 
                        <LI>You 			will receive a notice from us that your free trial period has 			ended or that the paying portion of your membership has begun if 			you choose to upgrade from a free listing.  We will bill your 			credit card on a monthly basis for your membership fee until you 			cancel. 
                      </OL>
                    <LI><strong>Billing</strong>
                      <OL TYPE="i">
                        <LI><strong>Recurring 			Billing.</strong> By starting your Tourist Town membership and 			providing a credit card for payment, you authorize us to charge 			you a monthly membership fee at the then current rate, and any 			other charges you may incur in connection with your use of the 			Tourist Town services. You acknowledge that the amount billed each 			month may vary from month to month for reasons that may include 			differing amounts due to promotional offers, and/or changing or 			adding features, and you authorize us to charge your credit card 			for such varying amounts, which may be billed monthly in one or 			more charges. 
                        <LI><strong>Price 			Changes.</strong> We reserve the right to adjust pricing for our 			service or any components thereof in any manner and at any time as 			we may determine in our sole and absolute discretion. Except as 			otherwise expressly provided for in these Terms of Use, any price 			changes to your service will take effect following email notice to 			you. 
                        <LI><strong>Billing 			Cycle.</strong> The membership fee for our service will be billed at 			the beginning of the paying portion of your membership and each 			month thereafter unless and until you cancel your membership. We 			automatically bill your credit card each month on the calendar day 			corresponding to the commencement of your paying membership. 			Membership fees are fully earned upon payment. We reserve the 			right to change the timing of our billing, in particular, as 			indicated below, if your credit card has not successfully settled. 			In the event your paying membership began on a day not contained 			in a given month, we may bill your credit card on a day in the 			applicable month or such other day as we deem appropriate. For 			example, if you started your Tourist Town membership or became a 			paying member on January 31st, your next payment date is likely to 			be February 28th, and your credit card would be billed on that 			date. Your renewal date may change due to changes in your 			Membership.  We may authorize your credit card in anticipation of 			membership or service-related charges. As used in these Terms of 			Use, &quot;billing&quot; shall indicate a charge, debit or other 			payment clearance, as applicable, against your credit card. Unless 			otherwise stated differently, month or monthly refers to your 			billing cycle. 
                        <LI><strong>No 			Refunds.</strong> PAYMENTS ARE NONREFUNDABLE AND THERE ARE NO REFUNDS 			OR CREDITS FOR PARTIALLY USED PERIODS. Following any cancellation, 			however, you will continue to have access to the service through 			the end of your current billing period. At any time, and for any 			reason, we may provide a refund, discount, or other consideration 			to some or all of our members (&quot;credits&quot;). The amount 			and form of such credits, and the decision to provide them, are at 			our sole and absolute discretion. The provision of credits in one 			instance does not entitle you to credits in the future for similar 			instances, nor does it obligate us to provide credits in the 			future, under any circumstance. 
                        <LI><strong>Payment 			Methods.</strong> You may edit your credit card information at any time 			by contacting Tourist Town (<A HREF="http://www.touristtown.ca/contactus">www.touristtown.ca/contactus</A>). 			If a payment is not successfully settled, due to expiration, 			insufficient funds, or otherwise, and you do not edit your credit 			card information or cancel your account (see, &quot;Cancellation&quot; 			below), you remain responsible for any uncollected amounts and 			authorize us to continue billing the credit card, as it may be 			updated. This may result in a change to your payment billing 			dates.
                        <LI><strong>Cancellation.</strong> You may cancel your Tourist Town membership at any time, and you 			will continue to have access to the Tourist Town service through 			the end of your monthly billing period. WE DO NOT PROVIDE REFUNDS 			OR CREDITS FOR ANY PARTIAL-MONTH MEMBERSHIP PERIODS. To cancel, 			contact Tourist Town (<A HREF="http://www.touristtown.ca/contactus">www.touristtown.ca/contactus</A> ). If you cancel your membership, your account will automatically 			close at the end of your current billing period. 
                      </OL>
                  </OL>
              </OL></td>
          </tr>
          <tr>
            <td colspan="2" align="left" valign="top" bgcolor="#8DC63F"><img src="images/spacer.gif" alt="" width="10" height="4" /></td>
          </tr>
          <tr>
            <td colspan="2" align="left" valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr>
                <td width="84%" align="center" bgcolor="#FFFFFF">Corporate Address: P.O. Box 2247 Port Elgin | <a href="http://visitsaugeenshores.ca/privacy.php">Privacy Policy</a> | <a href="terms.php">Terms of Use</a></td>
                </tr>
              </table>
              <p>&nbsp;</p></td>
          </tr>
          </table></td>
      </tr>
      <tr>
        <td><img src="images/spacer.gif" alt="" width="30" height="30" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
