<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Tourist Town - Small Town Tourism Websites</title>
        <link href="tt.css" rel="stylesheet" type="text/css" />
    </head>

    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center" valign="top" bgcolor="#FFFFFF"><?php require_once('TopNav.php'); ?></td>
            </tr>
            <tr>
                <td align="center" valign="top" bgcolor="#BABABA"><table width="960" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><img src="images/spacer.gif" width="30" height="30" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#FFFFFF"><table width="960" border="0" cellspacing="30" cellpadding="0">
                                    <tr>
                                        <td align="left" valign="top"><span class="PageTitle">Welcome to the future of Tourism Websites.</span>
                                            </p>
                                            <p>Tourist Town is a revolutionary approach to providing an online presence to small (very small) towns that depend on tourism. It is made up of 2 parts. Firstly, it is a software system that generates websites for communities. Secondly, it is a collection of tourism experts that work with communities to develop a attractive message to potential visitors.</p>
                                            <p>If you are a business in one of the existing communities using the Tourist Town system, please see the contact details of your area representative below.</p></td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" bgcolor="#8DC63F"><img src="images/spacer.gif" alt="" width="10" height="4" /></td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="middle" class="SectionHeading">
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="100%">
                                                        <div style="margin-top: 30px; float: left;width: 100%;text-align: center;">
                                                            <div style="width: auto;display: inline-block;text-align: center;">
                                                                <div id="marketing-logos">
                                                                    <div>
                                                                        <a href="http://www.visitsaugeenshores.ca" target="www.visitsaugeenshores.ca"><img src="images/Footer-SaugeenShores.jpg" alt="SaugeenShores" border="0" /></a>
                                                                        <a href="http://www.visitportelgin.ca" target="www.visitportelgin.ca"><img src="images/Footer-PortElgin.png" alt="Port Elgin" border="0" /></a>
                                                                        <a href="http://www.visitsouthampton.ca" target="www.visitsouthampton.ca"><img src="images/Footer-Southampton.png" alt="Southampton" border="0" /></a>
                                                                        <a href="http://www.visitwiarton.ca" target="www.visitwiarton.ca"><img src="images/Wiarton.png" alt="Wiarton" border="0" /></a>
                                                                        <a href="http://www.visitlionshead.ca" target="www.visitlionshead.ca"><img src="images/Footer-LionsHead.png" alt="Lion's Head" border="0" /></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="100%">
                                                        <div style="margin-top: 30px; float: left;width: 100%;text-align: center;">
                                                            <div style="width: auto;display: inline-block;text-align: center;">
                                                                <div id="marketing-logos">
                                                                    <div>
                                                                        <a href="http://www.visitsaublebeach.ca" target="www.visitsaublebeach.ca"><img style="margin: 25px 0 25px 25px;" src="images/Footer-SaubleBeach.png" alt="Sauble Beach" border="0" /></a>
                                                                        <a href="http://www.downtownportelgin.ca" target="www.downtownportelgin.ca"><img style="margin: 25px 0 25px 25px;" src="images/Footer-DowntownPortElgin.jpg" alt="Downtown Port Elgin" border="0" /></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" bgcolor="#8DC63F"><img src="images/spacer.gif" alt="" width="10" height="1" /></td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="center" valign="middle" class="SectionHeading">
                                            <p>
                                                <span class="PageTitle">519.378.3096<br />
                                                    Danielle Mulasmajic<br />
                                                    Partner, Tourist Town Solutions<br />
                                                    <a href="mailto:danielle@touristtown.ca">danielle@touristtown.ca</a><br />
                                                </span>
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" bgcolor="#8DC63F"><img src="images/spacer.gif" alt="" width="10" height="4" /></td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="0" cellpadding="5">
                                                <tr>
                                                    <td width="84%" align="center" bgcolor="#FFFFFF">Corporate Address: P.O. Box 2247 Port Elgin | <a href="http://visitsaugeenshores.ca/privacy.php">Privacy Policy</a> | <a href="terms.php">Terms of Use</a></td>
                                                </tr>
                                            </table>
                                            <p>&nbsp;</p></td>
                                    </tr>
                                </table></td>
                        </tr>
                        <tr>
                            <td><img src="images/spacer.gif" alt="" width="30" height="30" /></td>
                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td align="center" valign="top">&nbsp;</td>
            </tr>
            <tr>
                <td align="center" valign="top">&nbsp;</td>
            </tr>
        </table>
    </body>
</html>
