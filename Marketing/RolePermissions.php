<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Untitled Document</title>
<link href="tt.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="1000" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center" bgcolor="#EFEFEF"><table width="1144" border="0" cellspacing="1" cellpadding="10">
          <tr>
            <td colspan="6" bgcolor="#FFFFFF"><span class="PageTitle">Administration Permissions</span></td>
          </tr>
          <tr>
            <td width="517" bgcolor="#D6D6D6" class="bold"> LISTINGS</td>
            <td align="center" bgcolor="#D6D6D6" class="bold">Super Admin</td>
            <td align="center" bgcolor="#D6D6D6" class="bold">Regional Admin</td>
            <td align="center" bgcolor="#D6D6D6" class="bold">BG Admin</td>
            <td align="center" bgcolor="#D6D6D6" class="bold">Town</td>
            <td align="center" bgcolor="#D6D6D6" class="bold">Business</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">Manage Customers (Add, Delete, View, Stats)</td>
            <td width="100" align="center" bgcolor="#FFFFFF" class="PageTitle">•</td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">Free Listings</td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">Chamber of Commerce Listings</td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">Business Profile (Business Details, Hours, Social Media)</td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">Business Listings (Basic)</td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">Business Listing (Enhanced &amp; Listing Tools)</td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">Billing History</td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">Town Assets Listings</td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF">All Regions.</td>
            <td colspan="3" align="center" bgcolor="#FFFFFF">Only applies to applicable Region.</td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#D6D6D6" class="bold">REGIONS</td>
            <td width="100" align="center" bgcolor="#D6D6D6" class="bold">Super Admin</td>
            <td width="100" align="center" bgcolor="#D6D6D6" class="bold">Regional Admin</td>
            <td width="100" align="center" bgcolor="#D6D6D6" class="bold">BG Admin</td>
            <td width="100" align="center" bgcolor="#D6D6D6" class="bold">Town</td>
            <td width="100" align="center" bgcolor="#D6D6D6" class="bold">Business</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">Manage Categories</td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">Manage Sub-Categories</td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">Manage Regions</td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">Manage Amenity Icons</td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">Manage Events</td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">Manage Contests</td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">Manage Users</td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">&nbsp;</td>
            <td align="center" bgcolor="#FFFFFF">All Regions.</td>
            <td colspan="3" align="center" bgcolor="#FFFFFF">Only applies to applicable Region.</td>
            <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#D6D6D6" class="bold">OTHER</td>
            <td align="center" bgcolor="#D6D6D6" class="bold">Super Admin</td>
            <td align="center" bgcolor="#D6D6D6" class="bold">Regional Admin</td>
            <td align="center" bgcolor="#D6D6D6" class="bold">BG Admin</td>
            <td align="center" bgcolor="#D6D6D6" class="bold">Town</td>
            <td align="center" bgcolor="#D6D6D6" class="bold">Business</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">Manage Cart Items</td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">Manage Listing Points</td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">Manage Accounting</td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF"><span class="PageTitle">•</span></td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF">&nbsp;</td>
            <td width="100" align="center" bgcolor="#FFFFFF">All Regions.</td>
            <td colspan="3" align="center" bgcolor="#FFFFFF">Only applies to applicable Region.</td>
            <td width="100" align="center" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
