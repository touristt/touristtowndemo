<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Tourist Town - Tutorials</title>
<link href="tt.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top" bgcolor="#FFFFFF"><?php require_once('TopNav.php'); ?></td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="#BABABA"><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/spacer.gif" width="30" height="30" /></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><table width="960" border="0" cellspacing="30" cellpadding="0">
          <tr>
            <td colspan="2" align="left" valign="top" class="SectionHeading">Tutorials</td>
          </tr>
          <tr>
            <td colspan="2" align="left" valign="top" bgcolor="#8DC63F"><img src="images/spacer.gif" alt="" width="10" height="5" /></td>
          </tr>
          <tr>
            <td width="258" rowspan="5" align="left" valign="top" bgcolor="#FFFFFF"><?php require_once('Tutorial-Nav.php'); ?></td>
            <td align="left" valign="top"><p class="PageTitle">Resizing Images</p>
              <p>Before you can upload an image to your listing, you will need to have an image ready to upload. You can use a software program, such as Photoshop to resize the image to 170 pixels x 90 pixels, or you can use an online tool provided by Tourist Town.</p>
              <p class="bold">To use Tourist Town resize Tool:</p>
              <ol>
                <li>On the home page of your Tourist Town login (<a href="http://my.touristtown.ca">my.touristtown.ca</a>), at the bottom of page click on: <a href="http://resize.touristtown.ca" target="_blank">resize.touristtown.ca</a></li>
                <li>This will open a browser window: <a href="http://resize.touristtown.ca" target="_blank">http://resize.touristtown.ca</a></li>
                <li>Click &quot;Browse...&quot; button</li>
                <li>Select the photo you want to use</li>
                <li>Click &quot;Upload&quot;</li>
                <li>You will see your photo load into the &quot;Step 2&quot; section</li>
                <li>Click on the dropdown menu &quot;Select Cropping Size&quot;.</li>
                <li>Select &quot;Thumbnail - 170 x 90&quot;</li>
                <li>You will notice a rectangle will appear with 8 nodes and a dotted line. This is the shape that your image will be saved as. Click on one of the 8 nodes and you can make the cropping area bigger or smaller. If you want to resize at 100% of the original size, click &quot;Set Size&quot;</li>
                <li>When you are happy - click &quot;Save image Now&quot;.</li>
                <li>The image will save to your computer using the same file name and dimensions.(e.g. &quot;myphoto-170x90.jpg) </li>
              </ol></td>
          </tr>
          <tr>
            <td align="left" valign="top" bgcolor="#CCCCCC"><img src="images/spacer.gif" alt="" width="10" height="1" /></td>
          </tr>
          <tr>
            <td align="left" valign="top"><p class="PageTitle">Watch the Video Tutorial</p>
              <p>
                <iframe width="630" height="354" src="http://www.youtube.com/embed/ECtSpexSClQ?rel=0" frameborder="0" allowfullscreen></iframe>
              </p></td>
          </tr>
          <tr>
            <td align="left" valign="top" bgcolor="#CCCCCC"><img src="images/spacer.gif" alt="" width="10" height="1" /></td>
          </tr>
          <tr>
            <td width="642" align="left" valign="top"><p class="PageTitle">Still not sure? Call us on 519.832.3322.</p>
              <p>We'd love to talk to  you and walk you through the process over the phone. We're usually in the office 9am - 5pm weekdays. If we're not in, leave a message and we'll call you back. Or feel free to <a href="mailto:info@touristtown.ca">email</a> us outlining the issue.</p>
              <p><br />
          </p></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><img src="images/spacer.gif" alt="" width="30" height="30" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
