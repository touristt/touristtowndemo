<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Tourist Town - Tutorials</title>
<link href="tt.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top" bgcolor="#FFFFFF"><?php require_once('TopNav.php'); ?></td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="#BABABA"><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/spacer.gif" width="30" height="30" /></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><table width="960" border="0" cellspacing="30" cellpadding="0">
          <tr>
            <td colspan="2" align="left" valign="top" class="SectionHeading">Tutorials</td>
          </tr>
          <tr>
            <td colspan="2" align="left" valign="top" bgcolor="#8DC63F"><img src="images/spacer.gif" alt="" width="10" height="5" /></td>
          </tr>
          <tr>
            <td width="258" rowspan="5" align="left" valign="top" bgcolor="#FFFFFF"><?php require_once('Tutorial-Nav.php'); ?></td>
            <td align="left" valign="top"><p class="PageTitle">Map Settings</p>
              <p>The Tourist Town mapping system will allow visitors to find your listing using a map and will make planning their vacation and finding your business much easier. 
                This mapping system uses Google as it's mapping engine. To ensure your listing shows up on the maps, you will need to follow the below steps. Please note your listing WILL NOT show up on the mapping system if you do not follow the below steps.</p>
              <ol>
                <li>Login into your Tourist Town account. (<a href="http://my.touristtown.ca">my.touristtown.ca</a>)<br />
                  </li>
                <li>Click on &quot;Business Profile&quot;<br />
                  </li>
                <li>Towards the bottom of the form, you will see a title &quot;GPS&quot;. Next to GPS you will see Latitude and Longitude. This is where you will enter the longitude and latitude of your location from Google maps.<br />
                  </li>
                <li>Open another browser window and go to www.maps.google.com<br />
                  </li>
                <li>Next to the Google logo, enter your address. (eg. 664 Goderich Street, Port Elgin, Ontario)<br />
                  </li>
                <li>Click &quot;Search&quot; - on the magify glass icon<br />
                </li>
                <li>Google will load a map and show an orange symbol with the letter &quot;A&quot;.<br />
                  </li>
                <li>Confirm the location is correct. If it is proceed to next step.<br />
                  </li>
                <li>Right click on the &quot;A&quot;<br />
                  </li>
                <li>A white box will appear.<br />
                  </li>
                <li>Click on &quot;What's here?&quot;<br />
                  </li>
                <li>At the top of the page (next to Google logo) you will see 2 numbers seperated by a comma. The first number will be a positive (Latitude) and the second number a negative (Longitude).<br />
                  </li>
                <li>Select and copy the first number (eg. 44.437457)<br />
                  </li>
                <li>Go to your Tourist Town account and paste the number into the Latitude field on your Business Profile<br />
                  </li>
                <li>Go back to the google browser and copy the second number (eg. -81.388233). This number will be a negative number. Please ensure that you include the &quot;-&quot; (minus) symbol.<br />
                  </li>
                <li>Go to your Tourist Town account and paste the number into the Longitude field on your Business Profile<br />
                  </li>
                <li>Confirm you have pasted the correct number into the correct field. In this example it would read: Latitude 44.437457  Longitude -81.388233<br />
                  </li>
                <li>Click &quot;Submit&quot;</li>
              </ol></td>
          </tr>
          <tr>
            <td align="left" valign="top" bgcolor="#CCCCCC"><img src="images/spacer.gif" alt="" width="10" height="1" /></td>
          </tr>
          <tr>
            <td align="left" valign="top"><p class="PageTitle">Watch the Video Tutorial</p>
              <p>
                <iframe width="629" height="354" src="http://www.youtube.com/embed/EKLuNdD5PyE?rel=0" frameborder="0" allowfullscreen></iframe>
              </p></td>
          </tr>
          <tr>
            <td align="left" valign="top" bgcolor="#CCCCCC"><img src="images/spacer.gif" alt="" width="10" height="1" /></td>
          </tr>
          <tr>
            <td width="642" align="left" valign="top"><p class="PageTitle">
              <?php require_once('Support-Footer.php'); ?>
<br />
          </p></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><img src="images/spacer.gif" alt="" width="30" height="30" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
