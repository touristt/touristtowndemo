<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Tourist Town - Tutorials</title>
<link href="tt.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top" bgcolor="#FFFFFF"><?php require_once('TopNav.php'); ?></td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="#BABABA"><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/spacer.gif" width="30" height="30" /></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><table width="960" border="0" cellspacing="30" cellpadding="0">
          <tr>
            <td colspan="2" align="left" valign="top" class="SectionHeading">Tutorials</td>
          </tr>
          <tr>
            <td colspan="2" align="left" valign="top" bgcolor="#8DC63F"><img src="images/spacer.gif" alt="" width="10" height="5" /></td>
          </tr>
          <tr>
            <td width="258" rowspan="3" align="left" valign="top" bgcolor="#FFFFFF"><?php require_once('Tutorial-Nav.php'); ?></td>
            <td align="left" valign="top"><p class="PageTitle">Accomodation Search</p>
              <p>Visitors to the Tourism website, can search for accomodation based on their amenities and room count. Please be sure to update your applicable ammenities and number of rooms your business offers. Your listing will only show up in user searches once you have added the appropriate ammenities to your listing.</p>
              <p class="bold">To update your listing</p>
              <ol>
                <li>Login into your account (my.touristtown.ca)<br />
                </li>
                <li>Click on &quot;Listings&quot; in top navigation<br />
                </li>
                <li>Click &quot;Edit&quot;<br />
                </li>
                <li>Click &quot;Amenities&quot;<br />
                </li>
                <li>Scroll down to the bottom and select the number of bedrooms your business offers. You may select multiple room counts.</li>
              </ol>
              <p>Please ensure you update your listing so that your business shows up in these accomodation search results.</p></td>
          </tr>
          <tr>
            <td align="left" valign="top" bgcolor="#CCCCCC"><img src="images/spacer.gif" alt="" width="10" height="1" /></td>
          </tr>
          <tr>
            <td width="642" align="left" valign="top"><p class="PageTitle">
              <?php require_once('Support-Footer.php'); ?>
  <br />
              </p></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><img src="images/spacer.gif" alt="" width="30" height="30" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
