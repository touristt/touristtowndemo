<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="#F6F6F6"><table width="240" border="0" cellspacing="15" cellpadding="0">
      <tr>
        <td><table width="100%" border="0" cellspacing="5" cellpadding="0">
          <tr>
            <td class="bold">Member Tutorials</td>
          </tr>
          <tr>
            <td>• <a href="TT-Tutorials-AddingPhotos.php">Adding Photos</a></td>
          </tr>
          <tr>
            <td>• <a href="TT-Tutorials-ResizingImages.php">Resize Images</a></td>
          </tr>
          <tr>
            <td>• <a href="TT-Tutorials-MapSettings.php">Map Settings </a></td>
          </tr>
          <tr>
            <td>• <a href="TT-Tutorials-AccomodationSearch.php">Accomodation Search</a></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>