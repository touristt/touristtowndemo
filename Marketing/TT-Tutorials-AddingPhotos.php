<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Tourist Town - Tutorials</title>
<link href="tt.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" valign="top" bgcolor="#FFFFFF"><?php require_once('TopNav.php'); ?></td>
  </tr>
  <tr>
    <td align="center" valign="top" bgcolor="#BABABA"><table width="960" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><img src="images/spacer.gif" width="30" height="30" /></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><table width="960" border="0" cellspacing="30" cellpadding="0">
          <tr>
            <td colspan="2" align="left" valign="top" class="SectionHeading">Tutorials</td>
          </tr>
          <tr>
            <td colspan="2" align="left" valign="top" bgcolor="#8DC63F"><img src="images/spacer.gif" alt="" width="10" height="5" /></td>
          </tr>
          <tr>
            <td width="258" rowspan="5" align="left" valign="top" bgcolor="#FFFFFF"><?php require_once('Tutorial-Nav.php'); ?></td>
            <td align="left" valign="top"><p class="PageTitle">Adding Photos </p>
              <p>Before you can upload an image to your listing, you will need to have an image ready to upload. You can use a software program, such as Photoshop to resize the image or you can use an online tool provided by Tourist Town. Click here for <a href="TT-Tutorials-ResizingImages.php">Resize Images</a> tutorial.</p>
              <p class="bold">To upload a photo to your listing:</p>
              <ol>
                <li>Login ( <a href="http://my.touristtown.ca">my.touristtown.ca</a> )</li>
                <li>Click Listings tab at top of page</li>
                <li>Click &quot;Edit&quot;</li>
                <li>On left, click &quot;Main Listing Page&quot;</li>
                <li>Scroll down to &quot;Thumbnail&quot;.</li>
                <li>Click &quot;Browse&quot;</li>
                <li>Browse your computer to find the file you want to upload.</li>
                <li>Enter a name for the file in &quot;Thumbnail Description&quot;</li>
                <li>Scroll to bottom of page and click &quot;Submit&quot;</li>
              </ol></td>
          </tr>
          <tr>
            <td align="left" valign="top" bgcolor="#CCCCCC"><img src="images/spacer.gif" alt="" width="10" height="1" /></td>
          </tr>
          <tr>
            <td align="left" valign="top"><p class="PageTitle">Watch the Video Tutorial</p>
              <p>
                <iframe width="629" height="354" src="http://www.youtube.com/embed/klKrBOBk0Zs?rel=0" frameborder="0" allowfullscreen></iframe>
              </p></td>
          </tr>
          <tr>
            <td align="left" valign="top" bgcolor="#CCCCCC"><img src="images/spacer.gif" alt="" width="10" height="1" /></td>
          </tr>
          <tr>
            <td width="642" align="left" valign="top"><p class="PageTitle">
              <?php require_once('Support-Footer.php'); ?>
<br />
          </p></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><img src="images/spacer.gif" alt="" width="30" height="30" /></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="center" valign="top">&nbsp;</td>
  </tr>
</table>
</body>
</html>
