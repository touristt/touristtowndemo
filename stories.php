<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1356712584397235";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!--Description Start-->
<section class="description stories">
    <div class="description-wrapper">
        <div class="description-inner">
            <div class="story-left">
                <h1 class="heading-text"><?php echo $activeStory['S_Title'] ?></h1>
                <?php if (isset($REGION['R_Show_Hide_Story_Author']) && $REGION['R_Show_Hide_Story_Author'] == 1) { ?>
                    <div class="story-title margin-none"><?php echo $activeStory['S_Author'] ?></div>  
                <?php } ?>
                <div class="story-share">
                    <div class="story-share-wrapper">
                        <div class="story-share-text"><?php echo (isset($REGION['R_Story_Share_Text']) && $REGION['R_Story_Share_Text'] != '') ? $REGION['R_Story_Share_Text'] : 'Share this story...'; ?></div>
                        <div class="story-imgs">
                            <div class="fb-like" data-href="<?php print curPageURL(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"style="vertical-align: middle;"></div>
                            <iframe class="twitter-class"
                                    src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=http%3A%2F%2F<?php print curPageURL(); ?>&related=twitterapi%2Ctwitter&text=<?php echo $activeStory['S_Title'] ?> in <?php echo $REGION['R_Name'] ?>"
                                    width="70"
                                    height="25"
                                    style="border: 0; overflow: hidden; float: left;">
                            </iframe>

                        </div>
                    </div>
                </div>
                <?php
                $getStoryContent = "SELECT CP_Title, CP_Photo, CP_Description, CP_Video_Link, CP_Pdf, CP_Pdf_Title FROM tbl_Content_Piece WHERE CP_S_ID = '" . encode_strings($activeStory['S_ID'], $db) . "' ORDER BY CP_Order ASC";
                $resContent = mysql_query($getStoryContent, $db) or die("Invalid query: $getStoryContent -- " . mysql_error());
                $i = 1;
                while ($activeContent = mysql_fetch_assoc($resContent)) {
                    ?>
                    <div style="width: 580px; float: left;"> 
                        <?php
                        $VIDID = $activeContent['CP_Video_Link'];
                        $pos = strpos($VIDID, '=');
                        if ($pos == false) {
                            $exp_video_link = explode('/', $VIDID);
                            $video_link = end($exp_video_link);
                        } else {
                            $exp_video_link = explode("=", $VIDID);
                            $video_link = end($exp_video_link);
                        }
                        if ($VIDID != "") {
                            ?>
                            <div class="<?php echo"player" . $i . "_wrapper"; ?>" style="position: absolute;opacity: 0;margin-top: 30px;">
                                <input type="hidden" id="video-link-<?php echo $i; ?>" value="<?php echo $video_link ?>">
                                <div  id="<?php echo "player" . $i; ?>"></div>
                            </div>
                        <?php } ?>
                        <?php if ($VIDID != "") { ?>
                            <div style="
                                 position: absolute;
                                 width: 46%;" class="watch-video play-button show-slider-display">
                                <div style="
                                     margin: 280px auto;
                                     width: 149px;
                                     " class="slider-button">
                                    <img style="margin-top: -4px;
                                         position: relative;" class="video_icon" src="../../../images/videoicon.png" alt="Play">
                                    <a style="border: 2px solid #FFF;
                                       vertical-align: top;
                                       padding: 15px 20px 15px 55px;
                                       font-family: IdealSans-Light-Pro;
                                       font-weight: normal;
                                       text-transform: uppercase;
                                       color: #FFF;
                                       font-size: 15px;
                                       border-radius: 10px;
                                       margin-left: -50px;
                                       margin-top: -13px;
                                       text-decoration: none;
                                       cursor: pointer;
                                       background-color: rgba(0,0,0,0.5);" onclick="play_video(<?php echo $i; ?>, '<?php echo"player" . $i; ?>')">Play Video</a>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($activeContent['CP_Photo'] != "") {
                            ?>
                            <div class="story-photo"><img src="<?php echo IMG_LOC_REL . $activeContent['CP_Photo'] ?>" alt="<?php echo $activeContent['CP_Title'] ?>"/></div>
                        <?php } ?>
                        <div class="story-title"><?php echo $activeContent['CP_Title'] ?></div>
                        <div class="story-desc theme-paragraph ckeditor-anchor-listing"><?php echo $activeContent['CP_Description'] ?></div>
                        <?php if (isset($activeContent['CP_Pdf']) && $activeContent['CP_Pdf'] !== '') { ?>
                            <div class="story-desc theme-paragraph ckeditor-anchor-listing pdf-story-front-view"><a  href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $activeContent['CP_Pdf'] ?>" target="_blank"><?php echo ($activeContent['CP_Pdf_Title']) ? $activeContent['CP_Pdf_Title'] : "View PDF" ?></a></div>
                        <?php } ?>
                    </div>
                    <?php
                    $i++;
                }
                ?>
            </div>
            <input type="hidden" id="total_players" value="<?php echo $i; ?>">
            <div class="story-right">
                <form method="POST" action="">
                    <input type="hidden" name="op" value="searchStories">
                    <div class="story-filter">
                        <input type="hidden" name="currentStory" id="currentStory" value="<?php echo $url[2]; ?>">
                        <input type="hidden" name="currentRegion" id="currentRegion" value="<?php echo $REGION['R_ID'] ?>">
                        <input type="hidden" name="currentLimit" id="currentLimit" value="<?php echo $REGION['R_Sidebar_Stories'] ?>">

                        <div id="dr1" style="background-color: white;">
                            <div class="drop-down-arrow1"></div>
                        </div>
                        <select name="cat_stories" id="cat_stories">
                            <?php
                            $sqlTopTen = "SELECT C_ID, C_Name, RC_Name FROM tbl_Region_Category
                                           LEFT JOIN tbl_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                                           WHERE C_Parent = '" . $activeStory['C_Parent'] . "' AND RC_Status = 0 ORDER BY C_ID ASC";
                            $resTopTen = mysql_query($sqlTopTen, $db) or die("Invalid query: $sqlTopTen -- " . mysql_error());
                            while ($rowExploreSub = mysql_fetch_array($resTopTen)) {
                                ?>
                                <option value="<?php echo $rowExploreSub['C_ID'] ?>" <?php echo (($activeStory['S_Category'] == $rowExploreSub['C_ID']) ? "selected" : "") ?> >
                                    <?php echo (($rowExploreSub['RC_Name'] == "") ? $rowExploreSub['C_Name'] : $rowExploreSub['RC_Name']) ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </form>
                <div id="stories-right">
                    <?php
                    $stories = "SELECT S_ID, S_Category, S_Active, S_Title, S_Thumbnail FROM tbl_Story
                                INNER JOIN tbl_Story_Region ON S_ID = SR_S_ID
                                $StorySeasons_JOIN WHERE S_Category = '" . $activeStory['S_Category'] . "' AND SR_R_ID = '" . $REGION['R_ID'] . "' AND S_ID != '" . $url[2] . "' AND S_Active = 1 $StorySeasons_WHERE ORDER BY S_Title";
                    //See if this region has any limit
                    if ($REGION['R_Sidebar_Stories'] > 0) {
                        $stories .= " LIMIT " . $REGION['R_Sidebar_Stories'];
                    } else {
                        $stories .= " LIMIT 10";
                    }
                    $resStoryTopTen = mysql_query($stories, $db) or die("Invalid query: $stories -- " . mysql_error());
                    while ($rowStoryTopTen = mysql_fetch_assoc($resStoryTopTen)) {
                        if ($rowStoryTopTen['S_Thumbnail'] != '') {
                            $story_image = $rowStoryTopTen['S_Thumbnail'];
                        } else {
                            $story_image = $default_thumbnail_image;
                        }
                        ?>
                        <div class='thumbnails static-thumbs'>
                            <div class="thumb-item">
                                <a href="/<?php echo $storyCatSEORow['C_Name_SEO'] ?>/<?php echo clean($rowStoryTopTen['S_Title']) ?>/<?php echo clean($rowStoryTopTen['S_ID']) ?>">
                                    <img src="<?php echo IMG_LOC_REL . $story_image; ?>" alt="<?php echo $rowStoryTopTen['S_Title'] ?>" longdesc="<?php echo $rowStoryTopTen['S_Title'] ?>">
                                    <h3 class="thumbnail-heading"><?php echo (($activeStory['RC_Name'] == "") ? $activeStory['C_Name'] : $activeStory['RC_Name']) ?></h3>
                                    <h3 class="thumbnail-desc"><?php echo $rowStoryTopTen['S_Title'] ?></h3>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="upcoming-events">
                    <div class="upcoming-events-wrapper">
                        <a href="/<?php echo $storyCatSEORow['C_Name_SEO'] ?>/"><?php echo (isset($REGION['R_Story_See_All_Text']) && $REGION['R_Story_See_All_Text'] != '') ? $REGION['R_Story_See_All_Text'] : 'See All Stories'; ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Description End-->