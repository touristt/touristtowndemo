<?php
require_once "../include/config.inc.php";
require_once "../include/public-site-functions.inc.php";
if ($REGION['R_My_Trip'] == 0) {
    header("Location: /404.php");
    exit();
}
$SEOtitle = $REGION["R_SEO_Title"];
$SEOdescription = $REGION["R_SEO_Description"];
$SEOkeywords = $REGION["R_SEO_Keywords"];
require "../include/public/header.php";

?>
<!--Description Start-->
<section class="description search-results margin-bottom-none">
    <div class="description-wrapper">
        <div class="description-inner padding-bottom-none">
            <h1 class="heading-text"><?php echo (($REGION['R_Trip_Planner_Title'] == "") ? "Trip Planner" : $REGION['R_Trip_Planner_Title']) ?></h1>
        </div>
    </div>
</section>
<!--Thumbnail Grid-->
<section class="thumbnail-grid padding-bottom-none border-none">
    <div class="grid-wrapper">
        <div class="grid-inner border-none">
            <?PHP
            if ($REGION['R_Parent'] == 0) {
                $REG = '';
            } else {
                $REG = "AND BLP_R_ID = " . $REGION['R_ID'];
            }
            $sql = "SELECT DISTINCT BL_ID, BLP_Photo, BL_Name_SEO, BL_Photo_Alt, BL_Listing_Title, BL_Listing_Type, C_Name, RC_Name
                    FROM tbl_Session_Tripplanner
                    LEFT JOIN tbl_Business_Listing ON ST_BL_ID  = BL_ID
                    LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
                    LEFT JOIN tbl_Category ON C_ID = ST_Type_ID
                    LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                    LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                    WHERE C_ID = BLC_M_C_ID
                    AND ST_Session_ID = '" . encode_strings($_COOKIE["tripplanner"], $db) . "' $REG
                    GROUP BY BL_ID ORDER BY C_Name ASC, BL_Listing_Type DESC";

            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $i = 0;
            while ($row = mysql_fetch_assoc($result)) {
                $i++;
                if ($row['BLP_Photo'] != '') {
                    $listing_image = $row['BLP_Photo'];
                } else {
                    $listing_image = $default_thumbnail_image;
                }
                if ($i == 1) {
                    echo "<div class='thumbnails static-thumbs'>";
                }
                ?>
                <div class="thumb-item">
                    <a href="/profile/<?php echo $row['BL_Name_SEO'] ?>/<?php echo $row['BL_ID'] ?>/">
                        <img src="<?php echo IMG_LOC_REL . $listing_image ?>" alt="<?php echo $row['BL_Photo_Alt'] ?>" longdesc="<?php echo $row['BL_Listing_Title'] ?>"/>
                        <h3 class="thumbnail-heading"><?php echo (($row['RC_Name'] != "") ? $row['RC_Name'] : $row['C_Name']) ?></h3>
                        <h3 class="thumbnail-desc"><?php echo $row['BL_Listing_Title'] ?></h3>
                    </a>
                </div>
                <?php
                if ($i == 3) {
                    echo '</div>';
                    $i = 0;
                }
                ?>
                <?php
            }
            ?>
        </div>
    </div>
</section>    

<?php require '../include/public/footer.php'; ?>
