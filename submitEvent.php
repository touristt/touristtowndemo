<?PHP
///Email Templete dynamic
$email = "SELECT * FROM tbl_Email_Templates WHERE ET_ID = 9";
$res = mysql_query($email);
$rowEmail = mysql_fetch_assoc($res); 
$email_subject = $rowEmail['ET_Subject'];
$email_heading = $rowEmail['ET_Name'];
$email_body = $rowEmail['ET_Template'];
$email_footer = $rowEmail['ET_Footer'];
 // $url = 'http://'.$_SERVER[HTTP_HOST].$_SERVER['REQUEST_URI'];
//print_r($url);
if ($_POST['op'] == 'save') {
    $file_size = $_FILES['file']['size'];
    $max_filesize = 5342523;
    $pdf_name = str_replace(' ', '_', $_FILES['file']['name']);
    if ($pdf_name != "") {
        $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_name;
        $filePath = PDF_LOC_ABS . $random_name;
        if ($_FILES["file"]["type"] == "application/pdf") {
            if ($file_size < $max_filesize) {
                move_uploaded_file($_FILES["file"]["tmp_name"], $filePath);
            }
        }
    }
    $weekdays = array();
    $weekdays[] .= ($_REQUEST['weekdays-mon'] > 0) ? $_REQUEST['weekdays-mon'] : "0";
    $weekdays[] .= ($_REQUEST['weekdays-tue'] > 0) ? $_REQUEST['weekdays-tue'] : "0";
    $weekdays[] .= ($_REQUEST['weekdays-wed'] > 0) ? $_REQUEST['weekdays-wed'] : "0";
    $weekdays[] .= ($_REQUEST['weekdays-thu'] > 0) ? $_REQUEST['weekdays-thu'] : "0";
    $weekdays[] .= ($_REQUEST['weekdays-fri'] > 0) ? $_REQUEST['weekdays-fri'] : "0";
    $weekdays[] .= ($_REQUEST['weekdays-sat'] > 0) ? $_REQUEST['weekdays-sat'] : "0";
    $weekdays[] .= ($_REQUEST['weekdays-sun'] > 0) ? $_REQUEST['weekdays-sun'] : "0";
    $weeks = implode(',', $weekdays);
    if ($_REQUEST['all_day'] == '1') {
        $timing = "EventTimeAllDay = '" . encode_strings("1", $db) . "', ";
    } else {
        $timing = " EventTimeAllDay = '" . encode_strings("0", $db) . "', 
                    EventStartTime = '" . encode_strings($_REQUEST['starttime'], $db) . "', 
                    EventEndTime = '" . encode_strings(substr($_REQUEST['endtime'], 0, 95), $db) . "', ";
    }
    $sql = "INSERT Events_master SET 
            Pending = TRUE, 
            E_Region_ID = '" . encode_strings($_REQUEST['community'], $db) . "', 
            Title = '" . encode_strings($_REQUEST['title'], $db) . "', 
            E_Name_SEO = '" . encode_strings($_REQUEST['seoNameEvent'], $db) . "', 
            $timing
            ShortDesc = '" . encode_strings(substr($_REQUEST['shortdesc'], 0, 700), $db) . "', 
            Content = '" . encode_strings($_REQUEST['content'], $db) . "', 
            EventDateStart = '" . encode_strings($_REQUEST['startdate'], $db) . "', 
            EventDateEnd = '" . encode_strings($_REQUEST['enddate'], $db) . "', 
            LocationID = '" . encode_strings($_REQUEST['setLocationID'], $db) . "', 
            LocationList = '" . encode_strings($_REQUEST['location'], $db) . "', 
            StreetAddress = '" . encode_strings($_REQUEST['address'], $db) . "', 
            OrganizationID = '" . encode_strings($_REQUEST['setOrganizationID'], $db) . "', 
            Organization = '" . encode_strings($_REQUEST['organization'], $db) . "', 
            Recuring = '" . encode_strings($weeks, $db) . "', 
            ContactName = '" . encode_strings($_REQUEST['contact'], $db) . "', 
            ContactPhone = '" . encode_strings($_REQUEST['phone'], $db) . "', 
            ContactPhoneTollFree = '" . encode_strings($_REQUEST['tollfree'], $db) . "', 
            Email = '" . encode_strings($_REQUEST['email'], $db) . "', 
            Event_Facebook_link = '" . encode_strings($_REQUEST['facebook_link'], $db) . "', 
            WebSiteLink = '" . encode_strings($_REQUEST['website'], $db) . "',
            EventPdf = '" . encode_strings($random_name, $db) . "',
            E_Spam_Text = '" . encode_strings($_REQUEST['video_link'], $db) . "'";
    require 'include/picUpload.inc.php';
    if ($_FILES['pictures']['name'] != '') {
        $pic = Upload_Pic_Normal('0', 'pictures', 0, 0, true, IMG_LOC_ABS, 0, true); //echo $pic; exit();
        if ($pic) {
            $sql .= ", Event_Image = '" . encode_strings($pic, $db) . "'";
        }
    } //echo $sql; exit();
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $Event_Id = mysql_insert_id();
    if ($result) {
        // TRACK DATA ENTRY
        $Region_Track_Id = $REGION['R_ID'];
        $Region_Track = $REGION['R_Name'];
        Track_Data_Entry($Region_Track, $Region_Track_Id, 'Events', $Event_Id, 'Create Event', 'public end');
        $community = $_REQUEST['community'];
        ob_start();
        include 'include/email-template/mytouristtown-html-event.php';
        $html = ob_get_contents();
        ob_clean();
        $EEmail = "SELECT R_Name, R_Event_Email FROM tbl_Region WHERE R_ID = '" . encode_strings($community, $db) . "'";
        $resultEEmail = mysql_query($EEmail, $db) or die("Invalid query: $EEmail -- " . mysql_error());
        $rowEEmail = mysql_fetch_assoc($resultEEmail);
        if (isset($rowEEmail['R_Event_Email']) && $rowEEmail['R_Event_Email'] !== '' && $_REQUEST['video_link'] == '') {
           // $to = "imtiazork@gmail.com";
            $mail = new PHPMailer();
            $mail->IsMail();
            $mail->From = MAIN_CONTACT_EMAIL;
            $mail->FromName = MAIN_CONTACT_NAME;
            $mail->IsHTML(true);
            $mail->AddAddress($rowEEmail['R_Event_Email']);
            //$mail->AddAddress($to);
            $mail->Subject = $email_subject;
            $mail->MsgHTML($html);
            $mail->Send();
        }
    }
    ?>
    <section class="thumbnail-grid  border-none">
        <div class="grid-wrapper">
            <div class="grid-inner border-none padding-bottom-none">  
                <a name="submitEvent"></a><div class="form-inside-heading">
                    <h2>Submit Your Event</h2>
                </div>
                <div class="event-from-inside">
                    <p>Thank you for submitting your event. Please allow 48 hours for your event to be approved and to be shown in the event listings.</p>
                </div>
            </div>
        </div>
    </section>
    <?PHP
} else {
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".datepicker").datepicker({
                firstDay: 1,
                dateFormat: 'yy-mm-dd'
            });
            $('.timepicker').timepicker({});
        });
    </script>
    <section class="thumbnail-grid  border-none">
        <div class="grid-wrapper">
            <form onSubmit="return validateEventForm();" enctype="multipart/form-data" action="" method="post">
                <input type="hidden" name="op" value="save">
                <script>
                    function makeSEO(myVar) {
                        myVar = myVar.toLowerCase();
                        myVar = myVar.replace(/[^a-z0-9\s-]/gi, '');
                        myVar = myVar.replace(/"/g, '');
                        myVar = myVar.replace(/'/g, '');
                        myVar = myVar.replace(/&/g, '');
                        myVar = myVar.replace(/\//g, '');
                        myVar = myVar.replace(/,/g, '');
                        myVar = myVar.replace(/  /g, ' ');
                        myVar = myVar.replace(/ /g, '-');

                        $('#seoNameEvent').val(myVar);
                        return false;
                    }
                    function showfilename(input) {
                        var fup = document.getElementById('filename');
                        var fullpath = fup.value;
                        var fname = fullpath.substring(fullpath.lastIndexOf('\\') + 1);
                        $("#pdfname").text(fname);
                    }
                    function show_file(input) {
                        if (input.files && input.files[0]) {
                            $('#uploadFileMain').show();
                            var reader = new FileReader();

                            reader.onload = function (e) {
                                $('#uploadFile')
                                        .attr('src', e.target.result);
                            };

                            reader.readAsDataURL(input.files[0]);
                        }
                    }
                    $(document).ready(function () {
                        $("#phone").mask("999-999-9999");
                        $("#tollfree").mask("999-999-9999");
                    });
                </script>



                <div class="form-inside-heading">
                    <h2>Submit Your Event</h2>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">
                        Event Title *
                    </div>
                    <div class="event-from-inside-field">
                        <input name="title" type="text" id="title" value="" onKeyUp="makeSEO(this.value)" size="50" required />

                    </div>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">
                        Event Name SEO *
                    </div>
                    <div class="event-from-inside-field">
                        <input name="seoNameEvent" type="text" id="seoNameEvent" value="" onKeyUp="makeSEO(this.value)" size="50" required />

                    </div>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">
                        Event Photo
                    </div>
                    <div class="event-from-inside-field">
                        <div class="pdf_event" >
                            <label for="photonames" class="lb event-photo">Browse Photo</label>
                            <input class="" onchange="show_file(this)" id="photonames" type="file" name="pictures[]"  value="" style="visibility: hidden; position: absolute; z-index: 0"/>
                            <div class="approved-photo-div" id="uploadFileMain" style="display:none;">
                                <img id="uploadFile" src="">          
                            </div>
                        </div>
                    </div>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">
                        Upload PDF
                    </div>
                    <div class="event-from-inside-field">
                        <div class="pdf_event" >
                            <label for="filename" class="lb">Browse for PDF</label>
                            <input class="" onchange="showfilename(this)" id="filename" type="file" name="file" accept="application/pdf" value="" style="visibility: hidden; position: absolute; z-index: 0"/>
                            <span><label id="pdfname"></label></span>
                        </div>
                    </div>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">
                        Start Date *
                    </div>
                    <div class="event-from-inside-field">
                        <input class="datepicker" name="startdate" type="text" id="startdate" value="" size="50" required />
                        <span>Format: YYYY-MM-DD</span>
                    </div>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">
                        End Date *
                    </div>
                    <div class="event-from-inside-field">
                        <input class="datepicker" name="enddate" type="text" id="enddate" value="" size="50" required />
                        <span>Format: YYYY-MM-DD</span>
                    </div>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">Recurring</div>
                    <div class="event-from-inside-field">
                        <div class="weekDays-selector">
                            <input type="checkbox" id="weekday-mon" name="weekdays-mon" value="0" class="weekday" />
                            <label class="weekday-mon" for="weekday-mon">M</label>
                            <input type="checkbox" id="weekday-tue" name="weekdays-tue" value="0" class="weekday" />
                            <label class="weekday-tue" for="weekday-tue">T</label>
                            <input type="checkbox" id="weekday-wed" name="weekdays-wed" value="0" class="weekday" />
                            <label class="weekday-wed" for="weekday-wed">W</label>
                            <input type="checkbox" id="weekday-thu" name="weekdays-thu" value="0" class="weekday" />
                            <label class="weekday-thu" for="weekday-thu">T</label>
                            <input type="checkbox" id="weekday-fri" name="weekdays-fri" value="0" class="weekday" />
                            <label class="weekday-fri" for="weekday-fri">F</label>
                            <input type="checkbox" id="weekday-sat" name="weekdays-sat" value="0" class="weekday" />
                            <label class="weekday-sat" for="weekday-sat">S</label>
                            <input type="checkbox" id="weekday-sun" name="weekdays-sun" value="0" class="weekday" />
                            <label class="weekday-sun" for="weekday-sun">S</label>
                        </div>
                    </div>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">Timing</div>
                    <div class="event-from-inside-field">
                        <select name="starttime" id="starttime" required>
                            <option value="">Start Time</option>
                            <option value="00:00:00">N/A</option>
                            <option value="00:00:01">Dusk</option>
                            <?PHP
                            for ($i = 1; $i < 24; $i++) {
                                ?>
                                <option value="<?php echo $i ?>:00:00">
                                    <?php echo $i > 12 ? $i - 12 : $i ?>
                                    :00 
                                    <?php echo $i > 11 ? 'pm' : 'am' ?>
                                </option>
                                <option value="<?php echo $i ?>:15:00">
                                    <?php echo $i > 12 ? $i - 12 : $i ?>
                                    :15 
                                    <?php echo $i > 11 ? 'pm' : 'am' ?>
                                </option>
                                <option value="<?php echo $i ?>:30:00">
                                    <?php echo $i > 12 ? $i - 12 : $i ?>
                                    :30 
                                    <?php echo $i > 11 ? 'pm' : 'am' ?>
                                </option>
                                <option value="<?php echo $i ?>:45:00">
                                    <?php echo $i > 12 ? $i - 12 : $i ?>
                                    :45 
                                    <?php echo $i > 11 ? 'pm' : 'am' ?>
                                </option>
                                <?PHP
                            }
                            ?>
                        </select>
                        <select name="endtime" id="endtime" required>
                            <option value="">End Time</option>
                            <option value="00:00:00">N/A</option>
                            <option value="00:00:01">Dusk</option>
                            <?PHP
                            for ($i = 1; $i < 24; $i++) {
                                ?>
                                <option value="<?php echo $i ?>:00:00">
                                    <?php echo $i > 12 ? $i - 12 : $i ?>
                                    :00 
                                    <?php echo $i > 11 ? 'pm' : 'am' ?>
                                </option>
                                <option value="<?php echo $i ?>:15:00">
                                    <?php echo $i > 12 ? $i - 12 : $i ?>
                                    :15 
                                    <?php echo $i > 11 ? 'pm' : 'am' ?>
                                </option>
                                <option value="<?php echo $i ?>:30:00">
                                    <?php echo $i > 12 ? $i - 12 : $i ?>
                                    :30 
                                    <?php echo $i > 11 ? 'pm' : 'am' ?>
                                </option>
                                <option value="<?php echo $i ?>:45:00">
                                    <?php echo $i > 12 ? $i - 12 : $i ?>
                                    :45 
                                    <?php echo $i > 11 ? 'pm' : 'am' ?>
                                </option>
                                <?PHP
                            }
                            ?>
                        </select>
                        <input type="checkbox" id="all_day" name="all_day" value="0" />
                        <span>All day</span>
                    </div>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">Facebook</div>
                    <div class="event-from-inside-field">
                        <input name="facebook_link" type="text"  value="" size="50">
                    </div>
                </div>
                <div class="event-from-inside" style="display:none;">
                    <div class="event-from-inside-title">Video Link</div>
                    <div class="event-from-inside-field">
                        <input name="video_link" type="text"  value="" size="50">
                    </div>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">Short Description *</div>
                    <div class="description-text-front-events">
                        <textarea  name="shortdesc" class="tt-ckeditor" onblur="limitTextArea('shortdesc', 200);" onkeyup="limitTextArea('shortdesc', 200);" cols="73" rows="8" wrap="VIRTUAL" id="shortdesc" required></textarea>
                    </div>
                    <span>200 Characters</span>
                </div>
                <script>
                    $(document).ready(function () {
                        CKEDITOR.config.width = 400;

                    });

                </script>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">Full Description *</div>
                    <div class="description-text-front-events">
                        <textarea name="content" class="tt-ckeditor" onblur="limitTextArea('content', 700);" onkeyup="limitTextArea('content', 700);" cols="73" rows="8" wrap="VIRTUAL" id="content" required></textarea>  
                    </div>
                    <span> 700 Characters  </span>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">Community *</div>
                    <div class="event-from-inside-field">
                        <select name="community" class="community_select" required>
                            <option value="">Select Community</option>
                            <?PHP
                            $sql = "SELECT GROUP_CONCAT(RM_Child) AS RM_Childs FROM tbl_Region_Multiple LEFT JOIN tbl_Region ON R_ID = RM_Parent
                                        WHERE R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' AND R_Parent != 1";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            $row = mysql_fetch_assoc($result);
                            if ($row['RM_Childs'] != "") {
                                $getRegions = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_ID IN (" . $row['RM_Childs'] . ")";
                                $result = mysql_query($getRegions, $db) or die("Invalid query: $getRegions -- " . mysql_error());
                                while ($rowTMP = mysql_fetch_assoc($result)) {
                                    ?>
                                    <option value="<?php echo $rowTMP['R_ID']; ?>"><?php echo $rowTMP['R_Name'] ?></option>
                                    <?PHP
                                }
                            } else {
                                ?>
                                <option value="<?php echo $REGION['R_ID']; ?>" selected><?php echo $REGION['R_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <span style="margin-top: 10px; margin-left: 0;">If your event is not located in one of these communities, please select the community closest to the event.</span>
                    </div>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">Location *</div>
                    <div class="event-from-inside-field">
                        <div id="ajax-location">
                            <select name="setLocationID" id="setLocationID" required>
                                <!--  <option value="0">Select One or Use Alternate</option> -->
                                <option value="">Select One</option>
                                <option value="0">Use Alternate</option>
                                <?PHP
                                $EP = "SELECT RM_Child FROM tbl_Region_Multiple LEFT JOIN tbl_Region ON R_ID = RM_Parent
                                         WHERE R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
                                $resultEP = mysql_query($EP, $db);
                                $srchRegion = "";
                                if ($REGION['R_Parent'] == 0) {
                                    $event_counter = 0;
                                    while ($rowEP = mysql_fetch_assoc($resultEP)) {
                                        if ($event_counter == 0) {
                                            $operator = "";
                                        } else {
                                            $operator = " OR";
                                        }
                                        $srchRegion .= " $operator FIND_IN_SET (" . $rowEP['RM_Child'] . ", EL_Region_ID)";
                                        $event_counter++;
                                    }
                                } else {
                                    $srchRegion = " FIND_IN_SET (" . $REGION['R_ID'] . ", EL_Region_ID)";
                                }
                                $sql = "SELECT EL_ID, EL_Name FROM Events_Location WHERE $srchRegion ORDER BY EL_Name";
                                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                while ($rowTMP = mysql_fetch_assoc($result)) {
                                    ?>
                                    <option value="<?php echo $rowTMP['EL_ID'] ?>">
                                        <?php echo $rowTMP['EL_Name'] ?>
                                    </option>
                                    <?PHP
                                }
                                ?>
                            </select>
                        </div>
                        <div>
                            <input name="location" type="text" id="location" onfocus="clearLoc();" placeholder="Alternate Location"  size="50" />
                        </div>
                    </div>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">Address *</div>
                    <div class="event-from-inside-field">
                        <input name="address" type="text" id="address" value="" size="50" required />
                    </div>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">Organization *</div>
                    <div class="event-from-inside-field">
                        <div id="ajax-organization">
                            <select name="setOrganizationID" id="setOrganizationID" required>
                                <option value="">Select One</option>
                                <option value="0">Use Alternate</option>
                                <!-- <option value="0">Select One or Use Alternate</option> -->
                                <?PHP
                                $resultEP = mysql_query($EP, $db);
                                $srchRegion = "";
                                if ($REGION['R_Parent'] == 0) {
                                    $event_counter = 0;
                                    while ($rowEP = mysql_fetch_assoc($resultEP)) {
                                        if ($event_counter == 0) {
                                            $operator = "";
                                        } else {
                                            $operator = " OR";
                                        }
                                        $srchRegion .= " $operator FIND_IN_SET (" . $rowEP['RM_Child'] . ", EO_Region_ID)";
                                        $event_counter++;
                                    }
                                } else {
                                    $srchRegion = " FIND_IN_SET (" . $REGION['R_ID'] . ", EO_Region_ID)";
                                }
                                $sql = "SELECT EO_ID, EO_Name FROM Events_Organization WHERE $srchRegion ORDER BY EO_Name";
                                $result = mysql_query($sql, $db);
                                while ($rowTMP = mysql_fetch_assoc($result)) {
                                    ?>
                                    <option value="<?php echo $rowTMP['EO_ID'] ?>">
                                        <?php echo $rowTMP['EO_Name'] ?>
                                    </option>
                                    <?PHP
                                }
                                ?>

                            </select>
                        </div>
                        <div>
                            <input name="organization" type="text" id="organization" onfocus="clearOrg();" placeholder="Alternate Organization" size="50" />
                        </div>
                    </div>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">Contact Name *</div>
                    <div class="event-from-inside-field">
                        <input name="contact" type="text" id="contact" value="" size="50" required />
                    </div>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">Contact Phone</div>
                    <div class="event-from-inside-field">
                        <input name="phone" class="phone" type="text" id="phone" value="" size="50" />
                    </div>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">Additional Phone</div>
                    <div class="event-from-inside-field">
                        <input name="tollfree" class="phone" type="text" id="tollfree" value="" size="50" />
                    </div>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">Email</div>
                    <div class="event-from-inside-field">
                        <input name="email" type="text" id="email" value="" size="50" />
                    </div>
                </div>
                <div class="event-from-inside">
                    <div class="event-from-inside-title">Website</div>
                    <div class="event-from-inside-field">
                        <input name="website" type="text" id="website" value="" size="50" />
                        <span>Format: www.YourWebsite.com</span>
                    </div>
                </div> 
                <div class="event-from-inside">
                    <div class="event-from-inside-title"></div>
                    <div class="event-from-inside-field">
                        <div class="g-recaptcha" data-sitekey="6Lf4_U0UAAAAACQc7YpqpPkSE3-AQwjFiQwcgoXF"></div>
                    </div>
                </div> 
                <div class="event-from-inside">
                    <div class="event-from-inside-button">
                        <input type="submit" name="submit" value="Submit Event">
                    </div>
                </div>
                <div class="event-from-inside-waring">* denotes field is mandatory</div>
            </form>
        </div>
    </section>
<?PHP } ?>
<script>
    $(document).ready(function () {
        CKEDITOR.disableAutoInline = true; // Use CKEDITOR.replace() if element is <textarea>.
        $('.tt-ckeditor').ckeditor({
            customConfig: 'http://touristtowndemo.com/include/plugins/ckeditor/config.js'
        });
    });
</script>