<?php
require_once 'include/config.inc.php';
require_once 'include/public-site-functions.inc.php';
require_once 'include/public/header.php';
?>
<!--Thumbnail Grid-->
<section class="thumbnail-grid padding-bottom-none border-none">
    <div class="grid-wrapper">
        <div class="grid-inner border-none">
          <script type="text/javascript">
            var __machform_url = 'http://machform.brucecounty.on.ca/embed.php?id=41054';
            var __machform_height = 1792;
          </script>
          <div id="mf_placeholder"></div>
          <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
          <script type="text/javascript" src="http://machform.brucecounty.on.ca/js/jquery.ba-postmessage.min.js"></script>
          <script type="text/javascript" src="http://machform.brucecounty.on.ca/js/machform_loader.js"></script>
        </div>
    </div>
</section>
<?php
require_once 'include/public/footer.php';
?>
