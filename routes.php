<?php
require_once 'include/config.inc.php';
require_once 'update-impression-advert.php';
require_once 'include/public-site-functions.inc.php';

$sqlRoute = "SELECT * FROM tbl_Route WHERE R_RID = '" . encode_strings($REGION['R_ID'], $db) . "'";
$resRoute = mysql_query($sqlRoute, $db) or die("Invalid query: $sqlRoute -- " . mysql_error());
$Routes = mysql_fetch_assoc($resRoute);
$SEOtitle = $Routes['R_SEO_Title'];
$SEOdescription = $Routes['R_SEO_Description'];
$SEOkeywords = $Routes['R_SEO_Keywords'];
if ($REGION['R_My_Route'] == 0) {
    header("Location: /404.php");
    exit();
}
$sqlRouteCat = "SELECT RC_R_ID, RC_Order, RC_Feature_Photo, RC_Alt_Tag, RC_Name, RC_ID, RC_Status,  RC_Feature_Photo, RC_Introduction_Text FROM tbl_Route_Category 
                WHERE RC_R_ID ='" . encode_strings($REGION['R_ID'], $db) . "' AND RC_Status = 1
                ORDER BY RC_Order ASC";
$resRouteCat = mysql_query($sqlRouteCat, $db) or die("Invalid query: $sqlRouteCat -- " . mysql_error());

//check to pass mobile users to mobile version
$useragent = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
    header('Location: http://' . $REGION['R_Domain'] . DOMAIN_MOBILE_REL . 'routes/');
    exit;
}
?>
<?php require_once 'include/public/header.php'; ?>
<!--Slider Start-->
<section class="main_slider">
    <div class="slider_wrapper">
        <div class="slider_wrapper_video">
            <div class="sliderCycle" data-cycle-loader="wait" data-cycle-fx=fade data-cycle-timeout=0 data-cycle-prev="#prev" data-cycle-next="#next">
                <?php
                $i = 1;
                $show_default = true; //if no image, show default
                $VIDEOID = $Routes['R_Video'];
                $pos = strpos($VIDEOID, '=');
                if ($pos == false) {
                    $video_link = end(explode('/', $VIDEOID));
                } else {
                    $video_link = end(explode("=", $VIDEOID));
                }
                if ($Routes['R_Photo'] != '') {
                    $show_default = false;
                    ?>
                    <div class="slide-images">
                        <?php if ($VIDEOID != "") { ?>
                            <div class="<?php echo"player" . $i . "_wrapper"; ?>" style="position:absolute;opacity: 0;top:0;">
                                <input type="hidden" id="video-link-<?php echo $i; ?>" value="<?php echo $video_link ?>">
                                <div  id="<?php echo "player" . $i; ?>"></div>
                            </div>
                        <?php } ?>
                        <div class="slider-text">
                            <h1 class="show-slider-display"><?php echo $Routes['R_Slider_Title'] ?></h1>
                            <h3 class="show-slider-display"><?php echo $Routes['R_Slider_Description'] ?></h3>
                            <?php if ($VIDEOID != "") { ?>
                                <div class="watch-video play-button show-slider-display">
                                    <div class="slider-button">
                                        <img class="video_icon" src="http://<?php echo DOMAIN ?>/images/videoicon.png" alt="Play">
                                        <a onclick="play_video(<?php echo $i; ?>, '<?php echo"player" . $i; ?>')">Watch full video</a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <img class="slider_image" class="show-slider-display" src="<?php echo IMG_LOC_REL . $Routes['R_Photo'] ?>" alt="<?php echo ($Routes['R_Alt_Tag'] != '') ? $Routes['R_Alt_Tag'] : $Routes['R_Slider_Title']; ?>">
                    </div>
                    <?php
                } if ($show_default) {
                    ?>
                    <div class="slide-images" style="<?php echo $image_slider_style ?>">
                        <img class="slider_image" class="show-slider-display" src="<?php echo IMG_LOC_REL . $default_header_image; ?>" alt="<?php echo ($slide['alt'] != '') ? $slide['alt'] : $activeCat['C_Name']; ?>">
                    </div> 
                    <?php
                }
                $i++;
                ?>
            </div>
            <input type="hidden" id="total_players" value="<?php echo $i; ?>">
            <div class=center>
                <span id=prev></span>
                <span id=next></span>
            </div>
            <?php if ($THEME['TO_Slider_Overlay'] != "") { ?>
                <div class="image_overlay_img">
                    <div class="image_overlay">
                        <img src="<?php echo IMG_ICON_REL . $THEME['TO_Slider_Overlay']; ?>" alt="Slider Overlay">
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<!--Slider End-->
<!--Description Start-->
<section class="description theme-description">
    <div class="description-wrapper">
        <div class="description-inner">
            <h1 class="heading-text"><?php echo (($Routes['R_Name']) != '' ? $Routes['R_Name'] : '') ?></h1>
            <div class="site-text ckeditor-anchors">
                <?PHP if ($Routes['R_Description']) { ?>
                    <p><?PHP echo ($Routes['R_Description']); ?></p>
                <?PHP } ?>
            </div>
        </div>
    </div>
</section>
<!--Description End-->
<!-- Routes Categories -->
<section class="section_stories" class="padding-bottom-none border-none">
    <div class="grid-wrapper">
        <div class="grid-inner padding-bottom-none slider">
            <div class="feature-stories">
                <?php
                while ($rowRouteCat = mysql_fetch_array($resRouteCat)) {
                    if ($rowRouteCat['RC_Feature_Photo'] != '') {
                        $route_cat_image = $rowRouteCat['RC_Feature_Photo'];
                    } else {
                        $route_cat_image = $default_header_image;
                    }
                    ?>
                    <div class="feature-story">
                        <a href="/routes/<?php echo clean($rowRouteCat['RC_Name']) ?>/<?php echo $rowRouteCat['RC_ID']; ?>/">
                            <img src="<?php echo IMG_LOC_REL . $route_cat_image ?>" width="100%" height="auto" alt="<?php echo ($rowRouteCat['RC_Alt_Tag'] != '') ? $rowRouteCat['RC_Alt_Tag'] : $rowRouteCat['RC_Name'] ?>" />
                        </a>
                        <a href="/routes/<?php echo clean($rowRouteCat['RC_Name']) ?>/<?php echo $rowRouteCat['RC_ID']; ?>/">
                            <h1 align="center" class="heading-text"><?php echo $rowRouteCat['RC_Name'] ?></h1>
                        </a>
                        <?php
                        if ($rowRouteCat['RC_Introduction_Text'] != '') {
                            $string = preg_replace('/(.*)<\/p[^>]*>/i', '$1', $rowRouteCat['RC_Introduction_Text']);
                            ?>
                            <div class="story-description">
                                <?php echo $string ?>
                                <a href="/routes/<?php echo clean($rowRouteCat['RC_Name']) ?>/<?php echo $rowRouteCat['RC_ID']; ?>/">
                                    Read More
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>  
    </div>
</section>
<!-- Routes Categories end -->
<?php require_once 'include/public/footer.php'; ?>