<!--Whats on around town.-->
<section class="description whats-on event-nagative-margin">
    <div class="description-wrapper">
        <div class="event-filter">
            <div class="filter-inner">
                <?php require_once 'nav-left.php'; ?>
            </div>
        </div>
        <div class="events-wrapper">
            <div class="events">
                <?php
                //Getting Local Events from DB
                $sql = "SELECT EventDateStart, EventID, Title, E_Name_SEO, ShortDesc, LocationID, EventType, EventDateEnd, Pending FROM `Events_master` 
                        LEFT JOIN Events_Location ON EL_ID = LocationID";
                //weekly events
                if ($activeCat['C_ID'] == '87') {
                    if (isset($_REQUEST['opSearch'])) {
                        $sql .= " WHERE `EventDateStart` >= '" . encode_strings($_REQUEST['txtStartDay'], $db) . "'
                              AND EventDateStart <= '" . encode_strings($_REQUEST['txtEndDay'], $db) . "' ";
                    } else {
                        $sql .= " WHERE `EventType` = 3 AND `EventDateEnd` >= CURDATE()"; // AND `EventDateStart` <= CURDATE()   i comment it because of 2014 weekly events are not showing
                    }
                }
                //on going events
                elseif ($activeCat['C_ID'] == '88') {
                    if ($_REQUEST['opSearch']) {
                        $sql .= " WHERE `EventDateStart` >= '" . encode_strings($_REQUEST['txtStartDay'], $db) . "'
                              AND EventDateStart <= '" . encode_strings($_REQUEST['txtEndDay'], $db) . "' ";
                    } else {
                        $sql .= " WHERE `EventType` = 2 AND `EventDateEnd` >= CURDATE( ) AND `EventDateStart` <= CURDATE( ) ";
                    }
                }
                //upcommig events
                elseif ($activeCat['C_ID'] == '34') {
                    if ($_REQUEST['opSearch']) {
                        $sql .= " WHERE `EventDateStart` >= '" . encode_strings($_REQUEST['txtStartDay'], $db) . "'
                              AND EventDateStart <= '" . encode_strings($_REQUEST['txtEndDay'], $db) . "' ";
                    } else {
                        $sql .= " WHERE (`EventDateStart` >= CURDATE() OR `EventDateEnd` >= CURDATE()) ";
                    }
                }
                //Special events
                elseif ($activeCat['C_ID'] == '50') {
                    if ($_REQUEST['opSearch']) {
                        $sql .= " WHERE `EventDateStart` >= '" . encode_strings($_REQUEST['txtStartDay'], $db) . "'
                              AND EventDateStart <= '" . encode_strings($_REQUEST['txtEndDay'], $db) . "' ";
                    } else {
                        $sql .= " WHERE `EventType` = 5 AND `EventDateStart` >= CURDATE() ";
                    }
                }
                //Festivals
                elseif ($activeCat['C_ID'] == '35') {
                    if ($_REQUEST['opSearch']) {
                        $sql .= " WHERE `EventDateStart` >= '" . encode_strings($_REQUEST['txtStartDay'], $db) . "'
                              AND EventDateStart <= '" . encode_strings($_REQUEST['txtEndDay'], $db) . "' ";
                    } else {
                        $sql .= " WHERE `EventType` = 4 AND `EventDateStart` >= CURDATE() ";
                    }
                }

                //Festival and special events
                elseif ($activeCat['C_ID'] == '94') {
                    if ($_REQUEST['opSearch']) {
                        $sql .= " WHERE `EventDateStart` >= '" . encode_strings($_REQUEST['txtStartDay'], $db) . "'
                              AND EventDateStart <= '" . encode_strings($_REQUEST['txtEndDay'], $db) . "' ";
                    } else {
                       $sql .= " WHERE `EventType` = 6 AND `EventDateStart` >= CURDATE()";
                    }
                }
                //Volunteering
                elseif ($activeCat['C_ID'] == '102') {
                    if ($_REQUEST['opSearch']) {
                        $sql .= " WHERE `EventDateStart` >= '" . encode_strings($_REQUEST['txtStartDay'], $db) . "'
                              AND EventDateStart <= '" . encode_strings($_REQUEST['txtEndDay'], $db) . "' ";
                    } else {
                        $sql .= " WHERE `EventType` = 7 AND `EventDateStart` >= CURDATE() ";
                    }
                }
                //Planning an Event
                else{ 
                  $sql .= "  WHERE  Pending = 0 ";
                }
                $E_Region_ID = "";
                if ($REGION['R_Parent'] == 0 && $REGION['R_Type'] != 4) {
                    $EP = "SELECT RM_Child FROM `tbl_Region_Multiple` LEFT JOIN tbl_Region ON R_ID = RM_Parent WHERE R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
                    $resultEP = mysql_query($EP, $db) or die("Invalid query: $EP -- " . mysql_error());
                    $event_counter = 0;
                    while ($rowEP = mysql_fetch_assoc($resultEP)) {
                        if ($event_counter == 0) {
                            $operator = " AND (";
                        } else {
                            $operator = " OR";
                        }
                        $E_Region_ID .= " $operator FIND_IN_SET (" . $rowEP['RM_Child'] . ", E_Region_ID)";
                        $event_counter++;
                    }
                    $E_Region_ID .= " $operator FIND_IN_SET (" . $REGION['R_ID'] . ", E_Region_ID) )";
                } else {
                    $E_Region_ID = " AND FIND_IN_SET (" . $REGION['R_ID'] . ", E_Region_ID)";
                }
                $sql .= "  AND (`EventDateStart` >= CURDATE() OR ( `EventDateEnd` >= CURDATE( ) AND `EventDateStart` <= CURDATE( ))) $E_Region_ID";
                if ($activeCat['C_ID'] != '34') {
                    $sql .= " ORDER BY EventDateStart";
                } else {
                    $sql .= " ORDER BY EventDateEnd";
                } //echo $sql;
                // obtain the count of the number of rows in the current query
                // instantiate the Paginate object with said number of rows
                $result = mysql_query($sql, $db);
                $num_rows = mysql_num_rows($result);
                if($num_rows > 0){
                $pages = new Paginate(mysql_num_rows($result), 20);
                $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
                }
                $totalEvents = mysql_num_rows($result);
                if (mysql_num_rows($result) > 0) {
                    $i = 1;
                    $j = 1;
                    $totalEvents = mysql_num_rows($result);
                    while ($row = mysql_fetch_assoc($result)) {
                        if ($i == 1) {
                            if ($totalEvents == $j) {
                                ?>
                                <div class="event-column-outer margin-bottom-event">
                                <?php } else { ?>
                                    <div class="event-column-outer <?php echo ($j == 1) ? "margin-top-event" : "" ?>">
                                        <?php
                                    }
                                }
                                ?>
                                <div class="event-column">
                                    <div class="event-date">
                                      <?php 
                                      echo date('M j, Y', strtotime($row['EventDateStart']));
                                      if ($row['EventDateStart'] != $row['EventDateEnd'] && $row['EventDateEnd'] != '0000-00-00') { 
                                        echo ' - '. date('M j, Y', strtotime($row['EventDateEnd']));
                                      } 
                                      ?>
                                    </div>
                                    <div class="event-title"><a href="/events/<?php echo $row['E_Name_SEO'] ?>/<?php echo $row['EventID'] ?>"><?php echo $row['Title'] ?></a></div>
                                    <div class="event-desc"><?php echo ($row['ShortDesc'] != '') ? strip_tags($row['ShortDesc']) : ""; ?></div>
                                    <div class="event-more"><a href="/events/<?php echo $row['E_Name_SEO'] ?>/<?php echo $row['EventID'] ?>">More...</a></div>
                                </div>
                                <?PHP
                                if ($i == 2) {
                                    $i = 0;
                                    ?>
                                </div>
                                <?php
                            }
                            if ($totalEvents == $j && $i == 1) {
                                ?>
                            </div>
                            <?php
                        }
                        $i++;
                        $j++;
                    }
                    // display our pagination footer if set.
                    if (isset($pages)) {
                        ?>
                        <div class="event-pager">
                            <?php echo $pages->paginate(); ?>
                        </div>
                        <?php
                    }
                }else{
                    echo "No events yet.";
                }
                ?>
            </div>
            <div class="advertisement">
                <?php require_once 'advertise.php'; ?>
            </div>
        </div>
    </div>
</section>
<!--Whats on around town. end-->
<!--bar texture start-->
<div class="bar-texture"></div>
<!--bar texture end-->