// JavaScript Document
$(document).ready(function () {
    //Ajax Loader
    $(document).ajaxStart(function () {
        $('.custom-ajax').removeClass("custom-ajax").addClass('custom-ajax-show');
    }).ajaxStop(function () {
        $('.custom-ajax-show').removeClass("custom-ajax-show").addClass('custom-ajax');
    });
    $(".datepicker").datepicker({
        firstDay: 1,
        dateFormat: 'yy-mm-dd'
    });
    $(".previous-date-not-allowed").datepicker({
        minDate: 0,
        firstDay: 1,
        dateFormat: 'yy-mm-dd',
        prevText: "",
        nextText: ""
    });
    $(document).ready(function () {
        CKEDITOR.disableAutoInline = true; // Use CKEDITOR.replace() if element is <textarea>.
        $('.tt-ckeditor').ckeditor({
            customConfig: 'http://touristtowndemo.com/include/plugins/ckeditor/config.js'
        });

    });
    $(".button input[type='submit']").click(function () {
        $('.custom-ajax').removeClass("custom-ajax").addClass('custom-ajax-show');
        setTimeout(function () {
            $('.custom-ajax-show').removeClass("custom-ajax-show").addClass('custom-ajax');
        }, 2000);
    });
    $(".phone_number").mask("999-999-9999");

    $(".phone_toll").mask("9-999-999-9999");

    //suggested businesses
    $("#business_name").keyup(function () {
        var string = $("#business_name").val();
        if (string.length > 3) {
            $("#suggest-business").empty();
            $.ajax({
                url: "suggest_business.php",
                type: "POST",
                data: {
                    string: string
                },
                cache: false,
                global: false,
                success: function (html) {
                    if (html != '') {
                        $("#suggest-business").html(html);
                        $("#suggest-business").show();
                    } else {
                        $("#suggest-business").hide();
                    }
                }
            });
        } else {
            $("#suggest-business").hide();
            $("#suggest-business").empty();
        }
    });
});
function isClosed(myVar, myValue) {
    if (myValue == '00:00:01' || myValue == "00:00:02" || myValue == "00:00:03") {
        $("#" + myVar).hide();
        $("#" + myVar + " option:selected").prop("selected", false);
    } else {
        $("#" + myVar).show();
    }
}
function validatePassword(password, optional, password1)
{
    if (optional == true && password.length == 0 && password1.length == 0) {
        return true;
    } else {
        if (password.length < 6) {
            swal("Password!", "Password too short! At least 6 characters.", "warning");
            return false;
        }
        if (!password.match("[0-9]")) {
            swal("Password!", "Password must contain at least one digit!", "warning");
            return false;
        }
        if (!password.match("[A-Z]")) {
            swal("Password!", "Password must contain at least one uppercase letter!", "warning");
            return false;
        }
        if (!password.match("[a-z]")) {
            swal("Password!", "Password must contain at least one lowercase letter!", "warning");
            return false;
        }
        if (password != password1) {
            swal("Password!", "Password does not match!", "warning");
            return false;
        }

    }

}

var dialog_box = "";
function choose_file(dialog_id) {
    dialog_box = dialog_id;
    jQuery('.image_bank_input_file').val('');
    $(".dialog" + dialog_id).attr("title", "Choose Photo From").dialog({
        width: 500,
        height: 200,
        modal: true,
        draggable: false,
        resizable: false,
        open: function () {
            jQuery('.ui-widget-overlay').bind('click', function () {
                jQuery('#dialog' + dialog_id).dialog('close');
            });
        }
    });
}
function show_main_image(image_id) {
    $(".image-pop-up" + image_id).attr("title", "Image").dialog({
        width: 500,
        modal: true,
        draggable: false,
        resizable: false
    });
}
function show_image_library(image_id, bl_id, iteration) {
    var itr;
    if (iteration > 0) {
        itr = iteration;
    } else {
        itr = 0;
    }
    $.ajax({
        url: "show_image_bank.php",
        type: "GET",
        data: {
            image_id: image_id,
            bl_id: bl_id,
            iteration: itr
        },
        cache: false,
        success: function (html) {
            $("#image-library").html(html);
        }
    });
    var dWidth = $(window).width() * 0.95;
    var dHeight = $(window).height() * 0.95;
    jQuery('.dialog' + dialog_box).dialog('close');
    $("#image-library").attr("title", "Library").dialog({
        width: dWidth,
        height: dHeight,
        modal: true,
        draggable: false,
        resizable: false,
        open: function () {
            $("body").css("overflow", "hidden");
            jQuery('.ui-widget-overlay').bind('click', function () {
                jQuery('#image-library').dialog('close');
            });
        },
        close: function () {
            $('body').css('overflow', 'auto');
        }
    });
}

$(document).ready(function () {
    $('.dialog-close').on("click", function () {
        $(this).dialog("close");
    });
    //Getting name of the file when upload
    $('.setting-name').on('change', function (event, files, label) {
        var file_name = this.value.replace(/\\/g, '/').replace(/.*\//, '');
        $('#file-upload-name').text('');
        $('#file-upload-name').show();
        $('#file-upload-name').text(file_name);
    });
});
function select_image(id, image_id, img_bnk_name, iteration, IMG_URL) {
    jQuery('#image-library').dialog('close');
    var itr;
    if (iteration > 0) {
        itr = iteration;
    } else {
        itr = image_id;
    }
    //Empty browse and give library an image id
    $('#image_bank' + itr).val(id);
    $('#photo' + itr).val('');
    //show file name
    $('#image_name_bank' + itr).val('');
    $('#image_name_bank' + itr).show();
    $('#image_name_bank' + itr).val(img_bnk_name);

    // Show image preview
    $('.existing_imgs' + itr).hide();
    $('#photo' + itr).hide();
    $('.preview-img-script' + itr).show();
    $('.preview-img-script' + itr).attr('src', IMG_URL + img_bnk_name);

}


function select_multiple_image(id, image_id, iteration, img_bnk_name, IMG_URL) {
    jQuery('#image-library').dialog('close');
    var itr, idsArray;
    if (iteration > 0) {
        itr = iteration;
    } else {
        itr = image_id;
    }
    //Empty browse and give library an image id
    $('#image_bank' + itr).val(id);
    idsArray = id.split(",");
    if (idsArray.length > 1) {
        $('#uploadFile1').show();
        $('#preview').hide();
        document.getElementById("uploadFile1").value = "Multiple Images Selected";
    } else {
        //show file name
        $('#photo' + itr).val('');
        //show file name
        $('#image_name_bank' + itr).val('');
        $('#image_name_bank' + itr).show();
        $('#image_name_bank' + itr).val(img_bnk_name);
        // Show image preview
        $('#uploadFile1').hide();
        $('#preview').show();
        $('.existing_imgs' + itr).hide();
        $('#photo' + itr).hide();
        $('.preview-img-script' + itr).show();
        $('.preview-img-script' + itr).attr('src', IMG_URL + img_bnk_name);
        $('.preview-img-script' + itr).css('transform', '');
        $('.preview-img-script' + itr).css('max-width', '');
        $('.preview-img-script' + itr).css('max-height', '');
        $('.main-preview .preview-img-script' + itr).css('max-width', '');
        $('.main-preview .preview-img-script' + itr).css('max-height', '');
    }
}

function select_new_image(id, image_id, iteration) {
    jQuery('#image-library').dialog('close');
    var itr;
    if (iteration > 0) {
        itr = iteration;
    } else {
        itr = image_id;
    }
    $('#preview').hide();
    $('#uploadFile1').show();
    var inp = document.getElementById('photo' + itr);
    var imageVal = inp.files.length;
    if (imageVal == 1) {
        document.getElementById("uploadFile1").value = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '');
    } else
    {
        document.getElementById("uploadFile1").value = "Multiple Images Selected";
    }
    //Empty browse and give library an image id
    $('#photo' + itr).val(id);

    $('#image_bank' + itr).val('');

}

function show_file_name(image_id, input, iteration) {
    var file_name = input.value.replace(/\\/g, '/').replace(/.*\//, '');
    var id;
    if (iteration > 0) {
        id = iteration;
    } else {
        id = image_id;
    }
    if (file_name.length > 0) {
        //Empty library 
        $('#image_bank' + id).val('');
        //show file name
        $('#image_name_bank' + id).val('');
        $('#image_name_bank' + id).show();
        $('#image_name_bank' + id).val(file_name);
    }
    // Show image preview
    $('.existing_imgs' + id).hide();
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        $('#photo' + id).hide();
        $('.preview-img-script' + id).show();
        reader.onload = function (e) {
            $('.preview-img-script' + id).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function check_img_size(image_field, image_size) {
    var update = $('#update_' + image_field).val();
    var image_file = $("#photo" + image_field).val();
    if (image_file != '') {
//            var image_ext = image_file.split(".");
        var extesion_image = image_file.substr(image_file.lastIndexOf('.') + 1);
        var ext_check_image = extesion_image.toLowerCase();
        if (ext_check_image != 'jpg' && ext_check_image != 'jpeg' && ext_check_image != 'png' && ext_check_image != 'gif') {
            swal("Image!", "File Must be JPG, PNG, GIF!", "warning");
            $("#photo" + image_field).val('');
            $("#image_name_bank" + image_field).hide('');
            return false;
        }
    }
    if (($("#photo" + image_field))[0].files.length > 0) {
        var img_size = ($("#photo" + image_field))[0].files[0].size;
        if (img_size > image_size) {
            if (image_size == 3000000) {
                swal("Image!", "File Size must be less then 3MB!", "warning");
            } else {
                swal("Image!", "File Size must be less then 10MB!", "warning");
            }
            $("#photo" + image_field).val('');
            $("#image_name_bank" + image_field).hide('');
            return false;
        }
    } else if (($("#photo" + image_field))[0].files.length == 0) {
        if ($('#image_bank' + image_field).val() == "") {
            if (update != 1) {
                swal("Image!", "Please select image!", "warning");
                $("#photo" + image_field).val('');
                $("#image_name_bank" + image_field).hide('');
                return false;
            }
        }
    }
}

/*check email if exists*/
function checkin() {
    var check_email = $("#email_check_in").val();
    $.ajax({
        type: "POST",
        url: "email.php",
        data: {
            check_email: check_email
        }
    }).done(function (response) {
        if (response == 1)
        {
            $('#successSend').show();
            $('#errorSend').hide();
        } else {
            $('#errorSend').show();
            $('#successSend').hide();
        }
    });
    return false;
} 