<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/coupon.login.inc.mobile.php';
require_once '../../include/image-bank-usage-function.php';
require_once '../../include/track-data-entry.php';

$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    $sql = "SELECT BL_Listing_Title FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $delCouponImg = "DELETE tbl_Business_Feature_Coupon,tbl_Business_Feature_Coupon_Category_Multiple FROM tbl_Business_Feature_Coupon LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID WHERE BFC_ID = '" . encode_strings($_REQUEST['bfc_id'], $db) . "' AND BFC_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $resDelCouponImg = mysql_query($delCouponImg, $db) or die("Invalid query: $delCouponImg -- " . mysql_error());
    if ($resDelCouponImg) {
        $_SESSION['delete'] = 1;
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Coupon_Photo', $_REQUEST['bfc_id']);
        // TRACK DATA ENTRY
        $Coupon_id = $_REQUEST['bfc_id'];
        Track_Data_Entry('Listing',$BL_ID,'Manage Coupons',$Coupon_id,'Delete','mycoupon admin mobile');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location:manage-coupon.php?bl_id=" . $BL_ID);
    exit();
}
include '../../include/mycoupon/mobile/header.php';
?>

<div class="content-left">
    <div class="right">
        <div class="content-header">
            Manage Coupon
            <div class="link">
                <div class="add-link responsive-link">
                    <a href="add-manage-coupon.php?bl_id=<?php echo $BL_ID ?>">+ Add Coupon</a>
                </div>
            </div>
        </div>
        <div class='data-container' style="width: 90%;margin: 0 auto;">
            <div class='data-header'>
                <div class='data-header-coupon-title'>Coupon Name</div>
                <div class='data-header-coupon-option-no'>#Sent</div>
                <div class='data-header-coupon-option'>Edit</div>
                <div class='data-header-coupon-option'>Delete</div>
                <div class='data-header-coupon-option-status'>Status</div>
            </div>
            <?PHP
            $sql = "SELECT BFC_ID, BFC_Title, BFC_Status FROM tbl_Business_Feature_Coupon WHERE BFC_BL_ID = '$BL_ID'";
            $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($resultTMP)) {
                ?>
                <div class="data-content">
                    <div class="data-content-coupon-title">
                        <?php echo $row['BFC_Title'] ?>
                    </div>
                    <div class="data-content-coupon-option-no">
                        <?php
                        $sql_sent = "SELECT CU_ID, CU_Counter FROM tbl_Coupon_Usages WHERE CU_BFC_ID = '" . encode_strings($row['BFC_ID'], $db) . "' ORDER BY CU_Counter desc";
                        $result_count = mysql_query($sql_sent);
                        $row_count = mysql_fetch_assoc($result_count);
                        echo ($row_count['CU_Counter'] != '' ? $row_count['CU_Counter'] : '0');
                        ?>
                    </div>
                    <div class="data-content-coupon-option"><a href="add-manage-coupon.php?bl_id=<?php echo $BL_ID ?>&bfc_id=<?php echo $row['BFC_ID'] ?>">Edit</a></div>
                    <div class="data-content-coupon-option"><a onClick="return confirm('Are you sure?');" href="manage-coupon.php?bl_id=<?php echo $BL_ID ?>&bfc_id=<?php echo $row['BFC_ID'] ?>&op=del">Delete</a></div>
                    <div class="data-content-coupon-option-status"><?php echo $row['BFC_Status'] == 1 ? '<span class="approved">Approved</span>' : '<span class="pending">Pending</span>' ?></div>
                </div>
            <?PHP }
            ?>
        </div>
    </div>
</div>