<?php
require_once '../../include/config.inc.php';
include '../../include/mycoupon/mobile/header.php';

//check if email is available
$prepop = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
?>
<div class="login-outer-body">
    <div class="login-outer-container">
        <div class="login-container">
            <form name="form1" method="post" action="http://<?php echo COUPON_DOMAIN . BUSINESS_DIR ?>mobile/index.php">
            <input name="logop" type="hidden" id="logop" value="login">
                <div class="login-container-inside-div">
                            <div class="form-inside-div coupon-listing-title">
                                Sign In
                            </div>
                    <?php
                if (isset($_SESSION['login_error']) && $_SESSION['login_error'] == 1) {
                    ?>
                        <div class="login-from-inside-div margin-top-login-from">
                        <div class="invalid-user-warning">Your email or password doesn't match what we have on file. Please try again or contact <a href="mailto:info@touristtown.ca">Tourist Town</a> for help.</div>
                    </div>
                    <?php
                    unset($_SESSION['login_error']);
                }
                ?>

                <div class="login-from-inside-div margin-top-login-from">
                    <div class="from-inside-div-feild login-user-font-size" >
                    <input name="LOGIN_USER" id="LOGIN_USER" placeholder="Email" value="<?php echo $prepop ?>" type="text" size="35" required/>
                    </div>
                </div>

                <div class="login-from-inside-div margin-top-login-from">
                    <div class="from-inside-div-feild login-user-font-size" >
                    <input name="LOGIN_PASS" id="LOGIN_PASS" placeholder="Password" type="password" size="35" required/>
                    </div>
                </div>

                <div class="login-from-inside-div">
                    <div class="from-inside-div-button">
                        <input name="Submit" type="submit" value="Sign In" /></div>
                </div>
                 <div class="login-from-inside-div">
                     <div id="form-data-successfull-mobile-mgs" class="form-data">
                   <?php if (isset($_SESSION['change']) && $_SESSION['change'] != '') {
                            ?>
                            Your password has successfully changed.
                            <?php
                            }
                        unset($_SESSION['change']);
                        ?>  
                    </div>
                </div>
                <div class="login-from-inside-div">
                    <a href="http://<?php echo COUPON_DOMAIN . BUSINESS_DIR ?>mobile/index.php?logop=lost_pw">Click here if you have forgotten your password.</a>
                </div>


                    
                </div>
            </form>
        </div>

         <div class="login-container">
            
            <div class="login-container-inside-div">
             
                <div class="login-from-inside-div">
                    <div class="form-data">
                        Already have a Coupon Country account? <a href="<?php echo "http://" . COUPON_DOMAIN . BUSINESS_DIR ?>mobile/index.php"> Sign Up</a>
                    </div>
                </div>

                <div class="login-from-inside-div margin-top-login-from">
                    <div class="form-data">
                        Coupon Country is designed to serve businesses in Bruce County, Grey County and Huron County. If you are located in a community in one of these counties, you may already have a Tourist Town account that you can log in to Coupon County with.
                    </div>
                </div>

                <div class="login-from-inside-div margin-top-login-from">
                    <div class="form-data margin-top-login-from">
                        Please check here.
                    </div>
                </div>

                <div style="display: none;" id="successSend" class="reset-message-green"> You already have a touristtown account. </div>
            <div style="display: none;" id="errorSend" class="reset-message">There is no record of the email address that you have entered.</div>

                <form name="form1" method="post" action="" id="validation-form" onSubmit="return checkin();">

                    <div class="login-from-inside-div margin-top-login-from">
                        <div class="from-inside-div-feild login-user-font-size">
                            <input name="email" required="" size="25" placeholder="Email Address" type="email" id="email_check_in" class="email-reset">
                        </div>
                    </div>
                    <div class="login-from-inside-div">
                        <div class="from-inside-div-button">
                            <input type="submit" name="Submit" value="Check-in" >
                        </div>
                    </div>

                </form>
                


            </div>
        </div>

    </div>
</div>

<?php
// include '../../include/mycoupon/footer.php';
?>