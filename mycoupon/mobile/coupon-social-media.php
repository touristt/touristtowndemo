<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/adminFunctions.inc.php';
require_once '../../include/coupon.login.inc.mobile.php';
require_once '../../include/track-data-entry.php';

$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    $sql = "SELECT BL_Listing_Title FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}
if (isset($_POST['op']) && $_POST['op'] == 'save') {
    if ($_POST['counter'] > 0) {
        $sql = "UPDATE tbl_Business_Social SET 
                BS_FB_Link = '" . encode_strings($_POST['fb_link'], $db) . "',
                BS_T_Link = '" . encode_strings($_POST['t_link'], $db) . "',
                BS_I_Link = '" . encode_strings($_POST['i_link'], $db) . "'
                WHERE BS_BL_ID = '$BL_ID'";
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Social Media', '', 'Update', 'mycoupon admin mobile');
    } else {
        $sql = "INSERT INTO tbl_Business_Social(BS_BL_ID, BS_FB_Link, BS_T_Link, BS_I_Link) VALUES('" . encode_strings($BL_ID, $db) . "', '" . encode_strings($_POST['fb_link'], $db) . "', '" . encode_strings($_POST['t_link'], $db) . "', '" . encode_strings($_POST['i_link'], $db) . "')";
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Social Media', '', 'Add', 'mycoupon admin mobile');
    }
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: coupon-social-media.php?bl_id=" . $BL_ID);
    exit();
}
include '../../include/mycoupon/mobile/header.php';
?>
<div class="content-left">
    <div class="right">
        <div class="content-header">
            <?php echo $rowListing['BL_Listing_Title'] ?>
            <div class="instruction">
                Fields with this background<span></span>will show on free listings profile.
            </div>
        </div>
        <form name="form1" method="post" action="">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID; ?>">
            <input type="hidden" name="op" value="save">
            <?PHP
            $sql = "SELECT BS_FB_Link, BS_T_Link, BS_I_Link FROM tbl_Business_Social WHERE BS_BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $count = mysql_num_rows($result);
            $row = mysql_fetch_assoc($result);
            ?> 
            <div class="form-inside-div">
                <label>Facebook Link</label>
                <div class="form-data">
                    <input name="fb_link" type="text" value="<?php echo $row['BS_FB_Link'] ?>" size="50" />
                </div></div>

            <div class="form-inside-div">
                <label>Twitter Link</label>
                <div class="form-data">
                    <input name="t_link" type="text"  value="<?php echo $row['BS_T_Link'] ?>" size="50" />
                </div></div>

            <div class="form-inside-div">
                <label>Instagram Link</label>
                <div class="form-data">
                    <input name="i_link" type="text"  value="<?php echo $row['BS_I_Link'] ?>" size="50" />
                </div></div>

            <div class="form-inside-div">
                <div class="button">
                    <input type="hidden" name="counter" value="<?php echo $count; ?>">
                    <input type="submit" name="button2" value="Save Now"/>
                </div>
            </div>
        </form>
    </div>
</div>