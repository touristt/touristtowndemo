<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/coupon.login.inc.mobile.php';
include '../../include/mycoupon/mobile/header.php';
?>

<?php 
 $BL_ID = $_REQUEST['bl_id'];

?>
<div class="content-left">
    <div class="right">
        <div class="form-inside-div">
            <div class="menu-item-text-align-inside">
                <a href="manage-coupon.php?bl_id=<?php echo $BL_ID ?>" role="menuitem" tabindex="0">Manage Coupons</a>
            </div>
        </div>
        <div class="form-inside-div margin-top-login-from">
            <div class="menu-item-text-align-inside">
                <a href="coupon-listing-contact-details.php?bl_id=<?php echo $BL_ID ?>" role="menuitem" tabindex="0">Contact Details</a>
            </div>
        </div>
        <div class="form-inside-div margin-top-login-from">
            <div class="menu-item-text-align-inside">
                <a href="coupon-listing-mapping.php?bl_id=<?php echo $BL_ID ?>" role="menuitem" tabindex="0">Mapping</a>
            </div>
        </div>

        <div class="form-inside-div margin-top-login-from">
            <div class="menu-item-text-align-inside">
                <a href="coupon-hour.php?bl_id=<?php echo $BL_ID ?>" role="menuitem" tabindex="0">Hours</a>
            </div>
        </div>

        <div class="form-inside-div margin-top-login-from">
            <div class="menu-item-text-align-inside">
                <a href="coupon-social-media.php?bl_id=<?php echo $BL_ID ?>" role="menuitem" tabindex="0">Social Media</a>
            </div>
        </div>

        <div class="form-inside-div margin-top-login-from">
            <div class="menu-item-text-align-inside">
                <a href="listings.php" role="menuitem" tabindex="0">Home</a>
            </div>
        </div>

        <div class="form-inside-div margin-top-login-from">
            <div class="menu-item-text-align-inside">
                <?php if ($_SESSION['BUSINESS_ID']) { ?>
                    <a href="<?php echo "http://" . COUPON_DOMAIN . BUSINESS_DIR . "mobile/index.php?logop=logout" ?>">Log Out</a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?PHP
// include '../../include/mycoupon/footer.php';
?>