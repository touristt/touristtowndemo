<?php
require_once '../../include/config.inc.php';
include '../../include/mycoupon/mobile/header.php';
?>
<div class="login-outer-body">
    <div class="login-outer-container">
        <div class="login-container">
            <form name="form1" onSubmit="return validatePassword($('#LOGIN_PASS').val(), true, $('#LOGIN_PASS2').val())" method="post" action="http://<?php echo COUPON_DOMAIN . BUSINESS_DIR ?>mobile/">
                <input name="logop" type="hidden" id="logop" value="sign_up">
                <div class="login-container-inside-div">
                    
                 
                    <!-- <div class="login-detail-title-responsive">
                        <div class="signin-title-responsive-page">
                            <div class="login-detail-title">
                                Register
                            </div>
                        </div>
                    </div> -->
                    <script>
                function makeSEO(myVar) {
                    myVar = myVar.toLowerCase();
                    myVar = myVar.replace(/[^a-z0-9\s-]/gi, '');
                    myVar = myVar.replace(/"/g, '');
                    myVar = myVar.replace(/'/g, '');
                    myVar = myVar.replace(/&/g, '');
                    myVar = myVar.replace(/\//g, '');
                    myVar = myVar.replace(/,/g, '');
                    myVar = myVar.replace(/  /g, ' ');
                    myVar = myVar.replace(/ /g, '-');

                    $('#SEOname').val(myVar);
                    return false;
                }
            </script>
                    <div class="form-inside-div coupon-listing-title">
                        Register 
                    </div>
                      <?php
                    if (isset($_SESSION['mismatch']) && $_SESSION['mismatch'] == 1) {
                        ?>
                        <div class="login-from-inside-div">
                            <div class="invalid-user-warning">Password does not match.</div>
                        </div>
                        <?php
                        unset($_SESSION['mismatch']);
                    }
                    ?>
                    <?php
                    if (isset($_SESSION['exists_record']) && $_SESSION['exists_record'] == 1) {
                        ?>
                        <div class="login-from-inside-div">
                            <div class="invalid-user-warning">You already have an account with us, please <a href="http://mycoupon.touristtowndemo.com/mobile/login.php">Sign In</a> using this email address. Click <a href="http://mycoupon.touristtowndemo.com/mobile/lost-password.php">forgot password</a> if you have forgotten your password and a new one will be sent to you. <a href="mailto:info@couponcountry.ca">Contact us</a> if you need help.<br>
                            </div>
                        </div>
                        <?php
                        unset($_SESSION['exists_record']);
                    }
                    ?>
                    <div class="login-from-inside-div margin-top-login-from">
                        <div class="from-inside-div-feild login-user-font-size" >
                            <input name="business_name" id="business_name" placeholder="Business Name" type="text" value="<?php echo (isset($_REQUEST['business_name']) ? $_REQUEST['business_name'] : '') ?>" required onKeyUp="makeSEO(this.value)"/>
                        </div>
                        <input type="hidden" name="bname" id="SEOname" value="" onKeyUp="makeSEO(this.value)">
                        <div id="suggest-business" style="display:none;"></div>
                    </div>

                    <div class="login-from-inside-div margin-top-login-from">
                        <div class="from-inside-div-feild login-user-font-size">
                            <input name="LOGIN_USER" id="LOGIN_USER" placeholder="Email" type="email" value="<?php echo (isset($_REQUEST['LOGIN_USER']) ? $_REQUEST['LOGIN_USER'] : '') ?>" required/>
                        </div>
                    </div>

                    <div class="login-from-inside-div margin-top-login-from">
                        <div class="from-inside-div-feild login-user-font-size">
                            <input name="contact_name" placeholder="Contact Name" type="text" value="<?php echo (isset($_REQUEST['contact_name']) ? $_REQUEST['contact_name'] : '') ?>" required/>
                        </div>
                    </div>

                    <div class="login-from-inside-div margin-top-login-from">
                        <div class="from-inside-div-feild login-user-font-size">
                            <input name="address" placeholder="Street Address" type="text" value="<?php echo (isset($_REQUEST['address']) ? $_REQUEST['address'] : '') ?>" required/>
                        </div>
                    </div>

                    <div class="login-from-inside-div margin-top-login-from">
                        <div class="from-inside-div-feild login-user-font-size">
                            <input name="town" placeholder="Town" type="text" value="<?php echo (isset($_REQUEST['town']) ? $_REQUEST['town'] : '') ?>" required/>
                        </div>
                    </div>

                    <div class="login-from-inside-div margin-top-login-from">
                        <div class="from-inside-div-feild login-user-font-size">
                            <input name="LOGIN_PASS" id="LOGIN_PASS" placeholder="Password" type="password" required/>
                        </div>
                    </div>

                    <div class="login-from-inside-div margin-top-login-from">
                        <div class="from-inside-div-feild login-user-font-size">
                            <input name="CON_LOGIN_PASS" id="LOGIN_PASS2" placeholder="Confirm Password" type="password" required/>
                        </div>
                    </div>
                    
                    
                    
                    <div class="login-from-inside-div">
                        <div class="from-inside-div-button">
                            <input name="Submit" type="submit" value="Register Now" />
                        </div>
                    </div>
                    <div class="login-from-inside-div">
                    <label></label>
                    <div class="form-data">
                        <strong>Password must meet the following requirements:</strong>
                        <ul>
                            <li>At Least 6 Characters</li>
                            <li>Contain one Number</li>
                            <li>Contain one lowercase letter</li>
                            <li>Contain one uppercase letter</li>
                        </ul>
                    </div>
                </div>
                </div>
            </form>
        </div>

         <div class="login-container">
            
            <div class="login-container-inside-div">
                    
                <div class="login-from-inside-div">
                    <div class="form-data">
                        Already have a Coupon Country account? <a href="<?php echo "http://" . COUPON_DOMAIN . BUSINESS_DIR ?>mobile/login.php"> Sign In</a>
                    </div>
                </div>

                <div class="login-from-inside-div margin-top-login-from">
                    <div class="form-data">
                        Coupon Country is designed to serve businesses in Bruce County, Grey County and Huron County. If you are located in a community in one of these counties, you may already have a Tourist Town account that you can log in to Coupon County with.
                    </div>
                </div>

                <div class="login-from-inside-div margin-top-login-from">
                    <div class="form-data margin-top-login-from">
                        Please check here.
                    </div>
                </div>

                <div style="display: none;" id="successSend" class="reset-message-green"> You already have a touristtown account. </div>
            <div style="display: none;" id="errorSend" class="reset-message">There is no record of the email address that you have entered.</div>

                <form name="form1" method="post" action="" id="validation-form" onSubmit="return checkin();">

                    <div class="login-from-inside-div margin-top-login-from">
                        <div class="from-inside-div-feild login-user-font-size">
                            <input name="email" required="" size="25" placeholder="Email Address" type="email" id="email_check_in" class="email-reset">
                        </div>
                    </div>
                    <div class="login-from-inside-div">
                        <div class="from-inside-div-button">
                            <input type="submit" name="Submit" value="Check-in" >
                        </div>
                    </div>

                </form>
            
                <!-- <div id="successSend" class="reset-message-green"> You already have a touristtown account. </div>
                <div id="errorSend" class="reset-message">There is no record of the email address that you have entered.</div> -->
                


            </div>
        </div>

    </div>
</div>
<?php
// include '../../include/mycoupon/footer.php';
?>