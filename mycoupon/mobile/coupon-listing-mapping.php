<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/adminFunctions.inc.php';
require_once '../../include/coupon.login.inc.mobile.php';
require_once '../../include/track-data-entry.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];
$points_taken = 0;

if ($BL_ID > 0) {
    $sql = "SELECT BL_Listing_Title, BL_Street, BL_Town, BL_Province,BL_Country, BL_PostalCode, BL_Lat, BL_Long 
            FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}

if ($_POST['op'] == 'save') {
    $sql = "tbl_Business_Listing SET 
            BL_Street = '" . encode_strings($_REQUEST['address'], $db) . "', 
            BL_Town = '" . encode_strings($_REQUEST['town'], $db) . "', 
            BL_Province = '" . encode_strings($_REQUEST['province'], $db) . "',
            BL_Country = '" . encode_strings($_REQUEST['country'], $db) . "', 
            BL_PostalCode = '" . encode_strings($_REQUEST['postalcode'], $db) . "',
            BL_Lat = '" . encode_strings($_REQUEST['latitude'], $db) . "', 
            BL_Long = '" . encode_strings($_REQUEST['longitude'], $db) . "'";

    if ($_REQUEST['latitude'] != '' && $_REQUEST['longitude']) {
        $sql .= ", BL_Location = POINT(" . $_REQUEST['latitude'] . ", " . $_REQUEST['longitude'] . ")";
    }

    if ($BL_ID > 0) {
        $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Location & Mapping', '', 'Update', 'mycoupon admin mobile');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $id, 'Location & Mapping', '', 'Add', 'user admin mobile');
    }
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: coupon-listing-mapping.php?bl_id=" . $BL_ID);
    exit();
}
include '../../include/mycoupon/mobile/header.php';
?>
<div class="content-left">
    <div class="right">
        <div class="content-header">
            <?php echo $rowListing['BL_Listing_Title'] ?>
            <div class="instruction">
                Fields with this background<span></span>will show on free listings profile.
            </div>
        </div>
        <form name="form1" method="post" action="coupon-listing-mapping.php" onsubmit="return checkVal()">
            <input type="hidden" name="bl_id" value="<?= $BL_ID ?>">
            <input type="hidden" name="op" value="save">

            <div class="form-inside-div">
                <label>Street Address</label>
                <div class="form-data">
                    <input class="instructional-background" name="address" type="text" id="address" value="<?php echo $rowListing['BL_Street'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div">
                <label>Town</label>
                <div class="form-data">
                    <input class="instructional-background" name="town" type="text" id="town" value="<?php echo $rowListing['BL_Town'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div">
                <label>Province</label>
                <div class="form-data">
                    <input class="instructional-background" name="province" type="text" id="province" value="<?php echo $rowListing['BL_Province'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div">
                <label>Postal Code</label>
                <div class="form-data">
                    <input class="instructional-background" name="postalcode" type="text" id="postalcode" value="<?php echo $rowListing['BL_PostalCode'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div">
                <label>Country</label>
                <div class="form-data">
                    <input name="country" type="text" id="country" value="<?php echo $rowListing['BL_Country'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div">
                <div class="content-sub-header">
                    <div class="title">Google Maps</div>
                </div>
                <?php
                $help_text = show_help_text('Google Maps');
                if ($help_text != '') {
                    echo '<div class="gmap-left">' . $help_text . '</div>';
                }
                ?>
            </div>
            <div class="form-inside-div">
                <label>Latitude</label>
                <div class="form-data">
                    <input name="latitude" type="text" id="latitude" value="<?php echo $rowListing['BL_Lat'] ?>" size="10" />
                </div></div>

            <div class="form-inside-div">
                <label>Longitude</label>
                <div class="form-data">
                    <input name="longitude" type="text" id="longitude" value="<?php echo $rowListing['BL_Long'] ?>" size="10" />
                </div></div>

            <div class="form-inside-div">
                <div class="button">
                    <input type="submit" name="button2" value="Save Now"/>
                </div>
            </div> 
            <!-- </div> -->
        </form>
    </div>

</div>
<?php
// include '../include/mycoupon/footer.php';
?>
<script type="text/javascript">
    function checkVal() {
        var latitudeVal = $("#latitude").val();
        var longitudeVal = $("#longitude").val();
        if (latitudeVal != '' && longitudeVal == '') {
            alert('Longitude must be entered.');
            $("#longitude").focus();
            return false;
        }
        if (latitudeVal == '' && longitudeVal != '') {
            alert('Latitude must be entered.');
            $("#longitude").focus();
            return false;
        }
    }

    $("input#latitude").blur(function (event) {
        var regex = /^-?([0-8]?[0-9]|90)\.[0-9]{1,6}$/;
        var lat = $("#latitude").val();
        if (lat != '' && !regex.test(lat)) {
            alert("Invalid Latitude");
            $(this).val('');
        }
    });
    $("input#longitude").blur(function (event) {
        var regex = /^-?((1?[0-7]?|[0-9]?)[0-9]|180)\.[0-9]{1,6}$/;
        var lon = $("#longitude").val();
        if (lon != '' && !regex.test(lon)) {
            alert("Invalid Longitude");
            $(this).val('');
        }
    });
</script>
