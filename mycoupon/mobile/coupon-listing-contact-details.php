<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/adminFunctions.inc.php';
require_once '../../include/coupon.login.inc.mobile.php';
require_once '../../include/track-data-entry.php';

$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    $sql = "SELECT BL_Listing_Title, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website
            FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}

$sqlRegions = "SELECT BLCR_BLC_R_ID FROM tbl_Business_Listing_Category_Region WHERE BLCR_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
$resultRegions = mysql_query($sqlRegions, $db) or die("Invalid query: $sqlRegions -- " . mysql_error());
$temp = 0;
while ($rowRegions = mysql_fetch_assoc($resultRegions)) {
    $sqlRegion = "SELECT R_Show_Hide_Email FROM tbl_Region WHERE R_ID = '" . encode_strings($rowRegions['BLCR_BLC_R_ID'], $db) . "'";
    $resultRegion = mysql_query($sqlRegion, $db) or die("Invalid query: $sqlRegion -- " . mysql_error());
    $rowRegion = mysql_fetch_assoc($resultRegion);
    if ($rowRegion['R_Show_Hide_Email'] == 1) {
        $temp = 1;
    }
}

if ($_POST['op'] == 'save') {
    $sql = "tbl_Business_Listing SET 
            BL_Listing_Title = '" . encode_strings($_REQUEST['name'], $db) . "',  
            BL_Contact = '" . encode_strings($_REQUEST['contact'], $db) . "', 
            BL_Phone = '" . encode_strings($_REQUEST['phone'], $db) . "', 
            BL_Toll_Free = '" . encode_strings($_REQUEST['tollFree'], $db) . "', 
            BL_Cell = '" . encode_strings($_REQUEST['cell'], $db) . "', 
            BL_Fax = '" . encode_strings($_REQUEST['fax'], $db) . "', 
            BL_Email = '" . encode_strings($_REQUEST['email'], $db) . "', 
            BL_Website = '" . encode_strings($_REQUEST['website'], $db) . "'";
    if ($BL_ID > 0) {
        $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Contact Details', '', 'Update', 'mycoupon admin mobile');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $id, 'Contact Details', '', 'Add', 'mycoupon admin mobile');
    }
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: coupon-listing-contact-details.php?bl_id=" . $BL_ID);
    exit();
}
include '../../include/mycoupon/mobile/header.php';
?>
<div class="content-left">
    <div class="right">

    <div class="content-header">
        <?php echo $rowListing['BL_Listing_Title'] ?>
        <div class="instruction">
            Fields with this background<span></span>will show on free listings profile.
        </div>
    </div>

    <form name="form1" method="post">
        <input type="hidden" name="bl_id" value="<?php echo $BL_ID; ?>">
        <input type="hidden" name="op" value="save">

        <div class="form-inside-div">
            <label>Business Title</label>
            <div class="form-data">
                <input class="instructional-background" name="name" type="text" id="BLname" value="<?php echo $rowListing['BL_Listing_Title'] ?>" size="50"  required/>
            </div>
        </div>

        <div class="form-inside-div">
            <label>Contact Name</label>
            <div class="form-data">
                <input name="contact" type="text" value="<?php echo $rowListing['BL_Contact'] ?>" size="50" />
            </div>
        </div>

        <div class="form-inside-div">
            <label>Phone</label>
            <div class="form-data">
                <input class="instructional-background" name="phone" class="phone_number" type="text" value="<?php echo $rowListing['BL_Phone'] ?>" size="50" />
            </div>
        </div>


        <div class="form-inside-div">
            <label>Toll Free</label>
            <div class="form-data">
                <input name="tollFree" class="phone_toll" type="text" value="<?php echo $rowListing['BL_Toll_Free'] ?>" size="50" />
            </div>
        </div>


        <div class="form-inside-div">
            <label>Cell</label>
            <div class="form-data">
                <input name="cell" class="phone_number" type="text" value="<?php echo $rowListing['BL_Cell'] ?>" size="50" />
            </div>
        </div>

        <div class="form-inside-div">
            <label>Fax</label>
            <div class="form-data">
                <input name="fax" class="phone_number" type="text" value="<?php echo $rowListing['BL_Fax'] ?>" size="50" />
            </div>
        </div>

        <div class="form-inside-div">
            <label>Email</label>
            <div class="form-data">
                <input class="<?php echo ($temp == 1) ? 'instructional-background' : ''; ?>" name="email" type="email" value="<?php echo $rowListing['BL_Email'] ?>" size="50" />
            </div>
        </div>

        <div class="form-inside-div">
            <label>Website</label>
            <div class="form-data">
                <input name="website" type="text" value="<?php echo $rowListing['BL_Website'] ?>" size="50" />
            </div>
        </div>

        <div class="form-inside-div">
            <div class="button">
                <input type="submit" name="button2" value="Save Now"/>
            </div>
        </div>
        <!--  </div> -->
    </form>
</div>

</div>
<?php
// include '../include/mycoupon/footer.php';
?>