<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/coupon.login.inc.mobile.php';
include '../../include/mycoupon/mobile/header.php';
?>
<div class="content-left">
    <div class="right">
        <div class="form-inside-div">
            <div class="menu-item-text-align-inside">
                <a href="listings.php" role="menuitem" tabindex="0">Home</a>
            </div>
        </div>
        <div class="form-inside-div margin-top-login-from">
            <div class="menu-item-text-align-inside">
                <a href="login-details.php" role="menuitem" tabindex="0">Login Detail</a>
            </div>
        </div>
        <div class="form-inside-div margin-top-login-from">
            <div class="menu-item-text-align-inside">
                <a href="agreement.php" role="menuitem" tabindex="0">Terms and Conditions</a>
            </div>
        </div>

        <div class="form-inside-div margin-top-login-from">
            <div class="menu-item-text-align-inside">
                <?php if ($_SESSION['BUSINESS_ID']) { ?>
                    <a href="<?php echo "http://" . COUPON_DOMAIN . BUSINESS_DIR . "mobile/index.php?logop=logout" ?>">Log Out</a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?PHP
// include '../../include/mycoupon/footer.php';
?>