<?PHP
require_once '../include/PHPMailer/class.phpmailer.php';
require_once '../include/config.inc.php';
require_once '../include/coupon.login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';


$BL_ID = $_REQUEST['bl_id'];
$BFC_ID = $_REQUEST['bfc_id'];
$BID = $_SESSION['BUSINESS_ID'];
if ($BL_ID > 0) {
    $sql = "SELECT BL_Listing_Title FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
    $sql = "SELECT B_ID, B_Email FROM tbl_Business WHERE B_ID = '$BID' LIMIT 1";
    $result_coupon = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row_coupon = mysql_fetch_assoc($result_coupon);
} else {
    header('Location: index.php');
}
if ($BFC_ID > 0) {
    $getCoupon = "SELECT * FROM tbl_Business_Feature_Coupon WHERE BFC_ID = '" . encode_strings($BFC_ID, $db) . "'";
    $resCoupon = mysql_query($getCoupon, $db) or die("Invalid query: $getCoupon -- " . mysql_error());
    $actCoupon = mysql_fetch_assoc($resCoupon);
}
if (isset($_POST['submit']) && $_POST['submit'] !== '') {
    $expire_date = ($_POST['expire_date'] != '') ? $_POST['expire_date'] : '0000-00-00';
    $coupons = "tbl_Business_Feature_Coupon SET 
                BFC_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                BFC_Title = '" . encode_strings($_POST['name'], $db) . "',
                BFC_Description = '" . encode_strings($_POST['deal_description'], $db) . "',
                BFC_Terms_Conditions = '" . encode_strings($_POST['deal_terms'], $db) . "',
                BFC_Expiry_Date = '" . $expire_date . "'";
    require_once '../include/picUpload.inc.php';
    if ($_POST['image_bank'] == "") {
        // last @param 29 = Coupon image
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 29, 'Listing', $BL_ID);
        if (is_array($pic)) {
            $coupons .= ", BFC_Thumbnail = '" . encode_strings($pic['0']['1'], $db) . "',
                        BFC_Main_Image = '" . encode_strings($pic['0']['0'], $db) . "'";
        }
        $pic_id = $pic['1'];
    } else {
        $pic_id = $_POST['image_bank'];
        $ext = explode(".", $pic_id);
        $myLink = mt_rand(1000, 9999999);
        // last @param 29 = Coupon image
        $pic = Upload_Pic_Library($pic_id, 29);
        if (is_array($pic)) {
            $coupons .= ", BFC_Thumbnail = '" . encode_strings($pic['1'], $db) . "',
                     BFC_Main_Image = '" . encode_strings($pic['0'], $db) . "'";
        }
    }
    if ($BFC_ID > 0) {
        $couponQuery = "UPDATE $coupons WHERE BFC_ID = '" . encode_strings($BFC_ID, $db) . "'";
        $resCoupon = mysql_query($couponQuery, $db) or die("Invalid query: $couponQuery -- " . mysql_error());
        $coupon_id = $BFC_ID;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Add Coupon', $BFC_ID, 'Update', 'mycoupon admin');
    } else {
        $couponQuery = "INSERT $coupons";
        $resCoupon = mysql_query($couponQuery, $db) or die("Invalid query: $couponQuery -- " . mysql_error());
        $coupon_id = mysql_insert_id();
        $notification_email = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Add Coupon', $coupon_id, 'Add', 'mycoupon admin');
    }
    if ($resCoupon) {
        $_SESSION['success'] = 1;
        if ($pic_id > 0) {
            //Updating Image Bank table because we cannot insert coupon id through picUpload function.
            //If we create new coupon then we do not have the BFC_ID to insert in image bank table
            $sql_image_bank = "UPDATE tbl_Image_Bank SET IB_Coupons = '" . encode_strings($coupon_id, $db) . "' WHERE IB_ID = '" . encode_strings($pic_id, $db) . "'";
            mysql_query($sql_image_bank) or die("Invalid query: $sql_image_bank -- " . mysql_error());
            //Image usage from image bank//
            imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Coupon_Photo', $coupon_id);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    $sql_delete = "DELETE FROM tbl_Business_Feature_Coupon_Category_Multiple where BFCCM_BFC_ID = '" . encode_strings($coupon_id, $db) . "'";
    mysql_query($sql_delete);
    for ($i = 1; $i < $_POST['total_cat'] + 1; $i++) {
        $C_cat_id = $_POST['c_coupon_' . $i];
        if ($C_cat_id > 0) {
            $query_insert = "INSERT tbl_Business_Feature_Coupon_Category_Multiple SET 
                        BFCCM_BFC_ID = '" . encode_strings($coupon_id, $db) . "',
                        BFCCM_C_ID ='" . encode_strings($C_cat_id, $db) . "'";
            $resCoupon_insert = mysql_query($query_insert, $db) or die("Invalid query: $query_insert -- " . mysql_error());
        }
        $total_cat_vall[] = $C_cat_id;
    }
    /////ADD COUPON NOTIFIATIOON EMAIL 24 HOURS.
    if ($notification_email = 1) {
        $to = $row_coupon['B_Email'];
        //$to = "imtiazork@gmail.com";
        ob_start();
        include '../include/email-template/coupon-notification-email.php';
        $html = ob_get_contents();
        ob_clean();
        $mail = new PHPMailer();
        $mail->IsMail();
        $mail->From = MAIN_CONTACT_EMAIL;
        $mail->FromName = MAIN_CONTACT_NAME;
        $mail->IsHTML(true);
        $mail->AddAddress($to);
        $mail->CharSet = 'UTF-8';
        $mail->Subject = 'Coupon Notification Email';
        $mail->MsgHTML($html);
        //$mail->Send();
    }
    header("Location:manage-coupon.php?bl_id=" . $BL_ID);
    exit();
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $delCouponImg = "UPDATE tbl_Business_Feature_Coupon SET BFC_Thumbnail = '', BFC_Main_Image = '' WHERE BFC_ID = '" . encode_strings($BFC_ID, $db) . "'";
    $resDelCouponImg = mysql_query($delCouponImg, $db) or die("Invalid query: $delCouponImg -- " . mysql_error());
    if ($resDelCouponImg) {
        $_SESSION['delete'] = 1;
        //Updating Image Bank table because we cannot insert coupon id through picUpload function.
        //If we create new coupon then we do not have the BFC_ID to insert in image bank table
        $sql_image_bank = "UPDATE tbl_Image_Bank SET IB_Coupons = '' WHERE IB_ID = '" . encode_strings($pic_id, $db) . "'";
        mysql_query($sql_image_bank) or die("Invalid query: $sql_image_bank -- " . mysql_error());
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Coupon_Photo', $BFC_ID);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Add Coupon', $BFC_ID, 'Delete Image', 'mycoupon admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location:add-manage-coupon.php?bl_id=" . $BL_ID . "&bfc_id=" . $BFC_ID);
    exit();
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del_cat') {
    $bfccm_id = $_REQUEST['bfccm_id'];
    $qry = "Delete from tbl_Business_Feature_Coupon_Category_Multiple where BFCCM_ID = '" . encode_strings($bfccm_id, $db) . "' ";
    $resCoupon_delete = mysql_query($qry, $db) or die("Invalid query: $qry -- " . mysql_error());
    if ($resCoupon_delete) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Add Coupon', $BFC_ID, 'Delete Category', 'mycoupon admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location:add-manage-coupon.php?bl_id=" . $BL_ID . "&bfc_id=" . $BFC_ID);
    exit();
}
if ($BFC_ID) {
    $sql_category = "SELECT * FROM tbl_Business_Feature_Coupon_Category_Multiple WHERE BFCCM_BFC_ID = $BFC_ID ORDER BY BFCCM_ID ASC  ";
    $result_category = mysql_query($sql_category);
    while ($row1 = mysql_fetch_array($result_category)) {
        $totVal[] = $row1['BFCCM_C_ID'];
        $outputlastVal = implode(", ", $totVal);
    }
    $cat_coupon = array();
    $sql_cat = "SELECT C_ID, C_Name_SEO, C_Name FROM tbl_Category 
            WHERE C_Parent = (SELECT C_ID FROM tbl_Category WHERE C_Is_Product_Web = 1) AND C_ID NOT IN($outputlastVal)
            ORDER BY C_Name";
    $result_cat = mysql_query($sql_cat);
    while ($row = mysql_fetch_array($result_cat)) {
        $cat_coupon[] = $row;
    }
} else {
    $sql_category = "SELECT * FROM tbl_Business_Feature_Coupon_Category_Multiple ORDER BY BFCCM_ID ASC  ";
    $result_category = mysql_query($sql_category);
    while ($row1 = mysql_fetch_array($result_category)) {
        $totVal[] = $row1['BFCCM_C_ID'];
        $outputlastVal = implode(", ", $totVal);
    }
    $cat_coupon = array();
    $sql_cat = "SELECT C_ID, C_Name_SEO, C_Name FROM tbl_Category 
            WHERE C_Parent = (SELECT C_ID FROM tbl_Category WHERE C_Is_Product_Web = 1) ORDER BY C_Name";
    $result_cat = mysql_query($sql_cat);
    while ($row = mysql_fetch_array($result_cat)) {
        $cat_coupon[] = $row;
    }
}
$cat_coupon = json_encode($cat_coupon);
include '../include/mycoupon/header.php';
?>
<div class="main-container">
    <div class="coupon-listing-title">
        <?php echo $row['BL_Listing_Title'] ?>
    </div>
    <div class="main-container-body">
        <div class="left">
            <?PHP require '../include/nav-B-mypage-coupon.php'; ?>
        </div>
        <div class="right">
            <form name="form1" onSubmit="return check_img_size(28, 10000000)" enctype="multipart/form-data" method="post">
                <input type="hidden" name="bl_id" value="<?php echo $BL_ID; ?>">
                <input type="hidden" name="bfc_id" value="<?php echo $BFC_ID; ?>">
                <input type="hidden" name="op" value="save">

                <div class="form-inside-div">
                    <label>Coupon Title</label>
                    <div class="form-data">
                        <input name="name" type="text" id="BLname" value="<?php echo $actCoupon['BFC_Title']; ?>" maxlength="37" required/>
                    </div>
                </div>
                <div id="more_cat">
                    <?PHP
                    $sql_category = "SELECT * FROM tbl_Business_Feature_Coupon_Category_Multiple WHERE BFCCM_BFC_ID = $BFC_ID ORDER BY BFCCM_ID ASC  ";
                    $result_category = mysql_query($sql_category);
                    $categVal = mysql_query($sql_category);
                    $count_category = mysql_num_rows($result_category);
                    if ($count_category != 0) {
                        $i = 1;
                        while ($rows = mysql_fetch_array($categVal)) {
                            $valS[] = $rows['BFCCM_C_ID'];
                            $sizearray = implode(", ", $valS);
                        }
                        $first = reset($valS);
                        $last = end($valS);
                        $secound = $first . ',' . $last;
                        $output1 = array_slice($valS, 1, 2);
                        $firestVal = implode(", ", $output1);
                        $secoundVal = $first . ',' . $last;
                        $output3 = array_slice($valS, 0, -1);
                        $thirdVal = implode(", ", $output3);
                        $sizearrays = sizeof($valS);
                        while ($row1 = mysql_fetch_array($result_category)) {
                            ?>
                            <?php if ($i == 1) { ?>
                                <input type="hidden" name="total_cat" id="check-cat" value="<?php echo $count_category != 0 ? $count_category : 1 ?>">
                            <?php } ?>
                            <div class="form-inside-div">
                                <label>Category <?php echo $i ?></label>
                                <div class="form-data">
                                    <select name="c_coupon_<?php echo $i ?>" <?php echo ($i == 1) ? 'required' : '' ?>>
                                        <option value="">Select Category</option>
                                        <?php
                                        if ($i == 1) {
                                            if ($sizearrays == 1) {
                                                $catVal = '';
                                            } else {
                                                $catVal = " AND C_ID NOT IN($firestVal)";
                                            }
                                        } elseif ($i == 2) {
                                            if ($sizearrays == 2) {
                                                $catVal = " AND C_ID NOT IN($first)";
                                            } else {
                                                $catVal = " AND C_ID NOT IN($secoundVal)";
                                            }
                                        } elseif ($i == 3) {
                                            $catVal = " AND C_ID NOT IN($thirdVal)";
                                        }
                                        $sql_cat_coupon = "SELECT C_ID, C_Name_SEO, C_Name FROM tbl_Category
                                                            WHERE C_Parent = (SELECT C_ID FROM tbl_Category WHERE C_Is_Product_Web = 1) 
                                                            $catVal ORDER BY C_Name";
                                        $result_cat_coupon = mysql_query($sql_cat_coupon);
                                        while ($row = mysql_fetch_array($result_cat_coupon)) {
                                            ?>
                                            <option  <?php echo ($row1['BFCCM_C_ID'] == $row['C_ID']) ? "selected" : "" ?> value="<?php echo $row['C_ID'] ?>"><?php echo $row['C_Name'] ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <?php if ($i == 1 && $count_category != 3) { ?>
                                        <a class="add_categories" onclick="add_more_cat()">+Add Category</a>
                                    <?php } else if ($i != 1) {
                                        ?>
                                        <a onClick="return confirm('Are you sure?');" class="delete_categories" href="add-manage-coupon.php?bl_id=<?php echo $BL_ID ?>&bfc_id=<?php echo $BFC_ID ?>&op=del_cat&bfccm_id=<?php echo $row1['BFCCM_ID'] ?>">Delete</a>
                                    <?php }
                                    ?>
                                </div>
                            </div>           
                            <?php
                            $i++;
                        }
                    } else {
                        ?>
                        <input type="hidden" name="total_cat" id="check-cat" value="1">
                        <div class="form-inside-div">
                            <label>Category 1</label>
                            <div class="form-data">
                                <select name="c_coupon_1" required>
                                    <option value="">Select Category</option>
                                    <?php
                                    $sql_cat_coupon = "SELECT C_ID, C_Name_SEO, C_Name FROM tbl_Category
                                                        WHERE C_Parent = (SELECT C_ID FROM tbl_Category WHERE C_Is_Product_Web = 1) 
                                                        ORDER BY C_Name";
                                    $result_cat_coupon = mysql_query($sql_cat_coupon);
                                    while ($row = mysql_fetch_array($result_cat_coupon)) {
                                        ?>
                                        <option value="<?php echo $row['C_ID'] ?>"><?php echo $row['C_Name'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <a class="add_categories" onclick="add_more_cat()">+Add Category</a>
                            </div>
                        </div> 
                    <?php }
                    ?>
                </div>
                <div class="form-inside-div">
                    <label>Deal Description</label>
                    <div class="form-data">
                        <textarea class="tt-ckeditor" name="deal_description"><?php echo $actCoupon['BFC_Description']; ?></textarea>
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>Terms & Conditions</label>
                    <div class="form-data">
                        <textarea class="tt-ckeditor" name="deal_terms"><?php echo $actCoupon['BFC_Terms_Conditions']; ?></textarea>
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>Expiry Date</label>
                    <div class="form-data">
                        <input class="previous-date-not-allowed" type="text" name="expire_date"  value="<?php echo $actCoupon['BFC_Expiry_Date']; ?>"/>
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>Photo</label>
                    <div class="form-data">
                        <span class="select-photo" onclick="show_image_library(28, <?= $BL_ID; ?>, <?php echo $BL_ID ?>)">Select File</span>
                        <?php if ($actCoupon['BFC_Thumbnail'] != '' && $actCoupon['BFC_Main_Image']) { ?>
                            <span class="delete-photo"><a onClick="return confirm('Are you sure?');" href="add-manage-coupon.php?bl_id=<?php echo $BL_ID ?>&bfc_id=<?php echo $BFC_ID ?>&op=del&coupons=editcoupon">Delete Photo</a></span>
                        <?php } ?>
                        <input type="file" name="pic[]" onchange="show_file_name(28, this, <?php echo $BL_ID ?>)" id="photo<?php echo $BL_ID ?>" style="display: none;">
                        <input type="hidden" name="image_bank" class="image_bank" id="image_bank<?php echo $BL_ID ?>" value="">   
                        <div class="cropit-image-preview">
                            <img class="preview-img preview-img-script<?php echo $BL_ID ?>" style="display: none;" src="">  
                            <?php if ($actCoupon['BFC_Thumbnail'] != '') { ?>
                                <input type="hidden" id="update_<?= $BL_ID; ?>" value="1"/>
                                <img class="existing-img existing_imgs<?php echo $BL_ID ?>" src="http://<?php echo DOMAIN . IMG_LOC_REL . $actCoupon['BFC_Thumbnail'] ?>" >
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="form-inside-div">
                    <div class="button">
                        <input type="submit" name="submit" value="Save Now"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="image-library" style="display:none;"></div>
</div>
<script type="text/javascript">
    var next_count;
    function DeleteVal(id) {
        $("#lab_num_" + next_count).text('Category ' + id);
        $("#category_num_" + id).remove();
        var count = $("#check-cat").val();
        next_count = --count;
        $("#check-cat").val(next_count);
        $('#cat_coupon' + 3).attr('name', 'c_coupon_2');
        $('#cat_coupon' + 3).attr('class', 'cat_coupon_2');
        $('#cat_coupon' + 3).attr('id', 'cat_coupon2');
        $("#lab_num_" + 3).text('Category 2');
        $('#del_' + 3).attr("onclick", "DeleteVal(2)");
        $('#del_' + 3).attr('id', 'del_2');
        $('#category_num_' + 3).attr('id', 'category_num_2');
    }
    function add_more_cat()
    {
        var count = $("#check-cat").val();
        var cat_coupons =<?php echo $cat_coupon ?>;
        next_count = ++count;
        if (count < 4) {

            $("#more_cat").append('<div id="category_num_' + next_count + '"  class="form-inside-div DeleteValuse"><label id="lab_num_' + next_count + '">Category ' + next_count + '</label><div class="form-data"><select name="c_coupon_' + next_count + '" class="cat_coupon_' + next_count + '" id="cat_coupon' + next_count + '"><option value="">Select Category</option></select><span class="delete_categoriess" id="del_' + next_count + '" onClick="DeleteVal(' + next_count + ')">Delete</span></div></div>');
            $.each(cat_coupons, function (index, cat) {
                $("#cat_coupon" + next_count).append('<option value="' + cat.C_ID + '">' + cat.C_Name + '</option>');
            });
            $("#check-cat").val(next_count);
        } else
        {
            alert("You can select only 3 catgeories.", "warning");
        }
    }
</script>
<?PHP
include '../include/mycoupon/footer.php';
?>