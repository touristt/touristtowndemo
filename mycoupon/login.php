<?php
require_once '../include/config.inc.php';
include '../include/mycoupon/header.php';

//check if email is available
$prepop = isset($_REQUEST['email']) ? $_REQUEST['email'] : '';
?>
<div class="sign-container">
    <div class="login-container">
        <form name="form1" method="post" action="http://<?php echo COUPON_DOMAIN . BUSINESS_DIR ?>">
            <input name="logop" type="hidden" id="logop" value="login">
            <div class="login-container-inside-div">

                <div class="login-detail-title">
                    Sign In
                </div>
                <?php
                if (isset($_SESSION['login_error']) && $_SESSION['login_error'] == 1) {
                    ?>
                    <div class="login-from-inside-div">
                        <div class="invalid-user-warning">Your email or password doesn't match what we have on file. Please try again or contact <a href="mailto:info@touristtown.ca">Tourist Town</a> for help.</div>
                    </div>
                    <?php
                    unset($_SESSION['login_error']);
                }
                ?>
                <div class="login-from-inside-div">
                    <input name="LOGIN_USER" id="LOGIN_USER" placeholder="Email" value="<?php echo $prepop ?>" type="text" size="35" required/>
                </div>
                <div class="login-from-inside-div">
                    <input name="LOGIN_PASS" id="LOGIN_PASS" placeholder="Password" type="password" size="35" required/>
                </div>

                <div class="login-from-inside-div">
                    <div class="from-inside-div-button">
                        <input name="Submit" type="submit" value="Sign In" /></div>
                </div>
                <div class="login-from-inside-div">

                    <a href="http://<?php echo COUPON_DOMAIN . BUSINESS_DIR ?>?logop=lost_pw">Click here if you have forgotten your password.</a>
                </div>
            </div>
        </form>
    </div>
    <div class="Sign-in-up-msg">

        <div class="msg-body-container">
            <?php if (isset($_SESSION['change']) && $_SESSION['change'] != '') {
                ?>
                Your password has successfully changed.
                <?php
            }
            unset($_SESSION['change']);
            if (isset($_SESSION['link_expired']) && $_SESSION['link_expired'] == 1) {
                ?>
                <div class="login-from-inside-div">
                    <div class="invalid-user-warning">This link has expired or, already been used. <br></div>
                </div>
                <?php
            } unset($_SESSION['link_expired']);
            ?>
            <div class="message-body">Don't have a Coupon Country account? <a href="http://<?php echo COUPON_DOMAIN . BUSINESS_DIR ?>"> Sign up</a></div>
            <div class="message-body">Coupon Country is designed to serve businesses in Bruce County, Grey County and Huron County. If you are located in a community in one of these counties, you may already have a Tourist Town account that you can log in to Coupon County with.</div>
            <div class="message-body">
                Please check here.
            </div>
            <div id="successSend" class="reset-message-green"> You already have a touristtown account. </div>
            <div id="errorSend" class="reset-message">There is no record of the email address that you have entered.</div>

            <div class="login-container-reset" id="forget-form">
                <form name="form1" method="post" action="" id="validation-form" onSubmit="return checkin();">

                    <div class="login-from-inside-div reset">
                        <input name="email" required="" size="25" placeholder="Email Address" type="email" id="email_check_in" class="email-reset">
                    </div>

                    <div class="frombutton">
                        <input type="submit" name="Submit" value="check-in" >
                    </div>

                </form>
            </div>
            <div class="message-body-logo">
                <img src="http://<?php echo DOMAIN ?>/images/TouristTown-Logo.gif" alt="Tourist Town" width="195" height="43" hspace="20" vspace="20" border="0" />
            </div>    
        </div>    
    </div> 
</div>
<?php
include '../include/mycoupon/footer.php';
?>