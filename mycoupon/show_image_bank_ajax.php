<?PHP

require_once '../include/config.inc.php';
define("DOMAIN", 'touristtowndemo.com');
echo '<script type="text/javascript">maintain_selection()</script>';
$where = 'WHERE 1 = 1';
$bl_id = '';
$search_text = '';
$regionFilter = '';
$tempregionFilter = '';
$seasonFilter = '';
$catFilter = '';
$peopleFilter = '';
$ownerFilter = '';
$campaignFilter = '';
if (isset($_GET['bl_id']) && $_GET['bl_id'] != '') {
    $bl_id = $_GET['bl_id'];
    $where .= " AND FIND_IN_SET($bl_id, IB_Listings)";
}
if (isset($_GET['season']) && $_GET['season'] != '') {
    $seasonFilter = $_GET['season'];
    $where .= " AND IB_Season IN ($seasonFilter)";
}
if (isset($_GET['cat']) && $_GET['cat'] != '') {
    $catFilter = $_GET['cat'];
    $where .= " AND IB_Category IN ($catFilter)";
}
if (isset($_GET['people']) && $_GET['people'] != '') {
    $peopleFilter = $_GET['people'];
    $where .= " AND IB_People IN ($peopleFilter)";
}
if (isset($_GET['campaign']) && $_GET['campaign'] != '') {
    $campaignFilter = $_GET['campaign'];
    $where .= " AND IB_Campaign IN ($campaignFilter)";
}
if ($_GET['search_image']) {
    $search_text = explode(',', $_GET['search_image']);
    $search_word_len = count($search_text);
    foreach ($search_text as $key => $temp) {
        if ($key == 0) {
            $operation = 'AND';
        } else {
            $operation = 'OR';
        }
        if ($count > 0) {
            if ($key == 0) {
                $where .= " AND (";
                $OR = "";
            } else {
                $OR = " OR ";
            }
            $where .= " $OR (IB_Keyword LIKE '%" . mysql_real_escape_string($temp) . "%' OR IB_Name LIKE '%" . mysql_real_escape_string($temp) . "%' OR BL_Listing_Title LIKE '%" . mysql_real_escape_string($temp) . "%')";
            if ($key + 1 == $search_word_len) {
                $where .= ")";
            }
        } else {
            $where .= " $operation (IB_Keyword LIKE '%" . mysql_real_escape_string($temp) . "%' OR IB_Name LIKE '%" . mysql_real_escape_string($temp) . "%' OR BL_Listing_Title LIKE '%" . mysql_real_escape_string($temp) . "%')";
        }
    }
}
$sql = "SELECT IB_ID, IB_Thumbnail_Path, IB_Path, IB_Dimension FROM tbl_Image_Bank LEFT JOIN tbl_Image_Bank_Usage ON IBU_IB_ID = IB_ID
        LEFT JOIN tbl_Business_Listing ON BL_ID = IBU_BL_ID
        $where GROUP BY IB_ID ORDER BY IB_ID DESC";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$count = mysql_num_rows($result);

if ($count > 0) {
    while ($row = mysql_fetch_array($result)) {
        $image_id = (isset($_REQUEST['image_id']) ? $_REQUEST['image_id'] : "0");
        $content .= '<div id="bgcolor_' . $row['IB_ID'] . '" onclick="select_the_image(`' . $row['IB_Path'] . '`,`' . $row['IB_ID'] . '`)" class="image-hover-bank image-bank-image-section">
                     <div class="image-section-image-align">
                     <img src="http://' . DOMAIN . IMG_BANK_REL . $row['IB_Thumbnail_Path'] . '">
                     </div>
                     <div class="image-section-text-align">' . $row['IB_ID'] . ' - ' . $row['IB_Dimension'] . '</div>
                     </div>';
    }
} else {
    $content .= '<div class="no-image-found">No images found in image bank.</div>';
}
print $content;
