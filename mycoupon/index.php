<?PHP
require_once '../include/config.inc.php';
require_once '../include/coupon.login.inc.php';

$BID = $_SESSION['BUSINESS_ID'];

include '../include/mycoupon/header.php';
?>
<div class="main-container">
    <div class="coupon-listing-title">
        My Listings
    </div>
    <div class="main-container-body">
        <div class="left">
            <?PHP require '../include/nav-B-coupon.php'; ?>
        </div>
        <div class="right">
            <div class='data-container'>
                <div class='data-header'>
                    <div class='data-header-title'>Listing Name</div>
                    <div class='data-header-option'>View</div>
                </div>
                <?PHP
                $sql = "SELECT BL_ID, BL_Listing_Title FROM tbl_Business_Listing WHERE BL_B_ID = '$BID'";
                $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                while ($row = mysql_fetch_assoc($resultTMP)) {
                    ?>
                    <div class="data-content">
                        <div class="data-content-title">
                            <?php echo $row['BL_Listing_Title'] ?>
                        </div>
                        <div class="data-content-option"><a href="manage-coupon.php?bl_id=<?php echo $row['BL_ID'] ?>">View</a></div>
                    </div>
                <?PHP }
                ?>
            </div>
        </div>
    </div>
</div>
<?php 
include '../include/mycoupon/footer.php'; 
?>