<?php
require_once '../include/PHPMailer/class.phpmailer.php';
require_once '../include/config.inc.php';

$hash = md5(rand(0, 1000));
//////Email template 
$email = "SELECT * FROM tbl_Email_Templates WHERE ET_ID = 13";
$result_data = mysql_query($email);
$rowEmail = mysql_fetch_assoc($result_data);
$email_subject = $rowEmail['ET_Subject'];
$email_heading = $rowEmail['ET_Name'];
$email_body = $rowEmail['ET_Template'];
$email_footer = $rowEmail['ET_Footer'];
///
  $new_Val_hef = '<p style="margin-bottom: 15px; word-spacing: 1px; margin-top: 13px; font-family: IdealSans-Light-Pro; text-align: center; font-size: 15px; font-weight: bold; margin: 0; Margin-bottom: 15px; word-spacing: 5px;  text-align: justify;text-align:center;">
                        <a href="http://'. COUPON_DOMAIN .'/change-password.php?resetpassword=' .$hash. '" style="word-spacing: 0px;text-align: center; display:inline-block;text-decoration:none;background:#EB5424;border-radius:3px;color:white;font-family:Avenir Next, Avenir, sans-serif;font-size:14px;font-weight:500;line-height:35px;padding:10px 25px;margin:0px;" target="_blank">
                            RESET YOUR PASSWORD</a></p>'; 
 $old_Val_hef = '<p><a href="http://touristtowndemo.com" target="_blank">RESET YOUR PASSWORD</a></p>';
 $message_body = str_replace($old_Val_hef, $new_Val_hef, $rowEmail['ET_Template']); 

if (isset($_SESSION['B_Email']) && $_SESSION['B_Email'] !== '') {
    $email = $_SESSION['B_Email'];
    include('include/email-template.php');
    $to = $email;
    $html = ob_get_contents(); 
    ob_clean();
    $mail = new PHPMailer();
    $mail->From = COUPON_CONTACT_EMAIL;
    $mail->FromName = COUPON_CONTACT_NAME;
    $mail->IsHTML(true);
    $mail->AddReplyTo(COUPON_CONTACT_EMAIL, COUPON_CONTACT_NAME);
    $mail->AddAddress($to);
    $mail->Subject = $email_subject;
    $mail->Sender = $to;
    $mail->MsgHTML($html);
    $mail->CharSet = 'UTF-8';
    $mail->Body = $msg;
    $emailSent = $mail->Send();

    $update_tbl_bussniss = "Update tbl_Business SET B_Reset_Key='" . $hash . "' WHERE B_ID = '" . $_SESSION['B_ID'] . "'";
    mysql_query($update_tbl_bussniss, $db);
    unset($_SESSION['B_Email']);
    unset($_SESSION['B_ID']);
}

include '../include/mycoupon/header.php';
?>
<div class="sign-container">
    <div class='login-detail-change-password'>
        Reset Password
    </div>
    <div class="resect-password">
        <?php
        if (isset($_SESSION['success_forget']) && $_SESSION['success_forget'] !== '') {
            ?>
            <p class="success">
                You have requested to change your password and will receive an email link shortly. If you do not receive an email, please check your spam box or contact us at
                <a href="mailto:info@touristtown.ca.">info@touristtown.ca.</a> 
            </p>

            <p class="success">Tourist Town Team</p>
            <?php
            unset($_SESSION['success_forget']);
        } elseif (isset($_SESSION['error_forget']) && $_SESSION['error_forget'] !== '') {
            ?>
            <p>
                There is no record of the email address that you have entered. Please contact Tourist Town to speak to a support representative.                  
            </p>
            <p>Free Phone Support: 519.378.3096</p>
            <p><a href="mailto:info@touristtown.ca.">info@touristtown.ca.</a></p>

            <p>Tourist Town is the web platform that supports your local Tourism website.</p>
            <?php
            unset($_SESSION['error_forget']);
        } else {
            ?>
            <script type="text/javascript">
                window.location.href = 'login.php';
            </script>
            <?php
        }
        ?>

    </div>
</div>
</div>    
</div> 
</div>
<?php
include '../include/mycoupon/footer.php';
?>
