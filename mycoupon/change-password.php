<?php
require_once '../include/config.inc.php';

$key_val = $_REQUEST['resetpassword'];

$select_query = "SELECT B_ID, B_Email, B_Reset_Key FROM tbl_Business WHERE B_Reset_Key = '" . $key_val . "'";
$select_pass = mysql_query($select_query) or die(mysql_error());
$reset_key_value = mysql_fetch_assoc($select_pass);

if (isset($reset_key_value['B_Reset_Key']) && $reset_key_value['B_Reset_Key'] !== '') {
    include '../include/mycoupon/header.php';
    ?>
    <div class="login-container">
        <form name="form1" method="post" action="http://<?php echo COUPON_DOMAIN . BUSINESS_DIR; ?>" onSubmit="return validatePassword($('#LOGIN_PASS').val(), true, $('#LOGIN_PASS2').val())">
            <div class="forget-pass-container-inside-div">
                <input name="logop" type="hidden" id="logop" value="change_password">
                <input name="regain_pass" type="hidden"  value="<?php echo $_REQUEST['resetpassword']; ?>">

                <div class="login-from-inside-div">
                    <div class="signin-title-responsive-page">
                        Change Password
                    </div>
                </div>
                <?php
                if (isset($_SESSION['mismatch_regain_password']) && $_SESSION['mismatch_regain_password'] == 1) {
                    ?>
                    <div class="login-from-inside-div">
                        <div>Password Mismatch<br>
                            </a></div>
                    </div>
                    <?php
                    unset($_SESSION['mismatch_regain_password']);
                }
                if (isset($_SESSION['error_regain_password']) && $_SESSION['error_regain_password'] == 1) {
                    ?>
                    <div class="login-from-inside-div">
                        <div>Error saving data. Please try again.<br>
                            </a></div>
                    </div>
                    <?php
                    unset($_SESSION['error_regain_password']);
                }
                ?>
                <div class="login-from-inside-div forget">
                    <div class='login-from-inside-div change'>
                        <input name='LOGIN_PASS' id='LOGIN_PASS' placeholder='Password' type='password' required/>
                    </div>
                    <div class='login-from-inside-div change'>
                        <input name='CON_LOGIN_PASS' id='LOGIN_PASS2' placeholder='Confirm Password' type='password' required/>
                    </div>
                </div>

                <div class="login-from-inside-div">
                    <div class="from-inside-div-label" ></div>
                    <div class="from-inside-div-button">
                        <input type="submit" name="Submit" value="Submit">
                    </div>
                </div>
            </div>
        </form>
        <?php
    } else {
        $_SESSION['link_expired'] = 1;
        header("Location: " . BUSINESS_DIR . "login.php");
        exit();
    }
    ?>
</div>
<?php 
include '../include/mycoupon/footer.php'; 
?>