<?PHP
require_once '../include/config.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/coupon.login.inc.php';
require_once '../include/track-data-entry.php';
require_once '../include/image-bank-usage-function.php';
$preview_page = 1;
$coupon_preview =1;
$BL_ID = $_REQUEST['bl_id'];
$BFC_ID = $_REQUEST['bfc_id'];

if ($BL_ID > 0) {
    $sql = "SELECT BL_Listing_Title FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
} else {
    header('Location: index.php');
}
if ($BFC_ID > 0) {
    $getCoupon = "SELECT * FROM tbl_Business_Feature_Coupon WHERE BFC_ID = '" . encode_strings($BFC_ID, $db) . "'";
    $resCoupon = mysql_query($getCoupon, $db) or die("Invalid query: $getCoupon -- " . mysql_error());
    $actCoupon = mysql_fetch_assoc($resCoupon);// echo '<pre>'; print_r($actCoupon);
}
if($BFC_ID){
  $sql_category = "SELECT * FROM tbl_Business_Feature_Coupon_Category_Multiple WHERE BFCCM_BFC_ID = $BFC_ID ORDER BY BFCCM_ID ASC  "; 
        $result_category = mysql_query($sql_category);
            while ($row1 = mysql_fetch_array($result_category)) { 
       $totVal[] =$row1['BFCCM_C_ID']; 
       $outputlastVal = implode (", ", $totVal);
      } 
      $cat_coupon = array();
   $sql_cat = "SELECT C_ID, C_Name_SEO, C_Name FROM tbl_Category 
            WHERE C_Parent = (SELECT C_ID FROM tbl_Category WHERE C_Is_Product_Web = 1) AND C_ID NOT IN($outputlastVal)
            ORDER BY C_Name";
$result_cat = mysql_query($sql_cat);
while ($row = mysql_fetch_array($result_cat)) {
    $cat_coupon[] = $row;
 
}
}else{
  $sql_category = "SELECT * FROM tbl_Business_Feature_Coupon_Category_Multiple ORDER BY BFCCM_ID ASC  ";
        $result_category = mysql_query($sql_category);
            while ($row1 = mysql_fetch_array($result_category)) { 
       $totVal[] =$row1['BFCCM_C_ID']; 
       $outputlastVal = implode (", ", $totVal);
      } 
      $cat_coupon = array();
   $sql_cat = "SELECT C_ID, C_Name_SEO, C_Name FROM tbl_Category 
            WHERE C_Parent = (SELECT C_ID FROM tbl_Category WHERE C_Is_Product_Web = 1) ORDER BY C_Name";
$result_cat = mysql_query($sql_cat);
while ($row = mysql_fetch_array($result_cat)) {
    $cat_coupon[] = $row;
 
} 
}
$cat_coupon = json_encode($cat_coupon);
include '../include/mycoupon/header.php';
///// Coupon Save 
if (isset($_POST['op']) && $_POST['op'] == 'save_coupon') { //echo '<pre>'; print_r($_POST); exit;
    ob_start();
    $BL_ID  = $_REQUEST['bl_id'];
    $BFC_ID = $_REQUEST['bfc_id'];
    $expire_date = ($_POST['expire_date'] != '') ? $_POST['expire_date'] : '0000-00-00';
    $coupons = "tbl_Business_Feature_Coupon SET 
                BFC_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                BFC_Title = '" . encode_strings($_POST['name'], $db) . "',
                BFC_Description = '" . encode_strings($_POST['deal_description'], $db) . "',
                BFC_Terms_Conditions = '" . encode_strings($_POST['deal_terms'], $db) . "',
                BFC_Expiry_Date = '" . $expire_date . "'";
    require_once '../include/picUpload.inc.php';
    if ($_POST['image_bank'] == "") {
        // last @param 29 = Coupon image
        $pic1 = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 29, 'Listing', $BL_ID);
        
        if (is_array($pic1)) {
            $coupons .= ", BFC_Thumbnail = '" . encode_strings($pic1['0']['1'], $db) . "',
                        BFC_Main_Image = '" . encode_strings($pic1['0']['0'], $db) . "'";
        }
        $pic_id = $pic1['1'];
    } else {
        $pic_id = $_POST['image_bank'];
        $ext = explode(".", $pic_id);
        $myLink = mt_rand(1000, 9999999);
        // last @param 29 = Coupon image
        $pic2 = Upload_Pic_Library($pic_id, 29);
        if (is_array($pic2)) {
            $coupons .= ", BFC_Thumbnail = '" . encode_strings($pic2['1'], $db) . "',
                     BFC_Main_Image = '" . encode_strings($pic2['0'], $db) . "'";
        }
    }
    if ($BFC_ID > 0) {
        $couponQuery = "UPDATE $coupons WHERE BFC_ID = '" . encode_strings($BFC_ID, $db) . "'";
        $resCoupon = mysql_query($couponQuery, $db) or die("Invalid query: $couponQuery -- " . mysql_error());
        $coupon_id = $BFC_ID;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Add Coupon', $BFC_ID, 'Update', 'mycoupon admin');
    } else {
        $couponQuery = "INSERT $coupons";
        $resCoupon = mysql_query($couponQuery, $db) or die("Invalid query: $couponQuery -- " . mysql_error());
        $coupon_id = mysql_insert_id();
         $firbase =1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Add Coupon', $coupon_id, 'Add', 'mycoupon admin');
    }
      if ($resCoupon) {
        $_SESSION['success'] = 1;
        if ($pic_id > 0) {
            //Updating Image Bank table because we cannot insert coupon id through picUpload function.
            //If we create new coupon then we do not have the BFC_ID to insert in image bank table
            $sql_image_bank = "UPDATE tbl_Image_Bank SET IB_Coupons = '" . encode_strings($coupon_id, $db) . "' WHERE IB_ID = '" . encode_strings($pic_id, $db) . "'";
            mysql_query($sql_image_bank) or die("Invalid query: $sql_image_bank -- " . mysql_error());
            //Image usage from image bank//
            imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Coupon_Photo', $coupon_id);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    $sql_delete = "DELETE FROM tbl_Business_Feature_Coupon_Category_Multiple where BFCCM_BFC_ID = '" . encode_strings($coupon_id, $db) . "'";
    mysql_query($sql_delete);
    for ($i = 1; $i < $_POST['total_cat'] + 1; $i++) {
        $C_cat_id = $_POST['c_coupon_' . $i];
        if($C_cat_id > 0) {
            $query_insert = "INSERT tbl_Business_Feature_Coupon_Category_Multiple SET 
                        BFCCM_BFC_ID = '" . encode_strings($coupon_id, $db) . "',
                        BFCCM_C_ID ='" . encode_strings($C_cat_id, $db) . "'";
            $resCoupon_insert = mysql_query($query_insert, $db) or die("Invalid query: $query_insert -- " . mysql_error());
        }
    }
    
    
//     header("Location:add-manage-coupons.php?bl_id=".$BL_ID."&bfc_id=".$BFC_ID);
//     exit();
    
}
///// image delete coupon
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') { 
    
     $BL_ID  = $_REQUEST['bl_id'];
    $BFC_ID = $_REQUEST['bfc_id'];
    $pic_id = $_POST['image_bank'];//echo 'asd'. $pic_id; exit;
    $delCouponImg = "UPDATE tbl_Business_Feature_Coupon SET BFC_Thumbnail = '', BFC_Main_Image = '' WHERE BFC_ID = '" . encode_strings($BFC_ID, $db) . "'";
    $resDelCouponImg = mysql_query($delCouponImg, $db) or die("Invalid query: $delCouponImg -- " . mysql_error());
    if ($resDelCouponImg) {
        $_SESSION['delete'] = 1;
        //Updating Image Bank table because we cannot insert coupon id through picUpload function.
        //If we create new coupon then we do not have the BFC_ID to insert in image bank table
      $sql_image_bank = "UPDATE tbl_Image_Bank SET IB_Coupons = '' WHERE IB_ID = '" . encode_strings($BL_ID, $db) . "'";//exit;
        mysql_query($sql_image_bank) or die("Invalid query: $sql_image_bank -- " . mysql_error());
        //Delete from image usage when image is deleted
       // imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Coupon_Photo', $BFC_ID);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Add Coupon', $BFC_ID, 'Delete Image', 'mycoupon admin');
    } else {
        $_SESSION['delete_error'] = 1;
    } 
     header("Location:add-manage-coupons.php?bl_id=" . $BL_ID . "&bfc_id=" . $BFC_ID);
         
}
//// Delete Category
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del_cat') {
	$bfccm_id = $_REQUEST['bfccm_id']; 
    $BFC_ID = $_REQUEST['bfc_id'];
    $BL_ID = $_REQUEST['bl_id']; 
    $qry = "Delete from tbl_Business_Feature_Coupon_Category_Multiple where BFCCM_ID = '" . encode_strings($bfccm_id, $db) . "' ";
    $resCoupon_delete = mysql_query($qry, $db) or die("Invalid query: $qry -- " . mysql_error());
    if ($resCoupon_delete) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Add Coupon', $BFC_ID, 'Delete Category', 'mycoupon admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    // header("Location:add-manage-coupons.php?bl_id=" . $BL_ID . "&bfc_id=" . $BFC_ID );
     
}
//// contact datials
if (isset($_POST['op']) && $_POST['op'] == 'save_contact_details') {
	$BL_ID  = $_REQUEST['bl_id'];
	$BFC_ID = $_REQUEST['bfc_id'];
    $sql = "tbl_Business_Listing SET 
            BL_Listing_Title = '" . encode_strings($_REQUEST['name'], $db) . "',  
            BL_Contact = '" . encode_strings($_REQUEST['contact'], $db) . "', 
            BL_Phone = '" . encode_strings($_REQUEST['phone'], $db) . "', 
            BL_Toll_Free = '" . encode_strings($_REQUEST['tollFree'], $db) . "', 
            BL_Cell = '" . encode_strings($_REQUEST['cell'], $db) . "', 
            BL_Fax = '" . encode_strings($_REQUEST['fax'], $db) . "', 
            BL_Email = '" . encode_strings($_REQUEST['email'], $db) . "', 
            BL_Website = '" . encode_strings($_REQUEST['website'], $db) . "'";
    if ($BL_ID > 0) {
        $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Contact Details','','Update','mycoupon admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing',$id,'Contact Details','','Add','mycoupon admin');
    }
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
        
   // header("Location:add-manage-coupons.php?bl_id=" . $BL_ID . "&bfc_id=" . $BFC_ID );
      
}
//// mapping form
if (isset($_POST['op']) && $_POST['op'] == 'save_cutomer_listing_mapping') {
	$BL_ID  = $_REQUEST['bl_id'];
	$BFC_ID = $_REQUEST['bfc_id'];
    $sql = "tbl_Business_Listing SET 
            BL_Street = '" . encode_strings($_REQUEST['address'], $db) . "', 
            BL_Town = '" . encode_strings($_REQUEST['town'], $db) . "', 
            BL_Province = '" . encode_strings($_REQUEST['province'], $db) . "',
            BL_Country = '" . encode_strings($_REQUEST['country'], $db) . "', 
            BL_PostalCode = '" . encode_strings($_REQUEST['postalcode'], $db) . "',
            BL_Lat = '" . encode_strings($_REQUEST['latitude'], $db) . "', 
            BL_Long = '" . encode_strings($_REQUEST['longitude'], $db) . "'";
    if ($_REQUEST['latitude'] != '' && $_REQUEST['longitude']) {
        $sql_map .= ", BL_Location = POINT(" . $_REQUEST['latitude'] . ", " . $_REQUEST['longitude'] . ")";
    }


    if ($BL_ID > 0) {
      $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";//exit;
        $result1 = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Location & Mapping','','Update','mycoupon admin');
    } else {
        $sql = "INSERT " . $sql;
        $result1 = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing',$id,'Location & Mapping','','Add','user admin');
    }
    if ($result1) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
     header("Location:add-manage-coupons.php?bl_id=" . $BL_ID . "&bfc_id=" . $BFC_ID );
       
}
///Hours form
if ($_POST['op'] == 'save_hours') {
    $hDisabled = $_REQUEST['hDisabled'] != '' ? 1 : 0;
    $happointment = $_REQUEST['happointment'] != '' ? 1 : 0;
    $sql = "tbl_Business_Listing SET  
            BL_Hours_Disabled = '" . encode_strings($hDisabled, $db) . "', 
            BL_Hours_Appointment = '" . encode_strings($happointment, $db) . "', 
            BL_Hour_Mon_From = '" . encode_strings($_REQUEST['monFrom'], $db) . "', 
            BL_Hour_Mon_To = '" . encode_strings($_REQUEST['monTo'], $db) . "', 
            BL_Hour_Tue_From = '" . encode_strings($_REQUEST['tueFrom'], $db) . "', 
            BL_Hour_Tue_To = '" . encode_strings($_REQUEST['tueTo'], $db) . "', 
            BL_Hour_Wed_From = '" . encode_strings($_REQUEST['wedFrom'], $db) . "', 
            BL_Hour_Wed_To = '" . encode_strings($_REQUEST['wedTo'], $db) . "', 
            BL_Hour_Thu_From = '" . encode_strings($_REQUEST['thuFrom'], $db) . "', 
            BL_Hour_Thu_To = '" . encode_strings($_REQUEST['thuTo'], $db) . "', 
            BL_Hour_Fri_From = '" . encode_strings($_REQUEST['friFrom'], $db) . "', 
            BL_Hour_Fri_To = '" . encode_strings($_REQUEST['friTo'], $db) . "', 
            BL_Hour_Sat_From = '" . encode_strings($_REQUEST['satFrom'], $db) . "', 
            BL_Hour_Sat_To = '" . encode_strings($_REQUEST['satTo'], $db) . "', 
            BL_Hour_Sun_From = '" . encode_strings($_REQUEST['sunFrom'], $db) . "', 
            BL_Hour_Sun_To = '" . encode_strings($_REQUEST['sunTo'], $db) . "'";
    $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . $BL_ID . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Hours', '', 'Update', 'mycoupon admin');
    } else {
        $_SESSION['error'] = 1;
    }
    
}
/*social media*/
if (isset($_POST['op']) && $_POST['op'] == 'save_social_media') {
    if ($_POST['counter'] > 0) {
        $sql = "UPDATE tbl_Business_Social SET 
                BS_FB_Link = '" . encode_strings($_POST['fb_link'], $db) . "',
                BS_T_Link = '" . encode_strings($_POST['t_link'], $db) . "',
                BS_I_Link = '" . encode_strings($_POST['i_link'], $db) . "'
                WHERE BS_BL_ID = '$BL_ID'";
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Social Media','','Update','mycoupon admin');
    } else {
        $sql = "INSERT INTO tbl_Business_Social(BS_BL_ID, BS_FB_Link, BS_T_Link, BS_I_Link) VALUES('" . encode_strings($BL_ID, $db) . "', '" . encode_strings($_POST['fb_link'], $db) . "', '" . encode_strings($_POST['t_link'], $db) . "', '" . encode_strings($_POST['i_link'], $db) . "')";
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Social Media','','Add','mycoupon admin');
    }
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }    
}
?>
<div class="main-container">

     <?PHP require 'coupons-preview.php'; ?>
     
    <div id="image-library" style="display:none;"></div>
</div>
<!-- Coupon Form -->
<div class="right">
    <?php 
    $BL_ID = $_REQUEST['bl_id'];
    $BFC_ID = $_REQUEST['bfc_id'];
    ?>
    <form name="form1" class="storyForm" id="existing-coupon<?php echo $BFC_ID ?>" onSubmit="return check_img_size(<?php echo $BL_ID ?>, 10000000)" enctype="multipart/form-data" method="post" style="display: none;">
        <input type="hidden" name="bl_id" value="<?php echo $BL_ID; ?>">
        <input type="hidden" name="bfc_id" value="<?php echo $BFC_ID; ?>">
        <input type="hidden" name="op" value="save_coupon">

        <div class="form-inside-div">
            <label>Coupon Title</label>
            <div class="form-data">
                <input name="name" type="text" id="BLname" value="<?php echo $actCoupon['BFC_Title']; ?>" maxlength="37" required/>
            </div>
        </div>
                <div id="more_cat">
                    <?PHP
                    $sql_category = "SELECT * FROM tbl_Business_Feature_Coupon_Category_Multiple WHERE BFCCM_BFC_ID = $BFC_ID ORDER BY BFCCM_ID ASC  ";
                    $result_category = mysql_query($sql_category);
                    $categVal = mysql_query($sql_category);
                    $count_category = mysql_num_rows($result_category);
                    if ($count_category != 0) {
                        $i = 1;
                        while($rows= mysql_fetch_array($categVal)){
                  $valS[]  =$rows['BFCCM_C_ID'];
                  $sizearray = implode (", ", $valS);
                } 
                $first = reset($valS);
                $last = end($valS);
                $secound = $first . ','. $last;
                $output1 = array_slice($valS, 1, 2);$firestVal = implode (", ", $output1);
                $secoundVal = $first . ','. $last;
                $output3 = array_slice($valS, 0, -1);$thirdVal = implode (", ", $output3);
                $sizearrays = sizeof($valS);
                        while ($row1 = mysql_fetch_array($result_category)) {
                            ?>
                            <?php if ($i == 1) { ?>
                                <input type="hidden" name="total_cat" id="check-cat" value="<?php echo $count_category != 0 ? $count_category : 1 ?>">
                            <?php } ?>
                            <div class="form-inside-div">
                                <label>Category <?php echo $i ?></label>
                                <div class="form-data">
                                    <select name="c_coupon_<?php echo $i ?>" <?php echo ($i == 1) ? 'required' : '' ?>>
                                        <option value="">Select Category</option>
                                        <?php
                                        if($i ==1){ 
                                        if($sizearrays == 1){
                                            $catVal ='';
                                        }else{
                                           $catVal =" AND C_ID NOT IN($firestVal)";
                                        }
                                     }elseif($i ==2){
                                         if($sizearrays == 2){
                                             $catVal =" AND C_ID NOT IN($first)";
                                         }else{
                                        $catVal =" AND C_ID NOT IN($secoundVal)"; 
                                         }
                                     }elseif($i ==3){
                                        $catVal =" AND C_ID NOT IN($thirdVal)"; 
                                     }
                                        $sql_cat_coupon = "SELECT C_ID, C_Name_SEO, C_Name FROM tbl_Category
                                                            WHERE C_Parent = (SELECT C_ID FROM tbl_Category WHERE C_Is_Product_Web = 1) 
                                                            $catVal ORDER BY C_Name";
                                        $result_cat_coupon = mysql_query($sql_cat_coupon);
                                        while ($row = mysql_fetch_array($result_cat_coupon)) {
                                            ?>
                                            <option  <?php echo ($row1['BFCCM_C_ID'] == $row['C_ID']) ? "selected" : "" ?> value="<?php echo $row['C_ID'] ?>"><?php echo $row['C_Name'] ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <?php if ($i == 1 && $count_category != 3) { ?>
                                    <a class="add_categories" onclick="add_more_cat()">+Add Category</a>
                                <?php } else if ($i != 1) {
                                    ?>
                                    <a onClick="return confirm('Are you sure?');" class="delete_categories" href="add-manage-coupons.php?bl_id=<?php echo $BL_ID ?>&bfc_id=<?php echo $BFC_ID ?>&op=del_cat&bfccm_id=<?php echo $row1['BFCCM_ID'] ?>">Delete</a>
                                <?php }
                                ?>
                                </div>
                            </div>           
                            <?php
                            $i++;
                        }
                    } else {
                        ?>
                        <input type="hidden" name="total_cat" id="check-cat" value="1">
                        <div class="form-inside-div">
                            <label>Category 1</label>
                            <div class="form-data">
                                <select name="c_coupon_1" required>
                                    <option value="">Select Category</option>
                                    <?php
                                    $sql_cat_coupon = "SELECT C_ID, C_Name_SEO, C_Name FROM tbl_Category
                                                        WHERE C_Parent = (SELECT C_ID FROM tbl_Category WHERE C_Is_Product_Web = 1) 
                                                        ORDER BY C_Name";
                                    $result_cat_coupon = mysql_query($sql_cat_coupon);
                                    while ($row = mysql_fetch_array($result_cat_coupon)) {
                                        ?>
                                        <option value="<?php echo $row['C_ID'] ?>"><?php echo $row['C_Name'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <a class="add_categories" onclick="add_more_cat()">+Add Category</a>
                            </div>
                        </div> 
                    <?php }
                    ?>
                </div>
                <div class="form-inside-div">
                    <label>Deal Description</label>
                    <div class="form-data">
                        <textarea name="deal_description"><?php echo $actCoupon['BFC_Description']; ?></textarea>
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>Terms & Conditions</label>
                    <div class="form-data">
                        <textarea name="deal_terms"><?php echo $actCoupon['BFC_Terms_Conditions']; ?></textarea>
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>Expiry Date</label>
                    <div class="form-data">
                        <input class="previous-date-not-allowed" type="text" name="expire_date"  value="<?php echo $actCoupon['BFC_Expiry_Date']; ?>"/>
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>Photo</label>
                    <div class="form-data">
                        <span class="select-photo" onclick="show_image_library(28, <?php echo $BL_ID; ?>, <?php echo $BL_ID ?>)">Select File</span>
                        <?php if ($actCoupon['BFC_Main_Image'] != '' && $actCoupon['BFC_Main_Image']) { ?>
                            <span class="delete-photo"><a onClick="return confirm('Are you sure?');" href="add-manage-coupons.php?bl_id=<?php echo $BL_ID ?>&bfc_id=<?php echo $BFC_ID ?>&op=del">Delete Photo</a></span>
                        <?php } ?>
                        <input type="file" name="pic[]" onchange="show_file_name(28, this, <?php echo $BL_ID ?>)" id="photo<?php echo $BL_ID ?>" style="display: none;">
                        <input type="hidden" name="image_bank" class="image_bank" id="image_bank<?php echo $BL_ID ?>" value="">   
                        <div class="cropit-image-preview">
                            <img class="preview-img preview-img-script<?php echo $BL_ID ?>" style="display: none;" src="">  
                            <?php if ($actCoupon['BFC_Main_Image'] != '') { ?>
                                <input type="hidden" id="update_<?php echo $BL_ID; ?>" value="1"/>
                                <img class="existing-img existing_imgs<?php echo $BL_ID ?>" src="http://<?php echo DOMAIN . IMG_LOC_REL . $actCoupon['BFC_Main_Image'] ?>" >
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="form-inside-div">
                    <div class="button">
                        <input type="submit" name="submit" value="Save Now"/>
                    </div>
                </div>
            </form>
        </div>

<!--   Contact Datial Form -->
 <div class="right">
  <?php 
	  if ($BL_ID > 0) {
	    $sql = "SELECT BL_Listing_Title, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website
	            FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
	    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
	    $rowListing = mysql_fetch_assoc($result);
	} else {
	    header('Location: index.php');
	}
?>
         <form name="form1" class="storyForm" id="contact_details_from_<?php echo $BFC_ID ?>" method="post" style="display:none;">
                <input type="hidden" name="bl_id" value="<?php echo $BL_ID; ?>">
                <input type="hidden" name="op" value="save_contact_details">
                <input type="hidden" name="bfc_id" value="<?php echo $BFC_ID; ?>">
                <div class="form-inside-div">
                    <label>Business Title</label>
                    <div class="form-data">
                        <input name="name" type="text" id="BLname" value="<?php echo $rowListing['BL_Listing_Title'] ?>" size="50"  required/>
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>Contact Name</label>
                    <div class="form-data">
                        <input name="contact" type="text" value="<?php echo $rowListing['BL_Contact'] ?>" size="50" />
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>Phone</label>
                    <div class="form-data">
                        <input name="phone" class="phone_number" type="text" value="<?php echo $rowListing['BL_Phone'] ?>" size="50" />
                    </div>
                </div>

                <div class="form-inside-div">
                    <label>Toll Free</label>
                    <div class="form-data">
                        <input name="tollFree" class="phone_toll" type="text" value="<?php echo $rowListing['BL_Toll_Free'] ?>" size="50" />
                    </div>
                </div>

                <div class="form-inside-div">
                    <label>Cell</label>
                    <div class="form-data">
                        <input name="cell" class="phone_number" type="text" value="<?php echo $rowListing['BL_Cell'] ?>" size="50" />
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>Fax</label>
                    <div class="form-data">
                        <input name="fax" class="phone_number" type="text" value="<?php echo $rowListing['BL_Fax'] ?>" size="50" />
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>Email</label>
                    <div class="form-data">
                        <input name="email" type="email" value="<?php echo $rowListing['BL_Email'] ?>" size="38" />
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>Website</label>
                    <div class="form-data">
                        <input name="website" type="text" value="<?php echo $rowListing['BL_Website'] ?>" size="50" />
                    </div>
                </div>
                <div class="form-inside-div border-none">
                    <div class="button">
                        <input type="submit" name="button2" value="Save Now"/>
                    </div>
                </div>
            </form>
        </div>

        <!-- mapping form -->
        <div class="right">
        <?php 
        if ($BL_ID > 0) {
            $sql = "SELECT BL_Listing_Title, BL_Street, BL_Town, BL_Province,BL_Country, BL_PostalCode, BL_Lat, BL_Long 
                    FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $rowListing = mysql_fetch_assoc($result);
        } else {
            header('Location: index.php');
        }
        ?>
            <form name="form1" class="storyForm" id="customer_listing_mapping_from_<?php echo $BFC_ID ?>"  method="post" action="" style="display: none;">
                <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
                <input type="hidden" name="bfc_id" value="<?php echo $BFC_ID; ?>">
                <input type="hidden" name="op" value="save_cutomer_listing_mapping">
                <div class="form-inside-div">
                    <label>Street Address</label>
                    <div class="form-data">
                        <input name="address" type="text" id="address" value="<?php echo $rowListing['BL_Street'] ?>" size="50" />
                    </div>
                </div>

                <div class="form-inside-div">
                    <label>Town</label>
                    <div class="form-data">
                        <input name="town" type="text" id="town" value="<?php echo $rowListing['BL_Town'] ?>" size="50" />
                    </div>
                </div>

                <div class="form-inside-div">
                    <label>Province</label>
                    <div class="form-data">
                        <input name="province" type="text" id="province" value="<?php echo $rowListing['BL_Province'] ?>" size="50" />
                    </div>
                </div>

                <div class="form-inside-div">
                    <label>Country</label>
                    <div class="form-data">
                        <input name="country" type="text" id="country" value="<?php echo $rowListing['BL_Country'] ?>" size="50" />
                    </div>
                </div>


                <div class="form-inside-div">
                    <label>Postal Code</label>
                    <div class="form-data">
                        <input name="postalcode" type="text" id="postalcode" value="<?php echo $rowListing['BL_PostalCode'] ?>" size="50" />
                    </div>
                </div>

                <div class="form-inside-div">
                    <div class="content-sub-header">
                        <div class="title">Google Maps</div>
                    </div>
                    <?php
                    $help_text = show_help_text('Google Maps');
                    if ($help_text != '') {
                        echo '<div class="gmap-left">' . $help_text . '</div>';
                    }
                    ?>
                </div>
                <div class="form-inside-div">
                    <label>Latitude</label>
                    <div class="form-data">
                        <input name="latitude" type="text" id="latitude" value="<?php echo $rowListing['BL_Lat'] ?>" size="10" />
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>Longitude</label>
                    <div class="form-data">
                        <input name="longitude" type="text" id="longitude" value="<?php echo $rowListing['BL_Long'] ?>" size="10" />
                    </div>
                </div>
                <div class="form-inside-div border-none">
                    <div class="button">
                        <input type="submit" name="button2" value="Save Now"/>
                    </div>
                </div> 
            </form>
        </div>
      <!-- hour form -->
      <div class="right">
      <?php
      if ($BL_ID > 0) {
			    $sql = "SELECT BL_Listing_Title, BL_Hour_Mon_From, BL_Hour_Mon_To, BL_Hour_Tue_From, BL_Hour_Tue_To, BL_Hour_Wed_From, BL_Hour_Wed_To, 
			            BL_Hour_Thu_From, BL_Hour_Thu_To, BL_Hour_Fri_From, BL_Hour_Fri_To, BL_Hour_Sat_From, BL_Hour_Sat_To, BL_Hour_Sun_From, BL_Hour_Sun_To, BL_Hours_Disabled, BL_Hours_Appointment
			            FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
			    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
			    $rowListing = mysql_fetch_assoc($result);
			} else {
			    header('Location: index.php');
			}
			function fromHours($time) {
    echo "<option value='00:00:00'>Select One</option>";
    for ($i = 1; $i <= 23; $i++) {
        if ($i == 0) {
            echo "<option value='" . date('H:00:04', mktime($i)) . "'";
            echo $time == date('H:00:04', mktime($i)) ? ' selected' : '';
            echo ">" . date('g:ia', mktime($i, 0)) . "</option>";
        } else {
            echo "<option value='" . date('H:00:00', mktime($i)) . "'";
            echo $time == date('H:00:00', mktime($i)) ? ' selected' : '';
            echo ">" . date('g:ia', mktime($i, 0)) . "</option>";
        }
        echo "<option value='" . date('H:i:00', mktime($i, 30)) . "'";
        echo $time == date('H:i:00', mktime($i, 30)) ? ' selected' : '';
        echo ">" . date('g:ia', mktime($i, 30)) . "</option>";
    }
    echo "<option value='00:00:04'";
    echo $time == '00:00:04' ? ' selected' : '';
    echo ">12:00am</option>";
    echo "<option value='00:30:00'";
    echo $time == '00:30:00' ? ' selected' : '';
    echo ">12:30am</option>";
    echo "<option value='00:00:01'";
    echo $time == '00:00:01' ? ' selected' : '';
    echo ">Closed</option>";
    echo "<option value='00:00:03'";
    echo $time == '00:00:03' ? ' selected' : '';
    echo ">open 24 hours</option>";
}

function toHours($time) {
    echo "<option value='00:00:00'>Select One</option>";
    for ($i = 1; $i <= 23; $i++) {
        if ($i == 0) {
            echo "<option value='" . date('H:00:01', mktime($i)) . "'";
            echo $time == date('H:00:01', mktime($i)) ? ' selected' : '';
            echo ">" . date('g:ia', mktime($i, 0)) . "</option>";
        } else {
            echo "<option value='" . date('H:00:00', mktime($i)) . "'";
            echo $time == date('H:00:00', mktime($i)) ? ' selected' : '';
            echo ">" . date('g:ia', mktime($i, 0)) . "</option>";
        }
        echo "<option value='" . date('H:i:00', mktime($i, 30)) . "'";
        echo $time == date('H:i:00', mktime($i, 30)) ? ' selected' : '';
        echo ">" . date('g:ia', mktime($i, 30)) . "</option>";
    }
    echo "<option value='00:00:04'";
    echo $time == '00:00:04' ? ' selected' : '';
    echo ">12:00am</option>";
    echo "<option value='00:30:00'";
    echo $time == '00:30:00' ? ' selected' : '';
    echo ">12:30am</option>";
}
       ?>
            <form name="form1" class="storyForm" method="post" action="" id="house_form_<?php echo $BFC_ID; ?>"  style="display:none;">
                <input type="hidden" name="op" value="save_hours">
                <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
                <input type="hidden" name="bfc_id" value="<?php echo $BFC_ID; ?>">
                <div class="form-inside-div">
                    <?php
                    $help_text = show_help_text('Hours');
                    if ($help_text != '') {
                        echo '<div class="gmap-left">' . $help_text . '</div>';
                    }
                    ?>
                </div>
                <div class="form-inside-div hours">
                    <label>Monday</label>
                    <div class="form-data">
                        <select name="monFrom" id="monFrom" onchange="isClosed('monHide', this.value);">
                            <?PHP fromHours($rowListing['BL_Hour_Mon_From']); ?>
                        </select>
                        <span id="monHide" <?php echo $rowListing['BL_Hour_Mon_From'] == '00:00:01' || $rowListing['BL_Hour_Mon_From'] == '00:00:02' || $rowListing['BL_Hour_Mon_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                            <select name="monTo" id="monTo" onchange="">
                                <?PHP toHours($rowListing['BL_Hour_Mon_To']); ?>
                            </select>
                        </span>
                    </div>
                </div>

                <div class="form-inside-div hours">
                    <label>Tuesday</label>
                    <div class="form-data">
                        <select name="tueFrom" id="tueFrom" onchange="isClosed('tueHide', this.value);">
                            <?PHP fromHours($rowListing['BL_Hour_Tue_From']); ?>
                        </select>
                        <span id="tueHide" <?php echo $rowListing['BL_Hour_Tue_From'] == '00:00:01' || $rowListing['BL_Hour_Tue_From'] == '00:00:02' || $rowListing['BL_Hour_Tue_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                            <select name="tueTo" id="tueTo" onchange="">
                                <?PHP toHours($rowListing['BL_Hour_Tue_To']); ?>
                            </select>
                        </span>
                    </div>
                </div>

                <div class="form-inside-div hours">
                    <label>Wednesday</label>
                    <div class="form-data">
                        <select name="wedFrom" id="wedFrom" onchange="isClosed('wedHide', this.value);">
                            <?PHP fromHours($rowListing['BL_Hour_Wed_From']); ?>
                        </select>
                        <span id="wedHide" <?php echo $rowListing['BL_Hour_Wed_From'] == '00:00:01' || $rowListing['BL_Hour_Wed_From'] == '00:00:02' || $rowListing['BL_Hour_Wed_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                            <select name="wedTo" id="wedTo" onchange="">
                                <?PHP toHours($rowListing['BL_Hour_Wed_To']); ?>
                            </select>
                        </span>
                    </div>
                </div>

                <div class="form-inside-div hours">
                    <label>Thursday</label>
                    <div class="form-data">
                        <select name="thuFrom" id="thuFrom" onchange="isClosed('thuHide', this.value);">
                            <?PHP fromHours($rowListing['BL_Hour_Thu_From']); ?>
                        </select>
                        <span id="thuHide" <?php echo $rowListing['BL_Hour_Thu_From'] == '00:00:01' || $rowListing['BL_Hour_Thu_From'] == '00:00:02' || $rowListing['BL_Hour_Thu_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                            <select name="thuTo" id="thuTo" onchange="">
                                <?PHP toHours($rowListing['BL_Hour_Thu_To']); ?>
                            </select>
                        </span>
                    </div>
                </div>

                <div class="form-inside-div hours">
                    <label>Friday</label>
                    <div class="form-data">
                        <select name="friFrom" id="friFrom" onchange="isClosed('friHide', this.value);">
                            <?PHP fromHours($rowListing['BL_Hour_Fri_From']); ?>
                        </select>
                        <span id="friHide" <?php echo $rowListing['BL_Hour_Fri_From'] == '00:00:01' || $rowListing['BL_Hour_Fri_From'] == '00:00:02' || $rowListing['BL_Hour_Fri_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                            <select name="friTo" id="friTo" onchange="">
                                <?PHP toHours($rowListing['BL_Hour_Fri_To']); ?>
                            </select>
                        </span>
                    </div>
                </div>

                <div class="form-inside-div hours">
                    <label>Saturday</label>
                    <div class="form-data">
                        <select name="satFrom" id="satFrom" onchange="isClosed('satHide', this.value);">
                            <?PHP fromHours($rowListing['BL_Hour_Sat_From']); ?>
                        </select>
                        <span id="satHide" <?php echo $rowListing['BL_Hour_Sat_From'] == '00:00:01' || $rowListing['BL_Hour_Sat_From'] == '00:00:02' || $rowListing['BL_Hour_Sat_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                            <select name="satTo" id="satTo" onchange="">
                                <?PHP toHours($rowListing['BL_Hour_Sat_To']); ?>
                            </select>
                        </span>
                    </div>
                </div>

                <div class="form-inside-div hours">
                    <label>Sunday</label>
                    <div class="form-data">
                        <select name="sunFrom" id="sunFrom" onchange="isClosed('sunHide', this.value);">
                            <?PHP fromHours($rowListing['BL_Hour_Sun_From']); ?>
                        </select>
                        <span id="sunHide" <?php echo $rowListing['BL_Hour_Sun_From'] == '00:00:01' || $rowListing['BL_Hour_Sun_From'] == '00:00:02' || $rowListing['BL_Hour_Sun_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                            <select name="sunTo" id="sunTo">
                                <?PHP toHours($rowListing['BL_Hour_Sun_To']); ?>
                            </select>
                        </span>
                    </div>
                </div>

                <div class="form-inside-div">
                    <label>Disable Hours</label>
                    <div class="form-data">
                        <input name="hDisabled" type="checkbox" style="margin-top: 16px;" value="1" <?php echo $rowListing['BL_Hours_Disabled'] ? 'checked' : '' ?>>
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>By Appointment</label>
                    <div class="form-data">
                        <input name="happointment" type="checkbox" style="margin-top: 16px;" value="1" <?php echo $rowListing['BL_Hours_Appointment'] ? 'checked' : '' ?>>
                    </div>
                </div>

                <div class="form-inside-div border-none">
                    <div class="button">
                        <input type="submit" name="submit" value="Save Now"/>
                    </div>
                </div>
            </form> 
        </div>
        <!-- social media -->
         <div class="right">
         <?php 
         if ($BL_ID > 0) {
			    $sql = "SELECT BL_Listing_Title FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
			    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
			    $rowListing = mysql_fetch_assoc($result);
			} else {
			    header('Location: index.php');
			}
			
         ?>
            <form name="form1" id="social_media_<?php echo $id; ?>" class="social_media_<?php echo $id; ?>" method="post" action="" style="display: none;">
                <input type="hidden" name="bl_id" value="<?php echo $BL_ID; ?>">
                <input type="hidden" name="bfc_id" value="<?php echo $BFC_ID; ?>">
                <input type="hidden" name="op" value="save_social_media">
                <?PHP
                $sql = "SELECT BS_FB_Link, BS_T_Link, BS_I_Link FROM tbl_Business_Social WHERE BS_BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                $count = mysql_num_rows($result);
                $row = mysql_fetch_assoc($result);
                ?> 
                <div class="form-inside-div">
                    <label>Facebook Link</label>
                    <div class="form-data">
                        <input name="fb_link" type="text" value="<?php echo $row['BS_FB_Link'] ?>" size="50" />
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>Twitter Link</label>
                    <div class="form-data">
                        <input name="t_link" type="text"  value="<?php echo $row['BS_T_Link'] ?>" size="50" />
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>Instagram Link</label>
                    <div class="form-data">
                        <input name="i_link" type="text"  value="<?php echo $row['BS_I_Link'] ?>" size="50" />
                    </div>
                </div>
                <div class="form-inside-div border-none">
                    <div class="button">
                        <input type="hidden" name="counter" value="<?php echo $count; ?>">
                        <input type="submit" name="button2" value="Save Now"/>
                    </div>
                </div>
            </form>
        </div>
<?PHP
include '../include/mycoupon/footer.php';
?>
<script type="text/javascript">
//// coupon
	function coupon_edit(id_five){ 
 $("#existing-coupon" + id_five).attr("title", "Edit Coupon").dialog({
        modal: true,
        draggable: false,
        resizable: false,
        width: 750,
        open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#existing-coupon" + id_five).dialog('close');
                })
            }
    });   
}
///contact datial
function show_contact_details_form(id_two) {
        $("#contact_details_from_" + id_two).attr("title", "Contact Details").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 672,
        open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#contact_details_from_" + id_two).dialog('close');
                })
            }
        });
    }
   ///// mapping form
   function show_customer_listing_mapping(id_one) {
        $("#customer_listing_mapping_from_" + id_one).attr("title", "Mapping Details").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 660,
            heighr:600,
        open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#customer_listing_mapping_from_" + id_one).dialog('close');
                })
            }
        });
    } 
     function show_customer_hours_allday(id_three) { 
        $("#house_form_" + id_three).attr("title", "Hours Details").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 660,
        open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#house_form_" + id_three).dialog('close');
                })
            }
        });
    }
    function show_customer_social_media(id_four) { 
        $(".social_media_" + id_four).attr("title", "Social Media").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 660,
        open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery(".social_media_" + id_four).dialog('close');
                })
            }
        });
    }
 var next_count;
    function DeleteVal(id){
       $( "#lab_num_"+next_count).text('Category ' + id);
       $( "#category_num_"+id).remove();
        var count = $("#check-cat").val();        
        next_count = --count;
        $("#check-cat").val(next_count);
        $('#cat_coupon' + 3).attr('name', 'c_coupon_2');
        $('#cat_coupon' + 3).attr('class', 'cat_coupon_2'); 
        $('#cat_coupon' + 3).attr('id', 'cat_coupon2');
        $( "#lab_num_"+ 3).text('Category 2');
        $('#del_' + 3).attr("onclick","DeleteVal(2)");
        $('#del_' + 3).attr('id', 'del_2');
        $('#category_num_' + 3).attr('id', 'category_num_2');  
    }
    function add_more_cat()
    {
        var count = $("#check-cat").val();
        var cat_coupons =<?php echo $cat_coupon ?>;
        next_count = ++count;
        if (count < 4) {
            
            $("#more_cat").append('<div id="category_num_'+ next_count + '"  class="form-inside-div DeleteValuse"><label id="lab_num_' + next_count + '">Category ' + next_count + '</label><div class="form-data"><select name="c_coupon_' + next_count + '" class="cat_coupon_' + next_count + '" id="cat_coupon' + next_count + '"><option value="">Select Category</option></select><span class="delete_categoriess" id="del_'+ next_count+ '" onClick="DeleteVal('+next_count+')">Delete</span></div></div>');
            $.each(cat_coupons, function (index, cat) {
                $("#cat_coupon" + next_count).append('<option value="' + cat.C_ID + '">' + cat.C_Name + '</option>');
            });
            $("#check-cat").val(next_count);
        } else
        {
            alert("You can select only 3 catgeories.", "warning");
        }
    }
</script>