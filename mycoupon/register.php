<?php
require_once '../include/config.inc.php';
include '../include/mycoupon/header.php';
?>
<div class="sign-container">
    <div class="login-container">
        <form name="form1" onSubmit="return validatePassword($('#LOGIN_PASS').val(), true, $('#LOGIN_PASS2').val())" method="post" action="http://<?php echo COUPON_DOMAIN . BUSINESS_DIR ?>">
            <input name="logop" type="hidden" id="logop" value="sign_up">
            <div class="login-container-inside-div">
                <div class="login-detail-title">
                    Register
                </div>
                <?php
                if (isset($_SESSION['mismatch']) && $_SESSION['mismatch'] == 1) {
                    ?>
                    <div class="login-from-inside-div">
                        <div class="invalid-user-warning">Password does not match.</div>
                    </div>
                    <?php
                    unset($_SESSION['mismatch']);
                }
                ?>
                <?php
                if (isset($_SESSION['exists_record']) && $_SESSION['exists_record'] == 1) {
                    ?>
                    <div class="login-from-inside-div">
                        <div class="invalid-user-warning">You already have an account with us, please <a href="http://mycoupon.touristtowndemo.com/login.php">Sign In</a> using this email address. Click <a href="http://mycoupon.touristtowndemo.com/lost-password.php">forgot password</a> if you have forgotten your password and a new one will be sent to you. <a href="mailto:info@couponcountry.ca">Contact us</a> if you need help.<br>
                        </div>
                    </div>
                    <?php
                    unset($_SESSION['exists_record']);
                }
                ?>
                <script>
                    function makeSEO(myVar) {
                        myVar = myVar.toLowerCase();
                        myVar = myVar.replace(/[^a-z0-9\s-]/gi, '');
                        myVar = myVar.replace(/"/g, '');
                        myVar = myVar.replace(/'/g, '');
                        myVar = myVar.replace(/&/g, '');
                        myVar = myVar.replace(/\//g, '');
                        myVar = myVar.replace(/,/g, '');
                        myVar = myVar.replace(/  /g, ' ');
                        myVar = myVar.replace(/ /g, '-');

                        $('#SEOname').val(myVar);
                        return false;
                    }
                </script>
                <div class="login-from-inside-div">
                    <input name="business_name" id="business_name" placeholder="Business Name" type="text" value="<?php echo (isset($_REQUEST['business_name']) ? $_REQUEST['business_name'] : '') ?>" required onKeyUp="makeSEO(this.value)"/>
                    <input type="hidden" name="bname" id="SEOname" value="" onKeyUp="makeSEO(this.value)">
                    <div id="suggest-business" style="display:none;"></div>
                </div>
                <div class=login-from-inside-div>
                    <input name="LOGIN_USER" id="LOGIN_USER" placeholder="Email" type="email" value="<?php echo (isset($_REQUEST['LOGIN_USER']) ? $_REQUEST['LOGIN_USER'] : '') ?>" required/>
                </div>
                <div class="login-from-inside-div">
                    <input name="contact_name" placeholder="Contact Name" type="text" value="<?php echo (isset($_REQUEST['contact_name']) ? $_REQUEST['contact_name'] : '') ?>" required/>
                </div>
                <div class="login-from-inside-div">
                    <input name="address" placeholder="Street Address" type="text" value="<?php echo (isset($_REQUEST['address']) ? $_REQUEST['address'] : '') ?>" required/>
                </div>
                <div class="login-from-inside-div">
                    <input name="town" placeholder="Town" type="text" value="<?php echo (isset($_REQUEST['town']) ? $_REQUEST['town'] : '') ?>" required/>
                </div>
                <div class="login-from-inside-div">
                    <input name="LOGIN_PASS" id="LOGIN_PASS" placeholder="Password" type="password" required/>
                </div>
                <div class="login-from-inside-div">
                    <input name="CON_LOGIN_PASS" id="LOGIN_PASS2" placeholder="Confirm Password" type="password" required/>
                </div>
                <div class="login-from-inside-div">
                    <div class="from-inside-div-button">
                        <input name="Submit" type="submit" value="Register Now" /></div>
                </div>
                <div class="login-from-inside-div">
                    <label></label>
                    <div class="form-data">
                        <strong>Password must meet the following requirements:</strong>
                        <ul>
                            <li>At Least 6 Characters</li>
                            <li>Contain one Number</li>
                            <li>Contain one lowercase letter</li>
                            <li>Contain one uppercase letter</li>
                        </ul>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="Sign-in-up-msg">
        <div class="msg-body-container">
            <div class="message-body">Already have a Coupon Country account? <a href="<?php echo "http://" . COUPON_DOMAIN . BUSINESS_DIR ?>login.php"> Sign In</a></div>
            <div class="message-body">Coupon Country is designed to serve businesses in Bruce County, Grey County and Huron County. If you are located in a community in one of these counties, you may already have a Tourist Town account that you can log in to Coupon County with.</div>
            <div class="message-body">
                Please check here.
            </div>
            <div id="successSend" class="reset-message-green"> You already have a touristtown account. </div>
            <div id="errorSend" class="reset-message">There is no record of the email address that you have entered.</div>

            <div class="login-container-reset" id="forget-form">
                <form name="form1" method="post" action="" id="validation-form" onSubmit="return checkin();">

                    <div class="login-from-inside-div reset">
                        <input name="email" required="" size="25" placeholder="Email Address" type="email" id="email_check_in" class="email-reset">
                    </div>

                    <div class="frombutton">
                        <input type="submit" name="Submit" value="check-in" >
                    </div>

                </form>
            </div>
            <div class="message-body-logo">
                <img src="http://<?php echo DOMAIN ?>/images/TouristTown-Logo.gif" alt="Tourist Town" width="195" height="43" hspace="20" vspace="20" border="0" />
            </div>    
        </div>  
    </div>
</div>
<?php
include '../include/mycoupon/footer.php';
?>