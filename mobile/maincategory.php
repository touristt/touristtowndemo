<?php
$sql_feature = "SELECT S_ID, S_Title, S_Description, S_Thumbnail_Mobile FROM tbl_Story 
                LEFT JOIN tbl_Story_Homepage_Category ON S_ID = SHC_S_ID
                WHERE SHC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
                AND SHC_Category = '" . encode_strings($activeCat['C_ID'], $db) . "' AND SHC_Feature = 1
                AND S_Active = 1 GROUP BY S_ID ORDER BY SHC_Order ASC";
$result_feature = mysql_query($sql_feature, $db) or die("Invalid query: $sql_feature -- " . mysql_error());
$feature_count = mysql_num_rows($result_feature);
?>
<!--Main Title Start-->
<section class="main_title">
    <p>
        <?php
        if ($activeCat['RC_Category_Page_Title'] != '') {
            echo $activeCat['RC_Category_Page_Title'];
        } else {
            echo ($activeCat['RC_Name'] != '') ? $activeCat['RC_Name'] : $activeCat['C_Name'];
        }
        ?>
    </p>
</section>
<!--Main Title End-->
<!-- Feature Stories-->
<?php if ($feature_count > 0 && $REGION['R_Stories'] == 1 && $blog['RC_Status'] == 0) { ?>
    <section class="feature-items padding-none">
        <?php
        while ($row_feature = mysql_fetch_array($result_feature)) {
            if ($row_feature['S_Thumbnail_Mobile'] != '') {
                $feature_image = $row_feature['S_Thumbnail_Mobile'];
            } else {
                $feature_image = $default_header_image;
            }
            ?>
            <div class="feature">
                <div class="thumbnail">
                    <a href="<?php echo DOMAIN_MOBILE_REL . 'story/' . clean($row_feature['S_Title']) . '/' . $row_feature['S_ID'] ?>/">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $feature_image; ?>" width="100%" height="auto" alt="<?php echo $row_feature['S_Title'] ?>" />
                    </a>
                </div>
                <a class="feature-title" href="<?php echo DOMAIN_MOBILE_REL . 'story/' . clean($row_feature['S_Title']) . '/' . $row_feature['S_ID'] ?>/">
                    <?php echo $row_feature['S_Title'] ?>
                </a>
                <?php
                if ($row_feature['S_Description'] != '') {
                    $string = preg_replace('/(.*)<\/p[^>]*>/i', '$1', $row_feature['S_Description']);
                    ?>
                    <div class="description">
                        <?php echo $string ?>
                        <a class="read-more" href="<?php echo DOMAIN_MOBILE_REL . 'story/' . clean($row_feature['S_Title']) . '/' . $row_feature['S_ID'] ?>/">
                            Read More
                        </a>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </section>
<?php } ?>
<!-- Feature Stories end -->

<!--Thumbnail Grid-->
<section class="thumbnails">
    <?PHP
    if ($activeCat['C_ID'] != 8 && $activeCat['C_Is_Blog'] != 1) {
        $sql = "SELECT C_ID, C_Parent, C_Order, RC_Mobile_Thumbnail, C_Name_SEO, C_Name, RC_Name FROM tbl_Category 
                LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = " . encode_strings($REGION['R_ID'], $db) . "
                INNER JOIN tbl_Business_Listing_Category ON BLC_C_ID = C_ID   
                INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID AND BLCR_BLC_R_ID " . encode_strings((($REGION['R_Parent'] == 0 && $REGION['R_Type'] != 4 ) ? "IN " . $regionList : "= " . $REGION['R_ID']), $db) . "
                INNER JOIN tbl_Business_Listing ON BL_ID = BLC_BL_ID
                WHERE C_Parent = '" . encode_strings($activeCat['C_ID'], $db) . "' AND RC_Status = 0 AND RC_R_ID > 0 AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = '1' $include_free_listings
                GROUP BY C_ID ORDER BY RC_Order";
    } else {
        $sql = "SELECT C_ID, C_Parent, C_Order, RC_Mobile_Thumbnail, C_Name_SEO, C_Name, RC_Name FROM tbl_Region_Category 
                LEFT JOIN tbl_Category ON RC_C_ID = C_ID
                WHERE C_Parent = '" . encode_strings($activeCat['C_ID'], $db) . "' AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' AND RC_Status = 0 AND RC_R_ID > 0
                ORDER BY RC_Order";
    }
    $result = mysql_query($sql, $db) or die(mysql_error());
    while ($row = mysql_fetch_assoc($result)) {
        if ($row['RC_Mobile_Thumbnail'] != '') {
            $sub_cat_image = $row['RC_Mobile_Thumbnail'];
        } else {
            $sub_cat_image = $default_thumbnail_image;
        }
        ?>
        <div class="thumbnails-inner">
            <div class="thumbnail">
                <a href="<?php echo DOMAIN_MOBILE_REL . $activeCat['C_Name_SEO'] ?>/<?php echo $row['C_Name_SEO'] ?>/">
                    <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $sub_cat_image; ?>" alt="<?php echo ($row['RC_Name'] != '') ? $row['RC_Name'] : $row['C_Name'] ?>" />
                    <div class="title category"><p><?php echo ($row['RC_Name'] != '') ? $row['RC_Name'] : $row['C_Name'] ?></p></div>
                </a>
            </div>
        </div>
        <?php
    }
    ?>
    <?php 
 //Events
    if ($activeCat['C_ID'] == 8) {
        require_once 'events.php';
    }
    ?>
    <?php if ($REGION['R_Day_Trip'] == 1 && $rowcount != 0 && ($activeCat['C_ID'] == 5 || $activeCat['RC_C_ID'] == 5)) {
        if ($activeCat['RC_Mobile_Thumbnail'] != '') {
            $daytrip_image = $activeCat['RC_Mobile_Thumbnail'];
        } else {
            $daytrip_image = $default_thumbnail_image;
        }
        ?>
        <div class="thumbnails-inner">
            <div class="thumbnail">
                <a href="<?php echo DOMAIN_MOBILE_REL ?>things-to-do/day-trip">
                    <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $daytrip_image; ?>" alt="<?php echo $row['C_Name'] ?>" />
                    <div class="title category"><p>Day Trips</p></div>
                </a>
            </div>
        </div>
    <?php } ?>
</section>
<!--Thumbnail Grid-->
