<?php if ($url[2] == 'day-trip') { ?>
    <?php
    while ($Listings = mysql_fetch_assoc($resultListings)) {
        if ($Listings['BLP_Mobile_Photo'] != '') {
            $listing_image = $Listings['BLP_Mobile_Photo'];
        } else {
            $listing_image = $default_thumbnail_image;
        }
        ?>
        <div class="thumbnails-inner">
            <div class="thumbnail">
                <a href="<?php echo DOMAIN_MOBILE_REL . 'profile/' . $Listings['BL_Name_SEO'] . '/' . $Listings['BL_ID'] ?>/">
                    <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo $Listings['BL_Listing_Title'] ?>" />
                    <div class="title specialty"><p><?php echo $Listings['BL_Listing_Title']; ?></p></div>
                </a>
            </div>
        </div>
        <?php
    }

    // print_r($url[0]); 
} else {
    ?>

    <?php if ($REGION['R_Listings_Title'] != '') { ?>
        <h1 class="heading-text" align="center"><?php echo $REGION['R_Listings_Title']; ?></h1>
        <?php
    }
    if ($REGION['R_Parent'] == 0) {
        $REG = '';
    } else {
        $REG = "AND BLP_R_ID = " . $REGION['R_ID'];
    }
    $sqlListings = "SELECT DISTINCTROW BL_ID, BLP_Mobile_Header_Image, BLP_Mobile_Photo, BL_Photo_Alt, BL_Listing_Title, BL_Name_SEO 
                FROM tbl_Business_Listing_Category_Region
                LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BLCR_BL_ID AND BLC_C_ID = " . $activeCat['C_ID'] . "
                LEFT JOIN tbl_Business_Listing ON BL_ID = BLC_BL_ID
                LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
                LEFT JOIN tbl_Listing_Type ON LT_ID = BL_Listing_Type
                LEFT JOIN tbl_Business_Listing_Order ON BL_ID = BLO_BL_ID AND BLO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
                AND BLO_S_C_ID = BLC_C_ID
                WHERE BLCR_BLC_R_ID " . encode_strings((($REGION['R_Parent'] == 0 && $REGION['R_Type'] != 4) ? "IN " . $regionList : "= " . $REGION['R_ID']), $db) . " 
                AND hide_show_listing='1' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) $REG
                $include_free_listings group by BL_Listing_Title
                $order_listings";
//print $sqlListings;exit;
    $resultListings = mysql_query($sqlListings, $db);
    while ($Listings = mysql_fetch_assoc($resultListings)) {
        if ($Listings['BLP_Mobile_Photo'] != '') {
            $listing_image = $Listings['BLP_Mobile_Photo'];
        } else {
            $listing_image = $default_thumbnail_image;
        }
        ?>
        <div class="thumbnails-inner">
            <div class="thumbnail">
                <a href="<?php echo DOMAIN_MOBILE_REL . 'profile/' . $Listings['BL_Name_SEO'] . '/' . $Listings['BL_ID'] ?>/">
                    <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo $Listings['BL_Listing_Title']; ?>" />
                    <div class="title specialty"><p><?php echo $Listings['BL_Listing_Title']; ?></p></div>
                </a>
            </div>
        </div>
        <?php
    }
}
?>