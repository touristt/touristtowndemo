<?php
require_once '../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';

if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $routeID = $_REQUEST['id'];
} else {
    header("Location: " . DOMAIN_MOBILE_REL . "404.php");
    exit();
}
$sqlRoute = "SELECT IR_ID,IR_Photo, IR_Description, IR_Title, IR_Mobile_Photo, IR_Alt_Tag, IR_Start_Point, IR_End_Point, IR_Distance, IR_Surface, IR_Lat, IR_Long,
            IR_Zoom, IR_KML_Path, IR_SEO_Title, IR_SEO_Description, IR_SEO_Keywords FROM tbl_Individual_Route WHERE IR_ID = '" . encode_strings($routeID, $db) . "'";
$resRoute = mysql_query($sqlRoute, $db) or die("Invalid query: $sqlRoute -- " . mysql_error());
$Route = mysql_fetch_assoc($resRoute);
$SEOtitle = $Route['IR_SEO_Title'];
$SEOdescription = $Route['IR_SEO_Description'];
$SEOkeywords = $Route['IR_SEO_Keywords'];
$kml_array[] = array(
    'name' => str_replace(' ', '', $Route['IR_Title']),
    'url' => 'http://' . DOMAIN . MAP_LOC_REL . $Route['IR_KML_Path']
);

//Getting Gallery Images
$sqlG = "SELECT IRG_Title, IRG_Mobile_Photo FROM tbl_Individual_Route_Gallery WHERE IRG_IR_ID = '" . encode_strings($Route['IR_ID'], $db) . "' ORDER BY IRG_Order ASC";
$resG = mysql_query($sqlG, $db) or die("Invalid query: $sqlG -- " . mysql_error());
$count_gallery = mysql_num_rows($resG);
//Getting PDF Downloads
$sqlD = "SELECT IRD_Path, IRD_Title FROM tbl_Individual_Route_Downloads WHERE IRD_IR_ID='" . encode_strings($Route['IR_ID'], $db) . "' ORDER BY IRD_Order ASC";
$resD = mysql_query($sqlD, $db) or die("Invalid query: $sqlD -- " . mysql_error());
$count_download = mysql_num_rows($resD);

//Getting Region Theme
$getThemeOnRoute = "SELECT TO_Default_Thumbnail_Desktop FROM tbl_Theme_Options WHERE TO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
$themeRegionOnRoute = mysql_query($getThemeOnRoute, $db) or die("Invalid query: $getThemeOnRoute -- " . mysql_error());
$THEMEOnRoute = mysql_fetch_assoc($themeRegionOnRoute);

//Defualt Thumbnail Images
if ($THEMEOnRoute['TO_Default_Thumbnail_Desktop'] != '') {
    $default_header_route_image = $THEMEOnRoute['TO_Default_Thumbnail_Desktop'];
} else {
    $default_header_route_image = 'Default-Thumbnail-Desktop.jpg';
}

$OG_URL = curPageURL();
$OG_Type = 'Route';
$OG_Title = $Route['IR_Title'];
$OG_Description = $Route['IR_SEO_Description'];
$OG_Image = ($Route['IR_Photo'] != "") ? 'http://' . DOMAIN . IMG_LOC_REL . $Route['IR_Photo'] : 'http://' . DOMAIN . IMG_LOC_REL . $default_header_route_image;
$OG_Image_Width = 1600;
$OG_Image_Height = 640;
?>
<?php require_once 'include/public/header.php'; ?>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1356712584397235";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!--Slider Start-->
<section class="detail-page">
    <div class="main_image">
        <?php
        if ($Route['IR_Mobile_Photo'] != '') {
            $listing_image = $Route['IR_Mobile_Photo'];
        } else {
            $listing_image = $default_header_image;
        }
        ?>
        <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo $Route['IR_Alt_Tag'] ?>" />
    </div>
    <div class="listing-main margin-bottom-none">
        <div class="title route-title">
            <?php echo $Route['IR_Title'] ?>
        </div>
    </div>
    <div class="listing-main margin-top-none">
        <div class="fb-share-button" data-href="<?php print curPageURL(); ?>" data-layout="button_count"  data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php print curPageURL(); ?>%2F&amp;src=sdkpreparse">Share</a></div>
        <iframe class="twitter-class"
                src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=http%3A%2F%2F<?php print curPageURL(); ?>&related=twitterapi%2Ctwitter&text=<?php echo $Route['IR_Title'] ?> in <?php echo $REGION['R_Name'] ?>"
                width="70"
                height="25"
                style="border: 0; overflow: hidden; float: left;">
        </iframe>
    </div>
    <div class="listing-description">
        <?php echo $Route['IR_Description']; ?>
    </div>
    <div class="listing-detail-routes">
        <?php if ($Route['IR_Start_Point'] != '') { ?>
            <div class="address-heading">Start Point</div>
            <div class="address-data"><?php echo $Route['IR_Start_Point']; ?></div>
            <?php
        }
        if ($Route['IR_End_Point'] != '') {
            ?>
            <div class="address-heading">End Point</div>
            <div class="address-data"><?php echo $Route['IR_End_Point']; ?></div>
        <?php } ?>
        <div class="address-heading">Distance</div>
        <div class="address-data"><?php echo $Route['IR_Distance']; ?></div>
        <div class="address-heading">Surface</div>
        <div class="address-data"><?php echo $Route['IR_Surface']; ?></div>
        <?php if ($count_download > 0) { ?>
            <div class="address-heading">Downloads</div>
            <div class="address-data">
                <?php while ($rowD = mysql_fetch_assoc($resD)) { ?>
                    <a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $rowD['IRD_Path'] ?>"><?php echo $rowD['IRD_Title']; ?></a>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
    <?php if ($count_gallery > 0) { ?>
        <div class="gallery">
            <?php while ($rowG = mysql_fetch_assoc($resG)) { ?>
                <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $rowG['IRG_Mobile_Photo']; ?>" alt="<?php echo $rowG['IRG_Title'] ?>" />
            <?php } ?>
        </div>
    <?php } ?>
    <div class="map_wrapper">
        <?PHP
        $markers = array();
        $must_see = array();
        if ($REGION['R_Parent'] == 0) {
            $REG = '';
        } else {
            $REG = "AND BLP_R_ID = " . $REGION['R_ID'];
        }
        //Getting Map Listings
        $sqlMap = "SELECT DISTINCTROW BL_ID, BL_Listing_Title, BL_Name_SEO, BL_Lat, BL_Long, BL_Street, BL_Town, BLP_Mobile_Photo, BL_Photo_Alt, IRML_Must_See, 
                    RC.RC_Map_Icon as subcat_icon, RC1.RC_Map_Icon as cat_icon FROM tbl_Business_Listing 
                    LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
                    LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
                    INNER JOIN tbl_Region_Category RC ON RC.RC_C_ID = BLC_C_ID AND RC.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                    INNER JOIN tbl_Region_Category RC1 ON RC1.RC_C_ID = BLC_M_C_ID AND RC1.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                    LEFT JOIN tbl_Category ON RC.RC_C_ID = C_ID
                    INNER JOIN tbl_Individual_Route_Map_Listings ON IRML_BL_ID = BL_ID 
                    INNER JOIN tbl_Individual_Route ON IR_ID = IRML_IR_ID
                    WHERE IR_ID = '" . encode_strings($Route['IR_ID'], $db) . "' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing='1' AND RC.RC_Status = 0 AND RC1.RC_Status = 0 
                   $include_free_listings_on_map $REG
                    GROUP BY BL_ID ORDER BY IRML_Order ASC";
// generate icons for all listings on map regardless of page
        $resMap = mysql_query($sqlMap, $db) or die(mysql_error() . ' - ' . $sqlMap);
        while ($listMap = mysql_fetch_assoc($resMap)) {
            //Add to must see array if it is must see
            if ($listMap['IRML_Must_See'] == 1) {
                $must_see[] = $listMap;
            }

            //now add to map markers
            if ($listMap['BLP_Mobile_Photo'] != '') {
                $image = '<div class="thumbnail"><img src="http://' . DOMAIN . (IMG_LOC_REL . $listMap['BLP_Mobile_Photo']) . '" alt="' . htmlspecialchars(trim($listMap['BL_Photo_Alt']), ENT_QUOTES) . '" /></div>';
            } else {
                $image = '<div class="thumbnail"><img src="http://' . DOMAIN . (IMG_LOC_REL . $default_thumbnail_image) . '" alt="' . htmlspecialchars(trim($listMap['BL_Listing_Title']), ENT_QUOTES) . '" /></div>';
            }
            if ($listMap['subcat_icon'] != '') {
                $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $listMap['subcat_icon'];
            } elseif ($listMap['cat_icon'] != '') {
                $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $listMap['cat_icon'];
            } else {
                $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . 'default.png';
            }
            if ($listMap['BL_Lat'] && $listMap['BL_Long']) {
                $markers[] = array(
                    'id' => $listMap['BL_ID'],
                    'lat' => $listMap['BL_Lat'],
                    'lon' => $listMap['BL_Long'],
                    'name' => $listMap['BL_Listing_Title'],
                    'path' => '/profile/' . $listMap['BL_Name_SEO'] . '/' . $listMap['BL_ID'] . '/',
                    'icon' => $icon,
                    'main_photo' => $image,
                    'address' => htmlspecialchars(trim($listMap['BL_Street']), ENT_QUOTES),
                    'town' => htmlspecialchars(trim($listMap['BL_Town']), ENT_QUOTES)
                );
            }
        }
        $markers = json_encode($markers);
        ?>
        <div id="map_canvas" style="width:100%;height:300px;margin-bottom:10px;">&nbsp;</div>
        <?php
        $map_center['latitude'] = $Route['IR_Lat'];
        $map_center['longitude'] = $Route['IR_Long'];
        $map_center['zoom'] = $Route['IR_Zoom'];
        $kml_json = json_encode($kml_array);
        require_once 'map_script.php';
        ?>
    </div>
</section>
<!--Slider End-->
<!--Must See section-->
<?php if (count($must_see) > 0) { ?>
    <!--Main Title Start-->
    <section class="main_title">
        <p>Must Sees</p>
    </section>
    <section class="thumbnails">
        <?php
        foreach ($must_see as $rowMust) {
            if ($rowMust['BLP_Mobile_Photo'] != '') {
                $listing_image = $rowMust['BLP_Mobile_Photo'];
            } else {
                $listing_image = $default_thumbnail_image;
            }
            ?>
            <div class="thumbnails-inner">
                <div class="thumbnail">
                    <a href="<?php echo DOMAIN_MOBILE_REL . 'profile/' . $rowMust['BL_Name_SEO'] . '/' . $rowMust['BL_ID'] ?>/">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo $rowMust['BL_Listing_Title']; ?>" />
                        <div class="title specialty"><p><?php echo $rowMust['BL_Listing_Title']; ?></p></div>
                    </a>
                </div>
            </div>
            <?php
        }
        ?>
    </section>
<?php } ?>
<!--Must See section end-->
<?php require_once 'include/public/footer.php'; ?>