<?php
$sqlStories = "SELECT S_ID, S_Title, S_Thumbnail_Mobile FROM tbl_Story INNER JOIN tbl_Story_Region ON S_ID = SR_S_ID
              WHERE SR_R_ID = " . $REGION['R_ID'] . " AND S_Category = " . $activeCat['C_ID'] . " AND S_Active = 1 GROUP BY S_ID ORDER BY S_Title ";
$resultStories = mysql_query($sqlStories, $db);
$feature_count = mysql_num_rows($resultStories);
if ($feature_count > 0) {
    while ($Story = mysql_fetch_assoc($resultStories)) {
        if ($Story['S_Thumbnail_Mobile'] != '') {
            $story_image = $Story['S_Thumbnail_Mobile'];
        } else {
            $story_image = $default_thumbnail_image;
        }
        ?>
        <div class="thumbnails-inner">
            <div class="thumbnail">
                <a href="<?php echo DOMAIN_MOBILE_REL . 'story/' . clean($Story['S_Title']) . '/' . $Story['S_ID'] ?>/">
                    <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $story_image; ?>" alt="<?php echo $Story['S_Title'] ?>" />
                    <div class="title category"><p><?php echo $Story['S_Title'] ?></p></div>
                </a>
            </div>
        </div>
        <?php
    }
} else {
    echo "<p class='noStory'> No Stories at This Time.</p>";
}
?>