<?php
//Getting Local Events from DB
  $sqlEvents = "SELECT EventID, LocationID, EventType, Title, E_Name_SEO, EventDateStart, EventDateEnd FROM `Events_master` LEFT JOIN Events_Location ON EL_ID = LocationID";
//weekly events
if ($activeCat['C_ID'] == '87') {
  $sqlEvents .= " WHERE `EventType` = 3 AND `EventDateEnd` >= CURDATE()"; // AND `EventDateStart` <= CURDATE()   i comment it because of 2014 weekly events are not showing
}
//on going events
elseif ($activeCat['C_ID'] == '88') {
  $sqlEvents .= " WHERE `EventType` = 2 AND `EventDateEnd` >= CURDATE( ) AND `EventDateStart` <= CURDATE( ) ";
}
//upcommig events
elseif ($activeCat['C_ID'] == '34') {
  $sqlEvents .= " WHERE (`EventDateStart` >= CURDATE() OR `EventDateEnd` >= CURDATE()) ";
}
//Special events
elseif ($activeCat['C_ID'] == '50') {
  $sqlEvents .= " WHERE `EventType` = 5 AND `EventDateStart` >= CURDATE() ";
}
//Festivals
elseif ($activeCat['C_ID'] == '35') {
  $sqlEvents .= " WHERE `EventType` = 4 AND `EventDateStart` >= CURDATE() ";
}

//Festival and special events
elseif ($activeCat['C_ID'] == '94') { 
  $sqlEvents .= " WHERE `EventType` = 6 AND `EventDateStart` >= CURDATE() ";
}
//Volunteering
elseif ($activeCat['C_ID'] == '102') {
  $sqlEvents .= " WHERE `EventType` = 7 AND `EventDateStart` >= CURDATE() ";
}
//Planning an Event
else{ 
  $sqlEvents .= "   WHERE  Pending = 0";
}
//echo $sqlEvents;
$E_Region_ID = "";
if ($REGION['R_Parent'] == 0 && $REGION['R_Type'] != 4) {
  $EP = "SELECT RM_Child FROM `tbl_Region_Multiple` WHERE RM_Parent = '" . $REGION['R_ID'] . "'";
  $resultEP = mysql_query($EP);
  $event_counter = 0;
  while ($rowEP = mysql_fetch_assoc($resultEP)) {
    if ($event_counter == 0) {
      $operator = " AND (";
    } else {
      $operator = " OR";
    }
    $E_Region_ID .= " $operator FIND_IN_SET (" . $rowEP['RM_Child'] . ", E_Region_ID)";
    $event_counter++;
  }
  $E_Region_ID .= " $operator FIND_IN_SET (" . $REGION['R_ID'] . ", E_Region_ID) )";
} else {
  $E_Region_ID = " AND FIND_IN_SET (" . $REGION['R_ID'] . ", E_Region_ID)";
}
$sqlEvents .= " AND (`EventDateStart` >= CURDATE() OR ( `EventDateEnd` >= CURDATE( ) AND `EventDateStart` <= CURDATE( ))) $E_Region_ID";
if ($activeCat['C_ID'] != '34') {
  $sqlEvents .= " ORDER BY EventDateStart ";
} else {
  $sqlEvents .= " ORDER BY EventDateEnd ";
} 
$resultEvents = mysql_query($sqlEvents, $db);
while ($Event = mysql_fetch_assoc($resultEvents)) {
  ?>
  <div class="thumbnails-inner">
    <div class="event-detail-container">
      <div class="event-title">
        <a href="<?php echo DOMAIN_MOBILE_REL . 'event/' . $Event['E_Name_SEO'] . '/' . $Event['EventID'] ?>/">
          <?php echo $Event['Title'] ?>
        </a>
      </div>
      <div class="event-date">
        <p><?php echo date('M j, Y', strtotime($Event['EventDateStart'])) ?> <?php if ($Event['EventDateStart'] != $Event['EventDateEnd'] && $Event['EventDateEnd'] != '0000-00-00') { echo ' - '. date('M j, Y', strtotime($Event['EventDateEnd'])); } ?></p>
      </div>
    </div>
  </div>
  <?php
}
?>