<?php
$sql_feature = "SELECT S_ID, S_Title, S_Description, S_Thumbnail_Mobile FROM tbl_Story 
                LEFT JOIN tbl_Story_Homepage_Category ON S_ID = SHC_S_ID
                WHERE SHC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' 
                AND SHC_Category = '" . encode_strings($activeCat['C_ID'], $db) . "' AND SHC_Feature = 1
                AND S_Active = 1 GROUP BY S_ID ORDER BY SHC_Order ASC";
$result_feature = mysql_query($sql_feature, $db) or die("Invalid query: $sql_feature -- " . mysql_error());
$feature_count = mysql_num_rows($result_feature);
?>
<!--Main Title Start-->
<section class="main_title">
    <p>
        <?php
        if ($url[2] == 'day-trip') {
            echo 'Day Trips';
        } else {
            if ($activeCat['RC_Category_Page_Title'] != '') {
                echo $activeCat['RC_Category_Page_Title'];
            } else {
                echo ($activeCat['RC_Name'] != '') ? $activeCat['RC_Name'] : $activeCat['C_Name'];
            }
        }
        ?>       
    </p>
</section>
<!-- Feature Stories-->
<?php if ($feature_count > 0 && $REGION['R_Stories'] == 1 && $blog['RC_Status'] == 0) { ?>
    <section class="feature-items">
        <?php
        while ($row_feature = mysql_fetch_array($result_feature)) {
            if ($row_feature['S_Thumbnail_Mobile'] != '') {
                $feature_image = $row_feature['S_Thumbnail_Mobile'];
            } else {
                $feature_image = $default_header_image;
            }
            ?>
            <div class="feature">
                <div class="thumbnail">
                    <a href="<?php echo DOMAIN_MOBILE_REL . 'story/' . clean($row_feature['S_Title']) . '/' . $row_feature['S_ID'] ?>/">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $feature_image; ?>" width="100%" height="auto" alt="<?php echo $row_feature['S_Title'] ?>" />
                    </a>
                </div>
                <a class="feature-title" href="<?php echo DOMAIN_MOBILE_REL . 'story/' . clean($row_feature['S_Title']) . '/' . $row_feature['S_ID'] ?>/">
                    <?php echo $row_feature['S_Title'] ?>
                </a>
                <?php
                if ($row_feature['S_Description'] != '') {
                    $string = preg_replace('/(.*)<\/p[^>]*>/i', '$1', $row_feature['S_Description']);
                    ?>
                    <div class="description">
                        <?php echo $string ?>
                        <a class="read-more" href="<?php echo DOMAIN_MOBILE_REL . 'story/' . clean($row_feature['S_Title']) . '/' . $row_feature['S_ID'] ?>/">
                            Read More
                        </a>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </section>
    <?php
}
// Feature Stories end
?>
<!--Thumbnail Grid-->
<section class="thumbnails">
    <?php
    //Events
    if ($activeCat['C_Parent'] == 8) {
        require_once 'events.php';
    }
    //Stories
    else if ($activeCat['C_Parent'] == 121) {
        require_once 'stories.php';
    }
    //Listings
    else {
        require_once 'listings.php';
    }
    ?>
</section>