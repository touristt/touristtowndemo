<?php
require_once '../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';

if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    if ($REGION['R_Parent'] == 0) {
        $sql = "SELECT * FROM tbl_Business_Listing_Photo
        WHERE BLP_BL_ID  = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $row = mysql_fetch_assoc($result);
        $REG = $row['BLP_R_ID'];
    } else {
        $REG = $REGION['R_ID'];
    }
    $sql = "SELECT * FROM tbl_Business_Listing 
        LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
        WHERE BL_ID  = '" . encode_strings($_REQUEST['id'], $db) . "' AND BLP_R_ID  = '" . encode_strings($REG, $db) . "'";
    $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeBiz = mysql_fetch_assoc($resultTMP);
} else {
    header("Location: " . DOMAIN_MOBILE_REL . "404.php");
    exit();
}
if (isset($_POST['confirmlisting'])) {
    $sql_update_hash = "UPDATE tbl_Business_Listing SET BL_Free_Listing_status = 0, BL_Free_Listing_Status_Date = '0000-00-00' 
                        WHERE BL_ID = " . $activeBiz['BL_ID'];
    mysql_query($sql_update_hash);
    $_SESSION['update_hash'] = 1;
}

$SEOtitle = $activeBiz['BL_SEO_Title'] ? $activeBiz['BL_SEO_Title'] : $activeBiz['BL_Listing_Title'];
$SEOdescription = $activeBiz['BL_SEO_Description'];
$SEOkeywords = $activeBiz['BL_SEO_Keywords'];

//Getting Region Theme
$getThemeOnListing = "SELECT TO_Default_Header_Mobile FROM tbl_Theme_Options WHERE TO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
$themeRegionOnListing = mysql_query($getThemeOnListing, $db) or die("Invalid query: $getThemeOnListing -- " . mysql_error());
$THEMEOnListing = mysql_fetch_assoc($themeRegionOnListing);

//Defualt Thumbnail Images
if ($THEMEOnListing['TO_Default_Header_Mobile'] != '') {
    $default_header_listing_image = $THEMEOnListing['TO_Default_Header_Mobile'];
} else {
    $default_header_listing_image = 'Default-Main-Mobile.jpg';
}

//OG Tags for FB Share
$OG_URL = curPageURL();
$OG_Type = 'article';
$OG_Title = $activeBiz['BL_SEO_Title'] ? $activeBiz['BL_SEO_Title'] : $activeBiz['BL_Listing_Title'];
$OG_Description = $activeBiz['BL_SEO_Description'];
$OG_Image = ($activeBiz['BLP_Mobile_Header_Image'] != "") ? 'http://' . DOMAIN . IMG_LOC_REL . $activeBiz['BLP_Mobile_Header_Image'] : 'http://' . DOMAIN . IMG_LOC_REL . $default_header_listing_image;
$OG_Image_Width = 715;
$OG_Image_Height = 400;
require_once 'include/public/header.php';

//If the listing is free we have to show this snippet code
if ($activeBiz['hide_show_listing'] == 0) {
    $check_hide_status = $url[3];
    if ($check_hide_status == "") {
        ?>
        <section class="thumbnails">
            <div class="thumbnails-inner">
                <div class="thumbnail">
                    <p class="listing">
                        This listing has been disabled.
                    </p>
                </div>
            </div>
        </section>       
        <?php
        require 'include/public/footer.php';
        exit();
    }
    $startTime = date("h:i", strtotime('+5 minutes', $check_hide_status));
    $expire_time_check = date("h:i");
    if ($expire_time_check < $startTime) {
        
    } else {
        ?>
        <section class="thumbnails">
            <div class="thumbnails-inner">
                <div class="thumbnail">
                    <p class="listing">
                        This listing has been disabled.
                    </p>
                </div>
            </div>
        </section> 
        <?php
        require 'include/public/footer.php';
        exit();
    }
}
if ($activeBiz['BL_Listing_Type'] == 1) {
    if ($activeBiz['BLP_Mobile_Photo'] != '') {
        $listing_image = $activeBiz['BLP_Mobile_Photo'];
    } else {
        $listing_image = $default_thumbnail_image;
    }
    ?>
    <section class="detail-page">
        <div class="main_image">
            <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo $activeBiz['BL_Listing_Title'] ?>" longdesc="<?php echo $activeBiz['BL_Listing_Title'] ?>"/>
        </div>
        <?php
        $freeListing = "SELECT RC_Name, C_Name, BL_Free_Listing_Status, BL_Free_Listing_Status_Hash
                        FROM tbl_Business_Listing
                        LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID
                        LEFT JOIN tbl_Category ON BLC_C_ID = C_ID
                        LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID
                        AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                        WHERE BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'";
        $freeRes = mysql_query($freeListing);
        $freeRow = mysql_fetch_assoc($freeRes);
        ?>
        <div class="listing-main">
            <div class="title">
                <?php echo $activeBiz['BL_Listing_Title'] ?>
            </div>
            <div class="other"><?php echo (($freeRow['RC_Name'] != "") ? $freeRow['RC_Name'] : $freeRow['C_Name']) ?></div>
        </div>
        <div class="listing-address">
            <div class="listing-address-inner">
                <?php if ($activeBiz['BL_Street'] != '' || $activeBiz['BL_Town'] != '' || $activeBiz['BL_Province'] != '' || $activeBiz['BL_PostalCode'] != '') { ?>
                    <div class="info">
                        <div class="icon">
                            <img src="<?php echo 'http://' . DOMAIN . IMG_MOBILE_REL . 'location.png'; ?>" alt="Location">
                        </div>
                        <div class="address-heading">
                            <?php echo $activeBiz['BL_Street'] . ', ' . $activeBiz['BL_Town'] . ' ' . $activeBiz['BL_Province'] . ' ' . $activeBiz['BL_PostalCode']; ?>
                        </div>          
                    </div>
                    <?php
                }
                if ($REGION['R_Show_Hide_Phone'] == 1 && $activeBiz['BL_Phone'] != "") {
                    ?>
                    <div class="info">
                        <div class="icon">
                            <img src="<?php echo 'http://' . DOMAIN . IMG_MOBILE_REL . 'Listing-Phone.png'; ?>" alt="Phone">
                        </div>
                        <div class="address-heading"><?php echo $activeBiz['BL_Phone'] ?></div> 
                    </div>
                    <?php
                }
                if ($REGION['R_Show_Hide_Email'] == 1 && $activeBiz['BL_Email'] != "") {
                    ?>
                    <div class="info">
                        <div class="icon">
                            <img src="<?php echo 'http://' . DOMAIN . IMG_MOBILE_REL . 'Listing-Email.png'; ?>" alt="Email">
                        </div>
                        <div class="address-heading">
                            <a href="mailto:<?php echo $activeBiz['BL_Email'] ?>"><?php echo $activeBiz['BL_Email'] ?></a>
                        </div>
                    </div>
                <?php } ?>     
                <?php if (isset($_REQUEST['hash']) && $_REQUEST['hash'] == $freeRow['BL_Free_Listing_Status_Hash'] && $freeRow['BL_Free_Listing_Status'] == 1) { ?>
                    <div class="info">
                        <div class="icon">
                        </div>
                        <div class="address-heading">
                            <form action="" method ="post">
                                <input type="hidden" name="con" value="<?php echo $_REQUEST['hash']; ?>">
                                <h3 class="address-heading">
                                    <input type="submit" name="confirmlisting" value="Confirm Listing">
                                </h3>  
                            </form>
                        </div>
                    </div>   
                    <?php
                } elseif (isset($_REQUEST['hash']) && $_REQUEST['hash'] == $freeRow['BL_Free_Listing_Status_Hash'] && $freeRow['BL_Free_Listing_Status'] == 0) {
                    print '<script>swal("Thank you", "You have already confirmed your Listing.", "success");</script>';
                }
                ?>
            </div>
        </div>
    </section> 
    <?php
    if (isset($_SESSION['update_hash']) && $_SESSION['update_hash'] == 1) {
        print '<script>swal("Thank you", "Your Listing has been confirmed.", "success");</script>';
        unset($_SESSION['update_hash']);
    }
    require 'include/public/footer.php';
    exit();
}

function ctime($myTime, $end = false) {
    if ($myTime == '00:00:01' && !$end) {
        return 'Closed';
    } elseif ($myTime == '00:00:02') {
        return 'By Appointment';
    } elseif ($myTime == '00:00:03') {
        return 'Open 24 Hours';
    } else {
        $mySplit = explode(':', $myTime);
        return date('g:ia', mktime($mySplit[0], $mySplit[1], 1, 1, 1));
    }
}

//Getting Gallery Images if active
$count_gallery = 0;
$sqlFG = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 2 AND BLF_Active = 1";
$resultFG = mysql_query($sqlFG, $db) or die("Invalid query: $sqlFG -- " . mysql_error());
if (mysql_num_rows($resultFG) > 0) {
    $sqlG = "SELECT BFP_Title, BFP_Photo_710X440_Mobile FROM tbl_Business_Feature_Photo WHERE BFP_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' ORDER BY BFP_Order";
    $resultG = mysql_query($sqlG, $db) or die("Invalid query: $sqlG -- " . mysql_error());
    $count_gallery = mysql_num_rows($resultG);
}

//Getting Social Media links
$sqlSM = "SELECT * FROM tbl_Business_Social WHERE BS_BL_ID = " . $activeBiz['BL_ID'] . "";
$resultSM = mysql_query($sqlSM);
$rowSM = mysql_fetch_assoc($resultSM);

//Getting Amenities
$sqlAI = "SELECT *  
        FROM tbl_Business_Listing_Ammenity 
        LEFT JOIN tbl_Ammenity_Icons ON AI_ID = BLA_BA_ID 
        WHERE BLA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND AI_Image <> '' ORDER BY AI_Name";
$resultAI = mysql_query($sqlAI, $db);
$AIcount = mysql_num_rows($resultAI);

//Getting files to download
$sqlPDF = "SELECT * FROM tbl_Description_PDF WHERE D_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' ORDER BY D_PDF_Order ASC";
$resultPDF = mysql_query($sqlPDF, $db);
$PDFcount = mysql_num_rows($resultPDF);
?>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1356712584397235";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<?php
if ($activeBiz['BLP_Mobile_Header_Image'] != '') {
    $listing_image = $activeBiz['BLP_Mobile_Header_Image'];
} else {
    $listing_image = $default_header_image;
}
?>
<section class="detail-page">
    <div class="main_image">
        <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo $activeBiz['BL_Photo_Alt'] ?>" longdesc="<?php echo $activeBiz['BL_Listing_Title'] ?>"/>
    </div>
    <div class="listing-main">
        <div class="title">
            <?php echo $activeBiz['BL_Listing_Title'] ?>
        </div>
        <div class="other">
            <?php
            if ($activeBiz['BL_Town'] != '') {
                echo $activeBiz['BL_Town'];
            } if ($activeBiz['BL_Province'] != '') {
                echo ', ' . $activeBiz['BL_Province'];
            }
            if ($activeBiz['BL_Country'] != '') {
                echo ', ' . $activeBiz['BL_Country'];
            }
            ?>
        </div>
        <div class="other">
            <div class="fb-share-button" data-href="<?php print curPageURL(); ?>" data-layout="button_count"  data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php print curPageURL(); ?>%2F&amp;src=sdkpreparse">Share</a></div>
            <iframe class="twitter-class"
                    src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=http%3A%2F%2F<?php print curPageURL(); ?>&related=twitterapi%2Ctwitter&text=<?php echo $activeBiz['BL_Listing_Title'] ?> in <?php echo $REGION['R_Name'] ?>"
                    width="70"
                    height="25"
                    style="border: 0; overflow: hidden; float: left;">
            </iframe>
        </div>
    </div>
    <div class="listing-description">
        <?php echo $activeBiz['BL_Description']; ?>
    </div>
    <?php if ($count_gallery > 0) { ?>
        <div class="gallery">
            <?php while ($rowG = mysql_fetch_array($resultG)) { ?>
                <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $rowG['BFP_Photo_710X440_Mobile']; ?>" alt="<?php echo $rowG['BFP_Title'] ?>" />
            <?php } ?>
        </div>
    <?php }if ($rowSM['BS_Enabled'] > 0) { ?>
        <div class="social-media">
            <?php
            if (isset($rowSM['BS_FB_Link']) && $rowSM['BS_FB_Link'] != '') {
                $facebook_link = preg_replace('/^www\./', '', $rowSM['BS_FB_Link']);
                ?>
                <a target="_blank" href="<?php echo 'http://' . str_replace(array('http://', 'https://'), '', $facebook_link); ?>">
                    <img src="<?php echo 'http://' . DOMAIN . '/images/facebook.png'; ?>" alt="Facebook" />
                </a>
                <?php
            } if (isset($rowSM['BS_I_Link']) && $rowSM['BS_I_Link'] != '') {
                $rs_i_link = preg_replace('/^www\./', '', $rowSM['BS_I_Link']);
                ?>
                <a target="_blank" href="<?php echo 'http://' . str_replace(array('http://', 'https://'), '', $rs_i_link); ?>">
                    <img src="<?php echo 'http://' . DOMAIN . '/images/instagram.png'; ?>" alt="Instagram" />
                </a>
                <?php
            } if (isset($rowSM['BS_T_Link']) && $rowSM['BS_T_Link'] != '') {
                $rs_t_link = preg_replace('/^www\./', '', $rowSM['BS_T_Link']);
                ?>
                <a target="_blank" href="<?php echo 'http://' . str_replace(array('http://', 'https://'), '', $rs_t_link); ?>">
                    <img src="<?php echo 'http://' . DOMAIN . '/images/twitter.png'; ?>" alt="Twitter" />
                </a>
            <?php } ?>
        </div>
    <?php } ?>
    <div class="listing-address">
        <div class="listing-address-inner">
            <?php if ($activeBiz['BL_Street'] != '' || $activeBiz['BL_Town'] != '' || $activeBiz['BL_Province'] != '' || $activeBiz['BL_PostalCode'] != '') { ?>
                <div class="info">
                    <div class="icon">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_MOBILE_REL . 'location.png'; ?>" alt="Location">
                    </div>
                    <div class="address-heading">
                        <?php echo $activeBiz['BL_Street'] . ', ' . $activeBiz['BL_Town'] . ' ' . $activeBiz['BL_Province'] . ' ' . $activeBiz['BL_PostalCode']; ?>
                    </div>          
                </div>
                <?php
            }
            if ($REGION['R_Show_Hide_Phone'] == 1) {
                if ($activeBiz['BL_Phone'] != "") {
                    ?>
                    <div class="info">
                        <div class="icon">
                            <img src="<?php echo 'http://' . DOMAIN . IMG_MOBILE_REL . 'Listing-Phone.png'; ?>" alt="Phone">
                        </div>
                        <div class="address-heading"><?php echo $activeBiz['BL_Phone'] ?></div> 
                    </div>
                    <?php
                }
            }
            if ($activeBiz['BL_Website'] != '') {
                ?>
                <div class="info">
                    <div class="icon">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_MOBILE_REL . 'Listing-Website.png'; ?>" alt="Website">
                    </div>
                    <div class="address-heading">
                        <a href="<?php echo 'http://' . str_replace(array('http://', 'https://'), '', $activeBiz['BL_Website']); ?>">Visit Website</a>
                    </div> 
                </div>
                <?php
            }if ($REGION['R_Show_Hide_Email'] == 1) {
                if ($activeBiz['BL_Email'] != "") {
                    ?>
                    <div class="info">
                        <div class="icon">
                            <img src="<?php echo 'http://' . DOMAIN . IMG_MOBILE_REL . 'Listing-Email.png'; ?>" alt="Email">
                        </div>
                        <div class="address-heading">
                            <a href="mailto:<?php echo $activeBiz['BL_Email'] ?>">Send Email</a>
                        </div>
                    </div>
                    <?php
                }
            }
            if (!$activeBiz['BL_Hours_Disabled']) {
                if ($activeBiz['BL_Hours_Appointment'] == 1 || $activeBiz['BL_Hour_Mon_From'] != '00:00:00' || $activeBiz['BL_Hour_Tue_From'] != '00:00:00' || $activeBiz['BL_Hour_Wed_From'] != '00:00:00' || $activeBiz['BL_Hour_Thu_From'] != '00:00:00' || $activeBiz['BL_Hour_Fri_From'] != '00:00:00' || $activeBiz['BL_Hour_Sat_From'] != '00:00:00' || $activeBiz['BL_Hour_Sun_From'] != '00:00:00') {
                    ?>
                    <div class="info">
                        <div class="icon">
                            <img src="<?php echo 'http://' . DOMAIN . IMG_MOBILE_REL . 'Listing-Hours.png'; ?>" alt="Hours">
                        </div>
                        <div class="address-heading">
                            <a onclick="show_hide(1)">View Hours</a>
                            <div class="accordion hours">
                                <?php
                                if ($activeBiz['BL_Hours_Appointment']) {
                                    ?>
                                    <div class="accordion-heading">Hours</div>
                                    <div class="accordion-data">By Appointment</div>
                                    <?php
                                } else {
                                    if ($activeBiz['BL_Hour_Mon_From'] != '00:00:00') {
                                        ?>
                                        <div class="accordion-heading">Monday</div>
                                        <div class="accordion-data">
                                            <?php echo ctime($activeBiz['BL_Hour_Mon_From']); ?> <?php echo $activeBiz['BL_Hour_Mon_From'] == '00:00:01' || $activeBiz['BL_Hour_Mon_From'] == '00:00:02' || $activeBiz['BL_Hour_Mon_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Mon_To'], true); ?>
                                        </div>
                                    <?php } if ($activeBiz['BL_Hour_Tue_From'] != '00:00:00') { ?>
                                        <div class="accordion-heading">Tuesday</div>
                                        <div class="accordion-data">
                                            <?php echo ctime($activeBiz['BL_Hour_Tue_From']); ?> <?php echo $activeBiz['BL_Hour_Tue_From'] == '00:00:01' || $activeBiz['BL_Hour_Tue_From'] == '00:00:02' || $activeBiz['BL_Hour_Tue_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Tue_To'], true); ?>
                                        </div>
                                    <?php } if ($activeBiz['BL_Hour_Wed_From'] != '00:00:00') { ?>
                                        <div class="accordion-heading">Wednesday</div>
                                        <div class="accordion-data">
                                            <?php echo ctime($activeBiz['BL_Hour_Wed_From']); ?> <?php echo $activeBiz['BL_Hour_Wed_From'] == '00:00:01' || $activeBiz['BL_Hour_Wed_From'] == '00:00:02' || $activeBiz['BL_Hour_Wed_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Wed_To'], true); ?>
                                        </div>
                                    <?php } if ($activeBiz['BL_Hour_Thu_From'] != '00:00:00') { ?>
                                        <div class="accordion-heading">Thursday</div>
                                        <div class="accordion-data">
                                            <?php echo ctime($activeBiz['BL_Hour_Thu_From']); ?> <?php echo $activeBiz['BL_Hour_Thu_From'] == '00:00:01' || $activeBiz['BL_Hour_Thu_From'] == '00:00:02' || $activeBiz['BL_Hour_Thu_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Thu_To'], true); ?>
                                        </div>
                                    <?php } if ($activeBiz['BL_Hour_Fri_From'] != '00:00:00') { ?>
                                        <div class="accordion-heading">Friday</div>
                                        <div class="accordion-data">
                                            <?php echo ctime($activeBiz['BL_Hour_Fri_From']); ?> <?php echo $activeBiz['BL_Hour_Fri_From'] == '00:00:01' || $activeBiz['BL_Hour_Fri_From'] == '00:00:02' || $activeBiz['BL_Hour_Fri_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Fri_To'], true); ?>
                                        </div>
                                    <?php } if ($activeBiz['BL_Hour_Sat_From'] != '00:00:00') { ?>
                                        <div class="accordion-heading">Saturday</div>
                                        <div class="accordion-data">
                                            <?php echo ctime($activeBiz['BL_Hour_Sat_From']); ?> <?php echo $activeBiz['BL_Hour_Sat_From'] == '00:00:01' || $activeBiz['BL_Hour_Sat_From'] == '00:00:02' || $activeBiz['BL_Hour_Sat_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Sat_To'], true); ?>
                                        </div>
                                    <?php } if ($activeBiz['BL_Hour_Sun_From'] != '00:00:00') { ?>
                                        <div class="accordion-heading">Sunday</div>
                                        <div class="accordion-data">
                                            <?php echo ctime($activeBiz['BL_Hour_Sun_From']); ?> <?php echo $activeBiz['BL_Hour_Sun_From'] == '00:00:01' || $activeBiz['BL_Hour_Sun_From'] == '00:00:02' || $activeBiz['BL_Hour_Sun_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Sun_To'], true); ?>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>   
                        </div>
                    </div>
                    <?PHP
                }
            }
            if ($AIcount > 0) {
                ?>
                <div class="info">
                    <div class="icon">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_MOBILE_REL . 'Listing-Ammenity.png'; ?>" alt="Ammenity">
                    </div>
                    <div class="address-heading">
                        <a onclick="show_hide(2)">View Amenities</a>
                        <div class="accordion amenities">
                            <?php
                            while ($rowAI = mysql_fetch_array($resultAI)) {
                                ?>
                                <div class="accordion-data"><?php echo $rowAI['AI_Name'] ?></div>
                                <?php
                            }
                            ?>
                        </div>   
                    </div>
                </div>
                <?PHP
            }
            if ($PDFcount > 0) {
                ?>
                <div class="info">
                    <div class="icon">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_MOBILE_REL . 'Listing-Download.png'; ?>" alt="Downloads">
                    </div>
                    <div class="address-heading">
                        <a onclick="show_hide(3)">View Downloads</a>
                        <div class="accordion downloads">
                            <?php
                            while ($rowPDF = mysql_fetch_array($resultPDF)) {
                                ?>
                                <div class="accordion-data">
                                    <a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $rowPDF['D_PDF']; ?>"><?php echo $rowPDF['D_PDF_Title'] ?></a>
                                </div>
                                <?php
                            }
                            ?>
                        </div>   
                    </div>
                </div>
                <?PHP
            }
            ?>          
        </div>
    </div>
    <?php
    if ($activeBiz['BL_Lat'] != '' && $activeBiz['BL_Long']) {
        $markers = array();
        //add to markers for map
        if ($activeBiz['BLP_Mobile_Photo'] != '') {
            $image = '<div class="thumbnail"><img width="130" height="90" src="http://' . DOMAIN . (IMG_LOC_REL . $activeBiz['BLP_Mobile_Photo']) . '" alt="' . htmlspecialchars(trim($activeBiz['BL_Listing_Title']), ENT_QUOTES) . '" /></div>';
        } else {
            $image = '<div class="thumbnail"><img src="http://' . DOMAIN . (IMG_LOC_REL . $default_thumbnail_image) . '" alt="' . htmlspecialchars(trim($activeBiz['BL_Listing_Title']), ENT_QUOTES) . '" /></div>';
        }
        $markers[] = array(
            'id' => $activeBiz['BL_ID'],
            'lat' => $activeBiz['BL_Lat'],
            'lon' => $activeBiz['BL_Long'],
            'name' => $activeBiz['BL_Listing_Title'],
            'path' => '/profile/' . $activeBiz['BL_Name_SEO'] . '/' . $activeBiz['BL_ID'] . '/',
            'main_photo' => $image,
            'address' => htmlspecialchars(trim($activeBiz['BL_Street']), ENT_QUOTES),
            'town' => htmlspecialchars(trim($activeBiz['BL_Town']), ENT_QUOTES)
        );
        $markers = json_encode($markers);
        ?>
        <div id="map_canvas" style="width:100%;height: 390px;margin-bottom:10px;"></div>
        <?php
        $map_center['latitude'] = $activeBiz['BL_Lat'];
        $map_center['longitude'] = $activeBiz['BL_Long'];
        $map_center['zoom'] = $REGION['R_Zoom'];
        $kml_json = json_encode(array());
        require_once 'map_script.php';
    }
    ?>
</section> 
<?php require 'include/public/footer.php'; ?>
