<?php
require_once '../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';

//search by each word in the phrase except common words
$common_words = array('a', 'an', 'the', 'is', 'are', 'be', 'to', 'of', 'and', 'in', 'that', 'have', 'i', 'it', 'for', 'not', 'on',
    'with', 'he', 'as', 'you', 'do', 'at', 'this', 'but', 'his', 'by', 'from', 'they', 'we', 'her', 'she', 'or',
    'will', 'my', 'one', 'all', 'would', 'there', 'their', 'what', 'so', 'up', 'out', 'if', 'about', 'who', 'get',
    'which', 'go', 'me', 'when', 'make', 'can', 'like', 'no', 'just', 'him', 'know', 'take', 'into', 'your', 'good',
    'some', 'could', 'them', 'other', 'then', 'than', 'now', 'look', 'only', 'come', 'its', 'over', 'think', 'also',
    'back', 'after', 'use', 'two', 'how', 'our', 'first', 'well', 'way', 'even', 'because', 'any', 'these', 'give',
    'most', 'us');
require_once 'include/public/header.php';

$searchstring = array();
if (strlen($_GET['txtSearch']) > 3) {
    //search stories
    $searchtext = explode(' ', $_GET['txtSearch']);
    foreach ($searchtext as $char) {
        if (!in_array($char, $common_words)) {
            $searchstring[] = $char;
        }
    }
    $Count = count($searchstring);
    foreach ($searchstring as $char) {
        $temp++;
        if ($temp == 1) {
            $sql = "SELECT DISTINCTROW S_ID, S_Title, S_Thumbnail_Mobile FROM tbl_Story 
                    LEFT JOIN tbl_Story_Region ON SR_S_ID = S_ID 
                    LEFT JOIN tbl_Content_Piece ON CP_S_ID = S_ID
                    WHERE SR_R_ID = " . $REGION['R_ID'] . " AND S_Active = 1 
                    AND (S_Title LIKE '%" . encode_strings($char, $db) . "%' OR
                    S_Description LIKE '%" . encode_strings($char, $db) . "%' OR
                    CP_Title LIKE '%" . encode_strings($char, $db) . "%' OR
                    CP_Description LIKE '%" . encode_strings($char, $db) . "%'";
        } else {
            $sql .= "OR S_Title LIKE '%" . encode_strings($char, $db) . "%' OR
                      S_Description LIKE '%" . encode_strings($char, $db) . "%' OR
                      CP_Title LIKE '%" . encode_strings($char, $db) . "%' OR
                      CP_Description LIKE '%" . encode_strings($char, $db) . "%'";
        }
        if ($temp == $Count) {
            $sql .= ") GROUP BY S_ID ORDER BY S_ID DESC";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        }
    }
    if ($Count > 0 && mysql_num_rows($result) > 0) {
        //Main Title Start
        print '<section class="main_title">
                  <p>Stories</p>
              </section>';
        print '<section class="thumbnails">';
        while ($row = mysql_fetch_assoc($result)) {
            if ($row['S_Thumbnail_Mobile'] != '') {
                $story_image = $row['S_Thumbnail_Mobile'];
            } else {
                $story_image = $default_thumbnail_image;
            }
            ?>
            <div class="thumbnails-inner">
                <div class="thumbnail">
                    <a href="<?php echo DOMAIN_MOBILE_REL . 'story/' . clean($row['S_Title']) . '/' . $row['S_ID'] ?>/">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $story_image; ?>" alt="<?php echo $row['S_Title'] ?>" />
                        <div class="title"><p><?php echo ($row['S_Title'] != '') ? $row['S_Title'] : ''; ?></p></div>
                    </a> 
                </div>
            </div>
            <?php
        }
        print '</section>';
    }

    //search listings
    $exact_phrase = array();
    $exact_phrase_blid = array();
    $not_exact_phrase = array();
    if ($REGION['R_Parent'] == 0) {
        $REG = '';
    } else {
        $REG = "AND BLP_R_ID = " . $REGION['R_ID'];
    }
    //exact phrase search
    $sql = "SELECT DISTINCTROW BL_ID, BL_Name_SEO, BLP_Mobile_Photo, BL_Photo_Alt, BL_Listing_Title FROM tbl_Business_Listing 
            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
            LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
            LEFT JOIN tbl_Business_Listing_Category_Region ON  BLC_BL_ID = BLCR_BL_ID
            LEFT JOIN tbl_Region_Category ON RC_C_ID = BLC_C_ID AND RC_R_ID= '" . encode_strings($REGION['R_ID'], $db) . "'
            LEFT JOIN tbl_Category ON RC_C_ID = C_ID
            WHERE BLCR_BLC_R_ID " . encode_strings((($REGION['R_Parent'] == 0 && $REGION['R_Type'] != 4 ) ? "IN " . $regionList : "= " . $REGION['R_ID']), $db) . "
            AND BL_Listing_Title = '" . encode_strings($_GET['txtSearch'], $db) . "'
            AND hide_show_listing = '1' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND RC_Status =0 $include_free_listings $REG GROUP BY BL_ID";
    $resultListing = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($row = mysql_fetch_assoc($resultListing)) {
        $exact_phrase[] = $row;
        $exact_phrase_blid[] = $row['BL_ID'];
    }

    $temp = 0;
    foreach ($searchstring as $char) {
        $temp++;
        if ($temp == 1) {
            $sql = "SELECT DISTINCTROW BL_ID, BL_Name_SEO, BLP_Mobile_Photo, BL_Photo_Alt, BL_Listing_Title FROM tbl_Business_Listing 
                        LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
                        LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
                        LEFT JOIN tbl_Business_Listing_Category_Region ON  BLC_BL_ID = BLCR_BL_ID
                        LEFT JOIN tbl_Region_Category ON RC_C_ID = BLC_C_ID AND RC_R_ID= '" . encode_strings($REGION['R_ID'], $db) . "'
                        LEFT JOIN tbl_Category ON RC_C_ID = C_ID
                        WHERE BLCR_BLC_R_ID " . encode_strings((($REGION['R_Parent'] == 0 && $REGION['R_Type'] != 4 ) ? "IN " . $regionList : "= " . $REGION['R_ID']), $db) . " $SEASONS_WHERE $REG 
                        AND (BL_Listing_Title LIKE '%" . encode_strings($char, $db) . "%' OR
                        BL_Description LIKE '%" . encode_strings($char, $db) . "%' OR
                        BL_Search_Words LIKE '%" . encode_strings($char, $db) . "%' ";
        } else {
            $sql .= "OR BL_Listing_Title LIKE '%" . encode_strings($char, $db) . "%' OR
                        BL_Description LIKE '%" . encode_strings($char, $db) . "%' OR
                        BL_Search_Words LIKE '%" . encode_strings($char, $db) . "%' ";
        }
        if ($temp == $Count) {
            $sql .= ") AND hide_show_listing='1' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND RC_Status =0 $include_free_listings GROUP BY BL_ID ORDER BY ";
        }
    }
    $temp = 0;
    foreach ($searchstring as $char) {
        $temp++;
        if ($temp != 1) {
            $sql .= "+";
        }
        $sql .= "IF(BL_Listing_Title LIKE '%" . encode_strings($char, $db) . "%',1,0)+
                    IF(BL_Description LIKE '%" . encode_strings($char, $db) . "%',1,0)+
                    IF(BL_Search_Words LIKE '%" . encode_strings($char, $db) . "%',1,0)";
        if ($temp == $Count) {
            $sql .= " DESC, BL_Points DESC";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        }
    }
    while ($row = mysql_fetch_assoc($result)) {
        if (!in_array($row['BL_ID'], $exact_phrase_blid)) {
            $not_exact_phrase[] = $row;
        }
    }
    $listings = array_merge($exact_phrase, $not_exact_phrase);
    if (count($listings) > 0) {
        //Main Title Start
        print '<section class="main_title">
                  <p>Businesses</p>
              </section>';
        print '<section class="thumbnails">';
        foreach ($listings as $row) {
            if ($row['BLP_Mobile_Photo'] != '') {
                $listing_image = $row['BLP_Mobile_Photo'];
            } else {
                $listing_image = $default_thumbnail_image;
            }
            ?>
            <div class="thumbnails-inner">
                <div class="thumbnail">
                    <a href="<?php echo DOMAIN_MOBILE_REL . 'profile/' . $row['BL_Name_SEO'] . '/' . $row['BL_ID'] ?>/">
                        <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo $row['BL_Photo_Alt'] ?>" longdesc="<?php echo $row['BL_Listing_Title'] ?>"/>
                        <div class="title"><p><?php echo $row['BL_Listing_Title'] ?></p></div>
                    </a> 
                </div>
            </div>
            <?php
        }
        print '</section>';
    } else {
        print '<section class="thumbnails">
                <div class="thumbnails-inner">
                  <div class="thumbnail">
                    <p class="listing">Your search request found no businesses. Please try again.</p>
                  </div>
                </div>
              </section>';
    }
} else {
    print '<section class="thumbnails">
              <div class="thumbnails-inner">
                <div class="thumbnail">
                  <p class="listing">Search Term must be more than three (3) characters.</p>
                </div>
              </div>
          </section>';
}
require_once 'include/public/footer.php';
?>
