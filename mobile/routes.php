<?php
require_once '../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';

$sqlRoute = "SELECT R_Name, R_Description, R_SEO_Title, R_SEO_Description, R_SEO_Keywords FROM tbl_Route WHERE R_RID = '" . encode_strings($REGION['R_ID'], $db) . "'";
$resRoute = mysql_query($sqlRoute, $db) or die("Invalid query: $sqlRoute -- " . mysql_error());
$Routes = mysql_fetch_assoc($resRoute);
$SEOtitle = $Routes['R_SEO_Title'];
$SEOdescription = $Routes['R_SEO_Description'];
$SEOkeywords = $Routes['R_SEO_Keywords'];

$sqlRouteCat = "SELECT RC_ID, RC_Name, RC_Mobile_Photo FROM tbl_Route_Category 
                WHERE RC_R_ID ='" . encode_strings($REGION['R_ID'], $db) . "' AND RC_Status = 1
                ORDER BY RC_Order ASC";
$resRouteCat = mysql_query($sqlRouteCat, $db) or die("Invalid query: $sqlRouteCat -- " . mysql_error());
?>
<?php require_once 'include/public/header.php'; ?>
<!--Main Title Start-->
<section class="main_title">
    <p><?php echo $Routes['R_Name'] ?></p>
</section>
<!--Description Start-->
<section class="description">
    <?PHP if ($Routes['R_Description']) { ?>
        <p><?PHP echo ($Routes['R_Description']); ?></p>
    <?PHP } ?>
</section>
<!--Description End-->
<!-- Routes Categories -->
<section class="thumbnails">
    <?php while ($rowRouteCat = mysql_fetch_array($resRouteCat)) { ?>
        <div class="thumbnails-inner">
            <div class="thumbnail">
                <a href="<?php echo DOMAIN_MOBILE_REL . 'routes/' . clean($rowRouteCat['RC_Name']) . '/' . $rowRouteCat['RC_ID']; ?>/">
                    <?php if (isset($rowRouteCat['RC_Mobile_Photo']) && $rowRouteCat['RC_Mobile_Photo'] != '') { ?>
                        <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $rowRouteCat['RC_Mobile_Photo']; ?>" alt="<?php echo $rowRouteCat['RC_Name'] ?>"/>
                    <?php } else { ?>  
                        <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $default_thumbnail_image ?>" alt="<?php echo $rowRouteCat['RC_Name'] ?>"/>
                    <?php } ?>
                    <div class="title index">
                        <p><?php echo $rowRouteCat['RC_Name'] ?></p>
                    </div>
                </a>
            </div>
        </div>
    <?php } ?>
</section>
<!-- Routes Categories end -->
<?php require_once 'include/public/footer.php'; ?>