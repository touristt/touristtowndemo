<?php
require_once '../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';

if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $routeCatID = $_REQUEST['id'];
} else {
    header("Location: " . DOMAIN_MOBILE_REL . "404.php");
    exit();
}
$sqlRouteCat = "SELECT RC_ID, RC_Name, RC_Description, RC_SEO_Title, RC_SEO_Description, RC_SEO_Keywords FROM tbl_Route_Category
                WHERE RC_ID = '" . encode_strings($routeCatID, $db) . "'";
$resRouteCat = mysql_query($sqlRouteCat, $db) or die("Invalid query: $sqlRouteCat -- " . mysql_error());
$RouteCat = mysql_fetch_assoc($resRouteCat);
$SEOtitle = $RouteCat['RC_SEO_Title'];
$SEOdescription = $RouteCat['RC_SEO_Description'];
$SEOkeywords = $RouteCat['RC_SEO_Keywords'];

$sqlRoute = "SELECT IR_ID, IR_Title, IR_Mobile_Photo FROM tbl_Individual_Route 
            INNER JOIN tbl_Individual_Route_Category ON IR_ID = IRC_IR_ID 
            WHERE IRC_RC_ID = '" . encode_strings($RouteCat['RC_ID'], $db) . "'
            GROUP BY IR_ID
            ORDER BY IRC_Order ASC";
$resRoute = mysql_query($sqlRoute, $db) or die("Invalid query: $sqlRoute -- " . mysql_error());
?>
<?php require_once 'include/public/header.php'; ?>
<!--Main Title Start-->
<section class="main_title">
    <p><?php echo $RouteCat['RC_Name'] ?></p>
</section>
<!--Description Start-->
<section class="description">
    <?PHP if ($RouteCat['RC_Description']) { ?>
        <p><?PHP echo ($RouteCat['RC_Description']); ?></p>
    <?PHP } ?>
</section>
<!--Description End-->
<!-- Routes in Categories -->

<section class="thumbnails">
    <?php
    while ($rowRoute = mysql_fetch_array($resRoute)) {
        if ($rowRoute['IR_Mobile_Photo'] != '') {
            $route_cat_image = $rowRoute['IR_Mobile_Photo'];
        } else {
            $route_cat_image = $default_thumbnail_image;
        }
        ?>
        <div class="thumbnails-inner">
            <div class="thumbnail">
                <a href="<?php echo DOMAIN_MOBILE_REL . 'route/' . clean($rowRoute['IR_Title']) . '/' . $rowRoute['IR_ID']; ?>/">
                    <img src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $route_cat_image; ?>" alt="<?php echo $rowRoute['IR_Title'] ?>"/>
                    <div class="title index">
                        <p><?php echo $rowRoute['IR_Title'] ?></p>
                    </div>
                </a>
            </div>
        </div>
    <?php } ?>
</section>
<!-- Routes in Categories end -->
<?php require_once 'include/public/footer.php'; ?>