<?php
require_once '../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';

if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $sql = "SELECT EventID, Title, EventType, EventDateStart, Event_Image, EventDateEnd, Content, EventStartTime, EventEndTime, EventTimeAllDay, E_Region_ID, Recuring, 
            Organization, OrganizationID, LocationID, LocationList, StreetAddress, ContactName, ContactPhone, ContactPhoneTollFree, Email, WebSiteLink, 
            EventPdf, EO_Name, EL_Name, EL_Street, EL_Town FROM Events_master 
            LEFT JOIN Events_Location ON EL_ID = LocationID 
            LEFT JOIN Events_Organization ON EO_ID = OrganizationID
            WHERE EventID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeEvent = mysql_fetch_assoc($result);
} else {
    header("Location: " . DOMAIN_MOBILE_REL . "404.php");
    exit();
}
//OG Tags for FB Share
$eventdates = '';
if ($activeEvent['EventType'] == '3') {
    $eventdates .= date('l', strtotime($activeEvent['EventDateStart'])) . 's' . date('F j, Y', strtotime($activeEvent['EventDateStart']));
} elseif ($activeEvent['EventType'] == '2') {
    $eventdates .= date('F j, Y', strtotime($activeEvent['EventDateStart']));
} else {
    $eventdates .= date('l, F j, Y', strtotime($activeEvent['EventDateStart']));
}
if ($activeEvent['EventDateStart'] != $activeEvent['EventDateEnd']) {
    $eventdates .= ' until ' . date('F j, Y', strtotime($activeEvent['EventDateEnd']));
}
$OG_URL = curPageURL();
$OG_Type = 'article';
$OG_Title = $activeEvent['Title'] . ' - ' . $eventdates;
$OG_Description = $activeEvent['Content'];
//$OG_Image = 'http://'. DOMAIN . IMG_LOC_REL . 'Default-Header.jpg';
$OG_Image = ($activeEvent['Event_Image'] != "") ? 'http://' . DOMAIN . IMG_LOC_REL . $activeEvent['Event_Image'] : 'http://' . DOMAIN . IMG_LOC_REL . 'Default-Header.jpg';
$OG_Image_Width = 1600;
$OG_Image_Height = 640;
require_once 'include/public/header.php';
?>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1356712584397235";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!--event detail.-->
<section class="detail-page">
    <div class="listing-main">
        <div class="title event-title">
            <?php echo $activeEvent['Title'] ?>
        </div>
        <div class="other event-other">
            <?php echo date('M j, Y', strtotime($activeEvent['EventDateStart'])) ?> <?php if ($activeEvent['EventDateStart'] != $activeEvent['EventDateEnd'] && $activeEvent['EventDateEnd'] != '0000-00-00') {
                echo ' - ' . date('M j, Y', strtotime($activeEvent['EventDateEnd']));
            } ?>
        </div> <div class="other">
            <?php if ($activeEvent['Event_Image']) {
                php ?>
                <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $activeEvent['Event_Image']; ?>" >
    <?php }
?>
        </div>

        <div class="other">
            <div class="fb-share-button" data-href="<?php print curPageURL(); ?>" data-layout="button_count" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php print curPageURL(); ?>%2F&amp;src=sdkpreparse">Share</a></div>
            <iframe class="twitter-class"
                    src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=http%3A%2F%2F<?php print curPageURL(); ?>&related=twitterapi%2Ctwitter&text=<?php echo $activeEvent['Title'] ?> in <?php echo $REGION['R_Name'] ?>"
                    width="70"
                    height="25"
                    style="border: 0; overflow: hidden; float: left;">
            </iframe>
        </div>
    </div>
    <div class="listing-description">
        <?php echo $activeEvent['Content']; ?>
    </div>
    <div class="event-detail-item-container">
<?PHP if ($activeEvent['EventStartTime'] <> '00:00:00' || $activeEvent['EventEndTime'] <> '00:00:00') { ?>
            <div class="event-items-content-container">
                <div class="event-detail-heading">Time</div>
                <div class="event-detail-content">
                    <?php
                    if ($activeEvent['EventTimeAllDay'] == '1') {
                        echo "All day";
                    } else {
                        echo $activeEvent['EventStartTime'] == '00:00:01' ? 'Dusk' : ($activeEvent['EventStartTime'] == '00:00:00' ? '' : date('g:i a', strtotime($activeEvent['EventStartTime'])))
                        ?><?php
                        echo
                        $activeEvent['EventEndTime'] <> '00:00:00' ? (' to ' . ($activeEvent['EventEndTime'] == '00:00:01' ? 'Dusk' : date('g:i a', strtotime($activeEvent['EventEndTime'])))) : '';
                    }
                    ?>
                </div>
            </div>
                <?PHP } ?> 
        <div class="event-items-content-container">
            <div class="event-detail-heading">Community</div>
            <div class="event-detail-content">
                <?PHP
                if ($activeEvent['E_Region_ID']) {
                    $getRegionEvent = "SELECT GROUP_CONCAT(R_Name) AS R_Names FROM tbl_Region WHERE R_ID IN (" . $activeEvent['E_Region_ID'] . ")";
                    $rowRegionEvent = mysql_fetch_assoc(mysql_query($getRegionEvent));
                    if ($rowRegionEvent['R_Names'] != "") {
                        echo $rowRegionEvent['R_Names'];
                    }
                }
                ?>
            </div>
        </div>
        <?PHP
        $recuring = explode(",", $activeEvent['Recuring']);
        if (in_array(1, $recuring)) {
            ?>
            <div class="event-items-content-container">
                <div class="event-detail-heading">Recurring</div>
                <?php
                $weekdays = array();
                if ($recuring[0] == 1) {
                    $weekdays[] = "Mon";
                }
                if ($recuring[1] == 1) {
                    $weekdays[] = "Tue";
                }
                if ($recuring[2] == 1) {
                    $weekdays[] = "Wed";
                }
                if ($recuring[3] == 1) {
                    $weekdays[] = "Thu";
                }
                if ($recuring[4] == 1) {
                    $weekdays[] = "Fri";
                }
                if ($recuring[5] == 1) {
                    $weekdays[] = "Sat";
                }
                if ($recuring[6] == 1) {
                    $weekdays[] = "Sun";
                }
                echo '<div class="event-detail-content">';
                $i = 0;
                $count = count($weekdays);
                foreach ($weekdays as $day) {
                    if ($i > 0 && $i != $count) {
                        echo ', ';
                    }
                    echo $day;
                    $i++;
                }
                echo '</div>';
                ?>
            </div>
        <?php } ?>
<?php
if ($activeEvent['Organization'] || $activeEvent['OrganizationID']) {
    ?>
            <div class="event-items-content-container">
                <div class="event-detail-heading">Organization</div>
                <div class="event-detail-content">
            <?php echo $activeEvent['OrganizationID'] ? $activeEvent['EO_Name'] : $activeEvent['Organization']; ?>
                </div>
            </div>
            <?PHP
        }

        if ($activeEvent['LocationID']) {
            ?>
            <div class="event-items-content-container">
                <div class="event-detail-heading">Location</div>
                <div class="event-detail-content">
            <?php echo $activeEvent['EL_Name']; ?>
                </div>
            </div>
                    <?PHP if ($activeEvent['EL_Street']) { ?>
                <div class="event-items-content-container">
                    <div class="event-detail-heading">Address</div>
                    <div class="event-detail-content">
                <?php echo $activeEvent['EL_Street']; ?>, <?php echo $activeEvent['EL_Town']; ?>
                    </div>
                </div>
                <?PHP
            }
        } elseif ($activeEvent['LocationList'] && $activeEvent['LocationList'] <> 'Alternate Location') {
            if ($activeEvent['LocationList']) {
                ?>
                <div class="event-items-content-container">
                    <div class="event-detail-heading">Location</div>
                    <div class="event-detail-content">
                <?php echo $activeEvent['LocationList']; ?>
                    </div>
                </div>
                    <?PHP } if ($activeEvent['StreetAddress']) { ?>
                <div class="event-items-content-container">
                    <div class="event-detail-heading">Address</div>
                    <div class="event-detail-content">
                <?php echo $activeEvent['StreetAddress']; ?>
                    </div>
                </div>
                <?PHP
            }
        }
        ?>
                <?PHP if ($activeEvent['ContactName']) { ?>
            <div class="event-items-content-container">
                <div class="event-detail-heading">Contact Person</div>
                <div class="event-detail-content">
            <?php echo $activeEvent['ContactName']; ?>
                </div>
            </div>
                <?PHP } if ($activeEvent['ContactPhone']) { ?>
            <div class="event-items-content-container">
                <div class="event-detail-heading">Phone</div>
                <div class="event-detail-content">
            <?php echo $activeEvent['ContactPhone']; ?>
                </div>
            </div>
                <?PHP } if ($activeEvent['ContactPhoneTollFree']) { ?>
            <div class="event-items-content-container">
                <div class="event-detail-heading">Additional Phone</div>
                <div class="event-detail-content">
            <?php echo $activeEvent['ContactPhoneTollFree']; ?>
                </div>
            </div>
<?PHP } if ($activeEvent['Email']) { ?>
            <div class="event-items-content-container">
                <div class="event-detail-heading">Email</div>
                <div class="event-detail-content">
                    <a class="email-to" href="mailto:<?php echo $activeEvent['Email']; ?>"><?php echo $activeEvent['Email']; ?></a>
                </div>
            </div>
<?PHP } if ($activeEvent['WebSiteLink']) { ?>
            <div class="event-items-content-container">
                <div class="event-detail-heading">Website</div>
                <div class="event-detail-content">
                    <a href="http://<?php echo str_replace("http://", "", $activeEvent['WebSiteLink']); ?>" target="_blank"><?php echo $activeEvent['WebSiteLink']; ?></a>
                </div>
            </div>
<?PHP } ?>
        <!--  -->
<?PHP if ($activeEvent['EventPdf']) { ?>
            <div class="event-items-content-container">
                <div class="event-detail-heading">PDF</div>
                <div class="event-detail-content">
                    <a href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $activeEvent['EventPdf'] ?>" target="_blank">View</a>
                </div>
            </div>
<?PHP } ?>
        <!--  -->
    </div>
</section>
<!--Event detail end-->
<?php require_once 'include/public/footer.php'; ?>
