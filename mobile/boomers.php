<?php
require_once '../include/config.inc.php';
require_once 'include/public-site-functions.inc.php';

$SEOtitle = $REGION['R_SEO_Title'];
$SEOdescription = $REGION['R_SEO_Description'];
$SEOkeywords = $REGION['R_SEO_Keywords'];

$sql_feature = "SELECT S_ID, S_Title, S_Description, S_Thumbnail_Mobile FROM tbl_Story
                WHERE S_ID IN (65, 43, 62) GROUP BY S_ID";
$result_feature = mysql_query($sql_feature, $db) or die("Invalid query: $sql_feature -- " . mysql_error());
$feature_count = mysql_num_rows($result_feature);

$sql_secondary = "SELECT S_ID, S_Title, S_Description, S_Thumbnail_Mobile FROM tbl_Story
                  WHERE S_ID IN (70, 105, 68, 72, 77, 71, 120, 76, 49) GROUP BY S_ID";
$result_secondary = mysql_query($sql_secondary, $db) or die("Invalid query: $sql_secondary -- " . mysql_error());
$secondary_count = mysql_num_rows($result_secondary);
?>
<?php require_once 'include/public/header.php'; ?>
<!--Slider Start-->
<section class="main_image">
    <img class="slider_image" class="show-slider-display" src="<?php echo 'http://' . DOMAIN . '/images/mobile-boomers.jpg';?>" alt="boomers">
    <?php if ($THEME['TO_Slider_Overlay'] != "") { ?>
        <div class="image_overlay_img">
            <div class="image_overlay">
                <img src="<?php echo 'http://' . DOMAIN . IMG_ICON_REL . $THEME['TO_Slider_Overlay']; ?>" alt="Slider Overlay">
            </div>
        </div>
    <?php } ?>
</section>
<!--Slider End-->
<!--Description Start-->
<section class="description">
    <?PHP if ($REGION['R_Mobile_Description']) { ?>
        <p><?PHP echo ($REGION['R_Mobile_Description']); ?></p>
    <?PHP } ?>
</section>
<!--Description End-->
<!-- Feature Stories-->
<?php if ($feature_count > 0 && $REGION['R_Stories'] == 1 && $blog['RC_Status'] == 0) { ?>
    <section class="feature-items">
        <?php
        while ($row_feature = mysql_fetch_array($result_feature)) {
            if ($row_feature['S_Thumbnail_Mobile'] != '') {
                $feature_image = $row_feature['S_Thumbnail_Mobile'];
            } else {
                $feature_image = $default_thumbnail_image;
            }
            ?>
            <div class="feature">
                <div class="thumbnail">
                    <a href="<?php echo DOMAIN_MOBILE_REL . 'story/' . clean($row_feature['S_Title']) . '/' . $row_feature['S_ID'] ?>/">
                        <img  src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $feature_image; ?>" width="100%" height="auto" alt="<?php echo $row_feature['S_Title']; ?>" />
                    </a>
                </div>
                <a class="feature-title" href="<?php echo DOMAIN_MOBILE_REL . 'story/' . clean($row_feature['S_Title']) . '/' . $row_feature['S_ID'] ?>/">
                    <?php echo $row_feature['S_Title'] ?>
                </a>
                <?php
                if ($row_feature['S_Description'] != '') {
                    $string = preg_replace('/(.*)<\/p[^>]*>/i', '$1', $row_feature['S_Description']);
                    ?>
                    <div class="description">
                        <?php echo $string ?>
                        <a class="read-more" href="<?php echo DOMAIN_MOBILE_REL . 'story/' . clean($row_feature['S_Title']) . '/' . $row_feature['S_ID'] ?>/">
                            Read More
                        </a>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </section>
<?php } ?>
<!-- Feature Stories end -->
<!-- Secondary Stories-->
<?php if ($secondary_count > 0 && $REGION['R_Stories'] == 1 && $blog['RC_Status'] == 0) { ?>
    <section class="thumbnails secondary-stories">
        <div class="title"><?php echo ($REGION['R_Homepage_Stories_Title'] != '') ? $REGION['R_Homepage_Stories_Title'] : 'Stories' ?></div>
        <?php
        while ($row_secondary = mysql_fetch_array($result_secondary)) {
            if ($row_secondary['S_Thumbnail_Mobile'] != '') {
                $secondary_image = $row_secondary['S_Thumbnail_Mobile'];
            } else {
                $secondary_image = $default_thumbnail_image;
            }
            ?>
            <div class="thumbnails-inner">
                <div class="thumbnail">
                    <a href="<?php echo DOMAIN_MOBILE_REL . 'story/' . clean($row_secondary['S_Title']) . '/' . $row_secondary['S_ID'] ?>/">
                        <img  src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $secondary_image; ?>" alt="<?php echo $row_secondary['S_Title'] ?>">
                        <div class="title index"><p><?php echo ($row_secondary['S_Title'] != '') ? $row_secondary['S_Title'] : ''; ?></p></div>
                    </a>
                </div>
            </div>
            <?php
        }
        ?>
    </section>
<?php } ?>
<!-- Secondary Stories end-->
<!--Thumbnail Grid-->
<section class="thumbnails">
    <?php
    //Getting Active Categories for region
    if (isset($REGION['R_Homepage_Carousals']) && $REGION['R_Homepage_Carousals'] == 1) {
        $getCategory = "SELECT RC1.RC_Name, RC1.RC_Mobile_Thumbnail, C1.C_Name, C1.C_ID, C1.C_Name_SEO as subSEO, C2.C_Name_SEO as mainSEO FROM tbl_Region_Category RC1
                        LEFT JOIN tbl_Category C1 ON RC1.RC_C_ID = C1.C_ID AND RC1.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                        LEFT JOIN tbl_Category C2 ON C1.C_Parent = C2.C_ID
                        LEFT JOIN tbl_Region_Category RC2 ON RC2.RC_C_ID = C2.C_ID AND RC2.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                        WHERE C1.C_Parent != 0 AND C1.C_Parent != 8 AND C2.C_Is_Blog != 1 AND RC1.RC_Status = 0 AND RC2.RC_Status = 0 ORDER BY RC1.RC_Order ASC";
        $categoryCheck = "BLC_C_ID";
    } else {
        $getCategory = "SELECT RC_Mobile_Thumbnail, RC_Name, C_Name, C_ID, C_Name_SEO as mainSEO FROM tbl_Category 
                        LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                        WHERE C_Parent = 0 AND RC_Status=0 AND C_ID != 8 AND C_Is_Blog != 1 ORDER BY RC_Order ASC";
        $categoryCheck = "BLC_M_C_ID";
    }
    $catRes = mysql_query($getCategory);
    while ($homeCat = mysql_fetch_array($catRes)) {
        if ($homeCat['RC_Mobile_Thumbnail'] != '') {
            $listing_image = $homeCat['RC_Mobile_Thumbnail'];
        } else {
            $listing_image = $default_thumbnail_image;
        }
        if ($REGION['R_Type'] == 4) {
            //County Regions
            $show_homepage_check = ' AND BLCR_Home_status=1';
        }
        if ($REGION['R_Type'] == 1) {
            //Parent Regions
            $getSubCat = " SELECT BL_ID FROM tbl_Business_Listing
                           INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                           LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID=BL_ID
                           INNER JOIN tbl_Region_Category ON RC_C_ID = BLC_C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                           WHERE " . $categoryCheck . " = '" . encode_strings($homeCat['C_ID'], $db) . "' 
                           $show_homepage_check
                           AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = '1' $include_free_listings
                           AND BLCR_BLC_R_ID IN (SELECT R_ID from tbl_Region LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID where RM_Parent = '" . encode_strings($REGION['R_ID'], $db) . "')
                           GROUP BY BL_Listing_Title";
        } else {
            //Child Regions
            $getSubCat = " SELECT BL_ID FROM tbl_Business_Listing
                           INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                           LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID=BL_ID
                           INNER JOIN tbl_Region_Category ON RC_C_ID = BLC_C_ID AND RC_R_ID='" . encode_strings($REGION['R_ID'], $db) . "'
                           WHERE " . $categoryCheck . " = '" . encode_strings($homeCat['C_ID'], $db) . "'
                           $show_homepage_check
                           AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = '1'
                           AND BLCR_BLC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' $include_free_listings 
                           GROUP BY BL_Listing_Title";
        }
        $subCatRes = mysql_query($getSubCat, $db) or die(mysql_error() . '-' . $getSubCat);
        $count_homepage = mysql_num_rows($subCatRes);
        if ($count_homepage > 0) {
            ?>
            <div class="thumbnails-inner">
                <div class="thumbnail">
                    <a href="<?php echo DOMAIN_MOBILE_REL . $homeCat['mainSEO'] ?>/<?php echo (isset($homeCat['subSEO'])) ? $homeCat['subSEO'] : '' ?>">
                        <img  src="<?php echo 'http://' . DOMAIN . IMG_LOC_REL . $listing_image; ?>" alt="<?php echo ($homeCat['RC_Name'] != '') ? $homeCat['RC_Name'] : $homeCat['C_Name'] ?>"/>
                        <div class="title index">
                            <p><?php echo ($homeCat['RC_Name'] != '') ? $homeCat['RC_Name'] : $homeCat['C_Name'] ?></p>
                        </div>
                    </a>
                </div>
            </div>
        <?php } ?>
        <?php
    }
    ?>
</section>
<!--Thumbnail Grid-->
<?php require_once 'include/public/footer.php'; ?>
