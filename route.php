<?php
//echo 'The section is under active maintenance. We ll be back shortly.';exit;
require_once 'include/config.inc.php';
require_once 'update-impression-advert.php';
require_once 'include/public-site-functions.inc.php';
if ($REGION['R_My_Route'] == 0) {
    header("Location: /404.php");
    exit();
}
if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $routeID = $_REQUEST['id'];
} else {
    header("Location: /404.php");
    exit();
}
$sqlRoute = "SELECT * FROM tbl_Individual_Route WHERE IR_ID = '" . encode_strings($routeID, $db) . "'";
$resRoute = mysql_query($sqlRoute, $db) or die("Invalid query: $sqlRoute -- " . mysql_error());
$Route = mysql_fetch_assoc($resRoute);
$SEOtitle = $Route['IR_SEO_Title'];
$SEOdescription = $Route['IR_SEO_Description'];
$SEOkeywords = $Route['IR_SEO_Keywords'];
$kml_array[] = array(
    'name' => str_replace(' ', '', $Route['IR_Title']),
    'url' => 'http://' . DOMAIN . MAP_LOC_REL . $Route['IR_KML_Path']
);

//check to pass mobile users to mobile version
$useragent = $_SERVER['HTTP_USER_AGENT'];
if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
    header('Location: http://' . $REGION['R_Domain'] . DOMAIN_MOBILE_REL . 'route/' . clean($Route['IR_Title']) . '/' . $Route['IR_ID']);
    exit;
}

//Getting Gallery Images
$sqlG = "SELECT * FROM tbl_Individual_Route_Gallery WHERE IRG_IR_ID = '" . encode_strings($Route['IR_ID'], $db) . "' ORDER BY IRG_Order ASC";
$resG = mysql_query($sqlG, $db) or die("Invalid query: $sqlG -- " . mysql_error());
$count_gallery = mysql_num_rows($resG);
//Getting PDF Downloads
$sqlD = "SELECT * FROM tbl_Individual_Route_Downloads WHERE IRD_IR_ID='" . encode_strings($Route['IR_ID'], $db) . "' ORDER BY IRD_Order ASC";
$resD = mysql_query($sqlD, $db) or die("Invalid query: $sqlD -- " . mysql_error());
$count_download = mysql_num_rows($resD);

//Getting Region Theme
$getThemeOnRoute = "SELECT TO_Default_Thumbnail_Desktop FROM tbl_Theme_Options WHERE TO_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'";
$themeRegionOnRoute = mysql_query($getThemeOnRoute, $db) or die("Invalid query: $getThemeOnRoute -- " . mysql_error());
$THEMEOnRoute = mysql_fetch_assoc($themeRegionOnRoute);

//Defualt Thumbnail Images
if ($THEMEOnRoute['TO_Default_Thumbnail_Desktop'] != '') {
    $default_header_route_image = $THEMEOnRoute['TO_Default_Thumbnail_Desktop'];
} else {
    $default_header_route_image = 'Default-Thumbnail-Desktop.jpg';
}

$OG_URL = curPageURL();
$OG_Type = 'Route';
$OG_Title = $Route['IR_Title'];
$OG_Description = $Route['IR_SEO_Description'];
$OG_Image = ($Route['IR_Photo'] != "") ? 'http://' . DOMAIN . IMG_LOC_REL . $Route['IR_Photo'] : 'http://' . DOMAIN . IMG_LOC_REL . $default_header_route_image;

// $OG_Image = 'http://'. DOMAIN . IMG_LOC_REL . $Route['IR_Photo'];
$OG_Image_Width = 1600;
$OG_Image_Height = 640;

require_once 'include/public/header.php';
?>
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1356712584397235";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<!--Slider Start-->
<section class="main_slider">
    <div class="slider_wrapper">
        <div class="slider_wrapper_video">
            <div class="sliderCycle" data-cycle-loader="wait" data-cycle-fx=fade data-cycle-timeout=0 data-cycle-prev="#prev" data-cycle-next="#next">
                <?php
                $i = 1;
                $show_default = true; //if no image, show default
                $VIDEOID = $Route['IR_Video'];
                $pos = strpos($VIDEOID, '=');
                if ($pos == false) {
                    $video_link = end(explode('/', $VIDEOID));
                } else {
                    $video_link = end(explode("=", $VIDEOID));
                }
                if ($Route['IR_Photo'] != '') {
                    $show_default = false;
                    ?>
                    <div class="slide-images">
                        <?php if ($VIDEOID != "") { ?>
                            <div class="<?php echo"player" . $i . "_wrapper"; ?>" style="position:absolute;opacity: 0;top:0;">
                                <input type="hidden" id="video-link-<?php echo $i; ?>" value="<?php echo $video_link ?>">
                                <div  id="<?php echo "player" . $i; ?>"></div>
                            </div>
                        <?php } ?>
                        <div class="slider-text">
                            <h1 class="show-slider-display"><?php echo $Route['IR_Slider_Title'] ?></h1>
                            <h3 class="show-slider-display"><?php echo $Route['IR_Slider_Description'] ?></h3>
                            <?php if ($VIDEOID != "") { ?>
                                <div class="watch-video play-button show-slider-display">
                                    <div class="slider-button">
                                        <img class="video_icon" src="http://<?php echo DOMAIN ?>/images/videoicon.png" alt="Play">
                                        <a onclick="play_video(<?php echo $i; ?>, '<?php echo"player" . $i; ?>')">Watch full video</a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <img class="slider_image" class="show-slider-display" src="<?php echo IMG_LOC_REL . $Route['IR_Photo'] ?>" alt="<?php echo ($Route['IR_Alt_Tag'] != '') ? $Route['IR_Alt_Tag'] : $Route['IR_Slider_Title']; ?>">
                    </div>
                    <?php
                }
                //show default image if no image available
                if ($show_default) {
                    ?>
                    <div class="slide-images" style="<?php echo $image_slider_style ?>">
                        <img class="slider_image" class="show-slider-display" src="<?php echo IMG_LOC_REL . $default_header_image; ?>" alt="<?php echo ($slide['alt'] != '') ? $slide['alt'] : $activeCat['C_Name']; ?>">
                    </div>
                    <?php
                }
                $i++;
                ?>
            </div>
            <input type="hidden" id="total_players" value="<?php echo $i; ?>">
            <div class=center>
                <span id=prev></span>
                <span id=next></span>
            </div>
            <?php if ($THEME['TO_Slider_Overlay'] != "") { ?>
                <div class="image_overlay_img">
                    <div class="image_overlay">
                        <img src="<?php echo IMG_ICON_REL . $THEME['TO_Slider_Overlay']; ?>" alt="Slider Overlay">
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<!--Slider End-->

<section class="description listing-home-des margin-bottom-none">
    <div class="description-wrapper">
        <div class="event-filter">
            <div class="listing-title-detail">
                <div class="listing-title route-title"><?php echo $Route['IR_Title']; ?></div>
                <div class="listing-address"><?php echo $Route['IR_Sub_Title']; ?></div>
            </div>
            <div class="thumbnails listing-home-gallary">
                <div class="thumbnail-slider">
                    <div class="inner">
                        <?PHP
                        //Gal_feature_count query is shifted top of the page.
                        if ($count_gallery > 0) {
                            ?>
                            <ul class="bxslider">
                                <?PHP
                                while ($rowG = mysql_fetch_assoc($resG)) {
                                    if ($rowG['IRG_Thumbnail_Photo'] != "") {
                                        ?>
                                        <li><a href="/profile/gallery.php?irid=<?php echo $Route['IR_ID'] ?>&order=<?php echo $rowG['IRG_Order'] ?>" class="view-gallery"><img src="<?php echo IMG_LOC_REL . $rowG['IRG_Thumbnail_Photo']; ?>" alt="<?php echo $rowG['IRG_Title']; ?>"/></a></li>
                                    <?php } else if ($rowG['IRG_Photo'] != "") { ?>
                                        <li><a href="/profile/gallery.php?irid=<?php echo $Route['IR_ID'] ?>&order=<?php echo $rowG['IRG_Order'] ?>" class="view-gallery"><img src="<?php echo IMG_LOC_REL . $rowG['IRG_Photo']; ?>" alt="<?php echo $rowG['IRG_Title']; ?>"/></a></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>    
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<!--Description Start-->
<section class="description stories margin-bottom-none">
    <div class="description-wrapper">
        <div class="description-inner padding-none">
            <div class="listing-detail-left">
                <div class="listing-desc theme-paragraph ckeditor-anchor-listing">
                    <?PHP echo ($Route['IR_Description']); ?>
                </div>
            </div>
            <div class="listing-detail-right">
                <div class="listing-detail-address">
                    <div class="fb-share-button" data-href="<?php print curPageURL(); ?>" data-layout="button_count"  data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php print curPageURL(); ?>%2F&amp;src=sdkpreparse">Share</a></div>
                    <iframe class="twitter-class"
                            src="https://platform.twitter.com/widgets/tweet_button.html?size=s&url=http%3A%2F%2F<?php print curPageURL(); ?>&related=twitterapi%2Ctwitter&text=<?php echo $Route['IR_Title'] ?> in <?php echo $REGION['R_Name'] ?>"
                            width="70"
                            height="25"
                            style="border: 0; overflow: hidden; float: left;">
                    </iframe>
                </div>
                <div class="listing-detail-routes">
                    <?php if ($Route['IR_Start_Point'] != '') { ?>
                        <div class="address-heading">Start Point</div>
                        <div class="address-data"><?php echo $Route['IR_Start_Point']; ?></div>
                        <?php
                    }
                    if ($Route['IR_End_Point'] != '') {
                        ?>
                        <div class="address-heading">End Point</div>
                        <div class="address-data"><?php echo $Route['IR_End_Point']; ?></div>
                    <?php } ?>
                    <div class="address-heading">Distance</div>
                    <div class="address-data"><?php echo $Route['IR_Distance']; ?></div>
                    <div class="address-heading">Surface</div>
                    <div class="address-data"><?php echo $Route['IR_Surface']; ?></div>
                    <?php if ($count_download > 0) { ?>
                        <div class="address-heading">Downloads</div>
                        <div class="address-data">
                            <?php while ($rowD = mysql_fetch_assoc($resD)) { ?>
                                <a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $rowD['IRD_Path'] ?>"><?php echo $rowD['IRD_Title']; ?></a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Description End-->
<!--Map section-->
<section class="description margin-bottom-none maps">
    <?php require_once 'include/public/map_filters.php'; ?>
    <div class="map_wrapper">
        <?PHP
        $markers = array();
        $must_see = array();
        if ($REGION['R_Parent'] == 0) {
            $REG = '';
        } else {
            $REG = "AND BLP_R_ID = " . $REGION['R_ID'];
        }
        //Getting Map Listings
        $sqlMap = "SELECT DISTINCTROW BL_ID, BL_Listing_Title, BL_Name_SEO, BL_Lat, BL_Long, BL_Street, BL_Town, BLP_Photo, BL_Photo_Alt, IRML_Must_See, 
                    RC.RC_Name, RC.RC_Map_Icon as subcat_icon, RC1.RC_Map_Icon as cat_icon FROM tbl_Business_Listing 
                    LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
                    LEFT JOIN tbl_Business_Listing_Photo ON BL_ID = BLP_BL_ID
                    INNER JOIN tbl_Region_Category RC ON RC.RC_C_ID = BLC_C_ID AND RC.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                    INNER JOIN tbl_Region_Category RC1 ON RC1.RC_C_ID = BLC_M_C_ID AND RC1.RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                    LEFT JOIN tbl_Category ON RC.RC_C_ID = C_ID
                    INNER JOIN tbl_Individual_Route_Map_Listings ON IRML_BL_ID = BL_ID 
                    INNER JOIN tbl_Individual_Route ON IR_ID = IRML_IR_ID
                    WHERE IR_ID = '" . encode_strings($Route['IR_ID'], $db) . "' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing='1' AND RC.RC_Status = 0 AND RC1.RC_Status = 0
                    $include_free_listings_on_map $REG
                    GROUP BY BL_ID ORDER BY IRML_Order ASC";
// generate icons for all listings on map regardless of page
        $resMap = mysql_query($sqlMap, $db) or die(mysql_error() . ' - ' . $sqlMap);
        while ($listMap = mysql_fetch_assoc($resMap)) {
            //Add to must see array if it is must see
            if ($listMap['IRML_Must_See'] == 1) {
                $must_see[] = $listMap;
            }

            //now add to map markers
            if ($listMap['BLP_Photo'] != '') {
                $image = '<div class="thumbnail"><img src="' . (IMG_LOC_REL . $listMap['BLP_Photo']) . '" alt="' . htmlspecialchars(trim($listMap['BL_Photo_Alt']), ENT_QUOTES) . '" /></div>';
            } else {
                $image = '<div class="thumbnail"><img src="' . (IMG_LOC_REL . $default_thumbnail_image) . '" alt="' . htmlspecialchars(trim($listMap['BL_Listing_Title']), ENT_QUOTES) . '" /></div>';
            }
            if ($listMap['subcat_icon'] != '') {
                $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $listMap['subcat_icon'];
            } elseif ($listMap['cat_icon'] != '') {
                $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . $listMap['cat_icon'];
            } else {
                $icon = 'http://' . DOMAIN . IMG_ICON_MAP_REL . 'default.png';
            }
            if ($listMap['BL_Lat'] && $listMap['BL_Long']) {
                $markers[] = array(
                    'id' => $listMap['BL_ID'],
                    'lat' => $listMap['BL_Lat'],
                    'lon' => $listMap['BL_Long'],
                    'name' => $listMap['BL_Listing_Title'],
                    'path' => '/profile/' . $listMap['BL_Name_SEO'] . '/' . $listMap['BL_ID'] . '/',
                    'icon' => $icon,
                    'main_photo' => $image,
                    'address' => htmlspecialchars(trim($listMap['BL_Street']), ENT_QUOTES),
                    'town' => htmlspecialchars(trim($listMap['BL_Town']), ENT_QUOTES)
                );
            }
        }
        $markers = json_encode($markers);
        ?>
        <div id="map_canvas">&nbsp;</div>
        <?php
        $map_center['latitude'] = $Route['IR_Lat'];
        $map_center['longitude'] = $Route['IR_Long'];
        $map_center['zoom'] = $Route['IR_Zoom'];
        $kml_json = json_encode($kml_array);
        require_once 'map_script.php';
        ?>
    </div>
</section>
<!--Map section end-->
<!--Must See section-->
<?php  if (count($must_see) > 0) { ?>
    <section class="section_stories">
        <div class="grid-wrapper">
            <div class="grid-inner padding-none border-none">
                <h1 class="heading-text" align="center">Must Sees</h1>
                <?php
                $i = 0;
                foreach ($must_see as $rowMust) {
                    $i++;
                    if ($rowMust['BLP_Photo'] != '') {
                        $listing_image = $rowMust['BLP_Photo'];
                    } else {
                        $listing_image = $default_thumbnail_image;
                    }
                    if ($i == 1) {
                        echo "<div class='thumbnails static-thumbs'>";
                    }
                    ?>
                    <div class="thumb-item title">
                        <a href="/profile/<?php echo $rowMust['BL_Name_SEO'] ?>/<?php echo $rowMust['BL_ID'] ?>/"> 
                            <img src="<?php echo IMG_LOC_REL . $listing_image ?>" alt="<?php echo $rowMust['BL_Photo_Alt'] ?>" />
                            <h3 class="thumbnail-heading"><?php echo ($rowMust['RC_Name'] != '') ? $rowMust['RC_Name'] : $rowMust['C_Name'] ?></h3>
                            <h3 class="thumbnail-desc"><?php echo $rowMust['BL_Listing_Title']; ?></h3>
                        </a>
                    </div>
                    <?php
                    if ($i == 3) {
                        echo '</div>';
                        $i = 0;
                    }
                }
                ?>
            </div>
        </div>
    </section>
<?php } ?>
<!--Must See section end-->
<?php require_once 'include/public/footer.php'; ?>