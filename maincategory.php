
<?php
//if ($activeCat['RC_Status'] == 1) {
//    header("location: ../404.php");
//    exit();
//}
if (isset($url[1]) && $url[1] == 'day-trip') {
    ?>
    <section class="thumbnail-grid sub-cat-margin border-none full-div-sub-cat">
        <div class="grid-wrapper">
            <div class="grid-inner border-none padding-bottom-none">
                <h1 class="heading-text" align="center">Day Trips</h1>
                <?php
                $pages = new Paginate(mysql_num_rows($result_day_trip), 15);
                $result_day_trip = mysql_query($sql_day_trip . $pages->generateSql(), $db) or die("Invalid query: $sql_day_trip -- " . mysql_error());
                $i = 0;
                while ($row = mysql_fetch_assoc($result_day_trip)) {
                    $i++;
                    if ($row['BLP_Photo'] != '') {
                        $listing_image = $row['BLP_Photo'];
                    } else {
                        $listing_image = $default_thumbnail_image;
                    }
                    if ($i == 1) {
                        echo "<div class='thumbnails static-thumbs'>";
                    }
                    ?>
                    <div class="thumb-item">
                        <a href="/profile/<?php echo $row['BL_Name_SEO'] ?>/<?php echo $row['BL_ID'] ?>/"> 
                            <img src="<?php echo IMG_LOC_REL . $listing_image ?>" alt="<?php echo $row['BL_Photo_Alt'] ?>" />
                            <h3 class="thumbnail-heading"><?php echo ($activeCat['RC_Name'] != '') ? $activeCat['RC_Name'] : $activeCat['C_Name'] ?></h3>
                            <h3 class="thumbnail-desc"><?php echo $row['BL_Listing_Title']; ?></h3>
                        </a> 
                    </div>
                    <?php
                    if ($i == 3) {
                        echo '</div>';
                        $i = 0;
                    }
                }
                ?>
            </div>
            <?php
            // display our pagination footer if set.
            if (isset($pages)) {
                echo $pages->paginate();
            }
            ?>
        </div>
    </section>
    <?php
    // print_r($url[0]); 
} else {
    $sql = "SELECT C_ID, C_Is_Blog, C_Name_SEO, C_Name, RC_Name, RC_Description, RC_Category_Page_Title FROM tbl_Category 
            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
            WHERE C_ID = '" . encode_strings($_SESSION['CATEGORY'], $db) . "' ORDER BY C_Name LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeCat = mysql_fetch_assoc($result);

    $sql_category_slider = "SELECT RCP_Image, RCP_Video, RCP_Slider_Title, RCP_Slider_Description FROM tbl_Region_Category_Photos
                            WHERE RCP_C_ID = '" . encode_strings($activeCat['C_ID'], $db) . "' AND RCP_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "' order by RCP_Order";
    $result_category_slider = mysql_query($sql_category_slider, $db) or die("Invalid query: $sql_category_slider -- " . mysql_error());
    $slider = array();
    $i = 0;
// Select Random advertisments
    $adQueryCH = " SELECT BL_ID, A_C_ID, A_AT_ID, A_Status, A_Website, A_Is_Deleted, A_ID, A_Third_Party, A_Approved_Logo, BL_Name_SEO FROM tbl_Advertisement
                LEFT JOIN tbl_Business_Listing ON A_BL_ID = BL_ID $include_free_listings
                WHERE A_C_ID = '" . encode_strings($_SESSION['CATEGORY'], $db) . "' AND A_AT_ID = 1 AND A_Status = 3 AND A_Website = '" . encode_strings($REGION['R_ID'], $db) . "' 
                AND A_Is_Deleted = 0 AND (A_End_Date >= CURDATE() OR A_End_Date = 0000-00-00)
                GROUP BY A_ID
                ORDER BY RAND() LIMIT 4";
    $adResultCH = mysql_query($adQueryCH) or die("Invalid query: $adQueryCH -- " . mysql_error());
    $num_of_adv = mysql_num_rows($adResultCH);
//create array of all images except ads
    if ($Firstslider['RC_Image'] != "") {
        $slider[$i]['title'] = $Firstslider['RC_Slider_Title'];
        $slider[$i]['desc'] = $Firstslider['RC_Slider_Des'];
        $slider[$i]['image'] = $Firstslider['RC_Image'];
        $slider[$i]['video'] = $Firstslider['RC_Video'];
        $slider[$i]['alt'] = $Firstslider['RC_Alt'];
        $i++;
    }
    while ($active_category_slider = mysql_fetch_assoc($result_category_slider)) {
        if ($active_category_slider['RCP_Image'] != '') {
            $slider[$i]['title'] = $active_category_slider['RCP_Slider_Title'];
            $slider[$i]['desc'] = $active_category_slider['RCP_Slider_Description'];
            $slider[$i]['image'] = $active_category_slider['RCP_Image'];
            $slider[$i]['video'] = $active_category_slider['RCP_Video'];
            $slider[$i]['alt'] = $active_category_slider['RCP_Slider_Title'];
            $i++;
        }
    }
    $sql_feature = "SELECT S_ID, S_Category, S_Title, S_Feature_Image, S_Description, C_Parent from tbl_Story LEFT JOIN tbl_Story_Homepage_Category ON S_ID = SHC_S_ID
                LEFT JOIN tbl_Category ON S_Category = C_ID
                $StorySeasons_JOIN
                WHERE SHC_R_ID ='" . encode_strings($REGION['R_ID'], $db) . "' 
                AND SHC_Category = '" . encode_strings($_SESSION['CATEGORY'], $db) . "' AND SHC_Feature = 1 AND S_Active = 1 $StorySeasons_WHERE GROUP BY S_ID ORDER BY SHC_Order ASC";
    $result_feature = mysql_query($sql_feature, $db) or die("Invalid query: $sql_feature -- " . mysql_error());
    $feature_count = mysql_num_rows($result_feature);
    ?>
    <!--Slider Start-->
    <section class="main_slider">
        <div class="slider_wrapper">
            <div class="sliderCycle" data-cycle-loader="wait" data-cycle-fx=fade data-cycle-timeout=0 data-cycle-prev="#prev" data-cycle-next="#next">
                <?php
                $first_img = true;
                $j = 1;
                $show_default = true; //if no image, show default
                foreach ($slider as $slide) {
                    $VIDEOID = $slide['video'];
                    $pos = strpos($VIDEOID, '=');
                    if ($pos == false) {
                        $exp_video_link = explode('/', $VIDEOID);
                        $video_link = end($exp_video_link);
                    } else {
                        $exp_video_link = explode("=", $VIDEOID);
                        $video_link = end($exp_video_link);
                    }
                    if ($slide['image'] != '') {
                        $show_default = false;
                        if ($first_img) {
                            $image_slider_style = '';
                            $first_img = false;
                        } else {
                            $image_slider_style = 'display:none;';
                        }
                        ?>
                        <div class="slide-images" style="<?php echo $image_slider_style ?>">
            <?php if ($VIDEOID != "") { ?>
                                <div class="<?php echo"player" . $j . "_wrapper"; ?>" style="position:absolute;opacity: 0;top:0;">
                                    <input type="hidden" id="video-link-<?php echo $j; ?>" value="<?php echo $video_link ?>">
                                    <div  id="<?php echo "player" . $j; ?>"></div>
                                </div>
            <?php } ?>
                            <div class="slider-text">
                                <h1 class="show-slider-display"><?php echo $slide['title'] ?></h1>
                                <h3 class="show-slider-display"><?php echo $slide['desc'] ?></h3>
            <?php if ($VIDEOID != "") { ?>
                                    <div class="watch-video play-button show-slider-display">
                                        <div class="slider-button">
                                            <img class="video_icon" src="../images/videoicon.png" alt="Play">
                                            <a onclick="play_video(<?php echo $j; ?>, '<?php echo"player" . $j; ?>')">Watch full video</a>
                                        </div>
                                    </div>
            <?php } ?>
                            </div>
                            <img class="slider_image" class="show-slider-display" src="<?php echo IMG_LOC_REL . $slide['image'] ?>" alt="<?php echo ($slide['alt'] != '') ? $slide['alt'] : $activeCat['C_Name']; ?>">
                        </div>
                        <?php
                    }
                    $j++;
                }

                if ($num_of_adv > 0) {
                    while ($adRowCH = mysql_fetch_array($adResultCH)) {
                        $show_default = false;
                        if ($adRowCH['A_Third_Party'] != "") {
                            echo '<div class="slide-images"><a onclick = "advertClicks(' . $adRowCH['A_ID'] . ')" target="_blank" href="' . 'http://' . str_replace(array('http://', 'https://'), '', $adRowCH['A_Third_Party']) . '"><img src="' . IMG_LOC_REL . $adRowCH['A_Approved_Logo'] . '"></a></div>';
                        } else {
                            echo '<div class="slide-images"><a onclick = "advertClicks(' . $adRowCH['A_ID'] . ')" href="/profile/' . $adRowCH['BL_Name_SEO'] . '/' . $adRowCH['BL_ID'] . '/"><img src="' . IMG_LOC_REL . $adRowCH['A_Approved_Logo'] . '"></a></div>';
                        }
                        $AS_A_ID_CH = $adRowCH['A_ID'];
                        ADVERT_IMPRESSION($AS_A_ID_CH);
                    }
                } if ($show_default) {
                    ?>
                    <div class="slide-images" style="<?php echo $image_slider_style ?>">
                        <img class="slider_image" class="show-slider-display" src="<?php echo IMG_LOC_REL . $default_header_image; ?>" alt="<?php echo (($activeCat['RC_Name'] != '') ? $activeCat['RC_Name'] : $activeCat['C_Name']); ?>">
                    </div> 
                    <?php
                }
                ?>
            </div>
            <input type="hidden" id="total_players" value="<?php echo $j; ?>">
            <div class=center>
                <span id=prev></span>
                <span id=next></span>
            </div>
    <?php if ($THEME['TO_Slider_Overlay'] != "") { ?>
                <div class="image_overlay_img">
                    <div class="image_overlay">
                        <img src="<?php echo IMG_ICON_REL . $THEME['TO_Slider_Overlay']; ?>" alt="Slider Overlay">
                    </div>
                </div>
    <?php } ?>
        </div>
    </section>
    <!--Slider End-->
    <!--Description Start-->
    <section class="description theme-description <?php echo (($activeCat['C_Is_Blog'] == 1) ? "margin-bottom-none" : "") ?>">
        <div class="description-wrapper">
            <div class="description-inner <?php echo (($activeCat['C_Is_Blog'] == 1) ? "padding-bottom-none" : "") ?>">
                <h1 class="heading-text">
                    <?php
                    if ($activeCat['RC_Category_Page_Title'] != '') {
                        echo $activeCat['RC_Category_Page_Title'];
                    } else {
                        echo ($activeCat['RC_Name'] != '') ? $activeCat['RC_Name'] : $activeCat['C_Name'];
                        if ($REGION['R_Website_Title'] == 1) {
                            echo " in " . $REGION['R_Name'];
                        }
                    }
                    ?>
                </h1>
                <div class="site-text ckeditor-anchors">
                    <?PHP
                    if ($activeCat['RC_Description']) {
                        echo $activeCat['RC_Description'];
                    }
                    ?>
                </div>
                <?php
                //Explore Category for stories
                if ($REGION['R_Show_Hide_Story_Search'] == 1) {
                    if ($activeCat['C_Is_Blog'] == 1) {
                        $WHERE = "";
                        $cat_stories = "";
                        $textSearch = "";
                        if (isset($_GET['op']) && $_GET['op'] == "searchStories") {
                            $cat_stories = ($_GET['cat_stories']) ? $_GET['cat_stories'] : "";
                            $textSearch = ($_GET['textSearch']) ? $_GET['textSearch'] : "";
                            if ($cat_stories != "" && $textSearch != "") {
                                $WHERE = "AND (S_Title LIKE '%" . encode_strings($textSearch, $db) . "%' OR S_Author LIKE '%" . encode_strings($textSearch, $db) . "%' OR CP_Title LIKE '%" . encode_strings($textSearch, $db) . "%' OR CP_Description LIKE '%" . encode_strings($textSearch, $db) . "%' ) AND S_Category = '" . encode_strings($cat_stories, $db) . "'";
                            } else if ($cat_stories != "") {
                                $WHERE = "AND S_Category = '" . encode_strings($cat_stories, $db) . "'";
                            } else {
                                $WHERE = "AND (S_Title LIKE '%" . encode_strings($textSearch, $db) . "%' OR S_Author LIKE '%" . encode_strings($textSearch, $db) . "%' OR CP_Title LIKE '%" . encode_strings($textSearch, $db) . "%' OR CP_Description LIKE '%" . encode_strings($textSearch, $db) . "%')";
                            }
                        }
                        ?>
                        <div class="extra-category">
                            <div class="event-filter">
                                <div class="filter-inner border-none event-search-padding">
                                    <form method="GET" action="#section_stories">
                                        <input type="hidden" name="op" value="searchStories">
                                        <div class="first-filter">

                                            <div class="first-filter-story" style="background-color: white;">
                                                <div id="dr1" style="background-color: white;">

                                                    <div  class="drop-down-arrow1"></div>

                                                </div>
                                                <select name="cat_stories" onchange="this.form.submit();">
                                                    <option value="">Select Category</option>
                                                    <?php
                                                    $sql = "SELECT C_ID, C_Name, RC_Name FROM tbl_Region_Category
                                                        LEFT JOIN tbl_Category ON RC_C_ID = C_ID 
                                                        WHERE C_Parent = '" . encode_strings($_SESSION['CATEGORY'], $db) . "' AND RC_R_ID = " . $REGION['R_ID'] . " AND RC_Status=0 
                                                        GROUP BY C_ID ORDER BY RC_Order ASC";

                                                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                                    while ($rowExploreSub = mysql_fetch_array($result)) {
                                                        ?>
                                                        <option value="<?php echo $rowExploreSub['C_ID'] ?>" <?php echo (($rowExploreSub['C_ID'] == $cat_stories) ? "selected" : "") ?>>
                                                        <?php echo (($rowExploreSub['RC_Name'] == "") ? $rowExploreSub['C_Name'] : $rowExploreSub['RC_Name']) ?>
                                                        </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <span class="event-heading" style="margin-left: 15px;">or</span>
                                        <div class="second-filter">
                                            <input class="event-search-textbox" value="<?php echo $textSearch; ?>" name="textSearch" id="textSearch" onfocus="clearEventSearch();" size="15" type="text" placeholder="Search">
                                            <input class="event-button" type="submit" value="go">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
    </section>
    <!--Description End-->
    <!-- Feature Stories-->
    <?php if ($feature_count > 0 && $REGION['R_Stories'] == 1 && $blog['RC_Status'] == 0) { ?>
        <section class="section_stories" class="padding-bottom-none border-none">
            <div class="grid-wrapper">
                <div class="grid-inner padding-bottom-none slider">
                    <div class="feature-stories">
                        <?php
                        while ($row_feature = mysql_fetch_array($result_feature)) {
                            if ($row_feature['S_Feature_Image'] != '') {
                                $feature_image = $row_feature['S_Feature_Image'];
                            } else {
                                $feature_image = $default_header_image;
                            }
                            ?>
                            <div class="feature-story">
                                <a href="/<?php echo $storyCatSEORow['C_Name_SEO'] ?>/<?php echo clean($row_feature['S_Title']) ?>/<?php echo isset($row_feature['S_ID']) ? $row_feature['S_ID'] : ''; ?>/">
                                    <img src="<?php echo IMG_LOC_REL . $feature_image; ?>" width="100%" height="auto" alt="<?php echo $row_feature['S_Title'] ?>" />
                                </a>
                                <a href="/<?php echo $storyCatSEORow['C_Name_SEO'] ?>/<?php echo clean($row_feature['S_Title']) ?>/<?php echo isset($row_feature['S_ID']) ? $row_feature['S_ID'] : ''; ?>/">
                                    <h1 align="center" class="heading-text"><?php echo $row_feature['S_Title'] ?></h1>
                                </a>
                                <?php
                                if ($row_feature['S_Description'] != '') {
                                    $string = preg_replace('/(.*)<\/p[^>]*>/i', '$1', $row_feature['S_Description']);
                                    ?>
                                    <div class="story-description">
                <?php echo $string ?>
                                        <a href="/<?php echo $storyCatSEORow['C_Name_SEO'] ?>/<?php echo clean($row_feature['S_Title']) ?>/<?php echo isset($row_feature['S_ID']) ? $row_feature['S_ID'] : ''; ?>/">
                                            Read More
                                        </a>
                                        </p>
                                    </div>
                            <?php } ?>
                            </div>
        <?php } ?>
                    </div>
                </div>  
            </div>
        </section>
    <?php } ?>
    <!-- Feature Stories end -->
    <?PHP
    $LiveEnt = true;
    if (isset($_REQUEST['submitEventAgree']) && $_REQUEST['submitEventAgree']) {
        $LiveEnt = false;
        require_once 'submitEvent.php';
    } else {
        ?>
        <!--Thumbnail Grid-->
        <section id="section_stories" class="thumbnail-grid <?php echo (($activeCat['C_Is_Blog'] == 1) ? "" : "padding-bottom-none") ?> border-none">
            <div class="grid-wrapper">
                <div class="grid-inner border-none padding-bottom-none">
                    <?PHP
                    if ($_SESSION['CATEGORY'] == 8) {
                        $getEventSeo = "SELECT C_Name_SEO FROM tbl_Category WHERE C_ID = '" . encode_strings($_SESSION['CATEGORY'], $db) . "'";
                        $getEventSeoResult = mysql_query($getEventSeo, $db);
                        $getEventSeoRow = mysql_fetch_assoc($getEventSeoResult);
                        ?>
                        <div class="add-new-event" >
                            <div class="add-new-event-wrapper" style="margin-bottom: 0;">
                                <a href="/<?php echo $getEventSeoRow['C_Name_SEO'] ?>/?submitEventAgree=1">+Add your event</a>
                            </div>
                        </div>
                        <?php
                    }
                    if ($_SESSION['CATEGORY'] != 8) {
                        $sql = "SELECT RC_Name, C_ID, C_Parent, C_Order, RC_Thumbnail, C_Name_SEO, C_Name FROM tbl_Category 
                            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = " . encode_strings($REGION['R_ID'], $db) . "
                            INNER JOIN tbl_Business_Listing_Category ON BLC_C_ID = C_ID   
                            INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID AND BLCR_BLC_R_ID " . encode_strings((($REGION['R_Parent'] == 0 && $REGION['R_Type'] != 4 ) ? "IN " . $regionList : "= " . $REGION['R_ID']), $db) . "
                            INNER JOIN tbl_Business_Listing ON BL_ID = BLC_BL_ID
                            WHERE C_Parent = '" . encode_strings($_SESSION['CATEGORY'], $db) . "' and RC_Status = 0 AND RC_R_ID > 0 AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = '1' $include_free_listings
                            GROUP BY C_ID ORDER BY RC_Order ASC";
                    } else {
                        $sql = "SELECT RC_Name, C_ID, C_Parent, C_Order, RC_Thumbnail, C_Name_SEO, C_Name FROM tbl_Category 
                            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($REGION['R_ID'], $db) . "'
                            WHERE C_Parent = '" . encode_strings($_SESSION['CATEGORY'], $db) . "' and RC_Status = 0 AND RC_R_ID > 0
                            ORDER BY RC_Order ASC";
                    }
                    $result = mysql_query($sql, $db) or die(mysql_error());
                    $i = 0;
                    while ($row = mysql_fetch_assoc($result)) {
                        $i++;
                        if ($row['RC_Thumbnail'] != '') {
                            $sub_cat_image = $row['RC_Thumbnail'];
                        } else {
                            $sub_cat_image = $default_thumbnail_image;
                        }
                        if ($i == 1) {
                            echo "<div class='thumbnails static-thumbs'>";
                        }
                        ?>
                        <div class="thumb-item">
                            <a href="/<?php echo $activeCat['C_Name_SEO'] ?>/<?php echo $row['C_Name_SEO'] ?>/">
                                <img src="<?php echo IMG_LOC_REL . $sub_cat_image ?>" alt="<?php echo $row['C_Name'] ?>" />
                                <h3 class="thumbnail-heading"><?php echo (($row['RC_Name'] != '') ? $row['RC_Name'] : $row['C_Name']) ?></h3>
                            </a>
                        </div>
                        <?php
                        if ($i == 3) {
                            echo '</div>';
                            $i = 0;
                        }
                    }
                    if ($activeCat['C_Is_Blog'] == 1) {
                        $categoryWhere = '';
                        $sql = "SELECT C_ID, C_Name, RC_Name FROM tbl_Region_Category
                                LEFT JOIN tbl_Category ON RC_C_ID = C_ID 
                                WHERE C_Parent = '" . encode_strings($_SESSION['CATEGORY'], $db) . "' AND RC_R_ID = " . $REGION['R_ID'] . " AND RC_Status=0 
                                GROUP BY C_ID ORDER BY RC_Order ASC";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($rowExploreSub = mysql_fetch_array($result)) {
                            $contid[] = $rowExploreSub['C_ID'];
                        }
                        $child = implode(',', $contid);
                        if ($child != '') {
                            $categoryWhere = ' AND S_Category IN (' . $child . ')';
                            $stories = "SELECT S_ID, S_Category, S_Active, S_Title, S_Thumbnail, C_Name, C_Parent, RC_Name FROM tbl_Story 
                                        INNER JOIN tbl_Story_Region ON S_ID = SR_S_ID 
                                        LEFT JOIN tbl_Content_Piece ON S_ID = CP_S_ID
                                        INNER JOIN tbl_Region_Category ON RC_C_ID = S_Category AND RC_Status=0
                                        LEFT JOIN tbl_Category ON RC_C_ID = C_ID
                                        $StorySeasons_JOIN
                                        WHERE SR_R_ID = " . $REGION['R_ID'] . " $categoryWhere  AND S_Active = 1  AND RC_Status = 0 AND RC_R_ID = " . $REGION['R_ID'] . "
                                        $StorySeasons_WHERE $WHERE GROUP BY S_ID ORDER BY S_Title ";
                            $resStory = mysql_query($stories, $db) or die("Invalid query: $stories -- " . mysql_error());
                            $pages = new Paginate(mysql_num_rows($resStory), 15);
                            $resStory = mysql_query($stories . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
                            $i = 0;
                            $contVal = mysql_num_rows($resStory);
                            if ($contVal > 0) {
                                while ($rowStory = mysql_fetch_assoc($resStory)) {
                                    $i++;
                                    if ($rowStory['S_Thumbnail'] != '') {
                                        $story_image = $rowStory['S_Thumbnail'];
                                    } else {
                                        $story_image = $default_thumbnail_image;
                                    }
                                    if ($i == 1) {
                                        echo "<div class='thumbnails static-thumbs'>";
                                    }
                                    ?> 
                                    <div class="thumb-item">
                                        <a href="/<?php echo $activeCat['C_Name_SEO'] ?>/<?php echo clean($rowStory['S_Title']) ?>/<?php echo isset($rowStory['S_ID']) ? $rowStory['S_ID'] : ''; ?>/">
                                            <img src="<?php echo IMG_LOC_REL . $story_image; ?>" alt="<?php echo $rowStory['S_Title'] ?>" />
                                            <h3 class="thumbnail-heading"><?php echo (($rowStory['RC_Name'] == "") ? $rowStory['C_Name'] : $rowStory['RC_Name']) ?></h3>
                                            <h3 class="thumbnail-desc"><?php echo $rowStory['S_Title'] ?></h3>
                                        </a>
                                    </div>
                                    <?php
                                    if ($i == 3) {
                                        echo '</div>';
                                        $i = 0;
                                    }
                                }
                            } else {
                                echo '<p class="noStory" >No Stories at This Time.</p>';
                            }
                        } else {
                            echo '<p class="noStory" >No Stories at This Time.</p>';
                        }
                    }
                    ?>
                </div>

                <?php
                //print_r($_SESSION['CATEGORY']); exit;
                if ($_SESSION['CATEGORY'] == 8) {
                    require_once 'events.php';
                }
                // display our pagination footer if set.
                if (isset($pages)) {
                    echo $pages->paginate();
                }
                ?>
            </div>
        </section>
        <!--Thumbnail Grid-->
        <?php
    }
}
?>