<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('manage-contests', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['id'] > 0) {
    $sql = "SELECT C_ID, C_Name FROM tbl_Contests WHERE C_ID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
}

if ($_REQUEST['id'] < 1) {
    exit('No Contest Selected.');
}

$sql = "SELECT CE_Date, CE_Email, CE_PostalCode FROM tbl_Contests_Entry WHERE CE_C_ID = '" . encode_strings($row['C_ID'], $db) . "' ORDER BY CE_Date";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$out = '"Emai","Postal Code","Date Entered"' . "\n";
while ($data = mysql_fetch_assoc($result)) {
    $out .= '"' . $data['CE_Email'] . '","' . $data['CE_PostalCode'] . '","' . $data['CE_Date'] . '"' . "\n";
}

$filename = 'contest-entries_' . $row['C_Name'] . "_" . date('Y-m-d') . '.csv';
$ctype = "application/force-download";

header("Pragma: public"); // required
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private", false); // required for certain browsers 
header("Content-Type: $ctype");

header("Content-Disposition: attachment; filename=\"" . $filename . "\";");
header("Content-Transfer-Encoding: binary");
print($out);
exit();
?>