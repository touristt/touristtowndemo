<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';
require_once '../include/track-data-entry.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS']) && !in_array('manage-county-region', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
    $category = $_REQUEST['cat'];
    $subcategory = $_REQUEST['subcat'];
    $sql = "SELECT R_ID, R_Name, R_Type, R_Whats_NearBy FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
    $sql = "SELECT C_Name, RC_Name FROM tbl_Category LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID
            WHERE C_ID = '" . encode_strings($subcategory, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeSubcat = mysql_fetch_assoc($result);

    $sql = "SELECT RM_Child FROM tbl_Region_Multiple WHERE RM_Parent = '" . encode_strings($regionID, $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($row = mysql_fetch_assoc($result)) {
        $childRegion[] = $row['RM_Child'];
    }
}

if (isset($_REQUEST['del']) && $_REQUEST['del'] == 'true') {
    $sql = "SELECT BLC_BL_ID FROM tbl_Business_Listing_Category WHERE BLC_BL_ID = '" . $_REQUEST['bl_id'] . "'";
    $result = mysql_query($sql) or die(mysql_error());
    $subcatCount = mysql_num_rows($result);
    $sql = "SELECT BLCR_BL_ID FROM tbl_Business_Listing_Category_Region WHERE BLCR_BL_ID = '" . $_REQUEST['bl_id'] . "'";
    $result = mysql_query($sql) or die(mysql_error());
    $regionCount = mysql_num_rows($result);
    $sql = "DELETE FROM tbl_Business_Listing_Order WHERE BLO_BL_ID = '" . $_REQUEST['bl_id'] . "' AND BLO_M_C_ID = '" . $category . "' 
            AND BLO_S_C_ID = '" . $subcategory . "' AND BLO_R_ID = '" . $regionID . "'";
    mysql_query($sql) or die(mysql_error());
    if ($subcatCount == 1 && $regionCount == 1) {
        $sql = "DELETE FROM tbl_Business_Listing_Category_Region WHERE BLCR_BLC_R_ID = '" . $_REQUEST['rid'] . "' AND BLCR_BL_ID = '" . $_REQUEST['bl_id'] . "'";
        mysql_query($sql) or die(mysql_error());
        $sql = "DELETE FROM tbl_Business_Listing_Category WHERE BLC_C_ID = '" . $subcategory . "' AND BLC_M_C_ID = '" . $category . "'
                AND BLC_BL_ID = '" . $_REQUEST['bl_id'] . "'";
        mysql_query($sql) or die(mysql_error());
    } elseif ($subcatCount == 1 && $regionCount > 1) {
        $sql = "DELETE FROM tbl_Business_Listing_Category_Region WHERE BLCR_BLC_R_ID = '" . $_REQUEST['rid'] . "' AND BLCR_BL_ID = '" . $_REQUEST['bl_id'] . "'";
        mysql_query($sql) or die(mysql_error());
    } elseif ($subcatCount > 1 && $regionCount == 1) {
        $sql = "DELETE FROM tbl_Business_Listing_Category WHERE BLC_C_ID = '" . $subcategory . "' AND BLC_M_C_ID = '" . $category . "'
                AND BLC_BL_ID = '" . $_REQUEST['bl_id'] . "'";
        mysql_query($sql) or die(mysql_error());
    } elseif ($subcatCount > 1 && $regionCount > 1) {
        $sql = "DELETE FROM tbl_Business_Listing_Category WHERE BLC_C_ID = '" . $subcategory . "' AND BLC_M_C_ID = '" . $category . "'
                AND BLC_BL_ID = '" . $_REQUEST['bl_id'] . "'";
        mysql_query($sql) or die(mysql_error());
    }

    $_SESSION['listing_delete'] = 1;
    // TRACK DATA ENTRY
    Track_Data_Entry('Websites', $regionID, 'Listings', $_REQUEST['bl_id'], 'Remove Listing', 'super admin');
    if ($_REQUEST['page'] > 0) {
        $page = "&page=" . $_REQUEST['page'];
    } else {
        $page = "";
    }
    header("Location: /admin/region-listings.php?rid=" . $regionID . "&cat=" . $category . "&subcat=" . $subcategory . $page);
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">

    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Listings</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <div class="content-sub-header link-header region-header-padding">
            <div class="data-column spl-name padding-none" style="margin-top: 10px;">Select Listings for <?php echo $activeRegion['R_Name'] ?>
            </div>
            <div style="float: right;">
                <form name="form2" method="GET" id="" action="" onsubmit="return false;">
                    <input type="hidden" name="rid" value="<?php echo $regionID ?>">
                    <input type="text" name="search_txt" id="search_txt" value="<?php echo $_REQUEST['search_txt'] ?>" required>
                    <input type="submit" name="search_listing" value="Search" onclick="filter_listings('', '', '', '', $('#search_txt').val())">
                </form>
            </div>
        </div>
        <form name="form1" method="POST" id="saveListing">
            <input type="hidden" name="rid" id="rid" value="<?php echo $regionID ?>">
            <input type="hidden" name="cat" id="cat" value="<?php echo $category ?>">
            <input type="hidden" name="subcat" id="subcat" value="<?php echo $subcategory ?>">
            <input type="hidden" name="op" value="save">
            <div class="content-sub-header link-header region-header-padding">
                <div class="data-column spl-org-listngs padding-org-listing">
                    <select name="listing_type" id="listing_type" onchange="filter_listings($('#sort_region').val(), $('#categorydd').val(), $('#advert-subcats').val(), this.value, '')">
                        <option value="0">All</option>
                        <?PHP
                        $sql = "SELECT * FROM tbl_Listing_Type";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $row['LT_ID'] ?>" <?php echo ($row['LT_ID'] == $sort_region) ? 'selected' : '' ?> ><?php echo $row['LT_Name']; ?> </option>
                            <?php
                        }
                        ?>
                    </select>
                    <select name="sort_region" id="sort_region" onchange="getCategories(this.value), filter_listings(this.value, '', '', $('#listing_type').val(), '')" >
                        <option value="">Select Region:</option>
                        <option value="-1">All</option>
                        <?PHP
                        $sql = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Type NOT IN(1,6) ORDER BY R_Name";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_assoc($result)) {
                            if ($activeRegion['R_Type'] == 4) {
                                foreach ($childRegion as $ch) {
                                    if ($row['R_ID'] == $ch) {
                                        ?>
                                        <option value="<?php echo $row['R_ID'] ?>" <?php echo ($row['R_ID'] == $sort_region) ? 'selected' : '' ?> ><?php echo $row['R_Name']; ?> </option>
                                        <?php
                                    }
                                }
                            } else {
                                ?>
                                <option value="<?php echo $row['R_ID'] ?>" <?php echo ($row['R_ID'] == $sort_region) ? 'selected' : '' ?> ><?php echo $row['R_Name'] ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                    <select name="sortby" id="categorydd" onchange="getSubOptions($('#sort_region').val(), this.value)">
                        <option value="">Select Category:</option>
                    </select>
                    <select name="subcategory" id="advert-subcats" onchange="filter_listings($('#sort_region').val(), $('#categorydd').val(), this.value, $('#listing_type').val(), '')">
                        <option value="">Select Sub Category</option>
                    </select>

                    <select name="listings" id="listings" required>
                        <option value="">Select Listing</option>
                    </select> 

                    <input type="submit" value="+Add" class="stories-add-button" onclick="return saveListing('saveListing');" />
                </div>
            </div>
        </form>
        <div class="reorder-category">
            <?PHP
            $sql = "SELECT DISTINCT BL_ID, BL_Listing_Title, BLCR_BLC_R_ID, BLO_ID FROM tbl_Business_Listing 
                    LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                    INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID
                    LEFT JOIN tbl_Business_Listing_Order ON BL_ID = BLO_BL_ID AND BLO_R_ID = '" . $regionID . "' AND BLO_M_C_ID = '" . $category . "' 
                    AND BLO_S_C_ID = '" . $subcategory . "'
                    WHERE (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = 1 AND BLC_C_ID = '" . $subcategory . "' 
                    AND BLCR_BLC_R_ID = '" . encode_strings($regionID, $db) . "' GROUP BY BL_ID ORDER BY BLO_Order ASC";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                $blo_id = $row['BLO_ID'];
                if ($blo_id == '') {
                    $sqlCount = "SELECT BLO_Order FROM tbl_Business_Listing_Order WHERE BLO_R_ID = '" . $regionID . "' AND BLO_M_C_ID = '" . $category . "' 
                                 AND BLO_S_C_ID = '" . $subcategory . "' ORDER BY BLO_Order DESC LIMIT 1";
                    $resultCount = mysql_query($sqlCount) or die(mysql_error());
                    $orderCount = mysql_num_rows($resultCount);
                    $rowMax = mysql_fetch_row($resultCount);
                    if ($orderCount == 0) {
                        $sqlInsert = "INSERT tbl_Business_Listing_Order SET BLO_BL_ID = '" . $row['BL_ID'] . "', BLO_M_C_ID = '" . $category . "', 
                                      BLO_S_C_ID = '" . $subcategory . "', BLO_R_ID = '" . $regionID . "', BLO_Order = '1'";
                        mysql_query($sqlInsert) or die(mysql_error());
                        $blo_id = mysql_insert_id();
                    } else {
                        $sqlInsert = "INSERT tbl_Business_Listing_Order SET BLO_BL_ID = '" . $row['BL_ID'] . "', BLO_M_C_ID = '" . $category . "', 
                                      BLO_S_C_ID = '" . $subcategory . "', BLO_R_ID = '" . $regionID . "', BLO_Order = '" . ($rowMax[0] + 1) . "'";
                        mysql_query($sqlInsert) or die(mysql_error());
                        $blo_id = mysql_insert_id();
                    }
                }
                ?>
                <div class="data-content" id="recordsArray_<?php echo $blo_id ?>">
                    <div class="data-column spl-org-listngs" id="whats_near_<?php echo $row['BL_ID'] ?>" >
                        <div class="select-listings-nearby-title check11">
                            <?php echo $row['BL_Listing_Title'] ?>
                        </div>
                        <div class="remove-link"><a href="region-listings.php?rid=<?php echo $activeRegion['R_ID']; ?>&cat=<?php echo $category; ?>&subcat=<?php echo $subcategory; ?>&bl_id=<?php echo $row['BL_ID']; ?>&del=true" onclick="return confirm('Are you sure you want to perform this action?');">Remove</a></div>

                        <?php if ($activeRegion['R_Whats_NearBy'] == 1) { ?>
                            <div class="select-listings-nearby" id="remove_near_<?php echo $row['BL_ID'] ?>"><a href="region-listings-nearby.php?bl_id=<?php echo $row['BL_ID'] ?>&rid=<?php echo $activeRegion['R_ID'] ?>">What's Nearby?</a></div>
                        <?php } ?>
                    </div>
                </div>
                <?PHP
            }
            ?>
        </div>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script>
    $(function () {
        $(".reorder-category").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Business_Listing_Order&field=BLO_Order&id=BLO_ID';
                $.post("reorder.php", order, function (theResponse) {
                    swal("Listings Re-ordered!", "Listings Re-ordered successfully.", "success");
                });
            }
        });
    });
    function getSubOptions(r_id, cat_id) {
        $("#search_txt").val("");
        var current_region = $('#rid').val();
        var current_cat = $('#cat').val();
        var current_subcat = $('#subcat').val();
        $.ajax({
            type: "POST",
            url: 'filterListings.php',
            dataType: 'json',
            data: {
                category: cat_id,
                region: r_id,
                rl: 1,
                c_region: current_region,
                c_cat: current_cat,
                c_subcat: current_subcat
            }
        }).done(function (response) {
            $("#advert-subcats").empty();
            $("#advert-subcats").html(response.subcat);

            $('#listings').html(response.listings);
        });
    }

    function getCategories(r_id) {
        $("#search_txt").val("");
        $.ajax({
            type: "POST",
            url: 'filterListings.php',
            dataType: 'json',
            data: {
                region: r_id
            }
        }).done(function (response) {
            $("#categorydd").empty();
            $("#categorydd").html(response.maincat);

            $("#advert-subcats").empty();
            $("#advert-subcats").html("<option>Select Sub Category</option>");

            $("#listings").empty();
            $("#listings").html("<option>Select Listing</option>");
        });
    }

    function filter_listings(rid, catid, subcatid, listing_type, search_text) {
        var current_region = $('#rid').val();
        var current_cat = $('#cat').val();
        var current_subcat = $('#subcat').val();
        if (search_text != '') {
            $("#listing_type").val("0");

            $("#sort_region").val("");

            $("#categorydd").empty();
            $("#categorydd").html("<option>Select Category</option>");

            $("#advert-subcats").empty();
            $("#advert-subcats").html("<option>Select Sub Category</option>");

            $("#listings").empty();
            $("#listings").html("<option>Select Listing</option>");
        } else {
            $("#search_txt").val("");
        }
        $.ajax({
            type: "POST",
            url: 'filterListings.php',
            dataType: 'json',
            data: {
                region: rid,
                category: catid,
                subcategory: subcatid,
                listing_type: listing_type,
                search_text: search_text,
                rl: 1,
                c_region: current_region,
                c_cat: current_cat,
                c_subcat: current_subcat
            },
            success: function (response) {
                $('#listings').html(response.listings);
            }
        });
    }
    //save listing
    function saveListing(form) {
        var formdata = $('#' + form).serialize();
        $.ajax({
            type: "POST",
            url: 'saveListing.php',
            dataType: 'json',
            data: formdata,
            success: function (response) {
                filter_listings($('#sort_region').val(), $('#categorydd').val(), $('#advert-subcats').val(), $('#listing_type').val(), $('#search_txt').val());
                if (response.success && response.success == 1) {
                    swal("Listing Added", "Listing has been added successfully.", "success");
                }
                if (response.Listings && response.Listings !== '') {
                    $('.reorder-category').html(response.Listings);
                }
            }
        });
        return false;
    }
</script>