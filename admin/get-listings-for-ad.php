<?PHP

require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

$select = " SELECT BL_ID, BL_Listing_Title FROM tbl_Business_Listing 
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
            INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID 
            WHERE (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) 
            AND hide_show_listing = 1 GROUP BY BL_ID ORDER BY BL_Listing_Title";
$listings = mysql_query($select, $db) or die("Invalid query: $select -- " . mysql_error());
$key_name = '';
while ($list_row = mysql_fetch_array($listings)) {
    $key_name .= $list_row['BL_ID'] . '/' . $list_row['BL_Listing_Title'] . '@';
}
print_r($key_name);
?>