<?php

require_once '../include/config.inc.php';
require_once '../include/adminFunctions.inc.php';

if (isset($_REQUEST['export_ad_stats'])) {
// Download the file
    $region = '';
    $month = '';
    $year = '';
    $region_id = '';
    $month_id = '';
    $year_id = '';
    if ($_REQUEST['region_filter'] && $_REQUEST['region_filter'] != '') {
       // $region = $_REQUEST['region_filter'];
        $region_id = $_REQUEST['region_filter'];
        $region = 'AND R_ID  =' . $region_id;
    }
//    $month = $_REQUEST['month_filter'];
//    $year = $_REQUEST['year_filter'];
    ////Yeare and mnths
    if (isset($_REQUEST['month_filter']) && $_REQUEST['month_filter'] != '') {
        $month_id = $_REQUEST['month_filter'];
        $month = 'AND MONTH(AS_Date)   =' . $month_id;
    }
    if (isset($_REQUEST['year_filter']) && $_REQUEST['year_filter'] != '') {
        $year_id = $_REQUEST['year_filter'];
        $year = 'AND  YEAR(AS_Date)   =' . $year_id;
    }

    $filename = "active_ads_stats.csv";
    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename=' . $filename);
    header("Pragma: no-cache");
    header("Expires: 0");
    echo "\xEF\xBB\xBF";
// Fetch region
    $output = '';
    $output .= 'Category ,';
    $sql = "SELECT * FROM tbl_Advertisement_Type WHERE AT_ID NOT IN(3,4)";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $count = 0;

    while ($rowAT = mysql_fetch_assoc($result)) {
        $count++;
        $AT_ID[$count] = $rowAT['AT_ID'];
        /* if ($rowAT['AT_ID'] == 4) {
          $output .='Homepage , ,';
          } else { */
        $output .= $rowAT['AT_Name'] . ', ,';
        //}
    }
    $output .= ' Total ';
    $output .= "\n";
    echo $output;
    for ($j = 0; $j < 4; $j++) {
        if ($j == 0) {
            $output = ' , ';
        } else {
            $output .= 'Imp ,';
            $output .= 'Clicks ,';
        }
    }
    $output .= "\n";
    echo $output;


    $totalImpressions = 0;
    $totalClicks = 0;
    $totalImpressionsTypeBased = '';
    $totalClicksTypeBased = '';
    $sql = "SELECT C_ID, C_Name, RC_Name FROM `tbl_Category` 
            LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID
            LEFT JOIN tbl_Region ON RC_R_ID = R_ID 
            WHERE C_Parent = 0 $region GROUP BY C_ID ORDER BY RC_Name ASC";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($rowCat = mysql_fetch_assoc($result)) {
        $totalImpressionsCatBased = '';
        $totalClicksCatBased = '';
        $output = ($rowCat['RC_Name'] != '') ? $rowCat['RC_Name'] . ' ,' : $rowCat['C_Name'] . ' ,';
        for ($i = 1; $i <= $count; $i++) {
            if ($AT_ID[$i] == 4) {
                $record = show_ads_report($region_id, 0, $AT_ID[$i], $month_id, $year_id);
            } else {
                $record = show_ads_report($region_id, $rowCat['C_ID'], $AT_ID[$i], $month_id, $year_id);
                $totalImpressionsCatBased += $record['impressions'];
                $totalClicksCatBased += $record['clicks'];
            }
            $totalImpressionsTypeBased[$i] += $record['impressions'];
            $totalClicksTypeBased[$i] += $record['clicks'];
            if ($AT_ID[$i] == 4) {
                $output .= 'NA , NA ,';
            } else {
                if ($record['impressions'] != '') {
                    $output .= $record['impressions'] . ' ,';
                } else {
                    $output .= '0 ,';
                }
                if ($record['clicks'] != '') {
                    $output .= $record['clicks'] . ' ,';
                } else {
                    $output .= '0 ,';
                }
            }
        }
        $output .= $totalImpressionsCatBased . ' ,';
        $output .= $totalClicksCatBased . ' ,';
        $output .= "\n";
        echo $output;
    }
    $output = 'Total ,';
    for ($j = 1; $j <= $count; $j++) {
        $totalImpressions += $totalImpressionsTypeBased[$j];
        $totalClicks += $totalClicksTypeBased[$j];
        $output .= $totalImpressionsTypeBased[$j] . ' ,';
        $output .= $totalClicksTypeBased[$j] . ' ,';
    }
    $output .= $totalImpressions . ' ,';
    $output .= $totalClicks . ' ,';
    $output .= "\n";
    echo $output;
}
?>



