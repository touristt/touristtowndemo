<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if (!in_array('manage-routes', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} else {
    header("Location: /admin/regions.php");
    exit();
}
$where = "";
if (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
    $where = ' WHERE IRR_R_ID IN (' . $_SESSION['USER_LIMIT'] . ')';
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $IR_ID = $_REQUEST['id'];
    $RID = $_REQUEST['rid'];
    $sql = "DELETE tbl_Individual_Route, tbl_Individual_Route_Gallery, tbl_Individual_Route_Downloads, 
            tbl_Individual_Route_Map_Listings, tbl_Individual_Route_Category, tbl_Individual_Route_Region
            FROM tbl_Individual_Route
            LEFT JOIN tbl_Individual_Route_Gallery ON IR_ID = IRG_IR_ID
            LEFT JOIN tbl_Individual_Route_Downloads ON IR_ID = IRD_IR_ID
            LEFT JOIN tbl_Individual_Route_Map_Listings ON IR_ID = IRML_IR_ID
            LEFT JOIN tbl_Individual_Route_Category ON IR_ID = IRC_IR_ID
            LEFT JOIN tbl_Individual_Route_Region ON IR_ID = IRR_IR_ID
            WHERE IR_ID = '" . $IR_ID . "'";
    $result = mysql_query($sql) or die(mysql_error());
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $RID, 'Manage Routes', $IR_ID, 'Delete', 'super admin');
        imageBankUsageDelete('IBU_Individual_Route', $IR_ID);
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: routes.php?rid=" . $RID);
    exit();
}
//Get Active Region Information
$sql = "SELECT R_Type, R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Manage Routes</div>
        <div class="link"><a href="routes-categories.php?rid=<?php echo $regionID ?>">Route Categories</a></div>
        <div class="link"><a href="route.php?rid=<?php echo $regionID ?>">+ Add Route</a></div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">
            Manage Individual Routes

        </div>
        <div class="content-sub-header">
            <div class="data-column mup-name padding-none">Name</div>
            <div class="data-column spl-other padding-none">Edit</div>
            <div class="data-column spl-other padding-none">Delete</div>
            <div class="data-column spl-other padding-none">Map Listings</div>
        </div>
        <div class="reorder-category">
            <?PHP
            $sqlRoute = "SELECT DISTINCT tbl_Individual_Route.* FROM tbl_Individual_Route LEFT JOIN tbl_Individual_Route_Region ON IR_ID = IRR_IR_ID 
                         $where ORDER BY IR_Title ASC";

            $resRoute = mysql_query($sqlRoute, $db) or die("Invalid query: $sqlRoute -- " . mysql_error());
            while ($rowRoute = mysql_fetch_assoc($resRoute)) {
                ?>
                <div class="data-content">
                    <div class="data-column mup-name">
                        <?php echo $rowRoute['IR_Title']; ?>
                    </div>
                    <div class="data-column spl-other">
                        <a href="route.php?rid=<?php echo $activeRegion['R_ID'] ?>&id=<?php echo $rowRoute['IR_ID'] ?>">Edit</a>
                    </div>
                    <div class="data-column spl-other">
                        <a onclick="return confirm('Are you sure?')" href="routes.php?op=del&rid=<?php echo $activeRegion['R_ID'] ?>&id=<?php echo $rowRoute['IR_ID'] ?>">Delete</a>
                    </div>
                    <div class="data-column spl-other">
                        <a href="route-listings.php?rid=<?php echo $activeRegion['R_ID'] ?>&id=<?php echo $rowRoute['IR_ID'] ?>">Map Listings</a>
                    </div>
                </div>
                <?PHP
            }
            ?>
        </div>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>