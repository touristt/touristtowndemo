<?PHP
require_once '../include/config.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('manage-support', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$S_ID = $_REQUEST['id'];
$result = mysql_query("DELETE FROM tbl_Support_Section WHERE SS_ID = '$S_ID'");
$result2 = mysql_query("DELETE FROM tbl_Support WHERE S_SS_ID = '$S_ID'");
if ($result && $result2) {
    $_SESSION['delete'] = 1;
    // TRACK DATA ENTRY
    $id = $_REQUEST['id'];
    Track_Data_Entry('Support', '', 'Support', $id, 'Delete', 'super admin');
} else {
    $_SESSION['delete_error'] = 1;
}
header("Location: support-list.php");
?>