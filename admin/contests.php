<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('manage-contests', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Contests</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <div class="sub-menu">
            <ul>
                <li><a href="/admin/contests.php">Manage Contests</a></li>
                <li><a href="/admin/contest.php">+Add Contest</a></li>
            </ul>
        </div>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column cust-listing-title padding-none">Name</div>
            <div class="data-column cust-listing-other padding-none">Entries</div>
            <div class="data-column cust-listing-other padding-none">Stats</div>
            <div class="data-column cust-listing-other padding-none">Delete</div>
        </div>
        <?PHP
        $sql = "SELECT C_ID, C_Name FROM tbl_Contests ORDER BY C_Name";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            $sql = "SELECT COUNT(*) FROM tbl_Contests_Entry WHERE CE_C_ID = '" . encode_strings($row['C_ID'], $db) . "'";
            $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $rowEntries = mysql_fetch_row($resultTMP);
            ?>
            <div class="data-content">
                <div class="data-column cust-listing-title"><a href="contest.php?id=<?php echo  $row['C_ID'] ?>">
                        <?php echo  $row['C_Name'] ?>
                    </a></div>
                <div class="data-column cust-listing-other">
                    <?php echo  $rowEntries[0] ?>
                </div>
                <div class="data-column cust-listing-other"><a href="contest-stats.php?id=<?php echo  $row['C_ID'] ?>">Stats</a></div>
                <div class="data-column cust-listing-other"><a onClick="return confirm('Are you sure this action can not be undone!');" href="contest.php?op=del&amp;id=<?php echo  $row['C_ID'] ?>">Delete</a></div>
            </div>
            <?PHP
        }
        ?>
    </div>
</div>
<?PHP
require_once '..include/admin/footer.php';
?>