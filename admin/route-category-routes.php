<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('manage-routes', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} else {
    header("Location: /admin/regions.php");
    exit();
}

$where = "";
if (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
    $where = ' AND IRR_R_ID IN (' . $_SESSION['USER_LIMIT'] . ')';
}

if ($_REQUEST['rcid'] > 0) {
    $routeCatID = $_REQUEST['rcid'];
} else {
    header("Location: /admin/regions.php");
    exit();
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'save') {
    $route = $_REQUEST['route'];
    $RC_ID = $_REQUEST['rcid'];
    $RID = $_REQUEST['rid'];
    if ($route > 0) {
        $sqlMax = "SELECT MAX(IRC_Order) FROM tbl_Individual_Route_Category WHERE IRC_RC_ID = '$RC_ID'";
        $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
        $rowMax = mysql_fetch_row($resultMax);
        $sql = "INSERT tbl_Individual_Route_Category SET IRC_IR_ID = '" . $route . "', IRC_RC_ID = '" . $RC_ID . "', IRC_Order = '" . ($rowMax[0] + 1) . "'";
        $result = mysql_query($sql) or die(mysql_error());
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $RID, 'Manage Category Routes', $RC_ID, 'Add Category Route', 'super admin');
        header("Location: route-category-routes.php?rid=" . $RID . "&rcid=" . $RC_ID);
        exit();
    }
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $IRC_ID = $_REQUEST['id'];
    $RC_ID = $_REQUEST['rcid'];
    $RID = $_REQUEST['rid'];
    $sql = "DELETE FROM tbl_Individual_Route_Category WHERE IRC_ID = '" . $IRC_ID . "'";
    $result = mysql_query($sql) or die(mysql_error());
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $RID, 'Manage Category Routes', $RC_ID, 'Delete Category Route', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: route-category-routes.php?rid=" . $RID . "&rcid=" . $RC_ID);
    exit();
}
//Get Active Region Information
$sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);
//Get Active Route Category Information
$sqlRouteCat = "SELECT RC_Name FROM tbl_Route_Category WHERE RC_ID = '" . encode_strings($routeCatID, $db) . "' LIMIT 1";
$resRouteCat = mysql_query($sqlRouteCat, $db) or die("Invalid query: $sqlRouteCat -- " . mysql_error());
$activeRouteCat = mysql_fetch_assoc($resRouteCat);

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Manage Routes</div>
        <div class="link"><a href="routes.php?rid=<?php echo $regionID ?>">Individual Routes</a></div>
        <div class="link"><a href="routes-categories.php?rid=<?php echo $regionID ?>">Route Categories</a></div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">
            Manage Category Routes - <?php echo $activeRouteCat['RC_Name'] ?>

        </div>
        <div class="content-sub-header link-header region-header-padding">
            <form name="route_category_form" method="POST">
                <input type="hidden" name="rid" value="<?php echo $regionID ?>">
                <input type="hidden" name="rcid" value="<?php echo $routeCatID ?>">
                <input type="hidden" name="op" value="save">
                <div class="data-column spl-org-listngs padding-org-listing">
                    <select name="route" class="stories-cat" required>
                        <option value="">Select Route</option>
                        <?PHP
                        $sqlR = "SELECT DISTINCT IR_ID, IR_Title FROM tbl_Individual_Route LEFT JOIN tbl_Individual_Route_Region ON IR_ID = IRR_IR_ID 
                         WHERE IR_ID NOT IN (SELECT IRC_IR_ID FROM tbl_Individual_Route_Category WHERE IRC_RC_ID = '" . $routeCatID . "')
                         $where ORDER BY IR_Title ASC";
                        $resR = mysql_query($sqlR, $db) or die("Invalid query: $sqlR -- " . mysql_error());
                        while ($rowR = mysql_fetch_assoc($resR)) {
                            ?>
                            <option value="<?php echo $rowR['IR_ID'] ?>"><?php echo $rowR['IR_Title'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <input type="submit" value="+Add" class="stories-add-button">
                </div>

            </form>
        </div>
        <div class="content-sub-header">
            <div class="data-column mup-name padding-none">Name</div>
            <div class="data-column spl-other padding-none">&nbsp;</div>
            <div class="data-column spl-other padding-none">&nbsp;</div>
            <div class="data-column spl-other padding-none">Remove</div>
        </div>
        <div class="reorder-category">
            <?PHP
            $sqlRoute = "SELECT IR_ID, IR_Title, IRC_ID, IRC_RC_ID FROM tbl_Individual_Route 
                         INNER JOIN tbl_Individual_Route_Category ON IR_ID = IRC_IR_ID 
                         WHERE IRC_RC_ID = '" . $routeCatID . "'
                         GROUP BY IR_ID
                         ORDER BY IRC_Order ASC";

            $resRoute = mysql_query($sqlRoute, $db) or die("Invalid query: $sqlRoute -- " . mysql_error());
            while ($rowRoute = mysql_fetch_assoc($resRoute)) {
                ?>
                <div class="data-content" id="recordsArray_<?php echo $rowRoute['IRC_ID'] ?>">
                    <div class="data-column mup-name">
                        <?php echo $rowRoute['IR_Title']; ?>
                    </div>
                    <div class="data-column spl-other">&nbsp;</div>
                    <div class="data-column spl-other">&nbsp;</div>
                    <div class="data-column spl-other">
                        <a onclick="return confirm('Are you sure?')" href="route-category-routes.php?op=del&rid=<?php echo $activeRegion['R_ID'] ?>&id=<?php echo $rowRoute['IRC_ID'] ?>&rcid=<?php echo $rowRoute['IRC_RC_ID'] ?>">Remove</a>
                    </div>
                </div>
                <?PHP
            }
            ?>
        </div>
    </div>
</div>
<div id="dialog-message"></div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script>
    $(function () {
        $(".reorder-category").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Individual_Route_Category&field=IRC_Order&id=IRC_ID';
                $.post("reorder.php", order, function (theResponse) {
                    swal("Routes Re-ordered!", "Routes Re-Ordered successfully.", "success");
                });
            }
        });
    });
</script>