<?php
require_once '../include/config.inc.php';

$regionID = $_POST['rid'];
$categoryID = $_POST['id'];
$type = $_POST['type'];
$category = $_POST['category'];
$inclause = '';
//return
$stories = '<option value="">Select Story</option>';

//if all selected
if ($category > 0) {
    //check which dropdown
    if ($type == 'map_story') {
        $inclause = "SELECT SHC_S_ID FROM tbl_Story_Homepage_Category WHERE SHC_R_ID = '" . $regionID . "' AND SHC_Map = 1";
    } else if ($type == 'feature_story') {
        $inclause = "SELECT SHC_S_ID FROM tbl_Story_Homepage_Category WHERE SHC_R_ID = '" . $regionID . "' AND SHC_Homepage = 1 AND SHC_Feature = 1";
    } else if ($type == 'secondary_story') {
        $inclause = "SELECT SHC_S_ID FROM tbl_Story_Homepage_Category WHERE SHC_R_ID = '" . $regionID . "' AND SHC_Homepage = 1 AND SHC_Secondary = 1";
    } else if ($type == 'feature_cat_story') {
        $inclause = "SELECT SHC_S_ID FROM tbl_Story_Homepage_Category WHERE SHC_R_ID = '" . $regionID . "' AND SHC_Category = '" . $categoryID . "' AND SHC_Feature = 1";
    } else if ($type == 'overall_story') {
        $sqlStories = "SELECT S_ID, S_Title FROM tbl_Story WHERE S_Category = '" . $category . "'  AND S_ID NOT IN 
                (SELECT SR_S_ID FROM tbl_Story_Region WHERE SR_R_ID =  $regionID ) ORDER BY S_Title";
        $resStories = mysql_query($sqlStories);
    }
    if ($type != 'overall_story') {
        $sqlStories = "SELECT S_ID, S_Title FROM tbl_Story LEFT JOIN tbl_Story_Region ON S_ID = SR_S_ID WHERE S_Category = '" . $category . "' AND SR_R_ID = '" . $regionID . "'
                AND S_ID NOT IN 
                (" . $inclause . ")
                ORDER BY S_Title";
        $resStories = mysql_query($sqlStories);
    }
    while ($Stories = mysql_fetch_assoc($resStories)) {
        $stories .= '<option value="' . $Stories['S_ID'] . '">' . $Stories['S_Title'] . '</option>';
    }
}
$return['stories'] = $stories;
$return['type'] = $type;
print json_encode($return);
exit;
?>