<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('manage-users', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['id'] > 0) {
    $sql = "SELECT ER_ID, ER_Email, ER_Type FROM tbl_Email_Recipients WHERE ER_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
}

if ($_POST['op'] == 'save') {
    $sql = "tbl_Email_Recipients SET 
            ER_Email = '" . encode_strings($_REQUEST['email'], $db) . "', 
            ER_Type = '" . encode_strings($_REQUEST['type'], $db) . "',
            ER_Status = '" . encode_strings(1, $db) . "'";
    if ($row['ER_ID'] > 0) {
        $sql_check_email = "SELECT * FROM tbl_Email_Recipients WHERE ER_Email = '" . encode_strings($_REQUEST['email'], $db) . "' AND 
                            ER_Type = '" . encode_strings($_REQUEST['type'], $db) . "'";
        $result_check_email = mysql_query($sql_check_email, $db);
        $count_check_email = mysql_num_rows($result_check_email);
        if ($count_check_email > 0) {
            $_SESSION['email_already_exist'] = 1;
            header("Location: add_email_reciption.php");
            exit();
        } else {
            $sql = "UPDATE " . $sql . " WHERE ER_ID = '" . $row['ER_ID'] . "'";
            $result = mysql_query($sql, $db);
            // TRACK DATA ENTRY
            $id = $row['ER_ID'];
            Track_Data_Entry('Email', '', 'Manage Email Recipients', $id, 'Update Recipient', 'super admin');
        }
    } else {
        $sql_check_email = "SELECT * FROM tbl_Email_Recipients WHERE ER_Email = '" . encode_strings($_REQUEST['email'], $db) . "' AND
                             ER_Type = '" . encode_strings($_REQUEST['type'], $db) . "'";
        $result_check_email = mysql_query($sql_check_email, $db);
        $count_check_email = mysql_num_rows($result_check_email);
        if ($count_check_email > 0) {
            $_SESSION['email_already_exist'] = 1;
            header("Location: add_email_reciption.php");
            exit();
        } else {
            $sql = "INSERT " . $sql;
            $result = mysql_query($sql, $db);
            $id = mysql_insert_id();
            // TRACK DATA ENTRY
            Track_Data_Entry('Email', '', 'Manage Email Recipients', $id, 'Add Recipient', 'super admin');
        }
    }
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: email_reciptions.php");
    exit();
} elseif ($_GET['op'] == 'del') {
    $sql = "DELETE FROM tbl_Email_Recipients WHERE ER_ID = '" . $row['ER_ID'] . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $row['ER_ID'];
        Track_Data_Entry('Email', '', 'Manage Email Recipients', $id, 'Delete Recipient', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }

    header("Location: email_reciptions.php");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Email Notification Recipients</div>
        <div class="link">
        </div>
    </div>
    <div class="left"><?php require_once('../include/nav-home-sub.php'); ?></div>
    <div class="right">
        <form name="form1" method="post" action="add_email_reciption.php" enctype="multipart/form-data">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="id" value="<?php echo $row['ER_ID'] ?>">
            <div class="content-header">Details</div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Email Address</label>
                <div class="form-data">
                    <input name="email" type="email" value="<?php echo $row['ER_Email'] ?>" size="40" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Type</label>
                <div class="form-data">
                    <select name="type" required>
                        <option value="">Select Type</option>   
                        <?php
                        $sql_email_type = "SELECT * FROM  tbl_Email_Recipients_Type";
                        $Result_email_type = mysql_query($sql_email_type, $db) or die("Invalid query: $sql_email_type -- " . mysql_error());
                        while ($Row_email_type = mysql_fetch_assoc($Result_email_type)) {
                            ?>
                            <option value="<?php echo $Row_email_type['ERT_ID'] ?>" <?php echo ($Row_email_type['ERT_ID'] == $row['ER_Type']) ? 'Selected' : '' ?> ><?php echo $Row_email_type['ERT_Title'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="button5" id="button52" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>