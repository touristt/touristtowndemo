<?php
require_once '../include/config.inc.php';
require_once '../include/adminFunctions.inc.php';

if (isset($_REQUEST['export_contacts'])) {
// Download the file
    $filename = "Contact List.csv";
    header('Content-type: application/csv');
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment;filename=' . $filename);
    header("Pragma: no-cache");
    header("Expires: 0");
    echo "\xEF\xBB\xBF";
    // Filter customers by region
    // Filter customers by region
    $output = "";
    $output .= "Name , Email, Phone, Organization, Contact, Notes";
    $output .= "\n";
    $sql = "SELECT * FROM tbl_Client_Contacts";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($rowAT = mysql_fetch_assoc($result)) {
        $count++;
        $output .= '"' . str_replace('"', '""', $rowAT['CL_Name']) . '"'. ',' . '"' . str_replace('"', '""', $rowAT['CL_Email']) . '"' . ','
                 . '"' . str_replace('"', '""', $rowAT['CL_Phone']) . '"' . ',' . '"' . str_replace('"', '""', $rowAT['CL_Organization']) . '"' . ','
                 . '"' . str_replace('"', '""', $rowAT['CL_Contact']) . '"' . ',' . '"' . str_replace('"', '""', strip_tags($rowAT['CL_Notes'])) . '"';
        $output .= "\n";
    }
    echo $output;
}
?>



