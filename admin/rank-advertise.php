<?php
require_once '../include/login.inc.php';
require_once '../include/ranking.inc.php';

if (isset($_REQUEST['bl_id'])) {
    $BL_ID = $_REQUEST['bl_id'];
    $sql = "SELECT BL_Listing_Type FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    // get regions and categories of this listing
    $sql_region = " SELECT BLC_M_C_ID, BLC_C_ID, BLC_BL_ID, BLCR_BLC_R_ID, BL_Points, R_Parent, RM_Parent FROM tbl_Business_Listing
                    LEFT JOIN tbl_Business_Listing_Category  ON BLC_BL_ID = BL_ID
                    LEFT JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID 
                    LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                    LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID
                    WHERE BLC_BL_ID = '$BL_ID'";
    $res_region = mysql_query($sql_region);
    $regions = array();
    $category = array();
    $subcategories = array();
    while ($r = mysql_fetch_assoc($res_region)) {
        // add region parent as well if available
        if ($r['RM_Parent'] != 0 && !in_array($r['RM_Parent'], $regions)) {
            $regions[] = $r['RM_Parent'];
        }
        // add region
        if ($r['BLCR_BLC_R_ID'] != '' && !in_array($r['BLCR_BLC_R_ID'], $regions)) {
            $regions[] = $r['BLCR_BLC_R_ID'];
        }
        if ($r['BLC_M_C_ID'] != '' && !in_array($r['BLC_M_C_ID'], $category)) {
            $category[] = $r['BLC_M_C_ID'];
        }
        if ($r['BLC_C_ID'] != '' && !in_array($r['BLC_C_ID'], $subcategories)) {
            $subcategories[] = $r['BLC_C_ID'];
        }
    }
}
foreach ($regions as $key => $r) {
    if ($key == 0) {
        $default_region = $r;
    }
}

// See if a region is already selected
if (isset($_SESSION['ranking_region' . $BL_ID]) && $_SESSION['ranking_region' . $BL_ID] > 0) {
    $ranking_region = $_SESSION['ranking_region' . $BL_ID];
} else {

    $ranking_region = $default_region;
}
$sql = "SELECT R_Name FROM tbl_Region WHERE R_ID = '$ranking_region' LIMIT 1";
$result = mysql_query($sql, $db);
$region = mysql_fetch_assoc($result);
?>
<div class="content-right">
    <?php if (isset($_REQUEST['bl_id'])) { ?>
        <p class="title">Ranking Overview</p>
        <?php
        if ($rowListing['BL_Listing_Type'] == 1) {
            echo '<div class="rank">There is no Ranking, Points and Notifications for free listings. To get Rankings and Points please upgrade your listing type.</div>';
        } else {
            ?>
            <div class="rank">
                <div class="rank-container">
                    <span class="ranking-region"><?php echo $region['R_Name'] ?></span>
                    <div class="ranking-row paddingRanking bottomBorderNone">
                        <div class="ranking-title longtitle margin-none">My Page Points</div>
                        <div class="ranking-points"><?php echo calculate_listing_points($BL_ID, 'basic'); ?></div>
                    </div>
                    <div class="ranking-row paddingRanking bottomBorderNone">
                        <div class="ranking-title longtitle margin-none">Unused My Page Points</div>
                        <div class="ranking-points"><?php echo calculate_listing_points($BL_ID, 'available'); ?></div>
                    </div>
                    <div class="ranking-row paddingRanking bottomBorderNone">
                        <div class="ranking-title longtitle margin-none">My Add-Ons Points</div>
                        <div class="ranking-points"><?php echo calculate_listing_points($BL_ID, 'addon'); ?></div>
                    </div>
                    <div class="ranking-row paddingRanking bottomBorderNone">
                        <div class="ranking-title longtitle margin-none">Available Add-Ons Points</div>
                        <div class="ranking-points"><?php echo calculate_listing_points($BL_ID, 'available_addon'); ?></div>
                    </div>
                    <div class="ranking-row paddingRanking bottomBorderNone">
                        <div class="ranking-title longtitle margin-none">Total Points</div>
                        <div class="ranking-points"><?php echo calculate_listing_points($BL_ID, 'all'); ?></div>
                    </div>
                    <?php
                    $region_rank = calculate_ranking($BL_ID, 'region', $ranking_region, $ranking_region);
                    if (empty($category)) {
                        ?>
                        <label>To get rankings please add category and sub category to your listing.</label>
                        <?php
                    } else {
                        ?>
                        <div class="ranking-row paddingRanking bottomBorderNone">
                            <div class="ranking-title margin-none">Overall Ranking</div>
                            <div class="ranking-points"><?php echo $region_rank['rank'] . ' of ' . $region_rank['total'] ?></div>
                        </div>
                        <?php
                        foreach ($category as $cat) {
                            $sql_category = "SELECT * FROM tbl_Category WHERE C_ID = '$cat'";
                            $res_category = mysql_query($sql_category);
                            $c = mysql_fetch_assoc($res_category);
                            $category_rank = calculate_ranking($BL_ID, 'category', $cat, $ranking_region);
                            ?>
                            <span><?php echo $c['C_Name'] ?></span>
                            <div class="ranking-row paddingRanking bottomBorderNone">
                                <div class="ranking-title margin-none">Category Ranking</div>
                                <div class="ranking-points"><?php echo $category_rank['rank'] . ' of ' . $category_rank['total'] ?></div>
                            </div>
                            <?php
                            $i = 1;
                            foreach ($subcategories as $key => $subcat) {
                                $sql_subcat = "SELECT C_Name FROM tbl_Category WHERE C_ID = '$subcat' and C_Parent= '$cat'";
                                $res_subcat = mysql_query($sql_subcat);
                                $count_subcat = mysql_num_rows($res_subcat);
                                $s = mysql_fetch_assoc($res_subcat);
                                $subcat_rank = calculate_ranking($BL_ID, 'subcategory', $subcat, $ranking_region);
                                if ($count_subcat > 0) {
                                    ?>
                                    <span>Sub-Category <?php echo $i ?></span>
                                    <span class="margin-none ranking-sub-category-margin"><?php echo $s['C_Name'] ?></span>
                                    <div class="ranking-row paddingRanking bottomBorderNone">
                                        <div class="ranking-title margin-none">Category Ranking</div>
                                        <div class="ranking-points"><?php echo $subcat_rank['rank'] . ' of ' . $subcat_rank['total'] ?></div>
                                    </div>
                                    <div class="ranking-row paddingRanking bottomBorderNone">
                                        <div class="ranking-title margin-none">Page you appear</div>
                                        <?php if (isset($subcat_rank['rank'])) { ?>
                                            <div class="ranking-points"><?php echo ceil($subcat_rank['rank'] / 10); ?> of <?php echo ceil($subcat_rank['total'] / 10); ?>

                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php
                                    $i++;
                                }
                            }
                        }
                    }
                    ?>
                </div>
            </div>
            <?php
        }
    }
    ?>
</div>