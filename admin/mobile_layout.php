<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$sql = "SELECT * from tbl_Mobile_Layout LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$count = mysql_num_rows($result);
$Region_theme = mysql_fetch_assoc($result);

if (isset($_POST['save_color'])) {
    require_once '../include/picUpload.inc.php';
    $sql="tbl_Mobile_Layout set";
    $file= rand(1000,100000)."-".$_FILES["mobile_responsive_bg"]["name"];
    $temp_name=$_FILES["mobile_responsive_bg"]["tmp_name"];
    $filePath = IMG_LOC_ABS . $file;
    if ($_FILES['mobile_responsive_bg']['name'] != '') {
        move_uploaded_file($temp_name, $filePath);
        $sql .= " ML_Photo_Name = '" . encode_strings($file, $db) . "'";
         if ($Region_theme['ML_Photo_Name']) {
            Delete_Pic(IMG_LOC_ABS . $Region_theme['ML_Photo_Name']);
        }
    }
    if ($count > 0) {
        $sql = "UPDATE " . $sql;
         // TRACK DATA ENTRY
        Track_Data_Entry('Mobile', '', 'Mobile Background Image', '', 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
         // TRACK DATA ENTRY
        Track_Data_Entry('Mobile', '', 'Mobile Background Image', '', 'Add', 'super admin');
    }
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }

   header("Location: mobile_layout.php");
   exit();
}
if($_REQUEST['op'] == 'del')
{
    require '../include/picUpload.inc.php';
    $sql="Delete From tbl_Mobile_Layout where ML_ID=".$_REQUEST['id'];
    if ($Region_theme['ML_Photo_Name']) {
            Delete_Pic(IMG_LOC_ABS . $Region_theme['ML_Photo_Name']);
        }
    $result_del = mysql_query($sql, $db);
    if ($result_del) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['id'];
        Track_Data_Entry('Mobile', '', 'Mobile Background Image', $id, 'Delete', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: mobile_layout.php");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"> Mobile Background Image</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
    </div>
    <div class="right">
        <form action="" method="post" enctype="multipart/form-data" name="form1">
            <input type="hidden" name="op" value="save">
            <div class="content-header">Mobile Background Image</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Background Image</label>
                <div class="form-data form-data-logo margin-top-broswe-mbile-bg-image-page">
                    <span class="inputWrapper">&nbsp;&nbsp;Browse 
                        <input class="fileInput" type="file" name="mobile_responsive_bg" onchange="show_file(this)"/>
                    </span>
                    <?PHP
                    if ($Region_theme['ML_Photo_Name']) {
                    ?>
                        <a class="margin-left-resize" href="mobile_layout.php?op=del&id=<?php echo $Region_theme['ML_ID'] ?>">Delete</a>
                        <?php
                    } else {
                        echo "&nbsp;";
                    }
                    ?>

                </div>
                <div class="approved-photo-div" id="uploadFile1Main" <?php echo ($Region_theme['ML_Photo_Name'] == '') ? 'style="display:none;"' : ''; ?>>
                    <img id="uploadFile1" <?php echo ($Region_theme['ML_Photo_Name'] != '') ? 'src="http://' . DOMAIN . IMG_LOC_REL . $Region_theme['ML_Photo_Name'] . '"' : 'src=""'; ?>>          
                </div>
            </div>   
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="save_color" id="button22" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    function show_file(input) {
        if (input.files && input.files[0]) {
            $('#uploadFile1Main').show();
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#uploadFile1')
                        .attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<?PHP
require_once '../include/admin/footer.php';
?>
