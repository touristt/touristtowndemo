<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';
require '../include/PHPMailer/class.phpmailer.php';
require_once '../include/track-data-entry.php';

if (!in_array('customers', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
///Email Templete dynamic
$email = "SELECT * FROM tbl_Email_Templates WHERE ET_ID = 8";
$res = mysql_query($email);
$rowEmail = mysql_fetch_assoc($res); 
$email_subject = $rowEmail['ET_Subject'];
$email_heading = $rowEmail['ET_Name'];
$email_body = $rowEmail['ET_Template'];
$email_footer = $rowEmail['ET_Footer'];

if (isset($_GET['op']) && $_GET['op'] == 'approve') {
    $sql1 = "SELECT B_ID, BL_Listing_Title, B_Email, BL_Contact FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $Email = $rowListing['B_Email'];
    $Contact = $rowListing['BL_Contact'];
    $sql = "UPDATE tbl_Business_Feature_Coupon SET BFC_Status = 1 WHERE BFC_ID = '" . encode_strings($_REQUEST['bfc_id'], $db) . "'";
    $result = mysql_query($sql, $db);
    $message_body = str_replace("(Business Name here)", $rowListing['BL_Contact'], $rowEmail['ET_Template']);
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['bfc_id'];
        Track_Data_Entry('Coupons', '', 'Manage Coupon - Pending Coupons', $id, 'Approve', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    
    ob_start();
    include '../include/email-template/coupon-approval-email.php';
    $message = ob_get_contents();
    $mail = new PHPMailer();
    $mail->IsMail();
    $mail->From = MAIN_CONTACT_EMAIL;
    $mail->FromName = MAIN_CONTACT_NAME;
    $mail->IsHTML(true);
    $mail->AddAddress($Email);
    $mail->Subject = $email_subject;
    $mail->MsgHTML($message);
    $mail->Send();
    header("Location: listings-coupon.php");
    exit();
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $delCouponImg = "DELETE tbl_Business_Feature_Coupon,tbl_Business_Feature_Coupon_Category_Multiple FROM tbl_Business_Feature_Coupon LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID WHERE BFC_ID = '" . encode_strings($_REQUEST['bfc_id'], $db) . "'";
    $resDelCouponImg = mysql_query($delCouponImg, $db) or die("Invalid query: $delCouponImg -- " . mysql_error());
    if ($resDelCouponImg) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['bfc_id'];
        Track_Data_Entry('Coupons', '', 'Manage Coupon - Pending Coupons', $id, 'Delete', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location:listings-coupon.php");
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Coupon - Pending Coupons</div>
        <div class="link">           
        </div>
    </div>
    <div class="left">
        <?PHP
        require '../include/nav-manage-coupon.php';
        ?>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name-events">Name</div>
            <div class="busniss-name">Business Name</div>
            <div class="data-column padding-none spl-other active">Downloads</div>
            <div class="data-column spl-other padding-none active">Approve</div>
            <div class="data-column padding-none spl-other active">Delete</div>
        </div>
        <?PHP
        $sql = "SELECT BFC_Title, BL_ID, BFC_ID, BL_Listing_Title FROM tbl_Business_Feature_Coupon
                LEFT JOIN tbl_Business_Listing ON BFC_BL_ID = BL_ID
                WHERE BFC_Status = 0 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date =0000-00-00)";
        if (strlen($_REQUEST['strSearch']) > 3) {
            $sql .= " And BFC_Title LIKE '%" . encode_strings($_REQUEST['strSearch'], $db) . "%' ";
        }
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $pages = new Paginate(mysql_num_rows($result), 100);
        $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            ?>
            <div class="data-content">
                <div class="data-column spl-name-events"><a href="customer-feature-add-coupon.php?bl_id=<?php echo $row['BL_ID'] ?>&bfc_id=<?php echo $row['BFC_ID'] ?>&re_dir=1"><?php echo $row['BFC_Title'] ?></a></div>
                <div id="event-community" class="data-column spl-other-cc-list"><?php echo $row['BL_Listing_Title'] ?></div>
                <div class="data-column spl-other active"><a href="downloaded-coupons.php?bfc_id=<?php echo $row['BFC_ID'] ?>">Downloads</a></div>  
                <div class="data-column spl-other active"><a onclick="return confirm('Are you sure?')" href="listings-coupon.php?op=approve&bfc_id=<?php echo $row['BFC_ID'] ?>&bl_id=<?php echo $row['BL_ID'] ?>">Approve</a></div>           
                <div class="data-column spl-other active"><a onClick="return confirm('Are you sure?');" href="listings-coupon.php?bl_id=<?php echo $row['BL_ID'] ?>&bfc_id=<?php echo $row['BFC_ID'] ?>&op=del">Delete</a></div>
            </div>
            <?PHP
        }
        ?> 

        <?php
        if (isset($pages)) {
            echo $pages->paginate();
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>