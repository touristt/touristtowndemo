<?PHP

require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
$id = $_POST['bl_id'];
$bid = $_POST['bid'];
$total_amount = $_POST['total_amount'];
$accountManager = $_POST['accountManager'];
$paymentType = $_POST['paymentType'];
$renewaldate = $_POST['renewaldate'];
$expire_date = $_POST['expire_date'];
$date = date('Y-m-d');

if (!in_array('billing-history', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
if ($_POST['save'] == 'Pay') {
    $sql = "UPDATE tbl_Business_Listing SET
            BL_Payment_Type = '" . encode_strings($paymentType, $db) . "', 
            BL_Account_Manager = '" . encode_strings($accountManager, $db) . "',
            BL_Renewal_Date = '" . encode_strings($renewaldate, $db) . "', 
            BL_Renewal_Date_Last_Update = '" . encode_strings($_SESSION['USER_EM'] . '-' . $_SESSION['user_online'] . '-' . $_SESSION['USER_ID'], $db) . '-' . $date . "',
            BL_Total = '" . encode_strings($total_amount, $db) . "'
            WHERE BL_ID = '" . encode_strings($id, $db) . "'"; 
    mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    if (isset($_POST['billing_section']) && $_POST['billing_section'] == 1 && $paymentType == 4) {
        $select = "SELECT * FROM payment_profiles WHERE business_id = '" . encode_strings($bid, $db) . "'";
        $result = mysql_query($select, $db) or die("Invalid query: $select -- " . mysql_error());
        if (mysql_num_rows($result) > 0) {
            header('Location: payment/CreatePayment.php?bl_id=' . $id . '&bid=' . $bid . '&bil_sect=1');
            exit;
        } else {
            $_SESSION['WARNING_BILLING'] = 1;
        }
    }
    if ($paymentType == 2) {
        header('Location: payment/CreatePaymentUsingPayPal.php?bl_id=' . $id . '&bid=' . $bid);
    } else if ($paymentType == 4) {
        header('Location: payment/CreatePayment.php?bl_id=' . $id . '&bid=' . $bid);
    } else if ($paymentType == 1) {
        $payment_method = 'cash';
        header('Location: payment/CashPayment.php?bl_id=' . $id . '&bid=' . $bid . '&payment_method=' . $payment_method);
    } else if ($paymentType == 3 || $paymentType == 5) {
        if($paymentType == 3){
            $payment_method = 'cheque';
        }else{
            $payment_method = 'e-transfer';
        }
        header('Location: payment/ChequePayment.php?bl_id=' . $id . '&bid=' . $bid . '&payment_method=' . $payment_method);
    }
} else if ($_POST['save'] == 'Save') { 
    $sql = "UPDATE tbl_Business_Listing SET
            BL_Payment_Type = '" . encode_strings($paymentType, $db) . "', 
            BL_Account_Manager = '" . encode_strings($accountManager, $db) . "', 
            BL_Renewal_Date = '" . encode_strings($renewaldate, $db) . "', 
            BL_Trial_Expiry = '" . encode_strings($expire_date, $db) . "', 
            BL_Renewal_Date_Last_Update = '" . encode_strings($_SESSION['USER_EM'] . '-' . $_SESSION['user_online'] . '-' . $_SESSION['USER_ID'], $db) . '-' . $date . "',
            BL_Total = '" . encode_strings($total_amount, $db) . "'
            WHERE BL_ID = '" . encode_strings($id, $db) . "'"; //echo $sql; exit;
    mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $_SESSION['success'] = 1;
    header('Location: customer-listing-billing.php?bl_id=' . $id);
}
?>