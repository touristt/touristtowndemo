<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (isset($_REQUEST['id']) > 0) {
    $sql = "SELECT IBP_ID, IBP_Name, IBP_Description FROM tbl_Image_Bank_Photographer WHERE IBP_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
}
if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $sql = "tbl_Image_Bank_Photographer SET 
            IBP_Name = '" . encode_strings($_REQUEST['name'], $db) . "',
            IBP_Description = '" . encode_strings(trim(str_replace('<p>', '', str_replace('</p>', '', $_REQUEST['photographer_des']))), $db) . "'";
    if ($_REQUEST['photo_id'] == '') {
        $sql = "Insert " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Images', '', 'Manage Photographers', $id, 'Add', 'super admin');
    } else {
        $sql = "update " . $sql . " WHERE IBP_ID = '" . encode_strings($_REQUEST['photo_id'], $db) . "' ";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $_REQUEST['photo_id'];
        Track_Data_Entry('Images', '', 'Manage Photographers', $id, 'Update', 'super admin');
    }
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/photographer.php");
    exit();
}
if (isset($_GET['op']) && $_GET['op'] == 'del') {
    $sql = "DELETE FROM tbl_Image_Bank_Photographer  WHERE IBP_ID = '" . $row['IBP_ID'] . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $row['IBP_ID'] ;
        Track_Data_Entry('Images', '', 'Manage Photographers', $id, 'Delete', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: photographer.php");
    exit();
}

require_once '../include/admin/header.php';
?>

<div class="content-left full-width">

    <div class="title-link">
        <div class="title">Manage Photographers</div>
        <div class="link">
        </div>         
    </div>

    <div class="right full-width">
        <form name="form1" method="post" enctype="multipart/form-data" action="">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="photo_id"  value="<?php echo $row['IBP_ID'] ?>">
            <div class="content-header full-width"> 
                <div class="title">Add Photographer</div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Name</label>
                <div class="form-data">
                    <input name="name" type="text" value="<?php echo $row['IBP_Name'] ?>" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Description</label>
                <div class="form-data">
                    <textarea name="photographer_des" cols="85" rows="10" id="description-listing"><?php echo $row['IBP_Description'] ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width border-none">
                <div class="button">
                    <input type="submit" name="button" id="button" value="Save Photo" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>