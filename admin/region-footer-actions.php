<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $sql = "SELECT R_ID, R_Name, R_Type, FP_ID, FP_R_ID, FP_Photo, FP_Order, FP_Alt, FP_Link  FROM tbl_Footer_Photo
            LEFT JOIN tbl_Region ON FP_R_ID = R_ID 
            WHERE FP_ID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
} elseif ($_REQUEST['rid'] > 0) {
    $sql = "SELECT R_ID, R_Name, R_Type  FROM tbl_Region  
            WHERE R_ID = '" . encode_strings($_REQUEST['rid'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
}
$regionID = $activeRegion['R_ID'];

if (isset($_REQUEST['section'])) {
    $section = $_REQUEST['section'];
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    //$fp_link = preg_replace('/^www\./', '', $_REQUEST['link']);
    $sql = "tbl_Footer_Photo SET 
            FP_Alt = '" . encode_strings($_REQUEST['alt'], $db) . "',  
            FP_Link = '" . encode_strings($_REQUEST['link'], $db) . "',  
            FP_Section = '" . encode_strings($_REQUEST['section'], $db) . "'";

    require '../include/picUpload.inc.php';
    if ($_FILES['pic']['name'] != '') {
        $pic = Upload_Pic_Normal('0', 'pic', 0, 0, true, IMG_LOC_ABS, 0, true);
        if ($pic) {
            $sql .= ", FP_Photo = '" . encode_strings($pic, $db) . "'";
            if ($activeRegion['FP_Photo']) {
                Delete_Pic(IMG_LOC_ABS . $activeRegion['FP_Photo']);
            }
        }
    }
    if ($_REQUEST['id'] > 0) {
        $sql = "UPDATE " . $sql . " WHERE FP_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $id = $_REQUEST['id'];
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $regionID, 'Manage Footer', $section, 'Update Region Footer Details', 'super admin');
    } else {
        $sql = "INSERT " . $sql . ", FP_R_ID = '" . encode_strings($regionID, $db) . "'";

        $sqlMax = "SELECT MAX(FP_Order) FROM tbl_Footer_Photo WHERE FP_R_ID = '" . encode_strings($regionID, $db) . "'";
        $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
        $rowMax = mysql_fetch_row($resultMax);

        $sql .= ", FP_Order = '" . ($rowMax[0] + 1) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $regionID, 'Manage Footer', $section, 'Add Region Footer Details', 'super admin');
    }

    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: region-footer.php?rid=" . $regionID);
    exit();
} else if (isset($_GET['op']) && $_GET['op'] == 'del') {
    require_once '../include/picUpload.inc.php';

    if ($activeRegion['FP_Photo']) {
        Delete_Pic(IMG_LOC_ABS . $activeRegion['FP_Photo']);
    }

    $sql = "UPDATE tbl_Footer_Photo SET FP_Order = FP_Order - 1  
            WHERE FP_R_ID = '" . $activeRegion['FP_R_ID'] . "' && FP_Order > " . $activeRegion['FP_Order'];
    mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());

    $sql = "DELETE tbl_Footer_Photo
            FROM tbl_Footer_Photo  
            WHERE FP_ID = '" . $activeRegion['FP_ID'] . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $regionID, 'Manage Footer', $section, 'Delete', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: region-footer.php?rid=" . $regionID);
    exit();
}
if (isset($_GET['op']) && $_GET['op'] == 'del_photo') {
    require_once '../include/picUpload.inc.php';
    if ($activeRegion['FP_Photo']) {
        Delete_Pic(IMG_LOC_ABS . $activeRegion['FP_Photo']);
    }
    $sql = "UPDATE tbl_Footer_Photo SET FP_Photo = ''  
            WHERE FP_ID = '" . $activeRegion['FP_ID'] . "'";
    $result_del = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    if ($result_del) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $regionID, 'Manage Footer', $section, 'Delete Photo', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: region-footer-actions.php?id=" . $activeRegion['FP_ID'] . "&section=" . $section);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Manage Footer</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <form id="regionForm" action="/admin/region-footer-actions.php" method="post" enctype="multipart/form-data" name="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="section" value="<?php echo $section ?>">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <input type="hidden" name="id" value="<?php echo(isset($activeRegion['FP_ID']) && $activeRegion['FP_ID'] > 0) ? $activeRegion['FP_ID'] : '0'; ?>">
            <div class="content-header">Region Footer Details</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label><?php echo ($_REQUEST['section'] == 3) ? "Link Text" : "Alt" ?></label>
                <div class="form-data">
                    <input name="alt" type="text" id="textfield2" value="<?php echo (isset($activeRegion['FP_Alt'])) ? $activeRegion['FP_Alt'] : '' ?>" <?php echo ($_REQUEST['section'] == 3) ? 'required' : '' ?>/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Link</label>
                <div class="form-data">
                    <input name="link" type="text" id="textfield2" value="<?php echo (isset($activeRegion['FP_Link'])) ? $activeRegion['FP_Link'] : '' ?>" size="50" /> 
                </div>
            </div>
            <?php if ($_REQUEST['section'] != 3) { ?>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Photo</label>
                    <div class="form-data from-inside-div-text">
                        <div class="approved-photo-div">
                            <div class="inputWrapper adv-photo float-left">Browse
                                <input class="fileInput" type="file" name="pic[]" onchange="show_file(this)">
                            </div>
                        </div>
                        <div class="approved-photo-div">
                            <img id="uploadFile" style="<?php echo ($activeRegion['FP_Photo'] == '') ? 'display: none' : '' ?> " src="<?php echo (isset($activeRegion['FP_Photo']) != '') ? 'http://' . DOMAIN . IMG_LOC_REL . $activeRegion['FP_Photo'] : '' ?>">          
                        </div>
                        <?php if (isset($activeRegion['FP_Photo']) && $activeRegion['FP_Photo'] != '') {
                            ?>
                            <a class="deletePhoto delete-region-cat-photo" style="margin-top: 10px;width: 127px;" onclick="return confirm('Are you sure you want to delete photo?');" href="region-footer-actions.php?id=<?php echo $activeRegion['FP_ID'] ?>&op=del_photo&section=<?php echo $section ?>">Delete Photo</a>
                            <?php
                        }
                        ?>
                    </div>
                </div>  
            <?php } ?>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="button2" id="button22" value="Submit" />
                </div>
            </div>
        </form>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>
<script>
    function show_file(input) {
        if (input.files && input.files[0]) {
            $('#uploadFile').show();
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#uploadFile')
                        .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<?PHP
require_once '../include/admin/footer.php';
?>