<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';
require_once '../include/track-data-entry.php';

if (!in_array('superadmin', $_SESSION['USER_ROLES'])) {
    header("Location: /admin/");
    exit();
}

if (isset($_GET['op']) && $_GET['op'] == 'confirm') {
    $sql = "UPDATE tbl_Business_Listing SET BL_Free_Listing_status = 0, BL_Free_Listing_Status_Hash = 0, BL_Free_Listing_Status_Date = '0000-00-00'
            WHERE BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "'";
    $result = mysql_query($sql);
    header("Location:" . $_SERVER['HTTP_REFERER']);
}

$temp = false;
$regionJoin = '';
$regionWhere = '';
$regionLimit = array();
if ((isset($_REQUEST['sortregion']) && $_REQUEST['sortregion'] != '' && $_REQUEST['sortregion'] != '-1') || $_SESSION['USER_LIMIT'] > 0) {
    if (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
        $limits = explode(',', $_SESSION['USER_LIMIT']);
    } else {
        $limits = explode(',', $_REQUEST['sortregion']);
    }
    foreach ($limits as $limit) {
        $sql = "SELECT R_ID, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $region = mysql_fetch_assoc($result);
        if (isset($region['R_Type']) && $region['R_Type'] == 1) {
            $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($region['R_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                if ($row['R_ID'] != '' && !in_array($row['R_ID'], $regionLimit)) {
                    $regionLimit[] = $row['R_ID'];
                }
            }
        } else {
            if ($region['R_ID'] != '' && !in_array($region['R_ID'], $regionLimit)) {
                $regionLimit[] = $region['R_ID'];
            }
        }
    }
    $regionLimitCommaSeparated = rtrim(implode(',', $regionLimit), ',');
    $regionJoin = " LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                    INNER JOIN tbl_Business_Listing_Category_Region ON BL_ID = BLCR_BL_ID";
    $regionWhere .= " AND BLCR_BLC_R_ID IN (" . encode_strings($regionLimitCommaSeparated, $db) . ")";
        }
if (isset($_GET['op']) && $_GET['op'] == 'del') {
    $sql = "DELETE tbl_Business_Listing, tbl_Business_Listing_Ammenity, 
            tbl_Business_Listing_Category, tbl_Business_Listing_Category_Region,
            tbl_Business_Social, tbl_BL_Feature, payment_profiles,
            tbl_Business_Feature_About, tbl_Business_Feature_About_Photo,
            tbl_Business_Feature_About_Main_Photo, tbl_Business_Feature_Daily_Specials,
            tbl_Business_Feature_Daily_Specials_Description, tbl_Business_Feature_Entertainment_Acts,
            tbl_Business_Feature_Entertainment_Description, tbl_Business_Feature_Guest_Book, tbl_Business_Feature_Guest_Book_Description,
            tbl_Business_Feature_Menu, tbl_Business_Feature_Photo, tbl_Business_Feature_Product, tbl_Business_Feature_Product_Photo,
            tbl_Business_Feature_Coupon, tbl_Business_Feature_Coupon_Category_Multiple, tbl_Image_Bank_Usage
            FROM tbl_Business_Listing 
            LEFT JOIN tbl_Business_Listing_Ammenity ON BLA_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Listing_Category_Region ON BLCR_BLC_ID = BLC_ID 
            LEFT JOIN tbl_Business_Social ON BS_BL_ID = BL_ID
            LEFT JOIN tbl_BL_Feature ON BLF_BL_ID = BL_ID
            LEFT JOIN payment_profiles ON listing_id = BL_ID
            LEFT JOIN tbl_Business_Feature_About ON BFA_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_About_Photo ON BFAP_BFA_ID = BFA_ID
            LEFT JOIN tbl_Business_Feature_About_Main_Photo ON BFAMP_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Daily_Specials ON BFDS_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Daily_Specials_Description ON BFDSD_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Entertainment_Acts ON BFEA_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Entertainment_Description ON BFED_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Guest_Book ON BFGB_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Guest_Book_Description ON tbl_Business_Feature_Guest_Book_Description.BFGB_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Menu ON BFM_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Photo ON BFP_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Product ON tbl_Business_Feature_Product.BFP_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Product_Photo ON BFPP_BFP_ID = tbl_Business_Feature_Product.BFP_ID
            LEFT JOIN tbl_Business_Feature_Coupon ON tbl_Business_Feature_Coupon.BFC_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
            LEFT JOIN tbl_Image_Bank_Usage ON IBU_BL_ID = BL_ID
            WHERE BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());

    if ($result) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location:" . $_SERVER['HTTP_REFERER']);
    exit();
}
if (isset($_POST['saveVal']) && $_POST['saveVal'] !== '') {
    $check_val = $_POST['checkbox'];
    $listings = implode(",", $check_val);
    foreach ($check_val as $key1 => $value1) {
        $sql = "UPDATE tbl_Business_Listing SET BL_Free_Listing_status = 0, BL_Free_Listing_Status_Hash = 0, BL_Free_Listing_Status_Date = '0000-00-00'
            WHERE BL_ID = '" . encode_strings($value1, $db) . "'";
        $res1 = mysql_query($sql, $db);
        if ($res1) {
            $_SESSION['success'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
    }
    // TRACK DATA ENTRY
    Track_Data_Entry('Listings', $listings, 'Unverified Listings', '', 'Confirm All', 'super admin');
    header("Location:" . $_SERVER['HTTP_REFERER']);
    exit();
}
if (isset($_POST['deleteVal']) && $_POST['deleteVal'] !== '') {
    $check_val = $_POST['checkbox'];
    $listings = implode(",", $check_val);
    foreach ($check_val as $key1 => $value1) {
        $sql = "DELETE tbl_Business_Listing, tbl_Business_Listing_Ammenity, 
            tbl_Business_Listing_Category, tbl_Business_Listing_Category_Region,
            tbl_Business_Social, tbl_BL_Feature, payment_profiles,
            tbl_Business_Feature_About, tbl_Business_Feature_About_Photo,
            tbl_Business_Feature_About_Main_Photo, tbl_Business_Feature_Daily_Specials,
            tbl_Business_Feature_Daily_Specials_Description, tbl_Business_Feature_Entertainment_Acts,
            tbl_Business_Feature_Entertainment_Description, tbl_Business_Feature_Guest_Book, tbl_Business_Feature_Guest_Book_Description,
            tbl_Business_Feature_Menu, tbl_Business_Feature_Photo, tbl_Business_Feature_Product, tbl_Business_Feature_Product_Photo,
            tbl_Business_Feature_Coupon, tbl_Business_Feature_Coupon_Category_Multiple, tbl_Image_Bank_Usage
            FROM tbl_Business_Listing 
            LEFT JOIN tbl_Business_Listing_Ammenity ON BLA_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Listing_Category_Region ON BLCR_BLC_ID = BLC_ID 
            LEFT JOIN tbl_Business_Social ON BS_BL_ID = BL_ID
            LEFT JOIN tbl_BL_Feature ON BLF_BL_ID = BL_ID
            LEFT JOIN payment_profiles ON listing_id = BL_ID
            LEFT JOIN tbl_Business_Feature_About ON BFA_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_About_Photo ON BFAP_BFA_ID = BFA_ID
            LEFT JOIN tbl_Business_Feature_About_Main_Photo ON BFAMP_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Daily_Specials ON BFDS_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Daily_Specials_Description ON BFDSD_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Entertainment_Acts ON BFEA_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Entertainment_Description ON BFED_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Guest_Book ON BFGB_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Guest_Book_Description ON tbl_Business_Feature_Guest_Book_Description.BFGB_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Menu ON BFM_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Photo ON BFP_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Product ON tbl_Business_Feature_Product.BFP_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Product_Photo ON BFPP_BFP_ID = tbl_Business_Feature_Product.BFP_ID
            LEFT JOIN tbl_Business_Feature_Coupon ON tbl_Business_Feature_Coupon.BFC_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
            LEFT JOIN tbl_Image_Bank_Usage ON IBU_BL_ID = BL_ID
            WHERE BL_ID = '" . encode_strings($value1, $db) . "'";
        $res1 = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        if ($res1) {
            $_SESSION['delete'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
    }
    // TRACK DATA ENTRY
    Track_Data_Entry('Listings', $listings, 'Unverified Listings', '', 'Delete All', 'super admin');
    header("Location:" . $_SERVER['HTTP_REFERER']);
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Unverified Listings</div> 
        <div class="link">
            <form id="frmSort" name="form1" method="GET" action="" style="float:left;margin-top: 10px;">
                <?php
                if (!isset($_SESSION['USER_LIMIT']) || $_SESSION['USER_LIMIT'] == 0) {
                    $sortregion = (isset($_REQUEST['sortregion'])) ? $_REQUEST['sortregion'] : '';
                    ?>
                    <label>
                        <span class="addlisting">Region:</span>
                        <select name="sortregion" id="sortregion" onChange="getSubOptions(this.value)">
                            <option value="">Select Region</option>
                            <?php
                            $getRegions = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Domain != ''";
                            $resRegion = mysql_query($getRegions, $db) or die("Invalid query: $getRegions -- " . mysql_error());
                            while ($rowRegion = mysql_fetch_array($resRegion)) {
                                ?>
                                <option value="<?php echo $rowRegion['R_ID'] ?>" <?php echo ($sortregion == $rowRegion['R_ID']) ? 'selected="selected"' : ''; ?>><?php echo $rowRegion['R_Name'] ?></option>
                            <?php } ?>
                            <option value="-1">All</option>
                        </select>
                    </label>
                <?php } ?>
                <script type="text/javascript">
                    function getSubOptions(myVal) {
                        $('#frmSort').submit();
                    }
                </script>
            </form>
             <form class="export_form_width unverified" method="post" name="export_form" action="export-unverified-listings-data.php">
                <input type="hidden" name="sortregion" value="<?php echo $_REQUEST['sortregion'] ?>">
                <input type="hidden" name="strSearch" value="<?php echo $_REQUEST['strSearch'] ?>">
                <input type="submit" name="export_stats" value="Export"/>
            </form>
        </div>
    </div>
    <div class="left">
        <?PHP
        require '../include/nav-unverified-listings.php';
        ?>
    </div>
    <div class="right">
        <form name="form1" method="POST" action="">
            <div class="content-sub-header">
                <div class="data-column spl-name-events padding-none">Name</div>
                <div class="data-column spl-other-cc-list padding-none"></div>
                <div class="data-column spl-other-cc-list padding-none"></div>
                <div class="data-column spl-other-cc-list padding-none">Confirm</div>
                <div class="data-column spl-other-cc-list padding-none">Delete</div>
                <div class="data-column padding-none spl-other-cc-list padding-none"><input type="checkbox" id="checkall" />Select All</div>
            </div>
            <?PHP
            $where = '';
            if (strlen($_REQUEST['strSearch']) > 3) {
                $where = " AND BL_Listing_Title LIKE '%" . encode_strings($_REQUEST['strSearch'], $db) . "%' ";
            }
            $sql = "SELECT DISTINCT BL_ID, BL_Listing_Title FROM tbl_Business_Listing $regionJoin WHERE BL_Free_Listing_status = 1 AND BL_Listing_Type = 1 $where $regionWhere
                ORDER BY TRIM(BL_Listing_Title)";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $pages = new Paginate(mysql_num_rows($result), 100);
            $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                ?>
                <div class="data-content">
                    <div class="data-column spl-name-events"><a href="customer-listing-overview.php?bl_id=<?php echo $row['BL_ID'] ?>"><?php echo $row['BL_Listing_Title'] ?></a></div>
                    <div class="data-column spl-other-cc-list"></div>
                    <div class="data-column spl-other-cc-list"></div>
                    <div class="data-column spl-other-cc-list">
                        <a onClick="return confirm('Ar you sure this action can not be undone!');" href="unverified-listings.php?op=confirm&bl_id=<?php echo $row['BL_ID'] ?>">Confirm</a>
                    </div>
                    <div class="data-column spl-other-cc-list">
                        <a onClick="return confirm('Ar you sure this action can not be undone!');" href="unverified-listings.php?op=del&bl_id=<?php echo $row['BL_ID'] ?>">Delete</a>
                    </div>
                    <div class="data-column spl-other-cc-list">
                        <div> <input type="checkbox" name="checkbox[]" value="<?php echo $row['BL_ID'] ?>" /></div>
                    </div>
                </div>
                <?PHP
            }
            if (isset($pages)) {
                echo $pages->paginate();
            }
            ?>

            <div class="form-inside-div border-none" id="btn">
                <div class="button">
                    <input value="Confirm" type="submit" name="saveVal" class="btn-Approve">
                    <input value="Delete" type="submit" name="deleteVal" class="btn-Delete" onClick="return confirm('Are you sure?\nThis action CANNOT be undone!');">
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#checkall').click(function () {
            var checked = $(this).prop('checked');
            $('.spl-other-cc-list').find('input:checkbox').prop('checked', checked);
        });
    });
</script>
<?PHP
require_once '../include/admin/footer.php';
?>