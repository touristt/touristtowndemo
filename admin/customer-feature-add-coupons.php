<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/adminFunctions.inc.php';
require '../include/PHPMailer/class.phpmailer.php';
require_once '../include/track-data-entry.php';
$preview_page = 1;
$coupon_preview =1;

if (!in_array('listing-tools', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
} elseif (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
    $limits = explode(',', $_SESSION['USER_LIMIT']);
    $regionLimit = array();
    foreach ($limits as $limit) {
        $sql = "SELECT R_ID, R_Parent FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $region = mysql_fetch_assoc($result);
        if (isset($region['R_Type']) && $region['R_Type'] == 1) {
            $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($region['R_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                if ($row['R_ID'] != '' && !in_array($row['R_ID'], $regionLimit)) {
                    $regionLimit[] = $row['R_ID'];
                }
            }
        } else {
            if ($region['R_ID'] != '' && !in_array($region['R_ID'], $regionLimit)) {
                $regionLimit[] = $region['R_ID'];
            }
        }
    }
}
$regionLimitCommaSeparated = rtrim(implode(',', $regionLimit), ',');
$BL_ID = $_REQUEST['bl_id'];
$BFC_ID = $_REQUEST['bfc_id'];
if (isset($BL_ID) && $BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title, B_Email, BL_Contact FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
    $Email = $rowListing['B_Email'];
    $Contact = $rowListing['BL_Contact'];
} else {
    header("Location:customers.php");
    exit();
}
if (isset($BFC_ID) && $BFC_ID > 0) {
    $getCoupon = "SELECT BFC_ID, BFC_Title, BFC_Description, BFC_Terms_Conditions, BFC_Expiry_Date, BFC_Thumbnail, BFC_Main_Image, BFC_Status 
                  FROM tbl_Business_Feature_Coupon WHERE BFC_ID = '" . encode_strings($BFC_ID, $db) . "'";
    $resCoupon = mysql_query($getCoupon, $db) or die("Invalid query: $getCoupon -- " . mysql_error());
    $actCoupon = mysql_fetch_assoc($resCoupon);
    
}
if ($BFC_ID) {
    $sql_category = "SELECT * FROM tbl_Business_Feature_Coupon_Category_Multiple WHERE BFCCM_BFC_ID = $BFC_ID ORDER BY BFCCM_ID ASC  ";
    $result_category = mysql_query($sql_category);
    while ($row1 = mysql_fetch_array($result_category)) {
        $totVal[] = $row1['BFCCM_C_ID'];
        $outputlastVal = implode(", ", $totVal);
    }
    $cat_coupon = array();
    $sql_cat = "SELECT C_ID, C_Name_SEO, C_Name FROM tbl_Category 
            WHERE C_Parent = (SELECT C_ID FROM tbl_Category WHERE C_Is_Product_Web = 1) AND C_ID NOT IN($outputlastVal)
            ORDER BY C_Name";
    $result_cat = mysql_query($sql_cat);
    while ($row = mysql_fetch_array($result_cat)) {
        $cat_coupon[] = $row;
    }
} else {
    $sql_category = "SELECT * FROM tbl_Business_Feature_Coupon_Category_Multiple ORDER BY BFCCM_ID ASC  ";
    $result_category = mysql_query($sql_category);
    while ($row1 = mysql_fetch_array($result_category)) {
        $totVal[] = $row1['BFCCM_C_ID'];
        $outputlastVal = implode(", ", $totVal);
    }
    $cat_coupon = array();
    $sql_cat = "SELECT C_ID, C_Name_SEO, C_Name FROM tbl_Category 
                WHERE C_Parent = (SELECT C_ID FROM tbl_Category WHERE C_Is_Product_Web = 1) ORDER BY C_Name";
    $result_cat = mysql_query($sql_cat);
    while ($row = mysql_fetch_array($result_cat)) {
        $cat_coupon[] = $row;
    }
}
$cat_coupon = json_encode($cat_coupon);
///// Social Media
if (isset($_POST['op']) && $_POST['op'] == 'save_social_media') { 
    $bfc_id = $_REQUEST['bfc_id'];
    if ($_POST['counter'] > 0) {
        $sql = "UPDATE tbl_Business_Social SET 
                BS_FB_Link = '" . encode_strings($_POST['fb_link'], $db) . "',
                BS_T_Link = '" . encode_strings($_POST['t_link'], $db) . "',
                BS_I_Link = '" . encode_strings($_POST['i_link'], $db) . "'
                WHERE BS_BL_ID = '$BL_ID'";
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Social Media','','Update','super admin');
    } else {
        $sql = "INSERT INTO tbl_Business_Social(BS_BL_ID, BS_FB_Link, BS_T_Link, BS_I_Link) VALUES('" . encode_strings($BL_ID, $db) . "', '" . encode_strings($_POST['fb_link'], $db) . "', '" . encode_strings($_POST['t_link'], $db) . "', '" . encode_strings($_POST['i_link'], $db) . "')";
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Social Media','','Add','super admin');
    }
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
    
   header("Location:customer-feature-add-coupons.php?bl_id=" . $BL_ID . "&bfc_id=" . $bfc_id . "&re_dir=1");
   $_SESSION['success'] = 1;
   exit;
}
// UPDATE CONTACT DETAILS
if (isset($_POST['op']) && $_POST['op'] == 'save_contact_details') {
    $BL_ID = $_REQUEST['bl_id'];
    $bfc_id = $_REQUEST['bfc_id'];
    $sql = "tbl_Business_Listing SET 
            BL_Listing_Title = '" . encode_strings($_REQUEST['name'], $db) . "', 
            BL_Name_SEO = '" . encode_strings($_REQUEST['seoName'], $db) . "', 
            BL_Contact = '" . encode_strings($_REQUEST['contact'], $db) . "', 
            BL_Phone = '" . encode_strings($_REQUEST['phone'], $db) . "', 
            BL_Toll_Free = '" . encode_strings($_REQUEST['tollFree'], $db) . "', 
            BL_Cell = '" . encode_strings($_REQUEST['cell'], $db) . "', 
            BL_Fax = '" . encode_strings($_REQUEST['fax'], $db) . "', 
            BL_Email = '" . encode_strings($_REQUEST['email'], $db) . "', 
            BL_Website = '" . encode_strings($_REQUEST['website'], $db) . "'";

    if (isset($BL_ID) && $BL_ID > 0) {
        $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Contact Details', '', 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $id, 'Contact Details', '', 'Add', 'super admin');
    }

    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location:customer-feature-add-coupons.php?bl_id=" . $BL_ID . "&bfc_id=" . $bfc_id . "&re_dir=1");
   $_SESSION['success'] = 1;
   exit;
}
///// mapping
if (isset($_POST['op']) && $_POST['op'] == 'save_cutomer_listing_mapping') {
    $BL_ID = $_REQUEST['bl_id'];
    $bfc_id = $_REQUEST['bfc_id'];
    $sql_map = "tbl_Business_Listing SET
            BL_Country = '" . encode_strings($_REQUEST['country'], $db) . "',  
            BL_Street = '" . encode_strings($_REQUEST['address'], $db) . "', 
            BL_Town = '" . encode_strings($_REQUEST['town'], $db) . "', 
            BL_Province = '" . encode_strings($_REQUEST['province'], $db) . "', 
            BL_PostalCode = '" . encode_strings($_REQUEST['postalcode'], $db) . "',
            BL_Location_Description = '" . encode_strings($_REQUEST['locationdescription'], $db) . "',
            BL_Lat = '" . encode_strings($_REQUEST['latitude'], $db) . "', 
            BL_Long = '" . encode_strings($_REQUEST['longitude'], $db) . "'";
    if ($_REQUEST['latitude'] != '' && $_REQUEST['longitude']) {
        $sql_map .= ", BL_Location = POINT(" . $_REQUEST['latitude'] . ", " . $_REQUEST['longitude'] . ")";
    }
    if (isset($BL_ID) && $BL_ID > 0) { 
        $sql_map = "UPDATE " . $sql_map . " WHERE BL_ID = " . encode_strings($BL_ID, $db) ; //echo $sql_map; exit;
        $result = mysql_query($sql_map, $db);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Location & Mapping','','Update','super admin');
    } else {
        $sql_map = "INSERT " . $sql_map;
        $result = mysql_query($sql_map, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing',$id,'Location & Mapping','','Add','super admin');
    }
    
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
     header("Location:customer-feature-add-coupons.php?bl_id=" . $BL_ID . "&bfc_id=" . $bfc_id . "&re_dir=1");
   $_SESSION['success'] = 1;
   exit;
}
/// Delete Category
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del_cat') {
    $bfccm_id = $_REQUEST['bfccm_id']; 
    $bfc_id = $_REQUEST['bfc_id']; 
    $qry = "Delete from tbl_Business_Feature_Coupon_Category_Multiple where BFCCM_ID = '" . encode_strings($bfccm_id, $db) . "' ";
   //exit();
    $resCoupon_delete = mysql_query($qry, $db) or die("Invalid query: $qry -- " . mysql_error());
    if ($resCoupon_delete) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Add Coupon', $bfc_id, 'Delete Category', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location:customer-feature-add-coupons.php?bl_id=" . $BL_ID . "&bfc_id=" . $bfc_id . "&re_dir=1");
   $_SESSION['success'] = 1;
   exit;
}
////// Hours
if (isset($_POST['op']) && $_POST['op'] == 'save_hours') {
    $bfc_id = $_REQUEST['bfc_id'];
    $hDisabled = isset($_REQUEST['hDisabled']) != '' ? 1 : 0;
    $happointment = isset($_REQUEST['happointment']) != '' ? 1 : 0;
    $sql = "tbl_Business_Listing SET  
            BL_Hours_Disabled = '" . encode_strings($hDisabled, $db) . "', 
            BL_Hours_Appointment = '" . encode_strings($happointment, $db) . "', 
            BL_Hour_Mon_From = '" . encode_strings($_REQUEST['monFrom'], $db) . "', 
            BL_Hour_Mon_To = '" . encode_strings($_REQUEST['monTo'], $db) . "', 
            BL_Hour_Tue_From = '" . encode_strings($_REQUEST['tueFrom'], $db) . "', 
            BL_Hour_Tue_To = '" . encode_strings($_REQUEST['tueTo'], $db) . "', 
            BL_Hour_Wed_From = '" . encode_strings($_REQUEST['wedFrom'], $db) . "', 
            BL_Hour_Wed_To = '" . encode_strings($_REQUEST['wedTo'], $db) . "', 
            BL_Hour_Thu_From = '" . encode_strings($_REQUEST['thuFrom'], $db) . "', 
            BL_Hour_Thu_To = '" . encode_strings($_REQUEST['thuTo'], $db) . "', 
            BL_Hour_Fri_From = '" . encode_strings($_REQUEST['friFrom'], $db) . "', 
            BL_Hour_Fri_To = '" . encode_strings($_REQUEST['friTo'], $db) . "', 
            BL_Hour_Sat_From = '" . encode_strings($_REQUEST['satFrom'], $db) . "', 
            BL_Hour_Sat_To = '" . encode_strings($_REQUEST['satTo'], $db) . "', 
            BL_Hour_Sun_From = '" . encode_strings($_REQUEST['sunFrom'], $db) . "', 
            BL_Hour_Sun_To = '" . encode_strings($_REQUEST['sunTo'], $db) . "'";
    $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . $BL_ID . "'";
    $result = mysql_query($sql, $db);  
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Hours', '', 'Update', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location:customer-feature-add-coupons.php?bl_id=" . $BL_ID . "&bfc_id=" . $bfc_id . "&re_dir=1");
        exit();
}

//// Coupon save 
if (isset($_POST['op']) && $_POST['op'] = 'save_coupon') { 
    $bfc_id = $_REQUEST['bfc_id'];
    $expire_date = ($_POST['expire_date'] != '') ? $_POST['expire_date'] : '0000-00-00';
    $coupons = "tbl_Business_Feature_Coupon SET 
                BFC_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                BFC_Title = '" . encode_strings($_POST['name'], $db) . "',
                BFC_Description = '" . encode_strings($_POST['deal_description'], $db) . "',
                BFC_Terms_Conditions = '" . encode_strings($_POST['deal_terms'], $db) . "',
                BFC_Status = '" . encode_strings($_POST['status'], $db) . "',
                BFC_Expiry_Date = '" . $expire_date . "'";
    require_once '../include/picUpload.inc.php';
    if ($_POST['image_bank'] == "") {
        // last @param 29 = Coupon image
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 29, 'Listing', $BL_ID);
        if (is_array($pic)) {
            $coupons .= ", BFC_Thumbnail = '" . encode_strings($pic['0']['1'], $db) . "',
                        BFC_Main_Image = '" . encode_strings($pic['0']['0'], $db) . "'";
            $pic_id = $pic['1'];
        }
    } else {
        $pic_id = $_POST['image_bank'];
        // last @param 29 = Coupon image
        $pic = Upload_Pic_Library($pic_id, 29);
        if (is_array($pic)) {
            Update_Image_Bank_Listings($pic_id, $BL_ID);
            $coupons .= ", BFC_Thumbnail = '" . encode_strings($pic['1'], $db) . "',
                            BFC_Main_Image = '" . encode_strings($pic['0'], $db) . "'";
        }
    }
    if ($BFC_ID > 0) {
        $couponQuery = "UPDATE $coupons WHERE BFC_ID = '" . encode_strings($BFC_ID, $db) . "'";
        $resCoupon = mysql_query($couponQuery, $db) or die("Invalid query: $couponQuery -- " . mysql_error());
        $coupon_id = $BFC_ID;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Add Coupon', $BFC_ID, 'Update', 'super admin');
    } else {
        $couponQuery = "INSERT $coupons";
        $resCoupon = mysql_query($couponQuery, $db) or die("Invalid query: $couponQuery -- " . mysql_error());
        $coupon_id = mysql_insert_id();
        // TRACK DATA ENTRY
        $id = $BL_ID;
       Track_Data_Entry('Listing', $id, 'Add Coupon', $coupon_id, 'Add', 'super admin');
    }
    if ($resCoupon) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        if ($pic_id > 0) {
            //Updating Image Bank table because we cannot insert coupon id through picUpload function.
            //If we create new coupon then we do not have the BFC_ID to insert in image bank table
            $sql_image_bank = "UPDATE tbl_Image_Bank SET IB_Coupons = '" . encode_strings($coupon_id, $db) . "' WHERE IB_ID = '" . encode_strings($pic_id, $db) . "'";
            mysql_query($sql_image_bank) or die("Invalid query: $sql_image_bank -- " . mysql_error());
            //Image usage from image bank//
            imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Coupon_Photo', $coupon_id);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    $sql_delete = "DELETE FROM tbl_Business_Feature_Coupon_Category_Multiple where BFCCM_BFC_ID = '" . encode_strings($coupon_id, $db) . "'";
    mysql_query($sql_delete);
    for ($i = 1; $i < $_POST['total_cat'] + 1; $i++) {
        $C_cat_id = $_POST['c_coupon_' . $i];
        if ($C_cat_id > 0) {
            $query_insert = "INSERT tbl_Business_Feature_Coupon_Category_Multiple SET 
                        BFCCM_BFC_ID = '" . encode_strings($coupon_id, $db) . "',
                        BFCCM_C_ID ='" . encode_strings($C_cat_id, $db) . "'";
            $resCoupon_insert = mysql_query($query_insert, $db) or die("Invalid query: $query_insert -- " . mysql_error());
        }
    }
    if (isset($_POST['status']) && $_POST['status'] == 1) {
        if (is_array($_POST['region'])) {
            foreach ($_POST['region'] as $key => $val) {
                $sql = "INSERT INTO tbl_Business_Listing_Category_Region (BLCR_BL_ID, BLCR_BLC_R_ID)
                      VALUES('" . encode_strings($BL_ID, $db) . "', '" . encode_strings($val, $db) . "')";
                $result = mysql_query($sql, $db);
            }
        }
        foreach ($_POST['subcat'] as $val) {
            $sql = "SELECT COUNT(*) FROM tbl_Business_Listing_Category WHERE BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $row = mysql_fetch_row($result);
            if ($row[0] == 0) {
                $sql = "INSERT tbl_Business_Listing_Category SET BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                    BLC_M_C_ID = '" . encode_strings($_POST['category'], $db) . "', BLC_C_ID = '" . encode_strings($val, $db) . "'";
            } else {
                $sql = "INSERT tbl_Business_Listing_Category SET BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                    BLC_M_C_ID = '" . encode_strings($_POST['category'], $db) . "', BLC_C_ID = '" . encode_strings($val, $db) . "'";
            }
            $res1 = mysql_query($sql, $db);
        }
        $sql = "UPDATE tbl_Business_Listing SET hide_show_listing = 1 WHERE BL_ID = '$BL_ID'";
        $result = mysql_query($sql) or die(mysql_error());
    }
//    ob_start();
//    include '../include/email-template/coupon-approval-email.php';
//    $html = ob_get_contents();
//    ob_clean();
//    if (isset($Email) && $Email != '' && ($actCoupon['BFC_Status'] != $_POST['status'] && $_POST['status'] == 1)) {
//        $mail = new PHPMailer();
//        $mail->IsMail();
//        $mail->From = MAIN_CONTACT_EMAIL;
//        $mail->FromName = MAIN_CONTACT_NAME;
//        $mail->IsHTML(true);
//        $mail->AddAddress($Email);
//        $mail->CharSet = 'UTF-8';
//        $mail->Subject = "Coupon Approved";
//        $mail->MsgHTML($html);
//        //$mail->Send();
//    }
    $_SESSION['success'] = 1;
   header("Location:customer-feature-add-coupons.php?bl_id=" . $BL_ID . "&bfc_id=" . $bfc_id . "&re_dir=1");
     exit();
}

/// Dlete photo
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $delCouponImg = "UPDATE tbl_Business_Feature_Coupon SET BFC_Thumbnail = '', BFC_Main_Image = '' WHERE BFC_ID = '" . encode_strings($BFC_ID, $db) . "'";
    $resDelCouponImg = mysql_query($delCouponImg, $db) or die("Invalid query: $delCouponImg -- " . mysql_error());
    if ($resDelCouponImg) {
        //Updating Image Bank table because we cannot insert coupon id through picUpload function.
        //If we create new coupon then we do not have the BFC_ID to insert in image bank table
        $sql_image_bank = "UPDATE tbl_Image_Bank SET IB_Coupons = '' WHERE IB_ID = '" . encode_strings($pic_id, $db) . "'";
        mysql_query($sql_image_bank) or die("Invalid query: $sql_image_bank -- " . mysql_error());
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Coupon_Photo', $BFC_ID);
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Add Coupon', $BFC_ID, 'Delete Image', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location:customer-feature-add-coupons.php?bl_id=" . $BL_ID . "&bfc_id=" . $BFC_ID . "&re_dir=1");
        exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left" style="width: 100%; ">
    <?php require_once '../include/top-nav-listing.php'; ?> 
    <div style="width: 100%;">
        <?PHP require 'coupons-preview.php'; ?>
    </div>
<!--    <div class="left">
    </div>-->
      
    <div id="image-library" style="display:none;"></div>
</div>
<!--Coupon Add edit start-->

<form name="form1" action="" id="existing-coupon<?php echo $BFC_ID; ?>" onSubmit="return check_img_size(28, 10000000)"  enctype="multipart/form-data" method="post" style="display: none;">
    <div class="right">
        <div class="content-header">
            <div class="title">Add Coupon</div>
            <div class="link">
            </div>
        </div>       
    <input type="hidden" name="bl_id" value="<?php echo $BL_ID; ?>">
            <input type="hidden" name="bfc_id" value="<?php echo $BFC_ID; ?>">
            <input type="hidden" name="op" value="save_coupon">
            <div class="form-inside-div">
                <label>Coupon Title</label>
                <div class="form-data">
                    <input name="name" type="text" id="BLname" value="<?php echo $actCoupon['BFC_Title']; ?>" maxlength="37" required/>
                </div>
            </div>
            <div id="more_cat">
        <?PHP
        $sql_category = "SELECT * FROM tbl_Business_Feature_Coupon_Category_Multiple WHERE BFCCM_BFC_ID = $BFC_ID ORDER BY BFCCM_ID ASC  ";
        $result_category = mysql_query($sql_category);
        $categVal = mysql_query($sql_category);
        if ($result_category) {
            $count_category = mysql_num_rows($result_category);
        }
        if (isset($count_category) && $count_category != 0) {
            $i = 1;
            while ($rows = mysql_fetch_array($categVal)) {
                $valS[] = $rows['BFCCM_C_ID'];
                $sizearray = implode(", ", $valS);
            }
            $first = reset($valS);
            $last = end($valS);
            $secound = $first . ',' . $last;
            $output1 = array_slice($valS, 1, 2);
            $firestVal = implode(", ", $output1);
            $secoundVal = $first . ',' . $last;
            $output3 = array_slice($valS, 0, -1);
            $thirdVal = implode(", ", $output3);
            $sizearrays = sizeof($valS);
            while ($row1 = mysql_fetch_array($result_category)) {
        ?>
        <?php if ($i == 1) { ?>
            <input type="hidden" name="total_cat" id="check-cat" value="<?php echo $count_category != 0 ? $count_category : 1 ?>">
        <?php } ?>
        <div class="form-inside-div">
            <label>Category <?php echo $i ?></label>
            <div class="form-data">
                <select name="c_coupon_<?php echo $i ?>" <?php echo ($i == 1) ? 'required' : '' ?>>
                    <option value="">Select Category</option>
                    <?php
                    if ($i == 1) {
                        if ($sizearrays == 1) {
                            $catVal = '';
                        } else {
                            $catVal = " AND C_ID NOT IN($firestVal)";
                        }
                    } elseif ($i == 2) {
                        if ($sizearrays == 2) {
                            $catVal = " AND C_ID NOT IN($first)";
                        } else {
                            $catVal = " AND C_ID NOT IN($secoundVal)";
                        }
                    } elseif ($i == 3) {
                        $catVal = " AND C_ID NOT IN($thirdVal)";
                    }
                    $sql_cat_coupon = "SELECT C_ID, C_Name_SEO, C_Name FROM tbl_Category
                                       WHERE C_Parent = (SELECT C_ID FROM tbl_Category WHERE C_Is_Product_Web = 1) 
                                        $catVal  ORDER BY C_Name";

                    $result_cat_coupon = mysql_query($sql_cat_coupon);
                    while ($row = mysql_fetch_array($result_cat_coupon)) {
                        ?>
                        <option  <?php echo ($row1['BFCCM_C_ID'] == $row['C_ID']) ? "selected" : "" ?> value="<?php echo $row['C_ID'] ?>"><?php echo $row['C_Name'] ?></option>
                        <?php
                    }
                    ?>
                </select>
                </div>
              <?php if ($i == 1 && $count_category != 3) { ?>
                      <a class="add_categories" onclick="add_more_cat()">+Add Category</a>
                  <?php } else if ($i != 1) {
                      ?>
                      <a onClick="return confirm('Are you sure?');" class="delete_categories coupon" href="customer-feature-add-coupons.php?bl_id=<?php echo $BL_ID ?>&bfc_id=<?php echo $BFC_ID ?>&op=del_cat&bfccm_id=<?php echo $row1['BFCCM_ID'] ?>">Delete</a>
                      <input type="hidden" name="bfccm_id" value="<?php echo $row1['BFCCM_ID'] ?>">
                  <?php }
                  ?>
          </div> 
          <?php
          $i++;
         }
         } else {
            ?>
            <input type="hidden" name="total_cat" id="check-cat" value="1">
            <div class="form-inside-div">
                <label>Category 1</label>
                <div class="form-data">
                    <select name="c_coupon_1" required>
                        <option value="">Select Category</option>
                        <?php
                        $sql_cat_coupon = "SELECT C_ID, C_Name_SEO, C_Name FROM tbl_Category
                                           WHERE C_Parent = (SELECT C_ID FROM tbl_Category WHERE C_Is_Product_Web = 1) 
                                           ORDER BY C_Name";
                        $result_cat_coupon = mysql_query($sql_cat_coupon);
                        while ($row = mysql_fetch_array($result_cat_coupon)) {
                            ?>
                            <option value="<?php echo $row['C_ID'] ?>"><?php echo $row['C_Name'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <a class="add_categories" onclick="add_more_cat()">+Add Category</a>
                </div>
            </div> 
        <?php }
        ?>
            </div>
            <div class="form-inside-div">
                <label>Deal Description</label>
                <div class="form-data">
                    <textarea class="text-deal-des" name="deal_description"><?php echo $actCoupon['BFC_Description']; ?></textarea>
                </div>
            </div>
            <div class="form-inside-div">
                <label>Terms & Conditions</label>
                <div class="form-data">
                    <textarea class="text-deal-des" name="deal_terms"><?php echo $actCoupon['BFC_Terms_Conditions']; ?></textarea>
                </div>
            </div>
            <div class="form-inside-div">
                <label>Expiry Date</label>
                <div class="form-data">
                    <input class="previous-date-not-allowed" type="text" name="expire_date"  value="<?php echo $actCoupon['BFC_Expiry_Date']; ?>"/>
                </div>
            </div>
            <div class="form-inside-div">
                <label>Photo</label>
                <div class="form-data form-input-text">
                    <div class="div_image_library">
                        <span class="daily_browse" onclick="show_image_library(28)">Select File</span>
                        <input onchange="show_file_name(28, this, 0)" id="photo28" type="file" name="pic[]" style="display: none;"/>
                        <input type="hidden" name="image_bank" class="image_bank" id="image_bank28" value="">
                    </div>
                    <div class="form-inside-div add-about-us-item-image margin-none">
                        <div class="cropit-image-preview aboutus-photo-perview"> 
                            <img class="preview-img preview-img-script28" style="display: none;" src="">    
                            <?php if (isset($actCoupon['BFC_Main_Image']) && $actCoupon['BFC_Main_Image'] != '') { ?>
                                <input type="hidden" id="update_28" value="1"/>
                                <img class="existing-img existing_imgs28" alt="<?php echo  $actCoupon['BFC_Main_Image'] ?>" src="http://touristtowndemo.com/images/DB/<?php echo  $actCoupon['BFC_Main_Image'] ?>" >
                            <?php } ?>
                        </div>
                    </div>                    
                </div>
                <?php if (isset($actCoupon['BFC_Main_Image']) && $actCoupon['BFC_Main_Image'] != '') { ?>
                    <div class="delete-photo-coupon"><a onClick="return confirm('Are you sure?');" class="delete-coupon-pic" href="customer-feature-add-coupons.php?bl_id=<?php echo $BL_ID ?>&bfc_id=<?php echo $BFC_ID ?>&op=del">Delete Photo</a></div>
                <?php } ?>
            </div>
              <div class="form-inside-div">
                <label>Status</label>
                <div class="form-data">
                    <select name="status">
                        <option value="">Select One</option>
                        <option value="0" <?php echo ($actCoupon['BFC_Status'] == 0) ? 'selected' : '' ?>>Pending</option>
                        <option value="1" <?php echo ($actCoupon['BFC_Status'] == 1) ? 'selected' : '' ?>>Approved</option>
                    </select>
                </div>
            </div>
            <div class="form-inside-div">
                <div class="button">
                    <input type="submit" name="submit" value="Save Now"/>
                </div>
            </div>
        </div>
        </form>

<!--Coupon Add edit end-->

<!-- form for contact details-->
<?php
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing_contact_details = mysql_fetch_assoc($result);
    $BID = $rowListing_contact_details['B_ID'];
}
$sql = "SELECT * FROM tbl_Points_Category WHERE PC_ID != 14";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
while ($rowCategories = mysql_fetch_array($result)) {
    //Contact Details
    if ($rowCategories['PC_Name'] == 'Contact Details') {
        if (isset($rowListing_contact_details['BL_Listing_Title']) && $rowListing_contact_details['BL_Listing_Title'] != '' && isset($rowListing_contact_details['BL_Contact']) && $rowListing_contact_details['BL_Contact'] != '' && $rowListing_contact_details['BL_Phone'] != '' && $rowListing_contact_details['BL_Website'] != '' && $rowListing_contact_details['BL_Toll_Free'] != '' && $rowListing_contact_details['BL_Email'] != '') {
            $class = 'points-com';
        } else {
            $class = 'points-uncom';
        }
        $total_cd_points = $rowCategories['PC_Points'];
    }
}
?>
<form name="form1" id="contact_details_from_<?php echo $BFC_ID ?>" method="post" action="" style="display:none;">
    <div class="right">
    <script>
        function makeSEO(myVar) {
            myVar = myVar.toLowerCase();
            myVar = myVar.replace(/[^a-z0-9\s-]/gi, '');
            myVar = myVar.replace(/"/g, '');
            myVar = myVar.replace(/'/g, '');
            myVar = myVar.replace(/&/g, '');
            myVar = myVar.replace(/\//g, '');
            myVar = myVar.replace(/,/g, '');
            myVar = myVar.replace(/  /g, ' ');
            myVar = myVar.replace(/ /g, '-');

            $('#SEOname').val(myVar);
            return false;
        }
    </script>
    <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
    <input type="hidden" name="bfc_id" value="<?php echo $BFC_ID ?>">
    <input type="hidden" name="op" value="save_contact_details">
    <div class="content-header">Contact Details</div>

    <div class="form-inside-div">
        <label>Listing Title</label>
        <div class="form-data">
            <input class="instructional-background" name="name" type="text" id="BLname" value="<?php echo $rowListing_contact_details['BL_Listing_Title'] ?>" size="50" onKeyUp="makeSEO(this.value)" required/>
        </div>
        <?php
        $listing = show_field_points('Listing Title');
        if ($rowListing_contact_details['BL_Listing_Title']) {
            echo '<div class="points-com">' . $listing . ' pts</div>';
            $points_taken += $listing;
        } else {
            echo '<div class="points-uncom">' . $listing . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div">
        <label>SEO Name</label>
        <div class="form-data">
            <input name="seoName" id="SEOname" type="text" value="<?php echo $rowListing_contact_details['BL_Name_SEO'] ?>" onKeyUp="makeSEO(this.value)" size="50" required/>
        </div>
    </div>

    <div class="form-inside-div">
        <label>Contact Name</label>
        <div class="form-data">
            <input name="contact" type="text" value="<?php echo $rowListing_contact_details['BL_Contact'] ?>" size="50" />
        </div>
        <?php
        $contact = show_field_points('Contact Name');
        if ($rowListing_contact_details['BL_Contact']) {
            echo '<div class="points-com">' . $contact . ' pts</div>';
            $points_taken += $contact;
        } else {
            echo '<div class="points-uncom">' . $contact . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div">
        <label>Phone</label>
        <div class="form-data">
            <input class="instructional-background" name="phone" class="phone_number" type="text" value="<?php echo $rowListing_contact_details['BL_Phone'] ?>" size="50"/>
        </div>
        <?php
        $phone = show_field_points('Phone');
        if ($rowListing_contact_details['BL_Phone']) {
            echo '<div class="points-com">' . $phone . ' pts</div>';
            $points_taken += $phone;
        } else {
            echo '<div class="points-uncom">' . $phone . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div">
        <label>Toll Free</label>
        <div class="form-data">
            <input name="tollFree" class="phone_toll" type="text" value="<?php echo $rowListing_contact_details['BL_Toll_Free'] ?>" size="50" />
        </div>
        <?php
        $toll = show_field_points('Toll Free');
        if ($rowListing_contact_details['BL_Toll_Free']) {
            echo '<div class="points-com">' . $toll . ' pts</div>';
            $points_taken += $toll;
        } else {
            echo '<div class="points-uncom">' . $toll . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div">
        <label>Cell</label>
        <div class="form-data">
            <input name="cell" class="phone_number" type="text" value="<?php echo $rowListing_contact_details['BL_Cell'] ?>" size="50" />
        </div>
    </div>

    <div class="form-inside-div">
        <label>Fax</label>
        <div class="form-data">
            <input name="fax" class="phone_number" type="text" value="<?php echo $rowListing_contact_details['BL_Fax'] ?>" size="50" />
        </div>
    </div>

    <div class="form-inside-div">
        <label>Email</label>
        <div class="form-data">
            <input class="instructional-background" name="email" type="email" value="<?php echo $rowListing_contact_details['BL_Email'] ?>" size="50" />
        </div>
        <?php
        $Email = show_field_points('Email');
        if ($rowListing_contact_details['BL_Email']) {
            echo '<div class="points-com">' . $Email . ' pts</div>';
            $points_taken += $Email;
        } else {
            echo '<div class="points-uncom">' . $Email . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div">
        <label>Website</label>
        <div class="form-data">
            <input name="website" type="text" value="<?php echo $rowListing_contact_details['BL_Website'] ?>" size="50" />
        </div>
        <?php
        $website = show_field_points('Website');
        if ($rowListing_contact_details['BL_Website']) {
            echo '<div class="points-com">' . $website . ' pts</div>';
            $points_taken += $website;
        } else {
            echo '<div class="points-uncom">' . $website . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div border-none">
        <div class="button">
            <input type="submit" name="button2" value="Save Now"/>
        </div>
    </div>

    <div class="form-inside-div listing-ranking border-none">
        Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_cd_points ?> points
    </div>
    </div>
</form>

<!--end-->
<!-- CUSTOMER LISTING MAPPING -->
<?php
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words, BL_Location_Description, BL_Country  
            FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing_listing_mapping = mysql_fetch_assoc($result);
    $BID = $rowListing_listing_mapping['B_ID'];
}
$sql = "SELECT * FROM tbl_Points_Category WHERE PC_ID != 14";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
while ($rowCategories = mysql_fetch_array($result)) {
    //Mapping
    if ($rowCategories['PC_Name'] == 'Location & Mapping') {
        if (isset($rowListing_listing_mapping['BL_Street']) && $rowListing_listing_mapping['BL_Street'] != '' && $rowListing_listing_mapping['BL_Town'] != '' && $rowListing_listing_mapping['BL_Province'] != '' && $rowListing_listing_mapping['BL_PostalCode'] != '' && $rowListing_listing_mapping['BL_Lat'] != '' && $rowListing_listing_mapping['BL_Long'] != '') {
            $class = 'points-com';
        } else {
            $class = 'points-uncom';
        }
        $total_mapping_points = $rowCategories['PC_Points'];
    }
}
?>
<form name="form1" id="customer_listing_mapping_from_<?php echo $BFC_ID ?>" id="maps" method="post" action="" style="display:none; height: 600px;" onsubmit="return checkVal()">
    <div class="right">
    <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
    <input type="hidden" name="op" value="save_cutomer_listing_mapping">
     <input type="hidden" name="bfc_id" value="<?php echo $BFC_ID ?>">
    <div class="content-header">Location & Mapping</div>
    <div class="form-inside-div">
        <label>Street Address</label>
        <div class="form-data">
            <input class="instructional-background" name="address" type="text" id="address" value="<?php echo $rowListing_listing_mapping['BL_Street'] ?>" size="50" />
        </div>
        <?php
        $address = show_field_points('Street Address');
        $points_taken = 0;
        if ($rowListing_listing_mapping['BL_Street']) {
            echo '<div class="points-com">' . $address . ' pts</div>';
            $points_taken += $address;
        } else {
            echo '<div class="points-uncom">' . $address . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div">
        <label>Town</label>
        <div class="form-data">
            <input class="instructional-background" name="town" type="text" id="town" value="<?php echo $rowListing_listing_mapping['BL_Town'] ?>" size="50" />
        </div>
        <?php
        $town = show_field_points('Town');
        if ($rowListing_listing_mapping['BL_Town']) {
            echo '<div class="points-com">' . $town . ' pts</div>';
            $points_taken += $town;
        } else {
            echo '<div class="points-uncom">' . $town . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div">
        <label>Province</label>
        <div class="form-data">
            <input class="instructional-background" name="province" type="text" id="province" value="<?php echo $rowListing_listing_mapping['BL_Province'] ?>" size="50" />
        </div>
        <?php
        $province = show_field_points('Province');
        if ($rowListing_listing_mapping['BL_Province']) {
            echo '<div class="points-com">' . $province . ' pts</div>';
            $points_taken += $province;
        } else {
            echo '<div class="points-uncom">' . $province . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div">
        <label>Postal Code</label>
        <div class="form-data">
            <input class="instructional-background" name="postalcode" type="text" id="postalcode" value="<?php echo $rowListing_listing_mapping['BL_PostalCode'] ?>" size="50" />
        </div>
        <?php
        $postal = show_field_points('Postal Code');
        if ($rowListing_listing_mapping['BL_PostalCode']) {
            echo '<div class="points-com">' . $postal . ' pts</div>';
            $points_taken += $postal;
        } else {
            echo '<div class="points-uncom">' . $postal . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div">
        <label>Country</label>
        <div class="form-data">
            <input name="country" type="text" id="town" value="<?php echo $rowListing_listing_mapping['BL_Country'] ?>" size="50" />
        </div>
    </div>

    <div class="form-inside-div">
        <label>Location Description</label>
        <div class="form-data" id="ad_div">
            <textarea name="locationdescription" cols="85" rows="10" class="description" id="description-listing"><?php echo $rowListing_listing_mapping['BL_Location_Description'] ?></textarea>
        </div>
        <script>
            $('.editCkeditor').click(function(){
                $('#customer_listing_mapping_from_<?php echo $BFC_ID; ?>').attr("title", "Location & Mapping").dialog('open');
                $('#description-listing').ckeditor();
             });
             $(function(){ 
                $('#customer_listing_mapping_from_<?php echo $BFC_ID; ?>').attr("title", "Location & Mapping").dialog({
                    autoOpen:false,
                  close:function(){
                    var editor = $('.ui-dialog textarea').ckeditorGet();
                    editor.destroy();
                  }
                });
            });
    </script> 
    </div>

    <div class="form-inside-div">
        <div class="content-sub-header">
            <div class="title">Google Maps</div>
        </div>
        <?php
        $help_text = show_help_text('Google Maps');
        if ($help_text != '') {
            echo '<div class="gmap-left">' . $help_text . '</div>';
        }
        ?>
        <script type="text/javascript">
            function checkVal() {
                var latitudeVal = $("#latitude").val();
                var longitudeVal = $("#longitude").val();
                if (latitudeVal != '' && longitudeVal == '') {
                    alert('Longitude must be entered.');
                    $("#longitude").focus();
                    return false;
                }
                if (latitudeVal == '' && longitudeVal != '') {
                    alert('Latitude must be entered.');
                    $("#longitude").focus();
                    return false;
                }
            }
        </script>
        <div class="gmap-right">
            <div class="form-inside-div-gmap">
                <label>Latitude</label>
                <div class="form-data maps">
                    <input name="latitude" type="text" id="latitude" value="<?php echo $rowListing_listing_mapping['BL_Lat'] ?>" size="10" />
                </div>
                <?php
                $lat = show_field_points('Latitude');
                if ($rowListing_listing_mapping['BL_Lat']) {
                    echo '<div class="points-com">' . $lat . ' pts</div>';
                    $points_taken += $lat;
                } else {
                    echo '<div class="points-uncom">' . $lat . ' pts</div>';
                }
                ?>
            </div>
            <div class="form-inside-div-gmap border-none">
                <label>Longitude</label>
                <div class="form-data maps">
                    <input name="longitude" type="text" id="longitude" value="<?php echo $rowListing_listing_mapping['BL_Long'] ?>" size="10" />
                </div>
                <?php
                $long = show_field_points('Latitude');
                if ($rowListing_listing_mapping['BL_Long']) {
                    echo '<div class="points-com">' . $long . ' pts</div>';
                    $points_taken += $long;
                } else {
                    echo '<div class="points-uncom">' . $long . ' pts</div>';
                }
                ?>
            </div>
        </div>
    </div>

    <div class="form-inside-div border-none">
        <div class="button">
            <input type="submit" name="button2" value="Save Now"/>
        </div>
    </div> 

    <div class="form-inside-div listing-ranking border-none">
        Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_mapping_points ?> points
    </div>
    </div>
</form>
<!--END-->

<!--Social media form-->
<form name="form1" method="post" id="social_media_<?php echo $id; ?>" class="social_media_<?php echo $id; ?>" action="" style="display:none;">
            <input type="hidden" name="op" value="save_social_media">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="bfc_id" value="<?php echo $id; ?>">
            <div class="content-header">
                <div class="title">Social Media</div>
                <div class="link">
                    <?
                    $sql = "SELECT * FROM tbl_Points_Category WHERE PC_ID != 14";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    while ($rowCategories = mysql_fetch_array($result)) {
                        //Amenities
                        if ($rowCategories['PC_Name'] == 'Social Media') {
                            $sqlSM = "SELECT * FROM tbl_Business_Social WHERE BS_BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
                            $resultSM = mysql_query($sqlSM) or die(mysql_error());
                            $rowsSM = mysql_fetch_array($resultSM);
                            if ($rowsSM['BS_FB_Link'] != '' || $rowsSM['BS_T_Link'] != '' || $rowsSM['BS_Y_Link'] || $rowsSM['BS_I_Link']) {
                                $class = 'points-com';
                            } else {
                                $class = 'points-uncom';
                            }
                            $total_sm_points = $rowCategories['PC_Points'];
                        }
                    }
                    $sqlSM = "SELECT * FROM tbl_Business_Social WHERE BS_BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
                    $resultSM = mysql_query($sqlSM) or die(mysql_error());
                    $rowsSM = mysql_fetch_array($resultSM);
                    $SM = show_field_points('Hours');
                    if ($rowsSM['BS_FB_Link'] != '' || $rowsSM['BS_T_Link'] != '' || $rowsSM['BS_Y_Link'] || $rowsSM['BS_I_Link']) {
                        echo '<div class="points-com des-heading-point" style="float:right;margin-top: -13px;color: #66a025;">' . $SM . ' pts</div>';
                        $points_taken = $SM;
                    } else {
                        echo '<div class="points-uncom des-heading-point" style="float:right;margin-top: -13px;color: #66a025;">' . $SM . ' pts</div>';
                    }
                    ?>
                </div>
            </div>

            <?php
            $help_text = show_help_text('Social Media');
            if ($help_text != '') {
                echo '<div class="form-inside-div" style="border-bottom:3px solid #eeeeee;">' . $help_text . '</div>';
            }
            ?>

            <?PHP
            $sql = "SELECT BS_FB_Link, BS_T_Link, BS_I_Link FROM tbl_Business_Social WHERE BS_BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $count = mysql_num_rows($result);
            $row = mysql_fetch_assoc($result);
            ?> 

            <div class="form-inside-div">
                <label>Facebook Link</label>
                <div class="form-data">
                    <input name="fb_link" type="text" value="<?php echo $row['BS_FB_Link'] ?>" size="50" />
                </div>
            </div>

            <div class="form-inside-div">
                <label>Twitter Link</label>
                <div class="form-data">
                    <input name="t_link" type="text"  value="<?php echo $row['BS_T_Link'] ?>" size="50" />
                </div>
            </div>

            <div class="form-inside-div">
                <label>Instagram Link</label>
                <div class="form-data">
                    <input name="i_link" type="text"  value="<?php echo $row['BS_I_Link'] ?>" size="50" />
                </div>
            </div>

            <div class="form-inside-div">
                <div class="button">
                    <input type="hidden" name="counter" value="<?php echo $count; ?>">
                    <input type="submit" name="button" value="Submit" />
                </div>
            </div>
       

        <div class="form-inside-div listing-ranking border-none">
            Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_sm_points ?> points
        </div>
  </form>
    <!--house form-->
    <form name="form1" method="post" id="house_form_<?php echo $BFC_ID; ?>" action="" style="display:none;">
         <?php 
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title, BL_Hour_Mon_From, BL_Hour_Mon_To, BL_Hour_Tue_From, BL_Hour_Tue_To, BL_Hour_Wed_From, BL_Hour_Wed_To, 
            BL_Hour_Thu_From, BL_Hour_Thu_To, BL_Hour_Fri_From, BL_Hour_Fri_To, BL_Hour_Sat_From, BL_Hour_Sat_To, BL_Hour_Sun_From, BL_Hour_Sun_To, 
            BL_Hours_Disabled, BL_Hours_Appointment  FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
} 
 $sql = "SELECT * FROM tbl_Points_Category WHERE PC_ID != 14";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($rowCategories = mysql_fetch_array($result)) {
        //Hours
        if ($rowCategories['PC_Name'] == 'Hours') {
            $sqlHours = "SELECT * FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
            $resultHours = mysql_query($sqlHours) or die(mysql_error());
            $rowsHours = mysql_fetch_array($resultHours);
            if ($rowsHours) {
                if (!$rowsHours['BL_Hours_Disabled'] && ($rowsHours['BL_Hour_Mon_From'] != '00:00:00' || $rowsHours['BL_Hour_Tue_From'] != '00:00:00' || $rowsHours['BL_Hour_Wed_From'] != '00:00:00' || $rowsHours['BL_Hour_Thu_From'] != '00:00:00' || $rowsHours['BL_Hour_Fri_From'] != '00:00:00' || $rowsHours['BL_Hour_Sat_From'] != '00:00:00' || $rowsHours['BL_Hour_Sun_From'] != '00:00:00')) {
                    $class = 'points-com';
                } else {
                    $class = 'points-uncom';
                }
            }
            $total_hours_points = $rowCategories['PC_Points'];
        }
    }
?> 
        <div class="right">
            <input type="hidden" name="op" value="save_hours">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="bfc_id" value="<?php echo $id; ?>">
            <div class="content-header">
                <div class="title">Hours</div>
                <div class="link">
                <?php
                $sqlHours = "SELECT * FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
                    $resultHours = mysql_query($sqlHours) or die(mysql_error());
                    $rowsHours = mysql_fetch_array($resultHours);
                    $rowsHours['BL_Hours_Disabled'];
                    if ($rowsHours) { 
                        if (!$rowsHours['BL_Hours_Disabled'] && ($rowsHours['BL_Hour_Mon_From'] != '00:00:00' || $rowsHours['BL_Hour_Tue_From'] != '00:00:00' || $rowsHours['BL_Hour_Wed_From'] != '00:00:00' || $rowsHours['BL_Hour_Thu_From'] != '00:00:00' || $rowsHours['BL_Hour_Fri_From'] != '00:00:00' || $rowsHours['BL_Hour_Sat_From'] != '00:00:00' || $rowsHours['BL_Hour_Sun_From'] != '00:00:00')) {
                            $class = 'points-com';
                        } else {
                            $class = 'points-uncom';
                        }
                    } 
                 //   $total_hours_points = $rowCategories['PC_Points'];
                $Hours = show_field_points('Hours');
                if (!$rowsHours['BL_Hours_Disabled'] && ($rowsHours['BL_Hour_Mon_From'] != '00:00:00' || $rowsHours['BL_Hour_Tue_From'] != '00:00:00' || $rowsHours['BL_Hour_Wed_From'] != '00:00:00' || $rowsHours['BL_Hour_Thu_From'] != '00:00:00' || $rowsHours['BL_Hour_Fri_From'] != '00:00:00' || $rowsHours['BL_Hour_Sat_From'] != '00:00:00' || $rowsHours['BL_Hour_Sun_From'] != '00:00:00')) {
                    echo '<div class="points-com des-heading-point">' . $Hours . ' pts</div>';
                    $points_taken = $Hours;
                } else {
                    echo '<div class="points-uncom">' . $Hours . ' pts</div>';
                }
                ?>
                                </div>
                            </div>

                <?php
                $help_text = show_help_text('Hours');
                if ($help_text != '') {
                    echo '<div class="form-inside-div" style="border-bottom:3px solid #eeeeee;">' . $help_text . '</div>';
                }
                ?>
            <div class="form-inside-div hours">
                <label>Monday</label>
                <div class="form-data">
                    <select name="monFrom" id="monFrom" onchange="isClosed('monHide', this.value);">
                <?PHP  fromHours($rowListing['BL_Hour_Mon_From']); ?>
                    </select>
                    <span id="monHide" <?php echo $rowListing['BL_Hour_Mon_From'] == '00:00:01' || $rowListing['BL_Hour_Mon_From'] == '00:00:02' || $rowListing['BL_Hour_Mon_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                        <select name="monTo" id="monTo" onchange="">
                <?PHP toHours($rowListing['BL_Hour_Mon_To']); ?>
                        </select>
                    </span>
                </div>
            </div>
            <div class="form-inside-div hours">
                <label>Tuesday</label>
                <div class="form-data">
                    <select name="tueFrom" id="tueFrom" onchange="isClosed('tueHide', this.value);">
            <?PHP fromHours($rowListing['BL_Hour_Tue_From']); ?>
                    </select>
                    <span id="tueHide" <?php echo $rowListing['BL_Hour_Tue_From'] == '00:00:01' || $rowListing['BL_Hour_Tue_From'] == '00:00:02' || $rowListing['BL_Hour_Tue_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                        <select name="tueTo" id="tueTo" onchange="">
            <?PHP toHours($rowListing['BL_Hour_Tue_To']); ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="form-inside-div hours">
                <label>Wednesday</label>
                <div class="form-data">
                    <select name="wedFrom" id="wedFrom" onchange="isClosed('wedHide', this.value);">
<?PHP fromHours($rowListing['BL_Hour_Wed_From']); ?>
                    </select>
                    <span id="wedHide" <?php echo $rowListing['BL_Hour_Wed_From'] == '00:00:01' || $rowListing['BL_Hour_Wed_From'] == '00:00:02' || $rowListing['BL_Hour_Wed_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                        <select name="wedTo" id="wedTo" onchange="">
<?PHP toHours($rowListing['BL_Hour_Wed_To']); ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="form-inside-div hours">
                <label>Thursday</label>
                <div class="form-data">
                    <select name="thuFrom" id="thuFrom" onchange="isClosed('thuHide', this.value);">
<?PHP fromHours($rowListing['BL_Hour_Thu_From']); ?>
                    </select>
                    <span id="thuHide" <?php echo $rowListing['BL_Hour_Thu_From'] == '00:00:01' || $rowListing['BL_Hour_Thu_From'] == '00:00:02' || $rowListing['BL_Hour_Thu_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                        <select name="thuTo" id="thuTo" onchange="">
<?PHP toHours($rowListing['BL_Hour_Thu_To']); ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="form-inside-div hours">
                <label>Friday</label>
                <div class="form-data">
                    <select name="friFrom" id="friFrom" onchange="isClosed('friHide', this.value);">
<?PHP fromHours($rowListing['BL_Hour_Fri_From']); ?>
                    </select>
                    <span id="friHide" <?php echo $rowListing['BL_Hour_Fri_From'] == '00:00:01' || $rowListing['BL_Hour_Fri_From'] == '00:00:02' || $rowListing['BL_Hour_Fri_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                        <select name="friTo" id="friTo" onchange="">
<?PHP toHours($rowListing['BL_Hour_Fri_To']); ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="form-inside-div hours">
                <label>Saturday</label>
                <div class="form-data">
                    <select name="satFrom" id="satFrom" onchange="isClosed('satHide', this.value);">
<?PHP fromHours($rowListing['BL_Hour_Sat_From']); ?>
                    </select>
                    <span id="satHide" <?php echo $rowListing['BL_Hour_Sat_From'] == '00:00:01' || $rowListing['BL_Hour_Sat_From'] == '00:00:02' || $rowListing['BL_Hour_Sat_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                        <select name="satTo" id="satTo" onchange="">
<?PHP toHours($rowListing['BL_Hour_Sat_To']); ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="form-inside-div hours">
                <label>Sunday</label>
                <div class="form-data">
                    <select name="sunFrom" id="sunFrom" onchange="isClosed('sunHide', this.value);">
<?PHP fromHours($rowListing['BL_Hour_Sun_From']); ?>
                    </select>
                    <span id="sunHide" <?php echo $rowListing['BL_Hour_Sun_From'] == '00:00:01' || $rowListing['BL_Hour_Sun_From'] == '00:00:02' || $rowListing['BL_Hour_Sun_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                        <select name="sunTo" id="sunTo">
<?PHP toHours($rowListing['BL_Hour_Sun_To']); ?>
                        </select>
                    </span>
                </div>
            </div>

            <div class="form-inside-div">
                <label>Disable Hours</label>
                <div class="form-data">
                    <input name="hDisabled" type="checkbox" style="margin-top: 9px;" value="1" <?php echo $rowListing['BL_Hours_Disabled'] ? 'checked' : '' ?>>
                </div>
            </div>
            <div class="form-inside-div">
                <label>By Appointment</label>
                <div class="form-data">
                    <input name="happointment" type="checkbox" style="margin-top: 9px;" value="1" <?php echo $rowListing['BL_Hours_Appointment'] ? 'checked' : '' ?>>
                </div>
            </div>

            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" name="submit" value="Save Now"/>
                </div>
            </div>
            <div class="form-inside-div listing-ranking border-none">
                Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_hours_points ?> points
            </div>
       </div>
        <?php
        function fromHours($time) {
    echo "<option value='00:00:00'>Select One</option>";
    for ($i = 1; $i <= 23; $i++) {
        if ($i == 0) {
            echo "<option value='" . date('H:00:04', mktime($i)) . "'";
            echo $time == date('H:00:04', mktime($i)) ? ' selected' : '';
            echo ">" . date('g:ia', mktime($i, 0)) . "</option>";
        } else {
            echo "<option value='" . date('H:00:00', mktime($i)) . "'";
            echo $time == date('H:00:00', mktime($i)) ? ' selected' : '';
            echo ">" . date('g:ia', mktime($i, 0)) . "</option>";
        }
        echo "<option value='" . date('H:i:00', mktime($i, 30)) . "'";
        echo $time == date('H:i:00', mktime($i, 30)) ? ' selected' : '';
        echo ">" . date('g:ia', mktime($i, 30)) . "</option>";
    }
    echo "<option value='00:00:04'";
    echo $time == '00:00:04' ? ' selected' : '';
    echo ">12:00am</option>";
    echo "<option value='00:30:00'";
    echo $time == '00:30:00' ? ' selected' : '';
    echo ">12:30am</option>";
    echo "<option value='00:00:01'";
    echo $time == '00:00:01' ? ' selected' : '';
    echo ">Closed</option>";
    echo "<option value='00:00:03'";
    echo $time == '00:00:03' ? ' selected' : '';
    echo ">open 24 hours</option>";
}

function toHours($time) {
    echo "<option value='00:00:00'>Select One</option>";
    for ($i = 1; $i <= 23; $i++) {
        if ($i == 0) {
            echo "<option value='" . date('H:00:01', mktime($i)) . "'";
            echo $time == date('H:00:01', mktime($i)) ? ' selected' : '';
            echo ">" . date('g:ia', mktime($i, 0)) . "</option>";
        } else {
            echo "<option value='" . date('H:00:00', mktime($i)) . "'";
            echo $time == date('H:00:00', mktime($i)) ? ' selected' : '';
            echo ">" . date('g:ia', mktime($i, 0)) . "</option>";
        }
        echo "<option value='" . date('H:i:00', mktime($i, 30)) . "'";
        echo $time == date('H:i:00', mktime($i, 30)) ? ' selected' : '';
        echo ">" . date('g:ia', mktime($i, 30)) . "</option>";
    }
    echo "<option value='00:00:04'";
    echo $time == '00:00:04' ? ' selected' : '';
    echo ">12:00am</option>";
    echo "<option value='00:30:00'";
    echo $time == '00:30:00' ? ' selected' : '';
    echo ">12:30am</option>";
}
       ?>
 </form> 
    <?php if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
            } else {
    header("Location:customers.php");
    exit();
    }
?>

<script type="text/javascript">
     function show_customer_social_media(id_four) { 
        $(".social_media_" + id_four).attr("title", "Social Media").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 660,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery(".social_media_" + id_four).dialog('close');
                })
            }
        });
    }
 function show_customer_listing_mapping(id_one) {
        $("#customer_listing_mapping_from_" + id_one).attr("title", "Mapping Details").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 660,
            heighr:600,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#customer_listing_mapping_from_" + id_one).dialog('close');
                })
            }
        });
//         $('#description-listing').ckeditor();
//         CKEDITOR.disableAutoInline = true;
//         CKEDITOR.inline('locationdescription');
    }
 function show_contact_details_form(id_two) {
        $("#contact_details_from_" + id_two).attr("title", "Contact Details").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 672,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#contact_details_from_" + id_two).dialog('close');
                })
            }
        });
    }
    function show_customer_hours_allday(id_three) { 
        $("#house_form_" + id_three).attr("title", "Hours Details").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 660,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#house_form_" + id_three).dialog('close');
                })
            }
        });
    }
function coupon_edit(id_five){ 
 $("#existing-coupon" + id_five).attr("title", "Edit Coupon").dialog({
        modal: true,
        draggable: false,
        resizable: false,
        width: 750,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#existing-coupon" + id_five).dialog('close');
                })
            }
    });   
}
  var next_count;
function DeleteVal(id) {
        $("#lab_num_" + next_count).text('Category ' + id);
        $("#category_num_" + id).remove();
        var count = $("#check-cat").val();
        next_count = --count;
        $("#check-cat").val(next_count);
        $('#cat_coupon' + 3).attr('name', 'c_coupon_2');
        $('#cat_coupon' + 3).attr('class', 'cat_coupon_2');
        $('#cat_coupon' + 3).attr('id', 'cat_coupon2');
        $("#lab_num_" + 3).text('Category 2');
        $('#del_' + 3).attr("onclick", "DeleteVal(2)");
        $('#del_' + 3).attr('id', 'del_2');
        $('#category_num_' + 3).attr('id', 'category_num_2');
    }
 function add_more_cat()
    {
        var count = $("#check-cat").val();
        var cat_coupons =<?php echo $cat_coupon ?>;
        next_count = ++count;
        if (count < 4) {
            $("#more_cat").append('<div id="category_num_' + next_count + '"  class="form-inside-div DeleteValuse"><label id="lab_num_' + next_count + '">Category ' + next_count + '</label><div class="form-data"><select name="c_coupon_' + next_count + '" class="cat_coupon_' + next_count + '" id="cat_coupon' + next_count + '"><option value="">Select Category</option></select></div><span class="delete_categoriess" id="del_' + next_count + '" onClick="DeleteVal(' + next_count + ')">Delete</span></div>');
            $.each(cat_coupons, function (index, cat) {
                $("#cat_coupon" + next_count).append('<option value="' + cat.C_ID + '">' + cat.C_Name + '</option>');
            });
            $("#check-cat").val(next_count);

        } else
        {
            alert("You can select only 3 catgeories.", "warning");
        }
    }
     function get_sub_category(count_id)
    {
        var cat_id = $('#category').val();
        $.ajax({
            type: "GET",
            url: "get-customer-listings-subcat-new.php",
            data: {
                cat_id: cat_id
            }
        }).done(function (msg) {
            $(".listings_subcategory").empty();
            $(".listings_subcategory").html(msg);
        });
    }

</script>
<style>
    .cke_dialog { 
z-index: 1000000 !important; 
} 
.form-inside-div.hours .form-data {
    width: 400px !important;
    margin-top: 10px;
}
.right .content-header {
    float: left;
    width: 540px;
    background: #f0f6ec;
    font-weight: bold;
    padding: 20px;
    color: #66a025;
    font-size: 22px;
}
 .right .content-header .title{
         color: #66a025;
         width: 50%;
         float: left;
     }
     .right .content-header .link {
        float: right;
        width: 50%;
        text-align: right;
    }
    .des-heading-point{
            float: right;
            text-align: right;
    }
.right .form-inside-div .form-data {
    float: left;
    width: 311px;
    padding: 10px;
}
.right .form-inside-div input[type="text"] {
    width: 271px;
}
.right {
    float: left;
    width: 580px;
    padding: 0 25px;
    height: 550px;
}
.form-inside-div select {
    background-image: url(../include/img/select_arrow.gif);
}
.form-inside-div .points-com {
        background-image: url(../include/img/tick.png);
    }
 .form-inside-div .points-uncom {
        background-image: url(../include/img/sub.gif);
    }
.form-inside-div .gmap-right .form-inside-div-gmap {
    width: 110px;
}

</style>
<?PHP
require_once '../include/admin/footer.php';
?>