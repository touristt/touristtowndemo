<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
} elseif (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
    $limits = explode(',', $_SESSION['USER_LIMIT']);
    $regionLimit = array();
    foreach ($limits as $limit) {
        $sql = "SELECT R_ID FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $region = mysql_fetch_assoc($result);
        if ($region['R_ID'] != '' && !in_array($region['R_ID'], $regionLimit)) {
            $regionLimit[] = $region['R_ID'];
        }
    }
}
$regionLimitCommaSeparated = rtrim(implode(',', $regionLimit), ',');

if (isset($_REQUEST['del']) && $_REQUEST['del'] == 'true') {
    // EVENT LOCATOIN
    $sql = "SELECT EL_ID, EL_Region_ID FROM Events_Location WHERE FIND_IN_SET('" . $_REQUEST['rid'] . "', EL_Region_ID)";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());

    while ($rowRList = mysql_fetch_assoc($result)) {
        $pieces = explode(",", $rowRList['EL_Region_ID']);
        $key = array_search($_REQUEST['rid'], $pieces);
        if (FALSE !== $key) {
            unset($pieces[$key]);
        }
        $list = implode(",", $pieces);
        $sql_update = "UPDATE Events_Location SET EL_Region_ID = '" . $list . "' WHERE EL_ID = '" . $rowRList['EL_ID'] . "'";
        $result_update = mysql_query($sql_update, $db);
    }

    // EVENT MASTER
    $sql = "SELECT EventID, E_Region_ID FROM Events_master WHERE FIND_IN_SET('" . $_REQUEST['rid'] . "', E_Region_ID)";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());

    while ($rowRList = mysql_fetch_assoc($result)) {
        $pieces = explode(",", $rowRList['E_Region_ID']);
        $key = array_search($_REQUEST['rid'], $pieces);
        if (FALSE !== $key) {
            unset($pieces[$key]);
        }
        $list = implode(",", $pieces);
        $sql_update = "UPDATE Events_master SET E_Region_ID = '" . $list . "' WHERE EventID = '" . $rowRList['EventID'] . "'";
        $result_update = mysql_query($sql_update, $db);
    }

    // EVENT ORGANIZATION
    $sql = "SELECT EO_ID, EO_Region_ID FROM Events_Organization WHERE FIND_IN_SET('" . $_REQUEST['rid'] . "', EO_Region_ID)";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());

    while ($rowRList = mysql_fetch_assoc($result)) {
        $pieces = explode(",", $rowRList['EO_Region_ID']);
        $key = array_search($_REQUEST['rid'], $pieces);
        if (FALSE !== $key) {
            unset($pieces[$key]);
        }
        $list = implode(",", $pieces);
        $sql_update = "UPDATE Events_Organization SET EO_Region_ID = '" . $list . "' WHERE EO_ID = '" . $rowRList['EO_ID'] . "'";
        $result_update = mysql_query($sql_update, $db);
    }

    // IMAGE BANK
    $sql = "SELECT IB_ID, IB_Region FROM tbl_Image_Bank WHERE FIND_IN_SET('" . $_REQUEST['rid'] . "', IB_Region)";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());

    while ($rowRList = mysql_fetch_assoc($result)) {
        $pieces = explode(",", $rowRList['IB_Region']);
        $key = array_search($_REQUEST['rid'], $pieces);
        if (FALSE !== $key) {
            unset($pieces[$key]);
        }
        $list = implode(",", $pieces);
        $sql_update = "UPDATE tbl_Image_Bank SET IB_Region = '" . $list . "' WHERE IB_ID = '" . $rowRList['IB_ID'] . "'";
        $result_update = mysql_query($sql_update, $db);
    }


    $sql = "DELETE tbl_Region, tbl_Region_Multiple, tbl_Region_Themes_Photos,tbl_Region_Social, tbl_Region_404, tbl_Region_Category, tbl_Route, tbl_Business_Listing_Category_Region, tbl_Story_Homepage_Category, tbl_Route_Category, tbl_Theme_Options, Events_Towns, payment_profiles, tbl_404_Listing, tbl_Business_Listing_Daytrip, tbl_Business_Listing_VideoSP, tbl_Community_Organization_lisitings, tbl_Contests_Regions, tbl_Email_Detail, tbl_Footer_Photo, tbl_Image_Bank_Photographer_Region, tbl_Image_Bank_Usage, tbl_Individual_Route_Region, tbl_Region_BG, tbl_Region_Category_Photos, tbl_Region_Category_Stats, tbl_Region_Icon, tbl_Region_Listings_Nearby, tbl_region_map, tbl_Region_Themes, tbl_Region_Topnav_Link, tbl_Story_Region, tbl_Theme_Options_Mobile, tbl_User_Permission, tbl_User_Role
            FROM tbl_Region
            LEFT JOIN tbl_Region_Multiple ON tbl_Region_Multiple.RM_Parent = tbl_Region.R_ID
            LEFT JOIN tbl_Region_Themes_Photos ON tbl_Region_Themes_Photos.RTP_RT_RID = tbl_Region.R_ID
            LEFT JOIN tbl_Region_Social ON tbl_Region_Social.RS_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_Region_404 ON tbl_Region_404.RP_RID = tbl_Region.R_ID
            LEFT JOIN tbl_Region_Category ON tbl_Region_Category.RC_R_ID = tbl_Region.R_ID 
            LEFT JOIN tbl_Route ON tbl_Route.R_RID = tbl_Region.R_ID 
            LEFT JOIN tbl_Business_Listing_Category_Region ON tbl_Business_Listing_Category_Region.BLCR_BLC_R_ID = tbl_Region.R_ID 
            LEFT JOIN tbl_Story_Homepage_Category ON tbl_Story_Homepage_Category.SHC_R_ID = tbl_Region.R_ID 
            LEFT JOIN tbl_Route_Category ON tbl_Route_Category.RC_R_ID = tbl_Region.R_ID 
            LEFT JOIN tbl_Theme_Options ON tbl_Theme_Options.TO_R_ID = tbl_Region.R_ID
            LEFT JOIN Events_Towns  ON Events_Towns.ET_Region_ID =  tbl_Region.R_ID  
            LEFT JOIN payment_profiles  ON payment_profiles.region = tbl_Region.R_ID
            LEFT JOIN tbl_404_Listing  ON tbl_404_Listing.R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_Business_Listing_Daytrip  ON tbl_Business_Listing_Daytrip.BLD_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_Business_Listing_VideoSP  ON tbl_Business_Listing_VideoSP.BLV_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_Community_Organization_lisitings  ON  tbl_Community_Organization_lisitings.COL_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_Contests_Regions ON tbl_Contests_Regions.CR_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_Email_Detail ON tbl_Email_Detail.ED_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_Footer_Photo ON tbl_Footer_Photo.FP_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_Image_Bank_Photographer_Region ON tbl_Image_Bank_Photographer_Region.IBPR_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_Image_Bank_Usage ON tbl_Image_Bank_Usage.IBU_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_Individual_Route_Region ON tbl_Individual_Route_Region.IRR_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_Region_BG ON tbl_Region_BG.RBG_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_Region_Category_Photos ON tbl_Region_Category_Photos.RCP_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_Region_Category_Stats ON tbl_Region_Category_Stats.RCS_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_Region_Icon ON tbl_Region_Icon.RI_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_Region_Listings_Nearby ON tbl_Region_Listings_Nearby.RLN_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_region_map ON tbl_region_map.R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_Region_Themes ON tbl_Region_Themes.RT_RID = tbl_Region.R_ID
            LEFT JOIN tbl_Region_Topnav_Link ON tbl_Region_Topnav_Link.RTL_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_Story_Region ON tbl_Story_Region.SR_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_Theme_Options_Mobile ON tbl_Theme_Options_Mobile.TO_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_User_Permission ON tbl_User_Permission.UP_R_ID = tbl_Region.R_ID
            LEFT JOIN tbl_User_Role  ON tbl_User_Role.UR_R_ID = tbl_Region.R_ID
            WHERE tbl_Region.R_ID = '" . encode_strings($_REQUEST['rid'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());

    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['rid'];
        Track_Data_Entry('Communities', '', 'Community Without Website', $id, 'Delete', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: communities.php");
    exit();
}
// 

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Communities</div>
    </div>
    <div class="left">
        <?php require_once '../include/nav-B-community.php'; ?>
    </div>
    <div class="right">

        <?php
        $sqlTypes = "SELECT RT_ID, RT_Name FROM tbl_Region_Type WHERE RT_ID = 8 ORDER BY RT_Order ASC";
        $resTypes = mysql_query($sqlTypes);
        while ($types = mysql_fetch_assoc($resTypes)) {
            ?>
            <div class="content-header content-header-search content-region">
                <span><?php print $types['RT_Name'] ?></span>
            </div>

            <div class="region-container-layout" style="width:100%;">
                <div class="content-sub-header link-header region-header-padding">
                    <div class="data-column padding-none spl-name-store-communities">Name</div>
                    <div class="data-column padding-none spl-other-regions">Accepted Terms</div>
                    <div class="data-column padding-none ccp-other-regions">Manage</div>
                    <div class="data-column padding-none ccp-other-regions">Delete</div>
                </div>
                <?PHP
                $sql = "SELECT R_ID, R_Name, R_Parent, R_Status, R_Domain FROM tbl_Region";
                $sql .= $regionLimitCommaSeparated ? (" WHERE R_ID IN (" . $regionLimitCommaSeparated . ") ") : "";
                $sql .= $regionLimitCommaSeparated ? (" AND R_Type = 8") : (" WHERE R_Type = 8");
                $sql .= " ORDER BY R_Name";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                $counter = 0;
                while ($row = mysql_fetch_assoc($result)) {
                    $sql = "SELECT B_Agreement_Accepted
                            FROM `tbl_Business_Listing`
                            LEFT JOIN tbl_Business ON B_ID = `BL_B_ID` 
                            LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID
                            INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID 
                            WHERE `BL_Listing_Type` <> 1 
                            AND (B_Agreement_Accepted = 1 OR B_Agreement_Accepted = 0)
                            AND B_ID > 0
                            AND BLCR_BLC_R_ID ";
                    if ($row['R_Parent'] > 0) {
                        $sql .= "= '" . $row['R_ID'] . "'";
                    } else {
                        $sql .= "IN ";

                        $sql2 = "SELECT R_ID FROM tbl_Region WHERE R_Parent = '" . encode_strings($row['R_ID'], $db) . "'";
                        $resultRL = mysql_query($sql2, $db) or die("Invalid query: $sql2 -- " . mysql_error());
                        $sql .= "(" . $row['R_ID'];
                        while ($rowRL = mysql_fetch_assoc($resultRL)) {
                            $sql .= "," . $rowRL['R_ID'];
                        }
                        $sql .= ")";
                    }
                    $sql .= " GROUP BY B_ID";
                    $loginCounter = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    $acceptedTerms = 0;
                    $notAcceptedTerms = 0;
                    while ($loginCount = mysql_fetch_array($loginCounter)) {
                        if ($loginCount['B_Agreement_Accepted'] == 1) {
                            $acceptedTerms += $loginCount['B_Agreement_Accepted'];
                        }
                        if ($loginCount['B_Agreement_Accepted'] == 0) {
                            $notAcceptedTerms++;
                        }
                    }
                    $counter++;
                    ?>
                    <div class="data-content region-content-padding">
                        <div class="data-column spl-name-store-communities"><?php echo $row['R_Name'] ?></div>
                        <div class="data-column spl-other-regions"><?php echo $acceptedTerms ?> of <?php echo ($acceptedTerms + $notAcceptedTerms) ?> </div>
                        <div class="data-column ccp-other-regions"><a href="community.php?rid=<?php echo $row['R_ID'] ?>">Manage</a></div>
                        <div class="data-column ccp-other-regions"><a onClick="return confirm('Are you sure this action can not be undone!');" target="" href="communities.php?rid=<?php echo $row['R_ID'] ?>&del=true">Delete</a></div>
                    </div>
                    <?PHP
                }
                ?>   
            </div>

            <?php
        }
        ?>   
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>