<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Domains</div>
    </div>
    <div class="left">
        <div class="sub-menu">
            <ul>
                <li class="addlisting region-sub"><a href="domain.php">+Add Domain</a></li>
            </ul>
        </div>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name-store">Domain</div>
            <div class="data-column padding-none spl-other">Renewal Date</div>
            <div class="data-column padding-none spl-other">Edit</div>
            <div class="data-column padding-none spl-other">Delete</div>
        </div>
        <?PHP
        $sql = "SELECT D_ID, D_Domain, D_Renewal_Date FROM tbl_Domain ORDER BY D_Domain";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        if (mysql_num_rows($result)) {
            while ($row = mysql_fetch_assoc($result)) {
                ?>
                <div class="data-content">
                    <div class="data-column spl-name-store"><?php echo $row['D_Domain'] ?></div>
                    <div class="data-column spl-other"><?php echo $row['D_Renewal_Date'] ?></div>
                    <div class="data-column spl-other"><a href="domain.php?id=<?php echo $row['D_ID'] ?>">Edit</a></div>
                    <div class="data-column spl-other"><a onClick="return confirm('Are you sure?\nThis action CANNOT be undone!');" href="domain.php?op=del&amp;id=<?php echo $row['D_ID'] ?>">Delete</a></div>
                </div>
                <?PHP
            }
        } else {
            ?>
            <div class="data-content">
                <div class="data-column spl-name">You have not added domains yet.</div>
            </div>
            <?PHP
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>