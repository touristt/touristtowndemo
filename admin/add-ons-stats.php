<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
$regionLimit_id = '';
if (!in_array('customers', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
if (isset($_REQUEST['sortregion']) && $_REQUEST['sortregion'] != '-1' && $_REQUEST['sortregion'] > 0) {
    $regionLimit_id = $_POST['sortregion'];
    $sql = "SELECT R_ID, R_Parent FROM tbl_Region WHERE R_ID = $regionLimit_id LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $region = mysql_fetch_assoc($result);
    if ($region['R_Parent'] == 0) {
        $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($region['R_ID'], $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $first = true;
        $regionList .= "(";
        while ($row = mysql_fetch_assoc($result)) {
            if ($first) {
                $first = false;
            } else {
                $regionList .= ",";
            }
            $regionList .= $row['R_ID'];
        }
        $regionList .= ")";
    } else {
        $regionList = '(' . $region['R_ID'] . ')';
    }
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Listings - Add ons Report</div>
        <div class="link">
            <form id="region-filter" method="post">
                <label>
                    <span class="addlisting">Sort:</span>
                </label>  
                <select name="sortregion" id="sortregion" onChange="filter_region()">
                    <?php
                    $sortregion = isset($_REQUEST['sortregion']) ? $_REQUEST['sortregion'] : '';
                    $getRegions = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Domain != ''";
                    $resRegion = mysql_query($getRegions, $db) or die("Invalid query: $getRegions -- " . mysql_error());
                    while ($rowRegion = mysql_fetch_array($resRegion)) {
                        ?>
                        <option value="<?php echo $rowRegion['R_ID'] ?>" <?php echo ($sortregion == $rowRegion['R_ID']) ? 'selected="selected"' : ''; ?>><?php echo $rowRegion['R_Name'] ?></option>
                    <?php } ?>
                    <option value="-1" <?php echo ($sortregion > 0) ? '' : 'selected' ?>>All</option>
                </select>
            </form>
        </div>
    </div>
    <script>
        function filter_region()
        {
            $('#region-filter').submit();
        }
    </script>
    <div class="left">
        <?PHP require '../include/nav-managecustomers.php'; ?>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name-loc">Add ons Statistics</div>
        </div>
        <?php
        $sql_stats = "SELECT BLF_BL_ID,BFA_BL_ID FROM tbl_BL_Feature INNER JOIN tbl_Business_Feature_About ON BFA_BL_ID = BLF_BL_ID";
        if (isset($regionList)) {
            $sql_stats .= " LEFT JOIN tbl_Business_Listing ON BL_ID = BLF_BL_ID 
                            LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID
                            LEFT JOIN tbl_Business_Listing_Category_Region ON BLC_ID = BLCR_BLC_ID
                            WHERE BLCR_BLC_R_ID IN $regionList and BLF_F_ID = 1";
            $sql_stats .= " GROUP BY BFA_BL_ID";
        } else {
            $sql_stats .=" WHERE BLF_F_ID = 1 GROUP BY BFA_BL_ID ";
        }
        $result_stats = mysql_query($sql_stats);
        $count_stats = mysql_num_rows($result_stats);
        ?>

        <div class="data-content">
            <div class="data-column spl-name-loc">About us</div>
            <div class="data-column spl-other">
                <?php echo $count_stats ?>
            </div>
        </div>
        <?php
        $sql_stats = "SELECT BLF_BL_ID,BFP_BL_ID FROM tbl_BL_Feature 
                      INNER JOIN tbl_Business_Feature_Photo ON BFP_BL_ID = BLF_BL_ID";
        if (isset($regionList)) {
            $sql_stats .= " LEFT JOIN tbl_Business_Listing ON BL_ID = BLF_BL_ID 
                            LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID
                            LEFT JOIN tbl_Business_Listing_Category_Region ON BLC_ID = BLCR_BLC_ID
                            WHERE BLCR_BLC_R_ID IN $regionList and BLF_F_ID = 2";
            $sql_stats .= " GROUP BY BFP_BL_ID";
        } else {
            $sql_stats .=" WHERE BLF_F_ID = 3 GROUP BY BFP_BL_ID ";
        }
        $result_stats = mysql_query($sql_stats);
        $count_stats = mysql_num_rows($result_stats);
        ?>

        <div class="data-content">
            <div class="data-column spl-name-loc">Photo Gallery</div>
            <div class="data-column spl-other">
                <?php echo $count_stats ?>
            </div>
        </div>
        <?php
        $sql_stats = "SELECT BLF_BL_ID,BFP_BL_ID FROM tbl_BL_Feature 
                      INNER JOIN tbl_Business_Feature_Product ON BFP_BL_ID = BLF_BL_ID";
        if (isset($regionList)) {
            $sql_stats .= " LEFT JOIN tbl_Business_Listing ON BL_ID = BLF_BL_ID 
                            LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID
                            LEFT JOIN tbl_Business_Listing_Category_Region ON BLC_ID = BLCR_BLC_ID
                            WHERE BLCR_BLC_R_ID IN $regionList and BLF_F_ID = 3";
            $sql_stats .= " GROUP BY BFP_BL_ID";
        } else {
            $sql_stats .=" WHERE BLF_F_ID = 3 GROUP BY BFP_BL_ID ";
        }
        $result_stats = mysql_query($sql_stats);
        $count_stats = mysql_num_rows($result_stats);
        ?>

        <div class="data-content">
            <div class="data-column spl-name-loc">Our Products</div>
            <div class="data-column spl-other">
                <?php echo $count_stats ?>
            </div>
        </div>
        <?php
        $sql_stats = "SELECT BLF_BL_ID,BFM_BL_ID FROM tbl_BL_Feature 
                      INNER JOIN tbl_Business_Feature_Menu ON BFM_BL_ID = BLF_BL_ID";
        if (isset($regionList)) {
            $sql_stats .= " LEFT JOIN tbl_Business_Listing ON BL_ID = BLF_BL_ID 
                            LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID
                            LEFT JOIN tbl_Business_Listing_Category_Region ON BLC_ID = BLCR_BLC_ID
                            WHERE BLCR_BLC_R_ID IN $regionList and BLF_F_ID = 6";
            $sql_stats .= " GROUP BY BFM_BL_ID";
        } else {
            $sql_stats .=" WHERE BLF_F_ID = 6 GROUP BY BFM_BL_ID ";
        }
        $result_stats = mysql_query($sql_stats);
        $count_stats = mysql_num_rows($result_stats);
        ?>

        <div class="data-content">
            <div class="data-column spl-name-loc">Menu </div>
            <div class="data-column spl-other">
                <?php echo $count_stats ?>
            </div>
        </div>
        <?php
        $sql_stats = "SELECT BLF_BL_ID,BFDS_BL_ID FROM tbl_BL_Feature 
                      INNER JOIN tbl_Business_Feature_Daily_Specials ON BFDS_BL_ID = BLF_BL_ID";
        if (isset($regionList)) {
            $sql_stats .= " LEFT JOIN tbl_Business_Listing ON BL_ID = BLF_BL_ID 
                            LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID
                            LEFT JOIN tbl_Business_Listing_Category_Region ON BLC_ID = BLCR_BLC_ID
                            WHERE BLCR_BLC_R_ID IN $regionList and BLF_F_ID = 7";
            $sql_stats .= " GROUP BY BFDS_BL_ID";
        } else {
            $sql_stats .=" WHERE BLF_F_ID = 7 GROUP BY BFDS_BL_ID ";
        }
        $result_stats = mysql_query($sql_stats);
        $count_stats = mysql_num_rows($result_stats);
        ?>

        <div class="data-content">
            <div class="data-column spl-name-loc">Daily Special </div>
            <div class="data-column spl-other">
                <?php echo $count_stats ?>
            </div>
        </div>
        <?php
        $sql_stats = "SELECT BLF_BL_ID,BFEA_BL_ID FROM tbl_BL_Feature 
                      INNER JOIN tbl_Business_Feature_Entertainment_Acts ON BFEA_BL_ID = BLF_BL_ID";
        if (isset($regionList)) {
            $sql_stats .= " LEFT JOIN tbl_Business_Listing ON BL_ID = BLF_BL_ID 
                            LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID
                            LEFT JOIN tbl_Business_Listing_Category_Region ON BLC_ID = BLCR_BLC_ID
                            WHERE BLCR_BLC_R_ID IN $regionList and BLF_F_ID = 8";
            $sql_stats .= " GROUP BY BFEA_BL_ID";
        } else {
            $sql_stats .=" WHERE BLF_F_ID = 8 GROUP BY BFEA_BL_ID ";
        }
        $result_stats = mysql_query($sql_stats);
        $count_stats = mysql_num_rows($result_stats);
        ?>

        <div class="data-content">
            <div class="data-column spl-name-loc">Entertainment </div>
            <div class="data-column spl-other">
                <?php echo $count_stats ?>
            </div>
        </div>
        <?php
        $sql_stats = "SELECT BLF_BL_ID FROM tbl_BL_Feature ";
        if (isset($regionList)) {
            $sql_stats .= " LEFT JOIN tbl_Business_Listing ON BL_ID = BLF_BL_ID 
                            LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID
                            LEFT JOIN tbl_Business_Listing_Category_Region ON BLC_ID = BLCR_BLC_ID
                            WHERE BLCR_BLC_R_ID IN $regionList and BLF_F_ID = 11";
            $sql_stats .= " GROUP BY BLF_F_ID";
        } else {
            $sql_stats .=" WHERE BLF_F_ID = 11 GROUP BY BLF_F_ID ";
        }
        $result_stats = mysql_query($sql_stats);
        $count_stats = mysql_num_rows($result_stats);
        ?>

        <div class="data-content">
            <div class="data-column spl-name-loc">Power Ranking </div>
            <div class="data-column spl-other">
                <?php echo $count_stats ?>
            </div>
        </div>
        <?php
        $sql_stats = "SELECT BLF_BL_ID,BFGB_BL_ID FROM tbl_BL_Feature 
                      INNER JOIN tbl_Business_Feature_Guest_Book ON BFGB_BL_ID = BLF_BL_ID";
        if (isset($regionList)) {
            $sql_stats .= " LEFT JOIN tbl_Business_Listing ON BL_ID = BLF_BL_ID 
                            LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID
                            LEFT JOIN tbl_Business_Listing_Category_Region ON BLC_ID = BLCR_BLC_ID
                            WHERE BLCR_BLC_R_ID IN $regionList and BLF_F_ID = 12";
            $sql_stats .= " GROUP BY BFGB_BL_ID";
        } else {
            $sql_stats .=" WHERE BLF_F_ID = 12 GROUP BY BFGB_BL_ID ";
        }
        $result_stats = mysql_query($sql_stats);
        $count_stats = mysql_num_rows($result_stats);
        ?>

        <div class="data-content">
            <div class="data-column spl-name-loc">Guest Book </div>
            <div class="data-column spl-other">
                <?php echo $count_stats ?>
            </div>
        </div>
        <?php
        $sql_stats = "SELECT BLF_BL_ID,BFAM_BL_ID FROM tbl_BL_Feature 
                      INNER JOIN tbl_Business_Feature_Agendas_Minutes ON BFAM_BL_ID = BLF_BL_ID";
        if (isset($regionList)) {
            $sql_stats .= " LEFT JOIN tbl_Business_Listing ON BL_ID = BLF_BL_ID 
                            LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID
                            LEFT JOIN tbl_Business_Listing_Category_Region ON BLC_ID = BLCR_BLC_ID
                            WHERE BLCR_BLC_R_ID IN $regionList and BLF_F_ID = 13";
            $sql_stats .= " GROUP BY BFAM_BL_ID";
        } else {
            $sql_stats .=" WHERE BLF_F_ID = 13 GROUP BY BFAM_BL_ID ";
        }
        $result_stats = mysql_query($sql_stats);
        $count_stats = mysql_num_rows($result_stats);
        ?>

        <div class="data-content">
            <div class="data-column spl-name-loc">Agendas & Minutes</div>
            <div class="data-column spl-other">
                <?php echo $count_stats ?>
            </div>
        </div>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>