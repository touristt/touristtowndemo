<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';
if (!in_array('business-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
if (isset($_REQUEST['approved']) && $_REQUEST['approved'] > 0) {
    $BL_ID = $_REQUEST['approved'];
    $sql = "UPDATE tbl_Business_Listing SET BL_Free_Listing_status = 0 WHERE BL_ID = $BL_ID ";
    $result = mysql_query($sql);
    if ($result) {
        $_SESSION['success'] = 1;
    }
    header('location: inactivated_free_listings.php');
    exit();
}
if (isset($_REQUEST['email']) && $_REQUEST['email'] != '') {
    $sql = "SELECT B_ID, B_Email FROM tbl_Business WHERE B_Email = '" . encode_strings($_REQUEST['email'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowBUS = mysql_fetch_assoc($result);
    if (isset($rowBUS['B_ID'])) {
        $sql = "SELECT COUNT(BL_ID) FROM tbl_Business_Listing WHERE BL_B_ID = '" . encode_strings($rowBUS['B_ID'], $db) . "'";
        $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $rowListings = mysql_fetch_row($resultTMP);
    } else {
        $_SESSION['error'] = 1;
        header("Location: listings.php");
        exit();
    }
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Listings - Inactive Free Listings</div>
        <div class="link">
        </div>
    </div> 
    <div class="left"><?php require '../include/nav-managecustomers.php'; ?></div>
    <?php if (!isset($_REQUEST['email'])) { ?>
        <div class="right">
            <div class="content-sub-header">
                <div class="data-column spl-name-events padding-none">Email</div>
                <div class="data-column spl-other-cc-list padding-none">Approve</div>
                <div class="data-column spl-other-cc-list padding-none">Type</div>
                <div class="data-column spl-other-cc-list padding-none">Edit</div>
                <div class="data-column spl-other-cc-list padding-none">Delete</div>
            </div>
            <?PHP
            $sql = "SELECT BL_Listing_Title, BL_ID, BL_Listing_Type, B_ID FROM tbl_Business_Listing
                    LEFT JOIN tbl_Business ON BL_B_ID = B_ID";
            if (strlen(isset($_REQUEST['strSearch']) && $_REQUEST['strSearch']) > 3) {
                $where = " WHERE BL_Listing_Title LIKE '%" . encode_strings($_REQUEST['strSearch'], $db) . "%' AND BL_Listing_Type = 1 AND BL_Free_Listing_status = 1  AND hide_show_listing = 1 " . $LTname;
                $groupBy = " GROUP BY BL_ID";
                $orderBy = " ORDER BY BL_Listing_Title";
            } else if (strlen(isset($_REQUEST['strSearch_contact'])) > 3) {
                $where = " WHERE BL_Contact LIKE '%" . encode_strings($_REQUEST['strSearch_contact'], $db) . "%' AND BL_Listing_Type = 1 AND BL_Free_Listing_status = 1  AND hide_show_listing = 1 " . $LTname;
                $groupBy = " GROUP BY BL_ID";
                $orderBy = " ORDER BY BL_Listing_Title";
            } else {
                $where = " WHERE BL_Listing_Type = 1 AND BL_Free_Listing_status = 1 AND hide_show_listing = 1 ";
                $groupBy = " GROUP BY BL_ID";
                $orderBy = " ORDER BY BL_Listing_Title";
            }
            $sql .= $where . $groupBy . $orderBy;
            $result_free_listings = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $pages = new Paginate(mysql_num_rows($result_free_listings), 100);
            $result_free_listings = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row_free_listings = mysql_fetch_array($result_free_listings)) {
                ?>
                <div class="data-content">
                    <div class="data-column spl-name-events"><?php echo $row_free_listings['BL_Listing_Title'] ?></div>
                    <div class="data-column spl-other-cc-list"><a href="inactivated_free_listings.php?approved=<?php echo $row_free_listings['BL_ID'] ?>">Approve</a></div>
                    <div class="data-column spl-other-cc-list">
                        <?php
                        $sql_list_type = "SELECT * FROM tbl_Listing_Type ORDER BY LT_Order";
                        $result_list_type = mysql_query($sql_list_type, $db) or die("Invalid query: $sql_list_type -- " . mysql_error());
                        while ($row_list_type = mysql_fetch_assoc($result_list_type)) {
                            if ($row_free_listings['BL_Listing_Type'] == $row_list_type['LT_ID']) {
                                echo $row_list_type['LT_Name'];
                            }
                        }
                        ?>  
                    </div>
                    <div class="data-column spl-other-cc-list"><a href="customer-listings.php?id=<?php echo $row_free_listings['B_ID'] ?>">Edit</a></div>
                    <div class="data-column spl-other-cc-list"><a onclick="return confirm('Are you sure?\nThis action CANNOT be undone!');" href="free-listing-remove.php?del_id=<?php echo $row_free_listings['BL_ID'] ?>">Delete</a></div>
                </div>
            <?PHP } ?>
            <?php
            if (isset($pages)) {
                echo $pages->paginate();
            }
            ?>
        </div>
    <?php } else { ?>
        <div class="right">
            <div class="content-sub-header">
                <div class="data-column spl-name-events padding-none">Email</div>
                <div class="data-column spl-other-cc-list padding-none"></div>
                <div class="data-column spl-other-cc-list padding-none">#Listings</div>
                <div class="data-column spl-other-cc-list padding-none">Edit</div>
            </div>
            <div class="data-content">
                <div class="data-column spl-name-events"><a href="customer-listings.php?id=<?php echo $rowBUS['B_ID'] ?>"><?php echo $rowBUS['B_Email'] ?></a></div>
                <div class="data-column spl-other-cc-list"></div>
                <div class="data-column spl-other-cc-list"><?php echo $rowListings[0] ?></div>
                <div class="data-column spl-other-cc-list"><a href="customer-listings.php?id=<?php echo $rowBUS['B_ID'] ?>">Edit</a></div>
            </div>
        </div>
    <?php } ?>
</div>

<?PHP
require_once '../include/admin/footer.php';
?>