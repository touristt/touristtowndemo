<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

$limits = explode(',', $_SESSION['USER_LIMIT']);
if (!in_array('regions', $_SESSION['USER_PERMISSIONS']) && (!in_array('manage-county-region', $_SESSION['USER_PERMISSIONS']) || !in_array($_REQUEST['rid'], $limits))) {
    header("Location: /admin/");
    exit();
}

if (isset($_REQUEST['rid']) && $_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} else {
    header("Location: /admin/regions.php");
    exit();
}

$sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);

$sql = "SELECT * FROM tbl_Main_Navigation WHERE MN_R_ID = '" . encode_strings($regionID, $db) . "' AND MN_Static_Links NOT IN (1,2)";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$countNav = mysql_num_rows($result);
$sql = "SELECT C_ID, RC_C_ID, RC_R_ID, RC_Order FROM tbl_Category  
        LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID 
        WHERE C_Parent = '0' AND RC_R_ID = " . $regionID . " ORDER BY RC_Order";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$countMC = mysql_num_rows($result);
if ($countMC != 0 && $countNav == 0) {
    //Insert all categories with maps and routes links here
    while ($insertNav = mysql_fetch_assoc($result)) {
        $sqlInsertDynamic = "INSERT tbl_Main_Navigation SET MN_R_ID = '" . $insertNav['RC_R_ID'] . "', MN_C_ID = '" . $insertNav['RC_C_ID'] . "', 
                             MN_Order = '" . $insertNav['RC_Order'] . "'";
        mysql_query($sqlInsertDynamic) or die(mysql_error());
    }
}

// 1 = Maps
$sql = "SELECT * FROM tbl_Main_Navigation WHERE MN_R_ID = '" . encode_strings($regionID, $db) . "' AND MN_Static_Links = 1 ";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$countNav = mysql_num_rows($result);
if ($countNav == 0) {
    $sqlMax = "SELECT MAX(MN_Order) FROM tbl_Main_Navigation WHERE MN_R_ID = '" . $regionID . "'";
    $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
    $rowMax = mysql_fetch_row($resultMax);
    $sqlInsertStatic = "INSERT tbl_Main_Navigation SET MN_R_ID = '" . $regionID . "', MN_Static_Links = 1, 
                        MN_Order = '" . ($rowMax[0] + 1) . "'";
    mysql_query($sqlInsertStatic) or die(mysql_error());
}

// 2 = Routes
$sql = "SELECT * FROM tbl_Main_Navigation WHERE MN_R_ID = '" . encode_strings($regionID, $db) . "' AND MN_Static_Links = 2 ";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$countNav = mysql_num_rows($result);
if ($countNav == 0) {
    $sqlMax = "SELECT MAX(MN_Order) FROM tbl_Main_Navigation WHERE MN_R_ID = '" . $regionID . "'";
    $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
    $rowMax = mysql_fetch_row($resultMax);
    $sqlInsertStatic = "INSERT tbl_Main_Navigation SET MN_R_ID = '" . $regionID . "', MN_Static_Links = 2, 
                        MN_Order = '" . ($rowMax[0] + 1) . "'";
    mysql_query($sqlInsertStatic) or die(mysql_error());
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Manage Main Navigation</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">
            Main Navigation
        </div>

        <div class="reorder-main-nav">
            <?PHP
            $sql = "SELECT MN_ID, RC_Name, C_Name, MN_Static_Links FROM tbl_Main_Navigation
                    LEFT JOIN tbl_Region_Category ON RC_C_ID = MN_C_ID 
                    LEFT JOIN tbl_Category ON C_ID = MN_C_ID 
                    WHERE MN_R_ID = " . $regionID . " GROUP BY MN_ID ORDER BY MN_Order";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $counter = 0;
            while ($row = mysql_fetch_assoc($result)) {
                $counter++;
                ?>
                <div class="data-content" id="recordsArray_<?php echo $row['MN_ID'] ?>">
                    <div class="data-column spl-org-listngs">
                        <div class="select-listings-nearby-title check11">
                            <?php
                            if ($row['MN_Static_Links'] > 0) {
                                if ($row['MN_Static_Links'] == 1) {
                                    echo 'Maps';
                                } else {
                                    echo 'Routes';
                                }
                            } else {
                                echo $row['RC_Name'] == '' ? $row['C_Name'] : $row['RC_Name'];
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?PHP
            }
            ?>
        </div>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script>
    $(function () {
        $(".reorder-main-nav").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Main_Navigation&field=MN_Order&id=MN_ID';
                $.post("reorder.php", order, function (theResponse) {
                    swal("Navigation Re-Ordered", "Navigation Re-Ordered successfully.", "success");
                });
            }
        });
    });
</script>