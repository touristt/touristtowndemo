<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';
require_once '../include/track-data-entry.php';
if (!in_array('manage-events', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
if (!in_array('superadmin', $_SESSION['USER_ROLES']) && !in_array('admin', $_SESSION['USER_ROLES'])) {
    if (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
        $regionLimit = array();
        $limits = explode(',', $_SESSION['USER_LIMIT']);
        foreach ($limits as $limit) {
            $sql = "SELECT R_ID, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $region = mysql_fetch_assoc($result);
            if (isset($region['R_Type']) && $region['R_Type'] == 1) {
                $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($region['R_ID'], $db) . "'";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                while ($row = mysql_fetch_assoc($result)) {
                    if ($row['R_ID'] != '' && !in_array($row['R_ID'], $regionLimit)) {
                        $regionLimit[] = $row['R_ID'];
                    }
                }
            } else {
                if ($region['R_ID'] != '' && !in_array($region['R_ID'], $regionLimit)) {
                    $regionLimit[] = $region['R_ID'];
                }
            }
        }
    }
}

if (isset($_POST['op']) && $_POST['op'] == 'Del') {
    $delete_events = $_POST['expired_event'];
    $event_list = implode(",", $delete_events);
    foreach ($delete_events as $key1 => $value1) {
        $sql = "Delete from Events_master WHERE EventID = '" . encode_strings($value1, $db) . "'";
        $result = mysql_query($sql, $db);
    }
    // TRACK DATA ENTRY
    Track_Data_Entry('Event', $event_list, 'Expired Events', '', 'Delete', 'super admin');
    if ($result) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: events-delete.php");
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Events - Expired Events</div>
        <div class="link">
            <form class="export_form_width" id="frmSort" name="form1" method="GET" action="">
                <div class="accounting-dd">
                    <input class="datepicker event-search-date-input" name="startdate" type="text" placeholder="Start date" id="startdate" value="<?php echo (isset($_REQUEST['startdate'])) ? $_REQUEST['startdate'] : ''; ?>" size="50" />
                </div>
                <div class="accounting-dd">
                    <input class="datepicker event-search-date-input" name="enddate" type="text" id="enddate" placeholder="End date" value="<?php echo (isset($_REQUEST['enddate'])) ? $_REQUEST['enddate'] : ''; ?>" size="50" />
                </div>
                <div class="accounting-dd">
                    <select name="sortby" id="sortby" >
                        <option selected>Sort by:</option>
                        <option value="oldest" <?php echo (isset($_REQUEST['sortby']) && $_REQUEST['sortby'] == "oldest") ? 'selected' : ''; ?>>Date Oldest First</option>
                        <option value="newest" <?php echo (isset($_REQUEST['sortby']) && $_REQUEST['sortby'] == "newest") ? 'selected' : ''; ?>>Date Newest First</option>
                    </select>
                </div>
                <?php
                if (in_array('superadmin', $_SESSION['USER_ROLES']) || in_array('admin', $_SESSION['USER_ROLES'])) {
                    ?>
                    <div class="accounting-dd">
                        <select name="sortbyregion" id="sortbyregion" >

                            <option selected>Select Region:</option>
                            <?PHP
                            $sqlCom = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Parent != 0 ORDER BY R_Name";
                            $resultRegionList = mysql_query($sqlCom, $db) or die("Invalid query: $sqlCom -- " . mysql_error());
                            $region_list = explode(',', $row['E_Region_ID']);
                            while ($rowRList = mysql_fetch_assoc($resultRegionList)) {
                                ?>
                                <option value="<?php echo $rowRList['R_ID'] ?>" <?php echo (isset($_REQUEST['sortbyregion']) && $_REQUEST['sortbyregion'] == $rowRList['R_ID']) ? 'selected' : ''; ?>><?php echo $rowRList['R_Name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                <?php } ?>
                <div class="accounting-dd">
                    <input type="submit" name="accounting_btn" value="Filter"/>
                </div>
                <select name="sortbySub" id="sortbySub" onChange="$('#frmSort').submit();" style="display:none;">

                    <option selected>Sort by:</option>

                </select>
                <script type="text/javascript">
                    function getSubOptions(myVal) {
                        var subSort = new Array();
<?php echo $js ?>
                        if (myVal == 'upcoming') {
                            $('#frmSort').submit();
                        } else {
                            $('#sortbySub').html(subSort[myVal]);
                            $('#sortbySub').show();
                        }
                    }

                </script>
            </form>
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manageevents.php'; ?>
    </div>
    <div class="right">
        <div class="content-sub-header margin-left-delete-event">
            <div class="data-column padding-none spl-name-events">Name</div>
            <div class="data-column padding-none spl-other-cc-list-community-expired">Community</div>
            <div class="data-column padding-none spl-other-cc-list">Start</div>
            <div class="data-column padding-none spl-other-cc-list">End</div>
        </div>
        <?PHP
        $sql = "SELECT DISTINCT EventID, Title, EventDateStart, EventDateEnd FROM Events_master WHERE Pending = 2 AND EventDateEnd < CURDATE() ";
        if (strlen($_REQUEST['strSearch']) > 3) {
            $sql .= " AND Title LIKE '%" . encode_strings($_REQUEST['strSearch'], $db) . "%' ";
        }
        if (is_numeric($_REQUEST['sortby']) && $_REQUEST['sortby'] > 0) {
            $sql .= " AND EventType = '" . encode_strings($_REQUEST['sortby'], $db) . "' ";
        }
        if ((isset($_REQUEST['startdate']) && $_REQUEST['startdate'] != '') && (isset($_REQUEST['enddate']) && $_REQUEST['enddate'] == '')) {
            $sql .= " AND EventDateStart = '" . encode_strings($_REQUEST['startdate'], $db) . "' ";
        }
        if ((isset($_REQUEST['startdate']) && $_REQUEST['startdate'] == '') && (isset($_REQUEST['enddate']) && $_REQUEST['enddate'] != '')) {
            $sql .= " AND EventDateEnd = '" . encode_strings($_REQUEST['enddate'], $db) . "' ";
        }
        if (isset($_REQUEST['startdate']) && $_REQUEST['startdate'] != '' && isset($_REQUEST['enddate']) && $_REQUEST['enddate'] != '') {
            $sql .= " AND (EventDateStart BETWEEN '" . encode_strings($_REQUEST['startdate'], $db) . "' AND   '" . encode_strings($_REQUEST['enddate'], $db) . "' ";
            $sql .= " OR EventDateEnd   BETWEEN '" . encode_strings($_REQUEST['startdate'], $db) . "' AND  '" . encode_strings($_REQUEST['enddate'], $db) . "') ";
        }
        if (isset($_REQUEST['sortbyregion']) && $_REQUEST['sortbyregion'] > 0) {
            $sql .= " AND FIND_IN_SET (" . encode_strings($_REQUEST['sortbyregion'], $db) . ",E_Region_ID) ";
        }
        if (!in_array('superadmin', $_SESSION['USER_ROLES']) && !in_array('admin', $_SESSION['USER_ROLES'])) {
            $first = true;
            $sql .= " And (";
            foreach ($regionLimit as $e_region_id) {
                if ($first) {
                    $first = false;
                } else {
                    $sql .= " OR ";
                }
                $sql .= "FIND_IN_SET (" . $e_region_id . ",E_Region_ID )";
            }
            $sql .= " )";
        }
        if (isset($_REQUEST['sortby']) && $_REQUEST['sortby'] == 'oldest') {
            $sql .= "ORDER BY EventDateStart ASC";
        } else if (isset($_REQUEST['sortby']) && $_REQUEST['sortby'] == 'newest') {
            $sql .= "ORDER BY EventDateStart DESC";
        } else {
            $sql .= "ORDER BY EventDateStart DESC";
        }
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $count_event = mysql_num_rows($result);
        $pages = new Paginate(mysql_num_rows($result), 100);
        $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
        if ($count_event > 0) {
            ?>
            <div >
                <form method="post" action="" id="event_delete_form" >
                    <input type="checkbox" id="pc-box" class="check-box-event"  value="" > 
                    <input type="hidden" name="op" id="form-action-delete"  value="" > 
                    <div class="data-column spl-name-events">  
                        Select All
                    </div>
                    <div class="data-column spl-other-cc-list">
                    </div>
                    <div class="data-column spl-other-cc-list">
                    </div>  
                    <div class="data-column spl-other-cc-list">
                        <a class="category-pointer" onclick="event_delete_submit()">
                            Delete
                        </a>
                    </div> 

                    <?php
                    $i = 1;
                    while ($row = mysql_fetch_assoc($result)) {
                        ?>
                        <div class="data-content <?php echo ($i == 1) ? 'border-top-event' : '' ?>">
                            <?php
                            ?>
                            <input type="checkbox" class="cc-box check-box-event" name="expired_event[]" value="<?php echo $row['EventID'] ?>" >
                            <div class="data-column spl-name-events">  
                                <a href="event.php?id=<?php echo $row['EventID'] ?>">     
                                    <?php echo $row['Title'] ?>
                                </a></div>

                            <div  class="data-column spl-other-cc-list-community-expired">
                                <?php
                                $query = mysql_query("SELECT * FROM Events_master WHERE EventID = '" . $row['EventID'] . "'");
                                $row_check = mysql_fetch_assoc($query);
                                $regions = explode(',', $row_check['E_Region_ID']);
                                $counts = count($regions);
                                $i = 1;
                                foreach ($regions as $key => $region) {
                                    $query = mysql_query("SELECT R_Name FROM tbl_Region WHERE R_ID = '" . $region . "'");
                                    $rows = mysql_fetch_assoc($query);
                                    if ($i !== $counts) {
                                        echo $rows['R_Name'] . ', ';
                                    } else {
                                        echo $rows['R_Name'];
                                    }
                                    $i++;
                                }
                                ?>
                            </div>

                            <div class="data-column spl-other-cc-list">

                                <?php echo $row['EventDateStart'] ?>
                            </div>
                            <div class="data-column spl-other-cc-list">

                                <?php echo $row['EventDateEnd'] == '0000-00-00' ? '' : $row['EventDateEnd'] ?>
                            </div>
                        </div>
                        <?PHP
                        $i++;
                    }
                    if (isset($_REQUEST['startdate']) != '' || isset($_REQUEST['enddate']) != '') {
                        ?>
                    </form>
                <?php } ?>
            </div>   
            <?php
        }
        if (isset($pages)) {
            echo $pages->paginate();
        }
        ?>
    </div>
</div>
<script>

    $(document).ready(function () {
        $("#pc-box").click(function () {
            if (this.checked) {
                $(this).closest("div").find(".cc-box").prop("checked", true);
            }
            if (!this.checked) {
                $(this).closest("div").find(".cc-box").prop("checked", false);
                $(this).closest("div").prev().find("#pc-box").prop("checked", false);
            }
        });
        $(".cc-box").click(function () {
            if (!this.checked)
                $(this).closest("div").prev().find("#pc-box").prop("checked", false);
        });
        $("#startdate").change(function () {
            var starting_date = $('#startdate').val();
            var ending_date = $('#enddate').val();
            if (ending_date != '') {
                if (ending_date < starting_date)
                {
                    alert("End Date must be greater than start date");
                    $('#enddate').val("");
                }
            }
        });
        $("#enddate").change(function () {
            var starting_date = $('#startdate').val();
            var ending_date = $('#enddate').val();
            if (ending_date < starting_date)
            {
                alert("End Date must be greater than start date");
                $('#enddate').val("");
            }
        });
    });
    function event_delete_submit()
    {
        if ($('#pc-box').is(':checked'))
        {
            var checkedcount = $(":checkbox:checked").length - 1;
        } else
        {
            var checkedcount = $(":checkbox:checked").length;
        }
        if (confirm(" Are you sure you want to permanently delete " + checkedcount + " events?")) {
            $("#form-action-delete").val('Del');
            $("#event_delete_form").submit();
        }
        return false;
    }
</script>
<?PHP
require_once '../include/admin/footer.php';
?>