<?php

require_once '../include/config.inc.php';
require_once '../include/adminFunctions.inc.php';
//require_once '../include/class.Pagination.php';
if (isset($_REQUEST['export_stats'])) {
// Download the file
    $filename = "Business Listings.csv";
    header('Content-type: application/csv');
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment;filename=' . $filename);
    header("Pragma: no-cache");
    header("Expires: 0");
    echo "\xEF\xBB\xBF";
    // Filter customers by region
    // Filter customers by region
    $temp = false;
    $regionJoin = '';
    $regionWhere = '';
    $region_limit = '';
    $LTname = '';
    $LTjoin = '';
    $regionLimit = array();

    $regionWhere .= " AND BL_Listing_Type IN (";
    if (in_array('free-listings', $_SESSION['USER_PERMISSIONS'])) {
        $regionWhere .= "1";
        $temp = true;
    }
    if (in_array('business-listings', $_SESSION['USER_PERMISSIONS'])) {
        if ($temp == true) {
            $regionWhere .= ", ";
        }
        $regionWhere .= "4";
        $temp = true;
    }
    if (in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) {
        if ($temp == true) {
            $regionWhere .= ", ";
        }
        $regionWhere .= "5";
    }
    $regionWhere .= ")";

    if (isset($_REQUEST['type']) && $_REQUEST['type'] != '') {
        $LTname = " AND LT_Name = '" . encode_strings($_REQUEST['type'], $db) . "'";
        $LTjoin = " LEFT JOIN tbl_Listing_Type ON BL_Listing_Type = LT_ID";
    }
    if ((isset($_REQUEST['sortregion']) && $_REQUEST['sortregion'] != '' && $_REQUEST['sortregion'] != '-1') || $_SESSION['USER_LIMIT'] > 0) {
        if (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
            $limits = explode(',', $_SESSION['USER_LIMIT']);
        } else {
            $limits = explode(',', $_REQUEST['sortregion']);
        }
        foreach ($limits as $limit) {
            $sql = "SELECT R_ID, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $region = mysql_fetch_assoc($result);
            if (isset($region['R_Type']) && $region['R_Type'] == 1) {
                $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($region['R_ID'], $db) . "'";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                while ($row = mysql_fetch_assoc($result)) {
                    if ($row['R_ID'] != '' && !in_array($row['R_ID'], $regionLimit)) {
                        $regionLimit[] = $row['R_ID'];
                    }
                }
            } else {
                if ($region['R_ID'] != '' && !in_array($region['R_ID'], $regionLimit)) {
                    $regionLimit[] = $region['R_ID'];
                }
            }
        }
        $regionLimitCommaSeparated = rtrim(implode(',', $regionLimit), ',');
        $regionJoin = " LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                INNER JOIN tbl_Business_Listing_Category_Region ON BL_ID = BLCR_BL_ID";
        $regionWhere .= " AND BLCR_BLC_R_ID IN (" . encode_strings($regionLimitCommaSeparated, $db) . ")";
        if (in_array('bgadmin', $_SESSION['USER_ROLES'])) {
            if (isset($_REQUEST['type']) && $_REQUEST['type'] != 'Free') {
                $regionWhere .= " AND BL_ChamberMember > 0";
            }
            $regionWhere .= " AND BL_Listing_Type IN (1, 4)";
        }
    }
    $output = "";
    $output .= "Listings";
    $output .= "\n";
    $sql = "SELECT DISTINCT BL_ID, BL_Listing_Title, BL_Photo, BL_Listing_Type, BL_Points, DATEDIFF(BL_Renewal_Date, CURDATE()) as timeRemaining 
            FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID
            LEFT JOIN tbl_Business_Listing_Photo ON BLP_BL_ID = BL_ID
            ";
    if (strlen($_REQUEST['strSearch']) > 3) {
        $where = " WHERE BL_Listing_Title LIKE '%" . encode_strings($_REQUEST['strSearch'], $db) . "%'  AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) " . $LTname;
        $groupBy = " GROUP BY BL_ID";
        $orderBy = " ORDER BY TRIM(BL_Listing_Title)";
    } else if (strlen($_REQUEST['strSearch_contact']) > 3) {
        $where = " WHERE BL_Contact LIKE '%" . encode_strings($_REQUEST['strSearch_contact'], $db) . "%' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) " . $LTname;
        $groupBy = " GROUP BY BL_ID";
        $orderBy = " ORDER BY TRIM(BL_Listing_Title)";
    } elseif ($_REQUEST['sortby'] == 'upcoming') {
        $where = " WHERE DATEDIFF(B_Renewal_Date, CURDATE()) < 90 AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) " . $LTname;
        $groupBy = " GROUP BY BL_ID";
        $orderBy = " ORDER BY timeRemaining DESC";
    } elseif ($_REQUEST['sortby'] == 'points') {
        if (isset($_REQUEST['type']) && $_REQUEST['type'] != '') {
            $where = " WHERE LT_Name = '" . encode_strings($_REQUEST['type'], $db) . "' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY))";
        } else {
            $where = " WHERE (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = 1 ";
        }
        $groupBy = " GROUP BY BL_ID";
        $orderBy = " ORDER BY BL_Points DESC";
    } elseif ($_REQUEST['sortby'] == 'noncoc') {
        $where = " WHERE BL_ChamberMember != 1 AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) " . $LTname;
        $groupBy = " GROUP BY BL_ID";
        $orderBy = " ORDER BY BL_Points DESC";
    } elseif (is_numeric($_REQUEST['sortby']) && $_REQUEST['sortby'] > 0) {
        if ($_REQUEST['sortbySub'] == -1 || $_REQUEST['sortbySub'] == '') {
            if ($regionJoin == '') {
                $regionJoin = " LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID";
            }
            $where = " WHERE BLC_M_C_ID = '" . encode_strings($_REQUEST['sortby'], $db) . "'  AND BL_Free_Listing_status = 0" . $LTname;
            $groupBy = " GROUP BY BL_ID";
            $orderBy = " ORDER BY TRIM(BL_Listing_Title)";
        } else {
            //check if there is already region selected to avoid multiple times joinging with tbl_Business_Listing_Category
            if ($regionJoin == '') {
                $regionJoin = " LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID";
            }
            $where = " WHERE BLC_C_ID = '" . encode_strings($_REQUEST['sortbySub'], $db) . "' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) " . $LTname;
            $groupBy = " GROUP BY BL_ID";
            $orderBy = " ORDER BY TRIM(BL_Listing_Title)";
        }
    } else {
        if (isset($_REQUEST['type']) && $_REQUEST['type'] != '') {
            $where = " WHERE LT_Name = '" . encode_strings($_REQUEST['type'], $db) . "' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY))";
        } else {
            $where = " WHERE (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY))";
        }
        $groupBy = " GROUP BY BL_ID";
        $orderBy = " ORDER BY TRIM(BL_Listing_Title)";
    }
    // For listings with no main images
    if (isset($_REQUEST['filter_listings']) && $_REQUEST['filter_listings'] == 'no_image') {
        $where .= " AND BLP_Photo = ''  AND hide_show_listing =1 ";
    }
    // For listings with no thumbnail images
    if (isset($_REQUEST['filter_listings']) && $_REQUEST['filter_listings'] == 'no_image_thumbnail') {
        $where .= " AND  BLP_Header_Image = '' AND hide_show_listing =1 ";
    }
    // For listings with no map details
    if (isset($_REQUEST['filter_listings']) && $_REQUEST['filter_listings'] == 'no_map') {
        $where .= " AND (BL_Lat = '' OR BL_Long = '') AND hide_show_listing =1";
    }
    // For listings with no map details
    if (isset($_REQUEST['filter_listings']) && $_REQUEST['filter_listings'] == 'zero_billing') {
        $where .= " AND BL_Total = '0.00'";
    }
    // For listings with no map details
    if (isset($_REQUEST['filter_listings']) && $_REQUEST['filter_listings'] == 'annual_billing') {
        $where .= " AND BL_Billing_Type = -1";
    }
    // For listings Inactive
    if (isset($_REQUEST['filter_listings']) && $_REQUEST['filter_listings'] == "inactive_listings") {
        $where .= " AND hide_show_listing = 0";
    } else {
        if ($_REQUEST['strSearch'] == '' && $_REQUEST['strSearch_contact'] == '') {
            $where .= " AND hide_show_listing = 1";
        }
    }
    if (isset($_REQUEST['map'])) {
        $where .= " AND (BL_Lat = '' OR BL_Long = '')";
    }
    $sql .= $regionJoin . $LTjoin . $where . $regionWhere . $groupBy . $orderBy;
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());

    while ($rowAT = mysql_fetch_assoc($result)) {
        $count++;
        $output .= '"' . str_replace('"', '""', $rowAT['BL_Listing_Title']) . '"';
        $output .= "\n";
    }
    echo $output;
}
?>



