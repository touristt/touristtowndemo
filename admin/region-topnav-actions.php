<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['id'] > 0) {
    $sql = "SELECT R_ID, R_Name, R_Type, RTL_ID, RTL_Title, RTL_Link FROM tbl_Region_Topnav_Link LEFT JOIN tbl_Region ON RTL_R_ID = R_ID 
            WHERE RTL_ID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
} elseif ($_REQUEST['rid'] > 0) {
    $sql = "SELECT R_ID, R_Name R_Type, FROM tbl_Region WHERE R_ID = '" . encode_strings($_REQUEST['rid'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
}
$regionID = $activeRegion['R_ID'];

if ($_POST['op'] == 'save') {

    $sql = "tbl_Region_Topnav_Link SET 
            RTL_Title = '" . encode_strings($_REQUEST['title'], $db) . "',
            RTL_Link = '" . encode_strings($_REQUEST['link'], $db) . "'";

    if ($_REQUEST['id'] > 0) {
        $sql = "UPDATE " . $sql . " WHERE RTL_ID = '" . encode_strings($activeRegion['RTL_ID'], $db) . "'";
    } else {
        $sql = "INSERT " . $sql . ", RTL_R_ID = '" . encode_strings($regionID, $db) . "'";
    }

    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: region-topnav.php?rid=" . $regionID);
    exit();
}  elseif ($_GET['op'] == 'del') {
    $sql = "DELETE tbl_Region_Topnav_Link
            FROM tbl_Region_Topnav_Link  
            WHERE RTL_ID = '" . $activeRegion['RTL_ID'] . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: region-topnav.php?rid=" . $regionID);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Top Navigation</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP
        if ($regionID > 0) {
            require '../include/nav-manage-region.php';
        } else {
            echo "&nbsp;";
        }
        ?>
    </div>
    <div class="right">
        <div class="content-header">Add Top Navigation Link</div>
        <form action="/admin/region-topnav-actions.php" method="post" name="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <input type="hidden" name="id" value="<?php echo($activeRegion['RTL_ID'] > 0) ? $activeRegion['RTL_ID'] : '0'; ?>">
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Title</label>
                <div class="form-data">
                    <input type="text" name="title" id="text_colormain_menu_font" value="<?php echo ((isset($activeRegion['RTL_Title'])) ? $activeRegion['RTL_Title'] : '') ?>">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Link</label>
                <div class="form-data">
                    <input type="text" name="link" id="text_colormain_menu_font" value="<?php echo ((isset($activeRegion['RTL_Link'])) ? $activeRegion['RTL_Link'] : '') ?>">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="Save" id="button22" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>