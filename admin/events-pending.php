<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';
require_once '../include/track-data-entry.php';
if (!in_array('manage-events', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
if (!in_array('superadmin', $_SESSION['USER_ROLES']) && !in_array('admin', $_SESSION['USER_ROLES'])) {
    if (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
        $regionLimit = array();
        $limits = explode(',', $_SESSION['USER_LIMIT']);
        foreach ($limits as $limit) {
            $sql = "SELECT R_ID, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $region = mysql_fetch_assoc($result);
            if (isset($region['R_Type']) && $region['R_Type'] == 1) {
                $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($region['R_ID'], $db) . "'";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                while ($row = mysql_fetch_assoc($result)) {
                    if ($row['R_ID'] != '' && !in_array($row['R_ID'], $regionLimit)) {
                        $regionLimit[] = $row['R_ID'];
                    }
                }
            } else {
                if ($region['R_ID'] != '' && !in_array($region['R_ID'], $regionLimit)) {
                    $regionLimit[] = $region['R_ID'];
                }
            }
        }
    }
}
if (isset($_POST['saveVal']) && $_POST['saveVal'] !== '') {
    $check_val = $_POST['checkbox'];
    $event_list = implode(",", $check_val);
    foreach ($check_val as $key1 => $value1) {
        $sql = "UPDATE Events_master SET Pending = 0 WHERE EventID = '" . encode_strings($value1, $db) . "'";  // exit;
        $res1 = mysql_query($sql, $db);
        if ($res1) {
            $_SESSION['success'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
    }
    // TRACK DATA ENTRY
    Track_Data_Entry('Event', $event_list, 'Pending Events', '', 'Approve', 'super admin');
    header("Location: events-pending.php");
    exit();
}
if (isset($_POST['deleteVal']) && $_POST['deleteVal'] !== '') {
    $check_val = $_POST['checkbox'];
    $event_list = implode(",", $check_val);
    foreach ($check_val as $key1 => $value1) {
        $sql = "Delete from Events_master WHERE EventID = '" . encode_strings($value1, $db) . "'";
        $res1 = mysql_query($sql, $db);
        if ($res1) {
            $_SESSION['delete'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
    }
    // TRACK DATA ENTRY
    Track_Data_Entry('Event', $event_list, 'Pending Events', '', 'Delete', 'super admin');
    header("Location: events-pending.php");
    exit();
}
if (isset($_GET['op']) && $_GET['op'] == 'save') {
    if (isset($_GET['approve']) && $_GET['approve'] == '1') {
        $sql = "UPDATE Events_master SET Pending = 0 WHERE EventID = '" . encode_strings($_REQUEST['id'], $db) . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $_REQUEST['id'];
        Track_Data_Entry('Event', $id, 'Pending Events', '', 'Approve', 'super admin');
        if ($result) {
            $_SESSION['success'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
    } else if (isset($_GET['delete']) && $_GET['delete'] == '1') {
        $sql = "UPDATE Events_master SET Pending = 2 WHERE EventID = '" . encode_strings($_REQUEST['id'], $db) . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $_REQUEST['id'];
        Track_Data_Entry('Event', $id, 'Pending Events', '', 'Delete', 'super admin');
        if ($result) {
            $_SESSION['delete'] = 1;
        } else {
            $_SESSION['delete_error'] = 1;
        }
    }
    header("Location: events-pending.php");
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Events - Pending Events</div>
        <div class="link">
            <form class="export_form_width" id="frmSort" name="form1" method="GET" action="">
                <div class="accounting-dd">
                    <input class="datepicker event-search-date-input" name="startdate" type="text" placeholder="Start date" id="startdate" value="<?php
                    if (isset($_REQUEST['startdate'])) {
                        echo $_REQUEST['startdate'];
                    }
                    ?>" size="50" />
                </div>
                <div class="accounting-dd">
                    <input class="datepicker event-search-date-input" name="enddate" type="text" id="enddate" placeholder="End date" value="<?php
                    if (isset($_REQUEST['enddate'])) {
                        echo $_REQUEST['enddate'];
                    }
                    ?>" size="50" />
                </div>
                <div class="accounting-dd">
                    <select name="sortby" id="sortby" >
                        <option selected>Sort by:</option>
                        <option value="oldest" <?php echo (isset($_REQUEST['sortby']) && $_REQUEST['sortby'] == "oldest") ? 'selected' : ''; ?>>Date Oldest First</option>
                        <option value="newest" <?php echo (isset($_REQUEST['sortby']) && $_REQUEST['sortby'] == "newest") ? 'selected' : ''; ?>>Date Newest First</option>
                    </select>
                </div>
                <?php
                if (in_array('superadmin', $_SESSION['USER_ROLES']) || in_array('admin', $_SESSION['USER_ROLES'])) {
                    ?>
                    <div class="accounting-dd">
                        <select name="sortbyregion" id="sortbyregion" >
                            <option selected>Select Region:</option>
                            <?PHP
                            $sqlCom = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Parent != 0 ORDER BY R_Name";
                            $resultRegionList = mysql_query($sqlCom, $db) or die("Invalid query: $sqlCom -- " . mysql_error());
                            $region_list = explode(',', $row['E_Region_ID']);
                            while ($rowRList = mysql_fetch_assoc($resultRegionList)) {
                                ?>
                                <option value="<?php echo $rowRList['R_ID'] ?>" <?php echo (isset($_REQUEST['sortbyregion']) && $_REQUEST['sortbyregion'] == $rowRList['R_ID']) ? 'selected' : ''; ?>><?php echo $rowRList['R_Name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                <?php }
                ?>
                <div class="accounting-dd">
                    <input type="submit" name="accounting_btn" value="Filter"/>
                </div>
                <select name="sortbySub" id="sortbySub" onChange="$('#frmSort').submit();" style="display:none;">
                    <option selected>Sort by:</option>
                </select>
                <script type="text/javascript">
                    function getSubOptions(myVal) {
                        var subSort = new Array();
<?php echo $js ?>
                        if (myVal == 'upcoming') {
                            $('#frmSort').submit();
                        } else {
                            $('#sortbySub').html(subSort[myVal]);
                            $('#sortbySub').show();
                        }
                    }

                </script>
            </form>
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manageevents.php'; ?>
    </div>
    <form method="post" action="">
        <div class="right">
            <div class="content-sub-header">
                <div class="data-column padding-none spl-name-events-pending">Name</div>
                <div class="data-column padding-none spl-other-cc-list-community">Community</div>
                <div class="data-column padding-none spl-other-stories">Approve</div>
                <div class="data-column padding-none spl-other-stories">Delete</div>
                <div class="data-column padding-none spl-other-stories-pending-event-checkboxes"><input type="checkbox" id="checkall" />Select All</div>
            </div>
            <?PHP
            $sql = "SELECT DISTINCT EventID, Title, EventDateStart, EventDateEnd  FROM Events_master WHERE Pending = 1 AND EventDateEnd >= CURDATE() ";
            if (strlen($_REQUEST['strSearch']) > 3) {
                $sql .= " AND Title LIKE '%" . encode_strings($_REQUEST['strSearch'], $db) . "%' ";
            }
            if (is_numeric($_REQUEST['sortby']) && $_REQUEST['sortby'] > 0) {
                $sql .= " AND EventType = '" . encode_strings($_REQUEST['sortby'], $db) . "' ";
            }
            if ((isset($_REQUEST['startdate']) && $_REQUEST['startdate'] != '') && (isset($_REQUEST['enddate']) && $_REQUEST['enddate'] == '')) {
                $sql .= " AND EventDateStart = '" . encode_strings($_REQUEST['startdate'], $db) . "' ";
            }
            if ((isset($_REQUEST['startdate']) && $_REQUEST['startdate'] == '') && (isset($_REQUEST['enddate']) && $_REQUEST['enddate'] != '')) {
                $sql .= " AND EventDateEnd = '" . encode_strings($_REQUEST['enddate'], $db) . "' ";
            }
            if (isset($_REQUEST['startdate']) && $_REQUEST['startdate'] != '' && isset($_REQUEST['enddate']) && $_REQUEST['enddate'] != '') {
                $sql .= " AND (EventDateStart BETWEEN '" . encode_strings($_REQUEST['startdate'], $db) . "' AND   '" . encode_strings($_REQUEST['enddate'], $db) . "' ";
                $sql .= " OR EventDateEnd BETWEEN '" . encode_strings($_REQUEST['startdate'], $db) . "' AND  '" . encode_strings($_REQUEST['enddate'], $db) . "') ";
            }
            if (isset($_REQUEST['sortbyregion']) && $_REQUEST['sortbyregion'] > 0) {
                $sql .= " AND FIND_IN_SET (" . encode_strings($_REQUEST['sortbyregion'], $db) . ",E_Region_ID) ";
            }
            if (!in_array('superadmin', $_SESSION['USER_ROLES']) && !in_array('admin', $_SESSION['USER_ROLES'])) {
                $first = true;
                $sql .= " And (";
                foreach ($regionLimit as $e_region_id) {
                    if ($first) {
                        $first = false;
                    } else {
                        $sql .= " OR ";
                    }
                    $sql .= "FIND_IN_SET (" . $e_region_id . ",E_Region_ID )";
                }
                $sql .= " )";
            }
            if (isset($_REQUEST['sortby']) && $_REQUEST['sortby'] == 'oldest') {
                $sql .= "ORDER BY EventDateStart ASC";
            } else if (isset($_REQUEST['sortby']) && ($_REQUEST['sortby']) == 'newest') {
                $sql .= "ORDER BY EventDateStart DESC";
            } else {
                $sql .= "ORDER BY EventDateStart DESC";
            }
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $pages = new Paginate(mysql_num_rows($result), 100);
            $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                ?>
                <div class="data-content">
                    <div class="data-column spl-name-events-pending">
                        <a href="event.php?id=<?php echo $row['EventID'] ?>">
                            <?php echo $row['Title'] ?>
                        </a>
                    </div>
                    <div  class="data-column spl-other-cc-list-community">
                        <?php
                        $query = mysql_query("SELECT * FROM Events_master WHERE EventID = '" . $row['EventID'] . "'");
                        $row_check = mysql_fetch_assoc($query);
                        $regions = explode(',', $row_check['E_Region_ID']);
                        $counts = count($regions);
                        $i = 1;
                        foreach ($regions as $key => $region) {
                            $query = mysql_query("SELECT R_Name FROM tbl_Region WHERE R_ID = '" . $region . "'");
                            $rows = mysql_fetch_assoc($query);
                            if ($i !== $counts) {
                                echo $rows['R_Name'] . ', ';
                            } else {
                                echo $rows['R_Name'];
                            }
                            $i++;
                        }
                        ?>
                    </div>
                    <div class="data-column spl-other-stories"><a onClick="return confirm('Are you sure?');" href="events-pending.php?op=save&approve=1&id=<?php echo $row['EventID'] ?>">Approve</a></div>
                    <div class="data-column spl-other-stories"><a onClick="return confirm('Are you sure?');" href="events-pending.php?op=save&delete=1&id=<?php echo $row['EventID'] ?>">Delete</a></div>
                    <div class="ddata-column spl-other-stories">
                        <div class="checkboxes-pending-events"> <input type="checkbox" name="checkbox[]" value="<?php echo $row['EventID'] ?>" /></div>
                    </div>
                </div>
                <?PHP
            }
            if (isset($pages)) {
                echo $pages->paginate();
            }
            ?>
            <div class="form-inside-div border-none" id="btn">
                <div class="button">
                    <input value="Approve" type="submit" name="saveVal" class="btn-Approve">
                    <input value="Delete" type="submit" name="deleteVal" class="btn-Delete" onClick="return confirm('Are you sure?\nThis action CANNOT be undone!');">
                </div>
            </div>
        </div>
</div>
</form>
<script>
    $(document).ready(function () {
        $('#checkall').click(function () {
            var checked = $(this).prop('checked');
            $('.checkboxes-pending-events').find('input:checkbox').prop('checked', checked);
        });
    })
    $(document).ready(function () {
        $("#startdate").change(function () {
            var starting_date = $('#startdate').val();
            var ending_date = $('#enddate').val();
            if (ending_date != '') {
                if (ending_date < starting_date)
                {
                    alert("End Date must be greater than start date");
                    $('#enddate').val("");
                }
            }
        });
        $("#enddate").change(function () {
            var starting_date = $('#startdate').val();
            var ending_date = $('#enddate').val();
            if (ending_date < starting_date)
            {
                alert("End Date must be greater than start date");
                $('#enddate').val("");
            }
        });
    });
</script>
<?PHP
require_once '../include/admin/footer.php';
?>