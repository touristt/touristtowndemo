<?php

require_once '../include/config.inc.php';

$region = $_POST['region'];
$type = $_POST['sortby'];
$startdate = $_POST['startdate'];
$enddate = $_POST['enddate'];
$c_region = $_POST['c_region'];

//return
$events = '<option value="">Select Events</option>';

//if region selected
if ($region > 0 || $enddate !== '' || $startdate !== '' || $type !== 'Sort by:') {

    $sqlEvents = "SELECT DISTINCT Title, EventID FROM Events_master WHERE Pending = 0 AND EventDateEnd >= CURDATE() ";

    if (strlen($_REQUEST['strSearch']) > 3) {
        $sql .= " AND Title LIKE '%" . encode_strings($_REQUEST['strSearch'], $db) . "%' ";
    }
    if (is_numeric($_REQUEST['sortby']) && $_REQUEST['sortby'] > 0) {
        $sqlEvents .= " AND EventType = '" . encode_strings($_REQUEST['sortby'], $db) . "' ";
    }
    if ((isset($_REQUEST['startdate']) && $_REQUEST['startdate'] != '') && (isset($_REQUEST['enddate']) && $_REQUEST['enddate'] == '')) {
        $sqlEvents .= " AND EventDateStart = '" . encode_strings($_REQUEST['startdate'], $db) . "' AND NOT FIND_IN_SET (" . encode_strings($c_region, $db) . ",E_Region_ID) ";
    }
    if ((isset($_REQUEST['startdate']) && $_REQUEST['startdate'] == '') && (isset($_REQUEST['enddate']) && $_REQUEST['enddate'] != '')) {
        $sqlEvents .= " AND EventDateEnd = '" . encode_strings($_REQUEST['enddate'], $db) . "' AND NOT FIND_IN_SET (" . encode_strings($c_region, $db) . ",E_Region_ID) ";
    }
    if (isset($_REQUEST['startdate']) && $_REQUEST['startdate'] != '' && isset($_REQUEST['enddate']) && $_REQUEST['enddate'] != '') {
        $sqlEvents .= " AND (EventDateStart BETWEEN '" . encode_strings($_REQUEST['startdate'], $db) . "' AND   '" . encode_strings($_REQUEST['enddate'], $db) . "' ";
        $sqlEvents .= " OR EventDateEnd   BETWEEN '" . encode_strings($_REQUEST['startdate'], $db) . "' AND  '" . encode_strings($_REQUEST['enddate'], $db) . "') AND NOT FIND_IN_SET (" . encode_strings($c_region, $db) . ",E_Region_ID) ";
    }
    if ($_REQUEST['region'] && $_REQUEST['region'] > 0) {
        $sqlEvents .= "AND FIND_IN_SET (" . encode_strings($region, $db) . ",E_Region_ID) AND NOT FIND_IN_SET (" . encode_strings($c_region, $db) . ",E_Region_ID) ";
    }
    if (isset($_REQUEST['sortby']) && $_REQUEST['sortby'] == 'oldest') {
        $sqlEvents .= "AND NOT FIND_IN_SET (" . encode_strings($c_region, $db) . ",E_Region_ID) ORDER BY EventDateStart ASC";
    } else if (isset($_REQUEST['sortby']) && ($_REQUEST['sortby']) == 'newest') {
        $sqlEvents .= "AND NOT FIND_IN_SET (" . encode_strings($c_region, $db) . ",E_Region_ID) ORDER BY EventDateStart DESC";
    } else {
        $sqlEvents .= "ORDER BY EventDateStart DESC";
    }
//    print_r($sqlEvents);
    $resEvents = mysql_query($sqlEvents);
//  $Events = '<option value="">Events</option>';



    while ($Events = mysql_fetch_assoc($resEvents)) {
        $events .= '<option value="' . $Events['EventID'] . '">' . $Events['Title'] . '</option>';
    }
}


$return['event'] = $events;
print json_encode($return);
exit;
?>