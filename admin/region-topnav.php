<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} elseif ($_REQUEST['newRegion'] <> true) {
    header("Location: /admin/regions.php");
    exit();
}

$sql = "SELECT R_ID, R_Name, R_Type, R_Topnav_Title FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);

if ($_POST['op'] == 'save') {
    $sql = "tbl_Region SET R_Topnav_Title = '" . encode_strings($_REQUEST['nav_title'], $db) . "'";
    $sql = "UPDATE " . $sql . " WHERE R_ID = '" . encode_strings($regionID, $db) . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: region-topnav.php?rid=" . $regionID);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Top Navigation Bar</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP
        if ($regionID > 0) {
            require '../include/nav-manage-region.php';
        } else {
            echo "&nbsp;";
        }
        ?>
    </div>
    <div class="right">
        <div class="content-header"> 
            <div class="title">Top Navigation Links</div>
            <div class="link"><a href="/admin/region-topnav-actions.php?rid=<?php echo $activeRegion['R_ID'] ?>">+ Add Link</a></div>
        </div>
        <div class="content-sub-header">
            <div class="data-column rfp-title padding-none">Title</div>
            <div class="data-column rfp-other padding-none"></div>
            <div class="data-column rfp-other padding-none"></div>
            <div class="data-column rfp-other padding-none">Edit</div>
            <div class="data-column rfp-other padding-none">Delete</div>
        </div>
        <div class="brief">
        <?php
        $sql = "SELECT * FROM tbl_Region_Topnav_Link WHERE RTL_R_ID = '" . encode_strings($activeRegion['R_ID'], $db) . "' ORDER BY RTL_Order";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_array($result)) {
            ?>
            <div class="data-content" id="recordsArray_<?php echo $row['RTL_ID'] ?>">
                <div class="data-column rfp-title"><?php echo $row['RTL_Title'] ?></div>
                <div class="data-column rfp-other"></div>
                <div class="data-column rfp-other"></div>
                <div class="data-column rfp-other"><a href="region-topnav-actions.php?id=<?php echo $row['RTL_ID'] ?>">Edit</a></div>
                <div class="data-column rfp-other"><a onClick="return confirm('Are you sure?')" href="region-topnav-actions.php?id=<?php echo $row['RTL_ID'] ?>&amp;op=del">Delete</a></div>
            </div>
        <?php } ?>
            </div>
        
        <div class="content-header">Add Top Navigation Title</div>
        <form action="region-topnav.php" method="post" name="form">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>" id="rid">
            <!--Footer work-->
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Top Navigation Title</label>
                <div class="form-data">
                    <input type="text" name="nav_title" size="50" value="<?php echo $activeRegion['R_Topnav_Title'] ?>" required/>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="Save" value="Submit" />
                </div>
            </div>
        </form>
    </div>
    <div id="dialog-message"></div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script>
    $(function() {
        $(".brief").sortable({opacity: 0.9, cursor: 'move', update: function() {
                var rid = $("#rid").val();
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Region_Topnav_Link&field=RTL_Order&id=RTL_ID&rid='+rid;
                $.post("reorder_nav.php", order, function(theResponse) {
                    $("#dialog-message").html("Top Navigation Links Re-Ordered");
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            Ok: function() {
                                $(this).dialog("close");
                            }
                        }
                    });
                });
            }
        });
    });
</script>
