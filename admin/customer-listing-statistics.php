<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
//require_once '../include/google-api-php-client-master/src/Google/autoload.php';

$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    // get regions of the listings in this business
        $sql_region = "SELECT BLCR_BLC_R_ID, RM_Parent FROM tbl_Business_Listing
                        INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID 
                        Left JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                        INNER JOIN tbl_Region_Multiple ON RM_Child = R_ID
                        WHERE BLCR_BL_ID = $BL_ID ";
        $res_region = mysql_query($sql_region);
        $regions = array();
        while ($r = mysql_fetch_assoc($res_region)) {
            // add region parent as well if available
            if ($r['RM_Parent'] != 0 && !in_array($r['RM_Parent'], $regions)) {
                $regions[] = $r['RM_Parent'];
            }
            // add region
            if ($r['BLCR_BLC_R_ID'] != '' && !in_array($r['BLCR_BLC_R_ID'], $regions)) {
                $regions[] = $r['BLCR_BLC_R_ID'];
            }
        }
} else {
    header('Location: index.php');
}

require_once '../include/admin/header.php';
?>
<div class="content-left">

    <?php require_once '../include/top-nav-listing.php'; //top-nav-listing   ?> 

    <div class="title-link">
        <div class="title">My Page</div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div>
    </div>

    <div class="left">
        <?PHP
        require '../include/nav-listing-admin.php'; // nav-listing-admin
        ?>
    </div>

    <div class="right">
        <div class="content-header">Statistics</div>

        <?php
        $help_text = show_help_text('Statistics');
        if ($help_text != '') {
            echo '<div class="form-inside-div border-none">' . $help_text . '</div>';
        }
        ?>
            <?php
            $analytics = getService();
            $profile = getprofileId($analytics, $BL_ID ,$regions);

            function getService() {
                // Creates and returns the Analytics service object.
                // Load the Google API PHP Client Library.
                require_once '../include/google-api-php-client-master/src/Google/autoload.php';

                // Use the developers console and replace the values with your
                // service account email, and relative location of your key file.
                $service_account_email = 'analytics@tourist-town.iam.gserviceaccount.com';
                $key_file_location = 'Tourist Town-2a6f3c92156a.p12';

                // Create and configure a new client object.
                $client = new Google_Client();
                $client->setApplicationName("HelloAnalytics");
                $analytics = new Google_Service_Analytics($client);

                // Read the generated client_secrets.p12 key.
                $key = file_get_contents($key_file_location);
                $cred = new Google_Auth_AssertionCredentials(
                        $service_account_email, array(Google_Service_Analytics::ANALYTICS_READONLY), $key
                );
                $client->setAssertionCredentials($cred);
                if ($client->getAuth()->isAccessTokenExpired()) {
                    $client->getAuth()->refreshTokenWithAssertion($cred);
                }
                return $analytics;
            }

            function getprofileId(&$analytics, &$BL_ID, &$regions) {
                $regions=implode(',',$regions);
                $sql_listingname = mysql_query("SELECT * FROM  `tbl_Business_Listing` WHERE BL_ID=$BL_ID");
                $row_listingname = mysql_fetch_assoc($sql_listingname);
                $accounts = $analytics->management_accounts->listManagementAccounts();
                if (count($accounts->getItems()) > 0) {
                    $items = $accounts->getItems();
                    foreach ($items as $item) {
                        $firstAccountId = $item->getId();
                        $webproperties = $analytics->management_webproperties->listManagementWebproperties($firstAccountId);
                        if (count($webproperties->getItems()) > 0) {
                            $items_webproperties = $webproperties->getItems();
                            foreach ($items_webproperties as $items_webproperty) {
                                $firstWebpropertyId = $items_webproperty->getId();
                                $profiles = $analytics->management_profiles->listManagementProfiles($firstAccountId, $firstWebpropertyId);
                                if (count($profiles->getItems()) > 0) {
                                    $items_profiles = $profiles->getItems();
                                    foreach ($items_profiles as $items_profile) {
                                        $profileId = $items_profile->getId();
                                        $profileweb = $items_profile->getwebPropertyId();
                                        $sql_region_details=mysql_query("SELECT * FROM tbl_Region where R_ID IN ($regions) order by R_ID");
                                        while ($rows_get_region = mysql_fetch_array($sql_region_details)) {
                                            if($rows_get_region['R_Tracking_ID'] == $profileweb )
                                            {
                                                $Region_id=$rows_get_region['R_ID'];
                                                if (isset($profileId)) {
                                            $startdate = date('2014-01-01');
                                            $enddate = date('Y-m-d');
                                            $filters = array('filters' => 'ga:pagePath=~/profile/' . $row_listingname['BL_Name_SEO'] . '/' . $BL_ID . '/*', 'dimensions' => 'ga:year,ga:month', 'max-results' => '800000');
                                            $results_pages = getResultsGA($analytics, $profileId, $startdate, $enddate, 'ga:pageviews,ga:sessions,ga:uniquePageviews', $filters);
                                            printResultsPages($results_pages,$Region_id);
                                           
                                        }
                                            }
                                        }
                                        
                                    }
                                } else {
                                    throw new Exception('No views (profiles) found for this user.');
                                }
                            }
                        } else {
                            throw new Exception('No webproperties found for this user.');
                        }
                    }
                } else {
                    throw new Exception('No accounts found for this user.');
                }
            }

            function getResultsGA(&$analytics, $profileId, $startdate, $enddate, $matrics, $filters) {
                return $analytics->data_ga->get(
                                'ga:' . $profileId, $startdate, $enddate, $matrics, $filters);
            }
            ?>
            <?php

            function printResultsPages(&$results, &$r) {
                $rows = $results->getRows();
                $regionid = $r;
                $sql_regionname = mysql_query("SELECT R_Name FROM tbl_Region WHERE R_ID = '$regionid'");
                $regionname = mysql_fetch_assoc($sql_regionname);
                ?>
                <div class="content-header"><?php echo $regionname['R_Name']; ?></div>
                <div class="content-sub-header padding-none">
                    <div class="data-column">Year</div>
                    <div class="data-column">Month</div>
                    <div class="data-column">Viewed</div>
                    <div class="data-column">Sessions</div>
                    <div class="data-column">Unique</div>

                </div>

                <?php
                foreach (array_reverse($rows) as $monthrecord) {
                    ?>
                    <div class="data-content">
                        <div class="data-column"><?php print_r($monthrecord[0]); ?></div>
                        <div class="data-column"><?php print_r(date('M', strtotime($monthrecord[0].'-'.$monthrecord[1].'-'.'01')));?></div>
                        <div class="data-column"><?php print_r($monthrecord[2]); ?></div>
                        <div class="data-column"><?php print_r($monthrecord[3]); ?></div>
                        <div class="data-column"><?php print_r($monthrecord[4]); ?></div>
                    </div>
                    <?php
                }
            }
            ?>
    </div>
</div>

<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>