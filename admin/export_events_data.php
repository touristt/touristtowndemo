<?php
require_once '../include/config.inc.php';
require_once '../include/adminFunctions.inc.php';
if (isset($_REQUEST['export_events'])) {
// Download the file
    $filename = "Events.csv";
    header('Content-type: application/csv');
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment;filename=' . $filename);
    header("Pragma: no-cache");
    header("Expires: 0");
    echo "\xEF\xBB\xBF";

    $output = "";
    $output = 'Events ,' . 'Start Date ,' . 'End Date ,' . 'Full Description ,' . 'Time ,' . 'Community ,' . 'Organization ,' . 'Location ,' . 'Phone ,' . 'Additional Phone ,' . 'Email ,' . 'Website';
    $output .= "\n";
    $sql = "SELECT DISTINCT EventID, EL_Name, EO_Name, Content, LocationID, OrganizationID, Title, EventDateStart, EventDateEnd, EventTimeAllDay, Organization, LocationList, StreetAddress, ContactPhone, ContactPhoneTollFree, Email, WebSiteLink, EventStartTime, EventEndTime, E_Region_ID FROM Events_master 
        LEFT JOIN Events_Location ON EL_ID = LocationID 
        LEFT JOIN Events_Organization ON EO_ID = OrganizationID
        WHERE Pending = 0 AND EventDateEnd >= CURDATE() ";
    if (strlen($_REQUEST['strSearch']) > 3) {
        $sql .= " AND Title LIKE '%" . encode_strings($_REQUEST['strSearch'], $db) . "%' ";
    }
    if (is_numeric($_REQUEST['sortby']) && $_REQUEST['sortby'] > 0) {
        $sql .= " AND EventType = '" . encode_strings($_REQUEST['sortby'], $db) . "' ";
    }
    if ((isset($_REQUEST['startdate']) && $_REQUEST['startdate'] != '') && (isset($_REQUEST['enddate']) && $_REQUEST['enddate'] == '')) {
        $sql .= " AND EventDateStart = '" . encode_strings($_REQUEST['startdate'], $db) . "' ";
    }
    if ((isset($_REQUEST['startdate']) && $_REQUEST['startdate'] == '') && (isset($_REQUEST['enddate']) && $_REQUEST['enddate'] != '')) {
        $sql .= " AND EventDateEnd = '" . encode_strings($_REQUEST['enddate'], $db) . "' ";
    }
    if (isset($_REQUEST['startdate']) && $_REQUEST['startdate'] != '' && isset($_REQUEST['enddate']) && $_REQUEST['enddate'] != '') {
        $sql .= " AND (EventDateStart BETWEEN '" . encode_strings($_REQUEST['startdate'], $db) . "' AND   '" . encode_strings($_REQUEST['enddate'], $db) . "' ";
        $sql .= " OR EventDateEnd   BETWEEN '" . encode_strings($_REQUEST['startdate'], $db) . "' AND  '" . encode_strings($_REQUEST['enddate'], $db) . "') ";
    }
    if (isset($_REQUEST['sortbyregion']) && $_REQUEST['sortbyregion'] > 0) {
        $sql .= " AND FIND_IN_SET (" . encode_strings($_REQUEST['sortbyregion'], $db) . ",E_Region_ID) ";
    }
    if (!in_array('superadmin', $_SESSION['USER_ROLES']) && !in_array('admin', $_SESSION['USER_ROLES'])) {
        $first = true;
        $sql .= " AND (";
        foreach ($regionLimit as $e_region_id) {
            if ($first) {
                $first = false;
            } else {
                $sql .= " OR ";
            }
            $sql .= "FIND_IN_SET (" . $e_region_id . ",E_Region_ID )";
        }
        $sql .= " )";
    }
    if (isset($_REQUEST['sortby']) && $_REQUEST['sortby'] == 'oldest') {
        $sql .= "ORDER BY EventDateStart ASC";
    } else if (isset($_REQUEST['sortby']) && ($_REQUEST['sortby']) == 'newest') {
        $sql .= "ORDER BY EventDateStart DESC";
    } else {
        $sql .= "ORDER BY EventDateStart DESC";
    }
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($rowAT = mysql_fetch_assoc($result)) {
        if ($rowAT['EventTimeAllDay'] == '1') {
            $event_time = "All day";
        } else {
            $event_time = $rowAT['EventStartTime'] == '00:00:01' ? 'Dusk' : ($rowAT['EventStartTime'] == '00:00:00' ? '' : date('g:i a', strtotime($rowAT['EventStartTime'])));
            $event_time .= $rowAT['EventEndTime'] <> '00:00:00' ? (' to ' . ($rowAT['EventEndTime'] == '00:00:01' ? 'Dusk' : date('g:i a', strtotime($rowAT['EventEndTime'])))) : '';
        }

        $regions = explode(',', $rowAT['E_Region_ID']);
        $counts = count($regions);
        $i = 1;
        $reg ='';
        foreach ($regions as $key => $region) {
            $query = mysql_query("SELECT R_Name FROM tbl_Region WHERE R_ID = '" . $region . "'");
            $rows = mysql_fetch_assoc($query);
            if ($i !== $counts) {
                $reg .= $rows['R_Name'] . ', ';
            } else {
                $reg .= $rows['R_Name'];
            }
            $i++;
        }

        if ($rowAT['LocationID']) {
            $Location = $rowAT['EL_Name'];
            if ($rowAT['EL_Street']) {
                $Address = $rowAT['EL_Street'] . $rowAT['EL_Town'];
            }
        } elseif ($rowAT['LocationList'] && $rowAT['LocationList'] <> 'Alternate Location') {
            if ($rowAT['LocationList']) {
                $Location = $rowAT['LocationList'];
            } if ($rowAT['StreetAddress']) {
                $Address = $rowAT['StreetAddress'];
            }
        }

        $Org = $rowAT['OrganizationID'] ? $rowAT['EO_Name'] : $rowAT['Organization'];

        $output .= '"' . str_replace('"', '""', $rowAT['Title']) . '"' . ',' . '"' . str_replace('"', '""', $rowAT['EventDateStart']) . '"' . ',' . '"' . str_replace('"', '""', $rowAT['EventDateEnd']) . '"' . ',' . '"' . strip_tags($rowAT['Content']) . '"' . ',' . '"' . $event_time . '"' . ',' . '"' . $reg . '"' . ',' . '"' . str_replace('"', '""', $Org) . '"' . ',' . '"' . str_replace('"', '""', $Location) . '"' . ',' . '"' . str_replace('"', '""', $rowAT['ContactPhone']) . '"' . ',' . '"' . str_replace('"', '""', $rowAT['ContactPhoneTollFree']) . '"' . ',' . '"' . str_replace('"', '""', $rowAT['Email']) . '"' . ',' . '"' . str_replace('"', '""', $rowAT['WebSiteLink']) . '"';
        $output .= "\n";
    }
    echo $output;
}
?>



