<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/ranking.inc.php';

if (!in_array('delete-invoices', $_SESSION['USER_PERMISSIONS']) && !in_array('superadmin', $_SESSION['USER_ROLES'])) {
    header("Location: /admin/");
    exit();
}

$OP = isset($_GET['op']) ? $_GET['op'] : '';
$cc_refund = isset($_GET['cc_refund']) ? $_GET['cc_refund'] : '';
$DB_ID = isset($_GET['db_id']) ? $_GET['db_id'] : '';
$BL_ID = isset($_GET['bl_id']) ? $_GET['bl_id'] : '';

if (isset($_POST['op']) && $_POST['op'] == 'del') {
    $OP = isset($_POST['op']) ? $_POST['op'] : '';
    $ID = isset($_POST['id']) ? $_POST['id'] : '';
    $BL_ID = isset($_POST['bl_id']) ? $_POST['bl_id'] : '';
    $REASON = isset($_POST['reason']) ? $_POST['reason'] : '';
    $sql = "UPDATE tbl_Division_Billing SET DB_Deleted = 1, DB_Deleted_Date = NOW(), 
            DB_Deleted_By = '" . encode_strings($_SESSION['USER_EM'] . '-' . $_SESSION['user_online'] . '-' . $_SESSION['USER_ID'], $db) . "',
            DB_Deleted_Reason = '" . encode_strings($REASON, $db) . "'
            WHERE DB_ID = '" . encode_strings($DB_ID, $db) . "'";
    mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    header('Location: division-billing-invoices.php');
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'refund') {
    $OP = isset($_POST['op']) ? $_POST['op'] : '';
    $cc_refund = isset($_POST['cc_refund']) ? $_POST['cc_refund'] : '';
    $DB_ID = isset($_POST['db_id']) ? $_POST['db_id'] : '';
    $BL_ID = isset($_POST['bl_id']) ? $_POST['bl_id'] : '';
    $REASON = isset($_POST['reason']) ? $_POST['reason'] : '';

    //check if division billing has listing or not
    $sql_DB = "SELECT DB_ID, DB_Customer_Name, DB_Transaction_ID, DB_Total, DB_Profile_Id, DB_Payment_Profile FROM tbl_Division_Billing WHERE DB_ID = '" . $DB_ID . "'";
    $DB = mysql_fetch_assoc(mysql_query($sql_DB));
    $amount = $DB['DB_Total'];
    if ($cc_refund == 1) {
        if ($BL_ID > 0) {
            $sql_profile = "SELECT region FROM tbl_Business_Listing LEFT JOIN payment_profiles ON BL_B_ID = business_id WHERE BL_ID = " . $BL_ID . " LIMIT 1";
        } else {
            $sql_profile = "SELECT region FROM tbl_Division_Billing LEFT JOIN payment_profiles ON DB_Customer_Name = division_id WHERE DB_Customer_Name = '" . $DB['DB_Customer_Name'] . "' LIMIT 1";
        }
        $profile = mysql_fetch_assoc(mysql_query($sql_profile));
        // get authorize.net information of the specified region
        $sql_region = "SELECT * FROM tbl_Region WHERE R_ID = " . $profile['region'] . " LIMIT 1";
        $res_region = mysql_query($sql_region);
        $payment_region = mysql_fetch_assoc($res_region);
        require_once 'payment/config_payment.php';
        // Get Transaction Details
        $td_request = new AuthorizeNetTD;
        $transaction_details = $td_request->getTransactionDetails($DB['DB_Transaction_ID']);
        if ($transaction_details->xml->transaction->transactionStatus == 'settledSuccessfully') {
            $request = new AuthorizeNetCIM;
            $transaction = new AuthorizeNetTransaction;
            $transaction->amount = $amount;
            $transaction->customerProfileId = $DB['DB_Profile_Id'];
            $transaction->customerPaymentProfileId = $DB['DB_Payment_Profile'];
            $transaction->transId = $DB['DB_Transaction_ID']; // original transaction ID

            $response = $request->createCustomerProfileTransaction("Refund", $transaction);
            $transactionResponse = $response->getTransactionResponse();
            if ($transactionResponse->approved) {
                $delete = " UPDATE tbl_Division_Billing SET 
                        DB_Refund_Deleted = 1, 
                        DB_Refund_Deleted_Date = NOW(), 
                        DB_Refund_Deleted_By = '" . encode_strings($_SESSION['USER_EM'] . '-' . $_SESSION['user_online'] . '-' . $_SESSION['USER_ID'], $db) . "',
                        DB_Refund_Deleted_Reason = '" . encode_strings($REASON, $db) . "'
                        WHERE DB_ID = '" . encode_strings($DB_ID, $db) . "'";
                mysql_query($delete);
                $log = "Amount: '" . $amount . "' has been refunded to '" . $DB_ID . "' on '" . date("Y-m-d") . "'";
                file_put_contents('refund_amounts.log', $log . PHP_EOL, FILE_APPEND);
                $_SESSION['transaction_success'] = 2;
                header("Location: division-billing-invoices.php");
                exit();
            } else {
                $encoded = json_encode($response);
                $log = date('Y-m-d H:i:s') . ':  Division Billing: ' . $DB['DB_ID'] . '. Error: ' . $encoded . PHP_EOL;
                file_put_contents('refund_amounts.log', $log . PHP_EOL, FILE_APPEND);
                $_SESSION['transaction_error'] = 1;
                if ($transactionResponse->response_reason_text != '') {
                    $_SESSION['transaction_msg'] = $transactionResponse->response_reason_text;
                } elseif (current($response->xml->messages->message->text) != '') {
                    $_SESSION['transaction_msg'] = current($response->xml->messages->message->text);
                } else {
                    $_SESSION['transaction_msg'] = 'Unknown error';
                }
                header("Location: division-billing-invoice.php?db_id=" . $DB_ID);
                exit();
            }
        } elseif ($transaction_details->xml->transaction->transactionStatus == 'capturedPendingSettlement') {
            $void = new AuthorizeNetAIM;
            $void_response = $void->void($DB['DB_Transaction_ID']);
            if ($void_response->approved) {
                $delete = " UPDATE tbl_Division_Billing SET 
                        DB_Refund_Deleted = 1, 
                        DB_Refund_Deleted_Date = NOW(), 
                        DB_Refund_Deleted_By = '" . encode_strings($_SESSION['USER_EM'] . '-' . $_SESSION['user_online'] . '-' . $_SESSION['USER_ID'], $db) . "',
                        DB_Refund_Deleted_Reason = '" . encode_strings($REASON, $db) . "'
                        WHERE DB_ID = '" . encode_strings($DB_ID, $db) . "'";
                mysql_query($delete);
                $log = "Amount: '" . $amount . "' has been voided to '" . $DB['DB_ID'] . "' on '" . date("Y-m-d") . "'";
                file_put_contents('refund_amounts.log', $log . PHP_EOL, FILE_APPEND);
                $_SESSION['transaction_success'] = 2;
                header("Location: division-billing-invoices.php");
                exit();
            } else {
                $_SESSION['transaction_error'] = 1;
                if ($void_response->response_reason_text != '') {
                    $_SESSION['transaction_msg'] = $void_response->response_reason_text;
                } else {
                    $_SESSION['transaction_msg'] = 'Unknown error';
                }
                header("Location: division-billing-invoice.php?db_id=" . $DB_ID);
                exit();
            }
        } else {
            $_SESSION['transaction_error'] = 1;
            $_SESSION['transaction_msg'] = 'Transaction was not found.';
            header("Location: division-billing-invoice.php?db_id=" . $DB_ID);
            exit();
        }
    } else {
        $delete = " UPDATE tbl_Division_Billing SET 
                    DB_Refund_Deleted = 1, 
                    DB_Refund_Deleted_Date = NOW(), 
                    DB_Refund_Deleted_By = '" . encode_strings($_SESSION['USER_EM'] . '-' . $_SESSION['user_online'] . '-' . $_SESSION['USER_ID'], $db) . "',
                    DB_Refund_Deleted_Reason = '" . encode_strings($REASON, $db) . "'
                    WHERE DB_ID = '" . encode_strings($DB_ID, $db) . "'";
        mysql_query($delete);
        $log = "Amount: '" . $amount . "' has been refunded to '" . $DB_ID . "' on '" . date("Y-m-d") . "'";
        file_put_contents('refund_amounts.log', $log . PHP_EOL, FILE_APPEND);
        $_SESSION['transaction_success'] = 2;
        header("Location: division-billing-invoices.php");
        exit();
    }
}

require_once '../include/admin/header.php';
?>
<div class="content-left">

    <?php require_once '../include/top-nav-listing.php'; ?>
    <div class="title-link">
        <div class="title"><?php echo ($OP == 'del') ? 'Delete Invoice # ' . $DB_ID . '?' : 'Refund Invoice # ' . $DB_ID . '?'; ?></div>
        <div class="link">
            <a class="invoices" href="division-billing-invoice.php?db_id=<?php echo $DB_ID ?>">Cancel</a></td>
        </div>
    </div>
    <div class="left">
    </div>
    <div class="right">
        <form method="POST" action="">
            <input type="hidden" name="op" value="<?php echo $OP ?>">
            <input type="hidden" name="cc_refund" value="<?php echo $cc_refund ?>">
            <input type="hidden" name="db_id" value="<?php echo $DB_ID ?>">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Reason</label>
                <div class="form-data">
                    <textarea name="reason" cols="50" required></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin border-none"> 
                <div class="button">
                    <input type="submit" name="submit" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>

<?PHP
require_once '../include/admin/footer.php';
?>