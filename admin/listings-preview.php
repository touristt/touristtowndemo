<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';
require '../include/PHPMailer/class.phpmailer.php';
require_once '../include/ranking.inc.php';

if (!in_array('business-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$BL_ID = $_REQUEST['bl_id'];
$preview_page = 1;

if ($BL_ID > 0) {
    $sql = "SELECT B_ID FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
}

if (isset($_POST['op']) && $_POST['op'] == 'edit') {
    $BFP_ID = $_POST['bfp_id'];
    $text = $_POST['description' . $BFP_ID];
    $sql = "UPDATE tbl_Business_Feature_Photo SET BFP_Title = '$text' WHERE BFP_ID = '$BFP_ID'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit();
}

if ($_GET['op'] == 'del') {
    $BL_ID = $_GET['bl_id'];
    $BFP_ID = $_REQUEST['id'];
    $sql = "DELETE FROM tbl_Business_Feature_Photo 
            WHERE BFP_ID = '" . encode_strings($BFP_ID, $db) . "' AND BFP_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Photo_Gallery', $BFP_ID);
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save_contact_details') {
    $BL_ID = $_REQUEST['bl_id'];
    $sql = "tbl_Business_Listing SET 
            BL_Listing_Title = '" . encode_strings($_REQUEST['name'], $db) . "', 
            BL_Name_SEO = '" . encode_strings($_REQUEST['seoName'], $db) . "', 
            BL_Contact = '" . encode_strings($_REQUEST['contact'], $db) . "', 
            BL_Phone = '" . encode_strings($_REQUEST['phone'], $db) . "', 
            BL_Toll_Free = '" . encode_strings($_REQUEST['tollFree'], $db) . "', 
            BL_Cell = '" . encode_strings($_REQUEST['cell'], $db) . "', 
            BL_Fax = '" . encode_strings($_REQUEST['fax'], $db) . "', 
            BL_Email = '" . encode_strings($_REQUEST['email'], $db) . "', 
            BL_Website = '" . encode_strings($_REQUEST['website'], $db) . "'";

    if (isset($BL_ID) && $BL_ID > 0) {
        $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Contact Details', '', 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $id, 'Contact Details', '', 'Add', 'super admin');
    }

    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
}

if (isset($_POST['op']) && $_POST['op'] == 'save_cutomer_listing_mapping') {
    $BL_ID = $_REQUEST['bl_id'];
    $sql = "tbl_Business_Listing SET
            BL_Country = '" . encode_strings($_REQUEST['country'], $db) . "',  
            BL_Street = '" . encode_strings($_REQUEST['address'], $db) . "', 
            BL_Town = '" . encode_strings($_REQUEST['town'], $db) . "', 
            BL_Province = '" . encode_strings($_REQUEST['province'], $db) . "', 
            BL_PostalCode = '" . encode_strings($_REQUEST['postalcode'], $db) . "',
            BL_Location_Description = '" . encode_strings($_REQUEST['locationdescription'], $db) . "',
            BL_Lat = '" . encode_strings($_REQUEST['latitude'], $db) . "', 
            BL_Long = '" . encode_strings($_REQUEST['longitude'], $db) . "'";
    if ($_REQUEST['latitude'] != '' && $_REQUEST['longitude']) {
        $sql .= ", BL_Location = POINT(" . $_REQUEST['latitude'] . ", " . $_REQUEST['longitude'] . ")";
    }
    if (isset($BL_ID) && $BL_ID > 0) {
        $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Location & Mapping', '', 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $id, 'Location & Mapping', '', 'Add', 'super admin');
    }

    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
}

if ($_POST['op'] == 'save_main_photo') {
    require '../include/picUpload.inc.php';
    //Thumbnail Image
    if ($_POST['thumb_photo'] == "") {
        // last @param 21 = Listing Thumbnail Image
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 21, 'Listing', $BL_ID);
        if (is_array($pic)) {
            $sql = "UPDATE tbl_Business_Listing SET BL_Photo = '" . encode_strings($pic['0']['0'], $db) . "', BL_Mobile_Photo = '" . encode_strings($pic['0']['1'], $db) . "' WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
            $result = mysql_query($sql, $db);
        }
        $pic_id = $pic['1'];
    } else {
        $pic_id = $_POST['thumb_photo'];
        // last @param 21 = Listing Thumbnail Image
        $pic_response = Upload_Pic_Library($pic_id, 21);
        if ($pic_response) {
            Update_Image_Bank_Listings($pic_id, $BL_ID);
            $sql = "UPDATE tbl_Business_Listing SET BL_Photo = '" . encode_strings($pic_response['0'], $db) . "', BL_Mobile_Photo = '" . encode_strings($pic_response['1'], $db) . "' WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
            $result = mysql_query($sql, $db);
        }
    }
    if ($result && $pic_id > 0) {
        //Image usage from image bank.
        imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Main_Photo', 'Listing Thumbnail Image');
    }

    //Main Image
    if ($_POST['main_photo'] == "") {
        // last @param 22 = Listing Main Image
        $pic1 = Upload_Pic('1', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 22, 'Listing', $BL_ID);
        if (is_array($pic1)) {
            $sql1 = "UPDATE tbl_Business_Listing SET BL_Header_Image = '" . encode_strings($pic1['0']['0'], $db) . "',
                    BL_Mobile_Header_Image = '" . encode_strings($pic1['0']['1'], $db) . "'
                    WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
            $result1 = mysql_query($sql1, $db);
        }
        $pic_id1 = $pic1['1'];
    } else {
        $pic_id1 = $_POST['main_photo'];
        // last @param 22 = Listing Main Image
        $pic_response1 = Upload_Pic_Library($pic_id1, 22);
        if ($pic_response1) {
            Update_Image_Bank_Listings($pic_id1, $BL_ID);
            $sql1 = "UPDATE tbl_Business_Listing SET BL_Header_Image = '" . encode_strings($pic_response1['0'], $db) . "',
                    BL_Mobile_Header_Image = '" . encode_strings($pic_response1['1'], $db) . "'
                    WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
            $result1 = mysql_query($sql1, $db);
        }
    }
    $sql_video = "UPDATE tbl_Business_Listing SET BL_Video='" . encode_strings($_POST['home_video'], $db) . "' WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $result_video = mysql_query($sql_video, $db);
    if ($result1 && $pic_id1 > 0) {
        //Image usage from image bank.
        imageBankUsage($pic_id1, 'IBU_BL_ID', $BL_ID, 'IBU_Main_Photo', 'Listing Main Image');
    }
    if ($result || $result1 || $result_video) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Main Photos / Video', '', 'Update', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }

    //update points only for listing
    update_pointsin_business_tbl($BL_ID);
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit();
} elseif (isset($_GET['op']) && $_GET['op'] == 'del_main_photo') {
    if ($_REQUEST['type'] == 0) {
        $del = "BL_Photo = ''";
        $photo_type = 'Listing Thumbnail Image';
    } else {
        $del = "BL_Header_Image = '', BL_Mobile_Header_Image = ''";
        $photo_type = 'Listing Main Image';
    }
    $sql = "UPDATE tbl_Business_Listing SET $del WHERE BL_ID = '$BL_ID'";
    $result = mysql_query($sql);
    if ($result) {
        $_SESSION['delete'] = 1;
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Main_Photo', $photo_type);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Main Photos / Video', '', 'Delete Photo', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }

    //update points only for listing
    update_pointsin_business_tbl($BL_ID);
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save_customer_feature_gallery') {
    require '../include/picUpload.inc.php';
    if ($_POST['image_bank'] == "") {
        $ids = '';
        foreach ($_FILES['pic']['name'] as $key => $val) {
            // last @param 1 = Gallery Images
            $pic = Upload_Pic($key, 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 1, 'Listing', $BL_ID);
            if (is_array($pic)) {
                $sqlMax = "SELECT MAX(BFP_Order) FROM tbl_Business_Feature_Photo WHERE BFP_BL_ID = '$BL_ID'";
                $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
                $rowMax = mysql_fetch_row($resultMax);
                $sql = "INSERT tbl_Business_Feature_Photo SET BFP_Title = '" . encode_strings($_REQUEST['title'], $db) . "',
                        BFP_Photo_710X440 = '" . encode_strings($pic['0']['0'], $db) . "',
                        BFP_Photo_195X195 = '" . encode_strings($pic['0']['1'], $db) . "',
                        BFP_Photo_1000X600 = '" . encode_strings($pic['0']['2'], $db) . "',
                        BFP_Photo_285X210 = '" . encode_strings($pic['0']['3'], $db) . "',
                        BFP_Photo_710X440_Mobile = '" . encode_strings($pic['0']['4'], $db) . "',
                        BFP_BL_ID = '$BL_ID', BFP_Order = '" . ($rowMax[0] + 1) . "'";
                $pic_id = $pic['1'];
            }
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $id = mysql_insert_id();
            $ids = $pic_id . ',' . $ids;
            if ($result) {
                $_SESSION['success'] = 1;
                if ($pic_id > 0) {
                    //Image usage from image bank
                    imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Photo_Gallery', $id);
                }
            } else {
                $_SESSION['error'] = 1;
            }
        }
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $BL_ID, 'Photo Gallery', $ids, 'Add Image', 'super admin');
    } else {

        $ids = encode_strings($_POST['image_bank'], $db);
        $pic_ids = explode(",", $ids);
        foreach ($pic_ids as $pic_id) {
            $pic = Upload_Pic_Library($pic_id, 1);
            if (is_array($pic)) {
                Update_Image_Bank_Listings($pic_id, $BL_ID);
                $sqlMax = "SELECT MAX(BFP_Order) FROM tbl_Business_Feature_Photo WHERE BFP_BL_ID = '$BL_ID'";
                $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
                $rowMax = mysql_fetch_row($resultMax);
                $sql = "INSERT tbl_Business_Feature_Photo SET BFP_Title = '" . encode_strings($_REQUEST['title'], $db) . "',
                        BFP_Photo_710X440 = '" . encode_strings($pic['0'], $db) . "',
                        BFP_Photo_195X195 = '" . encode_strings($pic['1'], $db) . "',
                        BFP_Photo_1000X600 = '" . encode_strings($pic['2'], $db) . "',
                        BFP_Photo_285X210 = '" . encode_strings($pic['3'], $db) . "',
                        BFP_Photo_710X440_Mobile = '" . encode_strings($pic['4'], $db) . "',
                        BFP_BL_ID = '$BL_ID', BFP_Order = '" . ($rowMax[0] + 1) . "'";
            }
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $id = mysql_insert_id();
            if ($result) {
                $_SESSION['success'] = 1;
                if ($pic_id > 0) {
                    //Image usage from image bank
                    imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Photo_Gallery', $id);
                }
            } else {
                $_SESSION['error'] = 1;
            }
        }
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $BL_ID, 'Photo Gallery', $ids, 'Add Image', 'super admin');
    }
    //update points only for listing
    update_pointsin_business_tbl($BL_ID);

    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);

    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save_hours') {
    $hDisabled = isset($_REQUEST['hDisabled']) != '' ? 1 : 0;
    $happointment = isset($_REQUEST['happointment']) != '' ? 1 : 0;
    $sql = "tbl_Business_Listing SET  
            BL_Hours_Disabled = '" . encode_strings($hDisabled, $db) . "', 
            BL_Hours_Appointment = '" . encode_strings($happointment, $db) . "', 
            BL_Hour_Mon_From = '" . encode_strings($_REQUEST['monFrom'], $db) . "', 
            BL_Hour_Mon_To = '" . encode_strings($_REQUEST['monTo'], $db) . "', 
            BL_Hour_Tue_From = '" . encode_strings($_REQUEST['tueFrom'], $db) . "', 
            BL_Hour_Tue_To = '" . encode_strings($_REQUEST['tueTo'], $db) . "', 
            BL_Hour_Wed_From = '" . encode_strings($_REQUEST['wedFrom'], $db) . "', 
            BL_Hour_Wed_To = '" . encode_strings($_REQUEST['wedTo'], $db) . "', 
            BL_Hour_Thu_From = '" . encode_strings($_REQUEST['thuFrom'], $db) . "', 
            BL_Hour_Thu_To = '" . encode_strings($_REQUEST['thuTo'], $db) . "', 
            BL_Hour_Fri_From = '" . encode_strings($_REQUEST['friFrom'], $db) . "', 
            BL_Hour_Fri_To = '" . encode_strings($_REQUEST['friTo'], $db) . "', 
            BL_Hour_Sat_From = '" . encode_strings($_REQUEST['satFrom'], $db) . "', 
            BL_Hour_Sat_To = '" . encode_strings($_REQUEST['satTo'], $db) . "', 
            BL_Hour_Sun_From = '" . encode_strings($_REQUEST['sunFrom'], $db) . "', 
            BL_Hour_Sun_To = '" . encode_strings($_REQUEST['sunTo'], $db) . "'";
    $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . $BL_ID . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Hours', '', 'Update', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save_social_media') {
    if ($_POST['counter'] > 0) {
        $sql = "UPDATE tbl_Business_Social SET 
                BS_FB_Link = '" . encode_strings($_POST['fb_link'], $db) . "',
                BS_T_Link = '" . encode_strings($_POST['t_link'], $db) . "',
                BS_I_Link = '" . encode_strings($_POST['i_link'], $db) . "'
                WHERE BS_BL_ID = '$BL_ID'";
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Social Media', '', 'Update', 'super admin');
    } else {
        $sql = "INSERT INTO tbl_Business_Social(BS_BL_ID, BS_FB_Link, BS_T_Link, BS_I_Link) VALUES('" . encode_strings($BL_ID, $db) . "', '" . encode_strings($_POST['fb_link'], $db) . "', '" . encode_strings($_POST['t_link'], $db) . "', '" . encode_strings($_POST['i_link'], $db) . "')";
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Social Media', '', 'Add', 'super admin');
    }
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save_amenities') {
    $delete = "DELETE FROM tbl_Business_Listing_Ammenity WHERE BLA_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $res = mysql_query($delete, $db);
    foreach ($_POST['AMMid'] as $value) {
        $sql = "INSERT tbl_Business_Listing_Ammenity SET BLA_BA_ID = '$value', BLA_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $res1 = mysql_query($sql, $db);
    }
    // TRACK DATA ENTRY
    $id = $BL_ID;
    Track_Data_Entry('Listing', $id, 'Amenities', '', 'Update', 'super admin');
    if ($res) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        exit();
    } else {
        $_SESSION['error'] = 1;
    }
}

if (isset($_POST['op']) && $_POST['op'] == 'save_pdf') {
    $title = $_POST['pdf_title'];
    $file_size = $_FILES['file']['size'];
    $max_filesize = 5342523;
    $pdf_name = str_replace(' ', '_', $_FILES['file']['name']);
    if ($pdf_name != "") {
        $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_name;
        $filePath = PDF_LOC_ABS . $random_name;
        if ($_FILES["file"]["type"] == "application/pdf") {
            if ($file_size < $max_filesize) {
                move_uploaded_file($_FILES["file"]["tmp_name"], $filePath);
                $sql_pdf = "INSERT INTO tbl_Description_PDF SET D_BL_ID ='$BL_ID', D_PDF='" . encode_strings($random_name, $db) . "', D_PDF_Title = '" . encode_strings($title, $db) . "'";
                $sqlMax = "SELECT MAX(D_PDF_Order) FROM tbl_Description_PDF WHERE D_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
                $rowMax = mysql_fetch_row($resultMax);
                $sql_pdf .= ", D_PDF_Order = '" . ($rowMax[0] + 1) . "'";
                $result = mysql_query($sql_pdf);
                if ($result) {
                    $_SESSION['success'] = 1;
                    // TRACK DATA ENTRY
                    $id = $BL_ID;
                    Track_Data_Entry('Listing', $id, 'Upload A PDF', '', 'Add', 'super admin');
                } else {
                    $_SESSION['error'] = 1;
                }
            }
        }
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit;
}

if (isset($_POST['up']) && $_POST['up'] == 'upload_pdf') {
    $title = $_POST['pdf_update'];
    $pdf_name = str_replace(' ', '_', $_FILES['file']['name']);
    $file_size = $_FILES['file']['size'];
    $max_filesize = 5342523;
    $d_id_update = $_POST['d_id_update'];
    if ($pdf_name != "") {
        $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_name;
        $filePath = PDF_LOC_ABS . $random_name;
        if ($_FILES["file"]["type"] == "application/pdf") {
            if ($file_size < $max_filesize) {
                move_uploaded_file($_FILES["file"]["tmp_name"], $filePath);
                $select_pdf = "SELECT D_PDF FROM tbl_Description_PDF WHERE D_id= $d_id_update AND D_BL_ID ='" . $BL_ID . "'";
                $result_pdf = mysql_query($select_pdf);
                $pdf_row = mysql_fetch_assoc($result_pdf);
                if ($pdf_row['D_PDF'] != "") {
                    unlink(PDF_LOC_ABS . $pdf_row['D_PDF']);
                }
                $sql_pdf = "UPDATE tbl_Description_PDF SET D_BL_ID ='" . $BL_ID . "', D_PDF='" . mysql_real_escape_string($random_name) . "', D_PDF_Title = '" . mysql_real_escape_string($title) . "' WHERE D_id= $d_id_update";
                $result = mysql_query($sql_pdf);
                if ($result) {
                    $_SESSION['success'] = 1;
                } else {
                    $_SESSION['error'] = 1;
                }
            }
        }
    } else {
        $sql_pdf = "UPDATE tbl_Description_PDF SET D_PDF_Title='$title' where D_id= $d_id_update";
        $result = mysql_query($sql_pdf);
        if ($result) {
            $_SESSION['success'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
    }
    // TRACK DATA ENTRY
    $id = $BL_ID;
    Track_Data_Entry('Listing', $id, 'Upload A PDF', '', 'Update', 'super admin');
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit;
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del_pdf') {
    $select_pdf = "SELECT D_PDF FROM tbl_Description_PDF WHERE D_id= " . $_REQUEST['delete'] . " AND D_BL_ID ='" . $BL_ID . "'";
    $result_pdf = mysql_query($select_pdf);
    $pdf_row = mysql_fetch_assoc($result_pdf);
    if ($pdf_row['D_PDF'] != "") {
        unlink(PDF_LOC_ABS . $pdf_row['D_PDF']);
    }
    $sql = "DELETE FROM tbl_Description_PDF WHERE D_id = '" . $_REQUEST['delete'] . "'";
    $result = mysql_query($sql);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Upload A PDF', '', 'Delete PDF', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit;
}

if (isset($_POST['op']) && $_POST['op'] == 'save_desc_text') {
    $sql = "tbl_Business_Listing SET
            BL_Description = '" . encode_strings($_POST['maindescription'], $db) . "'";
    if ($BL_ID > 0) {
        $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Description Text', '', 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $id, 'Description Text', '', 'Add', 'super admin');
    }

    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);

    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save_trip_advisor') {

    $sql = "tbl_Business_Listing SET BL_Trip_Advisor = '" . encode_strings($_POST['tripadvisor'], $db) . "'";
    if ($BL_ID > 0) {
        $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Trip Advisor', '', 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $id, 'Trip Advisor', '', 'Add', 'super admin');
    }

    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);

    exit();
}

if (isset($_POST['button33'])) {
    $menutype = $_REQUEST['menu-type'];
    $menutypeid = $_REQUEST['menu-des-type'];
    $title = $_REQUEST['title'];
    $id = $_REQUEST['bl_id'];
    $des_acc_id = $_REQUEST['acc_id'];
    $descriptionNEW = $_REQUEST['descriptionNEW'];
    $price = ($_REQUEST['priceNEW']) ? $_REQUEST['priceNEW'] : '0.00';
    $sql_check = "Select BFM_ID, BFM_BL_ID from tbl_Business_Feature_Menu where BFM_ID = '$menutypeid' AND BFM_BL_ID = '$id'";
    $result_check = mysql_query($sql_check);
    $count_des = mysql_num_rows($result_check);
    $sql_op = "tbl_Business_Feature_Menu SET                     
                BFM_Title = '$title',
                BFM_Description = '$descriptionNEW',
                BFM_Price = '$price'";
    if ($count_des > 0) {
        $sql_op = "UPDATE " . $sql_op . " WHERE BFM_ID ='$menutypeid' AND BFM_BL_ID = '$id'";
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $id, 'Menu', $des_acc_id, 'Update Item', 'super admin');
    } else {
        $sql_op = "INSERT " . $sql_op . " ,BFM_Type  = '$menutype', BFM_BL_ID = '$id'";
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $id, 'Menu', $des_acc_id, 'Add Item', 'super admin');
        update_pointsin_business_tbl($id);
    }
    $result_op = mysql_query($sql_op);
    if ($result_op) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit;
}

if (isset($_REQUEST['save_des'])) {
    $des_type_id = $_REQUEST['menu-des-type'];
    $des_bl_id = $_REQUEST['menu-des-id'];
    $des_acc_id = $_REQUEST['acc_id'];
    $description = $_REQUEST['descriptionNEW_TYPE'];
    $price = ($_REQUEST['price']) ? $_REQUEST['price'] : '0.00';
    $sql_check = "Select BFMD_ID from tbl_Business_Feature_Menu_Description where BFMD_Type='$des_type_id' AND BFMD_BL_ID= $BL_ID";
    $result_check = mysql_query($sql_check);
    $count_des = mysql_num_rows($result_check);
    $sql_op = "tbl_Business_Feature_Menu_Description SET 						
                BFMD_Description = '$description'";
    if ($count_des > 0) {
        $sql_op = "UPDATE " . $sql_op . " WHERE BFMD_BL_ID = '$des_bl_id' AND BFMD_Type = '$des_type_id'";
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Menu', $des_acc_id, 'Update Description', 'super admin');
    } else {
        $sql_op = "INSERT " . $sql_op . " ,BFMD_BL_ID = '$des_bl_id', 
                    BFMD_Type = '$des_type_id' ";
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Menu', $des_acc_id, 'Add Description', 'super admin');
    }
    $result_op = mysql_query($sql_op);
    if ($result_op) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);

    exit;
}

if (isset($_REQUEST['updateTrue']) && $_REQUEST['updateTrue'] == 'UpdateValue') {
    $BFA_TITLE_Update = $_REQUEST['titleCategoryAccordion'];
    $BFA_DESC_Update = $_REQUEST['titleCategorytextareaAccordion'];
    $BFA_BL_ID_Update = $_REQUEST['BFA_ID_UPDATE'];
    $BFA_BL_ID = $_REQUEST['BL_ID'];
    $update = "UPDATE tbl_Business_Feature_About SET BFA_Title = '$BFA_TITLE_Update', BFA_Description = '$BFA_DESC_Update'
    WHERE BFA_ID = '$BFA_BL_ID_Update'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['BL_ID'];
        Track_Data_Entry('Listing', $id, 'About Us', $_REQUEST['acc'], 'Update Page', 'super admin');
        //update points only for listing
        update_pointsin_business_tbl($BFA_BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit();
} elseif (isset($_REQUEST['InsertTrue']) && $_REQUEST['InsertTrue'] == 'InsertValue') {
    $BFA_TITLE = $_REQUEST['titleCategory'];
    $BFA_DESC = $_REQUEST['titleCategorytextarea'];
    $BFA_BL_ID = $_REQUEST['BL_ID'];
    $sqlMax = "SELECT MAX(BFA_Order) FROM tbl_Business_Feature_About WHERE BFA_BL_ID = '$BFA_BL_ID'";
    $resultMax = mysql_query($sqlMax);
    $rowMax = mysql_fetch_row($resultMax);
    $sql = "INSERT tbl_Business_Feature_About SET 
            BFA_Title = '$BFA_TITLE', 
            BFA_BL_ID = '$BFA_BL_ID', 
            BFA_Description = '$BFA_DESC',
            BFA_Order = '" . ($rowMax[0] + 1) . "'";
    $result = mysql_query($sql);
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['BL_ID'];
        Track_Data_Entry('Listing', $id, 'About Us', '', 'Add Page', 'super admin');
        //update points only for listing
        update_pointsin_business_tbl($BFA_BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit();
}

$sql_abutus = "SELECT BFAMP_Photo_545X335, BFAMP_Photo from tbl_Business_Feature_About_Main_Photo where BFAMP_BL_ID= '$BL_ID'";
$result_abutus = mysql_query($sql_abutus, $db) or die("Invalid query: $sql_abutus -- " . mysql_error());

if (isset($_POST['op']) && $_POST['op'] == 'save_about_image') {
    $acc = (isset($_POST['acc'])) ? $_POST['acc'] : '';
    require_once '../include/picUpload.inc.php';
    if ($_POST['image_bank'] == "") {
        if ($_POST['main_image'] == "1") {
            // last @param 2 = About Us Main Image
            $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 2, 'Listing', $BL_ID);
            if (is_array($pic)) {
                if (mysql_num_rows($result_abutus) > 0) {
                   $sql = "UPDATE tbl_Business_Feature_About_Main_Photo SET
                            BFAMP_Photo_710X440 = '" . encode_strings($pic['0']['0'], $db) . "',
                            BFAMP_Photo_545X335 = '" . encode_strings($pic['0']['1'], $db) . "'
                            WHERE BFAMP_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                } else {
                   $sql = "INSERT tbl_Business_Feature_About_Main_Photo SET  
                            BFAMP_BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "',
                            BFAMP_Photo_710X440 = '" . encode_strings($pic['0']['0'], $db) . "',
                            BFAMP_Photo_545X335 = '" . encode_strings($pic['0']['1'], $db) . "'";
                }
                $pic_id = $pic['1'];
            }
        } else {
            // last @param 3 = About Us Accordion Image
            $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 3, 'Listing', $BL_ID);
            if (is_array($pic)) {
                $sql = "INSERT tbl_Business_Feature_About_Photo SET  
                        BFAP_Title = '" . encode_strings($_POST['title'], $db) . "',
                        BFAP_BFA_ID = '" . encode_strings($_POST['aid'], $db) . "',
                        BFAP_Photo_285X210 = '" . encode_strings($pic['0']['0'], $db) . "',
                        BFAP_Photo_470X320 = '" . encode_strings($pic['0']['1'], $db) . "'";
                $pic_id = $pic['1'];
            }
        }
    } else {
        $pic_id = $_POST['image_bank'];
        if ($_POST['main_image'] == "1") {
            // last @param 2 = About Us Main Image
            $pic = Upload_Pic_Library($pic_id, 2);
            if (is_array($pic)) {
                Update_Image_Bank_Listings($pic_id, $BL_ID);
                if (mysql_num_rows($result_abutus) > 0) {
                    $sql = "UPDATE tbl_Business_Feature_About_Main_Photo SET
                            BFAMP_Photo_710X440 = '" . encode_strings($pic['0'], $db) . "',
                            BFAMP_Photo_545X335 = '" . encode_strings($pic['1'], $db) . "'
                            WHERE BFAMP_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                } else {
                    $sql = "INSERT tbl_Business_Feature_About_Main_Photo SET  
                            BFAMP_BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "',
                            BFAMP_Photo_710X440 = '" . encode_strings($pic['0'], $db) . "',
                            BFAMP_Photo_545X335 = '" . encode_strings($pic['1'], $db) . "'";
                }
            }
        } else {
            

            // last @param 3 = About Us Accordion Image
            $pic = Upload_Pic_Library($pic_id, 3);
            if (is_array($pic)) {
                Update_Image_Bank_Listings($pic_id, $BL_ID);
                $sql = "INSERT tbl_Business_Feature_About_Photo SET  
                        BFAP_Title = '" . encode_strings($_POST['title'], $db) . "',
                        BFAP_BFA_ID = '" . encode_strings($_POST['aid'], $db) . "',
                        BFAP_Photo_285X210 = '" . encode_strings($pic['0'], $db) . "',
                        BFAP_Photo_470X320 = '" . encode_strings($pic['1'], $db) . "'";
            }
        }
    }
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $id = mysql_insert_id();
    if ($_POST['main_image'] == "1") {
        $id = "About Us Main Image";
    }
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'About Us', $acc, 'Update Image', 'super admin');
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        if ($pic_id > 0) {
            //Image usage from image bank
            imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_About_Photo', $id);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'edit_about_us_photo') {
    $BFAP_ID = $_POST['bfap_id'];
    $acc = (isset($_POST['acc'])) ? $_POST['acc'] : '';
    $text = $_POST['titleName' . $BFAP_ID];
    $sql = "UPDATE tbl_Business_Feature_About_Photo SET BFAP_Title = '$text' WHERE BFAP_ID = '$BFAP_ID'";

    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'About Us', $acc, 'Update Page', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);

    exit();
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del_about_main_photo') {
    $bl_id = $_REQUEST['bl_id'];
    $sql = "DELETE FROM tbl_Business_Feature_About_Main_Photo WHERE BFAMP_BL_ID = '$bl_id'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['bl_id'];
        Track_Data_Entry('Listing', $id, 'About Us', '', 'Delete Photo', 'super admin');
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_About_Photo', "About Us Main Image");
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit();
}

if ($_GET['op'] == 'del_about_photo') {
    $sql = "DELETE FROM tbl_Business_Feature_About_Photo WHERE BFAP_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_About_Photo', $_REQUEST['id']);
    } else {
        $_SESSION['delete_error'] = 1;
    }

    $sql = "UPDATE tbl_Business_Feature_About SET BFA_Order = BFA_Order - 1 
            WHERE BFA_Order > '" . encode_strings($row['BFA_Order'], $db) . "' AND 
            BFA_BL_ID = '" . $row['BFA_BL_ID'] . "'";
    mysql_query($sql, $db);

    header("Location: listings-preview.php?bl_id=" . $BL_ID);
    exit();
}

if (isset($_POST['button_description'])) {

    $daily_description = $_POST['daily-special-description'];
    $bl_id_description = $_POST['bl_id'];
    $sql_daily_des = mysql_query("SELECT BFDSD_ID from tbl_Business_Feature_Daily_Specials_Description WHERE BFDSD_BL_ID = '$BL_ID'");
    $count = mysql_num_rows($sql_daily_des);
    if ($count > 0) {
        $sql_daily_des_update = "UPDATE tbl_Business_Feature_Daily_Specials_Description SET BFDSD_Description = '$daily_description' WHERE BFDSD_BL_ID = '$bl_id_description'";
        $result = mysql_query($sql_daily_des_update);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Daily Features', '', 'Update Description', 'super admin');
    } else {
        $sql_daily_des_insert = "INSERT tbl_Business_Feature_Daily_Specials_Description SET BFDSD_BL_ID= '$bl_id_description',BFDSD_Description = '$daily_description'";
        $result = mysql_query($sql_daily_des_insert);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Daily Features', '', 'Add Description', 'super admin');
    }
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);

    exit;
}

if (isset($_POST['op']) && $_POST['op'] == 'save_daily_special') {
    $select = "SELECT BFDS_ID FROM tbl_Business_Feature_Daily_Specials 
                WHERE BFDS_BL_ID = '" . encode_strings($_POST['bl_id'], $db) . "' AND BFDS_Day = '" . encode_strings($_POST['weekno'], $db) . "'";
    $sql_check = mysql_query($select) or die(mysql_error());
    $rowDaily = mysql_fetch_array($sql_check);
    $count = mysql_num_rows($sql_check);
    require_once '../include/picUpload.inc.php';
    if ($_POST['image_bank'] == "") {
        // last @param 19 = Daily Features
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 19, 'Listing', $BL_ID);
        if ($count > 0) {
            if (is_array($pic)) {
                $sql = "UPDATE tbl_Business_Feature_Daily_Specials SET 
                        BFDS_Title = '" . encode_strings($_POST['title'], $db) . "', 
                        BFDS_Description = '" . encode_strings($_POST['description'], $db) . "', 
                        BFDS_Photo = '" . encode_strings($pic['0']['0'], $db) . "'
                        WHERE BFDS_BL_ID = '" . encode_strings($_POST['bl_id'], $db) . "' AND BFDS_Day = '" . encode_strings($_POST['weekno'], $db) . "'";
                $pic_id = $pic['1'];
            } else {
                $sql = "UPDATE tbl_Business_Feature_Daily_Specials SET 
                        BFDS_Title = '" . encode_strings($_POST['title'], $db) . "', 
                        BFDS_Description = '" . encode_strings($_POST['description'], $db) . "'
                        WHERE BFDS_BL_ID = '" . encode_strings($_POST['bl_id'], $db) . "' AND BFDS_Day = '" . encode_strings($_POST['weekno'], $db) . "'";
            }
            // TRACK DATA ENTRY
            $id = $BL_ID;
            Track_Data_Entry('Listing', $id, 'Daily Features', '', 'Update Item', 'super admin');
        } else {
            $sql = "INSERT tbl_Business_Feature_Daily_Specials SET 
                    BFDS_Title = '" . encode_strings($_POST['title'], $db) . "', 
                    BFDS_Description = '" . encode_strings($_POST['description'], $db) . "', 
                    BFDS_Photo = '" . encode_strings($pic['0']['0'], $db) . "', 
                    BFDS_BL_ID = '" . encode_strings($_POST['bl_id'], $db) . "', 
                    BFDS_Day = '" . encode_strings($_POST['weekno'], $db) . "'";
            $pic_id = $pic['1'];
            // TRACK DATA ENTRY
            $id = $BL_ID;
            Track_Data_Entry('Listing', $id, 'Daily Features', '', 'Add Item', 'super admin');
        }
    } else {
        $pic_id = $_POST['image_bank'];
        // last @param 19 = Daily Features
        $pic = Upload_Pic_Library($pic_id, 19);
        Update_Image_Bank_Listings($pic_id, $BL_ID);
        if ($count > 0) {
            if (is_array($pic)) {
                $sql = "UPDATE tbl_Business_Feature_Daily_Specials SET 
                        BFDS_Title = '" . encode_strings($_POST['title'], $db) . "', 
                        BFDS_Description = '" . encode_strings($_POST['description'], $db) . "', 
                        BFDS_Photo = '" . encode_strings($pic['0'], $db) . "'
                        WHERE BFDS_BL_ID = '" . encode_strings($_POST['bl_id'], $db) . "' AND BFDS_Day = '" . encode_strings($_POST['weekno'], $db) . "'";
            }
            // TRACK DATA ENTRY
            $id = $BL_ID;
            Track_Data_Entry('Listing', $id, 'Daily Features', '', 'Update Item', 'super admin');
        } else {
            $sql = "INSERT tbl_Business_Feature_Daily_Specials SET 
                    BFDS_Title = '" . encode_strings($_POST['title'], $db) . "', 
                    BFDS_Description = '" . encode_strings($_POST['description'], $db) . "', 
                    BFDS_Photo = '" . encode_strings($pic['0'], $db) . "', 
                    BFDS_BL_ID = '" . encode_strings($_POST['bl_id'], $db) . "', 
                    BFDS_Day = '" . encode_strings($_POST['weekno'], $db) . "'";
            // TRACK DATA ENTRY
            $id = $BL_ID;
            Track_Data_Entry('Listing', $id, 'Daily Features', '', 'Add Item', 'super admin');
        }
    }
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    if ($count > 0) {
        $id = $rowDaily['BFDS_ID'];
    } else {
        $id = mysql_insert_id();
    }
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        if ($pic_id > 0) {
            //Image usage from image bank
            imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Daily_Features', $id);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);

    exit();
}

if (isset($_GET['op']) && $_GET['op'] == 'del_photo') {
    $sql = "UPDATE tbl_Business_Feature_Daily_Specials SET BFDS_Photo = '' 
            WHERE BFDS_BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "' AND BFDS_Day = '" . encode_strings($_REQUEST['bds_id_photo'], $db) . "'";
    $result = mysql_query($sql);
    if ($result) {
        $_SESSION['delete'] = 1;
        $select = "SELECT BFDS_ID FROM tbl_Business_Feature_Daily_Specials 
                    WHERE BFDS_BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "' AND BFDS_Day = '" . encode_strings($_REQUEST['bds_id_photo'], $db) . "'";
        $sql_check = mysql_query($select) or die(mysql_error());
        $rowDaily = mysql_fetch_array($sql_check);
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Daily_Features', $rowDaily['BFDS_ID']);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Daily Features', '', 'Delete Image', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);

    exit();
}

if (isset($_GET['op']) && $_GET['op'] == 'del_daily_item') {
    $sql = "DELETE FROM tbl_Business_Feature_Daily_Specials WHERE BFDS_BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "' AND BFDS_Day = '" . encode_strings($_REQUEST['bds_item_id'], $db) . "'";
    $result = mysql_query($sql);
    if ($result) {
        $_SESSION['delete'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        $select = "SELECT BFDS_ID FROM tbl_Business_Feature_Daily_Specials 
                    WHERE BFDS_BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "' AND BFDS_Day = '" . encode_strings($_REQUEST['bds_item_id'], $db) . "'";
        $sql_check = mysql_query($select) or die(mysql_error());
        $rowDaily = mysql_fetch_array($sql_check);
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Daily_Features', $rowDaily['BFDS_ID']);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Daily Features', '', 'Delete Item', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);

    exit();
}

if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Street, BL_Town, BL_Province, BL_PostalCode, BL_Contact, BL_Phone, BL_Toll_Free, BL_Email, BL_Website, BL_Listing_Title 
            FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing_entertainment = mysql_fetch_assoc($result);
    $BID = $rowListing_entertainment['B_ID'];
}

if (isset($_POST['entertainment-description-btn'])) {

    $daily_description = $_POST['entertainment-description'];
    $bl_id_description = $_POST['ent_des_bl_id'];
    $sql_daily_des = mysql_query("SELECT BFED_ID from tbl_Business_Feature_Entertainment_Description WHERE BFED_BL_ID= '$bl_id_description'");
    $count = mysql_num_rows($sql_daily_des);
    if ($count > 0) {
        $sql_daily_des_update = "UPDATE tbl_Business_Feature_Entertainment_Description SET BFED_Description='$daily_description' WHERE BFED_BL_ID= '$bl_id_description'";
        $result = mysql_query($sql_daily_des_update);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Entertainment', '', 'Update Description', 'super admin');
    } else {
        $sql_daily_des_insert = "INSERT tbl_Business_Feature_Entertainment_Description SET BFED_BL_ID= '$bl_id_description',BFED_Description='$daily_description'";
        $result = mysql_query($sql_daily_des_insert);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Entertainment', '', 'Add Description', 'super admin');
    }
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit;
}

if (isset($_POST['op']) && $_POST['op'] == 'save_entertainment') {
    $title = $_REQUEST['title'];
    $description = $_REQUEST['description'];
    $hour = $_REQUEST['hour'];
    $end_hour = $_REQUEST['end_hour'];
    $eventdate = (($_REQUEST['eventdate'] != "") ? $_REQUEST['eventdate'] : "0000-00-00") . " " . $hour;
    $bl_id = $_REQUEST['bl_id'];
    $sql_region_event = "SELECT BL_ID, BLCR_BLC_R_ID, BL_Points, R_Parent, RM_Parent, RM_Child
                        FROM tbl_Business_Listing
                        INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                        INNER JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                        LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID
                        WHERE BL_ID ='$bl_id'
                        GROUP BY RM_Child";
    $result_region_event = mysql_query($sql_region_event);
    $event_regions = array();
    while ($r = mysql_fetch_assoc($result_region_event)) {
        // add region parent as well if available
        if ($r['R_Parent'] != 0 && !in_array($r['R_Parent'], $event_regions)) {
            $event_regions[] = $r['RM_Child'];
        }
    }
    $event_regions = implode(",", $event_regions);
    $location_detail = $rowListing_entertainment['BL_Street'] . ',' . $rowListing_entertainment['BL_Town'] . ',' . $rowListing_entertainment['BL_Province'] . ',' . $rowListing_entertainment['BL_PostalCode'];
    require_once '../include/picUpload.inc.php';
    if ($_POST['image_bank'] == "") {
        // last @param 20 = entertainment
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 20, 'Listing', $BL_ID);
        if (is_array($pic)) {
            $sql = "INSERT tbl_Business_Feature_Entertainment_Acts SET 
                    BFEA_BL_ID = '" . encode_strings($bl_id, $db) . "', 
                    BFEA_Name = '" . encode_strings($title, $db) . "',
                    BFEA_Photo = '" . encode_strings($pic['0']['0'], $db) . "',
                    BFEA_Description = '" . encode_strings($description, $db) . "',
                    BFEA_Time = '" . encode_strings($eventdate, $db) . "',
                    BFEA_End_Time = '" . encode_strings($end_hour, $db) . "'";
            $pic_id = $pic['1'];
        } else {
            $sql = "INSERT tbl_Business_Feature_Entertainment_Acts SET 
                    BFEA_BL_ID = '" . encode_strings($bl_id, $db) . "', 
                    BFEA_Name = '" . encode_strings($title, $db) . "',
                    BFEA_Description = '" . encode_strings($description, $db) . "',
                    BFEA_Time = '" . encode_strings($eventdate, $db) . "',
                    BFEA_End_Time = '" . encode_strings($end_hour, $db) . "'";
        }
    } else {
        $pic_id = $_POST['image_bank'];
        $ext = explode(".", $pic);
        $myLink = mt_rand(1000, 9999999);
        // last @param 20 = Entertainment
        $pic = Upload_Pic_Library($pic_id, 20);
        if (is_array($pic)) {
            Update_Image_Bank_Listings($pic_id, $BL_ID);
            $sql = "INSERT tbl_Business_Feature_Entertainment_Acts SET 
                    BFEA_BL_ID = '" . encode_strings($bl_id, $db) . "', 
                    BFEA_Name = '" . encode_strings($title, $db) . "',
                    BFEA_Photo = '" . encode_strings($pic['0'], $db) . "',
                    BFEA_Description = '" . encode_strings($description, $db) . "',
                    BFEA_Time = '" . encode_strings($eventdate, $db) . "',
                    BFEA_End_Time = '" . encode_strings($end_hour, $db) . "'";
        }
    }
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $id = mysql_insert_id();
    $sql_main_events = "INSERT Events_master SET 
                        E_Region_ID = '" . encode_strings($event_regions, $db) . "',
                        E_BL_Event_ID = '" . encode_strings($id, $db) . "',
                        Title = '" . encode_strings($title, $db) . "', 
                        EventType = '" . encode_strings(1, $db) . "',
                        ShortDesc = '" . encode_strings(substr($description, 0, 95), $db) . "', 
                        Content = '" . encode_strings(substr($description, 0, 700), $db) . "', 
                        EventDateStart = '" . encode_strings(($_REQUEST['eventdate'] != "") ? $_REQUEST['eventdate'] : "0000-00-00", $db) . "',
                        EventStartTime = '" . encode_strings($hour, $db) . "', 
                        EventEndTime = '" . encode_strings($end_hour, $db) . "', 
                        LocationList = '" . encode_strings($location_detail, $db) . "', 
                        StreetAddress = '" . encode_strings($rowListing_entertainment['BL_Street'], $db) . "', 
                        ContactName = '" . encode_strings($rowListing_entertainment['BL_Contact'], $db) . "', 
                        ContactPhone = '" . encode_strings($rowListing_entertainment['BL_Phone'], $db) . "', 
                        ContactPhoneTollFree = '" . encode_strings($rowListing_entertainment['BL_Toll_Free'], $db) . "', 
                        Email = '" . encode_strings($rowListing_entertainment['BL_Email'], $db) . "', 
                        WebSiteLink = '" . encode_strings($rowListing_entertainment['BL_Website'], $db) . "'";
    $result_event = mysql_query($sql_main_events, $db) or die("Invalid query: $sql_main_events -- " . mysql_error());
    if ($result || $result_event) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        if ($pic_id > 0) {
            //Image usage from image bank
            imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Entertainment', $id);
        }
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Entertainment', '', 'Add Event', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);

    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'edit_entertainment') {
    $acc = $_REQUEST['acc'];
    $title = $_REQUEST['title'];
    $description = $_REQUEST['description'];
    $hour = $_REQUEST['hour'];
    $end_hour = $_REQUEST['end_hour'];
    $eventdate = (($_REQUEST['eventdate'] != "") ? $_REQUEST['eventdate'] : "0000-00-00") . " " . $hour;
    $BFEA_ID = $_REQUEST['BFEA_ID'];
    $bl_id = $_REQUEST['bl_id'];
    $sql_region_event = "SELECT BL_ID, BLCR_BLC_R_ID, BL_Points, R_Parent, RM_Parent, RM_Child
                        FROM tbl_Business_Listing
                        INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                        INNER JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                        LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID
                        WHERE BL_ID ='$bl_id'
                        GROUP BY RM_Child";
    $result_region_event = mysql_query($sql_region_event);
    $event_regions = array();
    while ($r = mysql_fetch_assoc($result_region_event)) {
        // add region parent as well if available
        if ($r['R_Parent'] != 0 && !in_array($r['R_Parent'], $event_regions)) {
            $event_regions[] = $r['RM_Child'];
        }
    }
    $event_regions = implode(",", $event_regions);
    $location_detail = $rowListing_entertainment['BL_Street'] . ',' . $rowListing_entertainment['BL_Town'] . ',' . $rowListing_entertainment['BL_Province'] . ',' . $rowListing_entertainment['BL_PostalCode'];
    require_once '../include/picUpload.inc.php';
    $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 20, 'Listing', $BL_ID);
    if ($_POST['image_bank'] == "") {
        if (is_array($pic)) {
            $sql = "UPDATE tbl_Business_Feature_Entertainment_Acts SET  
                    BFEA_Name = '" . encode_strings($title, $db) . "', 
                    BFEA_Photo = '" . encode_strings($pic['0']['0'], $db) . "', 
                    BFEA_Description = '" . encode_strings($description, $db) . "', 
                    BFEA_Time = '" . encode_strings($eventdate, $db) . "',
                    BFEA_End_Time = '" . encode_strings($end_hour, $db) . "'  
                    WHERE BFEA_ID = '" . encode_strings($BFEA_ID, $db) . "'";
            $pic_id = $pic['1'];
        } else {
            $sql = "UPDATE tbl_Business_Feature_Entertainment_Acts SET  
                    BFEA_Name = '" . encode_strings($title, $db) . "', 
                    BFEA_Description = '" . encode_strings($description, $db) . "', 
                    BFEA_Time = '" . encode_strings($eventdate, $db) . "',
                    BFEA_End_Time = '" . encode_strings($end_hour, $db) . "'  
                    WHERE BFEA_ID = '" . encode_strings($BFEA_ID, $db) . "'";
        }
    } else {
        $pic_id = $_POST['image_bank'];
        $ext = explode(".", $pic);
        $myLink = mt_rand(1000, 9999999);
        // last @param 20 = Entertainment
        $pic = Upload_Pic_Library($pic_id, 20);
        if (is_array($pic)) {
            Update_Image_Bank_Listings($pic_id, $BL_ID);
            $sql = "UPDATE tbl_Business_Feature_Entertainment_Acts SET  
                    BFEA_Name = '" . encode_strings($title, $db) . "', 
                    BFEA_Photo = '" . encode_strings($pic['0'], $db) . "', 
                    BFEA_Description = '" . encode_strings($description, $db) . "', 
                    BFEA_Time = '" . encode_strings($eventdate, $db) . "',
                    BFEA_End_Time = '" . encode_strings($end_hour, $db) . "' 
                    WHERE BFEA_ID = '" . encode_strings($BFEA_ID, $db) . "'";
        }
    }
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $sql_main_events = "UPDATE Events_master SET 
                        E_Region_ID = '" . encode_strings($event_regions, $db) . "',
                        Title = '" . encode_strings($title, $db) . "', 
                        EventType = '" . encode_strings(1, $db) . "',
                        ShortDesc = '" . encode_strings(substr($description, 0, 95), $db) . "', 
                        Content = '" . encode_strings(substr($description, 0, 700), $db) . "', 
                        EventDateStart = '" . encode_strings(($_REQUEST['eventdate'] != "") ? $_REQUEST['eventdate'] : "0000-00-00", $db) . "',
                        EventStartTime = '" . encode_strings($hour, $db) . "', 
                        EventEndTime = '" . encode_strings($end_hour, $db) . "', 
                        LocationList = '" . encode_strings($location_detail, $db) . "', 
                        StreetAddress = '" . encode_strings($rowListing_entertainment['BL_Street'], $db) . "', 
                        ContactName = '" . encode_strings($rowListing_entertainment['BL_Contact'], $db) . "', 
                        ContactPhone = '" . encode_strings($rowListing_entertainment['BL_Phone'], $db) . "', 
                        ContactPhoneTollFree = '" . encode_strings($rowListing_entertainment['BL_Toll_Free'], $db) . "', 
                        Email = '" . encode_strings($rowListing_entertainment['BL_Email'], $db) . "', 
                        WebSiteLink = '" . encode_strings($rowListing_entertainment['BL_Website'], $db) . "'
                        WHERE E_BL_Event_ID = '" . encode_strings($BFEA_ID, $db) . "'";
    $result_event = mysql_query($sql_main_events, $db) or die("Invalid query: $sql_main_events -- " . mysql_error());
    if ($result || $result_event) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        if ($pic_id > 0) {
            $id = $BFEA_ID;
            //Image usage from image bank
            imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Entertainment', $id);
        }
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Entertainment', $acc, 'Update Event', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);

    exit();
}

if (isset($_GET['op']) && $_GET['op'] == 'del_event_photo') {
    $BFEA_ID_Photo = $_REQUEST['bfea_id'];
    $sql = "UPDATE tbl_Business_Feature_Entertainment_Acts SET BFEA_Photo ='' WHERE BFEA_ID = '$BFEA_ID_Photo'";
    $result = mysql_query($sql);
    if ($result) {
        $_SESSION['delete'] = 1;
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Entertainment', $BFEA_ID_Photo);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Entertainment', '', 'Delete Image', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);

    exit();
}

if (isset($_GET['op']) && $_GET['op'] == 'del_event') {
    $bl_id = $_REQUEST['bl_id'];
    $BFEA_ID = $_REQUEST['bfea_id'];
    $result = mysql_query("DELETE FROM tbl_Business_Feature_Entertainment_Acts WHERE BFEA_ID = '$BFEA_ID'") or die(mysql_error());
    $result_main_events = mysql_query("DELETE FROM Events_master WHERE E_BL_Event_ID = '$BFEA_ID'") or die(mysql_error());
    if ($result || $result_main_events) {
        $_SESSION['delete'] = 1;
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Entertainment', $BFEA_ID);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Entertainment', '', 'Delete Event', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
//update points only for listing
    update_pointsin_business_tbl($bl_id);
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);

    exit();
}

if ($_POST['op'] == 'save_photo') {
    require_once '../include/picUpload.inc.php';
    // last @param 23 = Agenda
    $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 23, 'Listing', $BL_ID);
    $sql_daily_des = mysql_query("select * from tbl_Business_Feature_Agendas_Minutes_Main_Photo where BFAMMP_BL_ID= '" . encode_strings($_POST['bl_id'], $db) . "'");
    $daily_des_img = mysql_fetch_assoc($sql_daily_des);
    $count = mysql_num_rows($sql_daily_des);
    if ($_POST['image_bank'] == "") {
        if (is_array($pic)) {
            if ($count > 0) {
                $sql = "UPDATE tbl_Business_Feature_Agendas_Minutes_Main_Photo SET 
                        BFAMMP_Photo = '" . encode_strings($pic['0']['0'], $db) . "'
                        WHERE BFAMMP_BL_ID= '" . encode_strings($_POST['bl_id'], $db) . "'";
                $pic_id = $pic['1'];
                // TRACK DATA ENTRY
                $id = $BL_ID;
                Track_Data_Entry('Listing', $id, 'Agendas & Minutes', '', 'Update Main Image', 'super admin');
            } else {
                $sql = "INSERT tbl_Business_Feature_Agendas_Minutes_Main_Photo SET 
                        BFAMMP_BL_ID= '" . encode_strings($_POST['bl_id'], $db) . "',
                        BFAMMP_Photo = '" . encode_strings($pic['0']['0'], $db) . "'";
                // TRACK DATA ENTRY
                $id = $BL_ID;
                Track_Data_Entry('Listing', $id, 'Agendas & Minutes', '', 'Add Main Image', 'super admin');
            }
        }
    } else {
        $pic_id = $_POST['image_bank'];
        // last @param 23 = About Us Main Image
        $pic = Upload_Pic_Library($pic_id, 23);
        if (is_array($pic)) {
            Update_Image_Bank_Listings($pic_id, $BL_ID);
            if ($count > 0) {
                $sql = "UPDATE tbl_Business_Feature_Agendas_Minutes_Main_Photo SET 
                        BFAMMP_Photo = '" . encode_strings($pic['0'], $db) . "'
                        WHERE BFAMMP_BL_ID= '" . encode_strings($_POST['bl_id'], $db) . "'";
                // TRACK DATA ENTRY
                $id = $BL_ID;
                Track_Data_Entry('Listing', $id, 'Agendas & Minutes', '', 'Update Main Image', 'super admin');
            } else {
                $sql = "INSERT tbl_Business_Feature_Agendas_Minutes_Main_Photo SET 
                        BFAMMP_BL_ID= '" . encode_strings($_POST['bl_id'], $db) . "',
                        BFAMMP_Photo = '" . encode_strings($pic['0'], $db) . "'";
                // TRACK DATA ENTRY
                $id = $BL_ID;
                Track_Data_Entry('Listing', $id, 'Agendas & Minutes', '', 'Add Main Image', 'super admin');
            }
        }
    }
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    if ($count > 0) {
        $id = $daily_des_img['BFAMMP_ID'];
    } else {
        $id = mysql_insert_id();
    }
    if ($result) {
        $_SESSION['success'] = 1;
        if ($pic_id > 0) {
            //Image usage from image bank
            imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Agenda', $id);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);

    exit;
}

if ($_GET['op'] == 'del_photo') {
    $update = "DELETE FROM tbl_Business_Feature_Agendas_Minutes_Main_Photo 
        WHERE BFAMMP_BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Agendas & Minutes', '', 'Delete Main Image', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);

    exit;
}

if ($_POST['op_1st'] == 'save_agenda_minutes') {
    $BL_ID = $_POST['bl_id'];
    $meeting_date = $_POST['meeting_date'];
    $BID = $_POST['bid'];
    $sql_meeting = "INSERT tbl_Business_Feature_Agendas_Minutes SET
                    BFAM_Date = '$meeting_date',
                    BFAM_BL_ID = '$BL_ID',
                    BFAM_B_ID = '$BID'";
    $file_size_agenda = $_FILES['file_agenda']['size'];
    $file_size_minute = $_FILES['file_minute']['size'];
    $max_filesize = 5342523;
    $pdf_agenda = str_replace(' ', '_', $_FILES['file_agenda']['name']);
    $pdf_minute = str_replace(' ', '_', $_FILES['file_minute']['name']);
    if ($pdf_minute != "") {
        $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_minute;
        $filePathMinute = PDF_LOC_ABS . $random_name;
        if ($_FILES["file_minute"]["type"] == "application/pdf") {
            if ($file_size_minute < $max_filesize) {
                move_uploaded_file($_FILES["file_minute"]["tmp_name"], $filePathMinute);
                $sql_meeting .= ", BFAM_Minute = '" . $random_name . "'";
            }
        }
    }
    if ($pdf_agenda != "") {
        $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_agenda;
        $filePathAgenda = PDF_LOC_ABS . $random_name;
        if ($_FILES["file_agenda"]["type"] == "application/pdf") {
            if ($file_size_agenda < $max_filesize) {
                move_uploaded_file($_FILES["file_agenda"]["tmp_name"], $filePathAgenda);
                $sql_meeting .= ", BFAM_Agenda = '" . $random_name . "'";
            }
        }
    }
    $sql_meeting .= ", BFAM_Keywords = '" . $_POST['new_keyword'] . "'";
    $result = mysql_query($sql_meeting);
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Agendas & Minutes', '', 'Add Agenda & Minutes', 'super admin');
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit;
}

if ($_POST['op'] == 'edit_agenda_minutes') {
    $select = "SELECT BFAM_Minute, BFAM_Agenda FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_ID = '" . encode_strings($_POST['BFAM_ID'], $db) . "'";
    $selRes = mysql_query($select, $db) or die("Invalid query: $select -- " . mysql_error());
    $rowSel = mysql_fetch_assoc($selRes);
    $sql = "UPDATE tbl_Business_Feature_Agendas_Minutes SET BFAM_Date = '" . encode_strings($_POST['meeting_date'], $db) . "'";
    $file_size_agenda = $_FILES['file_agenda']['size'];
    $file_size_minute = $_FILES['file_minute']['size'];
    $max_filesize = 5342523;
    $pdf_agenda = str_replace(' ', '_', $_FILES['file_agenda']['name']);
    $pdf_minute = str_replace(' ', '_', $_FILES['file_minute']['name']);
    if ($pdf_minute != "") {
        $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_minute;
        $filePathMinute = PDF_LOC_ABS . $random_name;
        if ($_FILES["file_minute"]["type"] == "application/pdf") {
            if ($file_size_minute < $max_filesize) {
                if ($rowSel['BFAM_Minute'] != "") {
                    unlink(PDF_LOC_ABS . $rowSel['BFAM_Minute']);
                }
                move_uploaded_file($_FILES["file_minute"]["tmp_name"], $filePathMinute);
                $sql .= ", BFAM_Minute = '" . $random_name . "'";
            }
        }
    }
    if ($pdf_agenda != "") {
        $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_agenda;
        $filePathAgenda = PDF_LOC_ABS . $random_name;
        if ($_FILES["file_agenda"]["type"] == "application/pdf") {
            if ($file_size_agenda < $max_filesize) {
                if ($rowSel['BFAM_Agenda'] != "") {
                    unlink(PDF_LOC_ABS . $rowSel['BFAM_Agenda']);
                }
                move_uploaded_file($_FILES["file_agenda"]["tmp_name"], $filePathAgenda);
                $sql .= ", BFAM_Agenda = '" . $random_name . "'";
            }
        }
    }
    $sql .= ", BFAM_Keywords = '" . encode_strings($_POST['new_keyword'], $db) . "'
                        WHERE BFAM_ID = '" . encode_strings($_POST['BFAM_ID'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Agendas & Minutes', '', 'Update Agenda & Minutes', 'super admin');
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit;
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del_pdf_agenda_minutes') {
    $BL_ID = $_REQUEST['bl_id'];
    $BFAM_ID = ($_REQUEST['del_pdf_age']) ? $_REQUEST['del_pdf_age'] : $_REQUEST['del_pdf_min'];
    $select = "SELECT BFAM_Agenda, BFAM_Minute FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_ID = '" . encode_strings($BFAM_ID, $db) . "'";
    $result = mysql_query($select, $db) or die("Invalid query: $select -- " . mysql_error());
    $PDF_FILE = mysql_fetch_assoc($result);
    $delete = "UPDATE tbl_Business_Feature_Agendas_Minutes SET ";
    if (isset($_REQUEST['del_pdf_age'])) {
        $delete .= "BFAM_Agenda = ''";
        unlink(PDF_LOC_ABS . $PDF_FILE['BFAM_Agenda']);
    } else if (isset($_REQUEST['del_pdf_min'])) {
        $delete .= "BFAM_Minute = ''";
        unlink(PDF_LOC_ABS . $PDF_FILE['BFAM_Minute']);
    }
    $delete .= " WHERE BFAM_ID = '" . encode_strings($BFAM_ID, $db) . "'";
    $result = mysql_query($delete, $db) or die("Invalid query: $delete -- " . mysql_error());
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Agendas & Minutes', '', 'Delete PDF', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);

    exit;
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del_agenda_minutes') {
    $BFAM_ID = $_REQUEST['delete'];
    $BL_ID = $_REQUEST['bl_id'];
    $select = "SELECT BFAM_Agenda, BFAM_Minute FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_ID = '" . encode_strings($BFAM_ID, $db) . "'";
    $result = mysql_query($select, $db) or die("Invalid query: $select -- " . mysql_error());
    $PDF_FILE = mysql_fetch_assoc($result);
    if ($PDF_FILE['BFAM_Agenda'] != "") {
        unlink(PDF_LOC_ABS . $PDF_FILE['BFAM_Agenda']);
    }
    if ($PDF_FILE['BFAM_Minute'] != "") {
        unlink(PDF_LOC_ABS . $PDF_FILE['BFAM_Minute']);
    }
    $delete = "DELETE FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_ID = '" . encode_strings($BFAM_ID, $db) . "'";
    $result = mysql_query($delete, $db) or die("Invalid query: $delete -- " . mysql_error());
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Agendas & Minutes', '', 'Delete Agendas & Minutes', 'super admin');
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);

    exit;
}

if (isset($_POST['button_guest-book'])) {
    $daily_description = $_POST['guest-book-description'];
    $sql_daily_des = mysql_query("SELECT * from tbl_Business_Feature_Guest_Book_Description WHERE BFGB_BL_ID = '$BL_ID'");
    $count = mysql_num_rows($sql_daily_des);
    if ($count > 0) {
        $sql_daily_des_update = "UPDATE tbl_Business_Feature_Guest_Book_Description SET BFGB_Description = '$daily_description' WHERE BFGB_BL_ID = '$BL_ID'";
        $result = mysql_query($sql_daily_des_update);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Guest Book', '', 'Update Description', 'super admin');
    } else {
        $sql_daily_des_insert = "INSERT tbl_Business_Feature_Guest_Book_Description SET BFGB_BL_ID = '$BL_ID', BFGB_Description = '$daily_description'";
        $result = mysql_query($sql_daily_des_insert);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Guest Book', '', 'Add Description', 'super admin');
    }
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);

    exit;
}

if (isset($_POST['Approve'])) {
    $BFGB_ID = $_POST['BFGB_ID'];
    $sql_guest_status_update = "UPDATE tbl_Business_Feature_Guest_Book SET BFGB_Status = '1' WHERE BFGB_ID= '$BFGB_ID'";
    $result = mysql_query($sql_guest_status_update);
    if ($result) {
        update_pointsin_business_tbl($BL_ID);
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Guest Book', '', 'Approve Review', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit;
}

if (isset($_POST['op']) && $_POST['op'] == 'save_region') {
    if ($_POST['bl_id'] > 0) {
        $id = $_POST['bl_id'];
    } else {
        $sqlMax = "SELECT MAX(BL_Number) FROM tbl_Business_Listing WHERE BL_B_ID = '" . encode_strings($_POST['bid'], $db) . "'";
        $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
        $rowMax = mysql_fetch_row($resultMax);
        $sql = "INSERT tbl_Business_Listing SET 
            BL_Listing_Title = '" . encode_strings($_REQUEST['name'], $db) . "',
            BL_Name_SEO = '" . encode_strings($_REQUEST['seoName'], $db) . "',
            BL_Listing_Type = '" . encode_strings($_REQUEST['type'], $db) . "',
            BL_B_ID = '" . encode_strings($_POST['bid'], $db) . "', 
            BL_Number = '" . encode_strings($rowMax[0] + 1, $db) . "', 
            BL_Creation_Date = CURDATE()";
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id($db);
        $features = "SELECT F_ID FROM  `tbl_Feature` WHERE F_Price = 0";
        $result_fe = mysql_query($features, $db);
        while ($free_feature = mysql_fetch_array($result_fe)) {
            $insert_feature = "INSERT tbl_BL_Feature SET 
                        BLF_BL_ID = '" . encode_strings($id, $db) . "',
                        BLF_F_ID = '" . encode_strings($free_feature['F_ID'], $db) . "',
                        BLF_Date = CURDATE(),
                        BLF_Active = 1,
                        BLF_Last_Update = '" . encode_strings($_SESSION['USER_EM'] . '-' . $_SESSION['user_online'] . '-' . $_SESSION['USER_ID'], $db) . "',
                        BLF_Agreement_Status = '" . encode_strings("1", $db) . "'";
            mysql_query($insert_feature, $db);
        }
        for ($season = 1; $season < 5; $season++) {
            $sql_seasons = "INSERT tbl_Business_Listing_Season SET BLS_S_ID = '$season', 
                BLS_BL_ID = '" . encode_strings($id, $db) . "'";
            $res1_seasons = mysql_query($sql_seasons, $db);
        }
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $id, 'Listing Details', '', 'Add Listing', 'super admin');
    }
    $sql = "DELETE FROM tbl_Business_Listing_Category_Region WHERE BLCR_BL_ID = '" . encode_strings($id, $db) . "'";
    mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    if (is_array($_POST['region'])) {
        foreach ($_POST['region'] as $key => $val) {
            $sql = "INSERT INTO tbl_Business_Listing_Category_Region (BLCR_BL_ID, BLCR_BLC_R_ID)
              VALUES('" . encode_strings($id, $db) . "', '" . encode_strings($val, $db) . "')";
            $result = mysql_query($sql, $db);
        }
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $id, 'Listing Details', '', 'Add Region', 'super admin');
    }
    if (!$_REQUEST['newR']) {
        if ($result) {
            $_SESSION['success'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $id);
    exit;
}

if (isset($_REQUEST['delRegion']) && $_REQUEST['delRegion'] > 0) {
    $sql = "DELETE FROM tbl_Business_Listing_Category_Region WHERE BLCR_ID = '" . encode_strings($_REQUEST['delRegion'], $db) . "'";
    $result = mysql_query($sql, $db);
    // TRACK DATA ENTRY
    $id = $BL_ID;
    Track_Data_Entry('Listing', $id, 'Regions', '', 'Delete Region', 'super admin');
    if ($result) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $id);
    exit();
}

// FOR SHOWING REGIONS
if (!in_array('business-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
} elseif (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
    $regionLimit = array();
    $limits = explode(',', $_SESSION['USER_LIMIT']);
    foreach ($limits as $limit) {
        $sql = "SELECT R_ID, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $region = mysql_fetch_assoc($result);
        if (isset($region['R_Type']) && $region['R_Type'] == 1) {
            $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($region['R_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                if ($row['R_ID'] != '' && !in_array($row['R_ID'], $regionLimit)) {
                    $regionLimit[] = $row['R_ID'];
                }
            }
        } else {
            if ($region['R_ID'] != '' && !in_array($region['R_ID'], $regionLimit)) {
                $regionLimit[] = $region['R_ID'];
            }
        }
    }
}
$regionLimitCommaSeparated = rtrim(implode(',', $regionLimit), ',');

if (isset($_POST['op']) && $_POST['op'] == 'save_seasons') {
    $delete = "DELETE FROM tbl_Business_Listing_Season WHERE BLS_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $res = mysql_query($delete, $db);
    foreach ($_POST['season'] as $value) {
        $sql = "INSERT tbl_Business_Listing_Season SET BLS_S_ID = '$value', 
                BLS_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $res1 = mysql_query($sql, $db);
    }
    if ($res) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Seasons', '', 'Add', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save_bokun') {
    $sql = "tbl_Business_Listing SET BL_Bokun = '" . encode_strings($_POST['bokun'], $db) . "'";
    if (isset($BL_ID) && $BL_ID > 0) {
        $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Bokun', '', 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $id, 'Bokun', '', 'Add', 'super admin');
    }

    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save_seo') {
    $sql = "tbl_Business_Listing SET BL_SEO_Title = '" . encode_strings($_POST['title'], $db) . "', BL_SEO_Description = '" . encode_strings($_POST['description'], $db) . "', 
            BL_SEO_Keywords = '" . encode_strings($_POST['keywords'], $db) . "', BL_Search_Words = '" . encode_strings($_POST['search'], $db) . "'";
    if ($BL_ID > 0) {
        $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Search Engine Optimization', '', 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $id, 'Search Engine Optimization', '', 'Add', 'super admin');
    }

    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    exit();
}

if (isset($BL_ID) && $BL_ID > 0) {
    $sql = "SELECT B_ID, BL_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words, BL_Billing_Type, BL_CoC_Dis_2, BL_Line_Item_Title, BL_Line_Item_Price FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
    $BL_ID = $rowListing['BL_ID'];
}

if (isset($_POST['op']) && $_POST['op'] == 'save_category') {

    if (isset($_POST['newCat']) && $_POST['newCat'] > 0) {
        mysql_query("DELETE tbl_Business_Listing_Category FROM tbl_Business_Listing_Category WHERE BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BLC_M_C_ID ='" . encode_strings($_POST['newCat_mutiple_id'], $db) . "'");
    }
    if (isset($_POST['subcat']) && is_array($_POST['subcat'])) {
        foreach ($_POST['subcat'] as $key => $val) {
            if (substr($key, 0, 1) == 'c') {
                $mykey = substr($key, 1);
                $sql = "UPDATE tbl_Business_Listing_Category SET BLC_C_ID = '" . encode_strings($val, $db) . "' 
                        WHERE BLC_ID = '" . encode_strings($mykey, $db) . "'";
                $res1 = mysql_query($sql, $db);
            } else {
                if ($val > 0) {
                    $sql = "SELECT COUNT(*) FROM tbl_Business_Listing_Category WHERE BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    $row = mysql_fetch_row($result);
                    if ($row[0] == 0) {
                        $sql = "INSERT tbl_Business_Listing_Category SET BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                                BLC_M_C_ID = '" . encode_strings($_POST['category'], $db) . "',
                                BLC_C_ID = '" . encode_strings($val, $db) . "'";
                        if ($row[0] >= 1) {
                            $sql .= ", BLC_Price = '19.00'";
                        }
                    } else {
                        $sql = "INSERT tbl_Business_Listing_Category SET BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                                BLC_M_C_ID = '" . encode_strings($_POST['category'], $db) . "',    
                                BLC_C_ID = '" . encode_strings($val, $db) . "'";
                        if ($row[0] >= 1) {
                            $sql .= ", BLC_Price = '19.00'";
                        }
                    }
                    $res1 = mysql_query($sql, $db);
                }
            }
        }
    }
    listing_billing($rowListing['BL_Billing_Type'], $BL_ID, $rowListing['BL_CoC_Dis_2'], $rowListing['BL_Line_Item_Title'], $rowListing['BL_Line_Item_Price']);
    if (!$_REQUEST['newSubCat']) {
        if ($res1) {
            $_SESSION['success'] = 1;
            // TRACK DATA ENTRY
            $id = $BL_ID;
            Track_Data_Entry('Listing', $id, 'Category', '', 'Update Category', 'super admin');
        } else {
            $_SESSION['error'] = 1;
        }
    }

    header("Location: listings-preview.php?bl_id=" . $BL_ID);
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save_new') {
    foreach ($_POST['subcat'] as $val) {
        $sql = "SELECT COUNT(*) FROM tbl_Business_Listing_Category WHERE BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $row = mysql_fetch_row($result);
        if ($row[0] == 0) {
            $sql = "INSERT tbl_Business_Listing_Category SET BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                    BLC_M_C_ID = '" . encode_strings($_POST['category'], $db) . "', BLC_C_ID = '" . encode_strings($val, $db) . "'";
            if ($row[0] >= 1) {
                $sql .= ", BLC_Price = '19.00'";
            }
        } else {
            $sql = "INSERT tbl_Business_Listing_Category SET BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                    BLC_M_C_ID = '" . encode_strings($_POST['category'], $db) . "', BLC_C_ID = '" . encode_strings($val, $db) . "'";
            if ($row[0] >= 1) {
                $sql .= ", BLC_Price = '19.00'";
            }
        }
    }
    $res1 = mysql_query($sql, $db);
    if ($res1) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Category', '', 'Add Category', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: listings-preview.php?bl_id=" . $BL_ID);
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save_type') {
    $sql = "tbl_Business_Listing SET BL_Listing_Type = '" . encode_strings($_POST['type'], $db) . "'";

    if ($_POST['type'] <> $rowListing['BL_Listing_Type']) {
        $sqlTMP = "SELECT LT_Cost, LT_Name, LT_ID FROM tbl_Listing_Type WHERE LT_ID = '" . encode_strings($_POST['type'], $db) . "'";
        $result = mysql_query($sqlTMP, $db) or die("Invalid query: $sqlTMP -- " . mysql_error());
        $row = mysql_fetch_assoc($result);

        $sql .= ", BL_Price = '" . encode_strings($row['LT_Cost'], $db) . "'";
        if ($_POST['type'] == 1) {
            $sql .= ", BL_Basic_Points = '" . encode_strings(0, $db) . "',
                      BL_Addon_Points = '" . encode_strings(0, $db) . "',
                      BL_Points = '" . encode_strings(0, $db) . "',
                      BL_SubTotal = '" . encode_strings(0.00, $db) . "',
                      BL_CoC_Dis_2 = '" . encode_strings(0.00, $db) . "',
                      BL_SubTotal3 = '" . encode_strings(0.00, $db) . "',
                      BL_Tax = '" . encode_strings(0.00, $db) . "',
                      BL_Total = '" . encode_strings(0.00, $db) . "',
                      BL_Renewal_Date = '" . encode_strings('0000-00-00', $db) . "',
                      BL_Renewal_Date_Last_Update = '" . encode_strings($_SESSION['USER_EM'] . '-' . $_SESSION['user_online'] . '-' . $_SESSION['USER_ID'], $db) . '-' . $date . "'";
        }
    }
    $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $res = mysql_query($sql, $db);
    // TRACK DATA ENTRY
    $id = $BL_ID;
    Track_Data_Entry('Listing', $id, 'Category', '', 'Update Type', 'super admin');
    if ($row['LT_Name'] == 'Free') {
        $sql = "UPDATE tbl_Advertisement set A_Status = 4 WHERE A_BL_ID = $BL_ID";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        if ($res) {
            $_SESSION['success'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
    } elseif ($row['LT_Name'] == 'Enhanced') {
        $sql = "UPDATE tbl_Advertisement set A_Status = 3 WHERE A_BL_ID = $BL_ID";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        if ($res) {
            $_SESSION['enhanced_success'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
    } else {
        $sql = "UPDATE tbl_Advertisement set A_Status = 3 WHERE A_BL_ID = $BL_ID";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        if ($res) {
            $_SESSION['success'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
    }
    if ($_POST['type'] > 1) {
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    }
}

if (isset($_REQUEST['delSubCat']) && $_REQUEST['delSubCat'] > 0) {
    $sql = "DELETE FROM tbl_Business_Listing_Category WHERE BLC_ID = '" . encode_strings($_REQUEST['delSubCat'], $db) . "' AND BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BLC_M_C_ID = '" . encode_strings($_REQUEST['del_cat'], $db) . "' ";
    $result = mysql_query($sql, $db);
    // TRACK DATA ENTRY
    $id = $BL_ID;
    Track_Data_Entry('Listing', $id, 'Category', '', 'Delete Subcategory', 'super admin');
    if ($result) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
    listing_billing($rowListing['BL_Billing_Type'], $BL_ID, $rowListing['BL_CoC_Dis_2']);
    header("Location: listings-preview.php?bl_id=" . $BL_ID);
    exit();
}

if (isset($_REQUEST['del_cat']) && $_REQUEST['del_cat'] > 0) {
    $sql = "DELETE FROM tbl_Business_Listing_Category WHERE BLC_M_C_ID = '" . encode_strings($_REQUEST['del_cat'], $db) . "' AND BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "' ";
    $result = mysql_query($sql, $db);
    $id = $BL_ID;
    Track_Data_Entry('Listing', $id, 'Category', '', 'Delete Category', 'super admin');
    if ($result) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
    listing_billing($rowListing['BL_Billing_Type'], $BL_ID, $rowListing['BL_CoC_Dis_2']);
    header("Location: listings-preview.php?bl_id=" . $BL_ID);
    exit();
}

if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Billing_Type, BL_CoC_Dis_2, BL_Line_Item_Title, BL_Line_Item_Price, BL_Listing_Title FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
}

if (isset($_REQUEST['addon_id']) && $_REQUEST['addon_id']) {
    $id = $_REQUEST['addon_id'];

    $getStatus = "SELECT BLF_Active FROM tbl_BL_Feature WHERE BLF_ID = '" . encode_strings($id, $db) . "'";
    $resultStatus = mysql_query($getStatus, $db) or die("Invalid query: $getStatus -- " . mysql_error());
    $rowStatus = mysql_fetch_assoc($resultStatus);
    $BLF_Active = ($rowStatus['BLF_Active'] == 1) ? "0" : "1";
    $del = "UPDATE tbl_BL_Feature SET BLF_Active = '" . $BLF_Active . "', BLF_Last_Update = '" . encode_strings($_SESSION['USER_EM'] . '-' . $_SESSION['user_online'] . '-' . $_SESSION['USER_ID'], $db) . "' WHERE BLF_ID = '" . encode_strings($id, $db) . "'";
    $result = mysql_query($del, $db) or die("Invalid query: $del -- " . mysql_error());
    if ($result) {
        $_SESSION['addon'] = 'deleted' . $BLF_Active;
    }

    listing_billing($rowListing['BL_Billing_Type'], $BL_ID, $rowListing['BL_CoC_Dis_2'], $rowListing['BL_Line_Item_Title'], $rowListing['BL_Line_Item_Price']);
//update points only for one listing
    update_pointsin_business_tbl($BL_ID);
    if (isset($_SESSION['success'])) {
        unset($_SESSION['success']);
    }
    header('location: /admin/listings-preview.php?bl_id=' . $BL_ID);
    exit;
}

if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    // get regions and categories of this listing
    $sql_region = " SELECT RM_Parent, BLCR_BLC_R_ID FROM tbl_Business_Listing
                    LEFT JOIN tbl_Business_Listing_Category  ON BLC_BL_ID = BL_ID
                    LEFT JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID 
                    LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                    LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID
                    WHERE BLC_BL_ID = '$BL_ID'";
    $res_region = mysql_query($sql_region);
    $regions = array();
    while ($r = mysql_fetch_assoc($res_region)) {
        // add region parent as well if available
        if ($r['RM_Parent'] != 0 && !in_array($r['RM_Parent'], $regions)) {
            $regions[] = $r['RM_Parent'];
        }
        // add region
        if ($r['BLCR_BLC_R_ID'] != '' && !in_array($r['BLCR_BLC_R_ID'], $regions)) {
            $regions[] = $r['BLCR_BLC_R_ID'];
        }
    }
} else {
    header("Location:customers.php");
    exit();
}
// getting the default region
foreach ($regions as $key => $r) {
    if ($key == 0) {
        $default_region = $r;
    }
}
// See if a region is already selected
if (isset($_SESSION['ranking_region' . $BL_ID]) && $_SESSION['ranking_region' . $BL_ID] > 0) {
    $ranking_region = $_SESSION['ranking_region' . $BL_ID];
} else {
    $ranking_region = $default_region;
}

if (isset($_POST['button_recom'])) {
    $Recommended_BL_ID = $_POST['recommended_bl_id'];
    $Recommends_BL_ID = $_POST['recommends_bl_id'];
    $search_text1 = $_POST['search_text1'];
    $category1 = $_POST['category1'];
    $subcategory1 = $_POST['subcategory1'];
    $text = $_POST['recommendation-text' . $Recommended_BL_ID];
    $email = $_POST['b_email'];
    $query = mysql_query("INSERT INTO tbl_Recommendations(R_Recommends, R_Recommended, R_Text, R_Seen) VALUES('$Recommends_BL_ID', '$Recommended_BL_ID', '$text', 0)") or die("Invalid query: $query -- " . mysql_error());

    //update points only for listing
    update_pointsin_business_tbl($Recommends_BL_ID);
    update_pointsin_business_tbl($Recommended_BL_ID);

    $sql = "SELECT BL_Listing_Title FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($Recommends_BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowRecommends = mysql_fetch_assoc($result);
    $sql = "SELECT BL_Listing_Title, BL_C_ID, BL_Points FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($Recommended_BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowRecommended = mysql_fetch_assoc($result);
    $sql = "SELECT R_Domain FROM tbl_Region WHERE R_ID = '" . encode_strings($ranking_region, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowRegion = mysql_fetch_assoc($result);
    $region_rank = calculate_ranking($Recommended_BL_ID, 'region', $ranking_region, $ranking_region);
    $category_rank = calculate_ranking($Recommended_BL_ID, 'category', $rowRecommended['BL_C_ID'], $ranking_region);
    if ($email != '') {
        $message = '<html>
                    <head>
                    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
                    <title>You are recommended</title>
                    </head>
                    <body>
                    <div><img src="http://touristtown.ca/images/eamil-touristtown-logo.gif" width="195" height="43" align="absmiddle" /></div>
                    <p>Congratulations ' . $rowRecommended['BL_Listing_Title'] . '!</p><br>
                    You�ve just been recommended by ' . $rowRecommends['BL_Listing_Title'] . ' on ' . $rowRegion['R_Domain'] . ' and earned 3 points!<br><br>
                    This has boosted your ranking to ' . $rowRecommended['BL_Points'] . ', which places your business at number ' . $category_rank['rank'] . ' out of ' . $category_rank['total'] . '.  Repay the favour by logging in here to recommend them. (my.touristtown.ca) By recommending other businesses you receive additional points to bump up your ranking even more!<br><br>
                    Recommendations help visitors easily find the best places to visit in your area which benefits everybody. After all, we want our visitors to have a fantastic time so they share their experience with friends and family and having a great time will encourage them to return!  Plus the businesses that are doing a fantastic job (like yours) will reap the rewards of being visited more often.<br><br>
                    Click here to login to your page now. (my.touristtown.ca)<br><br>
                    Regards,<br><br>
                    The Tourist Town Team<br><br>
                    info@touristtown.ca   

                    </body>

                    </html>';


        $mail = new PHPMailer();
        $mail->From = "no-reply@touristtown.com";
        $mail->FromName = "TouristTown";
        $mail->IsHTML(true);

        $mail->AddAddress($email);

        $mail->Subject = "You've Been Recommended on Tourist Town!";
        $mail->MsgHTML($message);
//        $mail->Send();
    }
    if ($query) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: /admin/listings-preview.php?bl_id=$Recommends_BL_ID&search_text=$search_text1&op=save_recom&category=$category1&subcategory=$subcategory1&#recom");
    exit();
}

if (isset($_POST['button-edit-text'])) {
    $R_ID = $_POST['recommended_r_id'];
    $R_Text = $_POST['recommendation-text' . $R_ID];
    $update = "UPDATE tbl_Recommendations SET R_Text = '$R_Text' WHERE R_ID = '$R_ID'";
    $query = mysql_query($update, $db);
    if ($query) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
}

if (isset($_GET['op']) && $_GET['op'] == 'save_recom') {
    $search_text = $_GET['search_text'];
    $category = $_GET['category'];
    $subcategory = $_GET['subcategory'];
    $region = "SELECT R_ID, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($ranking_region, $db) . "'";
    $regionResult = mysql_query($region, $db) or die("Invalid query: $region -- " . mysql_error());
    $regionData = mysql_fetch_array($regionResult);
    $search = " SELECT BL_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, B_Email FROM tbl_Business_Listing
                LEFT JOIN tbl_Business ON BL_B_ID = B_ID
                INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                LEFT JOIN tbl_Listing_Type ON BL_Listing_Type = LT_ID";
    if ($category != '' && $subcategory != '') {
        $search .= " WHERE BLC_C_ID = '" . encode_strings($subcategory, $db) . "'
                    AND BLC_M_C_ID = '" . encode_strings($category, $db) . "'
                    AND BL_Listing_Title LIKE '%$search_text%'";
    } else if ($category != '' && $subcategory == '') {
        $search .= " WHERE BLC_M_C_ID = '" . encode_strings($category, $db) . "'
                    AND BL_Listing_Title LIKE '%$search_text%'";
    } else {
        $search .= " WHERE BL_Listing_Title LIKE '%$search_text%'";
    }
    if ($regionData['R_Type'] == 1) {
        $search .= " AND BLCR_BLC_R_ID IN (SELECT R_ID from tbl_Region LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID where RM_Parent = '" . encode_strings($regionData['R_ID'], $db) . "')";
    } else {
        $search .= " AND BLCR_BLC_R_ID = '" . encode_strings($regionData['R_ID'], $db) . "'";
    }
    $search .= " AND BL_Listing_Type != 1
                AND BL_ID NOT IN(SELECT R_Recommended FROM tbl_Recommendations WHERE R_Recommends = '$BL_ID')
                AND BL_ID != '$BL_ID'
                GROUP BY BL_ID";
    $search_result = mysql_query($search, $db) or die("Invalid query: $search -- " . mysql_error());
}


if (isset($_GET['op']) && $_GET['op'] == 'delete_recom') {
    $rid = $_GET['rid'];
    $bl_id = $_GET['bl_id'];
    $sql = mysql_query("SELECT * FROM tbl_Recommendations WHERE R_ID = $rid");
    $result = mysql_fetch_array($sql) or die(mysql_error());

    $sql = mysql_query("DELETE FROM tbl_Recommendations where R_ID = $rid");
    if ($sql) {
        $_SESSION['delete'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($result['R_Recommends']);
        update_pointsin_business_tbl($result['R_Recommended']);
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: /admin/listings-preview.php?bl_id=" . $bl_id);
    exit();
}

require_once '../include/admin/header.php';
?>
<div>
    <?php require_once '../include/top-nav-listing.php'; ?>
    <?php
    $sql = "SELECT * FROM tbl_Business_Listing WHERE BL_ID  = $BL_ID";
    $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($resultTMP);
    $activeBiz = $row;

    function ctime($myTime, $end = false) {
        if ($myTime == '00:00:01' && !$end) {
            return 'Closed';
        } elseif ($myTime == '00:00:02') {
            return 'By Appointment';
        } elseif ($myTime == '00:00:03') {
            return 'Open 24 Hours';
        } else {
            $mySplit = explode(':', $myTime);
            return date('g:ia', mktime($mySplit[0], $mySplit[1], 1, 1, 1));
        }
    }

//Getting Gallery Images if active
    $count_gallery = 0;
    $sqlFG = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 2 AND BLF_Active = 1";
    $resultFG = mysql_query($sqlFG, $db) or die("Invalid query: $sqlFG -- " . mysql_error());
    if (mysql_num_rows($resultFG) > 0) {
        $sqlG = "SELECT BFP_ID,BFP_BL_ID, BFP_Title, BFP_Photo, BFP_Photo_195X195, BFP_Order FROM tbl_Business_Feature_Photo WHERE BFP_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' ORDER BY BFP_Order";
        $resultG = mysql_query($sqlG, $db) or die("Invalid query: $sqlG -- " . mysql_error());
        $count_gallery = mysql_num_rows($resultG);
    }

//Getting About us if active
    $count_about = 0;
    $sqlFA = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 1 AND BLF_Active = 1";
    $resultFA = mysql_query($sqlFA, $db) or die("Invalid query: $sqlFA -- " . mysql_error());
    if (mysql_num_rows($resultFA) > 0) {
        $sql = "SELECT BFA_ID FROM tbl_Business_Feature_About WHERE BFA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'";
        $resultAbt = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $count_about = mysql_num_rows($resultAbt);
    }

//Getting Menu if active
    $count_menu = 0;
    $sqlFM = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 6 AND BLF_Active = 1";
    $resultFM = mysql_query($sqlFM, $db) or die("Invalid query: $sqlFM -- " . mysql_error());
    if (mysql_num_rows($resultFM) > 0) {
        $sql = "SELECT BFM_ID FROM tbl_Business_Feature_Menu WHERE BFM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'";
        $resultMenu = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $count_menu = mysql_num_rows($resultMenu);
    }

//Getting Entertainment if active
    $count_Ent = 0;
    $sqlFE = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 8 AND BLF_Active = 1";
    $resultFE = mysql_query($sqlFE, $db) or die("Invalid query: $sqlFE -- " . mysql_error());
    if (mysql_num_rows($resultFE) > 0) {
        $sql = "SELECT BFEA_ID FROM tbl_Business_Feature_Entertainment_Acts WHERE BFEA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND BFEA_Time >= CURDATE()";
        $resultEnt = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $count_Ent = mysql_num_rows($resultEnt);
    }

//Getting Daily Feature if active
    $count_DS = 0;
    $sqlFDS = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 7 AND BLF_Active = 1";
    $resultFDS = mysql_query($sqlFDS, $db) or die("Invalid query: $sqlFDS -- " . mysql_error());
    if (mysql_num_rows($resultFDS) > 0) {
        $sql = "SELECT BFDS_ID FROM tbl_Business_Feature_Daily_Specials WHERE BFDS_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'";
        $resultDS = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $count_DS = mysql_num_rows($resultDS);
    }

//Getting Agenda Minutes if active
    $count_AM = 0;
    $sqlFAM = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 13 AND BLF_Active = 1";
    $resultFAM = mysql_query($sqlFAM, $db) or die("Invalid query: $sqlFAM -- " . mysql_error());
    if (mysql_num_rows($resultFAM) > 0) {
        $sql = "SELECT BFAM_ID FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'";
        $resultAM = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $count_AM = mysql_num_rows($resultAM);
    }

//Getting Guest Book if active
    $sqlFGB = "SELECT BLF_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = " . $activeBiz['BL_ID'] . " AND BLF_F_ID = 12 AND BLF_Active = 1";
    $resultFGB = mysql_query($sqlFGB, $db) or die("Invalid query: $sqlFGB -- " . mysql_error());

    $VIDEOID = $activeBiz['BL_Video'];
    $pos = strpos($VIDEOID, '=');
    if ($pos == false) {
        $exp_video_link = explode('/', $VIDEOID);
        $video_link = end($exp_video_link);
    } else {
        $exp_video_link = explode("=", $VIDEOID);
        $video_link = end($exp_video_link);
    }
    ?>
    <!--Slider start-->
    <section class="main_slider">
        <div class="slider_wrapper">
            <div class="slider_wrapper_video">
                <div class="sliderCycle1" data-cycle-loader="wait" data-cycle-fx=fade data-cycle-timeout=0 data-cycle-prev="#prev" data-cycle-next="#next">
                    <?php if ($activeBiz['BL_Header_Image'] != "") { ?>
                        <a id="edit_main_image" onclick="show_customer_listing_main_photo(<?php echo $BL_ID ?>)">
                            Edit
                        </a>
                        <div class="slide-images">  
                            <?php if ($VIDEOID != "") { ?>
                                <div class="player1_wrapper" style="display:none;position:absolute;opacity: 0;top:0;">
                                    <input type="hidden" id="video-link-1" value="<?php echo $video_link ?>">
                                    <div id="player1"></div>
                                </div>
                            <?php } ?>
                            <div class="slider-text slider-listing-text">
                                <div class="watch-video play-button">
                                    <div class="slider-button1 slider-listing-button">
                                        <?php if ($VIDEOID != "") { ?>
                                            <a class="watch-full-video-listing" onclick="play_video(1, 'player1')">Watch video</a>
                                        <?php } if ($count_gallery > 0) { ?>
                                            <a style="display: none;" class="view_all_photo view-gallery" href="/profile/<?php echo $activeBiz['BL_Name_SEO'] ?>/<?php echo $activeBiz['BL_ID'] ?>/gallery.php">VIEW ALL PHOTOS</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div> 
                            <img class="preview-img-home1 preview-img-script22" src=""> 
                            <img class="slider_image" src="<?php echo IMG_LOC_REL . $activeBiz['BL_Header_Image'] ?>" alt="<?php echo $activeBiz['BL_Header_Image_Alt'] ?>" longdesc="<?php echo $activeBiz['BL_Header_Image_Alt'] ?>">                    
                        </div>
                    <?php } else {
                        ?>

                        <a id="add_main_image" onclick="show_customer_listing_main_photo(<?php echo $BL_ID ?>)">
                            +Add Image
                        </a>
                        <!--                        <div class="slide-images">
                                                    <img class="preview-img-home1 preview-img-script22" src="">    
                                                    <img class="slider_image" src="<?php echo IMG_LOC_REL . "Default-Header.jpg" ?>" alt="<?php echo $activeBiz['BL_Header_Image_Alt'] ?>" longdesc="<?php echo $activeBiz['BL_Header_Image_Alt'] ?>">
                                                </div>-->
                        <div class="slide-images">
                            <?php if ($VIDEOID != "") { ?>
                                <div class="player1_wrapper" style="display:none;position:absolute;opacity: 0;top:0;">
                                    <input type="hidden" id="video-link-1" value="<?php echo $video_link ?>">
                                    <div  id="player1"></div>
                                </div>
                            <?php } ?>
                            <div class="slider-text slider-listing-text">
                                <div class="watch-video play-button">
                                    <div class="slider-button1 slider-listing-button">
                                        <?php if ($VIDEOID != "") { ?>
                                                <!--<img class="video_icon" src="http://<?php echo $REGION['R_Domain'] ?>/images/videoicon.png" alt="">-->
                                            <a class="watch-full-video-listing" onclick="play_video(1, 'player1')">Watch full video</a>
                                        <?php } if ($count_gallery > 0) { ?>
                                            <a class="view_all_photo view-gallery" href="/profile/<?php echo $activeBiz['BL_Name_SEO'] ?>/<?php echo $activeBiz['BL_ID'] ?>/gallery.php">VIEW ALL PHOTOS</a>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>   
                            <img class="preview-img-home1 preview-img-script22" src=""> 
                            <img class="slider_image" src="<?php echo IMG_LOC_REL . "Default-Header.jpg" ?>" alt="<?php echo $activeBiz['BL_Header_Image_Alt'] ?>" longdesc="<?php echo $activeBiz['BL_Header_Image_Alt'] ?>">                    
                        </div>
                    <?php }
                    ?>
                </div>
                <input type="hidden" id="total_players" value="2">
                <?php if ($THEME['TO_Slider_Overlay'] != "") { ?>
                    <!--                    <div class="image_overlay_img">
                                            <div class="image_overlay">
                                                <img src="<?php echo IMG_ICON_REL . $THEME['TO_Slider_Overlay']; ?>" alt="Slider Overlay">
                                            </div>
                                        </div>-->
                <?php } ?>
            </div>
        </div>
    </section>
    <!--Slider End-->

    <section class="description listing-home-des margin-bottom-none" style="background-color:white;">
        <div class="description-wrapper">
            <div class="event-filter">
                <div class="listing-title-detail" >
                    <div class="listing-title1 <?php if ($activeBiz['BL_Listing_Type'] == 1) { ?> listing-title-with-bg" <?php } else { ?> " style="margin-bottom: 20px;line-height: 40px;" <?php } ?> >
                        <?php if ($activeBiz['BL_Listing_Title'] != '') { ?>
                            <?php echo ($activeBiz['BL_Listing_Title']); ?>
                            <a onclick="return show_contact_details_form(<?php echo $BL_ID ?>)">
                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                            </a>
                        <?php } else { ?>
                            <div class="add-box-preview" >
                                <a onclick="return show_contact_details_form(<?php echo $BL_ID ?>)">
                                    +Add Title 
                                </a>
                            </div>  
                        <?php } ?>
                    </div>
                    <div class="listing-address <?php if ($activeBiz['BL_Listing_Type'] == 1) { ?> listing-title-with-bg <?php } ?>">
                        <?php if ($activeBiz['BL_Town'] != '' || $activeBiz['BL_Province'] != '' || $activeBiz['BL_Country'] != '') { ?>
                            <?php echo ($activeBiz['BL_Town'] != '' ? $activeBiz['BL_Town'] : '' ) . ($activeBiz['BL_Province'] != '' ? ', ' . $activeBiz['BL_Province'] : '') . ($activeBiz['BL_Country'] != '' ? ', ' . $activeBiz['BL_Country'] : ''); ?>
                            <a onclick="show_customer_listing_mapping(<?php echo $BL_ID ?>)">
                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                            </a>
                        <?php } else { ?>
                            <div class="add-box-preview" >
                                <a onclick="show_customer_listing_mapping(<?php echo $BL_ID ?>)">
                                    +Add Location & Mapping Details 
                                </a>
                            </div> 
                        <?php } ?>
                    </div>
                </div> 

                <div class="photo-gallery-limit">
                    <span>Photos <?php echo(($count_gallery == "") ? "0" : $count_gallery) ?> of 30</span>
                </div>
                <?php
                if ($count_gallery < 30) {
                    if ($count_gallery <= 0) {
                        ?>
                        <div class="listing-title-detail">
                            <div class="add-box-preview" >
                                <a onclick="show_customer_feature_gallery(<?php echo $BL_ID ?>)">
                                    +Add Photo 
                                </a>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div id="display_photo_button" class="content-header1">
                            <div class="link">
                                <div class="add-link"><a onclick="show_customer_feature_gallery(<?php echo $BL_ID ?>)">+Add Photo</a>
                                    <a onclick="show_customer_feature_gallery_ordering(<?php echo $BL_ID ?>)">Ordering</a>
                                </div>
                            </div>
                        </div> 
                        <?php
                    }
                }
                ?>
                <div id="image-library"></div>            

                <div class="thumbnails listing-home-gallary">                
                    <div class="thumbnail-slider">
                        <div class="inner">
                            <?PHP
                            if ($count_gallery > 0) {
                                ?>
                                <ul class="bxslider">
                                    <?PHP while ($rowG = mysql_fetch_assoc($resultG)) { ?>
                                        <li>   
                                            <div class="cross1"><a onClick="return confirm('Are you sure?');" href="listings-preview.php?op=del&id=<?php echo $rowG['BFP_ID'] ?>&bl_id=<?php echo $rowG['BFP_BL_ID'] ?>">x</a></div>
                                            <div class="desc" onclick="show_description(<?php echo $rowG['BFP_ID'] ?>);"><?php echo ($rowG['BFP_Title'] != '') ? $rowG['BFP_Title'] : '+Add Description'; ?></div> 

                                            <form action="" method="post" name="form" id="gallery-description-form<?php echo $rowG['BFP_ID'] ?>" style="display:none;">
                                                <div id="gallery_desc">
                                                    <input type="hidden" name="op" value="edit">
                                                    <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
                                                    <input type="hidden" name="bfp_id" value="<?php echo $rowG['BFP_ID'] ?>">
                                                    <textarea name="description<?php echo $rowG['BFP_ID'] ?>" style="width:420px !important;" size="50" required><?php echo $rowG['BFP_Title'] ?></textarea>
                                                </div>
                                                <div id="gallery_desc_save_btn">
                                                    <div class="form-data">
                                                        <input type="submit" name="button" value="Save Now"/>
                                                    </div>
                                                </div> 
                                            </form>
                                            <a href="/profile/<?php echo $activeBiz['BL_Name_SEO'] ?>/<?php echo $activeBiz['BL_ID'] ?>/gallery.php?order=<?php echo $rowG['BFP_Order'] ?>" class="view-gallery">
                                                <img src="<?php echo IMG_LOC_REL . $rowG['BFP_Photo_195X195']; ?>" alt="<?php echo $rowG['BFP_Title']; ?>"/>
                                            </a>
                                        </li>
                                        <?php
                                        if ($rowG['BFP_Photo_195X195'] != "") {
                                            ?>
                                        <?php } else if ($rowG['BFP_Photo'] != "") { ?>
                                            <li><a href="/profile/<?php echo $activeBiz['BL_Name_SEO'] ?>/<?php echo $activeBiz['BL_ID'] ?>/gallery.php?order=<?php echo $rowG['BFP_Order'] ?>" class="view-gallery">
                                                    <img src="<?php echo IMG_LOC_REL . $rowG['BFP_Photo']; ?>" alt="<?php echo $rowG['BFP_Title']; ?>"/>
                                                </a></li>
                                        <?php }
                                        ?>

                                        <?php
                                    }
                                    ?>
                                </ul>    
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="description stories margin-bottom-none">
        <div class="description-wrapper">
            <div class="description-inner padding-none">
                <div class="listing-detail-left">
                    <div  id="overview" class="listing-desc theme-paragraph ckeditor-anchor-listing">
                        <div style="clear:both" id="descr">
                            <?php if ($activeBiz['BL_Description'] != '') { ?>
                                <a class="a_edit desc_ck" onclick="show_desc_text(<?php echo $BL_ID ?>)">
                                    <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                </a>
                                <?php echo $activeBiz["BL_Description"]; ?>

                            <?php } else { ?>
                                <div class="add-box-preview" >
                                    <a onclick="show_desc_text(<?php echo $BL_ID ?>)">
                                        +Add Description Text 
                                    </a>
                                </div> 
                            <?php } ?>
                        </div>
                    </div>

                    <?php if ($activeBiz['BL_Trip_Advisor']) { ?>
                        <div id="reviews" class="listing-review review-body">
                            <div class="review-heading">Reviews</div>
                            <?php // echo $activeBiz['BL_Trip_Advisor']       ?>
                            <div style="float: right">
                                <?php if ($activeBiz['BL_Trip_Advisor'] != '') { ?>
                                    <a class="a_edit" onclick="show_trip_advisor(<?php echo $BL_ID ?>)">
                                        <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                    </a>
                                    <?php echo $activeBiz["BL_Trip_Advisor"]; ?>

                                <?php } ?>
                            </div>  
                        </div>
                    <?php } else { ?>
                        <div id="menu-info" class="listing-desc theme-paragraph">
                            <div class="menu-header-container heading-margin">
                                <div class="menu-heading">Reviews</div>
                            </div>
                            <div class="add-box-preview" style="clear:both">
                                <a onclick="show_trip_advisor(<?php echo $BL_ID ?>)">
                                    +Add Trip Advisor 
                                </a>
                            </div> 
                        </div>
                    <?php } ?>
                    <!-----MENU START--->
                    <?php // if ( ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 6)) { ?>
                    <div id="menu-info" class="listing-desc theme-paragraph">
                        <div class="menu-header-container">
                            <div class="menu-heading">Menu</div>
                            <div class="menu-Button">
                                <div class="drop-down-arrow listing"></div>
                                <select id="menu_key" onchange="show_menu()">
                                    <?php
                                    foreach ($menuSections as $key => $val) {
                                        $sql3 = "SELECT BFM_ID FROM tbl_Business_Feature_Menu 
                                                WHERE BFM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND 
                                                BFM_Type = '" . encode_strings($key, $db) . "'";
                                        $result3 = mysql_query($sql3, $db) or die("Invalid query: $sql3 -- " . mysql_error());
                                        $count = mysql_num_rows($result3);
//                                            if ($count > 0) {
                                        ?>
                                        <option value="<?php echo $key ?>"><?php echo $val['title'] ?></option>
                                        <?php
//                                            }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <?php
//                            $sql_des = "Select BFMD_Description, BFMD_Type from tbl_Business_Feature_Menu_Description where  BFMD_BL_ID= '" . encode_strings($activeBiz['BL_ID'], $db) . "'";
//                            $result_des = mysql_query($sql_des);
//                            $acco = 0;
//                            while ($row_des = mysql_fetch_assoc($result_des)) { 
                        foreach ($menuSections as $key => $val) {
                            $sql_des = "Select BFMD_Description, BFMD_Type from tbl_Business_Feature_Menu_Description where BFMD_Type = '" . $key . "' AND  BFMD_BL_ID= '" . encode_strings($activeBiz['BL_ID'], $db) . "'";
                            $result_des = mysql_query($sql_des);
                            $row_des = mysql_fetch_assoc($result_des);
                            ?>
                            <div class="menu-desc theme-paragraph meun-hide-show menu-<?php echo $key; ?>">
                                <?php if ($row_des['BFMD_Description'] != '') { ?>
                                    <a class="a_edit add_descr_<?php echo $key ?>" onclick="show_desc_form('<?php echo $key ?>')">
                                        <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                    </a>
                                    <?php echo $row_des['BFMD_Description']; ?>

                                <?php } else { ?>
                                    <div class="add-box-preview" >
                                        <a class="add_descr_<?php echo $key ?>"  onclick="show_desc_form('<?php echo $key ?>')">
                                            +Add Description Text 
                                        </a>
                                    </div> 
                                <?php } ?>

                            </div>
                            <?php
//                                $acco++;
                        }
                        $sql = "SELECT BFM_Title, BFM_Description, BFM_Price, BFM_Type FROM tbl_Business_Feature_Menu 
                                WHERE BFM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' 
                                ORDER BY BFM_Order";
                        $result1 = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        $title_type = '';
                        foreach ($menuSections as $key => $val) {
                            $sql = "SELECT BFM_ID, BFM_Title, BFM_Description, BFM_Price, BFM_Type FROM tbl_Business_Feature_Menu 
                                        WHERE BFM_Type = '" . $key . "' AND BFM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' 
                                        ORDER BY BFM_Order";
                            $result1 = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
//                            if there is no item then we need to insert the add item box
                            $count_items = mysql_num_rows($result1);
                            if ($count_items == 0) {
                                ?>
                                <div class="menu-desc meun-hide-show menu-<?php echo $key ?>">
                                    <div class="add-box-preview" >
                                        <a class="add_items_<?php echo $key ?>" onclick="show_add_item_form('<?php echo $key ?>')">
                                            +Add Item
                                        </a>
                                    </div>
                                </div>
                                <?php
                            }
                            $item_count = 0;
                            while ($data = mysql_fetch_assoc($result1)) {
//                                if ($title_type != $key) {
//                                    if ($title_type == "") {
//                                        
//                                    } else if ($title_type != $key) {
//                                        echo '</div>';
//                                    }
//                                    
                                ?>
                                <!--<div class="type-child">-->
                                <?php
//                                    }
                                ?>                   
                                <div class="menu-desc meun-hide-show menu-<?php echo $key ?>">
                                    <?php if ($item_count++ == 0) { ?>
                                        <div class="add-box-preview" >
                                            <a class="add_items_<?php echo $key ?>" onclick="show_add_item_form('<?php echo $key ?>')">
                                                +Add Item
                                            </a>
                                        </div> 
                                    <?php } if ($data['BFM_Title'] != '' || $data['BFM_Price'] != '' || $data['BFM_Description'] != '') { ?>

                                        <a class="a_edit edit_items_<?php echo $data['BFM_ID'] ?>" onclick="show_edit_item_form('<?php echo $data["BFM_ID"] ?>')">
                                            <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                        </a>
                                        <div class="menu-item-heading-container">
                                            <div class="menu-title"><?php echo $data["BFM_Title"]; ?></div>
                                            <div class="menu-price">$<?php echo $data["BFM_Price"]; //REPLACE                                                                                                                                                                                                                               ?></div>
                                        </div>
                                        <?php echo $data["BFM_Description"]; ?>

                                    <?php } else { ?>
                                        <div class="add-box-preview" >
                                            <a class="add_items_<?php echo $key ?>" onclick="show_add_item_form('<?php echo $key ?>')">
                                                +Add Item
                                            </a>
                                        </div> 
                                    <?php } ?>

                                </div>
                                <?php
                                $title_type = $key;
                            }
                        }
                        ?>
                        <!--</div>-->
                    </div>
                    <?php
                    if ($count_about > 0) {
                        ?>

                        <div id="about-info" class="listing-desc">
                            <div class="menu-header-container">
                                <div class="menu-heading">About Us</div>

                                <div class="menu-Button">
                                    <div class="drop-down-arrow listing"></div>
                                    <select id="aboutus_id" onchange="show_aboutus()">
                                        <?php
                                        $sql2 = " SELECT BFA_ID, BFA_Title FROM tbl_Business_Feature_About
                                              WHERE BFA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' 
                                              ORDER BY BFA_Order";
                                        $result2 = mysql_query($sql2, $db) or die("Invalid query: $sql1 -- " . mysql_error());
                                        while ($data2 = mysql_fetch_assoc($result2)) {
                                            ?>
                                            <option value="<?php echo $data2['BFA_ID'] ?>"><?php echo $data2['BFA_Title'] ?></option> 
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <a onclick="show_about_us_ordering()">(Ordering)</a>
                                </div>
                            </div>
                            <div class="form-inside-div border-none">
                                <div class="add-box-preview" style="clear:both">
                                    <a onclick="show_add_about_us_mainimage_form()">+Add Main Image</a>
                                </div> 
                                <div class="add-box-preview" style="clear:both">
                                    <a onclick="show_add_about_us_page_form()">+Add Page</a>
                                </div>
                            </div>
                            <?php
                            $sql = "SELECT BFA_ID, BFA_Description FROM tbl_Business_Feature_About
                                LEFT JOIN tbl_Business_Listing ON BL_ID = BFA_BL_ID
                                WHERE BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' ORDER BY BFA_Order";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            $acco = 0;
                            while ($row = mysql_fetch_assoc($result)) {
                                ?>
                                <div class="aboutus-body-detail aboutus-<?php echo $row['BFA_ID'] ?>">
                                    <div class="aboutus-desc theme-paragraph">
                                        <?php if ($row['BFA_Description'] != '') { ?>
                                            <a id="edit_about_us_page_form_<?php echo $row['BFA_ID'] ?>" onclick="show_edit_about_us_page_form(<?php echo $row['BFA_ID'] ?>)" class="a_edit">
                                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                            </a>
                                            <?php echo $row["BFA_Description"]; ?>

                                        <?php } else { ?>
                                            <div class="add-box-preview" >
                                                <a id="edit_about_us_page_form_<?php echo $row['BFA_ID'] ?>" onclick="show_edit_about_us_page_form(<?php echo $row['BFA_ID'] ?>)">
                                                    +Add Description 
                                                </a>
                                            </div> 
                                        <?php } ?>

                                    </div>

                                    <?php
                                    $sql_photo = " SELECT BFAP_Title, BFAP_Photo, BFAP_Photo_470X320 FROM tbl_Business_Feature_About_Photo
                                               WHERE BFAP_BFA_ID = '" . encode_strings($row['BFA_ID'], $db) . "' 
                                               ORDER BY BFAP_Order";
                                    $result_photo = mysql_query($sql_photo, $db) or die("Invalid query: $sql -- " . mysql_error());
                                    $count_photo = mysql_num_rows($result_photo);
                                    if ($count_photo > 0) {
                                        ?>
                                        <div class="about-image-container">
                                            <?php
                                            while ($data = mysql_fetch_assoc($result_photo)) {
                                                if ($data['BFAP_Photo_470X320'] != "") {
                                                    ?>
                                                    <img class="about-img" src="<?php echo IMG_LOC_REL . $data['BFAP_Photo_470X320']; ?>" alt="<?php echo $data['BFAP_Title']; ?>" longdesc="<?php echo '/'; //REPLACE                                                                                                                                                                                                                    ?>" />
                                                <?php } else if ($data['BFAP_Photo'] != "") { ?>
                                                    <img class="about-img" src="<?php echo IMG_LOC_REL . $data['BFAP_Photo']; ?>" alt="<?php echo $data['BFAP_Title']; ?>" longdesc="<?php echo '/'; //REPLACE                                                                                                                                                                                                                  ?>" />
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                        <?php
                                    }
                                    ?>                                                                       
                                </div>    
                                <?php
                                $acco++;
                            }
                            ?>
                        </div>

                        <!-----About Us End--->
                        <!-----Daily Features start--->
                    <?PHP } else { ?>
                        <div id="menu-info" class="listing-desc theme-paragraph">
                            <div class="menu-header-container heading-margin">
                                <div class="menu-heading">About Us</div>
                            </div>
                            <div class="add-box-preview" style="clear:both">
                                <a onclick="show_add_about_us_mainimage_form()">+Add Main Image</a>
                            </div> 
                            <div class="add-box-preview" style="clear:both">
                                <a onclick="show_add_about_us_page_form()">+Add Page</a>
                            </div>

                        </div>
                        <?php
                    }
                    ?>
                    <div id="daily-s-check" class="listing-desc">
                        <div class="menu-header-container">
                            <div class="menu-heading">Daily Features</div>
                        </div>
                        <?php
                        $features_bl_id = $activeBiz['BL_ID'];
                        $daily = "SELECT BFDSD_Description FROM `tbl_Business_Feature_Daily_Specials_Description` WHERE BFDSD_BL_ID = '$features_bl_id'";
                        $result1Daily = mysql_query($daily, $db) or die("Invalid query: $daily -- " . mysql_error());
                        $dataresult1Daily = mysql_fetch_assoc($result1Daily);
                        ?>
                        <div class="daily-description theme-paragraph">
                            <?php if ($dataresult1Daily['BFDSD_Description'] != '') { ?>
                                <a class="a_edit" id="add_daily_desc_click" onclick="show_add_daily_desc_form()">
                                    <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                </a>
                                <?php echo $dataresult1Daily["BFDSD_Description"]; ?>

                            <?php } else { ?>
                                <div class="add-box-preview" >
                                    <a id="add_daily_desc_click"  onclick="show_add_daily_desc_form()">
                                        +Add Description 
                                    </a>
                                </div> 
                            <?php } ?>

                        </div>
                        <?php
                        foreach ($days as $key => $val) {
                            $sql = "SELECT BFDS_Title, BFDS_Description, BFDS_Photo FROM tbl_Business_Feature_Daily_Specials 
                                    WHERE BFDS_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND 
                                    BFDS_Day = '" . encode_strings($key, $db) . "' LIMIT 1";
                            $result1 = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            $data = mysql_fetch_assoc($result1);
                            if ($data || !$data) {
                                ?>

                                <div class="thumbnail">
                                    <a class="a_edit" id="add_daily_click_<?php echo $key ?>" onclick="show_edit_daily_form(<?php echo $key ?>)">
                                        <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                    </a>
                                    <?php
                                    if ($data['BFDS_Photo'] != "") {
                                        $dailyFeaturePhoto = $data['BFDS_Photo'];
                                        ?>
                                        <div class="feature-img-outer">
                                            <img class="temp-width-feature" src="<?php echo IMG_LOC_REL . $dailyFeaturePhoto; ?>" alt="<?php echo $data['BFDS_Title']; //REPLACE                                                                                                                                                                 ?>"	longdesc="<?php echo '/'; //REPLACE                                                                                                                                                                 ?>" />
                                        </div>
                                        <?php
                                    }
                                    ?>        
                                    <div class="feature-body">
                                        <h5><?php echo $val; ?></h5>
                                        <div class="feature-title"><?php echo $data['BFDS_Title']; //REPLACE                                                                                                                                                                ?></div>
                                        <div class="feature-des">
                                            <?php echo $data['BFDS_Description']; //REPLACE                         ?>
                                        </div>

                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <!-----Daily Features End--->
                    <!-----Entertainment start--->              
                    <div id="Entertainment-check" class="listing-desc entert-padding-bottom">
                        <div class="menu-header-container">
                            <div class="menu-heading">Entertainment</div>                            
                            <div class="menu-Button">
                                <div class="drop-down-arrow listing"></div>
                                <select id="entert_id" onchange="show_entertainment()">
                                    <?php
                                    $sql = "SELECT BFEA_Time, DATE_FORMAT( BFEA_Time,  '%M' ) AS MONTH  FROM tbl_Business_Feature_Entertainment_Acts 
                                            WHERE BFEA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'  AND BFEA_Time >= CURDATE()
                                            Group by MONTH ORDER BY BFEA_Time ";
                                    $result1 = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                    while ($data2 = mysql_fetch_assoc($result1)) {
                                        ?>
                                        <option value="<?php echo date("M", strtotime($data2['BFEA_Time'])); ?>"><?php echo date("F  Y", strtotime($data2['BFEA_Time'])) ?></option> 
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <?php
                        $enter_bl_id = $activeBiz['BL_ID'];
                        $daily = "SELECT BFED_Description FROM `tbl_Business_Feature_Entertainment_Description` WHERE BFED_BL_ID = '$enter_bl_id'";
                        $result1Daily = mysql_query($daily, $db) or die("Invalid query: $daily -- " . mysql_error());
                        $dataresult1Daily = mysql_fetch_assoc($result1Daily);
                        ?>

                        <div class="entert-desc theme-paragraph">
                            <?php if ($dataresult1Daily['BFED_Description'] != '') { ?>
                                <a class="a_edit" id="add_enter_desc_click" onclick="show_enter_desc_form()">
                                    <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                </a>
                                <?php echo $dataresult1Daily["BFED_Description"]; ?>

                            <?php } else { ?>
                                <div class="add-box-preview" >
                                    <a id="add_enter_desc_click" onclick="show_enter_desc_form()">
                                        +Add Description 
                                    </a>
                                </div> 
                            <?php } ?>

                        </div>
                        <div class="entert-desc theme-paragraph" style="border:none">
                            <div class="add-box-preview" id="add_enter_click" onclick="show_add_enter_form()">
                                <a >
                                    +Add Event 
                                </a>
                            </div> 
                        </div>
                        <?php
                        $sql = "SELECT *, LOWER(DATE_FORMAT(BFEA_Time, '%l:%i%p')) as myTime , LOWER(DATE_FORMAT(BFEA_End_Time, '%l:%i%p')) as myendTime FROM tbl_Business_Feature_Entertainment_Acts 
                                WHERE BFEA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "'  AND BFEA_Time >= CURDATE()
                                ORDER BY BFEA_Time";
                        $result1 = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        $acco = 0;
                        while ($data = mysql_fetch_assoc($result1)) {
                            ?>

                            <div class="entert-desc entert-hide-show entert-<?php echo date("M", strtotime($data['BFEA_Time'])); ?>">
                                <a class="a_edit" id="add_enter_click<?php echo $data['BFEA_ID'] ?>" onclick="show_edit_enter_form(<?php echo $data['BFEA_ID'] ?>)">
                                    <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                </a>
                                <div class="entert-date"><?php echo date("F j, Y", strtotime($data['BFEA_Time'])); ?></div>
                                <div class="entert-title"><?php echo $data['BFEA_Name']; ?></div>                           
                                <div class="entert-body-desc theme-paragraph">
                                    <?PHP
                                    if (substr($data['BFEA_Description'], 0, 3) == '<p>') {
                                        echo $data['BFEA_Description'];
                                    } else {
                                        ?>
                                        <p><?php echo preg_replace("~\n~m", "<br>", $data['BFEA_Description']); ?></p>
                                    <?PHP } ?>
                                </div>                           
                            </div>
                            <?php
                            $acco++;
                        }
                        ?>
                    </div>
                    <!-----Entertainment End--->
                    <?php
//                    if (mysql_num_rows($resultFAM) > 0) {
//                        if ($count_AM > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 13)) {
                    ?>
                    <!-----Agenda start--->
                    <?php
                    $agendaDesc_title = "SELECT * FROM tbl_Feature WHERE F_ID = 13";
                    $resDesc_title = mysql_query($agendaDesc_title, $db) or die("Invalid query: $agendaDesc_title --" . mysql_error());
                    $descAgenda_title = mysql_fetch_assoc($resDesc_title);
                    ?>
                    <div id="Agenda-and-Minutes" class="listing-desc">
                        <div class="menu-header-container">
                            <div class="menu-heading"><?php echo $descAgenda_title['F_Name']; ?></div>
                            <div class="menu-Button">
                                <div class="drop-down-arrow listing"></div>
                                <select id="agenda_id" onchange="show_agenda()">
                                    <?php
                                    $agenda = "SELECT YEAR(STR_TO_DATE(BFAM_Date, '%Y-%m-%d')) as Agenda_Year FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' GROUP BY Agenda_Year ORDER BY Agenda_Year ASC";
                                    $result_agenda = mysql_query($agenda, $db) or die("Invalid query: $agenda -- " . mysql_error());
                                    while ($rowAgenda = mysql_fetch_assoc($result_agenda)) {
                                        ?>
                                        <option value="<?php echo $rowAgenda['Agenda_Year']; ?>"><?php echo $rowAgenda['Agenda_Year']; ?></option> 
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="add-box-preview" style="margin-top:45px;">
                                <a onclick="show_add_agenda_main_image_form()">
                                    +Add Main Image 
                                </a>
                            </div> 

                            <div class="add-box-preview" >
                                <a onclick="show_add_agenda_minute_form()">
                                    +Add Meeting
                                </a>
                            </div> 
                        </div>

                        <div class="agenda-desc theme-paragraph">
                            <?php echo $descAgenda_title['F_Description']; ?> 
                        </div>
                        <form action="" onsubmit="search_word();return false;" method ="post">
                            <div class="agenda-search-from">
                                <div class="form-inside-div border-none padding-top-bottom agenda-form-main">
                                    <div class="agenda-form">
                                        <input name="searchAgnda" id="search-agenda-key" type="text" placeholder="Search" value="<?php echo $_POST['searchAgnda'] ?>" required>
                                        <input type="submit" name="agenda_form"  value="go"/>
                                    </div>     
                                </div>
                            </div>
                        </form>
                        <div class="agenda-min-download-search" style="display: none;">
                            <div id="search-heading" class="pdf-div-heading " ></div>                                    
                            <?php
                            $complete_agenda = "SELECT * FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' ORDER BY BFAM_Date ASC";
                            $res_cmp_ag = mysql_query($complete_agenda, $db) or die("Invalid query: $complete_agenda -- " . mysql_error());
                            if (mysql_num_rows($res_cmp_ag) > 0) {
                                while ($data_agenda = mysql_fetch_assoc($res_cmp_ag)) {
                                    if ($data_agenda['BFAM_Minute'] != "" || $data_agenda['BFAM_Agenda'] != "") {
                                        $keywords = explode(",", $data_agenda['BFAM_Keywords']);
                                        ?>
                                        <div class="agenda-minutes agenda-show-hide <?php
                                        foreach ($keywords as $keysearch) {
                                            echo 'agenda-' . $keysearch . ' ';
                                        }
                                        ?>" style="display: none;">
                                            <div class="view-agenda-heading"><?php echo date("F d, Y", strtotime($data_agenda['BFAM_Date'])) ?></div>
                                            <?php if ($data_agenda['BFAM_Agenda'] != "") { ?><div class="view-agenda-minute"><a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $data_agenda['BFAM_Agenda'] ?>">View Agenda</a></div><?php } ?>
                                            <?php if ($data_agenda['BFAM_Minute'] != "") { ?><div class="view-agenda-minute"><a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $data_agenda['BFAM_Minute'] ?>">View Minutes</a></div><?php } ?>
                                        </div> 
                                        <?php
                                    }
                                }
                            }
                            ?>
                            <div class="agenda-minutes agenda-show-hide agenda-no-result " style="display: none;">No Result Found...</div> 
                        </div>
                        <?php
                        $agenda = "SELECT YEAR(STR_TO_DATE(BFAM_Date, '%Y-%m-%d')) as Agenda_Year FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' GROUP BY Agenda_Year ORDER BY Agenda_Year ASC";
                        $result_agenda = mysql_query($agenda, $db)
                                or die("Invalid query: $agenda -- " . mysql_error());
                        if (mysql_num_rows($result_agenda) > 0) {
                            while ($rowAgenda = mysql_fetch_array($result_agenda)) {
                                ?>
                                <div class="agenda-min-download agenda-<?php echo $rowAgenda['Agenda_Year']; ?>">
                                    <div class="pdf-div-heading"><?php echo $rowAgenda['Agenda_Year']; ?></div>                                    
                                    <?php
                                    $complete_agenda = "SELECT * FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND YEAR(STR_TO_DATE(BFAM_Date, '%Y-%m-%d')) = '" . encode_strings($rowAgenda['Agenda_Year'], $db) . "' ORDER BY BFAM_Date ASC";
                                    $res_cmp_ag = mysql_query($complete_agenda, $db)
                                            or die("Invalid query: $complete_agenda -- " . mysql_error());
                                    if (mysql_num_rows($res_cmp_ag) > 0) {
                                        while ($data_agenda = mysql_fetch_assoc($res_cmp_ag)) {
//                                                    if ($data_agenda['BFAM_Minute'] != "" || $data_agenda['BFAM_Agenda'] != "") {
                                            ?>
                                            <a class="a_edit agendmin" id="<?php echo $data_agenda['BFAM_ID'] . ' #@ ' . $data_agenda["BFAM_Keywords"] ?>" onclick="show_edit_agenda_minute_form(<?php echo $data_agenda['BFAM_ID'] ?>)">
                                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                            </a>

                                            <div class="agenda-minutes">
                                                <div class="view-agenda-heading"><?php echo date("F d, Y", strtotime($data_agenda['BFAM_Date'])) ?></div>
                                                <?php if ($data_agenda['BFAM_Agenda'] != "") { ?><div class="view-agenda-minute agenda-pdf"><a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $data_agenda['BFAM_Agenda'] ?>">View Agenda</a></div><?php } ?>
                                                <?php if ($data_agenda['BFAM_Minute'] != "") { ?><div class="view-agenda-minute minuts-pdf"><a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $data_agenda['BFAM_Minute'] ?>">View Minutes</a></div><?php } ?>
                                            </div> 
                                            <?php
//                                                    }
                                        }
                                    }
                                    ?>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <!-----agenda End--->                   
                    <?php // } else {  ?>
                    <!--                            <div id="menu-info" class="listing-desc theme-paragraph">
                                                    <div class="menu-header-container heading-margin">
                                                        <div class="menu-heading">Agendas & Minutes</div>
                                                    </div>
                                                    <div class="add-box-preview" style="clear:both">
                                                        <a href="/admin/customer-feature-agenda-minutes.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                                            +Add Agendas & Minutes
                                                        </a>
                                                    </div> 
                                                </div>-->
                    <?php
//                        }
//                    }
//                    if (mysql_num_rows($resultFGB) > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 12)) {
                    ?>
                    <div id="guest-book" class="listing-desc" style="display:none">
                        <div class="menu-header-container">
                            <div class="menu-heading">Guest Book</div>
                        </div>
                        <?php
                        $daily = "SELECT BFGB_Description FROM `tbl_Business_Feature_Guest_Book_Description` WHERE BFGB_BL_ID = '" . $activeBiz['BL_ID'] . "'";
                        $result1Daily = mysql_query($daily, $db);
                        $dataresult1Daily = mysql_fetch_assoc($result1Daily);
                        ?>
                        <div class="agenda-desc theme-paragraph">
                            <?php if ($dataresult1Daily['BFGB_Description'] != '') { ?>
                                <a class="a_edit" id="click_guest_book_desc" onclick="show_add_gb_desc_form()" >
                                    <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                </a>
                                <?php echo $dataresult1Daily['BFGB_Description']; ?>

                            <?php } else { ?>
                                <div class="add-box-preview" >
                                    <a id="click_guest_book_desc" onclick="show_add_gb_desc_form()">
                                        +Add Description 
                                    </a>
                                </div> 
                            <?php } ?>

                        </div>
                        <ul>
                            <?php
                            $sql = "SELECT * FROM tbl_Business_Feature_Guest_Book WHERE BFGB_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND 
                                                                            BFGB_Status = 1";
                            $result1 = mysql_query($sql, $db);
                            while ($data = mysql_fetch_assoc($result1)) {
                                ?>
                                <li class="thumbnail">
    <!--                                        <a class="a_edit" href="/admin/customer-feature-guest-book.php?bl_id=<?php echo $BL_ID ?>&go_to_preview=true">
                                        <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                    </a>-->
                                    <div class="guest-name theme-paragraph"><?php echo $data['BFGB_Name'] ?></div>
                                    <div class="guest-others theme-paragraph"><?php echo $data['BFGB_Title'] ?></div>
                                    <div class="guest-others">
                                        <?php
                                        $rating = $data['BFGB_Rating'];
                                        if ($rating == '1') {
                                            ?>
                                            <img src="<?php echo IMG_LOC_REL ?>selectr.png">
                                            <img src="<?php echo IMG_LOC_REL ?>blank.png">
                                            <img src="<?php echo IMG_LOC_REL ?>blank.png">
                                            <img src="<?php echo IMG_LOC_REL ?>blank.png">
                                            <img src="<?php echo IMG_LOC_REL ?>blank.png">
                                            <?php
                                        } else if ($rating == '2') {
                                            ?>
                                            <img src="<?php echo IMG_LOC_REL ?>selectr.png">
                                            <img src="<?php echo IMG_LOC_REL ?>selectr.png">
                                            <img src="<?php echo IMG_LOC_REL ?>blank.png">
                                            <img src="<?php echo IMG_LOC_REL ?>blank.png">
                                            <img src="<?php echo IMG_LOC_REL ?>blank.png">
                                            <?php
                                        } else if ($rating == '3') {
                                            ?>
                                            <img src="<?php echo IMG_LOC_REL ?>selectr.png">
                                            <img src="<?php echo IMG_LOC_REL ?>selectr.png">
                                            <img src="<?php echo IMG_LOC_REL ?>selectr.png">
                                            <img src="<?php echo IMG_LOC_REL ?>blank.png">
                                            <img src="<?php echo IMG_LOC_REL ?>blank.png">
                                            <?php
                                        } else if ($rating == '4') {
                                            ?>
                                            <img src="<?php echo IMG_LOC_REL ?>selectr.png">
                                            <img src="<?php echo IMG_LOC_REL ?>selectr.png">
                                            <img src="<?php echo IMG_LOC_REL ?>selectr.png">
                                            <img src="<?php echo IMG_LOC_REL ?>selectr.png">
                                            <img src="<?php echo IMG_LOC_REL ?>blank.png">
                                            <?php
                                        } else if ($rating == '5') {
                                            ?>
                                            <img src="<?php echo IMG_LOC_REL ?>selectr.png">
                                            <img src="<?php echo IMG_LOC_REL ?>selectr.png">
                                            <img src="<?php echo IMG_LOC_REL ?>selectr.png">
                                            <img src="<?php echo IMG_LOC_REL ?>selectr.png">
                                            <img src="<?php echo IMG_LOC_REL ?>selectr.png">
                                            <?php
                                        } else {
                                            ?>
                                            <img src="<?php echo IMG_LOC_REL ?>blank.png">
                                            <img src="<?php echo IMG_LOC_REL ?>blank.png">
                                            <img src="<?php echo IMG_LOC_REL ?>blank.png">
                                            <img src="<?php echo IMG_LOC_REL ?>blank.png">
                                            <img src="<?php echo IMG_LOC_REL ?>blank.png">
                                            <?php
                                        }
                                        ?></div>
                                    <div class="guest-others theme-paragraph"><?php echo date("F d, Y", strtotime($data['BFGB_Date'])); ?></div>
                                    <div class="guest-others theme-paragraph"><?php echo $data['BFGB_Desc'] ?></div>
                                </li>
                                <?php
                            }
                            ?>                                             
                        </ul>

                        <div class="form-inside-div heading-border-bottom" style="margin-left: 0">
                            <label class="guest-book-heading">New Comment</label>
                        </div>

                        <?PHP
                        $sql = "SELECT * FROM  tbl_Business_Feature_Guest_Book WHERE BFGB_BL_ID = '$BL_ID' AND BFGB_Status ='0' ORDER BY BFGB_Date DESC";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());

                        while ($data = mysql_fetch_assoc($result)) {
                            ?>
                            <form name="guest-book" method="post" action="">
                                <div class="data-content guest-book-container" style="margin-left: 0 !important">
                                    <input type="hidden" name="BFGB_ID" id="BFGB_ID" value="<?php echo $data['BFGB_ID']; ?>"> 
                                    <input type="hidden" name="id" id="BFGB_ID" value="<?php echo $BL_ID; ?>"> 
                                    <div class="form-inside-div guest-book-form first-div">
                                        <label>Date</label>
                                        <div class="form-data margin-top-guest">
                                            <?php echo $data['BFGB_Date']; ?>
                                        </div>
                                    </div>
                                    <div class="form-inside-div guest-book-form">
                                        <label>Ranking</label>
                                        <div class="form-data margin-top-guest rating-image">
                                            <?php
                                            $rating = $data['BFGB_Rating'];
                                            if ($rating == '1') {
                                                ?>
                                                <img src="../include/img/selectr.png">
                                                <img src="../include/img/blank.png">
                                                <img src="../include/img/blank.png">
                                                <img src="../include/img/blank.png">
                                                <img src="../include/img/blank.png">
                                                <?php
                                            } else if ($rating == '2') {
                                                ?>
                                                <img src="../include/img/selectr.png">
                                                <img src="../include/img/selectr.png">
                                                <img src="../include/img/blank.png">
                                                <img src="../include/img/blank.png">
                                                <img src="../include/img/blank.png">
                                                <?php
                                            } else if ($rating == '3') {
                                                ?>
                                                <img src="../include/img/selectr.png">
                                                <img src="../include/img/selectr.png">
                                                <img src="../include/img/selectr.png">
                                                <img src="../include/img/blank.png">
                                                <img src="../include/img/blank.png">
                                                <?php
                                            } else if ($rating == '4') {
                                                ?>
                                                <img src="../include/img/selectr.png">
                                                <img src="../include/img/selectr.png">
                                                <img src="../include/img/selectr.png">
                                                <img src="../include/img/selectr.png">
                                                <img src="../include/img/blank.png">
                                                <?php
                                            } else if ($rating == '5') {
                                                ?>
                                                <img src="../include/img/selectr.png">
                                                <img src="../include/img/selectr.png">
                                                <img src="../include/img/selectr.png">
                                                <img src="../include/img/selectr.png">
                                                <img src="../include/img/selectr.png">
                                                <?php
                                            } else {
                                                ?>
                                                <img src="../include/img/blank.png">
                                                <img src="../include/img/blank.png">
                                                <img src="../include/img/blank.png">
                                                <img src="../include/img/blank.png">
                                                <img src="../include/img/blank.png">
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-inside-div guest-book-form">
                                        <label>Name</label>
                                        <div class="form-data margin-top-guest">
                                            <?php echo $data['BFGB_Name']; ?>
                                        </div>
                                    </div>
                                    <div class="form-inside-div guest-book-form">
                                        <label>Title</label>
                                        <div class="form-data margin-top-guest">
                                            <?php echo $data['BFGB_Title']; ?>
                                        </div>
                                    </div>
                                    <div class="form-inside-div guest-book-form">
                                        <label>Description</label>
                                        <div class="form-data margin-top-guest desctpion-width">
                                            <?php echo $data['BFGB_Desc']; ?>
                                        </div>
                                    </div>
                                    <div class="form-inside-div guest-book-form">
                                        <input type="submit" name="Approve" class="" value="Approve Review" />
                                        <a href="#!" onclick="deleteitem_guest_book(<?php echo $data['BFGB_ID'] . ", " . $BL_ID ?>)">Decline & Delete</a>
                                    </div>
                                </div>
                            </form>
                            <?php
                        }
                        ?>
                        <div class="data-content guest-book-container" style="margin-left: 0 !important">
                            <div class="form-inside-div heading-border-bottom margin-left-none comment-heading-width">
                                <label class="guest-book-heading">Manage Comments</label>
                            </div>
                            <div class="form-inside-div guest-coment-detail">
                                <div class="data-comment-date">
                                    Date
                                </div>
                                <div class="data-comment-option">
                                    Delete
                                </div>
                                <div class="data-comment-title">
                                    Title
                                </div>

                            </div>

                            <?PHP
                            $sql_comments = "SELECT * FROM  tbl_Business_Feature_Guest_Book WHERE BFGB_BL_ID = '$BL_ID' AND BFGB_Status ='1' ORDER BY BFGB_Date DESC";
                            $result_comments = mysql_query($sql_comments, $db) or die("Invalid query: $sql -- " . mysql_error());

                            while ($data_comment = mysql_fetch_assoc($result_comments)) {
                                ?>
                                <div class="form-inside-div guest-coment-detail">
                                    <div class="data-comment-date">
                                        <?php echo $data_comment['BFGB_Date']; ?> 
                                    </div>
                                    <div class="data-comment-option font-color">
                                        <a href="#!" onclick="deleteitem_guest_book(<?php echo $data_comment['BFGB_ID'] . ", " . $BL_ID ?>)">Delete</a>
                                    </div>
                                    <div class="data-comment-title font-color">
                                        <?php echo $data_comment['BFGB_Title']; ?>
                                    </div>

                                </div>
                                <?php
                            }
                            ?>
                        </div>

                        <?php
                        $sql1 = "SELECT * FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = 12";
                        $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
                        $row1 = mysql_fetch_assoc($result1);
                        if ($row1['BLF_BL_ID'] != '') {
                            ?>
                            <div class="form-inside-div border-none text-align-center">
                                <a href="listings-preview.php?addon_id=<?php echo $row1['BLF_ID'] ?>&bl_id=<?php echo $BL_ID ?>"  onclick="return confirm('Are you sure you want to <?php echo $row1['BLF_Active'] == 1 ? "deactivate" : "activate" ?> this Add on?');"><?php echo $row1['BLF_Active'] == 1 ? "Deactivate" : "Activate" ?> This Add on</a>
                            </div>
                            <?PHP
                        }
                        ?>

                    </div>
                    <?php // } ?>
                    <!-----Guest Book End--->

                    <?php
                    $sql = "SELECT * FROM tbl_Points_Category WHERE PC_ID != 14";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    while ($rowCategories = mysql_fetch_array($result)) {
                        //Recommendations
                        if ($rowCategories['PC_Name'] == 'Recommendations') {
                            $recommends = "SELECT * FROM tbl_Business_Listing 
                                                LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
                                                LEFT JOIN tbl_Recommendations ON R_Recommended = BL_ID 
                                                WHERE R_Recommends = '" . encode_strings($BL_ID, $db) . "'";
                            $recommended = "SELECT * FROM tbl_Business_Listing 
                                                LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
                                                LEFT JOIN tbl_Recommendations ON R_Recommends = BL_ID 
                                                WHERE R_Recommended = '" . encode_strings($BL_ID, $db) . "'";
                            $recommends_result = mysql_query($recommends, $db) or die("Invalid query: $recommends -- " . mysql_error());
                            $recommends_count = mysql_num_rows($recommends_result);
                            $recommended_result = mysql_query($recommended, $db) or die("Invalid query: $recommended -- " . mysql_error());
                            $recommended_count = mysql_num_rows($recommended_result);
                            if ($recommends_count > 0) {
                                $class = 'points-com';
                            } else {
                                $class = 'points-uncom';
                            }
                            $total_recom_points = $rowCategories['PC_Points'];
                        }
                    }
                    ?>
                    <div id="recom" class="listing-desc">
                        <div class=" content-header menu-header-container" style="background:none">
                            <div class="menu-heading">Recommendation</div>
                            <div class="link">
                                <?PHP
                                if ($recommends_count > 0) {
                                    echo '<div class="points"><div class="points-com">' . $total_recom_points . ' pts</div></div>';
                                    $points_taken += $total_recom_points;
                                } else {
                                    echo '<div class="points"><div class="points-uncom">' . $total_recom_points . ' pts</div></div>';
                                }
                                ?>
                            </div>
                        </div>

                        <?php
                        $help_text = show_help_text('Recommendations');
                        if ($help_text != '') {
                            echo '<div class="form-inside-div border-none">' . $help_text . '</div>';
                        }
                        ?>

                        <div class="form-inside-div border-none">
                            <div class="content-sub-header">
                                <div class="title">Add new Recommendation</div>
                            </div>

                            <form name="form" action="" method="get" id="form">
                                <div class="form-inside-div recom margin-none">
                                    <label>
                                        Search
                                    </label>
                                    <input type="text" name="search_text" value="<?php echo (isset($search_text) != '') ? $search_text : ''; ?>" required>
                                    <input type="hidden" name="op" value="save_recom">
                                    <input type="hidden" name="bl_id" value="<?php echo $BL_ID; ?>">

                                    <input type="submit" value="Search" name="search">
                                </div>


                                <div class="form-inside-div recom margin-none">
                                    <label>Category</label>
                                    <div class="form-data">
                                        <select name="category" id="recommendation-cat" onChange="get_subcat();">

                                            <option value="">Select Main Category</option>
                                            <?PHP
                                            $category_sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = 0 AND C_Is_Blog != 1   AND C_Is_Product_Web != 1 ORDER BY C_Name";
                                            $category_result = mysql_query($category_sql, $db)or die("Invalid query: $category_sql -- " . mysql_error());
                                            while ($category_row = mysql_fetch_assoc($category_result)) {
                                                ?>
                                                <option value="<?php echo $category_row['C_ID'] ?>" <?php echo ($category == $category_row['C_ID']) ? 'selected' : ''; ?>><?php echo $category_row['C_Name'] ?></option>
                                                <?PHP
                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>

                                <div class="form-inside-div recom margin-none">
                                    <label>Sub-Category</label>
                                    <div class="form-data" id="recommendation-subcats">
                                        <select name="subcategory" id="recommendation-subcat" onChange="$('#form').submit();">
                                            <option value="">Select Sub Category</option>
                                            <?PHP
                                            if (isset($subcategory)) {
                                                $subcat_sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '$category' ORDER BY C_Name";
                                                $subcat_result = mysql_query($subcat_sql, $db)or die("Invalid query: $subcat_sql -- " . mysql_error());
                                                while ($subcat_row = mysql_fetch_assoc($subcat_result)) {
                                                    ?>
                                                    <option value="<?php echo $subcat_row['C_ID'] ?>" <?php echo ($subcategory == $subcat_row['C_ID']) ? 'selected' : ''; ?>><?php echo $subcat_row['C_Name'] ?></option>
                                                    <?PHP
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </form>

                            <!--            Listings to recommend-->
                            <div class="form-inside-div margin-none border-none">
                                <div class="unordered-list" id="listings-to-recommend">
                                    <ul>
                                        <?PHP
                                        if (isset($search_result)) {
                                            while ($search_row = mysql_fetch_assoc($search_result)) {
                                                ?>
                                                <li ><a style="width:11%;" onclick="return show_textbox_recom(<?php echo $search_row['BL_ID'] ?>)"><img src="../include/img/Add-Button.gif"></a>
                                                    <div class="listing-title">

                                                        <?PHP
                                                        $link_sql = "SELECT R_Domain, C_Name_SEO  FROM tbl_Business_Listing_Category 
                                                                    LEFT JOIN tbl_Category ON BLC_C_ID = C_ID 
                                                                    INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BLC_BL_ID 
                                                                    LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                                                                    WHERE BLC_BL_ID = '" . $search_row['BL_ID'] . "' 
                                                                    LIMIT 1";
                                                        $link_result = mysql_query($link_sql, $db)or die("Invalid query: $link_sql -- " . mysql_error());
                                                        $link_row = mysql_fetch_assoc($link_result);
                                                        if (isset($search_row['BL_Listing_Type']) && $search_row['BL_Listing_Type'] == 1) {
                                                            ?>
                                                            <a href="http://<?php echo $link_row['R_Domain'] ?>/<?php echo $link_row['C_Name_SEO'] ?>/<?php echo $link_row['C_Name_SEO'] ?>/" target="_blank"><?php echo $search_row['BL_Listing_Title'] ?></a>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <a href="http://<?php echo $link_row['R_Domain'] ?>/profile/<?php echo $search_row['BL_Name_SEO'] ?>/<?php echo $search_row['BL_ID'] ?>/" target="_blank"><?php echo $search_row['BL_Listing_Title'] ?></a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <form action="" method="post" name="form" id="recommendation-form<?php echo $search_row['BL_ID'] ?>" style="display:none;">
                                                        <div style="float:left; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                                            <input type="hidden" name="category1" value="<?php echo $category ?>">
                                                            <input type="hidden" name="subcategory1" value="<?php echo $subcategory ?>">
                                                            <input type="hidden" name="search_text1" value="<?php echo $search_text ?>">
                                                            <input type="hidden" name="recommended_bl_id" value="<?php echo $search_row['BL_ID'] ?>">
                                                            <input type="hidden" name="b_email" value="<?php echo $search_row['B_Email'] ?>">
                                                            <input type="hidden" name="recommends_bl_id" value="<?php echo $BL_ID ?>">
                                                            <textarea name="recommendation-text<?php echo $search_row['BL_ID'] ?>" style="width:420px !important;" rows="8"></textarea>
                                                        </div>
                                                        <div style="float:left;text-align: center; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                                            <div class="form-data">
                                                                <input type="submit" name="button_recom" value="Submit"/>
                                                            </div>
                                                        </div> 
                                                    </form></li>
                                                <?PHP
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <?PHP
                        if ($recommends_count > 0) {
                            $recommends_point += $recommends_count - 1;
                            $points_taken += $recommends_point;
                            ?>
                            <!--        Recommendations by this listing-->
                            <div class="form-inside-div border-none" style="width:610px;">
                                <div class="content-sub-header">
                                    <div class="title">Your Existing Recommendations</div>
                                    <div class="link">
                                        <?php
                                        if ($recommends_point > 0) {
                                            echo '<div class="points-com">' . $recommends_point . ' pts</div>';
                                            $points_taken += $phone;
                                        } else {
                                            echo '<div class="points-uncom">' . $recommends_point . ' pts</div>';
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="form-inside-div margin-none border-none" style="width:610px;">
                                    <div class="unordered-list existing-recommendations-custom" style="width:610px;">
                                        <ul>
                                            <?php
                                            while ($recommends_row = mysql_fetch_assoc($recommends_result)) {
                                                ?>

                                                <li>
                                                    <div class="listing-title existing-recommendations">
                                                        <?PHP
                                                        $link_sql1 = "SELECT R_Domain, C_Name_SEO FROM tbl_Business_Listing_Category 
                                                                        LEFT JOIN tbl_Category ON BLC_C_ID = C_ID 
                                                                        INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BLC_BL_ID 
                                                                        LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                                                                        WHERE BLC_BL_ID = '" . $recommends_row['BL_ID'] . "' 
                                                                        LIMIT 1";
                                                        $link_result1 = mysql_query($link_sql1, $db) or die("Invalid query: $link_sql1 -- " . mysql_error());
                                                        $link_row1 = mysql_fetch_assoc($link_result1);
                                                        if ($recommends_row['BL_Listing_Type'] == 1) {
                                                            ?>
                                                            <a href="http://<?php echo $link_row1['R_Domain'] ?>/<?php echo $link_row1['C_Name_SEO'] ?>/<?php echo $link_row1['C_Name_SEO'] ?>/" target="_blank"><?php echo $recommends_row['BL_Listing_Title'] ?></a>
                                                        <?php } else { ?>
                                                            <a href="http://<?php echo $link_row1['R_Domain'] ?>/profile/<?php echo $recommends_row['B_Name_SEO'] ?>/<?php echo $recommends_row['BL_ID'] ?>/" target="_blank"><?php echo $recommends_row['BL_Listing_Title'] ?></a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="existing-recommendations-edit-delete">
                                                        <a style="float:left;" onclick="return show_textbox_existing_recom(<?php echo $recommends_row['R_ID'] ?>)">Edit</a>
                                                        <a href="listings-preview.php?op=delete_recom&rid=<?php echo $recommends_row['R_ID'] ?>&bl_id=<?php echo $BL_ID ?>" onclick="return confirm('Are you sure you want to delete this recommendation?');">Delete</a>
                                                    </div>
                                                    <form action="" method="post" name="form" id="existing-recommendation-form<?php echo $recommends_row['R_ID'] ?>" style="display:none;">
                                                        <div style="float:left; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                                            <input type="hidden" name="recommended_r_id" value="<?php echo $recommends_row['R_ID'] ?>">
                                                            <textarea name="recommendation-text<?php echo $recommends_row['R_ID'] ?>" style="width:420px !important;" rows="8"><?php echo $recommends_row['R_Text'] ?></textarea>
                                                        </div>
                                                        <div style="float:left;text-align: center; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                                            <div class="form-data">
                                                                <input type="submit" name="button-edit-text" value="Submit"/>
                                                            </div>
                                                        </div> 
                                                    </form>
                                                </li>
                                                <?PHP
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        if ($recommended_count > 0) {
                            $recommended_point += $recommended_count * 3;
                            $points_taken += $recommended_point;
                            ?>
                            <!--        Recommendations to this listing-->
                            <div class="form-inside-div border-none">
                                <div class="content-sub-header">
                                    <div class="title">My Recommendations</div>
                                    <div class="link">
                                        <div class="points-com">
                                            <?php echo $recommended_point . " pts" ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-inside-div margin-none border-none">
                                    <div class="unordered-list existing-recommendations-custom">
                                        <ul>
                                            <?php
                                            while ($recommended_row = mysql_fetch_assoc($recommended_result)) {
                                                ?>

                                                <li>
                                                    <div class="listing-title existing-recommendations">
                                                        <?PHP
                                                        $link_sql1 = "SELECT R_Domain, C_Name_SEO FROM tbl_Business_Listing_Category 
                                                                        LEFT JOIN tbl_Category ON BLC_C_ID = C_ID 
                                                                        INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BLC_BL_ID 
                                                                        LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                                                                        WHERE BLC_BL_ID = '" . $recommended_row['BL_ID'] . "' 
                                                                        LIMIT 1";
                                                        $link_result1 = mysql_query($link_sql1, $db) or die("Invalid query: $link_sql1 -- " . mysql_error());
                                                        $link_row1 = mysql_fetch_assoc($link_result1);


                                                        if ($recommended_row['BL_Listing_Type'] == 1) {
                                                            ?>
                                                            <a href="http://<?php echo $link_row1['R_Domain'] ?>/<?php echo $link_row1['C_Name_SEO'] ?>/<?php echo $link_row1['C_Name_SEO'] ?>/" target="_blank"><?php echo $recommended_row['BL_Listing_Title'] ?></a>
                                                        <?php } else { ?>
                                                            <a href="http://<?php echo $link_row1['R_Domain'] ?>/profile/<?php echo $recommended_row['B_Name_SEO'] ?>/<?php echo $recommended_row['BL_ID'] ?>/" target="_blank"><?php echo $recommended_row['BL_Listing_Title'] ?></a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="existing-recommendations-edit-delete">
                                                        <a onclick="return view_recommendation(<?php echo $recommended_row['R_ID'] ?>)">View</a>
                                                        <a href="listings-preview.php?op=delete_recom&rid=<?php echo $recommended_row['R_ID'] ?>&bl_id=<?php echo $BL_ID ?>" onclick="return confirm('Are you sure you want to delete this recommendation?');">Delete</a>
                                                    </div>
                                                    <div class="form-inside-div" id="view-my-recommendation<?php echo $recommended_row['R_ID'] ?>" style="display:none;">
                                                        <?php echo $recommended_row['R_Text'] ?>
                                                    </div>
                                                </li>
                                                <?PHP
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="form-inside-div listing-ranking border-none">
                            Ranking Points: <?php echo $points_taken ?> points
                        </div>

                    </div>
                </div>
                <div class="listing-detail-right">
                    <div class="listing-detail-address border-top-none" style="border:none;">
                        <?php
                        if (isset($BL_ID) && $BL_ID > 0) {
                            $sql = "SELECT BLCR_BLC_R_ID, BLCR_ID FROM tbl_Business_Listing_Category_Region  WHERE BLCR_BL_ID = " . encode_strings($BL_ID, $db);
                            $sql .= $regionLimitCommaSeparated ? (" AND BLCR_BLC_R_ID IN(" . $regionLimitCommaSeparated . ") ") : "";
                            $resultRegion = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($rowRegion = mysql_fetch_assoc($resultRegion)) {
                                ?>
                                <?php
                                $sql = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Type NOT IN(1,6)";
                                $sql .= $regionLimitCommaSeparated ? (" AND R_ID IN(" . $regionLimitCommaSeparated . ") ") : "";
                                $sql .= " ORDER BY R_Name";
                                $resultRegionList = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                ?>                                      
                                <?PHP
                                while ($rowRList = mysql_fetch_assoc($resultRegionList)) {
                                    if ($rowRList['R_ID'] == $rowRegion['BLCR_BLC_R_ID']) {
                                        $regs[] = $rowRList['R_Name'];
                                    }
                                }
                                ?>
                                <?php
                            }
                        }
                        ?>

                        <?php if (isset($regs)) { ?>
                            <a class="a_edit" onclick="show_region_form(<?php echo $BL_ID ?>)">
                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                            </a>  

                            <div style="float:left;">
                                <div class="listing-detail-add-icon">
                                    <img src="/images/regionicon.png" alt="Location">
                                </div>
                                <div class="listing-detail-add-detail">
                                    <?php foreach ($regs as $key => $value) {
                                        ?>
                                        <div class="address-heading"> <?php echo $value ?> </div>
                                    <?php } ?>            
                                </div>
                            </div>
                        <?php } else { ?>

                            <div class="add-box-preview" >
                                <a onclick="show_region_form(<?php echo $BL_ID ?>)">
                                    +Add Region 
                                </a>
                            </div>
                        <?php } ?>



                    </div>
                    <div class="listing-detail-address border-top-none" style="border:none;">
                        <a class="a_edit" onclick="show_category_form(<?php echo $BL_ID ?>)">
                            <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                        </a>  

                        <div style="float:left;">
                            <div class="listing-detail-add-icon">
                                <img src="/images/category.png" alt="Location">
                            </div>
                            <div class="listing-detail-add-detail">
                                <div class="address-heading"> Category </div>          
                            </div>
                        </div>

                        <!--                            <div class="add-box-preview" >
                                                        <a onclick="show_category_form(<?php echo $BL_ID ?>)">
                                                            +Add Category 
                                                        </a>
                                                    </div>-->



                    </div>
                    <?PHP if ($REGION['R_My_Trip'] == 1) { ?>
                        <div class="listing-detail-tripplanner">
                            <div class="address-heading">
                                <?php
                                $sql_b_id = "SELECT BLC_M_C_ID, B_ID
                                        FROM tbl_Business
                                        LEFT JOIN tbl_Business_Listing ON BL_B_ID = B_ID
                                        INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                                        LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                                        LEFT JOIN tbl_Region_Category ON RC_C_ID = BLC_M_C_ID
                                        WHERE BL_ID='" . encode_strings($activeBiz["BL_ID"], $db) . "'";
                                $result_b_id = mysql_query($sql_b_id);
                                $row_b_id = mysql_fetch_array($result_b_id);
                                $sql_remove_trip = "SELECT * FROM  tbl_Session_Tripplanner where ST_Session_ID = '" . encode_strings($_COOKIE["tripplanner"], $db) . "'
                                                AND ST_Bus_ID = " . encode_strings($row_b_id['B_ID'], $db) . "
                                                AND ST_BL_ID = " . encode_strings($activeBiz['BL_ID'], $db) . "
                                                AND ST_Type_ID = " . encode_strings($row_b_id['BLC_M_C_ID'], $db) . "";
                                $result_remove_trip = mysql_query($sql_remove_trip);
                                $count_remove_trip = mysql_num_rows($result_remove_trip);
                                if ($count_remove_trip > 0) {
                                    ?>
                                    <div class="remove-item-tripplanner">
                                        <a href="#" style="line-height: 25px;" class="planner-link" id="<?php echo $row_b_id["BLC_M_C_ID"] . "_" . $row_b_id["B_ID"] . "_" . $activeBiz["BL_ID"] ?>">- Remove from My Trip</a>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="add-item-tripplanner"><a href="#" class="planner-link" id="<?php echo $row_b_id['BLC_M_C_ID'] . "_" . $row_b_id['B_ID'] . '_' . $activeBiz['BL_ID'] ?>">+Add to My Trip</a></div>
                                    <?PHP
                                }
                                ?>
                            </div>    
                        </div>
                    <?PHP } ?>
                    <div class="listing-detail-address border-top-none" <?php if ($activeBiz['BL_Listing_Type'] == 1) { ?> style="background:#cccccc" <?php } ?>>

                        <?php if ($activeBiz['BL_Street'] != "" || $activeBiz['BL_Town'] != "" || $activeBiz['BL_Province'] != "" || $activeBiz['BL_PostalCode'] != "") { ?>
                            <a class="a_edit" onclick="show_customer_listing_mapping(<?php echo $BL_ID ?>)">
                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                            </a>                                               

                            <div style="float:left;">
                                <div class="listing-detail-add-icon">
                                    <img src="/images/locationicon.png" alt="Location">
                                </div>
                                <div class="listing-detail-add-detail">


                                    <div  class="address-heading">
                                        <span id="street"><?php echo $activeBiz['BL_Street']; ?> </span>
                                    </div>
                                    <div class="address-heading">    
                                        <span id="town1"><?php echo $activeBiz['BL_Town']; ?> </span>
                                    </div>     
                                    <div class="address-heading">  
                                        <span id="province1"><?php echo $activeBiz['BL_Province']; ?> </span>
                                    </div>   
                                    <div class="address-heading">  
                                        <span id="postalcode"><?php echo $activeBiz['BL_PostalCode']; ?> </span>
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="add-box-preview" >
                                <a onclick="show_customer_listing_mapping(<?php echo $BL_ID ?>)">
                                    +Add Location Details 
                                </a>
                            </div> 
                        <?php } ?>
                    </div>
                    <?PHP
                    $sql = "SELECT *  
                        FROM tbl_Business_Listing_Ammenity 
                        LEFT JOIN tbl_Ammenity_Icons ON AI_ID = BLA_BA_ID 
                        WHERE BLA_BL_ID = '" . encode_strings($activeBiz['BL_ID'], $db) . "' AND AI_Image <> '' ORDER BY AI_Name";
                    $resultAI = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    $AIcount = mysql_num_rows($resultAI);

                    $sql_dpdf = "SELECT * FROM tbl_Description_PDF WHERE D_BL_ID='" . encode_strings($activeBiz['BL_ID'], $db) . "' ORDER BY D_PDF_Order ASC";
                    $result_dpdf = mysql_query($sql_dpdf, $db)
                            or die("Invalid query: $sql_dpdf -- " . mysql_error());
                    $pdf_num = mysql_num_rows($result_dpdf);
                    ?>
                    <div class="listing-detail-address border-top-none">
                        <?php if ($activeBiz['BL_Phone'] != "" || $activeBiz['BL_Toll_Free'] != "" || $activeBiz['BL_Fax'] != "" || $activeBiz['BL_Cell'] != "" || $activeBiz['BL_Website'] != "" || $activeBiz['BL_Email'] != "") { ?>
                            <a class="a_edit" onclick=" return show_contact_details_form(<?php echo $BL_ID ?>)">
                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                            </a>  

                            <?php if ($activeBiz['BL_Phone']) {
                                ?>
                                <div class="listing-address-container <?php if ($activeBiz['BL_Listing_Type'] == 1) { ?> listing-title-with-bg1 <?php } ?>">
                                    <div class="listing-detail-add-icon">
                                        <img src="/images/Listing-Phone.png" alt="">
                                    </div>
                                    <div class="listing-detail-add-detail">
                                        <div class="address-heading">
                                            <span id="phone1"><?php echo $activeBiz['BL_Phone']; ?> </span> 
                                        </div>   
                                    </div>
                                </div>    
                                <?PHP
                            } if ($activeBiz['BL_Toll_Free']) {
                                ?>
                                <div class="listing-address-container">
                                    <div class="listing-detail-add-icon">
                                        <img src="/images/Listing-Phone.png" alt="">
                                    </div>
                                    <div class="listing-detail-add-detail">
                                        <div class="address-heading">
                                            <span id="tollfree"><?php echo $activeBiz['BL_Toll_Free']; ?> </span>
                                        </div>   
                                    </div>
                                </div>  
                            <?php } if ($activeBiz['BL_Fax']) {
                                ?>
                                <div class="listing-address-container">
                                    <div class="listing-detail-add-icon">
                                        <img src="/images/fax.png" alt="Fax">
                                    </div>
                                    <div class="listing-detail-add-detail">
                                        <div class="address-heading" style=" margin-top: 3px;">
                                            <span id="fax"><?php echo $activeBiz['BL_Fax']; ?> </span>
                                        </div>   
                                    </div>
                                </div> 
                                <?php
                            }
                            if ($activeBiz['BL_Cell']) {
                                ?>
                                <div class="listing-address-container">
                                    <div class="listing-detail-add-icon">
                                        <img src="/images/Mobile.png" alt="Mobile">
                                    </div>
                                    <div class="listing-detail-add-detail">
                                        <div class="address-heading" style=" margin-top: 3px;">
                                            <span id="cell"><?php echo $activeBiz['BL_Cell']; ?> </span>
                                        </div>   
                                    </div>
                                </div> 
                                <?php
                            }
                            if ($activeBiz['BL_Website']) {
                                ?>
                                <div class="listing-address-container">
                                    <div class="listing-detail-add-icon">
                                        <img src="/images/Listing-Website.png" alt="Website">
                                    </div>
                                    <div class="listing-detail-add-detail">
                                        <div class="address-heading listing-leftnav-margin">
                                            <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $activeBiz['BL_Website']) ?>" target="_blank">View Website</a> 
                                        </div>   
                                    </div>
                                </div>
                                <?PHP
                            }
                            if ($activeBiz['BL_Email']) {
                                ?>
                                <div class="listing-address-container <?php if ($activeBiz['BL_Listing_Type'] == 1) { ?> listing-title-with-bg1 <?php } ?>">
                                    <div class="listing-detail-add-icon">
                                        <img src="/images/Listing-Email.png" alt="Email">
                                    </div>
                                    <div class="listing-detail-add-detail">
                                        <div class="address-heading listing-leftnav-margin">
                                            <a href="mailto:<?php echo $activeBiz['BL_Email']; ?>"><?php echo $activeBiz['BL_Email']; ?></a>
                                        </div>   
                                    </div>
                                </div>
                                <?PHP
                            }
                        } else {
                            ?>
                            <div class="add-box-preview" >
                                <a onclick=" return show_contact_details_form(<?php echo $BL_ID ?>)">
                                    +Add Contact Details 
                                </a>
                            </div> 
                            <?php
                        }

                        if (!$activeBiz['BL_Hours_Disabled'] && ($activeBiz['BL_Hours_Appointment'] == 1 || $activeBiz['BL_Hour_Mon_From'] != '00:00:00' || $activeBiz['BL_Hour_Tue_From'] != '00:00:00' || $activeBiz['BL_Hour_Wed_From'] != '00:00:00' || $activeBiz['BL_Hour_Thu_From'] != '00:00:00' || $activeBiz['BL_Hour_Fri_From'] != '00:00:00' || $activeBiz['BL_Hour_Sat_From'] != '00:00:00' || $activeBiz['BL_Hour_Sun_From'] != '00:00:00')) {
                            ?>
                            <a class="a_edit" onclick="show_customer_hours(<?php echo $BL_ID; ?>)">
                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                            </a>  

                            <div class="listing-address-container">
                                <div class="listing-detail-add-icon">
                                    <img src="/images/Listing-Hours.png" alt="Hours">
                                </div>
                                <div class="listing-detail-add-detail">
                                    <div class="address-heading listing-leftnav-margin accordion view-acc">
                                        <h4>View Hours</h4>
                                        <?php
                                        if ($activeBiz['BL_Hours_Appointment']) {
                                            ?>
                                            <div class="businessListing-timing">
                                                <div class="day-heading">Hours</div>
                                                <div class="day-timing">By Appointment</div>
                                            </div>
                                            <?php
                                        } else {
                                            ?>
                                            <div class="businessListing-timing">
                                                <?php
                                                if ($activeBiz['BL_Hour_Mon_From'] != '00:00:00') {
                                                    ?>
                                                    <div class="day-heading">Monday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($activeBiz['BL_Hour_Mon_From']); ?> <?php echo $activeBiz['BL_Hour_Mon_From'] == '00:00:01' || $activeBiz['BL_Hour_Mon_From'] == '00:00:02' || $activeBiz['BL_Hour_Mon_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Mon_To'], true); ?>
                                                    </div>
                                                <?php } if ($activeBiz['BL_Hour_Tue_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Tuesday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($activeBiz['BL_Hour_Tue_From']); ?> <?php echo $activeBiz['BL_Hour_Tue_From'] == '00:00:01' || $activeBiz['BL_Hour_Tue_From'] == '00:00:02' || $activeBiz['BL_Hour_Tue_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Tue_To'], true); ?>
                                                    </div>
                                                <?php } if ($activeBiz['BL_Hour_Wed_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Wednesday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($activeBiz['BL_Hour_Wed_From']); ?> <?php echo $activeBiz['BL_Hour_Wed_From'] == '00:00:01' || $activeBiz['BL_Hour_Wed_From'] == '00:00:02' || $activeBiz['BL_Hour_Wed_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Wed_To'], true); ?>
                                                    </div>
                                                <?php } if ($activeBiz['BL_Hour_Thu_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Thursday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($activeBiz['BL_Hour_Thu_From']); ?> <?php echo $activeBiz['BL_Hour_Thu_From'] == '00:00:01' || $activeBiz['BL_Hour_Thu_From'] == '00:00:02' || $activeBiz['BL_Hour_Thu_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Thu_To'], true); ?>
                                                    </div>
                                                <?php } if ($activeBiz['BL_Hour_Fri_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Friday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($activeBiz['BL_Hour_Fri_From']); ?> <?php echo $activeBiz['BL_Hour_Fri_From'] == '00:00:01' || $activeBiz['BL_Hour_Fri_From'] == '00:00:02' || $activeBiz['BL_Hour_Fri_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Fri_To'], true); ?>
                                                    </div>
                                                <?php } if ($activeBiz['BL_Hour_Sat_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Saturday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($activeBiz['BL_Hour_Sat_From']); ?> <?php echo $activeBiz['BL_Hour_Sat_From'] == '00:00:01' || $activeBiz['BL_Hour_Sat_From'] == '00:00:02' || $activeBiz['BL_Hour_Sat_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Sat_To'], true); ?>
                                                    </div>
                                                <?php } if ($activeBiz['BL_Hour_Sun_From'] != '00:00:00') { ?>
                                                    <div class="day-heading">Sunday</div>
                                                    <div class="day-timing">
                                                        <?php echo ctime($activeBiz['BL_Hour_Sun_From']); ?> <?php echo $activeBiz['BL_Hour_Sun_From'] == '00:00:01' || $activeBiz['BL_Hour_Sun_From'] == '00:00:02' || $activeBiz['BL_Hour_Sun_From'] == '00:00:03' ? '' : '- ' . ctime($activeBiz['BL_Hour_Sun_To'], true); ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>   
                                </div>
                            </div>
                        <?PHP } else {
                            ?>
                            <div class="listing-address-container">
                                <div class="add-box-preview" >
                                    <a onclick="show_customer_hours(<?php echo $BL_ID; ?>)">
                                        +Add Hours 
                                    </a>
                                </div> 
                            </div>
                        <?php }
                        ?>
                        <?php
                        if ($AIcount > 0) {
                            ?>
                            <a class="a_edit" onclick="show_amenities(<?php echo $BL_ID; ?>)">
                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                            </a>  
                            <div class="listing-address-container">
                                <div class="listing-detail-add-icon">
                                    <img src="/images/Listing-Ammenity.png" alt="Ammenity">
                                </div>
                                <div class="listing-detail-add-detail">
                                    <div class="address-heading listing-leftnav-margin accordion view-acc">                               
                                        <h4>View Amenities</h4>
                                        <div class="businessListing-timing">
                                            <?php
                                            while ($rowAI = mysql_fetch_assoc($resultAI)) {
                                                ?>
                                                <div class="acc-body">
                                                    <?php echo $rowAI['AI_Name']; ?>
                                                </div>    
                                            <?PHP } ?>
                                        </div> 
                                    </div>   
                                </div>                      
                            </div>
                        <?php } else {
                            ?>
                            <div class="listing-address-container">
                                <div class="add-box-preview" >
                                    <a onclick="show_amenities(<?php echo $BL_ID; ?>)">
                                        +Add Amenities 
                                    </a>
                                </div> 
                            </div>
                        <?php } ?>
                        <div class="listing-address-container">
                            <?php
                            if ($pdf_num > 0) {
                                ?>
                                <a class="a_edit" onclick="show_pdf(<?php echo $BL_ID; ?>)">
                                    <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                                </a> 
                                <div class="listing-detail-add-icon">
                                    <img src="/images/Listing-Download.png" alt="Download">
                                </div>
                                <div class="listing-detail-add-detail">
                                    <div class="address-heading listing-leftnav-margin accordion view-acc">
                                        <h4>View Downloads</h4>
                                        <div class="businessListing-timing">
                                            <?php
                                            while ($data_dpdf = mysql_fetch_assoc($result_dpdf)) {
                                                ?>
                                                <div class="acc-body theme-downloads"><a target="_blank" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $data_dpdf['D_PDF'] ?>"><?php echo $data_dpdf['D_PDF_Title']; ?></a></div>
                                                <?php
                                            }
                                            ?>
                                        </div>   
                                    </div>   
                                </div>

                            <?php } else { ?>
                                <div class="add-box-preview" >
                                    <a onclick="show_pdf(<?php echo $BL_ID; ?>)">
                                        +Upload PDF 
                                    </a>
                                </div> 
                            <?php } ?> </div>

                        <?php
//                        if (mysql_num_rows($resultFGB) > 0 || ($activeBiz['BL_Third_Party'] != "" && $activeBiz['BL_Active_Addon'] == 12)) {
                        ?>
                        <div class="listing-address-container">
                            <div class="listing-detail-add-icon">
                                <img src="/images/Listing-Guest-Book.png" alt="Guest Book">
                            </div>
                            <div class="listing-detail-add-detail">
                                <div class="address-heading listing-leftnav-margin">
                                    <a href="#guest-book" onclick="$('#guest-book').show();">Guest Book</a>
                                </div>   
                            </div>
                        </div>
                        <?php // } ?>

                        <div class="listing-address-container">
                            <div class="listing-detail-add-icon">
                                <img src="/images/Listing-Guest-Book.png" alt="Recommendation">
                            </div>
                            <div class="listing-detail-add-detail">
                                <div class="address-heading listing-leftnav-margin">
                                    <a href="#recom" onclick="$('#recom').show();">Recommendation</a>
                                </div>   
                            </div>
                        </div>

                        <div class="listing-address-container">
                            <div class="listing-detail-add-icon">
                                <img src="/images/season.png" alt="Seasons">
                            </div>
                            <div class="listing-detail-add-detail">
                                <div class="address-heading listing-leftnav-margin">
                                    <a onclick="show_season_form()">Seasons</a>
                                </div>   
                            </div>
                        </div>

                        <div class="listing-address-container">
                            <div class="listing-detail-add-icon">
                                <img src="/images/Bokun.png" alt="Bokun">
                            </div>
                            <div class="listing-detail-add-detail">
                                <div class="address-heading listing-leftnav-margin">
                                    <a onclick="show_bokun_form()">Bokun</a>
                                </div>   
                            </div>
                        </div>

                        <div class="listing-address-container">
                            <div class="listing-detail-add-icon">
                                <img src="/images/seo.png" alt="SEO">
                            </div>
                            <div class="listing-detail-add-detail">
                                <div class="address-heading listing-leftnav-margin">
                                    <a onclick="show_seo_form()">SEO</a>
                                </div>   
                            </div>
                        </div>
                    </div>
                    <?php
                    $social_blld = $activeBiz['BL_ID'];
                    $sql_social = "SELECT * FROM  tbl_Business_Social where BS_BL_ID=$social_blld";
                    $social_result = mysql_query($sql_social);
                    $row_soical = mysql_fetch_assoc($social_result);
                    ?>
                    <div class="listings-social-icon">
                        <?php
                        if ($row_soical['BS_FB_Link'] || $row_soical['BS_T_Link'] || $row_soical['BS_Y_Link'] || $row_soical['BS_I_Link']) {
                            ?>
                            <a class="a_edit" onclick="return show_customer_social_media(<?php echo $BL_ID; ?>)">
                                <img class="editBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />
                            </a> 

                            <?php if ($row_soical['BS_FB_Link'] != '') { ?>
                                <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $row_soical['BS_FB_Link']) ?>" target="_blank"><img src="<?php echo '/images/facebook.png' ?>"/></a>
                                <?php
                            }
                            if ($row_soical['BS_I_Link'] != '') {
                                ?> 
                                <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $row_soical['BS_I_Link']) ?>" target="_blank"><img src="<?php echo '/images/instagram.png' ?>"/></a>

                                <?php
                            }
                            ?>                                        
                            <?php
                            if ($row_soical['BS_T_Link'] != '') {
                                $twitter = explode("?", $row_soical['BS_T_Link']);
                                ?>
                                <a href="http://<?php echo str_replace(array('http://', 'https://'), '', $twitter[0]) ?>" target="_blank"><img src="<?php echo '/images/twitter.png' ?>"/></a>

                            <?php }
                            ?>

                        <?php } else { ?>
                            <div class="add-box-preview" >
                                <a onclick="return show_customer_social_media(<?php echo $BL_ID; ?>)">
                                    +Add Social Media 
                                </a>
                            </div> 
                        <?php } ?>
                    </div>
                </div>
                <!--right end -->
            </div>
        </div>
    </section>
    <style>
        .ui-accordion .ui-accordion-header{
            padding:0px;
        }
        .ui-accordion .ui-accordion-icons {
            padding-left: 0px; 
        }
    </style>


</div>

<!-- CONTACT DETAILS-->
<?php
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing_contact_details = mysql_fetch_assoc($result);
    $BID = $rowListing_contact_details['B_ID'];
}
$sql = "SELECT * FROM tbl_Points_Category WHERE PC_ID != 14";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
while ($rowCategories = mysql_fetch_array($result)) {
    //Contact Details
    if ($rowCategories['PC_Name'] == 'Contact Details') {
        if (isset($rowListing_contact_details['BL_Listing_Title']) && $rowListing_contact_details['BL_Listing_Title'] != '' && isset($rowListing_contact_details['BL_Contact']) && $rowListing_contact_details['BL_Contact'] != '' && $rowListing_contact_details['BL_Phone'] != '' && $rowListing_contact_details['BL_Website'] != '' && $rowListing_contact_details['BL_Toll_Free'] != '' && $rowListing_contact_details['BL_Email'] != '') {
            $class = 'points-com';
        } else {
            $class = 'points-uncom';
        }
        $total_cd_points = $rowCategories['PC_Points'];
    }
}
?>
<form class="cont_dialog" name="form1" id="contact_details_from_<?php echo $BL_ID ?>" method="post" action="" style="display:none;">
    <script>
        function makeSEO(myVar) {
            myVar = myVar.toLowerCase();
            myVar = myVar.replace(/[^a-z0-9\s-]/gi, '');
            myVar = myVar.replace(/"/g, '');
            myVar = myVar.replace(/'/g, '');
            myVar = myVar.replace(/&/g, '');
            myVar = myVar.replace(/\//g, '');
            myVar = myVar.replace(/,/g, '');
            myVar = myVar.replace(/  /g, ' ');
            myVar = myVar.replace(/ /g, '-');

            $('#SEOname').val(myVar);
            return false;
        }
    </script>
    <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
    <input type="hidden" name="op" value="save_contact_details">
    <div class="content-header">Contact Details</div>

    <div class="form-inside-div">
        <label>Listing Title</label>
        <div class="form-data">
            <input class="instructional-background" name="name" type="text" id="BLname" value="<?php echo $rowListing_contact_details['BL_Listing_Title'] ?>" size="50" onKeyUp="makeSEO(this.value)" required/>
        </div>
        <?php
        $listing = show_field_points('Listing Title');
        $points_taken = 0;
        if ($rowListing_contact_details['BL_Listing_Title']) {
            echo '<div class="points-com">' . $listing . ' pts</div>';
            $points_taken += $listing;
        } else {
            echo '<div class="points-uncom">' . $listing . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div">
        <label>SEO Name</label>
        <div class="form-data">
            <input name="seoName" id="SEOname" type="text" value="<?php echo $rowListing_contact_details['BL_Name_SEO'] ?>" onKeyUp="makeSEO(this.value)" size="50" required/>
        </div>
    </div>

    <div class="form-inside-div">
        <label>Contact Name</label>
        <div class="form-data">
            <input name="contact" type="text" value="<?php echo $rowListing_contact_details['BL_Contact'] ?>" size="50" />
        </div>
        <?php
        $contact = show_field_points('Contact Name');
        if ($rowListing_contact_details['BL_Contact']) {
            echo '<div class="points-com">' . $contact . ' pts</div>';
            $points_taken += $contact;
        } else {
            echo '<div class="points-uncom">' . $contact . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div">
        <label>Phone</label>
        <div class="form-data">
            <input class="instructional-background" name="phone" class="phone_number" type="text" value="<?php echo $rowListing_contact_details['BL_Phone'] ?>" size="50"/>
        </div>
        <?php
        $phone = show_field_points('Phone');
        if ($rowListing_contact_details['BL_Phone']) {
            echo '<div class="points-com">' . $phone . ' pts</div>';
            $points_taken += $phone;
        } else {
            echo '<div class="points-uncom">' . $phone . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div">
        <label>Toll Free</label>
        <div class="form-data">
            <input name="tollFree" class="phone_toll" type="text" value="<?php echo $rowListing_contact_details['BL_Toll_Free'] ?>" size="50" />
        </div>
        <?php
        $toll = show_field_points('Toll Free');
        if ($rowListing_contact_details['BL_Toll_Free']) {
            echo '<div class="points-com">' . $toll . ' pts</div>';
            $points_taken += $toll;
        } else {
            echo '<div class="points-uncom">' . $toll . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div">
        <label>Cell</label>
        <div class="form-data">
            <input name="cell" class="phone_number" type="text" value="<?php echo $rowListing_contact_details['BL_Cell'] ?>" size="50" />
        </div>
    </div>

    <div class="form-inside-div">
        <label>Fax</label>
        <div class="form-data">
            <input name="fax" class="phone_number" type="text" value="<?php echo $rowListing_contact_details['BL_Fax'] ?>" size="50" />
        </div>
    </div>

    <div class="form-inside-div">
        <label>Email</label>
        <div class="form-data">
            <input class="instructional-background" name="email" type="email" value="<?php echo $rowListing_contact_details['BL_Email'] ?>" size="50" />
        </div>
        <?php
        $Email = show_field_points('Email');
        if ($rowListing_contact_details['BL_Email']) {
            echo '<div class="points-com">' . $Email . ' pts</div>';
            $points_taken += $Email;
        } else {
            echo '<div class="points-uncom">' . $Email . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div">
        <label>Website</label>
        <div class="form-data">
            <input name="website" type="text" value="<?php echo $rowListing_contact_details['BL_Website'] ?>" size="50" />
        </div>
        <?php
        $website = show_field_points('Website');
        if ($rowListing_contact_details['BL_Website']) {
            echo '<div class="points-com">' . $website . ' pts</div>';
            $points_taken += $website;
        } else {
            echo '<div class="points-uncom">' . $website . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div border-none">
        <div class="button">
            <input type="submit" name="button2" value="Save Now"/>
        </div>
    </div>

    <div class="form-inside-div listing-ranking border-none">
        Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_cd_points ?> points
    </div>
</form>
<!--END-->

<!-- CUSTOMER LISTING MAPPING -->
<?php
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words, BL_Location_Description, BL_Country  
            FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing_listing_mapping = mysql_fetch_assoc($result);
    $BID = $rowListing_listing_mapping['B_ID'];
}
$sql = "SELECT * FROM tbl_Points_Category WHERE PC_ID != 14";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
while ($rowCategories = mysql_fetch_array($result)) {
    //Mapping
    if ($rowCategories['PC_Name'] == 'Location & Mapping') {
        if (isset($rowListing_listing_mapping['BL_Street']) && $rowListing_listing_mapping['BL_Street'] != '' && $rowListing_listing_mapping['BL_Town'] != '' && $rowListing_listing_mapping['BL_Province'] != '' && $rowListing_listing_mapping['BL_PostalCode'] != '' && $rowListing_listing_mapping['BL_Lat'] != '' && $rowListing_listing_mapping['BL_Long'] != '') {
            $class = 'points-com';
        } else {
            $class = 'points-uncom';
        }
        $total_mapping_points = $rowCategories['PC_Points'];
    }
}
?>
<form class="cont_dialog" name="form1" id="customer_listing_mapping_from_<?php echo $BL_ID ?>" method="post" action="" onsubmit="return checkVal()" style="display:none">
    <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
    <input type="hidden" name="op" value="save_cutomer_listing_mapping">
    <div class="content-header">Location & Mapping</div>

    <div class="form-inside-div">
        <label>Street Address</label>
        <div class="form-data">
            <input class="instructional-background" name="address" type="text" id="address" value="<?php echo $rowListing_listing_mapping['BL_Street'] ?>" size="50" />
        </div>
        <?php
        $address = show_field_points('Street Address');
        $points_taken = 0;
        if ($rowListing_listing_mapping['BL_Street']) {
            echo '<div class="points-com">' . $address . ' pts</div>';
            $points_taken += $address;
        } else {
            echo '<div class="points-uncom">' . $address . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div">
        <label>Town</label>
        <div class="form-data">
            <input class="instructional-background" name="town" type="text" id="town" value="<?php echo $rowListing_listing_mapping['BL_Town'] ?>" size="50" />
        </div>
        <?php
        $town = show_field_points('Town');
        if ($rowListing_listing_mapping['BL_Town']) {
            echo '<div class="points-com">' . $town . ' pts</div>';
            $points_taken += $town;
        } else {
            echo '<div class="points-uncom">' . $town . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div">
        <label>Province</label>
        <div class="form-data">
            <input class="instructional-background" name="province" type="text" id="province" value="<?php echo $rowListing_listing_mapping['BL_Province'] ?>" size="50" />
        </div>
        <?php
        $province = show_field_points('Province');
        if ($rowListing_listing_mapping['BL_Province']) {
            echo '<div class="points-com">' . $province . ' pts</div>';
            $points_taken += $province;
        } else {
            echo '<div class="points-uncom">' . $province . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div">
        <label>Postal Code</label>
        <div class="form-data">
            <input class="instructional-background" name="postalcode" type="text" id="postalcode" value="<?php echo $rowListing_listing_mapping['BL_PostalCode'] ?>" size="50" />
        </div>
        <?php
        $postal = show_field_points('Postal Code');
        if ($rowListing_listing_mapping['BL_PostalCode']) {
            echo '<div class="points-com">' . $postal . ' pts</div>';
            $points_taken += $postal;
        } else {
            echo '<div class="points-uncom">' . $postal . ' pts</div>';
        }
        ?>
    </div>

    <div class="form-inside-div">
        <label>Country</label>
        <div class="form-data">
            <input name="country" type="text" id="town" value="<?php echo $rowListing_listing_mapping['BL_Country'] ?>" size="50" />
        </div>
    </div>

    <div class="form-inside-div">
        <label>Location Description</label>
        <div class="form-data">
            <textarea name="locationdescription" cols="85" rows="10" id="description-listing1"><?php echo $rowListing_listing_mapping['BL_Location_Description'] ?></textarea>
        </div>
    </div>

    <div class="form-inside-div">
        <div class="content-sub-header">
            <div class="title">Google Maps</div>
        </div>
        <?php
        $help_text = show_help_text('Google Maps');
        if ($help_text != '') {
            echo '<div class="gmap-left">' . $help_text . '</div>';
        }
        ?>
        <script type="text/javascript">
            function checkVal() {
                var latitudeVal = $("#latitude").val();
                var longitudeVal = $("#longitude").val();
                if (latitudeVal != '' && longitudeVal == '') {
                    alert('Longitude must be entered.');
                    $("#longitude").focus();
                    return false;
                }
                if (latitudeVal == '' && longitudeVal != '') {
                    alert('Latitude must be entered.');
                    $("#longitude").focus();
                    return false;
                }
            }
        </script>
        <div class="gmap-right">
            <div class="form-inside-div-gmap">
                <label>Latitude</label>
                <div class="form-data">
                    <input name="latitude" type="text" id="latitude" value="<?php echo $rowListing_listing_mapping['BL_Lat'] ?>" size="10" />
                </div>
                <?php
                $lat = show_field_points('Latitude');
                if ($rowListing_listing_mapping['BL_Lat']) {
                    echo '<div class="points-com">' . $lat . ' pts</div>';
                    $points_taken += $lat;
                } else {
                    echo '<div class="points-uncom">' . $lat . ' pts</div>';
                }
                ?>
            </div>
            <div class="form-inside-div-gmap border-none">
                <label>Longitude</label>
                <div class="form-data">
                    <input name="longitude" type="text" id="longitude" value="<?php echo $rowListing_listing_mapping['BL_Long'] ?>" size="10" />
                </div>
                <?php
                $long = show_field_points('Latitude');
                if ($rowListing_listing_mapping['BL_Long']) {
                    echo '<div class="points-com">' . $long . ' pts</div>';
                    $points_taken += $long;
                } else {
                    echo '<div class="points-uncom">' . $long . ' pts</div>';
                }
                ?>
            </div>
        </div>
    </div>

    <div class="form-inside-div border-none">
        <div class="button">
            <input type="submit" name="button2" value="Save Now"/>
        </div>
    </div> 

    <div class="form-inside-div listing-ranking border-none">
        Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_mapping_points ?> points
    </div>

</form>
<!--END-->

<!-- CUSTOMER LISTING MAIN PHOTO -->
<?php
if (isset($BL_ID) && $BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words, BL_Video FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing_main_photo = mysql_fetch_assoc($result);
    $BID = $rowListing_main_photo['B_ID'];
}
?>
<form class="cont_dialog" action="" id="customer_listing_main_photo_<?php echo $BL_ID ?>" method="post" enctype="multipart/form-data" name="form1" style="display: none">
    <input type="hidden" name="op" value="save_main_photo">
    <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
    <div class="content-header">Main Photos / Video</div>
    <?PHP ?>
    <div class="form-inside-div" style="border:none;">
        <div class="content-sub-header">
            <div class="title">Thumbnail</div>
            <div class="link">
                <?php
                $thumbnail = show_field_points('Thumbnail');
                if ($rowListing_main_photo['BL_Photo']) {
                    echo '<div class="points-com">' . $thumbnail . ' pts</div>';
                    $points_taken = $thumbnail;
                } else {
                    echo '<div class="points-uncom">' . $thumbnail . ' pts</div>';
                }
                ?>
            </div>
        </div>

        <div class="form-inside-div margin-none border-none">
            <div class="div_image_library listing-main-photo">
                <span class="daily_browse thumbnail-button" onclick="show_image_library(21)">Select File</span>
                <input type="hidden" name="thumb_photo" id="image_bank21" value="">
                <input type="file" onchange="show_file_name_pc(21, this)" name="pic[]" id="photo21" style="display: none;">
                <div class="cropped-container">
                    <div class="image-editor">
                        <div class="cropit-image-preview-container margin-left-thumnail">
                            <div class="cropit-image-preview thumb-photo-perview">
                                <img class="preview-img thumb-preview preview-img-script21" style="display: none;" src="">    
                                <?php if ($rowListing_main_photo['BL_Photo'] != "") { ?>
                                    <img class="existing-img existing_imgs21 thumb-preview max-width-thumbnail" src="http://<?php echo DOMAIN . IMG_LOC_REL . $rowListing_main_photo['BL_Photo'] ?>">
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if ($rowListing_main_photo['BL_Photo'] != "") { ?><div class="data-column delete-main-photo1 padding-none thumbnail-button"><a class="deletePhoto margin-left-main" onClick="return confirm('Are you sure?');"  href="listings-preview.php?bl_id=<?php echo $BL_ID ?>&type=0&op=del">Delete Photo</a></div><?php } ?>
            </div>
        </div>
    </div>

    <div class="form-inside-div" style="border:none;">
        <div class="content-sub-header">
            <div class="title">Main Image</div>
            <div class="link">
                <?php
                $mainPhoto = show_field_points('Main Image');
                if ($rowListing_main_photo['BL_Header_Image']) {
                    echo '<div class="points-com">' . $mainPhoto . ' pts</div>';
                    $points_taken = $mainPhoto;
                } else {
                    echo '<div class="points-uncom">' . $mainPhoto . ' pts</div>';
                }
                ?>
            </div>
        </div>
        <style type="text/css">


        </style>
        <div class="form-inside-div margin-none border-none">
            <div class="cropped-container1">
                <div class="cropit-image-preview main-preview">
                    <img id="test" class="preview-img preview-img-script22" style="display: none;" src="">    
                    <?php if ($rowListing_main_photo['BL_Header_Image'] != "") { ?>
                        <img class="existing-img existing_imgs22 max-width-thumbnail" src="http://<?php echo DOMAIN . IMG_LOC_REL . $rowListing_main_photo['BL_Header_Image'] ?>">
                    <?php } ?>
                </div>
            </div>
            <div class="div_image_library main-image-buttons">
                <span class="daily_browse" onclick="show_image_library(22)">Select File</span>
                <input type="hidden" name="main_photo" id="image_bank22" value="">
                <input type="file" onchange="show_file_name_pc(22, this)" name="pic[]" id="photo22" style="display: none;">
                <?php if ($rowListing_main_photo['BL_Header_Image'] != "") { ?><div class="data-column delete-main-photo1 padding-none"><a class="deletePhoto margin-left-main" onClick="return confirm('Are you sure?');"  href="listings-preview.php?bl_id=<?php echo $BL_ID ?>&type=1&op=del_main_photo">Delete Photo</a></div><?php } ?>
            </div>
        </div>

    </div>
    <div class="form-inside-div">
        <label>Video</label>
        <div class="form-data">
            <input name="home_video"  type="text" value="<?php echo $rowListing_main_photo['BL_Video'] ?>" size="50" />
        </div>
    </div>

    <div class="form-inside-div border-none">
        <div class="button">
            <input type="submit" name="button" id="button" value="Save" />
        </div>
    </div>

    <div class="form-inside-div border-none main-photos">
        <div class="content-sub-header">
            <div class="title">Image Uploading Tips</div>
        </div>
        <?php
        $help_text = show_help_text('Main Photos');
        if ($help_text != '') {
            echo '<div class="form-inside-div margin-none border-none">' . $help_text . '</div>';
        }
        ?>
    </div>     
</form>
<!--END-->

<!-- CUSTOMER FEATURE GALLERY -->
<form class="cont_dialog" name="form1" id="customer_feature_gallery<?php echo $BL_ID ?>" method="post" onSubmit="return check_img_size(101, 10000000)"  enctype="multipart/form-data" action="" style="display:none;float:left;width:100%;margin-bottom: 10px;">
    <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
    <input type="hidden" name="op" value="save_customer_feature_gallery">
    <input type="hidden" name="image_bank" class="image_bank" id="image_bank101" value="">
    <div class="content-header">
        <div class="title">Add Photo</div>
        <div class="link">
            <?PHP
            $Gal = show_addon_points(2);
            if ($count_gallery > 0) {
                echo '<div class="points"><div class="points-com">' . $Gal . ' pts</div></div>';
            } else {
                echo '<div class="points"><div class="points-uncom">' . $Gal . ' pts</div></div>';
            }
            ?>
        </div>
    </div>
    <div class="form-inside-div">
        <label>Title</label>
        <div class="form-data">
            <input name="title" type="text" id="title" size="50" required/>
        </div>
    </div>

    <div class="form-inside-div">
        <label>Photo</label>
        <div class="form-data form-input-text">
            <div class="div_image_library">
                <span onchange="show_file_name(101, this, 0)" onclick="show_image_library(101, null,<?php echo $count_gallery ?>)">Select File</span>
                <input accept="image/*" id="photo101" type="file" name="pic[]" style="display: none;" multiple />
            </div>
            <input style="width:170px;" id="uploadFile1" class="uploadFileName" disabled>
            <div class="form-inside-div add-about-us-item-image margin-none" id="preview" style="display: none;">
                <div class="cropit-image-preview aboutus-photo-perview">
                    <img class="preview-img preview-img-script101" src="">    
                </div>
            </div>
        </div>
    </div>
    <div class="form-inside-div">
        <div class="button">
            <input type="submit" name="button" id="button" value="Save Photo" />
        </div>
    </div>

    <div class="form-inside-div border-none">
        <div class="form-data">
            Photo file limit size is 10MBs. If you're files are large they may take 10 seconds to load once you click "Save Photo".
        </div>
    </div>
    <?php
    $sql1 = "SELECT BLF_BL_ID, BLF_ID, BLF_Active FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = 2";
    $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
    $row1 = mysql_fetch_assoc($result1);
    if ($row1['BLF_BL_ID'] != '') {
        ?>
        <div class="form-inside-div border-none text-align-center ">
            <a href="listings-preview.php?addon_id=<?php echo $row1['BLF_ID'] ?>&bl_id=<?php echo $BL_ID ?>"  onclick="return confirm('Are you sure you want to <?php echo $row1['BLF_Active'] == 1 ? "deactivate" : "activate" ?> this Add on?');"><?php echo $row1['BLF_Active'] == 1 ? "Deactivate" : "Activate" ?> This Add on</a>
        </div>
        <?PHP
    }
    ?>
</form>

<div class="cont_dialog" id="gallery_ordering" style="display:none">
    <div class="form-inside-div border-none margin-none" id="listing-gallery">
        <ul class="gallery">
            <?PHP
            $counter = 1;
            $sqlGal = "SELECT * FROM tbl_Business_Feature_Photo	WHERE BFP_BL_ID = '" . encode_strings($BL_ID, $db) . "' ORDER BY BFP_Order";
            $resultGal = mysql_query($sqlGal, $db) or die("Invalid query: $sqlGal -- " . mysql_error());
            while ($data = mysql_fetch_assoc($resultGal)) {
                ?>
                <li class="image" id="recordsArray_<?php echo $data['BFP_ID'] ?>">
                    <?php if ($data['BFP_Photo_285X210'] != "") { ?>
                        <div class="listing-image"><img class="max-width-thumbnail" src="http://<?php echo DOMAIN . IMG_LOC_REL . $data['BFP_Photo_285X210'] ?>" alt="" /></div>
                    <?php } else { ?>
                        <div class="listing-image"><img class="max-width-thumbnail"  src="http://<?php echo DOMAIN . IMG_LOC_REL . $data['BFP_Photo'] ?>" alt="" width="285" height="210"/></div>
                    <?php } ?>
                    <div class="count"><?php echo $counter ?></div>
                    <div class="cross"><a onClick="return confirm('Are you sure?');" href="customer-feature-gallery-photo-delete.php?op=del&id=<?php echo $data['BFP_ID'] ?>&bl_id=<?php echo $data['BFP_BL_ID'] ?>&go_to_preview=<?php echo $_REQUEST['go_to_preview'] ?>">x</a></div>
                    <div class="desc" onclick="show_description(<?php echo $data['BFP_ID'] ?>);"><?php echo ($data['BFP_Title'] != '') ? $data['BFP_Title'] : '+ Add Description'; ?></div> 

                    <form action="" method="post" name="form" id="gallery-description-form<?php echo $data['BFP_ID'] ?>" style="display:none;">
                        <div style="float:left; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                            <input type="hidden" name="op" value="edit">
                            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
                            <input type="hidden" name="bfp_id" value="<?php echo $data['BFP_ID'] ?>">
                            <textarea name="description<?php echo $data['BFP_ID'] ?>" style="width:420px !important;" size="50" required><?php echo $data['BFP_Title'] ?></textarea>
                        </div>
                        <div style="float:left;text-align: center; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                            <div class="form-data">
                                <input type="submit" name="button" value="Save Now"/>
                            </div>
                        </div> 
                    </form>
                </li>
                <?PHP
                $counter++;
            }
            ?>
        </ul>
    </div>
</div>
<script>
    $(function () {
        $("#listing-gallery ul.gallery").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Business_Feature_Photo&field=BFP_Order&id=BFP_ID';
                $.post("reorder.php", order, function (theResponse) {
                    swal("success", "Re-order scuccessfully", "success");
                });
            }
        });
    });
</script>



<!--END-->

<!-- CUSTOMER HOURS -->
<form class="cont_dialog" name="form1" method="post" id="hours_form_<?php echo $BL_ID; ?>" action="" style="display:none;">
    <?php
    if ($BL_ID > 0) {
        $sql = "SELECT B_ID, BL_Listing_Title, BL_Hour_Mon_From, BL_Hour_Mon_To, BL_Hour_Tue_From, BL_Hour_Tue_To, BL_Hour_Wed_From, BL_Hour_Wed_To, 
            BL_Hour_Thu_From, BL_Hour_Thu_To, BL_Hour_Fri_From, BL_Hour_Fri_To, BL_Hour_Sat_From, BL_Hour_Sat_To, BL_Hour_Sun_From, BL_Hour_Sun_To, 
            BL_Hours_Disabled, BL_Hours_Appointment  FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $rowListing = mysql_fetch_assoc($result);
        $BID = $rowListing['B_ID'];
    }

    $sql = "SELECT * FROM tbl_Points_Category WHERE PC_ID != 14";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($rowCategories = mysql_fetch_array($result)) {
        //Hours
        if ($rowCategories['PC_Name'] == 'Hours') {
            $sqlHours = "SELECT * FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
            $resultHours = mysql_query($sqlHours) or die(mysql_error());
            $rowsHours = mysql_fetch_array($resultHours);
            if ($rowsHours) {
                if (!$rowsHours['BL_Hours_Disabled'] && ($rowsHours['BL_Hour_Mon_From'] != '00:00:00' || $rowsHours['BL_Hour_Tue_From'] != '00:00:00' || $rowsHours['BL_Hour_Wed_From'] != '00:00:00' || $rowsHours['BL_Hour_Thu_From'] != '00:00:00' || $rowsHours['BL_Hour_Fri_From'] != '00:00:00' || $rowsHours['BL_Hour_Sat_From'] != '00:00:00' || $rowsHours['BL_Hour_Sun_From'] != '00:00:00')) {
                    $class = 'points-com';
                } else {
                    $class = 'points-uncom';
                }
            }
            $total_hours_points = $rowCategories['PC_Points'];
        }
    }
    ?> 
    <div class="right">
        <input type="hidden" name="op" value="save_hours">
        <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
        <input type="hidden" name="bfc_id" value="<?php echo $id; ?>">
        <div class="content-header">
            <div class="title">Hours</div>
            <div class="link">
                <?php
                $sqlHours = "SELECT * FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
                $resultHours = mysql_query($sqlHours) or die(mysql_error());
                $rowsHours = mysql_fetch_array($resultHours);
                $rowsHours['BL_Hours_Disabled'];
                if ($rowsHours) {
                    if (!$rowsHours['BL_Hours_Disabled'] && ($rowsHours['BL_Hour_Mon_From'] != '00:00:00' || $rowsHours['BL_Hour_Tue_From'] != '00:00:00' || $rowsHours['BL_Hour_Wed_From'] != '00:00:00' || $rowsHours['BL_Hour_Thu_From'] != '00:00:00' || $rowsHours['BL_Hour_Fri_From'] != '00:00:00' || $rowsHours['BL_Hour_Sat_From'] != '00:00:00' || $rowsHours['BL_Hour_Sun_From'] != '00:00:00')) {
                        $class = 'points-com';
                    } else {
                        $class = 'points-uncom';
                    }
                }
//                $total_hours_points = $rowCategories['PC_Points'];
                $Hours = show_field_points('Hours');
                if (!$rowsHours['BL_Hours_Disabled'] && ($rowsHours['BL_Hour_Mon_From'] != '00:00:00' || $rowsHours['BL_Hour_Tue_From'] != '00:00:00' || $rowsHours['BL_Hour_Wed_From'] != '00:00:00' || $rowsHours['BL_Hour_Thu_From'] != '00:00:00' || $rowsHours['BL_Hour_Fri_From'] != '00:00:00' || $rowsHours['BL_Hour_Sat_From'] != '00:00:00' || $rowsHours['BL_Hour_Sun_From'] != '00:00:00')) {
                    echo '<div class="points-com des-heading-point">' . $Hours . ' pts</div>';
                    $points_taken = $Hours;
                } else {
                    echo '<div class="points-uncom">' . $Hours . ' pts</div>';
                }
                ?>
            </div>
        </div>

        <?php
        $help_text = show_help_text('Hours');
        if ($help_text != '') {
            echo '<div class="form-inside-div" style="border-bottom:3px solid #eeeeee;">' . $help_text . '</div>';
        }
        ?>
        <div class="form-inside-div hours">
            <label>Monday</label>
            <div class="form-data">
                <select name="monFrom" id="monFrom" onchange="isClosed('monHide', this.value);">
                    <?PHP fromHours($rowListing['BL_Hour_Mon_From']); ?>
                </select>
                <span id="monHide" <?php echo $rowListing['BL_Hour_Mon_From'] == '00:00:01' || $rowListing['BL_Hour_Mon_From'] == '00:00:02' || $rowListing['BL_Hour_Mon_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                    <select name="monTo" id="monTo" onchange="">
                        <?PHP toHours($rowListing['BL_Hour_Mon_To']); ?>
                    </select>
                </span>
            </div>
        </div>
        <div class="form-inside-div hours">
            <label>Tuesday</label>
            <div class="form-data">
                <select name="tueFrom" id="tueFrom" onchange="isClosed('tueHide', this.value);">
                    <?PHP fromHours($rowListing['BL_Hour_Tue_From']); ?>
                </select>
                <span id="tueHide" <?php echo $rowListing['BL_Hour_Tue_From'] == '00:00:01' || $rowListing['BL_Hour_Tue_From'] == '00:00:02' || $rowListing['BL_Hour_Tue_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                    <select name="tueTo" id="tueTo" onchange="">
                        <?PHP toHours($rowListing['BL_Hour_Tue_To']); ?>
                    </select>
                </span>
            </div>
        </div>

        <div class="form-inside-div hours">
            <label>Wednesday</label>
            <div class="form-data">
                <select name="wedFrom" id="wedFrom" onchange="isClosed('wedHide', this.value);">
                    <?PHP fromHours($rowListing['BL_Hour_Wed_From']); ?>
                </select>
                <span id="wedHide" <?php echo $rowListing['BL_Hour_Wed_From'] == '00:00:01' || $rowListing['BL_Hour_Wed_From'] == '00:00:02' || $rowListing['BL_Hour_Wed_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                    <select name="wedTo" id="wedTo" onchange="">
                        <?PHP toHours($rowListing['BL_Hour_Wed_To']); ?>
                    </select>
                </span>
            </div>
        </div>

        <div class="form-inside-div hours">
            <label>Thursday</label>
            <div class="form-data">
                <select name="thuFrom" id="thuFrom" onchange="isClosed('thuHide', this.value);">
                    <?PHP fromHours($rowListing['BL_Hour_Thu_From']); ?>
                </select>
                <span id="thuHide" <?php echo $rowListing['BL_Hour_Thu_From'] == '00:00:01' || $rowListing['BL_Hour_Thu_From'] == '00:00:02' || $rowListing['BL_Hour_Thu_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                    <select name="thuTo" id="thuTo" onchange="">
                        <?PHP toHours($rowListing['BL_Hour_Thu_To']); ?>
                    </select>
                </span>
            </div>
        </div>

        <div class="form-inside-div hours">
            <label>Friday</label>
            <div class="form-data">
                <select name="friFrom" id="friFrom" onchange="isClosed('friHide', this.value);">
                    <?PHP fromHours($rowListing['BL_Hour_Fri_From']); ?>
                </select>
                <span id="friHide" <?php echo $rowListing['BL_Hour_Fri_From'] == '00:00:01' || $rowListing['BL_Hour_Fri_From'] == '00:00:02' || $rowListing['BL_Hour_Fri_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                    <select name="friTo" id="friTo" onchange="">
                        <?PHP toHours($rowListing['BL_Hour_Fri_To']); ?>
                    </select>
                </span>
            </div>
        </div>

        <div class="form-inside-div hours">
            <label>Saturday</label>
            <div class="form-data">
                <select name="satFrom" id="satFrom" onchange="isClosed('satHide', this.value);">
                    <?PHP fromHours($rowListing['BL_Hour_Sat_From']); ?>
                </select>
                <span id="satHide" <?php echo $rowListing['BL_Hour_Sat_From'] == '00:00:01' || $rowListing['BL_Hour_Sat_From'] == '00:00:02' || $rowListing['BL_Hour_Sat_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                    <select name="satTo" id="satTo" onchange="">
                        <?PHP toHours($rowListing['BL_Hour_Sat_To']); ?>
                    </select>
                </span>
            </div>
        </div>

        <div class="form-inside-div hours">
            <label>Sunday</label>
            <div class="form-data">
                <select name="sunFrom" id="sunFrom" onchange="isClosed('sunHide', this.value);">
                    <?PHP fromHours($rowListing['BL_Hour_Sun_From']); ?>
                </select>
                <span id="sunHide" <?php echo $rowListing['BL_Hour_Sun_From'] == '00:00:01' || $rowListing['BL_Hour_Sun_From'] == '00:00:02' || $rowListing['BL_Hour_Sun_From'] == '00:00:03' ? " style='display:none'" : "" ?> >to
                    <select name="sunTo" id="sunTo">
                        <?PHP toHours($rowListing['BL_Hour_Sun_To']); ?>
                    </select>
                </span>
            </div>
        </div>

        <div class="form-inside-div">
            <label>Disable Hours</label>
            <div class="form-data">
                <input name="hDisabled" type="checkbox" style="margin-top: 9px;" value="1" <?php echo $rowListing['BL_Hours_Disabled'] ? 'checked' : '' ?>>
            </div>
        </div>
        <div class="form-inside-div">
            <label>By Appointment</label>
            <div class="form-data">
                <input name="happointment" type="checkbox" style="margin-top: 9px;" value="1" <?php echo $rowListing['BL_Hours_Appointment'] ? 'checked' : '' ?>>
            </div>
        </div>

        <div class="form-inside-div border-none">
            <div class="button">
                <input type="submit" name="submit" value="Save Now"/>
            </div>
        </div>
        <div class="form-inside-div listing-ranking border-none">
            Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_hours_points ?> points
        </div>
    </div>
    <?php

    function fromHours($time) {
        echo "<option value='00:00:00'>Select One</option>";
        for ($i = 1; $i <= 23; $i++) {
            if ($i == 0) {
                echo "<option value='" . date('H:00:04', mktime($i)) . "'";
                echo $time == date('H:00:04', mktime($i)) ? ' selected' : '';
                echo ">" . date('g:ia', mktime($i, 0)) . "</option>";
            } else {
                echo "<option value='" . date('H:00:00', mktime($i)) . "'";
                echo $time == date('H:00:00', mktime($i)) ? ' selected' : '';
                echo ">" . date('g:ia', mktime($i, 0)) . "</option>";
            }
            echo "<option value='" . date('H:i:00', mktime($i, 30)) . "'";
            echo $time == date('H:i:00', mktime($i, 30)) ? ' selected' : '';
            echo ">" . date('g:ia', mktime($i, 30)) . "</option>";
        }
        echo "<option value='00:00:04'";
        echo $time == '00:00:04' ? ' selected' : '';
        echo ">12:00am</option>";
        echo "<option value='00:30:00'";
        echo $time == '00:30:00' ? ' selected' : '';
        echo ">12:30am</option>";
        echo "<option value='00:00:01'";
        echo $time == '00:00:01' ? ' selected' : '';
        echo ">Closed</option>";
        echo "<option value='00:00:03'";
        echo $time == '00:00:03' ? ' selected' : '';
        echo ">open 24 hours</option>";
    }

    function toHours($time) {
        echo "<option value='00:00:00'>Select One</option>";
        for ($i = 1; $i <= 23; $i++) {
            if ($i == 0) {
                echo "<option value='" . date('H:00:01', mktime($i)) . "'";
                echo $time == date('H:00:01', mktime($i)) ? ' selected' : '';
                echo ">" . date('g:ia', mktime($i, 0)) . "</option>";
            } else {
                echo "<option value='" . date('H:00:00', mktime($i)) . "'";
                echo $time == date('H:00:00', mktime($i)) ? ' selected' : '';
                echo ">" . date('g:ia', mktime($i, 0)) . "</option>";
            }
            echo "<option value='" . date('H:i:00', mktime($i, 30)) . "'";
            echo $time == date('H:i:00', mktime($i, 30)) ? ' selected' : '';
            echo ">" . date('g:ia', mktime($i, 30)) . "</option>";
        }
        echo "<option value='00:00:04'";
        echo $time == '00:00:04' ? ' selected' : '';
        echo ">12:00am</option>";
        echo "<option value='00:30:00'";
        echo $time == '00:30:00' ? ' selected' : '';
        echo ">12:30am</option>";
    }
    ?>
</form> 
<!--END-->

<!--SOCIAL MEDIA-->
<?php
$sql = "SELECT * FROM tbl_Points_Category WHERE PC_ID != 14";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
while ($rowCategories = mysql_fetch_array($result)) {
    //Amenities
    if ($rowCategories['PC_Name'] == 'Social Media') {
        $sqlSM = "SELECT * FROM tbl_Business_Social WHERE BS_BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
        $resultSM = mysql_query($sqlSM) or die(mysql_error());
        $rowsSM = mysql_fetch_array($resultSM);
        if ($rowsSM['BS_FB_Link'] != '' || $rowsSM['BS_T_Link'] != '' || $rowsSM['BS_Y_Link'] || $rowsSM['BS_I_Link']) {
            $class = 'points-com';
        } else {
            $class = 'points-uncom';
        }
        $total_sm_points = $rowCategories['PC_Points'];
    }
}
?>
<form name="form1" method="post" id="social_media_<?php echo $BL_ID; ?>" action="" style="display:none;">
    <input type="hidden" name="op" value="save_social_media">
    <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
    <div class="content-header">
        <div class="title">Social Media</div>
        <div class="link">
            <?php
            $sqlSM = "SELECT * FROM tbl_Business_Social WHERE BS_BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
            $resultSM = mysql_query($sqlSM) or die(mysql_error());
            $rowsSM = mysql_fetch_array($resultSM);
            $SM = show_field_points('Hours');
            if ($rowsSM['BS_FB_Link'] != '' || $rowsSM['BS_T_Link'] != '' || $rowsSM['BS_Y_Link'] || $rowsSM['BS_I_Link']) {
                echo '<div class="points-com des-heading-point">' . $SM . ' pts</div>';
                $points_taken = $SM;
            } else {
                echo '<div class="points-uncom des-heading-point">' . $SM . ' pts</div>';
            }
            ?>
        </div>
    </div>

    <?php
    $help_text = show_help_text('Social Media');
    if ($help_text != '') {
        echo '<div class="form-inside-div" style="border-bottom:3px solid #eeeeee;">' . $help_text . '</div>';
    }
    ?>

    <?PHP
    $sql = "SELECT BS_FB_Link, BS_T_Link, BS_I_Link FROM tbl_Business_Social WHERE BS_BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $count = mysql_num_rows($result);
    $row = mysql_fetch_assoc($result);
    ?> 

    <div class="form-inside-div">
        <label>Facebook Link</label>
        <div class="form-data">
            <input name="fb_link" type="text" value="<?php echo $row['BS_FB_Link'] ?>" size="50" />
        </div>
    </div>

    <div class="form-inside-div">
        <label>Twitter Link</label>
        <div class="form-data">
            <input name="t_link" type="text"  value="<?php echo $row['BS_T_Link'] ?>" size="50" />
        </div>
    </div>

    <div class="form-inside-div">
        <label>Instagram Link</label>
        <div class="form-data">
            <input name="i_link" type="text"  value="<?php echo $row['BS_I_Link'] ?>" size="50" />
        </div>
    </div>

    <div class="form-inside-div">
        <div class="button">
            <input type="hidden" name="counter" value="<?php echo $count; ?>">
            <input type="submit" name="button" value="Submit" />
        </div>
    </div>


    <div class="form-inside-div listing-ranking border-none">
        Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_sm_points ?> points
    </div>
</form>
<!--END-->

<!--AMENITIES-->
<?php
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words, C_Name FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID LEFT JOIN tbl_Category ON BLC_M_C_ID = C_ID
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
}
$sql = "SELECT * FROM tbl_Points_Category WHERE PC_ID != 14";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
while ($rowCategories = mysql_fetch_array($result)) {
    //Amenities
    if ($rowCategories['PC_Name'] == 'Amenities') {
        $amm = "SELECT * FROM tbl_Business_Listing_Ammenity WHERE BLA_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $amm_result = mysql_query($amm, $db) or die("Invalid query: $amm -- " . mysql_error());
        $amm_count = mysql_num_rows($amm_result);
        if ($amm_count > 0) {
            $class = 'points-com';
        } else {
            $class = 'points-uncom';
        }
        $total_amenities_points = $rowCategories['PC_Points'];
    }
}
?>
<form class="cont_dialog" name="form1" method="post" id="amenities_<?php echo $BL_ID; ?>" action="" style="display:none;">
    <div class="content-header">
        <div class="title">Amenities - <?php echo $rowListing['C_Name'] ?></div>
        <div class="link">
            <?PHP
            $points_taken = 0;
            if ($amm_count > 0) {
                echo '<div class="points"><div class="points-com">' . $total_amenities_points . ' pts</div></div>';
                $points_taken += $total_amenities_points;
            } else {
                echo '<div class="points"><div class="points-uncom">' . $total_amenities_points . ' pts</div></div>';
            }
            ?>
        </div>
    </div>

    <?php
    $help_text = show_help_text('Amenities');
    if ($help_text != '') {
        echo '<div class="form-inside-div border-none">' . $help_text . '</div>';
    }
    ?>

    <?PHP
    if ($rowListing['BL_Listing_Type'] == 1) { // FREE
        echo '<div class="data-content">
                        Not available for free listings
                    </div>';
    } else {
        $listingAmmenities = array();
        while ($rowBA = mysql_fetch_row($amm_result)) {
            $listingAmmenities[] = $rowBA[0];
        }

        $sql = "SELECT AI_ID, AI_Image, AI_Name FROM tbl_Ammenity_Icons ORDER BY AI_Name";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $counter = 0;
        while ($row = mysql_fetch_assoc($result)) {
            if ($counter == 0) {
                $class = "amenity-column";
            } else {
                $class = "amenity-column2";
            }
            ?>
            <div class="<?php echo $class ?>">
                <div class="amenity-image">
                    <input name="ckbxAMM<?php echo $row['AI_ID'] ?>" type="checkbox" id="ckbxAMM<?php echo $row['AI_ID'] ?>" value="<?php echo $row['AI_ID'] ?>" align="absmiddle" <?php echo in_array($row['AI_ID'], $listingAmmenities) ? 'checked' : '' ?> />
                </div>
                <div class="amenity-name"><?php echo $row['AI_Name'] ?></div>
            </div>
            <?PHP
            $counter++;
            if ($counter == 2) {
                $counter = 0;
            }
        }
        ?>
        <div class="form-inside-div border-none">
            <div class="button">
                <input type="submit" onClick="return updateAMM(<?php echo $BL_ID ?>);" value="Save Now">
            </div>
        </div>
    <?php }
    ?>

    <div class="form-inside-div listing-ranking border-none" style="margin-top: 25px;">
        Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_amenities_points ?> points
    </div>
</form>
<!--END-->

<!--PDF-->
<?php
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
}
?>
<div class="cont_dialog" id="upload_pdf_<?php echo $BL_ID; ?>" style="display:none">
    <div class="content-header">
        <div class="title">Upload A PDF</div>
        <div class="link">
        </div>
    </div>
    <?php
    $help_text = show_help_text('Upload a PDF');
    if ($help_text != '') {
        echo '<div class="form-inside-div border-none">' . $help_text . '</div>';
    }
    ?>
    <form method="post" onsubmit="" id="pdf-update_<?php echo $BL_ID; ?>" name="form1" enctype="multipart/form-data" action="">
        <input type="hidden" name="op" value="save_pdf">
        <input type="hidden"  name="bl_id" value="<?php echo $BL_ID ?>" required>
        <input type="hidden"  class="upload_pdf_page" value="1">
        <div class="form-inside-div border-none des-pdf padding-top-zero" >
            <label for="description_pdf0" class="">Browse for PDF</label>
            <input class="file_ext0" onchange="show_file_name1(this.files[0].name, 1)" id="description_pdf0" type="file" name="file" accept="application/pdf" value="" style="visibility: hidden; position: absolute; z-index: 0"/>
            <div class="des-input">    
                <input type="text" name="pdf_title" class="required_title0" placeholder="Enter Title" value="" required/>
            </div>
            <input id="uploadFile_pdf" class="uploadFileName" disabled>
        </div>
        <div class="form-inside-div border-none">
            <div class="button">
                <input type="submit" name="buttonpdf"  onclick="" value="Save Now"/>
            </div>  
        </div>
    </form>
<!--    <style type="text/css">
        .ajaxSpinnerImage{
            display: none;
            z-index: -9999;   
        }
    </style>-->
    <div class="progress-container-main" style="display:none;">
        <div id="progress-container">
            <p class="progress-title">Upload in Progress</p>
            <p>Please leave this page open while your PDF is uploading</p>
            <div id="progressbox" style="display:none;">
                <div id="progressbar"></div>
                <div id="statustxt">0%</div>
            </div>
        </div>
    </div>
    <div class="form-inside-div border-none padding-top-des">
        <div class="content-sub-header">
            <div class="title">Manage PDFs</div>
        </div>
    </div>

    <div class="brief">
        <?php
        $sql_dpdf = "SELECT * from tbl_Description_PDF where D_BL_ID=$BL_ID ORDER BY D_PDF_Order ASC";
        $result_dpdf = mysql_query($sql_dpdf, $db) or die("Invalid query: $sql_dpdf -- " . mysql_error());

        while ($data_dpdf = mysql_fetch_assoc($result_dpdf)) {
            ?>
            <div class="form-inside-div border-none padding-top-none" id="recordsArray_<?php echo $data_dpdf['D_id'] ?>">
                <div class="data-pdf-title">
                    <a href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $data_dpdf['D_PDF'] ?>" target="_blank"><?php echo $data_dpdf['D_PDF_Title']; ?></a> 
                </div>
                <div class="data-pdf-edit">
                    <a class="delete-pointer" onclick="Editpdf(<?php echo $data_dpdf['D_id'] ?>)">Edit</a>
                </div>
                <div class="data-des-option">
                    <a class="delete-pointer" onClick="return confirm('Are you sure this action can not be undone!');" href="listings-preview.php?delete=<?php echo $data_dpdf['D_id'] ?>&op=del_pdf&bl_id=<?php echo $BL_ID ?>">Delete</a>
                </div>
                <form action="" enctype="multipart/form-data" onsubmit="" method="post" name="form" id="pdf-update<?php echo $data_dpdf['D_id'] ?>" style="display:none; overflow: hidden">
                    <input type="hidden" name="up" value="upload_pdf">
                    <input type="hidden"  name="d_id_update" value="<?php echo $data_dpdf['D_id'] ?>" required>
                    <input type="hidden"  name="bl_id" value="<?php echo $BL_ID ?>" required>
                    <input type="hidden"  class="upload_pdf_page" value="1">
                    <div class="pop-des-label">
                        <label for="description_pdf<?php echo $data_dpdf['D_id'] ?>" class="">Browse for PDF</label>
                        <input class="file_ext<?php echo $data_dpdf['D_id'] ?>" id="description_pdf<?php echo $data_dpdf['D_id'] ?>" type="file" name="file" accept="application/pdf" value="" style="visibility: hidden; position: absolute; z-index: 0"/>
                        <div class="popup-des-input">
                            <input type="text" class="required_title<?php echo $data_dpdf['D_id'] ?>" name="pdf_update" placeholder="Enter Title" value="<?php echo $data_dpdf['D_PDF_Title'] ?>" required/>
                        </div>
                    </div>
                    <div class="form-inside-div border-none text-algn-center">
                        <div class="button">
                            <input type="submit" class="first-div" name="update_pdf" onclick="" value="Save Now"/>
                        </div>  
                    </div>
                </form>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<!--END-->

<!--Description Text-->
<?php
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
}
$sql = "SELECT * FROM tbl_Points_Category WHERE PC_ID != 14";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
while ($rowCategories = mysql_fetch_array($result)) {
    //Description Text
    if ($rowCategories['PC_Name'] == 'Description Text') {
        if (isset($rowListing['BL_Description']) && $rowListing['BL_Description'] != '') {
            $class = 'points-com';
        } else {
            $class = 'points-uncom';
        }
        $total_desc_points = $rowCategories['PC_Points'];
    }
}
?>
<form class="cont_dialog" id="desc_text_<?php echo $BL_ID; ?>" action="" method="post" enctype="multipart/form-data" name="form1" style="display:none">
    <input type="hidden" name="op" value="save_desc_text">
    <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
    <div class="content-header">
        <div class="title">Description Text</div>
        <div class="link">
            <?php
            $desc = show_field_points('Description');
            if (isset($rowListing['BL_Description'])) {
                echo '<div class="points-com des-heading-point">' . $desc . ' pts</div>';
                $points_taken = $desc;
            } else {
                echo '<div class="points-uncom">' . $desc . ' pts</div>';
            }
            ?>
        </div>
    </div>
    <div class="form-inside-div border-none bottom-pading-none">
        <div class="form-data">
            <textarea    name="maindescription" cols="85" rows="10" id="description-listing-text"><?php echo $rowListing['BL_Description'] ?></textarea>                           
        </div>
    </div>
    <script>
//        $(document).ready(function () {
//            CKEDITOR.config.width = 555;
//
//        });

    </script>

    <div class="form-inside-div border-none">
        <div class="button">
            <input type="submit" name="button2" value="Save Now"/>
        </div>  
    </div>

    <div class="form-inside-div listing-ranking border-none">
        Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_desc_points ?> points
    </div>

    <div class="form-inside-div border-none">
        <div class="content-sub-header">
            <div class="title">Writing Content Tips</div>
        </div>
        <?php
        $help_text = show_help_text('Description');
        if ($help_text != '') {
            echo '<div class="form-inside-div margin-none border-none">' . $help_text . '</div>';
        }
        ?>
    </div>
</form>
<!--END-->

<!--TRIP ADVISOR--> 
<?php
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words, BL_Trip_Advisor FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
}
$sql = "SELECT * FROM tbl_Points_Category WHERE PC_ID != 14";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
while ($rowCategories = mysql_fetch_array($result)) {
    //Trip Advisor
    if ($rowCategories['PC_Name'] == 'Trip Advisor') {
        if (isset($rowListing['BL_Trip_Advisor']) && $rowListing['BL_Trip_Advisor'] != '') {
            $class = 'points-com';
        } else {
            $class = 'points-uncom';
        }
        $total_trip_points = $rowCategories['PC_Points'];
    }
}
?>
<div class="cont_dialog" id="trip_advisor_<?php echo $BL_ID; ?>" style="display:none">
    <form  action="" method="post" name="form1">
        <input type="hidden" name="op" value="save_trip_advisor">
        <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
        <div class="content-header">
            <div class="title">Trip Advisor</div>
            <div class="link">
                <?php
                $points_taken = 0;
                $trip_point = show_field_points('Trip Advisor');
                if ($rowListing['BL_Trip_Advisor']) {
                    echo '<div class="points-com des-heading-point">' . $trip_point . ' pts</div>';
                    $points_taken = $trip_point;
                } else {
                    echo '<div class="points-uncom">' . $trip_point . ' pts</div>';
                }
                ?>
            </div>
        </div>
        <div class="form-inside-div border-none bottom-pading-none">
            <div class="form-data">
                <textarea name="tripadvisor" cols="73" rows="8" style="width : 539px;"><?php echo $rowListing['BL_Trip_Advisor'] ?></textarea>                                   
            </div>
        </div>

        <div class="form-inside-div border-none">
            <div class="button">
                <input type="submit" name="button2" value="Save Now"/>
            </div>  
        </div>
    </form>
    <div class="form-inside-div listing-ranking border-none">
        Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_trip_points ?> points
    </div>
    <div class="form-inside-div">
        <div class="content-sub-header border-none">
            <div class="title">Instructions</div>
        </div>
        <?php
        $help_text = show_help_text('Trip Advisor');
        if ($help_text != '') {
            echo '<div class="form-inside-div margin-none border-none trip-advisor-links">' . $help_text . '</div>';
        }
        ?>
    </div>
</div>
<!--END-->

<!--MENU-->
<?php
$sql = "SELECT * FROM tbl_Feature WHERE F_ID NOT IN (3)";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
while ($row = mysql_fetch_assoc($result)) {
    //Menu
    if ($row['F_Name'] == 'Menu') {
        $sqlMenu = "SELECT * FROM tbl_Business_Feature_Menu WHERE BFM_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $resultMenu = mysql_query($sqlMenu, $db) or die("Invalid query: $sqlMenu -- " . mysql_error());
        $numRowsMenu = mysql_num_rows($resultMenu);
        if ($numRowsMenu > 0 && $row1['BLF_Active'] == "1") {
            $class = 'points-com';
        } else {
            $class = 'points-uncom';
        }
    }
}
?>
<div style="display:none">

    <?PHP
    foreach ($menuSections as $key => $val) {
        ?>
        <form name="menu-item" id="desc_form_<?php echo $key ?>" method="post" action=""  style="float:left;">
            <input type="hidden"  name="menu-des-type" value="<?php echo $key ?>">
            <input type="hidden"  name="menu-des-id" value="<?php echo $BL_ID ?>">
            <div class="content-header">
                <div class="title">Menu</div>
                <div class="link">
                    <?php
                    $Menu = show_addon_points(6);
                    if ($numRowsMenu > 0) {
                        echo '<div class="points-com">' . $Menu . ' pts</div>';
                    } else {
                        echo '<div class="points-uncom">' . $Menu . ' pts</div>';
                    }
                    ?>
                </div>
            </div>
            <?php
            $help_text = show_help_text('Menu');
            if ($help_text != '') {
                echo '<div class="form-inside-div border-none">' . $help_text . '</div>';
            }
            ?>
            <div class="form-inside-div add-menu-item-new ">
                <?php
                $sql_des = "Select BFMD_Description from tbl_Business_Feature_Menu_Description where BFMD_Type='$key' AND BFMD_BL_ID= $BL_ID";
                $result_des = mysql_query($sql_des);
                $row_des = mysql_fetch_assoc($result_des);
                ?>
                <label>Description</label>
                <div class="form-data add-menu-item-field wiziwig-menu">
                    <textarea name="descriptionNEW_TYPE" cols="40" rows="5" wrap="VIRTUAL" class="formtext textarea-width-menu" id="descriptionmain<?php echo $key ?>"><?php echo $row_des['BFMD_Description']; ?></textarea>
                </div>
                <script>
                    $('.add_descr_<?php echo $key ?>').click(function () {
                        $('#desc_form_<?php echo $key ?>').dialog('open');
                        $('#descriptionmain<?php echo $key ?>').ckeditor();
                    });
                    $(function () {
                        $('#desc_form_<?php echo $key ?>').attr("title", "Add Description").dialog({
                            autoOpen: false,
                        });
                    });
                </script>
            </div>
            <div class="form-inside-div">
                <div class="button menu-new-btn">
                    <input type="submit" name="save_des" value="Save Item" />
                </div>
            </div>
            <?php
            $sql1 = "SELECT BLF_BL_ID, BLF_ID, BLF_Active FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = 6";
            $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
            $row1 = mysql_fetch_assoc($result1);
            if ($row1['BLF_BL_ID'] != '') {
                ?>
                <div class="form-inside-div border-none text-align-center">
                    <a href="listings-preview.php?addon_id=<?php echo $row1['BLF_ID'] ?>&bl_id=<?php echo $BL_ID ?>"  onclick="return confirm('Are you sure you want to <?php echo $row1['BLF_Active'] == 1 ? "deactivate" : "activate" ?> this Add on?');"><?php echo $row1['BLF_Active'] == 1 ? "Deactivate" : "Activate" ?> This Add on</a>
                </div>
                <?PHP
            }
            ?>
        </form> 
        <form name="menu-item" method="post" action="" id="gallery-form<?php echo $key ?>" style="display: none; float:left;">
            <input type="hidden" id="menu-type-<?php echo $key ?>" name="menu-type" value="<?php echo $key ?>">
            <input type="hidden" id="menu-type-id-<?php echo $BL_ID ?>" name="menu-type-id" value="<?php echo $BL_ID ?>">
            <div class="form-inside-div add-menu-item-new">
                <label>Name</label>
                <div class="form-data add-menu-item-field menu-input-width">
                    <input name="title" type="text" id="title<?php echo $key ?>" size="50" required/>
                </div>
            </div>
            <div class="form-inside-div add-menu-item-new ">
                <label>Description</label>
                <div class="form-data add-menu-item-field wiziwig-menu">
                    <textarea name="descriptionNEW" cols="40" rows="5" wrap="VIRTUAL" class="formtext textarea-width-menu" id="descriptionNEW<?php echo $key ?>"></textarea>
                </div>
                <script>
                    $('.add_items_<?php echo $key ?>').click(function () {
                        $('#gallery-form<?php echo $key ?>').dialog('open');
                        $('#descriptionNEW<?php echo $key ?>').ckeditor();
                    });
                    $(function () {
                        $('#gallery-form<?php echo $key ?>').attr("title", "Add Item").dialog({
                            autoOpen: false,
                        });
                    });
                </script>
            </div>
            <div class="form-inside-div add-menu-item-new">
                <label>Price<span>$</span></label>
                <div class="form-data add-menu-item-field menu-input-width">
                    <input name="priceNEW" type="text" id="priceNEW<?php echo $key ?>" value="" size="5" />
                </div>
            </div>
            <div class="form-inside-div add-menu-item-image margin-bottom-prod margin-top-newp">
            </div>
            <div class="form-inside-div add-menu-item-image">
            </div>
            <div class="form-inside-div">
                <div class="button menu-new-btn">
                    <input type="submit" name="button33" class="button3<?php echo $key ?>"  value="Save Item" />
                </div>
            </div>   
        </form>
        <?PHP
        $sql = "SELECT BFM_ID, BFM_Title, BFM_Description, BFM_Price FROM tbl_Business_Feature_Menu 
                                WHERE BFM_BL_ID = '$BL_ID' AND 
				BFM_Type= '$key' ORDER BY BFM_Order";
        $result = mysql_query($sql, $db)
                or die("Invalid query: $sql -- " . mysql_error());
        $counter = 1;
        $numRows = mysql_num_rows($result);
        ?>
        <?php
        while ($data = mysql_fetch_assoc($result)) {
            ?>
            <form id="records_<?php echo $data['BFM_ID'] ?>" name="menu-item" method="post"   style="float:left;">
                <input type="hidden"  name="menu-des-type" value="<?php echo $data['BFM_ID'] ?>">
                <input type="hidden"  name="menu-des-id" value="<?php echo $BL_ID ?>">
                <div class="form-inside-div add-menu-item-new">
                    <label>Name</label>
                    <div class="form-data add-menu-item-field menu-input-width ">
                        <input name="title" type="text" id="title-<?php echo $data['BFM_ID'] ?>" value="<?php echo $data['BFM_Title'] ?>" size="25" required/>
                    </div>
                </div>
                <div class="form-inside-div add-menu-item-new">
                    <label>Description</label>
                    <div class="form-data add-menu-item-field wiziwig-menu">
                        <textarea name="descriptionNEW" cols="40" rows="7" wrap="VIRTUAL" class="formtext textarea-width-menu" id="description-<?php echo $data['BFM_ID'] ?>"><?php echo $data['BFM_Description'] ?></textarea>
                    </div>
                    <script>
                        $('.edit_items_<?php echo $data['BFM_ID'] ?>').click(function () {
                            $('#records_<?php echo $data['BFM_ID'] ?>').dialog('open');
                            $('#description-<?php echo $data['BFM_ID'] ?>').ckeditor();
                        });
                        $(function () {
                            $('#records_<?php echo $data['BFM_ID'] ?>').attr("title", "Edit Item").dialog({
                                autoOpen: false,
                            });
                        });
                    </script>
                </div>
                <div class="add-menu-item-container">
                    <div class="form-inside-div add-menu-item-new">
                        <label>Price<span>$</span></label>
                        <div class="form-data add-menu-item-field menu-input-width">
                            <input name="priceNEW" type="text" id="price-<?php echo $data['BFM_ID'] ?>" value="<?php echo $data['BFM_Price'] ?>" size="5" />
                        </div>
                    </div>
                </div>

                <div class="form-inside-div add-menu-item-button">
                    <div class="button product-butn-replace">
                        <input  type="submit" name="button33" class="margin-right-btn button<?php echo $data['BFM_ID'] ?>" value="Save Item" />
                        <input class="del_item" type="button" onclick="deleteitem(<?php echo $data['BFM_ID'] ?>, <?php echo $acco; ?>)" value="Delete Item"/>
                    </div>
                </div>
            </form>
            <script>
                function deleteitem(BFM_item_ID, ACTIVE_ACC) {
                    var meunid = <?php echo $BL_ID ?>;
                    var menutypeid = $('#menu-type-id-' + meunid).val();
                    if (confirm("Are you sure you want to delete item?")) {
                        $.post("customer-feature-menu-delete.php", {
                            BFM_ID_item: BFM_item_ID,
                            menutypeid: menutypeid,
                            BL_ID: meunid,
                            ACC: ACTIVE_ACC
                        }, function (result) {
                            $(location).attr('href', '/admin/listings-preview.php?bl_id=<?php echo $BL_ID ?>');

                        });
                    } else {
                        return false;
                    }
                }
            </script>
            <?PHP
            $counter++;
        }
        ?>

        <?PHP
    }
    ?>


</div>
<!--END-->

<!--ABOUT US-->
<?php
$sql_abutus = "SELECT BFAMP_Photo_545X335, BFAMP_Photo from tbl_Business_Feature_About_Main_Photo where BFAMP_BL_ID= '$BL_ID'";
$result_abutus = mysql_query($sql_abutus, $db) or die("Invalid query: $sql_abutus -- " . mysql_error());
$row_aboutus = mysql_fetch_assoc($result_abutus);
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_ID, BL_Listing_Title FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
}
$sqlAbt = "SELECT * FROM tbl_Business_Feature_About WHERE BFA_BL_ID = '$BL_ID' ORDER BY BFA_Order, BFA_ID";
$resultAbt1 = mysql_query($sqlAbt) or die(mysql_error());
$numRowsAbt = mysql_num_rows($resultAbt1);
?>
<div class="right" style="display:none">
    <input type="hidden" id="bidForProduct" value="<?php echo $BL_ID ?>">


    <form action="" method="POST" id="add_page_form" style="display: none">
        <div class="content-header">
            <div class="title">About Us</div>
            <div class="link">
                <?php
                $aboutUs = show_addon_points(1);
                if ($numRowsAbt > 0) {
                    echo '<div class="points-com">' . $aboutUs . ' pts</div>';
                } else {
                    echo '<div class="points-uncom">' . $aboutUs . ' pts</div>';
                }
                ?>
            </div>
        </div>
        <div class="form-inside-div border-none">
            <?php
            $help_text = show_help_text('About Us');
            if ($help_text != '') {
                echo '<div class="form-inside-div border-none padding-none margin-none">' . $help_text . '</div>';
            }
            ?>
        </div>
        <div class="addParentCategory" id="on-click-toggle-section" >
            <div class="form-inside-div add-product-section">
                <!-- ADDED NEW HIDDEN VALUE TO TARGET THE PARTICULAR SAVE OPTION -->
                <input type="hidden" name="InsertTrue" value="InsertValue">
                <input type="hidden" name="BL_ID" value="<?php echo $BL_ID ?>">
                <div class="form-data add-menu-item-field about-us-text-fields">
                    <label>Name</label>
                    <input id="titleCategory" name="titleCategory" type="text" required="required">
                </div>
                <div class="form-data about-us-text-fields wiziwig-about">
                    <label>Description</label>
                    <textarea id="titleCategorytextarea" class="description"  name="titleCategorytextarea"></textarea>
                </div>
                <div class="form-inside-div border-none">
                    <div class="button">
                        <input type="submit" value="Save">
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div id="main_image_form">
        <div class="content-header">
            <div class="title">About Us</div>
            <div class="link">
                <?php
                $aboutUs = show_addon_points(1);
                if ($numRowsAbt > 0) {
                    echo '<div class="points-com">' . $aboutUs . ' pts</div>';
                } else {
                    echo '<div class="points-uncom">' . $aboutUs . ' pts</div>';
                }
                ?>
            </div>
        </div>
        <div class="form-inside-div border-none">
            <?php
            $help_text = show_help_text('About Us');
            if ($help_text != '') {
                echo '<div class="form-inside-div border-none padding-none margin-none">' . $help_text . '</div>';
            }
            ?>
        </div>
        <div class="form-inside-div" style="border:none;">
            <div class="content-sub-header">
                <div class="title">Main Image</div>
                <div class="link">
                </div>
            </div>
            <div class="cropped-container1">
                <div class="image-editor1">
                    <div class="cropit-image-preview-container my-container-left">
                        <div class="cropit-image-preview main-preview">
                            <img class="preview-img preview-img-script202" style="display: none;" src="">    
                            <?php if ($row_aboutus['BFAMP_Photo_545X335'] != "") { ?>
                                <img class="existing_imgs202" src="http://<?php echo DOMAIN . IMG_LOC_REL . $row_aboutus['BFAMP_Photo_545X335'] ?>">
                            <?php } else if ($row_aboutus['BFAMP_Photo'] != "") { ?>
                                <img class="existing_imgs202" src="http://<?php echo DOMAIN . IMG_LOC_REL . $row_aboutus['BFAMP_Photo'] ?>">
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form name="form1" class="form-class-img-preview" method="post" onSubmit="return check_img_size(202, 10000000)"  enctype="multipart/form-data" action="">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="op" value="save_about_image">
            <input type="hidden" name="main_image" value="1">
            <input type="hidden" name="image_bank" class="image_bank" id="image_bank202" value="">
            <div class="form-inside-div margin-none border-none">
                <div class="div_image_library">
                    <span class="daily_browse"  onclick="show_image_library(202)">Select File</span>
                    <input type="file" name="pic[]" onchange="show_file_name_pc(202, this, 0)" id="photo202" style="display: none;">
                    <?php if ($row_aboutus['BFAMP_Photo_545X335'] != "") { ?><div class="data-column delete-main-photo1 padding-none"><a class="deletePhoto margin-left-main" onClick="return confirm('Are you sure?');" href="listings-preview.php?bl_id=<?php echo $BL_ID ?>&op=del_about_main_photo">Delete Photo</a></div><?php } ?>
                </div>
            </div>

            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" name="button" id="button" value="Save Image" />
                </div>
            </div>
        </form>
        <?php
        $sql1 = "SELECT BLF_BL_ID, BLF_ID, BLF_Active FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = 1";
        $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
        $row1 = mysql_fetch_assoc($result1);
        if ($row1['BLF_BL_ID'] != '') {
            ?>
            <div class="form-inside-div border-none text-align-center">
                <a href="listings-preview.php?addon_id=<?php echo $row1['BLF_ID'] ?>&bl_id=<?php echo $BL_ID ?>" onclick="return confirm('Are you sure you want to <?php echo $row1['BLF_Active'] == 1 ? "deactivate" : "activate" ?> this Add on?');"><?php echo $row1['BLF_Active'] == 1 ? "Deactivate" : "Activate" ?> This Add on</a>
            </div>
            <?PHP
        }
        ?>
    </div>
    <?PHP
    $acco = 0;
    while ($rowProductResult = mysql_fetch_array($resultAbt1)) {
        $description = $rowProductResult['BFA_Title'];
        if (strlen($description) > 45) {
            $description = substr($description, 0, 45) . '...';
        }
        ?>
        <div class="group" id="update_about_page_<?php echo $rowProductResult['BFA_ID']; ?>">
            <h3 class="accordion-rows about-us-accordion-h3 sorting-position"><span class="accordion-title"><?php echo $description; ?></span><label class="add-item-accordion"><a onclick="delete_about_us('<?php echo $rowProductResult['BFA_ID']; ?>')">Delete Page</a></label></h3> 
            <div class="sub-accordions accordion-padding">
                <form method="POST" action="" id="">
                    <div class="addParentCategory">
                        <input type="hidden" name="updateTrue" value="UpdateValue">
                        <input type="hidden" name="BFA_ID_UPDATE" value="<?php echo $rowProductResult['BFA_ID']; ?>">
                        <input type="hidden" name="BL_ID" value="<?php echo $BL_ID ?>">
                        <input type="hidden" name="acc" value="<?php echo $acco; ?>">
                        <div class="form-inside-div add-product-section add-product-section-in-accordion">
                            <div class="form-data add-menu-item-field about-us-text-fields">
                                <label>Name</label>
                                <input name="titleCategoryAccordion" id="titleCategoryAccordion_<?php echo $rowProductResult['BFA_ID']; ?>" type="text" value="<?php echo $rowProductResult['BFA_Title']; ?>" required="required">
                            </div>
                            <div class="form-data about-us-text-fields wiziwig-about">
                                <label>Description</label>
                                <textarea name="titleCategorytextareaAccordion" class="DescriptionHeight description" id="titleCategorytextareaAccordion_<?php echo $rowProductResult['BFA_ID']; ?>"><?php echo $rowProductResult['BFA_Description']; ?></textarea>
                            </div>
                            <script>
                                $('#edit_about_us_page_form_<?php echo $rowProductResult['BFA_ID']; ?>').click(function () {
                                    $('#update_about_page_<?php echo $rowProductResult['BFA_ID']; ?>').dialog('open');
                                    $('#titleCategorytextareaAccordion_<?php echo $rowProductResult['BFA_ID']; ?>').ckeditor();
                                });
                                $(function () {
                                    $('#update_about_page_<?php echo $rowProductResult['BFA_ID']; ?>').attr("title", "Edit Page").dialog({
                                        autoOpen: false,
                                    });
                                });
                            </script>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div1 border-none margin-top-add-photo">
                        <div class="button text-align-right">
                            <input type="submit" class="margin-btn-right1"  value="Save & Close"> 
                        </div>
                    </div>
                </form>
                <div class="form-inside-div form-inside-div2 border-none margin-top-add-photo">
                    <div class="button text-align-right">
                        <input type="submit" id="addProductSectionPhoto<?php echo $rowProductResult['BFA_ID']; ?>" value="+ Add Photo">
                    </div>
                </div>
                <div class="addParentCategory" id="on-click-toggle-sectionPhoto<?php echo $rowProductResult['BFA_ID']; ?>" style="display: none">
                    <form name="form1" method="post" onSubmit="return check_img_size(<?php echo $rowProductResult['BFA_ID']; ?>, 10000000)" enctype="multipart/form-data" style="float:left;width:100%;margin-bottom: 10px;">
                        <input type="hidden" name="id" class="blidforabout" value="<?php echo $BL_ID ?>">
                        <input type="hidden" name="aid" value="<?php echo $rowProductResult['BFA_ID']; ?>">
                        <input type="hidden" name="op" value="save_about_image">
                        <input type="hidden" name="acc" value="<?php echo $acco; ?>">
                        <input type="hidden" name="image_bank" class="image_bank" id="image_bank<?php echo $rowProductResult['BFA_ID'] ?>" value="">
                        <div class="form-inside-div">
                            <label>Title</label>
                            <div class="form-data">
                                <input name="title" type="text" id="title<?php echo $rowProductResult['BFA_ID']; ?>" size="50"/>
                            </div>
                        </div>
                        <div class="form-inside-div">
                            <label>Photo</label>
                            <div class="margin-none form-inside-div div_image_library about-us-cropit border-none">
                                <span class="daily_browse" onclick="show_image_library(3, <?php echo $rowProductResult['BFA_ID']; ?>)">Select File</span>
                                <input type="file" style="display :none;" name="pic[]" onChange="show_file_name_pc(3, this, <?php echo $rowProductResult['BFA_ID']; ?>);" id="photo<?php echo $rowProductResult['BFA_ID']; ?>">
                                <div class="form-inside-div add-about-us-item-image margin-none">
                                    <div class="cropit-image-preview aboutus-photo-perview">
                                        <img class="preview-img preview-img-script<?php echo $rowProductResult['BFA_ID']; ?>" style="display: none;" src="">    
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-inside-div">
                            <div class="button">
                                <input type="submit" name="button" id="button<?php echo $rowProductResult['BFA_ID']; ?>" value="Save Photo" />
                            </div>
                        </div>

                        <div class="form-inside-div border-none">
                            <div class="form-data">
                                Photo file limit size is 3MBs. If you're files are large they may take 10 seconds to load once you click "Save Photo".
                            </div>
                        </div>
                    </form>
                </div>
                <script>
                    CKEDITOR.disableAutoInline = true;
                    $(document).ready(function () {
                        var wzwig_bfa_ID = <?php echo $rowProductResult['BFA_ID']; ?>;
                        $('#titleCategorytextareaAccordion_' + wzwig_bfa_ID).ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
                    });
                    $(document).ready(function () {
                        var BFA_ID = '<?php echo $rowProductResult['BFA_ID']; ?>';
                        $("#addProductSectionPhoto" + BFA_ID).click(function () {
                            $("#on-click-toggle-sectionPhoto" + BFA_ID).toggle();
                        });
                    });
                </script>
                <?php
                $sqlPhoto = "SELECT BFAP_ID, BFAP_Photo_285X210, BFAP_Photo, BFAP_Title FROM tbl_Business_Feature_About_Photo WHERE BFAP_BFA_ID = '" . encode_strings($rowProductResult['BFA_ID'], $db) . "' ORDER BY BFAP_Order";
                $sqlPhotoResult = mysql_query($sqlPhoto) or die(mysql_error());
                $counter = 1;
                ?>
                <div class="form-inside-div border-none margin-none" id="listing-gallery-about-us">
                    <ul class="gallery">
                        <?php
                        while ($sqlPhotoResultData = mysql_fetch_array($sqlPhotoResult)) {
                            if (mysql_num_rows($sqlPhotoResult) > 0) {
                                ?>
                                <li class="image" id="recordsArray_<?php echo $sqlPhotoResultData['BFAP_ID'] ?>">
                                    <div class="listing-image">
                                        <?php if ($sqlPhotoResultData['BFAP_Photo_285X210'] != "") { ?>
                                            <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $sqlPhotoResultData['BFAP_Photo_285X210'] ?>" alt="" width="285" height="210"/>
                                        <?php } else { ?>
                                            <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $sqlPhotoResultData['BFAP_Photo'] ?>" alt=""/>
                                        <?php } ?>
                                    </div>
                                    <div class="count"><?php echo $counter ?></div>
                                    <div class="cross"><a onClick="return confirm('Are you sure?');" href="listings-preview.php?op=del_about_photo&amp;id=<?php echo $sqlPhotoResultData['BFAP_ID'] ?>&bl_id=<?php echo $BL_ID ?>&acc=<?php echo $acco; ?>">x</a></div>
                                    <div class="desc" onclick="show_description_about_us(<?php echo $sqlPhotoResultData['BFAP_ID'] ?>);"><?php echo ($sqlPhotoResultData['BFAP_Title'] != '') ? $sqlPhotoResultData['BFAP_Title'] : '+Add Description'; ?></div> 
                                </li>
                                <form action="" method="post" name="form" id="gallery-description-form-<?php echo $sqlPhotoResultData['BFAP_ID'] ?>" style="display:none;">
                                    <div style="float:left; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                        <input type="hidden" name="op" value="edit_about_us_photo">
                                        <input type="hidden" name="acc" value="<?php echo $acco; ?>">
                                        <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
                                        <input type="hidden" name="bfap_id" value="<?php echo $sqlPhotoResultData['BFAP_ID'] ?>">

                                        <input type="text" value="<?php echo $sqlPhotoResultData['BFAP_Title'] ?>" name="titleName<?php echo $sqlPhotoResultData['BFAP_ID'] ?>" style="width:420px !important;padding: 6px 10px !important;">

                                    </div>
                                    <div style="float:left;text-align: center; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                        <div class="form-data">
                                            <input type="submit" name="button" value="Save Now"/>
                                        </div>
                                    </div> 
                                </form>
                                <?php
                            }
                            $counter++;
                        }
                        ?>
                    </ul>
                </div>

            </div>    
        </div>    
        <?PHP
        $acco++;
    }
    ?>
</div>
<div id="about_us_ordering" class="menu-items-accodings product-accordion" style="display:none">                         
    <div id="accordion">
        <?PHP
        $sqlAbt = "SELECT * FROM tbl_Business_Feature_About WHERE BFA_BL_ID = '$BL_ID' ORDER BY BFA_Order, BFA_ID";
        $resultAbt = mysql_query($sqlAbt) or die(mysql_error());
        $numRowsAbt = mysql_num_rows($resultAbt);
        $acco = 0;
        while ($rowProductResult = mysql_fetch_array($resultAbt)) {
            $description = $rowProductResult['BFA_Title'];
            if (strlen($description) > 45) {
                $description = substr($description, 0, 45) . '...';
            }
            ?>
            <div class="group" id="recordsArray_<?php echo $rowProductResult['BFA_ID']; ?>">
                <h3 class="accordion-rows about-us-accordion-h3 sorting-position"><span class="accordion-title"><?php echo $description; ?></span></h3> 

            </div>    
            <?PHP
            $acco++;
        }
        ?>
    </div> 
</div>
<script>

    $(function () {
        $("#accordion")
                .sortable({
                    axis: "y",
                    handle: "h3",
                    update: function () {
                        var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Business_Feature_About&field=BFA_Order&id=BFA_ID';
                        $.post("reorder.php", order, function (theResponse) {
                            swal("success", "Re-order scuccessfully", "success");
                        });
                    }
                });
    });

    $(function () {
        $("#listing-gallery-about-us ul.gallery").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Business_Feature_About_Photo&field=BFAP_Order&id=BFAP_ID';
                $.post("reorder.php", order, function (theResponse) {
                    swal("success", "Re-order scuccessfully", "success");
                });
            }
        });
    });
</script>
<!--END-->

<!--DAILY FEATURES-->
<?php
$sql = "SELECT * FROM tbl_Feature WHERE F_ID NOT IN (3)";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
while ($row = mysql_fetch_assoc($result)) {
    //Daily Specials
    if ($row['F_Name'] == 'Daily Features') {
        $sqlDS = "SELECT * FROM tbl_Business_Feature_Daily_Specials WHERE BFDS_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $resultDS = mysql_query($sqlDS, $db) or die("Invalid query: $sqlDS -- " . mysql_error());
        $numRowsDS = mysql_num_rows($resultDS);
        if ($numRowsDS > 0 && $row1['BLF_Active'] == "1") {
            $class = 'points-com';
        } else {
            $class = 'points-uncom';
        }
    }
}
$sql_description_daily = mysql_query("SELECT BFDSD_ID,BFDSD_Description from tbl_Business_Feature_Daily_Specials_Description WHERE BFDSD_BL_ID = '$BL_ID'");
$data_description = mysql_fetch_assoc($sql_description_daily);
?>
<div style="display:none">
    <form name="form1" method="post" id="add_daily_desc"  action="">
        <div class="content-header">
            <div class="title">Daily Features</div>
            <div class="link">
                <?php
                $DS = show_addon_points(7);
                if ($numRowsDS > 0) {
                    echo '<div class="points-com">' . $DS . ' pts</div>';
                } else {
                    echo '<div class="points-uncom">' . $DS . ' pts</div>';
                }
                ?>
            </div>
        </div>

        <?php
        $help_text = show_help_text('Daily Specials');
        if ($help_text != '') {
            echo '<div class="form-inside-div" style="border-bottom:3px solid #eeeeee;">' . $help_text . '</div>';
        }
        ?>
        <div class="daily-special-specifivation">
            <div class="form-inside-div add-menu-item Description-width">
                <input type="hidden" name="bl_id" id="daily_bl_id" value="<?php echo $BL_ID ?>"> 
                <label>Description</label>
                <div class="form-data add-menu-item-field wiziwig-about">
                    <textarea name="daily-special-description" class="description-textarea description-ckeditor" id="daily-description"  cols="60" rows="3" ><?php echo $data_description['BFDSD_Description'] ?></textarea>
                </div>
                <script>
                    $('#add_daily_desc_click').click(function () {
                        $('#add_daily_desc').dialog('open');
                        $('#daily-description').ckeditor();
                    });
                    $(function () {
                        $('#add_daily_desc').attr("title", "Add Description").dialog({
                            autoOpen: false,
                        });
                    });
                </script>
            </div>
            <div class="form-inside-div add-menu-item-button">
                <div class="button">
                    <input type="submit" name="button_description" value="Save" />
                </div>      
            </div> 
        </div>   
        <?PHP
        $sql1 = "SELECT BLF_BL_ID, BLF_ID, BLF_Active FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = 7";
        $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
        $row1 = mysql_fetch_assoc($result1);
        if ($row1['BLF_BL_ID'] != '') {
            ?>
            <div class="form-inside-div border-none text-align-center">
                <a href="listings-preview.php?addon_id=<?php echo $row1['BLF_ID'] ?>&bl_id=<?php echo $BL_ID ?>"  onclick="return confirm('Are you sure you want to <?php echo $row1['BLF_Active'] == 1 ? "deactivate" : "activate" ?> this Add on?');"><?php echo $row1['BLF_Active'] == 1 ? "Deactivate" : "Activate" ?> This Add on</a>
            </div>
            <?PHP
        }
        ?>
    </form>         
    <?PHP
    foreach ($days as $key => $val) {
        $sql = "SELECT BFDS_BL_ID, BFDS_Title, BFDS_Description, BFDS_Photo FROM tbl_Business_Feature_Daily_Specials 
                    WHERE BFDS_BL_ID = '$BL_ID' AND BFDS_Day = '" . encode_strings($key, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db)
                or die("Invalid query: $sql -- " . mysql_error());
        $data = mysql_fetch_assoc($result);
        ?>
        <form <div style="display:none" name="menu-item" id="update_daily_<?php echo $key ?>" method="post" onSubmit="return check_img_size(<?php echo $key ?>, 10000000)"  enctype="multipart/form-data" action="">
                <input type="hidden" name="bl_id" value="<?php echo ($data['BFDS_BL_ID'] != "") ? $data['BFDS_BL_ID'] : $BL_ID ?>">
                <input type="hidden" name="weekno" value="<?php echo $key ?>">
                <input type="hidden" name="op" value="save_daily_special">
                <input type="hidden" id="update_<?php echo $key ?>" value="1">
                <input type="hidden" name="image_bank" class="image_bank" id="image_bank<?php echo $key ?>" value="">
                <div class="data-content daily-special-items">
                    <div class="form-inside-div add-menu-item"><label class="dayname-label"><?php echo $val ?></label></div>
                    <div class="add-menu-item-container">
                        <div class="form-inside-div add-menu-item">
                            <label>Name</label>    
                            <div class="form-data add-menu-item-field daily-special-width"> 
                                <input type="text" name="title" id="daily-title<?php echo $key ?>" value="<?php echo $data['BFDS_Title'] ?>" required/>
                            </div>
                        </div>

                        <div class="form-inside-div add-menu-item">
                            <label>Description</label>
                            <div class="form-data add-menu-item-field wizwig_editor_product_items">
                                <textarea name="description" class="description-ckeditor"  id="daily-description<?php echo $key ?>"  cols="40" rows="8" ><?php echo $data['BFDS_Description'] ?></textarea>
                            </div>
                            <script>
                                $('#add_daily_click_<?php echo $key ?>').click(function () {
                                    $('#update_daily_<?php echo $key ?>').dialog('open');
                                    $('#daily-description<?php echo $key ?>').ckeditor();
                                });
                                $(function () {
                                    $('#update_daily_<?php echo $key ?>').attr("title", "Add Detail").dialog({
                                        autoOpen: false,
                                    });
                                });
                            </script>
                        </div>

                    </div> 
                    <div class="form-inside-div div_image_library add-menu-item-image daily-feature-cropit">
                        <span class="daily_browse" onclick="show_image_library(19,<?php echo $key ?>)">Select File</span>
                        <input type="file" name="pic[]" onchange="show_file_name_pc(19, this, <?php echo $key ?>)" id="photo<?php echo $key ?>" style="display: none;">
                        <div class="cropit-image-preview aboutus-photo-perview">
                            <img class="preview-img preview-img-script<?php echo $key ?> max-width-thumbnail" style="display: none;" src="">    
                            <?php if ($data['BFDS_Photo'] != "") { ?>
                                <img class="existing-img existing_imgs<?php echo $key ?> max-width-thumbnail" src="http://<?php echo DOMAIN . IMG_LOC_REL . $data['BFDS_Photo'] ?>">
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div add-menu-item-image">
                        <?php if ($data['BFDS_Photo'] != "") { ?>
                            <div class="data-column daily-del">
                                <a class="deletePhoto" href="listings-preview.php?bl_id=<?php echo $BL_ID ?>&bds_id_photo=<?php echo $key ?>&op=del_photo" onClick="return confirm('Are you sure you want to delete photo?');">Delete Photo</a>
                            </div>
                        <?php } ?>
                        <div class="data-column delete-orange"></div>
                    </div>
                    <div class="form-inside-div border-none button-spaces">
                        <div class="button">
                            <input type="submit" name="button3" class="button<?php echo $key ?>" value="Save Daily Feature" />
                            <a class="btn-anchor" href="listings-preview.php?bl_id=<?php echo $BL_ID ?>&bds_item_id=<?php echo $key ?>&op=del_daily_item" onClick="return confirm('Are you sure you want to delete it?');">Delete Daily Feature</a>
                        </div>      
                    </div> 
                </div>
        </form>
        <?PHP
    }
    ?>
    <!--    <div id="image-library" style="display:none;"></div>
        <input id="image-library-usage" type="hidden" value="">-->
</div>
<!--END-->

<!--ENTERTAINMENT-->
<?php
$sql = "SELECT * FROM tbl_Feature WHERE F_ID NOT IN (3)";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
while ($row = mysql_fetch_assoc($result)) {
    //Entertainment
    if ($row['F_Name'] == 'Entertainment') {
        $sqlEnt = "SELECT * FROM tbl_Business_Feature_Entertainment_Acts WHERE BFEA_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $resultEnt = mysql_query($sqlEnt) or die(mysql_error());
        $numRowsEnt = mysql_num_rows($resultEnt);
        if ($numRowsEnt > 0 && $row1['BLF_Active'] == "1") {
            $class = 'points-com';
        } else {
            $class = 'points-uncom';
        }
    }
}
$sql_description_daily = mysql_query("SELECT BFED_Description FROM tbl_Business_Feature_Entertainment_Description WHERE BFED_BL_ID= '$BL_ID'");
$data_description = mysql_fetch_assoc($sql_description_daily);
?>
<div class="right" style="display:none">
    <form name="entertainment-des" id="add_enter_desc" method="post"  action="">
        <div class="content-header">
            <div class="title">Entertainment</div>
            <div class="link">
                <?php
                $Ent = show_addon_points(8);
                if ($numRowsEnt > 0) {
                    echo '<div class="points"><div class="points-com">' . $Ent . ' pts</div></div>';
                } else {
                    echo '<div class="points"><div class="points-uncom">' . $Ent . ' pts</div></div>';
                }
                ?>
            </div>
        </div>
        <?php
        $help_text = show_help_text('Entertainment');
        if ($help_text != '') {
            echo '<div class="form-inside-div">' . $help_text . '</div>';
        }
        ?>
        <div class="daily-special-specifivation ent-des-width">
            <div class="form-inside-div add-menu-item Description-width">
                <input type="hidden" name="ent_des_bl_id" id="daily_bl_id" value="<?php echo $BL_ID ?>"> 
                <label>Description</label>
                <div class="form-data add-menu-item-field wiziwig-about">
                    <textarea name="entertainment-description" class="Description-ent-textarea description-ckeditor" id="daily-description-enter"  cols="60" rows="3" ><?php echo $data_description['BFED_Description'] ?></textarea>
                </div>
                <script>
                    $('#add_enter_desc_click').click(function () {
                        $('#add_enter_desc').dialog('open');
                        $('#daily-description-enter').ckeditor();
                    });
                    $(function () {
                        $('#add_enter_desc').attr("title", "Add Description").dialog({
                            autoOpen: false,
                        });
                    });
                </script>
            </div>
            <div class="form-inside-div add-menu-item-button">
                <div class="button">
                    <input type="submit" name="entertainment-description-btn" value="Save" />
                </div>      
            </div> 
        </div> 
        <?php
        $sql1 = "SELECT BLF_BL_ID, BLF_ID, BLF_Active FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = 8";
        $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
        $row1 = mysql_fetch_assoc($result1);
        if ($row1['BLF_BL_ID'] != '') {
            ?>
            <div class="form-inside-div border-none text-align-center">
                <a href="listings-preview.php?addon_id=<?php echo $row1['BLF_ID'] ?>&bl_id=<?php echo $BL_ID ?>" onclick="return confirm('Are you sure you want to <?php echo $row1['BLF_Active'] == 1 ? "deactivate" : "activate" ?> this Add on?');"><?php echo $row1['BLF_Active'] == 1 ? "Deactivate" : "Activate" ?> This Add on</a>
            </div>
            <?PHP
        }
        ?>
    </form>  
    <form name="menu-item" onSubmit="return check_img_size(<?php echo $BL_ID ?>, 10000000)" enctype="multipart/form-data" method="post" action="" id="entertainment-form" style="display: none; float:left;overflow: hidden;">
        <div class="form-inside-div add-product-section add-product-section-in-accordion">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="op" value="save_entertainment">
            <input type="hidden" id="update_<?php echo $BL_ID ?>" value="1">
            <input type="hidden" name="image_bank" class="image_bank" id="image_bank<?php echo $BL_ID ?>" value="">
            <div class="add-menu-item-container margin-right-entr">
                <div class="form-inside-div add-menu-item">
                    <label class="entertment-margin padding-entr">Name</label>
                    <div class="form-data add-menu-item-field daily-special-width">
                        <input name="title" type="text" id="title" class="title-enter-width" size="50" value="" required/>
                    </div>
                </div>
                <div class="form-inside-div add-menu-item">
                    <label class="entertment-margin">Description</label>
                    <div class="form-data add-menu-item-field wizwig_editor_product_items">
                        <textarea name="description" class="description-ckeditor" cols="40" rows="7" id="title-entertainment"></textarea>
                    </div>
                    <script>
                        $('#add_enter_click').click(function () {
                            $('#entertainment-form').dialog('open');
                            $('#title-entertainment').ckeditor();
                        });
                        $(function () {
                            $('#entertainment-form').attr("title", "Add Event").dialog({
                                autoOpen: false,
                            });
                        });
                    </script>
                </div>
            </div>
            <div class="form-inside-div div_image_library add-menu-item-image product-cropit adjust-entert-width">
                <span class="daily_browse" onclick="show_image_library(20, <?php echo $BL_ID ?>)">Select File</span>
                <input type="file" name="pic[]" onchange="show_file_name_pc(20, this, <?php echo $BL_ID ?>)" id="photo<?php echo $BL_ID ?>" style="display: none;">
                <div class="form-inside-div add-about-us-item-image margin-none">
                    <div class="cropit-image-preview aboutus-photo-perview">
                        <img class="preview-img preview-img-script<?php echo $BL_ID ?>" style="display: none;" src="">    
                    </div>
                </div>
            </div>
            <div class="form-inside-div add-entr-item">
                <label class="entertment-margin padding-entr">Start time</label>
                <div class="form-data add-menu-item-field margin-about-entertainment">
                    <select name="hour" id="hour" class="ententment-select" required>
                        <option value="">Select Time</option>
                        <?PHP
                        for ($i = 1; $i <= 24; $i++) {
                            $j = 0;
                            if ($i == 24) {
                                $j = 1;
                                $i = "00";
                            }
                            ?>
                            <option value="<?php echo $i ?>:00:00">
                                <?php echo $i > 12 ? $i - 12 : $i ?>
                                :00 
                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                            </option>
                            <option value="<?php echo $i ?>:15:00">
                                <?php echo $i > 12 ? $i - 12 : $i ?>
                                :15 
                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                            </option>
                            <option value="<?php echo $i ?>:30:00">
                                <?php echo $i > 12 ? $i - 12 : $i ?>
                                :30 
                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                            </option>
                            <option value="<?php echo $i ?>:45:00">
                                <?php echo $i > 12 ? $i - 12 : $i ?>
                                :45 
                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                            </option>
                            <?PHP
                            if ($j == 1) {
                                $i = 24;
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-inside-div add-entr-item">
                <label class="entertment-margin padding-entr">End time</label>
                <div class="form-data add-menu-item-field margin-about-entertainment">
                    <select name="end_hour" id="end_hour" class="ententment-select" required>
                        <option value="">Select Hour</option>
                        <?PHP
                        for ($i = 1; $i <= 24; $i++) {
                            $j = 0;
                            if ($i == 24) {
                                $j = 1;
                                $i = "00";
                            }
                            ?>
                            <option value="<?php echo $i ?>:00:00">
                                <?php echo $i > 12 ? $i - 12 : $i ?>
                                :00 
                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                            </option>
                            <option value="<?php echo $i ?>:15:00">
                                <?php echo $i > 12 ? $i - 12 : $i ?>
                                :15 
                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                            </option>
                            <option value="<?php echo $i ?>:30:00">
                                <?php echo $i > 12 ? $i - 12 : $i ?>
                                :30 
                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                            </option>
                            <option value="<?php echo $i ?>:45:00">
                                <?php echo $i > 12 ? $i - 12 : $i ?>
                                :45 
                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                            </option>
                            <?PHP
                            if ($j == 1) {
                                $i = 24;
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-data add-menu-item-field margin-about-entertainment">
                <label class="entertment-margin padding-entr">Date</label>
                <input class="previous-date-not-allowed date-textbox-width" type="text" name="eventdate" id="event-date" value="<?php echo (isset($data['myDate']) ? $data['myDate'] : '');
                        ?>"/>
            </div>
            <div class="form-inside-div add-menu-item-button">
                <div class="button entr-butn-new">
                    <input type="submit" name="button-add-event" class="button-add-event" value="Save Event" />
                </div>   
            </div>   
        </div>   
    </form>
    <?PHP
    $sql = "SELECT * , DATE_FORMAT( BFEA_Time, '%Y-%m-%d' ) AS myDate, LOWER(DATE_FORMAT( BFEA_Time, '%T')) AS myHour, LOWER(DATE_FORMAT( BFEA_End_Time, '%T')) AS endHour FROM tbl_Business_Feature_Entertainment_Acts WHERE BFEA_BL_ID = '$BL_ID' 
                        ORDER BY BFEA_Time";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $acco = 0;
    while ($data = mysql_fetch_assoc($result)) {
        $originalDate = $data['myDate'];
        ?>
        <form name="menu-item" onSubmit="return check_img_size(<?php echo $data['BFEA_ID'] ?>, 10000000)" enctype="multipart/form-data" method="post" action="" id="entertainment-form<?php echo $data['BFEA_ID'] ?>" style="float:left;">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="op" value="edit_entertainment">
            <input type="hidden" name="acc" value="<?php echo $acco ?>">
            <input type="hidden" name="BFEA_ID" value="<?php echo $data['BFEA_ID'] ?>">
            <input type="hidden" id="update_<?php echo $data['BFEA_ID'] ?>" value="1">
            <input type="hidden" name="image_bank" class="image_bank" id="image_bank<?php echo $data['BFEA_ID'] ?>" value="">
            <div class="add-menu-item-container">
                <div class="form-inside-div add-menu-item">
                    <label class="entertment-margin ">Name</label>
                    <div class="form-data add-menu-item-field daily-special-width">
                        <input name="title" type="text" id="title_<?php echo $data['BFEA_ID']; ?>" size="50" value="<?php echo $data['BFEA_Name'] ?>" required/>
                    </div>
                </div>
                <div class="form-inside-div add-menu-item">
                    <label class="entertment-margin">Description</label>
                    <div class="form-data add-menu-item-field wizwig_editor_product_items">
                        <textarea name="description" class="description-ckeditor" cols="40" rows="7" id="title-entertainment<?php echo $data['BFEA_ID'] ?>"><?php echo $data['BFEA_Description'] ?></textarea>
                    </div>
                    <script>
                        $('#add_enter_click<?php echo $data['BFEA_ID']; ?>').click(function () {
                            $('#entertainment-form<?php echo $data['BFEA_ID']; ?>').dialog('open');
                            $('#title-entertainment<?php echo $data['BFEA_ID']; ?>').ckeditor();
                        });
                        $(function () {
                            $('#entertainment-form<?php echo $data['BFEA_ID']; ?>').attr("title", "Add Event").dialog({
                                autoOpen: false,
                            });
                        });
                    </script>
                </div>
            </div>
            <div class="form-inside-div div_image_library add-menu-item-image product-cropit">
                <span class="daily_browse" onclick="show_image_library(20, <?php echo $data['BFEA_ID'] ?>)">Select File</span>
                <input type="file" name="pic[]" onchange="show_file_name_pc(20, this, <?php echo $data['BFEA_ID'] ?>)" id="photo<?php echo $data['BFEA_ID'] ?>" style="display: none;">
                <div class="cropit-image-preview aboutus-photo-perview">
                    <img class="preview-img preview-img-script<?php echo $data['BFEA_ID'] ?>" style="display: none;" src="">    
                    <?php if ($data['BFEA_Photo'] != "") { ?>
                        <img class="existing-img existing_imgs<?php echo $data['BFEA_ID'] ?> max-width-thumbnail" src="http://<?php echo DOMAIN . IMG_LOC_REL . $data['BFEA_Photo'] ?>">
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div add-menu-item-image">
                <div class="data-column daily-del"> 
                    <a class="deletePhoto" href="listings-preview.php?bl_id=<?php echo $BL_ID ?>&bfea_id=<?php echo $data['BFEA_ID'] ?>&op=del_event_photo&acc=<?php echo $acco; ?>" onClick="return confirm('Are you sure you want to delete photo?');">Delete Photo</a>
                </div>
            </div>
            <div class="form-inside-div add-entr-item">
                <label class="entertment-margin">Start time</label>
                <div class="form-data add-menu-item-field">
                    <select name="hour" id="hour<?php echo $data['BFEA_ID'] ?>" class="ententment-select" required>
                        <option value="">Select Hour</option>
                        <?PHP
                        for ($i = 1; $i <= 24; $i++) {
                            if ($i < 10) {
                                $i = "0" . $i;
                            }
                            $j = 0;
                            if ($i == 24) {
                                $j = 1;
                                $i = "00";
                            }
                            ?>
                            <option value="<?php echo $i ?>:00:00" <?php
                            if ($data['myHour'] == $i . ":00:00") {
                                echo "selected";
                            }
                            ?> >
                                        <?php echo $i > 12 ? $i - 12 : $i ?>
                                :00 
                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                            </option>
                            <option value="<?php echo $i ?>:15:00" <?php
                            if ($data['myHour'] == $i . ":15:00") {
                                echo "selected";
                            }
                            ?> >
                                        <?php echo $i > 12 ? $i - 12 : $i ?>
                                :15 
                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                            </option>
                            <option value="<?php echo $i ?>:30:00" <?php
                            if ($data['myHour'] == $i . ":30:00") {
                                echo "selected";
                            }
                            ?> >
                                        <?php echo $i > 12 ? $i - 12 : $i ?>
                                :30 
                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                            </option>
                            <option value="<?php echo $i ?>:45:00" <?php
                            if ($data['myHour'] == $i . ":45:00") {
                                echo "selected";
                            }
                            ?>>
                                        <?php echo $i > 12 ? $i - 12 : $i ?>
                                :45 
                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                            </option>
                            <?PHP
                            if ($j == 1) {
                                $i = 24;
                            }
                        }
                        ?>

                    </select>
                </div>
            </div>
            <div class="form-inside-div add-entr-item">
                <label class="entertment-margin">End time</label>
                <div class="form-data add-menu-item-field">
                    <select name="end_hour" id="end_hour<?php echo $data['BFEA_ID'] ?>" class="ententment-select" required>
                        <option value="">Select Hour</option>
                        <?PHP
                        for ($i = 1; $i <= 24; $i++) {
                            if ($i < 10) {
                                $i = "0" . $i;
                            }
                            $j = 0;
                            if ($i == 24) {
                                $j = 1;
                                $i = "00";
                            }
                            ?>
                            <option value="<?php echo $i ?>:00:00" <?php
                            if ($data['endHour'] == $i . ":00:00") {
                                echo "selected";
                            }
                            ?> >
                                        <?php echo $i > 12 ? $i - 12 : $i ?>
                                :00 
                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                            </option>
                            <option value="<?php echo $i ?>:15:00" <?php
                            if ($data['endHour'] == $i . ":15:00") {
                                echo "selected";
                            }
                            ?>>
                                        <?php echo $i > 12 ? $i - 12 : $i ?>
                                :15 
                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                            </option>
                            <option value="<?php echo $i ?>:30:00" <?php
                            if ($data['endHour'] == $i . ":30:00") {
                                echo "selected";
                            }
                            ?>>
                                        <?php echo $i > 12 ? $i - 12 : $i ?>
                                :30 
                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                            </option>
                            <option value="<?php echo $i ?>:45:00" <?php
                            if ($data['endHour'] == $i . ":45:00") {
                                echo "selected";
                            }
                            ?>>
                                        <?php echo $i > 12 ? $i - 12 : $i ?>
                                :45 
                                <?php echo $i > 11 ? 'pm' : 'am' ?>
                            </option>
                            <?PHP
                            if ($j == 1) {
                                $i = 24;
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-inside-div add-menu-item">
                <label class="entertment-margin">Date</label>
                <div class="form-data add-menu-item-field">
                    <input class="previous-date-not-allowed date-textbox-width" type="text" name="eventdate" id="eventdate<?php echo $data['BFEA_ID'] ?>" value="<?php echo $data['myDate'] ?>">
                </div>
            </div>
            <div class="form-inside-div border-none button-spaces">
                <div class="button">
                    <input type="submit" name="button" class="button<?php echo $data['BFEA_ID']; ?>" value="Save Event" />
                    <a class="btn-anchor" href="listings-preview.php?bl_id=<?php echo $BL_ID ?>&bfea_id=<?php echo $data['BFEA_ID'] ?>&op=del_event&acc=<?php echo $acco; ?>" onClick="return confirm('Are you sure you want to delete event?');">Delete Event</a>
                </div>
            </div>
        </form>
        <?php
        $acco++;
    }
    ?>
<!--<input id="image-library-usage" type="hidden" value="">-->
</div>
<!--END-->

<!--AGENDA AND MINUTES-->
<?php
$sql = "SELECT * FROM tbl_Feature WHERE F_ID NOT IN (3)";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
while ($row = mysql_fetch_assoc($result)) {
    //Agendas & Minutes
    if ($row['F_Name'] == 'Agendas & Minutes') {
        $sqlAM = "SELECT * FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $resultAM = mysql_query($sqlAM) or die(mysql_error());
        $numRowsAM = mysql_num_rows($resultAM);
        if ($numRowsAM > 0 && $row1['BLF_Active'] == "1") {
            $class = 'points-com';
        } else {
            $class = 'points-uncom';
        }
    }
}
$sql_agendas = "SELECT BFAMMP_Photo from tbl_Business_Feature_Agendas_Minutes_Main_Photo where BFAMMP_BL_ID= '$BL_ID'";
$result_agendas = mysql_query($sql_agendas, $db) or die("Invalid query: $sql_agendas -- " . mysql_error());
$row_agendas = mysql_fetch_assoc($result_agendas);
?>
<div style="display:none">
    <div id="agenda_main_image">
        <div class="content-header">
            <div class="title">Agendas & Minutes</div>
            <div class="link">
                <?php
                $AgendaMinute = show_addon_points(13);
                if ($numRowsAM > 0) {
                    echo '<div class="points-com">' . $AgendaMinute . ' pts</div>';
                    $points_taken = $AgendaMinute;
                } else {
                    echo '<div class="points-uncom">' . $AgendaMinute . ' pts</div>';
                }
                ?>
            </div>
        </div>
        <div class="form-inside-div" style="border:none;">
            <div class="content-sub-header">
                <div class="title">Main Image</div>
                <div class="link">

                </div>
            </div>
        </div>
        <div class="cropped-container1">
            <div class="image-editor1">
                <div class="cropit-image-preview-container my-container-left">
                    <div class="cropit-image-preview main-preview">
                        <img class="preview-img preview-img-script23" style="display: none;" src="">    
                        <?php if (isset($row_agendas['BFAMMP_Photo']) && $row_agendas['BFAMMP_Photo'] != '') { ?>
                            <img class="existing-img existing_imgs23" src="http://<?php echo DOMAIN . IMG_LOC_REL . $row_agendas['BFAMMP_Photo'] ?>">
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <form name="form1" method="post" onSubmit="return check_img_size(23, 10000000)"  enctype="multipart/form-data" action="">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="op" value="save_photo">
            <input type="hidden" name="image_bank" class="image_bank" id="image_bank23" value="">
            <div class="form-inside-div margin-none border-none">
                <div class="div_image_library">
                    <span class="daily_browse" onclick="show_image_library(23)">Select File</span>
                    <input type="file" name="pic[]" onchange="show_file_name_pc(23, this, 0)" id="photo23" style="display: none;">
                    <div class="data-column delete-main-photo1 padding-none">
                        <?php if ($row_agendas['BFAMMP_Photo'] != '') { ?>
                            <a class="deletePhoto margin-left-main" onClick="return confirm('Are you sure?');"  href="customer-feature-agenda-minutes.php?bl_id=<?php echo $BL_ID ?>&op=del_photo&flag=<?php echo $flag ?>">Delete Photo</a>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" name="button" id="button" value="Save Image" />
                </div>
            </div>
        </form>
        <?php
        $sql1 = "SELECT * FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = 13";
        $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
        $row1 = mysql_fetch_assoc($result1);
        if ($row1['BLF_BL_ID'] != '') {
            ?>
            <div class="form-inside-div border-none text-align-center">
                <a href="listings-preview.php?addon_id=<?php echo $row1['BLF_ID'] ?>&bl_id=<?php echo $BL_ID ?>" onclick="return confirm('Are you sure you want to <?php echo $row1['BLF_Active'] == 1 ? "deactivate" : "activate" ?> this Add on?');"><?php echo $row1['BLF_Active'] == 1 ? "Deactivate" : "Activate" ?> This Add on</a>
            </div>
            <?PHP
        }
        ?>
    </div>
    <div class="form-inside-div border-none">
        <?php
        $help_text = show_help_text('Agendas & Minutes');
        if ($help_text != '') {
            echo '<div class="form-inside-div padding-none margin-none">' . $help_text . '</div>';
        }
        ?>
        <script>

            $(function () {
                //var BFAM_ID = "<?php echo $data['BFAM_ID'] ?>"; 
                var searched_items = $('.keywords-searched').val();
                //var searched_items = "";
                if (searched_items != "") {
                    var myarray = searched_items.split(',');
                    var json = [];
                    for (var i = 0; i < myarray.length; i++)
                    {
                        json.push({"name": myarray[i]});
                    }
                }
                $("#autocomplete").tokenInput(<?php include '../include/agenda_autocomplete.php'; ?>, {
                    onResult: function (item) {
                        if ($.isEmptyObject(item)) {
                            return [{id: '0', name: $("tester").text()}]
                        } else {
                            item.unshift({"name": $("tester").text()});
                            var lookup = {};
                            var result = [];

                            for (var temp, i = 0; temp = item[i++]; ) {
                                var name = temp.name;

                                if (!(name in lookup)) {
                                    lookup[name] = 1;
                                    result.push({"name": name});
                                }
                            }
                            return result;
                        }

                    },
                    onAdd: function (item) {
                        var value = $('.keywords-searched').val();
                        if (value != '') {
                            $('.keywords-searched').val(value + "," + item.name);
                        } else {
                            $('.keywords-searched').val(item.name);
                        }
                    },
                    onDelete: function (item) {
                        var value = $('.keywords-searched').val().split(",");
                        $('.keywords-searched').empty();
                        var data = "";
                        $.each(value, function (key, value) {
                            if (value != item.name) {
                                if (data != '') {
                                    data = data + "," + value;
                                } else {
                                    data = value;
                                }
                            }
                        });
                        $('.keywords-searched').val(data);
                    },
                    resultsLimit: 10,
                    prePopulate: json
                }
                );

            });
        </script>
        <form name="new-agenda" onsubmit="" id="pdf-update0" method="post"  action="" enctype="multipart/form-data"> 
            <input type="hidden" name="op_1st" value="save_agenda_minutes">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="bid" value="<?php echo $BID ?>">
            <input type="hidden" class="agenda_page" value="1">
            <div class="addParentCategory agenda_autocomplete">
                <div class="form-inside-div margin-left-none adenda-minute-main">
                    <label class="form-inside-div-adv">Meeting Date</label>
                    <div class="form-data add-menu-item-field"> 
                        <input class="previous-date-not-allowed required_fields0" type="text" name="meeting_date" value="" required/>
                    </div>
                    <div class="form-data add-menu-item-field padding-top-bottom minute-agenda-form">
                        <label class="form-inside-div-adv">Agenda</label>
                        <div class="agenda-min-pdf">
                            <label class="form-inside-div-adv" for="agenda_pdf_new0">Browse for PDF</label>
                            <input onchange="show_file_name_agenda(this.files[0].name, 1)" class="agenda_file_ext0" id="agenda_pdf_new0" type="file" accept="application/pdf" name="file_agenda" value="" style="visibility: hidden; position: absolute; z-index: 0">
                            <span class="agenda_pdf_new0 agenda_file_name">
                                <span id="addDate"></span>
                            </span>
                        </div>
                    </div>
                    <div class="form-data add-menu-item-field padding-top-bottom minute-agenda-form">
                        <label class="form-inside-div-adv">Minutes</label>
                        <div class="agenda-min-pdf">
                            <label class="form-inside-div-adv" for="minutes_pdf_new0">Browse for PDF</label>
                            <input class="minute_file_ext0" onchange="show_file_name_minuts(this.files[0].name, 1)" id="minutes_pdf_new0" type="file" accept="application/pdf" name="file_minute" value="" style="visibility: hidden; position: absolute; z-index: 0">
                            <span class="minutes_pdf_new0 agenda_file_name minuts">
                                <span id="editDate"></span>
                            </span>
                        </div>                           
                    </div>
                    <div class="form-data add-menu-item-field padding-top-bottom">
                        <label class="form-inside-div-adv">Keywords</label>
                        <input type="text" id="autocomplete" size="50"  />                            
                        <input type="hidden" id="listing_search" class="keywords-searched" name="new_keyword" value="">
                    </div>
                </div>
            </div>
            <div class="form-inside-div border-none margin-left-none">
                <div class="button">
                    <input type="submit" name="add_meeting" onclick="" value="Add Meeting Now" />
                </div>      
            </div> 
        </form>
        <div class="progress-container-main" style="display:none;">
            <div id="progress-container">
                <p class="progress-title">Upload in Progress</p>
                <p>Please leave this page open while your PDF is uploading</p>
                <div id="progressbox" style="display:none;">
                    <div id="progressbar"></div>
                    <div id="statustxt">0%</div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-inside-div border-none margin-top-agenda">
        <div class="content-sub-header manage-agenda-minute-title border-none">
            <div class="title">Manage Agendas & Minutes</div>
        </div>
    </div>
    <?PHP
    $sql = "SELECT * FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '$BL_ID' ORDER BY BFAM_Date DESC";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $i = 0;
    while ($data = mysql_fetch_assoc($result)) {
        ?>
        <div class="pdf-update<?php echo $data['BFAM_ID'] ?>">
            <form method="post" action="" id="<?php echo $data['BFAM_ID'] . ' #@ ' . $data["BFAM_Keywords"] ?>" enctype="multipart/form-data">
                <input type="hidden" name="op" value="edit_agenda_minutes">
                <input type="hidden" id="BFAM_ID" name="BFAM_ID" value="<?php echo $data['BFAM_ID'] ?>">
                <input type="hidden" class="agenda_page" value="1">
                <div class="addParentCategory agenda_autocomplete">
                    <div class="form-inside-div add-product-section border-none padding-none adenda-minute-main">
                        <div class="form-data add-menu-item-field padding-top-bottom">
                            <label>Date</label>
                            <input class="previous-date-not-allowed required_fields<?php echo $data['BFAM_ID'] ?>" type="text" name="meeting_date" value="<?php echo $data['BFAM_Date'] ?>">
                        </div>
                        <div class="form-data add-menu-item-field padding-top-bottom minute-agenda-form">
                            <label>Agenda</label>
                            <div class="agenda-min-pdf">
                                <label for="agenda_pdf_<?php echo $data['BFAM_ID'] ?>" class="">Browse for PDF</label>
                                <input onchange="show_file_name_agenda_edit(this.files[0].name, '<?php echo $i ?>')" class="agenda_file_ext<?php echo $data['BFAM_ID'] ?>" id="agenda_pdf_<?php echo $data['BFAM_ID'] ?>" accept="application/pdf" type="file" name="file_agenda" value="" style="visibility: hidden; position: absolute; z-index: 0">
                                <span class="agenda_edit" id="agenda_edit<?php echo $i++ ?>"></span>
                            </div>
                            <?php
                            if ($data['BFAM_Agenda'] != "") {
                                header('Content-type: application/pdf');
                                header('Content-Disposition: attachment; filename="' . $data['BFAM_Agenda'] . '"');
                                ?>
                                <div class="agenda-min-pdf view-delete">
                                    <a target="_blank" href="http://<?php echo DOMAIN . PDF_LOC_REL . $data['BFAM_Agenda'] ?>">View PDF</a>
                                </div>
                                <div class="agenda-min-pdf view-delete">
                                    <a onClick="return confirm('Are you sure this action can not be undone!');" href="/admin/listings-preview.php?del_pdf_age=<?php echo $data['BFAM_ID']; ?>&op=del_pdf_agenda_minutes&bl_id=<?php echo $BL_ID ?>">Delete PDF</a>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="form-data add-menu-item-field padding-top-bottom minute-agenda-form">
                            <label>Minutes</label>
                            <div class="agenda-min-pdf">
                                <label for="minutes_pdf_<?php echo $data['BFAM_ID'] ?>" class="">Browse for PDF</label>
                                <input onchange="show_file_name_minuts_edit(this.files[0].name, '<?php echo $i ?>')" class="minute_file_ext<?php echo $data['BFAM_ID'] ?>" id="minutes_pdf_<?php echo $data['BFAM_ID'] ?>" type="file" accept="application/pdf" name="file_minute" value="" style="visibility: hidden; position: absolute; z-index: 0">
                                <span class="agenda_edit_minuts" id="agenda_edit_minuts<?php echo $i++ ?>"></span>
                            </div>
                            <?php
                            if ($data['BFAM_Minute'] != "") {
                                header('Content-type: application/pdf');
                                header('Content-Disposition: attachment; filename="' . $data['BFAM_Minute'] . '"');
                                ?>
                                <div class="agenda-min-pdf view-delete">
                                    <a target="_blank" href="http://<?php echo DOMAIN . PDF_LOC_REL . $data['BFAM_Minute'] ?>">View PDF</a>
                                </div>
                                <div class="agenda-min-pdf view-delete">
                                    <a onClick="return confirm('Are you sure this action can not be undone!');" href="/admin/listings-preview.php?del_pdf_min=<?php echo $data['BFAM_ID']; ?>&op=del_pdf_agenda_minutes&bl_id=<?php echo $BL_ID ?>&flag=<?php echo $flag ?>">Delete PDF</a>
                                </div>
                            <?php } ?>                             
                        </div>
                        <div class="form-data  add-menu-item-field padding-top-bottom">
                            <label>Keywords</label>
                            <div class="form-data">
                                <input type="text" class="autocomplete_<?php echo $data['BFAM_ID'] ?>" size="50"/>
                                <input type="hidden" class="keywords-searched_<?php echo $data['BFAM_ID'] ?>" name="new_keyword" value="<?php echo (isset($data['BFAM_Keywords']) && $data['BFAM_Keywords'] != "") ? $data['BFAM_Keywords'] : "" ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-inside-div border-none margin-left-none">
                        <div class="button" style="width: 30%;margin-left: 22%;">
                            <input type="submit" name="save_now" onclick="progress_bar(<?php echo $data['BFAM_ID'] ?>)" value="Save Now" />
                        </div>
                        <div class="button" style="width:20%;margin-top: 11px;">
                            <a id="del_agenda_minute" onClick="return confirm('Are you sure this action can not be undone!');" href="listings-preview.php?delete=<?php echo $data['BFAM_ID']; ?>&op=del_agenda_minutes&bl_id=<?php echo $BL_ID ?>">Delete</a>
                        </div>
                    </div> 
                </div>
                <script>

                </script>
            </form>
        </div>
        <?php
    }
    ?>
    <script type="text/javascript">
        $(function () {
            $(".agendmin").one("click", function () {
                var values = this.id.split("#@");
                var BFAM_ID = values[0];
                var searched_items = values[1];

                if (searched_items.length >= 1 && searched_items.trim()) {
                    var myarray = searched_items.split(',');
                    var json = [];
                    for (var i = 0; i < myarray.length; i++)
                    {
                        json.push({"name": myarray[i]});
                    }
                }
                $(".autocomplete_" + BFAM_ID).tokenInput(<?php include '../include/agenda_autocomplete.php'; ?>, {
                    onResult: function (item) {
                        if ($.isEmptyObject(item)) {
                            return [{id: '0', name: $("tester").text()}]
                        } else {
                            item.unshift({"name": $("tester").text()});
                            var lookup = {};
                            var result = [];
                            for (var temp, i = 0; temp = item[i++]; ) {
                                var name = temp.name;

                                if (!(name in lookup)) {
                                    lookup[name] = 1;
                                    result.push({"name": name});
                                }
                            }
                            return result;
                        }

                    },
                    onAdd: function (item) {
                        var value = $('.keywords-searched_' + BFAM_ID).val();
                        if (value != '') {
                            $('.keywords-searched_' + BFAM_ID).val(value + "," + item.name);
                        } else {
                            $('.keywords-searched_' + BFAM_ID).val(item.name);
                        }
                    },
                    onDelete: function (item) {
                        var value = $('.keywords-searched_' + BFAM_ID).val().split(",");
                        $('.keywords-searched_' + BFAM_ID).empty();
                        var data = "";
                        $.each(value, function (key, value) {
                            if (value != item.name.trim()) {
                                if (data != '') {
                                    data = data + "," + value;
                                } else {
                                    data = value;
                                }
                            }
                        });
                        $('.keywords-searched_' + BFAM_ID).val(data);
                    },
                    resultsLimit: 10,
                    prePopulate: json
                });

            });
        });

    </script>
    <div class="form-inside-div listing-ranking border-none margin-top-agenda-points">
        Ranking Points: <?php echo $points_taken ?> points out of <?php echo $AgendaMinute ?> points
    </div>

</div>
<!--END-->

<!--GUEST BOOK-->
<?php
$sql = "SELECT * FROM tbl_Feature WHERE F_ID NOT IN (3)";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
while ($row = mysql_fetch_assoc($result)) {
    //Guest Book
    if ($row['F_Name'] == 'Guest Book') {
        $sqlGB = "SELECT * FROM tbl_Business_Feature_Guest_Book WHERE BFGB_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BFGB_Status = 1";
        $resultGB = mysql_query($sqlGB) or die(mysql_error());
        $numRowsGB = mysql_num_rows($resultGB);
        if ($numRowsGB > 0 && $row1['BLF_Active'] == "1") {
            $class = 'points-com';
        } else {
            $class = 'points-uncom';
        }
    }
}
$sql_description_daily = mysql_query("SELECT * from tbl_Business_Feature_Guest_Book_Description WHERE BFGB_BL_ID= '$BL_ID'");
$data_description = mysql_fetch_assoc($sql_description_daily);
?>
<div style="display:none" id="guest_book_form">
    <div class="content-header">
        <div class="title">Guest Book</div>
        <div class="link">
            <?php
            $guestBook = show_addon_points(12);
            if ($numRowsGB > 0) {
                echo '<div class="points-com">' . $guestBook . ' pts</div>';
            } else {
                echo '<div class="points-uncom">' . $guestBook . ' pts</div>';
            }
            ?>
        </div>
    </div>

    <?php
    $help_text = show_help_text('Guest Book');
    if ($help_text != '') {
        echo '<div class="form-inside-div border-none">' . $help_text . '</div>';
    }
    ?>
    <form name="form1" method="post"  action="">
        <div class="daily-special-specifivation">
            <div class="form-inside-div add-menu-item Description-width">
                <input type="hidden" name="id" id="daily_bl_id" value="<?php echo $BL_ID ?>"> 
                <label>Description</label>
                <div class="form-data add-menu-item-field wiziwig-about">
                    <textarea name="guest-book-description" class="Description-textarea description-ckeditor" id="daily-description-gb"  cols="60" rows="3" ><?php echo $data_description['BFGB_Description'] ?></textarea>
                </div>
                <script>
                    $('#click_guest_book_desc').click(function () {
                        $('#guest_book_form').dialog('open');
                        $('#daily-description-gb').ckeditor();
                    });
                    $(function () {
                        $('#guest_book_form').attr("title", "Add Description").dialog({
                            autoOpen: false,
                        });
                    });
                </script>
            </div>
            <div class="form-inside-div add-menu-item-button">
                <div class="button">
                    <input type="submit" name="button_guest-book" value="Save" />
                </div>      
            </div> 
        </div>      
    </form>
</div>
<!--END-->

<!--REGIONS-->
<?php
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
          WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
    $BL_ID = $rowListing['BL_ID'];
} elseif ($_REQUEST['bid'] > 0) {
    $sql = "SELECT B_ID FROM tbl_Business WHERE B_ID = '" . encode_strings($_REQUEST['bid'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);

    $BID = $rowListing['B_ID'];
} else {
    header("Location: customers.php");
    exit();
}
?>
<div style="display:none">
    <form name="form1" method="post" action="" id="frmRegion">
        <script>
            function makeSEO(myVar) {
                myVar = myVar.toLowerCase();
                myVar = myVar.replace(/[^a-z0-9\s-]/gi, '');
                myVar = myVar.replace(/"/g, '');
                myVar = myVar.replace(/'/g, '');
                myVar = myVar.replace(/&/g, '');
                myVar = myVar.replace(/\//g, '');
                myVar = myVar.replace(/,/g, '');
                myVar = myVar.replace(/  /g, ' ');
                myVar = myVar.replace(/ /g, '-');

                $('#SEOname').val(myVar);
                return false;
            }
        </script>
        <input type="hidden" name="op" value="save_region">
        <input type="hidden" name="bl_id" value="<?php echo (($rowListing['BL_ID'])) ? $rowListing['BL_ID'] : ''; ?>">
        <input type="hidden" name="bid" value="<?php echo $BID ?>">
        <input type="hidden" name="delR" id="delR" value="0">
        <input type="hidden" name="delC" id="delC" value="0">

        <div class="content-header">
            <div class="title">Websites</div>
            <?php if (isset($BL_ID)) { ?>
                <div class="link"><a href="#" onClick="return addRegion();" class="table-boldtext">+Add Region</a></div>
            <?php } ?>
        </div>

        <?PHP
        if (isset($BL_ID) && $BL_ID > 0) {
            $sql = "SELECT BLCR_BLC_R_ID, BLCR_ID FROM tbl_Business_Listing_Category_Region  WHERE BLCR_BL_ID = " . encode_strings($rowListing['BL_ID'], $db);
            $sql .= $regionLimitCommaSeparated ? (" AND BLCR_BLC_R_ID IN(" . $regionLimitCommaSeparated . ") ") : "";
            $resultRegion = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($rowRegion = mysql_fetch_assoc($resultRegion)) {
                ?>
                <?php
                $sql = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Type NOT IN(1,6)";
                $sql .= $regionLimitCommaSeparated ? (" AND R_ID IN(" . $regionLimitCommaSeparated . ") ") : "";
                $sql .= " ORDER BY R_Name";
                $resultRegionList = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                ?>                                      

                <div class="form-inside-div">
                    <label>Website</label>
                    <div class="form-data">
                        <select name="region[]" required>
                            <option value="">Select Region</option>
                            <?PHP
                            while ($rowRList = mysql_fetch_assoc($resultRegionList)) {
                                echo "<option value='" . $rowRList['R_ID'] . "'";
                                echo $rowRList['R_ID'] == $rowRegion['BLCR_BLC_R_ID'] ? 'selected' : '';
                                echo ">" . $rowRList['R_Name'] . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <a style="float:left;padding:17px 0 16px 20px;" href="listings-preview.php?bl_id=<?php echo $rowListing['BL_ID'] ?>&delRegion=<?php echo $rowRegion['BLCR_ID'] ?>" onClick="return delRegion();">Delete</a>
                </div>
                <?php
            }
        }
        if (!isset($BL_ID)) {
            ?>
            <div class="form-inside-div">
                <div class="content-sub-header" style="margin-bottom:10px;">
                    Listing Package
                </div>
                <label>Listing Package</label>
                <div class="form-data">
                    <?php
                    if (in_array('bgadmin', $_SESSION['USER_ROLES'])) {
                        $sql = "SELECT LT_ID, LT_Name FROM tbl_Listing_Type WHERE LT_ID IN (1, 4) ORDER BY LT_Order";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    } else if (in_array('town', $_SESSION['USER_ROLES'])) {
                        $sql = "SELECT LT_ID, LT_Name FROM tbl_Listing_Type WHERE LT_ID IN (1, 5) ORDER BY LT_Order";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    } else {
                        $sql = "SELECT LT_ID, LT_Name FROM tbl_Listing_Type ORDER BY LT_Order";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    }
                    ?>
                    <select name="type" required>
                        <option value="">Select Listing Package</option>
                        <?PHP
                        $js = '';
                        while ($row = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $row['LT_ID'] ?>" <?php echo ($rowListing['BL_Listing_Type'] == $row['LT_ID']) ? 'selected' : '' ?>><?php echo $row['LT_Name'] ?></option>
                            <?PHP
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-inside-div">
                <label>Listing Title</label>
                <div class="form-data">
                    <input name="name" type="text" value="" size="50" onKeyUp="makeSEO(this.value)" required/>
                </div>

            </div>
            <div class="form-inside-div">
                <label>SEO Name</label>
                <div class="form-data">
                    <input name="seoName" id="SEOname" type="text" value="" size="50" onKeyUp="makeSEO(this.value)" required/>
                </div>
            </div>
        <?php } ?>
        <div class="form-inside-div" id="add_new_region" style="display:none">
            <label>Region</label>
            <div class="form-data">
                <?php
                $sql = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Type NOT IN(1,6)";
                $sql .= $regionLimitCommaSeparated ? ("AND R_ID IN(" . $regionLimitCommaSeparated . ") ") : "";
                $sql .= " ORDER BY R_Name";
                $resultRegionList = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                ?>
                <select name="region[]" required>
                    <option value="">Select Region</option>
                    <?PHP
                    while ($rowRList = mysql_fetch_assoc($resultRegionList)) {
                        echo "<option value='" . $rowRList['R_ID'] . "'>" . $rowRList['R_Name'] . "</option>";
                    }
                    ?>
                </select>
            </div>
        </div>
        <script type="text/javascript">
            function addRegion() {
                $('#add_new_region').show();
                return false;
            }
            function delRegion() {
                if (confirm('Are you sure you want to delete this region?')) {
                    return true;
                }
                return false;
            }
        </script>
        <div class="form-inside-div border-none">
            <div class="button">
                <input type="submit" name="button2" value="Save Now">
            </div>
        </div>
    </form>
</div>
<!--END-->

<!--SEASONS-->
<?php
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
    $sqlLS = "SELECT BLS_S_ID FROM tbl_Business_Listing_Season WHERE BLS_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $resultLS = mysql_query($sqlLS, $db) or die("Invalid query: $sqlLS -- " . mysql_error());
} else {
    header("Location:customers.php");
    exit();
}
?>
<div style="display:none" id="seasons_form">

    <div class="content-header">
        <div class="title">Seasons</div>
        <div class="link">
        </div>
    </div>

    <form name="form1" method="post" action="">
        <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
        <input type="hidden" name="op" value="save_seasons">
        <?PHP
        $listingSeasons = array();
        while ($rowLS = mysql_fetch_array($resultLS)) {
            $listingSeasons[] = $rowLS['BLS_S_ID'];
        }
        $sqlSeasons = "SELECT S_ID, S_Name  FROM tbl_Seasons";
        $resultSeasons = mysql_query($sqlSeasons, $db) or die("Invalid query: $sqlSeasons -- " . mysql_error());
        while ($rowSeasons = mysql_fetch_array($resultSeasons)) {
            ?>
            <div class="form-inside-div">
                <input name="season[<?php echo $rowSeasons['S_ID'] ?>]" type="checkbox" value="<?php echo $rowSeasons['S_ID'] ?>" <?php echo in_array($rowSeasons['S_ID'], $listingSeasons) ? 'checked' : '' ?> /><?php echo $rowSeasons['S_Name'] ?>
            </div>
        <?php }
        ?>
        <div class="form-inside-div border-none">
            <div class="button">
                <input type="submit" value="Save Now">
            </div>
        </div>
    </form>

</div>
<!--END-->

<!--BOKUN-->
<?php
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words, BL_Bokun FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
} else {
    header("Location:customers.php");
    exit();
}
?>
<div style="display:none">
    <form id="bokun_form" action="" method="post" name="form1">
        <input type="hidden" name="op" value="save_bokun">
        <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
        <div class="content-header">
            <div class="title">Bokun</div>
            <div class="link">
            </div>
        </div>
        <div class="form-inside-div border-none bottom-pading-none">
            <div class="form-data">
                <textarea name="bokun" cols="73" rows="8" style="width : 539px;"><?php echo $rowListing['BL_Bokun'] ?></textarea>                                   
            </div>
        </div>

        <div class="form-inside-div border-none">
            <div class="button">
                <input type="submit" name="button2" value="Save Now"/>
            </div>  
        </div>
    </form>
</div>
<!--END-->

<!--SEO-->
<?php
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
}
$sql = "SELECT * FROM tbl_Points_Category WHERE PC_ID != 14";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
while ($rowCategories = mysql_fetch_array($result)) {
    //Search Engine
    if ($rowCategories['PC_Name'] == 'Search Engine') {
        if (isset($rowListing['BL_SEO_Title']) && $rowListing['BL_SEO_Title'] != '' && $rowListing['BL_SEO_Description'] != '' && $rowListing['BL_SEO_Keywords'] != '' && $rowListing['BL_Search_Words'] != '') {
            $class = 'points-com';
        } else {
            $class = 'points-uncom';
        }
        $total_seo_points = $rowCategories['PC_Points'];
    }
}
?>
<div style="display:none">
    <div class="listing-inside-div-tittle">
    </div>
    <form id="seo_form" action="" method="post" enctype="multipart/form-data" name="form1">
        <input type="hidden" name="op" value="save_seo">
        <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
        <div class="content-header">Search Engine Optimization
        </div>
        <?php
        $help_text = show_help_text('S.E.O');
        if ($help_text != '') {
            echo '<div class="form-inside-div">' . $help_text . '</div>';
        }
        ?>
        <div class="form-inside-div">
            <label>Title Tag</label>
            <div class="form-data">
                <input name="title" type="text" size="50" value="<?php echo $rowListing['BL_SEO_Title'] ?>"/> 
            </div>
            <?php
            $title = show_field_points('Title Tag');
            $points_taken = 0;
            if ($rowListing['BL_SEO_Title']) {
                echo '<div class="points-com">' . $title . ' pts</div>';
                $points_taken += $title;
            } else {
                echo '<div class="points-uncom">' . $title . ' pts</div>';
            }
            ?>
        </div>
        <div class="form-inside-div">
            <label  class="seo-padding-title">S.E.O Description</label>
            <div class="form-data">
                <input name="description" type="text" size="50" value="<?php echo $rowListing['BL_SEO_Description'] ?>"/> 
            </div>
            <?php
            $desc = show_field_points('S.E.O Description');
            if ($rowListing['BL_SEO_Description']) {
                echo '<div class="points-com">' . $desc . ' pts</div>';
                $points_taken += $desc;
            } else {
                echo '<div class="points-uncom">' . $desc . ' pts</div>';
            }
            ?>
        </div>
        <div class="form-inside-div">
            <label>Keywords</label>
            <div class="form-data">
                <input name="keywords" type="text" size="50" value="<?php echo $rowListing['BL_SEO_Keywords'] ?>"/> 
            </div> 
            <?php
            $keywords = show_field_points('Keywords');
            if ($rowListing['BL_SEO_Keywords']) {
                echo '<div class="points-com">' . $keywords . ' pts</div>';
                $points_taken += $keywords;
            } else {
                echo '<div class="points-uncom">' . $keywords . ' pts</div>';
            }
            ?>
        </div>  
        <div class="form-inside-div border-none">
            <div class="content-sub-header">
                <div class="title">My Page Searching</div>
            </div>
            <?php
            $help_text1 = show_help_text('My Page Search');
            if ($help_text1 != '') {
                echo '<div class="form-inside-div margin-none">' . $help_text1 . '</div>';
            }
            ?>
            <div class="form-inside-div margin-none">
                <label>Search Words</label>
                <div class="form-data">
                    <input name="search" type="text" size="50" value="<?php echo $rowListing['BL_Search_Words'] ?>" /> 
                </div>
                <?php
                $searchwords = show_field_points('Search Words');
                if ($rowListing['BL_Search_Words']) {
                    echo '<div class="points-com">' . $searchwords . ' pts</div>';
                    $points_taken += $searchwords;
                } else {
                    echo '<div class="points-uncom">' . $searchwords . ' pts</div>';
                }
                ?>
            </div>
        </div>


        <div class="form-inside-div border-none">
            <div class="button">
                <input type="submit" name="button2" value="Save Now"/>
            </div>
        </div> 

        <div class="form-inside-div listing-ranking border-none">
            Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_seo_points ?> points
        </div>
    </form>
</div>
<!--END-->


<!--CATEGORY-->
<?php
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words, BL_Billing_Type, BL_CoC_Dis_2, BL_Line_Item_Title, BL_Line_Item_Price FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID WHERE BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
    $BL_ID = $rowListing['BL_ID'];
}
?>
<div style="display:none" id="category_section">
    <div class="content-header">
        <div class="title">Category</div>
        <div class="link">
            <div class="add-link">
                <a onclick="show_form()">+Add Category</a>
            </div>            
        </div>
    </div>
    <form name="frmCategory" id="frmCategory" class="frmCategorySubmit" method="post" action="">
        <?php
        $sql_type = "SELECT BL_Listing_Type from tbl_Business_Listing WHERE BL_ID='" . encode_strings($BL_ID, $db) . "'";
        $result_type = mysql_query($sql_type);
        $row_type = mysql_fetch_array($result_type);
        ?>
        <input type="hidden" name="op" value="save_type">
        <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
        <?php
        if (in_array('bgadmin', $_SESSION['USER_ROLES'])) {
            echo '<input type="hidden" value="' . $row_type['BL_Listing_Type'] . '" name="type">';
        } else {
            ?>
            <div class="form-inside-div">
                <div class="content-sub-header" style="margin-bottom:10px;">
                    Listing Package
                </div>
                <label>Listing Package</label>
                <div class="form-data">
                    <?php
                    if (in_array('town', $_SESSION['USER_ROLES'])) {
                        $sql = "SELECT * FROM tbl_Listing_Type WHERE LT_ID IN (1, 5) ORDER BY LT_Order";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    } else {
                        $sql = "SELECT * FROM tbl_Listing_Type ORDER BY LT_Order";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    }
                    ?>
                    <select name="type" id="select" required>
                        <option value="">Select Listing Package</option>
                        <?PHP
                        $js = '';
                        while ($row = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $row['LT_ID'] ?>" <?php echo $row_type['BL_Listing_Type'] == $row['LT_ID'] ? 'selected' : '' ?>><?php echo $row['LT_Name'] ?></option>
                            <?PHP
                        }
                        ?>
                    </select>

                </div>
            </div>
            <div class="form-inside-div" style="border:none;">
                <div class="button">
                    <input type="submit" name="button2" value="Save" class="cat-button"/>
                </div>
            </div>
        <?php } ?>
    </form>   
    <script>
        function enable_payment() {
            if ($('#select').val() == '4') {
                swal({title: "Are you sure?", text: "Your billing will be resume on this action.", type: "warning", showCancelButton: true, confirmButtonColor: "#6dac29", confirmButtonText: "Yes please", cancelButtonText: "No thank you", closeOnConfirm: false, closeOnCancel: false},
                        function (isConfirm) {
                            if (isConfirm) {
                                $(".frmCategorySubmit").submit();
                            } else {
                                swal("Cancelled", "You have just cancelled adding sub category.", "error");
                            }
                        });
                return false
            }
        }
    </script>
    <form name="frm_new_Category" id="new_main_Category" method="post" action="" style="display: none;">
        <input type="hidden" name="op" value="save_new">
        <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
        <div class="form-inside-div">
            <div class="content-sub-header" style="margin-bottom:10px;">
                Main Category
            </div>
            <label>Main Category</label>
            <div class="form-data">
                <select name="category" id="category_0" onChange="get_sub_category(0)" required>

                    <option value="">Select Main Category</option>
                    <?PHP
                    $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = 0 AND C_Is_Blog=0 AND C_Is_Product_Web=0 ORDER BY C_Order";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    $js = '';
                    while ($row = mysql_fetch_assoc($result)) {
                        ?>
                        <option value="<?php echo $row['C_ID'] ?>"><?php echo $row['C_Name'] ?></option>
                        <?PHP
                    }
                    ?>

                </select>
            </div>
        </div>
        <div class="form-inside-div">
            <div class="content-sub-header" style="margin-bottom:10px;">
                Sub-Category
            </div>
            <label>Sub-Category 1</label>
            <div class="form-data listings_subcategory_0">
                <select class="instructional-background" name="" >
                    <option>Select Sub Category</option>
                </select>
            </div>
        </div>
        <div class="form-inside-div" style="border:none;">
            <div class="button">
                <input type="submit" name="button2" value="Save" class="cat-button"/>
            </div>
        </div>
    </form>
    <?php
    $sql_new = "SELECT BLC_M_C_ID FROM tbl_Business_Listing_Category LEFT JOIN tbl_Business_Listing ON BL_ID=BLC_BL_ID
                WHERE BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "' GROUP BY  BLC_M_C_ID";
    $result_get_details_new = mysql_query($sql_new, $db) or die("Invalid query: $sql -- " . mysql_error());
    $main_cat_count = 1;
    while ($rowListing_new = mysql_fetch_assoc($result_get_details_new)) {
        ?>
        <div class="content-header">
            <?php
            $sql_get_name = "SELECT C_Name FROM tbl_Category WHERE C_ID ='" . encode_strings($rowListing_new['BLC_M_C_ID'], $db) . "'";
            $result_get_name = mysql_query($sql_get_name, $db) or die("Invalid query: $sql_get_name -- " . mysql_error());
            $row_get_name = mysql_fetch_assoc($result_get_name);
            ?>
            <div class="title"><?php echo $row_get_name['C_Name'] ?></div>
            <div class="link">
                <div class="add-link">
                    <a onclick="return confirm('Are you sure?');" href="listings-preview.php?bl_id=<?php echo $BL_ID ?>&del_cat=<?php echo $rowListing_new['BLC_M_C_ID']; ?>">Delete</a>
                </div>            
            </div>
        </div>
        <form name="frmCategory" id="frmCategory" method="post" action="">
            <input type="hidden" name="op" value="save_category">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="delSubCat" id="delSubCat" value="0">
            <input type="hidden" name="newSubCat" id="newSubCat" value="0">
            <input type="hidden" name="newCat" id="newCat_<?php echo $main_cat_count ?>" value="0">
            <input type="hidden" name="newCat_mutiple_id"  value="<?php echo $rowListing_new['BLC_M_C_ID'] ?>">
            <div class="form-inside-div">
                <div class="content-sub-header" style="margin-bottom:10px;">
                    Main Category
                </div>
                <label>Main Category</label>
                <div class="form-data">
                    <select required name="category" id="category_<?php echo $main_cat_count ?>" onChange="get_sub_category(<?php echo $main_cat_count ?>)">

                        <option value="">Select Main Category</option>
                        <?PHP
                        $sql = "SELECT C_ID, C_Name  FROM tbl_Category WHERE C_Parent = 0  AND C_Is_Blog=0 AND  C_Is_Product_Web=0  ORDER BY C_Order";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        $js = '';
                        while ($row = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $row['C_ID'] ?>" <?php echo $rowListing_new['BLC_M_C_ID'] == $row['C_ID'] ? 'selected' : '' ?>><?php echo $row['C_Name'] ?></option>
                            <?PHP
                        }
                        ?>

                    </select>
                    <script type="text/javascript">
                        function confirmCat()
                        {
                            if (confirm('Are you sure, this can not be changed!')) {
                                newCat();
                            } else {
                                $('#category option').first().attr('selected', 'selected');
                                return false;
                            }
                        }
                        function newCat()
                        {
                            $("#newCat").val("1");
                            $('#frmCategory').submit();
                            return false;
                        }
                    </script> 
                </div>
            </div>

            <?PHP
            if ($rowListing_new['BLC_M_C_ID']) {
                $sql = "SELECT *, concat('c',BLC_ID) as arrKey FROM tbl_Business_Listing_Category
                        WHERE BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "' and BLC_M_C_ID= '" . encode_strings($rowListing_new['BLC_M_C_ID'], $db) . "' ORDER BY BLC_ID";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                $subCount = mysql_num_rows($result);

                if (isset($_REQUEST['newSubCat'])) {
                    $subCount++;
                }

                $subs = array();
                while ($rowSubs = mysql_fetch_assoc($result)) {
                    $subs[] = $rowSubs;
                }
                ?>

                <div class="form-inside-div">
                    <div class="content-sub-header" style="margin-bottom:10px;">
                        Sub-Category
                    </div>
                    <label>Sub-Category 1</label>
                    <div class="form-data listings_subcategory_<?php echo $main_cat_count ?>">
                        <?php
                        $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '" . encode_strings($rowListing_new['BLC_M_C_ID'], $db) . "' ORDER BY C_Order";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        ?>
                        <select class="instructional-background" required name="subcat[<?php echo $subs[0]['arrKey'] ?>]" id="subcat-<?php echo $subs[0]['arrKey'] ?>" >
                            <option value="">Select Sub Category</option>
                            <?PHP
                            while ($row = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo $row['C_ID'] ?>" <?php echo $subs[0]['BLC_C_ID'] == $row['C_ID'] ? 'selected' : '' ?>><?php echo $row['C_Name'] ?></option>
                                <?PHP
                            }
                            ?>

                        </select>
                    </div>
                </div>
                <div id="sub_category_childs_<?php echo $main_cat_count ?>">
                    <?PHP
                    if ($subCount > 1) {
                        for ($i = 1; $i < $subCount; $i++) {
                            ?>
                            <div class="form-inside-div">
                                <label>Sub-Category <?php echo $i + 1 ?></label>
                                <div class="form-data">
                                    <select class="instructional-background" name="subcat[<?php echo $subs[$i]['arrKey'] ?>]" id="subcat-<?php echo $subs[$i]['arrKey'] ?>">
                                        <option>Select Sub Category</option>
                                        <?PHP
                                        $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '" . encode_strings($rowListing_new['BLC_M_C_ID'], $db) . "' ORDER BY C_Order";
                                        $result = mysql_query($sql, $db)
                                                or die("Invalid query: $sql -- " . mysql_error());
                                        while ($row = mysql_fetch_assoc($result)) {
                                            ?>
                                            <option value="<?php echo $row['C_ID'] ?>" <?php echo $subs[$i]['BLC_C_ID'] == $row['C_ID'] ? 'selected' : '' ?>><?php echo $row['C_Name'] ?></option>
                                            <?PHP
                                        }
                                        ?>
                                    </select>
                                </div>
                                <?PHP if ($subs[$i]['BLC_ID']) { ?>
                                    <a style="float:left;padding:17px 0 16px 20px;" onClick="return delSubCat();" href="listings-preview.php?bl_id=<?php echo $rowListing['BL_ID'] ?>&delSubCat=<?php echo $subs[$i]['BLC_ID'] ?>&del_cat=<?php echo $rowListing_new['BLC_M_C_ID']; ?>">Delete</a>
                                    <?PHP
                                } else {
                                    echo "&nbsp;";
                                }
                                ?>
                            </div>
                            <?PHP
                        }
                    }
                    ?>
                </div>
                <div class="form-inside-div" id="sub_cat_new_<?php echo $main_cat_count ?>" style="display:none;">
                    <label>Sub-Category <?php echo $subCount + 1 ?></label>
                    <div class="form-data">
                        <?php
                        $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '" . encode_strings($rowListing_new['BLC_M_C_ID'], $db) . "' ORDER BY C_Order";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        ?>
                        <select class="instructional-background" name="subcat[]" id="subcat-<?php echo $subs[0]['arrKey'] ?>" class="listings_subcategory">
                            <option>Select Sub Category</option>
                            <?PHP
                            while ($row = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo $row['C_ID'] ?>"><?php echo $row['C_Name'] ?></option>
                                <?PHP
                            }
                            ?>

                        </select>
                    </div>
                </div>
                <div class="form-inside-div" style="border:none;">
                    <div class="button">
                        <?php if ($subCount == 0) { ?>
                            <input type="submit" name="button2" value="Save Now" class="cat-button"/>
                        <?php } elseif (isset($_REQUEST['newSubCat'])) { ?>
                            <input type="button" name="button2" value="Save Now" onClick='form_submit()' class="cat-button"/>
                            <?php
                        } else {
                            ?>
                            <a onClick="return newSubCat(<?php echo $main_cat_count ?>);" id="add_new_sub_cat_<?php echo $main_cat_count ?>" href="#">+Add Sub-Category</a>
                            <input type="submit" name="button2" value="Save Now"/>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
            <script type="text/javascript">
                function newSubCat(id)
                {
                    $("#sub_cat_new_" + id).show();
                    return false;
                }
                function form_submit() {
                    $('#frmCategory').submit();
                }
                function delSubCat()
                {
                    if (confirm('Are you sure you want to delete this sub-category?')) {
                        return true;
                    }
                    return false;
                }

            </script>

        </form>
        <?php
        $main_cat_count++;
    }
    ?>

</div>
<!--END-->
<input id="image-library-usage" type="hidden" value="">
<!--CSS-->
<style>
    .cont_dialog {
        height: 600px !important;
        /* position: fixed; */
        overflow: scroll !important;
    }
    .ui-dialog { 
        /*z-index: 9999 !important ;*/
        height: auto;
        /*overflow-y: scroll;*/
    }
    .ui-dialog .ui-dialog-content{
        /*overflow: hidden;*/
        /*margin-left:40px;*/
    }

    #entertainment-form .ui-dialog .ui-dialog-content{
        /*overflow: hidden;*/
    }


    .content-header {
        float: left;
        width: 540px;
        background: #f0f6ec;
        font-weight: bold;
        padding: 20px;
        color: #66a025;
        font-size: 22px;
    }
    .content-header .title {
        float: left;
        width: 50%;
        color: #66a025;
    }
    .content-header .link {
        float: right;
        width: 50%;
        text-align: right;
    }
    .content-header .link a {
        padding: 10px 15px;
        font-size: 15px;
        background-color: #70aa22;
        color: white;
        text-decoration: none;
        cursor: pointer;
        font-weight: lighter;
    }
    .content-header .link .add-link {
        float: right;
        margin-left: 35px;
    }
    .content-header .link .points-uncom {
        float: right;
        padding: 1px 0;
        width: 105px;
        text-align: right;
        background-image: url(../include/img/sub.gif);
        background-repeat: no-repeat;
        background-position: 25% 50%;
    }
    .content-header .link .points-com {
        float: right;
        padding: 1px 0;
        width: 105px;
        text-align: right;
        background-image: url(../include/img/tick.png);
        background-repeat: no-repeat;
        background-position: 40% 50%;
        font-size: 16px;
    }
    .form-inside-div{
        float:left;
        width:555px;
        padding:14px 0;
        border-bottom:1px solid #eeeeee;
        margin:0 7px 0 18px;
        font-size:16px;
    }
    .form-inside-div label {
        float: left;
        width: 115px;
        padding-top: 17px;
        padding-bottom: 16px;
        padding-left: 4px;
    }
    .form-inside-div .form-data {
        float: left;
    }
    .form-inside-div input[type="text"] {
        width: 300px;
        padding: 15px 10px;
        border: 1px solid #C7C7C7;
        font-size: 16px;
    }
    .form-inside-div .points-com {
        padding: 16px 2px 16px 0;
        float: left;
        width: 80px;
        text-align: right;
        background-image: url(../include/img/tick.png);
        background-repeat: no-repeat;
        background-position: 35% 50%;
    }
    .form-inside-div .button {
        float: left;
        width: 100%;
        text-align: center;
    }
    .form-inside-div input[type="submit"] {
        padding: 10px 35px;
        background: #6dac29;
        color: #fff7ff;
        border: none;
        font-size: 16px;
        cursor: pointer;
    }
    .form-inside-div.listing-ranking {
        background-color: #f9f9f9;
        text-align: center;
        font-size: 18px;
        font-weight: bold;
        color: black;
        margin-bottom: 45px;
    }
    .form-inside-div input[type="email"] {
        width: 300px;
        padding: 15px 10px;
        border: 1px solid #C7C7C7;
        font-size: 16px;
    }
    .form-inside-div .points-uncom {
        padding: 16px 2px 16px 0;
        float: left;
        width: 85px;
        text-align: right;
        background-image: url(../include/img/sub.gif);
        background-repeat: no-repeat;
        background-position: 35% 50%;
    }
    .content-sub-header {
        float: left;
        width: 100%;
        font-weight: bold;
        padding: 8px 0;
        color: #434343;
        border-bottom: 3px solid #eeeeee;
        font-size: 16px;
    }
    .form-inside-div .gmap-left {
        float: left;
        width: 190px;
        padding: 12px 15px;
    }
    .form-inside-div p {
        line-height: 1.5em;
    }
    .form-inside-div .gmap-right {
        float: left;
        width: 335px;
    }
    .form-inside-div .gmap-right .form-inside-div-gmap {
        float: left;
        width: 310px;
        padding: 14px 0;
        border-bottom: 1px solid #eeeeee;
        margin: 0 7px 0 18px;
    }
    .form-inside-div .gmap-right .form-inside-div-gmap label {
        width: 106px;
    }
    .form-inside-div .gmap-right .form-inside-div-gmap input[type="text"] {
        width: 91px;
        padding: 15px 10px;
        border: 1px solid #C7C7C7;
    }
    .content-sub-header .link {
        float: right;
        width: 50%;
        text-align: right;
    }
    .content-sub-header .link .points-com {
        float: right;
        padding: 1px 0;
        width: 105px;
        text-align: right;
        background-image: url(../include/img/tick.png);
        background-repeat: no-repeat;
        background-position: 40% 50%;
    }
    .content-sub-header .title {
        float: left;
        width: 50%;
    }
    .form-inside-div.hours select {
        width: 175px;
        padding: 3px 10px;
    }
    .form-inside-div.hours label {
        padding-top: 6px;
        padding-bottom: 0px;
    }
    .amenity-column {
        float: left;
        width: 230px;
        margin-right: 30px;
        padding: 10px;
        border-bottom: 1px solid #eeeeee;
    }
    .amenity-image {
        float: left;
    }
    .amenity-name {
        float: left;
        margin-left: 15px;
        padding: 3px 3px 3px 0;
        font-size: 15px;
    }
    .amenity-column2 {
        float: left;
        width: 280px;
        padding: 10px;
        border-bottom: 1px solid #eeeeee;
    }
    .del_item{
        padding: 10px 35px;
        background: #6dac29;
        color: #fff7ff;
        border: none;
        font-size: 16px;
        cursor: pointer;
    }
    /*Photo Gallery*/
    .form-inside-div#listing-gallery{
        float:left;
        width:580px;
    }
    .form-inside-div#listing-gallery .content-header{
        margin-bottom: 20px;
    }
    .form-inside-div#listing-gallery ul{
        float:left;
        width:100%;
        padding:0;
        margin:0;
        list-style-type: none;
    }
    .form-inside-div#listing-gallery ul li{
        float:left;
        position:relative;
        width:285px;
        height:210px;
    }
    .form-inside-div#listing-gallery ul li.image{
        margin:0 5px 10px 0;
    }
    /*.form-inside-div#listing-gallery ul li.image1{
        margin:0 0 10px 0;
    }*/
    .form-inside-div#listing-gallery ul li .listing-image{
        position:absolute;
    }
    .form-inside-div#listing-gallery ul li .count{
        position: absolute;
        color:white;
        background-color:#ff8809;
        padding:5px 10px;
    }
    .form-inside-div#listing-gallery ul li .cross a{
        position: absolute;
        margin-left: 259px;
        background-color: rgba(0, 0, 0, 0.70);
        color:white;
        padding:5px 10px;
        text-decoration: none;
    }
    .form-inside-div#listing-gallery ul li .desc{
        position: absolute;
        width: 255px;
        padding: 15px;
        background-color: rgba(0, 0, 0, 0.60);
        color:white;
        bottom:0;
        cursor:pointer;
    }
    .form-inside-div#listing-gallery-about-us{
        float:left;
        width:580px;
    }
    .form-inside-div#listing-gallery-about-us .content-header{
        margin-bottom: 20px;
    }
    .form-inside-div#listing-gallery-about-us ul{
        float:left;
        width:100%;
        padding:0;
        margin:0;
        list-style-type: none;
    }
    .form-inside-div#listing-gallery-about-us ul li{
        float:left;
        position:relative;
        width:285px;
        height:210px;
    }
    .form-inside-div#listing-gallery-about-us ul li.image{
        margin:0 5px 10px 0;
    }
    /*.form-inside-div#listing-gallery-about-us ul li.image1{
        margin:0 0 10px 0;
    }*/
    .form-inside-div#listing-gallery-about-us ul li .listing-image{
        position:absolute;
    }
    .form-inside-div#listing-gallery-about-us ul li .count{
        position: absolute;
        color:white;
        background-color:#ff8809;
        padding:5px 10px;
    }
    .form-inside-div#listing-gallery-about-us ul li .cross a{
        position: absolute;
        margin-left: 259px;
        background-color: rgba(0, 0, 0, 0.70);
        color:white;
        padding:5px 10px;
        text-decoration: none;
    }
    .form-inside-div#listing-gallery-about-us ul li .desc{
        position: absolute;
        width: 255px;
        padding: 15px;
        background-color: rgba(0, 0, 0, 0.60);
        color:white;
        bottom:0;
        cursor:pointer;
    }
    .form-inside-div1 {
        float: left;
        width: 372px;
        padding: 14px 0;
        border-bottom: 1px solid #eeeeee;
        margin: 0 7px 0 18px;
        font-size: 16px;
    }
    .form-inside-div2 {
        width: 148px;
        padding: 14px 0;
        border-bottom: 1px solid #eeeeee;
        margin: 0 7px 0 18px;
        font-size: 16px;
    }
    .daily-special-items {
        border : none !important; 
        margin-left: 15px;
        padding: 14px 0;
        width: 565px !important;
    }

    .form-inside-div a.btn-anchor {
        padding: 10px 35px;
        background: #6dac29;
        color: #fff7ff;
        border: none;
        font-size: 16px;
        cursor: pointer;
        text-decoration: none;
    }
    .data-column {
        float: left;
        width: 20%;
        text-align: center;
        min-height: 10px;
        padding: 10px 0;
        z-index: 1;
        position: relative;
    }
    .adenda-minute-main .agenda-min-pdf {
        float: left;
    }
    .adenda-minute-main .agenda-min-pdf label {
        width: 125px;
        background: #999999;
        color: #fff;
        border: none;
        font-size: 16px;
        cursor: pointer;
        text-align: center;
    }
    .adenda-minute-main .minute-agenda-form {
        width: 100%;
    }
    .content-wrapper .content .content-left.full-width .right .form-inside-div.image-bank-autocomplete .form-data ul.token-input-list {
        width: 300px;
        border: 1px solid #C7C7C7;
    }
    .addParentCategory .form-inside-div .form-data ul.token-input-list {
        width: 300px;
        border: 1px solid #C7C7C7;
        min-height: 30px;
        clear: none;
    }
    .addParentCategory.agenda_autocomplete .form-inside-div .form-data ul.token-input-list {
        float: left;
    }
    .addParentCategory .form-inside-div .form-data ul.token-input-list li.token-input-input-token{
        float: left;
        width: 15px;
        border: none;
    }
    .addParentCategory .form-inside-div .form-data ul.token-input-list li.token-input-input-token input{
        border: none !important;
    }
    .addParentCategory .form-inside-div .form-data ul.token-input-list li.token-input-token{
        margin: 5px 3px;
        padding: 0;
        background-color: #f4f6f6;
        border: 1px solid #ccd3d3;
        color: #000;
        font-size: 13px;
        cursor: default;
        display: block;
        float: left;
        border-radius: 5px;
        line-height: 12px !important;
    }
    .addParentCategory .form-inside-div .form-data ul.token-input-list li.token-input-token p{
        float: left;
        padding: 4px 5px;
        margin: 0;
        line-height: inherit !important;
        font-size: 13px;
    }
    .addParentCategory .form-inside-div .form-data ul.token-input-list li.token-input-token span{
        float: right;
        color: #777;
        cursor: pointer;
        border-left: 1px solid #ccd3d3;
        padding: 4px 4px;
    }

    .form-inside-div.image-bank-autocomplete .form-data ul.token-input-list li.token-input-input-token input[type="text"] {
        border: none;
    }
    #del_agenda_minute{
        padding: 10px 35px;
        background: #6dac29;
        color: #fff7ff;
        border: none;
        font-size: 16px;
        cursor: pointer;
        text-decoration: none;
    }
    .agenda-min-pdf.view-delete {
        padding: 7px 10px;
        float: right;
        margin-right: 6px;
    }
    .form-inside-div select {
        width: 321px;
        padding: 15px 10px;
        border: 1px solid #C7C7C7;
        -webkit-appearance: none;
        -moz-appearance: none;
        background-image: url(../include/img/select_arrow.gif);
        background-repeat: no-repeat;
        background-position: 95% 50%;
        text-indent: 0.01px;
        text-overflow: "";
        font-size: 16px;
    }
    .form-inside-div .unordered-list.existing-recommendations-custom {
        padding-right: 0;
        width: 535px;
    }
    .form-inside-div .unordered-list ul {
        float: left;
        width: 100%;
        margin: 0;
        padding: 0;
        list-style-type: none;
    }
    .form-inside-div .unordered-list ul li {
        float: left;
        width: 100%;
        margin: 0 0 15px 0;
        padding: 0;
    }
    .form-inside-div .unordered-list ul li .listing-title.existing-recommendations {
        width: 75%;
    }
    .form-inside-div .unordered-list ul li .listing-title a {
        float: left;
        width: 100%;
        color: #66a025;
        padding-top: 2px;
    }
    .form-inside-div .unordered-list ul li .existing-recommendations-edit-delete {
        float: right;
        width: 130px;
        text-align: right;
    }
    .form-inside-div .unordered-list ul li .existing-recommendations-edit-delete a {
        margin: 0;
        width: 65px;
        text-align: right;
        color: #6FAB24;
    }
    .form-inside-div .unordered-list ul li .listing-title {
        float: left;
        color: #66a025;
        padding-top: 2px;
    }
    .form-inside-div .recom input[type="text"] {
        width: 205px;
        padding: 15px 10px;
        border: 1px solid #C7C7C7;
        margin-right: 10px;
    }
    .form-inside-div label span {
        float: right;
        margin-right: 5px;
    }
    .drop-down-arrow {
        z-index: 99;
    }
    .form-inside-div .instructional-background {
        background-color: #eaeaea;
    }
    #about_us_ordering{
        overflow: hidden;
        cursor: pointer;
    }
    /*    .accordion-rows {
            background-color: #e6e6e6 !important;
            border: none !important;
            margin-bottom: 8px !important;
            padding: 6px;
            width: 500px;
            text-align: left;
        }*/
    h3.accordion-rows.ui-sortable-handle{
        padding: 14px;
        text-align: left;
        width: 500px;
    }

    .main_slider {
        float: left;
        width: 100%;
    }
    .main_slider .slider_wrapper {
        float: left;
        width: 100%;
        text-align: center;
        position: relative;
    }
    .main_slider .slider_wrapper_video {
        float: left;
        width: 100%;
        height: auto !important;
    }
    .main_slider .slider_wrapper {
        float: left;
        width: 100%;
        text-align: center;
        position: relative;
    }
    .main_slider .slider_wrapper .sliderCycle1 {
        float: left !important;
        width: 100% !important;
        max-width: 100% !important;
    }
    .main_slider .slider_wrapper .sliderCycle1 img {
        max-width: 100% !important;
        /*height: 640px;*/
    }
    .main_slider .slider_wrapper .sliderCycle1 .slide-images{
        float: left;
        text-align: center;
        font-size: 30px;
        font-family: ArcherPro-Bold_0;
        font-weight: normal;
        width: 100% !important;
        color: #fff;
        z-index: auto !important;
    }
    .main_slider .slider_wrapper .sliderCycle1 .slide-images .slider-text {
        margin: auto;
        width: 100%;
        margin-bottom: 75px;
        position: absolute;
        top: 37%;
    }
    .main_slider .slider_wrapper .sliderCycle1 .slide-images .slider-text a {
        color: #FFF;
        text-decoration: underline;
        font-weight: normal;
    }
    .main_slider .slider_wrapper .sliderCycle1 .slide-images .slider-text h1{
        width: 100%;
        text-align: center;
        margin-bottom: 25px;
        line-height: 1em;
        font-weight: normal;
    }
    .main_slider .slider_wrapper .sliderCycle1 .slide-images .slider-text h3{
        width: 100%;
        text-align: center;
        font-family: IdealSans-Light-Pro;
        font-size: 21px;
        font-weight: normal;
        line-height: 1em;
    }
    .main_slider .slider_wrapper .sliderCycle1 .slide-images img{
        width: 100%;
        margin: auto;
        background-size: cover;
        float: left;
    }
    .main_slider .slider_wrapper .sliderCycle1 .slide-images .slider-text .watch-video {
        width: 100%;
        text-align: center;
        float: left;
        margin-top: 50px;
        position: absolute;
        /*z-index: 500;*/
    }
    .main_slider .slider_wrapper .sliderCycle1 .slide-images .slider-text .watch-video .slider-button1 {
        /*width: 210px;*/
        margin: auto;
    }
    .main_slider .slider_wrapper .sliderCycle1 .slide-images .slider-text .watch-video .slider-button1 .video_icon {
        /*margin-top: -4px;*/
        position:absolute;
    }
    .main_slider .slider_wrapper .sliderCycle1 .slide-images .slider-text .watch-video .slider-button1.slider-listing-button .video_icon {
        /*margin-top: 19px;*/
    }
    .main_slider .slider_wrapper .sliderCycle1 .slide-images .slider-text .watch-video .slider-button1 img {
        float: left;
        background-size: initial;
        width: auto;
    }
    .main_slider .slider_wrapper .sliderCycle1 .slide-images .slider-text .watch-video .slider-button1 a {
        border: 2px solid #FFF;
        padding: 15px 25px 15px 55px;
        font-family: IdealSans-Light-Pro;
        font-weight: normal;
        text-transform: uppercase;
        color: #FFF;
        font-size: 15px;
        border-radius: 10px;
        margin-left: -50px;
        margin-top: -13px;
        text-decoration: none;
        cursor: pointer;
        background-color: rgba(0,0,0,0.5);
    }
    .main_slider .slider_wrapper .sliderCycle1 .slide-images .slider-text .watch-video .slider-button1 a.watch-full-video-listing {
        background-image: url(../images/videoicon.png);
        background-repeat: no-repeat;
        background-position: 9%;
    }
    .main_slider .slider_wrapper .sliderCycle1 .slide-images .slider-text .watch-video .slider-button1 a.view_all_photo {
        margin-left: 40px;
        padding: 15px 50px 15px 50px;
        background-color: rgba(0,0,0,0.5);
    }
    #add_main_image {
        position: absolute;
        margin-top: 90px;
        font-weight: bold;
        border: 1px dashed #6fab24;
        padding: 20px;
        width: 850px;
        margin-left: -40%;
    }
</style>

<!--JS-->
<script>
    $.ui.dialog.prototype._focusTabbable = function () {};
    function show_contact_details_form(bl_id) {
        $("#contact_details_from_" + bl_id).attr("title", "Edit Contact Details").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#contact_details_from_" + bl_id).dialog('close');
                })
            }
        });
    }

    function show_customer_listing_mapping(bl_id) {
        $("#customer_listing_mapping_from_" + bl_id).attr("title", "Edit Mapping Details").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#customer_listing_mapping_from_" + bl_id).dialog('close');
                })
            }
        });
        $('#description-listing1').ckeditor();
    }

    function show_customer_listing_main_photo(bl_id) {
        $("#customer_listing_main_photo_" + bl_id).attr("title", "Edit Main Photos / Video").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#customer_listing_main_photo_" + bl_id).dialog('close');
                })
            }
        });
    }

    function show_customer_feature_gallery(bl_id) {
        $("#customer_feature_gallery" + bl_id).attr("title", "Add Photo").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#customer_feature_gallery" + bl_id).dialog('close');
                })
            }
        });
    }

    function show_customer_hours(bl_id) {
        $("#hours_form_" + bl_id).attr("title", "Edit Hours").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#hours_form_" + bl_id).dialog('close');
                })
            }
        });
    }

    function show_customer_social_media(bl_id) {
        $("#social_media_" + bl_id).attr("title", "Edit Social Media").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#social_media_" + bl_id).dialog('close');
                })
            }
        });
    }

    function show_amenities(bl_id) {
        $("#amenities_" + bl_id).attr("title", "Edit Amenities").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#amenities_" + bl_id).dialog('close');
                })
            }
        });
    }

    function updateAMM(bl_id) {
        var checkedValues = $('input:checkbox:checked').map(function () {
            return this.value;
        }).get();
        $.post('listings-preview.php', {
            AMMid: checkedValues,
            bl_id: bl_id,
            op: 'save_amenities',
            status: 1
        }, function (done) {
            location.reload();
        });
        return false;
    }

    $(function () {
        $(".brief").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Description_PDF&field=D_PDF_Order&id=D_id';
                $.post("reorder.php", order, function (theResponse) {
                    swal("Pdf Re-ordered", "Pdf Re-ordered successfully.", "success");
                });
            }
        });
    });

    function Editpdf(pdf_id) {
        $("#pdf-update" + pdf_id).dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 500
        });
    }

    function show_file_name1(val, count) {
        $("#uploadFile_pdf").show();
        document.getElementById("uploadFile_pdf").value = val;
    }

    function show_pdf(bl_id) {
        $("#upload_pdf_" + bl_id).attr("title", "Edit PDF").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#upload_pdf_" + bl_id).dialog('close');
                })
            }
        });
    }

    function show_desc_text(bl_id) {
        $("#desc_text_" + bl_id).attr("title", "Edit Description").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#desc_text_" + bl_id).dialog('close');
                })
            }
        });
        $('#description-listing-text').ckeditor();
    }


    function show_trip_advisor(bl_id) {
        $("#trip_advisor_" + bl_id).attr("title", "Edit Trip Advisor").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#trip_advisor_" + bl_id).dialog('close');
                })
            }
        });
    }

    function show_desc_form(key) {
        $("#desc_form_" + key).attr("title", "Edit Description").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#desc_form_" + key).dialog('close');
                })
            }
        });
        $('#descriptionmain' + key).ckeditor();
    }

    function show_add_item_form(key) {
        $("#gallery-form" + key).attr("title", "Add Item").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#gallery-form" + key).dialog('close');
                })
            }
        });
    }

    function show_edit_item_form(key) {
        $("#records_" + key).attr("title", "Edit Item").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#records_" + key).dialog('close');
                })
            }
        });
        $('#descriptionNEW' + key).ckeditor();
    }

    function show_add_about_us_mainimage_form() {
        $("#main_image_form").attr("title", "Add Main Image").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#main_image_form").dialog('close');
                })
            }
        });
    }

    function show_add_about_us_page_form() {
        $("#add_page_form").attr("title", "Add Page").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#add_page_form").dialog('close');
                })
            }
        });
        $('#titleCategorytextarea').ckeditor();
    }

    function show_edit_about_us_page_form(key) {
        $("#update_about_page_" + key).attr("title", "Edit Page").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#update_about_page_" + key).dialog('close');
                })
            }
        });
    }

    function delete_about_us(bfa_id) {
        var BL_ID = $('#bidForProduct').val();
        if (confirm("Are you sure?")) {
            $.post("customer-feature-about-us-delete.php", {
                bfa_id: bfa_id,
                BL_ID: BL_ID
            }, function (done) {
                location.reload();
            });
        } else {
            return false;
        }
    }

    function show_description_about_us(bfp_id) {
        $("#gallery-description-form-" + bfp_id).attr("title", "Edit Description").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 500
        });
    }

    function show_add_daily_desc_form() {
        $("#add_daily_desc").attr("title", "Add Description").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#add_daily_desc").dialog('close');
                })
            }
        });
        $('#daily-description1').ckeditor();
    }

    function show_edit_daily_form(key) {
        $("#update_daily_" + key).attr("title", "Edit Page").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#update_daily_" + key).dialog('close');
                })
            }
        });
    }

    function show_enter_desc_form() {
        $("#add_enter_desc").attr("title", "Add Description").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#add_enter_desc").dialog('close');
                })
            }
        });
    }

    function show_add_enter_form() {
        $("#entertainment-form").attr("title", "Add Event").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#entertainment-form").dialog('close');
                })
            }
        });
    }

    function show_edit_enter_form(key) {
        $("#entertainment-form" + key).attr("title", "Edit Event").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#entertainment-form" + key).dialog('close');
                })
            }
        });
    }

    function show_add_agenda_main_image_form() {
        $("#agenda_main_image").attr("title", "Add Main Image").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#agenda_main_image").dialog('close');
                })
            }
        });
    }

    function show_add_agenda_minute_form() {
        $("#pdf-update0").attr("title", "Add Agenda & Minutes").dialog({
            modal: true,
            draggable: true,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#pdf-update0").dialog('close');
                })
            }
        });
    }

    function show_edit_agenda_minute_form(key) {
        $(".pdf-update" + key).attr("title", "Edit Agenda & Minutes").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery(".pdf-update" + key).dialog('close');
                })
            }
        });
    }

    function deleteitem_guest_book(BFGB_item_ID, BL_ID) {
        if (confirm('Are you sure you want to delete?')) {
            $.post("customer-feature-guest-book-delete.php", {
                BFGB_ID_item: BFGB_item_ID,
                BL_ID: BL_ID
            }, function (result) {
                if (result == 1) {
                    location.reload();
                }
            });
        } else
        {
            return false;
        }
    }

    function show_add_gb_desc_form() {
        $("#guest_book_form").attr("title", "Add Description").dialog({
            modal: true,
            draggable: true,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#guest_book_form").dialog('close');
                })
            }
        });
    }

    function show_region_form() {
        $("#frmRegion").attr("title", "Add Region").dialog({
            modal: true,
            draggable: true,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#frmRegion").dialog('close');
                })
            }
        });
    }

    function show_season_form() {
        $("#seasons_form").attr("title", "Add Seasons").dialog({
            modal: true,
            draggable: true,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#seasons_form").dialog('close');
                })
            }
        });
    }

    function show_bokun_form() {
        $("#bokun_form").attr("title", "Add Seasons").dialog({
            modal: true,
            draggable: true,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#bokun_form").dialog('close');
                })
            }
        });
    }

    function show_seo_form() {
        $("#seo_form").attr("title", "Add SEO").dialog({
            modal: true,
            draggable: true,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#seo_form").dialog('close');
                })
            }
        });
    }

    function get_sub_category(count_id)
    {
        var cat_id = $('#category_' + count_id).val();
        $.ajax({
            type: "GET",
            url: "get-customer-listings-subcat-new.php",
            data: {
                cat_id: cat_id
            }
        })
                .done(function (msg) {
                    $("#newCat_" + count_id).val("1");
                    $("#sub_category_childs_" + count_id).remove();
                    $("#add_new_sub_cat_" + count_id).remove();
                    $(".listings_subcategory_" + count_id).empty();
                    $(".listings_subcategory_" + count_id).html(msg);
                });
    }
    function show_form()
    {
        $("#new_main_Category").show();
    }

    function show_category_form() {
        $("#category_section").attr("title", "Add Category").dialog({
            modal: true,
            draggable: true,
            resizable: false,
            width: 700,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#category_section").dialog('close');
                })
            }
        });
    }

    function show_textbox_recom(bl_id) {
        $("#recommendation-form" + bl_id).attr("title", "Add Recommendation").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 600
        });
    }

    function show_textbox_existing_recom(rid) {
        $("#existing-recommendation-form" + rid).attr("title", "Edit Recommendation").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 600
        });
    }

    function show_customer_feature_gallery_ordering() {
        $("#gallery_ordering").attr("title", "Ordering").dialog({
            modal: true,
            draggable: true,
            resizable: false,
            width: 610,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#gallery_ordering").dialog('close');
                })
            }
        });
    }

    function show_about_us_ordering() {
        $("#about_us_ordering").attr("title", "Ordering").dialog({
            modal: true,
            draggable: true,
            resizable: false,
            width: 550,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#about_us_ordering").dialog('close');
                })
            }
        });
    }
    
    function show_text() {
//        $('#uploadFile1').show();
//        var files = document.getElementById("photo101").files;
//
//         alert(files[0].name);
//        
//                alert('hi');
//        $('input[type="file"]').change(function () {
//            var test = $(this).val(); //alert(test);
//            var imageVal = $(this)[0].files.length;alert(imageVal);
//            if (imageVal == 1) {
//                document.getElementById("uploadFile1").value = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '');
//                $('#preview').hide();
//                $('#image_bank1').val('');
//                return false;
//            }
//            document.getElementById("uploadFile1").value = "Multiple Images Selected";
//            $('#preview').hide();
//            $('#image_bank1').val('');
//            return false;
//        });

    }
    
    function show_file_name_pc(image_id, input, iteration) {
        //Get dimensions of image
       
        var id;
        if (iteration > 0) {
            id = iteration;
        } else {
            id = image_id;
        }
        // Show image preview
        $('.existing_imgs' + id).hide();
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            $('#photo' + id).hide();
            $('.preview-img-script' + id).show();
            reader.onload = function (e) {
                $('.preview-img-script' + id).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    function play_video(id, player_id) {
        PlayerReady(id, player_id);
    }
    function get_youtube_videoid(url) {
        var video_id = url.split('v=')[1];
        var ampersandPosition = video_id.indexOf('&');
        if (ampersandPosition != -1) {
            video_id = video_id.substring(0, ampersandPosition);
        }
        return video_id;
    }
    // 2. This code loads the IFrame Player API code asynchronously.
    var tag = document.createElement('script');

    tag.src = "http://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // 3. This function creates an <iframe> (and YouTube player)
    //    after the API code downloads.
    var total_players = $("#total_players").val();
    for (var i = 1; i < total_players; i++) {
        eval("var player" + i);
    }
    var current_player_id;

    function onYouTubePlayerAPIReady() {
        for (var i = 1; i < total_players; i++) {
            eval("player" + i + "= new YT.Player('player" + i + "'," + "{" +
                    "height: '538'," +
                    "width: '940'," +
                    "videoId:'" + $("#video-link-" + i).val() +
                    "',events: {'onStateChange': onPlayerStateChange}});");
        }
    }

    // 4. The API will call this function when the video player is ready.
    function PlayerReady(id, player_id) {
        var width = $(".player" + id + "_wrapper").parent(".slide-images").find('img.slider_image').width();
        var height = $(".player" + id + "_wrapper").parent(".slide-images").find('img.slider_image').height();
        if (width == null) {
            var width = 580;
            var height = 351;
        }
        $(".player" + id + "_wrapper").parent(".slide-images").find('img.slider_image').css('opacity', '0');
        $(".center #prev").css('display', 'none');
        $(".center #next").css('display', 'none');
        // $(".player" + id + "_wrapper").css('top', '0');
        $(".player" + id + "_wrapper").css('display', 'block');
        $(".player" + id + "_wrapper").css('opacity', '1');
        $(".player" + id + "_wrapper").css('z-index', '600');
        $('.sliderCycle1').cycle('pause');
        this[player_id].setSize(width, height);
        this[player_id].playVideo();
        current_player_id = id;
    }

    // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.ENDED) {
            stopVideo();
        }
    }
    function stopVideo() {
        //    this[player_id].stopVideo();
        $(".player" + current_player_id + "_wrapper").css('opacity', '0');
        $(".player" + current_player_id + "_wrapper").css('z-index', '0');
        $(".player" + current_player_id + "_wrapper").parent(".slide-images").find('img.slider_image').css('opacity', '1');
        if (total_players > 2) {
            $(".center #prev").css('display', 'block');
            $(".center #next").css('display', 'block');
        }
        $('.sliderCycle1').cycle('resume');
    }



</script>
<?PHP
require_once '../include/admin/footer.php';
?>