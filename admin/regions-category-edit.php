<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

$limits = explode(',', $_SESSION['USER_LIMIT']);
if (!in_array('regions', $_SESSION['USER_PERMISSIONS']) && (!in_array('manage-county-region', $_SESSION['USER_PERMISSIONS']) || !in_array($_REQUEST['rid'], $limits))) {
    header("Location: /admin/");
    exit();
}

if (isset($_REQUEST['rid']) && $_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} else {
    header('location: regions.php');
    exit();
}
$feature_category = $_REQUEST['feature_category'];
$sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);


if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $activeID = $_REQUEST['id'];
} else {
    header('location: regions.php');
    exit();
}

$sql = "SELECT C_ID, C_Parent, C_Name, RC_Name, RC_Link, RC_Image, RC_R_ID, RC_C_ID, RC_Image_Icon, RC_Map_Icon, RC_Slider_Title, RC_Slider_Des, RC_Alt, 
        RC_Video, RC_SEO_Title, RC_Category_Page_Title, RC_At_A_Glance_Text, RC_SEO_Keywords, RC_SEO_Description, RC_Description, RC_Mobile_Description FROM tbl_Category 
        LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = " . $activeRegion['R_ID'] . " 
        LEFT JOIN tbl_Business ON RC_Spokesperson = B_ID 
        WHERE C_ID = '" . encode_strings($activeID, $db) . "' ORDER BY C_Name LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeCat = mysql_fetch_assoc($result);
if ($activeCat['C_Parent'] == 0) {
    $id = $activeCat['C_ID'];
} else {
    $id = $activeCat['C_Parent'];
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $sql = "UPDATE tbl_Region_Category SET ";

    require '../include/picUpload.inc.php';
    if ($_POST['image_bank_desktop'] == "") {
        // last @param 4 = Region Category Main Image
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 4, 'Category', $id);
        if (is_array($pic)) {
            $sql .= "RC_Image = '" . encode_strings($pic['0']['0'], $db) . "', 
                    RC_Thumbnail = '" . encode_strings($pic['0']['1'], $db) . "',
                    RC_Mobile_Thumbnail = '" . encode_strings($pic['0']['2'], $db) . "', ";
        }
        $pic_id = $pic['1'];
    } else {
        $pic_id = $_POST['image_bank_desktop'];
        // last @param 4 = Region Category Main Image
        $pic_response = Upload_Pic_Library($pic_id, 4);
        if ($pic_response) {
            $sql .= "RC_Image = '" . encode_strings($pic_response['0'], $db) . "',
                    RC_Thumbnail = '" . encode_strings($pic_response['1'], $db) . "',
                    RC_Mobile_Thumbnail = '" . encode_strings($pic_response['2'], $db) . "',";
        }
    }

    $mobile_map = Upload_Pic_Normal('0', 'cat_icon', 0, 0, true, IMG_LOC_ABS, 10000000, true);
    if ($mobile_map) {
        $sql .= "RC_Image_Icon = '" . encode_strings($mobile_map, $db) . "', ";
        if ($Region_theme['RC_Image_Icon']) {
            Delete_Pic(IMG_LOC_ABS . $Region_theme['RC_Image_Icon']);
        }
    }

    $mobile_map_icon = Upload_Pic_Normal('0', 'cat_map_icon', 0, 0, true, IMG_ICON_MAP_ABS, 10000000, false, $_REQUEST['rid'] . "-" . $_REQUEST['id']);
    if ($mobile_map_icon) {
        $sql .= "RC_Map_Icon = '" . encode_strings($mobile_map_icon, $db) . "', ";
        if ($Region_theme['RC_Map_Icon']) {
            Delete_Pic(IMG_ICON_MAP_ABS . $Region_theme['RC_Map_Icon']);
        }
    }

    if (isset($_REQUEST['townAdmin']) && $_REQUEST['townAdmin'] == 1) {
        $sql .= "RC_Description  = '" . encode_strings(substr(str_replace('<p>', '', str_replace('</p>', '', $_REQUEST['description'])), 0), $db) . "',
                RC_Mobile_Description  = '" . encode_strings(substr(str_replace('<p>', '', str_replace('</p>', '', $_REQUEST['mobile_description'])), 0), $db) . "'
                WHERE RC_R_ID = '" . encode_strings($activeRegion['R_ID'], $db) . "' AND
                RC_C_ID = '" . encode_strings($activeID, $db) . "'";
    } else {
        if (strlen($_REQUEST['rvideo']) > 5) {
            $video_url = $_REQUEST['rvideo'];
        } else {
            $video_url = '';
        }
        $sql .= " RC_Video = '" . encode_strings($video_url, $db) . "',
                  RC_Name  = '" . encode_strings($_REQUEST['rname'], $db) . "',
                  RC_Link  = '" . encode_strings($_REQUEST['rlink'], $db) . "',
                  RC_Alt  = '" . encode_strings($_REQUEST['ralt'], $db) . "',
                  RC_Slider_Title  = '" . encode_strings($_REQUEST['sliderTitle'], $db) . "',
                  RC_Slider_Des  = '" . encode_strings($_REQUEST['slider_description'], $db) . "',
                  RC_SEO_Title  = '" . encode_strings($_REQUEST['seoTitle'], $db) . "',
                  RC_SEO_Keywords  = '" . encode_strings($_REQUEST['seoKeywords'], $db) . "',
                  RC_SEO_Description  = '" . encode_strings($_REQUEST['seoDescription'], $db) . "',
                  RC_Description  = '" . encode_strings(substr($_REQUEST['description'], 0), $db) . "',
                  RC_Mobile_Description  = '" . encode_strings(substr($_REQUEST['mobile_description'], 0), $db) . "',
                  RC_Category_Page_Title  = '" . encode_strings(substr($_REQUEST['category_page_title'], 0), $db) . "',
                  RC_At_A_Glance_Text  = '" . encode_strings(substr($_REQUEST['at_a_glance_text'], 0), $db) . "'
                  WHERE RC_R_ID = '" . encode_strings($activeRegion['R_ID'], $db) . "' AND
                  RC_C_ID = '" . encode_strings($activeID, $db) . "'";
    }

    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $regionID, 'Region Category Edit', $activeID, 'Update Category', 'super admin');
        //Image usage from image bank.
        if ($pic_id > 0) {
            imageBankUsage($pic_id, 'IBU_R_ID', $regionID, 'IBU_Region_Category', $activeID);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    header('location: regions-category-edit.php?rid=' . $_REQUEST['rid'] . '&id=' . $_REQUEST['id']);
    exit();
}
if (isset($_GET['op']) && $_GET['op'] == 'del') {
    $update = "UPDATE tbl_Region_Category SET ";
    if ($_GET['flag'] == 'm_photo') {
        $update .= " RC_Image = '', RC_Thumbnail='', RC_Mobile_Thumbnail=''";
    }
    if ($_GET['flag'] == 'icon') {
        $update .= " RC_Image_Icon= '' ";
    }
    if ($_GET['flag'] == 'map_icon') {
        $update .= " RC_Map_Icon= '' ";
    }
    $update .= " WHERE RC_R_ID = '" . encode_strings($_REQUEST['rid'], $db) . "' AND RC_C_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $regionID, 'Region Category Edit', $activeID, 'Delete ' . $_GET['flag'], 'super admin');
        if ($_GET['flag'] == 'm_photo') {
            $sql = "SELECT * FROM tbl_Image_Bank_Usage WHERE IBU_R_ID = '$regionID' AND IBU_Region_Category = '$activeID'";
            $res = mysql_query($sql);
            $rowIB = mysql_num_rows($res);
            if ($rowIB == '1') {
                //Delete from image usage when image is deleted
                imageBankUsageDelete('IBU_R_ID', $regionID, 'IBU_Region_Category', $activeID);
            } else {
                //Delete from image usage when image is deleted
                imageBankUsageDelete('IBU_R_ID', $regionID, 'IBU_Region_Category_C_ID', $activeID);
            }
        }
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header('location: regions-category-edit.php?rid=' . $_REQUEST['rid'] . '&id=' . $_REQUEST['id']);
    exit;
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'feature_save') {
    $feature_category = $_REQUEST['feature_category'];
    $feature_story = $_REQUEST['feature_story'];
    if ($feature_story != '') {
        $sqlMax = "SELECT MAX(SHC_Order) FROM tbl_Story_Homepage_Category WHERE SHC_R_ID = '" . $regionID . "' AND SHC_Category = '" . $activeID . "' AND SHC_Feature = 1";
        $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
        $rowMax = mysql_fetch_row($resultMax);
        $sql = " INSERT tbl_Story_Homepage_Category SET SHC_S_ID = '" . $feature_story . "', SHC_R_ID = '" . $regionID . "', SHC_Category = '" . $activeID . "', SHC_Feature = 1, SHC_Order = '" . ($rowMax[0] + 1) . "'";
        $result = mysql_query($sql) or die(mysql_error());
        $_SESSION['story_success'] = 1;
        header("Location: /admin/regions-category-edit.php?rid=" . $regionID . "&id=" . $activeID);
        exit();
    }
}
if (isset($_REQUEST['del']) && $_REQUEST['del'] == 'true') {
    $sql = " DELETE FROM tbl_Story_Homepage_Category WHERE SHC_ID = '" . $_REQUEST['shc_id'] . "'";
    $result = mysql_query($sql) or die(mysql_error());
    $_SESSION['story_delete'] = 1;
    header("Location: /admin/regions-category-edit.php?rid=" . $regionID . "&id=" . $activeID);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Manage Categories - <?php echo $activeCat['C_Name'] ?></div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <form id="regionForm" name="form1" method="post" enctype="multipart/form-data" action="">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="id" value="<?php echo $activeCat['C_ID'] ?>">
            <input type="hidden" name="rid" value="<?php echo $activeRegion['R_ID'] ?>">
            <?php echo (in_array('townTourist', $_SESSION['USER_PERMISSIONS'])) ? '<input type="hidden" name="townAdmin" value="1" />' : '' ?>
            <div class="content-header">Region Category Edit</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Category</label>
                <div class="form-data">
                    <?php if ($activeCat['RC_Name'] != '') { ?>
                        <input name="rname" type="text" id="textfield2" value="<?php echo $activeCat['RC_Name'] ?>" size="50" />
                    <?php } else { ?>
                        <input name="rname" type="text" id="textfield2" value="<?php echo $activeCat['C_Name'] ?>" size="50" />
                    <?php } ?>
                </div>
            </div>
            <?php if ($activeCat['C_Parent'] == 0) { ?>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Link</label>
                    <div class="form-data">
                        <input name="rlink" type="text" value="<?php echo $activeCat['RC_Link'] ?>" size="50" />
                    </div>
                </div>
            <?php } ?>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Region</label>
                <div class="form-data">
                    <?php echo $activeRegion['R_Name'] ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Primary Photo</label>
                <div class="form-data div_image_library cat-region-width">
                    <?php if ($activeCat['RC_Image'] != '') { ?>
                        <a class="deletePhoto delete-region-cat-photo" onClick="return confirm('Are you sure you want to delete photo?');" href="regions-category-edit.php?rid=<?php echo $activeCat['RC_R_ID'] ?>&id=<?php echo $activeCat['RC_C_ID'] ?>&op=del&flag=m_photo">Delete Photo</a>
                    <?php } ?>
                    <div class="cropit-image-preview aboutus-photo-perview">
                        <img class="preview-img preview-img-script4" style="display: none;" src="">    
                        <?php if ($activeCat['RC_Image'] != '') { ?>
                            <img class="existing-img existing_imgs4" src="<?php echo IMG_LOC_REL . $activeCat['RC_Image'] ?>" >
                        <?php } ?>
                    </div>
                    <span class="daily_browse" onclick="show_image_library(4)">Select File</span>
                    <input type="hidden" name="image_bank_desktop" id="image_bank4" value="">
                    <input type="file" onchange="show_file_name(4, this)" name="pic[]" id="photo4" style="display: none;">
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Icon</label>
                <div class="form-data div_image_library cat-region-width">
                    <?php if ($activeCat['RC_Image_Icon'] != '') { ?>
                        <a class="deletePhoto delete-region-cat-photo" onClick="return confirm('Are you sure you want to delete photo?');" href="regions-category-edit.php?rid=<?php echo $activeCat['RC_R_ID'] ?>&id=<?php echo $activeCat['RC_C_ID'] ?>&op=del&flag=icon">Delete Photo</a>
                    <?php } ?>
                    <div class="cropit-image-preview aboutus-photo-perview">
                        <img class="preview-img preview-img-script0" style="display: none;" src="">    
                        <?php if ($activeCat['RC_Image_Icon'] != '') { ?>
                            <img class="existing-img existing_imgs0" src="<?php echo IMG_LOC_REL . $activeCat['RC_Image_Icon'] ?>" >
                        <?php } ?>
                    </div>
                    <label for="photo0" class="daily_browse with-no-library" style="margin-top: 5px;">Browse</label>
                    <input type="file" onchange="show_file_name(0, this)" name="cat_icon[]" id="photo0" style="display: none;">
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Map Icon</label>
                <div class="form-data div_image_library cat-region-width">
                    <?php if ($activeCat['RC_Map_Icon'] != '') { ?>
                        <a class="deletePhoto delete-region-cat-photo" onClick="return confirm('Are you sure you want to delete photo?');" href="regions-category-edit.php?rid=<?php echo $activeCat['RC_R_ID'] ?>&id=<?php echo $activeCat['RC_C_ID'] ?>&op=del&flag=map_icon">Delete Photo</a>
                    <?php } ?>
                    <div class="cropit-image-preview aboutus-photo-perview">
                        <img class="preview-img preview-img-script1" style="display: none;" src="">    
                        <?php if ($activeCat['RC_Map_Icon'] != '') { ?>
                            <img class="existing-img existing_imgs1" src="<?php echo IMG_ICON_MAP_REL . $activeCat['RC_Map_Icon'] ?>" >
                        <?php } ?>
                    </div>
                    <label for="photo1" class="daily_browse with-no-library" style="margin-top: 5px;">Browse</label>
                    <input type="file" onchange="show_file_name(1, this)" name="cat_map_icon[]" id="photo1" style="display: none;">
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Slider Title</label>
                <div class="form-data">
                    <input name="sliderTitle" type="text"  value="<?php echo $activeCat['RC_Slider_Title'] ?>" size="50" maxlength="255"/> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Slider Description</label>
                <div class="form-data"> 
                    <textarea name="slider_description" cols="85" rows="10" class="tt-description"><?php echo $activeCat['RC_Slider_Des'] ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Alt Tag</label>
                <div class="form-data">
                    <input name="ralt" type="text" id="textfield2" value="<?php echo $activeCat['RC_Alt'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Category Page Title</label>
                <div class="form-data">
                    <input name="category_page_title" type="text" value="<?php echo $activeCat['RC_Category_Page_Title'] ?>" size="50" />
                </div>
            </div>
            <?php if ($activeCat['C_Parent'] != 0) { ?>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Text Above Map</label>
                    <div class="form-data">
                        <input name="at_a_glance_text" type="text" value="<?php echo $activeCat['RC_At_A_Glance_Text'] ?>" size="50" />
                    </div>
                </div>
            <?php } ?>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Video</label>
                <div class="form-data">
                    <input type="text" name="rvideo" value="<?php echo $activeCat['RC_Video'] ?>">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>SEO Title</label>
                <div class="form-data">
                    <input name="seoTitle" type="text" id="seoTitle" value="<?php echo $activeCat['RC_SEO_Title'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>SEO Keywords</label>
                <div class="form-data">
                    <input name="seoKeywords" type="text" id="textfield2" value="<?php echo $activeCat['RC_SEO_Keywords'] ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>SEO Description</label>
                <div class="form-data">
                    <textarea name="seoDescription" cols="50" wrap="VIRTUAL" id="textfield2"><?php echo $activeCat['RC_SEO_Description'] ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Description</label>
                <div class="form-data">
                    <textarea name="description" cols="85" rows="10" class="tt-description"><?php echo $activeCat['RC_Description'] ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Mobile Description</label>
                <div class="form-data">
                    <textarea name="mobile_description" cols="85" rows="10" class="tt-description"><?php echo $activeCat['RC_Mobile_Description'] ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin border-none"> 
                <div class="button">
                    <input type="submit" name="button2" id="button22" value="Submit" />
                </div>
            </div>
        </form>

        <!--Featured Stories Section-->
        <div class="content-header" id="section_stories">
            Feature Stories
        </div>
        <form name="form1" method="GET" id="feature_cat_form" action="/admin/regions-category-edit.php#scroll">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <input type="hidden" name="id" value="<?php echo $activeID ?>">
            <input type="hidden" name="type" value="feature_cat_story">
            <input type="hidden" name="op" value="save">
            <div class="content-sub-header link-header region-header-padding">
                <div class="data-column spl-org-listngs padding-org-listing">
                    <select name="category" id="feature_category" class="stories-cat" onchange="filterStories('feature_cat_form');" required>
                        <option value="">Select Category</option>
                        <?PHP
                        $sql = "SELECT C_ID FROM tbl_Region_Category 
                                LEFT JOIN tbl_Category ON C_ID = RC_C_ID
                                WHERE C_Parent = 0 AND C_Is_Blog = 1 AND RC_R_ID = '" . encode_strings($regionID, $db) . "'";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_assoc($result)) {
                            $sql1 = "SELECT C_ID, C_Name, RC_Name FROM tbl_Region_Category
                                    LEFT JOIN tbl_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($regionID, $db) . "'
                                    WHERE C_Parent = '" . $row['C_ID'] . "' AND RC_Status=0 ORDER BY RC_Name ASC";
                            $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
                            while ($row1 = mysql_fetch_assoc($result1)) {
                                ?>
                                <option value="<?php echo $row1['C_ID'] ?>" <?php echo ($row1['C_ID'] == $feature_category) ? 'selected' : '' ?>><?php echo $row1['C_Name'] ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                    <select name="story" id="feature_cat_story" class="stories" required>
                        <option value="">Select Story</option>
                        <?PHP
                        if ($feature_category > 0) {
                            $sql = "SELECT S_ID, S_Title FROM tbl_Story WHERE S_Category = '" . encode_strings($feature_category, $db) . "' AND S_Active = 1 
                                    AND S_ID NOT IN (SELECT SHC_S_ID FROM tbl_Story_Homepage_Category WHERE SHC_R_ID = '" . encode_strings($regionID, $db) . "' AND SHC_Category = ' " . $activeID . " ' AND SHC_Feature = 1) ORDER BY S_Title";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($row = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo $row['S_ID'] ?>"><?php echo $row['S_Title'] ?></option>
                                <?PHP
                            }
                        }
                        ?>
                    </select>
                    <input type="submit" value="+Add" class="stories-add-button" onclick="return saveFeatureStory('feature_cat_form');" />
                </div>
            </div>
        </form>

        <div class="reorder-category feature_cat_story">
            <?php
            $sql = "SELECT SHC_ID, S_Title FROM  tbl_Story 
                    LEFT JOIN tbl_Story_Homepage_Category ON S_ID = SHC_S_ID  
                    WHERE SHC_R_ID = '" . encode_strings($regionID, $db) . "' AND SHC_Category = ' " . $activeID . " ' AND S_Active = 1 AND SHC_Feature = 1 ORDER BY SHC_Order ASC";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                ?>
                <div class="data-content" id="recordsArray_<?php echo $row['SHC_ID'] ?>">
                    <div class="data-column spl-org-listngs ">
                        <div class="story-title"><?php echo $row['S_Title'] ?></div>
                        <div class="remove-link"><a href="regions-category-edit.php?rid=<?php echo $regionID; ?>&id=<?php echo $activeID; ?>&shc_id=<?php echo $row['SHC_ID']; ?>&del=true#scroll" onclick="return confirm('Are you sure you want to perform this action?');">Remove</a></div>
                    </div>
                </div>
            <?php } ?>
        </div>

        <?php if (in_array('regions', $_SESSION['USER_PERMISSIONS'])) { ?>
            <div class="content-header margin-top-agenda">Stats</div>
            <div class="content-sub-header">
                <div class="data-column cust-listing-title padding-none">Year</div>
                <div class="data-column cust-listing-other padding-none">Month</div>
                <div class="data-column cust-listing-other padding-none">Display</div>
                <div class="data-column cust-listing-other padding-none">Video</div>
            </div>
            <?PHP
            $sql = "SELECT RCS_Display, RCS_Video, DATE_FORMAT(RCS_Month,'%Y') as daYear, DATE_FORMAT(RCS_Month,'%M') as daMonth 
                    FROM tbl_Region_Category_Stats  
                    WHERE RCS_R_ID = '" . encode_strings($activeRegion['R_ID'], $db) . "' AND RCS_C_ID = '" . $activeCat['C_ID'] . "' 
                    ORDER BY RCS_Month DESC";
            $resultCount = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($bizCount = mysql_fetch_assoc($resultCount)) {
                ?>
                <div class="data-content">
                    <div class="data-column cust-listing-title"><?php echo $bizCount['daYear'] ?></div>
                    <div class="data-column cust-listing-other"><?php echo $bizCount['daMonth'] ?></div>
                    <div class="data-column cust-listing-other"><?php echo $bizCount['RCS_Display'] ?></div>
                    <div class="data-column cust-listing-other"><?php echo $bizCount['RCS_Video'] ?></div>
                </div>
                <?PHP
            }
        }
        ?>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>
<div id="dialog-message"></div>
<script>
    $(function () {
        var myhash = window.location.hash.substring(1);
        if (myhash != '')
        {
            $('html, body').animate({
                scrollTop: $('#section_stories').offset().top
            }, 2000);
            return false;
        }

        $(".reorder-category").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Story_Homepage_Category&field=SHC_Order&id=SHC_ID';
                $.post("reorder.php", order, function (theResponse) {
                    swal("Stories Re-ordered!", "Stories Re-ordered successfully.", "success");
                });
            }
        });
    });
    //filter listing
    function filterStories(form) {
        var formdata = $('#' + form).serialize();
        $.ajax({
            type: "POST",
            url: 'filterStories.php',
            dataType: 'json',
            data: formdata,
            success: function (response) {
                var type = response.type;
                if (response.stories && response.stories !== '') {
                    $('#' + type).html(response.stories);
                }
            }
        });
    }
    //save listing against route
    function saveFeatureStory(form) {
        var formdata = $('#' + form).serialize();
        $.ajax({
            type: "POST",
            url: 'saveFeatureStory.php',
            dataType: 'json',
            data: formdata,
            success: function (response) {
                filterStories(form);
                if (response.success && response.success == 1) {
                    swal("Data Saved", "Data has been saved successfully.", "success");
                }
                if (response.error && response.error == 1) {
                    swal("Error", "Error saving data. Please try again.", "error");
                }
                if (response.stories && response.stories !== '') {
                    $('.reorder-category.' + response.type).html(response.stories);
                }

                //clear selection of dropdowns
                $('#feature_cat_story').val('');
            }
        });
        return false;
    }
</script>
<?PHP
require_once '../include/admin/footer.php';
?>