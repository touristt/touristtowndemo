<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} else {
    header("Location: /admin/regions.php");
    exit();
}

$sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);

if ($_POST['op'] == 'save') {
    $sql = "tbl_Region_BG SET 
            RBG_Name = '" . encode_strings($_REQUEST['name'], $db) . "', 
            RBG_R_ID = '" . encode_strings($regionID, $db) . "'";

    require_once '../include/picUpload.inc.php';
    $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS);
    if ($pic) {
        $sql .= ", RBG_File = '" . encode_strings($pic, $db) . "'";
    }

    $sql = "INSERT " . $sql;
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
    }else{
        $_SESSION['error'] = 1;
    }

    header("Location: region-bg.php?rid=" . $regionID);
    exit();
} elseif ($_GET['op'] == 'del') {
    require_once '../include/picUpload.inc.php';

    $sqlTMP = "SELECT RBG_File FROM tbl_Region_BG WHERE RBG_ID = '" . encode_strings($_REQUEST['bgid'], $db) . "'";
    $result = mysql_query($sqlTMP, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);

    if ($row['RBG_File']) {
        Delete_Pic(IMG_LOC_ABS . $row['RBG_File']);
    }
    $sql = "DELETE tbl_Region_BG
            FROM tbl_Region_BG  
            WHERE RBG_ID = '" . $_REQUEST['bgid'] . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
    }else{
        $_SESSION['delete_error'] = 1;
    }
    header("Location: region-bg.php?rid=" . $regionID);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo  $activeRegion['R_Name'] ?> - Site Backgrounds</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php';?>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name">Name</div>
            <div class="data-column padding-none spl-other">View</div>
            <div class="data-column padding-none spl-other">Delete </div>
        </div>
        <?PHP
        $sql = "SELECT * FROM tbl_Region_BG WHERE RBG_R_ID = '" . encode_strings($regionID, $db) . "' ORDER BY RBG_Name";
        $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($resultTMP)) {
            ?>
            <div class="data-content">
                <div class="data-column spl-name"><?php echo  $row['RBG_Name'] ?></div>
                <div class="data-column spl-other"><a href="<?php echo  IMG_LOC_REL . $row['RBG_File'] ?>" target="_blank">View</a></div>
                <div class="data-column spl-other"><a href="region-bg.php?op=del&amp;rid=<?php echo  $regionID ?>&amp;bgid=<?php echo  $row['RBG_ID'] ?>" onClick="return confirm('Are you sure?');">Delete</a></div>
            </div>
            <?PHP
        }
        ?>

        <form action="/admin/region-bg.php" method="post" enctype="multipart/form-data" name="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="rid" value="<?php echo  $regionID ?>">
            <div class="content-header">Add Background</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Name</label>
                <div class="form-data">
                    <input name="name" type="text" id="textfield2" value="" size="50" required/> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Homepage Photo</label>
                <div class="form-data">
                    <span class="inputWrapper">&nbsp;&nbsp;Browse 
                        <input class="fileInput" type="file" name="pic[]" id="photo-<?php echo  $row['C_ID'] ?>"/>
                    </span>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="button2" id="button22" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
