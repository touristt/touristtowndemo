<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('business-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
}

$BID = $_REQUEST['id'];
require_once '../include/admin/header.php';
?>

<div class="content-left advert">
    <?php require_once '../include/nav-B-advertisement-credit-admin.php'; ?>
    <div class="title-link">
        <div class="title">Manage Campaigning</div>
        <div class="link">
        </div> 
        <div class="title float-right-text text-margin">

        </div>
    </div>
    <div class="right">
        <div class="content-header">
            <div class="title">Manage Campaign Library</div>
            <div class="link">

            </div>
        </div>
        <div class="menu-items-accodings margin-top-12">                         
            <div id="accordion">
                <?php
                $sql_advert_type = "SELECT AT_ID, AT_Name FROM tbl_Advertisement_Type WHERE AT_ID NOT IN(3,4)";
                $result_advert_type = mysql_query($sql_advert_type, $db) or die("Invalid query: $sql_advert_type -- " . mysql_error());
                while ($row_advert_type = mysql_fetch_assoc($result_advert_type)) {
                    $sql_advertisements = " SELECT A_Title, A_Approved_Logo FROM tbl_Advertisement WHERE A_B_ID = '" . encode_strings($BID, $db) . "' 
                                            AND A_AT_ID='" . encode_strings($row_advert_type['AT_ID'], $db) . "' AND A_Is_Deleted = 0 AND A_Status = 3 AND (A_End_Date >= CURDATE() OR A_End_Date = 0000-00-00)";
                    $result_advertisements = mysql_query($sql_advertisements, $db) or die("Invalid query: $sql_advertisements -- " . mysql_error());
                    $countAds = mysql_num_rows($result_advertisements);
                    $count = 0;
                    while ($rowAdvert = mysql_fetch_assoc($result_advertisements)) {
                        if ($count == 0) {
                            ?>
                            <h3 class="accordion-rows"><?php echo $row_advert_type['AT_Name']; ?></h3> 
                            <div class="sub-accordions accordion-padding">
                            <?php } ?>

                            <div class="form-inside-div form-inside-div-adv2">
                                <label><?php echo $rowAdvert['A_Title']; ?></label>
                                <div class="form-data image-align">
                                    <div class="approved-photo-div">
                                        <img src="http://<?php echo DOMAIN . "/" . IMG_LOC_REL . "/" . $rowAdvert['A_Approved_Logo'] ?>">          
                                    </div>
                                </div>
                            </div>

                            <?php
                            $count++;
                            if ($count == $countAds) {
                                ?>
                            </div>
                            <?php
                        }
                    }
                    ?> 
                    <?php
                }
                ?>
            </div> 
        </div>
    </div>
</div>    
<script>
    $(function () {
        $("#accordion").accordion({
            collapsible: true,
            heightStyle: 'content',
            active: false
        });
        $('.add-item-accordion > a').click(function (event) {
            if ($(this).parent().parent().hasClass("ui-accordion-header-active")) {
                event.stopPropagation(); // this is
            }
            event.preventDefault(); // the magic
        });
    });
</script>
<?PHP
require_once '../include/admin/footer.php';
?>