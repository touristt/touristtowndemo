<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/adminFunctions.inc.php';

if (!in_array('superadmin', $_SESSION['USER_ROLES'])) {
    header("Location: /admin/");
    exit();
}
$region = '';
$month = '';
$year = '';
$region_id = '';
$month_id = '';
$year_id = '';
if (isset($_REQUEST['sortregion']) && $_REQUEST['sortregion'] != '') {
    $region_id = $_REQUEST['sortregion'];
    $region = 'AND R_ID  =' . $region_id;
}
if (isset($_REQUEST['month']) && $_REQUEST['month'] != '') {
    $month_id = $_REQUEST['month'];
    $month = 'AND MONTH(AS_Date)   =' . $month_id;
}
if (isset($_REQUEST['year']) && $_REQUEST['year'] != '') {
    $year_id = $_REQUEST['year'];
    $year = 'AND  YEAR(AS_Date)   =' . $year_id;
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Campaigns</div>
        <div class="link">

        </div>
    </div>
    <div class="left advert-left-nav">
        <?php require_once '../include/nav-B-advertisement-admin.php'; ?>
    </div>
    <div class="right">
        <div class="content-header content-header-search">
            <span>Active Campaigns Reports</span>
            <form class="export_form_width" method="post" name="export_form" action="export_active_stats.php">
                <input type="hidden" name="month_filter" value="<?php echo $_REQUEST['month']; ?>">
                <input type="hidden" name="year_filter" value="<?php echo $_REQUEST['year'] ?>">
                <input type="hidden" name="region_filter" value="<?php echo $_REQUEST['sortregion']; ?>">
                <input type="submit" name="export_ad_stats" value="Export"/>
            </form>
        </div>
        <div class="link-header">
            <form id="frmSort" name="form1" method="GET" action="active-advertisement-reports.php">
                <label class="margin-left-label-select">
                    <?php
                    $sql = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Domain != ''";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    ?>
                    <select name="sortregion" id="sortregion" onChange="getOptions()">
                        <option value="">Select Region</option>
                        <?php
                        while ($rowRegion = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $rowRegion['R_ID'] ?>" <?php echo ($rowRegion['R_ID'] == $region_id) ? 'selected' : ''; ?>><?php echo $rowRegion['R_Name'] ?></option>
                        <?php }
                        ?>
                    </select>
                </label>

                <label class="margin-left-label-select">
                    <select name="month" onChange="getOptions()">
                        <option value="">Select Month</option>
                        <option value="01" <?php echo ($month_id == 1) ? "selected" : "" ?>>January</option>
                        <option value="02" <?php echo ($month_id == 2) ? "selected" : "" ?>>February</option>
                        <option value="03" <?php echo ($month_id == 3) ? "selected" : "" ?>>March</option>
                        <option value="04" <?php echo ($month_id == 4) ? "selected" : "" ?>>April</option>
                        <option value="05" <?php echo ($month_id == 5) ? "selected" : "" ?>>May</option>
                        <option value="06" <?php echo ($month_id == 6) ? "selected" : "" ?>>June</option>
                        <option value="07" <?php echo ($month_id == 7) ? "selected" : "" ?>>July</option>
                        <option value="08" <?php echo ($month_id == 8) ? "selected" : "" ?>>August</option>
                        <option value="09" <?php echo ($month_id == 9) ? "selected" : "" ?>>September</option>
                        <option value="10" <?php echo ($month_id == 10) ? "selected" : "" ?>>October</option>
                        <option value="11" <?php echo ($month_id == 11) ? "selected" : "" ?>>November</option>
                        <option value="12" <?php echo ($month_id == 12) ? "selected" : "" ?>>December</option>
                    </select>
                </label>
                <?php
                $year_current = date('Y');
                ?>
                <label class="margin-left-label-select">
                    <select name="year" onChange="getOptions()">
                        <option value="">Select Year</option>
                        <?php
                        for ($start_year = 2014; $start_year <= $year_current; $start_year++) {
                             ?>
                            <option value="<?php echo $start_year ?>" <?php echo ($year_id == $start_year) ? "selected" : "" ?>><?php echo $start_year ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </label>
                <script type="text/javascript">
                    function getOptions() {
                        $('#frmSort').submit();
                    }
                </script>
            </form>
        </div>
        <div class="content-sub-header padding-none">
            <div class="data-column aar-cat padding-none">Category</div>
            <?php
            $sql = "SELECT AT_ID, AT_Name FROM tbl_Advertisement_Type WHERE AT_ID NOT IN(3,4)";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $count = 0;
            while ($rowAT = mysql_fetch_assoc($result)) {
                $count++;
                $AT_ID[$count] = $rowAT['AT_ID'];
                ?>
                <div class="data-column aar-other statistics">
                    <div class="title"><?php echo $rowAT['AT_Name'] ?></div>
                    <div class="impressions">Imp</div>
                    <div class="clicks">Clicks</div>
                </div>
                <?php
            }
            ?>
            <div class="data-column aar-other statistics">
                <div class="title">Total</div>
                <div class="impressions">Imp</div>
                <div class="clicks">Clicks</div>
            </div>
        </div>
        <?php
        $totalImpressions = 0;
        $totalClicks = 0;
        $totalImpressionsTypeBased = '';
        $totalClicksTypeBased = '';
        $sql = "SELECT C_ID, RC_Name, C_Name FROM tbl_Category 
                LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID 
                LEFT JOIN tbl_Region ON RC_R_ID = R_ID
                WHERE C_Parent = 0 $region GROUP BY C_ID ORDER BY RC_Name ASC";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($rowCat = mysql_fetch_assoc($result)) {
            ?>
            <div class="data-content">
                <div class="data-column aar-cat"><?php echo ($rowCat['RC_Name'] != '') ? $rowCat['RC_Name'] : $rowCat['C_Name'] ?></div>
                <?php
                $totalImpressionsCatBased = '';
                $totalClicksCatBased = '';
                for ($i = 1; $i <= $count; $i++) {
                    if ($AT_ID[$i] == 4) {
                        $record = show_ads_report($region_id, 0, $AT_ID[$i], $month_id, $year_id);
                    } else {
                        $record = show_ads_report($region_id, $rowCat['C_ID'], $AT_ID[$i], $month_id, $year_id);
                        $totalImpressionsCatBased += $record['impressions'];
                        $totalClicksCatBased += $record['clicks'];
                    }
                    $totalImpressionsTypeBased[$i] += $record['impressions'];
                    $totalClicksTypeBased[$i] += $record['clicks'];
                    ?>

                    <div class="data-column aar-other statistics">
                        <?php
                        if ($record['impressions'] != '') {
                            ?>
                            <div class="impressions"><?php echo $record['impressions'] ?></div>
                        <?php } else { ?>
                            <div class="impressions">0</div>
                            <?php
                        }
                        if ($record['clicks'] != '') {
                            ?>
                            <div class="clicks"><?php echo $record['clicks'] ?></div>
                        <?php } else { ?>
                            <div class="clicks">0</div>
                            <?php
                        }
                        ?>
                    </div>
                <?php } ?>

                <div class="data-column aar-other statistics">
                    <div class="impressions"><?php echo $totalImpressionsCatBased ?></div>
                    <div class="clicks"><?php echo $totalClicksCatBased ?></div>
                </div>
            </div>
        <?php }
        ?>
        <div class="data-content aas-total">
            <div class="data-column aar-cat">
                <?php
                echo "Totals";
                ?>
            </div>
            <?php
            for ($j = 1; $j <= $count; $j++) {
                $totalImpressions += $totalImpressionsTypeBased[$j];
                $totalClicks += $totalClicksTypeBased[$j];
                ?>
                <div class="data-column aar-other statistics">
                    <div class="impressions"><?php echo $totalImpressionsTypeBased[$j] ?></div>
                    <div class="clicks"><?php echo $totalClicksTypeBased[$j] ?></div>
                </div>
            <?php } ?>
            <div class="data-column aar-other statistics">
                <div class="impressions"><?php echo $totalImpressions ?></div>
                <div class="clicks"><?php echo $totalClicks ?></div>
            </div>
        </div>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>