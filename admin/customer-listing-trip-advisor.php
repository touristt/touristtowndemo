<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('business-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
}

$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words, BL_Trip_Advisor FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
} else {
    header("Location:customers.php");
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {

    $sql = "tbl_Business_Listing SET BL_Trip_Advisor = '" . encode_strings($_POST['tripadvisor'], $db) . "'";
    if ($BL_ID > 0) {
        $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Trip Advisor', '', 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $id, 'Trip Advisor', '', 'Add', 'super admin');
    }

    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
    if (isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true) {
        header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    } else {
        header("Location: /admin/customer-listing-trip-advisor.php?bl_id=" . $BL_ID);
    }

    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left">

<?php require_once '../include/top-nav-listing.php'; ?>

    <div class="title-link">
        <div class="title">My Page - <?php echo $rowListing['BL_Listing_Title'] ?></div>
        <div class="link">
<?PHP
require_once('preview-link.php');
?>
        </div>
        <div class="instruction">
            Fields with this background<span></span>will show on free listings profile.
        </div>
    </div>

    <div class="left">
<?PHP require '../include/nav-mypage.php'; ?>
    </div>
    <div class="right">
        <form action="" method="post" name="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <div class="content-header">
                <div class="title">Trip Advisor</div>
                <div class="link">
<?php
$points_taken = 0;
$trip_point = show_field_points('Trip Advisor');
if ($rowListing['BL_Trip_Advisor']) {
    echo '<div class="points-com des-heading-point">' . $trip_point . ' pts</div>';
    $points_taken = $trip_point;
} else {
    echo '<div class="points-uncom">' . $trip_point . ' pts</div>';
}
?>
                </div>
            </div>
            <div class="form-inside-div border-none bottom-pading-none">
                <div class="form-data">
                    <textarea name="tripadvisor" cols="73" rows="8" style="width : 539px;"><?php echo $rowListing['BL_Trip_Advisor'] ?></textarea>                                   
                </div>
            </div>

            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" name="button2" value="Save Now"/>
                </div>  
            </div>
        </form>
        <div class="form-inside-div listing-ranking border-none">
            Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_trip_points ?> points
        </div>
        <div class="form-inside-div">
            <div class="content-sub-header border-none">
                <div class="title">Instructions</div>
            </div>
<?php
$help_text = show_help_text('Trip Advisor');
if ($help_text != '') {
    echo '<div class="form-inside-div margin-none border-none trip-advisor-links">' . $help_text . '</div>';
}
?>
        </div>
    </div>
</div>

<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>