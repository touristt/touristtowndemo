<?php

require_once '../include/config.inc.php';
$Region_Listing = 0;
$Region_Listing_Nearby = 0;
$rhl = '';
$region = $_POST['region'];
$category = $_POST['category'];
$subcategory = $_POST['subcategory'];
$search_text = $_POST['search_text'];
$listing_type = $_POST['listing_type'];
$daytrip = 0;

if (isset($_POST['daytrip']) && $_POST['daytrip'] != '') {
    $daytrip = $_POST['daytrip'];
}

if (isset($_POST['rl']) && $_POST['rl'] != '') {
    $Region_Listing = $_POST['rl'];
}

if (isset($_POST['rln']) && $_POST['rln'] != '') {
    $Region_Listing_Nearby = $_POST['rln'];
}

$rhl = $_POST['rhl'];
$bl_id = $_POST['bl_id'];
$current_region = $_POST['c_region'];
$current_cat = $_POST['c_cat'];
$current_subcat = $_POST['c_subcat'];
 $RID       = $_POST['hiddenId'];
 
//return
$maincat = '<option value="">Category</option>';
$subcat = '<option value="">Sub-Category</option>';
$listings = '<option value="">Select Listing</option>';

//if all selected 
if ($region > 0 && $category > 0 && $subcategory > 0 && $daytrip == 0 && $Region_Listing == 0 && $Region_Listing_Nearby == 0) {
  $sqlListings = "SELECT DISTINCT BL_ID, BL_Listing_Title FROM tbl_Business_Listing
                    LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
                    LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
                    INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID 
                    WHERE BL_ID NOT IN (SELECT IRML_BL_ID FROM tbl_Individual_Route_Map_Listings WHERE IRML_IR_ID = '".$RID."') AND BLC_C_ID = '" . $subcategory . "' AND BLCR_Home_status = 0 AND (BL_Free_Listing_Status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_Status_Date,INTERVAL 7 DAY))
                    AND hide_show_listing = 1 AND BLCR_BLC_R_ID = '" . $region . "' 
                    GROUP BY BL_ID ORDER BY BL_Listing_Title";
    $resListings = mysql_query($sqlListings);
    while ($Listings = mysql_fetch_assoc($resListings)) {
        $listings .= '<option  value="' . $Listings['BL_ID'] . '">' . $Listings['BL_Listing_Title'] . '</option>';
    }
}
// for day trip
if ($region > 0 && $category > 0 && $subcategory > 0 && $daytrip > 0 && $Region_Listing == 0 && $Region_Listing_Nearby == 0) {
    $sqlListings = "SELECT DISTINCT BL_ID, BL_Listing_Title FROM tbl_Business_Listing
                    LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
                    LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
                    INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID 
                    WHERE BLC_C_ID = '" . $subcategory . "' AND BLCR_Home_status = 0 AND (BL_Free_Listing_Status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_Status_Date,INTERVAL 7 DAY))
                    AND hide_show_listing = 1 AND BL_ID NOT IN (SELECT BLD_BL_ID FROM tbl_Business_Listing_Daytrip WHERE BLD_R_ID = '" . $region . "')
                    GROUP BY BL_ID ORDER BY BL_Listing_Title";
    $resListings = mysql_query($sqlListings);
    while ($Listings = mysql_fetch_assoc($resListings)) {
        $listings .= '<option value="' . $Listings['BL_ID'] . '">' . $Listings['BL_Listing_Title'] . '</option>';
    }
}
//  for extracting listings of region-lisitings.php page
if ((($category > 0 || $search_text != '') && $Region_Listing > 0) || ($Region_Listing > 0 && ($region > 0 || $region == -1 )) ) {
    $regionWhereForRegionListings = "";
    if ($region == -1) {
//        $regionJoinForRegionListings = "";
        $regionJoinForRegionListings = "INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID";
    } else {
        $regionJoinForRegionListings = "INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID";
        if ($region > 0) {
            $regionWhereForRegionListings = "AND BLCR_BLC_R_ID = $region";
        }
    }
    $sqlListings = "SELECT DISTINCT BL_ID, BL_Listing_Title FROM tbl_Business_Listing 
                    LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                    $regionJoinForRegionListings
                    WHERE (BL_Free_Listing_Status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_Status_Date,INTERVAL 7 DAY)) AND hide_show_listing = 1 $regionWhereForRegionListings";

    if (isset($region) && $region != 0) {
        $sql = "SELECT R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($region, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $activeRegion = mysql_fetch_assoc($result);
    }
    if (isset($category) && $category != 0) {
        $sqlListings .= " AND BLC_M_C_ID = $category";
    }
    if (isset($subcategory) && $subcategory != 0 && $category != 0) {
        $sqlListings .= " AND BLC_C_ID = $subcategory";
    }
    if (isset($listing_type) && $listing_type > 0) {
        $sqlListings .= " AND BL_Listing_Type = $listing_type";
    }
    if (isset($search_text) && $search_text != '') {
        $sqlListings .= " AND BL_Listing_Title LIKE '%" . encode_strings($search_text, $db) . "%'";
    }
    $sqlListings .= " GROUP BY BL_ID ORDER BY BL_Listing_Title";
    $resListings = mysql_query($sqlListings);
    if (($category > 0 || $search_text != '') && $Region_Listing > 0){
       $subcatcheck = "AND BLC_M_C_ID = $current_cat AND BLC_C_ID = $current_subcat"; 
    }else{
        $subcatcheck = "";
    }
    
    $sql_org_listings = "SELECT BLCR_BL_ID FROM tbl_Business_Listing INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                         INNER JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
                         WHERE BLCR_BLC_R_ID = $current_region $subcatcheck";
    $result_org_listings = mysql_query($sql_org_listings, $db) or die("Invalid query: $sql_org_listings -- " . mysql_error());
    while ($row_org_listings = mysql_fetch_assoc($result_org_listings)) {
        $childbl_id[] = $row_org_listings['BLCR_BL_ID'];
    }

    while ($Listings = mysql_fetch_assoc($resListings)) {
        if (!in_array($Listings['BL_ID'], $childbl_id)) {
            $listings .= '<option value="' . $Listings['BL_ID'] . '">' . $Listings['BL_Listing_Title'] . '</option>';
        }
    }
}

//  for extracting listings of region-lisitings-nearby.php page
if ($region > 0 && $category > 0 && $subcategory > 0 && $bl_id > 0 && $Region_Listing_Nearby > 0) {
    $sqlListings = "SELECT BL_ID, BL_Listing_Title FROM tbl_Business_Listing 
                    LEFT JOIN tbl_Business_Listing_Category_Region on BL_ID = BLCR_BL_ID
                    LEFT JOIN tbl_Business_Listing_Category on BL_ID = BLC_BL_ID
                    LEFT JOIN tbl_Region_Category ON RC_C_ID = BLC_M_C_ID
                    LEFT JOIN tbl_Region_Listings_Nearby ON BL_ID = RLN_DIS_BL_ID
                    WHERE (BL_Free_Listing_Status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_Status_Date,INTERVAL 7 DAY)) 
                    AND hide_show_listing = 1 AND RC_Status = 0 AND RLN_R_ID = '" . encode_strings($region, $db) . "' AND RLN_BL_ID = $bl_id";
    $sqlListings .= " AND BLCR_BLC_R_ID = $region And BL_ID != $bl_id";
    $sqlListings .= " AND BLC_M_C_ID = $category";
    $sqlListings .= " AND BLC_C_ID = $subcategory";
    $sqlListings .= " GROUP BY BL_ID ORDER BY BL_Listing_Title";
    $resListings = mysql_query($sqlListings);

    $sql_nearby = " SELECT RLN_DIS_BL_ID FROM  tbl_Region_Listings_Nearby 
                    WHERE RLN_R_ID = '" . encode_strings($region, $db) . "' AND RLN_BL_ID='$bl_id'";
    $result_nearby = mysql_query($sql_nearby, $db) or die(mysql_error());
    while ($row_nearby = mysql_fetch_assoc($result_nearby)) {
        $childbl_id[] = $row_nearby['RLN_DIS_BL_ID'];
    }

    while ($Listings = mysql_fetch_assoc($resListings)) {
        // if (in_array($Listings['BL_ID'], $childbl_id)){
        $listings .= '<option value="' . $Listings['BL_ID'] . '">' . $Listings['BL_Listing_Title'] . '</option>';
        // }
    }
}
//if region and category selected
if ($region != 0 && $category > 0) {
    if ($region == -1) {
        $regionWhereForSubCat = "";
    } else {
        $regionWhereForSubCat = "RC_R_ID = '" . $region . "' AND";
    }
    $sqlSubcategories = "SELECT RC_C_ID, RC_Name, C_Name, RC_Status, C_Parent FROM tbl_Region_Category LEFT JOIN tbl_Category ON RC_C_ID = C_ID
                         WHERE $regionWhereForSubCat C_Parent = '" . $category . "' AND RC_Status = 0 GROUP BY C_ID ORDER BY RC_Order ASC, RC_Name ASC, C_Name ASC";
    $resSubcategories = mysql_query($sqlSubcategories);
    while ($Subcategories = mysql_fetch_assoc($resSubcategories)) {
        $subcat .= '<option value="' . $Subcategories['RC_C_ID'] . '" ' . (($subcategory == $Subcategories['RC_C_ID']) ? 'selected' : '') . '>' . (($Subcategories['RC_Name'] != '') ? $Subcategories['RC_Name'] : $Subcategories['C_Name']) . '</option>';
    }
}
if ($region != 0) {
    if ($region == -1) {
        $regionWhereForCat = "";
    } else {
        $regionWhereForCat = "RC_R_ID = '" . $region . "' AND";
    }
    $sqlCategories = "SELECT RC_C_ID, RC_Name, C_Name, RC_Status, C_Parent FROM tbl_Region_Category LEFT JOIN tbl_Category ON RC_C_ID = C_ID
                      WHERE $regionWhereForCat C_Parent = 0 AND RC_Status = 0 AND C_Is_Blog != 1 AND C_Is_Product_Web != 1 GROUP BY C_ID ORDER BY RC_Order ASC, RC_Name ASC, C_Name ASC";
    $resCategories = mysql_query($sqlCategories);
    while ($Categories = mysql_fetch_assoc($resCategories)) {
        $maincat .= '<option value="' . $Categories['RC_C_ID'] . '" ' . (($category == $Categories['RC_C_ID']) ? 'selected' : '') . '>' . (($Categories['RC_Name'] != '') ? $Categories['RC_Name'] : $Categories['C_Name']) . '</option>';
    }
}
$return['maincat'] = $maincat;
$return['subcat'] = $subcat;
$return['listings'] = $listings;
print json_encode($return);
exit;
?>