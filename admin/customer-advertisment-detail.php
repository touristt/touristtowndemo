<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/PHPMailer/class.phpmailer.php';

$BID = $_REQUEST['id'];
$AID = $_REQUEST['advert_id'];
$sql = "SELECT B_Email FROM tbl_Business WHERE B_ID = '" . encode_strings($BID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$rowBUS = mysql_fetch_assoc($result);
$emailBUS = $rowBUS['B_Email'];
if ($AID > 0) {
    $sql_advertisements = " SELECT tbl_Advertisement.*, AT_ID, AT_Name, C_Name, R_Name, R_Domain, RC_Name FROM tbl_Advertisement
                            LEFT JOIN tbl_Region ON A_Website = R_ID 
                            LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID 
                            LEFT JOIN tbl_Category ON A_C_ID = C_ID 
                            LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID AND A_Website = RC_R_ID
                            WHERE A_ID = '" . encode_strings($AID, $db) . "'";
    $result_advertisements = mysql_query($sql_advertisements, $db) or die("Invalid query: $sql_advertisements -- " . mysql_error());
    $rowAdvert = mysql_fetch_assoc($result_advertisements);
    $title = $rowAdvert['A_Title'];
    $sql_sub_advert = "SELECT C_Name FROM tbl_Category WHERE C_ID = '" . encode_strings($rowAdvert['A_SC_ID'], $db) . "'";
    $result_sub = mysql_query($sql_sub_advert, $db) or die("Invalid query: $sql_sub_advert -- " . mysql_error());
    $rowAdvert_sub = mysql_fetch_assoc($result_sub);
    $Website_id = $rowAdvert['A_Website'];
    $category = $rowAdvert['A_C_ID'];
    $subcategory = $rowAdvert['A_SC_ID'];
} else {
    header('Location:customer-advertisment-myaccount.php?id=' . $BID);
}

if (isset($_POST['button_approve'])) {
    $sql_update_req = " UPDATE tbl_Advertisement SET A_Status='3', A_Active_Date = CURDATE() WHERE A_ID = '" . encode_strings($AID, $db) . "' LIMIT 1";
    $result_update_req = mysql_query($sql_update_req, $db) or die("Invalid query: $sql_update_req -- " . mysql_error());

    $_SESSION['advertisement_created'] = 1;
    header("Location: customer-advertisment-detail.php?advert_id=" . $AID . "&id=" . $BID);
    exit();
}

if (isset($_POST['button_change_request'])) {
    $sql_update_req = " UPDATE tbl_Advertisement SET A_Status='1' WHERE A_ID = '" . encode_strings($AID, $db) . "' LIMIT 1";
    $result_update_req = mysql_query($sql_update_req, $db) or die("Invalid query: $sql_update_req -- " . mysql_error());
    $sql_req_des = " Insert tbl_Advertisement_Change_Request SET 
                     ACR_A_AID = '" . encode_strings($AID, $db) . "',      
                     ACR_Description = '" . encode_strings($_POST['request_desc'], $db) . "',
                     ACR_Date = NOW()";
    $result_req_des = mysql_query($sql_req_des, $db) or die("Invalid query: $sql_req_des -- " . mysql_error());
    $_SESSION['request_sent'] = 1;
    header("Location: customer-advertisment-detail.php?advert_id=" . $AID . "&id=" . $BID);
    exit();
}

if ($_REQUEST['del'] == 'true') {
    $sql = " UPDATE tbl_Advertisement SET A_Is_Deleted = 1 WHERE A_ID = '" . encode_strings($AID, $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $_SESSION['delete'] = 1;
    header("Location:customer-advertisment-myaccount.php?id=$BID");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left advert">
    <?php require_once '../include/nav-B-advertisement-credit-admin.php'; ?>
    <div class="title-link">
        <div class="title">My Campaigns</div>
        <div class="link">
        </div> 
        <div class="title float-right-text text-margin"></div>
    </div>

    <div class="right">
        <div class="content-header">
            <div class="title ad-detail">
                <div class="advert-detail-header" style="margin-bottom: 5px;"><?php echo $rowAdvert['R_Name'] ?></div>
                <div class="advert-detail-header">
                    <?php
                    echo $rowAdvert['AT_Name'];
                    if ($rowAdvert['A_C_ID'] != 0) {
                        if ($rowAdvert['RC_Name'] != '') {
                            echo " / " . $rowAdvert['RC_Name'];
                        } else {
                            echo " / " . $rowAdvert['C_Name'];
                        }
                    }
                    if ($rowAdvert['A_SC_ID'] != 0) {
                        echo " / " . $rowAdvert_sub['C_Name'];
                    }
                    ?>
                </div>
            </div>
            <?php
            $sql = mysql_query("SELECT A_ID, A_Status,A_End_Date, A_Is_Deleted FROM tbl_Advertisement WHERE A_ID  = '" . $_REQUEST['advert_id'] . "' ORDER BY A_ID");
            $vals = mysql_fetch_assoc($sql);
            $stats = $vals['A_Status'];
            $A_Date = $vals['A_End_Date'];
            $delt = $vals['A_Is_Deleted'];
            if ($stats == 4 OR $delt == 1 OR ( $vals['A_End_Date'] < date('Y-m-d') AND $vals['A_End_Date'] != '0000-00-00')) {
                $disable_value = 1;
            } else {
                $disable_value = '';
            }
            ?>
            <div class="link ad-detail">
                <form name="form" method="get" action="customer-advertisment-detail.php">
                    <input type="hidden" name="id" value="<?php echo $BID; ?>">
                    <select name="advert_id" onchange="this.form.submit()">
                        <?php
                        $sql = "SELECT A_ID, A_Title FROM tbl_Advertisement WHERE A_B_ID = '$BID' ORDER BY A_ID";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($allAds = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $allAds['A_ID'] ?>" <?php echo ($AID == $allAds['A_ID']) ? 'selected' : '' ?>><?php echo $allAds['A_Title'] ?></option>
                        <?php } ?>
                    </select>
                </form>
            </div>
        </div>
        <div class="containter-adv">
            <div class="form-inside-div form-inside-div-adv">
                <label>Website</label>
                <div class="form-data from-inside-div-text">
                    <?php echo $rowAdvert['R_Domain']; ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-adv">
                <label>Campaign Type</label>
                <div class="form-data from-inside-div-text">
                    <?php echo $rowAdvert['AT_Name']; ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-adv" id="cat" <?php
            if ($rowAdvert['AT_ID'] == 4) {
                echo 'style="display :none"';
            }
            ?>>
                <input type="hidden" value="<?php echo $Website_id; ?>" id="region-id" >
                <input type="hidden" value="1" id="update-id" >
                <input type="hidden" value="<?php echo $rowAdvert['A_ID']; ?>" id="advert-id" >
                <input type="hidden" value="<?php echo $rowAdvert['AT_ID']; ?>" id="advert-type" >
                <label>Category</label>
                <div class="form-data" id="advert-cats">
                    <select class="adv-type-options <?php
                    if ($disable_value == 1) {
                        echo "disabled-val";
                    }
                    ?>" id="advert-cat" name="category" onChange="get_cat_advert_update();" <?php if ($disable_value == 1) { ?> disabled <?php } ?>>
                            <?PHP
                            if (isset($category)) {
                                $sql = "SELECT C_ID, C_Name, RC_Name FROM tbl_Category
                                    LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID
                                    LEFT JOIN tbl_Region ON RC_R_ID = R_ID 
                                    WHERE R_ID = '$Website_id' AND C_Parent = 0 ORDER BY RC_Order ASC";
                                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                while ($rowCat = mysql_fetch_assoc($result)) {
                                    ?>
                                <option value="<?php echo $rowCat['C_ID'] ?>" <?php echo ($rowCat['C_ID'] == $category) ? 'selected' : ''; ?>><?php echo ($rowCat['RC_Name'] != '') ? $rowCat['RC_Name'] : $rowCat['C_Name'] ?></option>
                                <?PHP
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-adv" id="sub-cat" <?php
            if ($rowAdvert['AT_ID'] == 2 || $rowAdvert['AT_ID'] == 4) {
                echo 'style="display :none"';
            }
            ?>>
                <label>Sub Category</label>
                <div class="form-data" id="advert-subcats">
                    <select class="adv-type-options <?php
                    if ($disable_value == 1) {
                        echo "disabled-val";
                    }
                    ?>"  id="advert-subcat" name="subcategory" onChange="get_subcat_advert_update();" <?php if ($disable_value == 1) { ?> disabled<?php } ?> >
                        <option value="">Select Sub Category</option>
                        <?PHP
                        if (isset($subcategory)) {
                            $sql = "SELECT C_ID, C_Name FROM tbl_Category 
                                    LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID
                                    LEFT JOIN tbl_Region ON RC_R_ID = R_ID 
                                    WHERE R_ID = '$Website_id' AND C_Parent = '$category' ORDER BY C_Name";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($rowSubcat = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo $rowSubcat['C_ID'] ?>" <?php echo ($rowSubcat['C_ID'] == $subcategory) ? 'selected' : ''; ?>><?php echo $rowSubcat['C_Name'] ?></option>
                                <?PHP
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-adv">
                <label>Campaign</label>
                <div class="form-data from-inside-div-text">
                    <?php
                    if ($rowAdvert['A_Campaign'] == 1) {
                        echo 'Yes';
                    } else {
                        echo 'No';
                    }
                    ?>
                </div>
            </div>
            <?php
            if ($rowAdvert['A_Status'] == 3) {
                ?>
                <div class="form-inside-div form-inside-div-adv">
                    <label>View Campaign</label>
                    <div class="form-data from-inside-div-text">
                        <div class="approved-photo-div">
                            <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $rowAdvert['A_Approved_Logo'] ?>">          
                        </div>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-adv">
                    <label>Original Start Date</label>
                    <div class="form-data from-inside-div-text">
                        <?php echo date('F d, Y', strtotime($rowAdvert['A_Date'])); ?>
                    </div>
                </div>
                <?php
                if ($rowAdvert['A_End_Date'] != '0000-00-00') {
                    ?>
                    <div class="form-inside-div form-inside-div-adv">
                        <label>End Date</label>
                        <div class="form-data from-inside-div-text">
                            <?php echo date('F d, Y', strtotime($rowAdvert['A_End_Date'])); ?>
                        </div>
                    </div>
                    <?php
                }
                $sql_Impressions = "SELECT * FROM  tbl_Advertisement_Statistics WHERE AS_A_ID = '" . encode_strings($rowAdvert['A_ID'], $db) . "'";
                $result_Impressions = mysql_query($sql_Impressions, $db) or die("Invalid query: $sql_Impressions -- " . mysql_error());
                $total_impressions = 0;
                $total_clicks = 0;
                while ($rowAdvert_Impressions = mysql_fetch_assoc($result_Impressions)) {
                    $total_impressions += $rowAdvert_Impressions['AS_Impression'];
                    $total_clicks += $rowAdvert_Impressions['AS_Clicks'];
                }
                ?>
                <div class="form-inside-div form-inside-div-adv">
                    <label>Impressions</label>
                    <div class="form-data from-inside-div-text">
                        <?php echo $total_impressions ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-adv">
                    <label>Clicks</label>
                    <div class="form-data from-inside-div-text">
                        <?php echo $total_clicks ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-adv">
                    <label>Monthly Cost</label>
                    <div class="form-data margin-top-11">
                        $<?php echo $rowAdvert['A_Total'] ?>
                    </div>
                    <?php if ($disable_value == '') { ?>
                        <a class="side-informtion margin-adver-cancel margin-top-11" onClick="return confirm('Are you sure you want to cancel your ad? This action can not be undone.');"  href="customer-advertisment-detail.php?del=true&advert_id=<?php echo $rowAdvert['A_ID'] ?>&id=<?php echo $BID ?>">Cancel Advertisement</a>
                    <?php } ?>
                </div>
                <?php
            }
            if ($rowAdvert['A_Status'] == 1) {
                ?>
                <div class="form-inside-div border-none">
                    <p> The Tourist Town team will now create your campaign. Once your campaign is created you will receive an email to : <?php echo $emailBUS ?></p>
                </div>
                <?php
            }
            if ($rowAdvert['A_Status'] == 2) {
                ?>
                <div class="form-inside-div border-none">
                    <p> Below is your campaign that will shown as per your request.As soon as you
                        approve the campaign it will be posted to the selected website and billing 
                        will commence.
                    </p>
                </div>
                <div class="form-inside-div border-none">
                    <div class="form-data image-align-advert">
                        <div class="approved-photo-div">
                            <img  src="http://<?php echo DOMAIN . "/" . IMG_LOC_REL . "/" . $rowAdvert['A_Approved_Logo'] ?>"> 
                        </div>
                    </div>
                </div>
                <div class="form-inside-div border-none">
                    <div class="button">
                        <form name="form1" method="post" enctype="multipart/form-data" action="customer-advertisment-detail.php" >
                            <input type="hidden" name="advert_id" value="<?php echo $AID; ?>">
                            <input type="hidden" name="id" value="<?php echo $BID; ?>">

                            <input type="submit" name="button_approve" class="margin-right-15" value="Approve Advertisement">
                            <input type="button" value="Request Change" onclick="show_update_content(<?php echo $rowAdvert['A_ID'] ?>);" >
                        </form>
                        <form action="" method="post" name="form" id="Update-Content-<?php echo $rowAdvert['A_ID']; ?>" style="display:none;">
                            <div style="float:left; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                <input type="hidden" name="advert_id" value="<?php echo $AID; ?>">
                                <input type="hidden" name="id" value="<?php echo $BID; ?>">
                                <textarea required name="request_desc" style="width: 422px;"></textarea>
                            </div>
                            <div style="float:left;text-align: center; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                <div class="form-data"  >
                                    <input type="submit" name="button_change_request" value="Save Now"/>
                                </div>
                            </div> 
                        </form>
                    </div>
                </div>
            <?php } ?>
        </div>

        <!--Statistics-->
        <?php
        if ($rowAdvert['A_Status'] == 3) {
            ?>
            <div class="content-header">
                <div class="title">
                    Campaigning Period
                </div>
            </div>
            <div class="monthly-statistic-header">
                <div class="data-column cust-listing-title padding-none">Year</div>
                <div class="data-column cust-listing-other padding-none">Month </div>
                <div class="data-column cust-listing-other padding-none">Impressions</div>
                <div class="data-column cust-listing-other padding-none">Clicks</div>
            </div>
            <?php
            $sql2 = "SELECT DISTINCT YEAR(AS_Date) as years, MONTH(AS_Date) as months FROM tbl_Advertisement_Statistics WHERE AS_A_ID = $AID order by YEAR(AS_Date) desc,month(AS_Date) desc";
            $result2 = mysql_query($sql2, $db) or die("Invalid query: $sql2 -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result2)) {
                $yearmonths[] = $row;
            }
            foreach ($yearmonths as $yrmonths) {
                $sql2 = "SELECT YEAR(AS_Date),MONTHNAME(AS_Date), SUM(AS_Impression) AS impression, SUM(AS_Clicks) AS clicks FROM tbl_Advertisement_Statistics 
                    WHERE AS_A_ID = $AID AND YEAR(AS_Date) = '" . $yrmonths['years'] . "' AND MONTH(AS_Date) = '" . $yrmonths['months'] . "'";

                $result2 = mysql_query($sql2, $db) or die("Invalid query: $sql2 -- " . mysql_error());
                $rowStats = mysql_fetch_array($result2);
                ?>
                <div class="data-content advert-statistics">
                    <div class="data-column cust-listing-title padding-none"><?php echo $rowStats['YEAR(AS_Date)']; ?></div>
                    <div class="data-column cust-listing-other padding-none"><?php echo $rowStats['MONTHNAME(AS_Date)']; ?></div>
                    <div class="data-column cust-listing-other padding-none"><?php echo ($rowStats['impression'] != '') ? $rowStats['impression'] : '0'; ?></div>
                    <div class="data-column cust-listing-other padding-none"><?php echo ($rowStats['clicks'] != '') ? $rowStats['clicks'] : '0'; ?></div>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>    
<script>
    function get_cat_advert_update() {
        var cat_id = document.getElementById('advert-cat').value;
        var region_id = document.getElementById('region-id').value;
        var advert_id = document.getElementById('advert-id').value;
        var advert_type = document.getElementById('advert-type').value;
        $.ajax({
            type: "GET",
            url: "get-customer-advert-cat-update.php",
            data: {
                cat_id: cat_id,
                website_id: region_id,
                advert_id: advert_id,
                advert_type: advert_type
            }
        }).done(function (msg) {
            if (msg == 1) {
                location.reload();
            } else {
                document.getElementById('advert-subcats').innerHTML = msg;
            }
        });
    }
    function get_subcat_advert_update() {
        var subcat_id = document.getElementById('advert-subcat').value;
        var advert_id = document.getElementById('advert-id').value;
        var cat_id = document.getElementById('advert-cat').value;
        $.ajax({
            type: "GET",
            url: "get-customer-advert-subcat-update.php",
            data: {
                subcat_id: subcat_id,
                advert_id: advert_id,
                cat_id: cat_id
            }
        }).done(function (msg) {
            location.reload();
        });
    }
    function show_update_content(a_id) {
        $("#Update-Content-" + a_id).attr("title", "Request Change").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 500
        });
    }
</script>
<?PHP
require_once '../include/admin/footer.php';
?>