<?php
include '../include/config.inc.php';
include '../include/accommodation_functions.php';
require_once '../include/login.inc.php';
require_once '../include/admin/header.php';
include '../include/accommodation_header.php';
/* Save all Logic */
$accommodation_ids = array();
$cat_id = 29; //category_id
if (isset($_REQUEST['cat_id'])) {
    $cat_id = $_REQUEST['cat_id'];
}
$filters = '';
$beds = '';
$search = '';
if (isset($_GET['filter'])) {
    if (isset($_GET['BL_Sports_Team_Welcome'])) {
        $filters['BL_Sports_Team_Welcome'] = 1;
    }
    if (isset($_GET['BL_Pets_Allowed'])) {
        $filters['BL_Pets_Allowed'] = 1;
    }
    if (isset($_GET['BL_Flexible_Rental'])) {
        $filters['BL_Flexible_Rental'] = 1;
    }
    if (isset($_GET['closed'])) {
        $closed = 0;
    }
    if (isset($_GET['BL_Weekly_Rental'])) {
        $filters['BL_Weekly_Rental'] = 1;
    }
    if (isset($_GET['bed_1'])) {
        $beds['bed_1'] = 0;
    }
    if (isset($_GET['bed_2'])) {
        $beds['bed_2'] = 0;
    }
    if (isset($_GET['bed_3'])) {
        $beds['bed_3'] = 0;
    }
    if (isset($_GET['bed_4'])) {
        $beds['bed_4'] = 0;
    }
    if (isset($_GET['bed_5'])) {
        $beds['bed_5'] = 0;
    }
    if (isset($_GET['bed_6'])) {
        $beds['bed_6'] = 0;
    }
    if (isset($_GET['bed_7'])) {
        $beds['bed_7'] = 0;
    }
    if (isset($_GET['bed_8'])) {
        $beds['bed_8'] = 0;
    }
}
if (isset($_GET['search_word'])) {
    $search = $_GET['search_word'];
}
$reg_id = 1; //region id by default
if (isset($_REQUEST['reg_id'])) {
    $reg_id = $_REQUEST['reg_id'];
}
// if its the bgadmin logged in
$regionlimit = false; // if super admin, false, if bg or region or town admin, then true
if ($_SESSION['USER_LIMIT'] > 0) {
    $reg_id = $_SESSION['USER_LIMIT'];
    $regionlimit = true;
}
if (isset($_GET['block_id'])) {
    $block_id = $_GET['block_id'];
    $archive = isset($_GET['archive']) ? $_GET['archive'] : 0;
} else {
    header('location:group_blocks.php');
    exit();
}
$category_info = category_name($cat_id);
$block = block_info($block_id);
?>
<?php if (isset($_SESSION['message'])) { ?>
    <div class="message">
        <?php
        echo $_SESSION['message'];
        unset($_SESSION['message']);
        ?>
    </div>
    <?php
}
$block_infomation = block_info($block_id);
?>
<div class="content-left full-width">
    <form action="cottage_parks.php" method="GET">
        <div class="title-link">
            <div class="title"><?php echo strtoupper($block_infomation['block_group_name']), ' - ', $category_info['C_Name']; ?> (For the Week of <?php echo date('F d', strtotime($block_infomation['start_date'])) . ' to ' . date('F d', strtotime($block_infomation['end_date'])) ?>)</div>
            <div class="addlisting">
                <input type="text" class="accommodation-cap-search" name="search_word" value="<?php //echo $search ?>" placeholder="Search"/>
                <input type="submit" class="top-search-btn" name="submit-search" value="GO"/>
            </div>
        </div>
        <div class="right full-width">         
            <div class="filter-row">
                <div class="filter-row1">
                    <?php
                    $gbid = ($block_infomation['block_group_id']) ? $block_infomation['block_group_id'] : -1;
                    $group_blocks_query = "SELECT * FROM `acc_blocks` where group_block_id =" . $gbid;
                    $group_blocks = mysql_query($group_blocks_query) or die(mysql_error());
                    if (mysql_num_rows($group_blocks) > 0) {
                        ?>
                        <select name="block_id" class="filter-block-list">
                            <?php
                            while ($block_data = mysql_fetch_array($group_blocks)) {
                                $active_block = '';
                                if ($block_id == $block_data['id']) {
                                    $active_block = 'selected="selected"';
                                }
                                ?>
                                <option <?php echo $active_block; ?>  value="<?php echo $block_data['id']; ?>"><?php echo $block_data['name']; ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                    <select name="cat_id" class="filter-acco-list">
                        <?php
                        $accommodation_type = mysql_query('SELECT C_ID, C_Name FROM `tbl_Category` tbc Where tbc.C_ID IN (29,30,31,59,45) ORDER BY tbc.C_ID ASC');
                        $serial = mysql_num_rows($accommodation_type);
                        $active_item = '';
                        while ($accomm = mysql_fetch_array($accommodation_type)) {
                            $active = '';
                            if ($cat_id == $accomm['C_ID']) {
                                $active = 'selected';
                                $active_item = $accomm['C_Name'];
                            }
                            print "<option $active value='" . $accomm['C_ID'] . "'>" . $accomm['C_Name'] . "</option>";
                        }
                        ?>
                    </select>
                    <?php
                    // only show region filter to super admin
                    if (!$regionlimit) {
                        ?>
                        <select name="reg_id" class="filter-hotels-list">
                            <?php
                            $accommodation_reg = mysql_query('SELECT R_ID, R_Name FROM `tbl_Region`');
                            while ($region = mysql_fetch_array($accommodation_reg)) {
                                $active = '';
                                if ($reg_id == $region['R_ID']) {
                                    $active = 'selected';
                                }
                                print_r("<option $active value='" . $region['R_ID'] . "'>" . $region['R_Name'] . "</option>");
                            }
                            ?>
                        </select>
                        <?php
                    }
                    ?>
                    <!--Add the archive filter-->
                    <input type="hidden" name="archive" value="<?php echo $archive ?>"/>
                    
                    <input type="submit" class="filter-btn" name="filter" value="Filter Now"/>
                </div>
                <div class="filter-row2">
                    <?php
                    if ($cat_id == 59 || $cat_id == 30) {
                        ?>
                        <div class="filter-beds">
                            # Bedrooms
                            <input type="checkbox" name="bed_1" <?php echo isset($beds['bed_1']) ? 'checked' : '' ?> class="filter-checkbox"/><lable for="bed_1" class="lable">1</lable>
                            <input type="checkbox" name="bed_2" <?php echo isset($beds['bed_2']) ? 'checked' : '' ?> class="filter-checkbox"/><lable for="bed_2" class="lable">2</lable>
                            <input type="checkbox" name="bed_3" <?php echo isset($beds['bed_3']) ? 'checked' : '' ?> class="filter-checkbox"/><lable for="bed_3" class="lable">3</lable>
                            <input type="checkbox" name="bed_4" <?php echo isset($beds['bed_4']) ? 'checked' : '' ?> class="filter-checkbox"/><lable for="bed_4" class="lable">4</lable>
                            <input type="checkbox" name="bed_5" <?php echo isset($beds['bed_5']) ? 'checked' : '' ?> class="filter-checkbox"/><lable for="bed_5" class="lable">5</lable>
                            <input type="checkbox" name="bed_6" <?php echo isset($beds['bed_6']) ? 'checked' : '' ?> class="filter-checkbox"/><lable for="bed_6" class="lable">6</lable>
                            <input type="checkbox" name="bed_7" <?php echo isset($beds['bed_7']) ? 'checked' : '' ?> class="filter-checkbox"/><lable for="bed_7" class="lable">7</lable>
                            <input type="checkbox" name="bed_8" <?php echo isset($beds['bed_8']) ? 'checked' : '' ?> class="filter-checkbox"/><lable for="bed_8" class="lable">8</lable>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="filter-row2">
                    <div class="filter-sports-team">
                        <input type="checkbox" name="BL_Sports_Team_Welcome" <?php echo isset($filters['BL_Sports_Team_Welcome']) ? 'checked' : '' ?>/><lable for="BL_Sports_Team_Welcome">No Sports Teams</lable>
                    </div>
                    <div class="filter-pets">
                        <input type="checkbox" name="BL_Pets_Allowed" <?php echo isset($filters['BL_Pets_Allowed']) ? 'checked' : '' ?>/><lable for="BL_Pets_Allowed">Pets Allowed</lable>
                    </div>
                    <div class="filter-rental">
                        <input type="checkbox" name="BL_Flexible_Rental" <?php echo isset($filters['BL_Flexible_Rental']) ? 'checked' : '' ?>/><lable for="BL_Flexible_Rental">Long Term Rental</lable>
                    </div>
                    <div class="filter-availabe">
                        <input type="checkbox" name="closed" <?php echo isset($closed) ? 'checked' : '' ?>/><lable for="closed">Available</lable>
                    </div>
                    <?php
                    if ($cat_id == 59 || $cat_id == 30) {
                        ?>
                        <div class="filter-chamber">
                            <input type="checkbox" name="BL_Weekly_Rental" <?php echo isset($filters['BL_Weekly_Rental']) ? 'checked' : '' ?>/><lable for="BL_Weekly_Rental">Weekly Rental Only</lable>
                        </div>
                    <?php } ?>
                </div>
            </div>
    </form>
    <!--data container-->
    <!--if archive, add class archive to make all textboxes disabled-->
    <div class="data-container">
        <?php
        $page_counter = 0;
        if ($cat_id == 59 || $cat_id == 30) {
            cat_type_header($cat_id, $active_item, $block);
            $hotels = get_listings($cat_id, $reg_id, 1, $filters, $search);
            $i = 1;
            if ($hotels['listings'] != '') {
                foreach ($hotels['listings'] as $row) {
                    $accom_id = $row['BL_ID'];
                    $accom_name = $row['BL_Listing_Title'];
                    $accom_sports_team_welcome = $row['BL_Sports_Team_Welcome'];
                    $accom_pets_allowed = $row['BL_Pets_Allowed'];
                    $accom_flexible_rental = $row['BL_Flexible_Rental'];
                    $accom_notes = $row['BL_Notes'];
                    $accom_weekly_rental = $row['BL_Weekly_Rental'];
                    $accom_closed = isset($row['closed']) && $row['closed'] != 0 ? 'checked' : '';
                    $accom_percentage = isset($row['BL_Percentage']) && $row['BL_Percentage'] != NULL ? $row['BL_Percentage'] : '50';
                    $capacity_wrt_group = "SELECT * FROM `acc_capacity_response` WHERE `block_id` = $block_id AND Accommodator_id = $accom_id";
                    if ($beds != '') {
                        $counter = 1;
                        foreach ($beds as $key => $value) {
                            if ($counter == 1) {
                                $capacity_wrt_group .= " AND ($key > $value";
                            } else {
                                $capacity_wrt_group .=" OR $key > $value";
                            }
                            $counter++;
                        }
                        $capacity_wrt_group .=")";
                    }
                    $acc_group_result = mysql_query($capacity_wrt_group);
                    $acc_group = mysql_fetch_array($acc_group_result);
                    $bed_1 = isset($acc_group['bed_1']) ? $acc_group['bed_1'] : 0;
                    $bed_2 = isset($acc_group['bed_2']) ? $acc_group['bed_2'] : 0;
                    $bed_3 = isset($acc_group['bed_3']) ? $acc_group['bed_3'] : 0;
                    $bed_4 = isset($acc_group['bed_4']) ? $acc_group['bed_4'] : 0;
                    $bed_5 = isset($acc_group['bed_5']) ? $acc_group['bed_5'] : 0;
                    $bed_6 = isset($acc_group['bed_6']) ? $acc_group['bed_6'] : 0;
                    $bed_7 = isset($acc_group['bed_7']) ? $acc_group['bed_7'] : 0;
                    $bed_8 = isset($acc_group['bed_8']) ? $acc_group['bed_8'] : 0;
                    $accom_general = $row['1x1'] + $row['2x2'] + $row['3x3'] + $row['4x4'] + $row['5x5'] + $row['6x6'] + $row['7x7'] + $row['8x8'];
                    
                    //check availability
                    $total_rooms_availability = $bed_1 + $bed_2 + $bed_3 + $bed_4 + $bed_5 + $bed_6 + $bed_7 + $bed_8;
                    $total_capacity = $accom_general;
                    $block_aval = $total_capacity;
                    if ($block_aval) {
                        $percent = round(($total_rooms_availability / $block_aval) * 100);
                    } else {
                        $percent = 0;
                    }
                    if(isset($closed) && $percent == 0) {
                      continue;
                    }
                    
                    if ($beds == '') {
                        $accommodation_ids[] = $accom_id;
                        $page_counter++;
                        /* Save all Logic */
                        ?>
                        <div class="accordion-section">
                            <div class="data-header-rows accordion-header">
                                <div class="header-row3-type-name" title="<?php echo $accom_name ?>">
                                    <?php echo (strlen($accom_name) > 30) ? substr($accom_name, 0, 28) . '...' : $accom_name; ?></div>
                                <div class="header-row3-cap cap">&nbsp;</div>
                                <form id="ajax_group_accom_cap_<?php echo $accom_id ?>" name="ajax_group_accom_cap_<?php echo $accom_id ?>" onSubmit="return block_capacity_update(this, <?php echo  $i; ?>)">
                                    <input type="hidden" name="accom_general" value="<?php echo $accom_general ?>">
                                    <input type="hidden" name="block_id" value="<?php echo $block_id ?>">
                                    <input type="hidden" name="accom_id" value="<?php echo $accom_id ?>">
                                    <input type="hidden" name="cat_id" value="<?php echo $cat_id ?>">
                                    <input type="hidden" name="cap_id" value="<?php echo isset($acc_group['id']) ? $acc_group['id'] : 0; ?>">
                                    <input type="hidden" name="type" value="1">
                                    <div class="header-row3-beds cap"><input type="number" name="<?php echo'bed_' . 1; ?>" value="<?php echo  ($row['1x1'] == 0) ? '' : $bed_1; ?>" class="beds-txt" max="<?php echo $row['1x1'] ?>" <?php echo  ($row['1x1'] == 0) ? 'disabled' : '' ?> /></div>
                                    <div class="header-row3-beds cap"><input type="number" name="<?php echo'bed_' . 2; ?>" value="<?php echo  ($row['2x2'] == 0) ? '' : $bed_2; ?>" class="beds-txt" max="<?php echo $row['2x2'] ?>" <?php echo  ($row['2x2'] == 0) ? 'disabled' : '' ?>/></div>
                                    <div class="header-row3-beds cap"><input type="number" name="<?php echo'bed_' . 3; ?>" value="<?php echo  ($row['3x3'] == 0) ? '' : $bed_3; ?>" class="beds-txt" max="<?php echo $row['3x3'] ?>" <?php echo  ($row['3x3'] == 0) ? 'disabled' : '' ?>/></div>
                                    <div class="header-row3-beds cap"><input type="number" name="<?php echo'bed_' . 4; ?>" value="<?php echo  ($row['4x4'] == 0) ? '' : $bed_4; ?>" class="beds-txt" max="<?php echo $row['4x4'] ?>" <?php echo  ($row['4x4'] == 0) ? 'disabled' : '' ?>/></div>
                                    <div class="header-row3-beds cap"><input type="number" name="<?php echo'bed_' . 5; ?>" value="<?php echo  ($row['5x5'] == 0) ? '' : $bed_5; ?>" class="beds-txt" max="<?php echo $row['5x5'] ?>" <?php echo  ($row['5x5'] == 0) ? 'disabled' : '' ?>/></div>
                                    <div class="header-row3-beds cap"><input type="number" name="<?php echo'bed_' . 6; ?>" value="<?php echo  ($row['6x6'] == 0) ? '' : $bed_6; ?>" class="beds-txt" max="<?php echo $row['6x6'] ?>" <?php echo  ($row['6x6'] == 0) ? 'disabled' : '' ?>/></div>
                                    <div class="header-row3-beds cap"><input type="number" name="<?php echo'bed_' . 7; ?>" value="<?php echo  ($row['7x7'] == 0) ? '' : $bed_7; ?>" class="beds-txt" max="<?php echo $row['7x7'] ?>" <?php echo  ($row['7x7'] == 0) ? 'disabled' : '' ?>/></div>
                                    <div class="header-row3-beds cap"><input type="number" name="<?php echo'bed_' . 8; ?>" value="<?php echo  ($row['8x8'] == 0) ? '' : $bed_8; ?>" class="beds-txt" max="<?php echo $row['8x8'] ?>" <?php echo  ($row['8x8'] == 0) ? 'disabled' : '' ?>/></div>
                                    <div class="header-row3-save">
                                    <?php
                                      if($archive == 0){
                                    ?>
                                      <input type="submit" name="beds_submit" value="Save" class="beds-btn"/>
                                    <?php 
                                      }
                                      else {
                                    ?>
                                      &nbsp;
                                    <?php
                                      }
                                    ?>
                                    </div>
                                </form>
                                <div class="header-row3-percent cap" id="cap-percent-<?php echo  $i; ?>"> 
                                    <?php
                                    echo $percent . "%";
                                    ?>
                                </div>
                            </div>
                            <div  class="data-header-rows accordion-content">
                                <div class="notes-info-col-full">
                                    <div class="contact1"><?php echo $accom_name; ?></div>
                                </div>
                                <div class="notes-info-col1">
                                    <div class="notess-info-contacts">
                                        <div class="contact1"><?php echo $row['BL_Contact']; ?></div>
                                        <div class="contact1"><a href="tel:<?php echo $row['BL_Phone'] ?>"><?php echo $row['BL_Phone']; ?></a></div>
                                        <div class="contact2"><a href="tel:<?php echo $row['BL_Cell'] ?>"><?php echo $row['BL_Cell']; ?></a></div>
                                    </div>
                                    <div class="notes-address"><?php echo $row['BL_Street']; ?></div>
                                    <div class="notes-city"><?php echo $row['BL_Town']; ?></div>
                                    <div class="notes-email"><a href="mailto:<?php echo $row['BL_Email'] ?>"><?php echo $row['BL_Email'] ?></a></div>
                                    <div class="notes-email"><a href="<?php echo $row['BL_Website'] ?>"><?php echo $row['BL_Website']; ?></a></div>
                                </div>
                                <form onsubmit="return block_capacity_update_notes(this)">
                                    <div class="notes-info-col2">
                                        <input type="hidden" name="accom_id" value="<?php echo $accom_id ?>">
                                        <div class="notess-info-features">
                                            <input type="checkbox" <?php echo $accom_sports_team_welcome == 1 ? 'checked' : '' ?> name="sports_team"/><lable for="sports_team">No Sports Teams</lable>
                                        </div>
                                        <div class="notess-info-features">
                                            <input type="checkbox" <?php echo $accom_pets_allowed == 1 ? 'checked' : '' ?> name="pets"/><lable for="pets">Pets Allowed</lable>
                                        </div>
                                        <div class="notess-info-features">
                                            <input type="checkbox" <?php echo $accom_flexible_rental == 1 ? 'checked' : '' ?> name="flexible_rental"/><lable for="flexible_rental">Long Term Rental</lable>
                                        </div>
                                        <?php
                                        if ($cat_id == 59 || $cat_id == 30) {
                                            ?>
                                            <div class="notess-info-features">
                                                <input type="checkbox" <?php echo $accom_weekly_rental == 1 ? 'checked' : '' ?> name="Weekly_Rental_Only"/><lable for="Weekly_Rental_Only">Weekly Rental Only</lable>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="notes-info-col3">
                                        <div class="notess-info-textarea">
                                            <textarea class='tt-ckeditor' name="notes"><?php echo $accom_notes ?></textarea>
                                        </div>
                                      <?php
                                        if($archive == 0){
                                      ?>
                                        <div class="notess-info-save-note" style="margin-top:10px;">
                                            <input type="submit" name="save_note" class="save-note-btn" value="Save Note"/>
                                        </div>
                                      <?php
                                        }
                                      ?>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <?php
                    } else {
                        if ($beds != '' && isset($acc_group['id'])) {
                            $accommodation_ids[] = $accom_id;
                            $page_counter++;
                            ?>
                            <div class="accordion-section">
                                <div class="data-header-rows accordion-header">
                                    <div class="header-row3-type-name" title="<?php echo $accom_name; ?>">
                                        <?php echo (strlen($accom_name) > 30) ? substr($accom_name, 0, 28) . '...' : $accom_name; ?>
                                    </div>
                                    <div class="header-row3-cap cap">&nbsp;</div>
                                    <form id="ajax_group_accom_cap_<?php echo $accom_id ?>" name="ajax_group_accom_cap_<?php echo $accom_id ?>" onSubmit="return block_capacity_update(this, <?php echo  $i; ?>)">
                                        <input type="hidden" name="accom_general" value="<?php echo $accom_general ?>">
                                        <input type="hidden" name="block_id" value="<?php echo $block_id ?>">
                                        <input type="hidden" name="accom_id" value="<?php echo $accom_id ?>">
                                        <input type="hidden" name="cat_id" value="<?php echo $cat_id ?>">
                                        <input type="hidden" name="cap_id" value="<?php echo isset($acc_group['id']) ? $acc_group['id'] : 0; ?>">
                                        <input type="hidden" name="type" value="1">
                                        <div class="header-row3-beds cap"><input type="number" name="<?php echo'bed_' . 1; ?>" value="<?php echo  ($row['1x1'] == 0) ? '' : $bed_1; ?>" class="beds-txt" max="<?php echo $row['1x1'] ?>" <?php echo  ($row['1x1'] == 0) ? 'disabled' : '' ?> /></div>
                                        <div class="header-row3-beds cap"><input type="number" name="<?php echo'bed_' . 2; ?>" value="<?php echo  ($row['2x2'] == 0) ? '' : $bed_2; ?>" class="beds-txt" max="<?php echo $row['2x2'] ?>" <?php echo  ($row['2x2'] == 0) ? 'disabled' : '' ?>/></div>
                                        <div class="header-row3-beds cap"><input type="number" name="<?php echo'bed_' . 3; ?>" value="<?php echo  ($row['3x3'] == 0) ? '' : $bed_3; ?>" class="beds-txt" max="<?php echo $row['3x3'] ?>" <?php echo  ($row['3x3'] == 0) ? 'disabled' : '' ?>/></div>
                                        <div class="header-row3-beds cap"><input type="number" name="<?php echo'bed_' . 4; ?>" value="<?php echo  ($row['4x4'] == 0) ? '' : $bed_4; ?>" class="beds-txt" max="<?php echo $row['4x4'] ?>" <?php echo  ($row['4x4'] == 0) ? 'disabled' : '' ?>/></div>
                                        <div class="header-row3-beds cap"><input type="number" name="<?php echo'bed_' . 5; ?>" value="<?php echo  ($row['5x5'] == 0) ? '' : $bed_5; ?>" class="beds-txt" max="<?php echo $row['5x5'] ?>" <?php echo  ($row['5x5'] == 0) ? 'disabled' : '' ?>/></div>
                                        <div class="header-row3-beds cap"><input type="number" name="<?php echo'bed_' . 6; ?>" value="<?php echo  ($row['6x6'] == 0) ? '' : $bed_6; ?>" class="beds-txt" max="<?php echo $row['6x6'] ?>" <?php echo  ($row['6x6'] == 0) ? 'disabled' : '' ?>/></div>
                                        <div class="header-row3-beds cap"><input type="number" name="<?php echo'bed_' . 7; ?>" value="<?php echo  ($row['7x7'] == 0) ? '' : $bed_7; ?>" class="beds-txt" max="<?php echo $row['7x7'] ?>" <?php echo  ($row['7x7'] == 0) ? 'disabled' : '' ?>/></div>
                                        <div class="header-row3-beds cap"><input type="number" name="<?php echo'bed_' . 8; ?>" value="<?php echo  ($row['8x8'] == 0) ? '' : $bed_8; ?>" class="beds-txt" max="<?php echo $row['8x8'] ?>" <?php echo  ($row['8x8'] == 0) ? 'disabled' : '' ?>/></div>
                                        <div class="header-row3-save">
                                        <?php
                                          if($archive == 0){
                                        ?>
                                          <input type="submit" name="beds_submit" value="Save" class="beds-btn"/>
                                        <?php 
                                          }
                                          else {
                                        ?>
                                          &nbsp;
                                        <?php
                                          }
                                        ?>
                                        </div>
                                    </form>
                                    <div class="header-row3-percent cap" id="cap-percent-<?php echo  $i; ?>"> 
                                        <?php
                                        echo $percent . "%";
                                        ?>
                                    </div>
                                </div>
                                <div  class="data-header-rows accordion-content">
                                    <div class="notes-info-col1">
                                        <div class="notess-info-contacts">
                                            <div class="contact1"><?php echo $accom_name; ?></div>
                                            <div class="contact1"><?php echo $row['BL_Contact']; ?></div>
                                            <div class="contact1"><a href="tel:<?php echo $row['BL_Phone'] ?>"><?php echo $row['BL_Phone']; ?></a></div>
                                            <div class="contact2"><a href="tel:<?php echo $row['BL_Cell'] ?>"><?php echo $row['BL_Cell']; ?></a></div>
                                        </div>
                                        <div class="notes-address"><?php echo $row['BL_Street']; ?></div>
                                        <div class="notes-city"><?php echo $row['BL_Town']; ?></div>
                                        <div class="notes-email"><a href="mailto:<?php echo $row['BL_Email'] ?>"><?php echo $row['BL_Email'] ?></a></div>
                                        <div class="notes-email"><a href="<?php echo $row['BL_Website'] ?>"><?php echo $row['BL_Website']; ?></a></div>
                                    </div>
                                    <form onsubmit="return block_capacity_update_notes(this)">
                                        <div class="notes-info-col2">
                                            <input type="hidden" name="accom_id" value="<?php echo $accom_id ?>">
                                            <div class="notess-info-features">
                                                <input type="checkbox" <?php echo $accom_sports_team_welcome == 1 ? 'checked' : '' ?> name="sports_team"/><lable for="sports_team">No Sports Teams</lable>
                                            </div>
                                            <div class="notess-info-features">
                                                <input type="checkbox" <?php echo $accom_pets_allowed == 1 ? 'checked' : '' ?> name="pets"/><lable for="pets">Pets Allowed</lable>
                                            </div>
                                            <div class="notess-info-features">
                                                <input type="checkbox" <?php echo $accom_flexible_rental == 1 ? 'checked' : '' ?> name="flexible_rental"/><lable for="flexible_rental">Long Term Rental</lable>
                                            </div>
                                            <?php
                                            if ($cat_id == 59 || $cat_id == 30) {
                                                ?>
                                                <div class="notess-info-features">
                                                    <input type="checkbox" <?php echo $accom_weekly_rental == 1 ? 'checked' : '' ?> name="Weekly_Rental_Only"/><lable for="Weekly_Rental_Only">Weekly Rental Only</lable>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <div class="notes-info-col3">
                                            <div class="notess-info-textarea">
                                                <textarea class='tt-ckeditor' name="notes"><?php echo $accom_notes ?></textarea>
                                            </div>
                                          <?php
                                            if($archive == 0){
                                          ?>
                                            <div class="notess-info-save-note" style="margin-top:10px;">
                                                <input type="submit" name="save_note" class="save-note-btn" value="Save Note"/>
                                            </div>
                                          <?php
                                            }
                                          ?>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    $i++;
                }
            }
        } 
        else if ($cat_id == 29 || $cat_id == 31 || $cat_id == 45) {
            $days = cat_type_header($cat_id, $active_item, $block);
            $hotels = get_listings($cat_id, $reg_id, 1, $filters, $search);
            $i = 1;
            if ($hotels['listings'] != '') {
                foreach ($hotels['listings'] as $row) {
                    $page_counter++;
                    $accom_id = $row['BL_ID'];
                    $accom_name = $row['BL_Listing_Title'];
                    $accom_sports_team_welcome = $row['BL_Sports_Team_Welcome'];
                    $accom_pets_allowed = $row['BL_Pets_Allowed'];
                    $accom_flexible_rental = $row['BL_Flexible_Rental'];
                    $accom_notes = $row['BL_Notes'];
                    $accom_weekly_rental = $row['BL_Weekly_Rental'];
                    $accom_closed = isset($row['closed']) && $row['closed'] != 0 ? 'checked' : '';
                    $accom_general = isset($row['general']) && $row['general'] != NULL ? $row['general'] : 0;
                    $accom_percentage = isset($row['BL_Percentage']) && $row['BL_Percentage'] != NULL ? $row['BL_Percentage'] : '50';
                    $capacity_wrt_group = "SELECT * FROM `acc_capacity_response` WHERE `block_id` = $block_id AND Accommodator_id = $accom_id ";
                    $acc_group_result = mysql_query($capacity_wrt_group);
                    $acc_group = mysql_fetch_array($acc_group_result);
                    $cap_id = isset($acc_group['id']) ? $acc_group['id'] : 0;
                    $notes = isset($acc_group['notes']) && $acc_group['notes'] != '' ? $acc_group['notes'] : '';
                    $sat = isset($acc_group['sat']) ? $acc_group['sat'] : 0;
                    $sun = isset($acc_group['sun']) ? $acc_group['sun'] : 0;
                    $mon = isset($acc_group['mon']) ? $acc_group['mon'] : 0;
                    $tue = isset($acc_group['tue']) ? $acc_group['tue'] : 0;
                    $wed = isset($acc_group['wed']) ? $acc_group['wed'] : 0;
                    $thu = isset($acc_group['thur']) ? $acc_group['thur'] : 0;
                    $fri = isset($acc_group['fri']) ? $acc_group['fri'] : 0;
                    if ($row['general'] == 0) {
                        $disabled = 'disabled';
                        $sat = '';
                        $sun = '';
                        $mon = '';
                        $tue = '';
                        $wed = '';
                        $thu = '';
                        $fri = '';
                    } else {
                        $disabled = '';
                    }
                    foreach ($days as $key => $value) {
                        switch ($key) {
                            case 'Sat':
                                $days['Sat'] = "<div class='header-row3-beds cap'><input type='number' name='sat' value='$sat' class='beds-txt' max='" . $row['general'] . "' " . $disabled . "/></div>";
                                break;
                            case 'Sun':
                                $days['Sun'] = "<div class='header-row3-beds cap'><input type='number' name='sun' value='$sun' class='beds-txt' max='" . $row['general'] . "' " . $disabled . "/></div>";
                                break;
                            case 'Mon':
                                $days['Mon'] = "<div class='header-row3-beds cap'><input type='number' name='mon' value='$mon' class='beds-txt' max='" . $row['general'] . "' " . $disabled . "/></div>";
                                break;
                            case 'Tue':
                                $days['Tue'] = "<div class='header-row3-beds cap'><input type='number' name='tue' value='$tue' class='beds-txt' max='" . $row['general'] . "' " . $disabled . "/></div>";
                                break;
                            case 'Wed':
                                $days['Wed'] = "<div class='header-row3-beds cap'><input type='number' name='wed' value='$wed' class='beds-txt' max='" . $row['general'] . "' " . $disabled . "/></div>";
                                break;
                            case 'Thu':
                                $days['Thu'] = "<div class='header-row3-beds cap'><input type='number' name='thur' value='$thu' class='beds-txt' max='" . $row['general'] . "' " . $disabled . "/></div>";
                                break;
                            case 'Fri':
                                $days['Fri'] = "<div class='header-row3-beds cap'><input type='number' name='fri' value='$fri' class='beds-txt' max='" . $row['general'] . "' " . $disabled . "/></div>";
                                break;
                            default:
                                break;
                        }
                    }
                    
                    //check availability
                    $total_rooms_availability = $sat + $sun + $mon + $tue + $wed + $thu + $fri;
                    $total_capacity = $accom_general;
                    $start_date = $block['start_date'];
                    $end_date = $block['end_date'];
                    $num_days = floor((strtotime($end_date) - strtotime($start_date)) / (60 * 60 * 24)) + 1;
                    $block_aval = $total_capacity * $num_days;
                    if ($block_aval) {
                        $percent = round(($total_rooms_availability / $block_aval) * 100);
                    } else {
                        $percent = 0;
                    }
                    if(isset($closed) && $percent == 0) {
                      continue;
                    }
                    
                    /* Save all Logic */
                    $accommodation_ids[] = $accom_id;
                    ?>
                    <div class="accordion-section">
                        <div class="data-header-rows accordion-header">
                            <div class="header-row3-type-name" title="<?php echo $accom_name; ?>">
                                <?php echo (strlen($accom_name) > 30) ? substr($accom_name, 0, 28) . '...' : $accom_name; ?></div>
                            <div class="header-row3-cap cap"> <?php echo $accom_general; ?> </div>
                            <form id="ajax_group_accom_cap_<?php echo $accom_id; ?>" name="ajax_group_accom_cap_<?php echo $accom_id ?>" onSubmit="return block_capacity_update(this, <?php echo  $i; ?>)">
                                <input type="hidden" name="accom_general" value="<?php echo $accom_general ?>">
                                <input type="hidden" name="block_id" value="<?php echo $block_id ?>">
                                <input type="hidden" name="accom_id" value="<?php echo $accom_id ?>">
                                <input type="hidden" name="cat_id" value="<?php echo $cat_id ?>">
                                <input type="hidden" name="cap_id" value="<?php echo isset($acc_group['id']) ? $acc_group['id'] : 0; ?>">
                                <input type="hidden" name="type" value="0">
                                <?php
                                foreach ($days as $key => $value) {
                                    echo $value;
                                }
                                ?>
                                <div class="header-row3-save">
                                <?php
                                  if($archive == 0){
                                ?>
                                  <input type="submit" name="beds_submit" value="Save" class="beds-btn"/>
                                <?php 
                                  }
                                  else {
                                ?>
                                  &nbsp;
                                <?php
                                  }
                                ?>
                                </div>
                            </form>
                            <div class="header-row3-percent cap" id="cap-percent-<?php echo  $i; ?>"> 
                                <?php
                                echo $percent . "%";
                                ?>
                            </div>
                        </div>
                        <div  class="data-header-rows accordion-content">
                            <div class="notes-info-col1">
                                <div class="notess-info-contacts">
                                    <div class="contact1"><?php echo $accom_name; ?></div>
                                    <div class="contact1"><?php echo $row['BL_Contact']; ?></div>
                                    <div class="contact1"><a href="tel:<?php echo $row['BL_Phone'] ?>"><?php echo $row['BL_Phone']; ?></a></div>
                                    <div class="contact2"><a href="tel:<?php echo $row['BL_Cell'] ?>"><?php echo $row['BL_Cell']; ?></a></div>
                                </div>
                                <div class="notes-address"><?php echo $row['BL_Street']; ?></div>
                                <div class="notes-city"><?php echo $row['BL_Town']; ?></div>
                                <div class="notes-email"><a href="mailto:<?php echo $row['BL_Email'] ?>"><?php echo $row['BL_Email'] ?></a></div>
                                <div class="notes-email"><a href="<?php echo $row['BL_Website'] ?>"><?php echo $row['BL_Website']; ?></a></div>
                            </div>
                            <form onsubmit="return block_capacity_update_notes(this)">
                                <div class="notes-info-col2">
                                    <input type="hidden" name="accom_id" value="<?php echo $accom_id ?>">
                                    <div class="notess-info-features">
                                        <input type="checkbox" <?php echo $accom_sports_team_welcome == 1 ? 'checked' : '' ?> name="sports_team"/><lable for="sports_team">No Sports Teams</lable>
                                    </div>
                                    <div class="notess-info-features">
                                        <input type="checkbox" <?php echo $accom_pets_allowed == 1 ? 'checked' : '' ?> name="pets"/><lable for="pets">Pets Allowed</lable>
                                    </div>
                                    <div class="notess-info-features">
                                        <input type="checkbox" <?php echo $accom_flexible_rental == 1 ? 'checked' : '' ?> name="flexible_rental"/><lable for="flexible_rental">Long Term Rental</lable>
                                    </div>
                                    <?php
                                    if ($cat_id == 59 || $cat_id == 30) {
                                        ?>
                                        <div class="notess-info-features">
                                            <input type="checkbox" <?php echo $accom_weekly_rental == 1 ? 'checked' : '' ?> name="Weekly_Rental_Only"/><lable for="Weekly_Rental_Only">Weekly Rental Only</lable>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <div class="notes-info-col3">
                                    <div class="notess-info-textarea">
                                        <textarea class='tt-ckeditor' name="notes"><?php echo $accom_notes ?></textarea>
                                    </div>
                                  <?php
                                    if($archive == 0){
                                  ?>
                                    <div class="notess-info-save-note" style="margin-top:10px;">
                                        <input type="submit" name="save_note" class="save-note-btn" value="Save Note"/>
                                    </div>
                                  <?php
                                    }
                                  ?>
                                </div>
                            </form>
                        </div>
                    </div>
                    <?php
                    $i++;
                }
            }
        }
        ?>    
    </div>
    <ul id="pagin">
        <?php
        if ($page_counter > 20) {
            echo '<li><span class="current" id="current">1</span></li>';
            for ($index = 1; $index < ceil($page_counter / 20); $index++) {
                $page = $index + 1;
                echo "<li><span href='#'>" . $page . "</span></li>";
            }
        }
        ?>
    </ul>
    <?php
    /* Save all Logic */
    if (!empty($accommodation_ids)) {
        ?>
        <div id="acc_ids" style="display: none;"><?php
    foreach ($accommodation_ids as $key => $value) {
        echo $value . ",";
    };
        ?></div>
        <?php
    }
    ?>
</div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>