<?php
include '../include/config.inc.php';
include '../include/accommodation_functions.php';
require_once '../include/login.inc.php';
require_once '../include/admin/header.php';
include '../include/accommodation_header.php';
?>
<tr>
    <td align="center">
        <table  width="940" border="0" cellspacing="0" cellpadding="0" align="center" class="content">
            <?php if (isset($_SESSION['message'])) { ?>
                <tr>
                    <td class="message">
                        <?php
                        echo $_SESSION['message'];
                        unset($_SESSION['message']);
                        ?>
                    </td>
                </tr>
            <?php } ?>
            <tr>
                <td>
                    <div class="title-row">
                        <div class="title-row-col1">Admin Email Settings</div>
                    </div>
                </td>
            </tr>
            <tr class="data-rows"> 
                <td>
                    <?php
                    if (isset($_GET['block_id']) && isset($_GET['listing_id'])) {
                        $block_data = block_info($_GET['block_id']);
                        print_r($block_data);
                        $block_id = $_GET['block_id'];
                        $listing_id = $_GET['listing_id'];
                        $listing_info = "SELECT DISTINCTROW acc_accommodator_capacity.*, BL_ID, BL_Listing_Title, C_Name, C_Parent, C_ID 
                                        FROM tbl_Business_Listing_Category_Region 
                                        LEFT JOIN tbl_Business_Listing_Category ON BLC_ID = BLCR_BLC_ID
                                        LEFT JOIN tbl_Business_Listing ON BL_ID = BLC_BL_ID 
                                        LEFT JOIN acc_accommodator_capacity ON tbl_Business_Listing.BL_ID = acc_accommodator_capacity.accommodator_id
                                        LEFT JOIN tbl_Category ON tbl_Business_Listing_Category.BLC_C_ID = tbl_Category.C_ID
                                        WHERE BL_ID = $listing_id";
                        $row = mysql_fetch_assoc(mysql_query($listing_info));
                        $cat_id = $row['C_ID'];
                        $active_item = $row['C_Name'];
                        if ($cat_id == 29 || $cat_id == 31 || $cat_id == 45) {
                            ?>
                            <div class="data-container cottage-capacity">
                                <div class="acc_cap_header-rows header-row1-color">
                                    <div class="header-row1-col1"><?php echo $active_item; ?></div>
                                    <div class="header-row1-col1"><?php echo $active_item; ?></div>
                                </div>
                                <div class="acc_cap_header-rows  header-row2-color">
                                    <div class="header-row2-col1">&nbsp;</div>
                                    <div class="header-row2-col2">Closed</div>
                                    <div class="header-row2-col3">Capacity</div>
                                    <div class="header-row2-col4">&nbsp;</div>
                                </div>
                                <?php
                                $accom_id = $row['BL_ID'];
                                $accom_name = $row['BL_Listing_Title'];
                                $cap_id = isset($row['id']) ? $row['id'] : 0;
                                $accom_closed = isset($row['closed']) && $row['closed'] != 0 ? 'checked' : '';
                                $accom_general = isset($row['general']) && $row['general'] != NULL ? $row['general'] : 0;
                                ?>
                                <div class = "acc_cap_header-rows data-rows accordion-section">
                                    <form onsubmit="return accomm_cap_update(this)">
                                        <input type = "hidden" name = "accom_id" value="<?php echo $accom_id ?>" />
                                        <div class = "header-row2-col1"><?php echo $accom_name ?></div>
                                        <div class = "header-row2-col2"><input <?php echo $accom_closed; ?> type = "checkbox" name = "closed" class = "closed-checkbox"/></div>
                                        <div class = "header-row2-col3"><input type = "text" name = "capacity" required value='<?php echo $accom_general; ?>' class = "capacity-txt"/></div>
                                        <div class = "header-row2-col4"><input type = "submit" name = "cap-btn" class = "cap-btn" value = "Save"/></div>
                                    </form>
                                </div>
                            </div>
                            <?php
                        } else if ($cat_id == 59 || $cat_id == 30) {
                            ?>
                            <div class="data-container cottage-capacity">
                                <div class="acc_cap_header-rows header-row1-color">
                                    <div class="header-row1-col1"><?php echo $active_item; ?></div>
                                </div>
                                <div class="acc_cap_header-rows  header-row2-color">
                                    <div class="header-row2-col1">&nbsp;</div>
                                    <div class="header-row2-col2">Closed</div>
                                    <div class="header-row2-col3">1</div>
                                    <div class="header-row2-col3">2</div>
                                    <div class="header-row2-col3">3</div>
                                    <div class="header-row2-col3">4</div>
                                    <div class="header-row2-col3">5</div>
                                    <div class="header-row2-col3">6</div>
                                    <div class="header-row2-col3">7</div>
                                    <div class="header-row2-col3">8</div>
                                    <div class="header-row2-col4">&nbsp;</div>
                                </div>
                                <?php
                                //loop here to fitch data
                                $accom_id = $row['BL_ID'];
                                $accom_name = $row['BL_Listing_Title'];
                                $cap_id = isset($row['id']) ? $row['id'] : 0;
                                $accom_closed = isset($row['closed']) && $row['closed'] != 0 ? 'checked' : '';
                                $accom_1x1 = isset($row['1x1']) && $row['1x1'] != NULL ? $row['1x1'] : 0;
                                $accom_2x2 = isset($row['2x2']) && $row['2x2'] != NULL ? $row['2x2'] : 0;
                                $accom_3x3 = isset($row['3x3']) && $row['3x3'] != NULL ? $row['3x3'] : 0;
                                $accom_4x4 = isset($row['4x4']) && $row['4x4'] != NULL ? $row['4x4'] : 0;
                                $accom_5x5 = isset($row['5x5']) && $row['5x5'] != NULL ? $row['5x5'] : 0;
                                $accom_6x6 = isset($row['6x6']) && $row['6x6'] != NULL ? $row['6x6'] : 0;
                                $accom_7x7 = isset($row['7x7']) && $row['7x7'] != NULL ? $row['7x7'] : 0;
                                $accom_8x8 = isset($row['8x8']) && $row['8x8'] != NULL ? $row['8x8'] : 0;
                                ?>
                                <div class = "acc_cap_header-rows data-rows accordion-section">
                                    <form onsubmit="return accomm_cap_update(this)">
                                        <input type = "hidden" name = "accom_id" value="<?php echo $accom_id ?>" />                                            
                                        <input type = "hidden" name = "block_id" value="<?php echo $block_id ?>" />                                            
                                        <div class = "header-row2-col1"><?php echo $accom_name ?></div>
                                        <div class = "header-row2-col2"><input <?php echo $accom_closed; ?> type = "checkbox" name = "closed" class = "closed-checkbox"/></div>
                                        <div class = "header-row2-col3"><input type = "text" name = "accom_1x1" required value='<?php echo $accom_1x1; ?>' class = "capacity-txt"/></div>
                                        <div class = "header-row2-col3"><input type = "text" name = "accom_2x2" required value='<?php echo $accom_2x2; ?>' class = "capacity-txt"/></div>
                                        <div class = "header-row2-col3"><input type = "text" name = "accom_3x3" required value='<?php echo $accom_3x3; ?>' class = "capacity-txt"/></div>
                                        <div class = "header-row2-col3"><input type = "text" name = "accom_4x4" required value='<?php echo $accom_4x4; ?>' class = "capacity-txt"/></div>
                                        <div class = "header-row2-col3"><input type = "text" name = "accom_5x5" required value='<?php echo $accom_5x5; ?>' class = "capacity-txt"/></div>
                                        <div class = "header-row2-col3"><input type = "text" name = "accom_6x6" required value='<?php echo $accom_6x6; ?>' class = "capacity-txt"/></div>
                                        <div class = "header-row2-col3"><input type = "text" name = "accom_7x7" required value='<?php echo $accom_7x7; ?>' class = "capacity-txt"/></div>
                                        <div class = "header-row2-col3"><input type = "text" name = "accom_8x8" required value='<?php echo $accom_8x8; ?>' class = "capacity-txt"/></div>
                                        <div class = "header-row2-col4"><input type = "submit" name = "cap-btn" class = "cap-btn" value = "Save"/></div>
                                    </form>
                                </div>

                                <?php
                            }
                            ?>
                        </div>
                        <?php
                    } else {
                        ?>
                <tr>
                    <td class="message">
                        <div class='error-message'>Sorry For Inconvenience Please Go to home page. </div>
                    </td>
                </tr>
            <?php } ?>
    </td>
</tr>
</table>
</td>
</tr>
<?PHP
require_once '../include/admin/footer.php';
?>