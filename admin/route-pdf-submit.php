<?PHP
require_once '../include/config.inc.php';
require_once '../include/track-data-entry.php';

if (isset($_POST['op']) && $_POST['op'] == 'save') {
  $IR_ID = $_POST['ir_id'];
  $RID = $_POST['rid'];
  $file_size = $_FILES['file']['size'];
  $sql = "tbl_Individual_Route_Downloads SET ";
  $max_filesize = 5342523;
  $pdf_name = str_replace(' ', '_', $_FILES['file']['name']);
  if ($pdf_name != "") {
    $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_name;
    $filePath = PDF_LOC_ABS . $random_name;
    if ($_FILES["file"]["type"] == "application/pdf") {
      if ($file_size < $max_filesize) {
        move_uploaded_file($_FILES["file"]["tmp_name"], $filePath);
        $sql .= "IRD_Path='$random_name',";
      } else {
        $_SESSION['error'] = 1;
      }
    } else {
      $_SESSION['error'] = 1;
    }
  }
  $sql .= " IRD_Title = '" . $_REQUEST['pdf_title'] . "',
            IRD_IR_ID = '" . $IR_ID . "'";
  if ($_POST['ird_id'] > 0) {
    $sql = "UPDATE " . $sql . " WHERE IRD_ID = '" . encode_strings($_POST['ird_id'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    // TRACK DATA ENTRY
    Track_Data_Entry('Websites', $RID, 'Manage Individual Route', $IR_ID, 'Update PDF', 'super admin');
  } else {
    $sqlMax = "SELECT MAX(IRD_Order) FROM tbl_Individual_Route_Downloads WHERE IRD_IR_ID = '" . encode_strings($BL_ID, $db) . "'";
    $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
    $rowMax = mysql_fetch_row($resultMax);
    $sql .= ", IRD_Order = '" . ($rowMax[0] + 1) . "'";
    $sql = "INSERT " . $sql;
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    // TRACK DATA ENTRY
    Track_Data_Entry('Websites', $RID, 'Manage Individual Route', $IR_ID, 'Add PDF', 'super admin');
  }
  if ($result) {
    $_SESSION['success'] = 1;
  } else {
    $_SESSION['error'] = 1;
  }
  header("Location: route.php?rid=" . $RID . "&id=" . $IR_ID);
  exit();
}
?>