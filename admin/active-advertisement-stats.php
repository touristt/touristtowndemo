<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/adminFunctions.inc.php';

if (!in_array('superadmin', $_SESSION['USER_ROLES'])) {
    header("Location: /admin/");
    exit();
}
require_once '../include/admin/header.php';
$whereClause = '';
if (isset($_REQUEST['srchByLstng']) && $_REQUEST['srchByLstng'] != '') {
    $srchByLstng = $_REQUEST['srchByLstng'];
    $whereClause .= ' AND (R_Name LIKE "%' . $srchByLstng . '%")';
}
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Campaigns</div>
        <div class="link">

        </div>
    </div>
    <div class="left advert-left-nav">
        <?php require_once '../include/nav-B-advertisement-admin.php'; ?>
    </div>
    <div class="right">
        <div class="content-header content-header-search">
            <span>Active Campaigns Stats</span>
            <form id="formSrchLstng" name="formSrchLstng" method="GET" action="active-advertisement-stats.php">
                <input type="text" name="srchByLstng" placeholder="Search by listing">
                <input type="button" onClick="getSubmitForm(this.value)" value="Go">
            </form>
            <form class="export_form_width" method="post" name="export_form" action="export_active_customer_stats.php">
                <input type="hidden" name="region_filter" value="">
                <input type="submit" name="export_stats" value="Export"/>
            </form>
        </div>
        <div class="link-header">

            <form id="frmSort" name="form1" method="GET" action="advertisement.php">
                <input type="hidden" name="status" value="3">
                <?php
                $adTypeDD = "SELECT * FROM tbl_Advertisement_Type WHERE AT_ID NOT IN(3,4)";
                $resType = mysql_query($adTypeDD, $db) or die("Invalid query: $adTypeDD -- " . mysql_error());
                ?>
                <label>
                    <select name="adType" id="adType" onChange="getSubOptions(this.value)">
                        <option value="">Select Type</option>
                        <?php
                        while ($DDType = mysql_fetch_array($resType)) {
                            ?>
                            <option value="<?php echo $DDType['AT_ID'] ?>"><?php echo $DDType['AT_Name'] ?></option>
                        <?php }
                        ?>
                    </select>
                </label>
                <label class="margin-left-label-select">

                    <?php
                    $ddRegion = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Domain != ''";
                    $ddResReg = mysql_query($ddRegion, $db) or die("Invalid query: $ddRegion -- " . mysql_error());
                    ?>
                    <select name="sortregion" id="sortregion" onChange="getSubOptions(this.value)">
                        <option value="">Select Region</option>
                        <?php
                        while ($rowDDRegion = mysql_fetch_assoc($ddResReg)) {
                            ?>
                            <option value="<?php echo $rowDDRegion['R_ID'] ?>"><?php echo $rowDDRegion['R_Name'] ?></option>
                        <?php }
                        ?>
                    </select>
                </label>
                <label class="margin-left-label-select">
                    <select name="categorydd" id="categorydd" onChange="getSubOptions(this.value)">
                        <option value="" selected>Category:</option>
                        <?PHP
                        $sqlCat = "SELECT C_ID, C_Name FROM `tbl_Category` WHERE C_Parent = 0 AND C_Is_Blog != 1 AND C_Is_Product_Web != 1 ORDER BY C_Order ASC";
                        $resultCat = mysql_query($sqlCat, $db) or die("Invalid query: $sqlCat -- " . mysql_error());
                        while ($rowMainCat = mysql_fetch_assoc($resultCat)) {
                            ?>
                            <option value="<?php echo $rowMainCat['C_ID'] ?>"><?php echo $rowMainCat['C_Name'] ?></option>
                            <?PHP
                        }
                        ?>
                    </select>
                </label>
                <label class="margin-left-label-select">
                    <select name="subcategorydd" id="subcategorydd" onChange="getSubOptions(this.value)">
                        <option value="" selected>Sub Category:</option>
                        <?PHP
                        $sqlSubCat = "SELECT C_ID, C_Name FROM `tbl_Category` WHERE C_Parent != 0 ORDER BY C_Name ASC";
                        $resultSubCat = mysql_query($sqlSubCat, $db) or die("Invalid query: $sqlSubCat -- " . mysql_error());
                        while ($rowSubCat = mysql_fetch_assoc($resultSubCat)) {
                            ?>
                            <option value="<?php echo $rowSubCat['C_ID'] ?>"><?php echo $rowSubCat['C_Name'] ?></option>
                            <?PHP
                        }
                        ?>
                    </select>
                </label>
                <label class="margin-left-label-select">
                    <select name="campaign" onChange="getSubOptions(this.value)">
                        <option value="">Sub Campaign:</option>
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </label>
                <script type="text/javascript">
                    function getSubOptions(myVal) {
                        $('#frmSort').submit();
                    }
                    function getSubmitForm(myVal) {
                        $('#formSrchLstng').submit();
                    }
                </script>
            </form>
        </div>
        <div class="content-sub-header padding-none">
            <div class="data-column aas-region">Website</div>
            <?php
            $sql = "SELECT * FROM tbl_Advertisement_Type WHERE AT_ID NOT IN(3,4)";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $count = 0;
            while ($rowAT = mysql_fetch_assoc($result)) { 
                $count++;
                $AT_ID[$count] = $rowAT['AT_ID']; 
                ?>
                <div class="data-column aas-other statistics"><?php echo ($rowAT['AT_ID'] == 4) ? 'Homepage' : $rowAT['AT_Name'] ?></div>
            <?php } ?>
            <div class="data-column aas-other statistics">Total</div>
        </div>
        <?php
        $totalCount = 0;
        $totalRevenue = 0;
        $totalCountTypeBased = array();
        $totalRevenueTypeBased = array();
        $sql = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Domain != '' $whereClause ORDER BY R_Name ASC";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($rowRegion = mysql_fetch_assoc($result)) {
            $totalCountRegionBased = 0;
            $totalRevenueRegionBased = 0;
            ?>
            <div class="data-content">
                <div class="data-column aas-region"><?php echo $rowRegion['R_Name'] ?></div>

                <?php
                for ($i = 1; $i <= $count; $i++) {
                    $record = show_ads_stats($rowRegion['R_ID'], $AT_ID[$i]);
                    $totalCountRegionBased += $record['count'];
                    $totalRevenueRegionBased += $record['revenue'];
                    $totalCountTypeBased[$i] +=  $record['count'];
                    $totalRevenueTypeBased[$i] += $record['revenue'];
                    ?>
                <div class="data-column aas-other statistics">
                        <?php echo $record['count'] . " ($" . $record['revenue'] . ")";
                    ?>
                    </div>
                <?php } ?>

                    <div class="data-column aas-other statistics">
                        <?php 
                    echo $totalCountRegionBased . " ($" . $totalRevenueRegionBased . ")";
                    ?>
                    </div>
                </div>
        <?php }
        ?>
        <div class="data-content aas-total">
            <div class="data-column aas-region">
                <?php
                echo "Totals";
                ?>
            </div>
            <?php
            for ($j = 1; $j <= $count; $j++) {
                $totalCount += $totalCountTypeBased[$j];
                $totalRevenue += $totalRevenueTypeBased[$j];
                    ?>
            <div class="data-column aas-other statistics">
                    <?php echo $totalCountTypeBased[$j] . " ($" . $totalRevenueTypeBased[$j] . ")";
                    ?>
                </div>
            <?php } ?>
            <div class="data-column aas-other statistics">
                <?php echo $totalCount . " ($" . $totalRevenue . ")"; ?>
            </div>
        </div>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>