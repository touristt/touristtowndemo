<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if (!in_array('business-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
} elseif (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
    $regionLimit = array();
    $limits = explode(',', $_SESSION['USER_LIMIT']);
    foreach ($limits as $limit) {
        $sql = "SELECT R_ID, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $region = mysql_fetch_assoc($result);
        if (isset($region['R_Type']) && $region['R_Type'] == 1) {
            $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($region['R_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                if ($row['R_ID'] != '' && !in_array($row['R_ID'], $regionLimit)) {
                    $regionLimit[] = $row['R_ID'];
                }
            }
        } else {
            if ($region['R_ID'] != '' && !in_array($region['R_ID'], $regionLimit)) {
                $regionLimit[] = $region['R_ID'];
            }
        }
    }
}
$regionLimitCommaSeparated = rtrim(implode(',', $regionLimit), ',');

if ($_REQUEST['bl_id'] > 0) {
    $sql = "SELECT B_ID, BL_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
          WHERE BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
    $BL_ID = $rowListing['BL_ID'];
} elseif ($_REQUEST['bid'] > 0) {
    $sql = "SELECT B_ID FROM tbl_Business WHERE B_ID = '" . encode_strings($_REQUEST['bid'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);

    $BID = $rowListing['B_ID'];
} else {
    header("Location: customers.php");
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    if ($_POST['bl_id'] > 0) {
        $id = $_POST['bl_id'];
    } else {
        $sqlMax = "SELECT MAX(BL_Number) FROM tbl_Business_Listing WHERE BL_B_ID = '" . encode_strings($_POST['bid'], $db) . "'";
        $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
        $rowMax = mysql_fetch_row($resultMax);
        $sql = "INSERT tbl_Business_Listing SET 
            BL_Listing_Title = '" . encode_strings($_REQUEST['name'], $db) . "',
            BL_Name_SEO = '" . encode_strings($_REQUEST['seoName'], $db) . "',
            BL_Listing_Type = '" . encode_strings($_REQUEST['type'], $db) . "',
            BL_B_ID = '" . encode_strings($_POST['bid'], $db) . "', 
            BL_Number = '" . encode_strings($rowMax[0] + 1, $db) . "', 
            BL_Creation_Date = CURDATE()";
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id($db);
        $features = "SELECT F_ID FROM  `tbl_Feature` WHERE F_Price = 0";
        $result_fe = mysql_query($features, $db);
        while ($free_feature = mysql_fetch_array($result_fe)) {
            $insert_feature = "INSERT tbl_BL_Feature SET 
                        BLF_BL_ID = '" . encode_strings($id, $db) . "',
                        BLF_F_ID = '" . encode_strings($free_feature['F_ID'], $db) . "',
                        BLF_Date = CURDATE(),
                        BLF_Active = 1,
                        BLF_Last_Update = '" . encode_strings($_SESSION['USER_EM'] . '-' . $_SESSION['user_online'] . '-' . $_SESSION['USER_ID'], $db) . "',
                        BLF_Agreement_Status = '" . encode_strings("1", $db) . "'";
            mysql_query($insert_feature, $db);
        }
        for ($season = 1; $season < 5; $season++) {
            $sql_seasons = "INSERT tbl_Business_Listing_Season SET BLS_S_ID = '$season', 
                BLS_BL_ID = '" . encode_strings($id, $db) . "'";
            $res1_seasons = mysql_query($sql_seasons, $db);
        }
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $id, 'Listing Details', '', 'Add Listing', 'super admin');
    }
    $sql = "DELETE FROM tbl_Business_Listing_Category_Region WHERE BLCR_BL_ID = '" . encode_strings($rowListing['BL_ID'], $db) . "'";
    mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());

    // GETTING THE CURRENT MAIN PHOTO AGAINST ANY REGION OF THIS BL_ID
    $sql = "SELECT * FROM tbl_Business_Listing_Photo WHERE BLP_BL_ID = '" . encode_strings($id, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing1 = mysql_fetch_assoc($result);
    if (is_array($_POST['region'])) {
        foreach ($_POST['region'] as $key => $val) {
            $sql = "INSERT INTO tbl_Business_Listing_Category_Region (BLCR_BL_ID, BLCR_BLC_R_ID)
              VALUES('" . encode_strings($id, $db) . "', '" . encode_strings($val, $db) . "')";
            $result = mysql_query($sql, $db);


            if (count($rowListing1['BLP_BL_ID']) >= 0 && !$_REQUEST['newR'] && $key + 1 == sizeof($_POST['region'])) {
                $sql2 = "INSERT tbl_Business_Listing_Photo SET BLP_Photo = '" . encode_strings($rowListing1['BLP_Photo'], $db) . "',
                    BLP_Mobile_Photo = '" . encode_strings($rowListing1['BLP_Mobile_Photo'], $db) . "',
                    BLP_Header_Image = '" . encode_strings($rowListing1['BLP_Header_Image'], $db) . "',
                    BLP_Mobile_Header_Image = '" . encode_strings($rowListing1['BLP_Mobile_Header_Image'], $db) . "',
                    BLP_BL_ID = '" . encode_strings($id, $db) . "',
                    BLP_R_ID = '" . encode_strings($val, $db) . "' ";
                $result2 = mysql_query($sql2, $db);
                
                $sql_thumb = "SELECT IBU_IB_ID FROM tbl_Image_Bank_Usage WHERE IBU_BL_ID = '" . encode_strings($id, $db) . "' AND IBU_Main_Photo = 'Listing Thumbnail Image' ";
                $result_thumb = mysql_query($sql_thumb, $db) or die("Invalid query: $sql_thumb -- " . mysql_error());
                $row_thumb = mysql_fetch_assoc($result_thumb);
                $thumbnail_id = $row_thumb['IBU_IB_ID'];

                $sql_main = "SELECT IBU_IB_ID FROM tbl_Image_Bank_Usage WHERE IBU_BL_ID = '" . encode_strings($id, $db) . "' AND IBU_Main_Photo = 'Listing Main Image' ";
                $result_main = mysql_query($sql_main, $db) or die("Invalid query: $sql_main -- " . mysql_error());
                $row_main = mysql_fetch_assoc($result_main);
                $mainimage_id = $row_main['IBU_IB_ID']; 
                if($thumbnail_id > 0 ){
                    imageBankUsage($thumbnail_id, 'IBU_BL_ID', $id, 'IBU_Main_Photo', 'Listing Thumbnail Image', 'IBU_R_ID', $val);
                }
                if($mainimage_id > 0){
                imageBankUsage($mainimage_id, 'IBU_BL_ID', $id, 'IBU_Main_Photo', 'Listing Main Image', 'IBU_R_ID', $val);
                }
            }
        }
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $id, 'Listing Details', '', 'Add Region', 'super admin');
    }
    if (!$_REQUEST['newR']) {
        if ($result) {
            $_SESSION['success'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
    }
    if ($_POST['add_new_region'] == 1) {
        header("Location: /admin/customer-listing-regions.php?bl_id=" . $id . "&newR=1");
        exit();
    } else {
        header("Location: /admin/customer-listing-regions.php?bl_id=" . $id . ($_REQUEST['newR'] ? ('&newR=' . $_REQUEST['newR']) : ''));
        exit();
    }
}
if (isset($_REQUEST['delRegion']) && $_REQUEST['delRegion'] > 0) {
    $R_ID = $_REQUEST['rid'];
    $sql1 = "DELETE FROM tbl_Business_Listing_Photo WHERE BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BLP_R_ID = '" . encode_strings($_REQUEST['rid'], $db) . "'";
    $result1 = mysql_query($sql1);

    $sql = "DELETE FROM tbl_Business_Listing_Category_Region WHERE BLCR_ID = '" . encode_strings($_REQUEST['delRegion'], $db) . "'";
    $result = mysql_query($sql, $db);

    // TRACK DATA ENTRY
    $id = $BL_ID;
    Track_Data_Entry('Listing', $id, 'Regions', '', 'Delete Region', 'super admin');
    if ($result) {
        $_SESSION['delete'] = 1;
//        Deleting  both of the main and thumbnail images as the region is no more
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Main_Photo', 'Listing Thumbnail Image', 'IBU_R_ID', $R_ID);
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Main_Photo', 'Listing Main Image', 'IBU_R_ID', $R_ID);
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: /admin/customer-listing-regions.php?bl_id=" . $rowListing['BL_ID']);
    exit();
}
require_once '../include/admin/header.php';
?>

<div class="content-left">

    <?php require_once '../include/top-nav-listing.php'; ?>

    <div class="title-link">
        <div class="title">My Page - <?php echo(($rowListing['BL_Listing_Title'])) ? $rowListing['BL_Listing_Title'] : ''; ?></div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div>
        <div class="instruction">
            Fields with this background<span></span>will show on free listings profile.
        </div>
    </div>

    <div class="left">
        <?PHP
        require '../include/nav-mypage.php';
        ?>
    </div>

    <div class="right">
        <form name="form1" method="post" action="" id="frmRegion">
            <script>
                function makeSEO(myVar) {
                    myVar = myVar.toLowerCase();
                    myVar = myVar.replace(/[^a-z0-9\s-]/gi, '');
                    myVar = myVar.replace(/"/g, '');
                    myVar = myVar.replace(/'/g, '');
                    myVar = myVar.replace(/&/g, '');
                    myVar = myVar.replace(/\//g, '');
                    myVar = myVar.replace(/,/g, '');
                    myVar = myVar.replace(/  /g, ' ');
                    myVar = myVar.replace(/ /g, '-');

                    $('#SEOname').val(myVar);
                    return false;
                }
            </script>
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="bl_id" value="<?php echo (($rowListing['BL_ID'])) ? $rowListing['BL_ID'] : ''; ?>">
            <input type="hidden" name="bid" value="<?php echo $BID ?>">
            <input type="hidden" name="newR" id="newR" value="0">
            <input type="hidden" name="delR" id="delR" value="0">
            <input type="hidden" name="delC" id="delC" value="0">

            <div class="content-header">
                <div class="title">Websites</div>
                <?php if (isset($BL_ID)) { ?>
                    <div class="link"><a href="#" onClick="return addRegion();" class="table-boldtext">+Add Website</a></div>
                <?php } ?>
            </div>

            <?PHP
            if (isset($BL_ID) && $BL_ID > 0) {
                $sql = "SELECT BLCR_BLC_R_ID, BLCR_ID FROM tbl_Business_Listing_Category_Region  WHERE BLCR_BL_ID = " . encode_strings($rowListing['BL_ID'], $db);
                $sql .= $regionLimitCommaSeparated ? (" AND BLCR_BLC_R_ID IN(" . $regionLimitCommaSeparated . ") ") : "";
                $resultRegion = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                while ($rowRegion = mysql_fetch_assoc($resultRegion)) {
                    ?>
                    <?php
                    $sql = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Type NOT IN(1,6)";
                    $sql .= $regionLimitCommaSeparated ? (" AND R_ID IN(" . $regionLimitCommaSeparated . ") ") : "";
                    $sql .= " ORDER BY R_Name";
                    $resultRegionList = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    ?>                                      

                    <div class="form-inside-div">
                        <label>Website</label>
                        <div class="form-data">
                            <select name="region[]" required>
                                <option value="">Select Website</option>
                                <?PHP
                                while ($rowRList = mysql_fetch_assoc($resultRegionList)) {
                                    echo "<option value='" . $rowRList['R_ID'] . "'";
                                    echo $rowRList['R_ID'] == $rowRegion['BLCR_BLC_R_ID'] ? 'selected' : '';
                                    echo ">" . $rowRList['R_Name'] . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <a style="float:left;padding:17px 0 16px 20px;" href="customer-listing-regions.php?bl_id=<?php echo $rowListing['BL_ID'] ?>&delRegion=<?php echo $rowRegion['BLCR_ID'] ?>&rid=<?php echo $rowRegion['BLCR_BLC_R_ID'] ?>" onClick="return delRegion();">Delete</a>
                    </div>
                    <?php
                }
            }
            if (!isset($BL_ID)) {
                ?>
                <div class="form-inside-div">
                    <div class="content-sub-header" style="margin-bottom:10px;">
                        Listing Type
                    </div>
                    <label>Listing Type</label>
                    <div class="form-data">
                        <?php
                        if (in_array('bgadmin', $_SESSION['USER_ROLES'])) {
                            $sql = "SELECT LT_ID, LT_Name FROM tbl_Listing_Type WHERE LT_ID IN (1, 4) ORDER BY LT_Order";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        } else if (in_array('town', $_SESSION['USER_ROLES'])) {
                            $sql = "SELECT LT_ID, LT_Name FROM tbl_Listing_Type WHERE LT_ID IN (1, 5) ORDER BY LT_Order";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        } else {
                            $sql = "SELECT LT_ID, LT_Name FROM tbl_Listing_Type ORDER BY LT_Order";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        }
                        ?>
                        <select name="type" required>
                            <option value="">Select Listing Type</option>
                            <?PHP
                            $js = '';
                            while ($row = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo $row['LT_ID'] ?>" <?php echo ($rowListing['BL_Listing_Type'] == $row['LT_ID']) ? 'selected' : '' ?>><?php echo $row['LT_Name'] ?></option>
                                <?PHP
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>Listing Title</label>
                    <div class="form-data">
                        <input name="name" type="text" value="" size="50" onKeyUp="makeSEO(this.value)" required/>
                    </div>

                </div>
                <div class="form-inside-div">
                    <label>SEO Name</label>
                    <div class="form-data">
                        <input name="seoName" id="SEOname" type="text" value="" size="50" onKeyUp="makeSEO(this.value)" required/>
                    </div>
                </div>
            <?php } if (isset($_REQUEST['newR']) && $_REQUEST['newR'] == 1) { ?>
                <div class="form-inside-div">
                    <label>Website</label>
                    <div class="form-data">
                        <?php
                        $sql = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Type NOT IN(1,6)";
                        $sql .= $regionLimitCommaSeparated ? ("AND R_ID IN(" . $regionLimitCommaSeparated . ") ") : "";
                        $sql .= " ORDER BY R_Name";
                        $resultRegionList = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        ?>
                        <select name="region[]" required>
                            <option value="">Select Website</option>
                            <?PHP
                            while ($rowRList = mysql_fetch_assoc($resultRegionList)) {
                                echo "<option value='" . $rowRList['R_ID'] . "'>" . $rowRList['R_Name'] . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
            <?php } ?>
            <script type="text/javascript">
                function addRegion() {
                    $('#newR').val(1);
                    $('#frmRegion').submit();
                    return false;
                }
                function delRegion() {
                    if (confirm('Are you sure you want to delete this website?')) {
                        return true;
                    }
                    return false;
                }
            </script>
            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" name="button2" value="Save Now">
                </div>
            </div>
        </form>
    </div>
</div>

<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>