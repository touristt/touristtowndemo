<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('manage-cart-items', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

require_once '../include/admin/header.php';
?>

<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Store</div>
        <div class="link">
        </div>
    </div>
    <div class="left"><?PHP require '../include/nav-cart.php'; ?></div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name">Name</div>
            <div class="data-column padding-none spl-other">Price</div>
            <div class="data-column padding-none spl-other">Save</div>
        </div>
        <?PHP
        $sql = "SELECT F_Name, F_Price, F_ID FROM tbl_Feature ORDER by F_ID ASC";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            ?>
            <div class="data-content">
                <div class="data-column spl-name"><?php echo $row['F_Name'] ?></div>
                <div class="data-column spl-other">$<?php echo $row['F_Price'] ?></div>
                <div class="data-column spl-other"><a href="store.php?id=<?php echo $row['F_ID'] ?>">Edit</a></div>
            </div>
            <?PHP
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>