<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
} else {
    header('Location: index.php');
}

require_once '../include/admin/header.php';
?>

<div class="content-left">
    <?php require_once '../include/top-nav-listing.php'; ?>
    <div class="title-link">
        <div class="title">My Page - <?php echo $rowListing['BL_Listing_Title'] ?></div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div>
    </div>

    <div class="left">
        <?PHP
        require '../include/nav-B-admin-statistics.php';
        ?>
    </div>

    <div class="right">
        <div class="content-header">Statistics</div>
        <div class="data padding-none">
            <div class="content-sub-header padding-none">
                <div class="data-column">Year</div>
                <div class="data-column">Month</div>
                <div class="data-column">Listed</div>
                <div class="data-column">Searched</div>
                <div class="data-column">Viewed</div>
            </div>
            <?PHP
            $sql = "SELECT *, DATE_FORMAT(BLS_Month,'%Y') as daYear, DATE_FORMAT(BLS_Month,'%M') as daMonth 
                    FROM tbl_Business_Listing_Stats  
                    WHERE BLS_BL_ID = '" . encode_strings($BL_ID, $db) . "' 
                    AND  BLS_R_ID = '" . encode_strings('0', $db) . "' 
                    ORDER BY BLS_Month DESC";
            $resultCount = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($bizCount = mysql_fetch_assoc($resultCount)) {
                ?>
                <div class="data-content">
                    <div class="data-column"><?php echo $bizCount['daYear'] ?></div>
                    <div class="data-column"><?php echo $bizCount['daMonth'] ?></div>
                    <div class="data-column"><?php echo $bizCount['BLS_Listing_Display'] ?></div>
                    <div class="data-column"><?php echo $bizCount['BLS_Search_Display'] ?></div>
                    <div class="data-column"><?php echo $bizCount['BLS_Profile_Views'] ?></div>
                </div>
            <?PHP } ?>
        </div>
    </div>
</div>

<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>