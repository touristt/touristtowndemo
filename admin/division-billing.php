<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('division-billing', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'save') {
    $billingtype = $_REQUEST['billingType'];
    $listing = $_REQUEST['listing'];
    if ($billingtype == 1) {
        //get listing data
        $sql_profile = "SELECT * FROM tbl_Business_Listing LEFT JOIN payment_profiles ON BL_B_ID = business_id WHERE BL_ID = " . $_REQUEST['listing'] . " LIMIT 1";
        $sql_profile_res = mysql_query($sql_profile);
        $profile = mysql_fetch_assoc($sql_profile_res);
        $customer_id = $profile['BL_B_ID'];
        $BL_ID = $_REQUEST['listing'];
        $customer_name = '';
        $customer = '';
        $item_id = $BL_ID;
        $item_name = $profile['BL_Listing_Title'];
    } else {
        //get division billing data
        $sql_profile = "SELECT * FROM tbl_Division_Billing LEFT JOIN payment_profiles ON DB_Customer_Name = division_id WHERE DB_Customer_Name = '" . $_REQUEST['customer_name'] . "' LIMIT 1";
        $sql_profile_res = mysql_query($sql_profile);
        $profile = mysql_fetch_assoc($sql_profile_res);
        $get_id = "SELECT * FROM tbl_Division_Billing ORDER BY DB_ID DESC LIMIT 1";
        $res_get_id = mysql_query($get_id);
        $row_get_id = mysql_fetch_assoc($res_get_id);
        $customer_id = $_REQUEST['customer_name'];
        $BL_ID = '0';
        $customer_name = $_REQUEST['customer_name'];
        $customer = $_REQUEST['customer'];
        $item_id = $row_get_id['DB_ID'] + 1 + 1;
        $item_name = $customer_name;
    }
    $desc = $_REQUEST['description'];
    $paymentType = $_REQUEST['paymentType'];
    $amount = $_REQUEST['amount'];
    if ($_REQUEST['discount']) {
        $discount = $_REQUEST['discount'];
    } else {
        $discount = '0.00';
    }
    //calculate total amount
    $subtotal = $amount - $discount;
    $tax = round($subtotal * .13, 2);
    $total = $subtotal + $tax;
    $date = $_REQUEST['date'];
    $accManager = $_REQUEST['accountManager'];

    if ($discount <= $amount) {
        if ($paymentType != 4 || $total == 0) {
            $sql = "INSERT tbl_Division_Billing SET 
                    DB_BL_ID = '" . encode_strings($BL_ID, $db) . "', 
                    DB_Customer_Name = '" . encode_strings($customer_name, $db) . "',
                    DB_Description = '" . encode_strings($desc, $db) . "',
                    DB_PT_ID = '" . encode_strings($paymentType, $db) . "',
                    DB_Amount = '" . encode_strings($amount, $db) . "',
                    DB_Discount = '" . encode_strings($discount, $db) . "',
                    DB_Subtotal = '" . encode_strings($subtotal, $db) . "',
                    DB_Tax = '" . encode_strings($tax, $db) . "',
                    DB_Total = '" . encode_strings($total, $db) . "',
                    DB_Date = '" . encode_strings($date, $db) . "',
                    DB_Account_Manager = '" . encode_strings($accManager, $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $_SESSION['transaction_success'] = 1;
            header("Location:division-billing-invoices.php");
            exit;
        } else {
            //make a transaction
            if ($_REQUEST['cc_number'] != '') {
                $card_number = $_REQUEST['cc_number'];
                $first_name = $_REQUEST['first_name'];
                $last_name = $_REQUEST['last_name'];
                $exp_month = $_REQUEST['exp_month'];
                $exp_year = $_REQUEST['exp_year'];
                $expiration_date = $exp_month . "/" . $exp_year;
                $address = $_REQUEST['address'];
                $zip = $_REQUEST['zip'];
                $reg = $_REQUEST['region'];
                // get authorize.net information of the specified region
                $sql_region = "SELECT * FROM tbl_Region WHERE R_ID = $reg LIMIT 1";
                $res_region = mysql_query($sql_region);
                $payment_region = mysql_fetch_assoc($res_region);
                require_once 'payment/config_payment.php';

                // Create new customer profile
                $request = new AuthorizeNetCIM;
                $customerProfile = new AuthorizeNetCustomer;
                $customerProfile->merchantCustomerId = $item_id . time();
                // Add payment profile.
                $paymentProfile = new AuthorizeNetPaymentProfile;
                $paymentProfile->customerType = "individual";
                $paymentProfile->payment->creditCard->cardNumber = $card_number;
                $paymentProfile->payment->creditCard->expirationDate = $expiration_date;
                $paymentProfile->billTo->firstName = $first_name;
                $paymentProfile->billTo->lastName = $last_name;
                $paymentProfile->billTo->address = $address;
                $paymentProfile->billTo->zip = $zip;
                $customerProfile->paymentProfiles[] = $paymentProfile;
                $profile_response = $request->createCustomerProfile($customerProfile, 'testMode');
                $parsedresponse = simplexml_load_string($profile_response->response, "SimpleXMLElement", LIBXML_NOWARNING);
                $profile_id = $parsedresponse->customerProfileId;
                $validationResponses = $profile_response->getValidationResponses();
                foreach ($validationResponses as $vr) {
                    $validate = $vr->approved;
                }
                //save profile if created
                if ($profile_id != '' && $validate == 1) {
                    $customer_profile = $request->getCustomerProfile($profile_id);
                    $parsedprofile = simplexml_load_string($customer_profile->response, "SimpleXMLElement", LIBXML_NOWARNING);
                    $profile_object = $parsedprofile->profile;
                    $payment_profile_id = $profile_object->paymentProfiles->customerPaymentProfileId;
                    $card_num = $profile_object->paymentProfiles->payment->creditCard->cardNumber;
                    $expiry_date = $profile_object->paymentProfiles->payment->creditCard->expirationDate;
                    $profile_email = $profile_object->email;
                    $created_date = date('Y-m-d H:i:s');
                    // insert the newly created profile
                    $lastdate = date('t', strtotime($_POST['exp_year'] . '-' . $_POST['exp_month'] . '-01'));
                    $expiration_card = $_POST['exp_year'] . '-' . $_POST['exp_month'] . '-' . $lastdate;
                    if ($billingtype == 1) {
                        $sql_profile = sprintf("INSERT INTO payment_profiles (profile_id, payment_profile_id, email, card_number, expiry_date, first_name, last_name, created_time, card_expiration, business_id, region, address, zip) VALUES('%d', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%d', '%s', '%s')", $profile_id, $payment_profile_id, $profile_email, $card_num, $expiry_date, mysql_real_escape_string($first_name), mysql_real_escape_string($last_name), $created_date, $expiration_card, $customer_id, $reg, mysql_real_escape_string($address), $zip);
                    } else {
                        $sql_profile = sprintf("INSERT INTO payment_profiles (profile_id, payment_profile_id, email, card_number, expiry_date, first_name, last_name, created_time, card_expiration, division_id, region, address, zip) VALUES('%d', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s', '%s')", $profile_id, $payment_profile_id, $profile_email, $card_num, $expiry_date, mysql_real_escape_string($first_name), mysql_real_escape_string($last_name), $created_date, $expiration_card, mysql_real_escape_string($customer_id), $reg, mysql_real_escape_string($address), $zip);
                    }
                    $result_profile = mysql_query($sql_profile);
                    if ($result_profile) {
                        // get authorize.net information of the specified region
                        $sql_region = "SELECT * FROM tbl_Region WHERE R_ID = $reg LIMIT 1";
                        $res_region = mysql_query($sql_region);
                        $payment_region = mysql_fetch_assoc($res_region);
                        require_once 'payment/config_payment.php';
                        $request = new AuthorizeNetCIM;
                        // Create Auth & Capture Transaction
                        $transaction = new AuthorizeNetTransaction;
                        $transaction->amount = $total;
                        $transaction->customerProfileId = (string) $profile_id;
                        $transaction->customerPaymentProfileId = (string) $payment_profile_id;

                        $lineItem = new AuthorizeNetLineItem;
                        $lineItem->itemId = $item_id;
                        $lineItem->name = "Division Billing";
                        $lineItem->description = $item_name;
                        $lineItem->quantity = "1";
                        $lineItem->unitPrice = $total;
                        $lineItem->taxable = "false";
                        $transaction->lineItems[] = $lineItem;

                        $response = $request->createCustomerProfileTransaction("AuthCapture", $transaction);
                        $encoded = json_encode($response);
                        $parsedresponse = simplexml_load_string($response->response, "SimpleXMLElement", LIBXML_NOWARNING);
                        $directResponseFields = explode(",", $parsedresponse->directResponse);
                        $responseCode = $directResponseFields[0]; // 1 = Approved 2 = Declined 3 = Error
                        $cardType = $directResponseFields[51]; // Card Type
                        if ($responseCode == 1) {
                            $sql = "INSERT tbl_Division_Billing SET 
                                    DB_BL_ID = '" . encode_strings($BL_ID, $db) . "', 
                                    DB_Customer_Name = '" . encode_strings($customer_name, $db) . "', 
                                    DB_Description = '" . encode_strings($desc, $db) . "',
                                    DB_PT_ID = '" . encode_strings($paymentType, $db) . "',
                                    DB_Amount = '" . encode_strings($amount, $db) . "',
                                    DB_Discount = '" . encode_strings($discount, $db) . "',
                                    DB_Subtotal = '" . encode_strings($subtotal, $db) . "',
                                    DB_Tax = '" . encode_strings($tax, $db) . "',
                                    DB_Total = '" . encode_strings($total, $db) . "',
                                    DB_Date = '" . encode_strings($date, $db) . "',
                                    DB_Transaction = '" . encode_strings($encoded, $db) . "',
                                    DB_Transaction_ID = '" . encode_strings($directResponseFields[6], $db) . "',
                                    DB_Card_Type = '" . encode_strings($cardType, $db) . "',
                                    DB_Account_Manager = '" . encode_strings($accManager, $db) . "',
                                    DB_Card_Number = '" . encode_strings($card_num, $db) . "',
                                    DB_First_Name = '" . encode_strings($first_name, $db) . "',
                                    DB_Last_Name = '" . encode_strings($last_name, $db) . "',
                                    DB_Profile_Id = '" . $profile_id . "',
                                    DB_Payment_Profile = '" . $payment_profile_id . "'";
                            $result = mysql_query($sql, $db);
                            $_SESSION['transaction_success'] = 1;
                            header("Location:division-billing-invoices.php");
                            exit;
                        } else {
                            // delete existing profiles for this listing
                            $sql_delete = sprintf("DELETE FROM payment_profiles WHERE division_id > 0 AND division_id = '%d'", $customer_id);
                            mysql_query($sql_delete);
                            $file = fopen('payment/authorize.log', 'a');
                            $message = date('Y-m-d H:i:s') . ':  Division Billing Listing Name: ' . $profile['BL_Listing_Title'] . '. Error: ' . $encoded . PHP_EOL;
                            fwrite($file, $message);
                            fclose($file);
                            $_SESSION['transaction_error'] = 1;
                            $_SESSION['transaction_error_msg'] = $directResponseFields[3];
                        }
                    } else {
                        $file = fopen('authorize.log', 'a');
                        $message = date('Y-m-d H:i:s') . ':  Creating profile: admin/division_billing.php Response from local sql query: ' . $sql_profile . PHP_EOL;
                        fwrite($file, $message);
                        fclose($file);
                        $_SESSION['error'] = 2;
                    }
                } else {
                    $file = fopen('authorize.log', 'a');
                    $message = json_encode($profile_response);
                    fwrite($file, "\n" . date("Y-m-d H:i:s") . ": admin/division_billing.php : response from authorize.net: " . $message);
                    fclose($file);
                    $_SESSION['error'] = 1;
                }
            } else {
                // get authorize.net information of the specified region
                if (mysql_num_rows($sql_profile_res) > 0) {
                    $sql_region = "SELECT * FROM tbl_Region WHERE R_ID = " . $profile['region'] . " LIMIT 1";
                    $res_region = mysql_query($sql_region);
                    $payment_region = mysql_fetch_assoc($res_region);
                    require_once 'payment/config_payment.php';

                    $request = new AuthorizeNetCIM;
                    // Create Auth & Capture Transaction
                    $transaction = new AuthorizeNetTransaction;
                    $transaction->amount = $total;
                    $transaction->customerProfileId = $profile['profile_id'];
                    $transaction->customerPaymentProfileId = $profile['payment_profile_id'];

                    $lineItem = new AuthorizeNetLineItem;
                    $lineItem->itemId = $item_id;
                    $lineItem->name = "Division Billing";
                    $lineItem->description = $item_name;
                    $lineItem->quantity = "1";
                    $lineItem->unitPrice = $total;
                    $lineItem->taxable = "false";
                    $transaction->lineItems[] = $lineItem;

                    $response = $request->createCustomerProfileTransaction("AuthCapture", $transaction, 'x_duplicate_window=0');
                    $encoded = json_encode($response);
                    $parsedresponse = simplexml_load_string($response->response, "SimpleXMLElement", LIBXML_NOWARNING);
                    $directResponseFields = explode(",", $parsedresponse->directResponse);
                    $responseCode = $directResponseFields[0]; // 1 = Approved 2 = Declined 3 = Error
                    $cardType = $directResponseFields[51]; // Card Type
                    if ($responseCode == 1) {
                        $sql = "INSERT tbl_Division_Billing SET 
                                DB_BL_ID = '" . encode_strings($BL_ID, $db) . "', 
                                DB_Customer_Name = '" . encode_strings($customer_name, $db) . "', 
                                DB_Description = '" . encode_strings($desc, $db) . "',
                                DB_PT_ID = '" . encode_strings($paymentType, $db) . "',
                                DB_Amount = '" . encode_strings($amount, $db) . "',
                                DB_Discount = '" . encode_strings($discount, $db) . "',
                                DB_Subtotal = '" . encode_strings($subtotal, $db) . "',
                                DB_Tax = '" . encode_strings($tax, $db) . "',
                                DB_Total = '" . encode_strings($total, $db) . "',
                                DB_Date = '" . encode_strings($date, $db) . "',
                                DB_Transaction = '" . encode_strings($encoded, $db) . "',
                                DB_Transaction_ID = '" . encode_strings($directResponseFields[6], $db) . "',
                                DB_Card_Type = '" . encode_strings($cardType, $db) . "',
                                DB_Account_Manager = '" . encode_strings($accManager, $db) . "',
                                DB_Card_Number = '" . encode_strings($profile['card_number'], $db) . "',
                                DB_First_Name = '" . encode_strings($profile['first_name'], $db) . "',
                                DB_Last_Name = '" . encode_strings($profile['last_name'], $db) . "',
                                DB_Profile_Id = '" . $profile['profile_id'] . "',
                                DB_Payment_Profile = '" . $profile['payment_profile_id'] . "'";
                        $result = mysql_query($sql, $db);
                        $_SESSION['transaction_success'] = 1;
                        header("Location:division-billing-invoices.php");
                        exit;
                    } else {
                        $file = fopen('payment/authorize.log', 'a');
                        $message = date('Y-m-d H:i:s') . ':  Division Billing Listing Name: ' . $profile['BL_Listing_Title'] . '. Error: ' . $encoded . PHP_EOL;
                        fwrite($file, $message);
                        fclose($file);
                        $_SESSION['transaction_error'] = 1;
                        $_SESSION['transaction_error_msg'] = $directResponseFields[3];
                    }
                }
            }
        }
    } else {
        $_SESSION['discount_error'] = 1;
    }
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Division Billing</div>
        <div class="link">
        </div>         
    </div>

    <div class="left">
        <?php require_once('../include/nav-home-sub.php'); ?>
    </div>

    <div class="right">
        <form name="form1" method="post" action="">
            <input type="hidden" name="op" value="save">
            <div class="content-header">Make a Billing</div>

            <div class="form-inside-div form-inside-div-width-admin">
                <label>Billing type</label>
                <div class="form-data">                                     
                    <select name="billingType" id="billing-type" onchange="check_billing_type(this.value)" required>
                        <option value="" <?php echo (!isset($billingtype)) ? 'selected' : '' ?>>Select billing type</option>
                        <option value="1" <?php echo (isset($billingtype) && $billingtype == 1) ? 'selected' : '' ?>>Listing</option>
                        <option value="2" <?php echo (isset($billingtype) && $billingtype == 2) ? 'selected' : '' ?>>Non Listing</option>
                    </select>  
                </div>
            </div>

            <div class="form-inside-div image-bank-autocomplete form-inside-div-width-admin" id="listing" style="<?php echo (isset($billingtype) && $billingtype == '1') ? 'display:block;' : 'display:none;' ?>">
                <label>Listings</label>
                <div class="form-data listings_count">
                    <input id="autocomplete_lisitngs" type="text"/>
                    <input type="hidden" id="listing_search" name="listing" value="<?php echo (isset($listing) && $listing != '') ? $listing : '' ?>">
                    <div id="auto-complete-div" style="display: none;"></div>
                    <?php
                    $key_name_listings = "";
                    if (isset($profile['BL_Listing_Title']) && $profile['BL_Listing_Title'] != "") {
                        $key_name_listings = "[";
                        $key_name_listings .= '{id:"' . $profile['BL_ID'] . '", name:"' . $profile['BL_Listing_Title'] . '"}';
                        $key_name_listings .= "]";
                    }
                    ?>
                </div>
            </div>

            <div class="form-inside-div image-bank-autocomplete form-inside-div-width-admin" id="customer" style="<?php echo (isset($billingtype) && $billingtype == '2') ? 'display:block;' : 'display:none;' ?>">
                <label>Customer</label>
                <div class="form-data listings_count">
                    <input id="autocomplete_customer" type="text"/>
                    <input type="hidden" id="customer_search" name="customer" value="<?php echo (isset($customer) && $customer != '') ? $customer : '' ?>">
                    <input type="hidden" id="customer_name" name="customer_name" value="<?php echo (isset($customer_name) && $customer_name != '') ? $customer_name : '' ?>">
                    <?php
                    $key_name_customer = "";
                    if (isset($profile['DB_Customer_Name']) && $profile['DB_Customer_Name'] != "") {
                        $key_name_customer = "[";
                        $key_name_customer .= '{id:"' . $profile['DB_ID'] . '", name:"' . $profile['DB_Customer_Name'] . '"}';
                        $key_name_customer .= "]";
                    }
                    ?>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin">
                <label>Description</label>
                <div class="form-data">
                    <textarea name="description" cols="65" rows="10" id="description-listing"><?php echo (isset($desc) && $desc != '') ? $desc : '' ?></textarea>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin">
                <label>Payment type</label>
                <div class="form-data">                                     
                    <select name="paymentType" id="select" onchange="check_payment_type(this.value)" required>
                        <option value="">Pay By</option>
                        <?PHP
                        $sql = "SELECT * FROM tbl_Payment_Type WHERE PT_ID != 1 ORDER BY PT_Name";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_assoc($result)) {
                            echo '<option value="' . $row['PT_ID'] . '"';
                            echo (isset($paymentType) && $paymentType == $row['PT_ID']) ? 'selected' : '';
                            echo '>' . $row['PT_Name'] . '</option>';
                        }
                        ?>
                    </select>  
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin" id="cc_number" style="<?php echo (isset($card_number) && $card_number != '') ? 'display:block;' : 'display:none;' ?>">
                <label>Card Number</label>
                <div class="form-data">
                    <input type="number" placeholder="Enter Card Number" name="cc_number" value="<?php echo (isset($card_number) && $card_number != '') ? $card_number : '' ?>" id="cc_number_input"/>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin" id="first_name" style="<?php echo (isset($first_name) && $first_name != '') ? 'display:block;' : 'display:none;' ?>">
                <label>First Name</label>
                <div class="form-data">
                    <input type="text" placeholder="Enter First Name" name="first_name" value="<?php echo (isset($first_name) && $first_name != '') ? $first_name : '' ?>" id="first_name_input"/>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin" id="last_name" style="<?php echo (isset($last_name) && $last_name != '') ? 'display:block;' : 'display:none;' ?>">
                <label>Last Name</label>
                <div class="form-data">
                    <input type="text" placeholder="Enter Last Name" name="last_name" value="<?php echo (isset($last_name) && $last_name != '') ? $last_name : '' ?>" id="last_name_input"/>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin" id="cc_exp_date" style="<?php echo (isset($card_number) && $card_number != '') ? 'display:block;' : 'display:none;' ?>">
                <label>Expiration Date</label>
                <div class="form-data">
                    <select name="exp_month" id="exp_month">
                        <option value="" <?php echo (!isset($exp_month)) ? 'selected' : '' ?>>Month</option>
                        <option value="01" <?php echo (isset($exp_month) && $exp_month == 01) ? 'selected' : '' ?>>01</option>
                        <option value="02" <?php echo (isset($exp_month) && $exp_month == 02) ? 'selected' : '' ?>>02</option>
                        <option value="03" <?php echo (isset($exp_month) && $exp_month == 03) ? 'selected' : '' ?>>03</option>
                        <option value="04" <?php echo (isset($exp_month) && $exp_month == 04) ? 'selected' : '' ?>>04</option>
                        <option value="05" <?php echo (isset($exp_month) && $exp_month == 05) ? 'selected' : '' ?>>05</option>
                        <option value="06" <?php echo (isset($exp_month) && $exp_month == 06) ? 'selected' : '' ?>>06</option>
                        <option value="07" <?php echo (isset($exp_month) && $exp_month == 07) ? 'selected' : '' ?>>07</option>
                        <option value="08" <?php echo (isset($exp_month) && $exp_month == 08) ? 'selected' : '' ?>>08</option>
                        <option value="09" <?php echo (isset($exp_month) && $exp_month == 09) ? 'selected' : '' ?>>09</option>
                        <option value="10" <?php echo (isset($exp_month) && $exp_month == 10) ? 'selected' : '' ?>>10</option>
                        <option value="11" <?php echo (isset($exp_month) && $exp_month == 11) ? 'selected' : '' ?>>11</option>
                        <option value="12" <?php echo (isset($exp_month) && $exp_month == 12) ? 'selected' : '' ?>>12</option>
                    </select>
                    <select name="exp_year" id="exp_year">
                        <option value="" <?php echo (!isset($exp_year)) ? 'selected' : '' ?>>Year</option>
                        <option value="15" <?php echo (isset($exp_year) && $exp_year == 15) ? 'selected' : '' ?>>2015</option>
                        <option value="16" <?php echo (isset($exp_year) && $exp_year == 16) ? 'selected' : '' ?>>2016</option>
                        <option value="17" <?php echo (isset($exp_year) && $exp_year == 17) ? 'selected' : '' ?>>2017</option>
                        <option value="18" <?php echo (isset($exp_year) && $exp_year == 18) ? 'selected' : '' ?>>2018</option>
                        <option value="19" <?php echo (isset($exp_year) && $exp_year == 19) ? 'selected' : '' ?>>2019</option>
                        <option value="20" <?php echo (isset($exp_year) && $exp_year == 20) ? 'selected' : '' ?>>2020</option>
                    </select>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin" id="address" style="<?php echo (isset($address) && $address != '') ? 'display:block;' : 'display:none;' ?>">
                <label>Address</label>
                <div class="form-data">
                    <input type="text" placeholder="Enter Address" name="address" value="<?php echo (isset($address) && $address != '') ? $address : '' ?>" id="address_input"/>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin" id="zip" style="<?php echo (isset($zip) && $zip != '') ? 'display:block;' : 'display:none;' ?>">
                <label>Postal Code</label>
                <div class="form-data">
                    <input type="text" placeholder="Enter Postal Code" name="zip" value="<?php echo (isset($zip) && $zip != '') ? $zip : '' ?>" id="zip_input"/>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin" id="region" style="<?php echo (isset($reg) && $reg != '') ? 'display:block;' : 'display:none;' ?>">
                <label>Region</label>
                <div class="form-data">
                    <select name="region" id="region_input">
                        <option value="">Select Region</option>
                        <?php
                        $sql = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_API_Login_ID != '' AND R_Transaction_Key != ''";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($region = mysql_fetch_assoc($result)) {
                            if (isset($reg) && $region['R_ID'] == $reg) {
                                $selected = 'selected';
                            } else {
                                $selected = '';
                            }
                            print '<option value="' . $region['R_ID'] . '" ' . $selected . '>' . $region['R_Name'] . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin">
                <label>Amount<span>$</span></label>
                <div class="form-data">
                    <input name="amount" type="text" value="<?php echo (isset($amount) && $amount != '') ? $amount : '' ?>" id="inputAmount" onkeyup="calcTotal()" required/>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin">
                <label>Discount<span>$</span></label>
                <div class="form-data">
                    <input name="discount" type="text" value="<?php echo (isset($discount) && $discount != '') ? $discount : '' ?>" id="inputDiscount" onkeyup="calcTotal()"/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Total<span>$</span></label>
                <div class="form-data">
                    <input name="total" type="text" value="" id="inputTotal" disabled/>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin">
                <label>Date</label>
                <div class="form-data">
                    <input name="date" type="text" value="<?php echo (isset($date) && $date != '') ? $date : '' ?>" class="datepicker" required/>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin">
                <label>Acc. Manager</label>
                <div class="form-data"> 
                    <select name="accountManager" required>
                        <option value="">Select Account Manager</option>
                        <?PHP
                        $sql = "SELECT U_ID, U_F_Name, U_L_Name FROM tbl_User WHERE U_Commission != 0 ORDER BY U_ID";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_assoc($result)) {
                            echo '<option value="' . $row['U_ID'] . '"';
                            echo (isset($accManager) && $accManager == $row['U_ID']) ? 'selected' : '';
                            echo '>' . $row['U_F_Name'] . ' ' . $row['U_L_Name'] . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" name="button2" value="Pay"/>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    function show_cc_fields() {
        $('#cc_number').css('display', 'block');
        $('#first_name').css('display', 'block');
        $('#last_name').css('display', 'block');
        $('#cc_exp_date').css('display', 'block');
        $('#address').css('display', 'block');
        $('#zip').css('display', 'block');
        $('#region').css('display', 'block');
        $('#cc_number_input').attr("required", "required");
        $('#first_name_input').attr("required", "required");
        $('#last_name_input').attr("required", "required");
        $('#exp_month').attr("required", "required");
        $('#exp_year').attr("required", "required");
        $('#address_input').attr("required", "required");
        $('#zip_input').attr("required", "required");
        $('#region_input').attr("required", "required");
    }
    function hide_cc_fields() {
        $('#cc_number').css('display', 'none');
        $('#first_name').css('display', 'none');
        $('#last_name').css('display', 'none');
        $('#cc_exp_date').css('display', 'none');
        $('#address').css('display', 'none');
        $('#zip').css('display', 'none');
        $('#region').css('display', 'none');
        $('#cc_number_input').removeAttr("required");
        $('#cc_number_input').val("");
        $('#first_name_input').removeAttr("required");
        $('#first_name_input').val("");
        $('#last_name_input').removeAttr("required");
        $('#last_name_input').val("");
        $('#exp_month').removeAttr("required");
        $('#exp_month').val("");
        $('#exp_year').removeAttr("required");
        $('#exp_year').val("");
        $('#address_input').removeAttr("required");
        $('#address_input').val("");
        $('#zip_input').removeAttr("required");
        $('#zip_input').val("");
        $('#region_input').removeAttr("required");
        $('#region_input').val("");
    }
    function check_billing_type(billing_type_id) {
        var listing = $('#listing_search').val();
        var customer = $('#customer_search').val();
        hide_cc_fields();
        if (billing_type_id == 1) {
            $('#listing').css('display', 'block');
            $('#customer').css('display', 'none');
            if (listing == '') {
                $('#token-input-autocomplete_lisitngs').attr("required", "required");
            }
            $('#customer_search').val("");
            $('#autocomplete_lisitngs').val("");
            $('#token-input-autocomplete_customer').removeAttr("required");
        } else {
            $('#listing').css('display', 'none');
            $('#customer').css('display', 'block');
            if (customer == '') {
                $('#token-input-autocomplete_customer').attr("required", "required");
            }
            $('#listing_search').val("");
            $('#autocomplete_customer').val("");
            $('#token-input-autocomplete_lisitngs').removeAttr("required");
        }
    }
    function check_payment_type(pay_by) {
        var listing = $('#listing_search').val();
        var customer = $('#customer_name').val();
        var billing_type = $('#billing-type').val();
        if (pay_by == 4) {
            if (billing_type == 1) {
                if (listing > 0) {
                    $.ajax({
                        type: "GET",
                        url: "check-cc-details.php",
                        data: {
                            listing: 1,
                            bl_id: listing
                        }
                    })
                            .done(function (msg) {
                                if (msg == 0) {
                                    show_cc_fields();
                                }
                            });
                }
            }
            if (billing_type == 2) {
                if (customer != '') {
                    $.ajax({
                        type: "GET",
                        url: "check-cc-details.php",
                        data: {
                            listing: 0,
                            c_id: customer
                        }
                    })
                            .done(function (msg) {
                                if (msg == 0) {
                                    show_cc_fields();
                                }
                            });
                } else {
                    show_cc_fields();
                }
            } else {
                hide_cc_fields();
            }
        } else {
            hide_cc_fields();
        }
    }
    function calcTotal() {
        var amount = $('#inputAmount').val();
        var discount = $('#inputDiscount').val();
        var total = amount - discount;
        if (total >= 0) {
            $('#inputTotal').val(total.toFixed(2));
        } else {
            swal("Error", "Discount must be less than amount!", "error");
        }
    }
    $(function ()
    {
        //autocomplete_listings
        $("#autocomplete_lisitngs").tokenInput(<?php include '../include/autocomplete_lisitngs_without_cat.php'; ?>, {
            onAdd: function (item_lisitngs) {
                var pay_by = $('#select').val();
                if (pay_by == 4) {
                    $.ajax({
                        type: "GET",
                        url: "check-cc-details.php",
                        data: {
                            listing: 1,
                            bl_id: item_lisitngs.id
                        }
                    }).done(function (msg) {
                                if (msg == 0) {
                                    show_cc_fields();
                                }
                            });
                }
                var list_val = $('#listing_search').val();
                if (list_val != '') {
                    $('#listing_search').val(list_val + "," + item_lisitngs.id);
                } else {
                    $('#listing_search').val(item_lisitngs.id);
                }
                $('#token-input-autocomplete_lisitngs').removeAttr("required");
            },
            onDelete: function (item_lisitngs) {
                hide_cc_fields();
                var value_listings = $('#listing_search').val().split(",");
                $('#listing_search').empty();
                var data_listings = "";
                $.each(value_listings, function (key, value_listings) {
                    if (value_listings != item_lisitngs.id) {
                        if (data_listings != '') {
                            data_listings = data_listings + "," + value_listings;
                        } else {
                            data_listings = value_listings;
                        }
                    }
                });
                $('#listing_search').val(data_listings);
                $('#token-input-autocomplete_lisitngs').attr("required", "required");
            },
            resultsLimit: 10,
            preventDuplicates: true,
            tokenLimit: 1,
            prePopulate: <?php echo ($key_name_listings != "") ? $key_name_listings : "\"\"" ?>
        }
        );

        //autocomplete_customer
        $("#autocomplete_customer").tokenInput(<?php include '../include/autocomplete_division_billing_customer.php'; ?>, {
            onResult: function (item) {
                if ($.isEmptyObject(item)) {
                    return [{id: '0', name: $("tester").text()}]
                } else {
                    item.unshift({"name": $("tester").text()});
                    var lookup = {};
                    var result = [];

                    for (var temp, i = 0; temp = item[i++]; ) {
                        var name = temp.name;
                        var id = temp.id;

                        if (!(name in lookup)) {
                            lookup[name] = 1;
                            result.push({"id": id, "name": name});
                        }
                    }
                    return result;
                }

            },
            onAdd: function (item_customer) {
                var pay_by = $('#select').val();
                if (pay_by == 4) {
                    $.ajax({
                        type: "GET",
                        url: "check-cc-details.php",
                        data: {
                            listing: 0,
                            c_id: item_customer.name
                        }
                    })
                            .done(function (msg) {
                                if (msg == 0) {
                                    show_cc_fields();
                                }
                            });
                }
                $('#customer_search').val(item_customer.id);
                $('#customer_name').val(item_customer.name);
                $('#token-input-autocomplete_customer').removeAttr("required");
            },
            onDelete: function (item_customer) {
                hide_cc_fields();
                $('#customer_search').val();
                $('#customer_name').val();
                $('#token-input-autocomplete_customer').attr("required", "required");
            },
            resultsLimit: 10,
            preventDuplicates: true,
            tokenLimit: 1,
            prePopulate: <?php echo ($key_name_customer != "") ? $key_name_customer : "\"\"" ?>
        }
        );
        $('.token-input-dropdown').css("width", "320px");
    })
</script>

<?PHP
if (isset($_SESSION['transaction_error']) && $_SESSION['transaction_error'] == 1) {
    if ($_SESSION['transaction_error_msg'] != "" || strlen($_SESSION['transaction_error_msg']) == "1") {
        echo '<script>
            swal({
                    title: "Transaction error",   
                    text: "Payment has not received due to: ' . $_SESSION['transaction_error_msg'] . '",   
                    type: "error",
                    html: true
                })
          </script>';
    } else {
        print '<script>swal("Transaction error", "Payment has not received due to: Unkown error.", "error");</script>';
    }
    unset($_SESSION['transaction_error_msg']);
    unset($_SESSION['transaction_error']);
}
if (isset($_SESSION['discount_error']) && $_SESSION['discount_error'] == 1) {
    print '<script>swal("Error", "Discount must be less than amount!", "error");</script>';
    unset($_SESSION['discount_error']);
}
if (isset($_SESSION['success']) && $_SESSION['success'] == 1) {
    print '<script>swal("Data Saved", "Data has been saved successfully.", "success");</script>';
    unset($_SESSION['success']);
}
if (isset($_SESSION['error']) && $_SESSION['error'] == 1) {
    print '<script>swal("Error", "We were unable to process your credit card. Please carefully review your credit card information and try again.", "error");</script>';
    unset($_SESSION['error']);
}
if ($_SESSION['error'] == 2) {
    print '<script>swal("Error", "Error saving data. Please try again.", "error");</script>';
    unset($_SESSION['error']);
}
require_once '../include/admin/footer.php';
?>