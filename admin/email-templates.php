<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
$template_id = 0;
$where = "";
if (isset($_GET['id']) && $_GET['id'] > 0) {
    $template_id = $_GET['id'];
    $where = " WHERE ET_ID = '" . encode_strings($template_id, $db) . "'";
}

if (isset($_POST['op']) && $_POST['op'] == "save") {
    $update = " UPDATE tbl_Email_Templates SET 
                ET_Template = '" . encode_strings($_REQUEST['email_template'], $db) . "',
                ET_Subject = '" . encode_strings($_REQUEST['subject'], $db) . "',
                ET_Footer = '" . encode_strings($_REQUEST['footer'], $db) . "'
                WHERE ET_ID = '" . encode_strings($_POST['et_id'], $db) . "'";
    $res = mysql_query($update, $db) or die("Invalid query: $update -- " . mysql_error());
    if ($res) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY 
        $id = $_REQUEST['et_id'];
        Track_Data_Entry('Email', '', 'Notification Messages', $id, 'Update', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/email-templates.php");
    exit();
}

$sql = "SELECT * FROM tbl_Email_Templates $where";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Emails - Notification Messages</div>
        <div class="link">
        </div>
    </div>
    <div class="left"><?php require_once('../include/nav-home-sub.php'); ?></div>
    <div class="right">
        <?php if ($template_id <= 0) { ?>  
            <div class="content-sub-header">
                <div class="data-column padding-none aepl-title">Templates</div>
            </div>
            <?PHP
            while ($row = mysql_fetch_assoc($result)) {
                ?>
                <div class="data-content">
                    <div class="data-column aepl-title">
                        <a href="email-templates.php?id=<?php echo $row['ET_ID'] ?>"><?php echo $row['ET_Name']; ?></a>
                    </div>
                </div>
                <?php
            }
        } else {
            $row = mysql_fetch_assoc($result);
            ?>
            <div class="content-sub-header">
                <div class="data-column padding-none aepl-title"><?php echo $row['ET_Name']; ?></div>
            </div>
            <form name="form1" method="post" action="">
                <input type="hidden" name="op" value="save">
                <input type="hidden" name="et_id"  value="<?php echo $row['ET_ID'] ?>">
                <div class="form-inside-div form-inside-div-width-admin">
                    <label>Email  Subject</label>
                    <div class="form-data">
                        <input type="text" size="40" value="<?php echo $row['ET_Subject'] ?>" id="subject" name="subject">
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin">
                    <label>Email  Body</label>
                    <div class="form-data">
                        <textarea name="email_template" class="tt-ckeditor" cols="40" rows="7" id="email-template"><?php echo $row['ET_Template'] ?></textarea>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin">
                    <label>Footer</label>
                    <div class="form-data">
                        <textarea name="footer" class="tt-ckeditor" cols="40" rows="7"><?php echo $row['ET_Footer'] ?></textarea>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin border-none">
                    <div class="button">
                        <input type="submit" name="button" id="button" value="Save Template" />
                    </div>
                </div>
            </form>
            <?php
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>