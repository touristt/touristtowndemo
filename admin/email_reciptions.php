<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('manage-users', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Emails - Email Notification Recipients</div>
        <div class="link">
            <a href="add_email_reciption.php">+Add Email Recipient</a>
        </div>
    </div> 
    <div class="left"><?php require_once('../include/nav-home-sub.php');  ?></div>
    <div class="right">
        <?php
        $role = "SELECT * FROM tbl_Email_Recipients_Type INNER JOIN tbl_Email_Recipients on ER_Type = ERT_ID ORDER BY ERT_Title ASC ";
        $roleResult = mysql_query($role, $db) or die("Invalid query: $role -- " . mysql_error());
        $i = 0;
        while ($roleRow = mysql_fetch_assoc($roleResult)) {
            if($roleRow['ERT_Title'] != $lastname)
            {
            ?>
            <div class="content-header"><?php echo $roleRow['ERT_Title'] ?> Email Recipients</div>
            <div class="content-sub-header">
                <div class="data-column padding-none mup-name">Email Address</div>
                <div class="data-column padding-none mup-email">Status</div>
                <div class="data-column padding-none mup-other">Edit</div>
                <div class="data-column padding-none mup-other">Delete</div>
            </div>
            <?php
            }
            ?>
            <div class="data-content">
                <div class="data-column mup-name"><?php echo $roleRow['ER_Email'] ?></div>
                <div class="data-column mup-email"><input type="checkbox" value="<?php echo $roleRow['ER_ID'] ?>" <?php echo ($roleRow['ER_Status']==1) ? 'checked' : '' ?> onclick="change_status(<?php echo $roleRow['ER_ID'] ?>,<?php echo ($roleRow['ER_Status']==1) ? 0 : 1 ?>)" ></div>
                <div class="data-column mup-other"><a href="add_email_reciption.php?id=<?php echo $roleRow['ER_ID'] ?>">Edit</a></div> 
                <div class="data-column mup-other"><a onClick="return confirm('Are you sure?\nThis action CANNOT be undone!');" href="add_email_reciption.php?id=<?php echo $roleRow['ER_ID'] ?>&amp;op=del">Delete</a></div>
            </div>
            <?PHP
            $lastname= $roleRow['ERT_Title'];
        }
        ?>
    </div>
</div>
<script>
    function change_status(er_id,status)
    {
        $.ajax({
            type: "GET",
            url: "change_email_status.php",
            data: {
                er_id: er_id, 
                status: status
            }
        })
        .done(function(msg) {
            if(status == 1){
              swal("Success", "Email reciepent status active", "success");  
            }else{
                swal("Success", "Email reciepent status Inactive", "success");  
            }
            
        });
    }
 </script>
<?PHP
if ($_SESSION['error'] == 2) {
    print '<script>swal("Error", "Email already exist. Please try another Email.", "error");</script>';
    unset($_SESSION['error']);
}
require_once '../include/admin/footer.php';
?>