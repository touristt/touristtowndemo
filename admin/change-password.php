<?php
require_once '../include/config.inc.php';

$title = "Change Password";

require_once '../include/admin/header.php';
?>
<table width="940" border="0" cellpadding="0" cellspacing="0" bgcolor="#676767" align="center">
    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td class="SectionHeader">Change Password</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table width="940" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
    <tr>
        <td bgcolor="#FFFFFF">
            <table width="800" border="0" cellspacing="20" cellpadding="0">
                <tr>
                    <td>
                        <form name="form1" method="post" action="http://<?php echo DOMAIN . INPUT_DIR; ?>">
                            <input name="logop" type="hidden" id="logop" value="chpw">
                            <input type="hidden" name="op" value="change">
                            <table width="500" align="center" border="0" cellpadding="0" cellspacing="15" >
                                <?php
                                if (isset($_SESSION['error_CW']) && $_SESSION['error_CW'] != '') {
                                    ?>
                                    <tr>
                                        <td colspan="2" ><?php echo $_SESSION['error_CW'] ?></td>
                                    </tr>
                                    <?php
                                    unset($_SESSION['error_CW']);
                                }
                                ?>
                                <tr>
                                    <td width="133" align="left" valign="top" ><strong>Old Password</strong></td>
                                    <td width="322" align="left" valign="top"><input type="password" name="old_pw" ></td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>New Password</strong></td>
                                    <td align="left" valign="top"><input type="password" name="new_pw"></td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top"><strong>Confirm New Password</strong></td>
                                    <td align="left" valign="top"><input type="password" name="new_pw_confirm"></td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">&nbsp;</td>
                                    <td align="left" valign="top"><input name="Submit" type="submit" value="Login Now" /></td>
                                </tr>
                            </table>
                        </form>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?php
require_once '..include/admin/footer.php';
?>