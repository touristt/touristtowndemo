<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require '../include/PHPMailer/class.phpmailer.php';
require_once '../include/track-data-entry.php';

$AID = isset($_REQUEST['advert_id']) ? $_REQUEST['advert_id'] : 0;
$inactive_ad = isset($_REQUEST['inactive_ad']) ? $_REQUEST['inactive_ad'] : 0;
$status = isset($_REQUEST['status']) ? $_REQUEST['status'] : 0;

if ($AID > 0) {
    $sql = "SELECT A_C_ID,A_BL_ID, A_SC_ID, AT_Cost, A_End_Date, A_Campaign, A_Approved_Logo, BL_Listing_Title, BL_Contact, R_Domain, B_Email, R_Name, A_ID, A_Third_Party, BL_Listing_Title, AT_Name, A_AT_ID, A_Status, 
            AT_ID, R_ID, R_ID, A_Is_Townasset, AT_Cost, A_Website, A_Title, A_Description, A_Notes, A_Approved_Logo, A_AT_ID, A_Is_Townasset, A_Discount, 
            A_Active_Date, A_End_Date, A_Active_Date, A_Is_Townasset, A_Date FROM tbl_Advertisement
            LEFT JOIN tbl_Region ON A_Website = R_ID 
            LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID 
            LEFT JOIN tbl_Category ON A_C_ID = C_ID
            LEFT JOIN tbl_Business_Listing ON A_BL_ID = BL_ID
            LEFT JOIN tbl_Business ON B_ID = BL_B_ID
            WHERE A_ID = '" . encode_strings($AID, $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowAdvert = mysql_fetch_assoc($result);
    $category = $rowAdvert['A_C_ID'];
    $subcategory = $rowAdvert['A_SC_ID'];
    $R_ID = $rowAdvert['R_ID'];
}
if (isset($_POST['butt-approve-photo'])) {

    $advert_status = $_POST['advert_status'];
    $domain = $_POST['domain'];
    $title = $_POST['title'];
    $Description = $_POST['Description'];
    $Note = $_POST['Note'];
    $cat = ($_REQUEST['category'] > 0) ? $_REQUEST['category'] : 0;
    $subcat = ($_REQUEST['subcategory'] > 0) ? $_REQUEST['subcategory'] : 0;
    $discount = ($_REQUEST['discount'] > 0) ? $_REQUEST['discount'] : 0;
    $keyval = ($_REQUEST['listings'] != "") ? $_REQUEST['listings'] : '0';
    if (isset($_REQUEST['campaign'])) {
        $campaign = 1;
    } else {
        $campaign = 0;
    }
    $selectBID = "SELECT BL_B_ID FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($keyval, $db) . "'";
    $resultBID = mysql_query($selectBID, $db) or die("Invalid query: $selectBID -- " . mysql_error());
    $rowBID = mysql_fetch_assoc($resultBID);
    $sql = "Update tbl_Advertisement SET A_C_ID = '" . encode_strings($cat, $db) . "', 
            A_Website = '" . encode_strings($_REQUEST['domain'], $db) . "', 
            A_SC_ID = '" . encode_strings($subcat, $db) . "',
            A_Title = '" . encode_strings($title, $db) . "',                  
            A_End_Date = '" . encode_strings(($_REQUEST['endDate'] != "") ? $_REQUEST['endDate'] : "0000-00-00", $db) . "',
            A_Description = '" . encode_strings($Description, $db) . "',
            A_Notes = '" . encode_strings($Note, $db) . "',
            A_B_ID = '" . encode_strings($rowBID['BL_B_ID'], $db) . "',
            A_BL_ID = '" . encode_strings($keyval, $db) . "',
            A_Third_Party = '" . encode_strings($_REQUEST['third_party'], $db) . "',
            A_Campaign = '" . encode_strings($campaign, $db) . "'";
    if ($advert_status == 1) {
        $sql .= " ,A_Status='2'";
    }
    if ($discount <= $rowAdvert['AT_Cost']) {
        $total = $rowAdvert['AT_Cost'] - $discount;
        $sql .= ", A_Discount = '" . encode_strings($discount, $db) . "', A_Total = '" . encode_strings($total, $db) . "'";
    } else {
        $_SESSION['advert_discount_error'] = $AID;
        header("Location:buy-an-advertisement.php?advert_id=$AID");
        exit;
    }
    require_once '../include/picUpload.inc.php';
    if (file_exists($_FILES['approve']['tmp_name'][0])) {
        $pic = Upload_Pic_Normal('0', 'approve', 0, 0, true, IMG_LOC_ABS, 0);
        if ($pic) {
            $sql .= ", A_Approved_Logo = '" . encode_strings($pic, $db) . "'";
            if ($rowAdvert['A_Approved_Logo']) {
                Delete_Pic(IMG_LOC_ABS . $rowAdvert['A_Approved_Logo']);
            }
        } else {
            header("Location:buy-an-advertisement.php?advert_id=$AID");
            exit;
        }
    }
    $sql .= " WHERE A_ID = '" . encode_strings($AID, $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    if ($advert_status == 1) {
        ob_start();
        include '../include/email-template/by-in-advertisment-improve-email.php';
        $message = ob_get_contents();
        ob_clean();
        $mail = new PHPMailer();
        $mail->IsMail();
        $mail->From = "no-reply@touristtown.com";
        $mail->FromName = "TouristTown";
        $mail->IsHTML(true);
        $mail->AddAddress($message);
        $mail->CharSet = 'UTF-8';
        $mail->AddAddress($rowAdvert['B_Email']);
        $mail->AddAddress($to);
        $mail->Subject = "Campaign Request for " . $rowAdvert['R_Name'] . " Tourism Website";
        $mail->MsgHTML($message);
        $mail->Subject = "Campaign Request for " . $rowAdvert['R_Name'] . " Tourism Website";
        $mail->MsgHTML($message);
        /// $mail->Send();
    }
    $_SESSION['update_photo'] = 1;
    // TRACK DATA ENTRY
    $id = $AID;
    Track_Data_Entry('Campaign', '', 'Buy A Campaign', $id, 'Update', 'super admin');
    header("Location:advertisement.php?status=" . $rowAdvert['A_Status']);
    exit;
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Campaigns</div>
        <div class="link">
        </div>
    </div>
    <div class="left advert-left-nav">
        <?php require_once '../include/nav-B-advertisement-admin.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">
            <?php echo ($inactive_ad == 1) ? 'Campaign' : 'Buy A Campaign'; ?>
            <?php
            if ($inactive_ad == 1) {
                ?>
                <div class="link">
                    <div class="add-link">
                        <a href="inactive_advertisement.php?active_req=<?php echo $rowAdvert['A_ID']; ?>" >Active</a>
                    </div>            
                </div>
                <?php
            }
            ?>
        </div>

        <style type="text/css">
            li.token-input-token p{width: 180px !important;
                                   font-size: 14px;}
            </style>
            <form name="form1" method="post" enctype="multipart/form-data" action="buy-an-advertisement.php" onSubmit="return validateForm();">
                <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Website </label>
                <div class="form-data" id="regions"> 
                    <select class="adv-type-options" id="region-id" name="domain" onChange="validate_buy_an_add(1, 0, 0, 0)" required>
                        <option required value="">Select Website</option>
                        <?PHP
                        $sql = "SELECT R_ID, R_Name FROM tbl_Region";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($rowRegion = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $rowRegion['R_ID'] ?>" <?php echo ($rowAdvert['A_Website'] == $rowRegion['R_ID']) ? "selected" : "" ?>><?php echo $rowRegion['R_Name']; ?></option>
                        <?PHP } ?>
                    </select>
                </div>
            </div>                       
            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Campaign Type</label>
                <div class="form-data from-inside-div-text">
                    <?php echo $rowAdvert['AT_Name']; ?>
                </div>
            </div>
            <input type="hidden" name="advert_id" value="<?php echo $AID ?>">
            <input type="hidden" id="advert-type" name="advert_type" value="<?php echo $rowAdvert['A_AT_ID']; ?>">
            <input type="hidden" name="advert_status" value="<?php echo $rowAdvert['A_Status'] ?>">
            <input type="hidden" name="status" value="<?php echo $status; ?>">
            <?php
            if ($rowAdvert['AT_ID'] != 4) {
                ?>
                <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin" id="cat">
                    <label>Category </label>
                    <div class="form-data" id="advert-cats">
                        <select class="adv-type-options"  name="category" id="advert-cat" id="advert-type" onchange="validate_buy_an_add(0, 0, 1, 0);">
                            <?PHP
                            if (isset($R_ID)) {
                                $sql = "SELECT C_ID, RC_Name, RC_Name, C_Name FROM  `tbl_Category` 
                                        LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID
                                        LEFT JOIN tbl_Region ON RC_R_ID = R_ID 
                                        WHERE R_ID = '" . encode_strings($rowAdvert['R_ID'], $db) . "' AND C_ID NOT IN(121) AND C_Parent = 0 ORDER BY RC_Order ASC";
                                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                while ($rowCat = mysql_fetch_assoc($result)) {
                                    ?>
                                    <option value="<?php echo $rowCat['C_ID'] ?>" <?php echo ($rowCat['C_ID'] == $category) ? 'selected' : ''; ?>><?php echo ($rowCat['RC_Name'] != '') ? $rowCat['RC_Name'] : $rowCat['C_Name'] ?></option>
                                    <?PHP
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <?php
            }
            if ($rowAdvert['AT_ID'] != 4 && $rowAdvert['AT_ID'] != 2) {
                ?>
                <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin" id="sub-cat">
                    <label>Sub Category</label>
                    <div class="form-data" id="advert-subcats">
                        <select class="adv-type-options" id="advert-subcat" name="subcategory" onchange="validate_buy_an_add(0, 0, 0, 1);">
                            <option value="">Select Sub Category</option>
                            <?PHP
                            if (isset($category)) {
                                $sql = "SELECT C_ID, C_Name FROM tbl_Category 
                                        LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID
                                        LEFT JOIN tbl_Region ON RC_R_ID = R_ID 
                                        WHERE R_ID = '" . encode_strings($rowAdvert['R_ID'], $db) . "' AND C_Parent = '$category' ORDER BY C_Name";
                                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                while ($rowSubcat = mysql_fetch_assoc($result)) {
                                    ?>
                                    <option value="<?php echo $rowSubcat['C_ID'] ?>" <?php echo ($rowSubcat['C_ID'] == $subcategory) ? 'selected' : ''; ?>><?php echo $rowSubcat['C_Name'] ?></option>
                                    <?PHP
                                }
                            }
                            ?>

                        </select>
                    </div>
                </div>
            <?php } ?>
            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>End Date</label>
                <div class="form-data">
                    <input type="text" name="endDate" class="previous-date-not-allowed" id="endDate" value="<?php echo $rowAdvert['A_End_Date']; ?>"/> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Campaign</label>
                <div class="form-data">
                    <input type="checkbox" name="campaign" id="campaign" value="<?php echo $rowAdvert['A_Campaign'] ?>"<?php echo ($rowAdvert['A_Campaign'] == 1) ? "checked" : '' ?>/> 
                </div>
            </div>
            <?php if ($rowAdvert['A_Is_Townasset'] != 1) { ?>
                <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                    <label>Monthly Cost</label>
                    <div class="form-data from-inside-div-text">
                        <?php echo "$" . $rowAdvert['AT_Cost'] . " (Plus taxes)"; ?>
                    </div>
                </div>
            <?php } ?>
            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Campaign Link</label>
                <?php if ($rowAdvert['A_Is_Townasset'] == '1') {
                    ?>
                    <div class="form-data">
                        <input name="third_party" type="text" value="<?php echo $rowAdvert['A_Third_Party']; ?>">
                    </div>
                <?php } else {
                    ?>
                    <div class="form-data listings_count">
                        <input id="autocomplete_lisitngs" type="text" value="<?php echo $rowAdvert['BL_Listing_Title']; ?>" /> 
                        <input type="hidden" id="listing_search" name="listings" value="<?php echo (isset($rowAdvert['A_BL_ID']) && $rowAdvert['A_BL_ID'] != '') ? $rowAdvert['A_BL_ID'] : '' ?>">
                    </div>
                <?php } ?>
                <a class="adv-sample" title="This is the listing where ad will setup">What is this?</a>
            </div>             
            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Title</label>
                <div class="form-data">
                    <input type="text" name="title" id="title" value="<?php echo $rowAdvert['A_Title']; ?>" >
                </div>
                <a class="adv-sample" title="This is the main message of your ad. Keep it short">What is this?</a>
            </div>            

            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Description</label>
                <div class="form-data wiziwig-desc description-text">
                    <textarea name="Description" class="tt-ckeditor" id="desc"><?php echo $rowAdvert['A_Description']; ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Notes</label>
                <div class="form-data wiziwig-desc description-text">
                    <textarea name="Note" class="tt-ckeditor" id="notes"><?php echo $rowAdvert['A_Notes']; ?></textarea>
                </div>
            </div>
            <?php
            $Photos = "SELECT AP_Photo FROM tbl_Advertisement_Photo WHERE AP_A_ID = '" . encode_strings($AID, $db) . "'";
            $PhotosResult = mysql_query($Photos, $db) or die("Invalid query: $Photos -- " . mysql_error());
            $count = 1;
            while ($PhotosRow = mysql_fetch_assoc($PhotosResult)) {
                ?>
                <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                    <label>Supplied Image <?php echo $count ?></label>
                    <div class="form-data from-inside-div-text">
                        <a target="blank" href="http://<?php echo DOMAIN . IMG_LOC_REL . $PhotosRow['AP_Photo'] ?>">View photo</a>
                    </div>
                </div>
                <?php
                $count++;
            }
            ?>

            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Approve Photo</label>
                <div class="form-data from-inside-div-text">
                    <?php
                    if ($inactive_ad != 1) {
                        ?>
                        <div class="approved-photo-div">
                            <div class="inputWrapper adv-photo float-left">Browse
                                <input class="fileInput" type="file" name="approve[]" onchange="show_file(this)" <?php echo ($rowAdvert['A_Status'] == 1) ? 'required' : '' ?>>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="approved-photo-div">
                        <img id="uploadFile" src="<?php echo ($rowAdvert['A_Approved_Logo'] != '') ? 'http://' . DOMAIN . IMG_LOC_REL . $rowAdvert['A_Approved_Logo'] : '' ?>">          
                    </div>
                    <?php echo ($rowAdvert['A_AT_ID'] == 1) ? "Image should be 1600X640 or equal aspect ratio." : 'Image should be 350X120 or equal aspect ratio.'; ?>
                </div>
            </div>
            <?php if ($rowAdvert['A_Is_Townasset'] != 1) { ?>
                <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                    <label>Discount</label>
                    <span style="float: left;margin: 10px 0 0px -15px;">$</span>
                    <input name="discount" type="text" id="discount" value="<?php echo $rowAdvert['A_Discount'] ?>" size="15" <?php echo ($inactive_ad == 1) ? 'disabled' : ''; ?> />
                </div>
            <?php }
            ?>

            <?php
            if ($inactive_ad != 1) {
                ?>           
                <div class="form-inside-div border-none margin-bottom-26 margin-top-11 form-inside-div-width-admin">
                    <div class="button">
                        <input type="submit" name="butt-approve-photo" value="Save & Close"/>
                    </div>
                </div>
                <?php
            }
            ?>
        </form>
        <?php
        if ($rowAdvert['A_Status'] == 3) {
            ?>
            <div class="content-header">
                <div class="title">
                    Campaigning Period
                </div>
            </div>
            <div class="menu-items-accodings">
                <div id="accordion">
                    <?php
                    $sql2 = "SELECT DISTINCT YEAR(AS_Date) as years FROM tbl_Advertisement_Statistics WHERE AS_A_ID = $AID order by YEAR(AS_Date) desc";
                    $result2 = mysql_query($sql2, $db) or die("Invalid query: $sql2 -- " . mysql_error());
                    while ($row1 = mysql_fetch_assoc($result2)) {
                        $rows[] = $row1;
                    }
                    $current_month = date('M', strtotime('+1 month'));
                    $current_year = date('Y');
                    foreach ($rows as $row) {
                        ?>  
                        <h3 class="accordion-rows" id="ddd"><span class="accordion-title"><?php echo $row['years']; ?></span></h3>
                        <div class="sub-accordions accordion-padding">
                            <div class="monthly-statistic-header form-inside-div-width-admin">
                                <div class="data-column adv-listing-title padding-none">Month</div>
                                <div class="data-column cust-listing-other padding-none">Clicks</div>
                                <div class="data-column cust-listing-other padding-none">Impressions</div>
                            </div>
                            <?php
                            $months = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
                            $clicks = 0;
                            $impressions = 0;
                            foreach ($months as $month) {

                                if ($row['years'] == $current_year && date('M', strtotime($row['years'] . '-' . $month . '-' . '01')) == $current_month) {
                                    break;
                                }

                                $sql2 = "SELECT YEAR(AS_Date),MONTHNAME(AS_Date), SUM(AS_Impression) AS impression, SUM(AS_Clicks) AS clicks FROM tbl_Advertisement_Statistics 
                                WHERE AS_A_ID = $AID AND YEAR(AS_Date) = '" . $row['years'] . "' AND MONTH(AS_Date) = '" . $month . "'";
                                $result2 = mysql_query($sql2, $db) or die("Invalid query: $sql2 -- " . mysql_error());
                                $rowStats = mysql_fetch_array($result2);
                                $clicks += $rowStats['clicks'];
                                $impressions += $rowStats['impression'];
                                $dateObj = DateTime::createFromFormat('!m', $month);
                                $monthName = $dateObj->format('F');
                                ?>
                                <div class="data-content advert-statistics form-inside-div-width-admin">
                                    <div class="data-column adv-listing-title padding-none"><?php echo $monthName; ?></div>
                                    <div class="data-column cust-listing-other padding-none"><?php echo ($rowStats['impression'] != '') ? $rowStats['clicks'] : '0'; ?></div>
                                    <div class="data-column cust-listing-other padding-none"><?php echo ($rowStats['impression'] != '') ? $rowStats['impression'] : '0'; ?></div>
                                </div>
                            <?php }
                            ?>
                            <div class="data-content advert-statistics form-inside-div-width-admin total">
                                <div class="data-column adv-listing-title padding-none">Total :</div>

                                <div class="data-column cust-listing-other padding-none"><?php echo $clicks ?></div>
                                <div class="data-column cust-listing-other padding-none"><?php echo $impressions ?></div>
                            </div> 
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<div id="auto-complete-div" style="display: none;"></div>
<script type="text/javascript">
    $(function () {
        $(".adv-sample").tooltip();
    });
    $(function () {

        $('.accordion-rows a').click(function (event) {
            if ($(this).parent().parent().hasClass("ui-accordion-header")) {
                event.stopPropagation(); // this is
            }
        });
        $("#accordion").accordion({
            collapsible: true,
            heightStyle: 'content',
            active: 0
        });
        $('.token-input-dropdown').css("width", "400px");
    });

    $(document).ready(function () {
        var region_id = $('#region-id').val();
        var search_text = $('#autocomplete_lisitngs').val();
        var searched_items = "";
        var json = [];
        if (search_text != "") {
            searched_items = search_text.split("@");
            for (var i = 0; i < searched_items.length; i++) {
                json.push({"name": searched_items[i]});
            }
        }
        $("#autocomplete_lisitngs").tokenInput(<?php include "../include/get-listings-for-ad-edit.php"; ?>, {
            onAdd: function (item_lisitngs) {
                var list_val = $('#listing_search').val();
                var list_vals = $('#autocomplete_lisitngs').val();
                if (list_val != '') {
                    $('#listing_search').val(item_lisitngs.id);
                } else {
                    $('#listing_search').val(item_lisitngs.id);
                }
            },
            onResult: function (item) {
                if ($.isEmptyObject(item)) {
                    return [{id: '0', name: $("tester").text()}]
                } else {
                    item.unshift({"name": $("tester").text()});
                    var lookup = {};
                    var result = [];

                    for (var temp, i = 0; temp = item[i++]; ) {
                        var name = temp.name;
                        var id = temp.id;
                        if (!(name in lookup)) {
                            lookup[name] = 1;
                            result.push({"name": name, "id": id});
                        }
                    }
                    return result;
                }

            },
            onDelete: function (item) {
                var emptyVal = $('#listing_search').val("");
                var value = $('#autocomplete_lisitngs').val().split(",");
                $('#autocomplete_lisitngs').empty();
                var data = "";
                $.each(value, function (key, value) {
                    if (value != item.name) {
                        if (data != '') {
                            data = data + "," + value;
                        } else {
                            data = value;
                        }
                    }
                });
                $('#autocomplete_lisitngs').val(data);
            },
            resultsLimit: 10,
            prePopulate: json,
            tokenLimit: 1
        });
    });

    var advert_total_cost = "";
    function validate_buy_an_add(R, AT, C, S)
    {
        var advert_type = $('#advert-type').val();
        var region_id = $('#region-id').val();
        //Onchange Region
        if (R == 1) {
            $("#advert-cat").val("");
            $.ajax({
                type: "GET",
                url: "get-customer-advert-cat.php",
                data: {
                    region_id: region_id,
                    advert_type: advert_type
                }
            }).done(function (msg) {
                $("#advert-cats").empty();
                $("#advert-cats").html(msg);
                $("#advert-subcat").val("");
                $("#advert-cat").val("");
                $("#listing").val("");
            });
            $.post("get-listings-for-ad.php", {
                R_ID: region_id
            }, function (done) {
                $('#listing_search').empty();
                $('#listing_search').text(done);
            });

        }
        //Onchange Ad Type
        if (AT == 1) {
            //Get Price based on Ad type
            $.post("get_advert_price.php", {
                advert_type: advert_type
            }, function (done) {
                if (done) {
                    $('#adver-total').hide();
                    $('#advert-total').html("");
                    advert_total_cost = "";
                } else {
                    $('#adver-total').show();
                    $('#advert-total').html('$' + done);
                    advert_total_cost = done;
                }
            });
            if (advert_type == '')
            {
                $("#view-sample").hide();
                $('#cat').show();
                $("#advert-cat").attr('required', 'required');
                $("#advert-cat").val("");
                $('#sub-cat').show();
                $("#advert-subcat").attr('required', 'required');
                $("#advert-subcat").val("");
                $("#sample-image-show").empty();
            }
            if (advert_type == 1)
            {
                $('#cat').show();
                $("#advert-cat").attr('required', 'required');
                $("#advert-cat").val("");
                $('#sub-cat').show();
                $("#advert-subcat").removeAttr('required');
                $("#advert-subcat").val("");
                $("#view-sample").show();
                $("#sample-image-show").empty();
                $("#sample-image-show").append("<img src='<?php echo "http://" . DOMAIN . IMG_LOC_REL . "8489225.jpg" ?>'>");
            }
            if (advert_type == 2)
            {
                $('#cat').show();
                $("#advert-cat").attr('required', 'required');
                $("#advert-cat").val("");
                $('#sub-cat').hide();
                $("#advert-subcat").removeAttr('required');
                $("#advert-subcat").val("");
                $("#view-sample").show();
                $("#sample-image-show").empty();
                $("#sample-image-show").append("<img src='<?php echo "http://" . DOMAIN . IMG_LOC_REL . "d-ad-3.png" ?>'>");
            }

        }
        var cat_id = $('#advert-cat').val();

        //Onchange Cat
        if (C == 1) {
            $.ajax({
                type: "GET",
                url: "get-customer-advert-subcat.php",
                data: {
                    cat_id: cat_id,
                    website_id: region_id,
                    advert_type: advert_type
                }
            }).done(function (msg) {
                $("#advert-subcats").empty();
                $("#advert-subcats").html(msg);
            });
        }
        var SubCat = $('#advert-subcat').val();
        if (region_id > 0 && cat_id > 0 && SubCat > 0 && advert_type > 0) {
            $.post("get-listings-for-ad.php", {
                R_ID: region_id,
                A_CAT: cat_id,
                A_SUBCAT: SubCat
            }, function (done) {
                $('#listing_search').empty();
                $('#listing_search').text(done);
            });

        } else if (region_id > 0 && cat_id > 0 && advert_type > 0) {
            $.post("get-listings-for-ad.php", {
                R_ID: region_id,
                A_CAT: cat_id
            }, function (done) {
                $('#listing_search').empty();
                $('#listing_search').text(done);
            });

        } else if (region_id > 0 && advert_type > 0) {
            $.post("get-listings-for-ad.php", {
                R_ID: region_id
            }, function (done) {
                $('#listing_search').empty();
                $('#listing_search').text(done);
            });
        } else if (region_id > 0) {
            $.post("get-listings-for-ad.php", {
                R_ID: region_id
            }, function (done) {
                $('#listing_search').empty();
                $('#listing_search').text(done);
            });
        }
    }

    function validateForm() {
        var advert_type = $('#advert-type').val();
        var img = document.getElementById('uploadFile');
        var width = img.naturalWidth;
        var height = img.naturalHeight;

        if (advert_type == 1) {
            if (width != 1600) {
                swal("", "Photo wrong size. Must be 1600 pixels wide.", "warning");
                return false;
            }
            if (height != 640) {
                swal("", "Photo wrong size. Must be 640 pixels high.", "warning");
                return false;
            }
        } else if (advert_type == 2) {
            if (width != 350) {
                swal("", "Photo wrong size. Must be 350 pixels wide.", "warning");
                return false;
            }
            if (height != 120) {
                swal("", "Photo wrong size. Must be 120 pixels high.", "warning");
                return false;
            }
        }
        var emptyVal = $('#listing_search').val();
        if (emptyVal == '') {
            alert("Campaign Link field is Required!");
            $('#listing_search').focus();
            $('#autocomplete_lisitngs').focus();
            $('#autocomplete_lisitngs').prop('required', true);
            return false;
        }
    }
    function show_file(input) {
        if (input.files && input.files[0]) {
            $('#uploadFile').show();
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#uploadFile')
                        .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<style>
    .menu-items-accodings{width: 825px !important;}
</style>

<?PHP
require_once '../include/admin/footer.php';
?>