<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('manage-support', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Support</div>
        <div class="addlisting"><a href="support.php">+Add Support</a></div>
        <div class="link">
        </div>
    </div>
    <div class="left">
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name">Name</div>
            <div class="data-column padding-none spl-other">Edit</div>
            <div class="data-column padding-none spl-other">Delete</div>
        </div>
        <?PHP
        $sql = "SELECT SS_ID, SS_Name  FROM tbl_Support_Section ORDER by SS_Name ASC";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            ?>
            <div class="data-content">
                <div class="data-column spl-name">
                    <a href="support-type.php?id=<?php echo $row['SS_ID'] ?>"><?php echo $row['SS_Name'] ?></a>
                </div>
                <div class="data-column spl-other">
                    <a href="support.php?id=<?php echo $row['SS_ID'] ?>">Edit</a>
                </div>
                <div class="data-column spl-other">    
                    <a href="support-delete.php?id=<?php echo $row['SS_ID'] ?>" onclick="return confirm('Are you sure you want to delete?');">Delete</a>
                </div>
            </div>
            <?PHP
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>