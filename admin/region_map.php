<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} elseif ($_REQUEST['newRegion'] <> true) {
    header("Location: /admin/regions.php");
    exit();
}

$sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);

$sql = "SELECT M_Description FROM tbl_region_map WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$map_record = mysql_fetch_assoc($result);
$map_rows = mysql_num_rows($result);

if ($_POST['op'] == 'save') {
    if ($map_rows > 0) {
        $sql = "UPDATE tbl_region_map SET M_Description = '" . encode_strings(trim(str_replace('<p>', '', str_replace('</p>', '', $_REQUEST['description']))), $db) . "' WHERE R_ID = '" . encode_strings($regionID, $db) . "'";
    } else {
        $description = encode_strings(trim(str_replace('<p>', '', str_replace('</p>', '', $_REQUEST['description']))), $db);
        $regionID = $_POST['rid'];
        $sql = "INSERT into tbl_region_map(M_Description,R_ID) values('$description','$regionID')";
    }
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    header("Location: region_map.php?rid=" . $regionID);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] . " - Map Description "; ?></div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <form action="/admin/region_map.php" method="post" enctype="multipart/form-data" name="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">

            <div class="content-header">Region Map Description</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Description</label>
                <div class="form-data">
                    <textarea name="description" cols="85" rows="10" class="tt-description"><?php echo $map_record['M_Description'] ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="button2" id="button22" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
