<?php
include '../include/config.inc.php';
include '../include/accommodation_functions.php';
require_once '../include/login.inc.php';
require_once '../include/admin/header.php';
include '../include/accommodation_header.php';
require_once '../include/track-data-entry.php';

date_default_timezone_set('America/New_York');
if (isset($_GET['id'])) {
    $group_id = $_GET['id'];
    $archive = isset($_GET['archive']) ? $_GET['archive'] : 0;
    $group_blocks_info_query = "SELECT * FROM `acc_group_block_info` WHERE id = $group_id";
    $group_blocks_info = mysql_query($group_blocks_info_query);
    if (mysql_num_rows($group_blocks_info) <= 0) {
        $_SESSION['message'] = "<div class='error-message'>No such group block.</div>";
        header('location:group_blocks.php');
    }
    $info_this_group = mysql_fetch_array($group_blocks_info);
} else {
    $_SESSION['message'] = "<div class='error-message'>No such group block.</div>";
    header('location:group_blocks.php');
}
if (isset($_POST['add_group'])) {
    if (isset($_POST['start_date']) && $_POST['end_date']) {
        $name = $_POST['name'];
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];
        $insert_check = "SELECT * FROM `acc_blocks` WHERE (start_date BETWEEN '$start_date' AND '$end_date') OR (end_date BETWEEN '$start_date' AND '$end_date')";
        $result = mysql_query($insert_check);
        if (mysql_num_rows($result) > 0) {
            $_SESSION['message'] = "<div class='error-message'>Block already exists in this date range.</div>";
        } else {
            $add_block_query = "INSERT INTO `acc_blocks`(`name`, `start_date`, `end_date`, `group_block_id`) VALUES ('$name','$start_date','$end_date','$group_id')";
            $result = mysql_query($add_block_query) or die("Invalid query: $add_block_query -- " . mysql_error());
            if ($result) {
                $new_block_id = mysql_insert_id();
                set_default_availibility($new_block_id);
                // TRACK DATA ENTRY
                Track_Data_Entry('Accomodation', '', 'Add Block', $new_block_id, 'Add Block', 'super admin');
                $_SESSION['message'] = "<div class='success-message'>Block added successfully to group block.</div>";
            }
        }
    } else {
        $_SESSION['message'] = "<div class='error-message'>Please select start and end date.</div>";
    }
}
$start_date_datepicker = "SELECT MAX(end_date) AS max_date FROM acc_blocks";
$result_datepicker = mysql_query($start_date_datepicker);
$result = mysql_fetch_array($result_datepicker);
$min_date = date("Y/n/j");
if (!empty($result['max_date'])) {
    $min_date = date("Y/n/j", strtotime("+1 day", strtotime($result['max_date'])));
}
?>
<div class="content-left full-width">
    <?php if (isset($_SESSION['message'])) { ?>
        <div class="message">
            <?php
            echo $_SESSION['message'];
            unset($_SESSION['message']);
            ?>
        </div>
    <?php } ?>
    <div class="right width-accommodation">
        <div class="content-header width-accommodation-header margin-top-group">
            <?php
            $group_information = group_block_info($info_this_group['id']);
            echo group_block_status($info_this_group['id']);
            echo ' ' . $info_this_group['name'];
            echo ' - ' . date("Y");
            ?>
            <div class="acc-date-header"><?php echo (isset($group_information['max_date']) && $group_information['max_date'] != NULL) ? $group_information['max_date'] : " - " ?></div>
            <div class="acc-date-header"><?php echo (isset($group_information['min_date']) && $group_information['min_date'] != NULL) ? $group_information['min_date'] : " - " ?></div> 
        </div>

        <?php
        $group_blocks_query = "SELECT * FROM `acc_blocks` WHERE group_block_id = $group_id";
        $group_blocks = mysql_query($group_blocks_query);
        if (mysql_num_rows($group_blocks) > 0) {
            ?>
            <div class="content-sub-header">
                <div class="data-column spl-other-cc-list padding-none"> # </div>
                <div class="data-column padding-none spl-name-events"> Name </div>
                <div class="data-column spl-other-cc-list padding-none"> Start </div>
                <div class="data-column spl-other-cc-list padding-none"> End </div>
                <div class="data-column spl-other-cc-list padding-none"></div>
            </div>       
            <?php
            while ($block = mysql_fetch_array($group_blocks)) {
                ?>
                <div class="data-content">
                    <div class="data-column spl-other-cc-list"><?php echo $block['id']; ?> </div>
                    <div  class="data-column spl-name-events"><a href="cottage_parks.php?block_id=<?php echo $block['id']; ?>&archive=<?php echo $archive ?>"><?php echo $block['name']; ?></a></div>
                    <div class="data-column spl-other-cc-list"><?php echo $block['start_date']; ?> </div>
                    <div class="data-column spl-other-cc-list"><?php echo $block['end_date']; ?> </div>
                    <?php
                    if ($archive == 0) {
                        ?>
                        <div class="data-column spl-other-cc-list"><a onclick="return confirm('Do you really want to delete this block? Availability data for this block will be deleted.');" href="delete_block.php?block_id=<?php echo $block['id']; ?>&group_id=<?php echo $group_id; ?>"><img src="images/delete.png"/></a> </div>
                        <?php
                    }
                    ?>
                </div>  
                <?php
            }
            ?>
            <?php
        }
        else
            echo '<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr class="row-header">
                    <th class="pending_block_summer_th block_serial">No Block Added In This Group</th>
                    </thead>
                    </table>';
        ?>
        <!--pending blocks ends-->
        <!--live groups start-->
        <?php
        if ($archive == 0) {
            ?>
            <div class="content-header width-accommodation-header">
                +Add Block
            </div>
            <div class="content-sub-header">
                <div class="data-column accommodation-add-block-title padding-none">Name</div>
                <div class="data-column accommodation-add-block-other padding-none">Start Date</div>
                <div class="data-column accommodation-add-block-other padding-none">End Date</div>
                <div class="data-column accommodation-add-block-other padding-none"> &nbsp;</div>
            </div> 
            <div class="data-content">
                <form name="add-block" method="post" action="group_block_info.php?id=<?php echo $group_id ?>">
                    <div class="data-column accommodation-add-block-title"><input type="text" required name="name"/> </div>
                    <div class="data-column accommodation-add-block-other"><input name="start_date" class="start_datepicker" type="text" readonly required/> </div>
                    <div class="data-column accommodation-add-block-other"><input name="end_date" class="end_datepicker" type="text" readonly required />  </div>
                    <div class="data-column accommodation-add-block-other"><input type="submit" name="add_group" value="Add New Block" class="add_group_btn"/> </div>
                </form>
            </div>
            <?php
        }
        ?> 
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        <script>
            $('.end_datepicker').datepicker('disable');
            $(".start_datepicker").datepicker({
                changeYear: true,
                dateFormat: 'yy-mm-dd',
                showButtonPanel: true,
                changeMonth: true,
                changeYear: true,
                showOn: "button",
                buttonImage: "css/images/calendar.png",
                buttonImageOnly: true,
                minDate: 0,
                minDate: new Date('<?php echo $min_date ?>'),
                maxDate: '+30Y',
                inline: true,
                onSelect: function(dateText, inst) {
                    var nyd = new Date(dateText);
                    $('.end_datepicker').datepicker('enable');
                    nyd.setDate(nyd.getDate() + 7);
                    $('.end_datepicker').datepicker("option", {
                        minDate: new Date(dateText),
                        maxDate: nyd
                    });
                }
            });
            $(".end_datepicker").datepicker({
                dateFormat: 'yy-mm-dd',
                showButtonPanel: true,
                showOn: "button",
                buttonImage: "css/images/calendar.png",
                buttonImageOnly: true,
                inline: true,
                disabled: true
            });
        </script>
    </div>
</div>
<div id="ui-datepicker-div" class="ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"></div>
<?PHP
require_once '../include/admin/footer.php';
?>