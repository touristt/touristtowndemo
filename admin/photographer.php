<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('image-bank', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Photographers</div>
        <div class="addlisting"><a href="add-photographer.php">+Add Photographer</a></div>
        <div class="link">
        </div>
    </div> 
    <div class="right full-width">
        <div class="content-sub-header">
            <div class="data-column padding-none mup-name">Name</div>
            <div class="data-column padding-none mup-email"></div>
            <div class="data-column padding-none mup-region"></div>
            <div class="data-column padding-none mup-other">Edit</div>
            <div class="data-column padding-none mup-other">Delete</div>
        </div>
        <?PHP
        $sql = "SELECT IBP_ID, IBP_Name FROM tbl_Image_Bank_Photographer";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            ?>
            <div class="data-content">
                <div class="data-column mup-name">
                    <?php
                    echo $row['IBP_Name'];
                    ?>
                </div>
                <div class="data-column mup-email"></div>
                <div class="data-column mup-region"></div>
                <div class="data-column mup-other"><a href="add-photographer.php?id=<?php echo $row['IBP_ID'] ?>">Edit</a></div>
                <div class="data-column mup-other"><a onClick="return confirm('Are you sure?\nThis action CANNOT be undone!');" href="add-photographer.php?id=<?php echo $row['IBP_ID'] ?>&amp;op=del">Delete</a></div>
            </div>
            <?PHP
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>