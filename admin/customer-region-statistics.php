<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
}
$sql = "SELECT R_Type, R_Tracking_ID, R_Domain, R_Domain_Alternates, R_Name FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);
//$tracking_id = $activeRegion['R_Tracking_ID'];
$tracking_id = 'UA-32156009-9';
$R_Domain = $activeRegion['R_Domain'];
$R_Domain_Alternates = $activeRegion['R_Domain_Alternates'];

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?></div>
        <div class="link">
        </div>
    </div>

    <div class="left">
        <?PHP
        if ($regionID > 0) {
            require '../include/nav-manage-region.php';
        }
        ?>
    </div>

    <div class="right">
        <div class="content-header">Statistics</div>
        <?php
        $analytics = getService();
        $profile = getprofileId($analytics, $regionID, $tracking_id, $R_Domain, $R_Domain_Alternates);

        function getService() {
            // Creates and returns the Analytics service object.
            // Load the Google API PHP Client Library.
            require_once '../include/google-api-php-client-master/src/Google/autoload.php';

            // Use the developers console and replace the values with your
            // service account email, and relative location of your key file.
            $service_account_email = 'analytics@tourist-town.iam.gserviceaccount.com';
            $key_file_location = 'Tourist Town-2a6f3c92156a.p12';

            // Create and configure a new client object.
            $client = new Google_Client();
            $client->setApplicationName("HelloAnalytics");
            $analytics = new Google_Service_Analytics($client);

            // Read the generated client_secrets.p12 key.
            $key = file_get_contents($key_file_location);
            $cred = new Google_Auth_AssertionCredentials(
                    $service_account_email, array(Google_Service_Analytics::ANALYTICS_READONLY), $key
            );
            $client->setAssertionCredentials($cred);
            if ($client->getAuth()->isAccessTokenExpired()) {
                $client->getAuth()->refreshTokenWithAssertion($cred);
            }
            return $analytics;
        }

        function getprofileId(&$analytics, &$regionID, &$tracking_id, &$R_Domain, &$R_Domain_Alternates) {
            $accounts = $analytics->management_accounts->listManagementAccounts();
            if (count($accounts->getItems()) > 0) {
                $items = $accounts->getItems();
                foreach ($items as $item) {
                    $firstAccountId = $item->getId();
                    $webproperties = $analytics->management_webproperties->listManagementWebproperties($firstAccountId);
                    if (count($webproperties->getItems()) > 0) {
                        $items_webproperties = $webproperties->getItems();
                        foreach ($items_webproperties as $items_webproperty) {
                            $firstWebpropertyId = $items_webproperty->getId();
                            $profiles = $analytics->management_profiles->listManagementProfiles($firstAccountId, $firstWebpropertyId);
                            if (count($profiles->getItems()) > 0) {
                                $items_profiles = $profiles->getItems();
                                foreach ($items_profiles as $items_profile) {
                                    $profileId = $items_profile->getId();
                                    $profileweb = $items_profile->getwebPropertyId();
                                    if ($tracking_id == $profileweb) {
                                        if (isset($profileId)) {
                                            $startdate = date('2014-01-01');
                                            $enddate = date('Y-m-d');
                                            $filters = array('filters' => 'ga:hostname=~' . $R_Domain . '|' . $R_Domain_Alternates, 'dimensions' => 'ga:year,ga:month', 'max-results' => '800000');
                                            $results_pages = getResultsGA($analytics, $profileId, $startdate, $enddate, 'ga:pageviews,ga:sessions,ga:uniquePageviews,ga:Users', $filters);
                                            printResultsPages($results_pages, $regionID);
                                        }
                                    }
                                }
                            } else {
                                throw new Exception('No views (profiles) found for this user.');
                            }
                        }
                    } else {
                        throw new Exception('No webproperties found for this user.');
                    }
                }
            } else {
                throw new Exception('No accounts found for this user.');
            }
        }

        function getResultsGA(&$analytics, $profileId, $startdate, $enddate, $matrics, $filters) {
            return $analytics->data_ga->get(
                            'ga:' . $profileId, $startdate, $enddate, $matrics, $filters);
        }
        ?>
        <?php

        function printResultsPages(&$results, &$r) {
            $rows = $results->getRows();
            ?>
            <div> 
                <div class="menu-items-accodings"> 
                    <div id="accordion">
                        <?php
                        $yearCurrent = date('Y');
                        $currentIteration = '';
                        $firstIteration = true;
                        foreach (array_reverse($rows) as $monthrecord) {

                            if ($currentIteration != $monthrecord[0]) {
                                //start accordion
                                if ($firstIteration) {
                                    $firstIteration = false;
                                } else {
                                    print '<div class="content-sub-header footer-width padding-none" id="footer-width">
                                <div class="data-column">Total</div>
                                                <div class="data-column">' . $users_total . '</div>
                                                <div class="data-column">' . $viewed_total . '</div>
                                                <div class="data-column">' . $sessions_total . '</div>
                                                <div class="data-column">' . $unique_total . '</div>
                            </div>';
                                    print '</div>';  //end the previous accordion
                                    $viewed_total = 0;
                                    $sessions_total = 0;
                                    $unique_total = 0;
                                    $users_total = 0;
                                }
                                //start new accordion
                                print '<h3 class="accordion-rows"><span class="accordion-title">' . $monthrecord[0] . '</span></h3>';
                                print '<div class="sub-accordions accordion-padding">';

                                //print headers
                                print '<div class="content-sub-header padding-none">                    
                                    <div class="data-column">Month</div>
                                    <div class="data-column">Users</div>
                                    <div class="data-column">Viewed</div>
                                    <div class="data-column">Sessions</div>
                                    <div class="data-column">Unique</div>
                                </div>';

                                $currentIteration = $monthrecord[0];
                            }
                            $viewed_total += $monthrecord[2];
                            $sessions_total += $monthrecord[3];
                            $unique_total += $monthrecord[4];
                            $users_total += $monthrecord[5];
                            ?>
                            <div class="data-content">  
                                <div class="data-column"><?php print date('M', strtotime($monthrecord[0] . '-' . $monthrecord[1] . '-' . '01')); ?></div>
                                <div class="data-column"><?php print $monthrecord[5]; ?></div>
                                <div class="data-column"><?php print $monthrecord[2]; ?></div>
                                <div class="data-column"><?php print $monthrecord[3]; ?></div>
                                <div class="data-column"><?php print $monthrecord[4]; ?></div>
                            </div>
                            <?php
                        }
                        print '<div class="content-sub-header footer-width padding-none" id="footer-width">
                                <div class="data-column">Total</div>
                                <div class="data-column">' . $users_total . '</div>
                                <div class="data-column">' . $viewed_total . '</div>
                                <div class="data-column">' . $sessions_total . '</div>
                                <div class="data-column">' . $unique_total . '</div>
                            </div>';
                        print '</div>';  //end the previous accordion
                        ?>                          
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<?php
require_once '../include/admin/footer.php';
?>
<script type="text/javascript">
    $(function () {
        $('.accordion-rows a').click(function (event) {
            if ($(this).parent().parent().hasClass("ui-accordion-header")) {
                event.stopPropagation(); // this is
            }
        });
        $("#accordion").accordion({
            collapsible: true,
            heightStyle: 'content',
            active: 0
        });
        $('.token-input-dropdown').css("width", "500px");
    });
</script>
<style type="text/css">
    .menu-items-accodings{width: 100% !important; }
</style>