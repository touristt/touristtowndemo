<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('manage-cart-items', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $item_price = ($_POST['item_price'] != '') ? $_POST['item_price'] : '0.00';
    $item_id = $_POST['item_id'];
    if ($item_id > 0) {
        $sql = sprintf("UPDATE tbl_Listing_Type SET LT_Cost='%s' WHERE LT_ID = '%d'", $item_price, $item_id);
    }
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id  = $item_id ;
        Track_Data_Entry('Store','','Manage Store - Listing Packages',$id,'Update','super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    //update listing for all records
    $sql = "SELECT BL_ID, BL_Billing_Type, BL_CoC_Dis_2, BL_Line_Item_Title, BL_Line_Item_Price FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            LEFT JOIN tbl_Category ON C_ID = BL_C_ID
            WHERE BL_Listing_Type = '" . $_POST['item_id'] . "'
            GROUP BY BL_ID";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($rowListing = mysql_fetch_array($result)) {
        listing_billing($rowListing['BL_Billing_Type'], $rowListing['BL_ID'], $rowListing['BL_CoC_Dis_2'], $rowListing['BL_Line_Item_Title'], $rowListing['BL_Line_Item_Price']);
    }
    header("Location: packages.php");
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Store - Listing Types</div>
        <div class="link">
        </div>
    </div> 
    <div class="left">              
        <?PHP require '../include/nav-cart.php'; ?></div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name">Listing Type</div>
            <div class="data-column padding-none spl-other">Price</div>
            <div class="data-column padding-none spl-other">Save</div>
        </div>            
        <ul class="brief">
            <?PHP
            $sql_types = "SELECT * FROM tbl_Listing_Type ORDER by LT_Order ASC";
            $result_types = mysql_query($sql_types, $db) or die("Invalid query: $sql_types -- " . mysql_error());
            while ($type = mysql_fetch_assoc($result_types)) {
                ?>
                <li class="option-width" id="recordsArray_<?php echo $type['LT_ID'] ?>">
                    <form action="" method="POST">
                        <div class="data-content">
                            <div class="data-column spl-name"><?php echo $type['LT_Name'] ?></div>
                            <div class="data-column spl-other">$<input type="text" name="item_price" size="10" value="<?php echo $type['LT_Cost'] ?>" /></div>
                            <div class="data-column spl-other padding-none">
                                <input type="hidden" name="item_id" value="<?php echo $type['LT_ID'] ?>" />
                                <input type="hidden" name="op" value="save" />
                                <input type="submit" name="save_item" value="Save" />
                            </div>
                        </div>
                    </form>
                </li>
                <?PHP
            }
            ?>
        </ul>
        <?php
        if (isset($_REQUEST['newItem']) && $_REQUEST['newItem'] == 1) {
            ?>
            <form action="" method="POST">
                <div class="data-content">
                    <div class="data-column spl-name"><input type="text" name="item_name" size="40" value="" /></div>
                    <div class="data-column spl-other">$<input type="text" name="item_price" size="10" value="" /></div>
                    <div class="data-column spl-other">
                        <input type="hidden" name="item_id" value="0" />
                        <input type="hidden" name="op" value="save" />
                        <input type="submit" name="save_item" value="Save" />
                    </div>
                </div>
            </form>
            <?PHP
        }
        ?>
    </div>
    <div id="dialog-message"></div>
</div>
<script type="text/javascript">
    $(function () {
        $("ul.brief").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Listing_Type&field=LT_Order&id=LT_ID';
                $.post("reorder.php", order, function (theResponse) {
                    $("#dialog-message").html("Packages Re-Ordered");
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                });
            }
        });
    });
</script>
<?PHP
require_once '../include/admin/footer.php';
?>