<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('ammenity-icons', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['id'] > 0) {
    $activeID = $_REQUEST['id'];
} else {
    $sql = "SELECT C_ID FROM tbl_Category WHERE C_Parent = 0 ORDER BY C_Name LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
    $activeID = $row['C_ID'];
}

$sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_ID = '" . encode_strings($activeID, $db) . "' ORDER BY C_Name LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeCat = mysql_fetch_assoc($result);

if ($_POST['op'] == 'save') {
    $sqlTMP = "SELECT * FROM tbl_Ammenity_Icons WHERE AI_ID = '" . encode_strings($_REQUEST['subcat'], $db) . "'";
    $result = mysql_query($sqlTMP, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);

    $sql = "tbl_Ammenity_Icons SET 
            AI_Name = '" . encode_strings($_REQUEST['name'], $db) . "', 
            AI_Search = '" . encode_strings($_REQUEST['search'], $db) . "'";

    require_once '../include/picUpload.inc.php';
    $pic = Upload_Pic('0', 'pic', 25, 25, true, IMG_LOC_ABS . 'ammenity-icons/');
    if ($pic) {
        $sql .= ", AI_Image = '" . encode_strings($pic, $db) . "'";

        if ($row['AI_Image']) {
            Delete_Pic(IMG_LOC_ABS . 'ammenity-icons/' . $row['AI_Image']);
        }
    }

    if ($_REQUEST['subcat'] > 0) {
        $sql = "UPDATE " . $sql . " WHERE AI_ID = '" . encode_strings($_REQUEST['subcat'], $db) . "'";
    } else {
        $sql = "INSERT " . $sql;
    }
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
    }else{
        $_SESSION['error'] = 1;
    }
    header("Location: ammenity-icons.php?id=" . $_REQUEST['id']);
    exit();
} elseif ($_GET['op'] == 'del') {
    require_once '../include/picUpload.inc.php';

    $sqlTMP = "SELECT * FROM tbl_Ammenity_Icons WHERE AI_ID = '" . encode_strings($_REQUEST['subcat'], $db) . "'";
    $result = mysql_query($sqlTMP, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);

    if ($row['AI_Image']) {
        Delete_Pic(IMG_LOC_ABS . 'ammenity-icons/' . $row['AI_Image']);
    }
    $sql = "DELETE tbl_Ammenity_Icons
            FROM tbl_Ammenity_Icons  
            WHERE AI_ID = '" . $_REQUEST['subcat'] . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
    }else{
        $_SESSION['delete_error'] = 1;
    }
    header("Location: ammenity-icons.php?id=" . $_REQUEST['id']);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Amenities - <?php echo $activeCat['C_Name'] ?></div>
        <div class="link">
            <a href="ammenity-icons.php?id=<?php echo $activeCat['C_ID'] ?>&op=new">+Add Icon</a>
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-ammenity-icons.php'; ?>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name-loc"><?php echo $activeCat['C_Name'] ?></div>
            <div class="data-column padding-none spl-other">Delete</div>
        </div>
        <?PHP
        $sql = "SELECT * FROM tbl_Ammenity_Icons ORDER BY AI_Name";
        $result = mysql_query($sql, $db)or die("Invalid query: $sql -- " . mysql_error());
        $counter = 0;
        while ($row = mysql_fetch_assoc($result)) {
            $counter++;
            ?>
            <form enctype="multipart/form-data" id="form-<?php echo $row['AI_ID'] ?>" name="form<?php echo $row['AI_ID'] ?>" method="post" action="ammenity-icons.php">
                <input type="hidden" name="id" value="<?php echo $activeCat['C_ID'] ?>">
                <input type="hidden" name="subcat" value="<?php echo $row['AI_ID'] ?>">
                <input type="hidden" name="op" value="save">
                <div class="data-content">
                    <div class="data-column spl-other padding-top-amunities">
                        <img class="float-left" src="<?php echo IMG_LOC_REL ?>ammenity-icons/<?php echo $row['AI_Image'] ?>" width="25" height="25" />
                        <div class="inputWrapper float-left margin-left-amunities ">&nbsp;&nbsp;Browse
                            <input class="fileInput" type="file" name="pic[]" id="photo-<?php echo $row['AI_ID'] ?>"/>
                        </div></div>
                    <div class="data-column spl-name-permission padding-top-amunities">
                        Title / Alt Tag
                        <input name="name" type="text" id="name-<?php echo $row['AI_ID'] ?>" value="<?php echo $row['AI_Name'] ?>" size="20" />
                    </div>
                    <div class="data-column spl-name-permission margin-left-amunities">Search
                        <input name="search" type="text" id="search-<?php echo $row['AI_ID'] ?>" size="2" maxlength="2" value="<?php echo $row['AI_Search'] ?>">
                        <input class="margin-left-amunities" type="submit" name="button2" id="button2" value="Save" /></div>
                    <div class="data-column spl-other-permission padding-top-amunities">
                        <td align="center" width="16%"><a onClick="return confirm('Are you sure?')" href="ammenity-icons.php?id=<?php echo $row['AI_Category'] ?>&amp;subcat=<?php echo $row['AI_ID'] ?>&amp;op=del">Delete</a></td>
                    </div>
                </div>
            </form>
            <?PHP
        }
        if ($_REQUEST['op'] == 'new') {
            ?>
            <form enctype="multipart/form-data" id="form11" name="form3" method="post" action="ammenity-icons.php">
                <input type="hidden" name="id" value="<?php echo $activeCat['C_ID'] ?>">
                <input type="hidden" name="subcat" value="0">
                <input type="hidden" name="op" value="save">
                <div class="data-content">
                    <div class="data-column spl-other">
                        <div class="inputWrapper">&nbsp;&nbsp;Browse
                            <input class="fileInput" type="file" name="pic[]" id="photo-<?php echo $row['AI_ID'] ?>" required/>
                        </div></div>

                    <div class="data-column spl-name-permission ">
                        Title / Alt Tag

                        <input name="name" type="text" id="name" value="" size="20" required/>
                    </div>
                    <div class="data-column spl-name-permission margin-left-amunities">
                        <input type="submit" name="button" id="button" value="Save" />
                    </div>
                </div>
            </form>

            <?PHP
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>