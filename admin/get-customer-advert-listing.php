<?php
include '../include/config.inc.php';
require_once '../include/login.inc.php';

$where = '';
$join = '';

if (isset($_GET['region_id']) || isset($_GET['cat_id']) || isset($_GET['subcat'])) {
    $sql = "SELECT R_ID, R_Parent FROM tbl_Region WHERE R_ID='" . encode_strings($_GET['region_id'], $db) . "'";
    $result = query($sql, $db);
    $REGION = mysql_fetch_assoc($result);
    $regionList = '';
    if ($REGION['R_Parent'] == 0) {
        $sql = "SELECT R_ID FROM tbl_Region WHERE R_Parent = '" . encode_strings($REGION['R_ID'], $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $first = true;
        $regionList .= "(";
        while ($row = mysql_fetch_assoc($result)) {
            if ($first) {
                $first = false;
            } else {
                $regionList .= ",";
            }
            $regionList .= $row['R_ID'];
        }
        $regionList .= ")";
    } else {
        $regionList = '(' . $REGION['R_ID'] . ')';
    }
    $join = "INNER JOIN tbl_Business_Listing_Category_Region ON BL_ID = BLCR_BL_ID";
    $where = "AND BLCR_BLC_R_ID IN " . encode_strings($regionList, $db) . "";

    if (isset($_GET['cat_id'])) {
        $where .= " AND BL_C_ID = '" . encode_strings($_GET['cat_id'], $db) . "'";
    }
    if (isset($_GET['subcat'])) {
        $join .= " LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID";
        $where .= " AND BLC_C_ID = '" . encode_strings($_GET['subcat'], $db) . "'";
    }
    ?>
    <select class="adv-type-options" name="listing" id="listing" required>
        <option required value="">Select Listing</option>
        <?PHP
        $sql = "SELECT BL_ID, BL_Listing_Title FROM `tbl_Business_Listing` $join WHERE BL_Listing_Type = 5 $where GROUP BY BL_ID ORDER BY BL_Listing_Title ASC";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            ?>
            <option value="<?php echo $row['BL_ID'] ?>"><?php echo $row['BL_Listing_Title'] ?></option>
            <?PHP
        }
        ?>
    </select>
    <?php
} else {
    ?>
    <select class="adv-type-options" name="listing" id="listing">
        <option required value="">Select Listing</option>
    </select>
    <?php
}
?>