<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('billing-history', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
$BL_ID = $_REQUEST['bl_id'];
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
} else {
    header("Location:customers.php");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left">

    <?php require_once '../include/top-nav-listing.php'; ?>

    <div class="title-link">
        <div class="title">Invoices - <?php echo $rowListing['BL_Listing_Title'] ?></div>
    </div>

    <div class="left">
        <?PHP
        require '../include/nav-billing.php';
        if (isset($_REQUEST['yr'])) {
            $YR = $_REQUEST['yr'];
        } else {
            $YR = $Recent_Year;
        }
        ?>
    </div>
    <div class="right">
        <div class="billing-inside-div-">
        </div>
        <div class="data-header"> 
            <div class="data-column">Invoice #</div>
            <div class="data-column">Date</div>
            <div class="data-column">Sub Total</div>
            <div class="data-column">Tax</div>
            <div class="data-column">Total</div>
        </div>                             

        <?PHP
        $listing_invoices = array();
        $listing_invoices_refunds = array();
        $all_listing_invoices = array();
        $all_invoices = array();
        $sql = "SELECT BB_ID, BB_Invoice_Num, BB_BL_ID, BB_Date, BB_Total, BB_Tax, BB_SubTotal3, BB_CoC_Dis_2, BB_Refund_Deleted_Date FROM tbl_Business_Billing WHERE BB_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BB_Deleted = 0 
               AND YEAR(BB_Date) = '" . $YR . "' ORDER BY BB_ID DESC";
        $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $i = 0;
        while ($row = mysql_fetch_assoc($resultTMP)) {
            $listing_invoices[$i] = $row;
            $listing_invoices[$i]['date'] = $row['BB_Date'];
            $i++;
        }
        $sql = "SELECT BB_ID, BB_Invoice_Num, BB_BL_ID, BB_Date, BB_Total, BB_Tax, BB_SubTotal3, BB_CoC_Dis_2, BB_Refund_Deleted, BB_Refund_Deleted_Date FROM tbl_Business_Billing WHERE BB_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BB_Deleted = 0 
               AND YEAR(BB_Refund_Deleted_Date) = '" . $YR . "' AND BB_Refund_Deleted = 1 ORDER BY BB_ID DESC";
        $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $i = 0;
        while ($row = mysql_fetch_assoc($resultTMP)) {
            $listing_invoices_refunds[$i] = $row;
            $listing_invoices_refunds[$i]['date'] = $row['BB_Refund_Deleted_Date'];
            $i++;
        }
        $all_listing_invoices = array_merge($listing_invoices, $listing_invoices_refunds);

        //Division Billing Invoices
        $division_invoices = array();
        $division_invoices_refunds = array();
        $all_division_invoices = array();
        $sql = "SELECT DB_ID, DB_BL_ID, DB_Date, DB_Total, DB_Tax, DB_Subtotal, DB_Discount, DB_Refund_Deleted_Date FROM tbl_Division_Billing WHERE DB_BL_ID = '" . encode_strings($BL_ID, $db) . "' 
                AND DB_Deleted = 0 AND YEAR(DB_Date) = '" . $YR . "' ORDER BY DB_ID DESC";
        $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $i = 0;
        while ($row = mysql_fetch_assoc($resultTMP)) {
            $division_invoices[$i] = $row;
            $division_invoices[$i]['date'] = $row['DB_Date'];
            $i++;
        }
        $sql = "SELECT DB_ID, DB_BL_ID, DB_Date, DB_Total, DB_Tax, DB_Subtotal, DB_Discount, DB_Refund_Deleted, DB_Refund_Deleted_Date FROM tbl_Division_Billing WHERE DB_BL_ID = '" . encode_strings($BL_ID, $db) . "' 
                AND DB_Deleted = 0 AND YEAR(DB_Refund_Deleted_Date) = '" . $YR . "' AND DB_Refund_Deleted = 1 ORDER BY DB_ID DESC";
        $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $i = 0;
        while ($row = mysql_fetch_assoc($resultTMP)) {
            $division_invoices_refunds[$i] = $row;
            $division_invoices_refunds[$i]['date'] = $row['DB_Refund_Deleted_Date'];
            $i++;
        }
        $all_division_invoices = array_merge($division_invoices, $division_invoices_refunds);

        $all_invoices = array_merge($all_listing_invoices, $all_division_invoices);

        function date_sort($a, $b) {
            return strtotime($b['date']) - strtotime($a['date']);
        }

        usort($all_invoices, "date_sort");
//        echo '<pre/>';print_r($all_invoices);exit;

        $gtotal = $tax = $subTotal = $discount3 = $refund_subtotal = $refund_tax = $refund_total = 0;
        foreach ($all_invoices as $invoice) {
            //show refunded columns in red
            $refund_class = "";
            if ((isset($invoice['BB_Refund_Deleted']) && $invoice['BB_Refund_Deleted'] == 1) || (isset($invoice['DB_Refund_Deleted']) && $invoice['DB_Refund_Deleted'] == 1)) {
                $refund_class = " refund";
                $refund_subtotal += (isset($invoice['BB_SubTotal3'])) ? $invoice['BB_SubTotal3'] : $invoice['DB_Subtotal'];
                $refund_tax += (isset($invoice['BB_Tax'])) ? $invoice['BB_Tax'] : $invoice['DB_Tax'];
                $refund_total += (isset($invoice['BB_Total'])) ? $invoice['BB_Total'] : $invoice['DB_Total'];
            } else {
                $gtotal += (isset($invoice['BB_Total'])) ? $invoice['BB_Total'] : $invoice['DB_Total'];
                $tax += (isset($invoice['BB_Tax'])) ? $invoice['BB_Tax'] : $invoice['DB_Tax'];
                $subTotal += (isset($invoice['BB_SubTotal3'])) ? $invoice['BB_SubTotal3'] : $invoice['DB_Subtotal'];
                $discount3 += (isset($invoice['BB_CoC_Dis_2'])) ? $invoice['BB_CoC_Dis_2'] : $invoice['DB_Discount'];
            }
            ?>                                  
            <div class="data-content<?php echo $refund_class; ?>">
                <div class="data-column">
                    <?php if (isset($invoice['BB_Invoice_Num'])) { ?>
                        <a href="customer-bill.php?bl_id=<?php echo $BL_ID; ?>&id=<?php echo $invoice['BB_ID'] ?>">
                            #<?php echo (isset($invoice['BB_Invoice_Num'])) ? $invoice['BB_Invoice_Num'] : $invoice['DB_ID'] ?>
                        </a>

                    <?php } else { ?>
                        <a href="division-billing-invoice.php?db_id=<?php echo $invoice['DB_ID'] ?>">
                            #<?php echo (isset($invoice['BB_Invoice_Num'])) ? $invoice['BB_Invoice_Num'] : $invoice['DB_ID'] ?>
                        </a> 
                    <?php } ?>

                </div>
<!--                <div class="data-column"><?php echo (isset($invoice['date'])) ? date("Y-m-d", strtotime($invoice['date'])) : '' ?></div>
                <div class="data-column">$<?php echo (isset($invoice['BB_SubTotal3'])) ? number_format($invoice['BB_SubTotal3'], 2) : number_format($invoice['DB_Subtotal'], 2) ?></div>
                <div class="data-column">$<?php echo (isset($invoice['BB_Tax'])) ? number_format($invoice['BB_Tax'], 2) : number_format($invoice['DB_Tax'], 2) ?></div>
                <div class="data-column">$<?php echo (isset($invoice['BB_Total'])) ? number_format($invoice['BB_Total'], 2) : number_format($invoice['DB_Total'], 2) ?></div>-->
                <?php
                $BB_SubTotal3 = (isset($invoice['BB_SubTotal3'])) ? number_format($invoice['BB_SubTotal3'], 2) : number_format($invoice['DB_Subtotal'], 2);
                $BB_Tax = (isset($invoice['BB_Tax'])) ? number_format($invoice['BB_Tax'], 2) : number_format($invoice['DB_Tax'], 2);
                $BB_Total = (isset($invoice['BB_Total'])) ? number_format($invoice['BB_Total'], 2) : number_format($invoice['DB_Total'], 2);
                ?>
                <div class="data-column"><?php echo (isset($invoice['date'])) ? date("Y-m-d", strtotime($invoice['date'])) : '' ?></div>
                <div class="data-column"><?php echo ($refund_class == '') ? "$" .$BB_SubTotal3 :  "($" . $BB_SubTotal3 . ")" ?></div>
                <div class="data-column"><?php echo ($refund_class == '') ? "$" .$BB_Tax :  "($" . $BB_Tax . ")" ?></div>
                <div class="data-column"><?php echo ($refund_class == '') ? "$" .$BB_Total :  "($" . $BB_Total . ")" ?></div>
                
            </div> 
            <?PHP
        }
        ?>        
        <div class="data-content">  
            <div class="data-column">Total</div>
            <div class="data-column">&nbsp;</div>
            <div class="data-column">$<?php echo number_format($subTotal, 2) ?></div>
            <div class="data-column">$<?php echo number_format($tax, 2) ?></div>
            <div class="data-column">$<?php echo number_format($gtotal, 2) ?></div>
        </div>
        <div class="data-content refund-color">  
            <div class="data-column">Refunds</div>
            <div class="data-column">&nbsp;</div>
            <div class="data-column">($<?php echo number_format($refund_subtotal, 2) ?>)</div>
            <div class="data-column">($<?php echo number_format($refund_tax, 2) ?>)</div>
            <div class="data-column">($<?php echo number_format($refund_total, 2) ?>)</div>
        </div>
        <div class="data-header">
            <div class="data-column">Customer Total</div>
            <div class="data-column">&nbsp;</div>
            <div class="data-column">$<?php echo number_format(($subTotal - $refund_subtotal), 2) ?></div>
            <div class="data-column">$<?php echo number_format(($tax - $refund_tax), 2) ?></div>
            <div class="data-column">$<?php echo number_format(($gtotal - $refund_total), 2) ?></div>
        </div>
        <div class="data-content">                           
            <div class="data-column">&nbsp;</div>
            <div class="data-column">&nbsp;</div>
            <div class="data-column">&nbsp;</div>
            <div class="data-column">&nbsp;</div>
            <div class="data-column">&nbsp;</div>
        </div>
        <div class="data-content">  
            <div class="data-column">Discount</div>
            <div class="data-column">&nbsp;</div>
            <div class="data-column">&nbsp;</div>
            <div class="data-column">&nbsp;</div>
            <div class="data-column">$<?php echo number_format($discount3, 2) ?></div>
        </div>
    </div>
</div>

<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>