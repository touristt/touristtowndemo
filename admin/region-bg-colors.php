<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} else {
    header("Location: /admin/regions.php");
    exit();
}

$sql = "SELECT tbl_Region_Themes.*, R_ID, R_Name, R_Type FROM tbl_Region_Themes left join tbl_Region on R_ID = RT_RID WHERE RT_RID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$count = mysql_num_rows($result);
$Region_theme = mysql_fetch_assoc($result);

if (isset($_POST['save_color'])) {
    $sql = "tbl_Region_Themes SET 
            RT_RID = '" . encode_strings($_REQUEST['rid'], $db) . "', 
            RT_TopBar_Color = '" . encode_strings($_REQUEST['topbar_background'], $db) . "', 
            RT_TopBar_Link_Color = '" . encode_strings($_REQUEST['topbar_links_color'], $db) . "', 
            RT_SearchButton_Color = '" . encode_strings($_REQUEST['search_button'], $db) . "', 
            RT_Theme_Background_Color = '" . encode_strings($_REQUEST['theme_background'], $db) . "',   
            RT_MainMenu_bg_Color = '" . encode_strings($_REQUEST['main_menu'], $db) . "',   
            RT_MainMenu_font_Color = '" . encode_strings($_REQUEST['main_menu_font'], $db) . "',   
            RT_MainMenu_hover_Color = '" . encode_strings($_REQUEST['main_menu_hover'], $db) . "',     
            RT_Link_Color = '" . encode_strings($_REQUEST['link_color'], $db) . "',   
            RT_Imagebottom_Link_Color = '" . encode_strings($_REQUEST['image_link_color'], $db) . "',   
            RT_ListingMenu_bg_Color = '" . encode_strings($_REQUEST['listing_menu'], $db) . "',   
            RT_ListingMenu_font_Color = '" . encode_strings($_REQUEST['listing_menu_font'], $db) . "',   
            RT_ListingSubMenu_font_Color = '" . encode_strings($_REQUEST['listing_sub_menu_font'], $db) . "',   
            RT_ListingMenu_Sub_Color = '" . encode_strings($_REQUEST['listing_sub_menu'], $db) . "',   
            RT_ListingMenu_hover_Color = '" . encode_strings($_REQUEST['listing_menu_hover'], $db) . "',   
            RT_ListingMenu_arrow_Color = '" . encode_strings($_REQUEST['listing_menu_arrow'], $db) . "',   
            RT_Desktop_logo_alt = '" . encode_strings($_REQUEST['desktop_logo_alt'], $db) . "',   
            RT_ListingContact_Color = '" . encode_strings($_REQUEST['listing_contact'], $db) . "',   
            RT_Container_Heading_Color = '" . encode_strings($_REQUEST['containers_heading'], $db) . "'";
    require '../include/picUpload.inc.php';
    if ($_POST['image_bank_desktop'] == "") {
        // last @param 9 = Region Desktop Logo
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 9, 'Region', $regionID);
        if (is_array($pic)) {
            $sql .= ", RT_Desktop_Logo = '" . encode_strings($pic['0']['0'], $db) . "'";
        }
        $pic_id = $pic['1'];
    } else {
        $pic_id = $_POST['image_bank_desktop'];
        // last @param 9 = Region Desktop Logo
        $pic_response = Upload_Pic_Library($pic_id, 9);
        if ($pic_response) {
            $sql .= ", RT_Desktop_Logo = '" . encode_strings($pic_response['0'], $db) . "'";
        }
    }
    if ($_POST['image_bank_mobile_map'] == "") {
        // last @param 13 = Region Map Icon
        $pic4 = Upload_Pic('4', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 13, 'Region', $regionID);
        if (is_array($pic4)) {
            $sql .= ", RT_Map_Icon = '" . encode_strings($pic4['0']['0'], $db) . "'";
        }
        $pic_id4 = $pic4['1'];
    } else {
        $pic_id4 = $_POST['image_bank_mobile_map'];
        // last @param 13 = Region Map Icon
        $pic_response4 = Upload_Pic_Library($pic_id4, 13);
        if ($pic_response4) {
            $sql .= ", RT_Map_Icon = '" . encode_strings($pic_response4['0']['0'], $db) . "'";
        }
    }

    if ($count > 0) {
        $sql = "UPDATE " . $sql . " WHERE RT_RID = '" . encode_strings($regionID, $db) . "'";
        $result = mysql_query($sql, $db);
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
    }

    if ($result) {
        $_SESSION['success'] = 1;
        //Image usage from image bank.
        if ($pic_id > 0) {
            imageBankUsage($pic_id, 'IBU_R_ID', $regionID, 'IBU_Region_Theme', 'Desktop Logo');
        }
        if ($pic_id1 > 0) {
            imageBankUsage($pic_id1, 'IBU_R_ID', $regionID, 'IBU_Region_Theme', 'Homepage Photo');
        }
        if ($pic_id4 > 0) {
            imageBankUsage($pic_id4, 'IBU_R_ID', $regionID, 'IBU_Region_Theme', 'Map Icon');
        }
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: region-bg-colors.php?rid=" . $regionID);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo  $Region_theme['R_Name'] ?> - Site Backgrounds</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP
        if ($regionID > 0) {
            require '../include/nav-manage-region.php';
        } else {
            echo "&nbsp;";
        }
        ?>
    </div>
    <div class="right">
        <form id="regionForm" action="/admin/region-bg-colors.php" method="post" enctype="multipart/form-data" name="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="rid" value="<?php echo  $regionID ?>">
            <div class="content-header">Add Theme Color</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Top Bar</label>
                <div class="form-data">
                    <div class="color-box-inner"><div class="color-box" id="topbar_background" style="background-color: <?php echo ((isset($Region_theme['RT_TopBar_Color'])) ? $Region_theme['RT_TopBar_Color'] : '') ?>"></div></div>
                    <input type="hidden" name="topbar_background" id="text_colortopbar_background" value="<?php echo ((isset($Region_theme['RT_TopBar_Color'])) ? $Region_theme['RT_TopBar_Color'] : '') ?>">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Top Bar Links</label>
                <div class="form-data">
                    <div class="color-box-inner"><div class="color-box" id="topbar_links_color" style="background-color: <?php echo ((isset($Region_theme['RT_TopBar_Link_Color'])) ? $Region_theme['RT_TopBar_Link_Color'] : '') ?>"></div></div>
                    <input type="hidden" name="topbar_links_color" id="text_colortopbar_links_color" value="<?php echo ((isset($Region_theme['RT_TopBar_Link_Color'])) ? $Region_theme['RT_TopBar_Link_Color'] : '') ?>">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Search Button</label>
                <div class="form-data">
                    <div class="color-box-inner"><div class="color-box" id="search_button" style="background-color: <?php echo ((isset($Region_theme['RT_SearchButton_Color'])) ? $Region_theme['RT_SearchButton_Color'] : '') ?>"></div></div>
                    <input type="hidden" name="search_button" id="text_colorsearch_button" value="<?php echo ((isset($Region_theme['RT_SearchButton_Color'])) ? $Region_theme['RT_SearchButton_Color'] : '') ?>">
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Theme Background</label>
                <div class="form-data">
                    <div class="color-box-inner"><div class="color-box" id="theme_background" style="background-color: <?php echo ((isset($Region_theme['RT_Theme_Background_Color'])) ? $Region_theme['RT_Theme_Background_Color'] : '') ?>"></div></div>
                    <input type="hidden" name="theme_background" id="text_colortheme_background" value="<?php echo ((isset($Region_theme['RT_Theme_Background_Color'])) ? $Region_theme['RT_Theme_Background_Color'] : '') ?>">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Main Menu</label>
                <div class="form-data">
                    <div class="color-box-inner"><div class="color-box" id="main_menu" style="background-color: <?php echo ((isset($Region_theme['RT_MainMenu_bg_Color'])) ? $Region_theme['RT_MainMenu_bg_Color'] : '') ?>"></div></div>
                    <input type="hidden" name="main_menu" id="text_colormain_menu" value="<?php echo ((isset($Region_theme['RT_MainMenu_bg_Color'])) ? $Region_theme['RT_MainMenu_bg_Color'] : '') ?>">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Main Menu Font</label>
                <div class="form-data">
                    <div class="color-box-inner"><div class="color-box" id="main_menu_font" style="background-color: <?php echo ((isset($Region_theme['RT_MainMenu_font_Color'])) ? $Region_theme['RT_MainMenu_font_Color'] : '') ?>"></div></div>
                    <input type="hidden" name="main_menu_font" id="text_colormain_menu_font" value="<?php echo ((isset($Region_theme['RT_MainMenu_font_Color'])) ? $Region_theme['RT_MainMenu_font_Color'] : '') ?>">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Main Menu Hover Font</label>
                <div class="form-data">
                    <div class="color-box-inner"><div class="color-box" id="main_menu_hover" style="background-color: <?php echo ((isset($Region_theme['RT_MainMenu_hover_Color'])) ? $Region_theme['RT_MainMenu_hover_Color'] : '') ?>"></div></div>
                    <input type="hidden" name="main_menu_hover" id="text_colormain_menu_hover" value="<?php echo ((isset($Region_theme['RT_MainMenu_hover_Color'])) ? $Region_theme['RT_MainMenu_hover_Color'] : '') ?>">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Links</label>
                <div class="form-data">
                    <div class="color-box-inner"><div class="color-box" id="link_color" style="background-color: <?php echo ((isset($Region_theme['RT_Link_Color'])) ? $Region_theme['RT_Link_Color'] : '') ?>"></div></div>
                    <input type="hidden" name="link_color" id="text_colorlink_color" value="<?php echo ((isset($Region_theme['RT_Link_Color'])) ? $Region_theme['RT_Link_Color'] : '') ?>">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Image Link Bar</label>
                <div class="form-data">
                    <div class="color-box-inner"><div class="color-box" id="image_link_color" style="background-color: <?php echo ((isset($Region_theme['RT_Imagebottom_Link_Color'])) ? $Region_theme['RT_Imagebottom_Link_Color'] : '') ?>"></div></div>
                    <input type="hidden" name="image_link_color" id="text_colorimage_link_color" value="<?php echo ((isset($Region_theme['RT_Imagebottom_Link_Color'])) ? $Region_theme['RT_Imagebottom_Link_Color'] : '') ?>"> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Listing Menu</label>
                <div class="form-data">
                    <div class="color-box-inner"><div class="color-box" id="listing_menu" style="background-color: <?php echo ((isset($Region_theme['RT_ListingMenu_bg_Color'])) ? $Region_theme['RT_ListingMenu_bg_Color'] : '') ?>"></div></div>
                    <input type="hidden" name="listing_menu" id="text_colorlisting_menu" value="<?php echo ((isset($Region_theme['RT_ListingMenu_bg_Color'])) ? $Region_theme['RT_ListingMenu_bg_Color'] : '') ?>"> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Listing Menu Font</label>
                <div class="form-data">
                    <div class="color-box-inner"><div class="color-box" id="listing_menu_font" style="background-color: <?php echo ((isset($Region_theme['RT_ListingMenu_font_Color'])) ? $Region_theme['RT_ListingMenu_font_Color'] : '') ?>"></div></div>
                    <input type="hidden" name="listing_menu_font" id="text_colorlisting_menu_font" value="<?php echo ((isset($Region_theme['RT_ListingMenu_font_Color'])) ? $Region_theme['RT_ListingMenu_font_Color'] : '') ?>"> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Listing Sub Menu Font</label>
                <div class="form-data">
                    <div class="color-box-inner"><div class="color-box" id="listing_sub_menu_font" style="background-color: <?php echo ((isset($Region_theme['RT_ListingSubMenu_font_Color'])) ? $Region_theme['RT_ListingSubMenu_font_Color'] : '') ?>"></div></div>
                    <input type="hidden" name="listing_sub_menu_font" id="text_colorlisting_sub_menu_font" value="<?php echo ((isset($Region_theme['RT_ListingSubMenu_font_Color'])) ? $Region_theme['RT_ListingSubMenu_font_Color'] : '') ?>"> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Listing Sub Menu</label>
                <div class="form-data">
                    <div class="color-box-inner"><div class="color-box" id="listing_sub_menu" style="background-color: <?php echo ((isset($Region_theme['RT_ListingMenu_Sub_Color'])) ? $Region_theme['RT_ListingMenu_Sub_Color'] : '') ?>"></div></div>
                    <input type="hidden" name="listing_sub_menu" id="text_colorlisting_sub_menu" value="<?php echo ((isset($Region_theme['RT_ListingMenu_Sub_Color'])) ? $Region_theme['RT_ListingMenu_Sub_Color'] : '') ?>"> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Listing Menu Hover</label>
                <div class="form-data">
                    <div class="color-box-inner"><div class="color-box" id="listing_menu_hover" style="background-color: <?php echo ((isset($Region_theme['RT_ListingMenu_hover_Color'])) ? $Region_theme['RT_ListingMenu_hover_Color'] : '') ?>"></div></div>
                    <input type="hidden" name="listing_menu_hover" id="text_colorlisting_menu_hover" value="<?php echo ((isset($Region_theme['RT_ListingMenu_hover_Color'])) ? $Region_theme['RT_ListingMenu_hover_Color'] : '') ?>">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Listing Menu Arrow</label>
                <div class="form-data">
                    <div class="color-box-inner"><div class="color-box" id="listing_menu_arrow" style="background-color: <?php echo ((isset($Region_theme['RT_ListingMenu_arrow_Color'])) ? $Region_theme['RT_ListingMenu_arrow_Color'] : '') ?>"></div></div>
                    <input type="hidden" name="listing_menu_arrow" id="text_colorlisting_menu_arrow" value="<?php echo ((isset($Region_theme['RT_ListingMenu_arrow_Color'])) ? $Region_theme['RT_ListingMenu_arrow_Color'] : '') ?>">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Listing Contact</label>
                <div class="form-data">
                    <div class="color-box-inner"><div class="color-box" id="listing_contact" style="background-color: <?php echo ((isset($Region_theme['RT_ListingContact_Color'])) ? $Region_theme['RT_ListingContact_Color'] : '') ?>"></div></div>
                    <input type="hidden" name="listing_contact" id="text_colorlisting_contact" value="<?php echo ((isset($Region_theme['RT_ListingContact_Color'])) ? $Region_theme['RT_ListingContact_Color'] : '') ?>">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Containers Heading</label>
                <div class="form-data">
                    <div class="color-box-inner"><div class="color-box" id="containers_heading" style="background-color: <?php echo ((isset($Region_theme['RT_Container_Heading_Color'])) ? $Region_theme['RT_Container_Heading_Color'] : '') ?>"></div></div>
                    <input type="hidden" name="containers_heading" id="text_colorcontainers_heading" value="<?php echo ((isset($Region_theme['RT_Container_Heading_Color'])) ? $Region_theme['RT_Container_Heading_Color'] : '') ?>">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Desktop Logo</label>
                <div class="form-data div_image_library cat-region-width">
                    <div class="cropit-image-preview aboutus-photo-perview">
                        <img class="preview-img preview-img-script9" style="display: none;" src="">    
                        <?php if ($Region_theme['RT_Desktop_Logo'] != '') { ?>
                            <img class="existing-img existing_imgs9" src="<?php echo  IMG_LOC_REL . $Region_theme['RT_Desktop_Logo'] ?>" >
                        <?php } ?>
                    </div>
                    <span class="daily_browse" onclick="choose_file(9);">Select Photo</span>
                    <div class="dialog-close dialog9 dialog-bank" style="display: none;">
                        <label for="photo9" class="daily_browse">Upload File</label>
                        <a onclick="show_image_library(9)">Add from Library</a>
                    </div>
                    <input type="hidden" name="image_bank_desktop" id="image_bank9" value="">
                    <input type="file" onchange="show_file_name(9, this)" name="pic[]" id="photo9" style="display: none;">
                    <input class="region-logo-prvw margin-theme-logo" type="text" placeholder="Alt name" name="desktop_logo_alt" value="<?php echo $Region_theme['RT_Desktop_logo_alt'] ?>">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Mobile Map Icon</label>
                <div class="form-data div_image_library cat-region-width">
                    <div class="cropit-image-preview aboutus-photo-perview">
                        <img class="preview-img preview-img-script13" style="display: none;" src="">    
                        <?php if ($Region_theme['RT_Map_Icon'] != '') { ?>
                            <img class="existing-img existing_imgs13" src="<?php echo  IMG_LOC_REL . $Region_theme['RT_Map_Icon'] ?>" >
                        <?php } ?>
                    </div>
                    <span class="daily_browse" onclick="choose_file(13);">Select Photo</span>
                    <div class="dialog-close dialog13 dialog-bank" style="display: none;">
                        <label for="photo13" class="daily_browse">Upload File</label>
                        <a onclick="show_image_library(13)">Add from Library</a>
                    </div>
                    <input type="hidden" name="image_bank_mobile_map" id="image_bank13" value="">
                    <input type="file" onchange="show_file_name(13, this)" name="pic[]" id="photo13" style="display: none;">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="save_color" id="submit_button" value="Submit" />
                </div>
            </div>

        </form>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>
<script>
    $(window).load(function(){
        var regionID = '<?php echo  $regionID ?>';
        if(typeof regionID != 'undefined' && regionID > 0){
            $( ".color-box" ).click(function() {
                var div_id = $(this).attr('id');     
                var savedColor = $('#'+div_id).attr('style').split("#");
                $('.color-box').colpickSetColor(savedColor[1]);
            });
        }
    });
    $(document).ready(function(){
        //*****************************************color box function*************************
        $('.color-box').colpick({
            colorScheme:'dark',
            layout:'rgbhex',
            color:'ff8800',
            onSubmit:function(hsb,hex,rgb,el) {
                $(el).css('background-color', '#'+hex);
                var div_id = $(el).attr('id');
                //change app background color
                document.getElementById("text_color"+div_id).value='#'+hex;
                $(el).colpickHide();
                
            }
        });
    });
</script>
<?PHP
require_once '../include/admin/footer.php';
?>
