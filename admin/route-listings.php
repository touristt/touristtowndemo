<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('manage-routes', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} else {
    header("Location: /admin/regions.php");
    exit();
}

if ($_REQUEST['id'] > 0) {
    $routeID = $_REQUEST['id'];
} else {
    header("Location: /admin/regions.php");
    exit();
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $IRML_ID = $_REQUEST['id'];
    $IR_ID = $_REQUEST['irid'];
    $RID = $_REQUEST['rid'];
    $sql = "DELETE FROM tbl_Individual_Route_Map_Listings WHERE IRML_ID = '" . $IRML_ID . "'";
    $result = mysql_query($sql) or die(mysql_error());
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $RID, 'Manage Route Listings', $IR_ID, 'Delete Route Listing', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: route-listings.php?rid=" . $RID . "&id=" . $IR_ID);
    exit();
}
//Get Active Region Information
$sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);
//Get Active Route Information
$sqlRoute = "SELECT IR_Title FROM tbl_Individual_Route WHERE IR_ID = '" . encode_strings($routeID, $db) . "' LIMIT 1";
$resRoute = mysql_query($sqlRoute, $db) or die("Invalid query: $sqlRoute -- " . mysql_error());
$activeRoute = mysql_fetch_assoc($resRoute);

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Manage Routes</div>
        <div class="link"><a href="routes.php?rid=<?php echo $regionID ?>">Individual Routes</a></div>
        <div class="link"><a href="routes-categories.php?rid=<?php echo $regionID ?>">Route Categories</a></div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">
            Manage Route Listings - <?php echo $activeRoute['IR_Title'] ?>

        </div>
        <div class="content-sub-header link-header region-header-padding">
            <form name="route_listings_form" id="route_listings_form" method="POST">
                <input type="hidden" name="rid" value="<?php echo $regionID ?>">
                <input type="hidden" id="rhid" name="irid" value="<?php echo $routeID ?>">
                <input type="hidden" name="op" value="save">
                <div class="data-column spl-org-listngs padding-org-listing">
                    <select name="region" id="region" class="stories-cat" onchange="filterListings();" required>
                        <option value="">Region</option>
                        <?PHP
                        $sqlR = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Type NOT IN(1,6,8) AND R_Status = 1 ORDER BY R_Name ASC";
                        $resR = mysql_query($sqlR, $db) or die("Invalid query: $sqlR -- " . mysql_error());
                        while ($rowR = mysql_fetch_assoc($resR)) {
                            ?>
                            <option value="<?php echo $rowR['R_ID'] ?>"><?php echo $rowR['R_Name'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <select name="category" id="category" class="stories-cat" onchange="filterListings();" required>
                        <option value="">Category</option>
                    </select>
                    <select name="subcategory" id="subcategory" class="stories-cat" onchange="filterListings();" required>
                        <option value="">Sub-Category</option>
                    </select>
                </div>
                <div class="data-column spl-org-listngs padding-org-listing">
                    <select name="listings" id="listings" class="stories" required>
                        <option value="">Listings</option>
                    </select>
                    <input type="checkbox" name="mustsee" id="mustsee" value="1" /> Must See 
                    <input type="submit" value="+Add" class="stories-add-button" onclick="return saveRouteListing();" />
                </div>

            </form>
        </div>
        <div class="content-sub-header">
            <div class="data-column mup-name padding-none">Name</div>
            <div class="data-column spl-other padding-none">&nbsp;</div>
            <div class="data-column spl-other padding-none">Must See</div>
            <div class="data-column spl-other padding-none">Remove</div>
        </div>
        <div class="reorder-category">
            <?PHP
            $sqlListings = "SELECT BL_ID, BL_Listing_Title, IRML_ID, IRML_IR_ID, IRML_Must_See FROM tbl_Business_Listing 
                            INNER JOIN tbl_Individual_Route_Map_Listings ON IRML_BL_ID = BL_ID 
                            WHERE IRML_IR_ID = '" . $routeID . "' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY))
                            GROUP BY BL_ID
                            ORDER BY IRML_Order ASC";

            $resListings = mysql_query($sqlListings, $db) or die("Invalid query: $sqlListings -- " . mysql_error());
            while ($rowListings = mysql_fetch_assoc($resListings)) {
                ?>
                <div class="data-content" id="recordsArray_<?php echo $rowListings['IRML_ID'] ?>">
                    <div class="data-column mup-name">
                        <?php echo $rowListings['BL_Listing_Title']; ?>
                    </div>
                    <div class="data-column spl-other">&nbsp;</div>
                    <div class="data-column spl-other"><input onchange="updateRouteListing(<?php echo $rowListings['IRML_ID'] ?>, this);" type="checkbox" name="mustsee" <?php echo ($rowListings['IRML_Must_See'] == 1) ? 'checked' : '' ?> /></div>
                    <div class="data-column spl-other">
                        <a onclick="return confirm('Are you sure?')" href="route-listings.php?op=del&rid=<?php echo $activeRegion['R_ID'] ?>&id=<?php echo $rowListings['IRML_ID'] ?>&irid=<?php echo $rowListings['IRML_IR_ID'] ?>">Remove</a>
                    </div>
                </div>
                <?PHP
            }
            ?>
        </div>
    </div>
</div>
<div id="dialog-message"></div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script>
    $(function () {
        $(".reorder-category").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Individual_Route_Map_Listings&field=IRML_Order&id=IRML_ID';
                $.post("reorder.php", order, function (theResponse) {
                    swal("Listings Re-Ordered!", "Route Listings Re-Ordered successfully.", "success");
                });
            }
        });
    });
    //filter listing
    function filterListings() {
        var region = $('#region').val();
        var category = $('#category').val();
        var subcategory = $('#subcategory').val();
        var hiddenId = $('#rhid').val();
        $.ajax({
            type: "POST",
            url: 'filterListings.php',
            dataType: 'json',
            
            data: {
                region: region,
                category: category,
                subcategory: subcategory,
                hiddenId : hiddenId
            },
            success: function (response) {

                if (response.maincat && response.maincat !== '') {
                    $('#category').html(response.maincat);
                }
                if (response.subcat && response.subcat !== '') {
                    $('#subcategory').html(response.subcat);
                }
                if (response.listings && response.listings !== '') {
                    $('#listings').html(response.listings);
                }
            }
        });
    } 
    //save listing against route
    function saveRouteListing() {
        var formdata = $('#route_listings_form').serialize(); 
        $.ajax({
            type: "POST",
            url: 'saveRouteListing.php',
            dataType: 'json',
            data: formdata,
            success: function (response) {
                filterListings();
                if (response.success && response.success == 1) {
                   swal("Data Saved", "Data has been saved successfully.", "success");
               }
                if (response.error && response.error == 1) {
                    swal("Error", "Error saving data. Please try again.", "error");
                }
                if (response.listings && response.listings !== '') {
                    $('.reorder-category').html(response.listings);
                }
            }
            
        });
        return false;
    }
    //update listing against route
    function updateRouteListing(IRML_ID, must_see) {
        MUSTSEE = ($(must_see).is(':checked')) ? 1 : 0;
        var region = '<?php echo $regionID ?>';
        $.ajax({
            type: "POST",
            url: 'updateRouteListing.php',
            dataType: 'json',
            data: {
                IRML_ID: IRML_ID,
                MUSTSEE: MUSTSEE,
                rid: region     // sending region id for track function
            },
            success: function (response) {

                if (response.success && response.success == 1) {
                    swal("Data Saved", "Data has been saved successfully.", "success");
                }
                if (response.error && response.error == 1) {
                    swal("Error", "Error saving data. Please try again.", "error");
                }
            }
        });
        return false;
    }
</script>