<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';
require_once '../include/track-data-entry.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
$sql = "SELECT * FROM tbl_Email LEFT JOIN tbl_Email_Detail ON ED_Email_ID=E_ID ";
if ($_REQUEST['op'] == 'save') {
    $month = $_REQUEST['month'];
    $srchBytitle = $_REQUEST['srchBytitle'];
    $sortregion = $_REQUEST['sortregion'];
    $sortby = $_REQUEST['sortby'];
    if ($sortby > 0) {
        $sortbySub = $_REQUEST['sortbySub'];
    } else {
        $sortbySub = 0;
    }
    $sortbyType = $_REQUEST['sortbyType'];
    $sortbyListing = $_REQUEST['sortbyListing'];
    if ($srchBytitle != '' || $sortregion != '' || $sortby != '' || $sortbySub != '' || $sortbyType != '' || $month != '') {
        $sql .= " where ";
    }
    if ($srchBytitle != '') {
        $sql .= " E_Title like '%$srchBytitle%' ";
    }
    if ($sortregion != '') {
        if ($srchBytitle != '') {
            $sql .= " and ";
        }
        $sql .= " FIND_IN_SET($sortregion, ED_R_ID)";
    }
    if ($sortby != '') {
        if ($srchBytitle != '' || $sortregion != '') {
            $sql .= " and ";
        }
        $sql .= " FIND_IN_SET($sortby, ED_C_ID)";
    }
    if ($sortbySub != '') {
        if ($srchBytitle != '' || $sortregion != '' || $sortby != '') {
            $sql .= " and ";
        }
        $sql .= " FIND_IN_SET($sortbySub, ED_SC_ID)";
    }
    if ($sortbyType != '') {
        if ($srchBytitle != '' || $sortregion != '' || $sortby != '') {
            $sql .= " and ";
        }
        $sql .= " FIND_IN_SET($sortbyType, ED_Listing_Type)";
    }
    if ($sortbyListing != '') {
        if ($srchBytitle != '' || $sortregion != '' || $sortby != '' || $sortbyType != '') {
            $sql .= " and ";
        }
        $sql .= " FIND_IN_SET($sortbyListing, ED_BL_ID)";
    }
    if ($month != '') {
        if ($srchBytitle != '' || $sortregion != '' || $sortby != '' || $sortbySub != '') {
            $sql .= " and ";
        }
        $sql .= " DATE_FORMAT(E_Date,'%m')='" . encode_strings($_REQUEST['month'], $db) . "'";
    }
}
$sql .= " GROUP BY E_ID  ORDER BY E_Date DESC ";
if ($_GET['op'] == 'del') {
    $getFile = mysql_query("SELECT E_Attachment FROM tbl_Email WHERE E_ID = '" . encode_strings($_REQUEST['id'], $db) . "'");
    $fileDel = mysql_fetch_assoc($getFile);
    $delRecord = mysql_query("DELETE FROM tbl_Email where E_ID= '" . encode_strings($_REQUEST['id'], $db) . "'");
    mysql_query("DELETE FROM tbl_Email_Detail where E_Email_ID= '" . encode_strings($_REQUEST['id'], $db) . "'");
    if ($delRecord) {
        require_once '../include/picUpload.inc.php';
        Delete_Pic('emailattachments/' . $fileDel['E_Attachment']);
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY 
        $id = $_REQUEST['id'];
        Track_Data_Entry('Email', '', 'Email Archive', $id, 'Delete', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("location: archive-email.php");
    exit;
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Emails</div>
        <div class="link">
            <form method="GET" id="date_filter_form" action="">
                <input type="hidden" name="op" value="save">
                <div class="accounting-dd">
                    <input type="text" name="srchBytitle" placeholder="Search by Title" value="<?php echo $srchBytitle ?>">
                    <?php
                    $sql_region = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Domain != '' AND R_Parent != 0";
                    $result_region = mysql_query($sql_region, $db) or die("Invalid query: $sql_region -- " . mysql_error());
                    ?>
                    <select name="sortregion" onchange="applyfilter()">
                        <option value="">Select Region</option>  
                        <?php
                        while ($rowRegion = mysql_fetch_assoc($result_region)) {
                            ?>
                            <option value="<?php echo $rowRegion['R_ID'] ?>" <?php echo ($sortregion == $rowRegion['R_ID']) ? 'selected="selected"' : ''; ?>><?php echo $rowRegion['R_Name'] ?></option>  
                            <?php
                        }
                        ?>
                    </select>
                    <select name="sortby" id="sortby" onchange="applyfilter()" style="width: 100px;">
                        <option value="">Select Category:</option>
                        <?PHP
                        if ($sortregion > 0) {
                            $sql_sortby = " SELECT * FROM tbl_Category 
                                            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($sortregion, $db) . "'
                                            WHERE C_Parent = 0 ORDER BY RC_Order ASC";
                        } else {
                            $sql_sortby = "SELECT * FROM tbl_Category WHERE C_Parent = 0 ORDER BY C_Name";
                        }
                        $result_sortby = mysql_query($sql_sortby, $db) or die("Invalid query: $sql_sortby -- " . mysql_error());
                        while ($row = mysql_fetch_assoc($result_sortby)) {
                            ?>
                            <option value="<?php echo $row['C_ID'] ?>" <?php echo ($sortby == $row['C_ID']) ? 'selected="selected"' : ''; ?>><?php echo ($row['RC_Name'] != '') ? $row['RC_Name'] : $row['C_Name'] ?></option>
                            <?PHP
                        }
                        ?>
                    </select>
                    <select name="sortbySub" onchange="applyfilter()">
                        <option value="">Select Subcategory:</option>
                        <?PHP
                        if ($sortby > 0) {
                            $sql_sortsub = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = $sortby ORDER BY C_Name";
                            $result_sortsub = mysql_query($sql_sortsub, $db) or die("Invalid query: $sql_sortsub -- " . mysql_error());
                            while ($row = mysql_fetch_assoc($result_sortsub)) {
                                ?>
                                <option value="<?php echo $row['C_ID'] ?>" <?php echo ($sortbySub == $row['C_ID']) ? 'selected="selected"' : ''; ?>><?php echo $row['C_Name'] ?></option>
                                <?PHP
                            }
                        }
                        ?>
                    </select>
                    <select name="sortbyType" onchange="applyfilter()">
                        <option value="">Select Listing Type:</option>
                        <option value="2" <?php echo ($sortbyType == '2') ? 'selected' : "" ?>>All</option>
                        <?PHP
                        $sql_type = "SELECT * FROM tbl_Listing_Type";
                        $result_type = mysql_query($sql_type, $db) or die("Invalid query: $sql_type -- " . mysql_error());
                        while ($row = mysql_fetch_assoc($result_type)) {
                            ?>
                            <option value="<?php echo $row['LT_ID'] ?>" <?php echo ($sortbyType == $row['LT_ID']) ? 'selected="selected"' : ''; ?>><?php echo $row['LT_Name'] ?></option>
                            <?PHP
                        }
                        ?>
                    </select>

                    <select name="sortbyListing" onchange="applyfilter()">
                        <option value="">Select Listing:</option>
                        <?PHP
                        $selectType = "";
                        if (isset($sortbyType) && $sortbyType > 0 && $sortbyType != '2') {
                            $selectType = " AND BL_Listing_Type = " . $sortbyType;
                        }
                        if ($sortbySub != "" && $sortby != "" && $sortregion == "") {
                            //Select Listings if category and subcategory selected on the basis of listing type (if available)
                            $select = " SELECT BL_ID, BL_Listing_Title FROM tbl_Business_Listing 
                                        LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
                                        LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
                                        WHERE (BLC_C_ID IN ($sortbySub) AND BL_C_ID IN ($sortby)) AND (B_Email != '' OR BL_Email != '') $selectType 
                                        AND hide_show_listing = 1 GROUP BY BL_ID ORDER BY BL_Listing_Title";

                            $result_listings = mysql_query($select, $db) or die("Invalid query: $select -- " . mysql_error());
                            while ($row = mysql_fetch_assoc($result_listings)) {
                                ?>
                                <option value="<?php echo $row['BL_ID'] ?>" <?php echo ($sortbyListing == $row['BL_ID']) ? 'selected="selected"' : ''; ?>><?php echo $row['BL_Listing_Title'] ?></option>
                                <?PHP
                            }
                        } else if ($sortby != "" && $sortregion == "" && $sortbySub == "") {
                            //Select Listings if category selected on the basis of listing type (if available)
                            $select = " SELECT BL_ID, BL_Listing_Title FROM tbl_Business_Listing 
                                        LEFT JOIN tbl_Business ON BL_B_ID = B_ID
                                        WHERE BL_C_ID IN ($sortby) AND (B_Email != '' OR BL_Email != '') $selectType AND hide_show_listing = 1 
                                        GROUP BY BL_ID ORDER BY BL_Listing_Title";

                            $result_listings = mysql_query($select, $db) or die("Invalid query: $select -- " . mysql_error());
                            while ($row = mysql_fetch_assoc($result_listings)) {
                                ?>
                                <option value="<?php echo $row['BL_ID'] ?>" <?php echo ($sortbyListing == $row['BL_ID']) ? 'selected="selected"' : ''; ?>><?php echo $row['BL_Listing_Title'] ?></option>
                                <?PHP
                            }
                        } else if ($sortregion != "" && $sortby != "" && $sortbySub == "") {
                            //Select Listings if category and region selected on the basis of listing type (if available)
                            $select = " SELECT BL_ID, BL_Listing_Title FROM tbl_Business_Listing 
                                        LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
                                        LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
                                        INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID 
                                        WHERE BL_C_ID IN ($sortby) AND BLCR_BLC_R_ID IN ($sortregion) AND (B_Email != '' OR BL_Email != '') $selectType 
                                        AND hide_show_listing = 1 GROUP BY BL_ID ORDER BY BL_Listing_Title";

                            $result_listings = mysql_query($select, $db) or die("Invalid query: $select -- " . mysql_error());
                            while ($row = mysql_fetch_assoc($result_listings)) {
                                ?>
                                <option value="<?php echo $row['BL_ID'] ?>" <?php echo ($sortbyListing == $row['BL_ID']) ? 'selected="selected"' : ''; ?>><?php echo $row['BL_Listing_Title'] ?></option>
                                <?PHP
                            }
                        } else if ($sortregion != "" && $sortby == "" && $sortbySub == "") {
                            //Select Listings if region selected on the basis of listing type (if available)
                            $select = " SELECT BL_ID, BL_Listing_Title FROM tbl_Business_Listing 
                                        LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
                                        LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
                                        INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID 
                                        WHERE BLCR_BLC_R_ID IN ($sortregion) AND (B_Email != '' OR BL_Email != '') $selectType AND hide_show_listing = 1 
                                        GROUP BY BL_ID ORDER BY BL_Listing_Title";

                            $result_listings = mysql_query($select, $db) or die("Invalid query: $select -- " . mysql_error());
                            while ($row = mysql_fetch_assoc($result_listings)) {
                                ?>
                                <option value="<?php echo $row['BL_ID'] ?>" <?php echo ($sortbyListing == $row['BL_ID']) ? 'selected="selected"' : ''; ?>><?php echo $row['BL_Listing_Title'] ?></option>
                                <?PHP
                            }
                        } else if ($sortregion != "" && $sortby != "" && $sortbySub != "") {
                            //Select Listings if region selected on the basis of listing type (if available)
                            $select = " SELECT BL_ID, BL_Listing_Title FROM tbl_Business_Listing 
                                        LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
                                        LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
                                        INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID 
                                        WHERE BLCR_BLC_R_ID IN ($sortregion) AND (BLC_C_ID IN ($sortbySub) AND BL_C_ID IN ($sortby)) AND 
                                        (B_Email != '' OR BL_Email != '') $selectType AND hide_show_listing = 1 GROUP BY BL_ID ORDER BY BL_Listing_Title";

                            $result_listings = mysql_query($select, $db) or die("Invalid query: $select -- " . mysql_error());
                            while ($row = mysql_fetch_assoc($result_listings)) {
                                ?>
                                <option value="<?php echo $row['BL_ID'] ?>" <?php echo ($sortbyListing == $row['BL_ID']) ? 'selected="selected"' : ''; ?>><?php echo $row['BL_Listing_Title'] ?></option>
                                <?PHP
                            }
                        }
                        ?>
                    </select>
                    <select name="month" onchange="applyfilter()" style="width: 60px;">
                        <option value="">Month</option>
                        <option value="01" <?php echo ($month == 1) ? "selected" : ""; ?>>January</option>
                        <option value="02" <?php echo ($month == 2) ? "selected" : ""; ?>>February</option>
                        <option value="03" <?php echo ($month == 3) ? "selected" : ""; ?>>March</option>
                        <option value="04" <?php echo ($month == 4) ? "selected" : ""; ?>>April</option>
                        <option value="05" <?php echo ($month == 5) ? "selected" : ""; ?>>May</option>
                        <option value="06" <?php echo ($month == 6) ? "selected" : ""; ?>>June</option>
                        <option value="07" <?php echo ($month == 7) ? "selected" : ""; ?>>July</option>
                        <option value="08" <?php echo ($month == 8) ? "selected" : ""; ?>>August</option>
                        <option value="09" <?php echo ($month == 9) ? "selected" : ""; ?>>September</option>
                        <option value="10" <?php echo ($month == 10) ? "selected" : ""; ?>>October</option>
                        <option value="11" <?php echo ($month == 11) ? "selected" : ""; ?>>November</option>
                        <option value="12" <?php echo ($month == 12) ? "selected" : ""; ?>>December</option>
                    </select>
                    <input type="submit" class="filter-btn" name="filter" value="Filter">
                </div>
            </form>
        </div>
    </div>
    <div class="left"><?php require_once('../include/nav-home-sub.php'); ?></div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none aepl-title">Title</div>
            <div class="data-column padding-none aepl-other">Date Sent</div>
            <div class="data-column padding-none aepl-other">Emails Sent</div>
            <div class="data-column padding-none aepl-other">Attachments</div>
            <div class="data-column padding-none aepl-other">Delete</div>
        </div>
        <?PHP
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $pages = new Paginate(mysql_num_rows($result), 30);
        $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            ?>

            <div class="data-content">
                <div class="data-column aepl-title">
                    <a href="email-detail.php?id=<?php echo $row['E_ID'] ?>"><?php echo $row['E_Title']; ?></a>
                </div>
                <div class="data-column aepl-other">
                    <?php echo date('M d, Y', strtotime($row['E_Date'])); ?>
                </div>
                <div class="data-column aepl-other">
                    <?php echo $row['E_Count']; ?>
                </div>
                <div class="data-column aepl-other">
                    <a href="emailattachments/<?php echo $row['E_Attachment'] ?>"><?php echo $row['E_Attachment'] ?></a>
                </div>
                <div class="data-column aepl-other"><a onClick="return confirm('Are you sure this action can not be undone!');" href="archive-email.php?op=del&amp;id=<?php echo $row['E_ID'] ?>">Delete</a></div>
            </div>
            <?php
        }
        if (isset($pages)) {
            echo $pages->paginate();
        }
        ?>
    </div>
</div>
<script type="text/javascript">
    function applyfilter()
    {
        $('#date_filter_form').submit();
    }
</script>
<?PHP
require_once '../include/admin/footer.php';
?>