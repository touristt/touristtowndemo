<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if (!in_array('manage-routes', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} else {
    header('location: regions.php');
    exit();
}
if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $activeRouteID = $_REQUEST['id'];
} else {
    $activeRouteID = 0;
}

$where = "";
$regionDeleteWhere = "";
$checked = "";
$community_class = ""; //for hiding community option when bruce county
//if county admin
if (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
    $regionDeleteWhere = 'AND IRR_R_ID IN (' . $_SESSION['USER_LIMIT'] . ')';
    $routeCheck = mysql_query("SELECT IR_ID FROM tbl_Individual_Route LEFT JOIN tbl_Individual_Route_Region ON IR_ID = IRR_IR_ID WHERE IRR_R_ID IN ('" . encode_strings($_SESSION['USER_LIMIT'], $db) . "')");
    $routeArray = array();
    while ($routeRow = mysql_fetch_array($routeCheck)) {
        $routeArray[] = $routeRow['IR_ID'];
    }
    if ($activeRouteID > 0 && !in_array($activeRouteID, $routeArray)) {
        header("location:stories.php");
    }
    $where = ' AND R_ID IN (' . $_SESSION['USER_LIMIT'] . ')';
    if ($activeRouteID == 0) {
        $checked = '1';
    }
    $community_class = "display: none;";
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $sql = "tbl_Individual_Route SET ";

    require '../include/picUpload.inc.php';
    if ($_POST['image_bank_desktop'] == "") {
        // last @param 30 = Route Main Image
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 32);
        if (is_array($pic)) {
            $sql .= "IR_Photo = '" . encode_strings($pic['0']['0'], $db) . "',
                    IR_Mobile_Photo = '" . encode_strings($pic['0']['1'], $db) . "',
                    IR_Feature_Photo = '" . encode_strings($pic['0']['2'], $db) . "',";
        }
        $pic_id = $pic['1'];
    } else {
        $pic_id = $_POST['image_bank_desktop'];
        // last @param 4 = Region Category Main Image
        $pic_response = Upload_Pic_Library($pic_id, 32);
        if ($pic_response) {
            $sql .= "IR_Photo = '" . encode_strings($pic_response['0'], $db) . "',
                    IR_Mobile_Photo = '" . encode_strings($pic_response['1'], $db) . "',
                    IR_Feature_Photo = '" . encode_strings($pic_response['2'], $db) . "',";
        }
    }

    //for KML upload
    $kml_name = str_replace(' ', '_', $_FILES['route_kml']['name']);
    if ($kml_name != "") {
        $random_name = date('Y-m-d') . rand(0, 99999) . $kml_name;
        $filePath = MAP_LOC_ABS . $random_name;
        move_uploaded_file($_FILES["route_kml"]["tmp_name"], $filePath);
        $sql .= "IR_KML_Path='$random_name',";
    }

    if (strlen($_REQUEST['route_video']) > 5) {
        $video_url = $_REQUEST['route_video'];
    } else {
        $video_url = '';
    }
    $zoom = ($_REQUEST['route_zoom'] > 0) ? $_REQUEST['route_zoom'] : 0;
    $sql .= " IR_Video = '" . encode_strings($video_url, $db) . "',
            IR_Title = '" . encode_strings($_REQUEST['route_title'], $db) . "',
            IR_Sub_Title = '" . encode_strings($_REQUEST['route_subtitle'], $db) . "',
            IR_Start_Point = '" . encode_strings($_REQUEST['route_start'], $db) . "',
            IR_End_Point = '" . encode_strings($_REQUEST['route_end'], $db) . "',
            IR_Distance = '" . encode_strings($_REQUEST['route_distance'], $db) . "',
            IR_Surface = '" . encode_strings($_REQUEST['route_surface'], $db) . "',
            IR_Introduction_Text = '" . encode_strings($_REQUEST['route_introduction'], $db) . "',
            IR_Description = '" . encode_strings($_REQUEST['route_description'], $db) . "',
            IR_Alt_Tag = '" . encode_strings($_REQUEST['route_alt'], $db) . "',
            IR_Slider_Title = '" . encode_strings($_REQUEST['slider_title'], $db) . "',
            IR_Slider_Description = '" . encode_strings($_REQUEST['slider_description'], $db) . "',
            IR_SEO_Title = '" . encode_strings($_REQUEST['seo_title'], $db) . "',
            IR_SEO_Keywords = '" . encode_strings($_REQUEST['seo_keywords'], $db) . "',
            IR_SEO_Description = '" . encode_strings($_REQUEST['seo_description'], $db) . "',
            IR_Lat = '" . encode_strings($_REQUEST['route_lat'], $db) . "',
            IR_Long = '" . encode_strings($_REQUEST['route_long'], $db) . "',
            IR_Zoom  = '" . encode_strings($zoom, $db) . "'";
    if ($_POST['id'] > 0) {
        $routeID = $_POST['id'];
        $sql = "UPDATE " . $sql . " WHERE IR_ID = '" . encode_strings($_POST['id'], $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        // TRACK DATA ENTRY
        $id = $_REQUEST['rid'];
        Track_Data_Entry('Websites', $id, 'Manage Individual Route', $routeID, 'Update Route', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $routeID = mysql_insert_id();
        // TRACK DATA ENTRY
        $id = $_REQUEST['rid'];
        Track_Data_Entry('Websites', $id, 'Manage Individual Route', $routeID, 'Add Route', 'super admin');
    }
    //insert regions against Routes
    $sql = "DELETE FROM tbl_Individual_Route_Region WHERE IRR_IR_ID = $routeID $regionDeleteWhere";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    foreach ($_REQUEST['childRegion'] as $child) {
        $sql = "tbl_Individual_Route_Region SET IRR_IR_ID = $routeID, IRR_R_ID = '$child'";
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    }
    if ($result) {
        $_SESSION['success'] = 1;
        //Image usage from image bank.
        if ($pic_id > 0) {
            imageBankUsage($pic_id, 'IBU_Individual_Route', $routeID);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    header('location: route.php?rid=' . $_REQUEST['rid'] . '&id=' . $routeID);
    exit();
}
if (isset($_GET['op']) && $_GET['op'] == 'del') {
    
    $update = "UPDATE tbl_Individual_Route SET IR_Photo = '', IR_Mobile_Photo='', IR_Feature_Photo='' WHERE IR_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['rid'];
        $routeID = $_REQUEST['id'];
        Track_Data_Entry('Websites', $id, 'Manage Individual Route', $routeID, 'Delete Photo', 'super admin');
        imageBankUsageDelete('IBU_Individual_Route', $_REQUEST['id']);
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header('location: route.php?rid=' . $_REQUEST['rid'] . '&id=' . $_REQUEST['id']);
    exit;
}
//Get Active Region Information
$sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);
//Get current Route detail

if (isset($activeRouteID) && $activeRouteID > 0) {
    $sqlRoute = "SELECT IR_ID, IR_Title, IR_Sub_Title, IR_Start_Point, IR_End_Point, IR_Distance, IR_Surface, IR_Introduction_Text, IR_Description, IR_Photo, IR_Slider_Title, IR_Slider_Description, IR_Alt_Tag, IR_Video, IR_SEO_Title, IR_SEO_Keywords, IR_SEO_Description, IR_Lat,
IR_Long, IR_Zoom, IR_KML_Path FROM tbl_Individual_Route WHERE IR_ID = '" . encode_strings($activeRouteID, $db) . "'";
    $resRoute = mysql_query($sqlRoute, $db)
            or die("Invalid query: $sqlRoute -- " . mysql_error());
    $activeRoute = mysql_fetch_assoc($resRoute);
    $sqlRouteRegion = "SELECT IRR_R_ID FROM tbl_Individual_Route_Region WHERE IRR_IR_ID = '" . encode_strings($activeRouteID, $db) . "'";
    $resRouteRegion = mysql_query($sqlRouteRegion, $db) or die("Invalid query: $sqlRouteRegion -- " . mysql_error());
    while ($rowRouteRegion = mysql_fetch_assoc($resRouteRegion)) {
        $childRegion[] = $rowRouteRegion['IRR_R_ID'];
    }
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Manage Routes</div>
        <div class="link"><a href="routes.php?rid=<?php echo $regionID ?>">Individual Routes</a></div>
        <div class="link"><a href="routes-categories.php?rid=<?php echo $regionID ?>">Route Categories</a></div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <form id="routeForm" name="routeForm" method="post" enctype="multipart/form-data" action="">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="id" value="<?php echo isset($activeRoute['IR_ID']) ? $activeRoute['IR_ID'] : 0 ?>">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <div class="content-header">
                Manage Individual Route<?php echo isset($activeRoute['IR_Title']) ? ' - ' . $activeRoute['IR_Title'] : '' ?>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Route Title</label>
                <div class="form-data">
                    <input name="route_title" type="text" value="<?php echo isset($activeRoute['IR_Title']) ? $activeRoute['IR_Title'] : '' ?>" size="50" required />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Route Sub-Title</label>
                <div class="form-data">
                    <input name="route_subtitle" type="text" value="<?php echo isset($activeRoute['IR_Sub_Title']) ? $activeRoute['IR_Sub_Title'] : '' ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width" style="<?php echo $community_class ?>"> 
                <label>Select Websites</label>
                <div class="form-data">
                    <div id="childRegion"> 
                        <?PHP
                        $currentRType = 0;
                        $RTypesArray = array(
                            1 => 'Parent Region',
                            2 => 'Community',
                            3 => 'Organization',
                            4 => 'County'
                        );
                        $sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_Type <= 4 AND R_Type > 0 AND R_Status = 1 $where ORDER BY R_Type, R_Name";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_assoc($result)) {
                            if ($currentRType != $row['R_Type']) {
                                $currentRType = $row['R_Type'];
                                echo '<div class="regionTypeTitle">' . $RTypesArray[$currentRType] . '</div>';
                            }
                            echo '<div class="childRegionCheckbox"><input type="checkbox" name="childRegion[]" value="' . $row['R_ID'] . '"';
                            echo (in_array($row['R_ID'], $childRegion) || $checked == '1') ? 'checked' : '';
                            echo '>' . $row['R_Name'] . "</div>";
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Start Point</label>
                <div class="form-data">
                    <textarea name="route_start" cols="85" rows="10" class="tt-description"><?php echo isset($activeRoute['IR_Start_Point']) ? $activeRoute['IR_Start_Point'] : '' ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>End Point</label>
                <div class="form-data">
                    <textarea name="route_end" cols="85" rows="10" class="tt-description"><?php echo isset($activeRoute['IR_End_Point']) ? $activeRoute['IR_End_Point'] : '' ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Distance</label>
                <div class="form-data">
                    <input name="route_distance" type="text" value="<?php echo isset($activeRoute['IR_Distance']) ? $activeRoute['IR_Distance'] : '' ?>" size="50" required />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Surface</label>
                <div class="form-data">
                    <input name="route_surface" type="text" value="<?php echo isset($activeRoute['IR_Surface']) ? $activeRoute['IR_Surface'] : '' ?>" size="50" required />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Route Introduction</label>
                <div class="form-data">
                    <textarea name="route_introduction" cols="85" rows="10" class="tt-description"><?php echo isset($activeRoute['IR_Introduction_Text']) ? $activeRoute['IR_Introduction_Text'] : '' ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Route Description</label>
                <div class="form-data">
                    <textarea name="route_description" cols="85" rows="10" class="tt-description"><?php echo isset($activeRoute['IR_Description']) ? $activeRoute['IR_Description'] : '' ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Primary Photo</label>
                <div class="form-data div_image_library cat-region-width">
                    <?php if (isset($activeRoute['IR_Photo']) && $activeRoute['IR_Photo'] != '') { ?>
                        <a class="deletePhoto delete-region-cat-photo" onClick="return confirm('Are you sure you want to delete photo?');" href="route.php?rid=<?php echo $regionID ?>&id=<?php echo $activeRoute['IR_ID'] ?>&op=del">Delete Photo</a>
                    <?php } ?>
                    <div class="cropit-image-preview aboutus-photo-perview">
                        <img class="preview-img preview-img-script31" style="display: none;" src="">    
                        <?php if (isset($activeRoute['IR_Photo']) && $activeRoute['IR_Photo'] != '') { ?>
                            <img class="existing-img existing_imgs31" src="<?php echo IMG_LOC_REL . $activeRoute['IR_Photo'] ?>" >
                        <?php } ?>
                    </div>
                    <span class="daily_browse" onclick="show_image_library(31)">Select File</span>
                    <input type="hidden" name="image_bank_desktop" id="image_bank31" value="">
                    <input type="file" onchange="show_file_name(31, this)" name="pic[]" id="photo31" style="display: none;">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Slider Title</label>
                <div class="form-data">
                    <input name="slider_title" type="text"  value="<?php echo isset($activeRoute['IR_Slider_Title']) ? $activeRoute['IR_Slider_Title'] : '' ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Slider Description</label>
                <div class="form-data"> 
                    <textarea name="slider_description" cols="85" rows="10" class="tt-description"><?php echo isset($activeRoute['IR_Slider_Description']) ? $activeRoute['IR_Slider_Description'] : '' ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Alt Tag</label>
                <div class="form-data">
                    <input name="route_alt" type="text" value="<?php echo isset($activeRoute['IR_Alt_Tag']) ? $activeRoute['IR_Alt_Tag'] : '' ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Video</label>
                <div class="form-data">
                    <input type="text" name="route_video" value="<?php echo isset($activeRoute['IR_Video']) ? $activeRoute['IR_Video'] : '' ?>">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>SEO Title</label>
                <div class="form-data">
                    <input name="seo_title" type="text" value="<?php echo isset($activeRoute['IR_SEO_Title']) ? $activeRoute['IR_SEO_Title'] : '' ?>" size="50" required />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>SEO Keywords</label>
                <div class="form-data">
                    <input name="seo_keywords" type="text" value="<?php echo isset($activeRoute['IR_SEO_Keywords']) ? $activeRoute['IR_SEO_Keywords'] : '' ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>SEO Description</label>
                <div class="form-data">
                    <textarea name="seo_description" cols="50"><?php echo isset($activeRoute['IR_SEO_Description']) ? $activeRoute['IR_SEO_Description'] : '' ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Map Latitude</label>
                <div class="form-data">
                    <input name="route_lat" type="text" value="<?php echo isset($activeRoute['IR_Lat']) ? $activeRoute['IR_Lat'] : '' ?>" size="50" required />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Map Longitude</label>
                <div class="form-data">
                    <input name="route_long" type="text" value="<?php echo isset($activeRoute['IR_Long']) ? $activeRoute['IR_Long'] : '' ?>" size="50" required />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Map Zoom</label>
                <div class="form-data">
                    <input name="route_zoom" type="text" value="<?php echo isset($activeRoute['IR_Zoom']) ? $activeRoute['IR_Zoom'] : '' ?>" size="50" required />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin" >
                <label>Upload KML file</label>
                <div class="form-data">
                    <label for="route_kml" class="des-pdf">Browse for KML</label>
                    <input id="route_kml" type="file" name="route_kml" value="" style="visibility: hidden; position: absolute; z-index: 0"/>
                </div>
                <?php if (isset($activeRoute['IR_KML_Path']) && $activeRoute['IR_KML_Path'] != '') { ?>
                    <div class="field_links">
                        <a href="<?php echo 'http://' . DOMAIN . MAP_LOC_REL . $activeRoute['IR_KML_Path'] ?>">View</a>
                        <a onClick="return confirm('Are you sure?');" href="route-kml-delete.php?op=del&ir_id=<?php echo $activeRoute['IR_ID'] ?>&rid=<?php echo $regionID ?>">Delete</a>
                    </div>
                <?php } ?>
            </div>
            <div class="form-inside-div form-inside-div-width-admin border-none"> 
                <div class="button">
                    <input type="submit" name="button2" value="Submit" />
                </div>
            </div>
        </form>

        <?php if ($activeRouteID > 0) { ?>
            <div class="form-inside-div form-inside-div-width-admin" id="listing-gallery">
                <div class="content-header">
                    <div class="title"><?php echo $activeRoute['IR_Title'] ?> - Gallery</div>
                    <div class="link">
                        <div class="add-link"><a href="#" onclick="return show_form();">+ Add Photo</a></div>
                    </div>
                </div>
                <form name="gallery_form" method="post" enctype="multipart/form-data" action="route-gallery-submit.php" id="gallery_form" style="display:none;">
                    <input type="hidden" name="ir_id" value="<?php echo $activeRoute['IR_ID'] ?>">
                    <input type="hidden" name="rid" value="<?php echo $regionID ?>">
                    <input type="hidden" name="op" value="save">
                    <input type="hidden" name="image_bank" class="image_bank" id="image_bank33" value="">
                    <div class="form-inside-div form-inside-div-width-admin">
                        <label>Title</label>
                        <div class="form-data">
                            <input name="gallery_title" type="text" size="50" required />
                        </div>
                    </div>

                    <div class="form-inside-div form-inside-div-width-admin">
                        <label>Photo</label>
                        <div class="form-data form-input-text">
                            <div class="div_image_library">
                                <span class="daily_browse" onclick="show_image_library(33)">Select File</span>
                                <input onchange="show_file_name(33, this, 0)" id="photo33" type="file" name="pic[]" style="display: none;"/>
                            </div>
                            <div class="form-inside-div add-about-us-item-image margin-none">
                                <div class="cropit-image-preview aboutus-photo-perview">
                                    <img class="preview-img preview-img-script33" style="display: none;" src="">    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-inside-div">
                        <div class="button">
                            <input type="submit" name="button" id="button" value="Save Photo" />
                        </div>
                    </div>
                </form>
                <ul class="gallery">
                    <?PHP
                    $sqlGallery = "SELECT IRG_ID, IRG_Thumbnail_Photo, IRG_IR_ID, IRG_Title FROM tbl_Individual_Route_Gallery WHERE IRG_IR_ID = " . $activeRoute['IR_ID'] . " ORDER BY IRG_Order";
                    $resGallery = mysql_query($sqlGallery) or die(mysql_error() . ' - ' . $sqlGallery);
                    $counter = 1;
                    while ($gallery = mysql_fetch_assoc($resGallery)) {
                        ?>
                        <li class="image" id="recordsArray_<?php echo $gallery['IRG_ID'] ?>">
                            <div class="listing-image"><img class="max-width-thumbnail" src="http://<?php echo DOMAIN . IMG_LOC_REL . $gallery['IRG_Thumbnail_Photo'] ?>" alt="" /></div>
                            <div class="count"><?php echo $counter ?></div>
                            <div class="cross"><a onClick="return confirm('Are you sure?');" href="route-gallery-photo-delete.php?op=del&id=<?php echo $gallery['IRG_ID'] ?>&ir_id=<?php echo $gallery['IRG_IR_ID'] ?>&rid=<?php echo $regionID ?>">x</a></div>
                            <div class="desc" onclick="show_description(<?php echo $gallery['IRG_ID'] ?>);"><?php echo ($gallery['IRG_Title'] != '') ? $gallery['IRG_Title'] : '+ Add Description'; ?></div> 

                            <form action="route-gallery-submit.php" method="post" name="form" id="gallery-description-form<?php echo $gallery['IRG_ID'] ?>" style="display:none;">
                                <div style="float:left; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                    <input type="hidden" name="op" value="save">
                                    <input type="hidden" name="ir_id" value="<?php echo $gallery['IRG_IR_ID'] ?>">
                                    <input type="hidden" name="irg_id" value="<?php echo $gallery['IRG_ID'] ?>">
                                    <input type="hidden" name="rid" value="<?php echo $regionID ?>">
                                    <input name="gallery_title" type="text" value="<?php echo $gallery['IRG_Title'] ?>" size="50" required />
                                </div>
                                <div style="float:left;text-align: center; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                    <div class="form-data">
                                        <input type="submit" name="button" value="Save Now"/>
                                    </div>
                                </div> 
                            </form>
                        </li>
                        <?PHP
                        $counter++;
                    }
                    ?>
                </ul>
            </div>

            <div class="form-inside-div form-inside-div-width-admin">
                <div class="content-header">
                    <div class="title"><?php echo $activeRoute['IR_Title'] ?> - Downloads</div>
                </div>
                <form method="post" name="pdfform" enctype="multipart/form-data" action="route-pdf-submit.php">
                    <input type="hidden" name="op" value="save">
                    <input type="hidden" name="ir_id" value="<?php echo $activeRoute['IR_ID'] ?>">
                    <input type="hidden" name="rid" value="<?php echo $regionID ?>">
                    <div class="form-inside-div form-inside-div-width-admin border-none" >
                        <label for="description_pdf" class="des-pdf">Browse for PDF</label>
                        <input class="file_ext0" id="description_pdf" type="file" name="file" accept="application/pdf" value="" style="visibility: hidden; position: absolute; z-index: 0"/>
                        <div class="des-input">    
                            <input type="text" name="pdf_title" class="required_title" placeholder="Enter Title" value="" required/>
                        </div>
                        <input type="submit" class="buttonpdf" name="buttonpdf" value="Save Now" />
                    </div>
                </form>
                <div class="form-inside-div form-inside-div-width-admin border-none padding-top-des">
                    <div class="content-sub-header">
                        <div class="title">Manage Downloads</div>
                    </div>
                </div>
                <div class="brief">
                    <?php
                    if ($activeRouteID > 0) {
                        $sqlPDF = "SELECT IRD_ID, IRD_Path, IRD_Title, IRD_IR_ID FROM tbl_Individual_Route_Downloads WHERE IRD_IR_ID = " . $activeRoute['IR_ID'] . " ORDER BY IRD_Order ASC";
                        $resultPDF = mysql_query($sqlPDF, $db)
                                or die("Invalid query: $sqlPDF -- " . mysql_error());
                        while ($PDF = mysql_fetch_assoc($resultPDF)) {
                            ?>
                            <div class="form-inside-div border-none padding-top-none" id="recordsArray_<?php echo $PDF['IRD_ID'] ?>">
                                <div class="data-pdf-title">
                                    <a href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $PDF['IRD_Path'] ?>"><?php echo $PDF['IRD_Title']; ?></a> 
                                </div>
                                <div class="data-pdf-edit">
                                    &nbsp;
                                </div>
                                <div class="data-des-option">
                                    <a class="delete-pointer" onClick="return confirm('Are you sure?');" href="route-pdf-delete.php?op=del&id=<?php echo $PDF['IRD_ID'] ?>&ir_id=<?php echo $PDF['IRD_IR_ID'] ?>&rid=<?php echo $regionID ?>">Delete</a>
                                </div>

                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        <?php } ?>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>
<div id="dialog-message"></div>
<script>
    $(function () {
        $("#listing-gallery ul.gallery").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Individual_Route_Gallery&field=IRG_Order&id=IRG_ID';
                $.post("reorder.php", order, function (theResponse) {
                    swal("Photos Re-Ordered!", "Gallery Photos Re-Ordered successfully", "success");
                });
            }
        });
        $(".brief").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Individual_Route_Downloads&field=IRD_Order&id=IRD_ID';
                $.post("reorder.php", order, function (theResponse) {
                    swal("Downloads Re-Ordered!", "Downloads Re-Ordered successfully", "success");
                });
            }
        });
    });
    //to show gallery form
    function show_form() {
        $('#gallery_form').show();
        return false;
    }
</script>
<?PHP
require_once '../include/admin/footer.php';
?>