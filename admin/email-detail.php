<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
if ($_REQUEST['id'] > 0) {
    $sql = "SELECT * FROM tbl_Email where E_ID= '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
}
if ($_REQUEST['del'] > 0) {
    $getFile = mysql_query("SELECT * FROM tbl_Email WHERE E_ID = '" . encode_strings($_REQUEST['del'], $db) . "'");
    $fileDel = mysql_fetch_assoc($getFile);
    $delRecord = mysql_query("DELETE FROM tbl_Email where E_ID= '" . encode_strings($_REQUEST['del'], $db) . "'");
    mysql_query("DELETE FROM tbl_Email_Detail where E_Email_ID= '" . encode_strings($_REQUEST['del'], $db) . "'");
    if ($delRecord) {
        require_once '../include/picUpload.inc.php';
        Delete_Pic('emailattachments/' . $fileDel['E_Attachment']);
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY 
        $id = $_REQUEST['del'];
        Track_Data_Entry('Email', '', 'Archive Emails', $id, 'Delete', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: /admin/archive-email.php");
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width ">
    <div class="title-link">
        <div class="title">
            Archive Emails
        </div>
    </div>
    <div class="left"><?php require_once('../include/nav-home-sub.php'); ?></div>
    <div class="right">
        <div class="content-sub-header">
            <div class="link email-detail"><a href="email-detail.php?del=<?php echo $row['E_ID'] ?>" >Delete</a></div> 
            <?php echo $row['E_Title']; ?>
        </div>
        <div class="data-content">
            <div class="data-column spl-email-subject">
                <?php echo 'Date & Time: ' . $row['E_Date'] ?>
            </div>
            <div class="data-column spl-email-date">

            </div>
        </div>
        <div class="email-detail-body">
            <?php echo $row['E_Body'] ?>
        </div>
        <div class="content-sub-header">
            File Attachments   
        </div>
        <div class="data-content">
            <div class="data-column spl-email-subject">
                <a href="emailattachments/<?php echo $row['E_Attachment'] ?>"><?php echo $row['E_Attachment'] ?></a>
            </div>
        </div>
    </div>
</div>

<?PHP
require_once '../include/admin/footer.php';
?>
