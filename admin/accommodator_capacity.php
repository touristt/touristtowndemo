<?php
include '../include/config.inc.php';
include '../include/accommodation_functions.php';
/* Save all Logic */
$accommodation_ids = array();
$cat_id = 29; //category_id
if (isset($_REQUEST['cat_id'])) {
    $cat_id = $_REQUEST['cat_id'];
}
$filters = '';
$beds = '';
$search = '';
if (isset($_GET['filter'])) {
    if (isset($_GET['BL_Sports_Team_Welcome'])) {
        $filters['BL_Sports_Team_Welcome'] = 1;
    }
    if (isset($_GET['BL_Pets_Allowed'])) {
        $filters['BL_Pets_Allowed'] = 1;
    }
    if (isset($_GET['BL_Flexible_Rental'])) {
        $filters['BL_Flexible_Rental'] = 1;
    }
    if (isset($_GET['closed'])) {
        $filters['closed'] = 0;
    }
    if (isset($_GET['BL_Weekly_Rental'])) {
        $filters['BL_Weekly_Rental'] = 1;
    }
    if (isset($_GET['bed_1'])) {
        $beds['bed_1'] = 0;
    }
    if (isset($_GET['bed_2'])) {
        $beds['bed_2'] = 0;
    }
    if (isset($_GET['bed_3'])) {
        $beds['bed_3'] = 0;
    }
    if (isset($_GET['bed_4'])) {
        $beds['bed_4'] = 0;
    }
    if (isset($_GET['bed_5'])) {
        $beds['bed_5'] = 0;
    }
    if (isset($_GET['bed_6'])) {
        $beds['bed_6'] = 0;
    }
    if (isset($_GET['bed_7'])) {
        $beds['bed_7'] = 0;
    }
    if (isset($_GET['bed_8'])) {
        $beds['bed_8'] = 0;
    }
}
if (isset($_GET['search_word'])) {
    $search = $_GET['search_word'];
}
$reg_id = 1; //region id by default
if (isset($_REQUEST['reg_id'])) {
    $reg_id = $_REQUEST['reg_id'];
}
// if its the bgadmin, regionadmin or town admin logged in
$regionlimit = false; // if super admin, false, if bg or region or town admin, then true
if ($_SESSION['USER_LIMIT'] > 0) {
    $reg_id = $_SESSION['USER_LIMIT'];
    $regionlimit = true;
}
$page = isset($_GET['page']) ? $_GET['page'] : 0;
require_once '../include/login.inc.php';
require_once '../include/admin/header.php';
include '../include/accommodation_header.php';
?>

<?php if (isset($_SESSION['message'])) { ?>
    <div class="message">
        <?php
        echo $_SESSION['message'];
        unset($_SESSION['message']);
        ?>
    </div>
<?php } ?>
<div class="content-left full-width">
    <form action='accommodator_capacity.php' method='get'>
        <div class="title-link">
            <div class="title">Accommodator Capacity</div>
            <div class="addlisting">
                <input type="text" class="accommodation-cap-search" name="search_word" value="" placeholder="Search"/>
                <input type="submit" class="top-search-btn" name="submit-search" value="GO"/>
            </div>
        </div>
        <div class="right full-width">
            <div class="filter-row">
                <div class="filter-row1">
                    <select name="cat_id" class="filter-acco-list">
                        <?php
                        $accommodation_type = mysql_query('SELECT C_ID, C_Name FROM `tbl_Category` tbc Where tbc.C_ID IN (29,30,31,59,45) ORDER BY tbc.C_ID ASC');
                        $active_item = '';
                        while ($accomm = mysql_fetch_array($accommodation_type)) {
                            $active = '';
                            if ($cat_id == $accomm['C_ID']) {
                                $active = 'selected';
                                $active_item = $accomm['C_Name'];
                            }
                            print "<option $active value='" . $accomm['C_ID'] . "'>" . $accomm['C_Name'] . "</option>";
                        }
                        ?>
                    </select>
                    <?php
                    // only show region filter to super admin
                    if (!$regionlimit) {
                        ?>
                        <select name="reg_id" class="filter-hotels-list">
                            <?php
                            $accommodation_reg = mysql_query('SELECT R_ID, R_Name FROM `tbl_Region`');
                            while ($region = mysql_fetch_array($accommodation_reg)) {
                                $active = '';
                                if ($reg_id == $region['R_ID']) {
                                    $active = 'selected';
                                }
                                print_r("<option $active value='" . $region['R_ID'] . "'>" . $region['R_Name'] . "</option>");
                            }
                            ?>
                        </select>
                        <?php
                    }
                    ?>
                    <input type="submit" class="filter-btn" name="filter" value="Filter Now"/>
                </div>
                <div class="filter-row2">
                    <?php
                    if ($cat_id == 59 || $cat_id == 30) {
                        ?>
                        <div class="filter-beds">
                            # Bedrooms
                            <input type="checkbox" name="bed_1" <?php echo isset($beds['bed_1']) ? 'checked' : '' ?> class="filter-checkbox"/><lable for="bed_1" class="lable">1</lable>
                            <input type="checkbox" name="bed_2" <?php echo isset($beds['bed_2']) ? 'checked' : '' ?> class="filter-checkbox"/><lable for="bed_2" class="lable">2</lable>
                            <input type="checkbox" name="bed_3" <?php echo isset($beds['bed_3']) ? 'checked' : '' ?> class="filter-checkbox"/><lable for="bed_3" class="lable">3</lable>
                            <input type="checkbox" name="bed_4" <?php echo isset($beds['bed_4']) ? 'checked' : '' ?> class="filter-checkbox"/><lable for="bed_4" class="lable">4</lable>
                            <input type="checkbox" name="bed_5" <?php echo isset($beds['bed_5']) ? 'checked' : '' ?> class="filter-checkbox"/><lable for="bed_5" class="lable">5</lable>
                            <input type="checkbox" name="bed_6" <?php echo isset($beds['bed_6']) ? 'checked' : '' ?> class="filter-checkbox"/><lable for="bed_6" class="lable">6</lable>
                            <input type="checkbox" name="bed_7" <?php echo isset($beds['bed_7']) ? 'checked' : '' ?> class="filter-checkbox"/><lable for="bed_7" class="lable">7</lable>
                            <input type="checkbox" name="bed_8" <?php echo isset($beds['bed_8']) ? 'checked' : '' ?> class="filter-checkbox"/><lable for="bed_8" class="lable">8</lable>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="filter-row2">
                    <div class="filter-sports-team">
                        <input type="checkbox" name="BL_Sports_Team_Welcome" <?php echo isset($filters['BL_Sports_Team_Welcome']) ? 'checked' : '' ?>/><lable for="BL_Sports_Team_Welcome">No Sports Teams</lable>
                    </div>
                    <div class="filter-pets">
                        <input type="checkbox" name="BL_Pets_Allowed" <?php echo isset($filters['BL_Pets_Allowed']) ? 'checked' : '' ?>/><lable for="BL_Pets_Allowed">Pets Allowed</lable>
                    </div>
                    <div class="filter-rental">
                        <input type="checkbox" name="BL_Flexible_Rental" <?php echo isset($filters['BL_Flexible_Rental']) ? 'checked' : '' ?>/><lable for="BL_Flexible_Rental">Long Term Rental</lable>
                    </div>
                    <div class="filter-availabe">
                        <input type="checkbox" name="closed" <?php echo isset($filters['closed']) ? 'checked' : '' ?>/><lable for="closed">Available</lable>
                    </div>
                    <?php
                    if ($cat_id == 59 || $cat_id == 30) {
                        ?>
                        <div class="filter-chamber">
                            <input type="checkbox" name="BL_Weekly_Rental" <?php echo isset($filters['BL_Weekly_Rental']) ? 'checked' : '' ?>/><lable for="BL_Weekly_Rental">Weekly Rental Only</lable>
                        </div>
                    <?php } ?>
                </div>
            </div>
    </form>
    <!--data container-->
    <?php
    $page_counter = 0;
    if ($cat_id == 29 || $cat_id == 31 || $cat_id == 45) {
        ?>
        <div class="data-container cottage-capacity">
            <div class="data-header-rows acc-padding-top-bottom content-header ccommodation-cap-heading-width header-row2-color">
                <div class="header-row2-col1"><?php echo $active_item; ?></div>
                <div class="header-row2-col3 acc-top-button"><input type="button" onclick="return accomm_cap_update_all();" class="Save-all-btn" name="save_all" value="Save All"></div>
            </div>
            <div class="acc_cap_header-rows  header-row2-color">
                <div class="header-row2-col1">&nbsp;</div>
                <div class="header-row2-col2">Closed</div>
                <div class="header-row2-col3">Capacity</div>
                <div class="header-row2-col4">&nbsp;</div>
            </div>
            <?php
            //loop here to fitch data
            $return_data = get_listings($cat_id, $reg_id, 0, $filters, $search);
            if ($return_data['listings'] != '') {
                foreach ($return_data['listings'] as $row) {
                    $page_counter++;
                    $accom_id = $row['BL_ID'];
                    $accom_name = $row['BL_Listing_Title'];
                    $cap_id = isset($row['id']) ? $row['id'] : 0;
                    $accom_closed = isset($row['closed']) && $row['closed'] != 0 ? 'checked' : '';
                    $accom_general = isset($row['general']) && $row['general'] != NULL ? $row['general'] : 0;
                    /* Save all Logic */
                    $accommodation_ids[] = $accom_id;
                    ?>
                    <div class = "acc_cap_header-rows data-rows accordion-section">
                        <div class="data-header-rows accommodation-cap-other-width accordion-header">
                            <form id="ajax_group_accom_cap_<?php echo $accom_id; ?>" name="ajax_group_accom_cap_<?php echo $accom_id ?>" onsubmit="return accomm_cap_update(this)">
                                <input type = "hidden" name = "accom_id" value="<?php echo $accom_id ?>" />

                                <div class = "header-row3-type-name padding-capacity acc-capacity-name">
                                    <?php echo (strlen($accom_name) > 30) ? substr($accom_name, 0, 28) . '...' : $accom_name; ?></div>
                                <div class = "header-row2-col2 acc_cap_padding_capacity"><input <?php echo $accom_closed; ?> type = "checkbox" name = "closed" class = "closed-checkbox"/></div>
                                <div class = "header-row2-col3"><input type = "text" name = "capacity" required value='<?php echo $accom_general; ?>' class = "capacity-txt"/></div>
                                <div class = "header-row2-col4"><input type = "submit" name = "cap-btn" class = "cap-btn" value = "Save"/></div>
                            </form>
                        </div>
                        <div  class="data-header-rows accommodation-cap-other-width accordion-content">
                            <div class="notes-info-col-full">
                                <div class="contact1"><?php echo $accom_name; ?></div>
                            </div>
                            <div class="notes-info-col1">
                                <div class="notess-info-contacts">
                                    <div class="contact1"><?php echo $row['BL_Contact']; ?></div>
                                    <div class="contact1"><a href="tel:<?php echo $row['BL_Phone'] ?>"><?php echo $row['BL_Phone']; ?></a></div>
                                    <div class="contact2"><a href="tel:<?php echo $row['BL_Cell'] ?>"><?php echo $row['BL_Cell']; ?></a></div>
                                </div>
                                <div class="notes-address"><?php echo $row['BL_Street']; ?></div>
                                <div class="notes-city"><?php echo $row['BL_Town']; ?></div>
                                <div class="notes-email"><a href="mailto:<?php echo $row['BL_Email'] ?>"><?php echo $row['BL_Email'] ?></a></div>
                                <div class="notes-email"><a href="<?php echo $row['BL_Website'] ?>"><?php echo $row['BL_Website']; ?></a></div>
                            </div>
                            <form onsubmit="return block_capacity_update_notes(this)">
                                <?php
                                $sql_listings = mysql_query("Select BL_Sports_Team_Welcome, BL_Pets_Allowed, BL_Flexible_Rental, BL_Notes FROM
                                                            `tbl_Business_Listing` WHERE BL_ID=$accom_id");
                                $result_listings = mysql_fetch_assoc($sql_listings);
                                $accom_sports_team_welcome = $result_listings['BL_Sports_Team_Welcome'];
                                $accom_pets_allowed = $result_listings['BL_Pets_Allowed'];
                                $accom_flexible_rental = $result_listings['BL_Flexible_Rental'];
                                $accom_notes = $result_listings['BL_Notes'];
                                ?>
                                <div class="notes-info-col2">
                                    <input type="hidden" name="accom_id" value="<?php echo $accom_id ?>">
                                    <div class="notess-info-features">
                                        <input type="checkbox" <?php echo $accom_sports_team_welcome == 1 ? 'checked' : '' ?> name="sports_team"/><lable for="sports_team">No Sports Teams</lable>
                                    </div>
                                    <div class="notess-info-features">
                                        <input type="checkbox" <?php echo $accom_pets_allowed == 1 ? 'checked' : '' ?> name="pets"/><lable for="pets">Pets Allowed</lable>
                                    </div>
                                    <div class="notess-info-features">
                                        <input type="checkbox" <?php echo $accom_flexible_rental == 1 ? 'checked' : '' ?> name="flexible_rental"/><lable for="flexible_rental">Long Term Rental</lable>
                                    </div>
                                </div>
                                <div class="notes-info-col3">
                                    <div class="notess-info-textarea">
                                        <textarea class='notes_hotel tt-ckeditor' name="notes"><?php echo $accom_notes ?></textarea>
                                    </div>
                                    <div class="notess-info-save-note" style="margin-top: 10px;">
                                        <input type="submit" name="save_note" class="save-note-btn" value="Save Note"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class = "acc_cap_header-rows data-rows">
                    <?php //echo $return_data['pagination'];   ?>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
    } else if ($cat_id == 59 || $cat_id == 30) {
        ?>
        <div class="data-container cottage-capacity">
            <div class="acc_cap_header-rows data-header-rows header-row1-color content-header header-row2-color ccommodation-cap-heading-width acc-padding-top-bottom">
                <div class="header-row1-col1"><?php echo $active_item; ?></div>
                <div class="header-row1-col2 acc-top-button"><input type="button" value="Save All" name="save_all" class="Save-all-btn" onclick="return accomm_cap_update_all();"></div>
            </div>
            <div class="acc_cap_header-rows  header-row3-color">
                <div class="header-row3-col1">&nbsp;</div>
                <div class="header-row3-col2"># of bedrooms</div>
            </div>
            <div class="acc_cap_header-rows  header-row2-color">
                <div class="header-row2-col1">&nbsp;</div>
                <div class="header-row2-col2">Closed</div>
                <div class="header-row2-col3">1</div>
                <div class="header-row2-col3">2</div>
                <div class="header-row2-col3">3</div>
                <div class="header-row2-col3">4</div>
                <div class="header-row2-col3">5</div>
                <div class="header-row2-col3">6</div>
                <div class="header-row2-col3">7</div>
                <div class="header-row2-col3">8</div>
                <div class="header-row2-col4">&nbsp;</div>
            </div>
            <?php
            //loop here to fitch data
            $return_data = get_listings($cat_id, $reg_id, 0, $filters, $search);
            if ($return_data['listings'] != '') {
                foreach ($return_data['listings'] as $row) {
                    $page_counter++;
                    $accom_id = $row['BL_ID'];
                    $accom_name = $row['BL_Listing_Title'];
                    $cap_id = isset($row['id']) ? $row['id'] : 0;
                    $accom_closed = isset($row['closed']) && $row['closed'] != 0 ? 'checked' : '';
                    $accom_1x1 = isset($row['1x1']) && $row['1x1'] != NULL ? $row['1x1'] : 0;
                    $accom_2x2 = isset($row['2x2']) && $row['2x2'] != NULL ? $row['2x2'] : 0;
                    $accom_3x3 = isset($row['3x3']) && $row['3x3'] != NULL ? $row['3x3'] : 0;
                    $accom_4x4 = isset($row['4x4']) && $row['4x4'] != NULL ? $row['4x4'] : 0;
                    $accom_5x5 = isset($row['5x5']) && $row['5x5'] != NULL ? $row['5x5'] : 0;
                    $accom_6x6 = isset($row['6x6']) && $row['6x6'] != NULL ? $row['6x6'] : 0;
                    $accom_7x7 = isset($row['7x7']) && $row['7x7'] != NULL ? $row['7x7'] : 0;
                    $accom_8x8 = isset($row['8x8']) && $row['8x8'] != NULL ? $row['8x8'] : 0;
                    /* Save all Logic */
                    $accommodation_ids[] = $accom_id;
                    ?>
                    <div class = "acc_cap_header-rows data-rows accordion-section">
                        <div class="data-header-rows accommodation-cap-other-width accordion-header">
                            <form id="ajax_group_accom_cap_<?php echo $accom_id; ?>" name="ajax_group_accom_cap_<?php echo $accom_id ?>" onsubmit="return accomm_cap_update(this)">
                                <input type = "hidden" name = "accom_id" value="<?php echo $accom_id ?>" /> 

                                <div class = "header-row3-type-name padding-capacity acc-capacity-name"><?php echo (strlen($accom_name) > 30) ? substr($accom_name, 0, 28) . '...' : $accom_name; ?></div>
                                <div class = "header-row2-col2"><input <?php echo $accom_closed; ?> type = "checkbox" name = "closed" class = "closed-checkbox"/></div>
                                <div class = "header-row2-col3"><input type = "text" name = "accom_1x1" required value='<?php echo $accom_1x1; ?>' class = "capacity-txt"/></div>
                                <div class = "header-row2-col3"><input type = "text" name = "accom_2x2" required value='<?php echo $accom_2x2; ?>' class = "capacity-txt"/></div>
                                <div class = "header-row2-col3"><input type = "text" name = "accom_3x3" required value='<?php echo $accom_3x3; ?>' class = "capacity-txt"/></div>
                                <div class = "header-row2-col3"><input type = "text" name = "accom_4x4" required value='<?php echo $accom_4x4; ?>' class = "capacity-txt"/></div>
                                <div class = "header-row2-col3"><input type = "text" name = "accom_5x5" required value='<?php echo $accom_5x5; ?>' class = "capacity-txt"/></div>
                                <div class = "header-row2-col3"><input type = "text" name = "accom_6x6" required value='<?php echo $accom_6x6; ?>' class = "capacity-txt"/></div>
                                <div class = "header-row2-col3"><input type = "text" name = "accom_7x7" required value='<?php echo $accom_7x7; ?>' class = "capacity-txt"/></div>
                                <div class = "header-row2-col3"><input type = "text" name = "accom_8x8" required value='<?php echo $accom_8x8; ?>' class = "capacity-txt"/></div>
                                <div class = "header-row2-col4"><input type = "submit" name = "cap-btn" class = "cap-btn" value = "Save"/></div>

                            </form>
                        </div>
                        <div  class="data-header-rows accommodation-cap-other-width accordion-content">
                            <div class="notes-info-col-full">
                                <div class="contact1"><?php echo $accom_name; ?></div>
                            </div>
                            <div class="notes-info-col1">
                                <div class="notess-info-contacts">
                                    <div class="contact1"><?php echo $row['BL_Contact']; ?></div>
                                    <div class="contact1"><a href="tel:<?php echo $row['BL_Phone'] ?>"><?php echo $row['BL_Phone']; ?></a></div>
                                    <div class="contact2"><a href="tel:<?php echo $row['BL_Cell'] ?>"><?php echo $row['BL_Cell']; ?></a></div>
                                </div>
                                <div class="notes-address"><?php echo $row['BL_Street']; ?></div>
                                <div class="notes-city"><?php echo $row['BL_Town']; ?></div>
                                <div class="notes-email"><a href="mailto:<?php echo $row['BL_Email'] ?>"><?php echo $row['BL_Email'] ?></a></div>
                                <div class="notes-email"><a href="<?php echo $row['BL_Website'] ?>"><?php echo $row['BL_Website']; ?></a></div>
                            </div>
                            <form onsubmit="return block_capacity_update_notes(this)">
                                <?php
                                $sql_listings = mysql_query("Select BL_Sports_Team_Welcome, BL_Pets_Allowed, BL_Flexible_Rental, BL_Notes FROM 
                                                            `tbl_Business_Listing` WHERE BL_ID=$accom_id");
                                $result_listings = mysql_fetch_assoc($sql_listings);
                                $accom_sports_team_welcome = $result_listings['BL_Sports_Team_Welcome'];
                                $accom_pets_allowed = $result_listings['BL_Pets_Allowed'];
                                $accom_flexible_rental = $result_listings['BL_Flexible_Rental'];
                                $accom_notes = $result_listings['BL_Notes'];
                                $accom_weekly_rental = $row['BL_Weekly_Rental'];
                                ?>
                                <div class="notes-info-col2">
                                    <input type="hidden" name="accom_id" value="<?php echo $accom_id ?>">
                                    <div class="notess-info-features">
                                        <input type="checkbox" <?php echo $accom_sports_team_welcome == 1 ? 'checked' : '' ?> name="sports_team"/><lable for="sports_team">No Sports Teams</lable>
                                    </div>
                                    <div class="notess-info-features">
                                        <input type="checkbox" <?php echo $accom_pets_allowed == 1 ? 'checked' : '' ?> name="pets"/><lable for="pets">Pets Allowed</lable>
                                    </div>
                                    <div class="notess-info-features">
                                        <input type="checkbox" <?php echo $accom_flexible_rental == 1 ? 'checked' : '' ?> name="flexible_rental"/><lable for="flexible_rental">Long Term Rental</lable>
                                    </div>
                                    <div class="notess-info-features">
                                        <input type="checkbox" <?php echo $accom_weekly_rental == 1 ? 'checked' : '' ?> name="Weekly_Rental_Only"/><lable for="Weekly_Rental_Only">Weekly Rental Only</lable>
                                    </div>
                                </div>
                                <div class="notes-info-col3">
                                    <div class="notess-info-textarea">
                                        <textarea class='notes_hotel tt-ckeditor' name="notes"><?php echo $accom_notes ?></textarea>
                                    </div>
                                    <div class="notess-info-save-note" style="margin-top: 10px;">
                                        <input type="submit" name="save_note" class="save-note-btn" value="Save Note"/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <?php
    }
    ?>
    <ul id="pagin">
        <?php
        if ($page_counter > 20) {
            echo '<li><span class="current" >1</span></li>';
            for ($index = 1; $index < ceil($page_counter / 20); $index++) {
                $page = $index + 1;
                echo "<li><span href='#'>" . $page . "</span></li>";
            }
        }
        ?>
    </ul>
</div>
</div>
<?php
/* Save all Logic */
if (!empty($accommodation_ids)) {
    ?>
    <div id="acc_ids" style="display: none;"><?php
    foreach ($accommodation_ids as $key => $value) {
        echo $value . ",";
    };
    ?></div>
    <?php
}
?>
<?PHP
require_once '../include/admin/footer.php';
?>