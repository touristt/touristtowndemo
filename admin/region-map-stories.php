<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('manage-stories', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if (isset($_REQUEST['rid']) && $_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} else {
    header("Location: /admin/regions.php");
    exit();
}
$feature_category = $_REQUEST['feature_category'];
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'save') {
    $feature_category = $_REQUEST['feature_category'];
    $feature_story = $_REQUEST['feature_story'];
    if ($feature_story != '') {
        $sqlMax = "SELECT MAX(SHC_Order) FROM tbl_Story_Homepage_Category WHERE SHC_R_ID = '" . $regionID . "' AND SHC_Map = 1";
        $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
        $rowMax = mysql_fetch_row($resultMax);
        $sql = " INSERT tbl_Story_Homepage_Category SET SHC_S_ID = '" . $feature_story . "', SHC_R_ID = '" . $regionID . "', SHC_Map = 1, SHC_Order = '" . ($rowMax[0] + 1) . "'";
        $result = mysql_query($sql) or die(mysql_error());
        $_SESSION['story_success'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $regionID, 'Map Stories', $feature_story, 'Add Map Story', 'super admin');
        header("Location: /admin/region-map-stories.php?rid=" . $regionID);
        exit();
    }
}

if (isset($_REQUEST['del']) && $_REQUEST['del'] == 'true') {
    $sql = " DELETE FROM tbl_Story_Homepage_Category WHERE SHC_ID = '" . $_REQUEST['shc_id'] . "'";
    $result = mysql_query($sql) or die(mysql_error());
    $_SESSION['story_delete'] = 1;
    // TRACK DATA ENTRY
    $story = $_REQUEST['shc_id'];
    Track_Data_Entry('Websites', $regionID, 'Map Stories', '', 'Delete Map Story', 'super admin');
    header("Location: /admin/region-map-stories.php?rid=" . $regionID);
    exit();
}

//Get Active Region Information
$sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Map Stories</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <!--Featured Stories Section-->
        <div class="content-header">
            Map Stories
        </div>
        <form name="form1" method="GET" id="feature_form">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <input type="hidden" name="type" value="map_story">
            <input type="hidden" name="op" value="save">
            <div class="content-sub-header link-header region-header-padding">
                <div class="data-column spl-org-listngs padding-org-listing">
                    <select name="category" id="feature_category" class="stories-cat" onchange="filterStories('feature_form');" required>
                        <option value="">Select Category</option>
                        <?PHP
                        $sql = "SELECT C_ID FROM tbl_Region_Category 
                                LEFT JOIN tbl_Category ON C_ID = RC_C_ID
                                WHERE C_Parent = 0 AND C_Is_Blog = 1 AND RC_R_ID = '" . encode_strings($regionID, $db) . "'";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_assoc($result)) {
                            $sql1 = "SELECT C_ID, C_Name, RC_Name FROM tbl_Region_Category
                                    LEFT JOIN tbl_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($regionID, $db) . "'
                                    WHERE C_Parent = '" . $row['C_ID'] . "' AND RC_Status=0 ORDER BY RC_Name ASC";
                            $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
                            while ($row1 = mysql_fetch_assoc($result1)) {
                                ?>
                                <option value="<?php echo $row1['C_ID'] ?>" <?php echo ($row1['C_ID'] == $feature_category) ? 'selected' : '' ?>><?php echo $row1['C_Name'] ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                    <select name="story" id="map_story" class="stories" required>
                        <option value="">Select Story</option>
                        <?PHP
                        if ($feature_category > 0) {
                            $sql = "SELECT S_ID, S_Title FROM tbl_Story WHERE S_Category = '" . encode_strings($feature_category, $db) . "' AND S_Active = 1 
                                    AND S_ID NOT IN (SELECT SHC_S_ID FROM tbl_Story_Homepage_Category 
                                    WHERE SHC_R_ID = '" . encode_strings($regionID, $db) . "' AND SHC_Map = 1) ORDER BY S_Title";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($row = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo $row['S_ID'] ?>"><?php echo $row['S_Title'] ?></option>
                                <?PHP
                            }
                        }
                        ?>
                    </select>
                    <input type="submit" value="+Add" class="stories-add-button" onclick="return saveFeatureStory('feature_form');" />
                </div>
            </div>
        </form>

        <div class="reorder-category map_story">
            <?php
            $sql = "SELECT SHC_ID, S_Title FROM  tbl_Story 
                    LEFT JOIN tbl_Story_Homepage_Category ON S_ID = SHC_S_ID  
                    WHERE SHC_R_ID = '" . encode_strings($regionID, $db) . "' AND S_Active = 1  AND SHC_Map = 1 ORDER BY SHC_Order ASC";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                ?>
                <div class="data-content" id="recordsArray_<?php echo $row['SHC_ID'] ?>">
                    <div class="data-column spl-org-listngs ">
                        <div class="story-title"><?php echo $row['S_Title'] ?></div>
                        <div class="remove-link"><a href="region-map-stories.php?rid=<?php echo $regionID; ?>&shc_id=<?php echo $row['SHC_ID']; ?>&del=true" onclick="return confirm('Are you sure you want to perform this action?');">Remove</a></div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<div id="dialog-message"></div>
<script>
    $(function () {
        $(".reorder-category").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Story_Homepage_Category&field=SHC_Order&id=SHC_ID';
                $.post("reorder.php", order, function (theResponse) {
                    swal("Stories Re-ordered!", "Stories Re-ordered successfully.", "success");
                });
            }
        });
    });
    //filter listing
    function filterStories(form) {
        var formdata = $('#' + form).serialize();
        $.ajax({
            type: "POST",
            url: 'filterStories.php',
            dataType: 'json',
            data: formdata,
            success: function (response) {
                var type = response.type;
                if (response.stories && response.stories !== '') {
                    $('#' + type).html(response.stories);
                }
            }
        });
    }
    //save listing against route
    function saveFeatureStory(form) {
        var formdata = $('#' + form).serialize();
        $.ajax({
            type: "POST",
            url: 'saveFeatureStory.php',
            dataType: 'json',
            data: formdata,
            success: function (response) {
                filterStories(form);
                if (response.success && response.success == 1) {
                    swal("Story Added", "Story has been added successfully.", "success");
                }
                if (response.error && response.error == 1) {
                    swal("Error", "Error adding stories. Please try again.", "error");
                }
                if (response.stories && response.stories !== '') {
                    $('.reorder-category.' + response.type).html(response.stories);
                }

                //clear selection of dropdowns
                $('#map_story').val('');
            }
        });
        return false;
    }
</script>
<?PHP
require_once '../include/admin/footer.php';
?>