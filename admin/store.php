<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

if ($_REQUEST['id'] > 0) {
    $sql = "SELECT F_ID, F_Name, F_Image, F_Description, F_Price, F_Example, F_Instruction_Video, F_File, F_Points FROM tbl_Feature 
            WHERE F_ID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $sql = "tbl_Feature SET 
            F_Name = '" . encode_strings($_REQUEST['name'], $db) . "', 
            F_Price = '" . encode_strings($_REQUEST['price'], $db) . "',
            F_Description = '" . encode_strings($_REQUEST['desc'], $db) . "', 
            F_Instruction_Video = '" . encode_strings($_REQUEST['video'], $db) . "', 
            F_Example = '" . encode_strings($_REQUEST['example'], $db) . "', 
            F_File = '" . encode_strings($_REQUEST['file'], $db) . "',
            F_Points = '" . encode_strings($_REQUEST['points'], $db) . "'";
    require '../include/picUpload.inc.php';
    if ($_POST['image_bank_pic'] == "") {
        // last @param 17 = Store Image
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 17);
        if (is_array($pic)) {
            $sql .= ", F_Image = '" . encode_strings($pic['0']['0'], $db) . "'";
        }
        $pic_id = $pic['1'];
    } else {
        $pic_id = $_POST['image_bank_pic'];
        // last @param 17 = Store Image
        $pic_response = Upload_Pic_Library($pic_id, 17);
        if ($pic_response) {
            $sql .= ", F_Image = '" . encode_strings($pic_response['0'], $db) . "'";
        }
    }
    if ($_REQUEST['id'] > 0) {
        $sql = "UPDATE " . $sql . " WHERE F_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
        $result = mysql_query($sql, $db);
        $id = $_REQUEST['id'];
        // TRACK DATA ENTRY
        Track_Data_Entry('Store','','Add On Details',$id,'Update','super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Store','','Add On Details',$id,'Add','super admin');
    }

    if ($result) {
        $_SESSION['success'] = 1;
        if ($pic_id > 0) {
            //Image usage from image bank.
            imageBankUsage($pic_id, 'IBU_F_ID', $id, '', '');
        }
    } else {
        $_SESSION['error'] = 1;
    }

    //update listing for all records
    $new_listing = 0;
    $sql = "SELECT BL_Billing_Type, BL_ID, BL_CoC_Dis_2, BL_Line_Item_Title, BL_Line_Item_Price FROM tbl_Business_Listing
            WHERE BL_Listing_Type NOT IN(1, 5)
            GROUP BY BL_ID";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($rowListing = mysql_fetch_array($result)) {
        listing_billing($rowListing['BL_Billing_Type'], $rowListing['BL_ID'], $rowListing['BL_CoC_Dis_2'], $rowListing['BL_Line_Item_Title'], $rowListing['BL_Line_Item_Price']);
    }

    if ($row['F_Points'] !== $_REQUEST['points']) {
        update_pointsin_business_tbl($new_listing); //declared in config.php
    }

    if ($_REQUEST['id'] > 0) {
        header("Location: store.php?id=" . $row['F_ID']);
    } else {
        header("Location: store-list.php");
    }
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Store</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP
        require '../include/nav-cart.php';
        ?>
    </div>
    <div class="right">
        <form name="form1" method="post" action="" enctype="multipart/form-data">
            <input type="hidden" name="bid" value="<?php echo $row['BL_ID'] ?>">
            <input type="hidden" name="op" value="save">
            <div class="content-header">Add On Details</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Name</label>
                <div class="form-data">
                    <input name="name" type="text" size="50" value="<?php echo $row['F_Name'] ?>" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Image</label>
                <div class="form-data div_image_library cat-region-width">
                    <div class="cropit-image-preview aboutus-photo-perview">
                        <img class="preview-img preview-img-script17" style="display: none;" src="">    
                        <?php if ($row['F_Image'] != '') { ?>
                            <img class="existing-img existing_imgs17" src="<?php echo IMG_LOC_REL . $row['F_Image'] ?>" >
                        <?php } ?>
                    </div>
                    <span class="daily_browse" onclick="show_image_library(17)">Select File</span>
                    <input type="hidden" name="image_bank_pic" id="image_bank17" value="">
                    <input type="file" onchange="show_file_name(17, this)" name="pic[]" id="photo17" style="display: none;">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Description</label>
                <div class="form-data">
                    <div class="form-inside-div border-none bottom-pading-none margin-none">
                        <div class="form-data">
                            <textarea name="desc" cols="85" rows="10" id="description-listing"><?php echo $row['F_Description'] ?></textarea>                           
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Price</label>
                <div class="form-data">
                    <input name="price" type="text" size="50" value="<?php echo $row['F_Price'] ?>" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Example</label>
                <div class="form-data">
                    <input name="example" type="text" size="50" value="<?php echo $row['F_Example'] ?>"/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Instructional Video</label>
                <div class="form-data">
                    <input name="video" type="text" size="50" value="<?php echo $row['F_Instruction_Video'] ?>"/>
                    <p>Link of this video must be embed link of source<br>
                        eg; http://www.dailymotion.com/embed/video/x12k542
                    </p>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Php File Name</label>
                <div class="form-data">
                    <input name="file" type="text" size="50" value="<?php echo $row['F_File'] ?>" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Points</label>
                <div class="form-data">
                    <input name="points" type="text" size="50" value="<?php echo $row['F_Points'] ?>"/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="Submit" value="Save" />
                </div>
            </div>
        </form>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>