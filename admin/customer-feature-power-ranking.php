<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/adminFunctions.inc.php';

if (!in_array('listing-tools', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
}

$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
} else {
    header("Location:customers.php");
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left">

    <?php require_once '../include/top-nav-listing.php'; ?>

    <div class="title-link">
        <div class="title">Add Ons - <?php echo  $rowListing['BL_Listing_Title'] ?></div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-listing-admin.php'; ?>
    </div>

    <div class="right">
        <div class="content-header">
            <div class="title">Power Ranking</div>
            <div class="link">
                <?php
                $powerRanking = show_addon_points(11);
                    echo '<div class="points-com">' . $powerRanking . ' pts</div>';
                
                ?>
            </div>
        </div>

        <?php
        $help_text = show_help_text('Power Ranking');
        if ($help_text != '') {
            echo '<div class="form-inside-div">' . $help_text . '</div>';
        }
        ?>
        
        <?php
        $sql1 = "SELECT * FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = 11";
        $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
        $row1 = mysql_fetch_assoc($result1);
        if ($row1['BLF_BL_ID'] != '') {
            ?>
            <div class="form-inside-div border-none text-align-center">
                <a href="customer-feature-add-ons.php?id=<?php echo $row1['BLF_ID'] ?>&bl_id=<?php echo $BL_ID ?>"  onclick="return confirm('Are you sure you want to <?php echo  $row1['BLF_Active'] == 1 ? "deactivate" : "activate" ?> this Add on?');"><?php echo  $row1['BLF_Active'] == 1 ? "Deactivate" : "Activate" ?> This Add on</a>
            </div>
            <?PHP
        }
        ?>
    </div>
</div>
<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>