<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if (!in_array('image-bank', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$BL_ID = isset($_REQUEST['listing']) ? $_REQUEST['listing'] : 0;
$ib_id = (isset($_REQUEST['ib_id']) ? $_REQUEST['ib_id'] : 0);
$pendVal = (isset($_REQUEST['pendVal']) ? $_REQUEST['pendVal'] : '');

if ($_SESSION['USER_ID'] > 0) {
    $sql = "SELECT U_F_Name FROM tbl_User WHERE U_ID = '" . encode_strings($_SESSION['USER_ID'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowUser = mysql_fetch_assoc($result);
}
if ($BL_ID > 0) {
    $sql = "SELECT BL_Listing_Title FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
}
$where = '';
if (in_array('county', $_SESSION['USER_ROLES'])) {
    $where = " LEFT JOIN tbl_User ON PO_ID = U_Owner where U_ID = '" . $_SESSION['USER_ID'] . "'";
}
$display = "";
if ($ib_id > 0) {
    $sql = "SELECT IB_Name, IB_Keyword, IB_Detail_Path, IB_Owner, IB_Photographer, IB_Region, IB_Category, IB_Listings, IB_People,  IB_Season, IB_Campaign, 
            IB_Location FROM tbl_Image_Bank LEFT JOIN tbl_Image_Bank_Photographer ON IB_Photographer = IBP_ID WHERE IB_ID = '" . encode_strings($ib_id, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowIb = mysql_fetch_assoc($result);
    $display = "style='display:none;'";
}
if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $ib_id = $_REQUEST['ib_id'];
    require '../include/picUpload.inc.php';
    if ($_SESSION['USER_LIMIT'] != '') {
        $community = $_SESSION['USER_LIMIT'];
    } else {
        $community = $_REQUEST['community'];
    }
    $date = date('Y-m-d');
    if ($ib_id > 0) {
        $sql = "tbl_Image_Bank SET 
                IB_Name = '" . encode_strings($_REQUEST['name'], $db) . "', 
                IB_Keyword = '" . encode_strings($_REQUEST['keyword'], $db) . "',
                IB_Owner = '" . encode_strings($_REQUEST['owner'], $db) . "',
                IB_Region = '" . encode_strings($community, $db) . "',
                IB_Category = '" . encode_strings($_REQUEST['category'], $db) . "',
                IB_Season = '" . encode_strings($_REQUEST['season'], $db) . "',
                IB_People = '" . encode_strings($_REQUEST['people'], $db) . "',
                IB_Listings = '" . encode_strings($_REQUEST['listings'], $db) . "',
                IB_Photographer = '" . encode_strings($_REQUEST['photographer'], $db) . "',
                IB_Campaign = '" . encode_strings($_REQUEST['campaign'], $db) . "',
                IB_Location = '" . encode_strings($_REQUEST['location'], $db) . "'";
        $sql = "UPDATE " . $sql . " WHERE IB_ID = '" . encode_strings($ib_id, $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        // TRACK DATA ENTRY
        Track_Data_Entry('Images','','Add Image to Image Bank',$ib_id,'Update','super admin');
    } else {
        foreach ($_FILES['pic']['name'] as $key => $val) {
            $sql = "tbl_Image_Bank SET 
                    IB_Name = '" . encode_strings($_REQUEST['name'], $db) . "', 
                    IB_Keyword = '" . encode_strings($_REQUEST['keyword'], $db) . "',
                    IB_Owner = '" . encode_strings($_REQUEST['owner'], $db) . "',
                    IB_Region = '" . encode_strings($community, $db) . "',
                    IB_Category = '" . encode_strings($_REQUEST['category'], $db) . "',
                    IB_Season = '" . encode_strings($_REQUEST['season'], $db) . "',
                    IB_People = '" . encode_strings($_REQUEST['people'], $db) . "',
                    IB_Listings = '" . encode_strings($_REQUEST['listings'], $db) . "',
                    IB_Date = '" . encode_strings($date, $db) . "',
                    IB_Photographer = '" . encode_strings($_REQUEST['photographer'], $db) . "',
                    IB_Campaign = '" . encode_strings($_REQUEST['campaign'], $db) . "',
                    IB_Location = '" . encode_strings($_REQUEST['location'], $db) . "'";
            $pic = Upload_Pic($key, 'pic', 0, 0, true, IMG_BANK_ABS, 15728640, true);
            if ($pic) {
                $path = str_replace(' ', "%20", 'http://' . DOMAIN . IMG_BANK_REL . $pic['0']);
                list($width, $height) = getimagesize($path);
                $dimension = $width . "X" . $height;
                $sql .= ", IB_Path = '" . encode_strings($pic['0'], $db) . "', IB_Thumbnail_Path = '" . encode_strings($pic['1'], $db) . "', 
                         IB_Detail_Path = '" . encode_strings($pic['2'], $db) . "', IB_Dimension = '" . encode_strings($dimension, $db) . "'";
                $sql = "INSERT " . $sql;
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                $id = mysql_insert_id();
                // TRACK DATA ENTRY
                Track_Data_Entry('Images','','Add Image to Image Bank',$id,'Add','super admin');
            }
        }
    }
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    exit();
}

require_once '../include/admin/header.php';
?>
<div id="autocomplete_data" style="display: none;"><?php include '../include/autocomplete.php' ?></div>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Image Bank</div>
        <div class="link">
        </div>         
    </div>

    <div class="right full-width">
        <form onSubmit="return false" id="MyUploadForm" name="form" id="progress-form" method="post" enctype="multipart/form-data" action="">
            <input type="hidden" name="op" value="save">
            <input type="hidden" id="image_page" value="1">
            <input type="hidden" id="pendVal" value="<?php echo $pendVal; ?>">
            <input type="hidden" name="image_bank" class="image_bank" id="image_bank" value="">
            <div class="content-header full-width">Add Image to Image Bank</div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Title</label>
                <div class="form-data">
                    <input name="name" type="text" id="image_title" required value="<?php echo (isset($rowIb['IB_Name']) && $rowIb['IB_Name'] != "") ? $rowIb['IB_Name'] : "" ?>"/>
                </div>
            </div>

            <div class="form-inside-div image-bank-autocomplete form-inside-div-width-admin full-width">
                <label>Keywords</label>
                <div class="form-data">
                    <input id="autocomplete" type="text"/>
                    <input type="hidden" id="keywords-searched" name="keyword" value="<?php echo (isset($rowIb['IB_Keyword']) && $rowIb['IB_Keyword'] != "") ? $rowIb['IB_Keyword'] : "" ?>">
                </div>
            </div>

            <!-- Check for Edit Page Otherwise it will work for second image upload in add image page -->
            <?php if (isset($ib_id) && $ib_id > 0) { ?>
                <div class="form-inside-div form-inside-div-width-admin full-width image-bank-margin-browse">
                    <label></label>
                    <div class="form-data image-bank-browse image_bank_width_browse">
                        <div class="approved-photo-div approved-photo-browse" <?php echo $display ?> >
                            <div class="inputWrapper adv-photo float-left">Browse<input  id="imageInput" onchange="show_file1(this)" class="setting-name fileInput" accept="image/*" type="file" name="pic[]" <?php echo ((isset($ib_id) && $ib_id > 0) ? "" : "required") ?>  /></div>
                        </div>
                        <div id="file-upload-name"></div>
                        <div class="approved-photo-div">
                            <?php if (isset($rowIb['IB_Detail_Path']) && $rowIb['IB_Detail_Path'] != "") { ?>
                                <a target="_blank" href=" <?php echo IMG_BANK_REL . $rowIb['IB_Detail_Path'] ?>"><img id="uploadFile" src="<?php echo IMG_BANK_REL . $rowIb['IB_Detail_Path']; ?>"></a>
                            <?php } else { ?>
                                <img id="uploadFile1" style="display: none;" src="" > 
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <!-- End of Check for Edit Page  -->   
                <div id="add-another-photo">
                    <div class="form-inside-div form-inside-div-width-admin full-width image-bank-margin-browse">
                        <label>Photos</label>
                        <div class="form-data image-bank-browse image_bank_width_browse">
                            <div class="inputWrapper adv-photo float-left">Browse
                                <input onchange="show_text()" id="imageInput" class="setting-name fileInput" accept="image/*"  type="file" name="pic[]" multiple>
                            </div>
                            <input id="uploadFile1" class="uploadFileName" disabled>
                            <div class="form-inside-div add-about-us-item-image margin-none" id="preview" style="display: none;">
                                <div class="cropit-image-preview aboutus-photo-perview">
                                    <img class="preview-img preview-img-script1" src="">    
                                </div>
                            </div>
                        </div> 
                    </div>
                </div> 
            <?php } ?>

            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Select Owner</label>
                <div class="form-data">
                    <select name="owner" required id="image_owner" onChange="getPhotographer(this.value)">
                        <option value="">Select Owner</option>
                        <?PHP
                        $sql = "SELECT PO_ID, PO_Name FROM tbl_Photographer_Owner" . $where;
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($region = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $region['PO_ID'] ?>" <?php echo ((isset($rowIb['IB_Owner']) && $region['PO_ID'] == $rowIb['IB_Owner'])) ? 'selected' : '' ?>><?php echo $region['PO_Name'] ?></option>
                            <?PHP
                        }
                        ?>
                    </select>
                </div>
            </div>

            <?php if (isset($rowIb['IB_Owner']) && $rowIb['IB_Owner'] != '') { ?>
                <div class="form-inside-div form-inside-div-width-admin full-width photo_region" >
                    <label>Photographer</label>
                    <?php
                    $sql = "SELECT IBP_ID, IBP_Name  FROM tbl_Owner_Photographes_Multiply
                            INNER JOIN tbl_Image_Bank_Photographer on IBP_ID= OPM_IBP_ID
                            WHERE OPM_PO_ID ='" . $rowIb['IB_Owner'] . "' ORDER BY IBP_ID";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    ?>
                    <div class="form-data photo_region">
                        <select class="photo_region" name="photographer" id="photographer" >
                            <option value="">Select Photographer</option>
                            <?PHP
                            while ($photographer = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo $photographer['IBP_ID'] ?>" <?php echo (isset($rowIb['IB_Photographer']) && $photographer['IBP_ID'] == $rowIb['IB_Photographer']) ? 'selected' : '' ?> ><?php echo $photographer['IBP_Name'] ?></option>
                                <?PHP
                            }
                            ?>
                        </select>
                    </div>
                </div>
            <?php } else { ?>
                <div class="form-inside-div form-inside-div-width-admin full-width photo_region" >
                    <label>Photographer</label>
                    <div class="form-data photo_region">
                        <select class="photo_region" name="photographer" id="photographer" >
                            <option value="">Select Photographer</option>
                        </select>
                    </div>
                </div>
                <?php
            }
            if (!in_array('county', $_SESSION['USER_ROLES'])) {
                ?>
                <div class="form-inside-div image-bank-autocomplete form-inside-div-width-admin full-width">
                    <label>Community</label>
                    <div class="form-data">
                        <input id="autocomplete_community" type="text"/>
                        <input type="hidden" id="autocomplete_community_searched" name="community" value="<?php echo (isset($rowIb['IB_Region']) && $rowIb['IB_Region'] != "") ? $rowIb['IB_Region'] : "" ?>">
                        <?php
                        $key_name_com = "";
                        if (isset($rowIb['IB_Region']) && $rowIb['IB_Region'] != "") {
                            $community_id = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_ID IN (" . $rowIb['IB_Region'] . ")";
                            $result = mysql_query($community_id);
                            $key_name_com = "[";
                            while ($row = mysql_fetch_assoc($result)) {
                                $key_name_com .= '{id:"' . $row['R_ID'] . '", name:"' . $row['R_Name'] . '"},';
                            }
                            $key_name_com .= rtrim(array_unique(explode(',', $key_name_com)), ',');
                            $key_name_com .= "]";
                        }
                        ?>
                    </div>
                </div>
            <?php } ?>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Category</label>
                <div class="form-data">
                    <select name="category" id="image_category" >
                        <option value="0">Select Category</option>
                        <?PHP
                        $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = 0";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($cat = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $cat['C_ID'] ?>" <?php echo (isset($rowIb['IB_Category']) && $cat['C_ID'] == $rowIb['IB_Category']) ? 'selected' : '' ?>><?php echo $cat['C_Name'] ?></option>
                            <?PHP
                        }
                        ?>
                    </select>
                </div>
            </div>
            <?php if (!in_array('county', $_SESSION['USER_ROLES'])) { ?>
                <div class="form-inside-div image-bank-autocomplete form-inside-div-width-admin full-width">
                    <label>Listings</label>
                    <div class="form-data listings_count">
                        <input id="autocomplete_lisitngs" type="text"/>
                        <input type="hidden" id="listing_search" name="listings" value="<?php echo (isset($rowIb['IB_Listings']) && $rowIb['IB_Listings'] != "") ? $rowIb['IB_Listings'] : "" ?>">
                        <?php
                        $key_name_listings = "";
                        if (isset($rowIb['IB_Listings']) && $rowIb['IB_Listings'] != "") {
                            $Listings_id = "SELECT BL_ID, BL_Listing_Title from tbl_Business_Listing WHERE BL_ID IN (" . $rowIb['IB_Listings'] . ") ";
                            $result_Listings = mysql_query($Listings_id);
                            $key_name_listings = "[";
                            while ($row_Listings = mysql_fetch_assoc($result_Listings)) {
                                $key_name_listings .= '{id:"' . $row_Listings['BL_ID'] . '", name:"' . $row_Listings['BL_Listing_Title'] . '"},';
                            }
                            $key_name_listings .= rtrim(array_unique(explode(',', $key_name_listings)), ',');
                            $key_name_listings .= "]";
                        }
                        ?>
                        <div id="auto-complete-div" style="display: none;"></div>
                        <div id="auto-complete-div-edit" style="display: none;">
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Are people in the photo?</label>
                <div class="form-data">
                    <select name="people" id="image_people" required>
                        <option value="">Select People</option>
                        <?PHP
                        $sql = "SELECT IBP_ID, IBP_Name FROM tbl_Image_Bank_People ORDER BY IBP_ID";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($people = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $people['IBP_ID'] ?>" <?php echo (isset($rowIb['IB_People']) && $people['IBP_ID'] == $rowIb['IB_People']) ? 'selected' : '' ?>><?php echo $people['IBP_Name'] ?></option>
                            <?PHP
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Season photo was taken</label>
                <div class="form-data">
                    <select name="season" id="image_season" required>
                        <option value="">Select Season</option>
                        <option value="-1" <?php echo (isset($rowIb['IB_Season']) && $rowIb['IB_Season'] == '-1') ? 'selected' : '' ?>>Not Applicable</option>
                        <?PHP
                        $sql = "SELECT IBS_ID, IBS_Name FROM tbl_Image_Bank_Season ORDER BY IBS_ID";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($season = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $season['IBS_ID'] ?>" <?php echo (isset($rowIb['IB_Season']) && $season['IBS_ID'] == $rowIb['IB_Season']) ? 'selected' : '' ?>><?php echo $season['IBS_Name'] ?></option>
                            <?PHP
                        }
                        ?>
                    </select>
                </div>
            </div>

            <?php if (!in_array('county', $_SESSION['USER_ROLES'])) { ?>
                <div class="form-inside-div form-inside-div-width-admin full-width">
                    <label>Is this a campaign image?</label>
                    <div class="form-data">
                        <select name="campaign" id="campaign_image" required>
                            <option value="">Select Campaign</option>
                            <?PHP
                            $sql = "SELECT IBC_ID, IBC_Name FROM tbl_Image_Bank_Campaign ORDER BY IBC_ID";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($campaign = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo $campaign['IBC_ID'] ?>" <?php echo (isset($rowIb['IB_Campaign']) && $campaign['IBC_ID'] == $rowIb['IB_Campaign']) ? 'selected' : '' ?>><?php echo $campaign['IBC_Name'] ?></option>
                                <?PHP
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-inside-div form-inside-div-width-admin full-width">
                    <label>Location</label>
                    <div class="form-data">
                        <input name="location" type="text" id="image_location"  value="<?php echo (isset($rowIb['IB_Location']) && $rowIb['IB_Location'] != "") ? $rowIb['IB_Location'] : "" ?>"/>
                    </div>
                </div>
            <?PHP } ?>

            <div class="form-inside-div form-inside-div-width-admin full-width">
                <div class="button">
                    <input type="submit" class="submit-autocomplete" onclick="progress_bar(<?php echo $ib_id ?>)" name="button" id="button" value="Save Photo" />
                </div>
            </div>

            <div class="form-inside-div border-none form-inside-div-width-admin full-width">
                <div class="form-data full-width">
                    Photo file limit size is 15MBs. If your files are large they may take 30 seconds to load once you click "Save Photo".
                </div>
            </div>
        </form>
        <div class="progress-container-main" style="display:none;">
            <div id="progress-container">
                <p class="progress-title">Upload in Progress</p>
                <p>Please leave this page open while your image is uploading</p>
                <div id="progressbox" style="display:none;">
                    <div id="progressbar"></div>
                    <div id="statustxt">0%</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function ()
    {
        //autocomplete_listings
        $("#autocomplete_lisitngs").tokenInput(<?php include '../include/autocomplete_lisitngs_without_cat.php'; ?>, {
            onAdd: function (item_lisitngs) {
                var list_val = $('#listing_search').val();
                if (list_val != '') {
                    $('#listing_search').val(list_val + "," + item_lisitngs.id);
                } else {
                    $('#listing_search').val(item_lisitngs.id);
                }
            },
            onDelete: function (item_lisitngs) {
                var value_listings = $('#listing_search').val().split(",");
                $('#listing_search').empty();
                var data_listings = "";
                $.each(value_listings, function (key, value_listings) {
                    if (value_listings != item_lisitngs.id) {
                        if (data_listings != '') {
                            data_listings = data_listings + "," + value_listings;
                        } else {
                            data_listings = value_listings;
                        }
                    }
                });
                $('#listing_search').val(data_listings);
            },
            resultsLimit: 10,
            preventDuplicates: true,
            prePopulate: <?php echo ($key_name_listings != "") ? $key_name_listings : "\"\"" ?>
        }
        );
        $('.token-input-dropdown').css("width", "320px");
    });

    $(function () {
        var search_text = $('#keywords-searched').val();
        var searched_items = ""
        if (search_text != "") {
            searched_items = search_text.split(",");
            var json = [];
            for (var i = 0; i < searched_items.length; i++) {
                json.push({"name": searched_items[i]});
            }
        }
        //autocomplete for lisitngs
        var search = $('#autocomplete_data').text();
        var searched_list = "";
        var json_data = [];
        if (search != "") {
            searched_list = search.split("%");
            for (var i = 0; i < searched_list.length - 1; i++) {
                json_data.push({"name": searched_list[i]});
            }
        }
        $("#autocomplete").tokenInput(json_data, {
            onResult: function (item) {
                if ($.isEmptyObject(item)) {
                    return [{id: '0', name: $("tester").text()}]
                } else {
                    item.unshift({"name": $("tester").text()});
                    var lookup = {};
                    var result = [];

                    for (var temp, i = 0; temp = item[i++]; ) {
                        var name = temp.name;

                        if (!(name in lookup)) {
                            lookup[name] = 1;
                            result.push({"name": name});
                        }
                    }
                    return result;
                }

            },
            onAdd: function (item) {
                var value = $('#keywords-searched').val();
                if (value != '') {
                    $('#keywords-searched').val(value + "," + item.name);
                } else {
                    $('#keywords-searched').val(item.name);
                }
            },
            onDelete: function (item) {
                var value = $('#keywords-searched').val().split(",");
                $('#keywords-searched').empty();
                var data = "";
                $.each(value, function (key, value) {
                    if (value != item.name) {
                        if (data != '') {
                            data = data + "," + value;
                        } else {
                            data = value;
                        }
                    }
                });
                $('#keywords-searched').val(data);
            },
            resultsLimit: 10,
            prePopulate: json
        }
        );
        //autocomplete for Community
        $("#autocomplete_community").tokenInput(<?php include '../include/autocomplete_community.php'; ?>, {
            onAdd: function (item_community) {
                var com_val = $('#autocomplete_community_searched').val();
                if (com_val != '') {
                    $('#autocomplete_community_searched').val(com_val + "," + item_community.id);
                } else {
                    $('#autocomplete_community_searched').val(item_community.id);
                }
            },
            onDelete: function (item_community) {
                var value_comm = $('#autocomplete_community_searched').val().split(",");
                $('#autocomplete_community_searched').empty();
                var data_comm = "";
                $.each(value_comm, function (key, value_comm) {
                    if (value_comm != item_community.id) {
                        if (data_comm != '') {
                            data_comm = data_comm + "," + value_comm;
                        } else {
                            data_comm = value_comm;
                        }
                    }
                });
                $('#autocomplete_community_searched').val(data_comm);
            },
            resultsLimit: 10,
            preventDuplicates: true,
            prePopulate: <?php echo ($key_name_com != "") ? $key_name_com : "\"\"" ?>
        }
        );
        $('.token-input-dropdown').css("width", "320px");
    });
    function show_file(input) {
        if (input.files && input.files[0]) {
            $('#uploadFile').show();
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#uploadFile')
                        .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    function show_file1(input) {
        if (input.files && input.files[0]) {
            $('#uploadFile1').show();
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#uploadFile1')
                        .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    //FUNCTION TO ALERT IF MORE THAN 10 PHOTOS ARE SELECTED
    $("input[type='file']").on("change", function () {
        var numFiles = $(this).get(0).files.length;
        if (numFiles > 20) {
            swal("", "You can upload only 20 photos.", "warning");
            $("#imageInput").val([]);
        }

    });
    function show_text() {
        $('#uploadFile1').show();
        $('input[type="file"]').change(function () {
            var test = $(this).val(); //alert(test);
            var imageVal = $(this)[0].files.length;
            if (imageVal == 1) {
                document.getElementById("uploadFile1").value = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '');
                $('#preview').hide();
                $('#image_bank1').val('');
                return false;
            }
            document.getElementById("uploadFile1").value = "Multiple Images Selected";
            $('#preview').hide();
            $('#image_bank1').val('');
            return false;
        });

    }

</script>
<?PHP
require_once '../include/admin/footer.php';
?>
<style>
/*   .custom-ajax-show{ 
        display: none;
         z-index: -9999; 
    }
  .ajaxSpinnerImage{
        display: none;
        z-index: -9999;   
    }*/
</style>