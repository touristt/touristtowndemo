<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/adminFunctions.inc.php';

if (!in_array('business-listings', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
} else {
    header("Location:customers.php");
    exit();
}
if ($_POST['op'] == 'save') {
    $acc = $_POST['acc'];
    require_once '../include/picUpload.inc.php';
    if ($_POST['image_bank'] == "") {
        // last @param 18 = Product Accordion image
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 18, 'Listing', $BL_ID);
        if (is_array($pic)) {
            $sql = "INSERT tbl_Business_Feature_Product SET 
                    BFP_Title = '" . encode_strings($_POST['title'], $db) . "',
                    BFP_Description = '" . encode_strings($_POST['descriptionNEW'], $db) . "',
                    BFP_Type = '" . encode_strings($_POST['PS_ID'], $db) . "',
                    BFP_Price = '" . encode_strings($_POST['priceNEW'], $db) . "',
                    BFP_BL_ID = '" . encode_strings($_POST['bl_id'], $db) . "',
                    BFP_Ask_Price = '" . encode_strings($_POST['ask_price'], $db) . "',
                    BFP_Photo = '" . encode_strings($pic['0']['0'], $db) . "'";
            $pic_id = $pic['1'];
        }
    } else {
        $pic_id = $_POST['image_bank'];
        $ext = explode(".", $pic);
        $myLink = mt_rand(1000, 9999999);
        // last @param 18 = Product Accordion image
        $pic = Upload_Pic_Library($pic_id, 18);
        if (is_array($pic)) {
            Update_Image_Bank_Listings($pic_id, $BL_ID);
            $sql = "INSERT tbl_Business_Feature_Product SET 
                    BFP_Title = '" . encode_strings($_POST['title'], $db) . "',
                    BFP_Description = '" . encode_strings($_POST['descriptionNEW'], $db) . "',
                    BFP_Type = '" . encode_strings($_POST['PS_ID'], $db) . "',
                    BFP_Price = '" . encode_strings($_POST['priceNEW'], $db) . "',
                    BFP_BL_ID = '" . encode_strings($_POST['bl_id'], $db) . "',
                    BFP_Ask_Price = '" . encode_strings($_POST['ask_price'], $db) . "',
                    BFP_Photo = '" . encode_strings($pic['0'], $db) . "'";
        }
    }
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $id = mysql_insert_id();
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
    update_pointsin_business_tbl($BL_ID);
        if ($pic_id > 0) {
            //Image usage from image bank
            imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Our_Product', $id);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/customer-feature-product.php?bl_id=" . $BL_ID . "&acc=" . $acc);
    exit();
}

if ($_POST['op'] == 'edit') {
    $acc = $_POST['acc'];
    // last @param 18 = Product Accordion image
    require_once '../include/picUpload.inc.php';
    if ($_POST['image_bank'] == "") {
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 18, 'Listing', $BL_ID);
        if (is_array($pic)) {
            $sql = "UPDATE tbl_Business_Feature_Product SET 						
                    BFP_Title = '" . encode_strings($_POST['title'], $db) . "',
                    BFP_Description = '" . encode_strings($_POST['description'], $db) . "',
                    BFP_Price = '" . encode_strings($_POST['price'], $db) . "',
                    BFP_Ask_Price = '" . encode_strings($_POST['ask_price'], $db) . "',
                    BFP_Photo = '" . encode_strings($pic['0']['0'], $db) . "'
                    WHERE BFP_ID = '" . encode_strings($_POST['BFP_ID'], $db) . "'";
            $pic_id = $pic['1'];
        } else {
            $sql = "UPDATE tbl_Business_Feature_Product SET 						
                    BFP_Title = '" . encode_strings($_POST['title'], $db) . "',
                    BFP_Description = '" . encode_strings($_POST['description'], $db) . "',
                    BFP_Ask_Price = '" . encode_strings($_POST['ask_price'], $db) . "',
                    BFP_Price = '" . encode_strings($_POST['price'], $db) . "'
                    WHERE BFP_ID = '" . encode_strings($_POST['BFP_ID'], $db) . "'";
        }
    } else {
        $pic_id = $_POST['image_bank'];
        $ext = explode(".", $pic);
        $myLink = mt_rand(1000, 9999999);
        // last @param 18 = Product Accordion image
        $pic = Upload_Pic_Library($pic_id, 18);
        if (is_array($pic)) {
            Update_Image_Bank_Listings($pic_id, $BL_ID);
            $sql = "UPDATE tbl_Business_Feature_Product SET 						
                    BFP_Title = '" . encode_strings($_POST['title'], $db) . "',
                    BFP_Description = '" . encode_strings($_POST['description'], $db) . "',
                    BFP_Price = '" . encode_strings($_POST['price'], $db) . "',
                    BFP_Ask_Price = '" . encode_strings($_POST['ask_price'], $db) . "',
                    BFP_Photo = '" . encode_strings($pic['0'], $db) . "'
                    WHERE BFP_ID = '" . encode_strings($_POST['BFP_ID'], $db) . "'";
        }
    }
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
    update_pointsin_business_tbl($BL_ID);
        if ($pic_id > 0) {
            $id = $_POST['BFP_ID'];
            //Image usage from image bank
            imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Our_Product', $id);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/customer-feature-product.php?bl_id=" . $BL_ID . "&acc=" . $acc);
    exit();
}

if ($_REQUEST['op'] == 'del') {
    $BFP_ID = $_REQUEST['bfp_id'];
    $BL_ID = $_REQUEST['bl_id'];
    $acc = $_REQUEST['acc'];
    $sql = "DELETE FROM tbl_Business_Feature_Product WHERE BFP_ID = '$BFP_ID'";
    $result = mysql_query($sql);
    if ($result) {
        $_SESSION['delete'] = 1;
        //update points only for listing
    update_pointsin_business_tbl($BL_ID);
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Our_Product', $BFP_ID);
    } else {
        $_SESSION['delete_error'] = 1;
    }

    header("Location: /admin/customer-feature-product.php?bl_id=" . $BL_ID . "&acc=" . $acc);
    exit();
}

$activeAccordion = '';
if (isset($_GET['acc']) && $_GET['acc'] != '') {
    $activeAccordion = $_GET['acc'];
} else {
    $activeAccordion = 'false';
}
require_once '../include/admin/header.php';
?>
<div class="content-left">
    <?php require_once '../include/top-nav-listing.php'; ?>
    <div class="title-link">
        <div class="title">Add Ons - <?php echo  $rowListing['BL_Listing_Title'] ?></div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div> 
    </div>
    <div class="left">
        <?PHP require '../include/nav-listing-admin.php'; ?>
    </div>
    <div class="right">
        <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
        <input type="hidden" name="bid" id="bidForProduct" value="<?php echo $BL_ID ?>">
        <input type="hidden" name="op" value="save">

        <div class="content-header">
            <div class="title">Our Products</div>
            <div class="link">
                <?php
                $Product = show_addon_points(3);
                if ($numRowsProd > 0) {
                    echo '<div class="points-com">' . $Product . ' pts</div>';
                } else {
                    echo '<div class="points-uncom">' . $Product . ' pts</div>';
                }
                ?>
            </div>
        </div>
        <div class="form-inside-div border-none">
            <?php
            $help_text = show_help_text('Our Products');
            if ($help_text != '') {
                echo '<div class="form-inside-div border-none padding-none margin-none">' . $help_text . '</div>';
            }
            ?>
            <div class="addProductAboutUs border-none">
                <a id="addProductSection">+ Add Product Section</a>
            </div>
        </div>
        <div class="addParentCategory" id="on-click-toggle-section" style="display: none">
            <div class="form-inside-div add-product-section">
                <div class="form-data add-menu-item-field margin-about-entertainment">
                    <label>Title</label>
                    <input id="titleCategory" type="text">
                </div>
                <div class="form-data add-menu-item-field">
                    <label>Description</label>
                    <textarea id="titleCategorytextarea"></textarea>
                </div>
                <div class="form-inside-div border-none">
                    <div class="button">
                        <input type="submit" onClick="addSection();" value="Save">
                    </div>
                </div>
            </div>
        </div>
        <div class="menu-items-accodings product-accordion ">                         
            <div id="accordion">
                <?PHP
                $SelectProduct = "SELECT * FROM tbl_Product_Section WHERE BL_ID = '$BL_ID' ORDER BY PS_Order";
                $SelectProductResult = mysql_query($SelectProduct) or die(mysql_error());
                $numRowsProdSec = mysql_num_rows($SelectProductResult);
                $acco = 0;
                while ($rowProductResult = mysql_fetch_array($SelectProductResult)) {
                    $description = $rowProductResult['PS_TITLE'];
                    if (strlen($description) > 35) {
                        $description = substr($description, 0, 35) . '...';
                    }
                    ?>
                    <div class="group" id="recordsArray_<?php echo $rowProductResult['PS_ID'] ?>">
                        <h3 class="accordion-rows about-us-accordion-h3"><span class="product-accordion-title"><?php echo $description; ?></span><label class="add-item-accordion delete-margin"><a onclick="deletesection('<?php echo $rowProductResult['PS_ID'] ?>')">+ Delete Section</a></label><label class="add-item-accordion"><a onclick="show_form('<?php echo $rowProductResult['PS_ID']; ?>')">+ Add Product</a></label></h3>
                        <div class="sub-accordions accordion-padding">
                            <div class="addParentCategory">
                                <div class="form-inside-div add-product-section add-product-section-in-accordion">
                                    <div class="form-data add-menu-item-field margin-about-entertainment">
                                        <label>Title</label>
                                        <input id="titleCategoryAccordion<?php echo $rowProductResult['PS_ID']; ?>" type="text" value="<?php echo $rowProductResult['PS_TITLE']; ?>">
                                        <input id="PS_ID" type="hidden" value="<?php echo $rowProductResult['PS_ID']; ?>">
                                    </div>
                                    <div class="form-data add-menu-item-field wizwig_editor_product">
                                        <label>Description</label>
                                        <textarea id="titleCategorytextareaAccordion<?php echo $rowProductResult['PS_ID']; ?>"><?php echo $rowProductResult['PS_DESC']; ?></textarea>
                                    </div>
                                    <!--                                    <div class="addProductSection">
                                                                            <a onclick="UpdateSection(<?php //echo $rowProductResult['PS_ID'];                                                        ?>);">Save</a>
                                                                        </div>-->
                                    <div class="form-inside-div border-none">
                                        <div class="button">
                                            <input type="submit" onClick="UpdateSection(<?php echo $rowProductResult['PS_ID']; ?>, <?php echo  $acco ?>);" value="Save">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <script>
                                function deletesection(section_id){
                                    if(confirm("Are you sure you want to delete this section?")){
                                        var bl_id = $('#bidForProduct').val();
                                        $.post("customer-feature-delete-section.php",{
                                            section_id:section_id,     
                                            bl_id:bl_id
                                        },
                                        function(result){
                                            if (result==1)
                                                location.reload();
                                        })
                                    }
                                    else {
                                        return false;
                                    }                                                                                                          
                                }
                            </script>
                            <div class="accordion-separator product-separator"></div>
                            <form name="menu-item" method="post" onSubmit="return check_img_size(<?php echo $rowProductResult['PS_ID'] ?>, 10000000)"  enctype="multipart/form-data" action="">
                                <input type="hidden" name="bl_id" value="<?php echo  $BL_ID ?>">
                                <input type="hidden" name="PS_ID" value="<?php echo $rowProductResult['PS_ID'] ?>">
                                <input type="hidden" name="op" value="save">
                                <input type="hidden" name="acc" value="<?php echo $acco?>">
                                <input type="hidden" name="image_bank" class="image_bank" id="image_bank<?php echo $rowProductResult['PS_ID'] ?>" value="">
                                <div id="gallery-form<?php echo $rowProductResult['PS_ID'] ?>" style="display: none;">
                                    <div class="add-menu-item-container">
                                        <div class="form-inside-div add-menu-item">
                                            <label>Name</label>
                                            <div class="form-data add-menu-item-field daily-special-width">
                                                <input name="title" type="text" id="title<?php echo $rowProductResult['PS_ID']; ?>" size="50" required/>
                                            </div>
                                        </div>
                                        <div class="form-inside-div add-menu-item wizwig_editor_product_items">
                                            <label>Description</label>
                                            <div class="form-data add-menu-item-field">
                                                <textarea name="descriptionNEW" cols="40" rows="7" wrap="VIRTUAL" class="formtext" id="descriptionNEW<?php echo $rowProductResult['PS_ID']; ?>"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-inside-div add-menu-item daily-special-width">
                                            <label>Price<span>$</span></label>
                                            <div class="form-data add-menu-item-field">
                                                <input name="priceNEW" type="text" id="priceNEW<?php echo $rowProductResult['PS_ID']; ?>" value="" size="5" required/>
                                            </div>
                                        </div>
                                        <div class="form-inside-div add-menu-item">
                                            <label>Ask Us<span></span></label>
                                            <div class="form-data add-menu-item-field">
                                                <input class="ask_price_product" name="ask_price" type="checkbox" id="askprice<?php echo $rowProductResult['PS_ID']; ?>" value="1" onclick="price_asked_new(<?php echo $rowProductResult['PS_ID']; ?>)"/>
                                    </div>
                                        </div>
                                    </div>
                                    <div class="form-inside-div div_image_library add-menu-item-image">
                                        <span class="daily_browse" onclick="choose_file(<?php echo $rowProductResult['PS_ID'] ?>);">Select Photo</span>
                                        <div class="dialog-close dialog<?php echo $rowProductResult['PS_ID'] ?> dialog-bank" style="display: none;">
                                            <label for="photo<?php echo $rowProductResult['PS_ID']; ?>" class="daily_browse">Upload New Photo</label>
                                            <a onclick="show_image_library(18,<?php echo $rowProductResult['PS_ID']; ?>)">Add from Library</a>
                                        </div>
                                        <input type="file" name="pic[]" onchange="show_file_name(18, this, <?php echo $rowProductResult['PS_ID'] ?>)" id="photo<?php echo $rowProductResult['PS_ID']; ?>" style="display: none;">
                                        <div class="form-inside-div add-about-us-item-image margin-none">
                                            <div class="cropit-image-preview aboutus-photo-perview">
                                                <img class="preview-img preview-img-script<?php echo $rowProductResult['PS_ID'] ?>" style="display: none;" src="">    
                                    </div>
                                        </div>
                                    </div>
                                    <div class="form-inside-div bottomBorderNone">
                                        <div class="button product-new-btn">
                                            <input type="submit" name="button3" class="button3<?php echo $rowProductResult['PS_ID']; ?>" value="Save Product" />
                                        </div>
                                    </div>
                                    <div class="accordion-separator product-separator"></div>
                                </div>
                            </form>
                            <?PHP
                            $BFP_Type = $rowProductResult['PS_ID'];
                            $sql = "SELECT * FROM tbl_Business_Feature_Product 
                                    WHERE BFP_BL_ID = '$BL_ID' AND 
                                    BFP_Type= '$BFP_Type' ORDER BY BFP_Order";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            $counter = 1;
                            $numRows = mysql_num_rows($result);
                            ?>
                            <div id="sortable-container">
                                <ul class="brief menu-order-sortable add-menu-item-container">
                                    <?php
                                    while ($data = mysql_fetch_assoc($result)) {
                                        ?>                    
                                        <li class="menu-order-sortable" id="recordsArray_<?php echo $data['BFP_ID'] ?>">
                                            <form name="menu-item" method="post" onSubmit="return check_img_size(<?php echo $data['BFP_ID'] ?>, 10000000)"  enctype="multipart/form-data" action="">
                                                <input type="hidden" name="bl_id" value="<?php echo  $BL_ID ?>">
                                                <input type="hidden" name="BFP_ID" value="<?php echo $data['BFP_ID'] ?>">
                                                <input type="hidden" name="op" value="edit">
                                                <input type="hidden" name="acc" value="<?php echo $acco?>">
                                                <input type="hidden" id="update_<?php echo $data['BFP_ID'] ?>" value="1">
                                                <input type="hidden" name="image_bank" class="image_bank" id="image_bank<?php echo $data['BFP_ID'] ?>" value="">
                                                <div class="add-menu-item-container">
                                                    <div class="form-inside-div add-menu-item">
                                                        <label>Name</label>
                                                        <div class="form-data add-menu-item-field daily-special-width">
                                                            <input name="title" type="text" id="title-<?php echo  $data['BFP_ID'] ?>" value="<?php echo  $data['BFP_Title'] ?>" size="25" required/>
                                                        </div>
                                                    </div>
                                                    <div class="form-inside-div add-menu-item wizwig_editor_product_items">
                                                        <label>Description</label>
                                                        <div class="form-data add-menu-item-field">
                                                            <textarea name="description" cols="40" rows="7" wrap="VIRTUAL" class="formtext" id="description-<?php echo  $data['BFP_ID'] ?>"><?php echo  $data['BFP_Description'] ?></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="add-menu-item-container">
                                                        <div class="form-inside-div add-menu-item">
                                                            <label>Price<span>$</span></label>
                                                            <div class="form-data add-menu-item-field daily-special-width">
                                                                <input name="price" type="text" id="price-<?php echo  $data['BFP_ID'] ?>" value="<?php echo  $data['BFP_Price'] ?>" size="5" required/>
                                                            </div>
                                                        </div>
                                                    </div>   
                                                    <div class="add-menu-item-container">
                                                        <div class="form-inside-div add-menu-item">
                                                            <label>Ask Us<span></span></label>
                                                            <div class="form-data add-menu-item-field">
                                                                <input class="ask_price_product" name="ask_price" type="checkbox" id="ask_value-<?php echo  $data['BFP_ID'] ?>" value="1" <?php echo  ($data['BFP_Ask_Price'] == 1) ? 'checked' : ''; ?>  onclick="price_asked(<?php echo  $data['BFP_ID'] ?>)" />
                                                </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-inside-div div_image_library add-menu-item-image">
                                                    <span class="daily_browse" onclick="choose_file(<?php echo $data['BFP_ID'] ?>);">Select Photo</span>
                                                    <div class="dialog-close dialog<?php echo $data['BFP_ID'] ?> dialog-bank" style="display: none;">
                                                        <label for="photo<?php echo $data['BFP_ID'] ?>" class="close_file_dialog daily_browse">Upload New Photo</label> 
                                                        <a onclick="show_image_library(18,<?php echo $data['BFP_ID']; ?>)">Add from Library</a>
                                                    </div>
                                                    <input type="file" name="pic[]" onchange="show_file_name(18, this, <?php echo $data['BFP_ID'] ?>)" id="photo<?php echo $data['BFP_ID'] ?>" style="display :none;">
                                                    <div class="form-inside-div add-about-us-item-image margin-none">
                                                        <div class="cropit-image-preview aboutus-photo-perview">
                                                            <img class="preview-img preview-img-script<?php echo $data['BFP_ID'] ?>" style="display: none;" src="">    
                                                            <?php if ($data['BFP_Photo'] != "") { ?>
                                                                <img class="existing-img existing_imgs<?php echo $data['BFP_ID'] ?>" src="http://<?php echo  DOMAIN . IMG_LOC_REL . $data['BFP_Photo'] ?>">
                                                            <?php } ?>
                                                </div>
                                                    </div>
                                                </div>
                                                <div class="form-inside-div add-menu-item-button">
                                                    <div class="button product-butn-replace">
                                                        <input type="submit" name="button3" class="margin-right-btn button<?php echo $data['BFP_ID'] ?>" value="Save Product" />
                                                        <a class="btn-anchor" href="customer-feature-product.php?bl_id=<?php echo  $BL_ID ?>&bfp_id=<?php echo  $data['BFP_ID'] ?>&op=del&acc=<?php echo  $acco ?>" onClick="return confirm('Are you sure you want to delete item?');">Delete Product</a>
                                                    </div>
                                                    <div class="delete-orange"></div>
                                                </div>
                                                <div class="accordion-separator product-separator"></div>
                                            </form>
                                        </li>
                                        <script>                                                            
                                            CKEDITOR.disableAutoInline = true;
                                            $( document ).ready( function() {
                                                var wzwigIDEdit = <?php echo $data['BFP_ID'] ?>;
                                                $( '#description-'+wzwigIDEdit ).ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
                                            });
                                        </script>
                                        <?PHP
                                        $counter++;
                                    }
                                    ?>
                                </ul>
                                <script>
                                    CKEDITOR.disableAutoInline = true;
                                    $( document ).ready( function() {
                                        var wzwigID = <?php echo $rowProductResult['PS_ID'] ?>;
                                        $( '#descriptionNEW'+wzwigID ).ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
                                        var wzwigIDupdate = <?php echo $rowProductResult['PS_ID']; ?>;
                                        $( '#titleCategorytextareaAccordion'+wzwigIDupdate ).ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
                                    });
                                </script>
                            </div>
                        </div>    
                    </div>    
                    <?PHP
                    $acco++;
                }
                ?>
            </div> 
        </div>
        <div id="image-library" style="display:none;"></div>
        <input id="image-library-usage" type="hidden" value="">
        <!--        </form>-->

        <?php
        $sql1 = "SELECT * FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = 3";
        $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
        $row1 = mysql_fetch_assoc($result1);
        if ($row1['BLF_BL_ID'] != '') {
            ?>
            <div class="form-inside-div border-none text-align-center">
                <a href="customer-feature-add-ons.php?id=<?php echo $row1['BLF_ID'] ?>&bl_id=<?php echo $BL_ID ?>"  onclick="return confirm('Are you sure you want to <?php echo  $row1['BLF_Active'] == 1 ? "deactivate" : "activate" ?> this Add on?');"><?php echo  $row1['BLF_Active'] == 1 ? "Deactivate" : "Activate" ?> This Add on</a>
            </div>
            <?PHP
        }
        ?>
    </div>
    <div id="dialog-message"></div>
</div>
<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>
<script>
    $(document).ready(function(){
        $("#addProductSection").click(function(){
            $("#on-click-toggle-section").toggle();
        });
    });    
    $(function() {
        $('.accordion-rows a').click(function(event) {
            if($(this).parent().parent().hasClass("ui-accordion-header")) {
                event.stopPropagation(); // this is
            }
        });
        $( "#accordion" )
        .accordion({
            collapsible: true,
            heightStyle: 'content',
            header: "> div > h3",
            active: <?php echo  $activeAccordion; ?>
        })
        .sortable({
            axis: "y",
            handle: "h3",
            update: function() {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Product_Section&field=PS_Order&id=PS_ID';
                $.post("reorder.php", order, function(theResponse) {
                    location.reload();
                });
            },
            stop: function( event, ui ) {
                // IE doesn't register the blur when sorting
                // so trigger focusout handlers to remove .ui-state-focus
                ui.item.children( "h3" ).triggerHandler( "focusout" );
 
                // Refresh accordion to handle new order
                $( this ).accordion( "refresh" );
            }
        });
    });
    function show_form(PS_ID){
        $('#gallery-form'+PS_ID).show(); 
        PS_ID.stopPropagation();
    }
    CKEDITOR.disableAutoInline = true;
    $( document ).ready( function() {
        $( '#titleCategorytextarea' ).ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
    });  
    function addSection(){
        var BL_ID = $('#bidForProduct').val();
        var addSection = $('#titleCategory').val();
        var addTextArea = $('#titleCategorytextarea').val();
        if(addSection != ""){
            $.post("customer-feature-add-product-section.php", {
                addSection: addSection,
                addTextArea: addTextArea,
                blID: BL_ID
            },function(done){
                if(done == 1){
                    location.reload();
                }
                else if(done == 0){
                    alert('Section Already exist!');
                    return false;
                    //                    window.location = "customer-feature-product.php?bl_id="+BL_ID;
                }
            });
        }
        else{
            alert("Title field is required");
            return false;
            //            window.location = "customer-feature-product.php?bl_id="+BL_ID;
        }
    }
   
    function UpdateSection(PS_ID, ACTIVE_ACC){
        var addSectionUpdate = $('#titleCategoryAccordion'+PS_ID).val();
        var addTextAreaUpdate = $('#titleCategorytextareaAccordion'+PS_ID).val();
        var BL_ID = $('#bidForProduct').val();
        //        var PS_ID = $('#PS_ID').val();
        if(addSectionUpdate != ""){
            $.post("customer-feature-add-product-section.php", {
                addSectionUpdate: addSectionUpdate,
                addTextAreaUpdate: addTextAreaUpdate,
                PS_ID: PS_ID,
                updateValue: 'updateTrue'
            },function(done){
                if(done == 1){
                    window.location.href = "customer-feature-product.php?bl_id="+BL_ID+"&acc="+ACTIVE_ACC;
                }
            });
        } else {
            alert("Title field is required");
            return false;
            //            window.location.href = "customer-feature-product.php?bl_id="+BL_ID;
        }
        
    }
    $(function() {
        $("ul.brief").sortable({opacity: 0.9, cursor: 'move', update: function() {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Business_Feature_Product&field=BFP_Order&id=BFP_ID';
                $.post("reorder.php", order, function(theResponse) {
                    $("#dialog-message").html("Product Items Re-Ordered");
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            Ok: function() {
                                $(this).dialog("close");
                            }
                        }
                    });
                });
            }
        });
    });
</script>