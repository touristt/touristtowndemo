<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

$where = "WHERE 1=1";
if (!in_array('manage-stories', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
if (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
    $where = ' WHERE SR_R_ID IN (' . $_SESSION['USER_LIMIT'] . ')';
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Stories</div>
        <div class="link">
            <form id="frmSort" name="form1" method="GET" action="">
                <input type="hidden" value="<?php echo $type; ?>" name="<?php echo $name; ?>">
                <?php
                if (!isset($_SESSION['USER_LIMIT']) || $_SESSION['USER_LIMIT'] == '') {
                    $sortregion = (isset($_REQUEST['sortregion'])) ? $_REQUEST['sortregion'] : '';
                    ?>
                    <label>
                        <span class="addlisting">Region:</span>
                        <select name="sortregion" id="sortregion" onChange="getSubOptions(this.value)">
                            <option value="">Select Region</option>
                            <?php
                            $getRegions = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Domain != ''";
                            $resRegion = mysql_query($getRegions, $db) or die("Invalid query: $getRegions -- " . mysql_error());
                            while ($rowRegion = mysql_fetch_array($resRegion)) {
                                ?>
                                <option value="<?php echo $rowRegion['R_ID'] ?>" <?php echo ($sortregion == $rowRegion['R_ID']) ? 'selected="selected"' : ''; ?>><?php echo $rowRegion['R_Name'] ?></option>
                            <?php } ?>
                        </select>
                    </label>

                    <label>
                        <span class="addlisting">Sort:<?php
                            $sortby = isset($_REQUEST['sortby']) ? $_REQUEST['sortby'] : '';
                            ?></span>
                        <select name="sortby" onChange="getSubOptions(this.value)">
                            <option value="">Select Category</option>
                            <?php
                            $sql = "SELECT C_ID FROM tbl_Category WHERE C_Is_Blog = 1";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            $rowExplore = mysql_fetch_array($result);
                            $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '" . $rowExplore['C_ID'] . "' ORDER BY C_ID ASC";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($rowExploreSub = mysql_fetch_array($result)) {
                                ?>
                                <option value="<?php echo $rowExploreSub['C_ID'] ?>" <?php echo ($sortby == $rowExploreSub['C_ID']) ? 'selected' : ''; ?>>
                                    <?php echo $rowExploreSub['C_Name']; ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>

                    </label>
                <?php } ?>
                <script type="text/javascript">
                    function getSubOptions(myVal) {
                        $('#frmSort').submit();
                    }
                </script>
            </form>
        </div>
    </div>

    <div class="left">
        <div class="sub-menu">
            <ul>
                <li class="addlisting region-sub"><a href="story.php">+Add Story</a></li>
            </ul>
        </div>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none spl-stories">Story</div>
            <div class="data-column padding-none spl-stories-author">Author</div>
            <div class="data-column padding-none spl-other-stories">Active</div>
            <div class="data-column padding-none spl-other-stories">Edit</div>
            <div class="data-column padding-none spl-other-stories">Delete</div>
        </div>
        <?PHP
        $regionBy = '';
        $sortBy = '';
        if (isset($_REQUEST['sortregion']) && $_REQUEST['sortregion'] !== '') {
            $regionBy = " AND SR_R_ID = " . $_REQUEST['sortregion'];
        }
        if (isset($_REQUEST['sortby']) && $_REQUEST['sortby'] !== '') {

            $sortBy = " AND S_Category = " . $_REQUEST['sortby'];
        }
        $sql = "SELECT DISTINCT tbl_Story.* FROM tbl_Story LEFT JOIN tbl_Story_Region ON S_ID = SR_S_ID $where $regionBy $sortBy ORDER BY S_Title ASC";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        if (mysql_num_rows($result)) {
            while ($row = mysql_fetch_assoc($result)) {
                if (isset($row['S_Active']) && $row['S_Active'] == 1) {
                    $checked = "checked";
                } else {
                    $checked = "";
                }
                ?>
                <div class="data-content">
                    <div class="data-column spl-stories"><a href="stories-preview.php?id=<?php echo $row['S_ID'] ?>"><?php echo $row['S_Title'] ?></a></div>
                    <div class="data-column spl-stories-author"><?php echo $row['S_Author'] ?></div>
                    <div class="data-column spl-other-stories"><input type="checkbox" name="display" id="display<?php echo $row['S_ID']; ?>" <?php echo $checked; ?> onclick=" return hide_show_stories(<?php echo $row['S_ID']; ?>);"></div>
                    <div class="data-column spl-other-stories"><a href="story.php?id=<?php echo $row['S_ID'] ?>">Edit</a></div>
                    <div class="data-column spl-other-stories"><a onClick="return confirm('Are you sure?\nThis action CANNOT be undone!');" href="story.php?op=del&amp;id=<?php echo $row['S_ID'] ?>">Delete</a></div>
                </div>
                <?PHP
            }
        } else {
            ?>
            <div class="data-content">
                <div class="data-column spl-name">You have not added stories yet.</div>
            </div>
            <?PHP
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>

<script>
    function hide_show_stories(id) {
        var value = document.getElementById('display' + id).checked;
        $.ajax({
            type: "POST",
            url: 'show_hide_story.php',
            dataType: 'json',
            data: {
                S_ID: id,
                show_hide: value
            },
            success: function (response) {
                if (response == 0) {
                    swal("Story Deactivated", "Story has been deactivated successfully.", "success");
                } else if(response == 1) {
                    swal("Story Activated", "Story has been activated successfully.", "success");
                }else{
                    swal("Error", "Error saving data. Please try again.", "error");
                }
            }
        });
    }
</script>