<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if (!in_array('manage-routes', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} else {
    header('location: regions.php');
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $sql = "tbl_Route SET ";

    require '../include/picUpload.inc.php';
    if ($_POST['image_bank_desktop'] == "") {
        // last @param 30 = Route Main Image
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 30);
        if (is_array($pic)) {
            $sql .= "R_Photo = '" . encode_strings($pic['0']['0'], $db) . "', 
                    R_Mobile_Photo = '" . encode_strings($pic['0']['1'], $db) . "',";
        }
        $pic_id = $pic['1'];
    } else {
        $pic_id = $_POST['image_bank_desktop'];
        // last @param 4 = Region Category Main Image
        $pic_response = Upload_Pic_Library($pic_id, 30);
        if ($pic_response) {
            $sql .= "R_Photo = '" . encode_strings($pic_response['0'], $db) . "',
                    R_Mobile_Photo = '" . encode_strings($pic_response['1'], $db) . "',";
        }
    }

    if (strlen($_REQUEST['route_video']) > 5) {
        $video_url = $_REQUEST['route_video'];
    } else {
        $video_url = '';
    }
    $sql .=" R_Video = '" . encode_strings($video_url, $db) . "',
            R_Name = '" . encode_strings($_REQUEST['route_name'], $db) . "',
            R_Link = '" . encode_strings($_REQUEST['route_link'], $db) . "',
            R_Alt_Tag = '" . encode_strings($_REQUEST['route_alt'], $db) . "',
            R_Slider_Title = '" . encode_strings($_REQUEST['slider_title'], $db) . "',
            R_Slider_Description = '" . encode_strings($_REQUEST['slider_description'], $db) . "',
            R_SEO_Title = '" . encode_strings($_REQUEST['seo_title'], $db) . "',
            R_SEO_Keywords  = '" . encode_strings($_REQUEST['seo_keywords'], $db) . "',
            R_SEO_Description  = '" . encode_strings($_REQUEST['seo_description'], $db) . "',
            R_Description  = '" . encode_strings($_REQUEST['route_description'], $db) . "',
            R_RID = '" . encode_strings($_REQUEST['rid'], $db) . "'";
    if ($_POST['id'] > 0) {
        $routeID = $_POST['id'];
        $sql = "UPDATE " . $sql . " WHERE R_ID = '" . encode_strings($_POST['id'], $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        // TRACK DATA ENTRY
        $id = $_REQUEST['rid'];
        Track_Data_Entry('Websites', $id, 'Manage Routes', $routeID, 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $routeID = mysql_insert_id();
        // TRACK DATA ENTRY
        $id = $_REQUEST['rid'];
        Track_Data_Entry('Websites', $id, 'Manage Routes', $routeID, 'Add', 'super admin');
    }
    if ($result) {
        $_SESSION['success'] = 1;
        //Image usage from image bank.
        if ($pic_id > 0) {
            imageBankUsage($pic_id, 'IBU_Route', $routeID);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    header('location: routes-main.php?rid=' . $_REQUEST['rid']);
    exit();
}
if (isset($_GET['op']) && $_GET['op'] == 'del') {
    $update = "UPDATE tbl_Route SET R_Photo = '', R_Mobile_Photo='' WHERE R_RID = '" . encode_strings($_REQUEST['rid'], $db) . "' AND R_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['delete'] = 1;
        imageBankUsageDelete('IBU_Route', $_REQUEST['id']);  
        // TRACK DATA ENTRY
        $id = $_REQUEST['rid'];
        Track_Data_Entry('Websites', $id, 'Manage Routes', '', 'Delete Photo', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header('location: routes-main.php?rid=' . $_REQUEST['rid']);
    exit;
}
//Get Active Region Information
$sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);
//Get Routes details
$sqlRoute = "SELECT R_ID, R_Name, R_Link, R_Description, R_Photo, R_RID, R_Photo, R_Slider_Title, R_Slider_Description, R_Alt_Tag, R_Video, R_SEO_Title, 
             R_SEO_Keywords, R_SEO_Description FROM tbl_Route WHERE R_RID = '" . encode_strings($activeRegion['R_ID'], $db) . "'";
$resRoute = mysql_query($sqlRoute, $db) or die("Invalid query: $sqlRoute -- " . mysql_error());
$activeRoute = mysql_fetch_assoc($resRoute);

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Manage Routes</div>
        <div class="link"><a href="routes.php?rid=<?php echo $regionID ?>">Individual Routes</a></div>
        <div class="link"><a href="routes-categories.php?rid=<?php echo $regionID ?>">Route Categories</a></div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <form id="routeForm" name="routeForm" method="post" enctype="multipart/form-data" action="">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="id" value="<?php echo $activeRoute['R_ID'] ?>">
            <input type="hidden" name="rid" value="<?php echo $activeRegion['R_ID'] ?>">
            <div class="content-header">
              Manage Routes
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Routes Name</label>
                <div class="form-data">
                    <input name="route_name" type="text" value="<?php echo isset($activeRoute['R_Name']) ? $activeRoute['R_Name'] : '' ?>" size="50" required />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Routes Link</label>
                <div class="form-data">
                    <input name="route_link" type="text" value="<?php echo isset($activeRoute['R_Link']) ? $activeRoute['R_Link'] : '' ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Primary Photo</label>
                <div class="form-data div_image_library cat-region-width">
                    <?php if (isset($activeRoute['R_Photo']) && $activeRoute['R_Photo'] != '') { ?>
                        <a class="deletePhoto delete-region-cat-photo" onClick="return confirm('Are you sure you want to delete photo?');" href="routes-main.php?rid=<?php echo $activeRoute['R_RID'] ?>&id=<?php echo $activeRoute['R_ID'] ?>&op=del">Delete Photo</a>
                    <?php } ?>
                    <div class="cropit-image-preview aboutus-photo-perview">
                        <img class="preview-img preview-img-script30" style="display: none;" src="">    
                        <?php if (isset($activeRoute['R_Photo']) && $activeRoute['R_Photo'] != '') { ?>
                            <img class="existing-img existing_imgs30" src="<?php echo IMG_LOC_REL . $activeRoute['R_Photo'] ?>" >
                        <?php } ?>
                    </div>
                    <span class="daily_browse" onclick="show_image_library(30)">Select File</span>
                    <input type="hidden" name="image_bank_desktop" id="image_bank30" value="">
                    <input type="file" onchange="show_file_name(30, this)" name="pic[]" id="photo30" style="display: none;">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Slider Title</label>
                <div class="form-data">
                    <input name="slider_title" type="text"  value="<?php echo isset($activeRoute['R_Slider_Title']) ? $activeRoute['R_Slider_Title'] : '' ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Slider Description</label>
                <div class="form-data"> 
                    <textarea name="slider_description" cols="85" rows="10" class="tt-description"><?php echo isset($activeRoute['R_Slider_Description']) ? $activeRoute['R_Slider_Description'] : '' ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Alt Tag</label>
                <div class="form-data">
                    <input name="route_alt" type="text" value="<?php echo isset($activeRoute['R_Alt_Tag']) ? $activeRoute['R_Alt_Tag'] : '' ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Video</label>
                <div class="form-data">
                    <input type="text" name="route_video" value="<?php echo isset($activeRoute['R_Video']) ? $activeRoute['R_Video'] : '' ?>">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>SEO Title</label>
                <div class="form-data">
                    <input name="seo_title" type="text" value="<?php echo isset($activeRoute['R_SEO_Title']) ? $activeRoute['R_SEO_Title'] : '' ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>SEO Keywords</label>
                <div class="form-data">
                    <input name="seo_keywords" type="text" value="<?php echo isset($activeRoute['R_SEO_Keywords']) ? $activeRoute['R_SEO_Keywords'] : '' ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>SEO Description</label>
                <div class="form-data">
                    <textarea name="seo_description" cols="50"><?php echo isset($activeRoute['R_SEO_Description']) ? $activeRoute['R_SEO_Description'] : '' ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Description</label>
                <div class="form-data">
                    <textarea name="route_description" cols="85" rows="10" class="tt-description"><?php echo isset($activeRoute['R_Description']) ? $activeRoute['R_Description'] : '' ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin border-none"> 
                <div class="button">
                    <input type="submit" name="button2" value="Submit" />
                </div>
            </div>
        </form>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>