<?PHP
require_once '../include/config.inc.php';
require_once '../include/track-data-entry.php';

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
  $IR_ID = $_REQUEST['ir_id'];
  $R_ID = $_REQUEST['rid'];
  $sqlKML = "SELECT IR_KML_Path FROM tbl_Individual_Route WHERE IR_ID ='" . $IR_ID . "'";
  $resKML = mysql_query($sqlKML);
  $KML = mysql_fetch_assoc($resKML);
  if ($KML['IR_KML_Path'] != "") {
    unlink(MAP_LOC_ABS . $KML['IR_KML_Path']);
  }
  $sql = "UPDATE tbl_Individual_Route SET IR_KML_Path = NULL WHERE IR_ID = '" . $IR_ID . "'";
  $result = mysql_query($sql);
  if ($result) {
    $_SESSION['delete'] = 1;
    // TRACK DATA ENTRY
    Track_Data_Entry('Websites', $R_ID, 'Manage Individual Route', $IR_ID, 'Delete KML', 'super admin');
  } else {
    $_SESSION['delete_error'] = 1;
  }
  header("Location: route.php?rid=" . $R_ID . "&id=" . $IR_ID);
  exit();
}
?>