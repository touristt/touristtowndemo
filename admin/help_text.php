<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';


if (isset($_POST['op']) && $_POST['op'] == 'save') {
    if (is_array($_POST['help_text'])) {
        foreach ($_POST['help_text'] as $key => $val) {
            $sql = "UPDATE tbl_Help_Text SET HT_Description = '" . encode_strings($val, $db) . "' WHERE HT_ID = '$key'";
            $result = mysql_query($sql, $db);
        }
    }
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Support', '', 'Manage Help Text', '', 'Update', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: help_text.php");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Help Text</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?php require_once('../include/nav-home-sub.php'); ?>
    </div>
    <div class="right">
        <form name="form1" method="post" action="">
            <input type="hidden" name="op" value="save">
            <?php
            $query = "SELECT HT_Name, HT_ID, HT_Description FROM tbl_Help_Text";
            $result = mysql_query($query, $db) or die("Invalid query: $query -- " . mysql_error());
            while ($row = mysql_fetch_array($result)) {
                ?>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label><?php echo $row['HT_Name']; ?></label>
                    <div class="form-data">
                        <textarea name="help_text[<?php echo$row['HT_ID']?>]" cols="85" rows="10" class="tt-description"><?php echo $row['HT_Description'] ?></textarea>
                    </div>
                </div>
            <?php } ?>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="submit" value="Save" />
                </div>
            </div>
            <script>
                $( document ).ready( function() {
                    CKEDITOR.config.width = 430;
                });
            </script>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>