<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('manage-users', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Chamber of Commerce</div>
        <div class="addlisting"><a href="cc-info.php">+Add Chamber</a></div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?php require_once('../include/nav-home-sub.php'); ?>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name-users">Chamber Name</div>
            <div class="data-column padding-none spl-other-email">Email Address</div>
            <div class="data-column padding-none spl-other-cc-list">Users</div>
            <div class="data-column padding-none spl-other-cc-list">Edit</div>
            <div class="data-column padding-none spl-other-cc-list">Delete</div>
        </div>
        <?PHP
        $sql = "SELECT * FROM tbl_Buying_Group ORDER BY BG_Name  ";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            $bg = ($bg == '#E6E6E6' ? '#FFFFFF' : '#E6E6E6');
            ?>
            <div class="data-content">
                <div class="data-column spl-name-users"><?php echo $row['BG_Name'] ?></div>
                <div class="data-column spl-other-email"><?php echo $row['BG_Email'] ?></div>
                <div class="data-column spl-other-cc-list"><a href="cc-users.php?bg_id=<?php echo $row['BG_ID'] ?>">Users</a></div>
                <div class="data-column spl-other-cc-list"><a href="cc-info.php?id=<?php echo $row['BG_ID'] ?>">Edit</a></div>
                <div class="data-column spl-other-cc-list"><a onClick="return confirm('Are you sure?\nThis action CANNOT be undone!');" href="cc-info.php?id=<?php echo $row['BG_ID'] ?>&amp;op=del">Delete</a></div>
            </div>
            <?PHP
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>