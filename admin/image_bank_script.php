<?php
require_once '../include/config.inc.php';
ini_set('max_execution_time', '3600');
ini_set('max_input_time', '3600');

//$getsizes = "SELECT IBS_Sizes, IBS_ID FROM tbl_Image_Bank_Sizes WHERE IBS_ID = 49 ORDER BY IBS_ID ASC";
$sizes = mysql_query($getsizes);
while ($image_sizes = mysql_fetch_array($sizes)) {
    $heightWidth = explode("X", $image_sizes['IBS_Sizes']);
    //GET ALL IMAGES TO BE RESIZED - GALLERY IMAGES - 1,2,3,4
//    $sqlImages = " SELECT IB_ID, IB_Path, BFP_ID, BFP_Photo_710X440 FROM tbl_Business_Feature_Photo INNER JOIN tbl_Image_Bank_Usage ON BFP_ID = IBU_Photo_Gallery
//                   LEFT JOIN tbl_Image_Bank ON IBU_IB_ID = IB_ID ORDER BY BFP_ID ASC LIMIT 2000, 500";
    //GET ALL IMAGES TO BE RESIZED - ABOUT MAIN PHOTO - 5,8
//    $sqlImages = " SELECT IB_ID, IB_Path, BFAMP_ID, BFAMP_Photo_545X335 FROM tbl_Business_Feature_About_Main_Photo INNER JOIN tbl_Image_Bank_Usage ON BFAMP_BL_ID = IBU_BL_ID
//                   LEFT JOIN tbl_Image_Bank ON IBU_IB_ID = IB_ID WHERE IBU_About_Photo = 'About Us Main Image' ORDER BY BFAMP_ID ASC";
    //GET ALL IMAGES TO BE RESIZED - HOMEPAGE HEADER - 9,10
//    $sqlImages = " SELECT IB_ID, IB_Path, BFAP_ID, BFAP_Photo_470X320 FROM tbl_Business_Feature_About_Photo INNER JOIN tbl_Image_Bank_Usage ON BFAP_ID = IBU_About_Photo
//                   LEFT JOIN tbl_Image_Bank ON IBU_IB_ID = IB_ID ORDER BY BFAP_ID ASC";
    //GET ALL IMAGES TO BE RESIZED - REGION CATEGORY - 11,12,13
//    $sqlImages = " SELECT IB_ID, IB_Path, RC_C_ID, RC_R_ID, RC_Mobile_Thumbnail FROM tbl_Region_Category INNER JOIN tbl_Image_Bank_Usage ON RC_C_ID = IBU_Region_Category AND RC_R_ID = IBU_R_ID
//                   LEFT JOIN tbl_Image_Bank ON IBU_IB_ID = IB_ID ORDER BY RC_C_ID ASC";
    //GET ALL IMAGES TO BE RESIZED - HOMEPAGE HEADER - 17, 39
//    $sqlImages = " SELECT IB_ID, IB_Path, RTP_ID, RTP_Photo FROM tbl_Region_Themes_Photos INNER JOIN tbl_Image_Bank_Usage ON RTP_ID = IBU_Region_Theme
//                   LEFT JOIN tbl_Image_Bank ON IBU_IB_ID = IB_ID ORDER BY RTP_ID ASC";
    //GET ALL IMAGES TO BE RESIZED - CATEGORY ICON - 21
//    $sqlImages = " SELECT IB_ID, IB_Path, C_ID, C_Icon FROM tbl_Category INNER JOIN tbl_Image_Bank_Usage ON C_ID = IBU_C_ID
//                   LEFT JOIN tbl_Image_Bank ON IBU_IB_ID = IB_ID ORDER BY C_ID ASC";
    //GET ALL IMAGES TO BE RESIZED - Product - 25
//    $sqlImages = " SELECT IB_ID, IB_Path, BFP_ID, BFP_Photo FROM tbl_Business_Feature_Product INNER JOIN tbl_Image_Bank_Usage ON BFP_ID = IBU_Our_Product
//                   LEFT JOIN tbl_Image_Bank ON IBU_IB_ID = IB_ID ORDER BY BFP_ID ASC";
    //GET ALL IMAGES TO BE RESIZED - DAILY SPECIALS - 26
//    $sqlImages = " SELECT IB_ID, IB_Path, BFDS_ID, BFDS_Photo FROM tbl_Business_Feature_Daily_Specials INNER JOIN tbl_Image_Bank_Usage ON BFDS_ID = IBU_Daily_Features
//                   LEFT JOIN tbl_Image_Bank ON IBU_IB_ID = IB_ID ORDER BY BFDS_ID ASC";
    //GET ALL IMAGES TO BE RESIZED - ENTERTAINEMENT - 27
//    $sqlImages = " SELECT IB_ID, IB_Path, BFEA_ID, BFEA_Photo FROM tbl_Business_Feature_Entertainment_Acts INNER JOIN tbl_Image_Bank_Usage ON BFEA_ID = IBU_Entertainment
//                   LEFT JOIN tbl_Image_Bank ON IBU_IB_ID = IB_ID ORDER BY BFEA_ID ASC";
    //GET ALL IMAGES TO BE RESIZED - LISTING THUMBNAIL - 28
//    $sqlImages = " SELECT IB_ID, IB_Path, BL_ID, BL_Photo FROM tbl_Business_Listing INNER JOIN tbl_Image_Bank_Usage ON BL_ID = IBU_BL_ID
//                   LEFT JOIN tbl_Image_Bank ON IBU_IB_ID = IB_ID WHERE BL_Photo != '' AND IBU_Main_Photo = 'Listing Thumbnail Image' ORDER BY BL_ID ASC";
    //GET ALL IMAGES TO BE RESIZED - LISTING MAIN - 29, 41
//    $sqlImages = " SELECT IB_ID, IB_Path, BL_ID, BL_Header_Image FROM tbl_Business_Listing INNER JOIN tbl_Image_Bank_Usage ON BL_ID = IBU_BL_ID
//                   LEFT JOIN tbl_Image_Bank ON IBU_IB_ID = IB_ID WHERE IBU_Main_Photo = 'Listing Main Image' ORDER BY BL_ID ASC";
    //GET ALL IMAGES TO BE RESIZED - Story - 43, 45
//    $sqlImages = " SELECT IB_ID, IB_Path, S_ID FROM tbl_Story INNER JOIN tbl_Image_Bank_Usage ON S_ID = IBU_S_ID
//                   LEFT JOIN tbl_Image_Bank ON IBU_IB_ID = IB_ID ORDER BY S_ID ASC";
    //GET ALL IMAGES TO BE RESIZED - Story Content - 44
//    $sqlImages = " SELECT IB_ID, IB_Path, CP_ID FROM tbl_Content_Piece INNER JOIN tbl_Image_Bank_Usage ON CP_ID = IBU_CP_ID
//                   LEFT JOIN tbl_Image_Bank ON IBU_IB_ID = IB_ID ORDER BY CP_ID ASC";
    $resultImages = mysql_query($sqlImages);
    while ($rowImages = mysql_fetch_array($resultImages)) {
        $scale_mylink = mt_rand(1000, 9999999);
        $file = IMG_BANK_ABS . $rowImages['IB_Path'];
        $img = new Imagick($file);
        $img->setImageResolution(72, 72);
        $img->resampleImage(72, 72, imagick::FILTER_UNDEFINED, 1);
        $d = $img->getImageGeometry();
        $sourceHeight = $d['height'];
        $sourceWidth = $d['width'];
        $targetHeight = $heightWidth[1];
        $targetWidth = $heightWidth[0];
        if ($image_sizes['IBS_ID'] == 3) {
            if ($sourceWidth > $sourceHeight) {
                $img->scaleImage($targetWidth, 0);
            } else {
                $img->scaleImage(0, $targetHeight);
            }
        } else {
            $sourceRatio = $sourceWidth / $sourceHeight;
            $targetRatio = $targetWidth / $targetHeight;
            if ($sourceRatio < $targetRatio) {
                $scale = $sourceWidth / $targetWidth;
            } else {
                $scale = $sourceHeight / $targetHeight;
            }
            $resizeWidth = ($sourceWidth / $scale);
            $resizeHeight = ($sourceHeight / $scale);
            $cropLeft = (($resizeWidth - $targetWidth) / 2);
            $cropTop = (($resizeHeight - $targetHeight) / 2);
            $img->scaleImage($resizeWidth, $resizeHeight);
            $img->cropImage($targetWidth, $targetHeight, $cropLeft, $cropTop);
        }
        $filename = $scale_mylink . $targetWidth . "X" . $targetHeight . $rowImages['IB_Path'];
        $imagepath = "image_resize/" . $filename;
        $ext = explode(".", $rowImages['IB_Path']);
        $keys = array_keys($ext);
        $last = end($keys);
        if (strtolower($ext[$last]) == 'jpg' || strtolower($ext[$last]) == 'jpeg') {
            header('Content-type: image/jpeg');
            $img->writeImage($imagepath);
        }
        if (strtolower($ext[$last]) == 'gif') {
            header('Content-type: image/gif');
            $img->writeImage($imagepath);
        }
        if (strtolower($ext[$last]) == 'png') {
            header('Content-type: image/png');
            $img->writeImage($imagepath);
        }
//        if (is_file(IMG_LOC_ABS . $rowImages['RC_Mobile_Thumbnail'])) { //Delete_Pic(IMG_LOC_ABS . $Region_theme['RC_Image_Icon']);
//            unlink(IMG_LOC_ABS . $rowImages['RC_Mobile_Thumbnail']);
//        }
        //UPDATE TABLE IMAGE BANK MULTIPLE IMAGES
//        $IMG_BNK_MULTIPLE = "INSERT tbl_Image_Bank_Multiple_Images SET IBMI_Path = '" . $filename . "', IBMI_IBS_ID = '" . $image_sizes['IBS_ID'] . "', 
//                             IBMI_IB_ID = '" . $rowImages['IB_ID'] . "'";
//        mysql_query($IMG_BNK_MULTIPLE) or die("Invalid query: $IMG_BNK_MULTIPLE -- " . mysql_error());
//        //UPDATE TABLE IMAGE BANK MULTIPLE IMAGES
//        $IMG_BNK_MULTIPLE = "UPDATE tbl_Image_Bank_Multiple_Images SET IBMI_Path = '" . $filename . "' WHERE IBMI_IBS_ID = '" . $image_sizes['IBS_ID'] . "' 
//                            AND IBMI_IB_ID = '" . $rowImages['IB_ID'] . "'";
//        mysql_query($IMG_BNK_MULTIPLE) or die("Invalid query: $IMG_BNK_MULTIPLE -- " . mysql_error());
        //UPDATE TABLE  - GALLERY
//        if ($image_sizes['IBS_ID'] == 1) {
//            $table = "UPDATE tbl_Business_Feature_Photo SET BFP_Photo_710X440 = '" . $filename . "' WHERE BFP_ID = '" . $rowImages['BFP_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
//        if ($image_sizes['IBS_ID'] == 2) {
//            $table = "UPDATE tbl_Business_Feature_Photo SET BFP_Photo_195X195 = '" . $filename . "' WHERE BFP_ID = '" . $rowImages['BFP_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
//        if ($image_sizes['IBS_ID'] == 3) {
//            $table = "UPDATE tbl_Business_Feature_Photo SET BFP_Photo_1000X600 = '" . $filename . "' WHERE BFP_ID = '" . $rowImages['BFP_ID'] . "'";
//            $table = "UPDATE tbl_Business_Feature_Photo SET BFP_Photo_285X210 = '" . $filename . "' WHERE BFP_ID = '" . $rowImages['BFP_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
//        if ($image_sizes['IBS_ID'] == 4) {
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
        //UPDATE TABLE  - ABOUT MAIN PHOTO
//        if ($image_sizes['IBS_ID'] == 5) {
//            $table = "UPDATE tbl_Business_Feature_About_Main_Photo SET BFAMP_Photo_710X440 = '" . $filename . "' WHERE BFAMP_ID = '" . $rowImages['BFAMP_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
//        if ($image_sizes['IBS_ID'] == 8) {
//            $table = "UPDATE tbl_Business_Feature_About_Main_Photo SET BFAMP_Photo_545X335 = '" . $filename . "' WHERE BFAMP_ID = '" . $rowImages['BFAMP_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
        //UPDATE TABLE  - ABOUT PHOTO
//        if ($image_sizes['IBS_ID'] == 9) {
//            $table = "UPDATE tbl_Business_Feature_About_Photo SET BFAP_Photo_285X210 = '" . $filename . "' WHERE BFAP_ID = '" . $rowImages['BFAP_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
//        if ($image_sizes['IBS_ID'] == 10) {
//            $table = "UPDATE tbl_Business_Feature_About_Photo SET BFAP_Photo_470X320 = '" . $filename . "' WHERE BFAP_ID = '" . $rowImages['BFAP_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
        //UPDATE TABLE  - REGION CATEGORY
//        if ($image_sizes['IBS_ID'] == 11) {
//            $table = "UPDATE tbl_Region_Category SET RC_Image = '" . $filename . "' WHERE RC_C_ID = '" . $rowImages['RC_C_ID'] . "' AND RC_R_ID = '" . $rowImages['RC_R_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
//        if ($image_sizes['IBS_ID'] == 12) {
//            $table = "UPDATE tbl_Region_Category SET RC_Thumbnail = '" . $filename . "' WHERE RC_C_ID = '" . $rowImages['RC_C_ID'] . "' AND RC_R_ID = '" . $rowImages['RC_R_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
//        if ($image_sizes['IBS_ID'] == 13) {
//            $table = "UPDATE tbl_Region_Category SET RC_Mobile_Thumbnail = '" . $filename . "' WHERE RC_C_ID = '" . $rowImages['RC_C_ID'] . "' AND RC_R_ID = '" . $rowImages['RC_R_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
        //UPDATE TABLE  - HOMEPAGE HEADER
//        if ($image_sizes['IBS_ID'] == 17) {
//            $table = "UPDATE tbl_Region_Themes_Photos SET RTP_Photo = '" . $filename . "' WHERE RTP_ID = '" . $rowImages['RTP_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
        //UPDATE TABLE  - CATEGORY ICON
//        if ($image_sizes['IBS_ID'] == 21) {
//            $table = "UPDATE tbl_Category SET C_Icon = '" . $filename . "' WHERE C_ID = '" . $rowImages['C_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
//        if ($image_sizes['IBS_ID'] == 49) {
//            $table = "UPDATE tbl_Category SET C_Icon_Mobile = '" . $filename . "' WHERE C_ID = '" . $rowImages['C_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
        //UPDATE TABLE  - PRODUCT
//        if ($image_sizes['IBS_ID'] == 25) {
//            $table = "UPDATE tbl_Business_Feature_Product SET BFP_Photo = '" . $filename . "' WHERE BFP_ID = '" . $rowImages['BFP_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
        //UPDATE TABLE  - DAILY SPECIALS
//        if ($image_sizes['IBS_ID'] == 26) {
//            $table = "UPDATE tbl_Business_Feature_Daily_Specials SET BFDS_Photo = '" . $filename . "' WHERE BFDS_ID = '" . $rowImages['BFDS_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
        //UPDATE TABLE  - ENTERTAINMENT
//        if ($image_sizes['IBS_ID'] == 27) {
//            $table = "UPDATE tbl_Business_Feature_Entertainment_Acts SET BFEA_Photo = '" . $filename . "' WHERE BFEA_ID = '" . $rowImages['BFEA_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
        //UPDATE TABLE  - LISTING THUMBNAIL
//        if ($image_sizes['IBS_ID'] == 46) {
//            $table = "UPDATE tbl_Business_Listing SET BL_Mobile_Photo = '" . $filename . "' WHERE BL_ID = '" . $rowImages['BL_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
        //UPDATE TABLE  - LISTING MAIN
//        if ($image_sizes['IBS_ID'] == 29) {
//            $table = "UPDATE tbl_Business_Listing SET BL_Header_Image = '" . $filename . "' WHERE BL_ID = '" . $rowImages['BL_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
        //UPDATE TABLE  - REGION CATEGORY
//        if ($image_sizes['IBS_ID'] == 40) {
//            $table = "UPDATE tbl_Region_Themes_Photos SET RTP_Mobile_Photo = '" . $filename . "' WHERE RTP_ID = '" . $rowImages['RTP_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
        //UPDATE TABLE  - LISTING MAIN
//        if ($image_sizes['IBS_ID'] == 41) {
//            $table = "UPDATE tbl_Business_Listing SET BL_Mobile_Header_Image = '" . $filename . "' WHERE BL_ID = '" . $rowImages['BL_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
        //UPDATE TABLE  - GALLERY
//        if ($image_sizes['IBS_ID'] == 42) {
//            $table = "UPDATE tbl_Business_Feature_Photo SET BFP_Photo_710X440_Mobile = '" . $filename . "' WHERE BFP_ID = '" . $rowImages['BFP_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
        //UPDATE TABLE  - STORY
//        if ($image_sizes['IBS_ID'] == 43) {
//            $table = "UPDATE tbl_Story SET S_Thumbnail_Mobile = '" . $filename . "' WHERE S_ID = '" . $rowImages['S_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
        //UPDATE TABLE  - CONTENT PIECE
//        if ($image_sizes['IBS_ID'] == 44) {
//            $table = "UPDATE tbl_Content_Piece SET CP_Photo_Mobile = '" . $filename . "' WHERE CP_ID = '" . $rowImages['CP_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
        //UPDATE TABLE  - STORY
//        if ($image_sizes['IBS_ID'] == 45) {
//            $table = "UPDATE tbl_Story SET S_Feature_Image = '" . $filename . "' WHERE S_ID = '" . $rowImages['S_ID'] . "'";
//            mysql_query($table) or die("Invalid query: $table -- " . mysql_error());
//        }
    }
}
?>