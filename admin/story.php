<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if (!in_array('manage-stories', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
$S_ID = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
$regions_for_analytics = array();
$where = "";
$regionDeleteWhere = "";
$checked = "";
$community_class = ""; //for hiding community option when bruce county
//if county admin
$story_direction = $_REQUEST['coupons'];
$story_add_directions = $_REQUEST['addStory'];
if (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
    $regionDeleteWhere = 'AND SR_R_ID IN (' . $_SESSION['USER_LIMIT'] . ')';
    $storyCheck = mysql_query("SELECT S_ID FROM tbl_Story LEFT JOIN tbl_Story_Region ON S_ID = SR_S_ID WHERE SR_R_ID = '" . encode_strings($_SESSION['USER_LIMIT'], $db) . "'");
    $storyArray = array();
    while ($storyRow = mysql_fetch_array($storyCheck)) {
        $storyArray[] = $storyRow['S_ID'];
    }
    if ($S_ID > 0 && !in_array($S_ID, $storyArray)) {
        header("location:stories.php");
    }
    $where = ' AND R_ID = ' . $_SESSION['USER_LIMIT'];
    if ($S_ID == 0) {
        $checked = '1';
    }
    $community_class = "display: none;";
}
$childRegion = array();
if ($S_ID > 0) {
    $sql = "SELECT * FROM tbl_Story WHERE S_ID = '" . encode_strings($S_ID, $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowStory = mysql_fetch_assoc($result);
    $sql = "SELECT * FROM tbl_Story_Region WHERE SR_S_ID = '" . encode_strings($S_ID, $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($rowRegions = mysql_fetch_assoc($result)) {
        $childRegion[] = $rowRegions['SR_R_ID'];
    }
    $sqlSS = "SELECT * FROM tbl_Story_Season WHERE SS_S_ID = '" . encode_strings($S_ID, $db) . "'";
    $resultSS = mysql_query($sqlSS, $db) or die("Invalid query: $sqlSS -- " . mysql_error());
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'story') {
//echo '<pre>'; print_r($_POST); exit;
    //ob_start();
    $sql = "tbl_Story SET 
            S_Title = '" . encode_strings($_REQUEST['title'], $db) . "', 
            S_Keyword = '" . encode_strings($_REQUEST['keyword'], $db) . "',
            S_Date = '" . encode_strings(($_REQUEST['date'] != "") ? date('Y-m-d', strtotime($_REQUEST['date'])) : "0000-00-00", $db) . "',
            S_Category = '" . encode_strings($_REQUEST['category'], $db) . "', 
            S_Author = '" . encode_strings($_REQUEST['author'], $db) . "',
            S_Description = '" . encode_strings($_REQUEST['description'], $db) . "',
            S_SEO_Title = '" . encode_strings($_REQUEST['seoTitle'], $db) . "', 
            S_SEO_Keywords = '" . encode_strings($_REQUEST['seoKeywords'], $db) . "', 
            S_SEO_Description = '" . encode_strings($_REQUEST['seoDescription'], $db) . "'";

    $regions = '';
    $i = 1;
    $count = count($_REQUEST['childRegion']);
    foreach ($_REQUEST['childRegion'] as $child) {
        // SELECTING REGIONS
        if ($i != $count) {
            $regions .= $child . ',';
        } else {
            $regions .= $child;
        }
        $i++;
    }
    $seasons = $_POST['season'];

    foreach ($seasons as $value) {
        // SELECTING JUST ONE SEASON AS MORE THAN ONE ARE NOT ALLOWED IN IMAGE DETAIL
        $season = $value;
        break;
    }
    if ($season == '') {
        $season = 0;
    }

    require '../include/picUpload.inc.php';
    if ($_POST['image_bank_story'] == "") {
        // last @param 4 = Region Category Main Image
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 27);
        if (is_array($pic)) {
            $sql .= ", S_Thumbnail = '" . encode_strings($pic['0']['0'], $db) . "',
                       S_Thumbnail_Mobile = '" . encode_strings($pic['0']['1'], $db) . "',
                       S_Feature_Image = '" . encode_strings($pic['0']['2'], $db) . "'";
            $pic_id = $pic['1'];
            // INSERTING REGION, CATEGORY, AND SEASAON AGAINST THE IMAGE ABOUT THIS STORY
            $IMG_BNK = "UPDATE tbl_Image_Bank SET IB_Region = '" . $regions . "', IB_Category = 121, IB_Season = '" . $season . "' WHERE IB_ID = '" . $pic_id . "' ";
            $result = mysql_query($IMG_BNK, $db) or die("Invalid query: $sql -- " . mysql_error());
        }
    } else {
        $pic_id = $_POST['image_bank_story'];
        // last @param 4 = Region Category Main Image
        $pic_response = Upload_Pic_Library($pic_id, 27);
        if ($pic_response) {
            $sql .= ", S_Thumbnail = '" . encode_strings($pic_response['0'], $db) . "',
                       S_Thumbnail_Mobile = '" . encode_strings($pic_response['1'], $db) . "',
                       S_Feature_Image = '" . encode_strings($pic_response['2'], $db) . "'";
        }
    }

    if ($rowStory['S_ID'] > 0) {
        $id = $S_ID;
        $sql = "UPDATE " . $sql . " WHERE S_ID = '" . $S_ID . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        // TRACK DATA ENTRY
        //Track_Data_Entry('Stories', '', 'Create Story/Story Details', $S_ID, 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql . ", S_Active = '" . encode_strings('1', $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        // Track_Data_Entry('Stories', '', 'Create Story/Story Details', $id, 'Add', 'super admin');
    }
    //insert regions against stories
    $sql = "DELETE FROM tbl_Story_Region WHERE SR_S_ID = '$id' $regionDeleteWhere";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    foreach ($_REQUEST['childRegion'] as $child) {
        $sql = "tbl_Story_Region SET SR_S_ID = '$id', SR_R_ID = '$child'";
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    }

    //insert seasons against stories
    $delete = "DELETE FROM tbl_Story_Season WHERE SS_S_ID = '" . encode_strings($id, $db) . "'";
    mysql_query($delete, $db) or die("Invalid query: $delete -- " . mysql_error());
    foreach ($_POST['season'] as $value) {
        $sql = "INSERT tbl_Story_Season SET SS_Season = '$value', 
                SS_S_ID = '" . encode_strings($id, $db) . "'";
        mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    }

    if ($result) {
        $_SESSION['success'] = 1;
        if ($pic_id > 0) {
            //Image usage from image bank.
            imageBankUsage($pic_id, 'IBU_S_ID', $id, '', '');
        }
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: story.php?id=" . $id);
    exit();
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {

    $sql = "DELETE tbl_Story, tbl_Story_Region, tbl_Story_Homepage_Category, tbl_Story_Season, tbl_Content_Piece, tbl_Image_Bank_Usage FROM tbl_Story
            LEFT JOIN tbl_Story_Region ON S_ID = SR_S_ID
            LEFT JOIN tbl_Story_Homepage_Category ON S_ID = SHC_S_ID
            LEFT JOIN tbl_Story_Season ON S_ID = SS_S_ID
            LEFT JOIN tbl_Content_Piece ON S_ID = CP_S_ID
            LEFT JOIN tbl_Image_Bank_Usage ON (CP_ID = IBU_CP_ID OR S_ID = IBU_S_ID) WHERE S_ID = '" . $S_ID . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Stories', '', 'Manage Stories', $S_ID, 'Delete Story', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }

    header("Location: stories.php");
    exit();
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del_photo') {
    require_once '../include/picUpload.inc.php';
    $update = "UPDATE tbl_Story SET";
    if ($_REQUEST['flag'] == 'hp') {
        $update .= " S_Thumbnail = '', S_Thumbnail_Mobile = '', S_Feature_Image = ''";
        if ($rowStory['S_Thumbnail']) {
            Delete_Pic(IMG_ICON_ABS . $rowStory['S_Thumbnail']);
            Delete_Pic(IMG_ICON_ABS . $rowStory['S_Thumbnail_Mobile']);
            Delete_Pic(IMG_ICON_ABS . $rowStory['S_Feature_Image']);
        }
    }
    $update .= " WHERE S_ID = '" . encode_strings($S_ID, $db) . "'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header('location: story.php?id=' . $rowStory['S_ID']);
    exit;
}

//  DELETE PDF FILE
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'delPdf') {
    $select_pdf = "SELECT CP_Pdf FROM tbl_Content_Piece WHERE CP_ID = " . $_REQUEST['delete'] . " ";
    $result_pdf = mysql_query($select_pdf);
    $pdf_row = mysql_fetch_assoc($result_pdf);
    if ($pdf_row['CP_Pdf'] != "") {
        unlink(PDF_LOC_ABS . $pdf_row['CP_Pdf']);
    }
    $sql = "UPDATE tbl_Content_Piece SET CP_Pdf = '' WHERE CP_ID = '" . $_REQUEST['delete'] . "'";
    $result = mysql_query($sql);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['delete'];
        Track_Data_Entry('Stories', '', 'Create Story/Story Details', $id, 'Delete PDF', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location:/admin/story.php?id=" . $_REQUEST['id']);
    exit;
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'content_add') {
    $sqlMax = "SELECT MAX(CP_Order) FROM tbl_Content_Piece WHERE CP_S_ID = '$S_ID'";
    $resultMax = mysql_query($sqlMax);
    $rowMax = mysql_fetch_row($resultMax);

    $sql = "tbl_Content_Piece SET 
            CP_S_ID = '" . encode_strings($S_ID, $db) . "', 
            CP_Title = '" . encode_strings($_REQUEST['content_title'], $db) . "', 
            CP_Video_Link = '" . encode_strings($_REQUEST['video'], $db) . "', 
            CP_Description = '" . encode_strings($_REQUEST['content_description'], $db) . "',
            CP_Pdf_Title   ='" . encode_strings($_REQUEST['pdfTitle'], $db) . "', 
            CP_Order = '" . ($rowMax[0] + 1) . "'";
    require '../include/picUpload.inc.php';
    $file_size = $_FILES['file']['size'];
    $max_filesize = 5342523;
    $pdf_name = str_replace(' ', '_', $_FILES['file']['name']);

    if ($pdf_name != "") {
        $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_name;
        $filePath = PDF_LOC_ABS . $random_name;
        if ($_FILES["file"]["type"] == "application/pdf") {
            if ($file_size < $max_filesize) {
                move_uploaded_file($_FILES["file"]["tmp_name"], $filePath);
            }
        }
    }
    if ($_POST['image_bank'] == "") {
        // last @param 26 = Story Content
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 26);
        if (is_array($pic)) {
            $sql .= ", CP_Photo = '" . encode_strings($pic['0']['0'], $db) . "', 
                       CP_Photo_Mobile = '" . encode_strings($pic['0']['1'], $db) . "'";
            $pic_id = $pic['1'];
        }
    } else {
        $pic_id = $_POST['image_bank'];
        // last @param 26 = Story Content
        $pic = Upload_Pic_Library($pic_id, 26);
        if (is_array($pic)) {
            $sql .= ", CP_Photo = '" . encode_strings($pic['0'], $db) . "', 
                       CP_Photo_Mobile = '" . encode_strings($pic['1'], $db) . "'";
        }
    }
    if ($random_name !== '') {
        $sql .= ", CP_Pdf = '" . encode_strings($random_name, $db) . "'";
    }
    $sql = "INSERT " . $sql;
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $id = mysql_insert_id();
    // TRACK DATA ENTRY
    Track_Data_Entry('Stories', '', 'Create Story/Story Details', $id, 'Add Story Content', 'super admin');

    if ($result) {
        $_SESSION['success'] = 1;
        if ($pic_id > 0) {
            //Image usage from image bank.
            imageBankUsage($pic_id, 'IBU_CP_ID', $id, '', '');
        }
    } else {
        $_SESSION['error'] = 1;
    }
    if (isset($story_direction) && $story_direction == 'editstory') {
        header("Location: stories-preview.php?id=" . $S_ID);
        exit();
    } else {
        header("Location: story.php?id=" . $S_ID);
        exit();
    }
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'content_update') {
    $acc = $_REQUEST['acc'];
    $sql = "tbl_Content_Piece SET  
            CP_Title = '" . encode_strings($_REQUEST['content_title'], $db) . "', 
            CP_Video_Link = '" . encode_strings($_REQUEST['video'], $db) . "',
            CP_Description = '" . encode_strings($_REQUEST['content_description'], $db) . "',
            CP_Pdf_Title   ='" . encode_strings($_REQUEST['pdfTitle'], $db) . "'";
    require '../include/picUpload.inc.php';
    $file_size = $_FILES['file']['size'];
    $max_filesize = 5342523;
    $pdf_name = str_replace(' ', '_', $_FILES['file']['name']);
    if ($pdf_name != "") {
        $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_name;
        $filePath = PDF_LOC_ABS . $random_name;
        if ($_FILES["file"]["type"] == "application/pdf") {
            if ($file_size < $max_filesize) {
                move_uploaded_file($_FILES["file"]["tmp_name"], $filePath);
            }
        }
    }

    if ($_POST['image_bank'] == "") {
        // last @param 26 = Story Content
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 26);
        if (is_array($pic)) {
            $sql .= ", CP_Photo = '" . encode_strings($pic['0']['0'], $db) . "',
                       CP_Photo_Mobile = '" . encode_strings($pic['0']['1'], $db) . "'";
            $pic_id = $pic['1'];
        }
    } else {
        $pic_id = $_POST['image_bank'];
        // last @param 26 = Story Content
        $pic = Upload_Pic_Library($pic_id, 26);
        if (is_array($pic)) {
            $sql .= ", CP_Photo = '" . encode_strings($pic['0'], $db) . "',
                       CP_Photo_Mobile = '" . encode_strings($pic['1'], $db) . "'";
        }
    }

    if ($pdf_name != '') {
        $sql .= ", CP_Pdf = '" . encode_strings($random_name, $db) . "'";
    }
    $sql = "UPDATE " . $sql . " WHERE CP_ID = '" . encode_strings($_REQUEST['cp_id'], $db) . "'";
    $result = mysql_query($sql, $db);
    // TRACK DATA ENTRY
    $id = $_REQUEST['cp_id'];
    Track_Data_Entry('Stories', '', 'Create Story/Story Details', $id, 'Update Story Content', 'super admin');

    if ($result) {
        $_SESSION['success'] = 1;
        if ($pic_id > 0) {
            //Image usage from image bank.
            imageBankUsage($pic_id, 'IBU_CP_ID', $_REQUEST['cp_id'], '', '');
        }
    } else {
        $_SESSION['error'] = 1;
    }
    if (isset($story_direction) && $story_direction == 'editstory') {
        header("Location: stories-preview.php?id=" . $S_ID);
        exit();
    } else {

        header("Location: story.php?id=" . $S_ID . "&acc=" . $acc);
        exit();
    }
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del_content_photo') {
    $acc = $_REQUEST['acc'];
    require_once '../include/picUpload.inc.php';
    $update = "UPDATE tbl_Content_Piece SET";

    $update .= " CP_Photo = '', CP_Photo_Mobile = ''";
    if ($rowStory['CP_Photo']) {
        Delete_Pic(IMG_ICON_ABS . $rowStory['CP_Photo']);
        Delete_Pic(IMG_ICON_ABS . $rowStory['CP_Photo_Mobile']);
    }

    $update .= " WHERE CP_ID = '" . encode_strings($_REQUEST['cp_id'], $db) . "'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
//    header('location: story.php?id=' . $rowStory['S_ID']);
    header("Location: story.php?id=" . $S_ID . "&acc=" . $acc);
    exit;
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'content_delete') {

    $sql = "DELETE tbl_Content_Piece FROM tbl_Content_Piece WHERE CP_ID = '" . $_REQUEST['cp_id'] . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_CP_ID', $_REQUEST['cp_id'], '', '');
        // TRACK DATA ENTRY
        $id = $_REQUEST['cp_id'];
        Track_Data_Entry('Stories', '', 'Create Story/Story Details', $id, 'Delete Story Content', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    if (isset($story_direction) && $story_direction == 'editstory') {
        header("Location: stories-preview.php?id=" . $S_ID);
        exit();
    } else {

        header("Location: story.php?id=" . $S_ID);
        exit();
    }
}

$activeAccordion = '';
if (isset($_REQUEST['acc']) && $_REQUEST['acc'] != '') {
    $activeAccordion = $_REQUEST['acc'];
} else {
    $activeAccordion = 'false';
}

require_once '../include/admin/header.php';
?>
<div id="autocomplete_data" style="display: none;"><?php include '../include/autocomplete_story.php' ?></div>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Create Story</div>
        <?php if ($S_ID > 0) { ?>
            <div class="link">
                <a href="#scroll" id="add-content">+Add Content</a>
            </div>
        <?php } ?>
    </div>
    <div class="right full-width" <?php if (isset($_REQUEST['Count']) && $_REQUEST['Count'] == 'countsVal') { ?> style="margin-top:10px !important;" <?php } ?>>
        <!--Story Detail-->
        <form name="form" method="post" action="" enctype="multipart/form-data">
            <input type="hidden" name="op" value="story">
            <input type="hidden" name="id" value="<?php echo $S_ID ?>">
            <input type="hidden" name="coupons" value="<?php echo $story_direction; ?>">
            <input type="hidden" id="StoryAdd" name="StoryAdd" value="<?php echo $story_add_directions; ?>">
            <div class="content-header full-width " >Story Details</div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Title</label>
                <div class="form-data">
                    <input name="title" type="text" value="<?php echo (isset($rowStory['S_Title']) ? $rowStory['S_Title'] : '') ?>" size="40" required/>
                </div>
            </div>

            <div class="form-inside-div image-bank-autocomplete form-inside-div-width-admin full-width">
                <label>Keywords</label>
                <div class="form-data">
                    <input id="autocomplete" type="text"/>
                    <input type="hidden" id="keywords-searched" name="keyword" value="<?php echo (isset($rowStory['S_Keyword']) && $rowStory['S_Keyword'] != "") ? $rowStory['S_Keyword'] : "" ?>">
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Date</label>
                <div class="form-data">
                    <input class="datepicker" name="date" type="text" value="<?php echo $rowStory['S_Date'] ?>" size="50"  required/>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Category</label>
                <div class="form-data">
                    <select name="category" required>
                        <option value="">Select Category</option>
                        <?php
                        $sql = "SELECT C_ID FROM tbl_Category WHERE C_Is_Blog = 1";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        $rowExplore = mysql_fetch_array($result);
                        $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '" . $rowExplore['C_ID'] . "' ORDER BY C_ID ASC";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($rowExploreSub = mysql_fetch_array($result)) {
                            ?>
                            <option value="<?php echo $rowExploreSub['C_ID'] ?>" <?php echo (isset($rowStory['S_Category']) && $rowExploreSub['C_ID'] == $rowStory['S_Category']) ? 'selected' : ''; ?>>
                                <?php echo $rowExploreSub['C_Name']; ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Author</label>
                <div class="form-data">
                    <input name="author" type="text" value="<?php echo (isset($rowStory['S_Author']) ? $rowStory['S_Author'] : '') ?>" size="40" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width"> 
                <label>Description</label>
                <div class="form-data">
                    <textarea name="description" class="tt-description"><?php echo $rowStory['S_Description']; ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width" style="<?php echo $community_class ?>"> 
                <label>Select Community</label>
                <div class="form-data">
                    <div id="childRegion"> 
                        <?PHP
                        $currentRType = 0;
                        $RTypesArray = array(
                            1 => 'Parent Region',
                            2 => 'Community',
                            3 => 'Organization',
                            4 => 'County',
                            7 => 'Specialty'
                        );
                        $sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_Type IN(1,2,3,4,7) AND R_Type > 0 $where ORDER BY R_Type, R_Name";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_assoc($result)) {
                            if ($currentRType != $row['R_Type']) {
                                $currentRType = $row['R_Type'];
                                echo '<div class="regionTypeTitle">' . $RTypesArray[$currentRType] . '</div>';
                            }
                            echo '<div class="childRegionCheckbox"><input type="checkbox" name="childRegion[]" value="' . $row['R_ID'] . '"';
                            echo (in_array($row['R_ID'], $childRegion) || $checked == '1') ? 'checked' : '';
                            echo '>' . $row['R_Name'] . "</div>";

                            if (in_array($row['R_ID'], $childRegion)) {
                                $regions_for_analytics[] = $row['R_ID'];
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>SEO Title </label>
                <div class="form-data">
                    <input name="seoTitle" type="text" value="<?php echo (isset($rowStory['S_SEO_Title']) ? $rowStory['S_SEO_Title'] : '') ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width"> 
                <label>SEO Description </label>
                <div class="form-data">
                    <input name="seoDescription" type="text" value="<?php echo (isset($rowStory['S_SEO_Description']) ? $rowStory['S_SEO_Description'] : '') ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width"> 
                <label>SEO Keywords</label>
                <div class="form-data">
                    <input name="seoKeywords" type="text" value="<?php echo (isset($rowStory['S_SEO_Keywords']) ? $rowStory['S_SEO_Keywords'] : '') ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width"> 
                <label>Seasons</label>
                <div class="form-data">
                    <div id="childRegion"> 
                        <?php
                        $StoriesSeasons = array();
                        while ($rowSS = mysql_fetch_array($resultSS)) {
                            $StoriesSeasons[] = $rowSS['SS_Season'];
                        }
                        $sql = "SELECT * FROM tbl_Seasons";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_array($result)) {
                            ?>
                            <div class="childRegionCheckbox">
                                <input name="season[<?php echo $row['S_ID'] ?>]" type="checkbox" value="<?php echo $row['S_ID'] ?>" <?php echo in_array($row['S_ID'], $StoriesSeasons) ? 'checked' : '' ?> /><?php echo $row['S_Name'] ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Photo</label>
                <div class="form-inside-div div_image_library about-us-cropit border-none">
                    <span onchange="show_file_name(27, this)" onclick="show_image_library(27)">Select File</span>
                    <input type="file" style="display :none;" name="pic[]" onChange="show_file_name(27, this);" id="photo27">
                    <input type="hidden" name="image_bank_story" id="image_bank27" value="">
                    <div class="cropit-image-preview aboutus-photo-perview">
                        <img class="preview-img preview-img-script27" id="container" style="display: none;" src="" id="chk">    
                        <?php if (isset($rowStory['S_Thumbnail']) && $rowStory['S_Thumbnail'] != '') { ?>
                            <input type="hidden" id="update_27" value="1"/>
                            <img class="existing-img existing_imgs27" src="<?php echo IMG_LOC_REL . $rowStory['S_Thumbnail'] ?>" >
                        <?php } ?>
                    </div>
                    <?php if ($rowStory['S_Thumbnail'] != '') { ?>
                        <a class="deletePhoto delete-region-cat-photo" style="margin-top: 10px;" onclick="return confirm('Are you sure you want to delete photo?');" href="story.php?id=<?php echo $rowStory['S_ID'] ?>&flag=hp&op=del_photo&">Delete Photo</a>
                    <?php } ?>
                </div>
            </div>

            <script type="text/javascript">

            </script>

            <div class="form-inside-div form-inside-div-width-admin full-width"> 
                <div class="button">
                    <input type="submit" name="button" value="Submit" />
                </div>
            </div>
        </form>

        <!--Add story content-->
        <form name="form1" method="post" action="#scroll" enctype="multipart/form-data" id="add-content-form" style="display: none">
            <input type="hidden" name="op" value="content_add">
            <input type="hidden" name="id" value="<?php echo $_REQUEST['id']; ?>">
            <input type="hidden" name="image_bank" class="image_bank" id="image_bank26" value="">
            <div class="content-header full-width" id="scroll">Content Details</div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Title</label>
                <div class="form-data">
                    <input name="content_title" type="text" size="40" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Photo</label>
                <div class="form-inside-div div_image_library about-us-cropit border-none">
                    <span class="daily_browse" onclick="show_image_library(26)">Select File</span>
                    <input type="file" style="display :none;" name="pic[]" onChange="show_file_name(26, this);" id="photo26">
                    <div class="form-inside-div add-about-us-item-image margin-none">
                        <div class="cropit-image-preview aboutus-photo-perview">
                            <img class="preview-img preview-img-script26" style="display: none;" src="">    
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>PDF Title</label>
                <div class="form-data">
                    <input name="pdfTitle" type="text" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width"> 
                <label>Upload PDF</label>
                <div class="form-data image-bank-browse image_bank_width_browse">
                    <div class="inputWrapper adv-photo float-left">Browse
                        <input id="imageInput filename"  class="setting-name fileInput file_ext0 required_title0" accept="application/pdf"  type="file" name="file"  onchange="show_file_names(this.files[0].name, 1)" multiple>
                    </div>
                    <input style="display: none;" id="uploadFile" class="uploadFileName" disabled>

                    <div class="margin-left-resize add_event_view">
                        <?php
                        if (isset($rowContent['CP_Pdf']) && $rowContent['CP_Pdf'] != '') {
                            ?>
                            <a target="_blank" class="pdf-color-btn" href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $rowContent['CP_Pdf'] ?>">
                                View
                            </a> 
                        </div>
                        <div class="margin-left-resize add_event_view">
                            <a class="delete-pointer pdf-color-btn"  onClick="return confirm('Are you sure this action can not be undone!');" href="story.php?id=<?php echo $_REQUEST['id'] ?>&delete=<?php echo $rowContent['CP_ID'] ?>&op=delPdf">Delete</a> 
                        <?php }
                        ?>
                    </div>
                </div> 
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Video Link</label>
                <div class="form-data">
                    <input name="video" type="text" size="50" /> 
                </div>
            </div>
            <!--  -->
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Description</label>
                <div class="form-data">
                    <textarea name="content_description" cols="85" class="tt-description" rows="10" id="description-listing"></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width"> 
                <div class="button">
                    <input type="submit" name="button" value="Save Content" />
                </div>
            </div>
        </form>

        <!--Content Record-->
        <?php if ($S_ID > 0) { ?>
            <div id="accordion" class="story">
                <?PHP
                $sql = "SELECT * FROM tbl_Content_Piece WHERE CP_S_ID = " . $S_ID . " ORDER BY CP_Order ASC";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                $acco = 0;
                $scroll = 0;
                while ($rowContent = mysql_fetch_array($result)) {
                    ?>
                    <div class="group" id="recordsArray_<?php echo $rowContent['CP_ID']; ?>"> 
                        <h3 class="accordion-rows">
                            <span id="scrolls_<?php echo $rowContent['CP_ID']; ?>" class="accordion-title"><?php echo $rowContent['CP_Title']; ?></span>
                            <label class="add-item-accordion"><a onclick="event.stopPropagation()" href="story.php?op=content_delete&cp_id=<?php echo $rowContent['CP_ID'] ?>&id=<?php echo $S_ID ?>">Delete Content</a></label>
                        </h3> 
                        <form name="form2" method="post" action="#recordsArray_<?php echo $rowContent['CP_ID']; ?>" enctype="multipart/form-data" class="story-content-update-form">
                            <input type="hidden" name="cp_id" value="<?php echo $rowContent['CP_ID'] ?>">
                            <input type="hidden" name="id" value="<?php echo $S_ID ?>">
                            <input type="hidden" name="op" value="content_update">
                            <input type="hidden" name="image_bank" class="image_bank" id="image_bank<?php echo $rowContent['CP_ID'] ?>" value="">
                            <input type="hidden" name="acc" value="<?php echo $acco; ?>">
                            <div class="form-inside-div form-inside-div-width-admin full-width">
                                <label>Title</label>
                                <div class="form-data">
                                    <input name="content_title" type="text" value="<?php echo $rowContent['CP_Title'] ?>"/>
                                </div>
                            </div>
                            <div class="form-inside-div form-inside-div-width-admin full-width">
                                <label>Photo</label>
                                <div class="form-inside-div div_image_library about-us-cropit border-none">
                                    <span class="daily_browse" onclick="show_image_library(26, <?php echo $rowContent['CP_ID']; ?>)">Select File</span>
                                    <input type="file" style="display :none;" name="pic[]" onChange="show_file_name(26, this, <?php echo $rowContent['CP_ID']; ?>);" id="photo<?php echo $rowContent['CP_ID']; ?>">
                                    <div class="cropit-image-preview aboutus-photo-perview">
                                        <img class="preview-img preview-img-script<?php echo $rowContent['CP_ID']; ?>" style="display: none;" src="">    
                                        <?php if ($rowContent['CP_Photo'] != '') { ?>
                                            <img class="existing-img existing_imgs<?php echo $rowContent['CP_ID']; ?>" src="<?php echo IMG_LOC_REL . $rowContent['CP_Photo'] ?>" >
                                        <?php } ?>
                                    </div>
                                    <?php if ($rowContent['CP_Photo'] != '') { ?>
                                        <a class="deletePhoto delete-region-cat-photo" style="margin-top: 10px;" onclick="return confirm('Are you sure you want to delete photo?');"href="story.php?op=del_content_photo&acc=<?php echo $acco ?>&cp_id=<?php echo $rowContent['CP_ID'] ?>&id=<?php echo $S_ID ?>">Delete Photo</a>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-inside-div form-inside-div-width-admin full-width">
                                <label>PDF Title</label>
                                <div class="form-data">
                                    <input name="pdfTitle" type="text" value="<?php echo (isset($rowContent['CP_Pdf_Title']) ? $rowContent['CP_Pdf_Title'] : '') ?>" size="50" /> 
                                </div>
                            </div>
                            <div class="form-inside-div form-inside-div-width-admin full-width"> 
                                <label>Upload PDF</label>
                                <div class="form-data image-bank-browse image_bank_width_browse">
                                    <div class="inputWrapper adv-photo float-left">Browse
                                        <input id="imageInput filename"  class="setting-name fileInput file_ext0 required_title0" accept="application/pdf"  type="file" name="file"  onchange="show_file_names(this.files[0].name, 1)" multiple>
                                    </div>
                                    <input style="display: none;" id="uploadFile" class="uploadFileName" disabled>

                                    <div class="margin-left-resize add_event_view">
                                        <?php
                                        if (isset($rowContent['CP_Pdf']) && $rowContent['CP_Pdf'] != '') {
                                            ?>
                                            <a target="_blank" class="pdf-color-btn"  href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $rowContent['CP_Pdf'] ?>">
                                                View
                                            </a> 
                                        </div>
                                        <div class="margin-left-resize add_event_view">
                                            <a class="delete-pointer pdf-color-btn"  onClick="return confirm('Are you sure this action can not be undone!');" href="story.php?id=<?php echo $_REQUEST['id'] ?>&delete=<?php echo $rowContent['CP_ID'] ?>&op=delPdf">Delete</a> 
                                        <?php }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                            <div class="form-inside-div form-inside-div-width-admin full-width">
                                <label>Video Link</label>
                                <div class="form-data">
                                    <input name="video" type="text" value="<?php echo (isset($rowContent['CP_Video_Link']) ? $rowContent['CP_Video_Link'] : '') ?>" size="50" /> 
                                </div>
                            </div>
                            <div class="form-inside-div form-inside-div-width-admin full-width">
                                <label>Description</label>
                                <div class="form-data">
                                    <textarea name="content_description" id="description_<?php echo $rowContent['CP_ID'] ?>" class="ckeditor"><?php echo $rowContent['CP_Description']; ?></textarea>
                                </div>
                            </div>
                            <div class="form-inside-div form-inside-div-width-admin full-width">
                                <div class="button">
                                    <input type="submit" name="button" value="Save Content" />
                                </div>
                            </div>
                        </form>
                        <script>

                            CKEDITOR.disableAutoInline = true;
                            $(document).ready(function () {
                                var CP_ID = <?php echo $rowContent['CP_ID']; ?>;
                                if (CKEDITOR.instances['description_' + CP_ID]) {
                                    delete CKEDITOR.instances['description_' + CP_ID];
                                    $('#description_' + CP_ID).ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
                                }
                                $('#description_' + CP_ID).ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
                                setTimeout(function () {
                                    CKEDITOR.replace('textarea');
                                }, 1000);
                            });
                        </script>  
                    </div>    
                    <?PHP
                    $acco++;
                    $scroll++;
                }
                ?>
            </div>
        <?php } ?>
    </div>
    <div id="dialog-message"></div>
    <div id="image-library" style="display:none;"></div>
    <input id="image-library-usage" type="hidden" value="">

    <div class="right full-width story-stats">
        <div class="content-header  full-width">Statistics</div>
        <div class="menu-items-accodings"> 
            <div id="accordion1">
                <?PHP
                // $regionID = 1;
                $region_tracking_ids = array(
                    1 => 'UA-32156009-4',
                    3 => 'UA-32156009-3',
                    17 => 'UA-32156009-11'
                );
                $regions = array();
                foreach ($regions_for_analytics as $key => $regionID) {
                    $sql = "SELECT R_Type, R_Tracking_ID, R_Domain, R_Domain_Alternates, R_Name FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    $activeRegion = mysql_fetch_assoc($result);
                    // $tracking_id='UA-32156009-4';
                    $tracking_id = $region_tracking_ids[$regionID];
                    $r_name = $activeRegion['R_Name'];
                    $R_Domain = $activeRegion['R_Domain'] . '/explore/' . clean($rowStory['S_Title']) . '/' . $S_ID;
                    $R_Domain_Alternates = $activeRegion['R_Domain_Alternates'];

                    $analytics = getService();
                    $profile = getprofileId($analytics, $regionID, $tracking_id, $R_Domain, $R_Domain_Alternates, $r_name);
                }

                function printResultsPages(&$results, &$r, &$r_name) {
                    $rows = $results->getRows();
                    $yearCurrent = date('Y');
                    $currentIteration = '';
                    $firstIteration = true;
                    foreach (array_reverse($rows) as $monthrecord) {
                        if ($currentIteration != $monthrecord[0]) {
                            //start accordion
                            if ($firstIteration) {
                                $firstIteration = false;
                            } else {
                                print '<div class="content-sub-header footer-width padding-none" id="footer-width">
                                <div class="data-column">Total</div>
                                                <div class="data-column">' . $users_total . '</div>
                                                <div class="data-column">' . $viewed_total . '</div>
                                                <div class="data-column">' . $sessions_total . '</div>
                                                <div class="data-column">' . $unique_total . '</div>
                            </div>';
                                print '</div>';  //end the previous accordion
                                $viewed_total = 0;
                                $sessions_total = 0;
                                $unique_total = 0;
                                $users_total = 0;
                            }
                            //start new accordion
                            if (!in_array($r_name, $regions)) {
                                print '<div class="content-sub-header padding-none">                    
                                    <div class="data-column" style="text-align: left;margin-bottom:5px;">' . $r_name . '</div>
                                </div><br>';
                            }
                            print '<h3 class="accordion-rows"><span class="accordion-title">' . $monthrecord[0] . '</span></h3>';
                            print '<div class="sub-accordions accordion-padding">';

                            //print headers
                            print '<div class="content-sub-header padding-none">                    
                                    <div class="data-column">Month</div>
                                    <div class="data-column">Users</div>
                                    <div class="data-column">Viewed</div>
                                    <div class="data-column">Sessions</div>
                                    <div class="data-column">Unique</div>
                                </div>';

                            $currentIteration = $monthrecord[0];
                        }
                        $viewed_total += $monthrecord[2];
                        $sessions_total += $monthrecord[3];
                        $unique_total += $monthrecord[4];
                        $users_total += $monthrecord[5];

                        $viewed += $monthrecord[2];
                        $sessions += $monthrecord[3];
                        $unique += $monthrecord[4];
                        $users += $monthrecord[5];
                        ?>
                        <div class="data-content">  

                            <div class="data-column"><?php print date('M', strtotime($monthrecord[0] . '-' . $monthrecord[1] . '-' . '01')); ?></div>

                            <div class="data-column"><?php print $monthrecord[5]; ?></div>
                            <div class="data-column"><?php print $monthrecord[2]; ?></div>
                            <div class="data-column"><?php print $monthrecord[3]; ?></div>
                            <div class="data-column"><?php print $monthrecord[4]; ?></div>
                        </div>

                        <?php
                        $regions[] = $r_name;
                    }
                    print '<div class="content-sub-header footer-width padding-none" id="footer-width">
                                <div class="data-column">Total</div>
                                <div class="data-column">' . $users_total . '</div>
                                <div class="data-column">' . $viewed_total . '</div>
                                <div class="data-column">' . $sessions_total . '</div>
                                <div class="data-column">' . $unique_total . '</div>
                            </div>';
                    // print '</div>'; 
                    ?>                          
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <?PHP
    require_once '../include/admin/footer.php';

    function clean($string) {
        $string = strtolower(str_replace(' ', '-', $string)); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    function getService() {
        // Creates and returns the Analytics service object.
        // Load the Google API PHP Client Library.
        require_once '../include/google-api-php-client-master/src/Google/autoload.php';

        // Use the developers console and replace the values with your
        // service account email, and relative location of your key file.
        $service_account_email = 'analytics@tourist-town.iam.gserviceaccount.com';
        $key_file_location = 'Tourist Town-2a6f3c92156a.p12';

        // Create and configure a new client object.
        $client = new Google_Client();
        $client->setApplicationName("HelloAnalytics");
        $analytics = new Google_Service_Analytics($client);

        // Read the generated client_secrets.p12 key.
        $key = file_get_contents($key_file_location);
        $cred = new Google_Auth_AssertionCredentials(
                $service_account_email, array(Google_Service_Analytics::ANALYTICS_READONLY), $key
        );
        $client->setAssertionCredentials($cred);
        if ($client->getAuth()->isAccessTokenExpired()) {
            $client->getAuth()->refreshTokenWithAssertion($cred);
        }
        return $analytics;
    }

    function getprofileId(&$analytics, &$regionID, &$tracking_id, &$R_Domain, &$R_Domain_Alternates, &$r_name) {
        $accounts = $analytics->management_accounts->listManagementAccounts();
        if (count($accounts->getItems()) > 0) {
            $items = $accounts->getItems();
            foreach ($items as $item) {
                $firstAccountId = $item->getId();
                $webproperties = $analytics->management_webproperties->listManagementWebproperties($firstAccountId);
                if (count($webproperties->getItems()) > 0) {
                    $items_webproperties = $webproperties->getItems();
                    foreach ($items_webproperties as $items_webproperty) {
                        $firstWebpropertyId = $items_webproperty->getId();
                        $profiles = $analytics->management_profiles->listManagementProfiles($firstAccountId, $firstWebpropertyId);
                        if (count($profiles->getItems()) > 0) {
                            $items_profiles = $profiles->getItems();
                            foreach ($items_profiles as $items_profile) {
                                $profileId = $items_profile->getId();
                                $profileweb = $items_profile->getwebPropertyId();
                                if ($tracking_id == $profileweb) {
                                    if (isset($profileId)) {
                                        $startdate = date('2016-08-01');
                                        $enddate = date('Y-m-d');
                                        $filters = array('filters' => 'ga:hostname=~' . $R_Domain . '|' . $R_Domain_Alternates, 'dimensions' => 'ga:year,ga:month', 'max-results' => '800000');
                                        $results_pages = getResultsGA($analytics, $profileId, $startdate, $enddate, 'ga:pageviews,ga:sessions,ga:uniquePageviews,ga:Users', $filters);
                                        printResultsPages($results_pages, $regionID, $r_name);
                                    }
                                }
                            }
                        } else {
                            throw new Exception('No views (profiles) found for this user.');
                        }
                    }
                } else {
                    throw new Exception('No webproperties found for this user.');
                }
            }
        } else {
            throw new Exception('No accounts found for this user.');
        }
    }

    function getResultsGA(&$analytics, $profileId, $startdate, $enddate, $matrics, $filters) {
        return $analytics->data_ga->get(
                        'ga:' . $profileId, $startdate, $enddate, $matrics, $filters);
    }
    ?>
    <script>
        function show_file_names(val, count) {
            $(".uploadFileName").show();
            $(".uploadFileName").val(val);
        }
        $(document).ready(function () {
            $("#add-content").click(function () {
                $("#add-content-form").toggle();
            });
            var storyAdd = '<?php echo $_REQUEST['addStory']; ?>';
            if (storyAdd != '' && storyAdd == 'addstories') {
                $("#add-content-form").show();
            }
        });
        $(function () {

            $("#accordion1").accordion({
                collapsible: true,
                heightStyle: 'content',
                active: false,
                header: "> h3"
            });

            $("#accordion")
                    .accordion({
                        collapsible: true,
                        heightStyle: 'content',
                        active: <?php echo $activeAccordion; ?>,
                        header: "> div > h3"
                    })
                    .sortable({
                        axis: "y",
                        handle: "h3",
                        start: function (event, ui)
                        {
                            var id_textarea = ui.item.find(".ckeditor").attr("id");
                            CKEDITOR.instances[id_textarea].destroy();
                        },
                        update: function () {
                            var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Content_Piece&field=CP_Order&id=CP_ID';
                            $.post("reorder.php", order, function (theResponse) {
                                swal("Success", "Stories Re-Ordered Successfully!", "success");
                            });
                        },
                        stop: function (event, ui)
                        {
                            var id_textarea = ui.item.find(".ckeditor").attr("id");
                            CKEDITOR.replace(id_textarea);
                            // IE doesn't register the blur when sorting
                            // so trigger focusout handlers to remove .ui-state-focus
                            ui.item.children("h3").triggerHandler("focusout");

                            // Refresh accordion to handle new order
                            $(this).accordion("refresh");
                        }
                    });
        });


        $(function () {
            var search_text = $('#keywords-searched').val();
            var searched_items = ""
            if (search_text != "") {
                searched_items = search_text.split(",");
                var json = [];
                for (var i = 0; i < searched_items.length; i++) {
                    json.push({"name": searched_items[i]});
                }
            }
            //autocomplete for lisitngs
            var search = $('#autocomplete_data').text();
            var searched_list = "";
            var json_data = [];
            if (search != "") {
                searched_list = search.split("%");
                for (var i = 0; i < searched_list.length - 1; i++) {
                    json_data.push({"name": searched_list[i]});
                }
            }
            $("#autocomplete").tokenInput(json_data, {
                onResult: function (item) {
                    if ($.isEmptyObject(item)) {
                        return [{id: '0', name: $("tester").text()}]
                    } else {
                        item.unshift({"name": $("tester").text()});
                        var lookup = {};
                        var result = [];

                        for (var temp, i = 0; temp = item[i++]; ) {
                            var name = temp.name;

                            if (!(name in lookup)) {
                                lookup[name] = 1;
                                result.push({"name": name});
                            }
                        }
                        return result;
                    }

                },
                onAdd: function (item) {
                    var value = $('#keywords-searched').val();
                    if (value != '') {
                        $('#keywords-searched').val(value + "," + item.name);
                    } else {
                        $('#keywords-searched').val(item.name);
                    }
                },
                onDelete: function (item) {
                    var value = $('#keywords-searched').val().split(",");
                    $('#keywords-searched').empty();
                    var data = "";
                    $.each(value, function (key, value) {
                        if (value != item.name) {
                            if (data != '') {
                                data = data + "," + value;
                            } else {
                                data = value;
                            }
                        }
                    });
                    $('#keywords-searched').val(data);
                },
                resultsLimit: 10,
                prePopulate: json
            }
            );

        });
    </script>
    <style type="text/css">
        .content-wrapper .content .content-left{ overflow: visible;   float: left;
                                                 width: 850px;
                                                 margin-right: 10px;
                                                 background-color: white;
                                                 padding-bottom: 20px;}
        .menu-items-accodings{width: 100% !important; }
    </style>