<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
$table = "";
$column = "";
$nameID = "";
$Order = "";
$id_col = "";
if (isset($_POST['location'])) {
    
    $table = "Events_Location";
    $column = "EL_Region_ID";
    $nameID = "setLocationID";
    $Order = "EL_Name";
    $id_col = "EL_ID";
} else {
    $table = "Events_Organization";
    $column = "EO_Region_ID";
    $nameID = "setOrganizationID";
    $Order = "EO_Name";
    $id_col = "EO_ID";
}
$queryconcat = "";
foreach ($_POST['childRegion'] as $key => $value) {
    if ($key == 0) {
        $queryconcat .= " FIND_IN_SET (" . $value . ",$column)";
    } else {
        $queryconcat .= " OR FIND_IN_SET (" . $value . ",$column)";
    }
}
?>
<select name="<?php echo $nameID?>" id="<?php echo $nameID?>" required>
    <option value="">Select One</option>
    <option value="0">Use Alternate</option>
    <?php
    $sql = "SELECT * FROM $table WHERE $queryconcat ORDER BY $Order";
    $result = mysql_query($sql, $db) or die(mysql_error() . $sql);
    while ($rowTMP = mysql_fetch_assoc($result)) {
        ?>
        <option value="<?php echo $rowTMP[$id_col] ?>"><?php echo $rowTMP[$Order] ?></option>
        <?PHP
    }
    ?>
</select>