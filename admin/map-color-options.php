<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if (isset($_REQUEST['rid']) && $_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} else {
    header("Location: /admin/regions.php");
    exit();
}
$sql = "SELECT R_Type, R_Homepage_Listings, R_Map_Nav_Title, R_Lat, R_Long, R_Zoom, R_Water_Color, R_Landscape_Color, R_Park_Color, 
        R_Administrative_Color, R_Road_Highway_Color, R_Road_Arterial_Color, R_Road_Local_Color, R_Map_Filters_Background_Color, R_Map_Filters_Drop_Down_Background_Color, R_Map_Filters_Text, R_Map_Filters_Drop_Down_Text,R_Map_Return_To_Web_Text,
        R_Map_Logo, R_Logo_Name, R_Map_Header_Image, R_Map_Mobile_Header_Image FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);

$sql = "SELECT M_Description FROM tbl_region_map WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$map_record = mysql_fetch_assoc($result);
$map_rows = mysql_num_rows($result);

$nav_title = $activeRegion['R_Map_Nav_Title'];
$latitude = $activeRegion['R_Lat'];
$longitude = $activeRegion['R_Long'];
$zoom_level = $activeRegion['R_Zoom'];
$water_color = $activeRegion['R_Water_Color'];
$landscape_color = $activeRegion['R_Landscape_Color'];
$park_color = $activeRegion['R_Park_Color'];
//$park_label_color = $activeRegion['R_Park_Label_Color'];
$administrative_color = $activeRegion['R_Administrative_Color'];
$road_highway_color = $activeRegion['R_Road_Highway_Color'];
$road_arterial_color = $activeRegion['R_Road_Arterial_Color'];
$road_local_color = $activeRegion['R_Road_Local_Color'];
$filters_background_color = $activeRegion['R_Map_Filters_Background_Color'];
$filters_text_value = explode("-", $activeRegion['R_Map_Filters_Text']);
$filters_drop_down_background_color = $activeRegion['R_Map_Filters_Drop_Down_Background_Color'];
$filters_drop_down_text_value = explode("-", $activeRegion['R_Map_Filters_Drop_Down_Text']);
$return_to_web_text_value = explode("-", $activeRegion['R_Map_Return_To_Web_Text']);
//$activeRegion['R_Homepage_Listings'];
if (isset($_POST['map_btn'])) {
    $nav_title = $_POST['nav_title'];
    $latitude = $_POST['latitude'];
    $longitude = $_POST['longitude'];
    $zoom_level = $_POST['zoom_level'];
    $water_color = $_POST['map_water_color'];
    $landscape_color = $_POST['map_landscape_color'];
    $park_color = $_POST['map_park_color'];
    $administrative_color = $_POST['map_administrative_color'];
    $road_highway_color = $_POST['map_road_highway_color'];
    $road_arterial_color = $_POST['map_road_arterial_color'];
    $road_local_color = $_POST['map_road_local_color'];
    $filters_background_color = $_POST['map_filters_background_color'];
    $filters_text = $_REQUEST['filters_font'] . "-" . $_REQUEST['filters_size'] . "-" . $_REQUEST['filters_color'];
    
    $filters_drop_down_background_color = $_POST['map_filters_drop_down_background_color'];
    $filters_text_drop_down = $_REQUEST['filters_font_drop_down'] . "-" . $_REQUEST['filters_size_drop_down'] . "-" . $_REQUEST['filters_color_drop_down'];
    $return_to_web_text = $_REQUEST['font_return_to_web'] . "-" . $_REQUEST['return_to_web_size'] . "-" . $_REQUEST['return_to_web_color'];
    
    $map_logo_name = $_POST['desktop_logo_alt'];
    $map_description = $_POST['desktop_logo_alt'];
    $map_zoom_listing = $_POST['listings_zoom_map'];
    $r_map_return_link = $_POST['R_Map_Return_Link'];
   $query = "tbl_Region SET R_Map_Nav_Title = '$nav_title', R_Lat='$latitude', R_Long='$longitude', R_Zoom='$zoom_level', R_Water_Color='$water_color', R_Landscape_Color='$landscape_color', R_Park_Color='$park_color', R_Administrative_Color='$administrative_color', R_Road_Highway_Color='$road_highway_color', R_Road_Arterial_Color='$road_arterial_color', R_Road_Local_Color='$road_local_color',R_Map_Filters_Background_Color='$filters_background_color',R_Map_Filters_Text = '$filters_text',R_Map_Filters_Drop_Down_Background_Color = '$filters_drop_down_background_color', R_Map_Filters_Drop_Down_Text = '$filters_text_drop_down', R_Map_Return_To_Web_Text = '$return_to_web_text', 
                R_Map_Return_Link ='$r_map_return_link',
                R_Logo_Name= '$map_logo_name',R_Map_Listings_Limit='$map_zoom_listing'";
    // image code start//
    /* Images/Icons Uploading Section */
    require_once '../include/picUpload.inc.php';
    $desktop_logo = Upload_Pic_Normal('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true);
    if ($desktop_logo) {
        $query .= ", R_Map_Logo = '" . encode_strings($desktop_logo, $db) . "'";
        if ($activeRegion['R_Map_Logo']) {
            Delete_Pic(IMG_LOC_ABS . $activeRegion['R_Map_Logo']);
        }
    }
    
    if ($_POST['image_bank_map'] == "") {
        // last @param 4 = Region Category Main Image
        $pic = Upload_Pic('0', 'pic1', 0, 0, true, IMG_LOC_ABS, 10000000, true, 34, 'Region', $regionID);
        if (is_array($pic)) {
            $query .= ", R_Map_Header_Image = '" . encode_strings($pic['0']['0'], $db) . "', 
                    R_Map_Mobile_Header_Image = '" . encode_strings($pic['0']['1'], $db) . "'";
        }
        $pic_id = $pic['1'];
    } else {
        $pic_id = $_POST['image_bank_map'];
        // last @param 4 = Region Category Main Image
        $pic_response = Upload_Pic_Library($pic_id, 34);
        if ($pic_response) {
            $query .= ", R_Map_Header_Image = '" . encode_strings($pic_response['0'], $db) . "',
                    R_Map_Mobile_Header_Image = '" . encode_strings($pic_response['1'], $db) . "'";
        }
    }
    
    //image code end//
   $sql = "UPDATE " . $query . " WHERE R_ID = '" . encode_strings($regionID, $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    
    if ($result && $pic_id > 0) {
        //Image usage from image bank.
        imageBankUsage($pic_id, 'IBU_R_ID', $regionID, 'IBU_Map_Header_Image', 1);
    }
    
    if (isset($_POST['op']) && $_POST['op'] == 'save') {
        if ($map_rows > 0) {
            $sql = "UPDATE tbl_region_map SET M_Description = '" . encode_strings(trim(str_replace('<p>', '', str_replace('</p>', '', $_REQUEST['description']))), $db) . "' WHERE R_ID = '" . encode_strings($regionID, $db) . "'";
        } else {
            $description = encode_strings(trim(str_replace('<p>', '', str_replace('</p>', '', $_REQUEST['description']))), $db);
            $regionID = $_POST['rid'];
            $sql = "INSERT into tbl_region_map(M_Description,R_ID) values('$description','$regionID')";
        }
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    }
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $regionID;
        Track_Data_Entry('Websites', $id, 'Manage Map', '', 'Update', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: map-color-options.php?rid=" . $regionID);
    exit();
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    require_once '../include/picUpload.inc.php';
    $update = "UPDATE tbl_Region SET";

    if ($_REQUEST['flag'] == 'desktop_logo') {
        $update .= " R_Map_Logo = ''";
        if ($activeRegion['R_Map_Logo']) {
            Delete_Pic(IMG_LOC_ABS . $activeRegion['R_Map_Logo']);
        }
    }

    $update .= " WHERE R_ID = '" . encode_strings($regionID, $db) . "'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $regionID;
        Track_Data_Entry('Websites', $id, 'Manage Map', '', 'Delete', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header('location: map-color-options.php?rid=' . $_REQUEST['rid']);
    exit;
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del_header_image') {
    require_once '../include/picUpload.inc.php';
    $update = "UPDATE tbl_Region SET";

    if ($_REQUEST['flag'] == 'header_photo') {
        $update .= " R_Map_Header_Image = '', R_Map_Mobile_Header_Image = ''";
        if ($activeRegion['R_Map_Header_Image']) {
            Delete_Pic(IMG_LOC_ABS . $activeRegion['R_Map_Header_Image']);
        }
    }

    $update .= " WHERE R_ID = '" . encode_strings($regionID, $db) . "'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $regionID;
        Track_Data_Entry('Websites', $id, 'Manage Map', '', 'Delete', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header('location: map-color-options.php?rid=' . $_REQUEST['rid']);
    exit;
}

$sql = "SELECT R_Homepage_Listings, R_Map_Nav_Title, R_Map_Listings_Limit, R_Map_Return_Link,  R_Lat, R_Long, R_Zoom, R_Water_Color, R_Landscape_Color, R_Park_Color, R_Park_Label_Color, 
        R_Administrative_Color, R_Road_Highway_Color, R_Road_Arterial_Color, R_Road_Local_Color, R_Map_Filters_Background_Color, R_Map_Filters_Drop_Down_Background_Color, R_Map_Filters_Text, R_Map_Filters_Drop_Down_Text, R_Map_Return_To_Web_Text,
        R_Map_Logo, R_Logo_Name,R_Name, R_Map_Header_Image, R_Map_Mobile_Header_Image FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);
$sql = "SELECT M_Description FROM tbl_region_map WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$map_record = mysql_fetch_assoc($result);

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Manage Map </div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <form method="post"  name="map_form" enctype="multipart/form-data">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <div class="content-header">Manage Map</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Header Photo</label>
                <div class="form-data div_image_library cat-region-width">
                    <?php if ($activeRegion['R_Map_Header_Image'] != '') { ?>
                    <a class="deletePhoto delete-region-cat-photo" onClick="return confirm('Are you sure you want to delete photo?');" href="map-color-options.php?rid=<?php echo $regionID ?>&op=del_header_image&flag=header_photo">Delete Photo</a>
                    <?php } ?>
                    <div class="cropit-image-preview aboutus-photo-perview">
                        <img class="preview-img preview-img-script34" style="display: none;" src="">    
                        <?php if ($activeRegion['R_Map_Header_Image'] != '') { ?>
                            <img class="existing-img existing_imgs34" src="<?php echo IMG_LOC_REL . $activeRegion['R_Map_Header_Image'] ?>" >
                        <?php } ?>
                    </div>
                    <span class="daily_browse" onclick="show_image_library(34)">Select File</span>
                    <input type="hidden" name="image_bank_map" id="image_bank34" value="">
                    <input type="file" onchange="show_file_name(34, this)" name="pic1[]" id="photo34" style="display: none;">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Description</label>
                <div class="form-data">
                    <textarea name="description" cols="85" rows="10" class="tt-description"><?php echo $map_record['M_Description'] ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Map Navigation Title</label>
                <div class="form-data theme-options">
                    <input type="text" name="nav_title" class="homepage-textbox small"  value="<?php echo (isset($nav_title)) ? $nav_title : '' ?>"/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Map Latitude</label>
                <div class="form-data theme-options">
                    <input type="text" name="latitude" class="homepage-textbox small"  value="<?php echo (isset($latitude)) ? $latitude : '' ?>"/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Map Longitude</label>
                <div class="form-data theme-options">
                    <input type="text" name="longitude" class="homepage-textbox small" value="<?php echo (isset($longitude)) ? $longitude : '' ?>" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Map Zoom Level</label>
                <div class="form-data theme-options">
                    <input type="text" name="zoom_level" class="homepage-textbox small"  value="<?php echo (isset($zoom_level)) ? $zoom_level : '' ?>"/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Listings on Map Page</label>
                <div class="form-data"> 
                    <input name="listings_zoom_map" type="text" value="<?php echo (isset($activeRegion['R_Map_Listings_Limit']) ? $activeRegion['R_Map_Listings_Limit'] : '10') ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Return to Website</label>
                <div class="form-data"> 
                    <input name="R_Map_Return_Link" type="text" value="<?php echo (isset($activeRegion['R_Map_Return_Link']) ? $activeRegion['R_Map_Return_Link'] : '') ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Water</label>
                <div class="form-data theme-options">
                    <div class="color-box-inner"><div class="color-box" id="map_water_color" style="background-color:<?php echo (isset($water_color)) ? $water_color : '#ffffff'; ?>"></div></div>
                    <input type="hidden" name="map_water_color" id="text_colormap_water_color" value="<?php echo (isset($water_color)) ? $water_color : ''; ?>" />
                    <?php if (isset($activeRegion['R_Water_Color']) && $activeRegion['R_Water_Color'] !== '') { ?>
                        <a class="margin-left-resize theme_browse_float_view hideVal hideDelete1"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'R_Water_Color'; ?>', 'map_water_color', 'hideDelete1', 'text_colormap_water_color', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                    <?php } ?>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Landscape</label>
                <div class="form-data theme-options">
                    <div class="color-box-inner"><div class="color-box" id="map_landscape_color"  style="background-color:<?php echo (isset($landscape_color)) ? $landscape_color : '#ffffff'; ?>"></div></div>
                    <input type="hidden" name="map_landscape_color" id="text_colormap_landscape_color" value="<?php echo (isset($landscape_color)) ? $landscape_color : ''; ?>" />
                    <?php if (isset($activeRegion['R_Landscape_Color']) && $activeRegion['R_Landscape_Color'] !== '') { ?>
                        <a class="margin-left-resize theme_browse_float_view hideVal hideDelete2"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'R_Landscape_Color'; ?>', 'map_landscape_color', 'hideDelete2', 'text_colormap_landscape_color', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Park</label>
                <div class="form-data theme-options">
                    <div class="color-box-inner"><div class="color-box" id="map_park_color" style="background-color:<?php echo (isset($park_color)) ? $park_color : '#ffffff'; ?>"></div></div>
                    <input type="hidden" name="map_park_color" id="text_colormap_park_color" value="<?php echo (isset($park_color)) ? $park_color : ''; ?>" />
                    <?php if (isset($activeRegion['R_Park_Color']) && $activeRegion['R_Park_Color'] !== '') { ?>
                        <a class="margin-left-resize theme_browse_float_view hideVal hideDelete3"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'R_Park_Color'; ?>', 'map_park_color', 'hideDelete3', 'text_colormap_park_color', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Administrative</label>
                <div class="form-data theme-options">
                    <div class="color-box-inner"><div class="color-box" id="map_administrative_color" style="background-color:<?php echo (isset($administrative_color)) ? $administrative_color : '#ffffff'; ?>"></div></div>
                    <input type="hidden" name="map_administrative_color" id="text_colormap_administrative_color" value="<?php echo (isset($administrative_color)) ? $administrative_color : ''; ?>" />
                    <?php if (isset($activeRegion['R_Administrative_Color']) && $activeRegion['R_Administrative_Color'] !== '') { ?>
                        <a class="margin-left-resize theme_browse_float_view hideVal hideDelete4"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'R_Administrative_Color'; ?>', 'map_administrative_color', 'hideDelete4', 'text_colormap_administrative_color', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Highway Road</label>
                <div class="form-data theme-options">
                    <div class="color-box-inner"><div class="color-box" id="map_road_highway_color" style="background-color:<?php echo (isset($road_highway_color)) ? $road_highway_color : '#ffffff'; ?>"></div></div>
                    <input type="hidden" name="map_road_highway_color" id="text_colormap_road_highway_color" value="<?php echo (isset($road_highway_color)) ? $road_highway_color : ''; ?>" />
                    <?php if (isset($activeRegion['R_Road_Highway_Color']) && $activeRegion['R_Road_Highway_Color'] !== '') { ?>
                        <a class="margin-left-resize theme_browse_float_view hideVal hideDelete5"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'R_Road_Highway_Color'; ?>', 'map_road_highway_color', 'hideDelete5', 'text_colormap_road_highway_color', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Arterial Road</label>
                <div class="form-data theme-options">
                    <div class="color-box-inner"><div class="color-box" id="map_road_arterial_color" style="background-color:<?php echo (isset($road_arterial_color)) ? $road_arterial_color : '#ffffff'; ?>"></div></div>
                    <input type="hidden" name="map_road_arterial_color" id="text_colormap_road_arterial_color" value="<?php echo (isset($road_arterial_color)) ? $road_arterial_color : ''; ?>" />
                    <?php if (isset($activeRegion['R_Road_Arterial_Color']) && $activeRegion['R_Road_Arterial_Color'] !== '') { ?>
                        <a class="margin-left-resize theme_browse_float_view hideVal hideDelete6"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'R_Road_Arterial_Color'; ?>', 'map_road_arterial_color', 'hideDelete6', 'text_colormap_road_arterial_color', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Local Road</label>
                <div class="form-data theme-options">
                    <div class="color-box-inner"><div class="color-box" id="map_road_local_color" style="background-color:<?php echo (isset($road_local_color) && $road_local_color != '') ? $road_local_color : '#ffffff'; ?>"></div></div>
                    <input type="hidden" name="map_road_local_color" id="text_colormap_road_local_color" value="<?php echo (isset($road_local_color)) ? $road_local_color : ''; ?>" />
                    <?php if (isset($activeRegion['R_Road_Local_Color']) && $activeRegion['R_Road_Local_Color'] !== '') { ?>
                        <a class="margin-left-resize theme_browse_float_view hideVal hideDelete7"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'R_Road_Local_Color'; ?>', 'map_road_local_color', 'hideDelete7', 'text_colormap_road_local_color', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                    <?php } ?>
                </div>
            </div>
            <!--  -->
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Filter Bar Color</label>
                <div class="form-data theme-options">
                    <div class="color-box-inner"><div class="color-box" id="map_filters_background_color" style="background-color:<?php echo (isset($filters_background_color)) ? $filters_background_color : '#ffffff'; ?>"></div></div>
                    <input type="hidden" name="map_filters_background_color" id="text_colormap_filters_background_color" value="<?php echo (isset($filters_background_color)) ? $filters_background_color : ''; ?>" />
                    <?php if (isset($activeRegion['R_Map_Filters_Background_Color']) && $activeRegion['R_Map_Filters_Background_Color'] !== '') { ?>
                        <a class="margin-left-resize theme_browse_float_view hideVal hideDelete8"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'R_Map_Filters_Background_Color'; ?>', 'map_filters_background_color', 'hideDelete8', 'text_colormap_filters_background_color', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Filter Drop Down Background</label>
                <div class="form-data theme-options">
                    <div class="color-box-inner"><div class="color-box" id="map_filters_drop_down_background_color" style="background-color:<?php echo (isset($filters_drop_down_background_color)) ? $filters_drop_down_background_color : '#ffffff'; ?>"></div></div>
                    <input type="hidden" name="map_filters_drop_down_background_color" id="text_colormap_filters_drop_down_background_color" value="<?php echo (isset($filters_drop_down_background_color)) ? $filters_drop_down_background_color : ''; ?>" />
                    <?php if (isset($activeRegion['R_Map_Filters_Drop_Down_Background_Color']) && $activeRegion['R_Map_Filters_Drop_Down_Background_Color'] !== '') { ?>
                        <a class="margin-left-resize theme_browse_float_view hideVal hideDelete80"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'R_Map_Filters_Drop_Down_Background_Color'; ?>', 'map_filters_drop_down_background_color', 'hideDelete80', 'text_colormap_filters_drop_down_background_color', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Filter Text</label>
                <div class="form-data theme-options">
                    <select class="theme-options-font" name="filters_font">
                        <option value="">Select Font...</option>
                        <?php
                        $getFonts = "SELECT TOF_Name, TOF_ID FROM tbl_Theme_Options_Fonts ORDER BY TOF_Name";
                        $res_fonts = mysql_query($getFonts);
                        while ($row_fonts = mysql_fetch_assoc($res_fonts)) {
                            ?>
                            <option style="font-family: <?php echo $row_fonts['TOF_Name']; ?>" <?php echo (($filters_text_value[0] == $row_fonts['TOF_ID']) ? "selected" : "") ?> value="<?php echo $row_fonts['TOF_ID'] ?>"><?php echo $row_fonts['TOF_Name'] ?></option>
                        <?php } ?>
                    </select>
                    <input class="theme-options-size" type="number" name="filters_size" size="50" value="<?php echo $filters_text_value[1]; ?>" placeholder="Font size">
                    <div class="color-box-inner theme-options-color"><div class="color-box" id="filters_color" style="background-color: <?php echo (($filters_text_value[2] != "") ? $filters_text_value[2] : '#ffffff') ?>"></div></div>
                    <input type="hidden" name="filters_color" id="text_colorfilters_color" value="<?php echo (($filters_text_value[2] != "") ? $filters_text_value[2] : '') ?>">
                    <?php if (isset($filters_text_value[2]) && $filters_text_value[2] !== '') { ?>
                        <a class="margin-left-resize theme_browse_float_view hideVal hideDelete9"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'R_Map_Filters_Text'; ?>', 'filters_color', 'hideDelete9', 'text_colorfilters_color', '<?php echo $filters_text_value[0]; ?>', '<?php echo $filters_text_value[1]; ?>', '<?php echo $filters_text_value[2]; ?>')">Remove</a>
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Filter Drop Down Text</label>
                <div class="form-data theme-options">
                    <select class="theme-options-font" name="filters_font_drop_down">
                        <option value="">Select Font...</option>
                        <?php
                        $getFonts = "SELECT TOF_Name, TOF_ID FROM tbl_Theme_Options_Fonts ORDER BY TOF_Name";
                        $res_fonts = mysql_query($getFonts);
                        while ($row_fonts = mysql_fetch_assoc($res_fonts)) {
                            ?>
                            <option style="font-family: <?php echo $row_fonts['TOF_Name']; ?>" <?php echo (($filters_drop_down_text_value[0] == $row_fonts['TOF_ID']) ? "selected" : "") ?> value="<?php echo $row_fonts['TOF_ID'] ?>"><?php echo $row_fonts['TOF_Name'] ?></option>
                        <?php } ?>
                    </select>
                    <input class="theme-options-size" type="number" name="filters_size_drop_down" size="50" value="<?php echo $filters_drop_down_text_value[1]; ?>" placeholder="Font size">
                    <div class="color-box-inner theme-options-color"><div class="color-box" id="filters_color_drop_down" style="background-color: <?php echo (($filters_drop_down_text_value[2] != "") ? $filters_drop_down_text_value[2] : '#ffffff') ?>"></div></div>
                    <input type="hidden" name="filters_color_drop_down" id="text_colorfilters_color_drop_down" value="<?php echo (($filters_drop_down_text_value[2] != "") ? $filters_drop_down_text_value[2] : '') ?>">
                    <?php if (isset($filters_drop_down_text_value[2]) && $filters_drop_down_text_value[2] !== '') { ?>
                        <a class="margin-left-resize theme_browse_float_view hideVal hideDelete90"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'R_Map_Filters_Drop_Down_Text'; ?>', 'filters_color_drop_down', 'hideDelete90', 'text_colorfilters_color_drop_down', '<?php echo $filters_drop_down_text_value[0]; ?>', '<?php echo $filters_drop_down_text_value[1]; ?>', '<?php echo $filters_drop_down_text_value[2]; ?>')">Remove</a>
                    <?php } ?>
                </div>
            </div>
            
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Return to Website Text</label>
                <div class="form-data theme-options">
                    <select class="theme-options-font" name="font_return_to_web">
                        <option value="">Select Font...</option>
                        <?php
                        $getFonts = "SELECT TOF_Name, TOF_ID FROM tbl_Theme_Options_Fonts ORDER BY TOF_Name";
                        $res_fonts = mysql_query($getFonts);
                        while ($row_fonts = mysql_fetch_assoc($res_fonts)) {
                            ?>
                            <option style="font-family: <?php echo $row_fonts['TOF_Name']; ?>" <?php echo (($return_to_web_text_value[0] == $row_fonts['TOF_ID']) ? "selected" : "") ?> value="<?php echo $row_fonts['TOF_ID'] ?>"><?php echo $row_fonts['TOF_Name'] ?></option>
                        <?php } ?>
                    </select>
                    <input class="theme-options-size" type="number" name="return_to_web_size" size="50" value="<?php echo $return_to_web_text_value[1]; ?>" placeholder="Font size">
                    <div class="color-box-inner theme-options-color"><div class="color-box" id="return_to_web_color" style="background-color: <?php echo (($return_to_web_text_value[2] != "") ? $return_to_web_text_value[2] : '#ffffff') ?>"></div></div>
                    <input type="hidden" name="return_to_web_color" id="text_colorreturn_to_web_color" value="<?php echo (($return_to_web_text_value[2] != "") ? $return_to_web_text_value[2] : '') ?>">
                    <?php if (isset($return_to_web_text_value[2]) && $return_to_web_text_value[2] !== '') { ?>
                        <a class="margin-left-resize theme_browse_float_view hideVal hideDelete91"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'R_Map_Return_To_Web_Text'; ?>', 'return_to_web_color', 'hideDelete91', 'text_colorreturn_to_web_color', '<?php echo $return_to_web_text_value[0]; ?>', '<?php echo $return_to_web_text_value[1]; ?>', '<?php echo $return_to_web_text_value[2]; ?>')">Remove</a>
                    <?php } ?>
                </div>
            </div>
            
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Map Logo</label>
                <div class="form-data div_image_library cat-region-width">
                    <label for="photo0" class="daily_browse with-no-library">Browse</label>
                    <div class="cropit-image-preview aboutus-photo-perview">
                        <img class="preview-img preview-img-script0" style="display: none;" src="">    
                        <?php if ($activeRegion['R_Map_Logo'] != '') { ?>
                            <img class="existing-img existing_imgs0" src="<?php echo IMG_LOC_REL . $activeRegion['R_Map_Logo'] ?>" >
                        <?php } ?>
                    </div>
                    <input type="file" onchange="show_file_name(0, this)" name="pic[]" id="photo0" style="display: none;">

                    <input class="region-logo-prvw margin-theme-logo" type="text" placeholder="Alt name" name="desktop_logo_alt" value="<?php echo $activeRegion['R_Logo_Name'] ?>">
                    <?php if ($activeRegion['R_Map_Logo'] != '') { ?>
                        <a class="deletePhoto delete-region-cat-photo" style="margin-top: 10px;" onclick="return confirm('Are you sure you want to delete photo?');" href="map-color-options.php?rid=<?php echo $regionID ?>&flag=desktop_logo&op=del">Delete Photo</a>
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="map_btn"  value="Submit">
                </div>
            </div>
        </form>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>
<script>
    function removeColor(id, colorVal, backgrontColorsCahnge, hideDeleted, hideDelete, font_family, font_size, font_color) {
        var remove = confirm("Are you sure this action can not be undone!");
        if (remove == true) {
            //// Table name set  tbl_Region
            var tblname = 'tbl_Region';
            var mapDel = 'deleteMap';
            if (font_family != '' && font_size != '') {
                var value = font_family + '-' + font_size + '-';
            }
            $.ajax({
                type: "POST",
                url: "remove-colors.php",
                data: {
                    regionIDGet: id,
                    themOptionField: colorVal,
                    tablename: tblname,
                    tblid: mapDel,
                    value: value,
                }
            }).done(function (msg) {
                if (msg == 1) {
                    $("#" + hideDelete).val('');
                    $('.' + hideDeleted).hide();
                    $('#' + backgrontColorsCahnge).css({'background-color': '#ffff'});
                    swal("Data Deleted", "Data has been deleted successfully.", "success");
                }
            });
        }
    }
    $(window).load(function () {
        var regionID = '<?php echo $regionID ?>';
        if (typeof regionID != 'undefined' && regionID > 0) {
            $(".color-box").click(function () {
                var div_id = $(this).attr('id');
                var savedColor = $('#' + div_id).attr('style').split("#");
                $('.color-box').colpickSetColor(savedColor[1]);
            });
        }
    });
    $(document).ready(function () {
        //*****************************************color box function*************************
        $('.color-box').colpick({
            colorScheme: 'dark',
            layout: 'rgbhex',
            onSubmit: function (hsb, hex, rgb, el) {
                $(el).css('background-color', '#' + hex);
                var div_id = $(el).attr('id');
                //change app background color
                document.getElementById("text_color" + div_id).value = '#' + hex;
                $(el).colpickHide();

            }
        });
    });
</script>
<?PHP
require_once '../include/admin/footer.php';
?>
