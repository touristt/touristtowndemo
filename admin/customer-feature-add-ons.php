<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/adminFunctions.inc.php';

if (!in_array('listing-tools', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
}
$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Billing_Type, BL_CoC_Dis_2, BL_Line_Item_Title, BL_Line_Item_Price, BL_Listing_Title FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
} else {
    header("Location:customers.php");
    exit();
}

if (isset($_REQUEST['id']) && $_REQUEST['id']) {
    $id = $_REQUEST['id'];

    $getStatus = "SELECT BLF_Active FROM tbl_BL_Feature WHERE BLF_ID = '" . encode_strings($id, $db) . "'";
    $resultStatus = mysql_query($getStatus, $db) or die("Invalid query: $getStatus -- " . mysql_error());
    $rowStatus = mysql_fetch_assoc($resultStatus);
    $BLF_Active = ($rowStatus['BLF_Active'] == 1) ? "0" : "1";
    $del = "UPDATE tbl_BL_Feature SET BLF_Active = '" . $BLF_Active . "', BLF_Last_Update = '" . encode_strings($_SESSION['USER_EM'] . '-' . $_SESSION['user_online'] . '-' . $_SESSION['USER_ID'], $db) . "' WHERE BLF_ID = '" . encode_strings($id, $db) . "'";
    $result = mysql_query($del, $db) or die("Invalid query: $del -- " . mysql_error());
    if ($result) {
        $_SESSION['addon'] = 'deleted' . $BLF_Active;
    }
    
    listing_billing($rowListing['BL_Billing_Type'], $BL_ID, $rowListing['BL_CoC_Dis_2'], $rowListing['BL_Line_Item_Title'], $rowListing['BL_Line_Item_Price']);
//update points only for one listing
    update_pointsin_business_tbl($BL_ID);
    if (isset($_SESSION['success'])) {
        unset($_SESSION['success']);
    }
    header('location: /admin/customer-feature-add-ons.php?bl_id=' . $BL_ID);
    exit;
}
require_once '../include/admin/header.php';
?>
<div class="content-left">

    <?php require_once '../include/top-nav-listing.php'; ?>

    <div class="title-link">
        <div class="title">Add Ons - <?php echo $rowListing['BL_Listing_Title'] ?></div>
        <div class="link">
            <?PHP
            require_once ('preview-link.php');
            ?>
        </div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-listing-admin.php'; ?>
    </div>
    <div class="right">
        <div class="listing-inside-div-tittle">
        </div>
        <div class="content-header">
            <div class="title">Add ons</div>
        </div>
        <?php
        $help_text = show_help_text('Add Ons');
        if ($help_text != '') {
            echo '<div class="form-inside-div border-none">' . $help_text . '</div>';
        }
        ?>
    </div>
</div>
<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>