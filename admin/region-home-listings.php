<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';
if (!in_array('regions', $_SESSION['USER_PERMISSIONS']) && !in_array('manage-county-region', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
    $sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
    $sortby = $_REQUEST['sortby'];
    $subcategory = $_REQUEST['subcategory'];
    $sql_org_listings = "SELECT BLCR_BL_ID FROM tbl_Business_Listing_Category_Region 
                         WHERE BLCR_BLC_R_ID = '" . encode_strings($regionID, $db) . "' and BLCR_Home_status=1";
    $result_org_listings = mysql_query($sql_org_listings, $db) or die("Invalid query: $sql_org_listings -- " . mysql_error());
    while ($row_org_listings = mysql_fetch_assoc($result_org_listings)) {
        $childbl_id[] = $row_org_listings['BLCR_BL_ID'];
    }
}

if (isset($_REQUEST['del']) && $_REQUEST['del'] == 'true') {
    $sql = "UPDATE tbl_Business_Listing_Category_Region SET BLCR_Home_status = 0 WHERE BLCR_BLC_R_ID ='" . $_REQUEST['rid'] . "' and BLCR_BL_ID ='" . $_REQUEST['bl_id'] . "'";
    $result = mysql_query($sql) or die(mysql_error());
    $_SESSION['listing_delete'] = 1;
    if ($_REQUEST['page'] > 0) {
        $page = "&page=" . $_REQUEST['page'];
    } else {
        $page = "";
    }
    header("Location: /admin/region-home-listings.php?rid=" . $_REQUEST['rid'] . $page);
    exit();
}

if ($_REQUEST['rid'] > 0 && $_REQUEST['listings'] > 0) { 
    $bl_id = $_REQUEST['listings'];
    $r_id = $_REQUEST['rid'];
    $page = $_REQUEST['page'];
    $sql = "UPDATE tbl_Business_Listing_Category_Region SET BLCR_Home_status = 1 WHERE BLCR_BLC_R_ID ='" . $_REQUEST['rid'] . "' and BLCR_BL_ID ='" . $_REQUEST['listings'] . "'";
    $result = mysql_query($sql) or die(mysql_error());
    $_SESSION['listing_success'] = 1;
    if ($_REQUEST['page'] > 0) {
        $page = "&page=" . $_REQUEST['page'];
    } else {
        $page = "";
    }
    header("Location: /admin/region-home-listings.php?rid=" . $_REQUEST['rid'] . $page);
    exit();
}

if (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
    $limits = explode(',', $_SESSION['USER_LIMIT']);
    $regionLimit = array();
    foreach ($limits as $limit) {
        $sql = "SELECT R_ID FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            if ($row['R_ID'] != '' && !in_array($row['R_ID'], $regionLimit)) {
                $regionLimit[] = $row['R_ID'];
            }
        }
    }
}
if ($activeRegion['R_Type'] != 3 && $activeRegion['R_Type'] != 4 && !in_array($regionID, $regionLimit)) {
    header("Location: /admin/regions.php");
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Homepage Listings</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <div class="content-sub-header link-header region-header-padding">
            <div class="data-column spl-name padding-none">Select Listings for Homepage <?php echo $activeRegion['R_Name'] ?>
            </div>
        </div>
        <form name="form1" method="POST" id="org_listings_form">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <div class="content-sub-header link-header region-header-padding">
                <div class="data-column spl-org-listngs padding-org-listing"><span class="addlisting font_weight_normal" >Sort: </span>
                    <select name="sortby" id="categorydd" onchange="filter_subcat(<?php echo $regionID ?>, this.value)">
                        <option value="">Sort by:</option>
                        <?PHP
                        $sql = "SELECT C_ID, C_Name, RC_Name FROM tbl_Category 
                                LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID
                                WHERE C_Parent = 0 AND RC_R_ID = " . $regionID . " AND RC_Status = 0 AND C_Is_Blog=0 AND C_Is_Product_Web = 0 GROUP BY RC_C_ID ORDER BY RC_Order";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $row['C_ID'] ?>" <?php echo ($row['C_ID'] == $sortby) ? 'selected' : '' ?> ><?php echo ($row['RC_Name'] != '') ? $row['RC_Name'] : $row['C_Name']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <select class="sort-listing-org-width" name="subcategory" id="subcats" onchange="filter_listings(<?php echo $regionID ?>, $('#categorydd').val(), this.value)">
                        <option value="">Select Sub Category</option>
                    </select>

                    <select class="sort-listing-org-width" name="listings" id="listings">
                        <option value="">Select Listings</option>

                    </select>
                    <input type="submit" value="+Add" class="stories-add-button"  />
                </div>
            </div>

        </form>

        <div id="reorder-category">
            <?PHP
            $sql = "SELECT BL_ID, BL_Listing_Title FROM tbl_Business_Listing 
                    LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                    INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID
                    WHERE (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) 
                    AND hide_show_listing = 1 AND BLCR_Home_status = 1 AND BLCR_BLC_R_ID = '" . encode_strings($regionID, $db) . "'";
            $sql .= " GROUP BY BL_ID ORDER BY BL_Listing_Title";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $pages = new Paginate(mysql_num_rows($result), 100);
            $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                ?>
                <div class="data-content">
                    <div class="data-column spl-org-listngs "><?php echo $row['BL_Listing_Title'] ?>
                        <div class="remove-link"><a href="region-home-listings.php?rid=<?php echo $regionID; ?>&bl_id=<?php echo $row['BL_ID']; ?>&del=true" onclick="return confirm('Are you sure you want to perform this action?');">Remove</a></div>

                    </div>
                </div>
                <?php
            }
            if (isset($pages)) {
                echo $pages->paginate();
            }
            ?>
        </div>


    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script>
    //filter listing
    function filter_subcat(rid, catid) {
        $.ajax({
            type: "POST",
            url: 'filterListings.php',
            dataType: 'json',
            data: {
                region: rid,
                category: catid
            },
            success: function (response) {
                $("#listings").empty();
                $("#listings").html("<option>Select Listing</option>");
                $('#subcats').html(response.subcat);
            }
        });
    }

    function filter_listings(rid, catid, subcatid) {
        $.ajax({
            type: "POST",
            url: 'filterListings.php',
            dataType: 'json',
            data: {
                region: rid,
                category: catid,
                subcategory: subcatid
            },
            success: function (response) {
                $('#listings').html(response.listings);
            }
        });
    }
</script>