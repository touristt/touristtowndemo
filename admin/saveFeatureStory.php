<?php
require_once '../include/config.inc.php';
require_once '../include/track-data-entry.php';

$return = array();
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'save') {
  $regionID = $_POST['rid'];
  $type = $_POST['type'];
//  $category = $_POST['category'];
  $categoryID = $_POST['id'];
  $story = $_REQUEST['story'];
  if ($story != '') {
      if($type == 'map_story') {
        $sqlMax = "SELECT MAX(SHC_Order) FROM tbl_Story_Homepage_Category WHERE SHC_R_ID = '" . $regionID . "' AND SHC_Map = 1";
        $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
        $rowMax = mysql_fetch_row($resultMax);
        $sql = " INSERT tbl_Story_Homepage_Category SET SHC_S_ID = '" . $story . "', SHC_R_ID = '" . $regionID . "', SHC_Map = 1, SHC_Order = '" . ($rowMax[0] + 1) . "'";
        $result = mysql_query($sql) or die(mysql_error());
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $regionID, 'Map Stories', $story, 'Add Map Story', 'super admin');
      }
      else if ($type == 'feature_story') {
        $sqlMax = "SELECT MAX(SHC_Order) FROM tbl_Story_Homepage_Category WHERE SHC_R_ID = '" . $regionID . "' AND SHC_Homepage = 1 AND SHC_Feature = 1";
        $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
        $rowMax = mysql_fetch_row($resultMax);
        $sql = " INSERT tbl_Story_Homepage_Category SET SHC_S_ID = '" . $story . "', SHC_R_ID = '" . $regionID . "', SHC_Homepage = 1, SHC_Feature = 1, SHC_Order = '" . ($rowMax[0] + 1) . "'";
        $result = mysql_query($sql) or die(mysql_error());
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $regionID, 'Homepage Stories', $story, 'Add Feature Story', 'super admin');
      }
      else if ($type == 'secondary_story') {
        $sqlMax = "SELECT MAX(SHC_Order) FROM tbl_Story_Homepage_Category WHERE SHC_R_ID = '" . $regionID . "' AND SHC_Homepage = 1 AND SHC_Secondary = 1";
        $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
        $rowMax = mysql_fetch_row($resultMax);
        $sql = " INSERT tbl_Story_Homepage_Category SET SHC_S_ID = '" . $story . "', SHC_R_ID = '" . $regionID . "', SHC_Homepage = 1, SHC_Secondary = 1, SHC_Order = '" . ($rowMax[0] + 1) . "'";
        $result = mysql_query($sql) or die(mysql_error());
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $regionID, 'Homepage Stories', $story, 'Add Secondary Story', 'super admin');
      }
      else if ($type == 'feature_cat_story') {
        $sqlMax = "SELECT MAX(SHC_Order) FROM tbl_Story_Homepage_Category WHERE SHC_R_ID = '" . $regionID . "' AND SHC_Category = '". $categoryID ."' AND SHC_Feature = 1";
        $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
        $rowMax = mysql_fetch_row($resultMax);
        $sql = " INSERT tbl_Story_Homepage_Category SET SHC_S_ID = '" . $story . "', SHC_R_ID = '" . $regionID . "', SHC_Category = '". $categoryID ."', SHC_Feature = 1, SHC_Order = '" . ($rowMax[0] + 1) . "'";
        $result = mysql_query($sql) or die(mysql_error());
      }
      if($result) {
        $return['success'] = 1;
      }
      else {
        $return['error'] = 1;
      }
      //return
      $stories = '';
      $sqlStories = "SELECT * FROM  tbl_Story 
                    LEFT JOIN tbl_Story_Homepage_Category ON S_ID = SHC_S_ID  
                    WHERE SHC_R_ID = '". $regionID ."' AND S_Active = 1";
      //check which type
      if($type == 'map_story') {
        $sqlStories .= " AND SHC_Map = 1";
      }
      else if($type == 'feature_story') {
        $sqlStories .= " AND SHC_Homepage = 1 AND SHC_Feature = 1";
      }
      else if($type == 'secondary_story') {
        $sqlStories .= " AND SHC_Homepage = 1 AND SHC_Secondary = 1";
      }
      else if($type == 'feature_cat_story') {
        $sqlStories .= " AND SHC_Category = '". $categoryID ."' AND SHC_Feature = 1";
      }
      $sqlStories .= " ORDER BY SHC_Order ASC";

      $resStories = mysql_query($sqlStories, $db) or die("Invalid query: $sqlStories -- " . mysql_error());
      while ($rowStories = mysql_fetch_assoc($resStories)) {
          $stories .= '<div class="data-content" id="recordsArray_'. $rowStories['SHC_ID'] .'">
                        <div class="data-column spl-org-listngs ">
                          <div class="story-title">'. $rowStories['S_Title'] .'</div>';
          //check which type
          if($type == 'map_story') {
            $stories .= '<div class="remove-link"><a href="region-map-stories.php?rid='. $regionID .'&shc_id='. $rowStories['SHC_ID'] .'&del=true" onclick="return confirm(\'Are you sure you want to perform this action?\');">Remove</a></div>';
          }
          else if($type == 'feature_story') {
            $stories .= '<div class="remove-link"><a href="region-home-stories.php?rid='. $regionID .'&shc_id='. $rowStories['SHC_ID'] .'&del=true" onclick="return confirm(\'Are you sure you want to perform this action?\');">Remove</a></div>';
          }
          else if($type == 'secondary_story') {
            $stories .= '<div class="remove-link"><a href="region-home-stories.php?rid='. $regionID .'&shc_id='. $rowStories['SHC_ID'] .'&del=true" onclick="return confirm(\'Are you sure you want to perform this action?\');">Remove</a></div>';
          }
          else if($type == 'feature_cat_story') {
            $stories .= '<div class="remove-link"><a href="regions-category-edit.php?rid='. $regionID .'&id='. $categoryID .'&shc_id='. $rowStories['SHC_ID'] .'&del=true#scroll" onclick="return confirm(\'Are you sure you want to perform this action?\');">Remove</a></div>';
          }
          $stories .= '</div>
                      </div>';
      }
      $return['stories'] = $stories;
  }
}
$return['type'] = $type;
print json_encode($return);exit;

?>