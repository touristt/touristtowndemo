<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if (!in_array('categories', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
//check if category or sub category
$parent = isset($_REQUEST['parent']) ? $_REQUEST['parent'] : 0;
//check if edit or insert
if($_REQUEST['id'] > 0) {
    $sql = "SELECT C_ID, C_Parent, C_Name, C_Name_SEO FROM tbl_Category WHERE C_ID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeCat = mysql_fetch_assoc($result);
    $parent = $activeCat['C_Parent'];
}
$sql_seo = "SELECT C_ID, C_Name_SEO FROM tbl_Category";
$result_seo = mysql_query($sql_seo, $db) or die("Invalid query: $sql -- " . mysql_error());
while ($row_seo = mysql_fetch_assoc($result_seo)) {
    $res[] = $row_seo['C_Name_SEO'];
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $sql = "tbl_Category SET C_Name = '" . encode_strings($_REQUEST['name'], $db) . "', C_Name_SEO = '" . encode_strings($_REQUEST['SEOname'], $db) . "', C_Parent = '" . encode_strings($_REQUEST['parent'], $db) . "'";
    if ($_REQUEST['id'] > 0) {
        $sql = "UPDATE " . $sql . " WHERE C_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $id = $_REQUEST['id'];
        // TRACK DATA ENTRY
        $id = $_REQUEST['id'];
        if($_REQUEST['parent'] == 0){
            Track_Data_Entry('Categories','','Manage Categories',$id,'Update Category','super admin');
        }else{
            Track_Data_Entry('Categories','','Manage Categories',$id,'Update Sub-Category','super admin');  
        }
    } else {
        $sql = "INSERT " . $sql;
        $sqlMax = "SELECT MAX(C_Order) FROM tbl_Category WHERE C_Parent = '" . encode_strings($_REQUEST['parent'], $db) . "'";
        $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
        $rowMax = mysql_fetch_row($resultMax);
        $sql .= ", C_Order = '" . ($rowMax[0] + 1) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        if($_REQUEST['parent'] == 0){
            Track_Data_Entry('Categories','','Manage Categories',$id,'Add Category','super admin'); 
        }else{
            Track_Data_Entry('Categories','','Manage Categories',$id,'Add Sub-Category','super admin'); 
        }
        
    }
    if ($result) {
        $_SESSION['success'] = 1;
        if($parent > 0) {
            header("Location: category.php?id=" . $parent);
            exit();
        }
        else {
            header("Location: category.php?id=" . $id);
            exit();
        }
    }
    else {
        $_SESSION['error'] = 1;
    }
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Categories</div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-managecategories.php'; ?>
    </div>
    <div class="right">
        <form id="regionForm" onSubmit="return validateForm();" name="form3" enctype="multipart/form-data" method="post" action="category-add.php">
            <script>
                function makeSEO(myVar) {
                    myVar = myVar.toLowerCase();
                    myVar = myVar.replace(/[^a-z0-9\s-]/gi, '');
                    myVar = myVar.replace(/"/g, '');
                    myVar = myVar.replace(/'/g, '');
                    myVar = myVar.replace(/&/g, '');
                    myVar = myVar.replace(/\//g, '');
                    myVar = myVar.replace(/,/g, '');
                    myVar = myVar.replace(/  /g, ' ');
                    myVar = myVar.replace(/ /g, '-');
                    $('#SEOname').val(myVar);
                    return false;
                }
                function validateForm() {
                    var js = <?php echo json_encode($res); ?>;
                    var vl = <?php echo json_encode($activeCat['C_Name_SEO']); ?>;
                    var value = $("#SEOname").val();
                    for (var i = js.length - 1; i >= 0; i--) {
                        if (js[i] == value && vl != value) {
                            $("#SEOname").val(value + "-");
                            validateForm();
                        }
                    }
                }
            </script>
            <input type="hidden" name="op" value="save">
            <div class="content-header">Category Details</div>
            <input type="hidden" name="parent" value="<?php echo $parent ?>">
            <input type="hidden" name="id" value="<?php echo isset($activeCat['C_ID']) ? $activeCat['C_ID'] : 0 ?>">
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Category Name</label>
                <div class="form-data">
                    <input id="seoTitle" name="name" type="text" onKeyUp="makeSEO(this.value)" value="<?php echo isset($activeCat['C_Name']) ? $activeCat['C_Name'] : '' ?>" size="22" maxlength="100"/> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>SEO Name</label>
                <div class="form-data">
                    <input name="SEOname" type="text" id="SEOname" onKeyUp="makeSEO(this.value)" value="<?php echo isset($activeCat['C_Name_SEO']) ? $activeCat['C_Name_SEO'] : '' ?>" size="22" maxlength="100"/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="button" id="button" value="Save" />
                </div>
            </div>
        </form>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>