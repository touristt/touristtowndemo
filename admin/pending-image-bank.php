<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';

if (!in_array('image-bank', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$where = 'WHERE 1=1';
if (in_array('county', $_SESSION['USER_ROLES'])) {
    $sql = "SELECT PO_ID FROM tbl_Photographer_Owner LEFT JOIN tbl_User ON PO_ID = U_Owner where U_ID = '" . $_SESSION['USER_ID'] . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowCounty = mysql_fetch_assoc($result);
    $where .= " AND IB_Owner = " . $rowCounty['PO_ID'];
}
if (isset($_GET['search_image']) && $_GET['search_image'] != '') {
    $join = 'LEFT JOIN tbl_Business_Listing ON FIND_IN_SET(BL_ID, IB_Listings)';
    $search_text = explode(',', $_GET['search_image']);
    $search_word_len = count($search_text);
    foreach ($search_text as $key => $temp) {
        if ($key == 0) {
            $operation = 'AND';
        } else {
            $operation = 'OR';
        }
        if ($count > 0) {
            if ($key == 0) {
                $where .= " AND (";
                $OR = "";
            } else {
                $OR = " OR ";
            }
            $where .= " $OR IB_Keyword LIKE '%" . mysql_real_escape_string($temp) . "%' OR IB_Name LIKE '%%" . mysql_real_escape_string($temp) . "%' OR BL_Listing_Title LIKE '%%" . mysql_real_escape_string($temp) . "%'";
            if ($key + 1 == $search_word_len) {
                $where .= ")";
            }
        } else {
            $where .= " $operation IB_Keyword LIKE '%" . mysql_real_escape_string($temp) . "%' OR IB_Name LIKE '%%" . mysql_real_escape_string($temp) . "%' OR BL_Listing_Title LIKE '%%" . mysql_real_escape_string($temp) . "%' ";
        }
    }
}
$order = "DESC";
if (isset($_REQUEST['img_sort']) && $_REQUEST['img_sort'] !== '') {
    $order = $_REQUEST['img_sort'];
} else {
     $order = 'DESC';
}
$sql = "SELECT IB_ID, IB_Keyword, IB_Path, IB_Region,IB_Thumbnail_Path, IB_Category, IB_Season, IB_People, IB_Photographer, IB_Location, IB_Owner, IB_Dimension
        FROM tbl_Image_Bank LEFT JOIN  tbl_Business_Listing  ON BL_ID = IB_Listings AND FIND_IN_SET(BL_ID, IB_Listings) AND hide_show_listing = 1
        $where AND (IB_Name = ''
        OR IB_Keyword = ''
        OR IB_Path = ''
        OR IB_Region = ''
        OR IB_Category = '0'
        OR IB_Season = '0'
        OR IB_People = '0'
        OR IB_Photographer = ''
        OR IB_Location = ''
        OR IB_Owner = '0') GROUP BY IB_ID ORDER BY IB_ID $order";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$pages = new Paginate(mysql_num_rows($result), 100);
$IMResult = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
$count = mysql_num_rows($result);

$records = 0;
if (isset($_GET['page'])) {
    $page = $_GET['page'];
    $records = 100 * ($page - 1);
}

require_once '../include/admin/header.php';
?>

<div class="content-left full-width">
    <div class="image-nav">
        <?php require_once('../include/pending-top-nav-image.php'); ?>
    </div>
    <div class="image-bank-container">
        <div class="image-bank-container-image-gallery">
            <?php
            if ($count > 0) {
                $counter = 1;
                $i = 1;
                while ($row = mysql_fetch_array($IMResult)) {
                    if ($i == 1) {
                        ?>
                        <div class="image-section-container <?php echo ($count - $counter < 4) ? 'image-bank-image-section-outer-last' : ''; ?>">
                        <?php } ?>
                        <div class="image-hover-bank <?php echo ($i == 4) ? 'image-bank-image-section-last' : ''; ?>">
                            <a href="add-image-bank.php?ib_id=<?php echo $row['IB_ID'] ?>&pendVal=pendingVal" class="image-bank-image-section">
                                <div class="image-section-image-align">
                                    <img src="http://<?php echo DOMAIN . IMG_BANK_REL . $row['IB_Thumbnail_Path'] ?>">
                                </div>
                                <div class="image-section-text-align">
                                    <?php echo $row['IB_ID'] . ' - ' . $row['IB_Dimension']; ?>
                                </div>
                            </a>
                            <a class="image-section-delete" onclick="return confirm('Are you sure you want to delete this image?')"    href="image-bank-photo-delete.php?page=<?php echo $page; ?>&img_sort=<?php echo $order; ?>&pending=1&ib_id=<?php echo $row['IB_ID'] ?>">X</a>
                            <a class="image-section-edit" href="add-image-bank.php?ib_id=<?php echo $row['IB_ID'] ?>&pendVal=pendingVal">Edit</a>
                        </div>
                        <?php
                        if ($i == 4 || $count - ($records + $counter) == 0) {
                            ?>
                        </div>
                        <?php
                    }
                    $i++;
                    $counter++;
                    if ($i == 5) {
                        $i = 1;
                    }
                }
            } else {
                echo '<div class="no-image-found">No images found in image bank.</div>';
            }
            ?>

        </div>
        <div class="image-bank-pagination">
            <?php
            if (isset($pages)) {
                echo $pages->paginate();
            }
            ?>
        </div>
    </div>
</div>
<div id="autocomplete_data" style="display: none;"><?php include '../include/autocomplete.php' ?></div>
<script>
    $(function () {
        var search_text = $('#keywords-searched').val();
        var searched_items = "";
        if (searched_items != "") {
            var json = [];
            json.push({"name": searched_items});
        }
        //autocomplete for lisitngs
        var search = $('#autocomplete_data').text();
        var searched_list = "";
        var json_data = [];
        if (search != "") {
            searched_list = search.split("%");
            for (var i = 0; i < searched_list.length - 1; i++) {
                json_data.push({"name": searched_list[i]});
            }
        }
        $("#autocomplete").tokenInput(json_data, {
            onResult: function (item) {
                if ($.isEmptyObject(item)) {
                    return [{id: '0', name: $("tester").text()}]
                } else {
                    item.unshift({"name": $("tester").text()});
                    var lookup = {};
                    var result = [];

                    for (var temp, i = 0; temp = item[i++]; ) {
                        var name = temp.name;

                        if (!(name in lookup)) {
                            lookup[name] = 1;
                            result.push({"name": name});
                        }
                    }
                    return result;
                }

            },
            onAdd: function (item) {
                var value = $('#keywords-searched').val();
                if (value != '') {
                    $('#keywords-searched').val(value + "," + item.name);
                    $('#auto_complete_form_submit').submit();
                } else {
                    $('#keywords-searched').val(item.name);
                    $('#auto_complete_form_submit').submit();
                }
            },
            onDelete: function (item) {
                var value = $('#keywords-searched').val().split(",");
                $('#keywords-searched').empty();
                var data = "";
                $.each(value, function (key, value) {
                    if (value != item.name) {
                        if (data != '') {
                            data = data + "," + value;
                        } else {
                            data = value;
                        }
                    }
                });
                $('#keywords-searched').val(data);
            },
            resultsLimit: 10,
            prePopulate: json
        }
        )
    });
</script>
<?PHP
require_once '../include/admin/footer.php';
?>