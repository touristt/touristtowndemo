<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('listing-tools', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
}

if (isset($_REQUEST['bl_id']) && $_REQUEST['bl_id'] > 0) {
    $sql = "SELECT B_ID, BL_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words, BL_Billing_Type, BL_CoC_Dis_2, BL_Line_Item_Title, BL_Line_Item_Price FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            LEFT JOIN tbl_Category ON C_ID = BL_C_ID 
            WHERE BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
    $BL_ID = $rowListing['BL_ID'];
} else {
    header("Location:customers.php");
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
// clear all the selections first
    $sql = "DELETE FROM tbl_BL_Feature WHERE BLF_BL_ID = '" . encode_strings($rowListing['BL_ID'], $db) . "' AND BLF_F_ID IN (11, 13)";
    $res = mysql_query($sql, $db);
    if (is_array($_POST['feature'])) {
        foreach ($_POST['feature'] as $key => $val) {
//update the feature data table
            $sql = "INSERT INTO tbl_BL_Feature(BLF_BL_ID, BLF_F_ID, BLF_Agreement_Status, BLF_Date, BLF_Active, BLF_Last_Update) VALUES('" . $rowListing['BL_ID'] . "','$val', '1', CURDATE(), '1', '" . encode_strings($_SESSION['USER_EM'] . '-' . $_SESSION['user_online'] . '-' . $_SESSION['USER_ID'], $db) . "')";
            $res1 = mysql_query($sql, $db);
        }
    }
    if ($res) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $rowListing['BL_ID'];
        Track_Data_Entry('Listing',$id,'Store','','Add','super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    listing_billing($rowListing['BL_Billing_Type'], $BL_ID, $rowListing['BL_CoC_Dis_2'], $rowListing['BL_Line_Item_Title'], $rowListing['BL_Line_Item_Price']);
//update points only for one listing
    update_pointsin_business_tbl($rowListing['BL_ID']);
}

require_once '../include/admin/header.php';
?>

<div class="content-left">

    <?php require_once '../include/top-nav-listing.php'; ?>

    <div class="title-link">
        <div class="title">Store - <?php echo $rowListing['BL_Listing_Title'] ?></div>
        <div class="link">
        </div>
    </div>

    <div class="left">
    </div>

    <div class="right">
        <form name="form1" method="post" action="">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="bl_id" value="<?php echo $rowListing['BL_ID'] ?>">

            <div class="content-header">Store</div>
            <?PHP
            $sql = "SELECT F_ID, F_Name, F_Price FROM tbl_Feature WHERE F_Price > 0";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                $sql1 = "SELECT BLF_BL_ID FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = " . $row['F_ID'];
                $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
                $row1 = mysql_fetch_assoc($result1);
                if ($row1['BLF_BL_ID'] != '') {
                    ?>
                    <div class="form-inside-div" id="my-pages">
                        <div class="title"><?php echo $row['F_Name'] ?></div>
                        <div class="category">$<?php echo $row['F_Price'] ?></div>
                        <div class="view"><input type="checkbox" name="feature[<?php echo $row['F_ID'] ?>]" value="<?php echo $row['F_ID'] ?>" checked/></div>
                    </div>
                <?php } else { ?>
                    <div class="form-inside-div" id="my-pages">
                        <div class="title"><?php echo $row['F_Name'] ?></div>
                        <div class="category">$<?php echo $row['F_Price'] ?></div>
                        <div class="view"><input type="checkbox" name="feature[<?php echo $row['F_ID'] ?>]" value="<?php echo $row['F_ID'] ?>"/></div>
                    </div>
                    <?php
                }
            }
            ?>

            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" name="button" id="button2" value="Submit" />
                </div>
            </div>

        </form>
    </div>
</div>

<?PHP
if (isset($_SESSION['WARNING']) && $_SESSION['WARNING'] == 1) {
    echo '<script>
            swal({
                    title: "Credit Card Info",   
                    text: "Please provide credit card info before buying Add On!",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#6dac29",   
                    confirmButtonText: "OK",   
                    closeOnConfirm: false 
                }, function(){   
                    window.location.href="customer-listing-credit-card-details.php?id=' . $BID . '" 
                });
          </script>';
    unset($_SESSION['WARNING']);
}
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>