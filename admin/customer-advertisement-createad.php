<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('superadmin', $_SESSION['USER_ROLES'])) {
    header("Location: /admin/");
    exit();
}

$query = "SELECT  * FROM tbl_Advertisement_Type WHERE AT_ID IN(1,2)";
$query_RESULT = mysql_query($query, $db) or die("Invalid query: $query -- " . mysql_error());

$values = array();
while ($query_all = mysql_fetch_assoc($query_RESULT)) {
    $values[] = $query_all['AT_Cost'];
}

if (isset($_REQUEST['butt']) && $_REQUEST['butt'] == 'Buy A Campaign') {
    if (isset($_REQUEST['campaign'])) {
        $campaign = 1;
    } else {
        $campaign = 0;
    }
    $sql = "SELECT BL_B_ID FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($_REQUEST['listings'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $advertType = "SELECT AT_Cost FROM tbl_Advertisement_Type WHERE AT_ID = '" . encode_strings($_REQUEST['advert_type'], $db) . "'";
    $resAdvertType = mysql_query($advertType, $db) or die("Invalid query: $advertType -- " . mysql_error());
    $rowType = mysql_fetch_assoc($resAdvertType);
    $discount = $_REQUEST['discount'];
    $total = $rowType['AT_Cost'] - $discount; //echo $discount; exit;
    $Description = $_POST['Description'];
    $Note = $_POST['Note'];
    $sql = "tbl_Advertisement SET 
            A_BL_ID = '" . encode_strings(($_REQUEST['listings'] != "") ? $_REQUEST['listings'] : 0, $db) . "',
            A_Website = '" . encode_strings($_REQUEST['domain'], $db) . "', 
            A_AT_ID = '" . encode_strings(($_REQUEST['advert_type'] != "") ? $_REQUEST['advert_type'] : 0, $db) . "', 
            A_C_ID = '" . encode_strings(($_REQUEST['category'] != "") ? $_REQUEST['category'] : 0, $db) . "',
            A_SC_ID = '" . encode_strings(($_REQUEST['subcategory'] != "") ? $_REQUEST['subcategory'] : 0, $db) . "',
            A_End_Date = '" . encode_strings(($_REQUEST['endDate'] != "") ? $_REQUEST['endDate'] : "0000-00-00", $db) . "',
            A_Campaign = '" . encode_strings($campaign, $db) . "',
            A_Title = '" . encode_strings($_REQUEST['title'], $db) . "', 
            A_Description = '" . encode_strings($Description, $db) . "',
            A_Notes = '" . encode_strings($Note, $db) . "',
            A_B_ID  ='" . encode_strings(($rowListing['BL_B_ID'] != "") ? $rowListing['BL_B_ID'] : 0, $db) . "',
            A_Date = CURDATE(),
            A_Status = 3,
            A_Active_Date = CURDATE(),
            A_Discount = '" . encode_strings(($_REQUEST['discount'] != "") ? $_REQUEST['discount'] : 0, $db) . "',
            A_Total = '" . encode_strings($total, $db) . "'";
    require '../include/picUpload.inc.php';
    $pic = Upload_Pic_Normal('0', 'approve', 0, 0, true, IMG_LOC_ABS, 0);
    if ($pic) {
        $sql .= ", A_Approved_Logo = '" . encode_strings($pic, $db) . "'";
    } else {
        header("Location:customer-advertisement-createad.php");
        exit;
    }
    $sql = "INSERT " . $sql;
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $id = mysql_insert_id();
    $_SESSION['success'] = 1;
    // TRACK DATA ENTRY
    Track_Data_Entry('Campaign', '', 'Create Campaign', $id, 'Add', 'super admin');
    header("Location: buy-an-advertisement.php?advert_id=$id");
    exit();
}

require_once '../include/admin/header.php';
?>
<script>
    $(function () {
        $(document).tooltip();
    });
</script>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Campaigns</div>
        <div class="link">
        </div>
    </div>

    <div class="left advert-left-nav">
        <?php require_once '../include/nav-B-advertisement-admin.php'; ?>
    </div>

    <div class="right">
        <!--Step One-->
        <form name="form1" method="post" action="customer-advertisement-createad.php" enctype="multipart/form-data" onSubmit="return validateForm();">
            <div class="content-header">Create Campaign</div>

            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Website</label>
                <div class="form-data" id="regions">
                    <select class="adv-type-options" id="region-id" name="domain" onChange="validate_buy_an_add(1, 0, 0, 0);" required>
                        <option required value="">Select Website</option>
                        <?PHP
                        $sql = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Domain != ''";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($rowRegion = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $rowRegion['R_ID'] ?>"><?php echo $rowRegion['R_Name'] ?></option>
                            <?PHP
                        }
                        ?>

                    </select>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Campaign Type</label>
                <div class="form-data">
                    <select class="adv-type-options" id="advert-type" name="advert_type" onChange="validate_buy_an_add(0, 1, 0, 0);" required>
                        <option value="">Select Campaign Type</option>
                        <?php
                        $sql = "SELECT AT_ID, AT_Name FROM tbl_Advertisement_Type WHERE AT_ID NOT IN(3,4)";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($rowAT = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $rowAT['AT_ID'] ?>"><?php echo $rowAT['AT_Name'] ?></option>
                        <?php } ?>

                    </select>
                </div>
                <a class="adv-sample" id="view-sample" onclick="show_image()" style="display:none">View Sample</a>
                <div id="sample-image" style="display :none;">
                    <div id="sample-image-show" >

                    </div>
                </div>

            </div>

            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin" id="cat">
                <label>Category</label>
                <div class="form-data" id="advert-cats">
                    <select class="adv-type-options" id="advert-cat" name="category" required="required">
                        <option value="" required>Select Category</option>
                    </select>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin" id="sub-cat">
                <label>Sub Category</label>
                <div class="form-data" id="advert-subcats">
                    <select class="adv-type-options" id="advert-subcat" name="subcategory" required="required">
                        <option value="">Select Sub Category</option>
                    </select>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>End Date</label>
                <div class="form-data">
                    <input type="text" name="endDate" class="previous-date-not-allowed" id="endDate"/> 
                </div>
            </div>

            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Campaign</label>
                <div class="form-data">
                    <input type="checkbox" name="campaign" id="campaign"/> 
                </div>
            </div>

            <div id="monthlCostDiv" class="form-inside-div form-inside-div-adv form-inside-div-width-admin" style="display: none;">
                <label>Monthly Cost</label>
                <div id="montlyCost" class="form-data from-inside-div-text">

                </div>
            </div>
            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Campaign Link</label>
                <div class="form-data listings_count">
                    <input id="autocomplete_lisitngs" type="text"/>
                    <input type="hidden" id="listing_search" name="listings" value="">
                </div>
                <a class="adv-sample" title="This is the listing where ad will setup">What is this?</a>
            </div>

            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Title</label>
                <div class="form-data">
                    <input type="text" name="title" id="title" required>
                </div>
                <a class="adv-sample" title="This is the main message of your ad. Keep it short">What is this?</a>
            </div>

            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Description</label>
                <div class="form-data wiziwig-desc description-text">
                    <textarea name="Description"  class="tt-ckeditor" id="desc"></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Notes</label>
                <div class="form-data wiziwig-desc description-text">
                    <textarea name="Note"  class="tt-ckeditor" id="notes"></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Upload Photo</label>
                <div class="form-data from-inside-div-text">
                    <div class="approved-photo-div">
                        <div class="inputWrapper adv-photo float-left">Browse
                            <input class="fileInput" id="approved-photo" type="file" name="approve[]" onchange="show_file(this)" required>
                        </div>
                    </div>
                    <div class="approved-photo-div">
                        <img id="uploadFile" style="display: none;"   src="">          
                    </div>
                    <div id="showVal" style="display: none;">

                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Discount</label>
                <span style="float: left;margin: 10px 0 0px -15px;">$</span>
                <input name="discount" type="text" id="discount"  size="15" />
            </div>
            <div class="form-inside-div border-none margin-bottom-26 margin-top-11 form-inside-div-width-admin">
                <div class="button">
                    <input onClick="return confirm('Are you sure, you want to create an ad!');" type="submit" name="butt" value="Buy A Campaign"/>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="auto-complete-div" style="display: none;"></div>
<script>
    $(document).ready(function () {
        $("#advert-type").on('change', function () {
            var adverType = $("#advert-type").val();
            if (adverType == 1) {
                $("#showVal").show();
                $("#montlyCost").show();
                $("#monthlCostDiv").show();
                $("#montlyCost").text('$<?php echo $values['0']; ?> (Plus taxes)');
                $("#showVal").text('Image should be 1600X640 or equal aspect ratio.');
            } else if (adverType == 2) {
                $("#showVal").show();
                $("#montlyCost").show();
                $("#monthlCostDiv").show();
                $("#montlyCost").text('$<?php echo $values['1']; ?> (Plus taxes)');
                $("#showVal").text('Image should be 350X120 or equal aspect ratio.');
            } else if (adverType == '') {
                $("#showVal").hide();
                $("#montlyCost").hide();
                $("#monthlCostDiv").hide();
            }
        });

    });
    function validateForm() {
        var advert_type = $('#advert-type').val();
        var approvedPhotos = $('#approved-photo').val();
        var img = document.getElementById('uploadFile');
        var width = img.naturalWidth;
        var height = img.naturalHeight;
        if (approvedPhotos == '') {
            swal("", "Please Select Image.", "warning");
            return false;
        }
        if (advert_type == 1) {
            if (width != 1600) {
                swal("", "Photo wrong size. Must be 1600 pixels wide.", "warning");
                return false;
            }
            if (height != 640) {
                swal("", "Photo wrong size. Must be 640 pixels high.", "warning");
                return false;
            }
        } else if (advert_type == 2) {
            if (width != 350) {
                swal("", "Photo wrong size. Must be 350 pixels wide.", "warning");
                return false;
            }
            if (height != 120) {
                swal("", "Photo wrong size. Must be 120 pixels high.", "warning");
                return false;
            }
        }
    }
    function show_image()
    {
        var check = $.trim($("#sample-image-show").html());
        if (check != '') {
            $("#sample-image").dialog({
                modal: true,
                draggable: false,
                resizable: false,
                show: 'blind',
                hide: 'blind',
                width: 'auto',
                open: function () {
                    jQuery('.ui-widget-overlay').bind('click', function () {
                        jQuery('#sample-image').dialog('close');
                    });
                }
            });
        }
        $("#ui-dialog-title-dialog").hide();
        $(".ui-dialog-titlebar").removeClass('ui-widget-header');
    }
    var advert_total_cost = "";
    function validate_buy_an_add(R, AT, C, S)
    {
        var advert_type = $('#advert-type').val();
        var region_id = $('#region-id').val();

        //Onchange Region
        if (R == 1) {
            $("#advert-cat").val("");
            $.ajax({
                type: "GET",
                url: "get-customer-advert-cat.php",
                data: {
                    region_id: region_id,
                    advert_type: advert_type
                }
            }).done(function (msg) {
                $("#advert-cats").empty();
                $("#advert-cats").html(msg);
                $("#advert-subcat").val("");
                $("#advert-cat").val("");
                $("#listing").val("");
            });
        }
        //Onchange Ad Type
        if (AT == 1) {
            //Get Price based on Ad type
            $.post("get_advert_price.php", {
                advert_type: advert_type
            }, function (done) {
                if (done == 0) {
                    $('#adver-total').hide();
                    $('#advert-total').html("");
                    advert_total_cost = "";
                } else {
                    $('#adver-total').show();
                    $('#advert-total').html('$' + done);
                    advert_total_cost = done;
                }
            });
            if (advert_type == '')
            {
                $("#view-sample").hide();
                $('#cat').show();
                $("#advert-cat").attr('required', 'required');
                $("#advert-cat").val("");
                $('#sub-cat').show();
                $("#advert-subcat").attr('required', 'required');
                $("#advert-subcat").val("");
                $("#sample-image-show").empty();
            }
            if (advert_type == 1)
            {
                $('#cat').show();
                $("#advert-cat").attr('required', 'required');
                $("#advert-cat").val("");
                $('#sub-cat').show();
                $("#advert-subcat").removeAttr('required');
                $("#advert-subcat").val("");
                $("#view-sample").show();
                $("#sample-image-show").empty();
                $("#sample-image-show").append("<img src='<?php echo "http://" . DOMAIN . IMG_LOC_REL . "8489225.jpg" ?>'>");
            }
            if (advert_type == 2)
            {
                $('#cat').show();
                $("#advert-cat").attr('required', 'required');
                $("#advert-cat").val("");
                $('#sub-cat').hide();
                $("#advert-subcat").removeAttr('required');
                $("#advert-subcat").val("");
                $("#view-sample").show();
                $("#sample-image-show").empty();
                $("#sample-image-show").append("<img src='<?php echo "http://" . DOMAIN . IMG_LOC_REL . "d-ad-3.png" ?>'>");
            }
            if (advert_type == 3)
            {
                $('#cat').show();
                $("#advert-cat").attr('required', 'required');
                $("#advert-cat").val("");
                $('#sub-cat').show();
                $("#advert-subcat").attr('required', 'required');
                $("#advert-subcat").val("");
                $("#view-sample").show();
                $("#sample-image-show").empty();
                $("#sample-image-show").append("<img src='<?php echo "http://" . DOMAIN . IMG_LOC_REL . "ad-1.png" ?>'>");
            }
            if (advert_type == 4)
            {
                $('#cat').hide();
                $("#advert-cat").removeAttr('required');
                $("#advert-cat").val("");
                $('#sub-cat').hide();
                $("#advert-subcat").removeAttr('required');
                $("#advert-subcat").val("");
                $("#view-sample").show();
                $("#sample-image-show").empty();
                $("#sample-image-show").append("<img src='<?php echo "http://" . DOMAIN . IMG_LOC_REL . "ad-1.png" ?>'>");
            }
        }
        var cat_id = $('#advert-cat').val();

        //Onchange Cat
        if (C == 1) {
            $.ajax({
                type: "GET",
                url: "get-customer-advert-subcat.php",
                data: {
                    cat_id: cat_id,
                    website_id: region_id,
                    advert_type: advert_type
                }
            }).done(function (msg) {
                $("#advert-subcats").empty();
                $("#advert-subcats").html(msg);
            });
        }
        var SubCat = $('#advert-subcat').val();
        if (region_id > 0 && cat_id > 0 && SubCat > 0 && advert_type > 0) {
            $('#discount').bind("keyup focusout", function () {
                var discount = $("#discount").val();
                if (parseFloat(discount) > advert_total_cost)
                {
                    swal("Error", "Discount must be less than Sub-Total!", "error");
                    var totalPrice = advert_total_cost;
                    $("#discount").val(0);
                    $("#advert-total").text("$" + totalPrice);
                } else
                {
                    if (discount == '0.00' || discount == '0' || discount == "") {
                        $("#advert-total").text("");
                        var totalPrice = advert_total_cost;
                        $("#advert-total").text("$" + totalPrice);
                    } else {
                        var totalPrice = advert_total_cost - parseFloat(discount);
                        $("#advert-total").text("");
                        $("#advert-total").text("$" + parseFloat(totalPrice).toFixed(2));
                    }
                }
            });
        } else if (region_id > 0 && cat_id > 0 && advert_type > 0) {
            $('#discount').bind("keyup focusout", function () {
                var discount = $("#discount").val();
                if (parseFloat(discount) > advert_total_cost)
                {
                    swal("Error", "Discount must be less than Sub-Total!", "error");
                    var totalPrice = advert_total_cost;
                    $("#discount").val(0);
                    $("#advert-total").text("$" + totalPrice);
                } else
                {
                    if (discount == '0.00' || discount == '0' || discount == "") {
                        $("#advert-total").text("");
                        var totalPrice = advert_total_cost;
                        $("#advert-total").text("$" + totalPrice);
                    } else {
                        var totalPrice = advert_total_cost - parseFloat(discount);
                        $("#advert-total").text("");
                        $("#advert-total").text("$" + parseFloat(totalPrice).toFixed(2));
                    }
                }
            });
        } else if (region_id > 0 && advert_type > 0) {
            $('#discount').bind("keyup focusout", function () {
                var discount = $("#discount").val();
                if (parseFloat(discount) > advert_total_cost)
                {
                    swal("Error", "Discount must be less than Sub-Total!", "error");
                    var totalPrice = advert_total_cost;
                    $("#discount").val(0);
                    $("#advert-total").text("$" + totalPrice);
                } else
                {
                    if (discount == '0.00' || discount == '0' || discount == "") {
                        $("#advert-total").text("");
                        var totalPrice = advert_total_cost;
                        $("#advert-total").text("$" + totalPrice);
                    } else {
                        var totalPrice = advert_total_cost - parseFloat(discount);
                        $("#advert-total").text("");
                        $("#advert-total").text("$" + parseFloat(totalPrice).toFixed(2));
                    }
                }
            });
        }
    
    $(function () {
        //autocomplete for lisitngs
        $.post("get-listings-for-ad.php", {
        }, function (done) {
            $('#auto-complete-div').empty();
            $('#auto-complete-div').text(done);
            var numItems = $('.listings_count > ul').length;
            if (numItems > 0) {
                $('#listing_search').val('');
                $('.listings_count ul').first().remove();
            }
            var search_list = $('#auto-complete-div').text();
            var searched_list_items = "";
            var json = [];
            if (search_list != "") {
                searched_list_items = search_list.split("@");
                for (var i = 0; i < searched_list_items.length - 1; i++) {
                    searched_list_item_test = searched_list_items[i].split("/");
                    json.push({"id": searched_list_item_test[0], "name": searched_list_item_test[1]});
                }
            }
            //autocomplete for lisitngs
            $("#autocomplete_lisitngs").tokenInput(json, {
                onAdd: function (item_lisitngs) {
                    var list_val = $('#listing_search').val();
                    if (list_val != '') {
                        $('#listing_search').val(list_val + "," + item_lisitngs.id);
                    } else {
                        $('#listing_search').val(item_lisitngs.id);
                    }
                },
                onDelete: function (item_lisitngs) {
                    var value_listings = $('#listing_search').val().split(",");
                    $('#listing_search').empty();
                    var data_listings = "";
                    $.each(value_listings, function (key, value_listings) {
                        if (value_listings != item_lisitngs.id) {
                            if (data_listings != '') {
                                data_listings = data_listings + "," + value_listings;
                            } else {
                                data_listings = value_listings;
                            }
                        }
                    });
                    $('#listing_search').val(data_listings);
                },
                resultsLimit: 10,
                preventDuplicates: true,
                tokenLimit: 1
            }
            )
            $('.token-input-dropdown').css("width", "258px");
        });
    });
    }
    function show_file(input) {
        if (input.files && input.files[0]) {
            $('#uploadFile').show();
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#uploadFile')
                        .attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<?PHP
require_once '../include/admin/footer.php';
?>