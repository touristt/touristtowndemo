<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('listing-tools', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
}

$BL_ID = $_REQUEST['bl_id'];

if (isset($BL_ID) && $BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
} else {
    header("Location:customers.php");
    exit();
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $delCouponImg = "DELETE tbl_Business_Feature_Coupon,tbl_Business_Feature_Coupon_Category_Multiple FROM tbl_Business_Feature_Coupon LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID WHERE BFC_ID = '" . encode_strings($_REQUEST['bfc_id'], $db) . "' AND BFC_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $resDelCouponImg = mysql_query($delCouponImg, $db) or die("Invalid query: $delCouponImg -- " . mysql_error());
    if ($resDelCouponImg) {
        $_SESSION['delete'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        $BFC_ID = $_REQUEST['bfc_id'];
        Track_Data_Entry('Listing', $id, 'Manage Coupon', $BFC_ID, 'Delete', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location:customer-feature-manage-coupon.php?bl_id=" . $BL_ID);
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left">
    <?php require_once '../include/top-nav-listing.php'; //top-nav-listing  ?> 
    <div class="title-link">
        <div class="title">Add Ons - <?php echo $rowListing['BL_Listing_Title'] ?></div>
        <div class="link">
            <?PHP
            require_once ('preview-link.php');
            ?>
        </div> 
    </div>

    <div class="left">
        <?PHP
        require '../include/nav-listing-admin.php'; // nav-listing-admin
        ?>
    </div>
    <div class="right">
        <div class="content-header">
            <div class="title">Manage Coupon</div>
            <div class="link">
               <a href='customer-feature-add-coupon.php?bl_id=<?php echo $BL_ID ?>'>+ Add Coupon</a>
            </div>
        </div>
        <div class="form-inside-div border-none padding-bottom-none">
            <div class="content-sub-header">
                <div class="data-pdf-title">Coupon Name</div>
                <div class="coupon-options">#Sent</div>
                <div class="coupon-options">Edit</div>
                <div class="coupon-options">Delete</div>
            </div>            
        </div>
        <?PHP
            $sql = "SELECT BFC_ID, BFC_Title FROM tbl_Business_Feature_Coupon WHERE BFC_BL_ID = '$BL_ID'";
            $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($resultTMP)) {
                ?>
        <div class="form-inside-div padding-coupon-items">
            <div class="data-pdf-title">
                <?php echo $row['BFC_Title'] ?>
            </div>
            <div class="coupon-options"><?php
                        $sql_sent="SELECT CU_Counter FROM  tbl_Coupon_Usages where CU_BFC_ID='" . encode_strings($row['BFC_ID'], $db) . "' order by CU_Counter desc";
                        $result_count=mysql_query($sql_sent);
                        $row_count=mysql_fetch_assoc($result_count);
                        echo ($row_count['CU_Counter']!='' ? $row_count['CU_Counter'] : '0');
                        ?></div>
            <div class="coupon-options"><a href="customer-feature-add-coupon.php?bl_id=<?php echo $BL_ID ?>&bfc_id=<?php echo $row['BFC_ID'] ?>">Edit</a></div>
            <div class="coupon-options"><a onClick="return confirm('Are you sure?');" href="customer-feature-manage-coupon.php?bl_id=<?php echo $BL_ID ?>&bfc_id=<?php echo $row['BFC_ID'] ?>&op=del">Delete</a></div>
        </div>
        <?PHP }
            ?>
    </div>
</div>    
<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>