<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('business-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
}

$BID = $_REQUEST['id'];
$sql = "SELECT A_ID FROM tbl_Advertisement WHERE A_B_ID = '" . encode_strings($BID, $db) . "' AND A_Is_Deleted = 0 AND A_AT_ID NOT IN(3,4) AND A_Status = 3 AND (A_End_Date >= CURDATE() OR A_End_Date = 0000-00-00)";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$count = mysql_num_rows($result);
$row = mysql_fetch_assoc($result);

require_once '../include/admin/header.php';
?>
<script>
    $(function () {
        $(document).tooltip();
    });
</script>
<div class="content-left advert">
    <?php require_once '../include/nav-B-advertisement-credit-admin.php'; ?>
    <div class="title-link">
        <div class="title">My Campaigns</div>
        <div class="link">

        </div> 
        <div class="title float-right-text text-margin"></div>
    </div>

    <div class="right">
        <div class="content-header">
            <div class="title">Overview</div>
            <div class="link">
                <label class="add-link"><a class="buy-advert" href="customer-advertisement.php?id=<?php echo $BID ?>">Buy Campaign</a></label>
            </div>
        </div>
        <div class="containter-adv">
            <div class="form-inside-div from-inside-div-advert-acc">
                <label>Number of Active Campaigns</label>
                <div class="form-data">
                    <?php echo $count; ?>
                </div>
            </div>
            <?php
            $sql_sum = "SELECT SUM(AS_Impression) AS total_impressions, SUM(AS_Clicks) AS total_clicks FROM tbl_Advertisement_Statistics
                        LEFT JOIN tbl_Advertisement ON A_ID = AS_A_ID WHERE A_B_ID = '" . encode_strings($BID, $db) . "' AND A_Is_Deleted = 0 AND A_AT_ID NOT IN(3,4) AND A_Status = 3 AND (A_End_Date >= CURDATE() OR A_End_Date = 0000-00-00)";
            $result_sum = mysql_query($sql_sum, $db) or die("Invalid query: $sql_sum -- " . mysql_error());
            $row_sum = mysql_fetch_assoc($result_sum);
            ?>
            <div class="form-inside-div from-inside-div-advert-acc ">
                <label>Total Impressions</label>
                <div class="form-data">
                    <?php
                    if ($row_sum['total_impressions'] != '') {
                        echo $row_sum['total_impressions'];
                    } else {
                        echo '0';
                    }
                    ?>
                </div>
                <a class="side-informtion" title="An impression is the number of times your ad is loaded onto the website and seen by users of the website">What is this?</a>
            </div>
            <div class="form-inside-div from-inside-div-advert-acc ">
                <label>Total Click</label>
                <div class="form-data">
                    <?php
                    if ($row_sum['total_clicks'] != '') {
                        echo $row_sum['total_clicks'];
                    } else {
                        echo '0';
                    }
                    ?>
                </div>
            </div>
            <div class="form-inside-div from-inside-div-advert-acc">
                <label>Monthly Cost</label>
                <div class="form-data">
                    <?php
                    $start_date = date('Y-m-d', strtotime(date('Y-m')));
                    $end_date = date('Y-m-t', strtotime($start_date));
                    $sql_total = "SELECT SUM(A_Total) as total FROM tbl_Advertisement where A_B_ID = $BID AND A_Is_Deleted = 0 AND A_AT_ID NOT IN(3,4) AND A_Status = 3 AND (A_End_Date >= CURDATE() OR A_End_Date = 0000-00-00)";
                    $result_total = mysql_query($sql_total, $db) or die("Invalid query: $sql_total -- " . mysql_error());
                    $row_total = mysql_fetch_array($result_total);
                    if ($row_total['total']) {
                        echo '$' . $row_total['total'];
                    } else {
                        echo '$0.00';
                    }
                    ?>

                </div>
            </div>
        </div>

        <!--Requested ADS-->
        <?php
        $requestedAds = 0;
        $sqlRequested = " SELECT A_ID, A_Title, A_SC_ID, A_C_ID, A_AT_ID, AT_Name, C_Name, R_Name, RC_Name FROM tbl_Advertisement
                            LEFT JOIN tbl_Region ON A_Website = R_ID 
                            LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID 
                            LEFT JOIN tbl_Category ON A_C_ID = C_ID
                            LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID AND A_Website = RC_R_ID
                            WHERE A_B_ID = '" . encode_strings($BID, $db) . "' AND A_Is_Deleted = 0 AND A_AT_ID NOT IN(3,4) AND A_Status = 1 AND (A_End_Date >= CURDATE() OR A_End_Date = 0000-00-00)";
        $resultRequested = mysql_query($sqlRequested, $db) or die("Invalid query: $sqlRequested -- " . mysql_error());
        $requestedCount = mysql_num_rows($resultRequested);
        while ($rowRequested = mysql_fetch_assoc($resultRequested)) {
            $AT_ID = $rowRequested['A_AT_ID'];
            $sqlRequestedsub = "SELECT * FROM tbl_Category WHERE C_ID = '" . encode_strings($rowRequested['A_SC_ID'], $db) . "'";
            $resultRequestedsub = mysql_query($sqlRequestedsub, $db) or die("Invalid query: $sqlRequestedsub -- " . mysql_error());
            $rowRequestedsub = mysql_fetch_assoc($resultRequestedsub);
            if ($requestedAds == 0) {
                ?>
                <div class="containter-adv">
                    <div class="content-header">
                        <div class="title">Requested Campaigns</div>
                        <div class="link">

                        </div>
                    </div>
                    <div class="content-sub-header">
                        <div class="data-column advert-listing align-left">Campaign Title</div>
                        <div class="data-column  align-left">Region</div>
                        <div class="data-column ">Campaign Type</div>
                        <div class="data-column advert-listing ">Category</div>
                    </div>
                <?php } ?>

                <div class="data-content">
                    <div class="data-column advert-listing align-left current-ad-padding"><a href="customer-advertisment-detail.php?advert_id=<?php echo $rowRequested['A_ID']; ?>&id=<?php echo $BID ?>"><?php echo $rowRequested['A_Title']; ?></a></div>
                    <div class="data-column  align-left current-ad-padding"><?php echo $rowRequested['R_Name']; ?></div>
                    <div class="data-column  current-ad-padding"><?php echo $rowRequested['AT_Name']; ?></div>
                    <div class="data-column advert-listing current-ad-padding">
                        <?php
                        if ($rowRequested['A_C_ID'] != 0) {
                            if ($rowRequested['RC_Name'] != '') {
                                echo $rowRequested['RC_Name'];
                            } else {
                                echo $rowRequested['C_Name'];
                            }
                        }
                        if ($rowRequested['A_SC_ID'] != 0) {
                            echo " / " . $rowRequestedsub['C_Name'];
                        }
                        ?>
                    </div>
                </div>

                <?php
                $requestedAds++;
                if ($requestedCount == $requestedAds) {
                    ?>
                </div>
                <?php
            }
        }
        ?>

        <!--Pending Approval-->
        <?php
        $pendingAds = 0;
        $sqlPending = " SELECT A_ID, A_Title, A_SC_ID, A_C_ID, A_AT_ID, AT_Name, C_Name, R_Name, RC_Name FROM tbl_Advertisement
                        LEFT JOIN tbl_Region ON A_Website = R_ID 
                        LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID 
                        LEFT JOIN tbl_Category ON A_C_ID = C_ID 
                        LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID AND A_Website = RC_R_ID
                        WHERE A_B_ID = '" . encode_strings($BID, $db) . "' AND A_Is_Deleted = 0 AND A_AT_ID NOT IN(3,4) AND A_Status = 2 AND (A_End_Date >= CURDATE() OR A_End_Date = 0000-00-00)";
        $resultPending = mysql_query($sqlPending, $db) or die("Invalid query: $sqlPending -- " . mysql_error());
        $PendingCount = mysql_num_rows($resultPending);
        while ($rowPending = mysql_fetch_assoc($resultPending)) {
            $AT_ID = $rowPending['A_AT_ID'];
            $sqlPendingsub = "SELECT * FROM tbl_Category WHERE C_ID = '" . encode_strings($rowPending['A_SC_ID'], $db) . "'";
            $resultPendingsub = mysql_query($sqlPendingsub, $db) or die("Invalid query: $sqlPendingsub -- " . mysql_error());
            $rowPendingsub = mysql_fetch_assoc($resultPendingsub);
            if ($pendingAds == 0) {
                ?>
                <div class="containter-adv">
                    <div class="content-header">
                        <div class="title">Pending Approval</div>
                        <div class="link">

                        </div>
                    </div>
                    <div class="content-sub-header">
                        <div class="data-column advert-listing align-left">Campaign Title</div>
                        <div class="data-column  align-left">Region</div>
                        <div class="data-column ">Campaign Type</div>
                        <div class="data-column advert-listing">Category</div>
                    </div>
                <?php } ?>

                <div class="data-content">
                    <div class="data-column advert-listing align-left current-ad-padding"><a href="customer-advertisment-detail.php?advert_id=<?php echo $rowPending['A_ID']; ?>&id=<?php echo $BID ?>"><?php echo $rowPending['A_Title']; ?></a></div>
                    <div class="data-column  align-left current-ad-padding"><?php echo $rowPending['R_Name']; ?></div>
                    <div class="data-column  current-ad-padding"><?php echo $rowPending['AT_Name']; ?></div>
                    <div class="data-column advert-listing  current-ad-padding">
                        <?php
                        if ($rowPending['A_C_ID'] != 0) {
                            if ($rowPending['RC_Name'] != '') {
                                echo $rowPending['RC_Name'];
                            } else {
                                echo $rowPending['C_Name'];
                            }
                        }
                        if ($rowPending['A_SC_ID'] != 0) {
                            echo " / " . $rowPendingsub['C_Name'];
                        }
                        ?>
                    </div>
                </div>

                <?php
                $pendingAds++;
                if ($PendingCount == $pendingAds) {
                    ?>
                </div>
                <?php
            }
        }
        ?>

        <!--Active ADS-->
        <?php
        $activeAds = 0;
        $sqlActive = " SELECT A_ID, A_Title, A_SC_ID, A_C_ID, A_AT_ID, AT_Name, C_Name, R_Name, RC_Name FROM tbl_Advertisement
                        LEFT JOIN tbl_Region ON A_Website = R_ID 
                        LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID 
                        LEFT JOIN tbl_Category ON A_C_ID = C_ID 
                        LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID AND A_Website = RC_R_ID
                        WHERE A_B_ID = '" . encode_strings($BID, $db) . "' AND A_Is_Deleted = 0 AND A_AT_ID NOT IN(3,4) AND A_Status = 3 AND (A_End_Date >= CURDATE() OR A_End_Date = 0000-00-00)";
        $resultActive = mysql_query($sqlActive, $db) or die("Invalid query: $sqlActive -- " . mysql_error());
        $ActiveCount = mysql_num_rows($resultActive);
        while ($rowActive = mysql_fetch_assoc($resultActive)) {
            $AT_ID = $rowActive['A_AT_ID'];
            $sqlActivesub = "SELECT * FROM tbl_Category WHERE C_ID = '" . encode_strings($rowActive['A_SC_ID'], $db) . "'";
            $resultActivesub = mysql_query($sqlActivesub, $db) or die("Invalid query: $sqlActivesub -- " . mysql_error());
            $rowActivesub = mysql_fetch_assoc($resultActivesub);
            if ($activeAds == 0) {
                ?>
                <div class="containter-adv">
                    <div class="content-header">
                        <div class="title">Active Campaigns</div>
                        <div class="link">

                        </div>
                    </div>
                    <div class="content-sub-header">
                        <div class="data-column advert-listing align-left">Campaign Title</div>
                        <div class="data-column  align-left">Region</div>
                        <div class="data-column ">Campaign Type</div>
                        <div class="data-column advert-listing ">Category</div>
                    </div>
                <?php } ?>

                <div class="data-content">
                    <div class="data-column advert-listing align-left current-ad-padding"><a href="customer-advertisment-detail.php?advert_id=<?php echo $rowActive['A_ID']; ?>&id=<?php echo $BID ?>"><?php echo $rowActive['A_Title']; ?></a></div>
                    <div class="data-column  align-left current-ad-padding"><?php echo $rowActive['R_Name']; ?></div>
                    <div class="data-column  current-ad-padding"><?php echo $rowActive['AT_Name']; ?></div>
                    <div class="data-column advert-listing current-ad-padding">
                        <?php
                        if ($rowActive['A_C_ID'] != 0) {
                            if ($rowActive['RC_Name'] != '') {
                                echo $rowActive['RC_Name'];
                            } else {
                                echo $rowActive['C_Name'];
                            }
                        }
                        if ($rowActive['A_SC_ID'] != 0) {
                            echo " / " . $rowActivesub['C_Name'];
                        }
                        ?>
                    </div>
                </div>

                <?php
                $activeAds++;
                if ($ActiveCount == $activeAds) {
                    ?>
                </div>
                <?php
            }
        }
        ?>
        <!-- Inactive datials start -->
        <?php
        $InactiveAds = 0;
        $sqlActive = " SELECT A_ID, A_Title, A_SC_ID, A_C_ID, A_AT_ID, AT_Name, C_Name, R_Name, RC_Name FROM tbl_Advertisement
                        LEFT JOIN tbl_Region ON A_Website = R_ID 
                        LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID 
                        LEFT JOIN tbl_Category ON A_C_ID = C_ID 
                        LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID AND A_Website = RC_R_ID
                        WHERE A_B_ID = '" . encode_strings($BID, $db) . "' AND A_Status = 4 AND A_AT_ID NOT IN(3,4) AND (A_End_Date >= CURDATE() OR A_End_Date = 0000-00-00)";
        $resultActive = mysql_query($sqlActive, $db) or die("Invalid query: $sqlActive -- " . mysql_error());
        $InactiveCount = mysql_num_rows($resultActive);
        while ($rowActive = mysql_fetch_assoc($resultActive)) {
            $AT_ID = $rowActive['A_AT_ID'];
            $sqlActivesub = "SELECT * FROM tbl_Category WHERE C_ID = '" . encode_strings($rowActive['A_SC_ID'], $db) . "'";
            $resultActivesub = mysql_query($sqlActivesub, $db) or die("Invalid query: $sqlActivesub -- " . mysql_error());
            $rowActivesub = mysql_fetch_assoc($resultActivesub);
            if ($InactiveAds == 0) {
                ?>
                <div class="containter-adv">
                    <div class="content-header">
                        <div class="title">Inactive Campaigns</div>
                        <div class="link">

                        </div>
                    </div>    
                    <div class="content-sub-header">
                        <div class="data-column advert-listing align-left">Campaign Title</div>
                        <div class="data-column  align-left">Region</div>
                        <div class="data-column ">Campaign Type</div>
                        <div class="data-column advert-listing ">Category</div>
                    </div>
                <?php } ?>

                <div class="data-content">
                    <div class="data-column advert-listing align-left current-ad-padding"><a href="customer-advertisment-detail.php?advert_id=<?php echo $rowActive['A_ID']; ?>&id=<?php echo $BID ?>"><?php echo $rowActive['A_Title']; ?></a></div>
                    <div class="data-column  align-left current-ad-padding"><?php echo $rowActive['R_Name']; ?></div>
                    <div class="data-column  current-ad-padding"><?php echo $rowActive['AT_Name']; ?></div>
                    <div class="data-column advert-listing current-ad-padding">
                        <?php
                        if ($rowActive['A_C_ID'] != 0) {
                            if ($rowActive['RC_Name'] != '') {
                                echo $rowActive['RC_Name'];
                            } else {
                                echo $rowActive['C_Name'];
                            }
                        }
                        if ($rowActive['A_SC_ID'] != 0) {
                            echo " / " . $rowActivesub['C_Name'];
                        }
                        ?>
                    </div>
                </div>

                <?php
                $InactiveAds++;
                if ($InactiveCount == $InactiveAds) {
                    ?>
                </div>
                <?php
            }
        }
        ?>
        <!-- Inactive datials end -->
        <!-- delete datials start -->
        <?php
        $deleteAds = 0;
        $sqlActive = " SELECT A_ID, A_Title, A_SC_ID, A_C_ID, A_AT_ID, AT_Name, C_Name, R_Name, RC_Name FROM tbl_Advertisement
                        LEFT JOIN tbl_Region ON A_Website = R_ID 
                        LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID 
                        LEFT JOIN tbl_Category ON A_C_ID = C_ID 
                        LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID AND A_Website = RC_R_ID
                        WHERE A_B_ID = '" . encode_strings($BID, $db) . "' AND A_Is_Deleted = 1 AND A_AT_ID NOT IN(3,4) AND (A_End_Date >= CURDATE() OR A_End_Date = 0000-00-00)";
        $resultActive = mysql_query($sqlActive, $db) or die("Invalid query: $sqlActive -- " . mysql_error());
        $deleteCount = mysql_num_rows($resultActive);
        while ($rowActive = mysql_fetch_assoc($resultActive)) {
            $AT_ID = $rowActive['A_AT_ID'];
            $sqlActivesub = "SELECT * FROM tbl_Category WHERE C_ID = '" . encode_strings($rowActive['A_SC_ID'], $db) . "'";
            $resultActivesub = mysql_query($sqlActivesub, $db) or die("Invalid query: $sqlActivesub -- " . mysql_error());
            $rowActivesub = mysql_fetch_assoc($resultActivesub);
            if ($deleteAds == 0) {
                ?>
                <div class="containter-adv">
                    <div class="content-header">
                        <div class="title">Delete Campaigns</div>
                        <div class="link">

                        </div>
                    </div>
                    <div class="content-sub-header">
                        <div class="data-column advert-listing align-left">Campaign Title</div>
                        <div class="data-column  align-left">Region</div>
                        <div class="data-column ">Campaign Type</div>
                        <div class="data-column advert-listing ">Category</div>
                    </div>
                <?php } ?>

                <div class="data-content">
                    <div class="data-column advert-listing align-left current-ad-padding"><a href="customer-advertisment-detail.php?advert_id=<?php echo $rowActive['A_ID']; ?>&id=<?php echo $BID ?>"><?php echo $rowActive['A_Title']; ?></a></div>
                    <div class="data-column  align-left current-ad-padding"><?php echo $rowActive['R_Name']; ?></div>
                    <div class="data-column  current-ad-padding"><?php echo $rowActive['AT_Name']; ?></div>
                    <div class="data-column advert-listing current-ad-padding">
                        <?php
                        if ($rowActive['A_C_ID'] != 0) {
                            if ($rowActive['RC_Name'] != '') {
                                echo $rowActive['RC_Name'];
                            } else {
                                echo $rowActive['C_Name'];
                            }
                        }
                        if ($rowActive['A_SC_ID'] != 0) {
                            echo " / " . $rowActivesub['C_Name'];
                        }
                        ?>
                    </div>
                </div>

                <?php
                $deleteAds++;
                if ($deleteCount == $deleteAds) {
                    ?>
                </div>
                <?php
            }
        }
        ?>
        <!-- delete datials end -->
        <!-- expired datials start -->
        <?php
        $expiredAds = 0;
        $sqlActive = " SELECT A_ID, A_Title, A_SC_ID, A_C_ID, A_AT_ID, AT_Name, C_Name, R_Name, RC_Name FROM tbl_Advertisement
                        LEFT JOIN tbl_Region ON A_Website = R_ID 
                        LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID 
                        LEFT JOIN tbl_Category ON A_C_ID = C_ID 
                        LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID AND A_Website = RC_R_ID
                        WHERE A_B_ID = '" . encode_strings($BID, $db) . "' AND A_AT_ID NOT IN(3,4) AND (A_End_Date < CURDATE() AND A_End_Date != 0000-00-00)";
        $resultActive = mysql_query($sqlActive, $db) or die("Invalid query: $sqlActive -- " . mysql_error());
        $expiredCount = mysql_num_rows($resultActive);
        while ($rowActive = mysql_fetch_assoc($resultActive)) {
            $AT_ID = $rowActive['A_AT_ID'];
            $sqlActivesub = "SELECT * FROM tbl_Category WHERE C_ID = '" . encode_strings($rowActive['A_SC_ID'], $db) . "'";
            $resultActivesub = mysql_query($sqlActivesub, $db) or die("Invalid query: $sqlActivesub -- " . mysql_error());
            $rowActivesub = mysql_fetch_assoc($resultActivesub);
            if ($expiredAds == 0) {
                ?>
                <div class="containter-adv">
                    <div class="content-header">
                        <div class="title">Expired Campaigns</div>
                        <div class="link">

                        </div>
                    </div>
                    <div class="content-sub-header">
                        <div class="data-column advert-listing align-left">Campaign Title</div>
                        <div class="data-column  align-left">Region</div>
                        <div class="data-column ">Campaign Type</div>
                        <div class="data-column advert-listing ">Category</div>
                    </div>
                <?php } ?>

                <div class="data-content">
                    <div class="data-column advert-listing align-left current-ad-padding"><a href="customer-advertisment-detail.php?advert_id=<?php echo $rowActive['A_ID']; ?>&id=<?php echo $BID ?>"><?php echo $rowActive['A_Title']; ?></a></div>
                    <div class="data-column  align-left current-ad-padding"><?php echo $rowActive['R_Name']; ?></div>
                    <div class="data-column  current-ad-padding"><?php echo $rowActive['AT_Name']; ?></div>
                    <div class="data-column advert-listing current-ad-padding">
                        <?php
                        if ($rowActive['A_C_ID'] != 0) {
                            if ($rowActive['RC_Name'] != '') {
                                echo $rowActive['RC_Name'];
                            } else {
                                echo $rowActive['C_Name'];
                            }
                        }
                        if ($rowActive['A_SC_ID'] != 0) {
                            echo " / " . $rowActivesub['C_Name'];
                        }
                        ?>
                    </div>
                </div>

                <?php
                $expiredAds++;
                if ($expiredCount == $expiredAds) {
                    ?>
                </div>
                <?php
            }
        }
        ?>
        <!-- expired datials end -->
    </div>
</div>    

<?PHP
require_once '../include/admin/footer.php';
?>