<?php
include '../include/config.inc.php';
include '../include/accommodation_functions.php';
require_once '../include/login.inc.php';
require_once '../include/admin/header.php';
include '../include/accommodation_header.php';

$pending_grp_block = mysql_query('SELECT * FROM `acc_group_block_info` WHERE status = 0 AND launch_status = 0');
$live_grp_block = mysql_query('SELECT * FROM `acc_group_block_info` WHERE status = 1');
$archived_grp_block = mysql_query('SELECT * FROM `acc_group_block_info`');
?>
<div class="content-left full-width">
    <?php if (isset($_SESSION['message'])) { ?>
        <div class="message">
            <?php
            echo $_SESSION['message'];
            unset($_SESSION['message']);
            ?>
        </div>
    <?php } ?>
    <div class="title-link">
        <div class="title">
            Group Blocks
        </div>
        <div class="addlisting">
            <a  href="add_group_block.php" class="add_group_block">+ Add Group Block</a>
        </div> 
    </div>
    <div class="right full-width">
        <div class="accordion-header first">
            <div  class="tt_ac_pending_header">
                Pending Group Blocks
            </div>
        </div>
        <div class="accordion-content">
            <div class="content-sub-header">
                <div class="data-column spl-name-accommodation padding-none">Name</div>
                <div class="data-column spl-other-cc-list padding-none">Start </div>
                <div class="data-column spl-other-cc-list padding-none">End</div>
                <div class="data-column spl-other-cc-list padding-none">Activate</div>
                <!--<div class="data-column spl-other-cc-list padding-none">Launch</div>-->
                <div class="data-column spl-other-cc-list padding-none"></div>
            </div>
            <?php
            if (mysql_num_rows($pending_grp_block) != 0) {
                while ($row_pending = mysql_fetch_array($pending_grp_block)) {
                    $start_end_date = group_block_info($row_pending["id"]);
                    $active_status = "<img src = 'images/active.png'/>";
                    if ($row_pending['status'] == 0) {
                        $id = $row_pending['id'];
                        $active_status = '<a class ="activate_group_block" href="javascript:openPopUp(' . $id . ',\'status\')"><img src = "images/activate.png"/></a>';
                        if ((isset($start_end_date['min_date']) && $start_end_date['min_date'] != NULL) && (isset($start_end_date['max_date']) && $start_end_date['max_date'] != NULL)) {
                            $active_status = '<a class ="activate_group_block" href="javascript:openPopUp(' . $id . ',\'status\')"><img src = "images/activate.png"/></a>';
                        } else {
                            $active_status = '<a onclick="return alert(\'Wait! You have not selected dates for the block.\')" class ="activate_group_block" style="cursor:pointer;"><img src = "images/activate.png"/></a>';
                        }
                    }
                    ?>
                    <div class="data-content">
                        <div class="data-column spl-name-accommodation"><?php echo "<a href='group_block_info.php?id=" . $row_pending["id"] . "'>" . $row_pending["name"] . "</a>"; ?> </div>
                        <div class="data-column spl-other-cc-list "><?php echo (isset($start_end_date['min_date']) && $start_end_date['min_date'] != NULL) ? $start_end_date['min_date'] : " - " ?> </div>
                        <div class="data-column spl-other-cc-list "><?php echo (isset($start_end_date['max_date']) && $start_end_date['max_date'] != NULL) ? $start_end_date['max_date'] : " - " ?> </div>
                        <div class="data-column spl-other-cc-list "><?php echo $active_status; ?> </div>
                        <div class="data-column spl-other-cc-list ">
                            <?php
                            if ((isset($start_end_date['min_date']) && $start_end_date['min_date'] != NULL) && (isset($start_end_date['max_date']) && $start_end_date['max_date'] != NULL)) {
                                echo "<a class='launch_group_block' href='javascript:openPopUp(" . $row_pending['id'] . ",\"launch\")'><img src='images/launch.png'/></a>";
                            } else {
                                echo "<a onclick='return alert(\"Wait! You have not selected dates for the block.\")' class='launch_group_block' style='cursor:pointer;'><img src='images/launch.png'/></a>";
                            }
                            ?>
                        </div>
                        <div class="data-column spl-other-cc-list"><?php echo "<a onclick='return confirm(\"Do you really want to delete this group block? All blocks and availability data will be deleted.\")'  href='delete_group_block.php?id=" . $row_pending["id"] . "'><img src='images/delete.png'/></a>"; ?></div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <form name="confirm_pass" method="GET" action="" id="confirm_pass" style="display: none;">
            <div style="float:left; margin-bottom: 20px;width: 100%;">
                <input type="text" id="u_name" name="u_name" value="<?php
                if (isset($_SESSION['USER_NAME'])) {
                    echo $_SESSION['USER_NAME'];
                }
                ?>" readonly="readonly" required="required" />
                <input type="password" name="u_pass" id="u_pass" required="required" placeholder="Password" />
                <input type="hidden" name="block_id" id="block_id" value="" />
                <input type="hidden" name="link_type" id="link_type" value="" />
            </div>
            <div style="float:left;text-align: center; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                <div class="form-data">
                    <input type="submit" name="button" value="Submit" onclick="return confirm_user();" />
                </div>
            </div>
        </form>
        <!--pending blocks ends-->
        <!--live groups start-->
        <div class="accordion-header">
            <div  class="tt_ac_pending_header">
                Active Group Blocks
            </div>
        </div>
        <div class="accordion-content" style="width:100% !important;">
            <div class="content-sub-header">
                <div class="data-column spl-name-accommodation padding-none">Name</div>
                <div class="data-column spl-other-cc-list padding-none">Start </div>
                <div class="data-column spl-other-cc-list padding-none">End</div>
                <div class="data-column spl-other-cc-list padding-none">Blocks</div>
                <div class="data-column spl-other-cc-list padding-none"></div>
                <div class="data-column spl-other-cc-list padding-none">Updated</div>
            </div>
            <?php
            if (mysql_num_rows($live_grp_block) > 0) {
                while ($row_live = mysql_fetch_array($live_grp_block)) {
                    $start_end_date = group_block_info($row_live['id']);
                    ?>
                    <div class="data-content">
                        <div class="data-column spl-name-accommodation"><?php echo "<a href='group_block_info.php?id=" . $row_live["id"] . "'>" . $row_live["name"] . "</a>"; ?> </div>
                        <div class="data-column spl-other-cc-list "><?php echo (isset($start_end_date['min_date']) && $start_end_date['min_date'] != NULL) ? $start_end_date['min_date'] : " - " ?> </div>
                        <div class="data-column spl-other-cc-list "><?php echo (isset($start_end_date['max_date']) && $start_end_date['max_date'] != NULL) ? $start_end_date['max_date'] : " - " ?> </div>
                        <div class="data-column spl-other-cc-list "><?php echo (isset($start_end_date['blocks']) && $start_end_date['blocks'] != NULL) ? $start_end_date['blocks'] : "-" ?> </div>
                        <div class="data-column spl-other-cc-list "><?php echo "<a onclick='return confirm(\"Do you really want to delete this block?\")' href='delete_group_block.php?id=" . $row_live["id"] . "'><img src='images/delete.png'/></a>"; ?></div>
                        <div class="data-column spl-other-cc-list "><?php echo $row_live['updated']; ?></div>
                    </div>
                    <?php
                }
            }
            ?>


        </div>
        <!--archived groups starts-->
        <div class="accordion-header">
            <div class="tt_ac_pending_header" >
                Archived Group Blocks
            </div>
        </div>
        <div class="accordion-content" style="width:100% !important;">
            <div class="content-sub-header">
                <div class="data-column spl-name-accommodation padding-none">Name</div>
                <div class="data-column spl-other-cc-list padding-none"></div>
                <div class="data-column spl-other-cc-list padding-none"></div>
                <div class="data-column spl-other-cc-list padding-none">Start </div>
                <div class="data-column spl-other-cc-list padding-none">End</div>
                <div class="data-column spl-other-cc-list padding-none"></div>

            </div>
            <?php
            if (mysql_num_rows($archived_grp_block) != 0) {
                while ($row_archive = mysql_fetch_array($archived_grp_block)) {
                    $start_end_date = group_block_info($row_archive["id"]);
                    $today = date('Y-m-d');
                    if ($today > date('Y-m-d', strtotime($start_end_date['max_date'])) && $start_end_date['max_date'] != NULL) {
                        ?>
                        <div class="data-content">
                            <div class="data-column spl-name-accommodation"><?php echo "<a href='group_block_info.php?id=" . $row_archive["id"] . "&archive=1'>" . $row_archive["name"] . "</a>"; ?> </div>
                            <div class="data-column spl-other-cc-list"></div>
                            <div class="data-column spl-other-cc-list"></div>
                            <div class="data-column spl-other-cc-list "><?php echo (isset($start_end_date['min_date']) && $start_end_date['min_date'] != NULL) ? $start_end_date['min_date'] : " - " ?> </div>
                            <div class="data-column spl-other-cc-list "><?php echo (isset($start_end_date['max_date']) && $start_end_date['max_date'] != NULL) ? $start_end_date['max_date'] : " - " ?> </div>
                        </div>
                        <?php
                    }
                }
            }
            ?>
        </div>
    </div>
</div>
<!--Archived groups ends-->
<?PHP
require_once '../include/admin/footer.php';
?>
	