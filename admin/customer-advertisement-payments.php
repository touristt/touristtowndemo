<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('superadmin', $_SESSION['USER_ROLES'])) {
    header("Location: /admin/");
    exit();
}
$select = "SELECT AT_Name, AT_ID, AT_Cost FROM tbl_Advertisement_Type WHERE AT_ID NOT IN(3,4)";
$result = mysql_query($select, $db) or die("Invalid query: $select -- " . mysql_error());

if (isset($_POST['butt']) && $_POST['butt'] == 'Save') {
    $select = "SELECT AT_ID, AT_Cost FROM tbl_Advertisement_Type WHERE AT_ID NOT IN(3,4)";
    $result = mysql_query($select, $db) or die("Invalid query: $select -- " . mysql_error());
    while ($row = mysql_fetch_array($result)) {
        if ($row['AT_Cost'] != $_POST['ad_cost_' . $row['AT_ID']]) {
            $update = "UPDATE tbl_Advertisement_Type SET AT_Cost = '" . $_POST['ad_cost_' . $row['AT_ID']] . "' WHERE AT_ID = '" . $_POST['ad_id_' . $row['AT_ID']] . "'";
            $up_res = mysql_query($update);
            if ($up_res) {
                $sql = "SELECT A_ID, A_Discount FROM tbl_Advertisement WHERE A_AT_ID = '" . $row['AT_ID'] . "'";
                $res = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                while ($advertrow = mysql_fetch_array($res)) {
                    if ($advertrow['A_Discount'] <= $_POST['ad_cost_' . $row['AT_ID']]) {
                        $total = $_POST['ad_cost_' . $row['AT_ID']] - $advertrow['A_Discount'];
                        $update = "UPDATE tbl_Advertisement SET A_Total = '" . $total . "' WHERE A_ID = '" . $advertrow['A_ID'] . "'";
                        $up_res = mysql_query($update);
                    }else{
                        $_SESSION['discount_is_greater'] = 1;
                    }
                }
                $_SESSION['success'] = 1;                
            } else {
                $_SESSION['error'] = 1;
            }
        }
    }
    // TRACK DATA ENTRY
    Track_Data_Entry('Campaign','','Campaign Payments','','Update','super admin');
    header("Location: customer-advertisement-payments.php");
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Campaigns</div>
        <div class="link">
        </div>
    </div>

    <div class="left advert-left-nav">
        <?php require_once '../include/nav-B-advertisement-admin.php'; ?>
    </div>

    <div class="right">
        <!--Step One-->
        <form name="form1" method="post" action="customer-advertisement-payments.php">
            <div class="content-header">Campaign Payments</div>
            <?php while ($row = mysql_fetch_array($result)) { ?>  
                <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                    <label><?php echo $row['AT_Name'] ?></label>
                    <span style="float: left;margin: 10px 0 0px -15px;">$</span>
                    <div class="form-data">
                        <input type="hidden" name="ad_id_<?php echo $row['AT_ID'] ?>" value="<?php echo $row['AT_ID'] ?>">
                        <input type="text" name="ad_cost_<?php echo $row['AT_ID'] ?>" value="<?php echo $row['AT_Cost'] ?>">
                    </div>
                </div>
            <?php } ?>
            <div class="form-inside-div border-none margin-bottom-26 margin-top-11 form-inside-div-width-admin">
                <div class="button">
                    <input onClick="return confirm('Are you sure, you want to change ad prices!');" type="submit" name="butt" value="Save"/>
                </div>
            </div>
        </form>
    </div>
</div>

<?PHP
require_once '../include/admin/footer.php';
?>