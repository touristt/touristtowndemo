<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';
if (!in_array('billing-history', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if (isset($_REQUEST['bl_id']) && $_REQUEST['bl_id'] > 0) {
    $sql = "SELECT B_ID, BL_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words, BL_Billing_Type, BL_Trial_Expiry, BL_Renewal_Date, BL_Line_Item_Title, BL_Line_Item_Price, BL_SubTotal, 
            BL_CoC_Dis_2, BL_Payment_Type, BL_Account_Manager, BL_Total FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID WHERE BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
    $BL_ID = $rowListing['BL_ID'];
} else {
    header("Location:customers.php");
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    if ($_POST['billingType'] == -1) {
        $multiplier = 12;
    } else if($_POST['billingType'] == 1){
        $multiplier = 1;
    }else if($_POST['billingType'] == 2){
       $multiplier = 2; 
    }
    listing_billing($_POST['billingType'], $BL_ID, $_POST['discount'], $_POST['title'], $_POST['price']);
    // TRACK DATA ENTRY
    $id = $_REQUEST['bl_id'];
    Track_Data_Entry('Listing',$id,'Billing','','Update','super admin');
    header("Location: customer-listing-billing.php?bl_id=" . $_REQUEST['bl_id']);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left">

    <?php require_once '../include/top-nav-listing.php'; ?>

    <div class="title-link">
        <div class="title">My Page - <?php echo $rowListing['BL_Listing_Title'] ?></div>
        <div class="instruction">
            Fields with this background<span></span>will show on free listings profile.
        </div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-mypage.php'; ?>
    </div>
    <div class="right">
        <form name="frmBilling" id="frmBilling" method="post" action="">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="bl_id" value="<?php echo $rowListing['BL_ID'] ?>">
            <input type="hidden" name="refresh" id="refresh" value="<?php echo isset($_REQUEST['refresh']); ?>">
            <div class="content-header">Billing</div>
            <div class="data-content">
                <div class="data-column inviced-tools-margin">Type of Billing</div>
                <div class="data-column">
                    <select name="billingType" id="billingType" onChange="$('#frmBilling').submit();">
                        <option value="0">Select One</option>
                        <option value="1" <?php echo $rowListing['BL_Billing_Type'] == 1 ? 'selected' : '' ?>>Monthly</option>
                        <option value="-1" <?php echo $rowListing['BL_Billing_Type'] == -1 ? 'selected' : '' ?>>Yearly</option>
                        <option value="2" <?php echo $rowListing['BL_Billing_Type'] == 2 ? 'selected' : '' ?>>Trial</option>
                    </select>
                </div>
            </div>
            <?PHP
            if ($rowListing['BL_Billing_Type'] <> 0) {
                ?>
                <?PHP
                if ($rowListing['BL_Billing_Type'] == -1) {
                    $multiplier = 12;
                } else if($rowListing['BL_Billing_Type'] == 1) {
                    $multiplier = 1;
                    echo "<input type='hidden' id='multiplierApplied' name='multiplierApplied' value='1'>";
                }else if($rowListing['BL_Billing_Type'] == 2){
                  $multiplier = 0;  
                }
                ?>
                <div class="data-content">
                    <div class="data-column inviced-tools-margin">Listing Type -                    
                        <?PHP
                        $sql = "SELECT LT_Name, LT_Cost FROM tbl_Listing_Type WHERE LT_ID = '" . encode_strings($rowListing['BL_Listing_Type'], $db) . "'";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        $rowLT = mysql_fetch_assoc($result);
                        echo $rowLT['LT_Name'];
                        ?>
                    </div>
                    <div class="data-column">
                        $<?php echo number_format(round($multiplier * $rowLT['LT_Cost'], 2), 2) ?>
                    </div>
                </div>
                <div class="data-header bold inviced-detail inviced-order-heading">Order Summary</div>
                <?PHP
                $sql = " SELECT * FROM tbl_BL_Feature LEFT JOIN tbl_Feature ON F_ID = BLF_F_ID
                         WHERE BLF_BL_ID = '" . encode_strings($rowListing['BL_ID'], $db) . "' AND F_Price > 0 AND BLF_Active = 1";
                $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                while ($row = mysql_fetch_assoc($resultTMP)) {
                    ?>
                    <div class="data-content">
                        <div class="data-column inviced-tools-margin"> <?php echo $row['F_Name'] ?></div>
                        <div class="data-column">$<?php echo number_format(round($multiplier * $row['F_Price'], 2), 2) ?></div>
                    </div>
                    <?PHP
                }
                
                //Advertisements
                $sql = " SELECT A_Title, A_Active_Date, A_Total, AT_Name FROM tbl_Advertisement LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID
                         WHERE A_BL_ID = '" . encode_strings($rowListing['BL_ID'], $db) . "' AND A_Status = 3 AND A_Total > 0
                         AND A_Is_Deleted = 0 AND A_Is_Townasset != 1 AND (A_End_Date > CURDATE() OR A_End_Date = '0000-00-00')";
                $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                $ads_total = 0;
                while ($row = mysql_fetch_assoc($resultTMP)) {
                    $renewalDate = strtotime('-1 month', strtotime($rowListing['BL_Renewal_Date']));
                    $renewalDate = date("Y-m-d", $renewalDate);
                    if ($renewalDate >= $row['A_Active_Date']) {
                        $ad_total = ($multiplier * $row['A_Total']);
                        $ads_total += $ad_total;
                    } else {
                        $datetime1 = strtotime(date('Y-m-d', strtotime($row['A_Active_Date'])));
                        $datetime2 = strtotime(date('Y-m-d', strtotime($rowListing['BL_Renewal_Date'])));

                        $secs = $datetime2 - $datetime1; // == <seconds between the two times>
                        $days = $secs / 86400;
                        $ad_total = ((($multiplier * $row['A_Total']) / 30) * $days);
                        if ($ad_total < 0) {
                            $ad_total = 0;
                        }
                        $ads_total += $ad_total;
                    }
                    if ($ad_total > 0) {
                        ?>
                        <div class="data-content">
                            <div class="data-column inviced-tools-margin"> <?php echo $row['A_Title'] . " - " . $row['AT_Name'] ?></div>
                            <div class="data-column">$<?php echo number_format($ad_total, 2) ?></div>
                        </div>
                        <?PHP
                    }
                }

                if (in_array('superadmin', $_SESSION['USER_ROLES'])) {
                    ?>
                    <div class="data-content">
                        <div class="data-column inviced-tools-margin" style="width:55% !important;">
                            <span>Add Line Item</span>
                            <input type="button" onclick="validate();" class="discount-button" value="Apply" />
                        </div>
                        <div class="data-column" style="float:right;width:40%;">
                            <input name="title" type="text" id="title" value="<?php echo $rowListing['BL_Line_Item_Title'] ?>" size="15" placeholder="Title"/>
                            <input name="price" type="text" id="price" value="<?php echo $rowListing['BL_Line_Item_Price'] ?>" size="15" placeholder="Price"/>
                        </div>
                    </div>
                    <?php
                }
                $subtotal = $rowListing['BL_SubTotal'] + $ads_total;
                $subtotal2 = $subtotal - $rowListing['BL_CoC_Dis_2'];
                $tax = round($subtotal2 * .13, 2);
                $total = $subtotal2 + $tax;
                ?>
                <div class="data-header sub-total-color">
                    <div class="data-column bold inviced-sub-margin ">Sub-Total 1</div>
                    <div class="data-column sub-total-color">$<?php echo ($rowListing['BL_Billing_Type'] == 2) ? '0' : number_format($subtotal, 2); ?></div>
                </div>
                <div class="data-content">
                    <div class="data-column bold inviced-sub-margin">
                        <span>Discount</span>
                        <input type="button" onclick="$('#frmBilling').submit();" class="discount-button" value="Apply Discount" />
                    </div>
                    <div class="data-column">
                        $<input name="discount" type="text" id="discount" value="<?php echo number_format($rowListing['BL_CoC_Dis_2'], 2) ?>" size="15" />
                    </div>
                </div>
                <div class="data-header sub-total-color">
                    <div class="data-column bold inviced-sub-margin sub-total-color">Sub-Total 2</div>
                    <div class="data-column sub-total-color">$<?php echo ($rowListing['BL_Billing_Type'] == 2) ? '0' : number_format($subtotal2, 2); ?></div>
                </div>
                <div class="data-content ">
                    <div class="data-column bold inviced-sub-margin">Taxes (13%)</div>
                    <div class="data-column">$<?php echo ($rowListing['BL_Billing_Type'] == 2) ? '0' : number_format($tax, 2); ?></div>
                </div>
                <div class="data-header total-invice-color ">
                    <div class="data-column bold inviced-sub-margin total-invice-color">Total</div>
                    <div class="data-column total-invice-color">$<?php echo ($rowListing['BL_Billing_Type'] == 2) ? '0' : number_format($total, 2); ?></div>
                </div>
                <?PHP
            }
            ?>
        </form>
        <?PHP
        if ($rowListing['BL_Billing_Type'] <> 0) {
            ?>
        <form method="POST" action="payment.php" name="payment_form">
                <div class="content-header" style="margin-top: 15px;"> <?php echo ($rowListing['BL_Billing_Type'] == 2) ? "Trial Expiry" : "Payment Details" ?></div>
                <div class="form-inside-div" <?php if($rowListing['BL_Billing_Type'] == 2){ ?> style="display: none;" <?php } ?>>
                    <label>Payment Total</label>
                    <div class="form-data" id="bl_total" style="padding: 17px 0 16px 20px;">$<?php echo ($rowListing['BL_Billing_Type'] == 2) ? '0' : number_format($total, 2); ?></div>
                </div>
                <?php if($rowListing['BL_Billing_Type'] == 1 || $rowListing['BL_Billing_Type'] == -1){ ?>
                <div class="form-inside-div">
                    <label>Payment Date</label>
                    <div class="form-data">
                        <input type="text" class="datepicker" name="renewaldate" value="<?php echo $rowListing['BL_Renewal_Date'] ?>" readonly />
                       </div>
                </div>
                 <?php } else { ?>
                <div class="form-inside-div">
                    <label>Expiry Date</label>
                    <div class="form-data">
                            <input type="text" class="previous-date-not-allowed" name="expire_date" value="<?php echo $rowListing['BL_Trial_Expiry'] ?>" readonly />
                     </div>
                </div>    
                     <?php } ?>
                            
                <div class="form-inside-div" <?php if($rowListing['BL_Billing_Type'] == 2){ ?> style="display: none;" <?php } ?>>
                    <label>Payment type</label>
                    <div class="form-data">                                     
                        <select name="paymentType" id="select" required>
                            <option value="">Pay By</option>
                            <?PHP
                            $sql = "SELECT PT_ID, PT_Name FROM tbl_Payment_Type ORDER BY PT_Name";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($row = mysql_fetch_assoc($result)) {
                                echo '<option value="' . $row['PT_ID'] . '"';
                                echo $rowListing['BL_Payment_Type'] == $row['PT_ID'] ? 'selected' : '';
                                echo '>' . $row['PT_Name'] . '</option>';
                            }
                            ?>
                        </select>  
                    </div>
                </div>
                <div class="form-inside-div" <?php if($rowListing['BL_Billing_Type'] == 2){ ?> style="display: none;" <?php } ?>>
                    <label>Acc. Manager</label>
                    <div class="form-data"> 
                        <select name="accountManager" id="select3" required>
                            <option value="">Select Account Manager</option>
                            <?PHP
                            $sql = "SELECT U_ID, U_F_Name, U_L_Name FROM tbl_User WHERE U_Commission != 0 ORDER BY U_ID";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($row = mysql_fetch_assoc($result)) {
                                echo '<option value="' . $row['U_ID'] . '"';
                                echo $rowListing['BL_Account_Manager'] == $row['U_ID'] ? 'selected' : '';
                                echo '>' . $row['U_F_Name'] . ' ' . $row['U_L_Name'] . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-inside-div">
                    <div class="button">
                        <input type="hidden" name="bl_id" value="<?php echo $rowListing['BL_ID'] ?>">
                        <input type="hidden" name="bid" value="<?php echo $BID ?>">
                        <input type="hidden" name="total_amount" value="<?php echo $rowListing['BL_Total'] ?>" />
                        <input type="submit" name="save" id="button4" value="Save" />
                        <input type="hidden" name="billing_section" value="1" />
                        <?php if($rowListing['BL_Billing_Type'] == 1 || $rowListing['BL_Billing_Type'] == -1){ ?>
                        <input type="submit" name="save" id="button4" value="Pay" />
                        <?php } ?>
                    </div>
                </div>
            </form>
            <?PHP
        }
        ?>
        </td>
        </tr>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        if ($("#refresh").val() == 1) {
            $('#frmBilling').submit()
        }
        $("input[name='renewaldate']").click(function() {
            $( "input[name='renewaldate']" ).datepicker({dateFormat:"yy-mm-dd"}).datepicker("setDate",new Date());
            var renewaldate = $(this).val();
            var renewalarray = renewaldate.split('-');
            var billing_type = $("#billingType").val();
            if(billing_type == -1) {
                if(renewalarray[2] > '27'){
                    if(renewalarray[1] == "12"){
                        renewaldate = (parseInt(renewalarray[0])+1) +'-01-02';
                    } else {
                        renewaldate = renewalarray[0] +'-'+ (parseInt(renewalarray[1])+1) +'-02';
                    }
                }
            }
            else {
                if(renewalarray[2] > '27'){
                    if(renewalarray[1] == "12"){
                        renewaldate = (parseInt(renewalarray[0])+1) +'-01-02';
                    } else {
                        renewaldate = renewalarray[0] +'-'+ (parseInt(renewalarray[1])+1) +'-02';
                    }
                }
            }
            $("input[name='renewaldate']").val(renewaldate);
        });
        $("input[name='renewaldate']").change(function() {
            var renewaldate = $(this).val();
            var renewalarray = renewaldate.split('-');
            var billing_type = $("#billingType").val();
            if(billing_type == -1) {
                if(renewalarray[2] > '27'){
                    if(renewalarray[1] == "12"){
                        renewaldate = (parseInt(renewalarray[0])+1) +'-01-02';
                    } else {
                        renewaldate = renewalarray[0] +'-'+ (parseInt(renewalarray[1])+1) +'-02';
                    }
                }
            }
            else {
                if(renewalarray[2] > '27'){
                    if(renewalarray[1] == "12"){
                        renewaldate = (parseInt(renewalarray[0])+1) +'-01-02';
                    } else {
                        renewaldate = renewalarray[0] +'-'+ (parseInt(renewalarray[1])+1) +'-02';
                    }
                }
            }
            $("input[name='renewaldate']").val(renewaldate);
        });
    });
    function validate(){
        if ($("#title").val() != '' && $("#price").val() != 0.00) {
            $('#frmBilling').submit();
        }else if ($("#title").val() != '' && $("#price").val() == 0.00) {
            alert('Please enter Price of Line Item!');
            return false;
        }else if ($("#title").val() == '' && $("#price").val() != 0.00) {
            alert('Please enter Title of Line Item!');
            return false;
        }else{
            $('#frmBilling').submit();
        }
    }
    function pad(n) {
        return (n < 10) ? ("0" + n) : n;
    }
</script>
<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>