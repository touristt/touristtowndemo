<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';
require_once '../include/image-bank-usage-function.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS']) && !in_array('manage-county-region', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} else {
    header('location: regions.php');
    exit();
}

$sql = "SELECT R_ID, R_Name, R_Type  FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);


if ($_REQUEST['id'] > 0) {
    $activeID = $_REQUEST['id'];
} else {
    header('location: regions.php');
    exit();
}

$sql = "SELECT C_ID, C_Parent, C_Name, RC_Name FROM tbl_Category 
        LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID AND RC_R_ID = " . $activeRegion['R_ID'] . " 
        LEFT JOIN tbl_Business ON RC_Spokesperson = B_ID 
        WHERE C_ID = '" . encode_strings($activeID, $db) . "' ORDER BY C_Name LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeCat = mysql_fetch_assoc($result);
if ($activeCat['C_Parent'] == 0) {
    $id = $activeCat['C_ID'];
} else {
    $id = $activeCat['C_Parent'];
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    require '../include/picUpload.inc.php';
    if ($_POST['image_bank_desktop'] == "") {
        // Region Header = 10, it creates 930X440 and store copy in image bank and local directory
        $slider_pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 10, 'Slider', $id);
        $slider_img = $slider_pic['0']['0'];
        $pic_id = $slider_pic['1'];
    } else {
        $pic_id = $_POST['image_bank_desktop']; 
        // last @param 10 = Region Header, it creates 930X440 and store copy in image bank and local directory
        $slider_pic = Upload_Pic_Library($pic_id, 10, 'Slider');
        $slider_img = $slider_pic['0'];
    }
    $sql_image = " INSERT tbl_Region_Category_Photos SET 
                    RCP_Slider_Title = '" . encode_strings($_REQUEST['sliderTitle'], $db) . "', 
                    RCP_Video = '" . encode_strings($_REQUEST['video'], $db) . "', 
                    RCP_Slider_Description = '" . encode_strings($_REQUEST['description'], $db) . "',
                    RCP_Image = '" . encode_strings($slider_img, $db) . "', 
                    RCP_C_ID = '" . $activeID . "', 
                    RCP_R_ID = '" . $activeRegion['R_ID'] . "'";
    $result = mysql_query($sql_image, $db) or die("Invalid query: $sql_image -- " . mysql_error());

    if (isset($result)) {
        $_SESSION['success'] = 1;
        $catVal = $activeCat['C_ID'];
        imageBankUsage($pic_id, 'IBU_R_ID', $regionID, 'IBU_C_ID ', $catVal, 'IBU_Region_Category', 1);
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $activeRegion['R_ID'], 'Region Category Sliders', $activeCat['C_ID'], 'Add Category Slider', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: category-slider.php?rid=" . $activeRegion['R_ID'] . "&id=" . $activeCat['C_ID']);
    exit();
}
if (isset($_POST['op']) && $_POST['op'] == 'update') {
    $sql = "Update tbl_Region_Category_Photos SET 
            RCP_Slider_Title = '" . encode_strings($_REQUEST['sliderTitle'], $db) . "', 
            RCP_Video = '" . encode_strings($_REQUEST['video'], $db) . "', 
            RCP_Slider_Description = '" . encode_strings($_REQUEST['description'], $db) . "' 
            where RCP_ID = '" . encode_strings($_REQUEST['image_id'], $db) . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $activeRegion['R_ID'], 'Region Category Sliders', $activeCat['C_ID'], 'Update Category Slider', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: category-slider.php?rid=" . $activeRegion['R_ID'] . "&id=" . $activeCat['C_ID']);
    exit();
}
if (isset($_GET['op']) && $_GET['op'] == 'del') {
    if (isset($_GET['img_name']) && $_GET['img_name'] != "") {
        $delete = "DELETE FROM tbl_Region_Category_Photos WHERE RCP_ID = '" . encode_strings($_REQUEST['rcpid'], $db) . "'";
        $result_del = mysql_query($delete);
        if ($result_del) {
            $_SESSION['delete'] = 1;
            //Delete from image usage when image is deleted
            $catVal = $activeCat['C_ID'];
            imageBankUsageDelete('IBU_R_ID', $regionID, 'IBU_C_ID', $catVal, 'IBU_Region_Category', 1);
            // TRACK DATA ENTRY
            Track_Data_Entry('Websites', $_REQUEST['rid'], 'Region Category Sliders', $_REQUEST['id'], 'Delete Category Slider', 'super admin');
            require '../include/picUpload.inc.php';
            Delete_Pic($_GET['img_name']);
        } else {
            $_SESSION['delete_error'] = 1;
        }
    }
    header('location: category-slider.php?rid=' . $_REQUEST['rid'] . '&id=' . $_REQUEST['id']);
    exit;
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - <?php echo $activeCat['C_Name'] ?></div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">Region Category Sliders</div>
        <div class="form-inside-div form-inside-div-width-admin"> 
            <label>Category</label>
            <div class="form-data">
                <?php if ($activeCat['RC_Name'] != '') { ?>
                    <input name="rname" type="text" id="textfield2" value="<?php echo $activeCat['RC_Name'] ?>" size="50" disabled style="background: none; border: none"/>
                <?php } else { ?>
                    <input name="rname" type="text" id="textfield2" value="<?php echo $activeCat['C_Name'] ?>" size="50" disabled style="background: none; border: none"/>
                <?php } ?>
            </div>
        </div>
        <?PHP
        $select = " SELECT * FROM tbl_Region_Category_Photos WHERE RCP_R_ID = '" . encode_strings($activeRegion['R_ID'], $db) . "' AND
                    RCP_C_ID = '" . encode_strings($activeID, $db) . "' order by RCP_Order";
        $result = mysql_query($select, $db) or die("Invalid query: $select -- " . mysql_error());
        $numRows = mysql_num_rows($result);
        $count = 1;
        ?>
        <form id="regionForm" name="form1" method="post" style="display: none" onSubmit="return check_img_size(4, 10000000)" enctype="multipart/form-data" action="/admin/category-slider.php">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="id" value="<?php echo $activeCat['C_ID'] ?>">
            <input type="hidden" name="rid" value="<?php echo $activeRegion['R_ID'] ?>">
            <?php echo (in_array('townTourist', $_SESSION['USER_PERMISSIONS'])) ? '<input type="hidden" name="townAdmin" value="1" />' : '' ?>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Slider Title</label>
                <div class="form-data">
                    <input name="sliderTitle" type="text" value="" size="50" maxlength="255"/> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Video Link</label>
                <div class="form-data">
                    <input name="video" type="text"  value="" size="255" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Description</label>
                <div class="form-data"> 
                    <textarea name="description" cols="85" rows="10" class="tt-description"></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin" >
                <label class="slider">Photo</label>
                <div class="form-data div_image_library cat-region-width">
                    <div class="div_image_library">
                        <span class="daily_browse" onclick="show_image_library(4)">Select File</span>
                        <input type="hidden" name="image_bank_desktop" id="image_bank4" value="">
                        <input type="hidden" name="image_count" value="<?php echo $numRows ?>">
                        <input onchange="show_file_name(4, this, 0)" id="photo4" type="file" name="pic[]" style="display: none;"/>
                    </div>
                    <div class="form-inside-div add-about-us-item-image margin-none">
                        <div class="cropit-image-preview aboutus-photo-perview">
                            <img class="preview-img preview-img-script4" style="display: none;" src="">    
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin border-none"> 
                <div class="button">
                    <input type="submit" name="button2" id="button22" value="Submit" />
                </div>
            </div>           
        </form>
        <div class="form-inside-div form-inside-div-width-admin border-none catgeory-slider-add-button">
            <?php if ($numRows < 10) { ?>
                <div class="add-link"><a onclick="show_form()">+Add Photo</a>
                </div>
            <?php } ?>
            <div class="photo-gallery-limit margin-right-15">
                <span>Photos <?php echo (($numRows == "") ? "0" : $numRows) ?> of 10</span>
            </div>
        </div>

        <script>
            function show_form() {
                $('#regionForm').show();
            }
        </script>
        <div class="form-inside-div border-none form-inside-div-width-admin region-slider-images" id="region-slider-list">
            <ul class="gallery category-slider-image">
                <?PHP
                while ($data = mysql_fetch_assoc($result)) {
                    ?>
                    <li class="image" id="recordsArray_<?php echo $data['RCP_ID'] ?>">
                        <div class="form-inside-div form-inside-div-width-admin"> 
                            <label>Photo <?php echo $count ?></label>
                            <div class="form-data div_image_library cat-region-width">
                                <a class="deletePhoto delete-region-cat-photo" onclick="return confirm('Are you sure you want to delete slider?');" href="category-slider.php?rid=<?php echo $data['RCP_R_ID'] ?>&id=<?php echo $data['RCP_C_ID'] ?>&op=del&img_name=<?php echo $data['RCP_Image'] ?>&rcpid=<?php echo $data['RCP_ID'] ?>">Delete Slider</a>
                                <div class="cropit-image-preview aboutus-photo-perview">  
                                    <?php if ($data['RCP_Image'] != "") { ?>
                                        <img class="existing-img" src="http://<?php echo DOMAIN . IMG_LOC_REL . $data['RCP_Image'] ?>">
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <form name="form1" method="post" action=""> 
                            <input type="hidden" name="id" value="<?php echo $activeCat['C_ID'] ?>">
                            <input type="hidden" name="rid" value="<?php echo $activeRegion['R_ID'] ?>">
                            <input type="hidden" name="image_id" value="<?php echo $data['RCP_ID'] ?>">
                            <input type="hidden" name="op" value="update">
                            <div class="form-inside-div form-inside-div-width-admin"> 
                                <label>Slider Title</label>
                                <div class="form-data">
                                    <input name="sliderTitle" type="text"  value="<?php echo $data['RCP_Slider_Title'] ?>" size="50" maxlength="255"/> 
                                </div>
                            </div>
                            <div class="form-inside-div form-inside-div-width-admin"> 
                                <label>Video Link</label>
                                <div class="form-data">
                                    <input name="video" type="text"  value="<?php echo $data['RCP_Video'] ?>" size="255" /> 
                                </div>
                            </div>
                            <div class="form-inside-div form-inside-div-width-admin"> 
                                <label>Description</label>
                                <div class="form-data"> 
                                    <textarea id="description_<?php echo $data['RCP_ID'] ?>" name="description" cols="85" rows="10" class="tt-description"><?php echo $data['RCP_Slider_Description'] ?></textarea>
                                </div>
                            </div>
                            <div class="form-inside-div  width-data-content border-none"> 
                                <div class="button"><input type="submit" name="update" id="button22" value="Submit" />
                                </div>
                            </div>
                        </form> 
                    </li>
                    <?PHP
                    $count++;
                }
                ?>
            </ul>
        </div>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>

<script>
    $(function () {
        $("#region-slider-list ul.gallery")
                .sortable({
                    opacity: 0.9,
                    cursor: 'move',
                    start: function (event, ui)
                    {
                        var id_textarea = ui.item.find(".tt-description").attr("id");
                        CKEDITOR.instances[id_textarea].destroy();
                    },
                    update: function () {
                        var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Region_Category_Photos&field=RCP_Order&id=RCP_ID';
                        $.post("reorder.php", order, function (theResponse) {
                            swal("Sliders Re-ordered!", "Sliders Re-ordered successfully.", "success");
                        });
                    },
                    stop: function (event, ui)
                    {
                        var id_textarea = ui.item.find(".tt-description").attr("id");
                        CKEDITOR.replace(id_textarea);
                    }
                });
    });
</script>


<?PHP
require_once '../include/admin/footer.php';
?>