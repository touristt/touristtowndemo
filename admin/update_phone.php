<?php
require_once '../include/config.inc.php';


$sql_listings = "SELECT BL_ID, BL_Listing_Title, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax FROM `tbl_Business_Listing`";
$res_listings = mysql_query($sql_listings);
$i = 0;
while($listing = mysql_fetch_assoc($res_listings)) {
  print 'BL_ID='. $listing['BL_ID'] . '-'. $listing['BL_Listing_Title'] .' and phone='. $listing['BL_Phone'] .' and tollfree='. $listing['BL_Toll_Free'] . ' and cell='. $listing['BL_Cell'] . ' and fax='. $listing['BL_Fax'] . '<hr>';
  $phone = preg_replace("/[^0-9,.]/", "", str_replace(array('.', ','),'',$listing['BL_Phone']));
  $tollfree = preg_replace("/[^0-9,.]/", "", str_replace(array('.', ','),'',$listing['BL_Toll_Free']));
  $cell = preg_replace("/[^0-9,.]/", "", str_replace(array('.', ','),'',$listing['BL_Cell']));
  $fax = preg_replace("/[^0-9,.]/", "", str_replace(array('.', ','),'',$listing['BL_Fax']));
  
  //format phone number field
  $formatted_phone = '';
  if($phone != '') {
    if($phone[0] == 1) {
      $formatted_phone = substr($phone, 0, 1) .'-'. substr($phone, 1, 3) .'-'. substr($phone, 4, 3) .'-'. substr($phone, 7, 4);
    }
    else {
      $formatted_phone = substr($phone, 0, 3) .'-'. substr($phone, 3, 3) .'-'. substr($phone, 6, 4);
    }
  }
  $formatted_tollfree = '';
  if($tollfree != '') {
    if($tollfree[0] == 1) {
      $formatted_tollfree = substr($tollfree, 0, 1) .'-'. substr($tollfree, 1, 3) .'-'. substr($tollfree, 4, 3) .'-'. substr($tollfree, 7, 4);
    }
    else {
      $formatted_tollfree = substr($tollfree, 0, 1) .'-'. substr($tollfree, 1, 3) .'-'. substr($tollfree, 4, 3) .'-'. substr($tollfree, 7, 4);
    }
  }
  $formatted_cell = '';
  if($cell != '') {
    if($cell[0] == 1) {
      $formatted_cell = substr($cell, 0, 1) .'-'. substr($cell, 1, 3) .'-'. substr($cell, 4, 3) .'-'. substr($cell, 7, 4);
    }
    else {
      $formatted_cell = substr($cell, 0, 3) .'-'. substr($cell, 3, 3) .'-'. substr($cell, 6, 4);
    }
  }
  $formatted_fax = '';
  if($fax != '') {
    if($fax[0] == 1) {
      $formatted_fax = substr($fax, 0, 1) .'-'. substr($fax, 1, 3) .'-'. substr($fax, 4, 3) .'-'. substr($fax, 7, 4);
    }
    else {
      $formatted_fax = substr($fax, 0, 3) .'-'. substr($fax, 3, 3) .'-'. substr($fax, 6, 4);
    }
  }
}

?>