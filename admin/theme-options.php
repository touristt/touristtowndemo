<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} else {
    header("Location: /admin/regions.php");
    exit();
}
$sql = "SELECT R_ID, R_Name, R_Type, R_My_Trip FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);
$sql = "SELECT * FROM tbl_Theme_Options WHERE TO_R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$count = mysql_num_rows($result);
$Region_theme = mysql_fetch_assoc($result);
if ($count > 0) {
    /*     * ******Exploding values to font[0]-size[1]-color[2]******* */
    $header_text_value = explode("-", $Region_theme['TO_Header_Text']);
    $season_text_value = explode("-", $Region_theme['TO_Season_Text']);
    $sec_nav_value = explode("-", $Region_theme['TO_Secondary_Navigation']);
    $main_nav_value = explode("-", $Region_theme['TO_Main_Navigation']);
    $slider_title_value = explode("-", $Region_theme['TO_Slider_Title']);
    $slider_desc_value = explode("-", $Region_theme['TO_Slider_Description']);
    $main_dd_value = explode("-", $Region_theme['TO_Drop_Down_Menu']);
    $main_page_title_value = explode("-", $Region_theme['TO_Main_Page_Title']);
    $main_page_body_value = explode("-", $Region_theme['TO_Main_Page_Body_Copy']);
    $cat_title_value = explode("-", $Region_theme['TO_Thumbnail_Category_Tite']);
    $view_all_value = explode("-", $Region_theme['TO_Thumbnail_View_All']);
    $sub_cat_value = explode("-", $Region_theme['TO_Thumbnail_Sub_Category_Title']);
    $listing_title_value = explode("-", $Region_theme['TO_Thumbnail_Listing_Title']);
    $coupon_title_value = explode("-", $Region_theme['TO_Thumbnail_Coupon_Title']);
    $homepage_register_event_value = explode("-", $Region_theme['TO_Homepage_Event_Register_Event_Text']);
    $event_main_title_value = explode("-", $Region_theme['TO_Homepage_Event_Main_Title']);
    $event_date_value = explode("-", $Region_theme['TO_Homepage_Event_Date']);
    $event_title_value = explode("-", $Region_theme['TO_Homepage_Event_Title']);
    $event_text_value = explode("-", $Region_theme['TO_Homepage_Event_Text']);
    $homepage_event_more_text_value = explode("-", $Region_theme['TO_Homepage_Event_More_Text']);
    $homepage_event_see_all_text_value = explode("-", $Region_theme['TO_Homepage_Event_See_All_Text']);
    $map_title_value = explode("-", $Region_theme['TO_Map_Filter_Font']);
    $footer_title_value = explode("-", $Region_theme['TO_Footer_Text_Title']);
    $footer_text_value = explode("-", $Region_theme['TO_Footer_Text']);
    $footer_links_value = explode("-", $Region_theme['TO_Footer_Links']);
    $footer_disc_value = explode("-", $Region_theme['TO_Footer_Disclaimer']);
    $listing_dd_text_value = explode("-", $Region_theme['TO_Listing_Drop_Down_Text']);
    $listing_title_text_value = explode("-", $Region_theme['TO_Listing_Title_Text']);
    $listing_title_location_value = explode("-", $Region_theme['TO_Listing_Location_Text']);
    $listing_title_navigation_value = explode("-", $Region_theme['TO_Listing_Navigation_Text']);
    $listing_location_text_value = explode("-", $Region_theme['TO_Listing_Location_Text']);
    $menu_text_value = explode("-", $Region_theme['TO_Menu_Text']);
    $listing_sub_heading_value = explode("-", $Region_theme['TO_Listing_Sub_Heading']);
    $listing_sub_nav_value = explode("-", $Region_theme['TO_Listing_Sub_Nav_Text']);
    $listing_day_text_value = explode("-", $Region_theme['TO_Listing_Day_Text']);
    $listing_hours_text_value = explode("-", $Region_theme['TO_Listing_Hours_Text']);
    $amenities_text_value = explode("-", $Region_theme['TO_Amenities_Text']);
    $download_text_value = explode("-", $Region_theme['TO_Downloads_Text']);
    $general_body_copy_value = explode("-", $Region_theme['TO_General_Body_Copy']);
    $whats_near_dd_value = explode("-", $Region_theme['TO_Whats_Nearby_Drop_Down_Text']);
    $gallery_desc_text_value = explode("-", $Region_theme['TO_Photo_Gallery_Description_Text']);
    $get_this_coupon_title_text_value = explode("-", $Region_theme['TO_Get_This_Coupon_Title_Text']);
    $get_this_coupon_title_description_value = explode("-", $Region_theme['TO_Get_This_Coupon_Description_Text']);
    $calender_dd_text_value = explode("-", $Region_theme['TO_Calendar_Drop_Down_Text']);
    $calender_dd_go_button_text_value = explode("-", $Region_theme['TO_Calendar_Go_Button_Text']);
    $detail_page_event_main_title_value = explode("-", $Region_theme['TO_Event_Page_Main_Title']);
    $detail_page_event_date_value = explode("-", $Region_theme['TO_Event_Page_Date']);
    $detail_page_event_data_label_value = explode("-", $Region_theme['TO_Event_Page_Data_Label']);
    $detail_page_event_data_content_value = explode("-", $Region_theme['TO_Event_Page_Data_Content']);
    $pagination_number_text_value = explode("-", $Region_theme['TO_Pagination_Number_Text']);
    $pagination_text_value = explode("-", $Region_theme['TO_Pagination_Text']);
    $to_trip_planner_number_value = explode("-", $Region_theme['TO_Trip_Planner_Number']);
    $text_box_text_inside_search_box_value = explode("-", $Region_theme['TO_Text_Box_Text_Inside_Box']);
    
    $search_box_button_text_value = explode("-", $Region_theme['TO_Search_Box_Button_Text']);
    
    $story_main_title_value = explode("-", $Region_theme['TO_Story_Page_Main_Title']);
    $story_share_text_value = explode("-", $Region_theme['TO_Story_Share_Text']);
    $story_content_title_value = explode("-", $Region_theme['TO_Story_Page_Content_Title']);
    $story_drop_down_text_value = explode("-", $Region_theme['TO_Story_Page_Drop_Down_Text']);
    $story_see_all_text_value = explode("-", $Region_theme['TO_Story_See_All_Text']);
    $route_main_title_value = explode("-", $Region_theme['TO_Route_Page_Main_Title']);
    $route_data_label_value = explode("-", $Region_theme['TO_Route_Page_Data_Label']);
    $route_data_content_value = explode("-", $Region_theme['TO_Route_Page_Data_Content']);
    $to_navigation_bar_bg_color = explode(">", $Region_theme['TO_Navigation_Bar']);
    $to_navigation_bar_bg = $to_navigation_bar_bg_color[0];
    $to_navigation_bar_color = $to_navigation_bar_bg_color[1];
    $home_des_bg_color = explode(">", $Region_theme['TO_Homepage_Description_Background']);
    $home_des_bg = $home_des_bg_color[0];
    $home_des_color = $home_des_bg_color[1];
    $listing_nav_gallary_bg_color = explode(">", $Region_theme['TO_Listing_Nav_Gallary_Background']);
    $listing_nav_gallary_bg = $listing_nav_gallary_bg_color[0];
    $listing_nav_gallary_color = $listing_nav_gallary_bg_color[1];
    $listing_whats_nearby_bg_color = explode(">", $Region_theme['TO_Listing_Whats_Nearby_Background']);
    $listing_whats_nearby_bg = $listing_whats_nearby_bg_color[0];
    $listing_whats_nearby_color = $listing_whats_nearby_bg_color[1];
    $to_bar_texture_bg_color = explode(">", $Region_theme['TO_Bar_Texture']);
    $to_bar_texture_bg = $to_bar_texture_bg_color[0];
    $to_bar_texture_color = $to_bar_texture_bg_color[1];
    $to_homepage_events_bg_color = explode(">", $Region_theme['TO_Homepage_Events_Description_Background']);
    $to_homepage_events_bg = $to_homepage_events_bg_color[0];
    $to_homepage_events_color = $to_homepage_events_bg_color[1];
}

if (isset($_POST['save_color'])) {

    $header_text = $_REQUEST['header_font'] . "-" . $_REQUEST['header_size'] . "-" . $_REQUEST['themeOption_header_text'];
    $season_text = $_REQUEST['season_font'] . "-" . $_REQUEST['season_size'] . "-" . $_REQUEST['themeOption_season_text'];
    $sec_nav = $_REQUEST['sec_nav_font'] . "-" . $_REQUEST['sec_nav_size'] . "-" . $_REQUEST['themeOption_sec_nav_text'];
    $main_nav = $_REQUEST['main_nav_font'] . "-" . $_REQUEST['main_nav_size'] . "-" . $_REQUEST['themeOption_main_nav_text'];
    $slider_title = $_REQUEST['slider_title_font'] . "-" . $_REQUEST['slider_title_size'] . "-" . $_REQUEST['themeOption_slider_title'];
    $slider_desc = $_REQUEST['slider_desc_font'] . "-" . $_REQUEST['slider_desc_size'] . "-" . $_REQUEST['themeOption_slider_desc'];
    $main_dd = $_REQUEST['main_dd_font'] . "-" . $_REQUEST['main_dd_size'] . "-" . $_REQUEST['themeOption_main_dd'];
    $main_page_title = $_REQUEST['main_page_title_font'] . "-" . $_REQUEST['main_page_title_size'] . "-" . $_REQUEST['themeOption_main_page_title'];
    $main_page_body = $_REQUEST['main_page_body_font'] . "-" . $_REQUEST['main_page_body_size'] . "-" . $_REQUEST['themeOption_main_page_body'];
    $cat_title = $_REQUEST['cat_title_font'] . "-" . $_REQUEST['cat_title_size'] . "-" . $_REQUEST['themeOption_cat_title'];
    $view_all = $_REQUEST['view_all_font'] . "-" . $_REQUEST['view_all_size'] . "-" . $_REQUEST['themeOption_view_all'];
    $sub_cat = $_REQUEST['sub_cat_font'] . "-" . $_REQUEST['sub_cat_size'] . "-" . $_REQUEST['themeOption_sub_cat'];
    $listing_title = $_REQUEST['listing_title_font'] . "-" . $_REQUEST['listing_title_size'] . "-" . $_REQUEST['themeOption_listing_title'];
    $coupon_title = $_REQUEST['coupon_title_font'] . "-" . $_REQUEST['coupon_title_size'] . "-" . $_REQUEST['themeOption_coupon_title'];
    $homepage_register_event = $_REQUEST['homepage_register_event_font'] . "-" . $_REQUEST['homepage_register_event_size'] . "-" . $_REQUEST['themeOption_homepage_register_event'];
    $event_main_title = $_REQUEST['event_main_title_font'] . "-" . $_REQUEST['event_main_title_size'] . "-" . $_REQUEST['themeOption_event_main_title'];
    $event_date = $_REQUEST['event_date_font'] . "-" . $_REQUEST['event_date_size'] . "-" . $_REQUEST['themeOption_event_date'];
    $event_title = $_REQUEST['event_title_font'] . "-" . $_REQUEST['event_title_size'] . "-" . $_REQUEST['themeOption_event_title'];
    $event_text = $_REQUEST['event_text_font'] . "-" . $_REQUEST['event_text_size'] . "-" . $_REQUEST['themeOption_event_text'];
    $homepage_event_more_text = $_REQUEST['homepage_event_more_text_font'] . "-" . $_REQUEST['homepage_event_more_text_size'] . "-" . $_REQUEST['themeOption_homepage_event_more_text'];
    $homepage_event_see_all_text = $_REQUEST['homepage_event_see_all_text_font'] . "-" . $_REQUEST['homepage_event_see_all_text_size'] . "-" . $_REQUEST['themeOption_homepage_event_see_all_text'];
    $footer_title = $_REQUEST['footer_title_font'] . "-" . $_REQUEST['footer_title_size'] . "-" . $_REQUEST['themeOption_footer_title'];
    $footer_text = $_REQUEST['footer_text_font'] . "-" . $_REQUEST['footer_text_size'] . "-" . $_REQUEST['themeOption_footer_text'];
    $footer_links = $_REQUEST['footer_links_font'] . "-" . $_REQUEST['footer_links_size'] . "-" . $_REQUEST['themeOption_footer_links'];
    $footer_disc = $_REQUEST['footer_disc_font'] . "-" . $_REQUEST['footer_disc_size'] . "-" . $_REQUEST['themeOption_footer_disc'];
    $listing_dd_text = $_REQUEST['listing_dd_text_font'] . "-" . $_REQUEST['listing_dd_text_size'] . "-" . $_REQUEST['themeOption_listing_dd_text'];
    $listing_title_text = $_REQUEST['listing_title_text_font'] . "-" . $_REQUEST['listing_title_text_size'] . "-" . $_REQUEST['themeOption_listing_title_text'];
    $listing_location_text = $_REQUEST['listing_location_text_font'] . "-" . $_REQUEST['listing_location_text_size'] . "-" . $_REQUEST['themeOption_listing_location_text'];
    $listing_navigation_text = $_REQUEST['listing_navigation_text_font'] . "-" . $_REQUEST['listing_navigation_text_size'] . "-" . $_REQUEST['themeOption_listing_navigation_text'];
    $general_body_copy = $_REQUEST['general_body_copy_font'] . "-" . $_REQUEST['general_body_copy_size'] . "-" . $_REQUEST['themeOption_general_body_copy'];
    $menu_text = $_REQUEST['menu_text_font'] . "-" . $_REQUEST['menu_text_size'] . "-" . $_REQUEST['themeOption_menu_text'];
    $listing_sub_heading = $_REQUEST['listing_sub_heading_font'] . "-" . $_REQUEST['listing_sub_heading_size'] . "-" . $_REQUEST['themeOption_listing_sub_heading'];
    $listing_sub_nav = $_REQUEST['listing_sub_nav_font'] . "-" . $_REQUEST['listing_sub_nav_size'] . "-" . $_REQUEST['themeOption_listing_sub_nav'];
    $listing_day_text = $_REQUEST['listing_hours_day_font'] . "-" . $_REQUEST['listing_hours_day_size'] . "-" . $_REQUEST['themeOption_listing_hours_day'];
    $listing_hours_text = $_REQUEST['listing_hours_text_font'] . "-" . $_REQUEST['listing_hours_text_size'] . "-" . $_REQUEST['themeOption_listing_hours_text'];
    $amenities_text = $_REQUEST['amenities_text_font'] . "-" . $_REQUEST['amenities_text_size'] . "-" . $_REQUEST['themeOption_amenities_text'];
    $download_text = $_REQUEST['download_text_font'] . "-" . $_REQUEST['download_text_size'] . "-" . $_REQUEST['themeOption_download_text'];
    $whats_near_dd = $_REQUEST['whats_near_dd_font'] . "-" . $_REQUEST['whats_near_dd_size'] . "-" . $_REQUEST['themeOption_whats_near_dd'];
    $gallery_desc_text = $_REQUEST['gallery_desc_text_font'] . "-" . $_REQUEST['gallery_desc_text_size'] . "-" . $_REQUEST['themeOption_gallery_desc_text'];
    $get_this_coupon_title_text = $_REQUEST['get_this_coupon_title_font'] . "-" . $_REQUEST['get_this_coupon_title_size'] . "-" . $_REQUEST['themeOption_get_this_coupon_title'];
    $get_this_coupon_desc_text = $_REQUEST['get_this_coupon_description_font'] . "-" . $_REQUEST['get_this_coupon_description_size'] . "-" . $_REQUEST['themeOption_get_this_coupon_description'];
    $calender_dd_text = $_REQUEST['calender_dd_text_font'] . "-" . $_REQUEST['calender_dd_text_size'] . "-" . $_REQUEST['themeOption_calender_dd_text'];
    $calender_dd_go_button_text = $_REQUEST['calender_dd_go_button_text_font'] . "-" . $_REQUEST['calender_dd_go_button_text_size'] . "-" . $_REQUEST['themeOption_calender_dd_go_button_text'];
    $detail_page_event_title = $_REQUEST['detail_page_event_main_title_font'] . "-" . $_REQUEST['detail_page_event_main_title_size'] . "-" . $_REQUEST['themeOption_detail_page_event_main_title'];
    $detail_page_event_date = $_REQUEST['detail_page_event_date_font'] . "-" . $_REQUEST['detail_page_event_date_size'] . "-" . $_REQUEST['themeOption_detail_page_event_date'];
    $detail_page_event_data_label = $_REQUEST['detail_page_event_data_label_font'] . "-" . $_REQUEST['detail_page_event_data_label_size'] . "-" . $_REQUEST['themeOption_detail_page_event_data_label'];
    $detail_page_event_data_content = $_REQUEST['detail_page_event_data_content_font'] . "-" . $_REQUEST['detail_page_event_data_content_size'] . "-" . $_REQUEST['themeOption_detail_page_event_data_content'];
    $story_main_title = $_REQUEST['story_main_title_font'] . "-" . $_REQUEST['story_main_title_size'] . "-" . $_REQUEST['themeOption_story_main_title'];
    $story_share_text = $_REQUEST['story_share_text_font'] . "-" . $_REQUEST['story_share_text_size'] . "-" . $_REQUEST['themeOption_story_share_text'];
    $story_content_title = $_REQUEST['story_content_title_font'] . "-" . $_REQUEST['story_content_title_size'] . "-" . $_REQUEST['themeOption_story_content_title'];
    $story_drop_down_text = $_REQUEST['story_drop_down_text_font'] . "-" . $_REQUEST['story_drop_down_text_size'] . "-" . $_REQUEST['themeOption_story_drop_down_text'];
    $story_see_all_text = $_REQUEST['story_see_all_text_font'] . "-" . $_REQUEST['story_see_all_text_size'] . "-" . $_REQUEST['themeOption_story_see_all_text'];
    $route_title = $_REQUEST['route_main_title_font'] . "-" . $_REQUEST['route_main_title_size'] . "-" . $_REQUEST['themeOption_route_main_title'];
    $route_data_label = $_REQUEST['route_data_label_font'] . "-" . $_REQUEST['route_data_label_size'] . "-" . $_REQUEST['themeOption_route_data_label'];
    $route_data_content = $_REQUEST['route_data_content_font'] . "-" . $_REQUEST['route_data_content_size'] . "-" . $_REQUEST['themeOption_route_data_content'];
    $pagination_number_text = $_REQUEST['pagination_number_text_font'] . "-" . $_REQUEST['pagination_number_text_size'] . "-" . $_REQUEST['themeOption_pagination_number_text'];
    $pagination_text = $_REQUEST['pagination_text_font'] . "-" . $_REQUEST['pagination_text_size'] . "-" . $_REQUEST['themeOption_pagination_text'];
    $to_trip_planner_number_text = $_REQUEST['trip_planner_number_font'] . "-" . $_REQUEST['trip_planner_number_size'] . "-" . $_REQUEST['trip_planner_number'];
    $text_box_text_inside_search_box = $_REQUEST['text_box_text_inside_search_box_font'] . "-" . $_REQUEST['text_box_text_inside_search_box_size'] . "-" . $_REQUEST['text_box_text_inside_search_box_number'];
    $search_box_button_text = $_REQUEST['search_box_button_text_font'] . "-" . $_REQUEST['search_box_button_text_font_size'] . "-" . $_REQUEST['search_box_button_text_number'];
    /*     * *Concatenating font-size-color** */

    $sql = "tbl_Theme_Options SET 
                TO_R_ID = $regionID,
                TO_Header_Text = '" . encode_strings($header_text, $db) . "',
                TO_Header_Background_Color = '" . encode_strings($_REQUEST['themeOption_header_bg_color'], $db) . "',
                TO_Logo_Space_Above = '" . encode_strings($_REQUEST['logo_space_above'], $db) . "',
                TO_Logo_Space_Below = '" . encode_strings($_REQUEST['logo_space_below'], $db) . "',
                TO_Logo_Background_Color = '" . encode_strings($_REQUEST['themeOption_logo_bg_color'], $db) . "',
                TO_Search_Box_Background_Color = '" . encode_strings($_REQUEST['themeOption_search_box_bg'], $db) . "',
                TO_Search_Box_Background_Button_Color = '" . encode_strings($_REQUEST['themeOption_search_box_btn_bg'], $db) . "',
                TO_Text_Box_Border_Inside_Search_Box = '" . encode_strings($_REQUEST['themeOption_text_box_border_inside_search_box'], $db) . "',
                TO_Text_Box_Text_Inside_Box = '" . encode_strings($text_box_text_inside_search_box, $db) . "',
                TO_Search_Box_Button_Text = '" . encode_strings($search_box_button_text, $db) . "',
                TO_Trip_Planner_Number_Background = '" . encode_strings($_REQUEST['trip_planner_number_background'], $db) . "',
                TO_Trip_Planner_Number = '" . encode_strings($to_trip_planner_number_text, $db) . "',
                TO_Season_Text = '" . encode_strings($season_text, $db) . "',
                TO_Season_Drop_Down_Background = '" . encode_strings($_REQUEST['themeOption_season_drop_down_background'], $db) . "',
                TO_Season_Drop_Down_Arrow = '" . encode_strings($_REQUEST['themeOption_season_drop_down_arrow'], $db) . "',
                TO_Secondary_Navigation = '" . encode_strings($sec_nav, $db) . "',
                TO_Main_Navigation = '" . encode_strings($main_nav, $db) . "',
                TO_Homepage_Description_Space = '" . encode_strings($_REQUEST['description_space'], $db) . "',
                TO_Slider_Title = '" . encode_strings($slider_title, $db) . "',
                TO_Slider_Description = '" . encode_strings($slider_desc, $db) . "',
                TO_Drop_Down_Menu = '" . encode_strings($main_dd, $db) . "',
                TO_Drop_Down_Menu_BG = '" . encode_strings($_REQUEST['themeOption_main_dd_bg'], $db) . "',
                TO_Main_Page_Title = '" . encode_strings($main_page_title, $db) . "',
                TO_Main_Page_Body_Copy = '" . encode_strings($main_page_body, $db) . "',
                TO_Thumbnail_Category_Tite = '" . encode_strings($cat_title, $db) . "',
                TO_Thumbnail_View_All = '" . encode_strings($view_all, $db) . "',
                TO_Thumbnail_Sub_Category_Title = '" . encode_strings($sub_cat, $db) . "',
                TO_Thumbnail_Listing_Title = '" . encode_strings($listing_title, $db) . "',
                TO_Thumbnail_Coupon_Title = '" . encode_strings($coupon_title, $db) . "',
                TO_Homepage_Event_Register_Event_Text = '" . encode_strings($homepage_register_event, $db) . "',
                TO_Homepage_Event_Main_Title = '" . encode_strings($event_main_title, $db) . "',
                TO_Homepage_Event_Date = '" . encode_strings($event_date, $db) . "',
                TO_Homepage_Event_Title = '" . encode_strings($event_title, $db) . "',
                TO_Homepage_Event_Text = '" . encode_strings($event_text, $db) . "',
                TO_Homepage_Event_More_Text = '" . encode_strings($homepage_event_more_text, $db) . "',
                TO_Homepage_Event_See_All_Text = '" . encode_strings($homepage_event_see_all_text, $db) . "',
                TO_Text_Link = '" . encode_strings($_REQUEST['TO_Text_Link'], $db) . "',
                TO_Footer_Text_Title = '" . encode_strings($footer_title, $db) . "',
                TO_Footer_Text = '" . encode_strings($footer_text, $db) . "',
                TO_Footer_Links = '" . encode_strings($footer_links, $db) . "',
                TO_Footer_Disclaimer = '" . encode_strings($footer_disc, $db) . "',
                TO_Footer_Background_Color = '" . encode_strings($_REQUEST['themeOption_footer_background'], $db) . "',
                TO_Thumbnail_Coupon_Title_Background_Color = '" . encode_strings($_REQUEST['coupon_title_background_color'], $db) . "',
                TO_Footer_Lines_Color = '" . encode_strings($_REQUEST['themeOption_footer_lines'], $db) . "',
                TO_Listing_Drop_Down_Text = '" . encode_strings($listing_dd_text, $db) . "',
                TO_Listing_Drop_Down_Background = '" . encode_strings($_REQUEST['themeOption_listing_dd_background'], $db) . "',
                TO_Listing_Drop_Down_Arrow = '" . encode_strings($_REQUEST['themeOption_listing_dd_arrow'], $db) . "',
                TO_Listing_Title_Text = '" . encode_strings($listing_title_text, $db) . "',
                TO_Listing_Location_Text = '" . encode_strings($listing_location_text, $db) . "',
                TO_Listing_Navigation_Text = '" . encode_strings($listing_navigation_text, $db) . "',
                TO_Menu_Text = '" . encode_strings($menu_text, $db) . "',
                TO_Listing_Sub_Heading = '" . encode_strings($listing_sub_heading, $db) . "',
                TO_Listing_Sub_Nav_Text = '" . encode_strings($listing_sub_nav, $db) . "',
                TO_Listing_Day_Text = '" . encode_strings($listing_day_text, $db) . "',
                TO_Listing_Hours_Text = '" . encode_strings($listing_hours_text, $db) . "',
                TO_Amenities_Text = '" . encode_strings($amenities_text, $db) . "',
                TO_Downloads_Text = '" . encode_strings($download_text, $db) . "',
                TO_General_Body_Copy = '" . encode_strings($general_body_copy, $db) . "',
                TO_Whats_Nearby_Drop_Down_Text = '" . encode_strings($whats_near_dd, $db) . "',
                TO_Whats_Nearby_Drop_Down_Background = '" . encode_strings($_REQUEST['themeOption_whats_nearby_dd_background'], $db) . "',
                TO_Whats_Nearby_Drop_Down_Arrow = '" . encode_strings($_REQUEST['themeOption_whats_nearby_dd_arrow'], $db) . "',
                TO_General_Body_Copy_Line_Spacing = '" . encode_strings($_REQUEST['general_body_copy_space'], $db) . "',
                TO_Photo_Gallery_Description_Text = '" . encode_strings($gallery_desc_text, $db) . "',
                TO_Get_This_Coupon_Title_Text = '" . encode_strings($get_this_coupon_title_text, $db) . "',
                TO_Get_This_Coupon_Description_Text = '" . encode_strings($get_this_coupon_desc_text, $db) . "',
                TO_Photo_Gallery_Background_Colour = '" . encode_strings($_REQUEST['themeOption_photo_gallery_bg_colour'], $db) . "',
                TO_Calendar_Drop_Down_Background = '" . encode_strings($_REQUEST['themeOption_calendar_dd_bg'], $db) . "',
                TO_Calendar_Drop_Down_Text = '" . encode_strings($calender_dd_text, $db) . "',
                TO_Calendar_Drop_Down_Outline = '" . encode_strings($_REQUEST['themeOption_calendar_dd_outline'], $db) . "',
                TO_Calendar_Go_Button_Background = '" . encode_strings($_REQUEST['themeOption_calendar_go_btn_bg'], $db) . "',
                TO_Calendar_Go_Button_Text = '" . encode_strings($calender_dd_go_button_text, $db) . "',
                TO_Event_Page_Main_Title = '" . encode_strings($detail_page_event_title, $db) . "',
                TO_Event_Page_Date = '" . encode_strings($detail_page_event_date, $db) . "',
                TO_Event_Page_Data_Label = '" . encode_strings($detail_page_event_data_label, $db) . "',
                TO_Event_Page_Data_Content	 = '" . encode_strings($detail_page_event_data_content, $db) . "',
                TO_Event_Page_Data_Border_Color	 = '" . encode_strings($_REQUEST['themeOption_detail_page_event_border_color'], $db) . "',
                TO_Story_Page_Main_Title = '" . encode_strings($story_main_title, $db) . "',
                TO_Story_Share_Text = '" . encode_strings($story_share_text, $db) . "',
                TO_Story_Page_Content_Title = '" . encode_strings($story_content_title, $db) . "',
                TO_Story_Page_Drop_Down_Text = '" . encode_strings($story_drop_down_text, $db) . "',
                TO_Story_Page_Drop_Down_Background = '" . encode_strings($_REQUEST['themeOption_story_drop_down_bg'], $db) . "',
                TO_Story_Page_Drop_Down_Arrow = '" . encode_strings($_REQUEST['themeOption_story_drop_down_arrow'], $db) . "',
                TO_Story_See_All_Text = '" . encode_strings($story_see_all_text, $db) . "',
                TO_Route_Page_Main_Title = '" . encode_strings($route_title, $db) . "',
                TO_Route_Page_Data_Label = '" . encode_strings($route_data_label, $db) . "',
                TO_Route_Page_Data_Content = '" . encode_strings($route_data_content, $db) . "',
                TO_Pagination_Number_Text = '" . encode_strings($pagination_number_text, $db) . "',
                TO_Pagination_Number_Background = '" . encode_strings($_REQUEST['themeOption_pagination_bg_no'], $db) . "',
                TO_Pagination_Selected_Number_Background = '" . encode_strings($_REQUEST['themeOption_pagination_bg_no_sel'], $db) . "',
                TO_Pagination_Line = '" . encode_strings($_REQUEST['themeOption_pagination_line'], $db) . "',
                TO_Pagination_Text = '" . encode_strings($pagination_text, $db) . "',
                TO_Desktop_Logo_Alt = '" . encode_strings($_REQUEST['desktop_logo_alt'], $db) . "'";


    /*     * * Images/Icons Uploading Section ** */
    require_once '../include/picUpload.inc.php';
    $navigation_bar_bgs = '';
    $navigation_bar_icon = Upload_Pic_Normal('0', 'navigation_bar_bg', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($navigation_bar_icon) {
        $navigation_bar_bgs = $navigation_bar_icon . ">" . $_REQUEST['navigation_bar'];
        if ($to_navigation_bar_bg) {
            Delete_Pic(IMG_ICON_ABS . $to_navigation_bar_bg);
        }
    } else {
        $navigation_bar_bgs = $to_navigation_bar_bg . ">" . $_REQUEST['navigation_bar'];
    }
    $sql .= ", TO_Navigation_Bar = '" . encode_strings($navigation_bar_bgs, $db) . "'";

    $home_des_bgs = '';
    $home_des_icon = Upload_Pic_Normal('0', 'home_des_bg', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($home_des_icon) {
        $home_des_bgs = $home_des_icon . ">" . $_REQUEST['home_des'];
        if ($home_des_bg) {
            Delete_Pic(IMG_ICON_ABS . $home_des_bg);
        }
    } else {
        $home_des_bgs = $home_des_bg . ">" . $_REQUEST['home_des'];
    }

    $sql .= ", TO_Homepage_Description_Background = '" . encode_strings($home_des_bgs, $db) . "'";

    $listing_nav_gallary_bgs = '';
    $listing_nav_gallary_icon = Upload_Pic_Normal('0', 'listing_nav_gallary_bg', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($listing_nav_gallary_icon) {
        $listing_nav_gallary_bgs = $listing_nav_gallary_icon . ">" . $_REQUEST['listing_nav_gallary'];
        if ($listing_nav_gallary_bg) {
            Delete_Pic(IMG_ICON_ABS . $listing_nav_gallary_bg);
        }
    } else {
        $listing_nav_gallary_bgs = $listing_nav_gallary_bg . ">" . $_REQUEST['listing_nav_gallary'];
    }

    $sql .= ", TO_Listing_Nav_Gallary_Background = '" . encode_strings($listing_nav_gallary_bgs, $db) . "'";

    $listing_whats_nearby_bgs = '';
    $listing_whats_nearby_icon = Upload_Pic_Normal('0', 'listing_whats_nearby_bg', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($listing_whats_nearby_icon) {
        $listing_whats_nearby_bgs = $listing_whats_nearby_icon . ">" . $_REQUEST['listing_whats_nearby'];
        if ($listing_whats_nearby_bg) {
            Delete_Pic(IMG_ICON_ABS . $listing_whats_nearby_bg);
        }
    } else {
        $listing_whats_nearby_bgs = $listing_whats_nearby_bg . ">" . $_REQUEST['listing_whats_nearby'];
    }

    $sql .= ", TO_Listing_Whats_Nearby_Background = '" . encode_strings($listing_whats_nearby_bgs, $db) . "'";

    $bar_texture_bgs = '';
    $bar_texture_icon = Upload_Pic_Normal('0', 'bar_texture_bg', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($bar_texture_icon) {
        $bar_texture_bgs = $bar_texture_icon . ">" . $_REQUEST['bar_texture'];
        if ($to_bar_texture_bg) {
            Delete_Pic(IMG_ICON_ABS . $to_bar_texture_bg);
        }
    } else {
        $bar_texture_bgs = $to_bar_texture_bg . ">" . $_REQUEST['bar_texture'];
    }

    $sql .= ", TO_Bar_Texture = '" . encode_strings($bar_texture_bgs, $db) . "'";

    $homepage_events_bgs = '';
    $homepage_events_icon = Upload_Pic_Normal('0', 'homepage_events_bg', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($homepage_events_icon) {
        $homepage_events_bgs = $homepage_events_icon . ">" . $_REQUEST['homepage_events'];
        if ($to_homepage_events_bg) {
            Delete_Pic(IMG_ICON_ABS . $to_homepage_events_bg);
        }
    } else {
        $homepage_events_bgs = $to_homepage_events_bg . ">" . $_REQUEST['homepage_events'];
    }

    $sql .= ", TO_Homepage_Events_Description_Background = '" . encode_strings($homepage_events_bgs, $db) . "'";

    $slider_overlay_icon = Upload_Pic_Normal('0', 'slider_overlay', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($slider_overlay_icon) {
        $sql .= ", TO_Slider_Overlay = '" . encode_strings($slider_overlay_icon, $db) . "'";
        if ($Region_theme['TO_Slider_Overlay']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Slider_Overlay']);
        }
    }
    $thumbnail_arrow_left_icon = Upload_Pic_Normal('0', 'thumbnail_arrow_left', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($thumbnail_arrow_left_icon) {
        $sql .= ", TO_Thumbnail_Arrow_Left = '" . encode_strings($thumbnail_arrow_left_icon, $db) . "'";
        if ($Region_theme['TO_Thumbnail_Arrow_Left']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Thumbnail_Arrow_Left']);
        }
    }
    $thumbnail_arrow_right_icon = Upload_Pic_Normal('0', 'thumbnail_arrow_right', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($thumbnail_arrow_right_icon) {
        $sql .= ", TO_Thumbnail_Arrow_Right = '" . encode_strings($thumbnail_arrow_right_icon, $db) . "'";
        if ($Region_theme['TO_Thumbnail_Arrow_Right']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Thumbnail_Arrow_Right']);
        }
    }

    $secondary_slider_overlay_icon = Upload_Pic_Normal('0', 'secondary_slider_overlay', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($secondary_slider_overlay_icon) {
        $sql .= ", TO_Secondary_Slider_Overlay = '" . encode_strings($secondary_slider_overlay_icon, $db) . "'";
        if ($Region_theme['TO_Secondary_Slider_Overlay']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Secondary_Slider_Overlay']);
        }
    }

    $Homepage_Event_Icon = Upload_Pic_Normal('0', 'Homepage_Event_Icon', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($Homepage_Event_Icon) {
        $sql .= ", TO_Homepage_Event_Icon = '" . encode_strings($Homepage_Event_Icon, $db) . "'";
        if ($Region_theme['TO_Homepage_Event_Icon']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Homepage_Event_Icon']);
        }
    }
    $Top_Page_Icon = Upload_Pic_Normal('0', 'Top_Page_Icon', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($Top_Page_Icon) {
        $sql .= ", TO_Top_Page_Icon = '" . encode_strings($Top_Page_Icon, $db) . "'";
        if ($Region_theme['TO_Top_Page_Icon']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Top_Page_Icon']);
        }
    }
    // 
    $scroll_down_arrow = Upload_Pic_Normal('0', 'Scroll_Down_Arrow', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($scroll_down_arrow) {
        $sql .= ", TO_Scroll_Down_Arrow = '" . encode_strings($scroll_down_arrow, $db) . "'";
        if ($Region_theme['TO_Scroll_Down_Arrow']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Scroll_Down_Arrow']);
        }
    }
    // 
    $whats_nearby_overlay_Icon = Upload_Pic_Normal('0', 'whats_nearby_overlay', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($whats_nearby_overlay_Icon) {
        $sql .= ", TO_Whats_Nearby_Overlay = '" . encode_strings($whats_nearby_overlay_Icon, $db) . "'";
        if ($Region_theme['TO_Whats_Nearby_Overlay']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Whats_Nearby_Overlay']);
        }
    }

    $desktop_logo = Upload_Pic_Normal('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true);
    if ($desktop_logo) {
        $sql .= ", TO_Desktop_Logo = '" . encode_strings($desktop_logo, $db) . "'";
        if ($Region_theme['TO_Desktop_Logo']) {
            Delete_Pic(IMG_LOC_ABS . $Region_theme['TO_Desktop_Logo']);
        }
    }
    
    $pic1 = Upload_Pic('0', 'pic1', 0, 0, true, IMG_LOC_ABS, 10000000, true, 22, 'Default Header Image');
    if (is_array($pic1)) {
        $sql .= ", TO_Default_Header_Desktop = '" . encode_strings($pic1['0']['0'], $db) . "',
                    TO_Default_Header_Mobile = '" . encode_strings($pic1['0']['1'], $db) . "'";
    }

    $pic2 = Upload_Pic('0', 'pic2', 0, 0, true, IMG_LOC_ABS, 10000000, true, 21, 'Default Header Thumbnail');
    if (is_array($pic2)) {
        $sql .= ", TO_Default_Thumbnail_Desktop = '" . encode_strings($pic2['0']['0'], $db) . "',
                    TO_Default_Thumbnail_Mobile = '" . encode_strings($pic2['0']['1'], $db) . "'";
    }

    if ($count > 0) {
        $sql = "UPDATE " . $sql . " WHERE TO_R_ID = '" . encode_strings($regionID, $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        // TRACK DATA ENTRY
        $id = $regionID;
        Track_Data_Entry('Websites', $id, 'Theme Options', '', 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        // TRACK DATA ENTRY
        $id = $regionID;
        Track_Data_Entry('Websites', $id, 'Theme Options', '', 'Add', 'super admin');
    }

    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: theme-options.php?rid=" . $regionID);
    exit();
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    require_once '../include/picUpload.inc.php';
    $update = "UPDATE tbl_Theme_Options SET";
    if ($_REQUEST['flag'] == 'navbar') {
        $update .= " TO_Navigation_Bar = '>" . encode_strings($to_navigation_bar_color, $db) . "'";
        if ($to_navigation_bar_bg) {
            Delete_Pic(IMG_ICON_ABS . $to_navigation_bar_bg);
        }
    }
    if ($_REQUEST['flag'] == 'slider_ol') {
        $update .= " TO_Slider_Overlay = ''";
        if ($Region_theme['TO_Slider_Overlay']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Slider_Overlay']);
        }
    }
    if ($_REQUEST['flag'] == 'home_d_bg') {
        $update .= " TO_Homepage_Description_Background = '>" . encode_strings($home_des_color, $db) . "'";
        if ($home_des_bg) {
            Delete_Pic(IMG_ICON_ABS . $home_des_bg);
        }
    }
    if ($_REQUEST['flag'] == 'thumb_arrow_left') {
        $update .= " TO_Thumbnail_Arrow_Left = ''";
        if ($Region_theme['TO_Thumbnail_Arrow_Left']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Thumbnail_Arrow_Left']);
        }
    }
    if ($_REQUEST['flag'] == 'thumb_arrow_right') {
        $update .= " TO_Thumbnail_Arrow_Right = ''";
        if ($Region_theme['TO_Thumbnail_Arrow_Right']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Thumbnail_Arrow_Right']);
        }
    }
    if ($_REQUEST['flag'] == 'bar_text_bg') {
        $update .= " TO_Bar_Texture = '>" . encode_strings($to_bar_texture_color, $db) . "'";
        if ($to_bar_texture_bg) {
            Delete_Pic(IMG_ICON_ABS . $to_bar_texture_bg);
        }
    }
    if ($_REQUEST['flag'] == 's_s_overlay') {
        $update .= " TO_Secondary_Slider_Overlay = ''";
        if ($Region_theme['TO_Secondary_Slider_Overlay']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Secondary_Slider_Overlay']);
        }
    }
    if ($_REQUEST['flag'] == 'home_event_d_bg') {
        $update .= " TO_Homepage_Events_Description_Background = '>" . encode_strings($to_homepage_events_color, $db) . "'";
        if ($to_homepage_events_bg) {
            Delete_Pic(IMG_ICON_ABS . $to_homepage_events_bg);
        }
    }
    if ($_REQUEST['flag'] == 'listing_nav_g_bg') {
        $update .= " TO_Listing_Nav_Gallary_Background = '>" . encode_strings($listing_nav_gallary_color, $db) . "'";
        if ($listing_nav_gallary_bg) {
            Delete_Pic(IMG_ICON_ABS . $listing_nav_gallary_bg);
        }
    }
    if ($_REQUEST['flag'] == 'listing_whats_nearby_bg') {
        $update .= " TO_Listing_Whats_Nearby_Background = '>" . encode_strings($listing_whats_nearby_color, $db) . "'";
        if ($listing_whats_nearby_bg) {
            Delete_Pic(IMG_ICON_ABS . $listing_whats_nearby_bg);
        }
    }
    if ($_REQUEST['flag'] == 'home_event_icon') {
        $update .= " TO_Homepage_Event_Icon = ''";
        if ($Region_theme['TO_Homepage_Event_Icon']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Homepage_Event_Icon']);
        }
    }
    if ($_REQUEST['flag'] == 'top_page_icon') {
        $update .= " TO_Top_Page_Icon = ''";
        if ($Region_theme['TO_Top_Page_Icon']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Top_Page_Icon']);
        }
    }
    if ($_REQUEST['flag'] == 'scroll_down_arrow') {
        $update .= " TO_Scroll_Down_Arrow = ''";
        if ($Region_theme['TO_Scroll_Down_Arrow']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Scroll_Down_Arrow']);
        }
    }
    if ($_REQUEST['flag'] == 'whats_nb_overlay') {
        $update .= " TO_Whats_Nearby_Overlay = ''";
        if ($Region_theme['TO_Whats_Nearby_Overlay']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Whats_Nearby_Overlay']);
        }
    }
    if ($_REQUEST['flag'] == 'desktop_logo') {
        $update .= " TO_Desktop_Logo = ''";
        if ($Region_theme['TO_Desktop_Logo']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Desktop_Logo']);
        }
    }
    if ($_REQUEST['flag'] == 'desktop_header') {
        $update .= " TO_Default_Header_Desktop = '', TO_Default_Header_Mobile = ''";
        if ($Region_theme['TO_Default_Header_Desktop']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Default_Header_Desktop']);
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Default_Header_Mobile']);
        }
    }
    
    if ($_REQUEST['flag'] == 'desktop_thumbnail') {
        $update .= " TO_Default_Thumbnail_Desktop = '', TO_Default_Thumbnail_Mobile = ''";
        if ($Region_theme['TO_Default_Thumbnail_Desktop']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Default_Thumbnail_Desktop']);
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Default_Thumbnail_Mobile']);
        }
    }
    if ($_REQUEST['flag'] == 'map_icon') { 
        $update .= " TO_Map_Icon = ''";
        if ($Region_theme['TO_Map_Icon']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Map_Icon']);
        }
    }
    $update .= " WHERE TO_R_ID = '" . encode_strings($regionID, $db) . "'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $regionID;
        Track_Data_Entry('Websites', $id, 'Theme Options', '', 'Delete', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header('location: theme-options.php?rid=' . $_REQUEST['rid']);
    exit;
}

$getFonts = "SELECT TOF_Name, TOF_ID FROM tbl_Theme_Options_Fonts ORDER BY TOF_Name";

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Theme Options</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <form id="regionForm" action="/admin/theme-options.php" method="post" enctype="multipart/form-data" name="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <div class="content-header">Manage Theme</div>
            <fieldset>
                <legend>Header</legend>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Header Bar Background Color</label>
                    <div class="form-data theme-options">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_header_bg_color" style="background-color: <?php echo (($Region_theme['TO_Header_Background_Color'] != "") ? $Region_theme['TO_Header_Background_Color'] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_header_bg_color" id="text_colorthemeOption_header_bg_color" value="<?php echo (($Region_theme['TO_Header_Background_Color'] != "") ? $Region_theme['TO_Header_Background_Color'] : '') ?>">
                        <?php if (isset($Region_theme['TO_Header_Background_Color']) && $Region_theme['TO_Header_Background_Color'] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete1"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Header_Background_Color'; ?>', 'themeOption_header_bg_color', 'hideDelete1', 'text_colorthemeOption_header_bg_color', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <?php if (isset($activeRegion['R_Type']) && ($activeRegion['R_Type'] != 6)) { ?>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Header Bar Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="header_font">
                                <option value="">Select Font...</option>
                                <?php
                                $res_fonts = mysql_query($getFonts);
                                while ($row_fonts = mysql_fetch_assoc($res_fonts)) {
                                    ?>
                                    <option style="font-family: <?php echo $row_fonts['TOF_Name']; ?>" <?php echo (($header_text_value[0] == $row_fonts['TOF_ID']) ? "selected" : "") ?> value="<?php echo $row_fonts['TOF_ID'] ?>"><?php echo $row_fonts['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="header_size" size="50" value="<?php echo $header_text_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_header_text" style="background-color: <?php echo (($header_text_value[2] != "") ? $header_text_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_header_text" id="text_colorthemeOption_header_text" value="<?php echo (($header_text_value[2] != "") ? $header_text_value[2] : '') ?>">
                            <?php if (isset($header_text_value[2]) && $header_text_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete2"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Header_Text'; ?>', 'themeOption_header_text', 'hideDelete2', 'text_colorthemeOption_header_text', '<?php echo $header_text_value[0]; ?>', '<?php echo $header_text_value[1]; ?>', '<?php echo $header_text_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } if ((isset($activeRegion['R_Type']) && $activeRegion['R_Type'] != 6) && $regionID != 26) { ?>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Header Bar Drop Down Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="season_font">
                                <option value="">Select Font...</option>
                                <?php
                                $season_fonts = mysql_query($getFonts);
                                while ($season_font = mysql_fetch_assoc($season_fonts)) {
                                    ?>
                                    <option style="font-family: <?php echo $season_font['TOF_Name']; ?>" <?php echo (($season_text_value[0] == $season_font['TOF_ID']) ? "selected" : "") ?> value="<?php echo $season_font['TOF_ID'] ?>"><?php echo $season_font['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="season_size" size="50" value="<?php echo $season_text_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_season_text" style="background-color: <?php echo (($season_text_value[2] != "") ? $season_text_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_season_text" id="text_colorthemeOption_season_text" value="<?php echo (($season_text_value[2] != "") ? $season_text_value[2] : '') ?>">
                            <?php if (isset($season_text_value[2]) && $season_text_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete3"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Season_Text'; ?>', 'themeOption_season_text', 'hideDelete3', 'text_colorthemeOption_season_text', '<?php echo $season_text_value[0]; ?>', '<?php echo $season_text_value[1]; ?>', '<?php echo $season_text_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Header Bar Drop Down Background</label>
                        <div class="form-data theme-options">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_season_drop_down_background" style="background-color: <?php echo (($Region_theme['TO_Season_Drop_Down_Background'] != "") ? $Region_theme['TO_Season_Drop_Down_Background'] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_season_drop_down_background" id="text_colorthemeOption_season_drop_down_background" value="<?php echo (($Region_theme['TO_Season_Drop_Down_Background'] != "") ? $Region_theme['TO_Season_Drop_Down_Background'] : '') ?>">
                            <?php if (isset($Region_theme['TO_Season_Drop_Down_Background']) && $Region_theme['TO_Season_Drop_Down_Background'] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete4"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Season_Drop_Down_Background'; ?>', 'themeOption_season_drop_down_background', 'hideDelete4', 'text_colorthemeOption_season_drop_down_background', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Header Bar Drop Down Arrow</label>
                        <div class="form-data theme-options">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_season_drop_down_arrow" style="background-color: <?php echo (($Region_theme['TO_Season_Drop_Down_Arrow'] != "") ? $Region_theme['TO_Season_Drop_Down_Arrow'] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_season_drop_down_arrow" id="text_colorthemeOption_season_drop_down_arrow" value="<?php echo (($Region_theme['TO_Season_Drop_Down_Arrow'] != "") ? $Region_theme['TO_Season_Drop_Down_Arrow'] : '') ?>">
                            <?php if (isset($Region_theme['TO_Season_Drop_Down_Arrow']) && $Region_theme['TO_Season_Drop_Down_Arrow'] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete5"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Season_Drop_Down_Arrow'; ?>', 'themeOption_season_drop_down_arrow', 'hideDelete5', 'text_colorthemeOption_season_drop_down_arrow', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Header Space Above Logo</label>
                    <div class="form-data theme-options">    
                        <input class="theme-options-size" type="text" name="logo_space_above" size="50" value="<?php echo $Region_theme['TO_Logo_Space_Above']; ?>" placeholder="Enter space">
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Header Space Below Logo</label>
                    <div class="form-data theme-options">    
                        <input class="theme-options-size" type="text" name="logo_space_below" size="50" value="<?php echo $Region_theme['TO_Logo_Space_Below']; ?>" placeholder="Enter space">
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Logo Background Color</label>
                    <div class="form-data theme-options">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_logo_bg_color" style="background-color: <?php echo (($Region_theme['TO_Logo_Background_Color'] != "") ? $Region_theme['TO_Logo_Background_Color'] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_logo_bg_color" id="text_colorthemeOption_logo_bg_color" value="<?php echo (($Region_theme['TO_Logo_Background_Color'] != "") ? $Region_theme['TO_Logo_Background_Color'] : '') ?>">
                        <?php if (isset($Region_theme['TO_Logo_Background_Color']) && $Region_theme['TO_Logo_Background_Color'] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete6"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Logo_Background_Color'; ?>', 'themeOption_logo_bg_color', 'hideDelete6', 'text_colorthemeOption_logo_bg_color', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Search Box Background</label>
                    <div class="form-data theme-options">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_search_box_bg" style="background-color: <?php echo (($Region_theme['TO_Search_Box_Background_Color'] != "") ? $Region_theme['TO_Search_Box_Background_Color'] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_search_box_bg" id="text_colorthemeOption_search_box_bg" value="<?php echo (($Region_theme['TO_Search_Box_Background_Color'] != "") ? $Region_theme['TO_Search_Box_Background_Color'] : '') ?>">
                        <?php if (isset($Region_theme['TO_Search_Box_Background_Color']) && $Region_theme['TO_Search_Box_Background_Color'] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete611"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Search_Box_Background_Color'; ?>', 'themeOption_search_box_bg', 'hideDelete611', 'text_colorthemeOption_search_box_bg', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <?php if (isset($activeRegion['R_Type']) && $activeRegion['R_Type'] != 6) { ?>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Search Box Button Background</label>
                    <div class="form-data theme-options">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_search_box_btn_bg" style="background-color: <?php echo (($Region_theme['TO_Search_Box_Background_Button_Color'] != "") ? $Region_theme['TO_Search_Box_Background_Button_Color'] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_search_box_btn_bg" id="text_colorthemeOption_search_box_btn_bg" value="<?php echo (($Region_theme['TO_Search_Box_Background_Button_Color'] != "") ? $Region_theme['TO_Search_Box_Background_Button_Color'] : '') ?>">
                        <?php if (isset($Region_theme['TO_Search_Box_Background_Button_Color']) && $Region_theme['TO_Search_Box_Background_Button_Color'] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete612"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Search_Box_Background_Button_Color'; ?>', 'themeOption_search_box_btn_bg', 'hideDelete612', 'text_colorthemeOption_search_box_btn_bg', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Text Box Border Inside Search Box</label>
                    <div class="form-data theme-options">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_text_box_border_inside_search_box" style="background-color: <?php echo (($Region_theme['TO_Text_Box_Border_Inside_Search_Box'] != "") ? $Region_theme['TO_Text_Box_Border_Inside_Search_Box'] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_text_box_border_inside_search_box" id="text_colorthemeOption_text_box_border_inside_search_box" value="<?php echo (($Region_theme['TO_Text_Box_Border_Inside_Search_Box'] != "") ? $Region_theme['TO_Text_Box_Border_Inside_Search_Box'] : '') ?>">
                        <?php if (isset($Region_theme['TO_Text_Box_Border_Inside_Search_Box']) && $Region_theme['TO_Text_Box_Border_Inside_Search_Box'] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete613"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Text_Box_Border_Inside_Search_Box'; ?>', 'themeOption_text_box_border_inside_search_box', 'hideDelete613', 'text_colorthemeOption_text_box_border_inside_search_box', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Text Box Text Inside Search Box</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="text_box_text_inside_search_box_font">
                            <option value="">Select Font...</option>
                            <?php
                            $getFonts = "SELECT TOF_Name, TOF_ID  FROM tbl_Theme_Options_Fonts ORDER BY TOF_Name";
                            $res_fonts = mysql_query($getFonts);
                            while ($row_fonts = mysql_fetch_assoc($res_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $row_fonts['TOF_Name']; ?>" <?php echo (($text_box_text_inside_search_box_value[0] == $row_fonts['TOF_ID']) ? "selected" : "") ?> value="<?php echo $row_fonts['TOF_ID'] ?>"><?php echo $row_fonts['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="text_box_text_inside_search_box_size" size="50" value="<?php echo $text_box_text_inside_search_box_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_text_box_text_inside_search_box_number" style="background-color: <?php echo (($text_box_text_inside_search_box_value[2] != "") ? $text_box_text_inside_search_box_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="text_box_text_inside_search_box_number" id="text_colorthemeOption_text_box_text_inside_search_box_number" value="<?php echo (($text_box_text_inside_search_box_value[2] != "") ? $text_box_text_inside_search_box_value[2] : '') ?>">                
                        <?php if (isset($text_box_text_inside_search_box_value[2]) && $text_box_text_inside_search_box_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete811"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Text_Box_Text_Inside_Box'; ?>', 'themeOption_text_box_text_inside_search_box_number', 'hideDelete811', 'text_colorthemeOption_text_box_text_inside_search_box_number', '<?php echo $text_box_text_inside_search_box_value[0]; ?>', '<?php echo $text_box_text_inside_search_box_value[1]; ?>', '<?php echo $text_box_text_inside_search_box_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <?php if (isset($activeRegion['R_Type']) && $activeRegion['R_Type'] != 6) { ?>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Search Box Button Text</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="search_box_button_text_font">
                            <option value="">Select Font...</option>
                            <?php
                            $getFonts = "SELECT TOF_Name, TOF_ID  FROM tbl_Theme_Options_Fonts ORDER BY TOF_Name";
                            $res_fonts = mysql_query($getFonts);
                            while ($row_fonts = mysql_fetch_assoc($res_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $row_fonts['TOF_Name']; ?>" <?php echo (($search_box_button_text_value[0] == $row_fonts['TOF_ID']) ? "selected" : "") ?> value="<?php echo $row_fonts['TOF_ID'] ?>"><?php echo $row_fonts['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="search_box_button_text_font_size" size="50" value="<?php echo $search_box_button_text_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_search_box_button_text_number" style="background-color: <?php echo (($search_box_button_text_value[2] != "") ? $search_box_button_text_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="search_box_button_text_number" id="text_colorthemeOption_search_box_button_text_number" value="<?php echo (($search_box_button_text_value[2] != "") ? $search_box_button_text_value[2] : '') ?>">                
                        <?php if (isset($search_box_button_text_value[2]) && $search_box_button_text_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete812"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Search_Box_Button_Text'; ?>', 'themeOption_search_box_button_text_number', 'hideDelete812', 'text_colorthemeOption_search_box_button_text_number', '<?php echo $search_box_button_text_value[0]; ?>', '<?php echo $search_box_button_text_value[1]; ?>', '<?php echo $search_box_button_text_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>

                <?php if (isset($activeRegion['R_My_Trip']) && $activeRegion['R_My_Trip'] != 0) { ?>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Trip Planner Number Background</label>
                        <div class="form-data theme-options">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_trip_planner_number_background" style="background-color: <?php echo (($Region_theme['TO_Trip_Planner_Number_Background'] != "") ? $Region_theme['TO_Trip_Planner_Number_Background'] : '') ?>"></div></div>
                            <input type="hidden" name="trip_planner_number_background" id="text_colorthemeOption_trip_planner_number_background" value="<?php echo (($Region_theme['TO_Trip_Planner_Number_Background'] != "") ? $Region_theme['TO_Trip_Planner_Number_Background'] : '') ?>">
                            <?php if (isset($Region_theme['TO_Trip_Planner_Number_Background']) && $Region_theme['TO_Trip_Planner_Number_Background'] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete7"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Trip_Planner_Number_Background'; ?>', 'themeOption_trip_planner_number_background', 'hideDelete7', 'text_colorthemeOption_trip_planner_number_background', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Trip Planner Number</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="trip_planner_number_font">
                                <option value="">Select Font...</option>
                                <?php
                                $getFonts = "SELECT TOF_Name, TOF_ID  FROM tbl_Theme_Options_Fonts ORDER BY TOF_Name";
                                $res_fonts = mysql_query($getFonts);
                                while ($row_fonts = mysql_fetch_assoc($res_fonts)) {
                                    ?>
                                    <option style="font-family: <?php echo $row_fonts['TOF_Name']; ?>" <?php echo (($to_trip_planner_number_value[0] == $row_fonts['TOF_ID']) ? "selected" : "") ?> value="<?php echo $row_fonts['TOF_ID'] ?>"><?php echo $row_fonts['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="trip_planner_number_size" size="50" value="<?php echo $to_trip_planner_number_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_trip_planner_number" style="background-color: <?php echo (($to_trip_planner_number_value[2] != "") ? $to_trip_planner_number_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="trip_planner_number" id="text_colorthemeOption_trip_planner_number" value="<?php echo (($to_trip_planner_number_value[2] != "") ? $to_trip_planner_number_value[2] : '') ?>">                
                            <?php if (isset($to_trip_planner_number_value[2]) && $to_trip_planner_number_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete8"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Trip_Planner_Number'; ?>', 'themeOption_trip_planner_number', 'hideDelete8', 'text_colorthemeOption_trip_planner_number', '<?php echo $to_trip_planner_number_value[0]; ?>', '<?php echo $to_trip_planner_number_value[1]; ?>', '<?php echo $to_trip_planner_number_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } if (isset($activeRegion['R_Type']) && ($activeRegion['R_Type'] != 6 && $activeRegion['R_Type'] != 7)) { ?>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Secondary Navigation</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="sec_nav_font">
                                <option value="">Select Font...</option>
                                <?php
                                $secfonts = mysql_query($getFonts);
                                while ($secNavfont = mysql_fetch_assoc($secfonts)) {
                                    ?>
                                    <option style="font-family: <?php echo $secNavfont['TOF_Name']; ?>" <?php echo (($sec_nav_value[0] == $secNavfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $secNavfont['TOF_ID'] ?>"><?php echo $secNavfont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="sec_nav_size" size="50" value="<?php echo $sec_nav_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_sec_nav_text" style="background-color: <?php echo (($sec_nav_value[2] != "") ? $sec_nav_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_sec_nav_text" id="text_colorthemeOption_sec_nav_text" value="<?php echo (($sec_nav_value[2] != "") ? $sec_nav_value[2] : '') ?>">
                            <?php if (isset($sec_nav_value[2]) && $sec_nav_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete9"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Secondary_Navigation'; ?>', 'themeOption_sec_nav_text', 'hideDelete9', 'text_colorthemeOption_sec_nav_text', '<?php echo $sec_nav_value[0]; ?>', '<?php echo $sec_nav_value[1]; ?>', '<?php echo $sec_nav_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Main Navigation</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="main_nav_font">
                            <option value="">Select Font...</option>
                            <?php
                            $mainfonts = mysql_query($getFonts);
                            while ($mainNavfont = mysql_fetch_assoc($mainfonts)) {
                                ?>
                                <option style="font-family: <?php echo $mainNavfont['TOF_Name']; ?>" <?php echo (($main_nav_value[0] == $mainNavfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $mainNavfont['TOF_ID'] ?>"><?php echo $mainNavfont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="main_nav_size" size="50" value="<?php echo $main_nav_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_main_nav_text" style="background-color: <?php echo (($main_nav_value[2] != "") ? $main_nav_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_main_nav_text" id="text_colorthemeOption_main_nav_text" value="<?php echo (($main_nav_value[2] != "") ? $main_nav_value[2] : '') ?>">
                        <?php if (isset($main_nav_value[2]) && $main_nav_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete10"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Main_Navigation'; ?>', 'themeOption_main_nav_text', 'hideDelete10', 'text_colorthemeOption_main_nav_text', '<?php echo $main_nav_value[0]; ?>', '<?php echo $main_nav_value[1]; ?>', '<?php echo $main_nav_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Main Navigation Drop Down</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="main_dd_font">
                            <option value="">Select Font...</option>
                            <?php
                            $main_dd_fonts = mysql_query($getFonts);
                            while ($mainDDFont = mysql_fetch_assoc($main_dd_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $mainDDFont['TOF_Name']; ?>" <?php echo (($main_dd_value[0] == $mainDDFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $mainDDFont['TOF_ID'] ?>"><?php echo $mainDDFont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="main_dd_size" size="50" value="<?php echo $main_dd_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_main_dd" style="background-color: <?php echo (($main_dd_value[2] != "") ? $main_dd_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_main_dd" id="text_colorthemeOption_main_dd" value="<?php echo (($main_dd_value[2] != "") ? $main_dd_value[2] : '') ?>">
                        <?php if (isset($main_dd_value[2]) && $main_dd_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete11"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Drop_Down_Menu'; ?>', 'themeOption_main_dd', 'hideDelete11', 'text_colorthemeOption_main_dd', '<?php echo $main_dd_value[0] ?>', '<?php echo $main_dd_value[1] ?>', '<?php echo $main_dd_value[2] ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Main Navigation Drop Down Background</label>
                    <div class="form-data theme-options">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_main_dd_bg" style="background-color: <?php echo (($Region_theme['TO_Drop_Down_Menu_BG'] != "") ? $Region_theme['TO_Drop_Down_Menu_BG'] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_main_dd_bg" id="text_colorthemeOption_main_dd_bg" value="<?php echo (($Region_theme['TO_Drop_Down_Menu_BG'] != "") ? $Region_theme['TO_Drop_Down_Menu_BG'] : '') ?>">
                        <?php if (isset($Region_theme['TO_Drop_Down_Menu_BG']) && $Region_theme['TO_Drop_Down_Menu_BG'] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete12"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Drop_Down_Menu_BG'; ?>', 'themeOption_main_dd_bg', 'hideDelete12', 'text_colorthemeOption_main_dd_bg', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <?php if (isset($activeRegion['R_Type']) && $activeRegion['R_Type'] != 6) { ?>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Main Navigation Bar</label>
                        <div class="form-data theme-options">
                            <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                                <input class="fileInput" type="file" name="navigation_bar_bg[]" onchange="show_file_name(this.files[0].name, 'main_navigation_bar')"/>
                            </span>
                            <input id="main_navigation_bar" class="uploadFileName" style="display:none;" disabled>
                            <?PHP if ($to_navigation_bar_bg) { ?>
                                <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $to_navigation_bar_bg; ?>" target="_blank">View</a>
                                <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options.php?rid=<?php echo $regionID ?>&flag=navbar&op=del">Delete</a>
                            <?php } ?>    
                            <div class="color-box-inner theme-options-color theme_color_float"><div class="color-box" id="themeOption_navigation_bar" style="background-color: <?php echo (($to_navigation_bar_color != "") ? $to_navigation_bar_color : '') ?>"></div></div>
                            <input type="hidden" name="navigation_bar" id="text_colorthemeOption_navigation_bar" value="<?php echo (($to_navigation_bar_color != "") ? $to_navigation_bar_color : '') ?>">       
                            <?php if (isset($to_navigation_bar_color) && $to_navigation_bar_color !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete13"  onclick="removeColorImage(<?php echo $regionID; ?>, '<?php echo 'TO_Navigation_Bar'; ?>', 'themeOption_navigation_bar', 'hideDelete13', 'text_colorthemeOption_navigation_bar', '<?php echo $to_navigation_bar_bg; ?>', '<?php echo $to_navigation_bar_color; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </fieldset>
            <fieldset>
                <legend>Main Slider</legend>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Title</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="slider_title_font">
                            <option value="">Select Font...</option>
                            <?php
                            $sliderTitlefonts = mysql_query($getFonts);
                            while ($sliderTitlefont = mysql_fetch_assoc($sliderTitlefonts)) {
                                ?>
                                <option style="font-family: <?php echo $sliderTitlefont['TOF_Name']; ?>" <?php echo (($slider_title_value[0] == $sliderTitlefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $sliderTitlefont['TOF_ID'] ?>"><?php echo $sliderTitlefont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="slider_title_size" size="50" value="<?php echo $slider_title_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_slider_title" style="background-color: <?php echo (($slider_title_value[2] != "") ? $slider_title_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_slider_title" id="text_colorthemeOption_slider_title" value="<?php echo (($slider_title_value[2] != "") ? $slider_title_value[2] : '') ?>">
                        <?php if (isset($slider_title_value[2]) && $slider_title_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete14"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Slider_Title'; ?>', 'themeOption_slider_title', 'hideDelete14', 'text_colorthemeOption_slider_title', '<?php echo $slider_title_value[0]; ?>', '<?php echo $slider_title_value[1]; ?>', '<?php echo $slider_title_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Description</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="slider_desc_font">
                            <option value="">Select Font...</option>
                            <?php
                            $slider_desc_fonts = mysql_query($getFonts);
                            while ($sliderDescfont = mysql_fetch_assoc($slider_desc_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $sliderDescfont['TOF_Name']; ?>" <?php echo (($slider_desc_value[0] == $sliderDescfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $sliderDescfont['TOF_ID'] ?>"><?php echo $sliderDescfont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="slider_desc_size" size="50" value="<?php echo $slider_desc_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_slider_desc" style="background-color: <?php echo (($slider_desc_value[2] != "") ? $slider_desc_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_slider_desc" id="text_colorthemeOption_slider_desc" value="<?php echo (($slider_desc_value[2] != "") ? $slider_desc_value[2] : '') ?>">
                        <?php if (isset($slider_desc_value[2]) && $slider_desc_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete15"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Slider_Description'; ?>', 'themeOption_slider_desc', 'hideDelete15', 'text_colorthemeOption_slider_desc', '<?php echo $slider_desc_value[0]; ?>', '<?php echo $slider_desc_value[1]; ?>', '<?php echo $slider_desc_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <?php if (isset($activeRegion['R_Type']) && $activeRegion['R_Type'] != 6) { ?>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Overlay</label>
                        <div class="form-data theme-options">
                            <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                                <input class="fileInput" type="file" name="slider_overlay[]" onchange="show_file_name(this.files[0].name, 'slider_overlay')"/>
                            </span>
                            <input id="slider_overlay" class="uploadFileName" style="display:none;" disabled>
                            <?PHP if ($Region_theme['TO_Slider_Overlay']) { ?>
                                <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $Region_theme['TO_Slider_Overlay']; ?>" target="_blank">View</a>
                                <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options.php?rid=<?php echo $regionID ?>&flag=slider_ol&op=del">Delete</a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </fieldset>
            <?php if (isset($activeRegion['R_Type']) && $activeRegion['R_Type'] != 6) { ?>
                <fieldset>
                    <legend>Main Description And Title</legend>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Title Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="main_page_title_font">
                                <option value="">Select Font...</option>
                                <?php
                                $main_page_title_fonts = mysql_query($getFonts);
                                while ($mainPageTitlefont = mysql_fetch_assoc($main_page_title_fonts)) {
                                    ?>
                                    <option style="font-family: <?php echo $mainPageTitlefont['TOF_Name']; ?>" <?php echo (($main_page_title_value[0] == $mainPageTitlefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $mainPageTitlefont['TOF_ID'] ?>"><?php echo $mainPageTitlefont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="main_page_title_size" size="50" value="<?php echo $main_page_title_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_main_page_title" style="background-color: <?php echo (($main_page_title_value[2] != "") ? $main_page_title_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_main_page_title" id="text_colorthemeOption_main_page_title" value="<?php echo (($main_page_title_value[2] != "") ? $main_page_title_value[2] : '') ?>">
                            <?php if (isset($main_page_title_value[2]) && $main_page_title_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete16"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Main_Page_Title'; ?>', 'themeOption_main_page_title', 'hideDelete16', 'text_colorthemeOption_main_page_title', '<?php echo $main_page_title_value[0]; ?>', '<?php echo $main_page_title_value[1]; ?>', '<?php echo $main_page_title_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Description Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="main_page_body_font">
                                <option value="">Select Font...</option>
                                <?php
                                $main_page_body_fonts = mysql_query($getFonts);
                                while ($mainPageBodyfont = mysql_fetch_assoc($main_page_body_fonts)) {
                                    ?>
                                    <option style="font-family: <?php echo $mainPageBodyfont['TOF_Name']; ?>" <?php echo (($main_page_body_value[0] == $mainPageBodyfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $mainPageBodyfont['TOF_ID'] ?>"><?php echo $mainPageBodyfont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="main_page_body_size" size="50" value="<?php echo $main_page_body_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_main_page_body" style="background-color: <?php echo (($main_page_body_value[2] != "") ? $main_page_body_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_main_page_body" id="text_colorthemeOption_main_page_body" value="<?php echo (($main_page_body_value[2] != "") ? $main_page_body_value[2] : '') ?>">
                            <?php if (isset($main_page_body_value[2]) && $main_page_body_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete17"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Main_Page_Body_Copy'; ?>', 'themeOption_main_page_body', 'hideDelete17', 'text_colorthemeOption_main_page_body', '<?php echo $main_page_body_value[0]; ?>', '<?php echo $main_page_body_value[1]; ?>', '<?php echo $main_page_body_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Description Background</label>
                        <div class="form-data theme-options">
                            <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                                <input class="fileInput" type="file" name="home_des_bg[]" onchange="show_file_name(this.files[0].name, 'description_background')"/>
                            </span>
                            <input id="description_background" class="uploadFileName" style="display:none;" disabled>
                            <?PHP if ($home_des_bg) { ?>
                                <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $home_des_bg; ?>" target="_blank">View</a>
                                <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options.php?rid=<?php echo $regionID ?>&flag=home_d_bg&op=del">Delete</a>
                            <?php } ?>    
                            <div class="color-box-inner theme-options-color theme_color_float"><div class="color-box" id="themeOption_home_des" style="background-color: <?php echo (($home_des_color != "") ? $home_des_color : '') ?>"></div></div>
                            <input type="hidden" name="home_des" id="text_colorthemeOption_home_des" value="<?php echo (($home_des_color != "") ? $home_des_color : '') ?>">       
                            <?php if (isset($home_des_color) && $home_des_color !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete18"  onclick="removeColorImage(<?php echo $regionID; ?>, '<?php echo 'TO_Homepage_Description_Background'; ?>', 'themeOption_home_des', 'hideDelete18', 'text_colorthemeOption_home_des', '<?php echo $home_des_bg; ?>', '<?php echo $home_des_color; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Description Spacing</label>
                        <div class="form-data theme-options">    
                            <input class="theme-options-size" type="text" name="description_space" size="50" value="<?php echo $Region_theme['TO_Homepage_Description_Space']; ?>" placeholder="Enter line space">
                        </div>
                    </div>
                </fieldset>
            <?php } ?>
            <fieldset>
                <legend>Thumbnail</legend>

                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Category Title</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="cat_title_font">
                            <option value="">Select Font...</option>
                            <?php
                            $cat_title_fonts = mysql_query($getFonts);
                            while ($catTitlefont = mysql_fetch_assoc($cat_title_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $catTitlefont['TOF_Name']; ?>" <?php echo (($cat_title_value[0] == $catTitlefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $catTitlefont['TOF_ID'] ?>"><?php echo $catTitlefont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="cat_title_size" size="50" value="<?php echo $cat_title_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_cat_title" style="background-color: <?php echo (($cat_title_value[2] != "") ? $cat_title_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_cat_title" id="text_colorthemeOption_cat_title" value="<?php echo (($cat_title_value[2] != "") ? $cat_title_value[2] : '') ?>">
                        <?php if (isset($cat_title_value[2]) && $cat_title_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete19"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Thumbnail_Category_Tite'; ?>', 'themeOption_cat_title', 'hideDelete19', 'text_colorthemeOption_cat_title', '<?php echo $cat_title_value[0]; ?>', '<?php echo $cat_title_value[1]; ?>', '<?php echo $cat_title_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>View All</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="view_all_font">
                            <option value="">Select Font...</option>
                            <?php
                            $view_all_fonts = mysql_query($getFonts);
                            while ($viewAllfont = mysql_fetch_assoc($view_all_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $viewAllfont['TOF_Name']; ?>" <?php echo (($view_all_value[0] == $viewAllfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $viewAllfont['TOF_ID'] ?>"><?php echo $viewAllfont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="view_all_size" size="50" value="<?php echo $view_all_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_view_all" style="background-color: <?php echo (($view_all_value[2] != "") ? $view_all_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_view_all" id="text_colorthemeOption_view_all" value="<?php echo (($view_all_value[2] != "") ? $view_all_value[2] : '') ?>">
                        <?php if (isset($view_all_value[2]) && $view_all_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete20"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Thumbnail_View_All'; ?>', 'themeOption_view_all', 'hideDelete20', 'text_colorthemeOption_view_all', '<?php echo $view_all_value[0]; ?>', '<?php echo $view_all_value[1]; ?>', '<?php echo $view_all_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Sub-Category Title</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="sub_cat_font">
                            <option value="">Select Font...</option>
                            <?php
                            $sub_cat_fonts = mysql_query($getFonts);
                            while ($subCatfont = mysql_fetch_assoc($sub_cat_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $subCatfont['TOF_Name']; ?>" <?php echo (($sub_cat_value[0] == $subCatfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $subCatfont['TOF_ID'] ?>"><?php echo $subCatfont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="sub_cat_size" size="50" value="<?php echo $sub_cat_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_sub_cat" style="background-color: <?php echo (($sub_cat_value[2] != "") ? $sub_cat_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_sub_cat" id="text_colorthemeOption_sub_cat" value="<?php echo (($sub_cat_value[2] != "") ? $sub_cat_value[2] : '') ?>">
                        <?php if (isset($sub_cat_value[2]) && $sub_cat_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete21"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Thumbnail_Sub_Category_Title'; ?>', 'themeOption_sub_cat', 'hideDelete21', 'text_colorthemeOption_sub_cat', '<?php echo $sub_cat_value[0]; ?>', '<?php echo $sub_cat_value[1]; ?>', '<?php echo $sub_cat_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Listing Title</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="listing_title_font">
                            <option value="">Select Font...</option>
                            <?php
                            $listing_title_fonts = mysql_query($getFonts);
                            while ($listingTitlefont = mysql_fetch_assoc($listing_title_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $listingTitlefont['TOF_Name']; ?>" <?php echo (($listing_title_value[0] == $listingTitlefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $listingTitlefont['TOF_ID'] ?>"><?php echo $listingTitlefont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="listing_title_size" size="50" value="<?php echo $listing_title_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_listing_title" style="background-color: <?php echo (($listing_title_value[2] != "") ? $listing_title_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_listing_title" id="text_colorthemeOption_listing_title" value="<?php echo (($listing_title_value[2] != "") ? $listing_title_value[2] : '') ?>">
                        <?php if (isset($listing_title_value[2]) && $listing_title_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete22"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Thumbnail_Listing_Title'; ?>', 'themeOption_listing_title', 'hideDelete22', 'text_colorthemeOption_listing_title', '<?php echo $listing_title_value[0]; ?>', '<?php echo $listing_title_value[1]; ?>', '<?php echo $listing_title_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <?php if (isset($activeRegion['R_Type']) && $activeRegion['R_Type'] == 6) { ?>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Coupon Title</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="coupon_title_font">
                                <option value="">Select Font...</option>
                                <?php
                                $coupon_title_fonts = mysql_query($getFonts);
                                while ($couponTitlefont = mysql_fetch_assoc($coupon_title_fonts)) {
                                    ?>
                                    <option style="font-family: <?php echo $couponTitlefont['TOF_Name']; ?>" <?php echo (($coupon_title_value[0] == $couponTitlefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $couponTitlefont['TOF_ID'] ?>"><?php echo $couponTitlefont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="coupon_title_size" size="50" value="<?php echo $coupon_title_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_coupon_title" style="background-color: <?php echo (($coupon_title_value[2] != "") ? $coupon_title_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_coupon_title" id="text_colorthemeOption_coupon_title" value="<?php echo (($coupon_title_value[2] != "") ? $coupon_title_value[2] : '') ?>">
                            <?php if (isset($coupon_title_value[2]) && $coupon_title_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete23"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Thumbnail_Coupon_Title'; ?>', 'themeOption_coupon_title', 'hideDelete23', 'text_colorthemeOption_coupon_title', '<?php echo $coupon_title_value[0]; ?>', '<?php echo $coupon_title_value[1]; ?>', '<?php echo $coupon_title_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if (isset($activeRegion['R_Type']) && $activeRegion['R_Type'] == 6) { ?>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Coupon Title Background Color</label>
                        <div class="form-data theme-options">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="coupon_title_background_color" style="background-color: <?php echo (($Region_theme['TO_Thumbnail_Coupon_Title_Background_Color'] != "") ? $Region_theme['TO_Thumbnail_Coupon_Title_Background_Color'] : '') ?>"></div></div>
                            <input type="hidden" name="coupon_title_background_color" id="text_colorcoupon_title_background_color" value="<?php echo (($Region_theme['TO_Thumbnail_Coupon_Title_Background_Color'] != "") ? $Region_theme['TO_Thumbnail_Coupon_Title_Background_Color'] : '') ?>">
                            <?php if (isset($Region_theme['TO_Thumbnail_Coupon_Title_Background_Color']) && $Region_theme['TO_Thumbnail_Coupon_Title_Background_Color'] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete24"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Thumbnail_Coupon_Title_Background_Color'; ?>', 'coupon_title_background_color', 'hideDelete24', 'text_colorcoupon_title_background_color', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>

                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Arrow Left</label>
                    <div class="form-data theme-options">
                        <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                            <input class="fileInput" type="file" name="thumbnail_arrow_left[]" onchange="show_file_name(this.files[0].name, 'arrow_left')"/>
                        </span>
                        <input id="arrow_left" class="uploadFileName" style="display:none;" disabled>
                        <?PHP if ($Region_theme['TO_Thumbnail_Arrow_Left']) { ?>
                            <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $Region_theme['TO_Thumbnail_Arrow_Left']; ?>" target="_blank">View</a>
                            <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options.php?rid=<?php echo $regionID ?>&flag=thumb_arrow_left&op=del">Delete</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Arrow Right</label>
                    <div class="form-data theme-options">
                        <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                            <input class="fileInput" type="file" name="thumbnail_arrow_right[]" onchange="show_file_name(this.files[0].name, 'arrow_right')"/>
                        </span>
                        <input id="arrow_right" class="uploadFileName" style="display:none;" disabled>
                        <?PHP if ($Region_theme['TO_Thumbnail_Arrow_Right']) { ?>
                            <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $Region_theme['TO_Thumbnail_Arrow_Right']; ?>" target="_blank">View</a>
                            <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options.php?rid=<?php echo $regionID ?>&flag=thumb_arrow_right&op=del">Delete</a>
                        <?php } ?>
                    </div>
                </div>
            </fieldset>
            <?php if (isset($activeRegion['R_Type']) && $activeRegion['R_Type'] != 6) { ?>
                <fieldset>
                    <legend>Overlays and Textures</legend>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Bar Texture</label>
                        <div class="form-data theme-options"> 
                            <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                                <input class="fileInput" type="file" name="bar_texture_bg[]" onchange="show_file_name(this.files[0].name, 'bar_texture')"/>
                            </span>
                            <input id="bar_texture" class="uploadFileName" style="display:none;" disabled>
                            <?PHP if ($to_bar_texture_bg) { ?>
                                <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $to_bar_texture_bg; ?>" target="_blank">View</a>
                                <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options.php?rid=<?php echo $regionID ?>&flag=bar_text_bg&op=del">Delete</a>
                            <?php } ?>    
                            <div class="color-box-inner theme-options-color theme_color_float"><div class="color-box" id="themeOption_bar_texture" style="background-color: <?php echo (($to_bar_texture_color != "") ? $to_bar_texture_color : '') ?>"></div></div>
                            <input type="hidden" name="bar_texture" id="text_colorthemeOption_bar_texture" value="<?php echo (($to_bar_texture_color != "") ? $to_bar_texture_color : '') ?>">       
                            <?php if (isset($to_bar_texture_color) && $to_bar_texture_color !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete25"  onclick="removeColorImage(<?php echo $regionID; ?>, '<?php echo 'TO_Bar_Texture'; ?>', 'themeOption_bar_texture', 'hideDelete25', 'text_colorthemeOption_bar_texture', '<?php echo $to_bar_texture_bg; ?>', '<?php echo $to_bar_texture_color; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <?php if (isset($activeRegion['R_Type']) && $activeRegion['R_Type'] != 7) { ?>
                        <div class="form-inside-div form-inside-div-width-admin"> 
                            <label>Secondary Image Overlay</label>
                            <div class="form-data theme-options">
                                <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                                    <input class="fileInput" type="file" name="secondary_slider_overlay[]" onchange="show_file_name(this.files[0].name, 'secondary_image_overlay')"/>
                                </span>
                                <input id="secondary_image_overlay" class="uploadFileName" style="display:none;" disabled>
                                <?PHP if ($Region_theme['TO_Secondary_Slider_Overlay']) { ?>
                                    <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $Region_theme['TO_Secondary_Slider_Overlay']; ?>" target="_blank">View</a>
                                    <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options.php?rid=<?php echo $regionID ?>&flag=s_s_overlay&op=del">Delete</a>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                </fieldset>
            <?php } if (isset($activeRegion['R_Type']) && $activeRegion['R_Type'] != 6) { ?>
                <fieldset>
                    <legend>Homepage Events</legend>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Background color</label>
                        <div class="form-data theme-options">
                            <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                                <input class="fileInput" type="file" name="homepage_events_bg[]" onchange="show_file_name(this.files[0].name, 'homepage_events_bg_color')"/>
                            </span>
                            <input id="homepage_events_bg_color" class="uploadFileName" style="display:none;" disabled>
                            <?PHP if ($to_homepage_events_bg) { ?> 
                                <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $to_homepage_events_bg; ?>" target="_blank">View</a>
                                <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options.php?rid=<?php echo $regionID ?>&flag=home_event_d_bg&op=del">Delete</a>
                            <?php } ?>    
                            <div class="color-box-inner theme-options-color theme_color_float"><div class="color-box" id="themeOption_homepage_events" style="background-color: <?php echo (($to_homepage_events_color != "") ? $to_homepage_events_color : '') ?>"></div></div>
                            <input type="hidden" name="homepage_events" id="text_colorthemeOption_homepage_events" value="<?php echo (($to_homepage_events_color != "") ? $to_homepage_events_color : '') ?>">       
                            <?php if (isset($to_homepage_events_color) && $to_homepage_events_color !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete26"  onclick="removeColorImage(<?php echo $regionID; ?>, '<?php echo 'TO_Homepage_Events_Description_Background'; ?>', 'themeOption_homepage_events', 'hideDelete26', 'text_colorthemeOption_homepage_events', '<?php echo $to_homepage_events_bg; ?>', '<?php echo $to_homepage_events_color; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Register Event Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="homepage_register_event_font">
                                <option value="">Select Font...</option>
                                <?php
                                $homepage_register_event_font = mysql_query($getFonts);
                                while ($homepageRegisterEventfont = mysql_fetch_assoc($homepage_register_event_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $homepageRegisterEventfont['TOF_Name']; ?>" <?php echo (($homepage_register_event_value[0] == $homepageRegisterEventfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $homepageRegisterEventfont['TOF_ID'] ?>"><?php echo $homepageRegisterEventfont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="homepage_register_event_size" size="50" value="<?php echo $homepage_register_event_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_homepage_register_event" style="background-color: <?php echo (($homepage_register_event_value[2] != "") ? $homepage_register_event_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_homepage_register_event" id="text_colorthemeOption_homepage_register_event" value="<?php echo (($homepage_register_event_value[2] != "") ? $homepage_register_event_value[2] : '') ?>">
                            <?php if (isset($homepage_register_event_value[2]) && $homepage_register_event_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete27"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Homepage_Event_Register_Event_Text'; ?>', 'themeOption_homepage_register_event', 'hideDelete27', 'text_colorthemeOption_homepage_register_event', '<?php echo $homepage_register_event_value[0]; ?>', '<?php echo $homepage_register_event_value[1]; ?>', '<?php echo $homepage_register_event_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Main Title Icon</label>
                        <div class="form-data theme-options">
                            <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                                <input class="fileInput" type="file" name="Homepage_Event_Icon[]" onchange="show_file_name(this.files[0].name, 'homepage_events_title_icon')"/>
                            </span>
                            <input id="homepage_events_title_icon" class="uploadFileName" style="display:none;" disabled>
                            <?PHP if ($Region_theme['TO_Homepage_Event_Icon']) { ?>
                                <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $Region_theme['TO_Homepage_Event_Icon']; ?>" target="_blank">View</a>
                                <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options.php?rid=<?php echo $regionID ?>&flag=home_event_icon&op=del">Delete</a>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Main Title Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="event_main_title_font">
                                <option value="">Select Font...</option>
                                <?php
                                $event_main_title_fonts = mysql_query($getFonts);
                                while ($eventMainTitlefont = mysql_fetch_assoc($event_main_title_fonts)) {
                                    ?>
                                    <option style="font-family: <?php echo $eventMainTitlefont['TOF_Name']; ?>" <?php echo (($event_main_title_value[0] == $eventMainTitlefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $eventMainTitlefont['TOF_ID'] ?>"><?php echo $eventMainTitlefont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="event_main_title_size" size="50" value="<?php echo $event_main_title_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_event_main_title" style="background-color: <?php echo (($event_main_title_value[2] != "") ? $event_main_title_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_event_main_title" id="text_colorthemeOption_event_main_title" value="<?php echo (($event_main_title_value[2] != "") ? $event_main_title_value[2] : '') ?>">
                            <?php if (isset($event_main_title_value[2]) && $event_main_title_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete28"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Homepage_Event_Main_Title'; ?>', 'themeOption_event_main_title', 'hideDelete28', 'text_colorthemeOption_event_main_title', '<?php echo $event_main_title_value[0]; ?>', '<?php echo $event_main_title_value[1]; ?>', '<?php echo $event_main_title_value[2]; ?>')">Remove</a>
                            <?php } ?>

                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Date Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="event_date_font">
                                <option value="">Select Font...</option>
                                <?php
                                $event_date_fonts = mysql_query($getFonts);
                                while ($eventDatefont = mysql_fetch_assoc($event_date_fonts)) {
                                    ?>
                                    <option style="font-family: <?php echo $eventDatefont['TOF_Name']; ?>" <?php echo (($event_date_value[0] == $eventDatefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $eventDatefont['TOF_ID'] ?>"><?php echo $eventDatefont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="event_date_size" size="50" value="<?php echo $event_date_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_event_date" style="background-color: <?php echo (($event_date_value[2] != "") ? $event_date_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_event_date" id="text_colorthemeOption_event_date" value="<?php echo (($event_date_value[2] != "") ? $event_date_value[2] : '') ?>">
                            <?php if (isset($event_date_value[2]) && $event_date_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete29"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Homepage_Event_Date'; ?>', 'themeOption_event_date', 'hideDelete29', 'text_colorthemeOption_event_date', '<?php echo $event_date_value[0]; ?>', '<?php echo $event_date_value[1]; ?>', '<?php echo $event_date_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Title Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="event_title_font">
                                <option value="">Select Font...</option>
                                <?php
                                $event_title_fonts = mysql_query($getFonts);
                                while ($eventTitlefont = mysql_fetch_assoc($event_title_fonts)) {
                                    ?>
                                    <option style="font-family: <?php echo $eventTitlefont['TOF_Name']; ?>" <?php echo (($event_title_value[0] == $eventTitlefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $eventTitlefont['TOF_ID'] ?>"><?php echo $eventTitlefont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="event_title_size" size="50" value="<?php echo $event_title_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_event_title" style="background-color: <?php echo (($event_title_value[2] != "") ? $event_title_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_event_title" id="text_colorthemeOption_event_title" value="<?php echo (($event_title_value[2] != "") ? $event_title_value[2] : '') ?>">
                            <?php if (isset($event_title_value[2]) && $event_title_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete30"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Homepage_Event_Title'; ?>', 'themeOption_event_title', 'hideDelete30', 'text_colorthemeOption_event_title', '<?php echo $event_title_value[0]; ?>', '<?php echo $event_title_value[1]; ?>', '<?php echo $event_title_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Description Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="event_text_font">
                                <option value="">Select Font...</option>
                                <?php
                                $event_text_fonts = mysql_query($getFonts);
                                while ($eventTextfont = mysql_fetch_assoc($event_text_fonts)) {
                                    ?>
                                    <option style="font-family: <?php echo $eventTextfont['TOF_Name']; ?>" <?php echo (($event_text_value[0] == $eventTextfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $eventTextfont['TOF_ID'] ?>"><?php echo $eventTextfont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="event_text_size" size="50" value="<?php echo $event_text_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_event_text" style="background-color: <?php echo (($event_text_value[2] != "") ? $event_text_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_event_text" id="text_colorthemeOption_event_text" value="<?php echo (($event_text_value[2] != "") ? $event_text_value[2] : '') ?>">
                            <?php if (isset($event_text_value[2]) && $event_text_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete31"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Homepage_Event_Text'; ?>', 'themeOption_event_text', 'hideDelete31', 'text_colorthemeOption_event_text', '<?php echo $event_text_value[0]; ?>', '<?php echo $event_text_value[1]; ?>', '<?php echo $event_text_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>More Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="homepage_event_more_text_font">
                                <option value="">Select Font...</option>
                                <?php
                                $homepage_event_more_text_fonts = mysql_query($getFonts);
                                while ($homepageEventMoreTextfont = mysql_fetch_assoc($homepage_event_more_text_fonts)) {
                                    ?>
                                    <option style="font-family: <?php echo $homepageEventMoreTextfont['TOF_Name']; ?>" <?php echo (($homepage_event_more_text_value[0] == $homepageEventMoreTextfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $homepageEventMoreTextfont['TOF_ID'] ?>"><?php echo $homepageEventMoreTextfont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="homepage_event_more_text_size" size="50" value="<?php echo $homepage_event_more_text_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_homepage_event_more_text" style="background-color: <?php echo (($homepage_event_more_text_value[2] != "") ? $homepage_event_more_text_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_homepage_event_more_text" id="text_colorthemeOption_homepage_event_more_text" value="<?php echo (($homepage_event_more_text_value[2] != "") ? $homepage_event_more_text_value[2] : '') ?>">
                            <?php if (isset($homepage_event_more_text_value[2]) && $homepage_event_more_text_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete32"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Homepage_Event_More_Text'; ?>', 'themeOption_homepage_event_more_text', 'hideDelete32', 'text_colorthemeOption_homepage_event_more_text', '<?php echo $homepage_event_more_text_value[0]; ?>', '<?php echo $homepage_event_more_text_value[1]; ?>', '<?php echo $homepage_event_more_text_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>See All Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="homepage_event_see_all_text_font">
                                <option value="">Select Font...</option>
                                <?php
                                $homepage_event_see_all_text_fonts = mysql_query($getFonts);
                                while ($homepageEventSeeAllTextfont = mysql_fetch_assoc($homepage_event_see_all_text_fonts)) {
                                    ?>
                                    <option style="font-family: <?php echo $homepageEventSeeAllTextfont['TOF_Name']; ?>" <?php echo (($homepage_event_see_all_text_value[0] == $homepageEventSeeAllTextfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $homepageEventSeeAllTextfont['TOF_ID'] ?>"><?php echo $homepageEventSeeAllTextfont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="homepage_event_see_all_text_size" size="50" value="<?php echo $homepage_event_see_all_text_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_homepage_event_see_all_text" style="background-color: <?php echo (($homepage_event_see_all_text_value[2] != "") ? $homepage_event_see_all_text_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_homepage_event_see_all_text" id="text_colorthemeOption_homepage_event_see_all_text" value="<?php echo (($homepage_event_see_all_text_value[2] != "") ? $homepage_event_see_all_text_value[2] : '') ?>">
                            <?php if (isset($homepage_event_see_all_text_value[2]) && $homepage_event_see_all_text_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete33"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Homepage_Event_See_All_Text'; ?>', 'themeOption_homepage_event_see_all_text', 'hideDelete33', 'text_colorthemeOption_homepage_event_see_all_text', '<?php echo $homepage_event_see_all_text_value[0]; ?>', '<?php echo $homepage_event_see_all_text_value[1]; ?>', '<?php echo $homepage_event_see_all_text_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                </fieldset>
            <?php } ?>
            <fieldset>
                <legend>Footer</legend>
                <?php if (isset($activeRegion['R_Type']) && ($activeRegion['R_Type'] != 6 && $activeRegion['R_Type'] != 7)) { ?>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Category Title Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="footer_title_font">
                                <option value="">Select Font...</option>
                                <?php
                                $footerTitleFonts = mysql_query($getFonts);
                                while ($footerTitleFont = mysql_fetch_assoc($footerTitleFonts)) {
                                    ?>
                                    <option style="font-family: <?php echo $footerTitleFont['TOF_Name']; ?>" <?php echo (($footer_title_value[0] == $footerTitleFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $footerTitleFont['TOF_ID'] ?>"><?php echo $footerTitleFont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="footer_title_size" size="50" value="<?php echo $footer_title_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_footer_title" style="background-color: <?php echo (($footer_title_value[2] != "") ? $footer_title_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_footer_title" id="text_colorthemeOption_footer_title" value="<?php echo (($footer_title_value[2] != "") ? $footer_title_value[2] : '') ?>">
                            <?php if (isset($footer_title_value[2]) && $footer_title_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete34"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Footer_Text_Title'; ?>', 'themeOption_footer_title', 'hideDelete34', 'text_colorthemeOption_footer_title', '<?php echo $footer_title_value[0]; ?>', '<?php echo $footer_title_value[1]; ?>', '<?php echo $footer_title_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } if (isset($activeRegion['R_Type']) && $activeRegion['R_Type'] != 7) { ?>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Sub Category Title Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="footer_text_font">
                                <option value="">Select Font...</option>
                                <?php
                                $footer_text_font = mysql_query($getFonts);
                                while ($footerTextFont = mysql_fetch_assoc($footer_text_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $footerTextFont['TOF_Name']; ?>" <?php echo (($footer_text_value[0] == $footerTextFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $footerTextFont['TOF_ID'] ?>"><?php echo $footerTextFont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="footer_text_size" size="50" value="<?php echo $footer_text_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_footer_text" style="background-color: <?php echo (($footer_text_value[2] != "") ? $footer_text_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_footer_text" id="text_colorthemeOption_footer_text" value="<?php echo (($footer_text_value[2] != "") ? $footer_text_value[2] : '') ?>">
                            <?php if (isset($footer_text_value[2]) && $footer_text_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete35"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Footer_Text'; ?>', 'themeOption_footer_text', 'hideDelete35', 'text_colorthemeOption_footer_text', '<?php echo $footer_text_value[0]; ?>', '<?php echo $footer_text_value[1]; ?>', '<?php echo $footer_text_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Text Links</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="footer_links_font">
                            <option value="">Select Font...</option>
                            <?php
                            $footer_links_fonts = mysql_query($getFonts);
                            while ($footerLinksFont = mysql_fetch_assoc($footer_links_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $footerLinksFont['TOF_Name']; ?>" <?php echo (($footer_links_value[0] == $footerLinksFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $footerLinksFont['TOF_ID'] ?>"><?php echo $footerLinksFont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="footer_links_size" size="50" value="<?php echo $footer_links_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_footer_links" style="background-color: <?php echo (($footer_links_value[2] != "") ? $footer_links_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_footer_links" id="text_colorthemeOption_footer_links" value="<?php echo (($footer_links_value[2] != "") ? $footer_links_value[2] : '') ?>">
                        <?php if (isset($footer_links_value[2]) && $footer_links_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete36"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Footer_Links'; ?>', 'themeOption_footer_links', 'hideDelete36', 'text_colorthemeOption_footer_links', '<?php echo $footer_links_value[0]; ?>', '<?php echo $footer_links_value[1]; ?>', '<?php echo $footer_links_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Disclaimer Text</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="footer_disc_font">
                            <option value="">Select Font...</option>
                            <?php
                            $footer_disc_font = mysql_query($getFonts);
                            while ($footerDiscFont = mysql_fetch_assoc($footer_disc_font)) {
                                ?>
                                <option style="font-family: <?php echo $footerDiscFont['TOF_Name']; ?>" <?php echo (($footer_disc_value[0] == $footerDiscFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $footerDiscFont['TOF_ID'] ?>"><?php echo $footerDiscFont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="footer_disc_size" size="50" value="<?php echo $footer_disc_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_footer_disc" style="background-color: <?php echo (($footer_disc_value[2] != "") ? $footer_disc_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_footer_disc" id="text_colorthemeOption_footer_disc" value="<?php echo (($footer_disc_value[2] != "") ? $footer_disc_value[2] : '') ?>">
                        <?php if (isset($footer_disc_value[2]) && $footer_disc_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete37"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Footer_Disclaimer'; ?>', 'themeOption_footer_disc', 'hideDelete37', 'text_colorthemeOption_footer_disc', '<?php echo $footer_disc_value[0]; ?>', '<?php echo $footer_disc_value[1]; ?>', '<?php echo $footer_disc_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Background Color</label>
                    <div class="form-data theme-options">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_footer_background" style="background-color: <?php echo (($Region_theme['TO_Footer_Background_Color'] != "") ? $Region_theme['TO_Footer_Background_Color'] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_footer_background" id="text_colorthemeOption_footer_background" value="<?php echo (($Region_theme['TO_Footer_Background_Color'] != "") ? $Region_theme['TO_Footer_Background_Color'] : '') ?>">
                        <?php if (isset($Region_theme['TO_Footer_Background_Color']) && $Region_theme['TO_Footer_Background_Color'] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete38"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Footer_Background_Color'; ?>', 'themeOption_footer_background', 'hideDelete38', 'text_colorthemeOption_footer_background', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Border Color</label>
                    <div class="form-data theme-options">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_footer_lines" style="background-color: <?php echo (($Region_theme['TO_Footer_Lines_Color'] != "") ? $Region_theme['TO_Footer_Lines_Color'] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_footer_lines" id="text_colorthemeOption_footer_lines" value="<?php echo (($Region_theme['TO_Footer_Lines_Color'] != "") ? $Region_theme['TO_Footer_Lines_Color'] : '') ?>">
                        <?php if (isset($Region_theme['TO_Footer_Lines_Color']) && $Region_theme['TO_Footer_Lines_Color'] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete39"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Footer_Lines_Color'; ?>', 'themeOption_footer_lines', 'hideDelete39', 'text_colorthemeOption_footer_lines', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Listing Page</legend>
                <?php if (isset($activeRegion['R_Type']) && $activeRegion['R_Type'] != 6) { ?>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Navigation/Thumbnail Gallery Background</label>
                        <div class="form-data theme-options">
                            <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                                <input class="fileInput" type="file" name="listing_nav_gallary_bg[]" onchange="show_file_name(this.files[0].name, 'listing_nav_gallery_bg')"/>
                            </span>
                            <input id="listing_nav_gallery_bg" class="uploadFileName" style="display:none;" disabled>
                            <?PHP if ($listing_nav_gallary_bg) { ?>
                                <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $listing_nav_gallary_bg; ?>" target="_blank">View</a>
                                <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options.php?rid=<?php echo $regionID ?>&flag=listing_nav_g_bg&op=del">Delete</a>
                            <?php } ?>    
                            <div class="color-box-inner theme-options-color theme_color_float"><div class="color-box" id="themeOption_listing_nav_gallary" style="background-color: <?php echo (($listing_nav_gallary_color != "") ? $listing_nav_gallary_color : '') ?>"></div></div>
                            <input type="hidden" name="listing_nav_gallary" id="text_colorthemeOption_listing_nav_gallary" value="<?php echo (($listing_nav_gallary_color != "") ? $listing_nav_gallary_color : '') ?>">       
                            <?php if (isset($listing_nav_gallary_color) && $listing_nav_gallary_color !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete40"  onclick="removeColorImage(<?php echo $regionID; ?>, '<?php echo 'TO_Listing_Nav_Gallary_Background'; ?>', 'themeOption_listing_nav_gallary', 'hideDelete40', 'text_colorthemeOption_listing_nav_gallary', '<?php echo $listing_nav_gallary_bg; ?>', '<?php echo $listing_nav_gallary_color; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Main Navigation</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="listing_navigation_text_font">
                                <option value="">Select Font...</option>
                                <?php
                                $listing_navigation_text_font = mysql_query($getFonts);
                                while ($listingnavigationTextfont = mysql_fetch_assoc($listing_navigation_text_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $listingnavigationTextfont['TOF_Name']; ?>" <?php echo (($listing_title_navigation_value[0] == $listingnavigationTextfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $listingnavigationTextfont['TOF_ID'] ?>"><?php echo $listingnavigationTextfont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="listing_navigation_text_size" size="50" value="<?php echo $listing_title_navigation_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_listing_navigation_text" style="background-color: <?php echo (($listing_title_navigation_value[2] != "") ? $listing_title_navigation_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_listing_navigation_text" id="text_colorthemeOption_listing_navigation_text" value="<?php echo (($listing_title_navigation_value[2] != "") ? $listing_title_navigation_value[2] : '') ?>">
                            <?php if (isset($listing_title_navigation_value[2]) && $listing_title_navigation_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete41"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Listing_Navigation_Text'; ?>', 'themeOption_listing_navigation_text', 'hideDelete41', 'text_colorthemeOption_listing_navigation_text', '<?php echo $listing_title_navigation_value[0]; ?>', '<?php echo $listing_title_navigation_value[1]; ?>', '<?php echo $listing_title_navigation_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Title</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="listing_title_text_font">
                            <option value="">Select Font...</option>
                            <?php
                            $listing_title_text_font = mysql_query($getFonts);
                            while ($listingTitleTextfont = mysql_fetch_assoc($listing_title_text_font)) {
                                ?>
                                <option style="font-family: <?php echo $listingTitleTextfont['TOF_Name']; ?>" <?php echo (($listing_title_text_value[0] == $listingTitleTextfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $listingTitleTextfont['TOF_ID'] ?>"><?php echo $listingTitleTextfont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="listing_title_text_size" size="50" value="<?php echo $listing_title_text_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_listing_title_text" style="background-color: <?php echo (($listing_title_text_value[2] != "") ? $listing_title_text_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_listing_title_text" id="text_colorthemeOption_listing_title_text" value="<?php echo (($listing_title_text_value[2] != "") ? $listing_title_text_value[2] : '') ?>">
                        <?php if (isset($listing_title_text_value[2]) && $listing_title_text_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete42"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Listing_Title_Text'; ?>', 'themeOption_listing_title_text', 'hideDelete42', 'text_colorthemeOption_listing_title_text', '<?php echo $listing_title_text_value[0]; ?>', '<?php echo $listing_title_text_value[1]; ?>', '<?php echo $listing_title_text_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Address</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="listing_location_text_font">
                            <option value="">Select Font...</option>
                            <?php
                            $listing_location_text_font = mysql_query($getFonts);
                            while ($listinglocationTextfont = mysql_fetch_assoc($listing_location_text_font)) {
                                ?>
                                <option style="font-family: <?php echo $listinglocationTextfont['TOF_Name']; ?>" <?php echo (($listing_title_location_value[0] == $listinglocationTextfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $listinglocationTextfont['TOF_ID'] ?>"><?php echo $listinglocationTextfont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="listing_location_text_size" size="50" value="<?php echo $listing_title_location_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_listing_location_text" style="background-color: <?php echo (($listing_title_location_value[2] != "") ? $listing_title_location_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_listing_location_text" id="text_colorthemeOption_listing_location_text" value="<?php echo (($listing_title_location_value[2] != "") ? $listing_title_location_value[2] : '') ?>">
                        <?php if (isset($listing_title_location_value[2]) && $listing_title_location_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete43"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Listing_Location_Text'; ?>', 'themeOption_listing_location_text', 'hideDelete43', 'text_colorthemeOption_listing_location_text', '<?php echo $listing_title_location_value[0]; ?>', '<?php echo $listing_title_location_value[1]; ?>', '<?php echo $listing_title_location_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <?php if (isset($activeRegion['R_Type']) && $activeRegion['R_Type'] != 6) { ?>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Menu Item Heading</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="menu_text_font">
                                <option value="">Select Font...</option>
                                <?php
                                $menu_text_font = mysql_query($getFonts);
                                while ($menuTextfont = mysql_fetch_assoc($menu_text_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $menuTextfont['TOF_Name']; ?>" <?php echo (($menu_text_value[0] == $menuTextfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $menuTextfont['TOF_ID'] ?>"><?php echo $menuTextfont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="menu_text_size" size="50" value="<?php echo $menu_text_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_menu_text" style="background-color: <?php echo (($menu_text_value[2] != "") ? $menu_text_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_menu_text" id="text_colorthemeOption_menu_text" value="<?php echo (($menu_text_value[2] != "") ? $menu_text_value[2] : '') ?>">
                            <?php if (isset($menu_text_value[2]) && $menu_text_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete44"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Menu_Text'; ?>', 'themeOption_menu_text', 'hideDelete44', 'text_colorthemeOption_menu_text', '<?php echo $menu_text_value[0]; ?>', '<?php echo $menu_text_value[1]; ?>', '<?php echo $menu_text_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Sub Heading</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="listing_sub_heading_font">
                                <option value="">Select Font...</option>
                                <?php
                                $listing_sub_heading_font = mysql_query($getFonts);
                                while ($listingSubHeadingFont = mysql_fetch_assoc($listing_sub_heading_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $listingSubHeadingFont['TOF_Name']; ?>" <?php echo (($listing_sub_heading_value[0] == $listingSubHeadingFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $listingSubHeadingFont['TOF_ID'] ?>"><?php echo $listingSubHeadingFont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="listing_sub_heading_size" size="50" value="<?php echo $listing_sub_heading_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_listing_sub_heading" style="background-color: <?php echo (($listing_sub_heading_value[2] != "") ? $listing_sub_heading_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_listing_sub_heading" id="text_colorthemeOption_listing_sub_heading" value="<?php echo (($listing_sub_heading_value[2] != "") ? $listing_sub_heading_value[2] : '') ?>">
                            <?php if (isset($listing_sub_heading_value[2]) && $listing_sub_heading_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete45"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Listing_Sub_Heading'; ?>', 'themeOption_listing_sub_heading', 'hideDelete45', 'text_colorthemeOption_listing_sub_heading', '<?php echo $listing_sub_heading_value[0]; ?>', '<?php echo $listing_sub_heading_value[1]; ?>', '<?php echo $listing_sub_heading_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Drop Down Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="listing_dd_text_font">
                                <option value="">Select Font...</option>
                                <?php
                                $listing_dd_text_font = mysql_query($getFonts);
                                while ($listingDDTextfont = mysql_fetch_assoc($listing_dd_text_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $listingDDTextfont['TOF_Name']; ?>" <?php echo (($listing_dd_text_value[0] == $listingDDTextfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $listingDDTextfont['TOF_ID'] ?>"><?php echo $listingDDTextfont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="listing_dd_text_size" size="50" value="<?php echo $listing_dd_text_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_listing_dd_text" style="background-color: <?php echo (($listing_dd_text_value[2] != "") ? $listing_dd_text_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_listing_dd_text" id="text_colorthemeOption_listing_dd_text" value="<?php echo (($listing_dd_text_value[2] != "") ? $listing_dd_text_value[2] : '') ?>">
                            <?php if (isset($listing_dd_text_value[2]) && $listing_dd_text_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete46"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Listing_Drop_Down_Text'; ?>', 'themeOption_listing_dd_text', 'hideDelete46', 'text_colorthemeOption_listing_dd_text', '<?php echo $listing_dd_text_value[0]; ?>', '<?php echo $listing_dd_text_value[1]; ?>', '<?php echo $listing_dd_text_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Drop Down Background</label>
                        <div class="form-data theme-options">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_listing_dd_background" style="background-color: <?php echo (($Region_theme['TO_Listing_Drop_Down_Background'] != "") ? $Region_theme['TO_Listing_Drop_Down_Background'] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_listing_dd_background" id="text_colorthemeOption_listing_dd_background" value="<?php echo (($Region_theme['TO_Listing_Drop_Down_Background'] != "") ? $Region_theme['TO_Listing_Drop_Down_Background'] : '') ?>">
                            <?php if (isset($Region_theme['TO_Listing_Drop_Down_Background']) && $Region_theme['TO_Listing_Drop_Down_Background'] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete47"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Listing_Drop_Down_Background'; ?>', 'themeOption_listing_dd_background', 'hideDelete47', 'text_colorthemeOption_listing_dd_background', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Drop Down Arrow</label>
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_listing_dd_arrow" style="background-color: <?php echo (($Region_theme['TO_Listing_Drop_Down_Arrow'] != "") ? $Region_theme['TO_Listing_Drop_Down_Arrow'] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_listing_dd_arrow" id="text_colorthemeOption_listing_dd_arrow" value="<?php echo (($Region_theme['TO_Listing_Drop_Down_Arrow'] != "") ? $Region_theme['TO_Listing_Drop_Down_Arrow'] : '') ?>">
                        <?php if (isset($Region_theme['TO_Listing_Drop_Down_Arrow']) && $Region_theme['TO_Listing_Drop_Down_Arrow'] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete48"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Listing_Drop_Down_Arrow'; ?>', 'themeOption_listing_dd_arrow', 'hideDelete48', 'text_colorthemeOption_listing_dd_arrow', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                <?php } ?>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Sidebar Text</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="listing_sub_nav_font">
                            <option value="">Select Font...</option>
                            <?php
                            $listing_sub_nav_font = mysql_query($getFonts);
                            while ($listingSubNavFont = mysql_fetch_assoc($listing_sub_nav_font)) {
                                ?>
                                <option style="font-family: <?php echo $listingSubNavFont['TOF_Name']; ?>" <?php echo (($listing_sub_nav_value[0] == $listingSubNavFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $listingSubNavFont['TOF_ID'] ?>"><?php echo $listingSubNavFont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="listing_sub_nav_size" size="50" value="<?php echo $listing_sub_nav_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_listing_sub_nav" style="background-color: <?php echo (($listing_sub_nav_value[2] != "") ? $listing_sub_nav_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_listing_sub_nav" id="text_colorthemeOption_listing_sub_nav" value="<?php echo (($listing_sub_nav_value[2] != "") ? $listing_sub_nav_value[2] : '') ?>">
                        <?php if (isset($listing_sub_nav_value[2]) && $listing_sub_nav_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete49"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Listing_Sub_Nav_Text'; ?>', 'themeOption_listing_sub_nav', 'hideDelete49', 'text_colorthemeOption_listing_sub_nav', '<?php echo $listing_sub_nav_value[0]; ?>', '<?php echo $listing_sub_nav_value[1]; ?>', '<?php echo $listing_sub_nav_value[0]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Days Text in View Hours</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="listing_hours_day_font">
                            <option value="">Select Font...</option>
                            <?php
                            $listing_hours_day_font = mysql_query($getFonts);
                            while ($listingHoursdayFont = mysql_fetch_assoc($listing_hours_day_font)) {
                                ?>
                                <option style="font-family: <?php echo $listingHoursdayFont['TOF_Name']; ?>" <?php echo (($listing_day_text_value[0] == $listingHoursdayFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $listingHoursdayFont['TOF_ID'] ?>"><?php echo $listingHoursdayFont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="listing_hours_day_size" size="50" value="<?php echo $listing_day_text_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_listing_hours_day" style="background-color: <?php echo (($listing_day_text_value[2] != "") ? $listing_day_text_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_listing_hours_day" id="text_colorthemeOption_listing_hours_day" value="<?php echo (($listing_day_text_value[2] != "") ? $listing_day_text_value[2] : '') ?>">
                        <?php if (isset($listing_day_text_value[2]) && $listing_day_text_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete50"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Listing_Day_Text'; ?>', 'themeOption_listing_hours_day', 'hideDelete50', 'text_colorthemeOption_listing_hours_day', '<?php echo $listing_day_text_value[0]; ?>', '<?php echo $listing_day_text_value[1]; ?>', '<?php echo $listing_day_text_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Hours Text in View Hours</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="listing_hours_text_font">
                            <option value="">Select Font...</option>
                            <?php
                            $listing_hours_text_font = mysql_query($getFonts);
                            while ($listingHoursTextFont = mysql_fetch_assoc($listing_hours_text_font)) {
                                ?>
                                <option style="font-family: <?php echo $listingHoursTextFont['TOF_Name']; ?>" <?php echo (($listing_hours_text_value[0] == $listingHoursTextFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $listingHoursTextFont['TOF_ID'] ?>"><?php echo $listingHoursTextFont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="listing_hours_text_size" size="50" value="<?php echo $listing_hours_text_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_listing_hours_text" style="background-color: <?php echo (($listing_hours_text_value[2] != "") ? $listing_hours_text_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_listing_hours_text" id="text_colorthemeOption_listing_hours_text" value="<?php echo (($listing_hours_text_value[2] != "") ? $listing_hours_text_value[2] : '') ?>">
                        <?php if (isset($listing_hours_text_value[2]) && $listing_hours_text_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete51"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Listing_Hours_Text'; ?>', 'themeOption_listing_hours_text', 'hideDelete51', 'text_colorthemeOption_listing_hours_text', '<?php echo $listing_hours_text_value[0]; ?>', '<?php echo $listing_hours_text_value[1]; ?>', '<?php echo $listing_hours_text_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Amenities Text in View Amenities</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="amenities_text_font">
                            <option value="">Select Font...</option>
                            <?php
                            $amenities_text_font = mysql_query($getFonts);
                            while ($amenitiesTextFont = mysql_fetch_assoc($amenities_text_font)) {
                                ?>
                                <option style="font-family: <?php echo $amenitiesTextFont['TOF_Name']; ?>" <?php echo (($amenities_text_value[0] == $amenitiesTextFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $amenitiesTextFont['TOF_ID'] ?>"><?php echo $amenitiesTextFont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="amenities_text_size" size="50" value="<?php echo $amenities_text_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_amenities_text" style="background-color: <?php echo (($amenities_text_value[2] != "") ? $amenities_text_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_amenities_text" id="text_colorthemeOption_amenities_text" value="<?php echo (($amenities_text_value[2] != "") ? $amenities_text_value[2] : '') ?>">
                        <?php if (isset($amenities_text_value[2]) && $amenities_text_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete52"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Amenities_Text'; ?>', 'themeOption_amenities_text', 'hideDelete52', 'text_colorthemeOption_amenities_text', '<?php echo $amenities_text_value[0]; ?>', '<?php echo $amenities_text_value[1]; ?>', '<?php echo $amenities_text_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Downloads Text in View Downloads</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="download_text_font">
                            <option value="">Select Font...</option>
                            <?php
                            $download_text_font = mysql_query($getFonts);
                            while ($downloadTextFont = mysql_fetch_assoc($download_text_font)) {
                                ?>
                                <option style="font-family: <?php echo $downloadTextFont['TOF_Name']; ?>" <?php echo (($download_text_value[0] == $downloadTextFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $downloadTextFont['TOF_ID'] ?>"><?php echo $downloadTextFont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="download_text_size" size="50" value="<?php echo $download_text_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_download_text" style="background-color: <?php echo (($download_text_value[2] != "") ? $download_text_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_download_text" id="text_colorthemeOption_download_text" value="<?php echo (($download_text_value[2] != "") ? $download_text_value[2] : '') ?>">
                        <?php if (isset($download_text_value[2]) && $download_text_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete53"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Downloads_Text'; ?>', 'themeOption_download_text', 'hideDelete53', 'text_colorthemeOption_download_text', '<?php echo $download_text_value[0]; ?>', '<?php echo $download_text_value[1]; ?>', '<?php echo $download_text_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <?php if (isset($activeRegion['R_Type']) && ($activeRegion['R_Type'] != 6 && $activeRegion['R_Type'] != 7)) { ?>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>What's Nearby Overlay</label>
                        <div class="form-data theme-options">
                            <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                                <input class="fileInput" type="file" name="whats_nearby_overlay[]" onchange="show_file_name(this.files[0].name, 'whats_nearby_overlay')"/>
                            </span>
                            <input id="whats_nearby_overlay" class="uploadFileName" style="display:none;" disabled>
                            <?PHP if ($Region_theme['TO_Whats_Nearby_Overlay']) { ?>
                                <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $Region_theme['TO_Whats_Nearby_Overlay']; ?>" target="_blank">View</a>
                                <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options.php?rid=<?php echo $regionID ?>&flag=whats_nb_overlay&op=del">Delete</a>
                            <?PHP } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>What's Nearby Background</label>
                        <div class="form-data theme-options">    
                            <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                                <input class="fileInput" type="file" name="listing_whats_nearby_bg[]" onchange="show_file_name(this.files[0].name, 'whats_nearby_bg')"/>
                            </span>
                            <input id="whats_nearby_bg" class="uploadFileName" style="display:none;" disabled>
                            <?PHP if ($listing_whats_nearby_bg) { ?>
                                <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $listing_whats_nearby_bg; ?>" target="_blank">View</a>
                                <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options.php?rid=<?php echo $regionID ?>&flag=listing_whats_nearby_bg&op=del">Delete</a>
                            <?php } ?>    
                            <div class="color-box-inner theme-options-color theme_color_float"><div class="color-box" id="themeOption_listing_whats_nearby" style="background-color: <?php echo (($listing_whats_nearby_color != "") ? $listing_whats_nearby_color : '') ?>"></div></div>
                            <input type="hidden" name="listing_whats_nearby" id="text_colorthemeOption_listing_whats_nearby" value="<?php echo (($listing_whats_nearby_color != "") ? $listing_whats_nearby_color : '') ?>">       
                            <?php if (isset($listing_whats_nearby_color) && $listing_whats_nearby_color !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete54"  onclick="removeColorImage(<?php echo $regionID; ?>, '<?php echo 'TO_Listing_Whats_Nearby_Background'; ?>', 'themeOption_listing_whats_nearby', 'hideDelete54', 'text_colorthemeOption_listing_whats_nearby', '<?php echo $listing_whats_nearby_bg; ?>', '<?php echo $listing_whats_nearby_color; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>What's Nearby Drop Down Background</label>
                        <div class="form-data theme-options">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_whats_nearby_dd_background" style="background-color: <?php echo (($Region_theme['TO_Whats_Nearby_Drop_Down_Background'] != "") ? $Region_theme['TO_Whats_Nearby_Drop_Down_Background'] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_whats_nearby_dd_background" id="text_colorthemeOption_whats_nearby_dd_background" value="<?php echo (($Region_theme['TO_Whats_Nearby_Drop_Down_Background'] != "") ? $Region_theme['TO_Whats_Nearby_Drop_Down_Background'] : '') ?>">
                            <?php if (isset($Region_theme['TO_Whats_Nearby_Drop_Down_Background']) && $Region_theme['TO_Whats_Nearby_Drop_Down_Background'] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete55"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Whats_Nearby_Drop_Down_Background'; ?>', 'themeOption_whats_nearby_dd_background', 'hideDelete55', 'text_colorthemeOption_whats_nearby_dd_background', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                            <?php } ?>
                        </div>           
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>What's Nearby Drop Down Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="whats_near_dd_font">
                                <option value="">Select Font...</option>
                                <?php
                                $whats_near_dd_font = mysql_query($getFonts);
                                while ($whatsNearDDFont = mysql_fetch_assoc($whats_near_dd_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $whatsNearDDFont['TOF_Name']; ?>" <?php echo (($whats_near_dd_value[0] == $whatsNearDDFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $whatsNearDDFont['TOF_ID'] ?>"><?php echo $whatsNearDDFont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="whats_near_dd_size" size="50" value="<?php echo $whats_near_dd_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_whats_near_dd" style="background-color: <?php echo (($whats_near_dd_value[2] != "") ? $whats_near_dd_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_whats_near_dd" id="text_colorthemeOption_whats_near_dd" value="<?php echo (($whats_near_dd_value[2] != "") ? $whats_near_dd_value[2] : '') ?>">
                            <?php if (isset($whats_near_dd_value[2]) && $whats_near_dd_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete56"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Whats_Nearby_Drop_Down_Text'; ?>', 'themeOption_whats_near_dd', 'hideDelete56', 'text_colorthemeOption_whats_near_dd', '<?php echo $whats_near_dd_value[0]; ?>', '<?php echo $whats_near_dd_value[1]; ?>', '<?php echo $whats_near_dd_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>What's Nearby Drop Down Arrow</label>
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_whats_nearby_dd_arrow" style="background-color: <?php echo (($Region_theme['TO_Whats_Nearby_Drop_Down_Arrow'] != "") ? $Region_theme['TO_Whats_Nearby_Drop_Down_Arrow'] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_whats_nearby_dd_arrow" id="text_colorthemeOption_whats_nearby_dd_arrow" value="<?php echo (($Region_theme['TO_Whats_Nearby_Drop_Down_Arrow'] != "") ? $Region_theme['TO_Whats_Nearby_Drop_Down_Arrow'] : '') ?>">
                        <?php if (isset($Region_theme['TO_Whats_Nearby_Drop_Down_Arrow']) && $Region_theme['TO_Whats_Nearby_Drop_Down_Arrow'] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete57"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Whats_Nearby_Drop_Down_Arrow'; ?>', 'themeOption_whats_nearby_dd_arrow', 'hideDelete57', 'text_colorthemeOption_whats_nearby_dd_arrow', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                <?php } if (isset($activeRegion['R_Type']) && $activeRegion['R_Type'] != 6) { ?>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Photo Gallery Description Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="gallery_desc_text_font">
                                <option value="">Select Font...</option>
                                <?php
                                $gallery_desc_text_font = mysql_query($getFonts);
                                while ($galleryDescTextFont = mysql_fetch_assoc($gallery_desc_text_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $galleryDescTextFont['TOF_Name']; ?>" <?php echo (($gallery_desc_text_value[0] == $galleryDescTextFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $galleryDescTextFont['TOF_ID'] ?>"><?php echo $galleryDescTextFont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="gallery_desc_text_size" size="50" value="<?php echo $gallery_desc_text_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_gallery_desc_text" style="background-color: <?php echo (($gallery_desc_text_value[2] != "") ? $gallery_desc_text_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_gallery_desc_text" id="text_colorthemeOption_gallery_desc_text" value="<?php echo (($gallery_desc_text_value[2] != "") ? $gallery_desc_text_value[2] : '') ?>">
                            <?php if (isset($gallery_desc_text_value[2]) && $gallery_desc_text_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete58"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Photo_Gallery_Description_Text'; ?>', 'themeOption_gallery_desc_text', 'hideDelete58', 'text_colorthemeOption_gallery_desc_text', '<?php echo $gallery_desc_text_value[0]; ?>', '<?php echo $gallery_desc_text_value[1]; ?>', '<?php echo $gallery_desc_text_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Photo Gallery Background Color</label>
                        <div class="form-data theme-options">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_photo_gallery_bg_colour" style="background-color: <?php echo (($Region_theme['TO_Photo_Gallery_Background_Colour'] != "") ? $Region_theme['TO_Photo_Gallery_Background_Colour'] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_photo_gallery_bg_colour" id="text_colorthemeOption_photo_gallery_bg_colour" value="<?php echo (($Region_theme['TO_Photo_Gallery_Background_Colour'] != "") ? $Region_theme['TO_Photo_Gallery_Background_Colour'] : '') ?>">
                            <?php if (isset($Region_theme['TO_Photo_Gallery_Background_Colour']) && $Region_theme['TO_Photo_Gallery_Background_Colour'] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete59"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Photo_Gallery_Background_Colour'; ?>', 'themeOption_photo_gallery_bg_colour', 'hideDelete59', 'text_colorthemeOption_photo_gallery_bg_colour', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } if (isset($activeRegion['R_Type']) && $activeRegion['R_Type'] == 6) { ?>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Get This Coupon Title Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="get_this_coupon_title_font">
                                <option value="">Select Font...</option>
                                <?php
                                $get_this_coupon_title_font = mysql_query($getFonts);
                                while ($getThisCouponTitleFont = mysql_fetch_assoc($get_this_coupon_title_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $getThisCouponTitleFont['TOF_Name']; ?>" <?php echo (($get_this_coupon_title_text_value[0] == $getThisCouponTitleFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $getThisCouponTitleFont['TOF_ID'] ?>"><?php echo $getThisCouponTitleFont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="get_this_coupon_title_size" size="50" value="<?php echo $get_this_coupon_title_text_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_get_this_coupon_title" style="background-color: <?php echo (($get_this_coupon_title_text_value[2] != "") ? $get_this_coupon_title_text_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_get_this_coupon_title" id="text_colorthemeOption_get_this_coupon_title" value="<?php echo (($get_this_coupon_title_text_value[2] != "") ? $get_this_coupon_title_text_value[2] : '') ?>">
                            <?php if (isset($get_this_coupon_title_text_value[2]) && $get_this_coupon_title_text_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete60"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Get_This_Coupon_Title_Text'; ?>', 'themeOption_get_this_coupon_title', 'hideDelete60', '<?php echo $get_this_coupon_title_text_value[0]; ?>', '<?php echo $get_this_coupon_title_text_value[1]; ?>', '<?php echo $get_this_coupon_title_text_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Get This Coupon Description Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="get_this_coupon_description_font">
                                <option value="">Select Font...</option>
                                <?php
                                $get_this_coupon_desc_font = mysql_query($getFonts);
                                while ($getThisCouponDescFont = mysql_fetch_assoc($get_this_coupon_desc_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $getThisCouponDescFont['TOF_Name']; ?>" <?php echo (($get_this_coupon_title_description_value[0] == $getThisCouponDescFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $getThisCouponDescFont['TOF_ID'] ?>"><?php echo $getThisCouponDescFont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="get_this_coupon_description_size" size="50" value="<?php echo $get_this_coupon_title_description_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_get_this_coupon_description" style="background-color: <?php echo (($get_this_coupon_title_description_value[2] != "") ? $get_this_coupon_title_description_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_get_this_coupon_description" id="text_colorthemeOption_get_this_coupon_description" value="<?php echo (($get_this_coupon_title_description_value[2] != "") ? $get_this_coupon_title_description_value[2] : '') ?>">
                            <?php if (isset($get_this_coupon_title_description_value[2]) && $get_this_coupon_title_description_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete61"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Get_This_Coupon_Description_Text'; ?>', 'themeOption_get_this_coupon_description', 'hideDelete61', '<?php echo $get_this_coupon_title_description_value[0]; ?>', '<?php echo $get_this_coupon_title_description_value[1]; ?>', '<?php echo $get_this_coupon_title_description_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Top Of Page Icon</label>
                    <div class="form-data theme-options">
                        <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                            <input class="fileInput" type="file" name="Top_Page_Icon[]" onchange="show_file_name(this.files[0].name, 'top_page_icon')"/>
                        </span>
                        <input id="top_page_icon" class="uploadFileName" style="display:none;" disabled>
                        <?PHP if ($Region_theme['TO_Top_Page_Icon']) { ?>
                            <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $Region_theme['TO_Top_Page_Icon']; ?>" target="_blank">View</a>
                            <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options.php?rid=<?php echo $regionID ?>&flag=top_page_icon&op=del">Delete</a>
                        <?php } ?>
                    </div>
                </div>
            </fieldset>
            <?php if (isset($activeRegion['R_Type']) && $activeRegion['R_Type'] != 6) { ?>
                <fieldset>
                    <legend>Events Page</legend>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Search Fields Background</label>
                        <div class="form-data theme-options">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_calendar_dd_bg" style="background-color: <?php echo (($Region_theme['TO_Calendar_Drop_Down_Background'] != "") ? $Region_theme['TO_Calendar_Drop_Down_Background'] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_calendar_dd_bg" id="text_colorthemeOption_calendar_dd_bg" value="<?php echo (($Region_theme['TO_Calendar_Drop_Down_Background'] != "") ? $Region_theme['TO_Calendar_Drop_Down_Background'] : '') ?>">
                            <?php if (isset($Region_theme['TO_Calendar_Drop_Down_Background']) && $Region_theme['TO_Calendar_Drop_Down_Background'] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete62"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Calendar_Drop_Down_Background'; ?>', 'themeOption_calendar_dd_bg', 'hideDelete62', 'text_colorthemeOption_calendar_dd_bg', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Search Fields Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="calender_dd_text_font">
                                <option value="">Select Font...</option>
                                <?php
                                $calender_dd_text_font = mysql_query($getFonts);
                                while ($calenderDDTextFont = mysql_fetch_assoc($calender_dd_text_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $calenderDDTextFont['TOF_Name']; ?>" <?php echo (($calender_dd_text_value[0] == $calenderDDTextFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $calenderDDTextFont['TOF_ID'] ?>"><?php echo $calenderDDTextFont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="calender_dd_text_size" size="50" value="<?php echo $calender_dd_text_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_calender_dd_text" style="background-color: <?php echo (($calender_dd_text_value[2] != "") ? $calender_dd_text_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_calender_dd_text" id="text_colorthemeOption_calender_dd_text" value="<?php echo (($calender_dd_text_value[2] != "") ? $calender_dd_text_value[2] : '') ?>">
                            <?php if (isset($calender_dd_text_value[2]) && $calender_dd_text_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete63"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Calendar_Drop_Down_Text'; ?>', 'themeOption_calender_dd_text', 'hideDelete63', 'text_colorthemeOption_calender_dd_text', '<?php echo $calender_dd_text_value[0]; ?>', '<?php echo $calender_dd_text_value[1]; ?>', '<?php echo $calender_dd_text_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Search Fields Outline</label>
                        <div class="form-data theme-options">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_calendar_dd_outline" style="background-color: <?php echo (($Region_theme['TO_Calendar_Drop_Down_Outline'] != "") ? $Region_theme['TO_Calendar_Drop_Down_Outline'] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_calendar_dd_outline" id="text_colorthemeOption_calendar_dd_outline" value="<?php echo (($Region_theme['TO_Calendar_Drop_Down_Outline'] != "") ? $Region_theme['TO_Calendar_Drop_Down_Outline'] : '') ?>">
                            <?php if (isset($Region_theme['TO_Calendar_Drop_Down_Outline']) && $Region_theme['TO_Calendar_Drop_Down_Outline'] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete64"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Calendar_Drop_Down_Outline'; ?>', 'themeOption_calendar_dd_outline', 'hideDelete64', 'text_colorthemeOption_calendar_dd_outline', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Go Button Background</label>
                        <div class="form-data theme-options">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_calendar_go_btn_bg" style="background-color: <?php echo (($Region_theme['TO_Calendar_Go_Button_Background'] != "") ? $Region_theme['TO_Calendar_Go_Button_Background'] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_calendar_go_btn_bg" id="text_colorthemeOption_calendar_go_btn_bg" value="<?php echo (($Region_theme['TO_Calendar_Go_Button_Background'] != "") ? $Region_theme['TO_Calendar_Go_Button_Background'] : '') ?>">
                            <?php if (isset($Region_theme['TO_Calendar_Go_Button_Background']) && $Region_theme['TO_Calendar_Go_Button_Background'] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete65"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Calendar_Go_Button_Background'; ?>', 'themeOption_calendar_go_btn_bg', 'hideDelete65', 'text_colorthemeOption_calendar_go_btn_bg', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Button Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="calender_dd_go_button_text_font">
                                <option value="">Select Font...</option>
                                <?php
                                $calender_dd_go_button_text_font = mysql_query($getFonts);
                                while ($calenderGoButtonTextFont = mysql_fetch_assoc($calender_dd_go_button_text_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $calenderGoButtonTextFont['TOF_Name']; ?>" <?php echo (($calender_dd_go_button_text_value[0] == $calenderGoButtonTextFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $calenderGoButtonTextFont['TOF_ID'] ?>"><?php echo $calenderGoButtonTextFont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="calender_dd_go_button_text_size" size="50" value="<?php echo $calender_dd_go_button_text_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_calender_dd_go_button_text" style="background-color: <?php echo (($calender_dd_go_button_text_value[2] != "") ? $calender_dd_go_button_text_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_calender_dd_go_button_text" id="text_colorthemeOption_calender_dd_go_button_text" value="<?php echo (($calender_dd_go_button_text_value[2] != "") ? $calender_dd_go_button_text_value[2] : '') ?>">
                            <?php if (isset($calender_dd_go_button_text_value[2]) && $calender_dd_go_button_text_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete66"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Calendar_Go_Button_Text'; ?>', 'themeOption_calender_dd_go_button_text', 'hideDelete66', 'text_colorthemeOption_calender_dd_go_button_text', '<?php echo $calender_dd_go_button_text_value[0]; ?>', '<?php echo $calender_dd_go_button_text_value[1]; ?>', '<?php echo $calender_dd_go_button_text_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Title</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="detail_page_event_main_title_font">
                                <option value="">Select Font...</option>
                                <?php
                                $detail_page_event_main_title_font = mysql_query($getFonts);
                                while ($detailPageEventMainTitlefont = mysql_fetch_assoc($detail_page_event_main_title_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $detailPageEventMainTitlefont['TOF_Name']; ?>" <?php echo (($detail_page_event_main_title_value[0] == $detailPageEventMainTitlefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $detailPageEventMainTitlefont['TOF_ID'] ?>"><?php echo $detailPageEventMainTitlefont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="detail_page_event_main_title_size" size="50" value="<?php echo $detail_page_event_main_title_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_detail_page_event_main_title" style="background-color: <?php echo (($detail_page_event_main_title_value[2] != "") ? $detail_page_event_main_title_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_detail_page_event_main_title" id="text_colorthemeOption_detail_page_event_main_title" value="<?php echo (($detail_page_event_main_title_value[2] != "") ? $detail_page_event_main_title_value[2] : '') ?>">
                            <?php if (isset($detail_page_event_main_title_value[2]) && $detail_page_event_main_title_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete67"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Event_Page_Main_Title'; ?>', 'themeOption_detail_page_event_main_title', 'hideDelete67', 'text_colorthemeOption_detail_page_event_main_title', '<?php echo $detail_page_event_main_title_value[0]; ?>', '<?php echo $detail_page_event_main_title_value[1]; ?>', '<?php echo $detail_page_event_main_title_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Date Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="detail_page_event_date_font">
                                <option value="">Select Font...</option>
                                <?php
                                $detail_page_event_date_font = mysql_query($getFonts);
                                while ($detailPageEventDatefont = mysql_fetch_assoc($detail_page_event_date_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $detailPageEventDatefont['TOF_Name']; ?>" <?php echo (($detail_page_event_date_value[0] == $detailPageEventDatefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $detailPageEventDatefont['TOF_ID'] ?>"><?php echo $detailPageEventDatefont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="detail_page_event_date_size" size="50" value="<?php echo $detail_page_event_date_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_detail_page_event_date" style="background-color: <?php echo (($detail_page_event_date_value[2] != "") ? $detail_page_event_date_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_detail_page_event_date" id="text_colorthemeOption_detail_page_event_date" value="<?php echo (($detail_page_event_date_value[2] != "") ? $detail_page_event_date_value[2] : '') ?>">
                            <?php if (isset($detail_page_event_date_value[2]) && $detail_page_event_date_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete68"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Event_Page_Main_Title'; ?>', 'themeOption_detail_page_event_date', 'hideDelete68', 'text_colorthemeOption_detail_page_event_date', '<?php echo $detail_page_event_date_value[0]; ?>', '<?php echo $detail_page_event_date_value[1]; ?>', '<?php echo $detail_page_event_date_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Data Label</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="detail_page_event_data_label_font">
                                <option value="">Select Font...</option>
                                <?php
                                $detail_page_event_data_label_font = mysql_query($getFonts);
                                while ($detailPageEventDataLabelfont = mysql_fetch_assoc($detail_page_event_data_label_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $detailPageEventDataLabelfont['TOF_Name']; ?>" <?php echo (($detail_page_event_data_label_value[0] == $detailPageEventDataLabelfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $detailPageEventDataLabelfont['TOF_ID'] ?>"><?php echo $detailPageEventDataLabelfont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="detail_page_event_data_label_size" size="50" value="<?php echo $detail_page_event_data_label_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_detail_page_event_data_label" style="background-color: <?php echo (($detail_page_event_data_label_value[2] != "") ? $detail_page_event_data_label_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_detail_page_event_data_label" id="text_colorthemeOption_detail_page_event_data_label" value="<?php echo (($detail_page_event_data_label_value[2] != "") ? $detail_page_event_data_label_value[2] : '') ?>">
                            <?php if (isset($detail_page_event_data_label_value[2]) && $detail_page_event_data_label_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete69"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Event_Page_Main_Title'; ?>', 'themeOption_detail_page_event_data_label', 'hideDelete69', 'text_colorthemeOption_detail_page_event_data_label', '<?php echo $detail_page_event_data_label_value[0]; ?>', '<?php echo $detail_page_event_data_label_value[1]; ?>', '<?php echo $detail_page_event_data_label_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Data Content</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="detail_page_event_data_content_font">
                                <option value="">Select Font...</option>
                                <?php
                                $detail_page_event_data_content_font = mysql_query($getFonts);
                                while ($detailPageEventDataContentfont = mysql_fetch_assoc($detail_page_event_data_content_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $detailPageEventDataContentfont['TOF_Name']; ?>" <?php echo (($detail_page_event_data_content_value[0] == $detailPageEventDataContentfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $detailPageEventDataContentfont['TOF_ID'] ?>"><?php echo $detailPageEventDataContentfont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="detail_page_event_data_content_size" size="50" value="<?php echo $detail_page_event_data_content_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_detail_page_event_data_content" style="background-color: <?php echo (($detail_page_event_data_content_value[2] != "") ? $detail_page_event_data_content_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_detail_page_event_data_content" id="text_colorthemeOption_detail_page_event_data_content" value="<?php echo (($detail_page_event_data_content_value[2] != "") ? $detail_page_event_data_content_value[2] : '') ?>">
                            <?php if (isset($detail_page_event_data_content_value[2]) && $detail_page_event_data_content_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete70"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Event_Page_Main_Title'; ?>', 'themeOption_detail_page_event_data_content', 'hideDelete70', 'text_colorthemeOption_detail_page_event_data_content', '<?php echo $detail_page_event_data_content_value[0]; ?>', '<?php echo $detail_page_event_data_content_value[1]; ?>', '<?php echo $detail_page_event_data_content_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Data Border Color</label>
                        <div class="form-data theme-options">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_detail_page_event_border_color" style="background-color: <?php echo (($Region_theme['TO_Event_Page_Data_Border_Color'] != "") ? $Region_theme['TO_Event_Page_Data_Border_Color'] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_detail_page_event_border_color" id="text_colorthemeOption_detail_page_event_border_color" value="<?php echo (($Region_theme['TO_Event_Page_Data_Border_Color'] != "") ? $Region_theme['TO_Event_Page_Data_Border_Color'] : '') ?>">
                            <?php if (isset($Region_theme['TO_Event_Page_Data_Border_Color']) && $Region_theme['TO_Event_Page_Data_Border_Color'] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete71"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Event_Page_Data_Border_Color'; ?>', 'themeOption_detail_page_event_border_color', 'hideDelete71', 'text_colorthemeOption_detail_page_event_border_color', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <legend>Stories Page</legend>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Title</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="story_main_title_font">
                                <option value="">Select Font...</option>
                                <?php
                                $story_main_title_font = mysql_query($getFonts);
                                while ($storyMainTitleFont = mysql_fetch_assoc($story_main_title_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $storyMainTitleFont['TOF_Name']; ?>" <?php echo (($story_main_title_value[0] == $storyMainTitleFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $storyMainTitleFont['TOF_ID'] ?>"><?php echo $storyMainTitleFont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="story_main_title_size" size="50" value="<?php echo $story_main_title_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_story_main_title" style="background-color: <?php echo (($story_main_title_value[2] != "") ? $story_main_title_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_story_main_title" id="text_colorthemeOption_story_main_title" value="<?php echo (($story_main_title_value[2] != "") ? $story_main_title_value[2] : '') ?>">
                            <?php if (isset($story_main_title_value[2]) && $story_main_title_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete72"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Story_Page_Main_Title'; ?>', 'themeOption_story_main_title', 'hideDelete72', 'text_colorthemeOption_story_main_title', '<?php echo $story_main_title_value[0]; ?>', '<?php echo $story_main_title_value[1]; ?>', '<?php echo $story_main_title_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Share Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="story_share_text_font">
                                <option value="">Select Font...</option>
                                <?php
                                $story_share_text_font = mysql_query($getFonts);
                                while ($storyShareTextFont = mysql_fetch_assoc($story_share_text_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $storyShareTextFont['TOF_Name']; ?>" <?php echo (($story_share_text_value[0] == $storyShareTextFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $storyShareTextFont['TOF_ID'] ?>"><?php echo $storyShareTextFont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="story_share_text_size" size="50" value="<?php echo $story_share_text_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_story_share_text" style="background-color: <?php echo (($story_share_text_value[2] != "") ? $story_share_text_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_story_share_text" id="text_colorthemeOption_story_share_text" value="<?php echo (($story_share_text_value[2] != "") ? $story_share_text_value[2] : '') ?>">
                            <?php if (isset($story_share_text_value[2]) && $story_share_text_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete73"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Story_Share_Text'; ?>', 'themeOption_story_share_text', 'hideDelete73', 'text_colorthemeOption_story_share_text', '<?php echo $story_share_text_value[0]; ?>', '<?php echo $story_share_text_value[1]; ?>', '<?php echo $story_share_text_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Content Title</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="story_content_title_font">
                                <option value="">Select Font...</option>
                                <?php
                                $story_content_title_font = mysql_query($getFonts);
                                while ($storyContentTitleFont = mysql_fetch_assoc($story_content_title_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $storyContentTitleFont['TOF_Name']; ?>" <?php echo (($story_content_title_value[0] == $storyContentTitleFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $storyContentTitleFont['TOF_ID'] ?>"><?php echo $storyContentTitleFont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="story_content_title_size" size="50" value="<?php echo $story_content_title_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_story_content_title" style="background-color: <?php echo (($story_content_title_value[2] != "") ? $story_content_title_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_story_content_title" id="text_colorthemeOption_story_content_title" value="<?php echo (($story_content_title_value[2] != "") ? $story_content_title_value[2] : '') ?>">
                            <?php if (isset($story_content_title_value[2]) && $story_content_title_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete74"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Story_Page_Content_Title'; ?>', 'themeOption_story_content_title', 'hideDelete74', 'text_colorthemeOption_story_content_title', '<?php echo $story_content_title_value[0]; ?>', '<?php echo $story_content_title_value[1]; ?>', '<?php echo $story_content_title_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Drop Down Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="story_drop_down_text_font">
                                <option value="">Select Font...</option>
                                <?php
                                $story_drop_down_text_font = mysql_query($getFonts);
                                while ($storyDropDownTextFont = mysql_fetch_assoc($story_drop_down_text_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $storyDropDownTextFont['TOF_Name']; ?>" <?php echo (($story_drop_down_text_value[0] == $storyDropDownTextFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $storyDropDownTextFont['TOF_ID'] ?>"><?php echo $storyDropDownTextFont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="story_drop_down_text_size" size="50" value="<?php echo $story_drop_down_text_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_story_drop_down_text" style="background-color: <?php echo (($story_drop_down_text_value[2] != "") ? $story_drop_down_text_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_story_drop_down_text" id="text_colorthemeOption_story_drop_down_text" value="<?php echo (($story_drop_down_text_value[2] != "") ? $story_drop_down_text_value[2] : '') ?>">
                            <?php if (isset($story_drop_down_text_value[2]) && $story_drop_down_text_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete75"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Story_Page_Drop_Down_Text'; ?>', 'themeOption_story_drop_down_text', 'hideDelete75', 'text_colorthemeOption_story_drop_down_text', '<?php echo $story_drop_down_text_value[0]; ?>', '<?php echo $story_drop_down_text_value[1]; ?>', '<?php echo $story_drop_down_text_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Drop Down Background</label>
                        <div class="form-data theme-options">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_story_drop_down_bg" style="background-color: <?php echo (($Region_theme['TO_Story_Page_Drop_Down_Background'] != "") ? $Region_theme['TO_Story_Page_Drop_Down_Background'] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_story_drop_down_bg" id="text_colorthemeOption_story_drop_down_bg" value="<?php echo (($Region_theme['TO_Story_Page_Drop_Down_Background'] != "") ? $Region_theme['TO_Story_Page_Drop_Down_Background'] : '') ?>">
                            <?php if (isset($Region_theme['TO_Story_Page_Drop_Down_Background']) && $Region_theme['TO_Story_Page_Drop_Down_Background'] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete76"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Story_Page_Drop_Down_Background'; ?>', 'themeOption_story_drop_down_bg', 'hideDelete76', 'text_colorthemeOption_story_drop_down_bg', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Drop Down Arrow</label>
                        <div class="form-data theme-options">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_story_drop_down_arrow" style="background-color: <?php echo (($Region_theme['TO_Story_Page_Drop_Down_Arrow'] != "") ? $Region_theme['TO_Story_Page_Drop_Down_Arrow'] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_story_drop_down_arrow" id="text_colorthemeOption_story_drop_down_arrow" value="<?php echo (($Region_theme['TO_Story_Page_Drop_Down_Arrow'] != "") ? $Region_theme['TO_Story_Page_Drop_Down_Arrow'] : '') ?>">
                            <?php if (isset($Region_theme['TO_Story_Page_Drop_Down_Arrow']) && $Region_theme['TO_Story_Page_Drop_Down_Arrow'] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete77"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Story_Page_Drop_Down_Arrow'; ?>', 'themeOption_story_drop_down_arrow', 'hideDelete77', 'text_colorthemeOption_story_drop_down_arrow', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>See All Stories Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="story_see_all_text_font">
                                <option value="">Select Font...</option>
                                <?php
                                $story_see_all_text_font = mysql_query($getFonts);
                                while ($StorySeeAllTextFont = mysql_fetch_assoc($story_see_all_text_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $StorySeeAllTextFont['TOF_Name']; ?>" <?php echo (($story_see_all_text_value[0] == $StorySeeAllTextFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $StorySeeAllTextFont['TOF_ID'] ?>"><?php echo $StorySeeAllTextFont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="story_see_all_text_size" size="50" value="<?php echo $story_see_all_text_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_story_see_all_text" style="background-color: <?php echo (($story_see_all_text_value[2] != "") ? $story_see_all_text_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_story_see_all_text" id="text_colorthemeOption_story_see_all_text" value="<?php echo (($story_see_all_text_value[2] != "") ? $story_see_all_text_value[2] : '') ?>">
                            <?php if (isset($story_see_all_text_value[2]) && $story_see_all_text_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete78"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Story_See_All_Text'; ?>', 'themeOption_story_see_all_text', 'hideDelete78', 'text_colorthemeOption_story_see_all_text', '<?php echo $story_see_all_text_value[0]; ?>', '<?php echo $story_see_all_text_value[1]; ?>', '<?php echo $story_see_all_text_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <legend>Routes Page</legend>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Title</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="route_main_title_font">
                                <option value="">Select Font...</option>
                                <?php
                                $route_main_title_font = mysql_query($getFonts);
                                while ($routeMainTitlefont = mysql_fetch_assoc($route_main_title_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $routeMainTitlefont['TOF_Name']; ?>" <?php echo (($route_main_title_value[0] == $routeMainTitlefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $routeMainTitlefont['TOF_ID'] ?>"><?php echo $routeMainTitlefont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="route_main_title_size" size="50" value="<?php echo $route_main_title_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_route_main_title" style="background-color: <?php echo (($route_main_title_value[2] != "") ? $route_main_title_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_route_main_title" id="text_colorthemeOption_route_main_title" value="<?php echo (($route_main_title_value[2] != "") ? $route_main_title_value[2] : '') ?>">
                            <?php if (isset($route_main_title_value[2]) && $route_main_title_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete79"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Route_Page_Main_Title'; ?>', 'themeOption_route_main_title', 'hideDelete79', 'text_colorthemeOption_route_main_title', '<?php echo $route_main_title_value[0]; ?>', '<?php echo $route_main_title_value[1]; ?>', '<?php echo $route_main_title_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Data Label</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="route_data_label_font">
                                <option value="">Select Font...</option>
                                <?php
                                $route_data_label_font = mysql_query($getFonts);
                                while ($routeDataLabelfont = mysql_fetch_assoc($route_data_label_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $routeDataLabelfont['TOF_Name']; ?>" <?php echo (($route_data_label_value[0] == $routeDataLabelfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $routeDataLabelfont['TOF_ID'] ?>"><?php echo $routeDataLabelfont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="route_data_label_size" size="50" value="<?php echo $route_data_label_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_route_data_label" style="background-color: <?php echo (($route_data_label_value[2] != "") ? $route_data_label_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_route_data_label" id="text_colorthemeOption_route_data_label" value="<?php echo (($route_data_label_value[2] != "") ? $route_data_label_value[2] : '') ?>">
                            <?php if (isset($route_data_label_value[2]) && $route_data_label_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete80"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Route_Page_Data_Label'; ?>', 'themeOption_route_data_label', 'hideDelete80', 'text_colorthemeOption_route_data_label', '<?php echo $route_data_label_value[0]; ?>', '<?php echo $route_data_label_value[1]; ?>', '<?php echo $route_data_label_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Data Content</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="route_data_content_font">
                                <option value="">Select Font...</option>
                                <?php
                                $route_data_content_font = mysql_query($getFonts);
                                while ($routeDataContentfont = mysql_fetch_assoc($route_data_content_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $routeDataContentfont['TOF_Name']; ?>" <?php echo (($route_data_content_value[0] == $routeDataContentfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $routeDataContentfont['TOF_ID'] ?>"><?php echo $routeDataContentfont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="route_data_content_size" size="50" value="<?php echo $route_data_content_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_route_data_content" style="background-color: <?php echo (($route_data_content_value[2] != "") ? $route_data_content_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_route_data_content" id="text_colorthemeOption_route_data_content" value="<?php echo (($route_data_content_value[2] != "") ? $route_data_content_value[2] : '') ?>">
                            <?php if (isset($route_data_content_value[2]) && $route_data_content_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete81"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Route_Page_Data_Content'; ?>', 'themeOption_route_data_content', 'hideDelete81', 'text_colorthemeOption_route_data_content', '<?php echo $route_data_content_value[0]; ?>', '<?php echo $route_data_content_value[1]; ?>', '<?php echo $route_data_content_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <legend>Pagination</legend>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Number Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="pagination_number_text_font">
                                <option value="">Select Font...</option>
                                <?php
                                $pagination_number_text_font = mysql_query($getFonts);
                                while ($paginationNumberTextFont = mysql_fetch_assoc($pagination_number_text_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $paginationNumberTextFont['TOF_Name']; ?>" <?php echo (($pagination_number_text_value[0] == $paginationNumberTextFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $paginationNumberTextFont['TOF_ID'] ?>"><?php echo $paginationNumberTextFont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="pagination_number_text_size" size="50" value="<?php echo $pagination_number_text_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_pagination_number_text" style="background-color: <?php echo (($pagination_number_text_value[2] != "") ? $pagination_number_text_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_pagination_number_text" id="text_colorthemeOption_pagination_number_text" value="<?php echo (($pagination_number_text_value[2] != "") ? $pagination_number_text_value[2] : '') ?>">
                            <?php if (isset($pagination_number_text_value[2]) && $pagination_number_text_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete82"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Pagination_Number_Text'; ?>', 'themeOption_pagination_number_text', 'hideDelete82', 'text_colorthemeOption_pagination_number_text', '<?php echo $pagination_number_text_value[0]; ?>', '<?php echo $pagination_number_text_value[1]; ?>', '<?php echo $pagination_number_text_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Number Background</label>
                        <div class="form-data theme-options">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_pagination_bg_no" style="background-color: <?php echo (($Region_theme['TO_Pagination_Number_Background'] != "") ? $Region_theme['TO_Pagination_Number_Background'] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_pagination_bg_no" id="text_colorthemeOption_pagination_bg_no" value="<?php echo (($Region_theme['TO_Pagination_Number_Background'] != "") ? $Region_theme['TO_Pagination_Number_Background'] : '') ?>">
                            <?php if (isset($Region_theme['TO_Pagination_Number_Background']) && $Region_theme['TO_Pagination_Number_Background'] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete83"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Pagination_Number_Background'; ?>', 'themeOption_pagination_bg_no', 'hideDelete83', 'text_colorthemeOption_pagination_bg_no', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Selected Number Background</label>
                        <div class="form-data theme-options">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_pagination_bg_no_sel" style="background-color: <?php echo (($Region_theme['TO_Pagination_Selected_Number_Background'] != "") ? $Region_theme['TO_Pagination_Selected_Number_Background'] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_pagination_bg_no_sel" id="text_colorthemeOption_pagination_bg_no_sel" value="<?php echo (($Region_theme['TO_Pagination_Selected_Number_Background'] != "") ? $Region_theme['TO_Pagination_Selected_Number_Background'] : '') ?>">
                            <?php if (isset($Region_theme['TO_Pagination_Selected_Number_Background']) && $Region_theme['TO_Pagination_Selected_Number_Background'] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete84"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Pagination_Selected_Number_Background'; ?>', 'themeOption_pagination_bg_no_sel', 'hideDelete84', 'text_colorthemeOption_pagination_bg_no_sel', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Border</label>
                        <div class="form-data theme-options">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_pagination_line" style="background-color: <?php echo (($Region_theme['TO_Pagination_Line'] != "") ? $Region_theme['TO_Pagination_Line'] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_pagination_line" id="text_colorthemeOption_pagination_line" value="<?php echo (($Region_theme['TO_Pagination_Line'] != "") ? $Region_theme['TO_Pagination_Line'] : '') ?>">
                            <?php if (isset($Region_theme['TO_Pagination_Line']) && $Region_theme['TO_Pagination_Line'] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete85"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Pagination_Line'; ?>', 'themeOption_pagination_line', 'hideDelete85', 'text_colorthemeOption_pagination_line', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Word Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="pagination_text_font">
                                <option value="">Select Font...</option>
                                <?php
                                $pagination_text_font = mysql_query($getFonts);
                                while ($paginationTextFont = mysql_fetch_assoc($pagination_text_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $paginationTextFont['TOF_Name']; ?>" <?php echo (($pagination_text_value[0] == $paginationTextFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $paginationTextFont['TOF_ID'] ?>"><?php echo $paginationTextFont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="pagination_text_size" size="50" value="<?php echo $pagination_text_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_pagination_text" style="background-color: <?php echo (($pagination_text_value[2] != "") ? $pagination_text_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_pagination_text" id="text_colorthemeOption_pagination_text" value="<?php echo (($pagination_text_value[2] != "") ? $pagination_text_value[2] : '') ?>">
                            <?php if (isset($pagination_text_value[2]) && $pagination_text_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete86"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Pagination_Text'; ?>', 'themeOption_pagination_text', 'hideDelete86', 'text_colorthemeOption_pagination_text', '<?php echo $pagination_text_value[0]; ?>', '<?php echo $pagination_text_value[1]; ?>', '<?php echo $pagination_text_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                </fieldset>
            <?php } ?>
            <fieldset>
                <legend>General</legend>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Text Link</label>
                    <div class="form-data theme-options">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_text_link" style="background-color: <?php echo (($Region_theme['TO_Text_Link'] != "") ? $Region_theme['TO_Text_Link'] : '') ?>"></div></div>
                        <input type="hidden" name="TO_Text_Link" id="text_colorthemeOption_text_link" value="<?php echo (($Region_theme['TO_Text_Link'] != "") ? $Region_theme['TO_Text_Link'] : '') ?>">
                        <?php if (isset($Region_theme['TO_Text_Link']) && $Region_theme['TO_Text_Link'] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete87"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Text_Link'; ?>', 'themeOption_text_link', 'hideDelete87', 'text_colorthemeOption_text_link', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Body Copy</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="general_body_copy_font">
                            <option value="">Select Font...</option>
                            <?php
                            $general_body_copy_font = mysql_query($getFonts);
                            while ($generalbodycopyfont = mysql_fetch_assoc($general_body_copy_font)) {
                                ?>
                                <option style="font-family: <?php echo $generalbodycopyfont['TOF_Name']; ?>" <?php echo (($general_body_copy_value[0] == $generalbodycopyfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $generalbodycopyfont['TOF_ID'] ?>"><?php echo $generalbodycopyfont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="general_body_copy_size" size="50" value="<?php echo $general_body_copy_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_general_body_copy" style="background-color: <?php echo (($general_body_copy_value[2] != "") ? $general_body_copy_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_general_body_copy" id="text_colorthemeOption_general_body_copy" value="<?php echo (($general_body_copy_value[2] != "") ? $general_body_copy_value[2] : '') ?>">
                        <?php if (isset($general_body_copy_value[2]) && $general_body_copy_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete88"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_General_Body_Copy'; ?>', 'themeOption_general_body_copy', 'hideDelete88', 'text_colorthemeOption_general_body_copy', '<?php echo $general_body_copy_value[0]; ?>', '<?php echo $general_body_copy_value[1]; ?>', '<?php echo $general_body_copy_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Body Copy Line Spacing</label>
                    <div class="form-data theme-options">    
                        <input class="theme-options-size" type="text" name="general_body_copy_space" size="50" value="<?php echo $Region_theme['TO_General_Body_Copy_Line_Spacing']; ?>" placeholder="Enter line space">
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Scroll Down Arrow</label>
                    <div class="form-data theme-options">
                        <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                            <input class="fileInput" type="file" name="Scroll_Down_Arrow[]" onchange="show_file_name(this.files[0].name, 'scroll_down_arrow')"/>
                        </span>
                        <input id="scroll_down_arrow" class="uploadFileName" style="display:none;" disabled>
                        <?PHP if ($Region_theme['TO_Scroll_Down_Arrow']) { ?>
                            <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $Region_theme['TO_Scroll_Down_Arrow']; ?>" target="_blank">View</a>
                            <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options.php?rid=<?php echo $regionID ?>&flag=scroll_down_arrow&op=del">Delete</a>
                        <?php } ?>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Logo</legend>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Desktop Logo</label>
                    <div class="form-data div_image_library cat-region-width">
                        <label for="photo0" class="daily_browse with-no-library">Browse</label>
                        <div class="cropit-image-preview aboutus-photo-perview">
                            <img class="preview-img preview-img-script0" style="display: none;" src="">    
                            <?php if ($Region_theme['TO_Desktop_Logo'] != '') { ?>
                                <img class="existing-img existing_imgs0" src="<?php echo IMG_LOC_REL . $Region_theme['TO_Desktop_Logo'] ?>" >
                            <?php } ?>
                        </div>
                        <input type="file" onchange="show_file_name(0, this)" name="pic[]" id="photo0" style="display: none;">

                        <input class="region-logo-prvw margin-theme-logo" type="text" placeholder="Alt name" name="desktop_logo_alt" value="<?php echo $Region_theme['TO_Desktop_Logo_Alt'] ?>">
                        <?php if ($Region_theme['TO_Desktop_Logo'] != '') { ?>
                            <a class="deletePhoto delete-region-cat-photo" style="margin-top: 10px;" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options.php?rid=<?php echo $regionID ?>&flag=desktop_logo&op=del">Delete Photo</a>
                        <?php } ?>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Default Images</legend>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Header Image</label>
                    <div class="form-data div_image_library cat-region-width">
                        <label for="photo1" class="daily_browse with-no-library">Browse</label>
                        <div class="cropit-image-preview aboutus-photo-perview">
                            <img class="preview-img preview-img-script1" style="display: none;" src="">    
                            <?php if ($Region_theme['TO_Default_Header_Desktop'] != '') { ?>
                                <img class="existing-img existing_imgs0" src="<?php echo IMG_LOC_REL . $Region_theme['TO_Default_Header_Desktop'] ?>" >
                            <?php } ?>
                        </div>
                        <input type="file" onchange="show_file_name(1, this)" name="pic1[]" id="photo1" style="display: none;">

                        <?php if ($Region_theme['TO_Default_Header_Desktop'] != '') { ?>
                            <a class="deletePhoto delete-region-cat-photo" style="margin-top: 10px;" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options.php?rid=<?php echo $regionID ?>&flag=desktop_header&op=del">Delete Photo</a>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Thumbnail Image</label>
                    <div class="form-data div_image_library cat-region-width">
                        <label for="photo2" class="daily_browse with-no-library">Browse</label>
                        <div class="cropit-image-preview aboutus-photo-perview">
                            <img class="preview-img preview-img-script2" style="display: none;" src="">    
                            <?php if ($Region_theme['TO_Default_Thumbnail_Desktop'] != '') { ?>
                                <img class="existing-img existing_imgs2" src="<?php echo IMG_LOC_REL . $Region_theme['TO_Default_Thumbnail_Desktop'] ?>" >
                            <?php } ?>
                        </div>
                        <input type="file" onchange="show_file_name(2, this)" name="pic2[]" id="photo2" style="display: none;">

                        <?php if ($Region_theme['TO_Default_Thumbnail_Desktop'] != '') { ?>
                            <a class="deletePhoto delete-region-cat-photo" style="margin-top: 10px;" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options.php?rid=<?php echo $regionID ?>&flag=desktop_thumbnail&op=del">Delete Photo</a>
                        <?php } ?>
                    </div>
                </div>

            </fieldset>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="save_color" id="submit_button" value="Submit" />
                </div>
            </div>

        </form>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>
<script>
    function removeColorImage(id, colorVal, backgrontColorsCahnge, hideDelete, divEmpty, font_size, font_color) {
        var remove = confirm("Are you sure this action can not be undone!");
        if (remove == true) {
            var imagedirest = 'imagelink';
            if (font_size != '') {
                var value = font_size + '>';
            }
            var tblname = 'tbl_Theme_Options';
            $.ajax({
                type: "POST",
                url: "remove-colors.php",
                data: {
                    regionID_Check: id,
                    themOptionField: colorVal,
                    tablename: tblname,
                    value: value,
                    images_check: imagedirest
                }
            }).done(function (msg) {
                if (msg == 1) {
                    $("#" + divEmpty).val('');
                    $('.' + hideDelete).hide();
                    $('#' + backgrontColorsCahnge).css({'background-color': '#ffff'});
                    swal("Data Deleted", "Data has been deleted successfully.", "success");
                }
            });
        }
    }
    function removeColor(id, colorVal, backgrontColorsCahnge, hideDelete, divEmpty, font_family, font_size, font_color) {
        var remove = confirm("Are you sure this action can not be undone!");
        if (remove == true) {
            if (font_family != '' && font_size != '') {
                var value = font_family + '-' + font_size + '-';
            }
            var tblname = 'tbl_Theme_Options';
            $.ajax({
                type: "POST",
                url: "remove-colors.php",
                data: {
                    regionID: id,
                    themOptionField: colorVal,
                    tablename: tblname,
                    value: value
                }
            }).done(function (msg) {
                if (msg == 1) {
                    $("#" + divEmpty).val('');
                    $('.' + hideDelete).hide();
                    $('#' + backgrontColorsCahnge).css({'background-color': '#ffff'});
                    swal("Data Deleted", "Data has been deleted successfully.", "success");
                }
            });
        }
    }

    $(window).load(function () {
        var regionID = '<?php echo $regionID ?>';
        if (typeof regionID != 'undefined' && regionID > 0) {
            $(".color-box").click(function () {
                var div_id = $(this).attr('id');
                var savedColor = $('#' + div_id).attr('style').split("#");
                $('.color-box').colpickSetColor(savedColor[1]);
            });
        }
    });
    $(document).ready(function () {
        //*****************************************color box function*************************
        $('.color-box').colpick({
            colorScheme: 'dark',
            layout: 'rgbhex',
            color: 'ff8800',
            onSubmit: function (hsb, hex, rgb, el) {
                $(el).css('background-color', '#' + hex);
                var div_id = $(el).attr('id');
                //change app background color
                document.getElementById("text_color" + div_id).value = '#' + hex;
                $(el).colpickHide();
            }
        });
    });
</script>
<?PHP
require_once '../include/admin/footer.php';
?>
