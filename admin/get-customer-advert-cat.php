<?php
include '../include/config.inc.php';
require_once '../include/login.inc.php';

$region_id = $_GET['region_id'];
$AT_ID = $_GET['advert_type'];
?>
<select name="category"  class="adv-type-options" id="advert-cat" onChange="validate_buy_an_add(0, 0, 1, 0)" <?php echo ($AT_ID != 4) ? 'required="required"' : ''; ?>>
    <option required value="">Select Category</option>
    <?PHP
    if ($region_id > 0) {
        $sql = "SELECT C_ID, C_Name, RC_Name FROM `tbl_Category` 
                LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID
                LEFT JOIN tbl_Region ON RC_R_ID = R_ID 
                WHERE R_ID = '" . encode_strings($region_id, $db) . "' AND C_ID NOT IN(121) AND C_ID NOT IN(121) AND C_Parent =0 ORDER BY RC_Order ASC";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            ?>
            <option value="<?php echo $row['C_ID'] ?>"><?php echo ($row['RC_Name'] != '') ? $row['RC_Name'] : $row['C_Name'] ?></option>
            <?PHP
        }
    }
    ?>
</select>