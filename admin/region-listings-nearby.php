<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';
if (!in_array('regions', $_SESSION['USER_PERMISSIONS']) && !in_array('manage-county-region', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
    $bl_id = $_REQUEST['bl_id'];
    $sortby = $_REQUEST['sortby'];
    $subcategory = $_REQUEST['subcategory'];
    $sql = "SELECT R_ID, R_Name, R_Type, R_Whats_NearBy FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
    $sql_nearby = " SELECT RLN_DIS_BL_ID FROM  tbl_Region_Listings_Nearby 
                    WHERE RLN_R_ID = '" . encode_strings($regionID, $db) . "' AND RLN_BL_ID='$bl_id'";
    $result_nearby = mysql_query($sql_nearby, $db) or die(mysql_error());
    while ($row_nearby = mysql_fetch_assoc($result_nearby)) {
        $childbl_id[] = $row_nearby['RLN_DIS_BL_ID'];
    }
}if ($activeRegion['R_Whats_NearBy'] != 1) {
    header("Location: /admin/regions.php");
    exit();
}
if (isset($_REQUEST['del']) && $_REQUEST['del'] == 'true') {
    $bl_id = $_REQUEST['bl_id'];
    $r_id = $_REQUEST['rid'];
    $d_bl_id = $_REQUEST['d_bl_id'];
    $sql = "INSERT INTO tbl_Region_Listings_Nearby SET RLN_R_ID ='" . $r_id . "', RLN_BL_ID ='" . $d_bl_id . "', RLN_DIS_BL_ID ='" . $bl_id . "'";
    $result = mysql_query($sql) or die(mysql_error());
    $_SESSION['listing_delete'] = 1;
    if ($_REQUEST['page'] > 0) {
        $page = "&page=" . $_REQUEST['page'];
    } else {
        $page = "";
    }
    header("Location: /admin/region-listings-nearby.php?bl_id=" . $_REQUEST['d_bl_id'] . "&rid=" . $_REQUEST['rid'] . $page);
    exit();
}

if ($_REQUEST['rid'] > 0 && $_REQUEST['listings'] > 0 && $_REQUEST['bl_id'] > 0) {
    $bl_id = $_REQUEST['listings'];
    $r_id = $_REQUEST['rid'];
    $en_bl_id = $_REQUEST['bl_id'];
    $sql = "DELETE FROM tbl_Region_Listings_Nearby WHERE RLN_R_ID = '$r_id' AND RLN_BL_ID = '$en_bl_id' AND RLN_DIS_BL_ID = '$bl_id '";
    $result = mysql_query($sql) or die(mysql_error());
    $_SESSION['success'] = 1;
    if ($_REQUEST['page'] > 0) {
        $page = "&page=" . $_REQUEST['page'];
    } else {
        $page = "";
    }
    header("Location: /admin/region-listings-nearby.php?bl_id=" . $en_bl_id . "&rid=" . $r_id . $page);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Listings</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">

        <div class="content-sub-header link-header region-header-padding">
            <div class="data-column spl-name padding-none">Select Listings for Nearby <?php echo $activeRegion['R_Name'] ?>
            </div>
        </div>
        <form name="form1" method="GET" id="org_listings_form">
            <input type="hidden" id="rid" name="rid" value="<?php echo $regionID ?>">
            <input type="hidden" id="bl_id" name="bl_id" value="<?php echo $bl_id ?>">
            <div class="content-sub-header link-header region-header-padding">
                <div class="data-column spl-org-listngs padding-org-listing"><span class="addlisting font_weight_normal" >Sort: </span>
                    <select name="sortby" id="categorydd" onchange="getSubOptions(<?php echo $regionID ?>, this.value)">
                        <option value="">Sort by:</option>
                        <?PHP
                        $sql = "SELECT C_ID, C_Name, RC_Name FROM tbl_Category 
                                LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID
                                WHERE C_Parent = 0 AND RC_R_ID = " . $regionID . " AND RC_Status = 0 AND C_Is_Blog=0 AND C_Is_Product_Web = 0 GROUP BY RC_C_ID ORDER BY RC_Order";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $row['C_ID'] ?>" <?php echo ($row['C_ID'] == $sortby) ? 'selected' : '' ?> ><?php echo ($row['RC_Name'] != '') ? $row['RC_Name'] : $row['C_Name']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <select class="sort-listing-org-width" name="subcategory" id="advert-subcat" onchange="filter_listings(<?php echo $regionID ?>, $('#categorydd').val(), this.value, <?php echo $bl_id ?>, 'rln')">
                        <option value="">Sub-Category</option>
                        <?PHP
                        if ($sortby > 0) {
                            $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '" . encode_strings($sortby, $db) . "' ORDER BY C_Name";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($row = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo $row['C_ID'] ?>" <?php echo ($row['C_ID'] == $subcategory) ? 'selected' : '' ?>><?php echo $row['C_Name'] ?></option>
                                <?PHP
                            }
                        }
                        ?>
                    </select>

                    <select class="sort-listing-org-width" name="listings" id="listings">
                        <option value="">Select Listing</option>

                    </select> 
                    <input type="submit" value="+Add" class="stories-add-button" />
                </div>
            </div>
        </form>
        <div id="reorder-category">
            <?PHP
            $sql = "SELECT BL_ID, BL_Listing_Title FROM tbl_Business_Listing 
                    LEFT JOIN tbl_Business_Listing_Category_Region on BL_ID = BLCR_BL_ID
                    LEFT JOIN tbl_Business_Listing_Category on BL_ID = BLC_BL_ID
                    LEFT JOIN tbl_Region_Category ON RC_C_ID = BLC_M_C_ID AND RC_Status = 0
                    LEFT JOIN tbl_Region_Listings_Nearby ON BL_ID = RLN_DIS_BL_ID AND RLN_BL_ID = $bl_id
                    WHERE hide_show_listing = 1 AND RLN_DIS_BL_ID IS NULL";

            $sql .= " AND BLCR_BLC_R_ID = $regionID AND BL_ID != $bl_id";

            $sql .= " GROUP BY BL_ID ORDER BY BL_Listing_Title";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $pages = new Paginate(mysql_num_rows($result), 30);
            $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                ?>
                <div class="data-content" >
                    <div class="data-column spl-org-listngs" id="whats_near_<?php echo $row['BL_ID'] ?>" ><div class="select-listings-nearby-title">
                            <?php echo $row['BL_Listing_Title'] ?></div>
                        <div class="remove-link"><a href="region-listings-nearby.php?bl_id=<?php echo $row['BL_ID']; ?>&rid=<?php echo $activeRegion['R_ID']; ?>&d_bl_id=<?php echo $bl_id; ?>&page=<?php echo $_REQUEST['page']; ?>&del=true" onclick="return confirm('Are you sure you want to perform this action?');">Remove</a></div>
                    </div>
                </div>
                <?PHP
            }
            ?>
        </div>
        <?php
        if (isset($pages)) {
            echo $pages->paginate();
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script>

    function getSubOptions(rid, catid)
    {
        $.ajax({
            type: "POST",
            url: "filterListings.php",
            dataType: 'json',
            data: {
                region: rid,
                category: catid
            },
            success: function (response) {
                $("#listings").empty();
                $("#listings").html("<option>Select Listing</option>");
                $("#advert-subcat").empty();
                $("#advert-subcat").html(response.subcat);
            }
        });
    }

    function filter_listings(rid, catid, subcatid, bl_id, rln) {
        $.ajax({
            type: "POST",
            url: 'filterListings.php',
            dataType: 'json',
            data: {
                region: rid,
                category: catid,
                subcategory: subcatid,
                bl_id: bl_id,
                rln: 1,
                c_region: 26
            },
            success: function (response) {
                $('#listings').html(response.listings);
            }
        });
    }
</script>