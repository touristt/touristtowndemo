<?php
include '../include/config.inc.php';
require_once '../include/login.inc.php';
require '../include/PHPMailer/class.phpmailer.php';

if (isset($_REQUEST['email_btn'])) {
    $selected_contacts = explode(',', $_REQUEST['contacts_list']);
    $sql = "INSERT tbl_Client_Contacts_Email SET CCE_Title = '" . encode_strings($_REQUEST['title'], $db) . "', 
            CCE_Body = '" . encode_strings($_REQUEST['body_txt'], $db) . "', 
            CCE_CL_ID = '" . encode_strings($_REQUEST['contacts_list'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    foreach ($selected_contacts as $contact) {
        $sql = "SELECT * FROM tbl_Client_Contacts WHERE CL_ID = $contact";
        $result = mysql_query($sql, $db);
        $row = mysql_fetch_assoc($result);
        $emails[] = $row['CL_Email'];
    }
    foreach ($emails as $email) {
        $message = '';
        ob_start();
        include '../include/email-template/client-contacts-email.php';
        $message = ob_get_contents();
        $mail = new PHPMailer();
        $mail->IsMail();
        $mail->From = MAIN_CONTACT_EMAIL;
        $mail->FromName = MAIN_CONTACT_NAME;
        $mail->IsHTML(true);
        $mail->AddAddress($email);
        $mail->Subject = $_POST['title'];
        $mail->MsgHTML($message);
        $mail->Send();
        $email_sent++;
    }
    $_SESSION['email_sent'] = 1;
    header("location:client-contact-send-email.php");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width ">
    <div class="title-link">
        <div class="title">
            Manage Client Contacts
        </div>
    </div>
    <div class="left"><?PHP require '../include/nav-manage-client-contacts.php'; ?></div>
    <div class="right">
        <div class="content-header content-header-search">
            <span>Email</span>
        </div>

        <div class="form-inside-div form-inside-div-width-admin checkall_cat_cat">
            <div style="margin: 0;border: none;" class="form-inside-div_email"> 
                <input type="checkbox" id="checkall_cat" />Select All
            </div>
            <div class="emailColumn">
                <?php
                $sql = "SELECT * FROM tbl_Client_Contacts";
                $result = mysql_query($sql, $db);
                ?>
                <div class="emailCategoryCheckbox margin" style="width: 100%"> 
                    <div class="subcatCheckbox" >
                        <?php
                        while ($row = mysql_fetch_assoc($result)) {
                            ?>
                            <div  class="subcatCheckboxInner" style="width: 33%;float:left;">
                                <input type="checkbox" name="contacts[]" class="selected_client" value="<?php echo $row['CL_ID'] ?>"><?php echo $row['CL_Name']; ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <form id="frmSort" name="form1"  method="post" action="client-contact-send-email.php">
            <input type="hidden" id="all_clients" name="contacts_list" value="">
            <div class="form-inside-div form-inside-div-width-admin-accommodation form-inside-div-width-admin"> 
                <label>Title</label>
                <div class="form-data">
                    <input type="text" id="title" name="title" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin-accommodation "> 
                <label>Message</label>
                <div class="form-data special_case" id="email_template">
                    <textarea id="special_case" class="ckeditor" name="body_txt"></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin-accommodation ">
                <div class="button">
                    <input type="submit" onclick="return get_listings();"  name="email_btn" value="Submit"/>
                    <input style="margin-left: 0px" id="preview" type="button" value="Preview"/> 
                </div>
            </div>
        </form>
        <div id="dialog" style="display:none;">

        </div>
    </div>
</div>
<script>
    function get_listings() {
        var subcategories = '';
        i = 0;
        $('.selected_client:checked').each(function () {
            if (i == 0) {
                subcategories += $(this).val();
            } else {
                subcategories += ',' + $(this).val();
            }
            i++;
        });
        $('#all_clients').val(subcategories);
    }

    $(window).load(function () {
        $('input[type=checkbox]').click(function () {
            // children checkboxes depend on current checkbox
            $(this).next().next().find('input[type=checkbox]').prop('checked', this.checked);
            // go up the hierarchy - and check/uncheck depending on number of children checked/unchecked
            $(this).parents('').prev().prev('input[type=checkbox]').prop('checked', function () {
                return $(this).next().next().find(':checked').length;
            });
        });
    });

    $('#checkall_cat').click(function () {
        var checked = $(this).prop('checked');
        $('.checkall_cat_cat').find('input:checkbox').prop('checked', checked);
    });

    $("#preview").click(function () {
        var template_id = $('#special_case').val();
        $('#dialog').html(template_id);
        $("#dialog").dialog("open");
        $("#dialog > p").css("line-height", "30px");
        $("#dialog >  p").css('margin-top', 20);
        $("#dialog > p").css('margin-bottom', 20);
        $("#dialog > h1, h2, h3").css("line-height", "35px");
        $("#dialog >  h1, h2, h3").css('margin-top', 25);
        $("#dialog > h1, h2, h3").css('margin-bottom', 25);
        $("#dialog > li > h1, h2, h3").css("line-height", "35px");
        $("#dialog > li > h1, h2, h3").css('margin-top', 25);
        $("#dialog > li >  h1, h2, h3").css('margin-bottom', 25);
        $("#dialog > ul > li").css("line-height", "30px");
        $("#dialog > ul > li").css('margin-top', 15);
        $("#dialog > ul > li").css('margin-bottom', 15);
        $("#dialog > ul > li").css("margin-left", 50);
        $("#dialog > ol > li").css("line-height", "30px");
        $("#dialog > li").css("margin-left", 50);
        $("#dialog > ol > li").css('margin-top', 15);
        $("#dialog > ol > li").css('margin-bottom', 15);
        $("#dialog > ol > li > h1, h2, h3").css("line-height", "30px");
        $("#dialog > ol > li > h1, h2, h3").css('margin-top', 15);
        $("#dialog > ol > li > h1, h2, h3").css('margin-bottom', 15);
        $(".ui-widget-content > a").css('color', '#0782C1');
        $("#dialog > p > a").css('color', '#0782C1');
        //var theColorIs = $('span').css("color"); alert(theColorIs);
    });
    $("#dialog").dialog({

        autoOpen: false,
        modal: true,
        maxWidth: 1000,
        maxHeight: 600,
        width: 1000,
        height: 600,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });

</script>
<?PHP
require_once '../include/admin/footer.php';
?>