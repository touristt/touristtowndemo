<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

$photographer = isset($_REQUEST['photographer']);
$photographers = '';
if (isset($_REQUEST['id']) > 0) {
    $sql = "SELECT PO_ID, PO_Name, PO_Description FROM tbl_Photographer_Owner WHERE PO_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
    $sql_multi = "SELECT OPM_IBP_ID FROM tbl_Owner_Photographes_Multiply WHERE OPM_PO_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result_multi = mysql_query($sql_multi, $db) or die("Invalid query: $sql_multi  -- " . mysql_error());
    while ($row_multi = mysql_fetch_assoc($result_multi)) {
        $photographers[] = $row_multi ['OPM_IBP_ID'];
    }
}
if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $ownerid = $_REQUEST['photo_id'];
    $sql = "tbl_Photographer_Owner SET PO_Name = '" . encode_strings($_REQUEST['name'], $db) . "',
            PO_Description = '" . encode_strings(trim(str_replace('<p>', '', str_replace('</p>', '', $_REQUEST['owner_des']))), $db) . "'";
    if ($_REQUEST['photo_id'] == '') {
        $sql = "Insert " . $sql;
        
    } else {
        $sql = "update " . $sql . " WHERE PO_ID = '" . encode_strings($_REQUEST['photo_id'], $db) . "' ";
        // TRACK DATA ENTRY
        Track_Data_Entry('Images','','Manage Owners',$ownerid,'Update','super admin');
    }
    $result = mysql_query($sql, $db);
    if ($ownerid == '') {
        $ownerid = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Images','','Manage Owners',$ownerid,'Add','super admin');
    } else {
        $sql = "DELETE FROM tbl_Owner_Photographes_Multiply WHERE OPM_PO_ID = '$ownerid'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    }
    foreach ($_REQUEST['photographer'] as $photographer) {
        $sql = "tbl_Owner_Photographes_Multiply SET OPM_PO_ID = '$ownerid', OPM_IBP_ID = '$photographer'";
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    }
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/owner.php");
    exit();
}
if (isset($_GET['op']) && $_GET['op'] == 'del') {
    $sql = "DELETE FROM tbl_Photographer_Owner  WHERE PO_ID = '" . $row['PO_ID'] . "'";
    $sql_reg = "DELETE FROM tbl_Owner_Photographes_Multiply WHERE OPM_PO_ID = '" . $row['PO_ID'] . "'";
    $result = mysql_query($sql, $db);
    $result_reg = mysql_query($sql_reg, $db);
    if ($result_reg) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $row['PO_ID'];
        Track_Data_Entry('Images','','Manage Owners',$id,'Delete','super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: /admin/owner.php");
    exit();
}

require_once '../include/admin/header.php';
?>

<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Owners</div>
        <div class="link">
        </div>         
    </div>
    <div class="right full-width">
        <form name="form1" method="post" enctype="multipart/form-data" action="">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="photo_id"  value="<?php echo $row['PO_ID'] ?>">
            <div class="content-header full-width"> 
                <div class="title">Add Owner</div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Name</label>
                <div class="form-data">
                    <input name="name" type="text" value="<?php echo $row['PO_Name'] ?>" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width"> 
                <label>Select Photographer</label>
                <div class="form-data">
                    <div id="childRegion"> 
                        <?PHP
                        $sql = "SELECT IBP_ID, IBP_Name FROM tbl_Image_Bank_Photographer";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row_photo_multi = mysql_fetch_assoc($result)) {
                            echo '<div class="childRegionCheckbox"><input type="checkbox" name="photographer[]" value="' . $row_photo_multi['IBP_ID'] . '"';
                            echo (in_array($row_photo_multi['IBP_ID'], $photographers)) ? 'checked' : '';
                            echo '>' . $row_photo_multi['IBP_Name'] . "</div>";
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Description</label>
                <div class="form-data">
                    <textarea name="owner_des" cols="85" rows="10" id="description-listing"><?php echo $row['PO_Description'] ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width border-none">
                <div class="button">
                    <input type="submit" name="button" id="button" value="Save Photo" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>