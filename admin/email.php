<?php
include '../include/config.inc.php';
require_once '../include/login.inc.php';
require '../include/PHPMailer/class.phpmailer.php';
require_once '../include/track-data-entry.php';

if (isset($_REQUEST['email_btn'])) {
    $emails = array(
//        0 => 'junaid.ilyas@app-desk.com',
        0 => 'faizanmuhammad903@gmail.com',
//        2 => 'saad.rehman@app-desk.com',
//        3 => 'afaq.baig@app-desk.com',
//        4 => 'hamid.nawaz@app-desk.com',
//        5 => 'ilyasraza.345@gmail.com',
//        6 => 'ilyasraza20@yahoo.com',
//        7 => 'ilyasraza@live.com',
//        8 => 'ilyasraza@live.com',
//        9 => 'ilyasraza@live.com',
//        10 => 'imtiazork@gmail.com',
//        11 => 'syedyasirshah52@gmail.com',
    );

    if (isset($_POST['templete_is_selected']) && $_POST['templete_is_selected'] > 0) {
        $update_template = "UPDATE tbl_Email_Listings_Templates SET 
                        ELT_Template = '" . encode_strings($_POST['body_txt'], $db) . "',
                        ELT_Subject = '" . encode_strings($_POST['title'], $db) . "',
                        ELT_Date = CURDATE()
                        WHERE ELT_ID = '" . encode_strings($_POST['templete_is_selected'], $db) . "'";
        mysql_query($update_template, $db) or die("Invalid query: $update_template -- " . mysql_error());
    } else {
        $insert_template = "INSERT tbl_Email_Listings_Templates SET 
                        ELT_Template = '" . encode_strings($_POST['body_txt'], $db) . "',
                        ELT_Subject = '" . encode_strings($_POST['title'], $db) . "',
                        ELT_Date = CURDATE()";
        mysql_query($insert_template, $db) or die("Invalid query: $insert_template -- " . mysql_error());
    }
//Email File Attachment
    $attachement = "";
    if (isset($_FILES['uploaded_file']) && $_FILES['uploaded_file']['error'] == UPLOAD_ERR_OK) {
        $file = rand(1000, 100000) . "-" . $_FILES["uploaded_file"]["name"];
        $temp_name = $_FILES["uploaded_file"]["tmp_name"];
        $filePath = 'emailattachments/' . $file;
        if ($_FILES['uploaded_file']['name'] != '') {
            move_uploaded_file($temp_name, $filePath);
            $attachement = encode_strings($file, $db);
        }
    }
    $sql = "INSERT INTO tbl_Email (E_Title, E_Body, E_Attachment, E_Date) VALUES('" . encode_strings($_POST['title'], $db) . "','" . encode_strings($_POST['body_txt'], $db) . "', '$attachement', NOW())";
    $result = mysql_query($sql) or die(mysql_error());
    $id = mysql_insert_id();
    $getFile = "SELECT * FROM  `tbl_Email` WHERE E_ID = '" . $id . "'";
    $fileRes = mysql_query($getFile);
    $sendFile = mysql_fetch_assoc($fileRes);
    $url = './emailattachments/' . $sendFile['E_Attachment'];
    $sql = "SELECT * FROM tbl_Email WHERE E_ID = $id";
    $result = mysql_query($sql, $db);
    $row = mysql_fetch_array($result);
    $email_sent = 0; //How many listings will recieve emails.
    $B_ID = array();
    if (isset($_REQUEST['all_listings']) && $_REQUEST['all_listings'] != '') {
        $all_listings = explode(',', $_REQUEST['all_listings']);
        $i = 0;
        foreach ($all_listings as $Selected_BL_ID) {
            $select = "SELECT * FROM `tbl_Business` 
                    LEFT JOIN tbl_Business_Listing ON BL_B_ID = B_ID
                    LEFT JOIN tbl_Business_Listing_Category_Region ON BL_ID = BLCR_BL_ID
                    LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                    WHERE BL_ID = '" . $Selected_BL_ID . "' GROUP BY BL_ID";
            $resultListings = mysql_query($select);
            $listing_email = "";
            while ($listing_row = mysql_fetch_array($resultListings)) {
                if (!in_array($listing_row['B_ID'], $B_ID)) {
                    $listing_email = $listing_row['B_Email'];
                    $listing_email = $emails[$i];
                    $stripEmail = explode("<input name=", $row['E_Body']);
                    $stripEmail2 = explode("/>", $stripEmail[2]);
                    ob_start();
                    include '../include/email-template/bulk-email.php';
                    $message = ob_get_contents();
                    $mail = new PHPMailer();
                    $mail->IsMail();
                    $mail->From = MAIN_CONTACT_EMAIL;
                    $mail->FromName = MAIN_CONTACT_NAME;
                    $mail->IsHTML(true);
                    if ($sendFile['E_Attachment'] != "") {
                        $mail->AddAttachment(realpath($url));
                    }
                    $mail->AddAddress($listing_email);
                    $mail->AddAddress($to);
                    $mail->Subject = $_POST['title'];
                    $mail->MsgHTML($message);
//$mail->Send();
                    $email_sent++; //How many listings will recieve emails.
                    $B_ID[] = $listing_row['B_ID'];
//                    $i++;
//                    if ($i == 12) {
//                        $i = 0;
//                    }
                }
            }
        }
        $update = "UPDATE tbl_Email SET E_Count = '" . $email_sent . "' WHERE E_ID = '" . $id . "'";
        mysql_query($update);
        $insert = "INSERT tbl_Email_Detail SET 
            ED_Email_ID = '" . $id . "', 
            ED_R_ID = '" . $_REQUEST['all_regions'] . "', 
            ED_C_ID = '" . $_REQUEST['all_categories'] . "', 
            ED_SC_ID = '" . $_REQUEST['all_subcategories'] . "', 
            ED_Listing_Type = '" . $_REQUEST['all_types'] . "',
            ED_BL_ID = '" . $_REQUEST['all_listings'] . "'";
        mysql_query($insert);
        $_SESSION['email_sent'] = 1;
// TRACK DATA ENTRY
        Track_Data_Entry('Email', '', 'Email Template', $id, 'Send Email', 'super admin');
    }
    header("location:email.php");
    exit();
}

if (isset($_POST['save_as_draft'])) {
    if (isset($_POST['templete_is_selected']) && $_POST['templete_is_selected'] > 0) {
        $update_template = "UPDATE tbl_Email_Listings_Templates SET 
                            ELT_Template = '" . encode_strings($_POST['body_txt'], $db) . "',
                            ELT_Subject = '" . encode_strings($_POST['title'], $db) . "',
                            ELT_Date = CURDATE()
                            WHERE ELT_ID = '" . encode_strings($_POST['templete_is_selected'], $db) . "'";
        mysql_query($update_template, $db) or die("Invalid query: $update_template -- " . mysql_error());
        $id = $_POST['templete_is_selected']; // Made it for tracking
    } else {
        $insert_template = "INSERT tbl_Email_Listings_Templates SET 
                            ELT_Template = '" . encode_strings($_POST['body_txt'], $db) . "',
                            ELT_Subject = '" . encode_strings($_POST['title'], $db) . "',
                            ELT_Date = CURDATE()";
        mysql_query($insert_template, $db) or die("Invalid query: $insert_template -- " . mysql_error());
        $id = mysql_insert_id(); // Made it for tracking
    }
    $_SESSION['email_saved_as_draft'] = 1;
// TRACK DATA ENTRY
    Track_Data_Entry('Email', '', 'Email Template', $id, 'Save As Draft', 'super admin');
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width ">
    <div class="title-link">
        <div class="title">
            Manage Emails - Email Template
        </div>
    </div>
    <div class="left"><?php require_once('../include/nav-home-sub.php'); ?></div>
    <div class="right">
        <div class="content-header content-header-search">
            <span>Email</span>
        </div>
        <form name="form2"  method="get" action="email.php">
            <div class="form-inside-div form-inside-div-width-admin checkall_communities"> 
                <label>Community</label>
                <div class="form-data">
                    <select name="community" id="community" onchange="this.form.submit()">
                        <option value="">Select Community</option>
                        <?php
                        $sql = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Domain != '' AND R_Parent != 0";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($rowRegion = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $rowRegion['R_ID'] ?>" <?php echo ($_REQUEST['community'] == $rowRegion['R_ID']) ? 'selected' : ''; ?>><?php echo $rowRegion['R_Name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </form>

        <?php if (isset($_REQUEST['community']) && $_REQUEST['community'] > 0) { ?>
            <div class="form-inside-div form-inside-div-width-admin checkall_cat_cat">
                <div style="margin: 0;border: none;" class="form-inside-div_email"> 
                    <input type="checkbox" id="checkall_cat" />Select All
                </div>
                <div class="emailColumn">
                    <?php
                    $sqlCat = "SELECT C_ID, C_Name, RC_Name FROM `tbl_Category` LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID 
                               WHERE RC_R_ID = '" . $_REQUEST['community'] . "' AND C_Parent = 0 AND C_ID NOT IN(121,8) ORDER BY RC_Order ASC";
                    $resultCat = mysql_query($sqlCat, $db) or die("Invalid query: $sqlCat -- " . mysql_error());
                    $categoryCount = mysql_num_rows($resultCat);
                    if ($categoryCount > 0) {
                        while ($rowMainCat = mysql_fetch_assoc($resultCat)) {
                            $sqlsub = "SELECT C_ID, C_Name FROM tbl_Category LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID 
                                   WHERE RC_R_ID = '" . $_REQUEST['community'] . "' AND C_Parent = '" . $rowMainCat['C_ID'] . "' ORDER BY C_Name";
                            $resultSubs = mysql_query($sqlsub, $db) or die("Invalid query: $sqlsub -- " . mysql_error());
                            $subCategoryCounts = mysql_num_rows($resultSubs);
                            if ($subCategoryCounts > 0) {
                                ?>
                                <div class="emailCategoryCheckbox margin">
                                    <input type="checkbox" name="category[]" class="selected_category" value="<?php echo $rowMainCat['C_ID'] ?>"><strong><?php echo ($rowMainCat['RC_Name'] != '') ? $rowMainCat['RC_Name'] : $rowMainCat['C_Name']; ?></strong>  
                                    <div  class="subcatCheckbox" >
                                        <?php
                                        while ($rowSub = mysql_fetch_assoc($resultSubs)) {
                                            ?>
                                            <div  class="subcatCheckboxInner">
                                                <input type="checkbox" name="subcategory[<?php echo $rowMainCat['C_ID'] ?>][]" class="selected_subcategory" value="<?php echo $rowSub['C_ID'] ?>"><?php echo ($rowSub['RC_Name'] != '') ? $rowSub['RC_Name'] : $rowSub['C_Name']; ?>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?php
                            } 
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin-accommodation form-inside-div-width-admin"> 
                <label>Listing Type</label>
                <div class="form-data" id="listing_pkg_checkbox">
                    <input name="type[]" class="select types" id="v1" type="checkbox" value="1"/>Free
                    <input name="type[]" class="select types" id="v5" type="checkbox" value="5"/>Town Asset
                    <input name="type[]" class="select types" id="v4" type="checkbox" value="4"/>Enhanced

                </div>
            </div>
            <!--no image-->
            <div class="form-inside-div form-inside-div-width-admin-accommodation form-inside-div-width-admin"> 
                <label>No images</label>
                <div class="form-data" id="listing_pkg_checkbox">
                    <input name="no_image" class="select no_image" id="noimage" type="checkbox" value="8"/>No images
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin-accommodation form-inside-div-width-admin"> 
                <label>Coupon</label>
                <div class="form-data" id="listing_pkg_checkbox">
                    <input name="coupon" class="select coupon" id="coupon" type="checkbox" value="9"/>Coupon
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin-accommodation form-inside-div-width-admin"> 
                <label>Unverified Listings</label>
                <div class="form-data" id="listing_pkg_checkbox">
                    <input name="u_listing" class="select u_listing" id="u_listing" type="checkbox" value="7"/>Unverified Listings
                </div>
            </div>
            <div id="get_listings" style="display: none"></div>
            <form id="frmSort" name="form1"  method="post" action="email.php" enctype="multipart/form-data">
                <input type="hidden" id="all_listings" name="all_listings" value="">
                <input type="hidden" id="all_regions" name="all_regions" value="">
                <input type="hidden" id="all_categories" name="all_categories" value="">
                <input type="hidden" id="all_subcategories" name="all_subcategories" value="">
                <input type="hidden" id="all_types" name="all_types" value="">
                <input type="hidden" id="noimage" name="no_image" value="">
                <input type="hidden" id="coupon" name="coupon" value="">
                <div class="form-inside-div form-inside-div-width-admin-accommodation form-inside-div-width-admin"> 
                    <label>Title</label>
                    <div class="form-data">
                        <input type="text" id="title" name="title" required/>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin-accommodation form-inside-div-width-admin"> 
                    <label>Email Templates</label>
                    <div class="form-data">
                        <input type="hidden" name="templete_is_selected" id="templete_is_selected" value="0">
                        <select name="template" id="template">
                            <option value="">Select Email Template</option>
                            <?php
                            $getTemplate = "SELECT * FROM tbl_Email_Listings_Templates";
                            $resTemplate = mysql_query($getTemplate);
                            while ($templates = mysql_fetch_array($resTemplate)) {
                                ?>
                                <option value="<?php echo $templates['ELT_ID'] ?>"><?php echo $templates['ELT_Subject'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Attachment</label>
                    <div class="form-data form-data-logo margin-attached-top">     
                        <div class="inputWrapper adv-photo">
                            Browse 
                            <input class="fileInput" type="file" name="uploaded_file" onchange="show_file_name(this.files[0].name)">
                            <!--</span>--> 
                        </div>
                        <input id="uploadFile"  class="name-attached-file" disabled>
                    </div>
                </div>
                <!--email body-->
                <div class="form-inside-div form-inside-div-width-admin-accommodation "> 
                    <label>Message</label>
                    <div class="form-data special_case" id="email_template">
                        <textarea id="special_case" class="ckeditor" name="body_txt"></textarea>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin-accommodation ">
                    <div class="button">
                        <input type="submit" onclick="return get_listings();"  name="email_btn" value="Submit"/>
                        <input type="submit" name="save_as_draft" value="Save As Draft"/>
                        <input style="margin-left: 0px" id="preview" type="button" value="Preview"/> 

                    </div>
                </div>
            </form>
            <div id="dialog" style="display:none;">

            </div>
        <?php } ?>
    </div>
</div>
<script>
    function get_listings() {
        var listings = '';
        var regions = $('#community').val();
        var regions = '';
        var categories = '';
        var subcategories = '';
        var types = '';
        var no_image = '';
        var coupon = '';
        var u_listing = '';
        var i = 0;
        $('.selected_listings:checked').each(function () {
            if (i == 0) {
                listings += $(this).val();
            } else {
                listings += ',' + $(this).val();
            }
            i++;
        });

        i = 0;
        //        $('.selected_regions:checked').each(function () {
        //            if (i == 0) {
        //                regions += $(this).val();
        //            } else {
        //                regions += ',' + $(this).val();
        //            }
        //            i++;
        //        });
        i = 0;
        $('.selected_category:checked').each(function () {
            if (i == 0) {
                categories += $(this).val();
            } else {
                categories += ',' + $(this).val();
            }
            i++;
        });
        i = 0;
        $('.selected_subcategory:checked').each(function () {
            if (i == 0) {
                subcategories += $(this).val();
            } else {
                subcategories += ',' + $(this).val();
            }
            i++;
        });
        i = 0;
        $('.types:checked').each(function () {
            if (i == 0) {
                types += $(this).val();
            } else {
                types += ',' + $(this).val();
            }
            i++;
        });
        $('.no_image:checked').click(function () {
            no_image = $(this).val();
        });

        $('.coupon:checked').click(function () {
            coupon = $(this).val();
        });
        $('.u_listing:checked').click(function () {
            u_listing = $(this).val();
        });
        $('#all_listings').val(listings);
        $('#all_regions').val(regions);
        $('#all_categories').val(categories);
        $('#all_subcategories').val(subcategories);
        $('#all_types').val(types);
        $('#noimage').val(no_image);
        $('#coupon').val(coupon);
        $('#u_listing').val(u_listing);
    }

    $(window).load(function () {
        $('input[type=checkbox]').click(function () {
            // children checkboxes depend on current checkbox
            $(this).next().next().find('input[type=checkbox]').prop('checked', this.checked);
            // go up the hierarchy - and check/uncheck depending on number of children checked/unchecked
            $(this).parents('').prev().prev('input[type=checkbox]').prop('checked', function () {
                return $(this).next().next().find(':checked').length;
            });
        });
    });

    $('#checkall').click(function () {
        var checked = $(this).prop('checked');
        $('.checkall_communities').find('input:checkbox').prop('checked', checked);
    });

    $('#checkall_cat').click(function () {
        var checked = $(this).prop('checked');
        $('.checkall_cat_cat').find('input:checkbox').prop('checked', checked);
    });


    $("#preview").click(function () {
        var template_id = $('#special_case').val();
        $('#dialog').html(template_id);
        $("#dialog").dialog("open");
        $("#dialog > p").css("line-height", "30px");
        $("#dialog >  p").css('margin-top', 20);
        $("#dialog > p").css('margin-bottom', 20);
        $("#dialog > h1, h2, h3").css("line-height", "35px");
        $("#dialog >  h1, h2, h3").css('margin-top', 25);
        $("#dialog > h1, h2, h3").css('margin-bottom', 25);
        $("#dialog > li > h1, h2, h3").css("line-height", "35px");
        $("#dialog > li > h1, h2, h3").css('margin-top', 25);
        $("#dialog > li >  h1, h2, h3").css('margin-bottom', 25);
        $("#dialog > ul > li").css("line-height", "30px");
        $("#dialog > ul > li").css('margin-top', 15);
        $("#dialog > ul > li").css('margin-bottom', 15);
        $("#dialog > ul > li").css("margin-left", 50);
        $("#dialog > ol > li").css("line-height", "30px");
        $("#dialog > li").css("margin-left", 50);
        $("#dialog > ol > li").css('margin-top', 15);
        $("#dialog > ol > li").css('margin-bottom', 15);
        $("#dialog > ol > li > h1, h2, h3").css("line-height", "30px");
        $("#dialog > ol > li > h1, h2, h3").css('margin-top', 15);
        $("#dialog > ol > li > h1, h2, h3").css('margin-bottom', 15);
        $(".ui-widget-content > a").css('color', '#0782C1');
        $("#dialog > p > a").css('color', '#0782C1');
        //var theColorIs = $('span').css("color"); alert(theColorIs);
    });
    $("#dialog").dialog({

        autoOpen: false,
        modal: true,
        maxWidth: 1000,
        maxHeight: 600,
        width: 1000,
        height: 600,
        show: {
            effect: "blind",
            duration: 1000
        },
        hide: {
            effect: "explode",
            duration: 1000
        }
    });



    $('.select').change(function () {
        var region = $('#community').val();
        var types = '';
        var no_imageVal = '';
        var couponVal = '';
        var u_listingVal = '';
        var i = 0;
        //type
        $('.types:checked').each(function () {
            if (i == 0) {
                types += $(this).val();
            } else {
                types += ',' + $(this).val();
            }
            i++;
        });
        //image checkbox
        $('.no_image:checked').each(function () {
            no_imageVal = $(this).val();

        });
        //coupon checkbox
        $('.coupon:checked').each(function () {
            couponVal = $(this).val();
        });
        //coupon checkbox
        $('.u_listing:checked').each(function () {
            u_listingVal = $(this).val();
        });

        var searchIDs = $("input:checkbox:checked").map(function () {
            return $(this).attr("name") + '-' + $(this).val();
        }).get(); // <----
        if (searchIDs != "") {
            $.post("get-listings-for-email.php", {
                searchIDs: searchIDs,
                listing_type: types,
                no_image: no_imageVal,
                coupons: couponVal,
                uLsitng: u_listingVal,
                region: region
            }, function (listings) {
                $('#get_listings').html('');
                $('#get_listings').show();
                $('#get_listings').append(listings);
            });
        }
    });

    $(document).on('change', '#template', function () {
        var template_id = $('#template').val();
        $.post("get-email-templates.php", {
            template_id: template_id
        }, function (templates) {
            $('#templete_is_selected').val(template_id);
            CKEDITOR.instances.special_case.setData(templates);
            // $('#dialog').html(templates);
        });

    });

    $(document).on('click', '#select_all_listings', function () {
        $(this).siblings('#listings_siblings')
                .find("input[type='checkbox']")
                .prop('checked', this.checked);
    });
    function show_file_name(val) {
        document.getElementById("uploadFile").value = val;
    }
</script>
<?PHP
require_once '../include/admin/footer.php';
?>