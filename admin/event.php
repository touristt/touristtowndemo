<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';
if (!in_array('manage-events', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
if (!in_array('superadmin', $_SESSION['USER_ROLES']) && !in_array('admin', $_SESSION['USER_ROLES'])) {
    if ($_SESSION['USER_LIMIT'] != '') {
        $limits = explode(',', $_SESSION['USER_LIMIT']);
        $regionLimit = array();
        foreach ($limits as $limit) {
            $sql = "SELECT R_ID, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $region = mysql_fetch_assoc($result);
            if ($region['R_Type'] == 1) {
                $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($region['R_ID'], $db) . "'";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                while ($row = mysql_fetch_assoc($result)) {
                    if ($row['R_ID'] != '' && !in_array($row['R_ID'], $regionLimit)) {
                        $regionLimit[] = $row['R_ID'];
                    }
                }
            } else {
                if ($region['R_ID'] != '' && !in_array($region['R_ID'], $regionLimit)) {
                    $regionLimit[] = $region['R_ID'];
                }
            }
        }
    }
}
if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $sql = "SELECT Recuring, EventID, E_Region_ID, Title, E_Name_SEO, Event_Image, E_Region_ID, EventType, EventDateStart, EventDateEnd, EventTimeAllDay, 
            EventStartTime, EventID, EventTimeAllDay, EventEndTime, Event_Facebook_link, ShortDesc, Content, LocationID, LocationList, StreetAddress, 
            OrganizationID, Organization, ContactName, ContactPhone, ContactPhoneTollFree, Email, WebSiteLink, Pending,EventPdf FROM Events_master 
            WHERE EventID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
    $recuring = explode(",", $row['Recuring']);
}
// CODE TO DELETE PDF FILE
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $select_pdf = "SELECT EventPdf FROM Events_master WHERE EventID = " . $_REQUEST['delete'] . " ";
    $result_pdf = mysql_query($select_pdf);
    $pdf_row = mysql_fetch_assoc($result_pdf);
    if ($pdf_row['EventPdf'] != "") {
        unlink(PDF_LOC_ABS . $pdf_row['EventPdf']);
    }
    $sql = "UPDATE Events_master SET EventPdf = '' WHERE EventID = '" . $_REQUEST['delete'] . "'";
    $result = mysql_query($sql);
    // TRACK DATA ENTRY
    $id = $_REQUEST['delete'];
    Track_Data_Entry('Event',$id,'Manage Events','','Delete PDF','super admin');
    if ($result) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location:/admin/event.php?id=" . $_REQUEST['delete']);
    exit;
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'deletephoto') {
    $EventID = $_REQUEST['rid'];
    $select_date = "SELECT Event_Image FROM Events_master WHERE EventID = " . $EventID . " ";
    $result_data = mysql_query($select_date);
    $row = mysql_fetch_assoc($result_data); 
    require '../include/picUpload.inc.php';
    $sql = "update Events_master SET Event_Image = '' WHERE EventID = '$EventID'";
    Delete_Pic(IMG_LOC_ABS . $row['Event_Image']);
    $result_del = mysql_query($sql, $db);
    // TRACK DATA ENTRY
    $id = $_REQUEST['rid'];
    Track_Data_Entry('Event',$id,'Manage Events','','Delete Photo','super admin');
    if ($result_del) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: event.php?id=" . $EventID);
    exit();
}
if (isset($_POST['op']) && $_POST['op'] == 'save') {

    if ($_REQUEST['pending'] == 2) {
        if ($_REQUEST['id'] > 0) {
            $sql = "UPDATE Events_master SET Pending = 2 WHERE EventID = '" . encode_strings($_REQUEST['id'], $db) . "'";
            $result = mysql_query($sql, $db);
        }
        // TRACK DATA ENTRY
        $id = $_REQUEST['id'];
        Track_Data_Entry('Event',$id,'Manage Events','','Delete','super admin'); 
        if ($result) {
            $_SESSION['delete'] = 1;
        } else {
            $_SESSION['delete_error'] = 1;
        }
        header("Location: events.php");
        exit();
    } else {
        $file_size = $_FILES['file']['size'];
        $max_filesize = 5342523;
        $pdf_name = str_replace(' ', '_', $_FILES['file']['name']);

        if ($pdf_name != "") {
            $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_name;
            $filePath = PDF_LOC_ABS . $random_name;
            if ($_FILES["file"]["type"] == "application/pdf") {
                if ($file_size < $max_filesize) {
                    move_uploaded_file($_FILES["file"]["tmp_name"], $filePath);
                }
            }
        }
        $weekdays = array();
        $weekdays[] .= ($_REQUEST['weekdays-mon'] > 0) ? $_REQUEST['weekdays-mon'] : "0";
        $weekdays[] .= ($_REQUEST['weekdays-tue'] > 0) ? $_REQUEST['weekdays-tue'] : "0";
        $weekdays[] .= ($_REQUEST['weekdays-wed'] > 0) ? $_REQUEST['weekdays-wed'] : "0";
        $weekdays[] .= ($_REQUEST['weekdays-thu'] > 0) ? $_REQUEST['weekdays-thu'] : "0";
        $weekdays[] .= ($_REQUEST['weekdays-fri'] > 0) ? $_REQUEST['weekdays-fri'] : "0";
        $weekdays[] .= ($_REQUEST['weekdays-sat'] > 0) ? $_REQUEST['weekdays-sat'] : "0";
        $weekdays[] .= ($_REQUEST['weekdays-sun'] > 0) ? $_REQUEST['weekdays-sun'] : "0";
        $weeks = implode(',', $weekdays);
        $community = implode(',', $_REQUEST['community']);
        if ($_REQUEST['all_day'] == '1') {
            $timing = "EventTimeAllDay = '" . encode_strings("1", $db) . "', ";
        } else {
            $timing = " EventTimeAllDay = '" . encode_strings("0", $db) . "', 
                        EventStartTime = '" . encode_strings($_REQUEST['starttime'], $db) . "', 
                        EventEndTime = '" . encode_strings($_REQUEST['endtime'], $db) . "', ";
        }
        $sql = "Events_master SET 
                E_Region_ID = '" . encode_strings($community, $db) . "',
                Pending = '" . encode_strings($_REQUEST['pending'], $db) . "', 
                Title = '" . encode_strings($_REQUEST['title'], $db) . "', 
                E_Name_SEO = '" . encode_strings($_REQUEST['seoNameEvent'], $db) . "',  
                $timing
                ShortDesc = '" . encode_strings(substr($_REQUEST['shortdesc'], 0, 95), $db) . "', 
                Content = '" . encode_strings(substr($_REQUEST['content'], 0, 700), $db) . "', 
                EventDateStart = '" . encode_strings(($_REQUEST['startdate'] != "") ? date('Y-m-d', strtotime($_REQUEST['startdate'])) : "0000-00-00", $db) . "',
                EventDateEnd = '" . encode_strings(($_REQUEST['enddate'] != "") ? date('Y-m-d', strtotime($_REQUEST['enddate'])) : "0000-00-00", $db) . "',
                LocationID = '" . encode_strings($_REQUEST['setLocationID'], $db) . "', 
                LocationList = '" . encode_strings($_REQUEST['location'], $db) . "', 
                StreetAddress = '" . encode_strings($_REQUEST['address'], $db) . "', 
                OrganizationID = '" . encode_strings($_REQUEST['setOrganizationID'], $db) . "', 
                Organization = '" . encode_strings($_REQUEST['organization'], $db) . "', 
                Recuring = '" . encode_strings($weeks, $db) . "', 
                ContactName = '" . encode_strings($_REQUEST['contact'], $db) . "', 
                ContactPhone = '" . encode_strings($_REQUEST['phone'], $db) . "', 
                ContactPhoneTollFree = '" . encode_strings($_REQUEST['tollfree'], $db) . "', 
                Email = '" . encode_strings($_REQUEST['email'], $db) . "', 
                Event_Facebook_link = '" . encode_strings($_REQUEST['facebook_link'], $db) . "', 
                WebSiteLink = '" . encode_strings($_REQUEST['website'], $db) . "'";
        if ($random_name !== '') {
            $sql .= ", EventPdf = '" . encode_strings($random_name, $db) . "'";
        }

        if ($_FILES['picture']['name'] != '') {
            require '../include/picUpload.inc.php';
            $pic = Upload_Pic_Normal('0', 'picture', 0, 0, true, IMG_LOC_ABS, 0, true);
            if ($pic) {
                $sql .= ", Event_Image = '" . encode_strings($pic, $db) . "'";
                if ($row['Event_Image']) {
                    Delete_Pic(IMG_LOC_ABS . $row['Event_Image']);
                }
            }
        }

        if ($_REQUEST['id'] > 0) {
            $sql = "UPDATE " . $sql . " WHERE EventID = '" . encode_strings($_REQUEST['id'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            // TRACK DATA ENTRY
            $id = $_REQUEST['id'];
            Track_Data_Entry('Event',$id,'Manage Events','','Update','super admin');
        } else {
            $sql = "INSERT " . $sql;
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $id = mysql_insert_id();
            // TRACK DATA ENTRY
            Track_Data_Entry('Event',$id,'Manage Events','','Add','super admin'); 
        }

        if ($result) {
            $_SESSION['success'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }

        if ($_REQUEST['id'] > 0) {
            $id = $_REQUEST['id'];
        } else {
            $id = mysql_insert_id($db);
        }


        header("Location: events.php");
        exit();
    }
}
if ($_GET['op'] == 'del') {
    $event_id = $_REQUEST['event_id'];
    $update = "UPDATE Events_master SET Event_Image = '' WHERE EventID = '" . encode_strings($_REQUEST['event_id'], $db) . "'";
    $result = mysql_query($update);
    if ($result) {
        imageBankUsageDelete('IBU_Event_ID', $event_id, '', '');
    } else {
        
    }
    header('location: event.php?id=' . $event_id);
    exit;
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Events<?php echo ($row['Title'] != '') ? " - " . $row['Title'] : ""; ?></div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manageevents.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">Event Information</div>
        <form onSubmit="return validateEventForm();" enctype="multipart/form-data" name="form1" method="post" action="/admin/event.php">
            <script>
                function makeSEO(myVar) {
                    myVar = myVar.toLowerCase();
                    myVar = myVar.replace(/[^a-z0-9\s-]/gi, '');
                    myVar = myVar.replace(/"/g, '');
                    myVar = myVar.replace(/'/g, '');
                    myVar = myVar.replace(/&/g, '');
                    myVar = myVar.replace(/\//g, '');
                    myVar = myVar.replace(/,/g, '');
                    myVar = myVar.replace(/  /g, ' ');
                    myVar = myVar.replace(/ /g, '-');

                    $('#seoNameEvent').val(myVar);
                    return false;
                }
            </script>
            <input type="hidden" name="id" value="<?php echo $row['EventID'] ?>">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="regionID" value="<?php echo $row['E_Region_ID'] ?>">
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Event Title</label>
                <div class="form-data">
                    <input name="title" type="text" id="title" value="<?php echo $row['Title'] ?>" size="50" onKeyUp="makeSEO(this.value)" required  />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Event Name SEO</label>
                <div class="form-data">
                    <input name="seoNameEvent" type="text" id="seoNameEvent" value="<?php echo $row['E_Name_SEO'] ?>" onKeyUp="makeSEO(this.value)" size="50" required  />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Community</label>
                <div class="form-data">
                    <div id="childRegion"> 
                        <?PHP
                        $regionLimitCommaSeparated = rtrim(implode(',', $regionLimit), ',');
                        if (!in_array('superadmin', $_SESSION['USER_ROLES']) && !in_array('admin', $_SESSION['USER_ROLES'])) {
                            $sqlCom = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Parent != 0 and R_ID IN (" . encode_strings($regionLimitCommaSeparated, $db) . ") ORDER BY R_Name";
                        } else {
                            $sqlCom = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Parent != 0 ORDER BY R_Name";
                        }
                        $resultRegionList = mysql_query($sqlCom, $db) or die("Invalid query: $sqlCom -- " . mysql_error());
                        $region_list = explode(',', $row['E_Region_ID']);
                        while ($rowRList = mysql_fetch_assoc($resultRegionList)) {
                            echo '<div class="childRegionCheckbox"><input type="checkbox"  class="community_select" name="community[]" value="' . $rowRList['R_ID'] . '"';
                            echo (in_array($rowRList['R_ID'], $region_list)) ? 'checked' : '';
                            echo '>' . $rowRList['R_Name'] . "</div>";
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Event Photo</label>
                <div class="form-data">
                    <div class="approved-photo-div">
                        <div class="inputWrapper adv-photo float-left">Browse
                            <input class="fileInput" id="approved-photo" type="file" name="picture[]" onchange="show_file(this)" >
                        </div>
                        <?php if ($row['Event_Image']) {
                            ?>
                                <!--<a class="margin-left-resize add_event_view" href="http://<?php echo DOMAIN . IMG_LOC_REL . $row['Event_Image'] ?>" target="_blank">View</a>-->
                            <a class="margin-left-resize add_event_view" onclick="return confirm('Are you sure you want to delete photo?');" href="event.php?rid=<?php echo $row['EventID'] ?>&op=deletephoto">Delete</a>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="approved-photo-div" id="uploadFile1Main" <?php echo ($row['Event_Image'] == '') ? 'style="display:none;"' : ''; ?>>
                        <img id="uploadFile1" <?php echo ($row['Event_Image'] != '') ? 'src="http://' . DOMAIN . IMG_LOC_REL . $row['Event_Image'] . '"' : 'src=""'; ?>>          
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Upload PDF</label>
                <div class="form-data image-bank-browse image_bank_width_browse">
                    <div class="inputWrapper adv-photo float-left">Browse
                        <input id="imageInput filename"  class="setting-name fileInput file_ext0 required_title0" accept="application/pdf"  type="file" name="file"  onchange="show_file_name(this.files[0].name, 1)" multiple>
                    </div>
                    <input style="display: none;" id="uploadFile" class="uploadFileName" disabled>
                    <div class="margin-left-resize add_event_view">
                        <?php
                        if (isset($row['EventPdf']) && $row['EventPdf'] != '') {
                            ?>
                            <a href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $row['EventPdf'] ?>">
                                View
                            </a> 
                        </div>
                        <div class="margin-left-resize add_event_view">
                            <a class="delete-pointer" onClick="return confirm('Are you sure this action can not be undone!');" href="event.php?delete=<?php echo $row['EventID'] ?>&op=del">Delete</a> 
                        <?php }
                        ?>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Start Date</label>
                <div class="form-data">
                    <input class="datepicker" name="startdate" type="text" id="startdate" value="<?php echo $row['EventDateStart'] ?>" size="50"  required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>End Date</label>
                <div class="form-data">
                    <input class="datepicker" name="enddate" type="text" id="enddate" value="<?php echo $row['EventDateEnd'] ?>" size="50" required />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Recurring</label>
                <div class="weekDays-selector">
                    <input type="checkbox" id="weekday-mon" name="weekdays-mon" value="<?php echo ($recuring[0] == 1) ? 1 : 0 ?>" <?php echo ($recuring[0] == 1) ? 'checked' : '' ?> class="weekday" />
                    <label class="weekday-mon" for="weekday-mon">M</label>
                    <input type="checkbox" id="weekday-tue" name="weekdays-tue" value="<?php echo ($recuring[1] == 1) ? 1 : 0 ?>" <?php echo ($recuring[1] == 1) ? 'checked' : '' ?> class="weekday" />
                    <label class="weekday-tue" for="weekday-tue">T</label>
                    <input type="checkbox" id="weekday-wed" name="weekdays-wed" value="<?php echo ($recuring[2] == 1) ? 1 : 0 ?>" <?php echo ($recuring[2] == 1) ? 'checked' : '' ?> class="weekday" />
                    <label class="weekday-wed" for="weekday-wed">W</label>
                    <input type="checkbox" id="weekday-thu" name="weekdays-thu" value="<?php echo ($recuring[3] == 1) ? 1 : 0 ?>" <?php echo ($recuring[3] == 1) ? 'checked' : '' ?> class="weekday" />
                    <label class="weekday-thu" for="weekday-thu">T</label>
                    <input type="checkbox" id="weekday-fri" name="weekdays-fri" value="<?php echo ($recuring[4] == 1) ? 1 : 0 ?>" <?php echo ($recuring[4] == 1) ? 'checked' : '' ?> class="weekday" />
                    <label class="weekday-fri" for="weekday-fri">F</label>
                    <input type="checkbox" id="weekday-sat" name="weekdays-sat" value="<?php echo ($recuring[5] == 1) ? 1 : 0 ?>" <?php echo ($recuring[5] == 1) ? 'checked' : '' ?> class="weekday" />
                    <label class="weekday-sat" for="weekday-sat">S</label>
                    <input type="checkbox" id="weekday-sun" name="weekdays-sun" value="<?php echo ($recuring[6] == 1) ? 1 : 0 ?>" <?php echo ($recuring[6] == 1) ? 'checked' : '' ?> class="weekday" />
                    <label class="weekday-sun" for="weekday-sun">S</label>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Timing</label>
                <div class="form-data">
                    <select name="starttime" id="starttime" <?php echo ($row['EventTimeAllDay'] == 1) ? 'style="display:none"' : 'required' ?>>
                        <option value="">Start Time</option>
                        <option value="00:00:00" <?php echo ($row['EventStartTime'] == '00:00:00' && $row['EventID']) ? 'selected' : '' ?>>N/A</option>
                        <option value="00:00:01" <?php echo $row['EventStartTime'] == '00:00:01' ? 'selected' : '' ?>>Dusk</option>
                        <?PHP
                        $startTime = substr($row['EventStartTime'], 0, 1) == 0 ? substr($row['EventStartTime'], 1) : $row['EventStartTime'];
                        for ($i = 1; $i < 24; $i++) {
                            ?>
                            <option value="<?php echo $i ?>:00:00" <?php echo $startTime == $i . ':00:00' ? 'selected' : '' ?>><?php echo $i > 12 ? $i - 12 : $i ?>:00 <?php echo $i > 11 ? 'pm' : 'am' ?></option>
                            <option value="<?php echo $i ?>:15:00" <?php echo $startTime == $i . ':15:00' ? 'selected' : '' ?>><?php echo $i > 12 ? $i - 12 : $i ?>:15 <?php echo $i > 11 ? 'pm' : 'am' ?></option>
                            <option value="<?php echo $i ?>:30:00" <?php echo $startTime == $i . ':30:00' ? 'selected' : '' ?>><?php echo $i > 12 ? $i - 12 : $i ?>:30 <?php echo $i > 11 ? 'pm' : 'am' ?></option>
                            <option value="<?php echo $i ?>:45:00" <?php echo $startTime == $i . ':45:00' ? 'selected' : '' ?>><?php echo $i > 12 ? $i - 12 : $i ?>:45 <?php echo $i > 11 ? 'pm' : 'am' ?></option>
                            <?PHP
                        }
                        ?>
                    </select>
                    <select name="endtime" id="endtime" <?php echo ($row['EventTimeAllDay'] == 1) ? 'style="display:none"' : 'required' ?>>
                        <option value="">End Time</option>
                        <option value="00:00:00" <?php echo ($row['EventEndTime'] == '00:00:00' && $row['EventID']) ? 'selected' : '' ?>>N/A</option>
                        <option value="00:00:01" <?php echo $row['EventEndTime'] == '00:00:01' ? 'selected' : '' ?>>Dusk</option>
                        <?PHP
                        $endTime = substr($row['EventEndTime'], 0, 1) == 0 ? substr($row['EventEndTime'], 1) : $row['EventEndTime'];
                        for ($i = 1; $i < 24; $i++) {
                            ?>
                            <option value="<?php echo $i ?>:00:00" <?php echo $endTime == $i . ':00:00' ? 'selected' : '' ?>><?php echo $i > 12 ? $i - 12 : $i ?>:00 <?php echo $i > 11 ? 'pm' : 'am' ?></option>
                            <option value="<?php echo $i ?>:15:00" <?php echo $endTime == $i . ':15:00' ? 'selected' : '' ?>><?php echo $i > 12 ? $i - 12 : $i ?>:15 <?php echo $i > 11 ? 'pm' : 'am' ?></option>
                            <option value="<?php echo $i ?>:30:00" <?php echo $endTime == $i . ':30:00' ? 'selected' : '' ?>><?php echo $i > 12 ? $i - 12 : $i ?>:30 <?php echo $i > 11 ? 'pm' : 'am' ?></option>
                            <option value="<?php echo $i ?>:45:00" <?php echo $endTime == $i . ':45:00' ? 'selected' : '' ?>><?php echo $i > 12 ? $i - 12 : $i ?>:45 <?php echo $i > 11 ? 'pm' : 'am' ?></option>
                            <?PHP
                        }
                        ?>
                    </select>
                    <input type="checkbox" id="all_day" <?php echo ($row['EventTimeAllDay'] == 1) ? 'checked' : '' ?> name="all_day" value="<?php echo ($row['EventTimeAllDay'] == 1) ? 1 : 0 ?>" />
                    <span>All day</span>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Facebook</label>
                <div class="form-data">
                    <input name="facebook_link" type="text" id="address" value="<?php echo $row['Event_Facebook_link'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Short Description<br>
                    200 characters</label>
                <div class="form-data wiziwig-desc description-text">
                    <textarea name="shortdesc" class="tt-ckeditor" onblur="limitTextArea('shortdesc', 200)" onkeyup="limitTextArea('shortdesc', 200)" cols="50" wrap="VIRTUAL" id="shortdesc" required><?php echo $row['ShortDesc'] ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Full Description<br>
                    700 Characters </label>
                <div class="form-data wiziwig-desc description-text">
                    <textarea name="content" class="tt-ckeditor" onblur="limitTextArea('content', 700)" onkeyup="limitTextArea('content', 700)" cols="50" rows="10" wrap="VIRTUAL" id="content" required><?php echo $row['Content'] ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Location</label>
                <div class="form-data">
                    <div id="ajax-location">
                        <select name="setLocationID" id="setLocationID" required>
                            <option value="">Select One</option>
                            <option value="0" <?php echo $row['LocationID'] == 0 ? 'selected' : '' ?>>Use Alternate</option>
                            <?PHP
                            if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
                                $queryconcat = "";
                                $region_locations = explode(",", $row['E_Region_ID']);
                                foreach ($region_locations as $key => $value) {
                                    if ($key == 0) {
                                        $queryconcat .= " FIND_IN_SET (" . $value . ",EL_Region_ID)";
                                    } else {
                                        $queryconcat .= " OR FIND_IN_SET (" . $value . ",EL_Region_ID)";
                                    }
                                }
                                $sql = "SELECT EL_ID, EL_Name  FROM Events_Location WHERE $queryconcat ORDER BY EL_Name";
                                $result = mysql_query($sql, $db);
                                while ($rowTMP = mysql_fetch_assoc($result)) {
                                    ?>
                                    <option value="<?php echo $rowTMP['EL_ID'] ?>" <?php echo $row['LocationID'] == $rowTMP['EL_ID'] ? 'selected' : '' ?>><?php echo $rowTMP['EL_Name'] ?></option>
                                    <?PHP
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <br>
                    <input name="location" type="text" id="location"  value="<?php echo $row['LocationList'] ?>" size="50"  />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Address</label>
                <div class="form-data">
                    <input name="address" type="text" id="address" value="<?php echo $row['StreetAddress'] ?>" size="50" required />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Organization</label>
                <div class="form-data">
                    <div id="ajax-organization">
                        <select name="setOrganizationID" id="setOrganizationID" required>
                            <option value="">Select One</option>
                            <option value="0" <?php echo $row['OrganizationID'] == 0 ? 'selected' : '' ?>>Use Alternate</option>
                            <?PHP
                            if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
                                $queryconcat = "";
                                foreach ($region_locations as $key => $value) {
                                    if ($key == 0) {
                                        $queryconcat .= " FIND_IN_SET (" . $value . ",EO_Region_ID)";
                                    } else {
                                        $queryconcat .= " OR FIND_IN_SET (" . $value . ",EO_Region_ID)";
                                    }
                                }
                                $sql = "SELECT EO_ID, EO_Name FROM Events_Organization WHERE $queryconcat ORDER BY EO_Name";
                                $result = mysql_query($sql, $db);
                                while ($rowTMP = mysql_fetch_assoc($result)) {
                                    ?>
                                    <option value="<?php echo $rowTMP['EO_ID'] ?>" <?php echo $row['OrganizationID'] == $rowTMP['EO_ID'] ? 'selected' : '' ?>><?php echo $rowTMP['EO_Name'] ?></option>
                                    <?PHP
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <br>
                    <input name="organization" type="text" id="organization"  value="<?php echo $row['Organization'] ?>" size="50"   />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Contact Name</label>
                <div class="form-data">
                    <input name="contact" type="text" id="contact" value="<?php echo $row['ContactName'] ?>" size="50" required />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Contact Phone</label>
                <div class="form-data">
                    <input name="phone" class="phone_number" type="text" id="phone" value="<?php echo $row['ContactPhone'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Additional Phone</label>
                <div class="form-data">
                    <input name="tollfree" class="phone_number" type="text" id="tollfree" value="<?php echo $row['ContactPhoneTollFree'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Email</label>
                <div class="form-data">
                    <input name="email" type="text" id="email" value="<?php echo $row['Email'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Website</label>
                <div class="form-data">
                    <input name="website" type="text" id="website" value="<?php echo $row['WebSiteLink'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Status</label>
                <div class="form-data">
                    <select name="pending" required>
                        <option value="">Select One</option>
                        <option value="1" <?php echo $row['Pending'] == 1 ? 'selected' : '' ?>>Pending</option>
                        <option value="0" <?php echo ($row['Pending'] == 0 && $row['EventID']) ? 'selected' : '' ?>>Approved</option>
                        <option value="2" <?php echo $row['Pending'] == 2 ? 'selected' : '' ?>>Delete</option>
                    </select>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="button2" id="button2" value="Submit" /> 
                </div>
            </div>
        </form>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>
<script type="text/javascript">

    function show_file_name(val, count) {
        $("#uploadFile").show();
        document.getElementById("uploadFile").value = val;
    }
    function show_file(input) {
        if (input.files && input.files[0]) {
            $('#uploadFile1Main').show();
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#uploadFile1')
                        .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>

<?PHP
require_once '../include/admin/footer.php';
?>