<?php
require_once '../include/config.inc.php';
require_once '../include/track-data-entry.php';

$return = array();
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'save') {
    $regionID = $_POST['rid'];
    $type = $_POST['type'];
    $story = $_REQUEST['story'];
    if ($story != '') {
        if ($type == 'overall_story') {
            $sql = "tbl_Story_Region SET SR_S_ID = '$story', SR_R_ID = '$regionID'";
            $sql = "INSERT " . $sql;
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            // TRACK DATA ENTRY
            Track_Data_Entry('Websites', $regionID, 'Map Stories', $story, 'Add Region Story', 'super admin');
        }
        if ($result) {
            $return['success'] = 1;
        } else {
            $return['error'] = 1;
        }
        //return
        $stories = '';
        $sqlStories = "SELECT DISTINCT tbl_Story.* FROM tbl_Story LEFT JOIN tbl_Story_Region ON S_ID = SR_S_ID WHERE SR_R_ID = '" . $regionID . "'   ORDER BY S_Title ASC";
        $resStories = mysql_query($sqlStories, $db) or die("Invalid query: $sqlStories -- " . mysql_error());
        while ($rowStories = mysql_fetch_assoc($resStories)) {
             if (isset($rowStories['S_Active']) && $rowStories['S_Active'] == 1) {
                    $checked = "checked";
                } else {
                    $checked = "";
                }
            $stories .= '<div class="data-content" id="recordsArray_' . $rowStories['S_ID'] . '">
                        <div class="data-column spl-org-listngs ">
                          <div class="story-title">' . $rowStories['S_Title'] . '</div>';
            if ($type == 'overall_story') {
                $stories .= '<div class="remove-link"><a style="margin-right: 15px;" href="story.php?id=' . $rowStories['S_ID'] . '" target="_blank">Edit</a><a href="region-stories.php?op=del&rid=' . $regionID . '&id=' . $rowStories['S_ID'] . '" onclick="return confirm(\'Are you sure you want to perform this action?\');">Remove</a></div>';
               $stories .= ' <div class="remove-link"><input type="checkbox" name="display" id="display'. $rowStories['S_ID'].'"  '.$checked.' onclick=" return hide_show_stories('. $rowStories['S_ID'].');"></div>';
            }
            $stories .= '</div>
                      </div>';
        }
        $return['stories'] = $stories;
    }
}
$return['type'] = $type;
print json_encode($return);
exit;
?>