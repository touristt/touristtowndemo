<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('manage-users', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $sql = "DELETE tbl_User_Permission FROM tbl_User_Permission";
    $delete_result = mysql_query($sql, $db);
    if (is_array($_POST['rights'])) {
        for ($i = 1; $i <= $_POST['total']; $i++) {
            foreach ($_POST['rights'][$i] as $val) {
                $values = explode(',', $val);
                $sql = "INSERT tbl_User_Permission SET UP_R_ID = '" . $values[1] . "', UP_P_ID = '" . $values[0] . "'";     
                $result = mysql_query($sql, $db);
            }
        }
    }
    if ($result && $delete_result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Users', '', 'Manage Permissions', '', 'Update', 'super admin');
    }else{
        $_SESSION['error'] = 1;
    }
    header("Location: permissions.php");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Permissions</div>
        <div class="link">
            <a href="permission.php">+Add Permission</a>
        </div>
    </div>
    <div class="left">
        <?php require_once('../include/nav-home-sub.php'); ?>
    </div>
    <div class="right">
        <form name="form1" method="post" action="permissions.php">
            <input type="hidden" name="op" value="save">
            <?PHP
            $section = "SELECT S_ID, S_Name FROM tbl_Section";
            $section_result = mysql_query($section) or die("Invalid query: $section_result -- " . mysql_error());
            while ($section_row = mysql_fetch_assoc($section_result)) {
                echo '<div class="content-sub-header">
                                                                        <div class="data-column padding-none spl-name-permission">' . $section_row['S_Name'] . '</div>';
                $role_query = "SELECT R_ID, R_Name FROM tbl_Role ORDER BY R_Order ASC";
                $count = 0;
                $role_result = mysql_query($role_query, $db) or die("Invalid query: $role_result -- " . mysql_error());
                while ($role_row = mysql_fetch_assoc($role_result)) {
                    $count++;
                    $roleid[$count] = $role_row['R_ID'];
                    echo '<div class="data-column padding-none spl-other-permission">' . $role_row['R_Name'] . '</div>';
                }
                echo '</div>';
                $sql = "SELECT P_Name, P_ID FROM tbl_Permission WHERE P_S_ID = '" . $section_row['S_ID'] ."'";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());

                while ($row = mysql_fetch_assoc($result)) {
                    echo '<div class="data-content">
                          <div class="data-column spl-name-permission"> ' . $row['P_Name'] . ' </div>';
                    for ($i = 1; $i <= $count; $i++) {
                        $query = "SELECT COUNT(*) as id FROM tbl_User_Permission WHERE UP_P_ID ='" . $row['P_ID'] . "' AND UP_R_ID = '" . $roleid[$i] . "'";
                        $query_result = mysql_query($query, $db)
                                or die("Invalid query: $query -- " . mysql_error());
                        while ($query_row = mysql_fetch_assoc($query_result)) {
                            if ($query_row['id'] == 0) {
                                echo '<div class="data-column spl-other-permission">
                                                                                    <input name="rights[' . $i . '][]" value="' . $row['P_ID'] . "," . $roleid[$i] . '" type="checkbox" id="rights-' . $i . '"/></div>';
                            } else if ($query_row['id'] == 1) {
                                echo '<div class="data-column spl-other-permission">
                                                                                    <input name="rights[' . $i . '][]" value="' . $row['P_ID'] . "," . $roleid[$i] . '" type="checkbox" id="rights-' . $i . '" checked="checked"/></div>';
                            }
                        }
                    }
                    echo '</div>';
                }
            }
            ?>
            <div class="form-inside-div  width-data-content"> 
                <div class="button"><input type="hidden" name="total" value="<?php echo $count; ?>">
                    <input type="submit" name="button5" id="button52" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>