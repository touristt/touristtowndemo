<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if (!in_array('superadmin', $_SESSION['USER_ROLES'])) {
    header("Location: /admin/");
    exit();
}

if($_REQUEST['id'] > 0) {
    $sql = "SELECT * FROM tbl_Client_Contacts WHERE CL_ID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $active_client_contact = mysql_fetch_assoc($result);
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $sql = "tbl_Client_Contacts SET CL_Name = '" . encode_strings($_REQUEST['name'], $db) . "', 
            CL_Organization = '" . encode_strings($_REQUEST['organization'], $db) . "', 
            CL_Contact = '" . encode_strings($_REQUEST['contact'], $db) . "', 
            CL_Phone = '" . encode_strings($_REQUEST['phone'], $db) . "', 
            CL_Email = '" . encode_strings($_REQUEST['email'], $db) . "', 
            CL_Notes = '" . encode_strings($_REQUEST['notes'], $db) . "'";
    if ($_REQUEST['id'] > 0) {
        $sql = "UPDATE " . $sql . " WHERE CL_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $id = $_REQUEST['id'];
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    }
    if ($result) {
        $_SESSION['success'] = 1;
        header("Location: client-contacts.php");
        exit();
        
    }
    else {
        $_SESSION['error'] = 1;
    }
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Client Contacts</div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-client-contacts.php'; ?>
    </div>
    <div class="right">
        <form name="form3" method="post" action="client-contact-add.php">
            
            <input type="hidden" name="op" value="save">
            <div class="content-header">Client Contacts Details</div>
            <input type="hidden" name="id" value="<?php echo isset($active_client_contact['CL_ID']) ? $active_client_contact['CL_ID'] : 0 ?>">
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Name</label>
                <div class="form-data">
                    <input name="name" type="text" value="<?php echo isset($active_client_contact['CL_Name']) ? $active_client_contact['CL_Name'] : '' ?>" size="22" maxlength="100" required/> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Organization</label>
                <div class="form-data">
                    <input name="organization" type="text" value="<?php echo isset($active_client_contact['CL_Organization']) ? $active_client_contact['CL_Organization'] : '' ?>" size="22" maxlength="100"/> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Contact</label>
                <div class="form-data">
                    <input name="contact" type="text" value="<?php echo isset($active_client_contact['CL_Contact']) ? $active_client_contact['CL_Contact'] : '' ?>" size="22" maxlength="100"/> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Phone</label>
                <div class="form-data">
                    <input name="phone" type="text" value="<?php echo isset($active_client_contact['CL_Phone']) ? $active_client_contact['CL_Phone'] : '' ?>" size="22" maxlength="100"/> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Email</label>
                <div class="form-data">
                    <input name="email" type="text" value="<?php echo isset($active_client_contact['CL_Email']) ? $active_client_contact['CL_Email'] : '' ?>" size="22" maxlength="100" required/> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Notes</label>
                <div class="form-data">
                    <textarea name="notes" cols="55" rows="10" id="description-listing"><?php echo isset($active_client_contact['CL_Notes']) ? $active_client_contact['CL_Notes'] : '' ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="button" id="button" value="Save" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>