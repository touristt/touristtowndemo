<?php

require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
if ($_REQUEST['del_id'] > 0) {
  $DEL_BL_ID = $_REQUEST['del_id'];
  $sql = "DELETE tbl_Business_Listing, tbl_Business_Listing_Ammenity, 
            tbl_Business_Listing_Category, tbl_Business_Listing_Category_Region,
            tbl_Business_Social, tbl_BL_Feature, payment_profiles,
            tbl_Business_Feature_About, tbl_Business_Feature_About_Photo,
            tbl_Business_Feature_About_Main_Photo, tbl_Business_Feature_Daily_Specials,
            tbl_Business_Feature_Daily_Specials_Description, tbl_Business_Feature_Entertainment_Acts,
            tbl_Business_Feature_Entertainment_Description, tbl_Business_Feature_Guest_Book, tbl_Business_Feature_Guest_Book_Description,
            tbl_Business_Feature_Menu, tbl_Business_Feature_Photo, tbl_Business_Feature_Product, tbl_Business_Feature_Product_Photo,
            tbl_Business_Feature_Coupon, tbl_Business_Feature_Coupon_Category_Multiple, tbl_Image_Bank_Usage
            FROM tbl_Business_Listing 
            LEFT JOIN tbl_Business_Listing_Ammenity ON BLA_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Listing_Category_Region ON BLCR_BLC_ID = BLC_ID 
            LEFT JOIN tbl_Business_Social ON BS_BL_ID = BL_ID
            LEFT JOIN tbl_BL_Feature ON BLF_BL_ID = BL_ID
            LEFT JOIN payment_profiles ON listing_id = BL_ID
            LEFT JOIN tbl_Business_Feature_About ON BFA_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_About_Photo ON BFAP_BFA_ID = BFA_ID
            LEFT JOIN tbl_Business_Feature_About_Main_Photo ON BFAMP_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Daily_Specials ON BFDS_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Daily_Specials_Description ON BFDSD_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Entertainment_Acts ON BFEA_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Entertainment_Description ON BFED_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Guest_Book ON BFGB_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Guest_Book_Description ON tbl_Business_Feature_Guest_Book_Description.BFGB_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Menu ON BFM_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Photo ON BFP_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Product ON tbl_Business_Feature_Product.BFP_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Product_Photo ON BFPP_BFP_ID = tbl_Business_Feature_Product.BFP_ID
            LEFT JOIN tbl_Business_Feature_Coupon ON tbl_Business_Feature_Coupon.BFC_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
            LEFT JOIN tbl_Image_Bank_Usage ON IBU_BL_ID = BL_ID
            WHERE BL_ID = '" . encode_strings($DEL_BL_ID, $db) . "'";
  $result = mysql_query($sql);
  if ($result) {
    $_SESSION['delete'] = 1;
  }
  header('location: inactivated_free_listings.php');
  exit();
}
?>

