<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require '../include/PHPMailer/class.phpmailer.php';
require_once '../include/ranking.inc.php';
require_once '../include/adminFunctions.inc.php';

if (!in_array('business-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
}

$BL_ID = $_REQUEST['bl_id'];
$points_taken = 0;

if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
    // get regions and categories of this listing
    $sql_region = " SELECT RM_Parent, BLCR_BLC_R_ID FROM tbl_Business_Listing
                    LEFT JOIN tbl_Business_Listing_Category  ON BLC_BL_ID = BL_ID
                    LEFT JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID 
                    LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                    LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID
                    WHERE BLC_BL_ID = '$BL_ID'";
    $res_region = mysql_query($sql_region);
    $regions = array();
    while ($r = mysql_fetch_assoc($res_region)) {
        // add region parent as well if available
        if ($r['RM_Parent'] != 0 && !in_array($r['RM_Parent'], $regions)) {
            $regions[] = $r['RM_Parent'];
        }
        // add region
        if ($r['BLCR_BLC_R_ID'] != '' && !in_array($r['BLCR_BLC_R_ID'], $regions)) {
            $regions[] = $r['BLCR_BLC_R_ID'];
        }
    }
} else {
    header("Location:customers.php");
    exit();
}
// getting the default region
foreach ($regions as $key => $r) {
    if ($key == 0) {
        $default_region = $r;
    }
}
// See if a region is already selected
if (isset($_SESSION['ranking_region' . $BL_ID]) && $_SESSION['ranking_region' . $BL_ID] > 0) {
    $ranking_region = $_SESSION['ranking_region' . $BL_ID];
} else {
    $ranking_region = $default_region;
}

if (isset($_POST['button'])) {
    $Recommended_BL_ID = $_POST['recommended_bl_id'];
    $Recommends_BL_ID = $_POST['recommends_bl_id'];
    $search_text1 = $_POST['search_text1'];
    $category1 = $_POST['category1'];
    $subcategory1 = $_POST['subcategory1'];
    $text = str_replace("'","\'",$_POST['recommendation-text' . $Recommended_BL_ID]);
    $email = $_POST['b_email'];
    $query = mysql_query("INSERT INTO tbl_Recommendations(R_Recommends, R_Recommended, R_Text, R_Seen) VALUES('$Recommends_BL_ID', '$Recommended_BL_ID', '$text', 0)") or die("Invalid query: $query -- " . mysql_error());

    //update points only for listing
    update_pointsin_business_tbl($Recommends_BL_ID);
    update_pointsin_business_tbl($Recommended_BL_ID);

    $sql = "SELECT BL_Listing_Title FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($Recommends_BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowRecommends = mysql_fetch_assoc($result);
    $sql = "SELECT BL_Listing_Title, BL_C_ID, BL_Points FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($Recommended_BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowRecommended = mysql_fetch_assoc($result);
    $sql = "SELECT R_Domain FROM tbl_Region WHERE R_ID = '" . encode_strings($ranking_region, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowRegion = mysql_fetch_assoc($result);
    $region_rank = calculate_ranking($Recommended_BL_ID, 'region', $ranking_region, $ranking_region);
    $category_rank = calculate_ranking($Recommended_BL_ID, 'category', $rowRecommended['BL_C_ID'], $ranking_region);
    if ($email != '') {
        $message = '<html>
                    <head>
                    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
                    <title>You are recommended</title>
                    </head>
                    <body>
                    <div><img src="http://touristtown.ca/images/eamil-touristtown-logo.gif" width="195" height="43" align="absmiddle" /></div>
                    <p>Congratulations ' . $rowRecommended['BL_Listing_Title'] . '!</p><br>
                    You�ve just been recommended by ' . $rowRecommends['BL_Listing_Title'] . ' on ' . $rowRegion['R_Domain'] . ' and earned 3 points!<br><br>
                    This has boosted your ranking to ' . $rowRecommended['BL_Points'] . ', which places your business at number ' . $category_rank['rank'] . ' out of ' . $category_rank['total'] . '.  Repay the favour by logging in here to recommend them. (my.touristtown.ca) By recommending other businesses you receive additional points to bump up your ranking even more!<br><br>
                    Recommendations help visitors easily find the best places to visit in your area which benefits everybody. After all, we want our visitors to have a fantastic time so they share their experience with friends and family and having a great time will encourage them to return!  Plus the businesses that are doing a fantastic job (like yours) will reap the rewards of being visited more often.<br><br>
                    Click here to login to your page now. (my.touristtown.ca)<br><br>
                    Regards,<br><br>
                    The Tourist Town Team<br><br>
                    info@touristtown.ca   

                    </body>

                    </html>';


        $mail = new PHPMailer();
        $mail->From = "no-reply@touristtown.com";
        $mail->FromName = "TouristTown";
        $mail->IsHTML(true);

        $mail->AddAddress($email);

        $mail->Subject = "You've Been Recommended on Tourist Town!";
        $mail->MsgHTML($message);
//        $mail->Send();
    }
    if ($query) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: /admin/customer-listing-recommendations.php?bl_id=$Recommends_BL_ID&search_text=$search_text1&op=save&category=$category1&subcategory=$subcategory1");
    exit();
}
if (isset($_POST['button-edit-text'])) {
    $R_ID = $_POST['recommended_r_id'];
    $R_Text = str_replace("'","\'",$_POST['recommendation-text' . $R_ID]);
    $update = "UPDATE tbl_Recommendations SET R_Text = '$R_Text' WHERE R_ID = '$R_ID'";
    $query = mysql_query($update, $db);
    if ($query) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
}
if (isset($_GET['op']) && $_GET['op'] == 'save') {
    $search_text = $_GET['search_text'];
    $category = $_GET['category'];
    $subcategory = $_GET['subcategory'];
    $region = "SELECT R_ID, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($ranking_region, $db) . "'";
    $regionResult = mysql_query($region, $db) or die("Invalid query: $region -- " . mysql_error());
    $regionData = mysql_fetch_array($regionResult);
    $search = " SELECT BL_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, B_Email FROM tbl_Business_Listing
                LEFT JOIN tbl_Business ON BL_B_ID = B_ID
                INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                LEFT JOIN tbl_Listing_Type ON BL_Listing_Type = LT_ID";
    if ($category != '' && $subcategory != '') {
        $search .= " WHERE BLC_C_ID = '" . encode_strings($subcategory, $db) . "'
                    AND BLC_M_C_ID = '" . encode_strings($category, $db) . "'
                    AND BL_Listing_Title LIKE '%" . encode_strings($search_text, $db) . "%'";
    } else if ($category != '' && $subcategory == '') {
        $search .= " WHERE BLC_M_C_ID = '" . encode_strings($category, $db) . "'
                    AND BL_Listing_Title LIKE '%" . encode_strings($search_text, $db) . "%'";
    } else {
        $search .= " WHERE BL_Listing_Title LIKE '%" . encode_strings($search_text, $db) . "%'";
    }
    if ($regionData['R_Type'] == 1) {
        $search .= " AND BLCR_BLC_R_ID IN (SELECT R_ID from tbl_Region LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID where RM_Parent = '" . encode_strings($regionData['R_ID'], $db) . "')";
    } else {
        $search .= " AND BLCR_BLC_R_ID = '" . encode_strings($regionData['R_ID'], $db) . "'";
    }
    $search .= " AND BL_Listing_Type != 1
                AND BL_ID NOT IN(SELECT R_Recommended FROM tbl_Recommendations WHERE R_Recommends = '$BL_ID')
                AND BL_ID != '$BL_ID'
                GROUP BY BL_ID";
    $search_result = mysql_query($search, $db) or die("Invalid query: $search -- " . mysql_error());
}
require_once '../include/admin/header.php';
?>
<div class="content-left">

    <?php require_once '../include/top-nav-listing.php'; ?>

    <div class="title-link">
        <div class="title">My Page - <?php echo $rowListing['BL_Listing_Title'] ?></div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div>
        <div class="instruction">
            Fields with this background<span></span>will show on free listings profile.
        </div>
    </div>

    <div class="left">
        <?PHP
        require '../include/nav-mypage.php';
        ?>
    </div>

    <div class="right">
        <div class="content-header">
            <div class="title">Recommendation</div>
            <div class="link">
                <?PHP
                if ($recommends_count > 0) {
                    echo '<div class="points"><div class="points-com">' . $total_recom_points . ' pts</div></div>';
                    $points_taken += $total_recom_points;
                } else {
                    echo '<div class="points"><div class="points-uncom">' . $total_recom_points . ' pts</div></div>';
                }
                ?>
            </div>
        </div>

        <?php
        $help_text = show_help_text('Recommendations');
        if ($help_text != '') {
            echo '<div class="form-inside-div border-none">' . $help_text . '</div>';
        }
        ?>

        <div class="form-inside-div border-none">
            <div class="content-sub-header">
                <div class="title">Add new Recommendation</div>
            </div>

            <form name="form" action="" method="get" id="form">
                <div class="form-inside-div recom margin-none">
                    <label>
                        Search
                    </label>
                    <input type="text" name="search_text" value="<?php echo (isset($search_text) != '') ? $search_text : ''; ?>" required>
                    <input type="hidden" name="op" value="save">
                    <input type="hidden" name="bl_id" value="<?php echo $BL_ID; ?>">

                    <input type="submit" value="Search" name="search">
                </div>


                <div class="form-inside-div recom margin-none">
                    <label>Category</label>
                    <div class="form-data">
                        <select name="category" id="recommendation-cat" onChange="get_subcat();">

                            <option value="">Select Main Category</option>
                            <?PHP
                            $category_sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = 0 AND C_Is_Blog != 1   AND C_Is_Product_Web != 1 ORDER BY C_Name";
                            $category_result = mysql_query($category_sql, $db)or die("Invalid query: $category_sql -- " . mysql_error());
                            while ($category_row = mysql_fetch_assoc($category_result)) {
                                ?>
                                <option value="<?php echo $category_row['C_ID'] ?>" <?php echo ($category == $category_row['C_ID']) ? 'selected' : ''; ?>><?php echo $category_row['C_Name'] ?></option>
                                <?PHP
                            }
                            ?>

                        </select>
                    </div>
                </div>

                <div class="form-inside-div recom margin-none">
                    <label>Sub-Category</label>
                    <div class="form-data" id="recommendation-subcats">
                        <select name="subcategory" id="recommendation-subcat" onChange="$('#form').submit();">
                            <option value="">Select Sub Category</option>
                            <?PHP
                            if (isset($subcategory)) {
                                $subcat_sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '$category' ORDER BY C_Name";
                                $subcat_result = mysql_query($subcat_sql, $db)or die("Invalid query: $subcat_sql -- " . mysql_error());
                                while ($subcat_row = mysql_fetch_assoc($subcat_result)) {
                                    ?>
                                    <option value="<?php echo $subcat_row['C_ID'] ?>" <?php echo ($subcategory == $subcat_row['C_ID']) ? 'selected' : ''; ?>><?php echo $subcat_row['C_Name'] ?></option>
                                    <?PHP
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </form>

            <!--            Listings to recommend-->
            <div class="form-inside-div margin-none border-none">
                <div class="unordered-list" id="listings-to-recommend">
                    <ul>
                        <?PHP
                        if (isset($search_result)) {
                        while ($search_row = mysql_fetch_assoc($search_result)) {
                            ?>
                            <li ><a style="width:11%;" onclick="return show_textbox(<?php echo $search_row['BL_ID'] ?>)"><img src="../include/img/Add-Button.gif"></a>
                                <div class="listing-title">

                                    <?PHP
                                    $link_sql = "SELECT R_Domain, C_Name_SEO  FROM tbl_Business_Listing_Category 
                                                LEFT JOIN tbl_Category ON BLC_C_ID = C_ID 
                                                INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BLC_BL_ID 
                                                LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                                                WHERE BLC_BL_ID = '" . $search_row['BL_ID'] . "' 
                                                LIMIT 1";
                                    $link_result = mysql_query($link_sql, $db)or die("Invalid query: $link_sql -- " . mysql_error());
                                    $link_row = mysql_fetch_assoc($link_result);
                                    if (isset($search_row['BL_Listing_Type']) && $search_row['BL_Listing_Type'] == 1) {
                                        ?>
                                        <a href="http://<?php echo $link_row['R_Domain'] ?>/<?php echo $link_row['C_Name_SEO'] ?>/<?php echo $link_row['C_Name_SEO'] ?>/" target="_blank"><?php echo $search_row['BL_Listing_Title'] ?></a>
                                        <?php
                                    } else {
                                        ?>
                                        <a href="http://<?php echo $link_row['R_Domain'] ?>/profile/<?php echo $search_row['BL_Name_SEO'] ?>/<?php echo $search_row['BL_ID'] ?>/" target="_blank"><?php echo $search_row['BL_Listing_Title'] ?></a>
                                        <?php
                                    }
                                    ?>
                                </div>
                                <form action="" method="post" name="form" id="recommendation-form<?php echo $search_row['BL_ID'] ?>" style="display:none;">
                                    <div style="float:left; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                        <input type="hidden" name="category1" value="<?php echo $category ?>">
                                        <input type="hidden" name="subcategory1" value="<?php echo $subcategory ?>">
                                        <input type="hidden" name="search_text1" value="<?php echo $search_text ?>">
                                        <input type="hidden" name="recommended_bl_id" value="<?php echo $search_row['BL_ID'] ?>">
                                        <input type="hidden" name="b_email" value="<?php echo $search_row['B_Email'] ?>">
                                        <input type="hidden" name="recommends_bl_id" value="<?php echo $BL_ID ?>">
                                        <textarea name="recommendation-text<?php echo $search_row['BL_ID'] ?>" style="width:420px !important;" rows="8"></textarea>
                                    </div>
                                    <div style="float:left;text-align: center; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                        <div class="form-data">
                                            <input type="submit" name="button" value="Submit"/>
                                        </div>
                                    </div> 
                                </form></li>
                            <?PHP
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>

        <?PHP
        if ($recommends_count > 0) {
            $recommends_point += $recommends_count - 1;
            $points_taken += $recommends_point;
            ?>
            <!--        Recommendations by this listing-->
            <div class="form-inside-div border-none">
                <div class="content-sub-header">
                    <div class="title">Your Existing Recommendations</div>
                    <div class="link">
                        <?php
                        if ($recommends_point > 0) {
                            echo '<div class="points-com">' . $recommends_point . ' pts</div>';
                            $points_taken += $phone;
                        } else {
                            echo '<div class="points-uncom">' . $recommends_point . ' pts</div>';
                        }
                        ?>
                    </div>
                </div>

                <div class="form-inside-div margin-none border-none">
                    <div class="unordered-list existing-recommendations-custom">
                        <ul>
                            <?php
                            while ($recommends_row = mysql_fetch_assoc($recommends_result)) {
                                ?>

                                <li>
                                    <div class="listing-title existing-recommendations">
                                        <?PHP
                                        $link_sql1 = "SELECT R_Domain, C_Name_SEO FROM tbl_Business_Listing_Category 
                                                    LEFT JOIN tbl_Category ON BLC_C_ID = C_ID 
                                                    INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BLC_BL_ID 
                                                    LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                                                    WHERE BLC_BL_ID = '" . $recommends_row['BL_ID'] . "' 
                                                    LIMIT 1";
                                        $link_result1 = mysql_query($link_sql1, $db) or die("Invalid query: $link_sql1 -- " . mysql_error());
                                        $link_row1 = mysql_fetch_assoc($link_result1);
                                        if ($recommends_row['BL_Listing_Type'] == 1) {
                                            ?>
                                            <a href="http://<?php echo $link_row1['R_Domain'] ?>/<?php echo $link_row1['C_Name_SEO'] ?>/<?php echo $link_row1['C_Name_SEO'] ?>/" target="_blank"><?php echo $recommends_row['BL_Listing_Title'] ?></a>
                                        <?php } else { ?>
                                            <a href="http://<?php echo $link_row1['R_Domain'] ?>/profile/<?php echo $recommends_row['B_Name_SEO'] ?>/<?php echo $recommends_row['BL_ID'] ?>/" target="_blank"><?php echo $recommends_row['BL_Listing_Title'] ?></a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="existing-recommendations-edit-delete">
                                    <a onclick="return show_textbox_existing(<?php echo $recommends_row['R_ID'] ?>)">Edit</a>
                                    <a href="delete-listing-recommendation.php?rid=<?php echo $recommends_row['R_ID'] ?>&bl_id=<?php echo $BL_ID ?>" onclick="return confirm('Are you sure you want to delete this recommendation?');">Delete</a>
                                    </div>
                                    <form action="" method="post" name="form" id="existing-recommendation-form<?php echo $recommends_row['R_ID'] ?>" style="display:none;">
                                        <div style="float:left; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                            <input type="hidden" name="recommended_r_id" value="<?php echo $recommends_row['R_ID'] ?>">
                                            <textarea name="recommendation-text<?php echo $recommends_row['R_ID'] ?>" style="width:420px !important;" rows="8"><?php echo $recommends_row['R_Text'] ?></textarea>
                                        </div>
                                        <div style="float:left;text-align: center; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                            <div class="form-data">
                                                <input type="submit" name="button-edit-text" value="Submit"/>
                                            </div>
                                        </div> 
                                    </form>
                                </li>
                                <?PHP
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
            <?php
        }
        if ($recommended_count > 0) {
            $recommended_point += $recommended_count * 3;
            $points_taken += $recommended_point;
            ?>
            <!--        Recommendations to this listing-->
            <div class="form-inside-div border-none">
                <div class="content-sub-header">
                    <div class="title">My Recommendations</div>
                    <div class="link">
                        <div class="points-com">
                            <?php echo $recommended_point . " pts" ?>
                        </div>
                    </div>
                </div>

                <div class="form-inside-div margin-none border-none">
                    <div class="unordered-list existing-recommendations-custom">
                        <ul>
                            <?php
                            while ($recommended_row = mysql_fetch_assoc($recommended_result)) {
                                ?>

                                <li>
                                    <div class="listing-title existing-recommendations">
                                        <?PHP
                                        $link_sql1 = "SELECT R_Domain, C_Name_SEO FROM tbl_Business_Listing_Category 
                                                    LEFT JOIN tbl_Category ON BLC_C_ID = C_ID 
                                                    INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BLC_BL_ID 
                                                    LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                                                    WHERE BLC_BL_ID = '" . $recommended_row['BL_ID'] . "' 
                                                    LIMIT 1";
                                        $link_result1 = mysql_query($link_sql1, $db) or die("Invalid query: $link_sql1 -- " . mysql_error());
                                        $link_row1 = mysql_fetch_assoc($link_result1);


                                        if ($recommended_row['BL_Listing_Type'] == 1) {
                                            ?>
                                            <a href="http://<?php echo $link_row1['R_Domain'] ?>/<?php echo $link_row1['C_Name_SEO'] ?>/<?php echo $link_row1['C_Name_SEO'] ?>/" target="_blank"><?php echo $recommended_row['BL_Listing_Title'] ?></a>
                                        <?php } else { ?>
                                            <a href="http://<?php echo $link_row1['R_Domain'] ?>/profile/<?php echo $recommended_row['B_Name_SEO'] ?>/<?php echo $recommended_row['BL_ID'] ?>/" target="_blank"><?php echo $recommended_row['BL_Listing_Title'] ?></a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="existing-recommendations-edit-delete">
                                    <a onclick="return view_recommendation(<?php echo $recommended_row['R_ID'] ?>)">View</a>
                                    <a href="delete-listing-recommendation.php?rid=<?php echo $recommended_row['R_ID'] ?>&bl_id=<?php echo $BL_ID ?>" onclick="return confirm('Are you sure you want to delete this recommendation?');">Delete</a>
                                    </div>
                                    <div class="form-inside-div" id="view-my-recommendation<?php echo $recommended_row['R_ID'] ?>" style="display:none;">
                                        <?php echo $recommended_row['R_Text'] ?>
                                    </div>
                                </li>
                                <?PHP
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php } ?>

        <div class="form-inside-div listing-ranking border-none">
            Ranking Points: <?php echo $points_taken ?> points
        </div>
    </div>
</div>
<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>