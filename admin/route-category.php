<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if (!in_array('manage-routes', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} else {
    header('location: regions.php');
    exit();
}

if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $activeCatID = $_REQUEST['id'];
} else {
    $activeCatID = 0;
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $sql = "tbl_Route_Category SET ";

    require '../include/picUpload.inc.php';
    if ($_POST['image_bank_desktop'] == "") {
        // last @param 30 = Route Main Image
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 31);
        if (is_array($pic)) {
            $sql .= "RC_Photo = '" . encode_strings($pic['0']['0'], $db) . "',
                    RC_Mobile_Photo = '" . encode_strings($pic['0']['1'], $db) . "',
                    RC_Feature_Photo = '" . encode_strings($pic['0']['2'], $db) . "',";
        }
        $pic_id = $pic['1'];
    } else {
        $pic_id = $_POST['image_bank_desktop'];
        // last @param 4 = Region Category Main Image
        $pic_response = Upload_Pic_Library($pic_id, 31);
        if ($pic_response) {
            $sql .= "RC_Photo = '" . encode_strings($pic_response['0'], $db) . "',
                    RC_Mobile_Photo = '" . encode_strings($pic_response['1'], $db) . "',
                    RC_Feature_Photo = '" . encode_strings($pic_response['2'], $db) . "',";
        }
    }

    if (strlen($_REQUEST['category_video']) > 5) {
        $video_url = $_REQUEST['category_video'];
    } else {
        $video_url = '';
    }
    $zoom = ($_REQUEST['category_zoom'] > 0) ? $_REQUEST['category_zoom'] : 0; 
    $sql .=" RC_Video = '" . encode_strings($video_url, $db) . "',
            RC_Name = '" . encode_strings($_REQUEST['category_name'], $db) . "',
            RC_Introduction_Text  = '" . encode_strings($_REQUEST['category_introduction'], $db) . "',
            RC_Description  = '" . encode_strings($_REQUEST['category_description'], $db) . "',
            RC_Alt_Tag = '" . encode_strings($_REQUEST['category_alt'], $db) . "',
            RC_Slider_Title = '" . encode_strings($_REQUEST['slider_title'], $db) . "',
            RC_Slider_Description = '" . encode_strings($_REQUEST['slider_description'], $db) . "',
            RC_SEO_Title = '" . encode_strings($_REQUEST['seo_title'], $db) . "',
            RC_SEO_Keywords  = '" . encode_strings($_REQUEST['seo_keywords'], $db) . "',
            RC_SEO_Description  = '" . encode_strings($_REQUEST['seo_description'], $db) . "',
            RC_Lat = '" . encode_strings($_REQUEST['category_lat'], $db) . "',
            RC_Long = '" . encode_strings($_REQUEST['category_long'], $db) . "',
            RC_Zoom  = '" . encode_strings($zoom, $db) . "',
            RC_Status  = '1',
            RC_R_ID = '" . encode_strings($_REQUEST['rid'], $db) . "'";
    if ($_POST['id'] > 0) {
        $routeCatID = $_POST['id'];
        $sql = "UPDATE " . $sql . " WHERE RC_ID = '" . encode_strings($_POST['id'], $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        // TRACK DATA ENTRY
        $id = $_REQUEST['rid'];
        Track_Data_Entry('Websites', $id, 'Manage Route Category ', $routeCatID, 'Update Route Category', 'super admin');
    } else {
        $sqlMax = "SELECT MAX(RC_Order) FROM tbl_Route_Category WHERE RC_R_ID = '". $_REQUEST['rid'] ."'";
        $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
        $rowMax = mysql_fetch_row($resultMax);
        
        $sql = "INSERT " . $sql . " , RC_Order = '". ($rowMax[0] + 1) ."'"; //echo $sql; exit;
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $routeCatID = mysql_insert_id();
        // TRACK DATA ENTRY
        $id = $_REQUEST['rid'];
        Track_Data_Entry('Websites', $id, 'Manage Route Category ', $routeCatID, 'Add Route Category', 'super admin');
    }
    if ($result) {
        $_SESSION['success'] = 1;
        //Image usage from image bank.
        if ($pic_id > 0) {
            imageBankUsage($pic_id, 'IBU_Route_Category', $routeCatID);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    header('location: route-category.php?rid=' . $_REQUEST['rid'] .'&id=' . $routeCatID);
    exit();
}
if (isset($_GET['op']) && $_GET['op'] == 'del') {
    $update = "UPDATE tbl_Route_Category SET RC_Photo = '', RC_Mobile_Photo='', RC_Feature_Photo='' WHERE RC_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['rid'];
        $routeCatID = $_REQUEST['id'];
        Track_Data_Entry('Websites', $id, 'Manage Route Category ', $routeCatID, 'Delete Photo', 'super admin');
        imageBankUsageDelete('IBU_Route_Category', $_REQUEST['id']);
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header('location: route-category.php?rid=' . $_REQUEST['rid'] .'&id=' . $_REQUEST['id']);
    exit;
}
//Get Active Region Information
$sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);
//Get Current Route Category
if($activeCatID > 0) {
  $sqlRouteCat = "SELECT * FROM tbl_Route_Category WHERE RC_ID = '" . encode_strings($activeCatID, $db) . "'";
  $resRouteCat = mysql_query($sqlRouteCat, $db) or die("Invalid query: $sqlRouteCat -- " . mysql_error());
  $activeRouteCat = mysql_fetch_assoc($resRouteCat);
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Manage Routes</div>
        <div class="link"><a href="routes.php?rid=<?php echo $regionID ?>">Individual Routes</a></div>
        <div class="link"><a href="routes-categories.php?rid=<?php echo $regionID ?>">Route Categories</a></div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <form id="routeForm" name="routeForm" method="post" enctype="multipart/form-data" action="">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="id" value="<?php echo isset($activeRouteCat['RC_ID']) ? $activeRouteCat['RC_ID'] : 0 ?>">
            <input type="hidden" name="rid" value="<?php echo $activeRegion['R_ID'] ?>">
            <div class="content-header">
              Manage Route Category<?php echo isset($activeRouteCat['RC_Name']) ? ' - '. $activeRouteCat['RC_Name'] : ''  ?>
             
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Category Name</label>
                <div class="form-data">
                  <input name="category_name" type="text" value="<?php echo isset($activeRouteCat['RC_Name']) ? $activeRouteCat['RC_Name'] : '' ?>" size="50" required />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Category Introduction</label>
                <div class="form-data">
                    <textarea name="category_introduction" cols="85" rows="10" class="tt-description"><?php echo isset($activeRouteCat['RC_Introduction_Text']) ? $activeRouteCat['RC_Introduction_Text'] : '' ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Category Description</label>
                <div class="form-data">
                    <textarea name="category_description" cols="85" rows="10" class="tt-description"><?php echo isset($activeRouteCat['RC_Description']) ? $activeRouteCat['RC_Description'] : '' ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Primary Photo</label>
                <div class="form-data div_image_library cat-region-width">
                    <?php if (isset($activeRouteCat['RC_Photo']) && $activeRouteCat['RC_Photo'] != '') { ?>
                        <a class="deletePhoto delete-region-cat-photo" onClick="return confirm('Are you sure you want to delete photo?');" href="route-category.php?rid=<?php echo $activeRouteCat['RC_R_ID'] ?>&id=<?php echo $activeRouteCat['RC_ID'] ?>&op=del">Delete Photo</a>
                    <?php } ?>
                    <div class="cropit-image-preview aboutus-photo-perview">
                        <img class="preview-img preview-img-script31" style="display: none;" src="">    
                        <?php if (isset($activeRouteCat['RC_Photo']) && $activeRouteCat['RC_Photo'] != '') { ?>
                            <img class="existing-img existing_imgs31" src="<?php echo IMG_LOC_REL . $activeRouteCat['RC_Photo'] ?>" >
                        <?php } ?>
                    </div>
                    <span class="daily_browse" onclick="show_image_library(31)">Select File</span>
                    <input type="hidden" name="image_bank_desktop" id="image_bank31" value="">
                    <input type="file" onchange="show_file_name(31, this)" name="pic[]" id="photo31" style="display: none;">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Slider Title</label>
                <div class="form-data">
                    <input name="slider_title" type="text"  value="<?php echo isset($activeRouteCat['RC_Slider_Title']) ? $activeRouteCat['RC_Slider_Title'] : '' ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Slider Description</label>
                <div class="form-data"> 
                    <textarea name="slider_description" cols="85" rows="10" class="tt-description"><?php echo isset($activeRouteCat['RC_Slider_Description']) ? $activeRouteCat['RC_Slider_Description'] : '' ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Alt Tag</label>
                <div class="form-data">
                    <input name="category_alt" type="text" value="<?php echo isset($activeRouteCat['RC_Alt_Tag']) ? $activeRouteCat['RC_Alt_Tag'] : '' ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Video</label>
                <div class="form-data">
                    <input type="text" name="category_video" value="<?php echo isset($activeRouteCat['RC_Video']) ? $activeRouteCat['RC_Video'] : '' ?>">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>SEO Title</label>
                <div class="form-data">
                    <input name="seo_title" type="text" value="<?php echo isset($activeRouteCat['RC_SEO_Title']) ? $activeRouteCat['RC_SEO_Title'] : '' ?>" size="50" required />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>SEO Keywords</label>
                <div class="form-data">
                    <input name="seo_keywords" type="text" value="<?php echo isset($activeRouteCat['RC_SEO_Keywords']) ? $activeRouteCat['RC_SEO_Keywords'] : '' ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>SEO Description</label>
                <div class="form-data">
                    <textarea name="seo_description" cols="50"><?php echo isset($activeRouteCat['RC_SEO_Description']) ? $activeRouteCat['RC_SEO_Description'] : '' ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Map Latitude</label>
                <div class="form-data">
                  <input name="category_lat" type="text" value="<?php echo isset($activeRouteCat['RC_Lat']) ? $activeRouteCat['RC_Lat'] : '' ?>" size="50" required />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Map Longitude</label>
                <div class="form-data">
                  <input name="category_long" type="text" value="<?php echo isset($activeRouteCat['RC_Long']) ? $activeRouteCat['RC_Long'] : '' ?>" size="50" required />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Map Zoom</label>
                <div class="form-data">
                  <input name="category_zoom" type="text" value="<?php echo isset($activeRouteCat['RC_Zoom']) ? $activeRouteCat['RC_Zoom'] : '' ?>" size="50" required />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin border-none"> 
                <div class="button">
                    <input type="submit" name="button2" value="Submit" />
                </div>
            </div>
        </form>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>