<?PHP
$link_sql = "SELECT BL_Listing_Type, R_Domain, C_Name_SEO, BL_Name_SEO, BL_ID FROM tbl_Business_Listing
            LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID
            LEFT JOIN tbl_Category ON BLC_C_ID = C_ID 
            INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BLC_BL_ID 
            LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
            WHERE BLC_BL_ID = '" . $BL_ID . "' AND R_Type != 8 AND R_Status = 1
            LIMIT 1";
$link_result = mysql_query($link_sql, $db) or die("Invalid query: $link_sql -- " . mysql_error());
$link_row = mysql_fetch_assoc($link_result);
$text = 'Preview Listing';
if(isset($preview) && $preview != ''){
    $text = $preview;
}

//show preview link only if applicable
if (filter_var('http://' . $link_row['R_Domain'], FILTER_VALIDATE_URL) && $link_row['BL_Name_SEO'] != '') {
    ?>
    <a href="http://<?php echo $link_row['R_Domain'] ?>/profile/<?php echo $link_row['BL_Name_SEO'] ?>/<?php echo $link_row['BL_ID'] ?>" target="_blank"><?php echo $text; ?></a>
    <?php
}
?>