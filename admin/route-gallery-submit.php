<?PHP
require_once '../include/config.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $IR_ID = $_POST['ir_id'];
    $RID = $_POST['rid'];
    $sql = "tbl_Individual_Route_Gallery SET ";
    require '../include/picUpload.inc.php';
    if ($_POST['image_bank'] == "") {
        // last @param 33 = Route Gallery Images
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 33);
        if (is_array($pic)) {
            $sql .= "IRG_Photo = '" . encode_strings($pic['0']['0'], $db) . "',
                    IRG_Mobile_Photo = '" . encode_strings($pic['0']['1'], $db) . "',
                    IRG_Thumbnail_Photo = '" . encode_strings($pic['0']['2'], $db) . "',";
        }
        $pic_id = $pic['1'];
    } else {
        $pic_id = $_POST['image_bank'];
        // last @param 33 = Route Gallery Images
        $pic = Upload_Pic_Library($pic_id, 33);
        if (is_array($pic)) {
            $sql .= "IRG_Photo = '" . encode_strings($pic['0'], $db) . "',
                    IRG_Mobile_Photo = '" . encode_strings($pic['1'], $db) . "',
                    IRG_Thumbnail_Photo = '" . encode_strings($pic['2'], $db) . "',";
        }
    }
    $sql .=" IRG_Title = '" . encode_strings($_REQUEST['gallery_title'], $db) . "',
            IRG_IR_ID = '". $IR_ID ."'";
    if ($_POST['irg_id'] > 0) {
        $routeGalID = $_POST['irg_id'];
        $sql = "UPDATE " . $sql . " WHERE IRG_ID = '" . encode_strings($_POST['irg_id'], $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $RID, 'Manage Individual Route', $IR_ID, 'Update Gallery Photo', 'super admin');
    } else {
        $sqlMax = "SELECT MAX(IRG_Order) FROM tbl_Individual_Route_Gallery WHERE IRG_IR_ID = '$IR_ID'";
        $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
        $rowMax = mysql_fetch_row($resultMax);
        $sql .= ", IRG_Order = '" . ($rowMax[0] + 1) . "'";
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $routeGalID = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $RID, 'Manage Individual Route', $IR_ID, 'Add Gallery Photo', 'super admin');
    }
    if ($result) {
        $_SESSION['success'] = 1;
        if ($pic_id > 0) {
            //Image usage from image bank
            imageBankUsage($pic_id, 'IBU_Individual_Route', $IR_ID, 'IBU_Individual_Route_Gallery', $routeGalID);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: route.php?rid=". $RID ."&id=". $IR_ID);
    exit();
}
?>