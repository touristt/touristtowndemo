<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('manage-users', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$sql = "SELECT BG_ID, BG_Name FROM tbl_Buying_Group WHERE BG_ID = " . $_REQUEST['bg_id'] . "";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$BG_row = mysql_fetch_assoc($result);

if ($_GET['bg_id'] <> $BG_row['BG_ID']) {
    header("Location: /admin/cc-list.php");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $BG_row['BG_Name'] ?>: Manage Users</div>
        <div class="link">
            <a href="cc-user.php?bg_id=<?php echo  $BG_row['BG_ID'] ?>">+Add User</a>
        </div>
    </div>
    <div class="left">
        <?php require_once('../include/nav-home-sub.php'); ?>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column ccp-title padding-none">User's Name</div>
            <div class="data-column ccp-title padding-none">Email Address</div>
            <div class="data-column ccp-other padding-none">Admin</div>
            <div class="data-column ccp-other padding-none">Edit</div>
            <div class="data-column ccp-other padding-none">Delete</div>
        </div>
        <?PHP
        $sql = "SELECT * FROM tbl_Buying_Group_User WHERE BGU_Buying_Group_ID = " . $BG_row['BG_ID'] . " ORDER BY BGU_F_Name, BGU_F_Name  ";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            $bg = ($bg == '#E6E6E6' ? '#FFFFFF' : '#E6E6E6');
            ?>
            <div class="data-content">
                <div class="data-column ccp-title"><?php echo  $row['BGU_F_Name'] ?> <?php echo  $row['BGU_L_Name'] ?></div>
                <div class="data-column ccp-title"><?php echo  $row['BGU_Email'] ?></div>
                <div class="data-column ccp-other"><?php echo  $row['BGU_Super_User'] ? '<img src="/images/tick-green.png" width="13" height="12">' : '&nbsp;' ?></div>
                <div class="data-column ccp-other"><a href="cc-user.php?bg_id=<?php echo  $BG_row['BG_ID'] ?>&amp;id=<?php echo  $row['BGU_ID'] ?>">Edit</a></div>
                <div class="data-column ccp-other"><a onClick="return confirm('Are you sure?\nThis action CANNOT be undone!');" href="cc-user.php?bg_id=<?php echo  $BG_row['BG_ID'] ?>&amp;id=<?php echo  $row['BGU_ID'] ?>&amp;op=del">Delete</a></div>
            </div>
            <?PHP
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>