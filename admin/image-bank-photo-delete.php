<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

$pending = $_GET['pending'];
$pages = $_GET['page'];
$order = $_GET['img_sort'];
$ib_id = $_REQUEST['ib_id'];
$fuleurl = $_SERVER['QUERY_STRING'];

require '../include/picUpload.inc.php';
$msg = Delete_Pic_Library($ib_id);
if ($msg == 1) {
    $_SESSION['delete'] = 1;
    if ($pending == 1) {
        // TRACK DATA ENTRY
        $id = $_REQUEST['ib_id'];
        Track_Data_Entry('Images','','Pending Images',$id,'Delete','super admin');
        if (isset($fuleurl) && $fuleurl !== '') {
            header("location:pending-image-bank.php?" . $fuleurl);
            exit();
        } else {
            header("location:pending-image-bank.php");
            exit();
        }
    } else {
        // TRACK DATA ENTRY
        $id = $_REQUEST['ib_id'];
        Track_Data_Entry('Images','','Image Bank',$id,'Delete','super admin');
        if (isset($fuleurl) && $fuleurl !== '') {
            header("location:image-bank.php?" . $fuleurl);
            exit();
        } elseif ($pages) {
            header("location:image-bank.php?page=" . $pages);
            exit();
        } elseif ($order) {
            header("location:image-bank.php?img_sort=" . $order);
            exit();
        } else {
            header("location:image-bank.php");
            exit();
        }
    }
} else {
    $_SESSION['delete_error'] = 1;
    header("location:image-bank.php");
    exit();
}