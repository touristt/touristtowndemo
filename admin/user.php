<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('manage-users', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['id'] > 0) {
    $sql = "SELECT * FROM tbl_User WHERE U_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
}

if ($_POST['op'] == 'save') {
    $Ur_G_Name = $_REQUEST['group-name'];
    $Ur_F_Name = $_REQUEST['name'];
    $Ur_email = $_REQUEST['email'];
    $Ur_uname = $_REQUEST['uname'];
    $Ur_title = $_REQUEST['title'];
    $Ur_phone = $_REQUEST['phone-number'];
    $Ur_commission = $_REQUEST['commission'];
    $Ur_address = $_REQUEST['address'];
    $Ur_town = $_REQUEST['town'];
    $Ur_postal = $_REQUEST['postal'];
    $Ur_province = $_REQUEST['province'];
    $Ur_roles = $_REQUEST['roles'];
    $Ur_limits = $_REQUEST['limits'][$Ur_roles];
    $Ur_owner = $_REQUEST['owner'];
    $Ur_owner == "" ? $Ur_owner = 0 : '';
    if(isset($_REQUEST['show_businesses'])){
        $show_businesses = 1;
    }else{
        $show_businesses = 0;
    }
    if ($_POST['roles'] != 'town') {
        $show_businesses = 0;
    }
    $sql = "tbl_User SET 
            U_G_Name = '" . encode_strings($_REQUEST['group-name'], $db) . "', 
            U_F_Name = '" . encode_strings($_REQUEST['name'], $db) . "', 
            U_Email = '" . encode_strings($_REQUEST['email'], $db) . "', 
            U_Username = '" . encode_strings($_REQUEST['uname'], $db) . "', 
            U_Title = '" . encode_strings($_REQUEST['title'], $db) . "',
            U_Phone_Number = '" . encode_strings($_REQUEST['phone-number'], $db) . "',
            U_Commission = '" . encode_strings($_REQUEST['commission'], $db) . "',
            U_Street_Address = '" . encode_strings($_REQUEST['address'], $db) . "',
            U_Town = '" . encode_strings($_REQUEST['town'], $db) . "',
            U_Postal_Code = '" . encode_strings($_REQUEST['postal'], $db) . "',
            U_Province = '" . encode_strings($_REQUEST['province'], $db) . "',
            U_Owner = '" . encode_strings($Ur_owner, $db) . "',
            U_Show_Businesses = '" . encode_strings($show_businesses, $db) . "'";
    if ($_REQUEST['password']) {
        $sql .= ", U_Password = MD5('" . encode_strings($_REQUEST['password'], $db) . "')";
    }
    $sql_check_email = "SELECT * FROM tbl_User WHERE 1=1 AND  U_Email = '" . encode_strings($_REQUEST['email'], $db) . "'  
                        AND U_ID != '" . $row['U_ID'] . "'";
    $result_check_email = mysql_query($sql_check_email, $db) or die("Invalid query: $sql_check_email -- " . mysql_error());
    $row_count_check_email = mysql_num_rows($result_check_email);
    $sql_check_user = "SELECT * FROM tbl_User WHERE 1=1 AND U_Username = '" . encode_strings($_REQUEST['uname'], $db) . "' 
                       AND U_ID != '" . $row['U_ID'] . "'";
    $result_check_user = mysql_query($sql_check_user, $db) or die("Invalid query: $sql_check_user -- " . mysql_error());
    $row_count_check_user = mysql_num_rows($result_check_user);
    if ($row_count_check_email > 0 && $row_count_check_user > 0) {
        $_SESSION['email_username_error'] = 1;
        if ($row['U_ID'] > 0) {
            header("Location: user.php?id=" . $row['U_ID']);
            exit();
        }
    } else if ($row_count_check_user > 0) {
        $_SESSION['username_error'] = 1;
        if ($row['U_ID'] > 0) {
            header("Location: user.php?id=" . $row['U_ID']);
            exit();
        }
    } else if ($row_count_check_email > 0) {
        $_SESSION['email_error'] = 1;
        if ($row['U_ID'] > 0) {
            header("Location: user.php?id=" . $row['U_ID']);
            exit();
        }
    } else {
        if (isset($_POST['roles'])) {
            if ($row['U_ID'] > 0) {
                $sql = "UPDATE " . $sql . " WHERE U_ID = '" . $row['U_ID'] . "'";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                // TRACK DATA ENTRY
                $id = $row['U_ID'];
                Track_Data_Entry('Users', '', 'Manage Users', $id, 'Update', 'super admin');
            } else {
                $sql = "INSERT " . $sql;
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                $id = mysql_insert_id($db);
                // TRACK DATA ENTRY
                Track_Data_Entry('Users', '', 'Manage Users', $id, 'Add', 'super admin');
            }
            
            $sql = "DELETE tbl_User_Role FROM tbl_User_Role WHERE UR_U_ID = " . $id;
            $delete_roles = mysql_query($sql, $db);
            if (isset($_POST['roles'])) {
                $val = $_POST['roles'];
                $sql = "INSERT tbl_User_Role SET UR_U_ID = '" . $id . "', UR_R_ID = '" . $val . "'";

                if ($_POST['limits'][$val]) {
                    if ($_POST['roles'] == 'town') {
                        $string = rtrim(implode(',', $_POST['limits'][$val]), ',');
                        $sql .= ", UR_Limit = '" . $string . "'";
                    } else {
                        $sql .= ", UR_Limit = '" . $_POST['limits'][$val] . "'";
                    }
                }
                $roles_result = mysql_query($sql, $db);
            }
            if ($result && $delete_roles && $roles_result) {
                $_SESSION['success'] = 1;
            } else {
                $_SESSION['error'] = 1;
            }
        } else {
            $_SESSION['error'] = 1;
        }
        header("Location: users.php");
        exit();
    }
} elseif ($_GET['op'] == 'del') {
    $sql = "DELETE tbl_User, tbl_User_Role FROM tbl_User LEFT JOIN tbl_User_Role ON UR_U_ID = U_ID WHERE U_ID = '" . $row['U_ID'] . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $row['U_ID'];
        Track_Data_Entry('Users', '', 'Manage Users', $id, 'Delete', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }

    header("Location: users.php");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Users</div>
        <div class="addlisting"><a href="user.php">+Add User</a></div>
        <div class="link">
        </div>
    </div>
    <div class="left"><?php require_once('../include/nav-home-sub.php'); ?></div>
    <div class="right">
        <form name="form1" method="post" action="user.php" onsubmit="return checkVal()" enctype="multipart/form-data">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="id" id="check_id" value="<?php echo $row['U_ID'] ?>">
            <div class="content-header">User Details</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Rights</label>

                <div class="form-data">
                    <?PHP
                    $sql = "SELECT * FROM tbl_Role LEFT JOIN tbl_User_Role ON R_ID = UR_R_ID && UR_U_ID = '" . $row[U_ID] . "' ORDER BY R_Order";
                    $resultRoles = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    $i = 1;
                    while ($rowRoles = mysql_fetch_assoc($resultRoles)) {
                        $id = $rowRoles['R_ID'];
                        echo '
                      <input name="roles" value="' . $rowRoles['R_ID'] . '" class="checkValuse"  type="radio" id="roles-' . $rowRoles['R_ID'] . '" '
                        . (( $rowRoles['R_ID'] == $rowRoles['UR_R_ID'] && $rowRoles['UR_U_ID'] > 0) ? 'checked="checked"' : '' ) . '  ' . ( $rowRoles['R_ID'] == $Ur_roles ? 'checked="checked"' : '' ) . '  onchange="change_fields(this)" required/>
                          ' . $rowRoles['R_Name'];
                        if ($rowRoles['R_ID'] == 'bgadmin') {
                            echo " -  Limit to: <select class='margin-top-user' name='limits[" . $rowRoles['R_ID'] . "]' id='dd-" . $rowRoles['R_ID'] . "'>";
                            $sql = "SELECT * FROM tbl_Buying_Group ORDER BY BG_Name";
                            $resultBG = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            echo '<option value=""';
                            echo $rowRoles['UR_Limit'] == '' ? 'selected' : '';
                            echo '>None Selected</option>';
                            while ($rowBG = mysql_fetch_assoc($resultBG)) {
                                echo '<option value="' . $rowBG['BG_ID'] . '"';
                                echo $rowRoles['UR_Limit'] == $rowBG['BG_ID'] ? 'selected' : '';
                                echo ($Ur_limits == $rowBG['BG_ID'] && $rowRoles['R_ID'] == $Ur_roles) ? 'selected' : '';
                                echo '>' . $rowBG['BG_Name'] . '</option>';
                            }
                            echo "</select>";
                        }
                        if ($rowRoles['R_ID'] == 'regionadmin' || $rowRoles['R_ID'] == 'county') {
                            echo " -  Limit to: <select class='margin-top-user' name='limits[" . $rowRoles['R_ID'] . "]' id='dd-" . $rowRoles['R_ID'] . "'>";
                            $sql = "SELECT R_ID, R_Name FROM tbl_Region ORDER BY R_Name";
                            $resultRegions = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            echo '<option value=""';
                            echo $rowRoles['UR_Limit'] == '' ? 'selected' : '';
                            echo '>None Selected</option>';
                            while ($rowRegions = mysql_fetch_assoc($resultRegions)) {
                                echo '<option value="' . $rowRegions['R_ID'] . '"';
                                echo $rowRoles['UR_Limit'] == $rowRegions['R_ID'] ? 'selected' : '';
                                echo ($Ur_limits == $rowRegions['R_ID'] && $rowRoles['R_ID'] == $Ur_roles) ? 'selected' : '';
                                echo '>' . $rowRegions['R_Name'] . '</option>';
                            }
                            echo "</select>";
                        }
                        if ($rowRoles['R_ID'] == 'town') {
                            $townlimit = explode(',', $rowRoles['UR_Limit']);
                            echo ' -  Limit to: <input type="checkbox" name="show_businesses" value="" ' . (($row['U_Show_Businesses'] == 1) ? 'checked' : '') . '/>Show Businesses';
                            echo '<div class="limit-checkboxes">';
                            $sql = "SELECT R_ID, R_Name FROM tbl_Region ORDER BY R_Name";
                            $resultTowns = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($rowTowns = mysql_fetch_assoc($resultTowns)) {
                                echo '<div class="limit-checkbox">';
                                echo '<input type="checkbox" name="limits[' . $rowRoles['R_ID'] . '][]" value="' . $rowTowns['R_ID'] . '" ' . (in_array($rowTowns['R_ID'], $townlimit) ? 'checked' : '') . '/>' . $rowTowns['R_Name'];
                                echo '</div>';
                            }
                            echo '</div>';
                        }
                        echo '<br>';
                        $i++;
                    }
                    ?>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin" id="group-name" style="display:none">
                <label id="group-label"></label>
                <div class="form-data">
                    <input name="group-name" type="text" value="<?php echo ($row['U_G_Name'] != '') ? $row['U_G_Name'] : $Ur_G_Name; ?>" size="40">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Contact Name</label>
                <div class="form-data">
                    <input name="name" type="text" value="<?php echo ($row['U_F_Name'] != '') ? $row['U_F_Name'] : $Ur_F_Name; ?>" size="40" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Title</label>
                <div class="form-data">
                    <input name="title" type="text" id="title" value="<?php echo ($row['U_Title'] != '') ? $row['U_Title'] : $Ur_title; ?>" size="40">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Email Address</label>
                <div class="form-data">
                    <input name="email" type="email" value="<?php echo ($row['U_Email'] != '') ? $row['U_Email'] : $Ur_email; ?>" size="40" id="email_check"  required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Username</label>
                <div class="form-data">
                    <input name="uname" type="text" id="user_name"  value="<?php echo ($row['U_Username'] != '') ? $row['U_Username'] : $Ur_uname; ?>" size="40" required>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Password</label>
                <div class="form-data">
                    <?php if (isset($_REQUEST['id']) && $_REQUEST['id'] !== '') { ?>
                        <input name="password" type="password" size="40" />                  
                    <?php } else { ?>
                        <input name="password" type="password" size="40" required /> 
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin" id="phone-number" style="display:none;">
                <label>Phone Number</label>
                <div class="form-data">
                    <input name="phone-number" id="phone" type="text" value="<?php echo ($row['U_Phone_Number'] != '') ? $row['U_Phone_Number'] : $Ur_phone; ?>" size="40" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin" id="commission" style="display:none;">
                <label>Commission (%)</label>
                <div class="form-data">
                    <input name="commission" type="text" id="commission-text" value="<?php echo ($row['U_Commission'] != '') ? $row['U_Commission'] : $Ur_commission; ?>" pattern="[0-9]{1,3}" title="You can enter only digits" maxlength="3" size="40"/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Street Address</label>
                <div class="form-data">
                    <input name="address" type="text" value="<?php echo ($row['U_Street_Address'] != '') ? $row['U_Street_Address'] : $Ur_address; ?>" size="40" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Town</label>
                <div class="form-data">
                    <input name="town" type="text" value="<?php echo ($row['U_Town'] != '') ? $row['U_Town'] : $Ur_town; ?>" size="40" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Postal Code</label>
                <div class="form-data">
                    <input name="postal" type="text" value="<?php echo ($row['U_Postal_Code'] != '') ? $row['U_Postal_Code'] : $Ur_postal; ?>" size="40" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Province</label>
                <div class="form-data">
                    <input name="province" type="text" value="<?php echo ($row['U_Province'] != '') ? $row['U_Postal_Code'] : $Ur_province; ?>" size="40" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin" id="owner" style="display:none;">
                <label>Image Bank Owner</label>
                <div class="form-data">
                    <select name="owner" id="input_owner">
                        <option value="">Select Owner</option>
                        <?PHP
                        $sql = "SELECT * FROM tbl_Photographer_Owner";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($owner = mysql_fetch_assoc($result)) {
                            if (isset($_REQUEST['id']) && $_REQUEST['id'] !== '') {
                                ?>
                                <option value="<?php echo $owner['PO_ID'] ?>" <?php echo ($owner['PO_ID'] == $row['U_Owner']) ? 'selected' : '' ?>><?php echo $owner['PO_Name'] ?></option>
                                <?PHP
                            } else {
                                ?>
                                <option value="<?php echo $owner['PO_ID'] ?>" <?php echo ($owner['PO_ID'] == $Ur_owner) ? 'selected' : '' ?>><?php echo $owner['PO_Name'] ?></option>
                                <?PHP
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="button5" id="button52" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    if ($('#roles-bgadmin').is(":checked")) {
        document.getElementById("group-label").innerHTML = 'Buying Group Name';
        document.getElementById("group-name").style.display = 'block';
        document.getElementById("dd-bgadmin").required = true;
    }
    if ($('#roles-regionadmin').is(":checked")) {
        document.getElementById("group-label").innerHTML = 'Regional Name';
        document.getElementById("group-name").style.display = 'block';
        document.getElementById("commission").style.display = 'block';
        document.getElementById("phone-number").style.display = 'block';
        document.getElementById("dd-regionadmin").required = true;
    }
    if ($('#roles-superadmin').is(":checked")) {
        document.getElementById("group-label").innerHTML = 'Admin Name';
        document.getElementById("group-name").style.display = 'block';
        document.getElementById("phone-number").style.display = 'block';
    }
    if ($('#roles-town').is(":checked")) {
        document.getElementById("group-label").innerHTML = 'Town Name';
        document.getElementById("group-name").style.display = 'block';
        document.getElementById("phone-number").style.display = 'block';
        document.getElementById("dd-town").required = true;
    }
    if ($('#roles-county').is(":checked")) {
        document.getElementById("group-label").innerHTML = 'County Name';
        document.getElementById("group-name").style.display = 'block';
        document.getElementById("phone-number").style.display = 'block';
        document.getElementById("commission").style.display = 'none';
        document.getElementById("owner").style.display = 'block';
        document.getElementById("input_owner").required = true;
        document.getElementById("dd-county").required = true;
    }
    function change_fields(select) {
        if ($(select).is(":checked")) {
            if (select.id == 'roles-bgadmin') {
                document.getElementById("group-label").innerHTML = 'Buying Group Name';
                document.getElementById("group-name").style.display = 'block';
                document.getElementById("commission").style.display = 'none';
                document.getElementById("phone-number").style.display = 'none';
                document.getElementById("owner").style.display = 'none';
                document.getElementById("input_owner").required = false;
                document.getElementById("dd-county").required = false;
                document.getElementById("dd-town").required = false;
                document.getElementById("dd-regionadmin").required = false;
                document.getElementById("dd-bgadmin").required = true;
            }
            if (select.id == 'roles-regionadmin') {
                document.getElementById("group-label").innerHTML = 'Regional Name';
                document.getElementById("group-name").style.display = 'block';
                document.getElementById("commission").style.display = 'block';
                document.getElementById("phone-number").style.display = 'block';
                document.getElementById("owner").style.display = 'none';
                document.getElementById("input_owner").required = false;
                document.getElementById("dd-county").required = false;
                document.getElementById("dd-town").required = false;
                document.getElementById("dd-regionadmin").required = true;
                document.getElementById("dd-bgadmin").required = false;
            }
            if (select.id == 'roles-superadmin') {
                document.getElementById("group-label").innerHTML = 'Admin Name';
                document.getElementById("group-name").style.display = 'block';
                document.getElementById("phone-number").style.display = 'block';
                document.getElementById("commission").style.display = 'none';
                document.getElementById("owner").style.display = 'none';
                document.getElementById("input_owner").required = false;
                document.getElementById("dd-county").required = false;
                document.getElementById("dd-town").required = false;
                document.getElementById("dd-regionadmin").required = false;
                document.getElementById("dd-bgadmin").required = false;
            }
            if (select.id == 'roles-town') {
                document.getElementById("group-label").innerHTML = 'Town Name';
                document.getElementById("group-name").style.display = 'block';
                document.getElementById("phone-number").style.display = 'block';
                document.getElementById("commission").style.display = 'none';
                document.getElementById("owner").style.display = 'none';
                document.getElementById("input_owner").required = false;
                document.getElementById("dd-county").required = false;
                document.getElementById("dd-town").required = true;
                document.getElementById("dd-regionadmin").required = false;
                document.getElementById("dd-bgadmin").required = false;
            }
            if (select.id == 'roles-county') {
                document.getElementById("group-label").innerHTML = 'County Name';
                document.getElementById("group-name").style.display = 'block';
                document.getElementById("phone-number").style.display = 'block';
                document.getElementById("commission").style.display = 'none';
                document.getElementById("owner").style.display = 'block';
                document.getElementById("input_owner").required = true;
                document.getElementById("dd-county").required = true;
                document.getElementById("dd-town").required = false;
                document.getElementById("dd-regionadmin").required = false;
                document.getElementById("dd-bgadmin").required = false;
            }
        }
    }
</script>
<?PHP
require_once '../include/admin/footer.php';
?>