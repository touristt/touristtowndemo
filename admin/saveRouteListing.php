<?php
require_once '../include/config.inc.php';
require_once '../include/track-data-entry.php';

$return = array();
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'save') {
    $IR_ID = $_REQUEST['irid'];
    $RID = $_REQUEST['rid'];
    $BL_ID = $_REQUEST['listings'];
    $MUSTSEE = isset($_REQUEST['mustsee']) ? 1 : 0;
    if ($BL_ID > 0) {
        $sqlMax = "SELECT MAX(IRML_Order) FROM tbl_Individual_Route_Map_Listings WHERE IRML_IR_ID = '$IR_ID'";
        $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
        $rowMax = mysql_fetch_row($resultMax);
        $sqlCheck = "SELECT IRML_ID FROM tbl_Individual_Route_Map_Listings WHERE IRML_IR_ID = '" . $IR_ID . "' AND IRML_BL_ID = '" . $BL_ID . "'";
        $resCheck = mysql_query($sqlCheck);
        if(mysql_num_rows($resCheck) > 0) {
          $Check = mysql_fetch_assoc($resCheck);
          $sql = "UPDATE tbl_Individual_Route_Map_Listings SET IRML_Must_See = '". $MUSTSEE ."' WHERE IRML_ID = '" . $Check['IRML_ID'] . "'";
        }
        else {
          $sql = "INSERT tbl_Individual_Route_Map_Listings SET IRML_IR_ID = '" . $IR_ID . "', IRML_BL_ID = '" . $BL_ID . "', IRML_Must_See = '". $MUSTSEE ."', IRML_Order = '". ($rowMax[0] + 1) ."'";
        }
        $result = mysql_query($sql) or die(mysql_error());
        if($result) {
          $return['success'] = 1;
          // TRACK DATA ENTRY
          Track_Data_Entry('Websites', $RID, 'Manage Route Listings', $IR_ID, 'Add Route Listing', 'super admin');
        }
        else {
          $return['error'] = 1;
        }
        //return
        $listings = '';
        $sqlListings = "SELECT BL_ID, BL_Listing_Title, IRML_ID, IRML_IR_ID, IRML_Must_See FROM tbl_Business_Listing 
                        INNER JOIN tbl_Individual_Route_Map_Listings ON IRML_BL_ID = BL_ID 
                        WHERE IRML_IR_ID = '". $IR_ID ."' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY))
                        GROUP BY BL_ID
                        ORDER BY IRML_Order ASC";

        $resListings = mysql_query($sqlListings, $db) or die("Invalid query: $sqlListings -- " . mysql_error());
        while ($rowListings = mysql_fetch_assoc($resListings)) {
            $listings .= '<div class="data-content" id="recordsArray_'. $rowListings['IRML_ID'] .'">
                <div class="data-column mup-name">
                  '. $rowListings['BL_Listing_Title'] .'
                </div>
                <div class="data-column spl-other">&nbsp;</div>
                <div class="data-column spl-other"><input type="checkbox" name="mustsee" '. (($rowListings['IRML_Must_See'] == 1) ? 'checked' : '') .' /></div>
                <div class="data-column spl-other">
                  <a onclick="return confirm(\'Are you sure?\')" href="route-listings.php?op=del&rid='. $RID .'&id='. $rowListings['IRML_ID'] .'&irid='. $rowListings['IRML_IR_ID'] .'">Remove</a>
                </div>
            </div>';
        }

        $return['listings'] = $listings;
    }
}

print json_encode($return);exit;

?>