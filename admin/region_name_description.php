<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} elseif ($_REQUEST['newRegion'] <> true) {
    header("Location: /admin/regions.php");
    exit();
}

$sql = "SELECT R_ID, R_Name, R_Type, R_Name_Description FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);


if ($_POST['op'] == 'save') {
    $sql = "UPDATE tbl_Region SET R_Name_Description='" . encode_strings(trim(str_replace('<p>', '', str_replace('</p>', '', $_REQUEST['description']))), $db) . "' 
            WHERE R_ID = '" . encode_strings($activeRegion['R_ID'], $db) . "'";
    $result = mysql_query($sql) or die(mysql_error());

    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
    }else{
        $_SESSION['error'] = 1;
    }
    header("Location: region_name_description.php?rid=" . $regionID);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo  $activeRegion['R_Name'] . " - Description "; ?></div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <form action="/admin/region_name_description.php" method="post" name="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="rid" value="<?php echo  $regionID ?>">
            <div class="content-header"><?php echo  $activeRegion['R_Name'] . " Description"; ?></div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Description</label>
                <div class="form-data">
                    <textarea name="description" cols="85" rows="10" id="description-listing"><?php echo  $activeRegion['R_Name_Description'] ?></textarea>                           
                </div>
            </div>
            <div class="form-inside-div  width-data-content border-none"> 
                <div class="button"><input type="submit" name="button2" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
