<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
}

$sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);
$sql = "SELECT RS_FB_Icon, RS_SE_Icon, RS_Mobile_SE_Icon, RS_T_Icon, RS_Y_Icon, RS_I_Icon, RS_S_Icon, RS_Info_Icon, RS_C_Icon, RS_FB_Link, RS_T_Link, 
        RS_T_Icon, RS_T_Icon, RS_Y_Link, RS_I_Link, RS_S_Link, RS_Info_Link, RS_C_Link FROM tbl_Region_Social 
        WHERE RS_R_ID = '" . encode_strings($regionID, $db) . "'";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$count = mysql_num_rows($result);
$row = mysql_fetch_assoc($result);
if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $sql = "tbl_Region_Social SET 
            RS_FB_Link = '" . encode_strings($_POST['fb_link'], $db) . "',
            RS_T_Link = '" . encode_strings($_POST['t_link'], $db) . "',
            RS_Y_Link = '" . encode_strings($_POST['y_link'], $db) . "',
            RS_S_Link = '" . encode_strings($_POST['s_link'], $db) . "',
            RS_I_Link = '" . encode_strings($_POST['i_link'], $db) . "',
            RS_Info_Link = '" . encode_strings($_POST['info_link'], $db) . "',
            RS_C_Link = '" . encode_strings($_POST['c_link'], $db) . "'";
    require '../include/picUpload.inc.php';
    if ($_FILES['search_icon']['name'] != '') {
        $pic = Upload_Pic_Normal('0', 'search_icon', 0, 0, true, IMG_LOC_ABS, 0, true);
        if ($pic) {
            $sql .= ", RS_SE_Icon = '" . encode_strings($pic, $db) . "'";
            if ($row['RS_SE_Icon']) {
                Delete_Pic(IMG_LOC_ABS . $row['RS_SE_Icon']);
            }
        }
    }
    if ($_FILES['mobile_search_icon']['name'] != '') {
        $pic = Upload_Pic_Normal('0', 'mobile_search_icon', 0, 0, true, IMG_LOC_ABS, 0, true);
        if ($pic) {
            $sql .= ", RS_Mobile_SE_Icon = '" . encode_strings($pic, $db) . "'";
            if ($row['RS_Mobile_SE_Icon']) {
                Delete_Pic(IMG_LOC_ABS . $row['RS_Mobile_SE_Icon']);
            }
        }
    }
    if ($_FILES['fb_icon']['name'] != '') {
        $pic = Upload_Pic_Normal('0', 'fb_icon', 0, 0, true, IMG_LOC_ABS, 0, true);
        if ($pic) {
            $sql .= ", RS_FB_Icon = '" . encode_strings($pic, $db) . "'";
            if ($row['RS_FB_Icon']) {
                Delete_Pic(IMG_LOC_ABS . $row['RS_FB_Icon']);
            }
        }
    }
    if ($_FILES['t_icon']['name'] != '') {
        $pic = Upload_Pic_Normal('0', 't_icon', 0, 0, true, IMG_LOC_ABS, 0, true);
        if ($pic) {
            $sql .= ", RS_T_Icon = '" . encode_strings($pic, $db) . "'";
            if ($row['RS_T_Icon']) {
                Delete_Pic(IMG_LOC_ABS . $row['RS_T_Icon']);
            }
        }
    }
    if ($_FILES['y_icon']['name'] != '') {
        $pic = Upload_Pic_Normal('0', 'y_icon', 0, 0, true, IMG_LOC_ABS, 0, true);
        if ($pic) {
            $sql .= ", RS_Y_Icon = '" . encode_strings($pic, $db) . "'";
            if ($row['RS_Y_Icon']) {
                Delete_Pic(IMG_LOC_ABS . $row['RS_Y_Icon']);
            }
        }
    }
    if ($_FILES['i_icon']['name'] != '') {
        $pic = Upload_Pic_Normal('0', 'i_icon', 0, 0, true, IMG_LOC_ABS, 0, true);
        if ($pic) {
            $sql .= ", RS_I_Icon = '" . encode_strings($pic, $db) . "'";
            if ($row['RS_I_Icon']) {
                Delete_Pic(IMG_LOC_ABS . $row['RS_I_Icon']);
            }
        }
    }
    if ($_FILES['s_icon']['name'] != '') {
        $pic = Upload_Pic_Normal('0', 's_icon', 0, 0, true, IMG_LOC_ABS, 0, true);
        if ($pic) {
            $sql .= ", RS_S_Icon = '" . encode_strings($pic, $db) . "'";
            if ($row['RS_S_Icon']) {
                Delete_Pic(IMG_LOC_ABS . $row['RS_S_Icon']);
            }
        }
    }
    if ($_FILES['info_icon']['name'] != '') {
        $pic = Upload_Pic_Normal('0', 'info_icon', 0, 0, true, IMG_LOC_ABS, 0, true);
        if ($pic) {
            $sql .= ", RS_Info_Icon = '" . encode_strings($pic, $db) . "'";
            if ($row['RS_Info_Icon']) {
                Delete_Pic(IMG_LOC_ABS . $row['RS_Info_Icon']);
            }
        }
    }
    if ($_FILES['c_icon']['name'] != '') {
        $pic = Upload_Pic_Normal('0', 'c_icon', 0, 0, true, IMG_LOC_ABS, 0, true);
        if ($pic) {
            $sql .= ", RS_C_Icon = '" . encode_strings($pic, $db) . "'";
            if ($row['RS_C_Icon']) {
                Delete_Pic(IMG_LOC_ABS . $row['RS_C_Icon']);
            }
        }
    }
    if ($_POST['counter'] > 0) {
        $sql = "UPDATE " . $sql . " WHERE RS_R_ID = '$regionID'";
         // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $regionID, 'Manage Social Media', '', 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql . ", RS_R_ID = '" . encode_strings($regionID, $db) . "'";
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $regionID, 'Manage Social Media', '', 'Add', 'super admin');
    }
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: region-social-media.php?rid=" . $regionID);
    exit();
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    require '../include/picUpload.inc.php';
    $sql = "update tbl_Region_Social SET ";
    if ($_REQUEST['flag'] == 'search') {
        $sql .= " RS_SE_Icon = ''";
        if ($row['RS_SE_Icon']) {
            Delete_Pic(IMG_LOC_ABS . $row['RS_SE_Icon']);
        }
    }
    if ($_REQUEST['flag'] == 'mobile_search') {
        $sql .= " RS_Mobile_SE_Icon = ''";
        if ($row['RS_Mobile_SE_Icon']) {
            Delete_Pic(IMG_LOC_ABS . $row['RS_Mobile_SE_Icon']);
        }
    }
    if ($_REQUEST['flag'] == 'fb') {
        $sql .= " RS_FB_Icon = ''";
        if ($row['RS_FB_Icon']) {
            Delete_Pic(IMG_LOC_ABS . $row['RS_FB_Icon']);
        }
    }
    if ($_REQUEST['flag'] == 'tw') {
        $sql .= " RS_T_Icon = ''";
        if ($row['RS_T_Icon']) {
            Delete_Pic(IMG_LOC_ABS . $row['RS_T_Icon']);
        }
    }
    if ($_REQUEST['flag'] == 'yout') {
        $sql .= " RS_Y_Icon = ''";
        if ($row['RS_Y_Icon']) {
            Delete_Pic(IMG_LOC_ABS . $row['RS_Y_Icon']);
        }
    }
    if ($_REQUEST['flag'] == 'in') {
        $sql .= " RS_I_Icon = ''";
        if ($row['RS_I_Icon']) {
            Delete_Pic(IMG_LOC_ABS . $row['RS_I_Icon']);
        }
    }
    if ($_REQUEST['flag'] == 'snapc') {
        $sql .= " RS_S_Icon = ''";
        if ($row['RS_S_Icon']) {
            Delete_Pic(IMG_LOC_ABS . $row['RS_S_Icon']);
        }
    }
    if ($_REQUEST['flag'] == 'info') {
        $sql .= " RS_Info_Icon = ''";
        if ($row['RS_Info_Icon']) {
            Delete_Pic(IMG_LOC_ABS . $row['RS_Info_Icon']);
        }
    }
    if ($_REQUEST['flag'] == 'contact') {
        $sql .= " RS_C_Icon = ''";
        if ($row['RS_C_Icon']) {
            Delete_Pic(IMG_LOC_ABS . $row['RS_C_Icon']);
        }
    }
    $sql .= " WHERE RS_R_ID = '$regionID'";
    $result_del = mysql_query($sql, $db);
    if ($result_del) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $regionID, 'Manage Social Media', $_REQUEST['flag'], 'Delete', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: region-social-media.php?rid=" . $regionID);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Manage Social Media</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <form action="/admin/region-social-media.php" method="post" name="form" enctype="multipart/form-data">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <div class="content-header">Social Media Details</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Search Icon</label>
                <div class="form-data from-inside-div-text">
                    <div class="approved-photo-div">
                        <div class="inputWrapper adv-photo float-left">Browse
                            <input class="fileInput" type="file" name="search_icon[]" onchange="show_file(this, 'search')">
                        </div>
                    </div>
                    <div class="approved-photo-div">
                        <img id="uploadFile_search" src="<?php echo ($row['RS_SE_Icon'] != '') ? 'http://' . DOMAIN . IMG_LOC_REL . $row['RS_SE_Icon'] : '' ?>">          
                    </div>
                    <?php if ($row['RS_SE_Icon'] != '') {
                        ?>
                        <a class="deletePhoto delete-region-cat-photo" style="margin-top: 5px;width: 127px;" onclick="return confirm('Are you sure you want to delete photo?');" href="region-social-media.php?rid=<?php echo $regionID ?>&op=del&flag=search">Delete Photo</a>
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Mobile Search Icon</label>
                <div class="form-data from-inside-div-text">
                    <div class="approved-photo-div">
                        <div class="inputWrapper adv-photo float-left">Browse
                            <input class="fileInput" type="file" name="mobile_search_icon[]" onchange="show_file(this, 'mobile_search')">
                        </div>
                    </div>
                    <div class="approved-photo-div">
                        <img id="uploadFile_mobile_search" src="<?php echo ($row['RS_Mobile_SE_Icon'] != '') ? 'http://' . DOMAIN . IMG_LOC_REL . $row['RS_Mobile_SE_Icon'] : '' ?>">          
                    </div>
                    <?php if ($row['RS_Mobile_SE_Icon'] != '') {
                        ?>
                        <a class="deletePhoto delete-region-cat-photo" style="margin-top: 5px;width: 127px;" onclick="return confirm('Are you sure you want to delete photo?');" href="region-social-media.php?rid=<?php echo $regionID ?>&op=del&flag=mobile_search">Delete Photo</a>
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin border-none"> 
                <label>Facebook Link</label>
                <div class="form-data"> 
                    <input name="fb_link" type="text" value="<?php echo $row['RS_FB_Link'] ?>" size="50"> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Facebook Icon</label>
                <div class="form-data from-inside-div-text">
                    <div class="approved-photo-div">
                        <div class="inputWrapper adv-photo float-left">Browse
                            <input class="fileInput" type="file" name="fb_icon[]" onchange="show_file(this, 'fb')">
                        </div>
                    </div>
                    <div class="approved-photo-div">
                        <img id="uploadFile_fb" src="<?php echo ($row['RS_FB_Icon'] != '') ? 'http://' . DOMAIN . IMG_LOC_REL . $row['RS_FB_Icon'] : '' ?>">          
                    </div>
                    <?php if ($row['RS_FB_Icon'] != '') {
                        ?>
                        <a class="deletePhoto delete-region-cat-photo" style="margin-top: 5px;width: 127px;" onclick="return confirm('Are you sure you want to delete photo?');" href="region-social-media.php?rid=<?php echo $regionID ?>&op=del&flag=fb">Delete Photo</a>
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Twitter Link</label>
                <div class="form-data"> 
                    <input name="t_link" type="text" value="<?php echo $row['RS_T_Link'] ?>" size="50"> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Twitter Icon</label>
                <div class="form-data from-inside-div-text">
                    <div class="approved-photo-div">
                        <div class="inputWrapper adv-photo float-left">Browse
                            <input class="fileInput" type="file" name="t_icon[]" onchange="show_file(this, 't')">
                        </div>
                    </div>
                    <div class="approved-photo-div">
                        <img id="uploadFile_t" src="<?php echo ($row['RS_T_Icon'] != '') ? 'http://' . DOMAIN . IMG_LOC_REL . $row['RS_T_Icon'] : '' ?>">          
                    </div>
                    <?php if ($row['RS_T_Icon'] != '') {
                        ?>
                        <a class="deletePhoto delete-region-cat-photo" style="margin-top: 5px;width: 127px;" onclick="return confirm('Are you sure you want to delete photo?');" href="region-social-media.php?rid=<?php echo $regionID ?>&op=del&flag=tw">Delete Photo</a>
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Youtube Link</label>
                <div class="form-data"> 
                    <input name="y_link" type="text" value="<?php echo $row['RS_Y_Link'] ?>" size="50"> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Youtube Icon</label>
                <div class="form-data from-inside-div-text">
                    <div class="approved-photo-div">
                        <div class="inputWrapper adv-photo float-left">Browse
                            <input class="fileInput" type="file" name="y_icon[]" onchange="show_file(this, 'y')">
                        </div>
                    </div>
                    <div class="approved-photo-div">
                        <img id="uploadFile_y" src="<?php echo ($row['RS_Y_Icon'] != '') ? 'http://' . DOMAIN . IMG_LOC_REL . $row['RS_Y_Icon'] : '' ?>">          
                    </div>
                    <?php if ($row['RS_Y_Icon'] != '') {
                        ?>
                        <a class="deletePhoto delete-region-cat-photo" style="margin-top: 5px;width: 127px;" onclick="return confirm('Are you sure you want to delete photo?');" href="region-social-media.php?rid=<?php echo $regionID ?>&op=del&flag=yout">Delete Photo</a>
                    <?php } ?>

                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Instagram Link</label>
                <div class="form-data">  
                    <input name="i_link" type="text" value="<?php echo $row['RS_I_Link'] ?>" size="50"> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Instagram Icon</label>
                <div class="form-data from-inside-div-text">
                    <div class="approved-photo-div">
                        <div class="inputWrapper adv-photo float-left">Browse
                            <input class="fileInput" type="file" name="i_icon[]" onchange="show_file(this, 'i')">
                        </div>
                    </div>
                    <div class="approved-photo-div">
                        <img id="uploadFile_i" src="<?php echo ($row['RS_I_Icon'] != '') ? 'http://' . DOMAIN . IMG_LOC_REL . $row['RS_I_Icon'] : '' ?>">          
                    </div>
                    <?php if ($row['RS_I_Icon'] != '') {
                        ?>
                        <a class="deletePhoto delete-region-cat-photo" style="margin-top: 5px;width: 127px;" onclick="return confirm('Are you sure you want to delete photo?');" href="region-social-media.php?rid=<?php echo $regionID ?>&op=del&flag=in">Delete Photo</a>
                    <?php } ?>

                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Snapchat Link</label>
                <div class="form-data">  
                    <input name="s_link" type="text" value="<?php echo $row['RS_S_Link'] ?>" size="50"> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Snapchat Icon</label>
                <div class="form-data from-inside-div-text">
                    <div class="approved-photo-div">
                        <div class="inputWrapper adv-photo float-left">Browse
                            <input class="fileInput" type="file" name="s_icon[]" onchange="show_file(this, 's')">
                        </div>
                    </div>
                    <div class="approved-photo-div">
                        <img id="uploadFile_s" src="<?php echo ($row['RS_S_Icon'] != '') ? 'http://' . DOMAIN . IMG_LOC_REL . $row['RS_S_Icon'] : '' ?>">          
                    </div>
                    <?php if ($row['RS_S_Icon'] != '') {
                        ?>
                        <a class="deletePhoto delete-region-cat-photo" style="margin-top: 5px;width: 127px;" onclick="return confirm('Are you sure you want to delete photo?');" href="region-social-media.php?rid=<?php echo $regionID ?>&op=del&flag=snapc">Delete Photo</a>
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Information Link</label>
                <div class="form-data">  
                    <input name="info_link" type="text" value="<?php echo $row['RS_Info_Link'] ?>" size="50"> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Information Icon</label>
                <div class="form-data from-inside-div-text">
                    <div class="approved-photo-div">
                        <div class="inputWrapper adv-photo float-left">Browse
                            <input class="fileInput" type="file" name="info_icon[]" onchange="show_file(this, 'info')">
                        </div>
                    </div>
                    <div class="approved-photo-div">
                        <img id="uploadFile_info" src="<?php echo ($row['RS_Info_Icon'] != '') ? 'http://' . DOMAIN . IMG_LOC_REL . $row['RS_Info_Icon'] : '' ?>">          
                    </div>
                    <?php if ($row['RS_Info_Icon'] != '') {
                        ?>
                        <a class="deletePhoto delete-region-cat-photo" style="margin-top: 5px;width: 127px;" onclick="return confirm('Are you sure you want to delete photo?');" href="region-social-media.php?rid=<?php echo $regionID ?>&op=del&flag=info">Delete Photo</a>
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Contact Link</label>
                <div class="form-data">  
                    <input name="c_link" type="text" value="<?php echo $row['RS_C_Link'] ?>" size="50"> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Contact Icon</label>
                <div class="form-data from-inside-div-text">
                    <div class="approved-photo-div">
                        <div class="inputWrapper adv-photo float-left">Browse
                            <input class="fileInput" type="file" name="c_icon[]" onchange="show_file(this, 'c')">
                        </div>
                    </div>
                    <div class="approved-photo-div">
                        <img id="uploadFile_c" src="<?php echo ($row['RS_C_Icon'] != '') ? 'http://' . DOMAIN . IMG_LOC_REL . $row['RS_C_Icon'] : '' ?>">          
                    </div>
                    <?php if ($row['RS_C_Icon'] != '') {
                        ?>
                        <a class="deletePhoto delete-region-cat-photo" style="margin-top: 5px;width: 127px;" onclick="return confirm('Are you sure you want to delete photo?');" href="region-social-media.php?rid=<?php echo $regionID ?>&op=del&flag=contact">Delete Photo</a>
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="hidden" name="counter" value="<?php echo $count; ?>">
                    <input type="submit" name="button" value="Submit" />
                </div>
            </div>
    </div>
</div>
<script>
    function show_file(input, media) {
        if (input.files && input.files[0]) {
            $('#uploadFile_' + media).show();
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#uploadFile_' + media)
                        .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<?PHP
require_once '../include/admin/footer.php';
?>
