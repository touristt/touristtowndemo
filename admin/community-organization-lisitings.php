<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';
if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
    $sortby = $_REQUEST['sortby'];
    $subcategory = $_REQUEST['subcategory'];
    $sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
    $sql_community_parent = "SELECT RM_Parent FROM  tbl_Region_Multiple where RM_Child='" . encode_strings($regionID, $db) . "'";
    $result_community_parent = mysql_query($sql_community_parent, $db) or die("Invalid query: $sql_community_parent -- " . mysql_error());
    $active_community_parent = mysql_fetch_assoc($result_community_parent);
    $CO_Community = $active_community_parent['RM_Parent'];
    $sql_org_listings = "SELECT BLCR_BL_ID FROM  tbl_Business_Listing_Category_Region WHERE BLCR_BLC_R_ID = '" . encode_strings($regionID, $db) . "'";
    $result_org_listings = mysql_query($sql_org_listings, $db) or die("Invalid query: $sql_org_listings -- " . mysql_error());
    while ($row_org_listings = mysql_fetch_assoc($result_org_listings)) {
        $childbl_id[] = $row_org_listings['BLCR_BL_ID'];
    }
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <?php
    $sql_name = "SELECT R_Name FROM tbl_Region WHERE R_ID = $CO_Community LIMIT 1";
    $result_name = mysql_query($sql_name, $db) or die("Invalid query: $sql_name -- " . mysql_error());
    $activeRegion_name = mysql_fetch_assoc($result_name);
    ?>
    <div class="title-link">
        <div class="title"><?php echo  $activeRegion['R_Name'] ?><?php echo  '  ' ?><?php echo  $activeRegion_name['R_Name'] ?> - Listings</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">

        <div class="content-sub-header link-header region-header-padding">
            <div class="data-column spl-name padding-none">Select Listings for <?php echo  $activeRegion['R_Name'] ?><?php echo  '  ' ?><?php echo  $activeRegion_name['R_Name'] ?>
            </div>
        </div>
        <form name="form1" method="GET" id="org_listings_form" action="/admin/community-organization-lisitings.php">
            <input type="hidden" name="rid" value="<?php echo  $regionID ?>">
            <div class="content-sub-header link-header region-header-padding">
                <div class="data-column spl-other-email "><span class="addlisting font_weight_normal" >Region: </span><span><?php echo  $activeRegion_name['R_Name'] ?></span></div>
                <div class="data-column spl-name padding-org-listing"><span class="addlisting font_weight_normal" >Sort: </span>
                    <select name="sortby" id="categorydd" onchange="$('#org_listings_form').submit()">
                        <option value="">Sort by:</option>
                        <?PHP
                        $sql = "SELECT C_ID, C_Name FROM tbl_Category LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID LEFT JOIN tbl_Region ON RC_R_ID = R_ID
                                WHERE R_ID = $CO_Community  AND C_Parent = 0 ORDER BY C_Name";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo  $row['C_ID'] ?>" <?php echo  ($row['C_ID'] == $sortby) ? 'selected' : '' ?> ><?php echo  $row['C_Name'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <select class="sort-listing-org-width" name="subcategory" id="advert-subcats" onchange="$('#org_listings_form').submit()">
                        <option value="">Select Sub Category</option>
                        <?PHP
                        if ($sortby > 0) {
                            $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '" . encode_strings($sortby, $db) . "' ORDER BY C_Name";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($row = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo  $row['C_ID'] ?>" <?php echo  ($row['C_ID'] == $subcategory) ? 'selected' : '' ?>><?php echo  $row['C_Name'] ?></option>
                                <?PHP
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <?PHP
            $sql = "SELECT BL_ID, BL_Listing_Title FROM tbl_Business_Listing 
                    LEFT JOIN tbl_Business_Listing_Category_Region on BL_ID = BLCR_BL_ID
                    LEFT JOIN tbl_Business_Listing_Category on BL_ID = BLC_BL_ID
                    LEFT JOIN tbl_Region_Category ON RC_C_ID = BLC_M_C_ID AND RC_R_ID= $CO_Community
                    WHERE BLCR_BLC_R_ID=$CO_Community AND hide_show_listing = 1 and RC_Status =0";
            if (isset($sortby) && $sortby != 0) {
                $sql .= " AND BLC_M_C_ID=$sortby";
            }
            if (isset($subcategory) && $subcategory != 0 && $sortby != 0) {
                $sql .= " AND BLC_C_ID=$subcategory";
            }
            $sql .= " GROUP BY BL_ID ORDER BY BL_Listing_Title";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $pages = new Paginate(mysql_num_rows($result), 30);
            $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                ?>
                <div class="data-content">
                    <div class="data-column spl-org-listngs "><input type="checkbox" class="org-check-align" name="organization_lisiting[]" value="<?php echo  $row['BL_ID'] ?>" <?php echo  (in_array($row['BL_ID'], $childbl_id)) ? 'checked' : '' ?> onclick="<?php echo  (in_array($row['BL_ID'], $childbl_id)) ? 'remove_listings(' . $row['BL_ID'] . ',' . $activeRegion['R_ID'] . ')' : 'add_listings(' . $row['BL_ID'] . ',' . $activeRegion['R_ID'] . ')' ?>" ><?php echo  $row['BL_Listing_Title'] ?></div>
                </div>
            <?PHP } ?>
            <?php
            if (isset($pages)) {
                echo $pages->paginate();
            }
            ?>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script>
    function remove_listings(bl_id,r_id)
    {
        $.ajax({
            type: "GET",
            url: "remove_org_listings.php",
            data: {
                bl_id: bl_id, 
                r_id: r_id
            }
        })
        .done(function(msg) {     
        });
    }
    function add_listings(bl_id,r_id)
    {
        $.ajax({
            type: "GET",
            url: "add_org_listings.php",
            data: {
                bl_id: bl_id, 
                r_id: r_id
            }
        })
        .done(function(msg) {
        });
    }
    function getSubOptions(cat_id)
    {
        $.ajax({
            type: "GET",
            url: "get_sub_cat.php",
            data: {
                cat_id: cat_id
            }
        })
        .done(function(msg) {
            $("#advert-subcats").empty();
            $("#advert-subcats").html(msg);
        });
    }
</script>