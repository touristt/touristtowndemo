<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('image-bank', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$id = $_REQUEST['id'];
if ($id > 0) {
    $sql = "SELECT IB.*, C.C_Name, IBS.IBS_Name, IBP.IBP_Name, IBPH.IBP_Name as photographer, IBC_Name
            FROM tbl_Image_Bank IB LEFT JOIN tbl_Category C ON IB.IB_Category = C.C_ID
            LEFT JOIN tbl_Image_Bank_Season IBS ON IB.IB_Season = IBS.IBS_ID
            LEFT JOIN tbl_Image_Bank_People IBP ON IB.IB_People = IBP.IBP_ID
            LEFT JOIN tbl_Image_Bank_Photographer IBPH ON IB.IB_Photographer = IBPH.IBP_ID
            LEFT JOIN tbl_Image_Bank_Campaign IBC ON IB.IB_Campaign = IBC.IBC_ID
            WHERE IB_ID = $id";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_array($result);

    $usage = "SELECT * FROM tbl_Image_Bank_Usage WHERE IBU_IB_ID = $id";
    $resUsg = mysql_query($usage, $db) or die("Invalid query: $usage -- " . mysql_error());
} else {
    header("Location: /admin/image-bank.php");
    exit();
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    require '../include/picUpload.inc.php';
    $val = Delete_Pic_Library($id);
    if ($val == 1) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Imagas','','Photo Detail',$id,'Delete','super admin');
        //go back to page which user was on
        $queryString = $_SERVER['QUERY_STRING'];
        if (strpos($queryString, '&id') !== false) {
            $pagepath = explode('&id=', $queryString);
            $pagepathno = '?' . $pagepath[0];
        } else {
            $pagepathno = '';
        }
        header("Location: /admin/image-bank.php" . $pagepathno);
        exit;
    } else {
        $_SESSION['delete_error'] = 1;
        header("Location: /admin/image-bank-photo-detail.php?id=$id");
        exit;
    }
}
//go back to page which user was on
$queryString = $_SERVER['QUERY_STRING'];
if (strpos($queryString, '&id') !== false) {
    $pagepath = explode('&id=', $queryString);
    $pagepathno = $pagepath[0];
} else {
    $pagepathno = '';   
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="image-nav">
        <?php require_once('../include/top-nav-image.php'); ?>
    </div>
    <div class="right center-image-bank">
        <div class="image_bank_top_buttons">
            <?php
            $sql_pervious = "SELECT IB_ID FROM tbl_Image_Bank where IB_ID > $id order by IB_ID ASC limit 1";
            $result_pervious = mysql_query($sql_pervious);
            $row_pervious = mysql_fetch_array($result_pervious);
            $count_pervious = mysql_num_rows($result_pervious);
            if ($count_pervious > 0) {
                $back = 'back.png';
            ?>
            <div class="image_bank_back_button">
                    <a href="image-bank-photo-detail.php?<?php echo $pagepathno; ?>&id=<?php echo $row_pervious['IB_ID'] ?>">
                        <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $back ?>">
                </a>
            </div>
            <?php
            }
            $sql_next = "SELECT IB_ID FROM tbl_Image_Bank where IB_ID < $id order by IB_ID DESC limit 1";
            $result_next = mysql_query($sql_next);
            $row_next = mysql_fetch_array($result_next);
            $count_next = mysql_num_rows($result_next);
            if ($count_next > 0) {
                $next = 'next.png';
            ?>
            <div class="image_bank_next_button">
                    <a href="image-bank-photo-detail.php?<?php echo $pagepathno; ?>&id=<?php echo $row_next['IB_ID'] ?>">
                        <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $next ?>">
                </a>
            </div>
            <?php
            }
            ?>
        </div>
        <div class="image-bank-left">
            <div class="image-bank-photo">
                <a class="various" href="http://<?php echo DOMAIN . IMG_BANK_REL . $row['IB_Path'] ?>">
                    <img src="http://<?php echo DOMAIN . IMG_BANK_REL . $row['IB_Detail_Path'] ?>">
                </a>
            </div>
            <div class="photo-detail">
                <div class="photo-detail-container">
                    <div class="download-photo">
                      <a href="http://<?php echo DOMAIN . IMG_BANK_REL . $row['IB_Path'] ?>" download="<?php echo $row['IB_ID']; ?>">Download Image</a> | 
                      <a href="add-image-bank.php?ib_id=<?php echo $row['IB_ID']; ?>">Edit Image</a> | 
                      <a href="image-bank-photo-detail.php?<?php echo $pagepathno ?>&id=<?php echo $row['IB_ID']; ?>&op=del" onclick="return confirm('Are you sure you want to delete!');">Delete Image</a>
                    </div>
                </div>
            </div>
            <?php if ($row['IB_Keyword'] != "") { ?>
                <div class="keywords">
                    <label>Keywords</label>
                    <div class="keyword">
                        <?php
                        $keywords = explode(',', $row['IB_Keyword']);
                        foreach ($keywords as $keyword) {
                            echo '<a href="image-bank.php?search_image=' . $keyword . '">' . $keyword . '</a>';
                        }
                        ?>
                    </div>
                </div>
            <?php } ?>
            
        </div>
        <div class="image-bank-right">
            <h2><?php echo $row['IB_Name'] ?></h2>
            <div class="download-photo">
                <a href="http://<?php echo DOMAIN . IMG_BANK_REL . $row['IB_Path'] ?>" download="<?php echo $row['IB_ID']; ?>">Download this photo</a>
            </div>
            <div class="photo-details">
                <div class="photo-labels">ID Number: </div><div class="photo-data"><?php echo $row['IB_ID']; ?></div>
                <div class="photo-labels">Uploaded</div><div class="photo-data"><?php echo date('F d, Y', strtotime($row['IB_Date'])); ?></div>
                <?php
                $ImageOwner = "";
                if ($row['IB_Owner'] != 0) {
                    $region = "SELECT PO_Name FROM tbl_Photographer_Owner WHERE PO_ID = '" . $row['IB_Owner'] . "'";
                    $resRegion = mysql_query($region, $db) or die("Invalid query: $region -- " . mysql_error());
                    $regionRow = mysql_fetch_assoc($resRegion);
                    $ImageOwner = $regionRow['PO_Name'];
                }
                ?>
                <div class="photo-labels">Image Owner</div><div class="photo-data"><?php echo $ImageOwner; ?></div>
                <div class="photo-labels">Community</div><div class="photo-data">
                    <?php
                    if ($row['IB_Region'] != "") {
                        $community_id = "SELECT R_Name FROM tbl_Region WHERE R_ID IN (" . $row['IB_Region'] . ")";
                        $result = mysql_query($community_id);
                        $total = mysql_num_rows($result);
                        $counter = 1;
                        while ($rowRegion = mysql_fetch_assoc($result)) {
                            if ($total != $counter) {
                                echo $rowRegion['R_Name'] . ", ";
                            } else {
                                echo $rowRegion['R_Name'];
                            }
                            $counter++;
                        }
                    }
                    ?>
                </div>
                <div class="photo-labels">Season photo was taken</div><div class="photo-data active">
                    <?php
                    if ($row['IB_Season'] == "-1") {
                        echo 'Not Applicable';
                    } if ($row['IB_Season'] == "0") {
                        echo 'Not Selected';
                    } else {
                        echo $row['IBS_Name'];
                    }
                    ?>
                </div>
                <div class="photo-labels">Category</div><div class="photo-data"><?php echo ($row['IB_Category'] != "0") ? $row['C_Name'] : "Not Selected"; ?></div>
                <div class="photo-labels">Listings</div><div class="photo-data">
                    <?php
                    if ($row['IB_Listings'] != "") {
                        $listings_id = "SELECT BL_ID, BL_Listing_Title from tbl_Business_Listing WHERE BL_ID IN (" . $row['IB_Listings'] . ")";
                        $result_listing = mysql_query($listings_id);
                        $total_listings = mysql_num_rows($result_listing);
                        $counter_listings = 1;
                        while ($rowlistings = mysql_fetch_assoc($result_listing)) {
                            if ($total_listings != $counter_listings) {
                                echo $rowlistings['BL_Listing_Title'] . ", ";
                            } else {
                                echo $rowlistings['BL_Listing_Title'];
                            }
                            $counter_listings++;
                        }
                    }
                    ?>
                </div>
                <div class="photo-labels">Photographer</div><div class="photo-data"><?php echo $row['photographer']; ?></div>
                <div class="photo-labels">Are people in the photo?</div><div class="photo-data"><?php echo ($row['IB_People'] != 0) ? $row['IBP_Name'] : "Not Selected"; ?></div>
                <div class="photo-labels">Is this a campaign image?</div><div class="photo-data"><?php echo ($row['IB_Campaign'] != 0) ? $row['IBC_Name'] : 'Not Selected'; ?></div>
                <div class="photo-labels">Location</div><div class="photo-data"><?php echo ($row['IB_Location'] != "") ? $row['IB_Location'] : "Not Selected"; ?></div>
                <div class="photo-labels">Dimension</div><div class="photo-data"><?php echo $row['IB_Dimension']; ?></div>
            </div>
            
        </div>
    </div>
</div>
<div id="autocomplete_data" style="display: none;"><?php include '../include/autocomplete.php' ?></div>

<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
$get_Url_data = str_replace("/admin/image-bank-photo-detail.php",'' , $_SERVER["REQUEST_URI"]);
?>
<script> 
    // managage back button click (and backspace)
var count = 0; // needed for safari
window.onload = function () { 
    if (typeof history.pushState === "function") { 
        history.pushState("back", null, null);          
        window.onpopstate = function () { 
            history.pushState('back', null, null);              
            if(count == 2){window.location = "image-bank.php<?php echo $get_Url_data ?>";}
         }; 
     }
 }  
setTimeout(function(){count = 2;},200)

    $(function () {
        $("#accordion")
        .accordion({
            collapsible: true,
            heightStyle: 'content',
            active: false
        });
    });
    $(function () {
        //autocomplete for lisitngs
        var search = $('#autocomplete_data').text();
        var searched_list = "";
        var json_data = [];
        if (search != "") {
            searched_list = search.split("%");
            for (var i = 0; i < searched_list.length - 1; i++) {
                json_data.push({"name": searched_list[i]});
            }
        }        
        $("#autocomplete").tokenInput(json_data, {
            onResult: function (item) {
                if ($.isEmptyObject(item)) {
                    return [{id: '0', name: $("tester").text()}]
                } else {
                    item.unshift({"name": $("tester").text()});
                    var lookup = {};
                    var result = [];

                    for (var temp, i = 0; temp = item[i++]; ) {
                        var name = temp.name;

                        if (!(name in lookup)) {
                            lookup[name] = 1;
                            result.push({"name": name});
                        }
                    }
                    return result;
                }

            },
            onAdd: function (item) {
                var value = $('#keywords-searched').val();
                if (value != '') {
                    $('#keywords-searched').val(value + "," + item.name);
                    $('#auto_complete_form_submit').submit();
                } else {
                    $('#keywords-searched').val(item.name);
                    $('#auto_complete_form_submit').submit();
                }
            },
            resultsLimit: 10
        }
    )
    });
    $(document).ready(function () {
        $(".various").fancybox({
//            maxWidth: 1100,
//            maxHeight: 600,
//            fitToView: false,
//            width: '90%',
//            height: '90%',
//            autoSize: false,
//            closeClick: false,
//            openEffect: 'none',
//            closeEffect: 'none'
        });
    });
</script>