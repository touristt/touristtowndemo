<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('division-billing', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">

    <div class="title-link">
        <div class="title">Manage Division Billing - Invoices</div>
    </div>

    <div class="left">
        <?php require_once('../include/nav-home-sub.php'); ?>
    </div>

    <?php
    $division_billings = array();
    $division_billings_refund = array();
    $all_division_billings = array();
    $sql = "SELECT DB_ID, DB_Customer_Name, DB_Date, DB_Subtotal, DB_Tax, DB_Total, DB_Refund_Deleted_Date, DB_Card_Type, BL_ID, BL_Listing_Title,
            DB_First_Name, DB_Last_Name, BL_ID, BL_Listing_Title FROM tbl_Division_Billing LEFT JOIN tbl_Business_Listing ON DB_BL_ID = BL_ID 
            WHERE DB_Deleted = 0 ORDER BY DB_ID DESC";
    $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $i = 0;
    while ($row_db = mysql_fetch_assoc($resultTMP)) {
        $division_billings[$i] = $row_db;
        $division_billings[$i]['date'] = $row_db['DB_Date'];
        $i++;
    }
    $sql = "SELECT DB_ID, DB_Customer_Name, DB_Date, DB_Subtotal, DB_Tax, DB_Total, DB_Refund_Deleted, DB_Refund_Deleted_Date, DB_Card_Type, BL_ID, BL_Listing_Title,
            DB_First_Name, DB_Last_Name, BL_ID, BL_Listing_Title FROM tbl_Division_Billing LEFT JOIN tbl_Business_Listing ON DB_BL_ID = BL_ID 
            WHERE DB_Deleted = 0 AND DB_Refund_Deleted = 1 ORDER BY DB_ID DESC";
    $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $i = 0;
    while ($row_db = mysql_fetch_assoc($resultTMP)) {
        $division_billings_refund[$i] = $row_db;
        $division_billings_refund[$i]['date'] = $row_db['DB_Refund_Deleted_Date'];
        $i++;
    }
    $all_division_billings = array_merge($division_billings, $division_billings_refund);

    function sortDivision($a, $b) {
        return strtotime($b['date']) - strtotime($a['date']);
    }

    usort($all_division_billings, "sortDivision");
//    echo '<pre/>';print_r($all_division_billings);exit;
    ?>
    <div class="right">
        <div class="data-header"> 
            <div class="data-column advert-billing">Invoice #</div>
            <div class="data-column advert-billing2">&nbsp;</div>
            <div class="data-column advert-billing1">Date</div>
            <div class="data-column advert-billing2">Sub Total</div>
            <div class="data-column advert-billing2">Tax</div>
            <div class="data-column advert-billing2">Total</div>
        </div>  
        <?PHP
        $gtotal = 0;
        $tax = 0;
        $subTotal = 0;
        $discount = 0;
        $refund_subtotal = $refund_tax = $refund_total = 0;
        foreach ($all_division_billings as $row) {
            //show refunded columns in red
            $refund_class = "";
            if (isset($row['DB_Refund_Deleted']) && $row['DB_Refund_Deleted'] == 1) {
                $date_db = date("Y-m-d", strtotime($row['DB_Refund_Deleted_Date']));
                $refund_class = " refund";
                $refund_subtotal += $row['DB_Subtotal'];
                $refund_tax += $row['DB_Tax'];
                $refund_total += $row['DB_Total'];
            } else {
                $date_db = date("Y-m-d", strtotime($row['DB_Date']));
                $gtotal += $row['DB_Total'];
                $tax += $row['DB_Tax'];
                $subTotal += $row['DB_Subtotal'];
                $discount += $row['DB_Discount'];
            }
            ?>                                  
            <div class="data-content<?php echo $refund_class; ?>">
                <div class="data-column advert-billing">
                    <a class="support-nav-b" href="division-billing-invoice.php?db_id=<?php echo $row['DB_ID']; ?>">#
                        <?php
                        if ($row['BL_ID'] > 0) {
                            echo $row['DB_ID'] . " - " . $row['BL_Listing_Title'];
                        } else {
                            echo $row['DB_ID'] . " -  " . $row['DB_Customer_Name'];
                        }
                        ?>
                    </a>
                </div>
                <div class="data-column advert-billing2">&nbsp;</div>
                <div class="data-column advert-billing1"><?php echo $date_db ?></div>
                <div class="data-column advert-billing2"><?php echo ($refund_class == '') ? "$" .number_format($row['DB_Subtotal'], 2) : "($" . number_format($row['DB_Subtotal'], 2) . ")" ?></div>
                <div class="data-column advert-billing2"><?php echo ($refund_class == '') ? "$" .number_format($row['DB_Tax'], 2) : "($" . number_format($row['DB_Tax'], 2) . ")" ?></div>
                <div class="data-column advert-billing2"><?php echo ($refund_class == '') ? "$" .number_format($row['DB_Total'], 2) : "($" . number_format($row['DB_Total'], 2) . ")" ?></div>
            </div> 
            <?PHP
        }
        ?>
        <div class="data-content">  
            <div class="data-column advert-billing">Total</div>
            <div class="data-column advert-billing2">&nbsp;</div>
            <div class="data-column advert-billing1">&nbsp;</div>
            <div class="data-column advert-billing2">$<?php echo number_format($subTotal, 2) ?></div>
            <div class="data-column advert-billing2">$<?php echo number_format($tax, 2) ?></div>
            <div class="data-column advert-billing2">$<?php echo number_format($gtotal, 2) ?></div>
        </div>
        <div class="data-content refund-color">  
            <div class="data-column advert-billing">Refunds</div>
            <div class="data-column advert-billing2">&nbsp;</div>
            <div class="data-column advert-billing1">&nbsp;</div>
            <div class="data-column advert-billing2">($<?php echo number_format($refund_subtotal, 2) ?>)</div>
            <div class="data-column advert-billing2">($<?php echo number_format($refund_tax, 2) ?>)</div>
            <div class="data-column advert-billing2">($<?php echo number_format($refund_total, 2) ?>)</div>
        </div>
        <div class="data-header">
            <div class="data-column advert-billing">Customer Total</div>
            <div class="data-column advert-billing2">&nbsp;</div>
            <div class="data-column advert-billing1">&nbsp;</div>
            <div class="data-column advert-billing2">$<?php echo number_format(($subTotal - $refund_subtotal), 2) ?></div>
            <div class="data-column advert-billing2">$<?php echo number_format(($tax - $refund_tax), 2) ?></div>
            <div class="data-column advert-billing2">$<?php echo number_format(($gtotal - $refund_total), 2) ?></div>
        </div>
        <div class="data-content">                           
            <div class="data-column advert-billing">&nbsp;</div>
            <div class="data-column advert-billing2">&nbsp;</div>
            <div class="data-column advert-billing1">&nbsp;</div>
            <div class="data-column advert-billing2">&nbsp;</div>
            <div class="data-column advert-billing2">&nbsp;</div>
            <div class="data-column advert-billing2">&nbsp;</div>
        </div>
        <div class="data-content">  
            <div class="data-column advert-billing">Discount</div>
            <div class="data-column advert-billing2">&nbsp;</div>
            <div class="data-column advert-billing1">&nbsp;</div>
            <div class="data-column advert-billing2">&nbsp;</div>
            <div class="data-column advert-billing2">&nbsp;</div>
            <div class="data-column advert-billing2">$<?php echo number_format($discount, 2) ?></div>
        </div>
    </div>
</div>

<?PHP
require_once '../include/admin/footer.php';
?>