<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if (isset($_REQUEST['rid']) && $_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
    $sql = "SELECT * FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
    $sql = "SELECT RM_Child FROM tbl_Region_Multiple WHERE RM_Parent = '" . encode_strings($regionID, $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($row = mysql_fetch_assoc($result)) {
        $childRegion[] = $row['RM_Child'];
    }
} else {
    $regionID = 0;
}

$regionType = (isset($_REQUEST['type']) && $_REQUEST['type'] != '') ? $_REQUEST['type'] : $activeRegion['R_Type'];

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $sql = "tbl_Region SET 
            R_Name = '" . encode_strings($_REQUEST['name'], $db) . "',
            R_Type  = '" . encode_strings($regionType, $db) . "'";
    if ($regionID > 0) {
        $sql = "UPDATE " . $sql . " WHERE R_ID = '" . encode_strings($regionID, $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        // TRACK DATA ENTRY
        Track_Data_Entry('Communities', '', 'Manage Website', $regionID, 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $regionID = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Communities', '', 'Manage Website', $regionID, 'Add', 'super admin');
    }
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: community.php?rid=" . $regionID);
    exit();
} elseif (isset($_GET['op']) && $_GET['op'] == 'del') {
    header("Location: coummunity.php?rid=" . $regionID);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo isset($activeRegion['R_Name']) ? $activeRegion['R_Name'] : '' ?> - Manage Website</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-community.php'; ?>
    </div>
    <div class="right">
        <form action="/admin/community.php" method="post" enctype="multipart/form-data" name="form1" id="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <input type="hidden" name="type" value="<?php echo $regionType ?>">
            <div class="content-header">Website Profile</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Website Name</label>
                <div class="form-data">
                    <input name="name" type="text" id="textfield2" value="<?php echo (isset($activeRegion['R_Name']) ? $activeRegion['R_Name'] : '') ?>" size="50" required/> 
                </div>
            </div>
            
            <div class="form-inside-div  width-data-content border-none"> 
                <div class="button"><input type="submit" name="button2" id="button22" value="Submit" />
                </div>
            </div>
        </form>
    </div>
    <div id="dialog-message"></div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>

