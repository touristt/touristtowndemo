<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['id'] > 0) {
    $sql = "SELECT R_ID, R_Name, R_Type, RI_ID, RI_Name, RI_Photo, RI_R_ID, RI_Link, RI_Order FROM tbl_Region_Icon
            LEFT JOIN tbl_Region ON RI_R_ID = R_ID 
            WHERE RI_ID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
} elseif ($_REQUEST['rid'] > 0) {
    $sql = "SELECT * FROM tbl_Region  
			WHERE R_ID = '" . encode_strings($_REQUEST['rid'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db)
            or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
}
$regionID = $activeRegion['R_ID'];

if ($_POST['op'] == 'save') {

    $sql = "tbl_Region_Icon SET 
            RI_Name = '" . encode_strings($_REQUEST['name'], $db) . "', 
            RI_Link = '" . encode_strings($_REQUEST['link'], $db) . "'";

    require '../include/picUpload.inc.php';
    if ($_POST['image_bank_pic'] == "") {
        // last @param 8 = Region Icon Images
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 8, 'Region', $regionID);
        if (is_array($pic)) {
            $sql .= ", RI_Photo = '" . encode_strings($pic['0']['0'], $db) . "'";
        }
        $pic_id = $pic['1'];
    } else {
        $pic_id = $_POST['image_bank_pic'];
        // last @param 8 = Region Icon Images
        $pic_response = Upload_Pic_Library($pic_id, 8);
        if ($pic_response) {
            $sql .= ", RI_Photo = '" . encode_strings($pic_response['0'], $db) . "'";
        }
    }
    if ($_REQUEST['id'] > 0) {
        $sql = "UPDATE " . $sql . " WHERE RI_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
        $result = mysql_query($sql, $db);
        $id = $_REQUEST['id'];
    } else {
        $sql = "INSERT " . $sql . ", RI_R_ID = '" . encode_strings($regionID, $db) . "'";
        $sqlMax = "SELECT MAX(RI_Order) FROM tbl_Region_Icon WHERE RI_R_ID = '" . encode_strings($regionID, $db) . "'";
        $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
        $rowMax = mysql_fetch_row($resultMax);
        $sql .= ", RI_Order = '" . ($rowMax[0] + 1) . "'";
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
    }

    if ($result) {
        $_SESSION['success'] = 1;
        if ($pic_id > 0) {
            //Image usage from image bank.
            imageBankUsage($pic_id, 'IBU_R_ID', $regionID, 'IBU_Region_Icon', $id);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: region.php?rid=" . $regionID . "#homepagelinks");
    exit();
} elseif ($_GET['op'] == 'del') {
    $id = $activeRegion['RI_ID'];
    $sql = "UPDATE tbl_Region_Icon SET RI_Order = RI_Order - 1 WHERE RI_R_ID = '" . $activeRegion['RI_R_ID'] . "' && RI_Order > " . $activeRegion['RI_Order'];
    mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());

    $sql = "DELETE tbl_Region_Icon
            FROM tbl_Region_Icon  
            WHERE RI_ID = '" . $activeRegion['RI_ID'] . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        //Image usage from image bank.
        imageBankUsageDelete('IBU_R_ID', $regionID, 'IBU_Region_Icon', $id);
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: region.php?rid=" . $activeRegion['RI_R_ID'] . "#homepagelinks");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo  $activeRegion['R_Name'] ?></div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <form id="regionForm" action="/admin/region-icons.php" method="post" enctype="multipart/form-data" name="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="id" value="<?php echo  $activeRegion['RI_ID'] ?>">
            <input type="hidden" name="rid" value="<?php echo  $regionID ?>">
            <input type="hidden" name="image_bank" class="image_bank" id="image_bank0" value="">
            <div class="content-header">Region Icon</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Name</label>
                <div class="form-data">
                    <input name="name" type="text" id="textfield2" value="<?php echo  $activeRegion['RI_Name'] ?>" size="50" required/> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Photo</label>
                <div class="form-data div_image_library cat-region-width">
                    <div class="cropit-image-preview aboutus-photo-perview">
                        <img class="preview-img preview-img-script8" style="display: none;" src="">    
                        <?php if ($activeRegion['RI_Photo'] != '') { ?>
                            <img class="existing-img existing_imgs8" src="<?php echo  IMG_LOC_REL . $activeRegion['RI_Photo'] ?>" >
                        <?php } ?>
                    </div>
                    <span class="daily_browse" onclick="choose_file(8);">Select Photo</span>
                    <div class="dialog-close dialog8 dialog-bank" style="display: none;">
                        <label for="photo8" class="daily_browse">Upload File</label>
                        <a onclick="show_image_library(8)">Add from Library</a>
                    </div>
                    <input type="hidden" name="image_bank_pic" id="image_bank8" value="">
                    <input type="file" onchange="show_file_name(8, this)" name="pic[]" id="photo8" style="display: none;">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Link</label>
                <div class="form-data">
                    <input name="link" type="text"  value="<?php echo  $activeRegion['RI_Link'] ?>" size="50">
                </div>
            </div>
            <div class="form-inside-div  width-data-content border-none"> 
                <div class="button"><input type="submit" name="button2" id="button22" value="Submit" />
                </div>
            </div>
        </form>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>