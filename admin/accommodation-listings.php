<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';
if (!in_array('manage-accommodation', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$Join = '';
$Where = '';
$regionList = '';
$catRegions = '';
$regionLimit = array();
if (($_REQUEST['sortregion'] != '' && $_REQUEST['sortregion'] != '-1') || (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '')) {
    if (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
        $limits = explode(',', $_SESSION['USER_LIMIT']);
    } else {
        $limits = explode(',', $_REQUEST['sortregion']);
    }
    foreach ($limits as $limit) {
        $sql = "SELECT R_ID, R_Parent FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $region = mysql_fetch_assoc($result);
        if (isset($region['R_Type']) && $region['R_Type'] == 1) {
            $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($region['R_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                if ($row['R_ID'] != '' && !in_array($row['R_ID'], $regionLimit)) {
                    $regionLimit[] = $row['R_ID'];
                }
            }
        } else {
            if ($region['R_ID'] != '' && !in_array($region['R_ID'], $regionLimit)) {
                $regionLimit[] = $region['R_ID'];
            }
        }
    }
    $regionLimitCommaSeparated = rtrim(implode(',', $regionLimit), ',');
    $Join = " INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID";
    $Where = " AND BLCR_BLC_R_ID IN (" . encode_strings($regionLimitCommaSeparated, $db) . ")";
    $catRegions = " AND RC_R_ID IN (" . encode_strings($regionLimitCommaSeparated, $db) . ")";
    if (in_array('bgadmin', $_SESSION['USER_ROLES'])) {
        $Where .= " AND BL_Listing_Type IN (1, 4)";
    }
}

if ($_REQUEST['subcategory']) {
    $subcat = $_REQUEST['subcategory'];
}
require_once '../include/admin/header.php';
include '../include/accommodation_header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Accommodation Listings</div>
        <div class="link">
            <form name="form1" method="GET" id="acc_listings" action="">
                <?php
                if (!isset($_SESSION['USER_LIMIT']) || $_SESSION['USER_LIMIT'] == '') {
                    $sortregion = isset($_REQUEST['sortregion']) ? $_REQUEST['sortregion'] : '';
                    ?>
                    <label>
                        <span class="addlisting">Region:</span>
                        <select name="sortregion" id="sortregion" onChange="$('#acc_listings').submit()">
                            <option value="">Select Region</option>
                            <option value="1" <?php echo ($sortregion == '1') ? 'selected="selected"' : ''; ?>>Saugeen Shores</option>
                            <option value="3" <?php echo ($sortregion == '3') ? 'selected="selected"' : ''; ?>>Port Elgin</option>
                            <option value="4" <?php echo ($sortregion == '4') ? 'selected="selected"' : ''; ?>>Southampton</option>
                            <option value="6" <?php echo ($sortregion == '6') ? 'selected="selected"' : ''; ?>>Wiarton</option>
                            <option value="8" <?php echo ($sortregion == "8") ? 'selected="selected"' : ''; ?>>Lion's Head</option>
                            <option value="-1">All</option>
                        </select>
                    </label>
                <?php } ?>
                <select class="sort-listing-org-width" name="subcategory" id="advert-subcats" onchange="$('#acc_listings').submit()">
                    <option value="">Sort by</option>
                    <?PHP
                    $sql = "SELECT C_ID, C_Name FROM tbl_Category
                            LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID
                            WHERE C_Parent = '4' $catRegions GROUP BY C_ID ORDER BY C_Name";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    while ($row = mysql_fetch_assoc($result)) {
                        ?>
                        <option value="<?php echo $row['C_ID'] ?>" <?php echo ($row['C_ID'] == $subcat) ? 'selected' : '' ?>><?php echo $row['C_Name'] ?></option>
                        <?PHP
                    }
                    ?>
                </select>
            </form>
        </div>
    </div>
    <div class="right full-width">

        <div class="content-sub-header link-header region-header-padding">
            <div class="data-column spl-name padding-none">Select Listings for Accommodation
            </div>
        </div>
        <?PHP
        if (isset($subcat) && $subcat != 0) {
            $Where .= " AND BLC_C_ID = $subcat";
        }
        $sql = "SELECT BL_ID, BL_Listing_Title, BL_Show_In_Accommodation FROM 
                tbl_Business_Listing LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID $Join WHERE BL_C_ID = 4 $Where GROUP BY BL_ID ORDER BY BL_Listing_Title";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $pages = new Paginate(mysql_num_rows($result), 100);
        $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
        $i = 1;
        while ($row = mysql_fetch_assoc($result)) {
            ?>
            <div class="data-content">
                <div class="data-column spl-org-listngs ">
                    <input type="checkbox" class="org-check-align" id="check-<?php echo $i; ?>" value="<?php echo $row['BL_Show_In_Accommodation'] ?>" <?php echo ($row['BL_Show_In_Accommodation'] == 1) ? 'checked' : '' ?> onclick="show_in_accommodation(<?php echo $row['BL_ID'] . ", " . $row['BL_Show_In_Accommodation'] . ", " . $i ?>)" ><?php echo $row['BL_Listing_Title'] ?></div>
            </div>
            <?PHP
            $i++;
        }
        ?>
        <?php
        if (isset($pages)) {
            echo $pages->paginate();
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script>
    function show_in_accommodation(bl_id, val, counter)
    {
        var value;
        if (val == 1) {
            value = 0;
        } else {
            value = 1;
        }
        $.ajax({
            type: "GET",
            url: "show_in_accommodation.php",
            data: {
                bl_id: bl_id,
                val: value
            }
        })
                .done(function (msg) {
                    $('#check-' + counter).val(value);
                    $('#check-' + counter).attr("onclick", "show_in_accommodation(" + bl_id + ", " + value + ", " + counter + ")");
                    if (value == 0) {
                        swal("Listing Removed", "Listing has been removed from Accommodation successfully!", "success");
                    } else {
                        swal("Listing Added", "Listing has been added into Accommodation successfully!", "success");
                    }
                });
    }
</script>