<?php
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
$cat_id = '';
if ($_GET['cat_id']) {
    $cat_id = $_GET['cat_id'];
}
?>
<select name="subcategory" class="adv-type-options" id="advert-subcat" onchange="$('#org_listings_form').submit()">
    <option value="">Select Sub Category</option>
    <?PHP
    if ($cat_id > 0) {
        $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '" . encode_strings($cat_id, $db) . "' ORDER BY C_Name";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            ?>
            <option value="<?php echo $row['C_ID'] ?>"><?php echo $row['C_Name'] ?></option>
            <?PHP
        }
    }
    ?>
</select>
