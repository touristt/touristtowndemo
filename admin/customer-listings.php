<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('business-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
}

$regionWhere = '';

if ($_REQUEST['id'] > 0) {
    $sql = "SELECT B_ID FROM tbl_Business WHERE B_ID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowBUS = mysql_fetch_assoc($result);
    $BID = $rowBUS['B_ID'];
}

if ($_SESSION['USER_LIMIT'] != '') {
    $limits = explode(',', $_SESSION['USER_LIMIT']);
    $regionLimit = array();
    foreach ($limits as $limit) {
        $sql = "SELECT R_ID, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $region = mysql_fetch_assoc($result);
        if ($region['R_Type'] == 1) {
            $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($region['R_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                if ($row['R_ID'] != '' && !in_array($row['R_ID'], $regionLimit)) {
                    $regionLimit[] = $row['R_ID'];
                }
            }
        } else {
            if ($region['R_ID'] != '' && !in_array($region['R_ID'], $regionLimit)) {
                $regionLimit[] = $region['R_ID'];
            }
        }
    }
    $regionLimitCommaSeparated = rtrim(implode(',', $regionLimit), ',');
    $regionWhere = " AND BLCR_BLC_R_ID IN (" . encode_strings($regionLimitCommaSeparated, $db) . ")";
}


if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $BL_ID = $_POST['bl_id'];
    $email = $_POST['email'];
    $sql = "SELECT B_ID FROM tbl_Business WHERE B_Email = '" . encode_strings($email, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $count = mysql_num_rows($result);
    $row = mysql_fetch_assoc($result);
    if ($count > 0) {
        $sql = "UPDATE tbl_Business_Listing SET BL_B_ID = '" . $row['B_ID'] . "' WHERE BL_ID = '$BL_ID'";
        $result = mysql_query($sql, $db);
        $sql = "UPDATE tbl_Advertisement SET A_B_ID = '" . $row['B_ID'] . "' WHERE A_BL_ID = '$BL_ID'";
        $result = mysql_query($sql, $db);
        $_SESSION['listing_transferred_success'] = 1;
    } else {
        $_SESSION['listing_transferred_error'] = 1;
    }
    header("Location: /admin/customer-listings.php?id=" . $BID);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Home</div>
        <div class="link">
            <a href="customer-listing-regions.php?bid=<?php echo $rowBUS['B_ID'] ?>&op=new">+Add Listing</a>
        </div>
    </div>
    <div class="left">
        <?PHP
        require '../include/nav-businessprofile.php';
        ?>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column mp-listing-title padding-none">Listing Title</div>
            <div class="data-column mp-listing-other-2 padding-none">Active</div>
            <div class="data-column mp-listing-cat padding-none">Category</div>
            <div class="data-column mp-listing-other padding-none">Class</div>
            <div class="data-column mp-listing-other padding-none">Renewal</div>
            <div class="data-column mp-listing-preview padding-none">Preview</div>
            <div class="data-column mp-listing-preview padding-none">Transfer</div>
            <div class="data-column mp-listing-other-2 padding-none">Edit</div>
            <div class="data-column mp-listing-other-2 padding-none">Delete</div>
        </div>
        <?PHP
        $sql = "SELECT BL_Listing_Title, BL_Name_SEO,  LT_Name, BL_Listing_Type, BL_Points, hide_show_listing, BL_ID, R_Domain,
                DATEDIFF(BL_Renewal_Date, CURDATE()) as timeRemaining FROM tbl_Business_Listing                                 
                LEFT JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                LEFT JOIN tbl_Listing_Type ON BL_Listing_Type = LT_ID 
                LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
                WHERE BL_B_ID = '" . encode_strings($rowBUS['B_ID'], $db) . "' $regionWhere
                GROUP BY BL_ID";
        $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($resultTMP)) {
            $BL_ID = $row['BL_ID'];
            $preview = 'Preview';
            ?>
            <div class="data-content">
                <div class="data-column mp-listing-title">
                    <?php echo $row['BL_Listing_Title']; ?>
                </div>
                <?php
                if (isset($row['hide_show_listing']) && $row['hide_show_listing'] == 1) {
                    $checked = "checked";
                } else {
                    $checked = "";
                }
                ?>
                <div class="data-column mp-listing-other-2">
                    <input type="checkbox" name="display" id="display<?php echo $row['BL_ID']; ?>" value="<?php echo $row['hide_show_listing']; ?>" <?php echo $checked; ?> onclick=" return hide_show_listings(<?php echo $row['BL_ID']; ?>);">
                </div>
                <div class="data-column mp-listing-cat">
                    <?php
                    $sql_sub_categories = "SELECT C_Name FROM tbl_Business_Listing_Category LEFT JOIN tbl_Category ON C_ID = BLC_M_C_ID
                                           WHERE BLC_BL_ID = " . encode_strings($row['BL_ID'], $db) . " GROUP BY BLC_M_C_ID ";
                    $result_sub_categories = mysql_query($sql_sub_categories, $db) or die("Invalid query: $sql_sub_categories -- " . mysql_error());
                    while ($row_sub_categories = mysql_fetch_array($result_sub_categories)) {
                        ?>
                        <div>
                            <?php
                            echo $row_sub_categories['C_Name'];
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="data-column mp-listing-other">
                    <?php echo $row['LT_Name'] ?>
                </div>
                <div class="data-column mp-listing-other">
                    <?php echo $row['timeRemaining'] ? $row['timeRemaining'] : 0 ?> days
                </div>
                <div class="data-column mp-listing-preview">
                    <?php require('preview-link.php'); ?>
                </div>
                <div class="data-column mp-listing-preview"><a style="cursor:pointer;" onclick="transfer_listing(<?php echo $row['BL_ID'] ?>);">Transfer</a></div>
                <form action="" method="post" name="form" id="transfer-listing-form<?php echo $row['BL_ID'] ?>" style="display:none;">
                    <div style="float:left; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                        <input type="hidden" name="op" value="save">
                        <input type="hidden" name="id" value="<?php echo $BID ?>">
                        <input type="hidden" name="bl_id" value="<?php echo $row['BL_ID'] ?>">
                        <input type="email" name="email" style="width:420px !important;" size="50" required placeholder="Enter Email">
                    </div>
                    <div style="float:left;text-align: center; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                        <div class="form-data">
                            <input type="submit" name="button" value="Save Now"/>
                        </div>
                    </div> 
                </form>
                <div class="data-column mp-listing-other-2"><a href="customer-listing-overview.php?bl_id=<?php echo $row['BL_ID'] ?>">Edit</a></div>
                <div class="data-column mp-listing-other-2"><a onClick="return confirm('Are you sure this action can not be undone!');" href="customer-listing-overview.php?op=del&amp;bl_id=<?php echo $row['BL_ID'] ?>">Delete</a></div>
            </div>
            <?PHP
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script language="javascript">
    function hide_show_listings(business_listing_id) {
        var value = document.getElementById('display' + business_listing_id).checked;
        $.ajax({
            type: "POST",
            url: 'show_hide_listing.php',
            dataType: 'json',
            data: {
                BL_ID: business_listing_id,
                show_hide: value
            },
            success: function (response) {
                if (response == 0) {
                    swal("Listing Deactivated", "Listing has been deactivated successfully.", "success");
                } else if (response == 1) {
                    swal("Listing Activated", "Listing has been activated successfully.", "success");
                } else {
                    swal("Error", "Error saving data. Please try again.", "error");
                }
            }
        });
    }
</script>
