<?php

require_once '../include/config.inc.php';

if (isset($_REQUEST['export_stats'])) {
// Download the file
    $filename = "Customer_stats.csv";
    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename=' . $filename);
    header("Pragma: no-cache");
    header("Expires: 0");
    echo "\xEF\xBB\xBF";
// Fetch region
    if (isset($_POST['region_filter']) && $_POST['region_filter'] > 0) {
        $regionLimit_id = $_POST['region_filter'];
        $sql = "SELECT R_ID, R_Parent FROM tbl_Region WHERE R_ID = $regionLimit_id LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $region = mysql_fetch_assoc($result);
        if ($region['R_Parent'] == 0) {
            $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($region['R_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $first = true;
            $regionLimit .= "IN ('";
            while ($row = mysql_fetch_assoc($result)) {
                if ($first) {
                    $first = false;
                } else {
                    $regionLimit .= "','";
                }
                $regionLimit .= $row['R_ID'];
            }
            $regionLimit .= "')";
        } else {
            $regionLimit = " = " . $regionLimit_id;
        }
        $JOIN = 'INNER JOIN tbl_Business_Listing_Category_Region ON BL_ID = BLCR_BL_ID';
        $WHERE = 'AND BLCR_BLC_R_ID ' . $regionLimit;
    }
    // SELECTING ALL THE LISTINGS TYPES FROM DATABASE
    $sql1 = "SELECT LT_ID, LT_Name FROM tbl_Listing_Type";
    $result = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
    $output = '"Customers Statistics",';
    $output .= "\n";
    echo $output;
    while ($type = mysql_fetch_assoc($result)) {

        // SELECTING ACTIVE LISTINGS .W.R.T. LISTING TYPE 
        $query = "SELECT DISTINCT BL_ID FROM tbl_Business_Listing $JOIN WHERE BL_Listing_Type = '" . $type['LT_ID'] . "' AND hide_show_listing = 1 $WHERE GROUP BY BL_ID";
        $result1 = mysql_query($query, $db) or die("Invalid query: $query -- " . mysql_error());
        $count1 = mysql_num_rows($result1);

        // SELECTING INACTIVE LISTINGS W.R.T. LISTING TYPE
        $query = "SELECT DISTINCT BL_ID FROM tbl_Business_Listing $JOIN WHERE BL_Listing_Type = '" . $type['LT_ID'] . "' AND hide_show_listing = 0 $WHERE GROUP BY BL_ID";
        $result2 = mysql_query($query, $db) or die("Invalid query: $query -- " . mysql_error());
        $count2 = mysql_num_rows($result2);

        $output = $type['LT_Name'] . " Listings";
        $output .= "\n";
        echo $output;

        $output = $count1;
        $output .= "\n";
        echo 'Active: ' . $output;

        $output = $count2;
        $output .= "\n";
        echo 'Inactive:' . $output;

        $output = $count1 + $count2;
        $output .= "\n";
        echo 'Total: ' . $output;
    }
}
?>



