<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('superadmin', $_SESSION['USER_ROLES'])) {
    header("Location: /admin/");
    exit();
}
if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $sql = "tbl_Permission SET 
            P_ID = '" . encode_strings(str_replace(' ', '-', strtolower($_REQUEST['permission'])), $db) . "',
            P_Name = '" . encode_strings($_REQUEST['permission'], $db) . "' ,
            P_S_ID = '" . encode_strings($_REQUEST['section'], $db) . "' ";
    $sql = "INSERT " . $sql;
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Users', '', 'Manage Permissions', '', 'Add', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: permissions.php");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Permissions</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?php require_once('../include/nav-home-sub.php'); ?>
    </div>
    <div class="right">
        <div class="content-header">Permission Details</div>
        <form name="form1" method="post" action="">
            <input type="hidden" name="op" value="save">
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Permission Name</label>
                <div class="full-width-form-data">
                    <input name="permission" type="text" size="40" required/>
                </div>
                <?php
                $query = "SELECT S_ID, S_Name FROM tbl_Section";
                $result = mysql_query($query) or die("Invalid query: $result -- " . mysql_error());
                ?>
                <div class="full-width-form-data">
                    <select name="section" required="required">
                        <option value="">SELECT SECTION</option>
                        <?php
                        while ($row = mysql_fetch_assoc($result)) {
                            echo '<option value="' . $row['S_ID'] . '">' . $row['S_Name'] . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="button5" id="button52" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>