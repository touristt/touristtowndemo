<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/adminFunctions.inc.php';

if (!in_array('business-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Website, BL_Street, BL_Town, BL_Province, 
            BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, 
            BL_SEO_Keywords, BL_Search_Words, BL_ChamberMember, BL_Billing_Type FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
    // get regions
    $sql_region = " SELECT BLCR_BLC_R_ID, R_Parent FROM tbl_Business_Listing INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                    INNER JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID WHERE BLCR_BL_ID = '$BL_ID'";
    $res_region = mysql_query($sql_region);
    $regions = array();
    while ($r = mysql_fetch_assoc($res_region)) {
        if ($r['R_Parent'] != 0 && !in_array($r['R_Parent'], $regions)) {
            $regions[] = $r['R_Parent'];
        }
        if ($r['BLCR_BLC_R_ID'] != '' && !in_array($r['BLCR_BLC_R_ID'], $regions)) {
            $regions[] = $r['BLCR_BLC_R_ID'];
        }
    }
} else {
    header("Location:customers.php");
    exit();
}


if ($_POST['op'] == 'save') {
    $sql = "tbl_Business_Listing SET BL_ChamberMember = '" . encode_strings($_REQUEST['chamber'], $db) . "'";
    $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $res = mysql_query($sql, $db);

    if (($_REQUEST['chamber'] <> $rowListing['BL_ChamberMember'] && $rowListing['BL_ID'] > 0) ||
            ($_REQUEST['chamber'] && $rowListing['BL_ID'] < 1)) {

        require_once '../include/sendmail.inc.php';

        $sql = "SELECT BL_Billing_Type, BL_CoC_Dis_2 FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $row = mysql_fetch_assoc($result);
        if ($row['BL_Billing_Type'] == -1) {
            $multiplier = 12;
        } else {
            $multiplier = 1;
        }
        $sql_bl = "SELECT  LT_Cost FROM tbl_Listing_Type WHERE LT_ID = '4'"; // listing price
        $listing_price = mysql_result(mysql_query($sql_bl), 0);
        $sql = "SELECT COUNT(*) FROM  tbl_Business_Listing_Category WHERE BLC_BL_ID = " . $rowListing['BL_ID'];
        $totalSubCategories = mysql_result(mysql_query($sql), 0);
        $totalSubCategories--; //First subcategory is free, the additionals have price
        $sql_cat = "SELECT $totalSubCategories * ($multiplier * LS_Cost) as category_price FROM tbl_Listing_SubCategory"; // categories price
        $cat_price = mysql_result(mysql_query($sql_cat), 0);
        $sql_lf = "SELECT SUM($multiplier * F_Price) as feature_price FROM `tbl_BL_Feature` LEFT JOIN `tbl_Feature` ON BLF_F_ID = F_ID WHERE BLF_BL_ID = " . $rowListing['BL_ID']; // tools price
        $feature_price = mysql_result(mysql_query($sql_lf), 0);
        if ($_REQUEST['chamber'] != 0) {
            $sub = "Chamber Membership Addition";
            $discount = $row['BL_CoC_Dis_2'] + $listing_price;
            $sql_update = "UPDATE tbl_Business_Listing SET BL_Listing_Type = 4, BL_CoC_Dis_2 = '" . encode_strings($discount, $db) . "' WHERE BL_ID = " . encode_strings($BL_ID, $db);
        } else {
            $sub = "Chamber Membership Cancelled";
            if ($feature_price + $cat_price > 0) {
                if ($row['BL_CoC_Dis_2'] <= $listing_price) {
                    $discount = 0;
                    $sql_update = "UPDATE tbl_Business_Listing SET BL_CoC_Dis_2 = '$discount' WHERE BL_ID = " . encode_strings($BL_ID, $db);
                } else {
                    $discount = $row['BL_CoC_Dis_2'] - $listing_price;
                    $sql_update = "UPDATE tbl_Business_Listing SET BL_CoC_Dis_2 = '" . encode_strings($discount, $db) . "' WHERE BL_ID = " . encode_strings($BL_ID, $db);
                }
            } else {
                if ($row['BL_CoC_Dis_2'] <= $listing_price) {
                    $discount = 0;
                    $sql_update = "UPDATE tbl_Business_Listing SET BL_Listing_Type = 1, BL_CoC_Dis_2 = '$discount' WHERE BL_ID = " . encode_strings($BL_ID, $db);
                } else {
                    $discount = $row['BL_CoC_Dis_2'] - $listing_price;
                    $sql_update = "UPDATE tbl_Business_Listing SET BL_Listing_Type = 1, BL_CoC_Dis_2 = '" . encode_strings($discount, $db) . "' WHERE BL_ID = " . encode_strings($BL_ID, $db);
                }
            }
        }
        $res2 = mysql_query($sql_update);
        if ($res && $res2) {
            $_SESSION['success'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
        listing_billing($rowListing['BL_Billing_Type'], $BL_ID, $discount);
    }


    header("Location: /admin/customer-listing-buying-group.php?bl_id=" . $BL_ID);
    exit();
}

require_once '../include/admin/header.php';
?>

<div class="content-left">

    <?php require_once '../include/top-nav-listing.php'; ?>

    <div class="title-link">
        <div class="title">My Page - <?php echo $rowListing['BL_Listing_Title'] ?></div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div>
    </div>

    <div class="left">
        <?PHP
        require '../include/nav-mypage.php';
        ?>
    </div>

    <div class="right">
        <form name="form1" method="post" action="">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="op" value="save">

            <div class="content-header">Buying Group</div>

            <div class="form-inside-div">
                <label>Buying Group Member</label>
                <div class="form-data">
                    <select name="chamber" id="chamber">
                        <option value="0">Select One</option>
                        <option value="0" <?php echo !$rowListing['BL_ChamberMember'] && $rowListing['BL_ID'] > 0 ? "selected" : "" ?>>No</option>
                        <?php 
                            $sql = "SELECT U_ID, U_G_Name FROM tbl_User LEFT JOIN tbl_User_Role ON U_ID = UR_U_ID WHERE UR_R_ID = 'bgadmin' AND UR_Limit IN(" . implode(',', $regions) . ")";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while($bgadmin = mysql_fetch_assoc($result)){
                        ?>
                        <option value="<?php echo $bgadmin['U_ID'] ?>" <?php echo $rowListing['BL_ChamberMember'] == $bgadmin['U_ID'] ? "selected" : "" ?>><?php echo $bgadmin['U_G_Name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" name="button2" value="Save Now"/>
                </div>
            </div>
        </form>
    </div>
</div>

<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>