<?php
include '../include/config.inc.php';
include '../include/accommodation_functions.php';
$search_word = '';
if (isset($_REQUEST['search_word'])) {
    $search_word = $_REQUEST['search_word'];
}
require_once '../include/login.inc.php';
require_once '../include/admin/header.php';
include '../include/accommodation_header.php';
?>
<tr>
    <td align="center">
        <table  width="940" border="0" cellspacing="0" cellpadding="0" align="center" class="content">
            <?php if (isset($_SESSION['message'])) { ?>
                <tr>
                    <td class="message">
                        <?php
                        echo $_SESSION['message'];
                        unset($_SESSION['message']);
                        ?>
                    </td>
                </tr>
            <?php } ?>
            <tr><td>
                    <div class="title-row">
                        <div class="title-row-col1">Accommodators Listing</div>
                        <div class="title-row-col2">
                            <form action="search.php" method="GET">
                                <input type="text" class="top-search-txt" name="search_word" value="<?php echo $search_word; ?>" placeholder="Search"/>
                                <input type="submit" class="top-search-btn" name="submit-search" value="GO"/>
                            </form>
                        </div>
                    </div>
                    <div class="data-container search cottage-capacity">
                        <div class="acc_cap_header-rows  header-row2-color">
                            <div class="header-row2-col1">&nbsp;</div>
                            <div class="header-row2-col3">Region</div>
                            <div class="header-row2-col3">Category</div>
                        </div>
                        <?php
                        //loop here to fitch data
                        $return_data = get_search_result($search_word);
                        if ($return_data != '') {
                            $page_counter = 0;
                            while ($row = mysql_fetch_array($return_data)) {
                                $page_counter++;
                                $accom_id = $row['BL_ID'];
                                if ($row['BL_Override_Title'] == 0) {
                                    $accom_name = $row['B_Name'];
                                } elseif ($row['BL_Override_Title'] == 1) {
                                    $accom_name = $row['BL_Listing_Title'];
                                }
                                $cap_id = isset($row['id']) ? $row['id'] : 0;
                                ?>
                                <div class = "acc_cap_header-rows data-rows accordion-section">
                                    <form onsubmit="return accomm_cap_update(this)">
                                        <input type = "hidden" name = "accom_id" value="<?php echo $accom_id ?>" />                                            
                                        <div class = "header-row2-col1"><?php echo $accom_name ?></div>
                                        <div class = "header-row2-col3"><label><?php echo $row['R_Name']; ?></label></div>
                                        <div class = "header-row2-col3 last"><label><?php echo $row['C_Name']; ?></label></div>
                                    </form>
                                </div>

                                <?php
                            }
                        }
                        ?>
                    </div>
                    <ul id="pagin">
                        <?php
                        if ($page_counter > 10) {
                            echo '<li><span class="current" >1</span></li>';
                            for ($index = 1; $index < ceil($page_counter / 10); $index++) {
                                $page = $index + 1;
                                echo "<li><span href='#'>" . $page . "</span></li>";
                            }
                        }
                        ?>
                    </ul>
            </tr>
    </td>
</table>
</td>
</tr>
<?PHP
require_once '../include/admin/footer.php';
?>