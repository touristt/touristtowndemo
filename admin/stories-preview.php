<?php
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';
$preview_page = 1;
$preview_page_Region = 3;
$sql_category = "SELECT * FROM tbl_Story WHERE S_ID = '" . $_REQUEST['id'] . "' ";
$result_category = mysql_query($sql_category);
$activeStory = mysql_fetch_array($result_category);
require_once '../include/admin/header.php';
if (!in_array('manage-stories', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
$CP_IDS = $_REQUEST['id'];
$S_ID = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
$regions_for_analytics = array();
$childRegion = array();
$where = "";
$regionDeleteWhere = "";
$checked = "";
$community_class = ""; //for hiding community option when bruce county
//if county admin
if (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
    $regionDeleteWhere = 'AND SR_R_ID IN (' . $_SESSION['USER_LIMIT'] . ')';
    $storyCheck = mysql_query("SELECT S_ID FROM tbl_Story LEFT JOIN tbl_Story_Region ON S_ID = SR_S_ID WHERE SR_R_ID = '" . encode_strings($_SESSION['USER_LIMIT'], $db) . "'");
    $storyArray = array();
    while ($storyRow = mysql_fetch_array($storyCheck)) {
        $storyArray[] = $storyRow['S_ID'];
    }
    if ($S_ID > 0 && !in_array($S_ID, $storyArray)) {
        header("location:stories.php");
    }
    $where = ' AND R_ID = ' . $_SESSION['USER_LIMIT'];
    if ($S_ID == 0) {
        $checked = '1';
    }
    $community_class = "display: none;";
}
$sql = "SELECT * FROM tbl_Story WHERE S_ID = '" . encode_strings($S_ID, $db) . "'";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$rowStory = mysql_fetch_assoc($result);
$sql = "SELECT * FROM tbl_Story_Region WHERE SR_S_ID = '" . encode_strings($S_ID, $db) . "'";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
while ($rowRegions = mysql_fetch_assoc($result)) {
    $childRegion[] = $rowRegions['SR_R_ID'];
}
//print_r($childRegion);
$region_All = implode('',$childRegion);
$_SESSION['storyRegion'] =$region_All ;

$sqlSS = "SELECT * FROM tbl_Story_Season WHERE SS_S_ID = '" . encode_strings($S_ID, $db) . "'";
$resultSS = mysql_query($sqlSS, $db) or die("Invalid query: $sqlSS -- " . mysql_error());

$getStoryContent = "SELECT CP_ID, CP_S_ID, CP_Title, CP_Photo, CP_Description, CP_Video_Link, CP_Pdf, CP_Pdf_Title  FROM tbl_Content_Piece WHERE CP_S_ID = '" . encode_strings($activeStory['S_ID'], $db) . "' ORDER BY CP_Order ASC";
$resContent = mysql_query($getStoryContent, $db) or die("Invalid query: $getStoryContent -- " . mysql_error());

$activeContent = mysql_fetch_assoc($resContent);
$activeContentCount = mysql_num_rows($resContent);

/////// Story Add and Edit 
if (isset($_POST['button_story']) && $_POST['button_story'] !== '') {
    require_once '../include/image-bank-usage-function.php';
    $sql = "SELECT * FROM tbl_Story WHERE S_ID = '" . encode_strings($S_ID, $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowStory = mysql_fetch_assoc($result);
    $S_ID = $_POST['id'];
    $s_id = $_REQUEST['id'];
    $id = $S_ID; 
    $sql = "tbl_Story SET 
            S_Title = '" . encode_strings($_REQUEST['title'], $db) . "', 
            S_Category = '" . encode_strings($_REQUEST['category'], $db) . "', 
            S_Author = '" . encode_strings($_REQUEST['author'], $db) . "',
            S_Description = '" . encode_strings($_REQUEST['description'], $db) . "',
            S_SEO_Title = '" . encode_strings($_REQUEST['seoTitle'], $db) . "', 
            S_SEO_Keywords = '" . encode_strings($_REQUEST['seoKeywords'], $db) . "', 
            S_SEO_Description = '" . encode_strings($_REQUEST['seoDescription'], $db) . "'";

    $regions = '';
    $i = 1;
    $count = count($_REQUEST['childRegion']);
    foreach ($_REQUEST['childRegion'] as $child) {
        // SELECTING REGIONS
        if ($i != $count) {
            $regions .= $child . ',';
        } else {
            $regions .= $child;
        }
        $i++;
    }
    $seasons = $_POST['season'];

    foreach ($seasons as $value) {
        // SELECTING JUST ONE SEASON AS MORE THAN ONE ARE NOT ALLOWED IN IMAGE DETAIL
        $season = $value;
        break;
    }
    if ($season == '') {
        $season = 0;
    }

    require '../include/picUpload.inc.php';
    if ($_POST['image_bank_story'] == "") {
        // last @param 4 = Region Category Main Image
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 27);
        if (is_array($pic)) {
            $sql .= ", S_Thumbnail = '" . encode_strings($pic['0']['0'], $db) . "',
                       S_Thumbnail_Mobile = '" . encode_strings($pic['0']['1'], $db) . "',
                       S_Feature_Image = '" . encode_strings($pic['0']['2'], $db) . "'";
            $pic_id = $pic['1'];
            // INSERTING REGION, CATEGORY, AND SEASAON AGAINST THE IMAGE ABOUT THIS STORY
            $IMG_BNK = "UPDATE tbl_Image_Bank SET IB_Region = '" . $regions . "', IB_Category = 121, IB_Season = '" . $season . "' WHERE IB_ID = '" . $pic_id . "' ";
            $result = mysql_query($IMG_BNK, $db) or die("Invalid query: $sql -- " . mysql_error());
        }
    } else {
        $pic_id = $_POST['image_bank_story'];
        // last @param 4 = Region Category Main Image
        $pic_response = Upload_Pic_Library($pic_id, 27);
        if ($pic_response) {
            $sql .= ", S_Thumbnail = '" . encode_strings($pic_response['0'], $db) . "',
                       S_Thumbnail_Mobile = '" . encode_strings($pic_response['1'], $db) . "',
                       S_Feature_Image = '" . encode_strings($pic_response['2'], $db) . "'";
        }
    }

    if ($rowStory['S_ID'] > 0) {
        $S_ID = $_POST['id'];
        $id = $S_ID;
        $sql = "UPDATE " . $sql . " WHERE S_ID = '" . $S_ID . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        // TRACK DATA ENTRY
        Track_Data_Entry('Stories', '', 'Create Story/Story Details', $S_ID, 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql . ", S_Active = '" . encode_strings('1', $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Stories', '', 'Create Story/Story Details', $id, 'Add', 'super admin');
    }
    //insert regions against stories
    $sql = "DELETE FROM tbl_Story_Region WHERE SR_S_ID = '$id'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    foreach ($_REQUEST['childRegion'] as $child) {
        $sql = "tbl_Story_Region SET SR_S_ID = '$id', SR_R_ID = '$child'";
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    }

    //insert seasons against stories
    $delete = "DELETE FROM tbl_Story_Season WHERE SS_S_ID = '" . encode_strings($id, $db) . "'";
    mysql_query($delete, $db) or die("Invalid query: $delete -- " . mysql_error());
    foreach ($_POST['season'] as $value) {
        $sql = "INSERT tbl_Story_Season SET SS_Season = '$value', 
                SS_S_ID = '" . encode_strings($id, $db) . "'";
        mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    }

    if ($result) {
        $_SESSION['success'] = 1;
        if ($pic_id > 0) {
            //Image usage from image bank.
            imageBankUsage($pic_id, 'IBU_S_ID', $id, '', '');
        }
    } else {
        $_SESSION['error'] = 1;
    } 
    //storyCnage
    $ckecvar = 1;
    //header("Location:stories-preview.php?id=".$id);
    //die();

    $result = 0;
}
////// Add Content
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'content_add') {
    $S_ID = $_POST['id'];
    $sqlMax = "SELECT MAX(CP_Order) FROM tbl_Content_Piece WHERE CP_S_ID = '$S_ID'";
    $resultMax = mysql_query($sqlMax);
    $rowMax = mysql_fetch_row($resultMax);

    $sql = "tbl_Content_Piece SET 
            CP_S_ID = '" . encode_strings($S_ID, $db) . "', 
            CP_Title = '" . encode_strings($_REQUEST['content_title'], $db) . "', 
            CP_Video_Link = '" . encode_strings($_REQUEST['video'], $db) . "', 
            CP_Description = '" . encode_strings($_REQUEST['content_description'], $db) . "',
            CP_Pdf_Title   ='" . encode_strings($_REQUEST['pdfTitle'], $db) . "', 
            CP_Order = '" . ($rowMax[0] + 1) . "'";
    require '../include/picUpload.inc.php';
    $file_size = $_FILES['file']['size'];
    $max_filesize = 5342523;
    $pdf_name = str_replace(' ', '_', $_FILES['file']['name']);

    if ($pdf_name != "") {
        $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_name;
        $filePath = PDF_LOC_ABS . $random_name;
        if ($_FILES["file"]["type"] == "application/pdf") {
            if ($file_size < $max_filesize) {
                move_uploaded_file($_FILES["file"]["tmp_name"], $filePath);
            }
        }
    }
    if ($_POST['image_bank'] == "") {
        // last @param 26 = Story Content
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 26);
        if (is_array($pic)) {
            $sql .= ", CP_Photo = '" . encode_strings($pic['0']['0'], $db) . "', 
                       CP_Photo_Mobile = '" . encode_strings($pic['0']['1'], $db) . "'";
            $pic_id = $pic['1'];
        }
    } else {
        $pic_id = $_POST['image_bank'];
        // last @param 26 = Story Content
        $pic = Upload_Pic_Library($pic_id, 26);
        if (is_array($pic)) {
            $sql .= ", CP_Photo = '" . encode_strings($pic['0'], $db) . "', 
                       CP_Photo_Mobile = '" . encode_strings($pic['1'], $db) . "'";
        }
    }
    if ($random_name !== '') {
        $sql .= ", CP_Pdf = '" . encode_strings($random_name, $db) . "'";
    }
    $sql = "INSERT " . $sql;
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $id = mysql_insert_id();
    // TRACK DATA ENTRY
    Track_Data_Entry('Stories', '', 'Create Story/Story Details', $id, 'Add Story Content', 'super admin');

    if ($result) {
        $_SESSION['success'] = 1;
        if ($pic_id > 0) {
            //Image usage from image bank.
            imageBankUsage($pic_id, 'IBU_CP_ID', $id, '', '');
        }
    } else {
        $_SESSION['error'] = 1;
    }
}
//////////// Update Content
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'content_update') {
    //echo '<pre>'; print_r($_POST); exit();
    $S_ID = $_POST['id'];
    $sql = "tbl_Content_Piece SET  
            CP_Title = '" . encode_strings($_REQUEST['content_title'], $db) . "', 
            CP_Video_Link = '" . encode_strings($_REQUEST['video'], $db) . "',
            CP_Description = '" . mysql_real_escape_string($_REQUEST['content_description'], $db) . "',
            CP_Pdf_Title   ='" . encode_strings($_REQUEST['pdfTitle'], $db) . "'";
   // echo $sql; exit();
    require '../include/picUpload.inc.php';
    $file_size = $_FILES['file']['size'];
    $max_filesize = 5342523;
    $pdf_name = str_replace(' ', '_', $_FILES['file']['name']);
    if ($pdf_name != "") {
        $random_name = date('Y-m-d') . rand(0, 99999) . $pdf_name;
        $filePath = PDF_LOC_ABS . $random_name;
        if ($_FILES["file"]["type"] == "application/pdf") {
            if ($file_size < $max_filesize) {
                move_uploaded_file($_FILES["file"]["tmp_name"], $filePath);
            }
        }
    }

    if ($_POST['image_bank'] == "") {
        // last @param 26 = Story Content
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 26);
        if (is_array($pic)) {
            $sql .= ", CP_Photo = '" . encode_strings($pic['0']['0'], $db) . "',
                       CP_Photo_Mobile = '" . encode_strings($pic['0']['1'], $db) . "'";
            $pic_id = $pic['1'];
        }
    } else {
        $pic_id = $_POST['image_bank'];
        // last @param 26 = Story Content
        $pic = Upload_Pic_Library($pic_id, 26);
        if (is_array($pic)) {
            $sql .= ", CP_Photo = '" . encode_strings($pic['0'], $db) . "',
                       CP_Photo_Mobile = '" . encode_strings($pic['1'], $db) . "'";
        }
    }

    if ($pdf_name != '') {
        $sql .= ", CP_Pdf = '" . encode_strings($random_name, $db) . "'";
    }
    $sql = "UPDATE " . $sql . " WHERE CP_ID = '" . encode_strings($_REQUEST['cp_id'], $db) . "'";
    $result = mysql_query($sql, $db);
    // TRACK DATA ENTRY
    $id = $_REQUEST['cp_id'];
    Track_Data_Entry('Stories', '', 'Create Story/Story Details', $id, 'Update Story Content', 'super admin');

    if ($result) {
        $_SESSION['success'] = 1;
        if ($pic_id > 0) {
            //Image usage from image bank.
            imageBankUsage($pic_id, 'IBU_CP_ID', $_REQUEST['cp_id'], '', '');
        }
    } else {
        $_SESSION['error'] = 1;
    }
}
/////////////Delete Content
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'content_delete') {
    $idGet = $_REQUEST['id']; //print_r($idGet); print_r($_REQUEST['cp_id']); exit;
    $sql = "DELETE tbl_Content_Piece FROM tbl_Content_Piece WHERE CP_ID = '" . $_REQUEST['cp_id'] . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    if ($result) {
        $_SESSION['delete'] = 1;
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_CP_ID', $_REQUEST['cp_id'], '', '');
        // TRACK DATA ENTRY
        $id = $_REQUEST['cp_id'];
        //Track_Data_Entry('Stories', '', 'Preview Story', $id, 'Delete Story Content ', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
}
//////  DELETE PDF FILE
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'delPdf') {
    $select_pdf = "SELECT CP_Pdf FROM tbl_Content_Piece WHERE CP_ID = " . $_REQUEST['delete'] . " ";
    $result_pdf = mysql_query($select_pdf);
    $pdf_row = mysql_fetch_assoc($result_pdf);
    if ($pdf_row['CP_Pdf'] != "") {
        unlink(PDF_LOC_ABS . $pdf_row['CP_Pdf']);
    }
    $sql = "UPDATE tbl_Content_Piece SET CP_Pdf = '' WHERE CP_ID = '" . $_REQUEST['delete'] . "'";
    $result = mysql_query($sql);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['delete'];
        Track_Data_Entry('Stories', '', 'Create Story/Story Details', $id, 'Delete PDF', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
}
?> 
<div id="fb-root"></div>

<style>
<?php if ($activeContentCount > 0) {
    ?>
    .content-wrapper {  
        width: 1101px !important; 
        background-color: white;
        margin-left: 124px;
    }
    <?php
} else {
    ?>
    .content-wrapper {  
        width: 1101px !important; 
        background-color: white;
        margin-left: 132px;
     }        
    <?php
}
?>
    .ui-dialog { 
        z-index: 9999 !important ;
    }
    .content-wrapper .content .content-left{ 
        overflow: visible;  
        float: left;
        width: 850px;
        margin-right: 10px;
        background-color: white;
        padding-bottom: 20px;
    }
    .menu-items-accodings{
        width: 100% !important;
    }

</style>
<section class="descriptions stories">
    <div class="content-left full-width">
        <div class="title-link">
            <div class="title">Preview Story</div>
            <!--            <div class="add-box">
                            <a class="story-add-new" onclick="return add_new_story()">
                                +Add Story                                       
                            </a>
                        </div>-->
            <div class="link">
                <a onclick="return show_tories_add(<?php echo $CP_IDS ?>)" class="story_add_<?php echo $CP_IDS ?>">+Add Content</a>

            </div>
        </div>

    </div>

    <div class="stories-full-div">
        <div class="description-wrapper">
            <div class="description-inner">
                <div class="story-left">

                    <div id="stories-preview1"> 
                        <h1 id="desc1" class="heading-text full-width">
                            <?php 
                            if (isset($activeStory['S_Title']) && $activeStory['S_Title'] != '') { ?>
                                <?php echo $activeStory['S_Title']; ?> 
                                <a onclick="return tories_edit(<?php echo $CP_IDS ?>)" class="story_edit_data_<?php echo $CP_IDS ?> reorder-image">
                                    <img class="editBtn-backgrondmaianTitle"   src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" />  
                                </a>
                            <?php } else {
                                ?>
                                <div class="add-box">
                                    <a onclick="return show_tories_edit(<?php echo $activeContent['CP_ID'] ?>)" class="story_edit_data_<?php echo $CP_IDS ?>" >
                                        +Add Title 
                                    </a>
                                </div>      
                            <?php }
                            ?>
                        </h1>
                    </div>
                    <div  id="stories-preview2">
                        <div   class="story-titles margin-none full-width">
                            <?php //print_r($TO_Story_Page_Content_Title);
                            if (isset($activeStory['S_Author']) && $activeStory['S_Author'] != '') { ?>  
                                <?php
                                echo $activeStory['S_Author'];
                                if ($activeContentCount > 0) {
                                    ?>
                                    <a onclick="return story_order()" class="reorder-image">
                                        <img class="editBtn-backgrondmaianTitle"   src="http://<?php echo DOMAIN . '/images/order-icon.png'; ?>" alt="order" />  
                                    </a>
                                <?php
                                }
                            } else {
                                ?>
                                <div class="add-box">
                                    <a onclick="return show_tories_edit(<?php echo $activeContent['CP_ID'] ?>)">
                                        +Add Author 
                                    </a>
                                </div>      
                            <?php }
                            ?></div>  
                        <label class="edit-btn-front-side">
                            <a onclick="return show_tories_edit(<?php echo $activeContent['CP_ID'] ?>)">
                                <span style="background-image:url('<?php echo "/images/edit-icon.png"; ?>')" class="editBtn-backgrond"></span>
                            </a>
                        </label>
                    </div>
                    <div class="story-div1" id="region-slider-list">

                        <?php
                        $getStoryContent = "SELECT CP_ID, CP_S_ID, CP_Title, CP_Photo, CP_Description, CP_Video_Link, CP_Pdf, CP_Pdf_Title  FROM tbl_Content_Piece WHERE CP_S_ID = '" . encode_strings($activeStory['S_ID'], $db) . "' ORDER BY CP_Order ASC";
                        $resContent = mysql_query($getStoryContent, $db) or die("Invalid query: $getStoryContent -- " . mysql_error());
                        $i = 1;
                        $acco = 0;
                        while ($activeContent = mysql_fetch_array($resContent)) {
                            ?>
                            <div id="gallery_<?php echo $activeContent['CP_ID']; ?>">            
                                <form name="form2" method="post" action="#recordsArray_<?php echo $activeContent['CP_ID']; ?>" enctype="multipart/form-data" class="story-content-update-form">
                                    <input type="hidden" name="acc" value="<?php echo $acco; ?>">
                                    <input type="hidden" name="recordsArray" id="recordsArray" value="<?php echo $activeContent['CP_ID'] ?>">
                                    <input type="hidden" name="id" value="<?php echo $S_ID ?>"> 

                                    <div class="content-full">
                                        <div class="story-div2"> 
                                            <div class="story-photo">
                                                <div class="stories-preview8">
                                                    <div class="story-titles margin-none">
                                                        <?php
                                                        $VIDID = $activeContent['CP_Video_Link'];
                                                        $pos = strpos($VIDID, '=');
                                                        if ($pos == false) {
                                                            $video_link = end(explode('/', $VIDID));
                                                        } else {
                                                            $video_link = end(explode("=", $VIDID));
                                                        }
                                                        if ($VIDID != "") {
                                                            ?>
                                                            <div class="<?php echo "player" . $i . "_wrapper"; ?>"  style="position: absolute;opacity: 0;">
                                                                <input type="hidden" id="video-link-<?php echo $i; ?>" value="<?php echo $video_link ?>">
                                                                <div  id="<?php echo "player" . $i; ?>"></div>
                                                            </div>
                                                            <?php
                                                        }
                                                        if ($VIDID != "") {
                                                            ?>
                                                            <div  class="watch-video play-button show-slider-display"> 
                                                                <div class="slider-button">
                                                                    <img  style="position: relative;" class="video_icon stories-preview7" src="../../../images/videoicon.png" alt="Play">
                                                                    <a class="video-button-story" onclick="play_video(<?php echo $i; ?>, '<?php echo"player" . $i; ?>')">Play Video</a>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        }
                                                        if (isset($activeContent['CP_Photo']) && $activeContent['CP_Photo'] != '') {
                                                            ?>  
                                                            <img  class="stories-preview9" class="coupon-img" src="<?php echo IMG_LOC_REL . $activeContent['CP_Photo'] ?>" alt="<?php echo $activeContent['CP_Title'] ?>"/></div>
                                                    <?php } else {
                                                        ?>
                                                        <div class="add-box">
                                                            <a href="story.php?id=<?php echo $CP_IDS; ?>&story=editstory&acc=<?php echo $acco; ?>#recordsArray_<?php echo $activeContent['CP_ID']; ?>">

                                                                <img class="stories-preview10"  class="coupon-img" src="http://<?php echo DOMAIN . '/images/Listing-NoPhoto.jpg'; ?>" alt="<?php echo $activeContent['CP_Title'] ?>"/>
                                                        </div>
                                                        </a>
                                                    </div>      
                                                <?php }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="stories-preview13"> 
                                            <div  class="story-titles margin-none">
                                                <div class="story-titles">
                                                    <?php if (isset($activeContent['CP_Title']) && $activeContent['CP_Title'] != '') {
                                                        ?>
                                                        <div class="stories-preview14">
                                                        <?php echo $activeContent['CP_Title'] ?> 
                                                        </div> 
                                                        <a onclick="return confirm('Are you sure?')" href="stories-preview.php?id=<?php echo $CP_IDS; ?>&op=content_delete&cp_id=<?php echo $activeContent['CP_ID']; ?>">
                                                            <img class="deleteBtnbackgrond"  src="http://<?php echo DOMAIN . '/images/warning_icon.png'; ?>" alt="Delete" />
                                                        </a>
                                                        <a onclick="return show_tories_edit(<?php echo $activeContent['CP_ID'] ?>)" class="story_<?php echo $activeContent['CP_ID']; ?>">
                                                            <img class="editBtn-backgrond"  src="http://<?php echo DOMAIN . '/images/edit-icon.png'; ?>" alt="edit" /> 
                                                        </a> <?php } else {
                                                             ?>
                                                        <div class="add-box">
                                                            <a onclick="return show_tories_edit(<?php echo $activeContent['CP_ID'] ?>)" class="story_<?php echo $activeContent['CP_ID']; ?>">
                                                                +Add Title                                       
                                                            </a>

                                                        </div>      
                                                    <?php }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="stories-preview15">
                                            <div id="desce<?php echo $c; ?>"  class="story-titles margin-none">
                                                <div class="story-desc theme-paragraph ckeditor-anchor-listing">
                                                    <?php if (isset($activeContent['CP_Description']) && $activeContent['CP_Description'] != '') {
                                                        ?>
                                                        <div class="stories-preview16">
                                                            <?php
                                                           //echo $str = str_replace("�&quot;", "",$activeContent['CP_Description']);
                                                            //echo $str1 = str_replace('?"', '', $str);
                                                            echo $activeContent['CP_Description'];
                                                            if (isset($activeContent['CP_Pdf']) && $activeContent['CP_Pdf'] !== '') {
                                                                ?>
                                                                <a  href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $activeContent['CP_Pdf'] ?>" target="_blank"><?php echo ($activeContent['CP_Pdf_Title'] !=='') ? $activeContent['CP_Pdf_Title'] : "View PDF" ?></a>
                                                                <?php } ?>
                                                        </div>

                                                    <?php } else {
                                                        ?>
                                                        <div class="add-box">
                                                            <a class="story_<?php echo $activeContent['CP_ID'] ?>" onclick="return show_tories_edit(<?php echo $activeContent['CP_ID'] ?>)">
                                                                +Add Description                                       
                                                            </a>
                                                        </div>      
                                                    <?php }
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form> 
                            </div>

                            <div class="div-story-preview" style="display:none;">
                                <form action="" method="post" class="storyForm" name="form" id="existing-story-form<?php echo $activeContent['CP_ID'] ?>" enctype="multipart/form-data"  style="display:none;">
                                    <input type="hidden" name="cp_id" value="<?php echo $activeContent['CP_ID'] ?>">
                                    <input type="hidden" name="id" value="<?php echo $activeContent['CP_S_ID'] ?>">
                                    <input type="hidden" name="op" value="content_update">
                                    <input type="hidden" name="image_bank" class="image_bank" id="image_bank<?php echo $activeContent['CP_ID'] ?>" value="">
                                    <div class="form-inside-div form-inside-div-width-admin full-width story-popup">
                                        <label class="story-title-preview">Title</label>
                                        <div class="form-data">
                                            <input name="content_title" class="story-textbox-preview" type="text" value="<?php echo $activeContent['CP_Title'] ?>"/>
                                        </div>
                                    </div>
                                    <div class="form-inside-div form-inside-div-width-admin full-width story-popup">
                                        <label class="story-title-preview">Photo</label>
                                        <div class="form-inside-div div_image_library about-us-cropit border-none" id="story_preview_image">
                                            <span class="daily_browse" onclick="show_image_library(26, <?php echo $activeContent['CP_ID']; ?>)">Select File</span>
                                            <input type="file" style="display :none;" name="pic[]" onChange="show_file_name(26, this, <?php echo $activeContent['CP_ID']; ?>);" id="photo<?php echo $activeContent['CP_ID']; ?>">
                                            <div class="cropit-image-preview aboutus-photo-perview-story">
                                                <img class="preview-img preview-img-script<?php echo $activeContent['CP_ID']; ?>" style="display: none;" src="">    
                                                <?php if ($activeContent['CP_Photo'] != '') { ?>
                                                    <img class="existing-img existing_imgs<?php echo $activeContent['CP_ID']; ?>" src="<?php echo IMG_LOC_REL . $activeContent['CP_Photo'] ?>" >
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-inside-div form-inside-div-width-admin full-width story-popup">
                                        <label class="story-title-preview">PDF Title</label>
                                        <div class="form-data">
                                            <input name="pdfTitle" type="text" class="story-textbox-preview" value="<?php echo (isset($activeContent['CP_Pdf_Title']) ? $activeContent['CP_Pdf_Title'] : '') ?>" size="50" /> 
                                        </div>
                                    </div>
                                    <div class="form-inside-div form-inside-div-width-admin full-width story-popup"> 
                                        <label class="story-title-preview">Upload PDF</label>
                                        <div class="form-data image-bank-browse image_bank_width_browse">
                                            <div class="inputWrapper adv-photo float-left">Browse
                                                <input id="imageInput filename"  class="setting-name fileInput file_ext0 required_title0" accept="application/pdf"  type="file" name="file"  onchange="show_file_names(this.files[0].name, 1)" multiple>
                                            </div>
                                            <input style="display: none;" id="uploadFile" class="uploadFileName" disabled>

                                            <div class="margin-left-resize add_event_view">
                                                <?php
                                                if (isset($activeContent['CP_Pdf']) && $activeContent['CP_Pdf'] != '') {
                                                    ?>
                                                    <a target="_blank" class="pdf-color-btn"  href="<?php echo 'http://' . DOMAIN . PDF_LOC_REL . $activeContent['CP_Pdf'] ?>">
                                                        View
                                                    </a> 
                                                </div>
                                                <div class="margin-left-resize add_event_view">
                                                    <a class="delete-pointer pdf-color-btn"  onClick="return confirm('Are you sure this action can not be undone!')" href="stories-preview.php?id=<?php echo $_REQUEST['id'] ?>&delete=<?php echo $activeContent['CP_ID'] ?>&op=delPdf">Delete</a> 

                                                </div>
                                            <?php }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-inside-div form-inside-div-width-admin full-width story-popup">
                                        <label class="story-title-preview">Video Link</label>
                                        <div class="form-data">
                                            <input name="video" class="story-textbox-preview" type="text" value="<?php echo (isset($activeContent['CP_Video_Link']) ? $activeContent['CP_Video_Link'] : '') ?>" size="50" /> 
                                        </div>
                                    </div>
                                    <div class="form-inside-div form-inside-div-width-admin full-width story-popup">
                                        <label class="story-title-preview">Description</label>
                                        <div class="form-data content_description">
                                            <textarea name="content_description" id="description_<?php echo $activeContent['CP_ID'] ?>"  class="ckeditor"><?php echo htmlspecialchars($activeContent['CP_Description']) ?></textarea>
                                        </div>
                                        <?php $BFC_ID = $activeContent['CP_ID']; ?>
                                        <script>
                                            ////story_
                                            $('.story_<?php echo $BFC_ID; ?>').click(function () {
                                                $('#existing-story-form<?php echo $BFC_ID; ?>').dialog('open');
                                                $('#description_<?php echo $BFC_ID ?>').ckeditor();
                                            });
                                            $(function () {
                                                $('#existing-story-form<?php echo $BFC_ID; ?>').attr("title", "Edit content story").dialog({
                                                    autoOpen: false,
                                                });
                                            });
                                        </script>
                                    </div>

                                    <div class="story_preview_button">
                                        <div class="form-data">
                                            <input type="submit" name="button-edit-text" value="Submit"/>
                                        </div>
                                    </div> 
                                </form>  
                            </div> 
                        </div>
                    </div>
                    <?php
                    $i++;
                    $acco++;
                }
                ?>
            </div></div> 
        <div class="div-story-preview_order" style="display:none;">

            <div id="accordion">  
                <?PHP
                $sql = "SELECT * FROM tbl_Content_Piece WHERE CP_S_ID = " . $S_ID . " ORDER BY CP_Order ASC";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                while ($rowContent = mysql_fetch_array($result)) {
                    ?>
                    <div class="group" id="recordsArray_<?php echo $rowContent['CP_ID']; ?>"> 
                        <h3 class="accordion-rows">
                            <span id="scrolls_<?php echo $rowContent['CP_ID']; ?>" class="accordion-title"><?php echo $rowContent['CP_Title']; ?></span>
                        </h3> 
                    </div>       
                 <?PHP } ?>
            </div> 
        </div> 
        <style>
            h3.accordion-rows.ui-sortable-handle{
                padding: 14px;
                text-align: left;
                background-color: gray;
            }
        </style>
        <?php
        $_SESSION['stories'] = 1;
        $session_value = $_SESSION['stories']
        ?>
        <script>;
            someVarNames = 0;
            $(function () {
                $("#accordion")
                        .sortable({
                            axis: "y",
                            handle: "h3",
                            update: function () {
                                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Content_Piece&field=CP_Order&id=CP_ID';
                                $.post("reorder.php", order, function (theResponse) {
                                    var myvar = '<?php echo $session_value; ?>';
                                    var contactID = "<?php echo $rowContent['CP_ID']; ?>";
                                    var id = '<?php echo $S_ID; ?>';
                                    if (myvar == 1) {
                                        var someVarName = 1;
                                        localStorage.setItem("someVarName", someVarName);
                                        window.location.href = "stories-preview.php?id=" + id;
                                        myvar == 0;
                                    }

                                });
                            }
                        });
            });
            var someVarNames = localStorage.getItem("someVarName");
            if (someVarNames == 1) {
                swal("Success", "Stories Re-Ordered Successfully!", "success");
                localStorage.clear();

            }
        </script>
    </div> 
</div>
<input type="hidden" id="total_players" value="<?php echo $i; ?>">
</div>
</div>
</div>
<div class="div-story-preview">
    <form action="" method="post" class="storyForm"  name="form" id="existing-story-forms<?php echo $_REQUEST['id'] ?>" enctype="multipart/form-data"  style="display:none;">
        <input type="hidden" name="op" value="content_add">
        <input type="hidden" name="id" value="<?php echo $_REQUEST['id']; ?>">
        <input type="hidden" name="image_bank" class="image_bank" id="image_bank26" value="">
        <div class="form-inside-div form-inside-div-width-admin full-width story-popup">
            <label class="story-title-preview">Title</label>
            <div class="form-data">
                <input name="content_title" class="story-textbox-preview" type="text" required />
            </div>
        </div>
        <div class="form-inside-div form-inside-div-width-admin full-width story-popup">
            <label class="story-title-preview">Photo</label>
            <div class="form-inside-div div_image_library about-us-cropit border-none" id="story_preview_image">
                <span class="daily_browse" onclick="show_image_library(26)">Select File</span>
                <input type="file" style="display :none;" name="pic[]" onChange="show_file_name(26, this);" id="photo26">
                <div class="form-inside-div add-about-us-item-image margin-none">
                    <div class="cropit-image-preview aboutus-photo-perview">
                        <img class="preview-img preview-img-script26" style="display: none;" src="">    
                    </div>
                </div>
            </div>
        </div>
        <div class="form-inside-div form-inside-div-width-admin full-width story-popup">
            <label class="story-title-preview">PDF Title</label>
            <div class="form-data">
                <input name="pdfTitle" type="text" class="story-textbox-preview"  size="50" /> 
            </div>
        </div>
        <div class="form-inside-div form-inside-div-width-admin full-width story-popup"> 
            <label class="story-title-preview">Upload PDF</label>
            <div class="form-data image-bank-browse image_bank_width_browse">
                <div class="inputWrapper adv-photo float-left">Browse
                    <input id="imageInput filename"  class="setting-name fileInput file_ext0 required_title0" accept="application/pdf"  type="file" name="file"  onchange="show_file_names(this.files[0].name, 1)" multiple>
                </div>
                <input style="display: none;" id="uploadFile" class="uploadFileName" disabled>

            </div>
        </div>
        <div class="form-inside-div form-inside-div-width-admin full-width story-popup">
            <label class="story-title-preview">Video Link</label>
            <div class="form-data">
                <input name="video" class="story-textbox-preview" type="text"  size="50"/> 
            </div>
        </div>
        <div class="form-inside-div form-inside-div-width-admin full-width story-popup">
            <label class="story-title-preview">Description</label>
            <div class="form-data content_description">
                <textarea name="content_description" id="addStory"  class="ckeditor"></textarea>
            </div>
        </div>
        <div class="story_submit_button">
            <div class="form-data">
                <input type="submit" name="button" value="Save Content" />
            </div>
        </div> 
    </form>
</div>

<!--    Edit Story-->
<div class="right full-width" <?php if (isset($_REQUEST['Count']) && $_REQUEST['Count'] == 'countsVal') { ?> id="edit_story_form" <?php } ?>>
    <div class="div-story-preview">
        <form action="" method="post" class="storyForm"  name="form" id="existing-story<?php echo $_REQUEST['id'] ?>" enctype="multipart/form-data"  style="display:none;">
            <input type="hidden" name="op" id="story-add" value="storyCnage">
            <input type="hidden" name="id" value="<?php echo $_REQUEST['id'] ?>">
            <div class="form-inside-div form-inside-div-width-admin full-width story-popup">
                <label class="story-title-preview">Title</label>
                <div class="form-data">
                    <input name="title" class="story-textbox-preview" type="text" value="<?php echo (isset($rowStory['S_Title']) ? $rowStory['S_Title'] : '') ?>" size="40" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width story-popup">
                <label class="story-title-preview">Category</label>
                <div class="form-data">
                    <select name="category" required class="story-textbox-preview">
                        <option value="">Select Category</option>
                        <?php
                        $sql = "SELECT C_ID FROM tbl_Category WHERE C_Is_Blog = 1";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        $rowExplore = mysql_fetch_array($result);
                        $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '" . $rowExplore['C_ID'] . "' ORDER BY C_ID ASC";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($rowExploreSub = mysql_fetch_array($result)) {
                            ?>
                            <option value="<?php echo $rowExploreSub['C_ID'] ?>" <?php echo (isset($rowStory['S_Category']) && $rowExploreSub['C_ID'] == $rowStory['S_Category']) ? 'selected' : ''; ?>>
                            <?php echo $rowExploreSub['C_Name']; ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width story-popup">
                <label class="story-title-preview">Author</label>
                <div class="form-data">
                    <input name="author" class="story-textbox-preview" type="text" value="<?php echo (isset($rowStory['S_Author']) ? $rowStory['S_Author'] : '') ?>" size="40" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width story-popup"> 
                <label class="story-title-preview">Description</label>
                <div class="form-data content_description">
                    <textarea name="description" class="tt-description story-textbox-preview"><?php echo $rowStory['S_Description']; ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width story-popup" style="<?php echo $community_class ?>"> 
                <label class="story-title-preview">Select Community</label>
                <div class="form-data content_description">
                    <div id="childRegion"> 
                        <?PHP
                        $currentRType = 0;
                        $RTypesArray = array(
                            1 => 'Parent Region',
                            2 => 'Community',
                            3 => 'Organization',
                            4 => 'County',
                            7 => 'Specialty'
                        );

                        $sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_Type IN(1,2,3,4,7) AND R_Type > 0 $where ORDER BY R_Type, R_Name";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_array($result)) {
                            if ($currentRType != $row['R_Type']) {
                                $currentRType = $row['R_Type'];
                                echo '<div class="regionTypeTitles">' . $RTypesArray[$currentRType] . '</div>';
                            }
                            echo '<div class="childRegionCheckboxs"><input type="checkbox" name="childRegion[]" value="' . $row['R_ID'] . '"';
                            echo (in_array($row['R_ID'], $childRegion) || $checked == '1') ? 'checked' : '';
                            echo '>' . $row['R_Name'] . "</div>";

                            if (in_array($row['R_ID'], $childRegion)) {
                                $regions_for_analytics[] = $row['R_ID'];
                            }
                        } 
                        ?>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width story-popup">
                <label class="story-title-preview">SEO Title </label>
                <div class="form-data">
                    <input name="seoTitle" class="story-textbox-preview" type="text" value="<?php echo (isset($rowStory['S_SEO_Title']) ? $rowStory['S_SEO_Title'] : '') ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width story-popup"> 
                <label class="story-title-preview">SEO Description </label>
                <div class="form-data">
                    <input name="seoDescription" class="story-textbox-preview" type="text" value="<?php echo (isset($rowStory['S_SEO_Description']) ? $rowStory['S_SEO_Description'] : '') ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width story-popup"> 
                <label class="story-title-preview">SEO Keywords</label>
                <div class="form-data">
                    <input name="seoKeywords" class="story-textbox-preview" type="text" value="<?php echo (isset($rowStory['S_SEO_Keywords']) ? $rowStory['S_SEO_Keywords'] : '') ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width story-popup"> 
                <label class="story-title-preview">Seasons</label>
                <div class="form-data season-preview" id="story_seasons">
                    <div id="childRegion"> 
                        <?php
                        $StoriesSeasons = array();
                        while ($rowSS = mysql_fetch_array($resultSS)) {
                            $StoriesSeasons[] = $rowSS['SS_Season'];
                        }
                        $sql = "SELECT * FROM tbl_Seasons";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_array($result)) {
                            ?>
                            <div class="childRegionCheckboxs">
                                <input name="season[<?php echo $row['S_ID'] ?>]" type="checkbox" value="<?php echo $row['S_ID'] ?>" <?php echo in_array($row['S_ID'], $StoriesSeasons) ? 'checked' : '' ?> /><?php echo $row['S_Name'] ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width story-popup">
                <label class="story-title-preview">Photo</label>
                <div class="form-inside-div div_image_library about-us-cropit border-none" id="story_photo">
                    <span onchange="show_file_name(27, this)" onclick="show_image_library(27)">Select File</span>
                    <input type="file" style="display :none;" name="pic[]" onChange="show_file_name(27, this);" id="photo27">
                    <input type="hidden" name="image_bank_story" id="image_bank27" value="">
                    <div class="cropit-image-preview aboutus-photo-perview-story-edit">
                        <img class="preview-img preview-img-script27" id="container" style="display: none;" src="" id="chk">    
                        <?php if (isset($rowStory['S_Thumbnail']) && $rowStory['S_Thumbnail'] != '') { ?>
                            <img class="existing-img existing_imgs27" src="<?php echo IMG_LOC_REL . $rowStory['S_Thumbnail'] ?>" >
                        <?php } ?>
                    </div>
                </div>

            </div>

            <div class="form-inside-div form-inside-div-width-admin full-width story-popup"> 
                <div class="button" style="text-align: center; width: 77%;">
                    <input type="submit" name="button_story" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>
</section>

<div id="image-library" style="display:none;"></div>

<?PHP
$activeAccordion = '';
if (isset($_REQUEST['acc']) && $_REQUEST['acc'] != '') {
    $activeAccordion = $_REQUEST['acc'];
} else {
    $activeAccordion = 'false';
}
$BL_ID = $_REQUEST['id'];
?>
<script>
/// Add New story
    $('.story-add-new').click(function () {
        $('#new-story-form').dialog('open');
        $('#add-new-story').ckeditor();
    });

    $(function () {
        $('#new-story-form').attr("title", "Create Story").dialog({
            autoOpen: false,
        });
    });
     $('.story_edit_data_<?php echo $BL_ID; ?>').click(function () {

        $('#storyID').val('storyCnage');
    });

    ////story_add_
    $('.story_add_<?php echo $BL_ID; ?>').click(function () {
        $('#existing-story-forms<?php echo $BL_ID; ?>').dialog('open');
    });

    $(function () {
        $('#existing-story-forms<?php echo $BL_ID; ?>').attr("title", "Add story content").dialog({
            autoOpen: false,
        });
    });

///tories_edit  
    $('.story_edit_data_<?php echo $BL_ID; ?>').click(function () {
        $('#existing-story<?php echo $BL_ID; ?>').dialog('open');
        $('.tt-description').ckeditor();
    });

    $(function () {
        $('#existing-story<?php echo $BL_ID; ?>').attr("title", "Edit story").dialog({
            autoOpen: false,
        });
    });

    function show_file_names(val, count) {
        $(".uploadFileName").show();
        $(".uploadFileName").val(val);
    }
    function show_tories_edit(id) {
        $("#existing-story-form" + id).attr("title", "Edit content story").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 900,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#existing-story-form" + id).attr("title", "Edit content story").dialog('close');
                })
            }
        });   
    }
    ///story_order
    function story_order() {
        $(".div-story-preview_order").attr("title", "Content ordering").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 900,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery(".div-story-preview_order").dialog('close');
                })
            }
        });  
    }
    function show_tories_add(add) {
        $("#existing-story-forms" + add).attr("title", "Add content story").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 900,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#existing-story-forms" + add).attr("title", "Add content story").dialog('close');
                })
            }
        });

    }

    function tories_edit(id) {
        $("#existing-story" + id).attr("title", "Edit story").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 900,
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    jQuery("#existing-story" + id).dialog('close');
                })
            }
        });
    }
</script>
<div class="content-wrapper">
    <div class="right full-width story-stats">
        <div class="content-header story  full-width">Statistics</div>
        <div class="menu-items-accodings"> 
            <div id="accordion1">
                <?PHP
                ///$regionID = 1;
                $region_tracking_ids = array(
                    1 => 'UA-32156009-4',
                    3 => 'UA-32156009-3',
                    17 => 'UA-32156009-11'
                );
                $regions = array();
                foreach ($regions_for_analytics as $key => $regionID) {
                    $sql = "SELECT R_Type, R_Tracking_ID, R_Domain, R_Domain_Alternates, R_Name FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    $activeRegion = mysql_fetch_assoc($result);
                    // $tracking_id='UA-32156009-4';
                    $tracking_id = $region_tracking_ids[$regionID];
                    $r_name = $activeRegion['R_Name'];
                    $R_Domain = $activeRegion['R_Domain'] . '/explore/' . clean($rowStory['S_Title']) . '/' . $S_ID;
                    $R_Domain_Alternates = $activeRegion['R_Domain_Alternates'];

                    $analytics = getService();
                    $profile = getprofileId($analytics, $regionID, $tracking_id, $R_Domain, $R_Domain_Alternates, $r_name);
                }

                function printResultsPages(&$results, &$r, &$r_name) {
                    $rows = $results->getRows();
                    $yearCurrent = date('Y');
                    $currentIteration = '';
                    $firstIteration = true;
                    foreach (array_reverse($rows) as $monthrecord) {
                        if ($currentIteration != $monthrecord[0]) {
                            //start accordion
                            if ($firstIteration) {
                                $firstIteration = false;
                            } else {
                                print '<div class="content-sub-header-story footer-width padding-none" id="footer-width">
                                <div class="data-column story-preview">Total</div>
                                                <div class="data-column story-preview">' . $users_total . '</div>
                                                <div class="data-column story-preview">' . $viewed_total . '</div>
                                                <div class="data-column story-preview">' . $sessions_total . '</div>
                                                <div class="data-column story-preview">' . $unique_total . '</div>
                            </div>';
                                print '</div>';  //end the previous accordion
                                $viewed_total = 0;
                                $sessions_total = 0;
                                $unique_total = 0;
                                $users_total = 0;
                            }
                            //start new accordion
                            if (!in_array($r_name, $regions)) {
                                print '<div class="content-sub-header-story padding-none">                    
                                    <div class="data-column story-preview" style="text-align: left;margin-bottom:5px;">' . $r_name . '</div>
                                </div><br>';
                            }
                            print '<h3 class="accordion-rows"><span class="accordion-title">' . $monthrecord[0] . '</span></h3>';
                            print '<div class="sub-accordions accordion-padding">';

                            //print headers
                            print '<div class="content-sub-header-story padding-none">                    
                                    <div class="data-column story-preview">Month</div>
                                    <div class="data-column story-preview">Users</div>
                                    <div class="data-column story-preview">Viewed</div>
                                    <div class="data-column story-preview">Sessions</div>
                                    <div class="data-column story-preview">Unique</div>
                                </div>';

                            $currentIteration = $monthrecord[0];
                        }
                        $viewed_total += $monthrecord[2];
                        $sessions_total += $monthrecord[3];
                        $unique_total += $monthrecord[4];
                        $users_total += $monthrecord[5];

                        $viewed += $monthrecord[2];
                        $sessions += $monthrecord[3];
                        $unique += $monthrecord[4];
                        $users += $monthrecord[5];
                        ?>
                        <div class="data-content story">  

                            <div class="data-column story-preview"><?php print date('M', strtotime($monthrecord[0] . '-' . $monthrecord[1] . '-' . '01')); ?></div>

                            <div class="data-column story-preview"><?php print $monthrecord[5]; ?></div>
                            <div class="data-column story-preview"><?php print $monthrecord[2]; ?></div>
                            <div class="data-column story-preview"><?php print $monthrecord[3]; ?></div>
                            <div class="data-column story-preview"><?php print $monthrecord[4]; ?></div>
                        </div>

                        <?php
                        $regions[] = $r_name;
                    }
                    print '<div class="content-sub-header-story footer-width padding-none" id="footer-width">
                                <div class="data-column story-preview">Total</div>
                                <div class="data-column story-preview">' . $users_total . '</div>
                                <div class="data-column story-preview">' . $viewed_total . '</div>
                                <div class="data-column story-preview">' . $sessions_total . '</div>
                                <div class="data-column story-preview">' . $unique_total . '</div>
                            </div>';
// print '</div>'; 
                    ?>                          
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
<?php
function clean($string) {
    $string = strtolower(str_replace(' ', '-', $string)); // Replaces all spaces with hyphens.
    return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
}
function getService() {
    // Creates and returns the Analytics service object.
    // Load the Google API PHP Client Library.
    require_once '../include/google-api-php-client-master/src/Google/autoload.php';

    // Use the developers console and replace the values with your
    // service account email, and relative location of your key file.
    $service_account_email = 'analytics@tourist-town.iam.gserviceaccount.com';
    $key_file_location = 'Tourist Town-2a6f3c92156a.p12';

    // Create and configure a new client object.
    $client = new Google_Client();
    $client->setApplicationName("HelloAnalytics");
    $analytics = new Google_Service_Analytics($client);

    // Read the generated client_secrets.p12 key.
    $key = file_get_contents($key_file_location);
    $cred = new Google_Auth_AssertionCredentials(
            $service_account_email, array(Google_Service_Analytics::ANALYTICS_READONLY), $key
    );
    $client->setAssertionCredentials($cred);
    if ($client->getAuth()->isAccessTokenExpired()) {
        $client->getAuth()->refreshTokenWithAssertion($cred);
    }
    return $analytics;
}
function getprofileId(&$analytics, &$regionID, &$tracking_id, &$R_Domain, &$R_Domain_Alternates, &$r_name) {
    $accounts = $analytics->management_accounts->listManagementAccounts();
    if (count($accounts->getItems()) > 0) {
        $items = $accounts->getItems();
        foreach ($items as $item) {
            $firstAccountId = $item->getId();
            $webproperties = $analytics->management_webproperties->listManagementWebproperties($firstAccountId);
            if (count($webproperties->getItems()) > 0) {
                $items_webproperties = $webproperties->getItems();
                foreach ($items_webproperties as $items_webproperty) {
                    $firstWebpropertyId = $items_webproperty->getId();
                    $profiles = $analytics->management_profiles->listManagementProfiles($firstAccountId, $firstWebpropertyId);
                    if (count($profiles->getItems()) > 0) {
                        $items_profiles = $profiles->getItems();
                        foreach ($items_profiles as $items_profile) {
                            $profileId = $items_profile->getId();
                            $profileweb = $items_profile->getwebPropertyId();
                            if ($tracking_id == $profileweb) {
                                if (isset($profileId)) {
                                    $startdate = date('2016-08-01');
                                    $enddate = date('Y-m-d');
                                    $filters = array('filters' => 'ga:hostname=~' . $R_Domain . '|' . $R_Domain_Alternates, 'dimensions' => 'ga:year,ga:month', 'max-results' => '800000');
                                    $results_pages = getResultsGA($analytics, $profileId, $startdate, $enddate, 'ga:pageviews,ga:sessions,ga:uniquePageviews,ga:Users', $filters);
                                    printResultsPages($results_pages, $regionID, $r_name);
                                }
                            }
                        }
                    } else {
                        throw new Exception('No views (profiles) found for this user.');
                    }
                }
            } else {
                throw new Exception('No webproperties found for this user.');
            }
        }
    } else {
        throw new Exception('No accounts found for this user.');
    }
}

function getResultsGA(&$analytics, $profileId, $startdate, $enddate, $matrics, $filters) {
    return $analytics->data_ga->get(
                    'ga:' . $profileId, $startdate, $enddate, $matrics, $filters);
}
?>
<script>
    $(function () {

        $("#accordion1").accordion({
            collapsible: true,
            heightStyle: 'content',
            active: false,
            header: "> h3"
        });
    });
    function show_file_names(val, count) {
        $(".uploadFileName").show();
        $(".uploadFileName").val(val);
    }
    $(document).ready(function () {
        $("#add-content").click(function () {
            $("#add-content-form").toggle();
        });
        var storyAdd = '<?php echo $_REQUEST['addStory']; ?>';
        if (storyAdd != '' && storyAdd == 'addstories') {
            $("#add-content-form").show();
        }
    });
    function play_video(id, player_id) {
        PlayerReady(id, player_id);
    }
    function get_youtube_videoid(url) {
        var video_id = url.split('v=')[1];
        var ampersandPosition = video_id.indexOf('&');
        if (ampersandPosition != -1) {
            video_id = video_id.substring(0, ampersandPosition);
        }
        return video_id;
    }
    // 2. This code loads the IFrame Player API code asynchronously.
    var tag = document.createElement('script');

    tag.src = "http://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // 3. This function creates an <iframe> (and YouTube player)
    //    after the API code downloads.
    var total_players = $("#total_players").val();
    for (var i = 1; i < total_players; i++) {
        eval("var player" + i);
    }
    var current_player_id;

    function onYouTubePlayerAPIReady() {
        for (var i = 1; i < total_players; i++) {
            eval("player" + i + "= new YT.Player('player" + i + "'," + "{" +
                    "height: '350'," +
                    "width: '580'," +
                    "videoId:'" + $("#video-link-" + i).val() +
                    "',events: {'onStateChange': onPlayerStateChange}});");
        }
    }

    // 4. The API will call this function when the video player is ready.
    function PlayerReady(id, player_id) {
        var width = $(".player" + id + "_wrapper").width();
        var height = $(".player" + id + "_wrapper").height();
        //console.log(width);
        if (width == null) {
            var width = 580;
            var height = 350;
        }
        $(".player" + id + "_wrapper").parent(".slide-images").find('img.slider_image').css('opacity', '0');
        $(".center #prev").css('display', 'none');
        $(".center #next").css('display', 'none');
        $(".player" + id + "_wrapper").css('opacity', '1');
        $(".player" + id + "_wrapper").css('z-index', '600');
        this[player_id].setSize(width, height);
        this[player_id].playVideo();
        current_player_id = id;
    }

    // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.ENDED) {
            stopVideo();
        }
    }
    function stopVideo() {
        //    this[player_id].stopVideo();
        $(".player" + current_player_id + "_wrapper").css('opacity', '0');
        $(".player" + current_player_id + "_wrapper").css('z-index', '0');
        $(".player" + current_player_id + "_wrapper").parent(".slide-images").find('img.slider_image').css('opacity', '1');
        if (total_players > 2) {
            $(".center #prev").css('display', 'block');
            $(".center #next").css('display', 'block');
        }
        $('.sliderCycle').cycle('resume');
    }
    $(document).ready(function () {
        var d = $('.storyForm');
        d.scrollTop(d.prop("scrollHeight"));
    });
</script>    
<?php
require_once '../include/admin/footer.php';
?>
<style>
  .content-wrapper .content .content-left{ 
        overflow: visible;  
        float: left;
        width: 850px;
        margin-right: 10px;
        background-color: white;
        padding-bottom: 20px;
    }
    .menu-items-accodings{
        width: 100% !important;
    }
</style>
