<?php
require_once '../include/config.inc.php';
require_once '../include/adminFunctions.inc.php';

if (isset($_REQUEST['export_stats'])) {
// Download the file
    $filename = "active_ads_stats_payment.csv";
    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename=' . $filename);
    header("Pragma: no-cache");
    header("Expires: 0");
    echo "\xEF\xBB\xBF";
// Fetch region

    $output = '';
    $output.='Website ,';
    $sql = "SELECT * FROM tbl_Advertisement_Type WHERE AT_ID NOT IN(3,4)";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $count = 0;
    while ($rowAT = mysql_fetch_assoc($result)) {
        $count++;
        $AT_ID[$count] = $rowAT['AT_ID'];
        if ($rowAT['AT_ID'] == 4) {
            $output .='Homepage , ';
        } else {
            $output .= $rowAT['AT_Name'] . ',';
        }
    }
    $output .= ' Total ';
    $output .="\n";
    echo $output;

    $totalCount = 0;
    $totalRevenue = 0;
    $sql = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Domain != '' ORDER BY R_Name ASC";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($rowRegion = mysql_fetch_assoc($result)) {
        $totalCountRegionBased = 0;
        $totalRevenueRegionBased = 0;
        $output = $rowRegion['R_Name'] . ' ,';
        for ($i = 1; $i <= $count; $i++) {
            $record = show_ads_stats($rowRegion['R_ID'], $AT_ID[$i]);
            $totalCountRegionBased += $record['count'];
            $totalRevenueRegionBased += $record['revenue'];
            $totalCountTypeBased[$i] += $record['count'];
            $totalRevenueTypeBased[$i] += $record['revenue'];

            $output .= $record['count'] . " ($" . $record['revenue'] . ") ,";
        }
        $output .= $totalCountRegionBased . " ($" . $totalRevenueRegionBased . ") ,";
        $output .="\n";
        echo $output;
    }

    $output = "Totals ,";

    for ($j = 1; $j <= $count; $j++) {
        $totalCount += $totalCountTypeBased[$j];
        $totalRevenue += $totalRevenueTypeBased[$j];
        $output .= $totalCountTypeBased[$j] . " ($" . $totalRevenueTypeBased[$j] . ") ,";
    }
    $output .= $totalCount . " ($" . $totalRevenue . ") ,";

    $output .="\n";
    echo $output;
}
?>



