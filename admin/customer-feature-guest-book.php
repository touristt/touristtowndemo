<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';
header("Content-type: text/html; charset=utf-8");

if (!in_array('listing-tools', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
}

$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
} else {
    header("Location:customers.php");
    exit();
}
if (isset($_POST['button_guest-book'])) {
    $daily_description = $_POST['guest-book-description'];
    $sql_daily_des = mysql_query("SELECT * from tbl_Business_Feature_Guest_Book_Description WHERE BFGB_BL_ID = '$BL_ID'");
    $count = mysql_num_rows($sql_daily_des);
    if ($count > 0) {
        $sql_daily_des_update = "UPDATE tbl_Business_Feature_Guest_Book_Description SET BFGB_Description = '$daily_description' WHERE BFGB_BL_ID = '$BL_ID'";
        $result = mysql_query($sql_daily_des_update);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Guest Book','','Update Description','super admin');
    } else {
        $sql_daily_des_insert = "INSERT tbl_Business_Feature_Guest_Book_Description SET BFGB_BL_ID = '$BL_ID', BFGB_Description = '$daily_description'";
        $result = mysql_query($sql_daily_des_insert);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Guest Book','','Add Description','super admin');
    }
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);   
    }
    else
    {
    header("Location: /admin/customer-feature-guest-book.php?bl_id=" . $BL_ID);
    }
    
    exit;
}
$sql_description_daily = mysql_query("SELECT * from tbl_Business_Feature_Guest_Book_Description WHERE BFGB_BL_ID= '$BL_ID'");
$data_description = mysql_fetch_assoc($sql_description_daily);
if (isset($_POST['Approve'])) {
    $BFGB_ID = $_POST['BFGB_ID'];
    $sql_guest_status_update = "UPDATE tbl_Business_Feature_Guest_Book SET BFGB_Status = '1' WHERE BFGB_ID= '$BFGB_ID'";
    $result = mysql_query($sql_guest_status_update);
    if ($result) {
        update_pointsin_business_tbl($BL_ID);
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Guest Book','','Approve Review','super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: /admin/customer-feature-guest-book.php?bl_id=" . $BL_ID);
    exit;
}
require_once '../include/admin/header.php';
?>
<div class="content-left">

    <?php require_once '../include/top-nav-listing.php'; ?>

    <div class="title-link">
        <div class="title">Add Ons - <?php echo  $rowListing['BL_Listing_Title'] ?></div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-listing-admin.php'; ?>
    </div>

    <div class="right">
        <div class="content-header">
            <div class="title">Guest Book</div>
            <div class="link">
                <?php
                $guestBook = show_addon_points(12);
                if ($numRowsGB > 0) {
                    echo '<div class="points-com">' . $guestBook . ' pts</div>';
                } else {
                    echo '<div class="points-uncom">' . $guestBook . ' pts</div>';
                }
                ?>
            </div>
        </div>

        <?php
        $help_text = show_help_text('Guest Book');
        if ($help_text != '') {
            echo '<div class="form-inside-div border-none">' . $help_text . '</div>';
        }
        ?>
        <form name="form1" method="post"  action="">
            <div class="daily-special-specifivation">
                <div class="form-inside-div add-menu-item Description-width">
                    <input type="hidden" name="id" id="daily_bl_id" value="<?php echo  $BL_ID ?>"> 
                    <label>Description</label>
                    <div class="form-data add-menu-item-field wiziwig-about">
                        <textarea name="guest-book-description" class="Description-textarea description-ckeditor" id="daily-description"  cols="60" rows="3" ><?php echo  $data_description['BFGB_Description'] ?></textarea>
                    </div>
                </div>
                <div class="form-inside-div add-menu-item-button">
                    <div class="button">
                        <input type="submit" name="button_guest-book" value="Save" />
                    </div>      
                </div> 
            </div>      
        </form>  
        <div class="form-inside-div heading-border-bottom">
            <label class="guest-book-heading">New Comment</label>
        </div>

        <?PHP
        $sql = "SELECT * FROM  tbl_Business_Feature_Guest_Book WHERE BFGB_BL_ID = '$BL_ID' AND BFGB_Status ='0' ORDER BY BFGB_Date DESC";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());

        while ($data = mysql_fetch_assoc($result)) {
            ?>
            <form name="guest-book" method="post" action="">
                <div class="data-content guest-book-container">
                    <input type="hidden" name="BFGB_ID" id="BFGB_ID" value="<?php echo $data['BFGB_ID']; ?>"> 
                    <input type="hidden" name="id" id="BFGB_ID" value="<?php echo $BL_ID; ?>"> 
                    <div class="form-inside-div guest-book-form first-div">
                        <label>Date</label>
                        <div class="form-data margin-top-guest">
                            <?php echo $data['BFGB_Date']; ?>
                        </div>
                    </div>
                    <div class="form-inside-div guest-book-form">
                        <label>Ranking</label>
                        <div class="form-data margin-top-guest rating-image">
                            <?php
                            $rating = $data['BFGB_Rating'];
                            if ($rating == '1') {
                                ?>
                                <img src="../include/img/selectr.png">
                                <img src="../include/img/blank.png">
                                <img src="../include/img/blank.png">
                                <img src="../include/img/blank.png">
                                <img src="../include/img/blank.png">
                                <?php
                            } else if ($rating == '2') {
                                ?>
                                <img src="../include/img/selectr.png">
                                <img src="../include/img/selectr.png">
                                <img src="../include/img/blank.png">
                                <img src="../include/img/blank.png">
                                <img src="../include/img/blank.png">
                                <?php
                            } else if ($rating == '3') {
                                ?>
                                <img src="../include/img/selectr.png">
                                <img src="../include/img/selectr.png">
                                <img src="../include/img/selectr.png">
                                <img src="../include/img/blank.png">
                                <img src="../include/img/blank.png">
                                <?php
                            } else if ($rating == '4') {
                                ?>
                                <img src="../include/img/selectr.png">
                                <img src="../include/img/selectr.png">
                                <img src="../include/img/selectr.png">
                                <img src="../include/img/selectr.png">
                                <img src="../include/img/blank.png">
                                <?php
                            } else if ($rating == '5') {
                                ?>
                                <img src="../include/img/selectr.png">
                                <img src="../include/img/selectr.png">
                                <img src="../include/img/selectr.png">
                                <img src="../include/img/selectr.png">
                                <img src="../include/img/selectr.png">
                                <?php
                            } else {
                                ?>
                                <img src="../include/img/blank.png">
                                <img src="../include/img/blank.png">
                                <img src="../include/img/blank.png">
                                <img src="../include/img/blank.png">
                                <img src="../include/img/blank.png">
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="form-inside-div guest-book-form">
                        <label>Name</label>
                        <div class="form-data margin-top-guest">
                            <?php echo $data['BFGB_Name']; ?>
                        </div>
                    </div>
                    <div class="form-inside-div guest-book-form">
                        <label>Title</label>
                        <div class="form-data margin-top-guest">
                            <?php echo $data['BFGB_Title']; ?>
                        </div>
                    </div>
                    <div class="form-inside-div guest-book-form">
                        <label>Description</label>
                        <div class="form-data margin-top-guest desctpion-width">
                            <?php echo $data['BFGB_Desc']; ?>
                        </div>
                    </div>
                    <div class="form-inside-div guest-book-form">
                        <input type="submit" name="Approve" class="" value="Approve Review" />
                        <a href="#!" onclick="deleteitem(<?php echo $data['BFGB_ID'] . ", " . $BL_ID ?>)">Decline & Delete</a>
                    </div>
                </div>
            </form>
            <?php
        }
        ?>
        <div class="data-content guest-book-container">
            <div class="form-inside-div heading-border-bottom margin-left-none comment-heading-width">
                <label class="guest-book-heading">Manage Comments</label>
            </div>
            <div class="form-inside-div guest-coment-detail">
                <div class="data-comment-date">
                    Date
                </div>
                <div class="data-comment-option">
                    Delete
                </div>
                <div class="data-comment-title">
                    Title
                </div>

            </div>

            <?PHP
            $sql_comments = "SELECT * FROM  tbl_Business_Feature_Guest_Book WHERE BFGB_BL_ID = '$BL_ID' AND BFGB_Status ='1' ORDER BY BFGB_Date DESC";
            $result_comments = mysql_query($sql_comments, $db) or die("Invalid query: $sql -- " . mysql_error());

            while ($data_comment = mysql_fetch_assoc($result_comments)) {
                ?>
                <div class="form-inside-div guest-coment-detail">
                    <div class="data-comment-date">
                        <?php echo $data_comment['BFGB_Date']; ?> 
                    </div>
                    <div class="data-comment-option font-color">
                        <a href="#!" onclick="deleteitem(<?php echo $data_comment['BFGB_ID'] . ", " . $BL_ID ?>)">Delete</a>
                    </div>
                    <div class="data-comment-title font-color">
                        <?php echo $data_comment['BFGB_Title']; ?>
                    </div>

                </div>
                <?php
            }
            ?>
        </div>

        <?php
        $sql1 = "SELECT * FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = 12";
        $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
        $row1 = mysql_fetch_assoc($result1);
        if ($row1['BLF_BL_ID'] != '') {
            ?>
            <div class="form-inside-div border-none text-align-center">
                <a href="customer-feature-add-ons.php?id=<?php echo $row1['BLF_ID'] ?>&bl_id=<?php echo $BL_ID ?>"  onclick="return confirm('Are you sure you want to <?php echo  $row1['BLF_Active'] == 1 ? "deactivate" : "activate" ?> this Add on?');"><?php echo  $row1['BLF_Active'] == 1 ? "Deactivate" : "Activate" ?> This Add on</a>
            </div>
            <?PHP
        }
        ?>

    </div>
</div>
<script>
    CKEDITOR.disableAutoInline = true;
    $( document ).ready( function() {
        $( '#daily-description').ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
    });

    function deleteitem(BFGB_item_ID, BL_ID){
        if(confirm('Are you sure you want to delete?')) {                               
            $.post("customer-feature-guest-book-delete.php",{
                BFGB_ID_item: BFGB_item_ID,
                BL_ID: BL_ID
            }, function(result){
                if(result == 1){
                    location.reload();
                }
            });
        }
        else
        {
            return false;
        }
    }
</script>
<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>