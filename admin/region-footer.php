<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} elseif ($_REQUEST['newRegion'] <> true) {
    header("Location: /admin/regions.php");
    exit();
}

$sql = "SELECT R_ID, R_Name, R_Type, R_Footer_Text, R_Mobile_Footer FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $sql = "tbl_Region SET 
            R_Mobile_Footer = '" . encode_strings(trim(str_replace('<p>', '', str_replace('</p>', '', $_REQUEST['mobile_footer']))), $db) . "',
            R_Footer_Text = '" . encode_strings($_REQUEST['footer_text'], $db) . "'";

    $sql = "UPDATE " . $sql . " WHERE R_ID = '" . encode_strings($regionID, $db) . "'";

    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $regionID;
        Track_Data_Entry('Websites', $id, 'Manage Footer', '', 'Update Footer Description', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: region-footer.php?rid=" . $regionID);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Manage Footer</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <!--Footer Section 1-->
        <div class="content-header"> 
            <div class="title">Footer Logos(Section - 1)</div>
            <div class="link"><a href="/admin/region-footer-actions.php?rid=<?php echo $activeRegion['R_ID'] ?>&section=1">+ Add Photo</a></div>
        </div>
        <div class="content-sub-header">
            <div class="data-column rfp-title padding-none">Alt</div>
            <div class="data-column rfp-other padding-none"></div>
            <div class="data-column rfp-other padding-none">View</div>
            <div class="data-column rfp-other padding-none">Edit</div>
            <div class="data-column rfp-other padding-none">Delete</div>
        </div>
        <div class="brief">
        <?php
        $sql = "SELECT FP_ID, FP_Alt, FP_Photo FROM tbl_Footer_Photo WHERE FP_R_ID = '" . encode_strings($activeRegion['R_ID'], $db) . "' 
                AND FP_Section = 1 ORDER BY FP_Order ASC";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $rowCount = mysql_num_rows($result);
        $counter = 0;
        while ($row = mysql_fetch_array($result)) {
            $counter++;
            ?>
                <div class="data-content" id="recordsArray_<?php echo $row['FP_ID'] ?>">
                    <div class="data-column rfp-title"><?php echo $row['FP_Alt'] ?></div>
                    <div class="data-column rfp-other"></div>
                <div class="data-column rfp-other">
                    <?PHP
                    if ($row['FP_Photo']) {
                        echo '<a href="' . IMG_LOC_REL . $row['FP_Photo'] . '" target="_blank">View</a>';
                    } else {
                        echo "No Photo";
                    }
                    ?>
                </div>
                    <div class="data-column rfp-other"><a href="region-footer-actions.php?id=<?php echo $row['FP_ID'] ?>&section=1">Edit</a></div>
                    <div class="data-column rfp-other"><a onClick="return confirm('Are you sure?')" href="region-footer-actions.php?id=<?php echo $row['FP_ID'] ?>&amp;op=del">Delete</a></div>
            </div>
        <?php } ?>
        </div>

        <!--Footer Section 2-->
        <div class="content-header"> 
            <div class="title">Footer Logos(Section - 2)</div>
            <div class="link"><a href="/admin/region-footer-actions.php?rid=<?php echo $activeRegion['R_ID'] ?>&section=2">+ Add Photo</a></div>
        </div>
        <div class="content-sub-header">
            <div class="data-column rfp-title padding-none">Alt</div>
            <div class="data-column rfp-other padding-none"></div>
            <div class="data-column rfp-other padding-none">View</div>
            <div class="data-column rfp-other padding-none">Edit</div>
            <div class="data-column rfp-other padding-none">Delete</div>
        </div>
        <div class="brief">
        <?php
        $sql = "SELECT FP_ID, FP_Alt, FP_Photo FROM tbl_Footer_Photo WHERE FP_R_ID = '" . encode_strings($activeRegion['R_ID'], $db) . "' 
                AND FP_Section = 2 ORDER BY FP_Order ASC";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $rowCount = mysql_num_rows($result);
        $counter = 0;
        while ($row = mysql_fetch_array($result)) {
            $counter++;
            ?>
                <div class="data-content" id="recordsArray_<?php echo $row['FP_ID'] ?>">
                    <div class="data-column rfp-title"><?php echo $row['FP_Alt'] ?></div>
                    <div class="data-column rfp-other"></div>
                <div class="data-column rfp-other">
                    <?PHP
                    if ($row['FP_Photo']) {
                        echo '<a href="' . IMG_LOC_REL . $row['FP_Photo'] . '" target="_blank">View</a>';
                    } else {
                        echo "No Photo";
                    }
                    ?>
                </div>
                    <div class="data-column rfp-other"><a href="region-footer-actions.php?id=<?php echo $row['FP_ID'] ?>&section=2">Edit</a></div>
                    <div class="data-column rfp-other"><a onClick="return confirm('Are you sure?')" href="region-footer-actions.php?id=<?php echo $row['FP_ID'] ?>&amp;op=del">Delete</a></div>
            </div>
        <?php } ?>
        </div>
        <!--Footer Section 3-->
        <div class="content-header"> 
            <div class="title">Footer Logos(Section - 3)</div>
            <div class="link"><a href="/admin/region-footer-actions.php?rid=<?php echo $activeRegion['R_ID'] ?>&section=4">+ Add Photo</a></div>
        </div>
        <div class="content-sub-header">
            <div class="data-column rfp-title padding-none">Alt</div>
            <div class="data-column rfp-other padding-none"></div>
            <div class="data-column rfp-other padding-none">View</div>
            <div class="data-column rfp-other padding-none">Edit</div>
            <div class="data-column rfp-other padding-none">Delete</div>
        </div>
        <div class="brief">
        <?php
        $sql = "SELECT FP_ID, FP_Alt, FP_Photo FROM tbl_Footer_Photo WHERE FP_R_ID = '" . encode_strings($activeRegion['R_ID'], $db) . "' 
                AND FP_Section = 4 ORDER BY FP_Order ASC";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $rowCount = mysql_num_rows($result);
        $counter = 0;
        while ($row = mysql_fetch_array($result)) {
            $counter++;
            ?>
                <div class="data-content" id="recordsArray_<?php echo $row['FP_ID'] ?>">
                    <div class="data-column rfp-title"><?php echo $row['FP_Alt'] ?></div>
                    <div class="data-column rfp-other"></div>
                <div class="data-column rfp-other">
                    <?PHP
                    if ($row['FP_Photo']) {
                        echo '<a href="' . IMG_LOC_REL . $row['FP_Photo'] . '" target="_blank">View</a>';
                    } else {
                        echo "No Photo";
                    }
                    ?>
                </div>
                    <div class="data-column rfp-other"><a href="region-footer-actions.php?id=<?php echo $row['FP_ID'] ?>&section=4">Edit</a></div>
                    <div class="data-column rfp-other"><a onClick="return confirm('Are you sure?')" href="region-footer-actions.php?id=<?php echo $row['FP_ID'] ?>&amp;op=del">Delete</a></div>
            </div>
        <?php } ?>
        </div>
        <!--Footer Section 4-->
        <div class="content-header"> 
            <div class="title">Footer Logos(Section - 4)</div>
            <div class="link"><a href="/admin/region-footer-actions.php?rid=<?php echo $activeRegion['R_ID'] ?>&section=5">+ Add Photo</a></div>
        </div>
        <div class="content-sub-header">
            <div class="data-column rfp-title padding-none">Alt</div>
            <div class="data-column rfp-other padding-none"></div>
            <div class="data-column rfp-other padding-none">View</div>
            <div class="data-column rfp-other padding-none">Edit</div>
            <div class="data-column rfp-other padding-none">Delete</div>
        </div>
        <div class="brief">
        <?php
        $sql = "SELECT FP_ID, FP_Alt, FP_Photo FROM tbl_Footer_Photo WHERE FP_R_ID = '" . encode_strings($activeRegion['R_ID'], $db) . "' 
                AND FP_Section = 5 ORDER BY FP_Order ASC";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $rowCount = mysql_num_rows($result);
        $counter = 0;
        while ($row = mysql_fetch_array($result)) {
            $counter++;
            ?>
                <div class="data-content" id="recordsArray_<?php echo $row['FP_ID'] ?>">
                    <div class="data-column rfp-title"><?php echo $row['FP_Alt'] ?></div>
                    <div class="data-column rfp-other"></div>
                <div class="data-column rfp-other">
                    <?PHP
                    if ($row['FP_Photo']) {
                        echo '<a href="' . IMG_LOC_REL . $row['FP_Photo'] . '" target="_blank">View</a>';
                    } else {
                        echo "No Photo";
                    }
                    ?>
                </div>
                    <div class="data-column rfp-other"><a href="region-footer-actions.php?id=<?php echo $row['FP_ID'] ?>&section=5">Edit</a></div>
                    <div class="data-column rfp-other"><a onClick="return confirm('Are you sure?')" href="region-footer-actions.php?id=<?php echo $row['FP_ID'] ?>&amp;op=del">Delete</a></div>
            </div>
        <?php } ?>
        </div>
        <!--Footer Section 5-->
        <div class="content-header"> 
            <div class="title">Footer Text Links(Section - 5)</div>
            <div class="link"><a href="/admin/region-footer-actions.php?rid=<?php echo $activeRegion['R_ID'] ?>&section=3">+ Add Link</a></div>
        </div>
        <div class="content-sub-header">
            <div class="data-column rfp-title padding-none">Link Text</div>
            <div class="data-column rfp-other padding-none"></div>
            <div class="data-column rfp-other padding-none"></div>
            <div class="data-column rfp-other padding-none">Edit</div>
            <div class="data-column rfp-other padding-none">Delete</div>
        </div>
        <div class="brief">
            <?php
            $sql3 = "SELECT FP_ID, FP_Alt, FP_Photo FROM tbl_Footer_Photo WHERE FP_R_ID = '" . encode_strings($activeRegion['R_ID'], $db) . "' 
                     AND FP_Section = 3 ORDER BY FP_Order ASC";
            $result3 = mysql_query($sql3, $db) or die("Invalid query: $sql3 -- " . mysql_error());
            $counter3 = 0;
            while ($row3 = mysql_fetch_array($result3)) {
                $counter3++;
                ?>
                <div class="data-content" id="recordsArray_<?php echo $row3['FP_ID'] ?>">
                    <div class="data-column rfp-title"><?php echo $row3['FP_Alt'] ?></div>
                    <div class="data-column rfp-other"></div>
                    <div class="data-column rfp-other">
                    </div>
                    <div class="data-column rfp-other"><a href="region-footer-actions.php?id=<?php echo $row3['FP_ID'] ?>&section=3">Edit</a></div>
                    <div class="data-column rfp-other"><a onClick="return confirm('Are you sure?')" href="region-footer-actions.php?id=<?php echo $row3['FP_ID'] ?>&amp;op=del">Delete</a></div>
                </div>
            <?php } ?>
        </div>

        <!--Footer Description-->
        <form action="region-footer.php" method="post" name="form">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>" id="rid">
            <!--Footer work-->
            <div class="content-header">Footer Description</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Footer Text</label>
                <div class="form-data">
                    <textarea name="footer_text" cols="85" rows="10" class="tt-description"><?php echo $activeRegion['R_Footer_Text'] ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Mobile Footer</label>
                <div class="form-data">
                    <textarea name="mobile_footer" cols="85" rows="10" class="tt-description"><?php echo $activeRegion['R_Mobile_Footer'] ?></textarea>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="button" id="button22" value="Submit" />
                </div>
            </div>
        </form>
    </div>
    <div id="dialog-message"></div>
</div>
<script>
    $(function() {
        $(".brief").sortable({opacity: 0.9, cursor: 'move', update: function() {
                var rid = $("#rid").val();
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Footer_Photo&field=FP_Order&id=FP_ID&rid='+rid;
                $.post("reorder.php", order, function(theResponse) {
                    $("#dialog-message").html("Footer Logos Re-Ordered");
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            Ok: function() {
                                $(this).dialog("close");
                            }
                        }
                    });
                });
            }
        });
    });
</script>
<?PHP
require_once '../include/admin/footer.php';
?>
