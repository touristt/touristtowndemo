<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
$regionLimit_id = '';
if (!in_array('customers', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Coupons - Statistics</div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-coupon.php'; ?>
    </div>

    <div class="right">
        <form name="listing_form" method="post" action="#">

            <div class="content-sub-header">
                <div class="data-column spl-other state padding-none">Total Active</div>
                <div class="data-column spl-other state padding-none">Total Inactive</div>
                <div class="data-column spl-other state padding-none">Expired</div>
                <div class="data-column spl-other state padding-none">Total</div>
            </div>
            <?php
            // SELECTING ACTIVE LISTINGS .W.R.T. LISTING TYPE
            $query = "SELECT BL_ID, BFC_ID, BFC_Title, BL_Listing_Title FROM tbl_Business_Feature_Coupon
            LEFT JOIN tbl_Business_Listing ON BFC_BL_ID = BL_ID
            WHERE BFC_Status = 1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date =0000-00-00)";
            $result1 = mysql_query($query, $db) or die("Invalid query: $query -- " . mysql_error());
            $count1 = mysql_num_rows($result1);

            // SELECTING INACTIVE LISTINGS W.R.T. LISTING TYPE
            $query = "SELECT BL_ID, BFC_ID, BFC_Title, BL_Listing_Title FROM tbl_Business_Feature_Coupon
            LEFT JOIN tbl_Business_Listing ON BFC_BL_ID = BL_ID
            WHERE BFC_Status != 1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date =0000-00-00)";
            $result2 = mysql_query($query, $db) or die("Invalid query: $query -- " . mysql_error());
            $count2 = mysql_num_rows($result2);

            // SELECTING Expired LISTINGS W.R.T. LISTING TYPE
            $query = "SELECT BL_ID, BFC_ID, BFC_Title, BL_Listing_Title FROM tbl_Business_Feature_Coupon
            LEFT JOIN tbl_Business_Listing ON BFC_BL_ID = BL_ID
            WHERE BFC_Expiry_Date < CURDATE() AND BFC_Expiry_Date !=0000-00-00";
            $result3 = mysql_query($query, $db) or die("Invalid query: $query -- " . mysql_error());
            $count3 = mysql_num_rows($result3);
            ?>
            <div class="data-content">

                <div class="data-column spl-other state">
                    <?php echo $count1; ?>
                </div>
                <div class="data-column spl-other state">
                    <?php echo $count2; ?>
                </div>
                <div class="data-column spl-other state">
                    <?php echo $count3 ?>
                </div>
                <div class="data-column spl-other state">
                    <?php
                    echo $total_counts = $count1 + $count2 + $count3;
                    ?>
                </div>
            </div>
            <!--Download accodings start -->
            <div class="content-header content-header-search content-region download">
                <span>Coupon Downloads</span> 
            </div>
        <div class="menu-items-accodings">
            <div id="accordion">
                <?php
                $years = array();
                $months = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
                $year_current = date('Y');
                $month_current = date('m', strtotime('+1 month'));
                for ($start_year = 2017; $start_year <= $year_current; $start_year++) {
                    $years[] = $start_year;
                } 
                $years = array_reverse($years);
                foreach ($years as $year) {
                    ?>
                    <h3 class="accordion-rows"><span class="accordion-title"><?php echo $year; ?></span></h3> 
                    <div class="sub-accordions accordion-padding">
                        <div class="content-sub-header">
                            <div class="data-column spl-other download padding-none">Month</div>
                            <div class="data-column spl-other download padding-none">Downloads</div>
                        </div>
                        <?php
                        $mailSent = 0;
                        foreach ($months as $month) { 
                            if ($year == $year_current && $month == $month_current) {
                                break;
                            }
                            ?>
                            <div class="data-content">
                                <div class="data-column spl-other download">
                                    <?php
                                    print date('M', strtotime($year . '-' . $month . '-' . '01'));
                                    ?>
                                </div>
                                <div class="data-column spl-other download">
                                    <?php
                                    // SELECTING Mail Sending 
                                    $total_Sent_Mail = 0;
                                    $sql_sent = "SELECT CU_ID FROM tbl_Coupon_Usages
                                                 WHERE MONTH(CU_Date) = '" . $month . "' AND YEAR(CU_Date) = '" . $year . "'";
                                    $result_count = mysql_query($sql_sent, $db) or die("Invalid query: $sql_sent -- " . mysql_error());
                                    $total_Sent_Mail = mysql_num_rows($result_count);
                                    $mailSent += $total_Sent_Mail;
                                    echo $total_Sent_Mail;
                                    ?>
                                </div>
                            </div>            
                            <?php
                            
                        }
                        ?> 
                        <div class="content-sub-header">
                            <div class="data-column spl-other download padding-none">Total :</div>                            
                            <div class="data-column spl-other download padding-none">
                                <?php
                                echo $mailSent;
                                ?>
                            </div>                            
                        </div>       
                    </div>
                    <?php
                } 
                ?>
            </div>
        </div>
            
        <!--Download end acconrdion-->

    </div>
</div>
<script>
    $(function () {

        $('.accordion-rows a').click(function (event) {
            if ($(this).parent().parent().hasClass("ui-accordion-header")) {
                event.stopPropagation(); // this is
            }
        });
        $("#accordion").accordion({
            collapsible: true,
            heightStyle: 'content',
            active: 0
        });
        $('.token-input-dropdown').css("width", "400px");
    });
</script>
<style>
    .menu-items-accodings{width: 830px !important; margin-top: 10px;}
</style> 
<?PHP
require_once '../include/admin/footer.php';
?>