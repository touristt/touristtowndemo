<?php
include '../include/config.inc.php';
include '../include/accommodation_functions.php';
require_once '../include/login.inc.php';
require_once '../include/admin/header.php';
include '../include/accommodation_header.php';
/* 1 for activation email and 2 for launch */
$result = activation_and_launch_emails(1);
?>
<?php if (isset($_SESSION['message'])) { ?>
    <div class="message">
        <?php
        echo $_SESSION['message'];
        unset($_SESSION['message']);
        ?>
    </div>
<?php } ?>
<div class="content-left full-width ">
    <div class="title-link">
        <div class="title">
            Activation Email Template
        </div>
    </div>
    <div class="left">
        <?PHP
        require '../include/nav-B-admin-email.php';
        ?>
    </div>
    <div class="right">
        <form name="activation_email_tpl" method="post" action="activation_email_tpl_action.php" enctype="multipart/form-data">
            <!--email image-->
            <div class="form-inside-div form-inside-div-width-admin-accommodation "> 
                <label>Header Image<br> 640 X 240 </label>
                <div class="form-data">
                    <div class="image-container">
                        <?php if (isset($result->img)) { ?> 
                            <img src="<?php echo $result->img; ?>" width="640" height="240"/> 
                            <?php
                        } else {
                            echo'<div style="margin-top:85px;font-size:40px;">image 640px X 200px</div>';
                        }
                        ?>
                    </div> 
                    <div style="margin-top:20px;float:left;">
                        <input type="file" name="img"/>
                    </div>
                </div>
            </div>


            <!--email body-->
            <div class="form-inside-div form-inside-div-width-admin-accommodation "> 
                <label>Body Copy</label>
                <div class="form-data acco-email-page"><textarea class="ckeditor" name="body_txt"><?php
                        if (isset($result->footer_txt)) {
                            echo $result->body_txt;
                        }
                        ?></textarea> </div>
            </div>    
            <div class="form-inside-div form-inside-div-width-admin-accommodation "> 
                <label>Footer</label>
                <div class="form-data acco-email-page"><div><textarea class="ckeditor" name="footer_txt"><?php
                        if (isset($result->footer_txt)) {
                            echo $result->footer_txt;
                        }
                        ?></textarea></div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin-accommodation ">
                <div class="button">
                    <input type="hidden" name="template_id" value="<?php if (isset($result->id)) {
                            echo $result->id;
                        } ?>"/>
                    <input type="hidden" name="old_path" value="<?php if (isset($result->img)) {
                            echo $result->img;
                        } ?>"/>
                    <input type="submit" name="activation_email_btn" value="Submit"/>

                </div>
            </div>
        </form>
    </div>
</div>
<!--pending blocks ends-->

<?PHP
require_once '../include/admin/footer.php';
?>