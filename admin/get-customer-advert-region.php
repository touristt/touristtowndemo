<?php
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

$listing_id = $_GET['bl_id'];
// get regions and categories of this listing
$sql_region = "SELECT BL_C_ID, BLC_C_ID, BLC_BL_ID, BLCR_BLC_R_ID, BL_Points, R_Parent FROM tbl_Business_Listing
                LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID 
                INNER JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                WHERE BLC_BL_ID = '$listing_id'";
$res_region = mysql_query($sql_region);
$regions = array();
while ($r = mysql_fetch_assoc($res_region)) {
    // add region parent as well if available
    if ($r['R_Parent'] != 0 && !in_array($r['R_Parent'], $regions)) {
        $select = "SELECT R_ID, R_Domain FROM tbl_Region_Multiple LEFT JOIN tbl_Region ON RM_Parent = R_ID WHERE RM_Child = '". $r['BLCR_BLC_R_ID'] ."'";
        $result = mysql_query($select, $db) or die("Invalid query: $select -- " . mysql_error());
        $parent = mysql_fetch_assoc($result);
        if($parent['R_Domain'] != "" && !in_array($parent['R_ID'], $regions)){
            $regions[] = $parent['R_ID'];
        }
    }
    // add region
    if ($r['BLCR_BLC_R_ID'] != '' && !in_array($r['BLCR_BLC_R_ID'], $regions)) {
        $regions[] = $r['BLCR_BLC_R_ID'];
    }
}
?>
<select class="adv-type-options" id="region-id" name="domain" onChange="validate_buy_an_add(0, 1, 0, 0, 0);" required>
    <option required value="">Select Website</option>
    <?php
    foreach ($regions as $key => $r) {
        $sql = "SELECT R_ID, R_Domain FROM tbl_Region WHERE R_ID = '$r' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $region = mysql_fetch_assoc($result);
        print '<option value="' . $region['R_ID'] . '">' . $region['R_Domain'] . '</option>';
    }
    ?>
</select>