<?php
include '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/admin/header.php';
include '../include/accommodation_header.php';
?>
<?php if (isset($_SESSION['message'])) { ?>
    <div class="message">
        <?php
        echo $_SESSION['message'];
        unset($_SESSION['message']);
        ?>
    </div>
    <?php
}
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">
            Add Group Blocks
        </div>
        <div class="addlisting">
        </div> 
    </div>
    <div class="left">
    </div>
    <div class="right">
        <form name="add_group_block" method="post" action="add_group_block_action.php">
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Group Block Name</label>
                <div class="full-width-form-data">
                    <input type="text" name="group_block_name" required="required" class="add_group_txt"/>
                    <input type="submit" name="btn_save" value="Save" class="add_group_btn"/>
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>