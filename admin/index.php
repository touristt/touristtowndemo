<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Dashboard</div>
        <div class="link">
        </div>
    </div> 
    <div class="right dashboard-full-width">
        <?php
        $help_text = show_help_text('Dashboard');
        if ($help_text != '') {
            echo '<div class="data-content border-none"><div class="data-column" style="width:100%;text-align: left;">' . $help_text . '</div></div>';
        }
        /// Pending image
        if (in_array('image-bank', $_SESSION['USER_PERMISSIONS'])) {
            $imageBank = "SELECT IB_ID FROM tbl_Image_Bank WHERE 
                IB_Name = '' 
                OR IB_Keyword = '' 
                OR IB_Path = '' 
                OR IB_Region = '' 
                OR IB_Category = '0' 
                OR IB_Season = '0' 
                OR IB_People = '0' 
                OR IB_Photographer = '' 
                OR IB_Location = '' 
                OR IB_Owner = '0'";
            $resImg = mysql_query($imageBank, $db) or die("Invalid query: $imageBank -- " . mysql_error());
            $countImg = mysql_num_rows($resImg);
            ?>
            <div class="content-sub-header">
                <div class="data-column" style="width:100%;text-align: left;">Pending Images in Image Bank</div>
            </div>
            <div class="data-content">
                <div class="data-column" style="width:100%;text-align: left;"><a href="pending-image-bank.php">Pending images (<?php echo $countImg; ?>)</a></div>
            </div>
            <?php
        }
        /// Pending Listings
//        if (in_array('business-listings', $_SESSION['USER_PERMISSIONS'])) {
        if (in_array('superadmin', $_SESSION['USER_ROLES'])) {
            $sql_listings = "SELECT DISTINCT BL_ID FROM tbl_Business_Listing
                            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                            WHERE BLC_ID IS NULL OR BLCR_ID IS NULL";
            $result_listings = mysql_query($sql_listings);
            $count_listings = mysql_num_rows($result_listings);
            ?>
            <div class="content-sub-header">
                <div class="data-column" style="width:100%;text-align: left;">Pending Listings</div>
            </div>
            <div class="data-content">
                <div class="data-column" style="width:100%;text-align: left;"><a href="pending-listings.php">Pending Listings (<?php echo $count_listings ?>)</a></div>
            </div>
            <?php
            /// Unverified Listings
            $sql_unverified_listings = "SELECT DISTINCT BL_ID FROM tbl_Business_Listing WHERE BL_Free_Listing_status = 1 AND BL_Listing_Type = 1";
            $result_unverified_listings = mysql_query($sql_unverified_listings);
            $count_unverified_listings = mysql_num_rows($result_unverified_listings);
            ?>
            <div class="content-sub-header">
                <div class="data-column" style="width:100%;text-align: left;">Unverified Listings</div>
            </div>
            <div class="data-content">
                <div class="data-column" style="width:100%;text-align: left;"><a href="unverified-listings.php">Unverified Listings (<?php echo $count_unverified_listings; ?>)</a></div>
            </div>
            <?php
        }
        /// Pending Event
        if (in_array('manage-events', $_SESSION['USER_PERMISSIONS'])) {
            if (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
                $regionLimit = array();
                $limits = explode(',', $_SESSION['USER_LIMIT']);
                foreach ($limits as $limit) {
                    $sql = "SELECT R_ID, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    $region = mysql_fetch_assoc($result);
                    if (isset($region['R_Type']) && $region['R_Type'] == 1) {
                        $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($region['R_ID'], $db) . "'";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_assoc($result)) {
                            if ($row['R_ID'] != '' && !in_array($row['R_ID'], $regionLimit)) {
                                $regionLimit[] = $row['R_ID'];
                            }
                        }
                    } else {
                        if ($region['R_ID'] != '' && !in_array($region['R_ID'], $regionLimit)) {
                            $regionLimit[] = $region['R_ID'];
                        }
                    }
                }
            }
            $sql_event = "SELECT DISTINCT EventID FROM Events_master WHERE Pending = 1 AND EventDateEnd >= CURDATE()";
            if (!in_array('superadmin', $_SESSION['USER_ROLES']) && !in_array('admin', $_SESSION['USER_ROLES'])) {
                $first = true;
                $sql_event .= " AND (";
                foreach ($regionLimit as $e_region_id) {
                    if ($first) {
                        $first = false;
                    } else {
                        $sql_event .= " OR ";
                    }
                    $sql_event .= "FIND_IN_SET (" . $e_region_id . ",E_Region_ID )";
                }
                $sql_event .= " )";
            }
            $result_event = mysql_query($sql_event, $db) or die("Invalid query: $sql_event -- " . mysql_error());
            $count_event = mysql_num_rows($result_event);
            ?>
            <div class="content-sub-header">
                <div class="data-column" style="width:100%;text-align: left;">Pending Event</div>
            </div>
            <div class="data-content">
                <div class="data-column" style="width:100%;text-align: left;"><a href="events-pending.php">Pending events (<?php echo $count_event ?>)</a></div>
            </div>
            <?php
        }

        if (in_array('superadmin', $_SESSION['USER_ROLES'])) {
            /// Pending Request Content Change
            $sql_rcc = "SELECT RCC_ID FROM tbl_Request_Content_Change INNER JOIN tbl_Business_Listing ON BL_ID = RCC_BL_ID WHERE RCC_Status = 0";
            $result_rcc = mysql_query($sql_rcc, $db) or die("Invalid query: $sql_rcc -- " . mysql_error());
            $count_rcc = mysql_num_rows($result_rcc);
            ?>
            <div class="content-sub-header">
                <div class="data-column" style="width:100%;text-align: left;">Pending Request Content Change</div>
            </div>
            <div class="data-content">
                <div class="data-column" style="width:100%;text-align: left;"><a href="request-change-pending.php">Pending Request (<?php echo $count_rcc ?>)</a></div>
            </div>
            <?php
            /// Pending Coupon
            $sql_coupon = " SELECT BL_ID FROM tbl_Business_Feature_Coupon
                        LEFT JOIN tbl_Business_Listing ON BFC_BL_ID = BL_ID
                        WHERE BFC_Status = 0 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date =0000-00-00)";
            $result_coupon = mysql_query($sql_coupon, $db) or die("Invalid query: $sql_coupon -- " . mysql_error());
            $count_coupon = mysql_num_rows($result_coupon);
            ?>
            <div class="content-sub-header">
                <div class="data-column" style="width:100%;text-align: left;">Pending Coupon</div>
            </div>
            <div class="data-content">
                <div class="data-column" style="width:100%;text-align: left;"><a href="listings-coupon.php">Pending coupon (<?php echo $count_coupon ?>)</a></div>
            </div>
            <?php
        }
        /// Credit Card Payments Overdues
        if (in_array('billing-history', $_SESSION['USER_PERMISSIONS'])) {
            ?>
            <div class="content-sub-header">
                <div class="data-column" style="width:100%;text-align: left;">Credit Card Payments Overdues</div>
            </div>
            <?php
            $sql_card = "SELECT BL_Renewal_Date, BL_ID, BL_Listing_Title, PL_ID FROM tbl_Business_Listing
                        LEFT JOIN tbl_Payment_Logs ON BL_ID = PL_BL_ID
                        WHERE BL_Renewal_Date <= CURDATE() AND BL_Renewal_Date != '0000-00-00' AND BL_Payment_Type = 4 AND BL_Total > 0 
                        AND BL_Listing_Type = 4 AND hide_show_listing = 1 GROUP BY BL_ID";
            $result_card = mysql_query($sql_card, $db) or die("Invalid query: $sql_card -- " . mysql_error());
            while ($row_card = mysql_fetch_assoc($result_card)) {
                ?>
                <div class="data-content">
                    <div class="data-column" style="width:90%;text-align: left;">
                        <a href="customer-listing-billing.php?bl_id=<?php echo $row_card['BL_ID'] ?>"><?php echo $row_card['BL_Listing_Title'] . " (Due Date: " . $row_card['BL_Renewal_Date'] . ")" ?></a>
                    </div>
                    <?php
                    $sql_reason = "SELECT PL_ID, PL_Error_Msg, PL_DATE FROM tbl_Payment_Logs WHERE 
                                   PL_BL_ID = " . $row_card['BL_ID'] . " AND DATE(PL_DATE) >= '" . $row_card['BL_Renewal_Date'] . "'
                                   ORDER BY PL_ID DESC LIMIT 1";
                    $result_reason = mysql_query($sql_reason, $db) or die("Invalid query: $sql_reason -- " . mysql_error());
                    $row_reason = mysql_fetch_assoc($result_reason);
                    if ($row_reason['PL_ID'] > 0) {
                        ?>
                        <div class="data-column" style="text-align: right;width:10%;">
                            <a onclick="show_req(<?php echo $row_reason['PL_ID']; ?>);">Reason</a>
                        </div>
                        <div id="failure<?php echo $row_reason['PL_ID']; ?>" style=" display:none;">            
                            <div style="float:left; width:96%; padding:7px 2%; font-size:16px; border-bottom: 1px solid #cccccc;">
                                <span style="float: left; width: 25%; font-weight: bold;">Error Message:</span>
                                <div style="float:left; width: 75%;"><?php echo $row_reason['PL_Error_Msg']; ?></div>
                            </div>
                            <div style="float:left; width:96%; padding:7px 2%; font-size:16px; border-bottom: 1px solid #cccccc;">
                                <span style="float: left; width: 25%; font-weight: bold;">Date & Time:</span>
                                <div style="float:left; width: 75%;"><?php echo $row_reason['PL_DATE']; ?></div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php
            }
            ?>
            <!--Non-Credit Card overdues-->
            <div class="content-sub-header">
                <div class="data-column" style="width:100%;text-align: left;">Non-Credit Card Payments Overdues</div>
            </div>
            <?php
            $sql_noncard = "SELECT BL_Renewal_Date, BL_ID, BL_Listing_Title FROM tbl_Business_Listing    
                            WHERE BL_Renewal_Date <= CURDATE() AND BL_Renewal_Date != '0000-00-00' AND hide_show_listing = 1 
                            AND BL_Payment_Type != 4 AND BL_Total > 0 AND BL_Listing_Type = 4 GROUP BY BL_ID";
            $result_noncard = mysql_query($sql_noncard, $db) or die("Invalid query: $sql_noncard -- " . mysql_error());
            while ($row_noncard = mysql_fetch_assoc($result_noncard)) {
                ?>
                <div class="data-content">
                    <div class="data-column" style="width:100%;text-align: left;"><a href="customer-listing-billing.php?bl_id=<?php echo $row_noncard['BL_ID'] ?>"><?php echo $row_noncard['BL_Listing_Title'] . " (Due Date: " . $row_noncard['BL_Renewal_Date'] . ")" ?></div>
                </div>
                <?php
            }
            ?>
            <!--Yearly Upcoming Renewal in a Month-->
            <div class="content-sub-header">
                <div class="data-column" style="width:100%;text-align: left;">Yearly Upcoming Renewals This Month</div>
            </div>
            <?php
            $date = date("Y-m-d");
            $upcoming_date = date('Y-m-d', strtotime("+1 month"));
            $sql_upcoming = "SELECT BL_Renewal_Date, DATEDIFF(BL_Renewal_Date, CURDATE()) as timeRemaining, BL_ID, BL_Listing_Title
                            FROM tbl_Business_Listing
                            WHERE BL_Renewal_Date <= '$upcoming_date' AND BL_Renewal_Date > '$date' AND BL_Billing_type = -1
                            AND BL_Total != 0 AND hide_show_listing = 1 AND BL_Listing_Type = 4 GROUP BY BL_ID ORDER BY timeRemaining ASC";
            $result_upcoming = mysql_query($sql_upcoming, $db) or die("Invalid query: $sql_upcoming -- " . mysql_error());
            while ($row_upcoming = mysql_fetch_assoc($result_upcoming)) {
                ?>
                <div class="data-content">
                    <div class="data-column" style="width:100%;text-align: left;"><a href="customer-listing-billing.php?bl_id=<?php echo $row_upcoming['BL_ID'] ?>"><?php echo $row_upcoming['BL_Listing_Title'] . " (Due Date: " . $row_upcoming['BL_Renewal_Date'] . ")" ?></a></div>
                </div>
                <?php
            }
            ?>
            <?php
        } if (!in_array('superadmin', $_SESSION['USER_ROLES']) && !in_array('admin', $_SESSION['USER_ROLES'])) {
            echo '<div style="float:left; width: 100%; margin-top: 25px;">';
            if (in_array('town', $_SESSION['USER_ROLES'])) {
                print 'To manage Events, go to Admin > Events. <br><br>
            If you have changes to make to a Listing, go to Change Request. <br><br>';
            }if (in_array('county', $_SESSION['USER_ROLES'])) {
                print 'To manage customers, click <a href="region-home-listings.php?rid=' . $_SESSION['USER_LIMIT'] . '"> Listings</a>.<br><br>';
            }
            print 'For all other requests, please email <a href="mailto:info@touristtown.ca">info@touristtown.ca</a><br><br>';
            echo '</div>';
        }
        ?>
    </div>
</div>
<script>
    function show_req(PL_ID) {
        $("#failure" + PL_ID).attr("title", "Payment Failure Reason").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 500
        });
    }
</script>
<?php
require_once '../include/admin/footer.php';
?>