<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

$limits = explode(',', $_SESSION['USER_LIMIT']);
if (!in_array('regions', $_SESSION['USER_PERMISSIONS']) && (!in_array('manage-county-region', $_SESSION['USER_PERMISSIONS']) || !in_array($_REQUEST['rid'], $limits))) {
    header("Location: /admin/");
    exit();
}

if (isset($_REQUEST['rid']) && $_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} else {
    header("Location: /admin/regions.php");
    exit();
}
$cat = $_REQUEST['cat'];
$sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'save') {
    $cat = $_REQUEST['cat'];
    if ($cat != '') {
        $sqlMax = "SELECT MAX(RC_Order) FROM tbl_Region_Category WHERE RC_R_ID = '" . $regionID . "'";
        $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
        $rowMax = mysql_fetch_row($resultMax);
        $sql = " INSERT tbl_Region_Category SET RC_C_ID = '" . $cat . "', RC_R_ID = '" . $regionID . "', RC_Status = 1, RC_Order = '" . ($rowMax[0] + 1) . "'";
        $result = mysql_query($sql) or die(mysql_error());
        //Insert into Main Navigation
        $sqlMax = "SELECT MAX(MN_Order) FROM tbl_Main_Navigation WHERE MN_R_ID = '" . $regionID . "'";
        $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
        $rowMax = mysql_fetch_row($resultMax);
        $sql = " INSERT tbl_Main_Navigation SET MN_C_ID = '" . $cat . "', MN_R_ID = '" . $regionID . "', MN_Order = '" . ($rowMax[0] + 1) . "'";
        $result = mysql_query($sql) or die(mysql_error());
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $regionID, 'Manage Categories', $cat, 'Add Category', 'super admin');
        header("Location: /admin/regions-category.php?rid=" . $regionID);
        exit();
    }
}

if (isset($_REQUEST['del']) && $_REQUEST['del'] == 'true') {
    $sql = " DELETE FROM tbl_Region_Category WHERE RC_C_ID = '" . $_REQUEST['cid'] . "' AND RC_R_ID = '" . $regionID . "'";
    $result = mysql_query($sql) or die(mysql_error());
    $sql = " DELETE FROM tbl_Main_Navigation WHERE MN_C_ID = '" . $_REQUEST['cid'] . "' AND MN_R_ID = '" . $regionID . "'";
    $result = mysql_query($sql) or die(mysql_error());
    $_SESSION['delete'] = 1;
    // TRACK DATA ENTRY
    Track_Data_Entry('Websites', $regionID, 'Manage Categories', $_REQUEST['cid'], 'Delete Category', 'super admin');
    header("Location: /admin/regions-category.php?rid=" . $regionID);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Manage Categories</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">
            Categories
        </div>
        <form name="form1" method="GET" id="form">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <input type="hidden" name="op" value="save">
            <div class="content-sub-header link-header region-header-padding">
                <div class="data-column spl-org-listngs padding-org-listing">
                    <select name="cat" class="stories-cat" required>
                        <option value="">Select Category</option>
                        <?PHP
                        $sql = "SELECT C_ID, C_Name FROM tbl_Category  
                                LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID  AND RC_R_ID = " . $activeRegion['R_ID'] . "
                                WHERE C_Parent = '0' AND RC_C_ID is NULL ORDER BY C_Order";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $row['C_ID'] ?>" <?php echo (isset($row['C_ID']) == $cat) ? 'selected' : '' ?>><?php echo $row['C_Name'] ?></option>
                            <?php
                        }
                        ?>
                    </select>
                    <input type="submit" value="+Add" class="stories-add-button">
                </div>
            </div>
        </form>
        <div class="content-sub-header category">
            <div class="data-column spl-name-events">Categories</div>
            <div class="data-column spl-other-cc-list">Active</div>
            <div class="data-column spl-other-cc-list">Edit</div>
            <div class="data-column spl-other">Manage Slider</div>
            <div class="data-column spl-other">Remove</div>
        </div>
        <div class="reorder-category">
            <?PHP
            $sql = "SELECT C_ID, RC_Name, C_Name, RC_Status, RC_C_ID, RC_R_ID FROM tbl_Category  
                    LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID 
                    WHERE C_Parent = '0' AND RC_R_ID = " . $activeRegion['R_ID'] . " ORDER BY RC_Order";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $counter = 0;
            while ($row = mysql_fetch_assoc($result)) {
                
                $counter++;
                ?>
                <div class="data-content" id="recordsArray_<?php echo $row['C_ID'] ?>">
                    <div class="data-column spl-name-events">
                        <?php if($row['RC_C_ID'] ==8){ ?>
                        <label class="event-category-link"><?php echo $row['RC_Name'] == '' ? $row['C_Name'] : $row['RC_Name'] ?> </label>                       
                        <?php }else{ ?>
                         <a href="regions-subcategory.php?rid=<?php echo $activeRegion['R_ID'] ?>&amp;id=<?php echo $row['C_ID'] ?>">
                            <?php echo $row['RC_Name'] == '' ? $row['C_Name'] : $row['RC_Name'] ?>
                        </a>
                        <?php } ?>                         
                    </div>
                    <?php if ($row['RC_Status'] == 0) { ?>
                        <div class="data-column spl-other-cc-list">
                            <input id="check_function_<?php echo $row['C_ID'] ?>" type="checkbox" onclick="update_status(1, <?php echo $row['RC_C_ID'] . ", " . $row['RC_R_ID'] ?>)" <?php echo ($row['C_ID'] == $row['RC_C_ID']) ? 'checked' : '' ?>>
                        </div>
                    <?php } else if ($row['RC_Status'] == 1) { ?>
                        <div class="data-column spl-other-cc-list">
                            <input id="check_function_<?php echo $row['C_ID'] ?>" type="checkbox" onclick="update_status(0, <?php echo $row['RC_C_ID'] . ", " . $row['RC_R_ID'] ?>)">
                        </div>
                    <?php } ?>
                    <div class="data-column spl-other-cc-list">
                        <a href="regions-category-edit.php?rid=<?php echo $activeRegion['R_ID'] ?>&amp;id=<?php echo $row['C_ID'] ?>">Edit</a>
                    </div>
                    <div class="data-column spl-other">
                        <a href="category-slider.php?rid=<?php echo $activeRegion['R_ID'] ?>&amp;id=<?php echo $row['C_ID'] ?>">Manage Slider</a>
                    </div>
                    <div class="data-column spl-other">
                        <a href="regions-category.php?del=true&rid=<?php echo $activeRegion['R_ID'] ?>&cid=<?php echo $row['C_ID'] ?>&type=<?php echo $type ?>" onclick="return confirm('Are you sure?')">Remove</a>
                    </div>
                </div>
                <?PHP
            }
            ?>
        </div>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script>
    function update_status(status, cid, rid) {
        $.ajax({
            type: "GET",
            url: "update-region-category-status.php",
            data: {
                status: status,
                cid: cid,
                rid: rid,
                parent: 1
            }
        }).done(function (msg) {
            if (msg == 1) {
                if (status == 1) {
                    $("#check_function_" + cid).attr("onclick", 'update_status( 0 ,' + cid + ',' + rid + ')');
                }
                if (status == 0) {
                    $("#check_function_" + cid).attr("onclick", 'update_status( 1 ,' + cid + ',' + rid + ')');
                }
                swal("Data Saved", "Data has been saved successfully.", "success");
            } else {
                $("#check_function_" + cid).removeAttr("checked");
                swal("Categories Limit Exceeded", "Only 7 Categories can be enabled for a website!", "warning");
            }
        });
    }
    $(function () {
        $(".reorder-category").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Region_Category&field=RC_Order&id=RC_C_ID&r_field_name=RC_R_ID&region_id=<?php echo $activeRegion['R_ID'] ?>';
                $.post("reorder.php", order, function (theResponse) {
                    swal("Categories Re-Ordered", "Categories Re-Ordered successfully.", "success");
                });
            }
        });
    });
</script>