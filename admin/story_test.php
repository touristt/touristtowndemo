<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
if (!in_array('manage-stories', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

    require '../include/picUpload.inc.php';
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Create Story</div>
        <?php if ($S_ID > 0) { ?>
            <div class="link">
                <a href="#!" id="add-content">+Add Content</a>
            </div>
        <?php } ?>
    </div>
    <div class="right full-width">
        <!--Story Detail-->
        <form name="form" method="post" action="" enctype="multipart/form-data">
            <input type="hidden" name="op" value="story">
            <input type="hidden" name="id" value="<?php echo $S_ID ?>">
            <div class="content-header full-width">Story Details</div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Title</label>
                <div class="form-data">
                    <input name="title" type="text" value="<?php echo (isset($rowStory['S_Title']) ? $rowStory['S_Title'] : '') ?>" size="40" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Category</label>
                <div class="form-data">
                    <select name="category" required>
                        <option value="">Select Category</option>
                        <option value="">Select Sesson</option>
                         <option value="">Select Category</option>
                          <option value="">Select Seson</option>
                    </select>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Author</label>
                <div class="form-data">
                    <input name="author" type="text" value="" size="40" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width"> 
                <label>Description</label>
                <div class="form-data">
                    <textarea name="description" class="tt-description"></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width"> 
                <label>Select Community</label>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>SEO Title </label>
                <div class="form-data">
                    <input name="seoTitle" type="text" value="" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width"> 
                <label>SEO Description </label>
                <div class="form-data">
                    <input name="seoDescription" type="text" value="" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width"> 
                <label>SEO Keywords</label>
                <div class="form-data">
                    <input name="seoKeywords" type="text" value="" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width"> 
                <label>Seasons</label>
                <div class="form-data">
                    <div id="childRegion"> 
                        <?php
                        $StoriesSeasons = array();
                        while ($rowSS = mysql_fetch_array($resultSS)) {
                            $StoriesSeasons[] = $rowSS['SS_Season'];
                        }
                        $sql = "SELECT * FROM tbl_Seasons";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_array($result)) {
                            ?>
                            <div class="childRegionCheckbox">
                                <input name="season[<?php echo $row['S_ID'] ?>]" type="checkbox" value="<?php echo $row['S_ID'] ?>" <?php echo in_array($row['S_ID'], $StoriesSeasons) ? 'checked' : '' ?> /><?php echo $row['S_Name'] ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Photo</label>
                <div class="form-inside-div div_image_library about-us-cropit border-none">
                    <span class="daily_browse" onclick="choose_file(27);">Select Photo</span>
                    <div class="dialog-close dialog27 dialog-bank" style="display: none;">
                        <label for="photo27" class="close_file_dialog daily_browse">Upload New Photo</label> 
                        <a onclick="show_image_library(27)">Add from Library</a>
                    </div>
                    <input type="file" style="display :none;" name="pic[]" onChange="show_file_name(27, this);" id="photo27">
                    <input type="hidden" name="image_bank_story" id="image_bank27" value="">
                    <div class="cropit-image-preview aboutus-photo-perview">
                        <img class="preview-img preview-img-script27" id="container" style="display: none;" src="" id="chk">    
                        <?php if (isset($rowStory['S_Thumbnail']) && $rowStory['S_Thumbnail'] != '') { ?>
                            <img class="existing-img existing_imgs27" src="<?php echo IMG_LOC_REL . $rowStory['S_Thumbnail'] ?>" >
                        <?php } ?>
                    </div>
                </div>
            </div>
            
            <script type="text/javascript">
            
            </script>
            
            <div class="form-inside-div form-inside-div-width-admin full-width"> 
                <div class="button">
                    <input type="submit" name="button" value="Submit" />
                </div>
            </div>
        </form>

        <!--Add story content-->
        <form name="form1" method="post" action="" enctype="multipart/form-data" id="add-content-form" style="display: none">
            <input type="hidden" name="op" value="content_add">
            <input type="hidden" name="id" value="<?php echo $S_ID ?>">
            <input type="hidden" name="image_bank" class="image_bank" id="image_bank26" value="">
            <div class="content-header full-width">Content Details</div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Title</label>
                <div class="form-data">
                    <input name="content_title" type="text" size="40" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Photo</label>
                <div class="form-inside-div div_image_library about-us-cropit border-none">
                    <span class="daily_browse" onclick="choose_file(26);">Select Photo</span>
                    <div class="dialog-close dialog26 dialog-bank" style="display: none;">
                        <label for="photo26" class="close_file_dialog daily_browse">Upload New Photo</label> 
                        <a onclick="show_image_library(26)">Add from Library</a>
                    </div>
                    <input type="file" style="display :none;" name="pic[]" onChange="show_file_name(26, this);" id="photo26">
                    <div class="form-inside-div add-about-us-item-image margin-none">
                        <div class="cropit-image-preview aboutus-photo-perview">
                            <img class="preview-img preview-img-script26" style="display: none;" src="">    
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Video Link</label>
                <div class="form-data">
                    <input name="video" type="text" size="50" /> 
                </div>
            </div>
                            <!--  -->
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Description</label>
                <div class="form-data">
                    <textarea name="content_description" cols="85" rows="10" id="description-listing"></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width"> 
                <div class="button">
                    <input type="submit" name="button" value="Save Content" />
                </div>
            </div>
        </form>

        <!--Content Record-->
        <?php if ($S_ID > 0) { ?>
            <div id="accordion" class="story">
                <?PHP
                $sql = "SELECT * FROM tbl_Content_Piece WHERE CP_S_ID = " . $S_ID . " ORDER BY CP_Order ASC";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                $acco = 0;
                while ($rowContent = mysql_fetch_array($result)) {
                    ?>
                    <div class="group" id="recordsArray_<?php echo $rowContent['CP_ID']; ?>">
                        <h3 class="accordion-rows"><span class="accordion-title"><?php echo $rowContent['CP_Title']; ?></span><label class="add-item-accordion"><a onclick="event.stopPropagation()" href="story.php?op=content_delete&cp_id=<?php echo $rowContent['CP_ID'] ?>&id=<?php echo $S_ID ?>">Delete Content</a></label></h3> 
                        <form name="form2" method="post" action="" enctype="multipart/form-data" class="story-content-update-form">
                            <input type="hidden" name="cp_id" value="<?php echo $rowContent['CP_ID'] ?>">
                            <input type="hidden" name="id" value="<?php echo $S_ID ?>">
                            <input type="hidden" name="op" value="content_update">
                            <input type="hidden" name="image_bank" class="image_bank" id="image_bank<?php echo $rowContent['CP_ID'] ?>" value="">
                            <input type="hidden" name="acc" value="<?php echo $acco; ?>">
                            <div class="form-inside-div form-inside-div-width-admin full-width">
                                <label>Title</label>
                                <div class="form-data">
                                    <input name="content_title" type="text" value="<?php echo $rowContent['CP_Title'] ?>"/>
                                </div>
                            </div>
                            <div class="form-inside-div form-inside-div-width-admin full-width">
                                <label>Photo</label>
                                <div class="form-inside-div div_image_library about-us-cropit border-none">
                                    <span class="daily_browse" onclick="choose_file(<?php echo $rowContent['CP_ID'] ?>);">Select Photo</span>
                                    <div class="dialog-close dialog<?php echo $rowContent['CP_ID'] ?> dialog-bank" style="display: none;">
                                        <label for="photo<?php echo $rowContent['CP_ID']; ?>" class="close_file_dialog daily_browse">Upload New Photo</label> 
                                        <a onclick="show_image_library(26, <?php echo $rowContent['CP_ID']; ?>)">Add from Library</a>
                                    </div>
                                    <input type="file" style="display :none;" name="pic[]" onChange="show_file_name(26, this, <?php echo $rowContent['CP_ID']; ?>);" id="photo<?php echo $rowContent['CP_ID']; ?>">
                                    <div class="cropit-image-preview aboutus-photo-perview">
                                        <img class="preview-img preview-img-script<?php echo $rowContent['CP_ID']; ?>" style="display: none;" src="">    
                                        <?php if ($rowContent['CP_Photo'] != '') { ?>
                                            <img class="existing-img existing_imgs<?php echo $rowContent['CP_ID']; ?>" src="<?php echo IMG_LOC_REL . $rowContent['CP_Photo'] ?>" >
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <!--  -->
                            <div class="form-inside-div form-inside-div-width-admin full-width">
                                <label>Video Link</label>
                                <div class="form-data">
                                    <input name="video" type="text" value="<?php echo (isset($rowContent['CP_Video_Link']) ?$rowContent['CP_Video_Link'] : '') ?>" size="50" /> 
                                </div>
                            </div>
                            <div class="form-inside-div form-inside-div-width-admin full-width">
                                <label>Description</label>
                                <div class="form-data">
                                    <textarea name="content_description" id="description_<?php echo $rowContent['CP_ID'] ?>" class="ckeditor"><?php echo $rowContent['CP_Description']; ?></textarea>
                                </div>
                            </div>
                            <div class="form-inside-div form-inside-div-width-admin full-width">
                                <div class="button">
                                    <input type="submit" name="button" value="Save Content" />
                                </div>
                            </div>
                        </form>
                        <script>
                            CKEDITOR.disableAutoInline = true;
                            $( document ).ready( function() {
                                var CP_ID = <?php echo $rowContent['CP_ID']; ?>;
                                if(CKEDITOR.instances['description_'+CP_ID]) {
                                    delete CKEDITOR.instances['description_'+CP_ID];
                                    $( '#description_'+CP_ID ).ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
                                }
                                $( '#description_'+CP_ID ).ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
                                setTimeout(function(){
                                    CKEDITOR.replace('textarea');
                                },1000);
                            });
                        </script>  
                    </div>    
                    <?PHP
                    $acco++;
                }
                ?>
            </div>
        <?php } ?>
    </div>
    <div id="dialog-message"></div>
    <div id="image-library" style="display:none;"></div>
    <input id="image-library-usage" type="hidden" value="">
</div>  


<?PHP
require_once '../include/admin/footer.php';
?>
<script>
    $(document).ready(function(){
        $("#add-content").click(function(){
            $("#add-content-form").toggle();
        });
    }); 
    $(function() {
        $( "#accordion" )
        .accordion({
            collapsible: true,
            heightStyle: 'content',
            active: <?php echo $activeAccordion; ?>,
            header: "> div > h3"
        })
        .sortable({
            axis: "y",
            handle: "h3",
            start: function (event, ui) 
            {
                var id_textarea = ui.item.find(".ckeditor").attr("id");
                CKEDITOR.instances[id_textarea].destroy();
            },
            update: function() {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Content_Piece&field=CP_Order&id=CP_ID';
                $.post("reorder.php", order, function(theResponse) {
                    $("#dialog-message").html("Stories Re-Ordered Successfully!");
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            Ok: function() {
                                $(this).dialog("close");
                            }
                        }
                });
                });
            },
            stop: function (event, ui) 
            {
                var id_textarea = ui.item.find(".ckeditor").attr("id");
                CKEDITOR.replace(id_textarea);
                // IE doesn't register the blur when sorting
                // so trigger focusout handlers to remove .ui-state-focus
                ui.item.children( "h3" ).triggerHandler( "focusout" );
 
                // Refresh accordion to handle new order
                $( this ).accordion( "refresh" );
            }
        });
    });
</script>