<?PHP
require_once '../include/config.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('manage-support', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$S_ID = $_REQUEST['sid'];
$result = mysql_query("DELETE FROM tbl_Support WHERE S_ID = '$S_ID'");
if ($result) {
    $_SESSION['delete'] = 1;
    // TRACK DATA ENTRY
    $id = $_REQUEST['id'];
    Track_Data_Entry('Support', '', 'Support', $id, 'Delete Item', 'super admin');
} else {
    $_SESSION['delete_error'] = 1;
}
header("Location: support-type.php?id=" . $_REQUEST['id']);
?>