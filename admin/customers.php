<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('customers', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
}

if (isset($_REQUEST['email']) && $_REQUEST['email'] != '') {
    $sql = "SELECT B_ID, B_Email FROM tbl_Business WHERE B_Email = '" . encode_strings($_REQUEST['email'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowBUS = mysql_fetch_assoc($result);
    if ($rowBUS['B_ID']) {
        $sql = "SELECT COUNT(*) FROM tbl_Business_Listing WHERE BL_B_ID = '" . encode_strings($rowBUS['B_ID'], $db) . "'";
        $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $rowListings = mysql_fetch_row($resultTMP);
    } else {
        $_SESSION['email_not_found'] = 1;
        header("Location:listings.php");
        exit();
    }
}

if (isset($_GET['op']) && $_GET['op'] == 'del') {
    $sql = "SELECT BL_ID FROM tbl_Business_Listing WHERE BL_B_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($listing = mysql_fetch_assoc($result)) {
        $sql2 = "DELETE tbl_Business_Listing, tbl_Business_Listing_Ammenity, tbl_Business_Listing_Category, tbl_Business_Listing_Category_Region,
                tbl_Business_Social, tbl_BL_Feature, payment_profiles, tbl_Business_Feature_About, tbl_Business_Feature_About_Photo,
                tbl_Business_Feature_About_Main_Photo, tbl_Business_Feature_Daily_Specials, tbl_Business_Feature_Daily_Specials_Description, 
                tbl_Business_Feature_Entertainment_Acts, tbl_Business_Feature_Entertainment_Description, tbl_Business_Feature_Guest_Book, 
                tbl_Business_Feature_Guest_Book_Description, tbl_Business_Feature_Menu, tbl_Business_Feature_Photo, tbl_Business_Feature_Product, 
                tbl_Business_Feature_Product_Photo, tbl_Business_Feature_Coupon, tbl_Business_Feature_Coupon_Category_Multiple, tbl_Image_Bank_Usage, 
                tbl_Advertisement, tbl_Advertisement_Billing, tbl_Advertisement_Change_Request, tbl_Advertisement_Photo, tbl_Advertisement_Statistics
                FROM tbl_Business_Listing 
                LEFT JOIN tbl_Business_Listing_Ammenity ON BLA_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Listing_Category_Region ON BLCR_BLC_ID = BLC_ID 
                LEFT JOIN tbl_Business_Social ON BS_BL_ID = BL_ID
                LEFT JOIN tbl_BL_Feature ON BLF_BL_ID = BL_ID
                LEFT JOIN payment_profiles ON listing_id = BL_ID
                LEFT JOIN tbl_Business_Feature_About ON BFA_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Feature_About_Photo ON BFAP_BFA_ID = BFA_ID
                LEFT JOIN tbl_Business_Feature_About_Main_Photo ON BFAMP_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Feature_Daily_Specials ON BFDS_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Feature_Daily_Specials_Description ON BFDSD_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Feature_Entertainment_Acts ON BFEA_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Feature_Entertainment_Description ON BFED_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Feature_Guest_Book ON BFGB_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Feature_Guest_Book_Description ON tbl_Business_Feature_Guest_Book_Description.BFGB_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Feature_Menu ON BFM_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Feature_Photo ON BFP_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Feature_Product ON tbl_Business_Feature_Product.BFP_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Feature_Product_Photo ON BFPP_BFP_ID = tbl_Business_Feature_Product.BFP_ID
                LEFT JOIN tbl_Business_Feature_Coupon ON tbl_Business_Feature_Coupon.BFC_BL_ID = BL_ID
                LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
                LEFT JOIN tbl_Image_Bank_Usage ON IBU_BL_ID = BL_ID
                LEFT JOIN tbl_Advertisement ON BL_ID = A_BL_ID 
                LEFT JOIN tbl_Advertisement_Billing ON A_ID = AB_A_ID 
                LEFT JOIN tbl_Advertisement_Change_Request ON A_ID = ACR_A_AID 
                LEFT JOIN tbl_Advertisement_Photo ON A_ID = AP_A_ID 
                LEFT JOIN tbl_Advertisement_Statistics ON A_ID = AS_A_ID 
                WHERE BL_ID = '" . encode_strings($listing['BL_ID'], $db) . "'";
        $result2 = mysql_query($sql2, $db) or die("Invalid query: $sql2 -- " . mysql_error());
    }
    $sql3 = "DELETE tbl_Business FROM tbl_Business WHERE B_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result3 = mysql_query($sql3, $db) or die("Invalid query: $sql3 -- " . mysql_error());
    if ($result && $result2 && $result3) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location:" . $_SERVER['HTTP_REFERER']);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Customers</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
    </div>
    <form id="frmSort" name="form" method="GET" action="">
        <div class="right">
            <div class="content-sub-header">
                <div class="data-column spl-name-events padding-none">Email</div>
                <div class="data-column spl-other-cc-list padding-none"></div>
                <div class="data-column spl-other-cc-list padding-none">#Listings</div>
                <div class="data-column spl-other-cc-list padding-none">Edit</div>
                <div class="data-column spl-other-cc-list padding-none">Delete</div>
            </div>
            <div class="data-content">
                <div class="data-column spl-name-events"><a href="customer-listings.php?id=<?php echo $rowBUS['B_ID'] ?>"><?php echo $rowBUS['B_Email'] ?></a></div>
                <div class="data-column spl-other-cc-list"></div>
                <div class="data-column spl-other-cc-list"><?php echo $rowListings[0] ?></div>
                <div class="data-column spl-other-cc-list"><a href="customer-listings.php?id=<?php echo $rowBUS['B_ID'] ?>">Edit</a></div>
                <div class="data-column spl-other-cc-list"><a onClick="return confirm('Are you sure this action can not be undone!');" href="customers.php?op=del&amp;id=<?php echo $rowBUS['B_ID'] ?>">Delete</a></div>
            </div>
        </div>
    </form>
</div>
<?php
require_once '../include/admin/footer.php';
?>
