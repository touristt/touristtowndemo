<?php

require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (isset($_REQUEST['export_accounting'])) {
    $join = '';
    $region = $_REQUEST['region_filter'];
    $year = $_REQUEST['year_filter'];
    $month = $_REQUEST['month_filter'];
// Download the file
    $filename = "accounting.csv";
    header('Content-type: application/csv');
    header('Content-Disposition: attachment; filename=' . $filename);
    header("Pragma: no-cache");
    header("Expires: 0");
    echo "\xEF\xBB\xBF";
// Fetch region
    if (in_array('superadmin', $_SESSION['USER_ROLES'])) {
        $sales_user = $_REQUEST['manager_filter'];
    } else {
        $sales_user = $_SESSION['USER_ID'];
    }
    if ($year == "" && $month == "" && $sales_user == "") {
        $date = date('Y-m');
        $display_date = date('F') . ' ' . date('Y');
        $where_clause = "AND DATE_FORMAT(BB_Date,'%Y-%m') = '$date'";
        $where_clause_refund = "AND DATE_FORMAT(BB_Refund_Deleted_Date,'%Y-%m') = '$date'";
        $where_clause_advert = " AND DATE_FORMAT(AB_Active_Date,'%Y-%m') = '$date'";
        $where_clause_division_billing = " AND DATE_FORMAT(DB_Date,'%Y-%m') = '$date'";
        $where_clause_division_billing_refund = " AND DATE_FORMAT(DB_Refund_Deleted_Date,'%Y-%m') = '$date'";
    } else if ($year == "" && $sales_user == "") {
        $where_clause = "AND DATE_FORMAT(BB_Date,'%m') = '$month'";
        $where_clause_refund = "AND DATE_FORMAT(BB_Refund_Deleted_Date,'%m') = '$month'";
        $where_clause_advert = " AND DATE_FORMAT(AB_Active_Date,'%m') = '$month'";
        $where_clause_division_billing = " AND DATE_FORMAT(DB_Date,'%m') = '$month'";
        $where_clause_division_billing_refund = " AND DATE_FORMAT(DB_Refund_Deleted_Date,'%m') = '$month'";
    } else if ($month == "" && $sales_user == "") {
        $where_clause = "AND DATE_FORMAT(BB_Date,'%Y') = '$year'";
        $where_clause_refund = "AND DATE_FORMAT(BB_Refund_Deleted_Date,'%Y') = '$year'";
        $where_clause_advert = " AND DATE_FORMAT(AB_Active_Date,'%Y') = '$year'";
        $where_clause_division_billing = " AND DATE_FORMAT(DB_Date,'%Y') = '$year'";
        $where_clause_division_billing_refund = " AND DATE_FORMAT(DB_Refund_Deleted_Date,'%Y') = '$year'";
    } else if ($month == "" && $year == "") {
        $date = date('Y-m');
        $where_clause = "AND BB_Account_Manager = '$sales_user'";
        $where_clause_refund = "AND BB_Account_Manager = '$sales_user'";
    } else if ($sales_user == "") {
        $date = $year . "-" . $month;
        $where_clause = "AND DATE_FORMAT(BB_Date,'%Y-%m') = '$date'";
        $where_clause_refund = "AND DATE_FORMAT(BB_Refund_Deleted_Date,'%Y-%m') = '$date'";
        $where_clause_advert = " AND DATE_FORMAT(AB_Active_Date,'%Y-%m') = '$date'";
        $where_clause_division_billing = " AND DATE_FORMAT(DB_Date,'%Y-%m') = '$date'";
        $where_clause_division_billing_refund = " AND DATE_FORMAT(DB_Refund_Deleted_Date,'%Y-%m') = '$date'";
    } else if ($year == "") {
        $where_clause = "AND DATE_FORMAT(BB_Date,'%m') = '$month' AND BB_Account_Manager = '$sales_user'";
        $where_clause_refund = "AND DATE_FORMAT(BB_Refund_Deleted_Date,'%m') = '$month' AND BB_Account_Manager = '$sales_user'";
        $where_clause_advert = " AND DATE_FORMAT(AB_Active_Date,'%m') = '$month'";
        $where_clause_division_billing = " AND DATE_FORMAT(DB_Date,'%m') = '$month'";
        $where_clause_division_billing_refund = " AND DATE_FORMAT(DB_Refund_Deleted_Date,'%m') = '$month'";
    } else if ($month == "") {
        $where_clause = "AND DATE_FORMAT(BB_Date,'%Y') = '$year' AND BB_Account_Manager = '$sales_user'";
        $where_clause_refund = "AND DATE_FORMAT(BB_Refund_Deleted_Date,'%Y') = '$year' AND BB_Account_Manager = '$sales_user'";
        $where_clause_advert = " AND DATE_FORMAT(AB_Active_Date,'%Y') = '$year'";
        $where_clause_division_billing = " AND DATE_FORMAT(DB_Date,'%Y') = '$year'";
        $where_clause_division_billing_refund = " AND DATE_FORMAT(DB_Refund_Deleted_Date,'%Y') = '$year'";
    } else if ($month != "" && $year != "" && $sales_user != "") {
        $date = $year . "-" . $month;
        $where_clause = "AND DATE_FORMAT(BB_Date,'%Y-%m') = '$date' AND BB_Account_Manager = '$sales_user'";
        $where_clause_refund = "AND DATE_FORMAT(BB_Refund_Deleted_Date,'%Y-%m') = '$date' AND BB_Account_Manager = '$sales_user'";
        $where_clause_advert = " AND DATE_FORMAT(AB_Active_Date,'%Y-%m') = '$date'";
        $where_clause_division_billing = " AND DATE_FORMAT(DB_Date,'%Y-%m') = '$date'";
        $where_clause_division_billing_refund = " AND DATE_FORMAT(DB_Refund_Deleted_Date,'%Y-%m') = '$date'";
    }

    if ($region != '') {
        $sql = "SELECT R_ID, R_Parent FROM tbl_Region WHERE R_ID = " . $region;
        $result = mysql_query($sql);
        $r = mysql_fetch_assoc($result);
        $regionList = '';
        if ($r['R_Parent'] == 0) {
            $sql = "SELECT RM_Child FROM tbl_Region_Multiple WHERE RM_Parent = '" . $r['R_ID'] . "'";
            $result = mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
            $first = true;
            $regionList .= "(";
            while ($row = mysql_fetch_assoc($result)) {
                if ($first) {
                    $first = false;
                } else {
                    $regionList .= ",";
                }
                $regionList .= $row['RM_Child'];
            }
            $regionList .= ")";
        } else {
            $regionList = '(' . $r['R_ID'] . ')';
        }
        $join .= " INNER JOIN tbl_Business_Listing_Category_Region ON BL_ID = BLCR_BL_ID ";
        $where_clause .= " AND BLCR_BLC_R_ID IN $regionList";
        $where_clause_refund .= " AND BLCR_BLC_R_ID IN $regionList";
        $where_clause_advert .= " AND BLCR_BLC_R_ID IN $regionList";
    }

    if ($month != "") {
        $display_date = date('F', strtotime("2000-$month-01")) . ' ' . $year;
    } else if ($year != "") {
        $display_date = $year;
    } else if ($sales_user != "") {
        $display_date = "";
    }

    $output = 'Listing Sales (Credit Card)  -' . $display_date . ', ';
    $output .= "\n";
    echo $output;

   echo $output = 'Business Name , Card Holder , Card Type , Date , Inv. # , Sub-Total , Tax , Total ';
 echo   $output = "\n";
    //echo $output;
        $listing_billings = array();
        $listing_billings_refund = array();
        $all_listing_billings = array();
        //listing billings without refunds
        $sql = "SELECT BL_ID, BL_Listing_Title, BB_ID, BB_Date, BB_Refund_Deleted_Date, BB_SubTotal3, BB_Tax, BB_Total, BB_Card_Type, BB_Card_Number, BB_First_Name, BB_Last_Name, BB_Invoice_Num,
                U_Commission, pp.first_name, pp.last_name, cd.first_name as fname, cd.last_name as lname FROM tbl_Business_Billing
                LEFT JOIN tbl_Business ON B_ID = BB_B_ID
                LEFT JOIN tbl_Business_Listing ON BL_ID = BB_BL_ID
                LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID 
                $join
                LEFT JOIN cards_details cd ON B_ID = cd.business_id
                LEFT JOIN payment_profiles pp ON pp.business_id = B_ID
                LEFT JOIN tbl_User on BB_Account_Manager = U_ID
                WHERE BB_Deleted = 0 AND BB_Payment_Type = 4 AND BB_Total > 0 $where_clause GROUP BY BB_ID ORDER BY BB_ID";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $i = 0;
        while ($row = mysql_fetch_assoc($result)) {
            $listing_billings[$i] = $row;
            $listing_billings[$i]['date'] = $row['BB_Date'];
            $i++;
        }
        //listing billings with refunds
        $sql = "SELECT BL_ID, BL_Listing_Title, BB_ID, BB_Date, BB_Refund_Deleted_Date, BB_SubTotal3, BB_Tax, BB_Total, BB_Card_Type, BB_Card_Number, BB_First_Name, BB_Last_Name, BB_Invoice_Num,
                U_Commission, BB_Refund_Deleted, pp.first_name, pp.last_name, cd.first_name as fname, cd.last_name as lname FROM tbl_Business_Billing
                LEFT JOIN tbl_Business ON B_ID = BB_B_ID
                LEFT JOIN tbl_Business_Listing ON BL_ID = BB_BL_ID
                LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID 
                $join
                LEFT JOIN cards_details cd ON B_ID = cd.business_id
                LEFT JOIN payment_profiles pp ON pp.business_id = B_ID
                LEFT JOIN tbl_User on BB_Account_Manager = U_ID
                WHERE BB_Deleted = 0 AND BB_Payment_Type = 4 AND BB_Total > 0 AND BB_Refund_Deleted = 1 $where_clause_refund GROUP BY BB_ID ORDER BY BB_ID";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $i = 0;
        while ($row = mysql_fetch_assoc($result)) {
            $listing_billings_refund[$i] = $row;
            $listing_billings_refund[$i]['date'] = $row['BB_Refund_Deleted_Date'];
            $i++;
        }
        $all_listing_billings = array_merge($listing_billings, $listing_billings_refund);

        function sortListing($a, $b) {
            return strtotime($a['date']) - strtotime($b['date']);
        }

        usort($all_listing_billings, "sortListing");

        $subtotal = $tax = $total = $refund_subtotal = $refund_tax = $refund_total = 0;
        
        $overall_refund_subtotal = $overall_refund_tax = $overall_refund_total = 0;
        $overall_listing_subtotal = $overall_listing_tax = $overall_listing_total = 0;
        $overall_subtotal = $overall_tax = $overall_total = 0;
        
        foreach ($all_listing_billings as $row) { //echo '<pre>'; print_r($row);
        $commission_percent = $row['U_Commission'];
        //show refunded columns in red
        $refund_class = "";
        if (isset($row['BB_Refund_Deleted']) && $row['BB_Refund_Deleted'] == 1) {
            $date = date("m/d/Y", strtotime($row['BB_Refund_Deleted_Date']));
            $refund_class = " refund";
            $refund_subtotal += $row['BB_SubTotal3'];
            $refund_tax += $row['BB_Tax'];
            $refund_total += $row['BB_Total'];
        } else {
            $date = date("m/d/Y", strtotime($row['BB_Date']));
            $subtotal = $subtotal + $row['BB_SubTotal3'];
            $tax = $tax + $row['BB_Tax'];
            $total = $total + $row['BB_Total'];
        }
        // $output = str_replace("," , " " ,$row['BL_Listing_Title']) . ',';
       echo  $row['BL_Listing_Title'] .',';
        if (isset($row['BB_First_Name'])) {
            echo  $row['BB_First_Name'] . ' ' . $row['BB_Last_Name'] . ',';
         } else {
            echo  $row['fname'] . ' ' . $row['lname'] . ',';
        }
        if ($row['BB_Card_Type'] != '') {
            if ($row['BB_Card_Type'] == 'Visa') {
                echo  'V ,';
            } else if ($row['BB_Card_Type'] == 'MasterCard') {
               echo  'MC ,';
            } else if ($row['BB_Card_Type'] == 'American Express') {
               echo  'A ,';
            } else {
                echo  'N/A ,';
            }
        } else {
            echo  'N/A ,';
        }
        $subTotal_one = ($refund_class == '') ? "$" . $row['BB_SubTotal3'] : "($" . $row['BB_SubTotal3'] . ")";
        $tex_one =($refund_class == '') ? "$" . $row['BB_Tax'] : "($" . $row['BB_Tax'] . ")";
        $total_one =($refund_class == '') ? "$" . $row['BB_Total'] : "($" . $row['BB_Total'] . ")";
        echo $output= $row['BB_Date'] . ' ,';
        echo $output= $row['BB_Invoice_Num'] . ' ,';
        echo $output= $subTotal_one . ' ,';
        echo $output= $tex_one . ' ,';
        echo $output= $total_one;
        echo "\n";
        //echo $output;
        
    }
   
    // variables to calculate monthly subtotal, tax and total for listings 
    $overall_listing_subtotal = $subtotal - $refund_subtotal;
    $overall_listing_tax = $tax - $refund_tax;
    $overall_listing_total = $total - $refund_total;
    // variables to calculate overall subtotal, tax and total for listings 
    $overall_subtotal += $subtotal;
    $overall_tax += $tax;
    $overall_total += $total;
    $overall_refund_subtotal += $refund_subtotal;
    $overall_refund_tax += $refund_tax;
    $overall_refund_total += $refund_total;
    $commission = $subtotal / 100;
    $commission = round($commission * $commission_percent, 2);
    $grand_sub_total = $grand_sub_total + $subtotal;
    $grand_tax_total = $grand_tax_total + $tax;
    $grand_total = $grand_total + $total;

    // print_r($subtotal); echo '/';print_r($refund_subtotal);
    $subtotal_var = $subtotal - $refund_subtotal;
    $tax_var = $tax - $refund_tax;
    $total_var = $total - $refund_total;
    $subtotal_one =($subtotal >= 0) ? "$" . $subtotal : "($" . $subtotal . ")";
    $tax_one =($tax >= 0) ? "$" . $tax : "($" . $tax . ")";
    $total_one =($total >= 0) ? "$" . $total : "($" . $total . ")";
    ////SubTotal
    $output1 = 'SubTotal , , , , ,';
    $output1 .= $subtotal_one . ' ,';
    $output1 .= $tax_one . ' ,';
    $output1 .= $total_one;
    $output1 .= "\n \n ";
    echo $output1;
    ////Refund
    $refund_subtotal_one =($refund_subtotal >= 0) ? "$" . $refund_subtotal : "($" . $refund_subtotal . ")";
    $refund_tax_one      =($refund_tax >= 0) ? "$" . $refund_tax : "($" . $refund_tax . ")";
    $refund_total_one    =($refund_total >= 0) ? "$" . $refund_total : "($" . $refund_total . ")";
    $output2 = 'Refunds , , , , ,';
    $output2 .= $refund_subtotal_one . ' ,';
    $output2 .= $refund_tax_one . ' ,';
    $output2 .= $refund_total_one;
    $output2 .= "\n \n ";
    echo $output2;
    ///Total
    $subtotal_var_one =($subtotal_var >= 0) ? "$" . $subtotal_var : "($" . $subtotal_var . ")";
    $tax_var_one      =($tax_var >= 0) ? "$" . $tax_var : "($" . $tax_var . ")";
    $total_var_one    =($total_var >= 0) ? "$" . $total_var : "($" . $total_var . ")";
    $output = 'Total , , , , ,';
    $output .= $subtotal_var_one . ' ,';
    $output .= $tax_var_one . ' ,';
    $output .= $total_var_one;
    $output .= "\n \n ";
    echo $output;

    if ($sales_user != "") {
        $output = 'Commission , , , , , , ,';
        $output .= "$" . $commission;
        $output .= "\n \n ";
        echo $output;
    }

    $output = 'Listing Sales (Cheque/Cash/E-transfer) -' . $display_date . ', ';
    $output .= "\n";
    echo $output;

    $output = 'Business Name ,, Payment Method , Date , Inv. # , Sub-Total , Tax , Total ';
    $output .= "\n";
    echo $output;
    $listing_billings_nonCC = array();
    $listing_billings_nonCC_refund = array();
    $all_listing_billings_nonCC = array();
    $sql = "SELECT BL_ID, BL_Listing_Title, BB_ID, BB_Date, BB_Refund_Deleted_Date, BB_SubTotal3, BB_Tax, BB_Total, BB_Invoice_Num,
                U_Commission, PT_Name FROM tbl_Business_Billing bb 
                LEFT JOIN tbl_Business b ON b.B_ID = bb.BB_B_ID 
                LEFT JOIN tbl_Business_Listing bl ON bl.BL_ID = bb.BB_BL_ID 
                LEFT JOIN tbl_Business_Listing_Category blc ON bl.BL_ID = blc.BLC_BL_ID
                LEFT JOIN tbl_Payment_Type ON BB_Payment_Type = PT_ID
                $join
                LEFT JOIN cards_details cd ON b.B_ID = cd.business_id
                LEFT JOIN payment_profiles pp ON pp.business_id = b.B_ID
                LEFT JOIN tbl_User u on bb.BB_Account_Manager = u.U_ID
                WHERE BB_Deleted = 0 AND BB_Payment_Type != 4 AND BB_Total > 0 $where_clause 
                GROUP BY BB_ID ORDER BY BB_ID";
    $i = 0;
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($row = mysql_fetch_assoc($result)) {
        $listing_billings_nonCC[$i] = $row;
        $listing_billings_nonCC[$i]['date'] = $row['BB_Date'];
        $i++;
    }
    //listing billings with refunds
    $sql = "SELECT BL_ID, BL_Listing_Title, BB_ID, BB_Date, BB_SubTotal3, BB_Tax, BB_Total, BB_Invoice_Num,
                U_Commission, PT_Name, BB_Refund_Deleted_Date, BB_Refund_Deleted
                FROM tbl_Business_Billing
                LEFT JOIN tbl_Business ON B_ID = BB_B_ID
                LEFT JOIN tbl_Business_Listing ON BL_ID = BB_BL_ID
                LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID 
                LEFT JOIN tbl_Payment_Type ON BB_Payment_Type = PT_ID
                $join
                LEFT JOIN cards_details cd ON B_ID = cd.business_id
                LEFT JOIN payment_profiles pp ON pp.business_id = B_ID
                LEFT JOIN tbl_User on BB_Account_Manager = U_ID
                WHERE BB_Deleted = 0 AND BB_Payment_Type != 4 AND BB_Total > 0 AND BB_Refund_Deleted = 1 
                $where_clause_refund GROUP BY BB_ID ORDER BY BB_ID";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $i = 0;
    while ($row = mysql_fetch_assoc($result)) {
        $listing_billings_nonCC_refund[$i] = $row;
        $listing_billings_nonCC_refund[$i]['date'] = $row['BB_Refund_Deleted_Date'];
        $i++;
    }
    $all_listing_billings_nonCC = array_merge($listing_billings_nonCC, $listing_billings_nonCC_refund);

    function sortListingNonCC($a, $b) {
        return strtotime($a['date']) - strtotime($b['date']);
    }

    usort($all_listing_billings_nonCC, "sortListingNonCC");

    $subtotal = $tax = $total = $refund_subtotal = $refund_tax = $refund_total = 0;

    foreach ($all_listing_billings_nonCC as $row) {      //  echo '<pre>';        print_r($row); exit;
        $commission_percent = $row['U_Commission'];
        //show refunded columns in red
        $refund_class = "";
        if (isset($row['BB_Refund_Deleted']) && $row['BB_Refund_Deleted'] == 1) {
            $date = date("m/d/Y", strtotime($row['BB_Refund_Deleted_Date']));
            $refund_class = " refund";
            $refund_subtotal += $row['BB_SubTotal3'];
            $refund_tax += $row['BB_Tax'];
            $refund_total += $row['BB_Total'];
        } else {
            $date = date("m/d/Y", strtotime($row['BB_Date']));
            $subtotal = $subtotal + $row['BB_SubTotal3'];
            $tax = $tax + $row['BB_Tax'];
            $total = $total + $row['BB_Total'];
        }
        ///
        echo $row['BL_Listing_Title'] . ',,';
         echo $row['PT_Name'] . ',';
         $subtotal_two  = ($refund_class == '') ? "$" . $row['BB_SubTotal3'] : "($" . $row['BB_SubTotal3'] . ")";
         $tax_two       = ($refund_class == '') ? "$" . $row['BB_Tax'] : "($" . $row['BB_Tax'] . ")";
         $total_two     = ($refund_class == '') ? "$" . $row['BB_Total'] : "($" . $row['BB_Total'] . ")";
        echo $date . ' ,';
        echo $row['BB_Invoice_Num'] . ' ,';
        echo $subtotal_two . ' ,';
        echo $tax_two . ' ,';
        echo $total_two;
        echo "\n";
       // echo $output;
    }
    
    $overall_listing_subtotal += $subtotal - $refund_subtotal;
    $overall_listing_tax += $tax - $refund_tax;
    $overall_listing_total += $total - $refund_total;

    $overall_subtotal += $subtotal;
    $overall_tax += $tax;
    $overall_total += $total;
    $overall_refund_subtotal += $refund_subtotal;
    $overall_refund_tax += $refund_tax;
    $overall_refund_total += $refund_total;
    $commission = $subtotal / 100;
    $commission = round($commission * $commission_percent, 2);
    $grand_sub_total = $grand_sub_total + $subtotal;
    $grand_tax_total = $grand_tax_total + $tax;
    $grand_total = $grand_total + $total;

    $output1 = 'Subtotal , ,,,,';
    $output1 .= "$" . $subtotal . ' ,'; 
    $output1 .= "$" . $tax . ' ,';
    $output1 .= "$" . $total;
    $output1 .= "\n \n ";
    echo $output1;

    /////Refund
    $output2 = 'Refund , ,,,,';
    $output2 .= "$" . $refund_subtotal . ' ,'; 
    $output2 .= "$" . $refund_tax . ' ,';
    $output2 .= "$" . $refund_total;
    $output2 .= "\n \n ";
    echo $output2;



    $total_sub = $subtotal - $refund_subtotal;
    $total_tax = $tax - $refund_tax;
    $total_tot = $total - $refund_total;
    $output = 'Total , , , ,, ';
    $output .= "$" . $total_sub . ' ,';
    $output .= "$" . $total_tax . ' ,';
    $output .= "$" . $total_tot;
    $output .= "\n \n ";
    echo $output;

    $output5 = 'Monthly Listing Sales Total , ,,,, ';
    $output5 .= "$" . $overall_listing_subtotal . ' ,';
    $output5 .= "$" . $overall_listing_tax . ' ,';
    $output5 .= "$" . $overall_listing_total;
    $output5 .= "\n \n ";
    echo $output5;

    if ($sales_user != "") {
        $output = 'Commission , , , , , , ,';
        $output .= "$" . $commission;
        $output .= "\n \n ";
        echo $output;
    }
//// OK
    //Advertisement

    $output = 'Division Sales (Credit Card)  -' . $display_date . ', ';
    $output .= "\n";
    echo $output;

    $output = 'Business Name , Card Holder , Card Type , Date , Inv. # , Sub-Total , Tax , Total ';
    $output .= "\n";
    echo $output;

    ///test3
    $division_billings = array();
    $division_billings_refund = array();
    $all_division_billings = array();
    //division billings without refunds
    $sql_db = "SELECT DB_ID, DB_Customer_Name, DB_Date, DB_Subtotal, DB_Tax, DB_Total, DB_Refund_Deleted_Date, DB_Card_Type, BL_ID, BL_Listing_Title,
                   DB_First_Name, DB_Last_Name FROM tbl_Division_Billing LEFT JOIN tbl_Business_Listing ON DB_BL_ID = BL_ID
                   LEFT JOIN payment_profiles ON business_id = BL_B_ID WHERE DB_PT_ID = 4 AND DB_Total > 0 AND DB_Deleted = 0 $where_clause_division_billing ORDER BY DB_ID";
    $result_db = mysql_query($sql_db, $db) or die("Invalid query: $sql_db -- " . mysql_error());
    $i = 0;
    while ($row_db = mysql_fetch_assoc($result_db)) {
        $division_billings[$i] = $row_db;
        $division_billings[$i]['date'] = $row_db['DB_Date'];
        $i++;
    }
    $sql_db = "SELECT DB_ID, DB_Customer_Name, DB_Date, DB_Subtotal, DB_Tax, DB_Total, DB_Refund_Deleted, DB_Refund_Deleted_Date, DB_Card_Type, BL_ID, BL_Listing_Title,
                   DB_First_Name, DB_Last_Name FROM tbl_Division_Billing LEFT JOIN tbl_Business_Listing ON DB_BL_ID = BL_ID
                   LEFT JOIN payment_profiles ON business_id = BL_B_ID WHERE DB_PT_ID = 4 AND DB_Total > 0 AND DB_Deleted = 0 AND DB_Refund_Deleted = 1 $where_clause_division_billing_refund ORDER BY DB_ID";
    $result_db = mysql_query($sql_db, $db) or die("Invalid query: $sql_db -- " . mysql_error());
    $i = 0;
    while ($row_db = mysql_fetch_assoc($result_db)) {
        $division_billings_refund[$i] = $row_db;
        $division_billings_refund[$i]['date'] = $row_db['DB_Refund_Deleted_Date'];
        $i++;
    }
    $all_division_billings = array_merge($division_billings, $division_billings_refund);

    function sortDivision($a, $b) {
        return strtotime($a['date']) - strtotime($b['date']);
    }

    usort($all_division_billings, "sortDivision");

    $subtotal = $tax = $total = $refund_subtotal = $refund_tax = $refund_total = 0;
    $overall_billing_subtotal = $overall_billing_tax = $overall_billing_total = 0;
    foreach ($all_division_billings as $row_db) { //echo '<pre>'; print_r($row_db);
        //show refunded columns in red
        $refund_class = "";
        if (isset($row_db['DB_Refund_Deleted']) && $row_db['DB_Refund_Deleted'] == 1) {
            $date_db = date("m/d/Y", strtotime($row_db['DB_Refund_Deleted_Date']));
            $refund_class = " refund";
            $refund_subtotal += $row_db['DB_Subtotal'];
            $refund_tax += $row_db['DB_Tax'];
            $refund_total += $row_db['DB_Total'];
        } else {
            $date_db = date("m/d/Y", strtotime($row_db['DB_Date']));
            $subtotal = $subtotal + $row_db['DB_Subtotal'];
            $tax = $tax + $row_db['DB_Tax'];
            $total = $total + $row_db['DB_Total'];
        }
//        if ($row_db['BL_ID'] > 0) {
//            $output .= $row_db['BL_Listing_Title']. ',';
//        } else {
//            $output .= $row_db['DB_Customer_Name']. ',';
//        }
       echo $output = str_replace(",", " ", $row_db['BL_Listing_Title']) . ',';
        if (isset($row_db['DB_First_Name'])) {
            echo $row_db['DB_First_Name'] . ' ' . $row_db['DB_Last_Name'] . ' ,';
        } else {
            echo $row_db['DB_First_Name'] . ' ' . $row_db['DB_Last_Name'] . ' ,';
        }
        if ($row_db['DB_Card_Type'] != '') {
            if ($row_db['DB_Card_Type'] == 'Visa') {
                echo 'V ,';
            } else if ($row_db['DB_Card_Type'] == 'MasterCard') {
                echo 'MC ,';
            } else if ($row_db['DB_Card_Type'] == 'American Express') {
                echo 'A ,';
            } else {
                echo 'Cash ,';
            }
        } else {
            echo 'Cash ,';
        }
        if ($row_db['DB_Date'] ) {
            echo $row_db['DB_Date']. ',';
        }
        if ($row_db['BL_ID'] >0 ) {
            echo $row_db['BL_ID']. ',';
        }
        if ($row_db['DB_Subtotal'] ) {
            $a = ($refund_class == '') ? "$" . $row_db['DB_Subtotal'] : "($" . $row_db['DB_Subtotal'] . ")";
            echo $a. ',';
        }
        if ($row_db['DB_Tax'] ) {
            $b= ($refund_class == '') ? "$" . $row_db['DB_Tax'] : "($" . $row_db['DB_Tax'] . ")";
            echo $b. ',';
        }
        if ($row_db['DB_Total'] ) {
            $c = ($refund_class == '') ? "$" . $row_db['DB_Total'] : "($" . $row_db['DB_Total'] . ")";
            echo $c. ',';
        }
        echo "\n";
        ////

        
    }
    $overall_billing_subtotal = $subtotal - $refund_subtotal;
    $overall_billing_tax = $tax - $refund_tax;
    $overall_billing_total = $total - $refund_total;
    $overall_subtotal += $subtotal;
    $overall_tax += $tax;
    $overall_total += $total;
    $overall_refund_subtotal += $refund_subtotal;
    $overall_refund_tax += $refund_tax;
    $overall_refund_total += $refund_total;
    $grand_division_sub_total = $grand_division_sub_total + $subtotal;
    $grand_division_tax_total = $grand_division_tax_total + $tax;
    $grand_division_total = $grand_division_total + $total;
    $output1 = "\n \n ";
    $output1 .= 'Subtotal,,,,,';
    $output1 .= "$" . $subtotal . ',';
    //$output1 .= "$" . $subtotal . ' ,';
    $output1 .= "$" . $tax . ',';
    $output1 .= "$" . $total;
    $output1 .= "\n \n ";
    echo $output1;

    /////Refund
    $output2 = 'Refund,,,,,';
    $output2 .= "$" . $refund_subtotal . ' ,'; 
    $output2 .= "$" . $refund_tax . ',';
    $output2 .= "$" . $refund_total;
    $output2 .= "\n \n ";
    echo $output2;

    $total_sub1 = $subtotal - $refund_subtotal;
    $total_tax1 = $tax - $refund_tax;
    $total_tot1 = $total - $refund_total;
    $output4 = 'Total,,,,,';
    $output4 .= "$" . $total_sub1 . ',';
    $output4 .= "$" . $total_tax1 . ',';
    $output4 .= "$" . $total_tot1;
    $output4 .= "\n \n ";
    echo $output4;

    $output = 'Division Sales (Cheque/Cash/E-transfer)  -' . $display_date . ', ';
    $output .= "\n";
    echo $output;

    $output = 'Business Name , Card Holder , Card Type , Date , Inv. # , Sub-Total , Tax , Total ';
    $output .= "\n";
    echo $output;

    ///test4 strt
    $division_billings_nonCC = array();
    $division_billings_nonCC_refund = array();
    $all_division_nonCC_billings = array();
    $sql_db = "SELECT DB_ID, DB_Customer_Name, DB_Date, DB_Subtotal, DB_Tax, DB_Total, BL_ID, BL_Listing_Title, PT_Name 
                   FROM tbl_Division_Billing LEFT JOIN tbl_Business_Listing ON DB_BL_ID = BL_ID
                   LEFT JOIN payment_profiles ON business_id = BL_B_ID
                   LEFT JOIN tbl_Payment_Type ON DB_PT_ID = PT_ID
                   WHERE DB_PT_ID != 4 AND DB_Total > 0 AND DB_Deleted = 0 $where_clause_division_billing 
                   ORDER BY DB_ID";
    $result_db = mysql_query($sql_db, $db) or die("Invalid query: $sql_db -- " . mysql_error());
    $i = 0;
    while ($row_db = mysql_fetch_assoc($result_db)) {
        $division_billings_nonCC[$i] = $row_db;
        $division_billings_nonCC[$i]['date'] = $row_db['DB_Date'];
        $i++;
    }
    $sql_db = "SELECT DB_ID, DB_Customer_Name, DB_Date, DB_Subtotal, DB_Tax, DB_Total, BL_ID, BL_Listing_Title, PT_Name, 
                   DB_Refund_Deleted, DB_Refund_Deleted_Date
                   FROM tbl_Division_Billing LEFT JOIN tbl_Business_Listing ON DB_BL_ID = BL_ID
                   LEFT JOIN payment_profiles ON business_id = BL_B_ID 
                   LEFT JOIN tbl_Payment_Type ON DB_PT_ID = PT_ID
                   WHERE DB_PT_ID != 4 AND DB_Total > 0 AND DB_Deleted = 0 AND 
                   DB_Refund_Deleted = 1 $where_clause_division_billing_refund ORDER BY DB_ID";
    $result_db = mysql_query($sql_db, $db) or die("Invalid query: $sql_db -- " . mysql_error());
    $i = 0;
    while ($row_db = mysql_fetch_assoc($result_db)) {
        $division_billings_nonCC_refund[$i] = $row_db;
        $division_billings_nonCC_refund[$i]['date'] = $row_db['DB_Refund_Deleted_Date'];
        $i++;
    }
    $all_division_nonCC_billings = array_merge($division_billings_nonCC, $division_billings_nonCC_refund);

    function sortDivisionNonCC($a, $b) {
        return strtotime($a['date']) - strtotime($b['date']);
    }

    usort($all_division_nonCC_billings, "sortDivisionNonCC");

    $subtotal = $tax = $total = $refund_subtotal = $refund_tax = $refund_total = 0;
    foreach ($all_division_nonCC_billings as $row_db) {  //echo '<pre>'; print_r($row_db);
        //show refunded columns in red
        $date_advert = date("m/d/Y", strtotime($row_db['DB_Customer_Name']));
        $refund_class = "";
        if (isset($row_db['DB_Refund_Deleted']) && $row_db['DB_Refund_Deleted'] == 1) {
            $date_db = date("m/d/Y", strtotime($row_db['DB_Refund_Deleted_Date']));
            $refund_class = " refund";
            $refund_subtotal += $row_db['DB_Subtotal'];
            $refund_tax += $row_db['DB_Tax'];
            $refund_total += $row_db['DB_Total'];
        } else {
            $date_db = date("m/d/Y", strtotime($row_db['DB_Date']));
            $subtotal = $subtotal + $row_db['DB_Subtotal'];
            $tax = $tax + $row_db['DB_Tax'];
            $total = $total + $row_db['DB_Total'];
        }

        if ($row_db['BL_ID'] > 0) {
            $output = str_replace(",", " ", $row_db['BL_Listing_Title']) . ',';
        } else {
            $output = str_replace(",", " ", $row_db['BL_Listing_Title']) . ',';
        }

        if (isset($row_db['PT_Name'])) {
            $output .= $row_db['PT_Name'] . ' ,';
        } else {
            $output .= $row_db['PT_Name'] . ' ,';
        }
        if ($row_db['PT_Name'] != '') {
            if ($row_db['PT_Name'] == 'Visa') {
                $output .= 'V ,';
            } else if ($row_db['PT_Name'] == 'MasterCard') {
                $output .= 'MC ,';
            } else if ($row_db['PT_Name'] == 'American Express') {
                $output .= 'A ,';
            } else {
                $output .= 'N/A ,';
            }
        } else {
            $output .= 'N/A ,';
        }
        $total_third_sub= ($refund_class == '') ? "$" . $row_db['DB_Subtotal'] : "($" . $row_db['DB_Subtotal'] . ")" ;
        $total_third_tax=($refund_class == '') ? "$" . $row_db['DB_Tax'] : "($" . $row_db['DB_Tax'] . ")";
        $total_third_total= ($refund_class == '') ? "$" . $row_db['DB_Total'] : "($" . $row_db['DB_Total'] . ")";
        $output .= $date_advert . ' ,';
        $output .= $row_db['DB_ID'] . ' ,';
        $output .= $total_third_sub . ' ,';
        $output .= $total_third_tax . ' ,';
        $output .= $total_third_total;
        $output .= "\n";
        echo $output;
    }
    $overall_billing_subtotal += $subtotal - $refund_subtotal;
    $overall_billing_tax += $tax - $refund_tax;
    $overall_billing_total += $total - $refund_total;
    $overall_subtotal += $subtotal;
    $overall_tax += $tax;
    $overall_total += $total;
    $overall_refund_subtotal += $refund_subtotal;
    $overall_refund_tax += $refund_tax;
    $overall_refund_total += $refund_total;
    $grand_division_sub_total = $grand_division_sub_total + $subtotal;
    $grand_division_tax_total = $grand_division_tax_total + $tax;
    $grand_division_total = $grand_division_total + $total;

//,,,,,';
    $output1 = "Subtotal" . ', ,,,, ';
    $output1 .= "$" . $subtotal . ' ,';
    $output1 .= "$" . $tax . ' ,';
    $output1 .= "$" . $total;
    $output1 .= "\n";
    echo $output1;

    $output2 = "Refunds" . ', , ,,,';
    $output2 .= "$" . $refund_subtotal . ' ,';
    $output2 .= "$" . $refund_tax . ' ,';
    $output2 .= "$" . $refund_total;
    $output2 .= "\n";
    echo $output2;

    $val_sub = $subtotal - $refund_subtotal;
    $val_txt = $tax - $refund_tax;
    $val_total = $total - $refund_total;

    $output3 = "Total" . ', , ,,,';
    $output3 .= "$" . $val_sub . ' ,';
    $output3 .= "$" . $val_txt . ' ,';
    $output3 .= "$" . $val_total;
    $output3 .= "\n";
    echo $output3;

    $output4 = "Monthly Division Sales Total" . ', ,,,, ';
    $output4 .= "$" . $overall_billing_subtotal . ' ,';
    $output4 .= "$" . $overall_billing_tax . ' ,';
    $output4 .= "$" . $overall_billing_total;
    $output4 .= "\n";
    echo $output4;

    $val_billing_subtotal = $overall_subtotal - $overall_refund_subtotal;
    $val_billing_tax = $overall_tax - $overall_refund_tax;
    $val_billing_total = $overall_total - $overall_refund_total;

    $output5 = "Monthly Grand Total" . ', , ,,,';
    $output5 .= "$" . $val_billing_subtotal . ' ,';
    $output5 .= "$" . $val_billing_tax . ' ,';
    $output5 .= "$" . $val_billing_total;
    $output5 .= "\n";
    echo $output5;
    ///test end
}
?>



