<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';
if (!in_array('customers', $_SESSION['USER_PERMISSIONS']) && !in_array('business-listings', $_SESSION['USER_PERMISSIONS'])  && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Pending Listings</div>
        <div class="link">           
        </div>
    </div>
    <div class="left">
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column spl-name padding-none">Name</div>
            <div class="data-column spl-other padding-none">Edit</div>
            <div class="data-column spl-other padding-none">Delete</div>
        </div>
        <?php
            $sql_listings = "SELECT BL_ID,BL_Listing_Title FROM tbl_Business_Listing ";
            $result_listings=mysql_query($sql_listings);
            while ($row_listings = mysql_fetch_array($result_listings)) {
               $sql_cat = "SELECT BLC_BL_ID FROM tbl_Business_Listing_Category WHERE BLC_BL_ID = ".$row_listings['BL_ID'];
               $result_cat=mysql_query($sql_cat);
               $row_cat_count=mysql_num_rows($result_cat);
               $sql_region = "SELECT BLCR_BL_ID FROM tbl_Business_Listing_Category_Region WHERE BLCR_BL_ID = ".$row_listings['BL_ID'];
               $result_region=mysql_query($sql_region);
               $row_cat_region=mysql_num_rows($result_region);
               if($row_cat_count==0 || $row_cat_region==0)
               {
                   ?>
                  <div class="data-content">
                    <div class="data-column spl-name"><?php echo $row_listings['BL_Listing_Title'] ?></div>
                    <div class="data-column spl-other"><a href="customer-listing-overview.php?bl_id=<?php echo $row_listings['BL_ID'] ?>">Edit</a></div>
                    <div class="data-column spl-other"><a onClick="return confirm('Are you sure?');" href="customer-listing-overview.php?bl_id=<?php echo $row_listings['BL_ID'] ?>&op=del&return=pending">Delete</a></div>
                  </div>
                 <?php
               }
            }
         ?>            
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>