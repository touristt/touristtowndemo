<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('categories', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if (isset($_GET['op']) && $_GET['op'] == 'del') {
    //Delete sub-categories
    $sql_sub = "DELETE tbl_Category FROM tbl_Category WHERE C_Parent = '" . $_REQUEST['id'] . "'";
    $res_sub = mysql_query($sql_sub, $db);
    //Delete main-category
    $sql_main = "DELETE tbl_Category FROM tbl_Category WHERE C_ID = '" . $_REQUEST['id'] . "'";
    $res_main = mysql_query($sql_main, $db);
    if ($res_main) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['id'];
        Track_Data_Entry('Categories','','Manage Categories',$id,'Delete Category','super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: categories.php");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Categories</div>
        <div class="link">
            <a href="category-add.php">+Add Category</a>
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-managecategories.php'; ?>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column spl-name padding-none">Name</div>
            <div class="data-column spl-other padding-none">Edit</div>
            <div class="data-column spl-other padding-none">Delete</div>
        </div>
        <div class="reorder-category">
            <?PHP
            $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = 0 ORDER BY C_Order";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                ?>
                <div class="data-content" id="recordsArray_<?php echo $row['C_ID'] ?>">
                    <form id="form3" name="form3" enctype="multipart/form-data" method="post" action="category.php">
                        <input type="hidden" name="op" value="save">
                        <div class="data-content">
                            <div class="data-column spl-name">
                                <a href="category.php?id=<?php echo $row['C_ID'] ?>"><?php echo $row['C_Name'] ?></a>
                            </div>
                            <div class="data-column spl-other">
                                <a href="category-add.php?id=<?php echo $row['C_ID'] ?>">Edit</a>
                            </div>
                            <div class="data-column spl-other"><a onClick="return confirm('Are you sure? Sub-categories will also be deleted.')" href="categories.php?id=<?php echo $row['C_ID'] ?>&op=del">Delete</a></div>
                        </div>
                    </form>
                </div>
                <?PHP
            }
            ?>
            <div id="image-library" style="display:none;"></div>
        </div>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script>
    $(function () {
        $(".reorder-category").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Category&field=C_Order&id=C_ID';
                $.post("reorder.php", order, function (theResponse) {
                    swal("Categories Re-Ordered", "Categories Re-Ordered successfully.", "success");
                });
            }
        });
    });
</script>