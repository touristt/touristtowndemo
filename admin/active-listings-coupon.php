<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';
require_once '../include/track-data-entry.php';

if (!in_array('customers', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
if (isset($_GET['op']) && $_GET['op'] == 'pending') {
    $sql = "UPDATE tbl_Business_Feature_Coupon SET BFC_Status = 0 WHERE BFC_ID = '" . encode_strings($_REQUEST['bfc_id'], $db) . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['bfc_id'];
        Track_Data_Entry('Coupons','','Manage Coupon - Active Coupons',$id,'Pending','super admin');
    } else {
        $_SESSION['email_existence_error'] = 1;
    }
    header("Location: active-listings-coupon.php");
    exit();
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $delCouponImg = "DELETE tbl_Business_Feature_Coupon,tbl_Business_Feature_Coupon_Category_Multiple FROM tbl_Business_Feature_Coupon LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID WHERE BFC_ID = '" . encode_strings($_REQUEST['bfc_id'], $db) . "'";
    $resDelCouponImg = mysql_query($delCouponImg, $db) or die("Invalid query: $delCouponImg -- " . mysql_error());
    if ($resDelCouponImg) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['bfc_id'];
        Track_Data_Entry('Coupons','','Manage Coupon - Active Coupons',$id,'Delete','super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location:active-listings-coupon.php");
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Coupon - Active Coupons</div>
        <div class="link">           
        </div>
    </div>
    <div class="left">
        <?PHP
        require '../include/nav-manage-coupon.php';
        ?>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name-events">Name</div>
            <div class="busniss-name">Business Name</div>
            <div class="data-column padding-none spl-other active">Downloads</div>
            <div class="data-column padding-none spl-other active">Pending</div>            
            <div class="data-column padding-none spl-other active">Delete</div>
        </div>
        <?PHP
        $sql = "SELECT BL_ID, BFC_ID, BFC_Title, BL_Listing_Title FROM tbl_Business_Feature_Coupon
                LEFT JOIN tbl_Business_Listing ON BFC_BL_ID = BL_ID
                WHERE BFC_Status = 1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date =0000-00-00) ";
        if (strlen($_REQUEST['strSearch']) > 3) {
            $sql .= " And BFC_Title LIKE '%" . encode_strings($_REQUEST['strSearch'], $db) . "%' ";
        }
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $pages = new Paginate(mysql_num_rows($result), 100);
        $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            ?>
            <div class="data-content">
                <div class="data-column spl-name-events"><a href="customer-feature-add-coupons.php?bl_id=<?php echo $row['BL_ID'] ?>&bfc_id=<?php echo $row['BFC_ID'] ?>&re_dir=1"><?php echo $row['BFC_Title'] ?></a></div>
                <div id="event-community" class="data-column spl-other-cc-list"><?php echo $row['BL_Listing_Title'] ?></div>
                <div class="data-column spl-other active"><a href="downloaded-coupons.php?bfc_id=<?php echo $row['BFC_ID'] ?>">Downloads</a></div>  
                <div class="data-column spl-other active"><a onclick="return confirm('Are you sure?')" href="active-listings-coupon.php?op=pending&bfc_id=<?php echo $row['BFC_ID'] ?>">Pending</a></div>  
                <div class="data-column spl-other active"><a onClick="return confirm('Are you sure?');" href="active-listings-coupon.php?bl_id=<?php echo $row['BL_ID'] ?>&bfc_id=<?php echo $row['BFC_ID'] ?>&op=del">Delete</a></div>
            </div>
            <?PHP
        }
        ?> 

        <?php
        if (isset($pages)) {
            echo $pages->paginate();
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>