<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('manage-users', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Users</div>
        <div class="addlisting"><a href="user.php">+Add User</a></div>
        <div class="link">
        </div>
    </div> 
    <div class="left"><?php require_once('../include/nav-home-sub.php'); ?></div>
    <div class="right">
        <?php
        $role = "SELECT R_Name, R_ID FROM tbl_Role ORDER BY R_Order ASC";
        $roleResult = mysql_query($role, $db) or die("Invalid query: $role -- " . mysql_error());
        while ($roleRow = mysql_fetch_assoc($roleResult)) {
            ?>
            <div class="content-header"><?php echo $roleRow['R_Name'] ?></div>
            <div class="content-sub-header">
                <div class="data-column padding-none mup-name">
                    <?php
                    if (isset($roleRow['R_ID']) == 'bgadmin') {
                        echo "Name";
                    } else {
                        echo "User's Name";
                    }
                    ?>
                </div>
                <div class="data-column padding-none mup-email">Email Address</div>
                <div class="data-column padding-none mup-region">
                    <?php
                    if ($roleRow['R_ID'] != 'superadmin') {
                        echo 'Region';
                    }
                    ?>
                </div>
                <div class="data-column padding-none mup-other">Edit</div>
                <div class="data-column padding-none mup-other">Delete</div>
            </div>
            <?PHP
            $sql = "SELECT U_F_Name, U_Email, U_ID, UR_Limit FROM tbl_User LEFT JOIN tbl_User_Role ON U_ID = UR_U_ID
                    WHERE UR_R_ID = '" . $roleRow['R_ID'] . "' ORDER BY U_F_Name";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                ?>
                <div class="data-content">
                    <div class="data-column mup-name">
                        <?php
                        echo $row['U_F_Name'];
                        ?>
                    </div>
                    <div class="data-column mup-email"><?php echo $row['U_Email'] ?></div>
                    <div class="data-column mup-region">
                        <?php
                        if ($roleRow['R_ID'] != 'superadmin') {
                            $limits = explode(',', $row['UR_Limit']);
                            $first = true;
                            foreach ($limits as $limit) {
                                $sql1 = "SELECT R_Name FROM tbl_Region WHERE R_ID = '" . $limit . "' ORDER BY R_Name";
                                $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
                                $row1 = mysql_fetch_assoc($result1);
                                if($first == true){
                                    echo $row1['R_Name'];
                                    $first = false;
                                }else{
                                    echo ', ' . $row1['R_Name'];
                                }
                            }
                        }
                        ?></div>
                    <div class="data-column mup-other"><a href="user.php?id=<?php echo $row['U_ID'] ?>">Edit</a></div>
                    <div class="data-column mup-other"><a onClick="return confirm('Are you sure?\nThis action CANNOT be undone!');" href="user.php?id=<?php echo $row['U_ID'] ?>&amp;op=del">Delete</a></div>
                </div>
                <?PHP
            }
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>