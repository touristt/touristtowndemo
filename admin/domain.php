<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['id'] > 0) {
    $sql = "SELECT * FROM tbl_Domain WHERE D_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'save') {
    $sql = "tbl_Domain SET 
            D_Domain = '" . encode_strings($_REQUEST['domain'], $db) . "',
            D_Renewal_Date = '" . encode_strings($_REQUEST['date'], $db) . "'";

    if ($row['D_ID'] > 0) {
        $sql = "UPDATE " . $sql . " WHERE D_ID = '" . $row['D_ID'] . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $row['D_ID'];
        Track_Data_Entry('Domains', '', 'Domain Details', $id, 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id($db);
        // TRACK DATA ENTRY
        Track_Data_Entry('Domains', '', 'Domain Details', $id, 'Add', 'super admin');
    }

    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: domains.php");
    exit();
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $sql = "DELETE tbl_Domain FROM tbl_Domain WHERE D_ID = '" . $row['D_ID'] . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $row['D_ID'];
        Track_Data_Entry('Domains', '', 'Manage Domains', $id, 'Delete', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }

    header("Location: domains.php");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Domains</div>
        <div class="addlisting"></div>
    </div>
    
    <div class="right full-width">
        <form name="form1" method="post" action="domain.php">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="id" value="<?php echo $row['D_ID'] ?>">
            <div class="content-header full-width">Domain Details</div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Domain</label>
                <div class="form-data">
                    <input name="domain" type="text" value="<?php echo $row['D_Domain'] ?>" size="40" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Renewal Date</label>
                <div class="form-data">
                    <input name="date" class="datepicker" type="text" value="<?php echo $row['D_Renewal_Date'] ?>" size="40" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width"> 
                <div class="button">
                    <input type="submit" name="button" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>