<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/ranking.inc.php';

if (!in_array('billing-history', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$BL_ID = $_REQUEST['bl_id'];
$ID = $_REQUEST['id'];

if (isset($BL_ID) && $BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
} else {
    header('Location: customers.php');
}

if (isset($ID) && $ID > 0) {
    $sql = "SELECT tbl_Business_Billing.*, BL_Listing_Title, BL_Street, BL_Address, BL_Town, BL_Province, BL_PostalCode, BL_Billing_Type, BLCR_BLC_R_ID, PT_Name 
            FROM tbl_Business_Billing 
            LEFT JOIN tbl_Payment_Status ON BB_Payment_Status = PS_ID 
            LEFT JOIN tbl_Payment_Type ON BB_Payment_Type = PT_ID 
            LEFT JOIN tbl_User ON BB_Account_Manager = U_ID
            LEFT JOIN tbl_Category ON BB_Category = C_ID
            LEFT JOIN tbl_Business_Listing ON BL_ID = BB_BL_ID
            LEFT JOIN tbl_Business_Listing_Category  ON BLC_BL_ID = BL_ID
            INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
            WHERE BB_ID = '$ID' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowBus = mysql_fetch_assoc($result);
    $region = $rowBus['BLCR_BLC_R_ID'];
} else {
    header("Location:customer-billing.php?bl_id=" . $BL_ID);
    exit();
}

//billing period for which the amount was charged
//monthly billing
if ($rowBus['BL_Billing_Type'] == 1) {
    $start = strtotime("-1 month", strtotime($rowBus['BB_Renewal_Date']));
    $invoice_start_date = date("M jS, Y", $start);
} else if ($rowBus['BL_Billing_Type'] == -1) {
    $end = strtotime("-1 year", strtotime($rowBus['BB_Renewal_Date']));
    $invoice_start_date = date("M jS, Y", $end);
}
$invoice_end_date = date("M jS, Y", strtotime("-1 day", strtotime($rowBus['BB_Renewal_Date'])));

$sql_region = "SELECT R_Clientname, R_Street, R_Town, R_Province, R_PostalCode FROM tbl_Region WHERE R_ID = $region LIMIT 1";
$res_region = mysql_query($sql_region);
$rowreg = mysql_fetch_assoc($res_region);

require_once '../include/admin/header.php';
?>
<div class="content-left">

    <?php require_once '../include/top-nav-listing.php'; ?>

    <div class="title-link">
        <div class="title">Invoices - <?php echo (strlen($rowListing['BL_Listing_Title']) > 30) ? substr($rowListing['BL_Listing_Title'], 0, 30) . '...' : $rowListing['BL_Listing_Title']; ?></div>
        <div class="link">
            <?php if (in_array('delete-invoices', $_SESSION['USER_PERMISSIONS'])) { ?>
                <a class="invoices" href="/admin/customer-bill-delete.php?id=<?php echo $rowBus['BB_ID'] ?>&op=del&bl_id=<?php echo $BL_ID ?>">Delete Invoice</a></td>
            <?php } ?>
            <?php
            if ($rowBus['BB_Refund_Deleted'] != 1) {
                $cc_refund = 0;
                if ($rowBus['BB_Transaction_ID'] > 0) {
                    $cc_refund = 1;
                }
                if (in_array('superadmin', $_SESSION['USER_ROLES'])) {
                    $current_date = strtotime(date('Y-m-d'));
                    $billing_date = strtotime($rowBus['BB_Date']);
                    $secs = $current_date - $billing_date; // == <seconds between the two times>
                    $days = $secs / 86400;
                    if ($days < 121) {
                        ?>
                        <a class="invoices" href="/admin/customer-bill-delete.php?id=<?php echo $rowBus['BB_ID'] ?>&op=refund&bl_id=<?php echo $BL_ID ?>&cc_refund=<?php echo $cc_refund; ?>">Refund Invoice</a></td>
                        <?php
                    }
                }
            } else {
                echo '<span class="invoices-text">Refunded</span>';
            }
            ?>
        </div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-billing.php'; ?>
    </div>
    <div class="right">
        <div class="page-print">
            <div class="billing-inside-div-tittle">
                <div class="data-content">
                    <div class="data-column invoice-heading">Invoice Number : </div>
                    <div class="data-column invoice-no"><?php echo $rowBus['BB_Invoice_Num'] ?></div>
                    <div class="data-column invoice-date">Date Paid: </div>
                    <div class="data-column invoice-date-bb"><?php echo $rowBus['BB_Date'] ?></div>
                    <?php if ($rowBus['BB_Refund_Deleted'] == 1) { ?>
                        <div class="data-column invoice-heading"></div>
                        <div class="data-column invoice-no"></div>
                        <div class="data-column invoice-date">Date Refund: </div>
                        <div class="data-column invoice-date-bb"><?php echo date('Y-m-d', strtotime($rowBus['BB_Refund_Deleted_Date'])) ?></div>
                    <?php } ?>
                </div>
            </div>
            <div class="invoice-address">
                <div class="data-left  inviced-left">
                    <div class="data-header-inv bold inviced-head">Invoiced To</div>
                    <div class="data-content-address margine-inviced-bottom">
                        <?php echo $rowBus['BL_Listing_Title'] != '' ? trim($rowBus['BL_Listing_Title']) : ''; ?>
                        <?php echo $rowBus['BL_Street'] != '' ? '<br>' . trim($rowBus['BL_Street']) : ''; ?>
                        <?php echo $rowBus['BL_Address'] != '' ? '<br>' . trim($rowBus['BL_Address']) : ''; ?>
                        <?php
                        echo $rowBus['BL_Town'] != '' ? '<br>' . trim($rowBus['BL_Town']) : '';
                        echo $rowBus['BL_Province'] != '' ? ', ' . trim($rowBus['BL_Province']) : '';
                        echo $rowBus['BL_PostalCode'] != '' ? ', ' . trim($rowBus['BL_PostalCode']) : '';
                        ?>
                        <br> Canada
                    </div>
                </div>
                <div class="data-right">
                    <div class="data-header-inv bold pay-head">Pay To</div>
                    <div class="data-content-address margine-inviced-bottom">
                        <?php
                        echo $rowreg['R_Clientname'] != '' ? trim($rowreg['R_Clientname']) : '';
                        echo $rowreg['R_Street'] != '' ? '<br>' . trim($rowreg['R_Street']) : '';
                        echo $rowreg['R_Town'] != '' ? '<br>' . trim($rowreg['R_Town']) : '';
                        echo $rowreg['R_Province'] != '' ? ', ' . trim($rowreg['R_Province']) : '';
                        echo $rowreg['R_PostalCode'] != '' ? ', ' . trim($rowreg['R_PostalCode']) : '';
                        ?>
                        <br>Canada

                    </div>
                </div>
            </div>
            <div class="invoice-pay-main">
                <div class="data-column bold payment-text type-width">Payment Type :</div>
                <div class="data-column card-name"><?php echo $rowBus['PT_Name'] ?></div>
            </div>
            <div class="card-main">
                <div class="data-column bold last-card-digits">Last four digits of card :</div>
                <?php
                $cardno = explode("XXXX", $rowBus['BB_Card_Number']);
                ?>
                <div class="data-column last-card-digits-no"><?php echo ($cardno[1] != '') ? $cardno[1] : 'N/A' ?></div>
            </div>

            <div class="data-header bold inviced-detail inviced-order-heading">Order Summary</div>
            <div class="data-content billing-period">
                <div class="data-column">Billing Period:</div>
                <div class="data-column dates-column"><?php echo $invoice_start_date . ' - ' . $invoice_end_date; ?></div>
            </div>
            <?PHP
            $sql = "SELECT * FROM tbl_Business_Billing_Lines WHERE BBL_BB_ID = '" . encode_strings($rowBus['BB_ID'], $db) . "'";
            $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($resultTMP)) {
                ?>
                <div class="data-content">
                    <div class="data-column inviced-tools-margin"><?php echo $row['BBL_Title'] ?></div>
                    <div class="data-column">$<?php echo $row['BBL_Price'] ?></div>
                </div>
                <?PHP
            }
            ?>
            <div class="data-header sub-total-color">
                <div class="data-column bold inviced-sub-margin ">Sub-Total 1</div>
                <div class="data-column sub-total-color">$<?php echo $rowBus['BB_SubTotal'] ?> </div>
            </div>
            <div class="data-content">
                <div class="data-column bold inviced-sub-margin">Discount</div>
                <div class="data-column">$<?php echo $rowBus['BB_CoC_Dis_2'] ?></div>
            </div>
            <div class="data-header sub-total-color">
                <div class="data-column bold inviced-sub-margin sub-total-color">Sub-Total 2</div>
                <div class="data-column sub-total-color">$<?php echo $rowBus['BB_SubTotal3'] ?></div>
            </div>
            <div class="data-content ">
                <div class="data-column bold inviced-sub-margin">Taxes (13%)</div>
                <div class="data-column">$<?php echo $rowBus['BB_Tax'] ?></div>
            </div>
            <div class="data-header total-invice-color ">
                <div class="data-column bold inviced-sub-margin total-invice-color">Total</div>
                <div class="data-column total-invice-color">$<?php echo $rowBus['BB_Total'] ?></div>
            </div>
            <div class="data-content">

                <div class="data-column renewel-date"><?php echo $rowBus['BB_Renewal_Date'] ?></div>
                <div class="data-column bold renewel-text">Your next invoice date is :</div>
            </div>

        </div>
        <div class="data-content print-link">
            <div class="print-button">
                <a href="#" onclick="window.print();">Print Invoice</a>
            </div>
        </div>
    </div>
</div>

<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>