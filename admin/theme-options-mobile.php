<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if (isset($_REQUEST['rid']) && $_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} else {
    header("Location: /admin/regions.php");
    exit();
}

$sql = "SELECT R_Type, R_Name FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);
$sql = "SELECT * FROM tbl_Theme_Options_Mobile WHERE TO_R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$count = mysql_num_rows($result);
$Region_theme = mysql_fetch_assoc($result);
if ($count > 0) {
    /*     * ******Exploding values to font[0]-size[1]-color[2]******* */
    $main_nav_value = explode("-", $Region_theme['TO_Main_Navigation']);
    $main_nav_sel_value = explode("-", $Region_theme['TO_Main_Navigation_Selected']);
    $slider_title_value = explode("-", $Region_theme['TO_Slider_Title']);
    $main_page_title_value = explode("-", $Region_theme['TO_Main_Page_Title']);
    $main_page_body_value = explode("-", $Region_theme['TO_Main_Page_Body_Copy']);
    $cat_title_value = explode("-", $Region_theme['TO_Thumbnail_Category_Tite']);
    $subcat_title_value = explode("-", $Region_theme['TO_Thumbnail_Sub_Category_Title']);
    $listing_title_value = explode("-", $Region_theme['TO_Thumbnail_Listing_Title']);
    $event_main_title_value = explode("-", $Region_theme['TO_Homepage_Event_Main_Title']);
    $event_date_value = explode("-", $Region_theme['TO_Homepage_Event_Date']);
    $event_title_value = explode("-", $Region_theme['TO_Homepage_Event_Title']);
    $footer_links_value = explode("-", $Region_theme['TO_Footer_Links']);
    $footer_disc_value = explode("-", $Region_theme['TO_Footer_Disclaimer']);
    $listing_title_text_value = explode("-", $Region_theme['TO_Listing_Title_Text']);
    $listing_title_location_value = explode("-", $Region_theme['TO_Listing_Location_Text']);
    $listing_sub_nav_value = explode("-", $Region_theme['TO_Listing_Sub_Nav_Text']);
    $listing_day_text_value = explode("-", $Region_theme['TO_Listing_Day_Text']);
    $listing_hours_text_value = explode("-", $Region_theme['TO_Listing_Hours_Text']);
    $amenities_text_value = explode("-", $Region_theme['TO_Amenities_Text']);
    $download_text_value = explode("-", $Region_theme['TO_Downloads_Text']);
    $get_this_coupon_title_text_value = explode("-", $Region_theme['TO_Get_This_Coupon_Title_Text']);
    $get_this_coupon_title_description_value = explode("-", $Region_theme['TO_Get_This_Coupon_Description_Text']);
    $general_body_copy_value = explode("-", $Region_theme['TO_General_Body_Copy']);
    $detail_page_event_main_title_value = explode("-", $Region_theme['TO_Event_Page_Main_Title']);
    $detail_page_event_date_value = explode("-", $Region_theme['TO_Event_Page_Date']);
    $detail_page_event_data_label_value = explode("-", $Region_theme['TO_Event_Page_Data_Label']);
    $detail_page_event_data_content_value = explode("-", $Region_theme['TO_Event_Page_Data_Content']);
    $story_main_title_value = explode("-", $Region_theme['TO_Story_Page_Main_Title']);
    $story_share_text_value = explode("-", $Region_theme['TO_Story_Share_Text']);
    $story_content_title_value = explode("-", $Region_theme['TO_Story_Page_Content_Title']);
    $route_main_title_value = explode("-", $Region_theme['TO_Route_Page_Main_Title']);
    $route_data_label_value = explode("-", $Region_theme['TO_Route_Page_Data_Label']);
    $route_data_content_value = explode("-", $Region_theme['TO_Route_Page_Data_Content']);
    $home_des_bg_color = explode(">", $Region_theme['TO_Homepage_Description_Background']);
    $home_des_bg = $home_des_bg_color[0];
    $home_des_color = $home_des_bg_color[1];
    $to_bar_texture_bg_color = explode(">", $Region_theme['TO_Bar_Texture']);
    $to_bar_texture_bg = $to_bar_texture_bg_color[0];
    $to_bar_texture_color = $to_bar_texture_bg_color[1];
    $to_subcat_texture_bg_color = explode(">", $Region_theme['TO_Subcat_Texture']);
    $to_subcat_texture_bg = $to_subcat_texture_bg_color[0];
    $to_subcat_texture_color = $to_subcat_texture_bg_color[1];
    $to_listing_texture_bg_color = explode(">", $Region_theme['TO_Listing_Texture']);
    $to_listing_texture_bg = $to_listing_texture_bg_color[0];
    $to_listing_texture_color = $to_listing_texture_bg_color[1];
    $to_category_header_text = explode("-", $Region_theme['TO_Category_Header_Text']);
    $to_category_header_bg_color = explode(">", $Region_theme['TO_Category_Header_BG']);
    $to_category_header_bg = $to_category_header_bg_color[0];
    $to_category_header_color = $to_category_header_bg_color[1];
    $text_box_text_inside_search_box_value = explode("-", $Region_theme['TO_Text_Box_Text_Inside_Box']);
    $search_box_button_text_value = explode("-", $Region_theme['TO_Search_Box_Button_Text']);
}

if (isset($_POST['save_color'])) {
    /*     * *Concatenating font-size-color** */
    $main_nav = $_REQUEST['main_nav_font'] . "-" . $_REQUEST['main_nav_size'] . "-" . $_REQUEST['themeOption_main_nav_text'];
    $main_nav_sel = $_REQUEST['main_nav_sel_font'] . "-" . $_REQUEST['main_nav_sel_size'] . "-" . $_REQUEST['themeOption_main_nav_sel_text'];
    $slider_title = $_REQUEST['slider_title_font'] . "-" . $_REQUEST['slider_title_size'] . "-" . $_REQUEST['themeOption_slider_title'];
    $main_page_title = $_REQUEST['main_page_title_font'] . "-" . $_REQUEST['main_page_title_size'] . "-" . $_REQUEST['themeOption_main_page_title'];
    $main_page_body = $_REQUEST['main_page_body_font'] . "-" . $_REQUEST['main_page_body_size'] . "-" . $_REQUEST['themeOption_main_page_body'];
    $cat_title = $_REQUEST['cat_title_font'] . "-" . $_REQUEST['cat_title_size'] . "-" . $_REQUEST['themeOption_cat_title'];
    $cat_header_text = $_REQUEST['category_header_title_font'] . "-" . $_REQUEST['category_header_title_size'] . "-" . $_REQUEST['themeOption_category_header_title'];
    $subcat_title = $_REQUEST['subcat_title_font'] . "-" . $_REQUEST['subcat_title_size'] . "-" . $_REQUEST['themeOption_subcat_title'];
    $listing_title = $_REQUEST['listing_title_font'] . "-" . $_REQUEST['listing_title_size'] . "-" . $_REQUEST['themeOption_listing_title'];
    $event_main_title = $_REQUEST['event_main_title_font'] . "-" . $_REQUEST['event_main_title_size'] . "-" . $_REQUEST['themeOption_event_main_title'];
    $event_date = $_REQUEST['event_date_font'] . "-" . $_REQUEST['event_date_size'] . "-" . $_REQUEST['themeOption_event_date'];
    $event_title = $_REQUEST['event_title_font'] . "-" . $_REQUEST['event_title_size'] . "-" . $_REQUEST['themeOption_event_title'];
    $footer_links = $_REQUEST['footer_links_font'] . "-" . $_REQUEST['footer_links_size'] . "-" . $_REQUEST['themeOption_footer_links'];
    $footer_disc = $_REQUEST['footer_disc_font'] . "-" . $_REQUEST['footer_disc_size'] . "-" . $_REQUEST['themeOption_footer_disc'];
    $listing_title_text = $_REQUEST['listing_title_text_font'] . "-" . $_REQUEST['listing_title_text_size'] . "-" . $_REQUEST['themeOption_listing_title_text'];
    $listing_location_text = $_REQUEST['listing_location_text_font'] . "-" . $_REQUEST['listing_location_text_size'] . "-" . $_REQUEST['themeOption_listing_location_text'];
    $general_body_copy = $_REQUEST['general_body_copy_font'] . "-" . $_REQUEST['general_body_copy_size'] . "-" . $_REQUEST['themeOption_general_body_copy'];
    $listing_sub_nav = $_REQUEST['listing_sub_nav_font'] . "-" . $_REQUEST['listing_sub_nav_size'] . "-" . $_REQUEST['themeOption_listing_sub_nav'];
    $listing_day_text = $_REQUEST['listing_hours_day_font'] . "-" . $_REQUEST['listing_hours_day_size'] . "-" . $_REQUEST['themeOption_listing_hours_day'];
    $listing_hours_text = $_REQUEST['listing_hours_text_font'] . "-" . $_REQUEST['listing_hours_text_size'] . "-" . $_REQUEST['themeOption_listing_hours_text'];
    $amenities_text = $_REQUEST['amenities_text_font'] . "-" . $_REQUEST['amenities_text_size'] . "-" . $_REQUEST['themeOption_amenities_text'];
    $download_text = $_REQUEST['download_text_font'] . "-" . $_REQUEST['download_text_size'] . "-" . $_REQUEST['themeOption_download_text'];
    $get_this_coupon_title_text = $_REQUEST['get_this_coupon_title_font'] . "-" . $_REQUEST['get_this_coupon_title_size'] . "-" . $_REQUEST['themeOption_get_this_coupon_title'];
    $get_this_coupon_desc_text = $_REQUEST['get_this_coupon_description_font'] . "-" . $_REQUEST['get_this_coupon_description_size'] . "-" . $_REQUEST['themeOption_get_this_coupon_description'];
    $detail_page_event_title = $_REQUEST['detail_page_event_main_title_font'] . "-" . $_REQUEST['detail_page_event_main_title_size'] . "-" . $_REQUEST['themeOption_detail_page_event_main_title'];
    $detail_page_event_date = $_REQUEST['detail_page_event_date_font'] . "-" . $_REQUEST['detail_page_event_date_size'] . "-" . $_REQUEST['themeOption_detail_page_event_date'];
    $detail_page_event_data_label = $_REQUEST['detail_page_event_data_label_font'] . "-" . $_REQUEST['detail_page_event_data_label_size'] . "-" . $_REQUEST['themeOption_detail_page_event_data_label'];
    $detail_page_event_data_content = $_REQUEST['detail_page_event_data_content_font'] . "-" . $_REQUEST['detail_page_event_data_content_size'] . "-" . $_REQUEST['themeOption_detail_page_event_data_content'];
    $story_main_title = $_REQUEST['story_main_title_font'] . "-" . $_REQUEST['story_main_title_size'] . "-" . $_REQUEST['themeOption_story_main_title'];
    $story_share_text = $_REQUEST['story_share_text_font'] . "-" . $_REQUEST['story_share_text_size'] . "-" . $_REQUEST['themeOption_story_share_text'];
    $story_content_title = $_REQUEST['story_content_title_font'] . "-" . $_REQUEST['story_content_title_size'] . "-" . $_REQUEST['themeOption_story_content_title'];
    $route_title = $_REQUEST['route_main_title_font'] . "-" . $_REQUEST['route_main_title_size'] . "-" . $_REQUEST['themeOption_route_main_title'];
    $route_data_label = $_REQUEST['route_data_label_font'] . "-" . $_REQUEST['route_data_label_size'] . "-" . $_REQUEST['themeOption_route_data_label'];
    $route_data_content = $_REQUEST['route_data_content_font'] . "-" . $_REQUEST['route_data_content_size'] . "-" . $_REQUEST['themeOption_route_data_content'];
    $text_box_text_inside_search_box = $_REQUEST['text_box_text_inside_search_box_font'] . "-" . $_REQUEST['text_box_text_inside_search_box_size'] . "-" . $_REQUEST['text_box_text_inside_search_box_number'];
    $search_box_button_text = $_REQUEST['search_box_button_text_font'] . "-" . $_REQUEST['search_box_button_text_font_size'] . "-" . $_REQUEST['search_box_button_text_number'];
    /*     * *Concatenating font-size-color** */
    $sql = "tbl_Theme_Options_Mobile SET 
            TO_R_ID = $regionID,
            TO_Main_Navigation = '" . encode_strings($main_nav, $db) . "',
            TO_Main_Navigation_Selected = '" . encode_strings($main_nav_sel, $db) . "',
            TO_Search_Box_Background_Color = '" . encode_strings($_REQUEST['themeOption_search_box_bg'], $db) . "',
            TO_Search_Box_Background_Button_Color = '" . encode_strings($_REQUEST['themeOption_search_box_btn_bg'], $db) . "',
            TO_Text_Box_Border_Inside_Search_Box = '" . encode_strings($_REQUEST['themeOption_text_box_border_inside_search_box'], $db) . "',
            TO_Text_Box_Text_Inside_Box = '" . encode_strings($text_box_text_inside_search_box, $db) . "',
            TO_Search_Box_Button_Text = '" . encode_strings($search_box_button_text, $db) . "',
            TO_Homepage_Description_Space = '" . encode_strings($_REQUEST['description_space'], $db) . "',
            TO_Slider_Title = '" . encode_strings($slider_title, $db) . "',
            TO_Main_Page_Title = '" . encode_strings($main_page_title, $db) . "',
            TO_Main_Page_Body_Copy = '" . encode_strings($main_page_body, $db) . "',
            TO_Thumbnail_Category_Tite = '" . encode_strings($cat_title, $db) . "',
            TO_Thumbnail_Sub_Category_Title = '" . encode_strings($subcat_title, $db) . "',
            TO_Thumbnail_Listing_Title = '" . encode_strings($listing_title, $db) . "',
            TO_Category_Header_Text = '" . encode_strings($cat_header_text, $db) . "',
            TO_Homepage_Event_Main_Title = '" . encode_strings($event_main_title, $db) . "',
            TO_Homepage_Event_Date = '" . encode_strings($event_date, $db) . "',
            TO_Homepage_Event_Title = '" . encode_strings($event_title, $db) . "',
            TO_Footer_Links = '" . encode_strings($footer_links, $db) . "',
            TO_Footer_Disclaimer = '" . encode_strings($footer_disc, $db) . "',
            TO_Footer_Background_Color = '" . encode_strings($_REQUEST['themeOption_footer_background'], $db) . "',
            TO_Footer_Lines_Color = '" . encode_strings($_REQUEST['themeOption_footer_lines'], $db) . "',
            TO_Listing_Title_Text = '" . encode_strings($listing_title_text, $db) . "',
            TO_Listing_Location_Text = '" . encode_strings($listing_location_text, $db) . "',
            TO_Listing_Sub_Nav_Text = '" . encode_strings($listing_sub_nav, $db) . "',
            TO_Listing_Day_Text = '" . encode_strings($listing_day_text, $db) . "',
            TO_Listing_Hours_Text = '" . encode_strings($listing_hours_text, $db) . "',
            TO_Amenities_Text = '" . encode_strings($amenities_text, $db) . "',
            TO_Downloads_Text = '" . encode_strings($download_text, $db) . "',
            TO_Get_This_Coupon_Title_Text = '" . encode_strings($get_this_coupon_title_text, $db) . "',
            TO_Get_This_Coupon_Description_Text = '" . encode_strings($get_this_coupon_desc_text, $db) . "',
            TO_General_Body_Copy = '" . encode_strings($general_body_copy, $db) . "',
            TO_General_Body_Copy_Line_Spacing = '" . encode_strings($_REQUEST['general_body_copy_space'], $db) . "',
            TO_Event_Page_Main_Title = '" . encode_strings($detail_page_event_title, $db) . "',
            TO_Event_Page_Date = '" . encode_strings($detail_page_event_date, $db) . "',
            TO_Event_Page_Data_Label = '" . encode_strings($detail_page_event_data_label, $db) . "',
            TO_Event_Page_Data_Content	 = '" . encode_strings($detail_page_event_data_content, $db) . "',
            TO_Event_Page_Data_Border_Color	 = '" . encode_strings($_REQUEST['themeOption_detail_page_event_border_color'], $db) . "',
            TO_Story_Page_Main_Title = '" . encode_strings($story_main_title, $db) . "',
            TO_Story_Share_Text = '" . encode_strings($story_share_text, $db) . "',
            TO_Story_Page_Content_Title = '" . encode_strings($story_content_title, $db) . "',
            TO_Route_Page_Main_Title = '" . encode_strings($route_title, $db) . "',
            TO_Route_Page_Data_Label = '" . encode_strings($route_data_label, $db) . "',
            TO_Route_Page_Data_Content = '" . encode_strings($route_data_content, $db) . "',
            TO_Desktop_Logo_Alt = '" . encode_strings($_REQUEST['desktop_logo_alt'], $db) . "'";


    /*     * * Images/Icons Uploading Section ** */
    require_once '../include/picUpload.inc.php';
    $home_des_bgs = '';
    $home_des_icon = Upload_Pic_Normal('0', 'home_des_bg', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($home_des_icon) {
        $home_des_bgs = $home_des_icon . ">" . $_REQUEST['home_des'];
        if ($home_des_bg) {
            Delete_Pic(IMG_ICON_ABS . $home_des_bg);
        }
    } else {
        $home_des_bgs = $home_des_bg . ">" . $_REQUEST['home_des'];
    }

    $sql .= ", TO_Homepage_Description_Background = '" . encode_strings($home_des_bgs, $db) . "'";

    $bar_texture_bgs = '';
    $bar_texture_icon = Upload_Pic_Normal('0', 'bar_texture_bg', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($bar_texture_icon) {
        $bar_texture_bgs = $bar_texture_icon . ">" . $_REQUEST['bar_texture'];
        if ($to_bar_texture_bg) {
            Delete_Pic(IMG_ICON_ABS . $to_bar_texture_bg);
        }
    } else {
        $bar_texture_bgs = $to_bar_texture_bg . ">" . $_REQUEST['bar_texture'];
    }
    $sql .= ", TO_Bar_Texture = '" . encode_strings($bar_texture_bgs, $db) . "'";

//category header background
    $category_header_bg = '';
    $category_header_icon = Upload_Pic_Normal('0', 'category_header_bg', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($category_header_icon) {
        $category_header_bg = $category_header_icon . ">" . $_REQUEST['category_header_color'];
        if ($to_category_header_bg) {
            Delete_Pic(IMG_ICON_ABS . $to_category_header_bg);
        }
    } else {
        $category_header_bg = $to_category_header_bg . ">" . $_REQUEST['category_header_color'];
    }
    $sql .= ", TO_Category_Header_BG = '" . encode_strings($category_header_bg, $db) . "'";

    $subcat_texture_bgs = '';
    $subcat_texture_icon = Upload_Pic_Normal('0', 'subcat_texture_bg', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($subcat_texture_icon) {
        $subcat_texture_bgs = $subcat_texture_icon . ">" . $_REQUEST['subcat_texture'];
        if ($to_subcat_texture_bg) {
            Delete_Pic(IMG_ICON_ABS . $to_subcat_texture_bg);
        }
    } else {
        $subcat_texture_bgs = $to_subcat_texture_bg . ">" . $_REQUEST['subcat_texture'];
    }
    $sql .= ", TO_Subcat_Texture = '" . encode_strings($subcat_texture_bgs, $db) . "'";

    $listing_texture_bgs = '';
    $listing_texture_icon = Upload_Pic_Normal('0', 'listing_texture_bg', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($listing_texture_icon) {
        $listing_texture_bgs = $listing_texture_icon . ">" . $_REQUEST['listing_texture'];
        if ($to_listing_texture_bg) {
            Delete_Pic(IMG_ICON_ABS . $to_listing_texture_bg);
        }
    } else {
        $listing_texture_bgs = $to_listing_texture_bg . ">" . $_REQUEST['listing_texture'];
    }
    $sql .= ", TO_Listing_Texture = '" . encode_strings($listing_texture_bgs, $db) . "'";

    $slider_overlay_icon = Upload_Pic_Normal('0', 'slider_overlay', 0, 0, true, IMG_ICON_ABS, 0, true);
    if ($slider_overlay_icon) {
        $sql .= ", TO_Slider_Overlay = '" . encode_strings($slider_overlay_icon, $db) . "'";
        if ($Region_theme['TO_Slider_Overlay']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Slider_Overlay']);
        }
    }
    $desktop_logo = Upload_Pic_Normal('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true);
    if ($desktop_logo) {
        $sql .= ", TO_Desktop_Logo = '" . encode_strings($desktop_logo, $db) . "'";
        if ($Region_theme['TO_Desktop_Logo']) {
            Delete_Pic(IMG_LOC_ABS . $Region_theme['TO_Desktop_Logo']);
        }
    }

    if ($count > 0) {
        $sql = "UPDATE " . $sql . " WHERE TO_R_ID = '" . encode_strings($regionID, $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        // TRACK DATA ENTRY
        $id = $regionID;
        Track_Data_Entry('Websites', $id, 'Theme Options Mobile', '', 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        // TRACK DATA ENTRY
        $id = $regionID;
        Track_Data_Entry('Websites', $id, 'Theme Options Mobile', '', 'Add', 'super admin');
    }

    if ($result) {
        $_SESSION['success'] = 1;
//Image usage from image bank.
        if ($pic_id > 0) {
            imageBankUsage($pic_id, 'IBU_R_ID', $regionID, 'IBU_Region_Theme', 'Desktop Logo');
        }
        if ($pic_id4 > 0) {
            imageBankUsage($pic_id4, 'IBU_R_ID', $regionID, 'IBU_Region_Theme', 'Map Icon');
        }
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: theme-options-mobile.php?rid=" . $regionID);
    exit();
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    require_once '../include/picUpload.inc.php';
    $update = "UPDATE tbl_Theme_Options_Mobile SET";
    if ($_REQUEST['flag'] == 'slider_ol') {
        $update .= " TO_Slider_Overlay = ''";
        if ($Region_theme['TO_Slider_Overlay']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Slider_Overlay']);
        }
    }
    if ($_REQUEST['flag'] == 'home_d_bg') {
        $update .= " TO_Homepage_Description_Background = '>" . encode_strings($home_des_color, $db) . "'"; //
        if ($home_des_bg) {
            Delete_Pic(IMG_ICON_ABS . $home_des_bg);
        }
    }
    if ($_REQUEST['flag'] == 'bar_text_bg') {
        $update .= " TO_Bar_Texture = '>" . encode_strings($to_bar_texture_color, $db) . "'";
        if ($to_bar_texture_bg) {
            Delete_Pic(IMG_ICON_ABS . $to_bar_texture_bg);
        }
    }
    if ($_REQUEST['flag'] == 'cat_header_bg') {
        $update .= " TO_Category_Header_BG = '>" . encode_strings($to_category_header_color, $db) . "'";
        if ($to_category_header_bg) {
            Delete_Pic(IMG_ICON_ABS . $to_category_header_bg);
        }
    }
    if ($_REQUEST['flag'] == 'subcat_text_bg') {
        $update .= " TO_Subcat_Texture = '>" . encode_strings($to_subcat_texture_color, $db) . "'";
        if ($to_subcat_texture_bg) {
            Delete_Pic(IMG_ICON_ABS . $to_subcat_texture_bg);
        }
    }
    if ($_REQUEST['flag'] == 'listing_text_bg') {
        $update .= " TO_Listing_Texture = '>" . encode_strings($to_listing_texture_color, $db) . "'";
        if ($to_listing_texture_bg) {
            Delete_Pic(IMG_ICON_ABS . $to_listing_texture_bg);
        }
    }
    if ($_REQUEST['flag'] == 'desktop_logo') {
        $update .= " TO_Desktop_Logo = ''";
        if ($Region_theme['TO_Desktop_Logo']) {
            Delete_Pic(IMG_ICON_ABS . $Region_theme['TO_Desktop_Logo']);
        }
    }
    $update .= " WHERE TO_R_ID = '" . encode_strings($regionID, $db) . "'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $regionID;
        Track_Data_Entry('Websites', $id, 'Theme Options Mobile', '', 'Delete', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header('location: theme-options-mobile.php?rid=' . $_REQUEST['rid']);
    exit;
}

$getFonts = "SELECT * FROM tbl_Theme_Options_Fonts ORDER BY TOF_Name";

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Theme Options</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <form id="regionForm" action="/admin/theme-options-mobile.php" method="post" enctype="multipart/form-data" name="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <div class="content-header">Manage Theme</div>
            <fieldset>
                <legend>Header</legend>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Main Navigation</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="main_nav_font">
                            <option value="">Select Font...</option>
                            <?php
                            $mainfonts = mysql_query($getFonts);
                            while ($mainNavfont = mysql_fetch_assoc($mainfonts)) {
                                ?>
                                <option style="font-family: <?php echo $mainNavfont['TOF_Name']; ?>" <?php echo (($main_nav_value[0] == $mainNavfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $mainNavfont['TOF_ID'] ?>"><?php echo $mainNavfont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="main_nav_size" size="50" value="<?php echo $main_nav_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_main_nav_text" style="background-color: <?php echo (($main_nav_value[2] != "") ? $main_nav_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_main_nav_text" id="text_colorthemeOption_main_nav_text" value="<?php echo (($main_nav_value[2] != "") ? $main_nav_value[2] : '') ?>">
                        <?php if (isset($main_nav_value[2]) && $main_nav_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete1"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Main_Navigation'; ?>', 'themeOption_main_nav_text', 'hideDelete1', 'text_colorthemeOption_main_nav_text', '<?php echo $main_nav_value[0]; ?>', '<?php echo $main_nav_value[1]; ?>', '<?php echo $main_nav_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Main Navigation - Selected</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="main_nav_sel_font">
                            <option value="">Select Font...</option>
                            <?php
                            $mainfonts = mysql_query($getFonts);
                            while ($mainNavfont = mysql_fetch_assoc($mainfonts)) {
                                ?>
                                <option style="font-family: <?php echo $mainNavfont['TOF_Name']; ?>" <?php echo (($main_nav_sel_value[0] == $mainNavfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $mainNavfont['TOF_ID'] ?>"><?php echo $mainNavfont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="main_nav_sel_size" size="50" value="<?php echo $main_nav_sel_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_main_nav_sel_text" style="background-color: <?php echo (($main_nav_sel_value[2] != "") ? $main_nav_sel_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_main_nav_sel_text" id="text_colorthemeOption_main_nav_sel_text" value="<?php echo (($main_nav_sel_value[2] != "") ? $main_nav_sel_value[2] : '') ?>">
                        <?php if (isset($main_nav_sel_value[2]) && $main_nav_sel_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete2"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Main_Navigation_Selected'; ?>', 'themeOption_main_nav_sel_text', 'hideDelete2', 'text_colorthemeOption_main_nav_sel_text', '<?php echo $main_nav_sel_value[0]; ?>', '<?php echo $main_nav_sel_value[1]; ?>', '<?php echo $main_nav_sel_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Search Box Background</label>
                    <div class="form-data theme-options">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_search_box_bg" style="background-color: <?php echo (($Region_theme['TO_Search_Box_Background_Color'] != "") ? $Region_theme['TO_Search_Box_Background_Color'] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_search_box_bg" id="text_colorthemeOption_search_box_bg" value="<?php echo (($Region_theme['TO_Search_Box_Background_Color'] != "") ? $Region_theme['TO_Search_Box_Background_Color'] : '') ?>">
                        <?php if (isset($Region_theme['TO_Search_Box_Background_Color']) && $Region_theme['TO_Search_Box_Background_Color'] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete611"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Search_Box_Background_Color'; ?>', 'themeOption_search_box_bg', 'hideDelete611', 'text_colorthemeOption_search_box_bg', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Search Box Button Background</label>
                    <div class="form-data theme-options">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_search_box_btn_bg" style="background-color: <?php echo (($Region_theme['TO_Search_Box_Background_Button_Color'] != "") ? $Region_theme['TO_Search_Box_Background_Button_Color'] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_search_box_btn_bg" id="text_colorthemeOption_search_box_btn_bg" value="<?php echo (($Region_theme['TO_Search_Box_Background_Button_Color'] != "") ? $Region_theme['TO_Search_Box_Background_Button_Color'] : '') ?>">
                        <?php if (isset($Region_theme['TO_Search_Box_Background_Button_Color']) && $Region_theme['TO_Search_Box_Background_Button_Color'] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete612"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Search_Box_Background_Button_Color'; ?>', 'themeOption_search_box_btn_bg', 'hideDelete612', 'text_colorthemeOption_search_box_btn_bg', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Text Box Border Inside Search Box</label>
                    <div class="form-data theme-options">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_text_box_border_inside_search_box" style="background-color: <?php echo (($Region_theme['TO_Text_Box_Border_Inside_Search_Box'] != "") ? $Region_theme['TO_Text_Box_Border_Inside_Search_Box'] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_text_box_border_inside_search_box" id="text_colorthemeOption_text_box_border_inside_search_box" value="<?php echo (($Region_theme['TO_Text_Box_Border_Inside_Search_Box'] != "") ? $Region_theme['TO_Text_Box_Border_Inside_Search_Box'] : '') ?>">
                        <?php if (isset($Region_theme['TO_Text_Box_Border_Inside_Search_Box']) && $Region_theme['TO_Text_Box_Border_Inside_Search_Box'] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete613"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Text_Box_Border_Inside_Search_Box'; ?>', 'themeOption_text_box_border_inside_search_box', 'hideDelete613', 'text_colorthemeOption_text_box_border_inside_search_box', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Text Box Text Inside Search Box</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="text_box_text_inside_search_box_font">
                            <option value="">Select Font...</option>
                            <?php
                            $getFonts = "SELECT TOF_Name, TOF_ID  FROM tbl_Theme_Options_Fonts ORDER BY TOF_Name";
                            $res_fonts = mysql_query($getFonts);
                            while ($row_fonts = mysql_fetch_assoc($res_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $row_fonts['TOF_Name']; ?>" <?php echo (($text_box_text_inside_search_box_value[0] == $row_fonts['TOF_ID']) ? "selected" : "") ?> value="<?php echo $row_fonts['TOF_ID'] ?>"><?php echo $row_fonts['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="text_box_text_inside_search_box_size" size="50" value="<?php echo $text_box_text_inside_search_box_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_text_box_text_inside_search_box_number" style="background-color: <?php echo (($text_box_text_inside_search_box_value[2] != "") ? $text_box_text_inside_search_box_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="text_box_text_inside_search_box_number" id="text_colorthemeOption_text_box_text_inside_search_box_number" value="<?php echo (($text_box_text_inside_search_box_value[2] != "") ? $text_box_text_inside_search_box_value[2] : '') ?>">                
                        <?php if (isset($text_box_text_inside_search_box_value[2]) && $text_box_text_inside_search_box_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete811"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Text_Box_Text_Inside_Box'; ?>', 'themeOption_text_box_text_inside_search_box_number', 'hideDelete811', 'text_colorthemeOption_text_box_text_inside_search_box_number', '<?php echo $text_box_text_inside_search_box_value[0]; ?>', '<?php echo $text_box_text_inside_search_box_value[1]; ?>', '<?php echo $text_box_text_inside_search_box_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Search Box Button Text</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="search_box_button_text_font">
                            <option value="">Select Font...</option>
                            <?php
                            $getFonts = "SELECT TOF_Name, TOF_ID  FROM tbl_Theme_Options_Fonts ORDER BY TOF_Name";
                            $res_fonts = mysql_query($getFonts);
                            while ($row_fonts = mysql_fetch_assoc($res_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $row_fonts['TOF_Name']; ?>" <?php echo (($search_box_button_text_value[0] == $row_fonts['TOF_ID']) ? "selected" : "") ?> value="<?php echo $row_fonts['TOF_ID'] ?>"><?php echo $row_fonts['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="search_box_button_text_font_size" size="50" value="<?php echo $search_box_button_text_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_search_box_button_text_number" style="background-color: <?php echo (($search_box_button_text_value[2] != "") ? $search_box_button_text_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="search_box_button_text_number" id="text_colorthemeOption_search_box_button_text_number" value="<?php echo (($search_box_button_text_value[2] != "") ? $search_box_button_text_value[2] : '') ?>">                
                        <?php if (isset($search_box_button_text_value[2]) && $search_box_button_text_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete812"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Search_Box_Button_Text'; ?>', 'themeOption_search_box_button_text_number', 'hideDelete812', 'text_colorthemeOption_search_box_button_text_number', '<?php echo $search_box_button_text_value[0]; ?>', '<?php echo $search_box_button_text_value[1]; ?>', '<?php echo $search_box_button_text_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Main Slider</legend>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Title</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="slider_title_font">
                            <option value="">Select Font...</option>
                            <?php
                            $sliderTitlefonts = mysql_query($getFonts);
                            while ($sliderTitlefont = mysql_fetch_assoc($sliderTitlefonts)) {
                                ?>
                                <option style="font-family: <?php echo $sliderTitlefont['TOF_Name']; ?>" <?php echo (($slider_title_value[0] == $sliderTitlefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $sliderTitlefont['TOF_ID'] ?>"><?php echo $sliderTitlefont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="slider_title_size" size="50" value="<?php echo $slider_title_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_slider_title" style="background-color: <?php echo (($slider_title_value[2] != "") ? $slider_title_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_slider_title" id="text_colorthemeOption_slider_title" value="<?php echo (($slider_title_value[2] != "") ? $slider_title_value[2] : '') ?>">
                        <?php if (isset($slider_title_value[2]) && $slider_title_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete3"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Slider_Title'; ?>', 'themeOption_slider_title', 'hideDelete3', 'text_colorthemeOption_slider_title', '<?php echo $slider_title_value[0]; ?>', '<?php echo $slider_title_value[1]; ?>', '<?php echo $slider_title_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Overlay</label>
                    <div class="form-data theme-options">
                        <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                            <input class="fileInput" type="file" name="slider_overlay[]" onchange="show_file_name(this.files[0].name, 'slider_overlay')"/>
                        </span>
                        <input id="slider_overlay" class="uploadFileName" style="display:none;" disabled>
                        <?PHP if ($Region_theme['TO_Slider_Overlay']) { ?>
                            <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $Region_theme['TO_Slider_Overlay']; ?>" target="_blank">View</a>
                            <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options-mobile.php?rid=<?php echo $regionID ?>&flag=slider_ol&op=del">Delete</a>
                        <?php } ?>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Main Description And Title</legend>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Title Text</label>
                    <div class="form-data theme-options"> 
                        <select class="theme-options-font" name="main_page_title_font">
                            <option value="">Select Font...</option>
                            <?php
                            $main_page_title_fonts = mysql_query($getFonts);
                            while ($mainPageTitlefont = mysql_fetch_assoc($main_page_title_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $mainPageTitlefont['TOF_Name']; ?>" <?php echo (($main_page_title_value[0] == $mainPageTitlefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $mainPageTitlefont['TOF_ID'] ?>"><?php echo $mainPageTitlefont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="main_page_title_size" size="50" value="<?php echo $main_page_title_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_main_page_title" style="background-color: <?php echo (($main_page_title_value[2] != "") ? $main_page_title_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_main_page_title" id="text_colorthemeOption_main_page_title" value="<?php echo (($main_page_title_value[2] != "") ? $main_page_title_value[2] : '') ?>">
                        <?php if (isset($main_page_title_value[2]) && $main_page_title_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete4"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Main_Page_Title'; ?>', 'themeOption_main_page_title', 'hideDelete4', 'text_colorthemeOption_main_page_title', '<?php echo $main_page_title_value[0]; ?>', '<?php echo $main_page_title_value[1]; ?>', '<?php echo $main_page_title_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Description Text</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="main_page_body_font">
                            <option value="">Select Font...</option>
                            <?php
                            $main_page_body_fonts = mysql_query($getFonts);
                            while ($mainPageBodyfont = mysql_fetch_assoc($main_page_body_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $mainPageBodyfont['TOF_Name']; ?>" <?php echo (($main_page_body_value[0] == $mainPageBodyfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $mainPageBodyfont['TOF_ID'] ?>"><?php echo $mainPageBodyfont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="main_page_body_size" size="50" value="<?php echo $main_page_body_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_main_page_body" style="background-color: <?php echo (($main_page_body_value[2] != "") ? $main_page_body_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_main_page_body" id="text_colorthemeOption_main_page_body" value="<?php echo (($main_page_body_value[2] != "") ? $main_page_body_value[2] : '') ?>">
                        <?php if (isset($main_page_body_value[2]) && $main_page_body_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete5"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Main_Page_Body_Copy'; ?>', 'themeOption_main_page_body', 'hideDelete5', 'text_colorthemeOption_main_page_body', '<?php echo $main_page_body_value[0]; ?>', '<?php echo $main_page_body_value[1]; ?>', '<?php echo $main_page_body_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Description Background</label>
                    <div class="form-data theme-options">
                        <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                            <input class="fileInput" type="file" name="home_des_bg[]" onchange="show_file_name(this.files[0].name, 'description_background')"/>
                        </span>
                        <input id="description_background" class="uploadFileName" style="display:none;" disabled>
                        <?PHP if ($home_des_bg) { ?>
                            <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $home_des_bg; ?>" target="_blank">View</a>
                            <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options-mobile.php?rid=<?php echo $regionID ?>&flag=home_d_bg&op=del">Delete</a>
                        <?php } ?>    
                        <div class="color-box-inner theme-options-color theme_color_float"><div class="color-box" id="themeOption_home_des" style="background-color: <?php echo (($home_des_color != "") ? $home_des_color : '') ?>"></div></div>
                        <input type="hidden" name="home_des" id="text_colorthemeOption_home_des" value="<?php echo (($home_des_color != "") ? $home_des_color : '') ?>">       
                        <?php if (isset($home_des_color) && $home_des_color !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete6"  onclick="removeColorImage(<?php echo $regionID; ?>, '<?php echo 'TO_Homepage_Description_Background'; ?>', 'themeOption_home_des', 'hideDelete6', 'text_colorthemeOption_home_des', '<?php echo $home_des_bg; ?>', '<?php echo $home_des_color; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Description Spacing</label>
                    <div class="form-data theme-options">    
                        <input class="theme-options-size" type="text" name="description_space" size="50" value="<?php echo $Region_theme['TO_Homepage_Description_Space']; ?>" placeholder="Enter line space">
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Secondary Navigations</legend>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Category Text</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="cat_title_font">
                            <option value="">Select Font...</option>
                            <?php
                            $cat_title_fonts = mysql_query($getFonts);
                            while ($catTitlefont = mysql_fetch_assoc($cat_title_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $catTitlefont['TOF_Name']; ?>" <?php echo (($cat_title_value[0] == $catTitlefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $catTitlefont['TOF_ID'] ?>"><?php echo $catTitlefont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="cat_title_size" size="50" value="<?php echo $cat_title_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_cat_title" style="background-color: <?php echo (($cat_title_value[2] != "") ? $cat_title_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_cat_title" id="text_colorthemeOption_cat_title" value="<?php echo (($cat_title_value[2] != "") ? $cat_title_value[2] : '') ?>">
                        <?php if (isset($cat_title_value[2]) && $cat_title_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete7"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Thumbnail_Category_Tite'; ?>', 'themeOption_cat_title', 'hideDelete7', 'text_colorthemeOption_cat_title', '<?php echo $cat_title_value[0]; ?>', '<?php echo $cat_title_value[1]; ?>', '<?php echo $cat_title_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Category Background</label>
                    <div class="form-data theme-options"> 
                        <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                            <input class="fileInput" type="file" name="bar_texture_bg[]" onchange="show_file_name(this.files[0].name, 'category_background')"/>
                        </span>
                        <input id="category_background" class="uploadFileName" style="display:none;" disabled>
                        <?PHP if ($to_bar_texture_bg) { ?>
                            <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $to_bar_texture_bg; ?>" target="_blank">View</a>
                            <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options-mobile.php?rid=<?php echo $regionID ?>&flag=bar_text_bg&op=del">Delete</a>
                        <?php } ?>    
                        <div class="color-box-inner theme-options-color theme_color_float"><div class="color-box" id="themeOption_bar_texture" style="background-color: <?php echo (($to_bar_texture_color != "") ? $to_bar_texture_color : '') ?>"></div></div>
                        <input type="hidden" name="bar_texture" id="text_colorthemeOption_bar_texture" value="<?php echo (($to_bar_texture_color != "") ? $to_bar_texture_color : '') ?>">       
                        <?php if (isset($to_bar_texture_color) && $to_bar_texture_color !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete8"  onclick="removeColorImage(<?php echo $regionID; ?>, '<?php echo 'TO_Bar_Texture'; ?>', 'themeOption_bar_texture', 'hideDelete8', 'text_colorthemeOption_bar_texture', '<?php echo $to_bar_texture_bg; ?>', '<?php echo $to_bar_texture_color; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Sub-Category Text</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="subcat_title_font">
                            <option value="">Select Font...</option>
                            <?php
                            $subcat_title_fonts = mysql_query($getFonts);
                            while ($subcatTitlefont = mysql_fetch_assoc($subcat_title_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $subcatTitlefont['TOF_Name']; ?>" <?php echo (($subcat_title_value[0] == $subcatTitlefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $subcatTitlefont['TOF_ID'] ?>"><?php echo $subcatTitlefont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="subcat_title_size" size="50" value="<?php echo $subcat_title_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_subcat_title" style="background-color: <?php echo (($subcat_title_value[2] != "") ? $subcat_title_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_subcat_title" id="text_colorthemeOption_subcat_title" value="<?php echo (($subcat_title_value[2] != "") ? $subcat_title_value[2] : '') ?>">
                        <?php if (isset($subcat_title_value[2]) && $subcat_title_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete9"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Thumbnail_Sub_Category_Title'; ?>', 'themeOption_subcat_title', 'hideDelete9', 'text_colorthemeOption_subcat_title', '<?php echo $subcat_title_value[0]; ?>', '<?php echo $subcat_title_value[1]; ?>', '<?php echo $subcat_title_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Sub-Category Background</label>
                    <div class="form-data theme-options"> 
                        <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                            <input class="fileInput" type="file" name="subcat_texture_bg[]" onchange="show_file_name(this.files[0].name, 'subcategory_background')"/>
                        </span>
                        <input id="subcategory_background" class="uploadFileName" style="display:none;" disabled>
                        <?PHP if ($to_subcat_texture_bg) { ?>
                            <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $to_subcat_texture_bg; ?>" target="_blank">View</a>
                            <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options-mobile.php?rid=<?php echo $regionID ?>&flag=subcat_text_bg&op=del">Delete</a>
                        <?php } ?>    
                        <div class="color-box-inner theme-options-color theme_color_float"><div class="color-box" id="themeOption_subcat_texture" style="background-color: <?php echo (($to_subcat_texture_color != "") ? $to_subcat_texture_color : '') ?>"></div></div>
                        <input type="hidden" name="subcat_texture" id="text_colorthemeOption_subcat_texture" value="<?php echo (($to_subcat_texture_color != "") ? $to_subcat_texture_color : '') ?>">
                        <?php if (isset($to_subcat_texture_color) && $to_subcat_texture_color !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete10"  onclick="removeColorImage(<?php echo $regionID; ?>, '<?php echo 'TO_Subcat_Texture'; ?>', 'themeOption_subcat_texture', 'hideDelete10', 'text_colorthemeOption_subcat_texture', '<?php echo $to_subcat_texture_bg; ?>', '<?php echo $to_subcat_texture_color; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Listing Text</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="listing_title_font">
                            <option value="">Select Font...</option>
                            <?php
                            $listing_title_fonts = mysql_query($getFonts);
                            while ($listingTitlefont = mysql_fetch_assoc($listing_title_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $listingTitlefont['TOF_Name']; ?>" <?php echo (($listing_title_value[0] == $listingTitlefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $listingTitlefont['TOF_ID'] ?>"><?php echo $listingTitlefont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="listing_title_size" size="50" value="<?php echo $listing_title_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_listing_title" style="background-color: <?php echo (($listing_title_value[2] != "") ? $listing_title_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_listing_title" id="text_colorthemeOption_listing_title" value="<?php echo (($listing_title_value[2] != "") ? $listing_title_value[2] : '') ?>">
                        <?php if (isset($listing_title_value[2]) && $listing_title_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete11"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Thumbnail_Listing_Title'; ?>', 'themeOption_listing_title', 'hideDelete11', 'text_colorthemeOption_listing_title', '<?php echo $listing_title_value[0]; ?>', '<?php echo $listing_title_value[1]; ?>', '<?php echo $listing_title_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Listing Background</label>
                    <div class="form-data theme-options"> 
                        <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                            <input class="fileInput" type="file" name="listing_texture_bg[]" onchange="show_file_name(this.files[0].name, 'listing_background')"/>
                        </span>
                        <input id="listing_background" class="uploadFileName" style="display:none;" disabled>
                        <?PHP if ($to_listing_texture_bg) { ?>
                            <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $to_listing_texture_bg; ?>" target="_blank">View</a>
                            <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options-mobile.php?rid=<?php echo $regionID ?>&flag=listing_text_bg&op=del">Delete</a>
                        <?php } ?>    
                        <div class="color-box-inner theme-options-color theme_color_float"><div class="color-box" id="themeOption_listing_texture" style="background-color: <?php echo (($to_listing_texture_color != "") ? $to_listing_texture_color : '') ?>"></div></div>
                        <input type="hidden" name="listing_texture" id="text_colorthemeOption_listing_texture" value="<?php echo (($to_listing_texture_color != "") ? $to_listing_texture_color : '') ?>">
                        <?php if (isset($to_listing_texture_color) && $to_listing_texture_color !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete12"  onclick="removeColorImage(<?php echo $regionID; ?>, '<?php echo 'TO_Listing_Texture'; ?>', 'themeOption_listing_texture', 'hideDelete12', 'text_colorthemeOption_listing_texture', '<?php echo $to_listing_texture_bg; ?>', '<?php echo $to_listing_texture_color; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Event Title</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="event_main_title_font">
                            <option value="">Select Font...</option>
                            <?php
                            $event_main_title_fonts = mysql_query($getFonts);
                            while ($eventMainTitlefont = mysql_fetch_assoc($event_main_title_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $eventMainTitlefont['TOF_Name']; ?>" <?php echo (($event_main_title_value[0] == $eventMainTitlefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $eventMainTitlefont['TOF_ID'] ?>"><?php echo $eventMainTitlefont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="event_main_title_size" size="50" value="<?php echo $event_main_title_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_event_main_title" style="background-color: <?php echo (($event_main_title_value[2] != "") ? $event_main_title_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_event_main_title" id="text_colorthemeOption_event_main_title" value="<?php echo (($event_main_title_value[2] != "") ? $event_main_title_value[2] : '') ?>">
                        <?php if (isset($event_main_title_value[2]) && $event_main_title_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete13"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Homepage_Event_Main_Title'; ?>', 'themeOption_event_main_title', 'hideDelete13', 'text_colorthemeOption_event_main_title', '<?php echo $event_main_title_value[0]; ?>', '<?php echo $event_main_title_value[1]; ?>', '<?php echo $event_main_title_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Event Date</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="event_date_font">
                            <option value="">Select Font...</option>
                            <?php
                            $event_date_fonts = mysql_query($getFonts);
                            while ($eventDatefont = mysql_fetch_assoc($event_date_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $eventDatefont['TOF_Name']; ?>" <?php echo (($event_date_value[0] == $eventDatefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $eventDatefont['TOF_ID'] ?>"><?php echo $eventDatefont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="event_date_size" size="50" value="<?php echo $event_date_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_event_date" style="background-color: <?php echo (($event_date_value[2] != "") ? $event_date_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_event_date" id="text_colorthemeOption_event_date" value="<?php echo (($event_date_value[2] != "") ? $event_date_value[2] : '') ?>">
                        <?php if (isset($event_date_value[2]) && $event_date_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete14"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Homepage_Event_Date'; ?>', 'themeOption_event_date', 'hideDelete14', 'text_colorthemeOption_event_date', '<?php echo $event_date_value[0]; ?>', '<?php echo $event_date_value[1]; ?>', '<?php echo $event_date_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Category and Sub-Category Main Page Title</legend>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Text</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="category_header_title_font">
                            <option value="">Select Font...</option>
                            <?php
                            $subcat_title_fonts = mysql_query($getFonts);
                            while ($subcatTitlefont = mysql_fetch_assoc($subcat_title_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $subcatTitlefont['TOF_Name']; ?>" <?php echo (($to_category_header_text[0] == $subcatTitlefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $subcatTitlefont['TOF_ID'] ?>"><?php echo $subcatTitlefont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="category_header_title_size" size="50" value="<?php echo $to_category_header_text[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_category_header_title" style="background-color: <?php echo (($to_category_header_text[2] != "") ? $to_category_header_text[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_category_header_title" id="text_colorthemeOption_category_header_title" value="<?php echo (($to_category_header_text[2] != "") ? $to_category_header_text[2] : '') ?>">
                        <?php if (isset($to_category_header_text[2]) && $to_category_header_text[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete15"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Category_Header_Text'; ?>', 'themeOption_category_header_title', 'hideDelete15', 'text_colorthemeOption_category_header_title', '<?php echo $to_category_header_text[0]; ?>', '<?php echo $to_category_header_text[1]; ?>', '<?php echo $to_category_header_text[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Background</label>
                    <div class="form-data theme-options">
                        <span class="inputWrapper theme_browse_float">&nbsp;&nbsp;Browse 
                            <input class="fileInput" type="file" name="category_header_bg[]" onchange="show_file_name(this.files[0].name, 'category_subcategory_title_background')"/>
                        </span>
                        <input id="category_subcategory_title_background" class="uploadFileName" style="display:none;" disabled>
                        <?PHP if ($to_category_header_bg) { ?>
                            <a class="margin-left-resize theme_browse_float_view" href="<?php echo IMG_ICON_REL . $to_category_header_bg; ?>" target="_blank">View</a>';
                            <a class="margin-left-resize theme_browse_float_view" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options-mobile.php?rid=<?php echo $regionID ?>&flag=cat_header_bg&op=del">Delete</a>
                        <?php } ?>    
                        <div class="color-box-inner theme-options-color theme_color_float"><div class="color-box" id="themeOption_category_header_bg" style="background-color: <?php echo (($to_category_header_color != "") ? $to_category_header_color : '') ?>"></div></div>
                        <input type="hidden" name="category_header_color" id="text_colorthemeOption_category_header_bg" value="<?php echo (($to_category_header_color != "") ? $to_category_header_color : '') ?>">
                        <?php if (isset($to_category_header_color) && $to_category_header_color !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete16"  onclick="removeColorImage(<?php echo $regionID; ?>, '<?php echo 'TO_Category_Header_BG'; ?>', 'themeOption_category_header_bg', 'hideDelete16', 'text_colorthemeOption_category_header_bg', '<?php echo $to_category_header_bg; ?>', '<?php echo $to_category_header_color; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Footer</legend>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Text Links</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="footer_links_font">
                            <option value="">Select Font...</option>
                            <?php
                            $footer_links_fonts = mysql_query($getFonts);
                            while ($footerLinksFont = mysql_fetch_assoc($footer_links_fonts)) {
                                ?>
                                <option style="font-family: <?php echo $footerLinksFont['TOF_Name']; ?>" <?php echo (($footer_links_value[0] == $footerLinksFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $footerLinksFont['TOF_ID'] ?>"><?php echo $footerLinksFont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="footer_links_size" size="50" value="<?php echo $footer_links_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_footer_links" style="background-color: <?php echo (($footer_links_value[2] != "") ? $footer_links_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_footer_links" id="text_colorthemeOption_footer_links" value="<?php echo (($footer_links_value[2] != "") ? $footer_links_value[2] : '') ?>">
                        <?php if (isset($footer_links_value[2]) && $footer_links_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete17"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Footer_Links'; ?>', 'themeOption_footer_links', 'hideDelete17', 'text_colorthemeOption_footer_links', '<?php echo $footer_links_value[0]; ?>', '<?php echo $footer_links_value[1]; ?>', '<?php echo $footer_links_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Disclaimer Text</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="footer_disc_font">
                            <option value="">Select Font...</option>
                            <?php
                            $footer_disc_font = mysql_query($getFonts);
                            while ($footerDiscFont = mysql_fetch_assoc($footer_disc_font)) {
                                ?>
                                <option style="font-family: <?php echo $footerDiscFont['TOF_Name']; ?>" <?php echo (($footer_disc_value[0] == $footerDiscFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $footerDiscFont['TOF_ID'] ?>"><?php echo $footerDiscFont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="footer_disc_size" size="50" value="<?php echo $footer_disc_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_footer_disc" style="background-color: <?php echo (($footer_disc_value[2] != "") ? $footer_disc_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_footer_disc" id="text_colorthemeOption_footer_disc" value="<?php echo (($footer_disc_value[2] != "") ? $footer_disc_value[2] : '') ?>">
                        <?php if (isset($footer_disc_value[2]) && $footer_disc_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete18"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Footer_Disclaimer'; ?>', 'themeOption_footer_disc', 'hideDelete18', 'text_colorthemeOption_footer_disc', '<?php echo $footer_disc_value[0]; ?>', '<?php echo $footer_disc_value[1]; ?>', '<?php echo $footer_disc_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Background Color</label>
                    <div class="form-data theme-options">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_footer_background" style="background-color: <?php echo (($Region_theme['TO_Footer_Background_Color'] != "") ? $Region_theme['TO_Footer_Background_Color'] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_footer_background" id="text_colorthemeOption_footer_background" value="<?php echo (($Region_theme['TO_Footer_Background_Color'] != "") ? $Region_theme['TO_Footer_Background_Color'] : '') ?>">
                        <?php if (isset($Region_theme['TO_Footer_Background_Color']) && $Region_theme['TO_Footer_Background_Color'] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete19"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Footer_Background_Color'; ?>', 'themeOption_footer_background', 'hideDelete19', 'text_colorthemeOption_footer_background', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Border Color</label>
                    <div class="form-data theme-options">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_footer_lines" style="background-color: <?php echo (($Region_theme['TO_Footer_Lines_Color'] != "") ? $Region_theme['TO_Footer_Lines_Color'] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_footer_lines" id="text_colorthemeOption_footer_lines" value="<?php echo (($Region_theme['TO_Footer_Lines_Color'] != "") ? $Region_theme['TO_Footer_Lines_Color'] : '') ?>">
                        <?php if (isset($Region_theme['TO_Footer_Lines_Color']) && $Region_theme['TO_Footer_Lines_Color'] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete20"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Footer_Lines_Color'; ?>', 'themeOption_footer_lines', 'hideDelete20', 'text_colorthemeOption_footer_lines', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Listing Page</legend>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Title</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="listing_title_text_font">
                            <option value="">Select Font...</option>
                            <?php
                            $listing_title_text_font = mysql_query($getFonts);
                            while ($listingTitleTextfont = mysql_fetch_assoc($listing_title_text_font)) {
                                ?>
                                <option style="font-family: <?php echo $listingTitleTextfont['TOF_Name']; ?>" <?php echo (($listing_title_text_value[0] == $listingTitleTextfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $listingTitleTextfont['TOF_ID'] ?>"><?php echo $listingTitleTextfont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="listing_title_text_size" size="50" value="<?php echo $listing_title_text_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_listing_title_text" style="background-color: <?php echo (($listing_title_text_value[2] != "") ? $listing_title_text_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_listing_title_text" id="text_colorthemeOption_listing_title_text" value="<?php echo (($listing_title_text_value[2] != "") ? $listing_title_text_value[2] : '') ?>">
                        <?php if (isset($listing_title_text_value[2]) && $listing_title_text_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete21"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Listing_Title_Text'; ?>', 'themeOption_listing_title_text', 'hideDelete21', 'text_colorthemeOption_listing_title_text', '<?php echo $listing_title_text_value[0]; ?>', '<?php echo $listing_title_text_value[1]; ?>', '<?php echo $listing_title_text_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Address</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="listing_location_text_font">
                            <option value="">Select Font...</option>
                            <?php
                            $listing_location_text_font = mysql_query($getFonts);
                            while ($listinglocationTextfont = mysql_fetch_assoc($listing_location_text_font)) {
                                ?>
                                <option style="font-family: <?php echo $listinglocationTextfont['TOF_Name']; ?>" <?php echo (($listing_title_location_value[0] == $listinglocationTextfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $listinglocationTextfont['TOF_ID'] ?>"><?php echo $listinglocationTextfont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="listing_location_text_size" size="50" value="<?php echo $listing_title_location_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_listing_location_text" style="background-color: <?php echo (($listing_title_location_value[2] != "") ? $listing_title_location_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_listing_location_text" id="text_colorthemeOption_listing_location_text" value="<?php echo (($listing_title_location_value[2] != "") ? $listing_title_location_value[2] : '') ?>">
                        <?php if (isset($listing_title_location_value[2]) && $listing_title_location_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete22"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Listing_Location_Text'; ?>', 'themeOption_listing_location_text', 'hideDelete22', 'text_colorthemeOption_listing_location_text', '<?php echo $listing_title_location_value[0]; ?>', '<?php echo $listing_title_location_value[1]; ?>', '<?php echo $listing_title_location_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Sub Navigation Text</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="listing_sub_nav_font">
                            <option value="">Select Font...</option>
                            <?php
                            $listing_sub_nav_font = mysql_query($getFonts);
                            while ($listingSubNavFont = mysql_fetch_assoc($listing_sub_nav_font)) {
                                ?>
                                <option style="font-family: <?php echo $listingSubNavFont['TOF_Name']; ?>" <?php echo (($listing_sub_nav_value[0] == $listingSubNavFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $listingSubNavFont['TOF_ID'] ?>"><?php echo $listingSubNavFont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="listing_sub_nav_size" size="50" value="<?php echo $listing_sub_nav_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_listing_sub_nav" style="background-color: <?php echo (($listing_sub_nav_value[2] != "") ? $listing_sub_nav_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_listing_sub_nav" id="text_colorthemeOption_listing_sub_nav" value="<?php echo (($listing_sub_nav_value[2] != "") ? $listing_sub_nav_value[2] : '') ?>">
                        <?php if (isset($listing_sub_nav_value[2]) && $listing_sub_nav_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete23"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Listing_Sub_Nav_Text'; ?>', 'themeOption_listing_sub_nav', 'hideDelete23', 'text_colorthemeOption_listing_sub_nav', '<?php echo $listing_sub_nav_value[0]; ?>', '<?php echo $listing_sub_nav_value[1]; ?>', '<?php echo $listing_sub_nav_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Hours Day</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="listing_hours_day_font">
                            <option value="">Select Font...</option>
                            <?php
                            $listing_hours_day_font = mysql_query($getFonts);
                            while ($listingHoursdayFont = mysql_fetch_assoc($listing_hours_day_font)) {
                                ?>
                                <option style="font-family: <?php echo $listingHoursdayFont['TOF_Name']; ?>" <?php echo (($listing_day_text_value[0] == $listingHoursdayFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $listingHoursdayFont['TOF_ID'] ?>"><?php echo $listingHoursdayFont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="listing_hours_day_size" size="50" value="<?php echo $listing_day_text_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_listing_hours_day" style="background-color: <?php echo (($listing_day_text_value[2] != "") ? $listing_day_text_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_listing_hours_day" id="text_colorthemeOption_listing_hours_day" value="<?php echo (($listing_day_text_value[2] != "") ? $listing_day_text_value[2] : '') ?>">
                        <?php if (isset($listing_day_text_value[2]) && $listing_day_text_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete24"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Listing_Day_Text'; ?>', 'themeOption_listing_hours_day', 'hideDelete24', 'text_colorthemeOption_listing_hours_day', '<?php echo $listing_day_text_value[0]; ?>', '<?php echo $listing_day_text_value[1]; ?>', '<?php echo $listing_day_text_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Hours Time</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="listing_hours_text_font">
                            <option value="">Select Font...</option>
                            <?php
                            $listing_hours_text_font = mysql_query($getFonts);
                            while ($listingHoursTextFont = mysql_fetch_assoc($listing_hours_text_font)) {
                                ?>
                                <option style="font-family: <?php echo $listingHoursTextFont['TOF_Name']; ?>" <?php echo (($listing_hours_text_value[0] == $listingHoursTextFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $listingHoursTextFont['TOF_ID'] ?>"><?php echo $listingHoursTextFont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="listing_hours_text_size" size="50" value="<?php echo $listing_hours_text_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_listing_hours_text" style="background-color: <?php echo (($listing_hours_text_value[2] != "") ? $listing_hours_text_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_listing_hours_text" id="text_colorthemeOption_listing_hours_text" value="<?php echo (($listing_hours_text_value[2] != "") ? $listing_hours_text_value[2] : '') ?>">
                        <?php if (isset($listing_hours_text_value[2]) && $listing_hours_text_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete25"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Listing_Hours_Text'; ?>', 'themeOption_listing_hours_text', 'hideDelete25', 'text_colorthemeOption_listing_hours_text', '<?php echo $listing_hours_text_value[0]; ?>', '<?php echo $listing_hours_text_value[1]; ?>', '<?php echo $listing_hours_text_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Amenities Text</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="amenities_text_font">
                            <option value="">Select Font...</option>
                            <?php
                            $amenities_text_font = mysql_query($getFonts);
                            while ($amenitiesTextFont = mysql_fetch_assoc($amenities_text_font)) {
                                ?>
                                <option style="font-family: <?php echo $amenitiesTextFont['TOF_Name']; ?>" <?php echo (($amenities_text_value[0] == $amenitiesTextFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $amenitiesTextFont['TOF_ID'] ?>"><?php echo $amenitiesTextFont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="amenities_text_size" size="50" value="<?php echo $amenities_text_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_amenities_text" style="background-color: <?php echo (($amenities_text_value[2] != "") ? $amenities_text_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_amenities_text" id="text_colorthemeOption_amenities_text" value="<?php echo (($amenities_text_value[2] != "") ? $amenities_text_value[2] : '') ?>">
                        <?php if (isset($amenities_text_value[2]) && $amenities_text_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete26"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Amenities_Text'; ?>', 'themeOption_amenities_text', 'hideDelete26', 'text_colorthemeOption_amenities_text', '<?php echo $amenities_text_value[0]; ?>', '<?php echo $amenities_text_value[1]; ?>', '<?php echo $amenities_text_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Downloads Text</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="download_text_font">
                            <option value="">Select Font...</option>
                            <?php
                            $download_text_font = mysql_query($getFonts);
                            while ($downloadTextFont = mysql_fetch_assoc($download_text_font)) {
                                ?>
                                <option style="font-family: <?php echo $downloadTextFont['TOF_Name']; ?>" <?php echo (($download_text_value[0] == $downloadTextFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $downloadTextFont['TOF_ID'] ?>"><?php echo $downloadTextFont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="download_text_size" size="50" value="<?php echo $download_text_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_download_text" style="background-color: <?php echo (($download_text_value[2] != "") ? $download_text_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_download_text" id="text_colorthemeOption_download_text" value="<?php echo (($download_text_value[2] != "") ? $download_text_value[2] : '') ?>">
                        <?php if (isset($download_text_value[2]) && $download_text_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete27"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Downloads_Text'; ?>', 'themeOption_download_text', 'hideDelete27', 'text_colorthemeOption_download_text', '<?php echo $download_text_value[0]; ?>', '<?php echo $download_text_value[1]; ?>', '<?php echo $download_text_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <?php if (isset($activeRegion['R_Type']) && $activeRegion['R_Type'] == 6) { ?>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Get This Coupon Title Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="get_this_coupon_title_font">
                                <option value="">Select Font...</option>
                                <?php
                                $get_this_coupon_title_font = mysql_query($getFonts);
                                while ($getThisCouponTitleFont = mysql_fetch_assoc($get_this_coupon_title_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $getThisCouponTitleFont['TOF_Name']; ?>" <?php echo (($get_this_coupon_title_text_value[0] == $getThisCouponTitleFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $getThisCouponTitleFont['TOF_ID'] ?>"><?php echo $getThisCouponTitleFont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="get_this_coupon_title_size" size="50" value="<?php echo $get_this_coupon_title_text_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_get_this_coupon_title" style="background-color: <?php echo (($get_this_coupon_title_text_value[2] != "") ? $get_this_coupon_title_text_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_get_this_coupon_title" id="text_colorthemeOption_get_this_coupon_title" value="<?php echo (($get_this_coupon_title_text_value[2] != "") ? $get_this_coupon_title_text_value[2] : '') ?>">
                            <?php if (isset($get_this_coupon_title_text_value[2]) && $get_this_coupon_title_text_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete28"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Get_This_Coupon_Title_Text'; ?>', 'themeOption_get_this_coupon_title', 'hideDelete28', '<?php echo $get_this_coupon_title_text_value[0]; ?>', '<?php echo $get_this_coupon_title_text_value[1]; ?>', '<?php echo $get_this_coupon_title_text_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div form-inside-div-width-admin"> 
                        <label>Get This Coupon Description Text</label>
                        <div class="form-data theme-options">
                            <select class="theme-options-font" name="get_this_coupon_description_font">
                                <option value="">Select Font...</option>
                                <?php
                                $get_this_coupon_desc_font = mysql_query($getFonts);
                                while ($getThisCouponDescFont = mysql_fetch_assoc($get_this_coupon_desc_font)) {
                                    ?>
                                    <option style="font-family: <?php echo $getThisCouponDescFont['TOF_Name']; ?>" <?php echo (($get_this_coupon_title_description_value[0] == $getThisCouponDescFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $getThisCouponDescFont['TOF_ID'] ?>"><?php echo $getThisCouponDescFont['TOF_Name'] ?></option>
                                <?php } ?>
                            </select>
                            <input class="theme-options-size" type="number" name="get_this_coupon_description_size" size="50" value="<?php echo $get_this_coupon_title_description_value[1]; ?>" placeholder="Font size">
                            <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_get_this_coupon_description" style="background-color: <?php echo (($get_this_coupon_title_description_value[2] != "") ? $get_this_coupon_title_description_value[2] : '') ?>"></div></div>
                            <input type="hidden" name="themeOption_get_this_coupon_description" id="text_colorthemeOption_get_this_coupon_description" value="<?php echo (($get_this_coupon_title_description_value[2] != "") ? $get_this_coupon_title_description_value[2] : '') ?>">
                            <?php if (isset($get_this_coupon_title_description_value[2]) && $get_this_coupon_title_description_value[2] !== '') { ?>
                                <a class="margin-left-resize theme_browse_float_view hideVal hideDelete29"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Get_This_Coupon_Description_Text'; ?>', 'themeOption_get_this_coupon_description', 'hideDelete29', '<?php echo $get_this_coupon_title_description_value[0]; ?>', '<?php echo $get_this_coupon_title_description_value[1]; ?>', '<?php echo $get_this_coupon_title_description_value[2]; ?>')">Remove</a>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </fieldset>
            <fieldset>
                <legend>Events Page</legend>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Title</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="detail_page_event_main_title_font">
                            <option value="">Select Font...</option>
                            <?php
                            $detail_page_event_main_title_font = mysql_query($getFonts);
                            while ($detailPageEventMainTitlefont = mysql_fetch_assoc($detail_page_event_main_title_font)) {
                                ?>
                                <option style="font-family: <?php echo $detailPageEventMainTitlefont['TOF_Name']; ?>" <?php echo (($detail_page_event_main_title_value[0] == $detailPageEventMainTitlefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $detailPageEventMainTitlefont['TOF_ID'] ?>"><?php echo $detailPageEventMainTitlefont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="detail_page_event_main_title_size" size="50" value="<?php echo $detail_page_event_main_title_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_detail_page_event_main_title" style="background-color: <?php echo (($detail_page_event_main_title_value[2] != "") ? $detail_page_event_main_title_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_detail_page_event_main_title" id="text_colorthemeOption_detail_page_event_main_title" value="<?php echo (($detail_page_event_main_title_value[2] != "") ? $detail_page_event_main_title_value[2] : '') ?>">
                        <?php if (isset($detail_page_event_main_title_value[2]) && $detail_page_event_main_title_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete30"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Event_Page_Main_Title'; ?>', 'themeOption_detail_page_event_main_title', 'hideDelete30', 'text_colorthemeOption_detail_page_event_main_title', '<?php echo $detail_page_event_main_title_value[0]; ?>', '<?php echo $detail_page_event_main_title_value[1]; ?>', '<?php echo $detail_page_event_main_title_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Date Text</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="detail_page_event_date_font">
                            <option value="">Select Font...</option>
                            <?php
                            $detail_page_event_date_font = mysql_query($getFonts);
                            while ($detailPageEventDatefont = mysql_fetch_assoc($detail_page_event_date_font)) {
                                ?>
                                <option style="font-family: <?php echo $detailPageEventDatefont['TOF_Name']; ?>" <?php echo (($detail_page_event_date_value[0] == $detailPageEventDatefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $detailPageEventDatefont['TOF_ID'] ?>"><?php echo $detailPageEventDatefont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="detail_page_event_date_size" size="50" value="<?php echo $detail_page_event_date_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_detail_page_event_date" style="background-color: <?php echo (($detail_page_event_date_value[2] != "") ? $detail_page_event_date_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_detail_page_event_date" id="text_colorthemeOption_detail_page_event_date" value="<?php echo (($detail_page_event_date_value[2] != "") ? $detail_page_event_date_value[2] : '') ?>">
                        <?php if (isset($detail_page_event_date_value[2]) && $detail_page_event_date_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete31"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Event_Page_Main_Title'; ?>', 'themeOption_detail_page_event_date', 'hideDelete31', 'text_colorthemeOption_detail_page_event_date', '<?php echo $detail_page_event_date_value[0]; ?>', '<?php echo $detail_page_event_date_value[1]; ?>', '<?php echo $detail_page_event_date_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Data Label</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="detail_page_event_data_label_font">
                            <option value="">Select Font...</option>
                            <?php
                            $detail_page_event_data_label_font = mysql_query($getFonts);
                            while ($detailPageEventDataLabelfont = mysql_fetch_assoc($detail_page_event_data_label_font)) {
                                ?>
                                <option style="font-family: <?php echo $detailPageEventDataLabelfont['TOF_Name']; ?>" <?php echo (($detail_page_event_data_label_value[0] == $detailPageEventDataLabelfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $detailPageEventDataLabelfont['TOF_ID'] ?>"><?php echo $detailPageEventDataLabelfont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="detail_page_event_data_label_size" size="50" value="<?php echo $detail_page_event_data_label_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_detail_page_event_data_label" style="background-color: <?php echo (($detail_page_event_data_label_value[2] != "") ? $detail_page_event_data_label_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_detail_page_event_data_label" id="text_colorthemeOption_detail_page_event_data_label" value="<?php echo (($detail_page_event_data_label_value[2] != "") ? $detail_page_event_data_label_value[2] : '') ?>">
                        <?php if (isset($detail_page_event_data_label_value[2]) && $detail_page_event_data_label_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete32"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Event_Page_Main_Title'; ?>', 'themeOption_detail_page_event_data_label', 'hideDelete32', 'text_colorthemeOption_detail_page_event_data_label', '<?php echo $detail_page_event_data_label_value[0]; ?>', '<?php echo $detail_page_event_data_label_value[1]; ?>', '<?php echo $detail_page_event_data_label_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Data Content</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="detail_page_event_data_content_font">
                            <option value="">Select Font...</option>
                            <?php
                            $detail_page_event_data_content_font = mysql_query($getFonts);
                            while ($detailPageEventDataContentfont = mysql_fetch_assoc($detail_page_event_data_content_font)) {
                                ?>
                                <option style="font-family: <?php echo $detailPageEventDataContentfont['TOF_Name']; ?>" <?php echo (($detail_page_event_data_content_value[0] == $detailPageEventDataContentfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $detailPageEventDataContentfont['TOF_ID'] ?>"><?php echo $detailPageEventDataContentfont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="detail_page_event_data_content_size" size="50" value="<?php echo $detail_page_event_data_content_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_detail_page_event_data_content" style="background-color: <?php echo (($detail_page_event_data_content_value[2] != "") ? $detail_page_event_data_content_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_detail_page_event_data_content" id="text_colorthemeOption_detail_page_event_data_content" value="<?php echo (($detail_page_event_data_content_value[2] != "") ? $detail_page_event_data_content_value[2] : '') ?>">
                        <?php if (isset($detail_page_event_data_content_value[2]) && $detail_page_event_data_content_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete33"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Event_Page_Main_Title'; ?>', 'themeOption_detail_page_event_data_content', 'hideDelete33', 'text_colorthemeOption_detail_page_event_data_content', '<?php echo $detail_page_event_data_content_value[0]; ?>', '<?php echo $detail_page_event_data_content_value[1]; ?>', '<?php echo $detail_page_event_data_content_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Data Border Color</label>
                    <div class="form-data theme-options">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_detail_page_event_border_color" style="background-color: <?php echo (($Region_theme['TO_Event_Page_Data_Border_Color'] != "") ? $Region_theme['TO_Event_Page_Data_Border_Color'] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_detail_page_event_border_color" id="text_colorthemeOption_detail_page_event_border_color" value="<?php echo (($Region_theme['TO_Event_Page_Data_Border_Color'] != "") ? $Region_theme['TO_Event_Page_Data_Border_Color'] : '') ?>">
                        <?php if (isset($Region_theme['TO_Event_Page_Data_Border_Color']) && $Region_theme['TO_Event_Page_Data_Border_Color'] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete34"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Event_Page_Data_Border_Color'; ?>', 'themeOption_detail_page_event_border_color', 'hideDelete34', 'text_colorthemeOption_detail_page_event_border_color', '<?php echo $font_Famail = ""; ?>', '<?php echo $font_Size = ""; ?>', '<?php echo $font_Color = ""; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Stories Page</legend>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Title</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="story_main_title_font">
                            <option value="">Select Font...</option>
                            <?php
                            $story_main_title_font = mysql_query($getFonts);
                            while ($storyMainTitleFont = mysql_fetch_assoc($story_main_title_font)) {
                                ?>
                                <option style="font-family: <?php echo $storyMainTitleFont['TOF_Name']; ?>" <?php echo (($story_main_title_value[0] == $storyMainTitleFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $storyMainTitleFont['TOF_ID'] ?>"><?php echo $storyMainTitleFont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="story_main_title_size" size="50" value="<?php echo $story_main_title_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_story_main_title" style="background-color: <?php echo (($story_main_title_value[2] != "") ? $story_main_title_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_story_main_title" id="text_colorthemeOption_story_main_title" value="<?php echo (($story_main_title_value[2] != "") ? $story_main_title_value[2] : '') ?>">
                        <?php if (isset($story_main_title_value[2]) && $story_main_title_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete35"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Story_Page_Main_Title'; ?>', 'themeOption_story_main_title', 'hideDelete35', 'text_colorthemeOption_story_main_title', '<?php echo $story_main_title_value[0]; ?>', '<?php echo $story_main_title_value[1]; ?>', '<?php echo $story_main_title_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Share Text</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="story_share_text_font">
                            <option value="">Select Font...</option>
                            <?php
                            $story_share_text_font = mysql_query($getFonts);
                            while ($storyShareTextFont = mysql_fetch_assoc($story_share_text_font)) {
                                ?>
                                <option style="font-family: <?php echo $storyShareTextFont['TOF_Name']; ?>" <?php echo (($story_share_text_value[0] == $storyShareTextFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $storyShareTextFont['TOF_ID'] ?>"><?php echo $storyShareTextFont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="story_share_text_size" size="50" value="<?php echo $story_share_text_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_story_share_text" style="background-color: <?php echo (($story_share_text_value[2] != "") ? $story_share_text_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_story_share_text" id="text_colorthemeOption_story_share_text" value="<?php echo (($story_share_text_value[2] != "") ? $story_share_text_value[2] : '') ?>">
                        <?php if (isset($story_share_text_value[2]) && $story_share_text_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete36"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Story_Share_Text'; ?>', 'themeOption_story_share_text', 'hideDelete36', 'text_colorthemeOption_story_share_text', '<?php echo $story_share_text_value[0]; ?>', '<?php echo $story_share_text_value[1]; ?>', '<?php echo $story_share_text_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Content Title</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="story_content_title_font">
                            <option value="">Select Font...</option>
                            <?php
                            $story_content_title_font = mysql_query($getFonts);
                            while ($storyContentTitleFont = mysql_fetch_assoc($story_content_title_font)) {
                                ?>
                                <option style="font-family: <?php echo $storyContentTitleFont['TOF_Name']; ?>" <?php echo (($story_content_title_value[0] == $storyContentTitleFont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $storyContentTitleFont['TOF_ID'] ?>"><?php echo $storyContentTitleFont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="story_content_title_size" size="50" value="<?php echo $story_content_title_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_story_content_title" style="background-color: <?php echo (($story_content_title_value[2] != "") ? $story_content_title_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_story_content_title" id="text_colorthemeOption_story_content_title" value="<?php echo (($story_content_title_value[2] != "") ? $story_content_title_value[2] : '') ?>">
                        <?php if (isset($story_content_title_value[2]) && $story_content_title_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete37"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Story_Page_Content_Title'; ?>', 'themeOption_story_content_title', 'hideDelete37', 'text_colorthemeOption_story_content_title', '<?php echo $story_content_title_value[0]; ?>', '<?php echo $story_content_title_value[1]; ?>', '<?php echo $story_content_title_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>Routes Page</legend>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Title</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="route_main_title_font">
                            <option value="">Select Font...</option>
                            <?php
                            $route_main_title_font = mysql_query($getFonts);
                            while ($routeMainTitlefont = mysql_fetch_assoc($route_main_title_font)) {
                                ?>
                                <option style="font-family: <?php echo $routeMainTitlefont['TOF_Name']; ?>" <?php echo (($route_main_title_value[0] == $routeMainTitlefont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $routeMainTitlefont['TOF_ID'] ?>"><?php echo $routeMainTitlefont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="route_main_title_size" size="50" value="<?php echo $route_main_title_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_route_main_title" style="background-color: <?php echo (($route_main_title_value[2] != "") ? $route_main_title_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_route_main_title" id="text_colorthemeOption_route_main_title" value="<?php echo (($route_main_title_value[2] != "") ? $route_main_title_value[2] : '') ?>">
                        <?php if (isset($route_main_title_value[2]) && $route_main_title_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete38"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Route_Page_Main_Title'; ?>', 'themeOption_route_main_title', 'hideDelete38', 'text_colorthemeOption_route_main_title', '<?php echo $route_main_title_value[0]; ?>', '<?php echo $route_main_title_value[1]; ?>', '<?php echo $route_main_title_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Data Label</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="route_data_label_font">
                            <option value="">Select Font...</option>
                            <?php
                            $route_data_label_font = mysql_query($getFonts);
                            while ($routeDataLabelfont = mysql_fetch_assoc($route_data_label_font)) {
                                ?>
                                <option style="font-family: <?php echo $routeDataLabelfont['TOF_Name']; ?>" <?php echo (($route_data_label_value[0] == $routeDataLabelfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $routeDataLabelfont['TOF_ID'] ?>"><?php echo $routeDataLabelfont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="route_data_label_size" size="50" value="<?php echo $route_data_label_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_route_data_label" style="background-color: <?php echo (($route_data_label_value[2] != "") ? $route_data_label_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_route_data_label" id="text_colorthemeOption_route_data_label" value="<?php echo (($route_data_label_value[2] != "") ? $route_data_label_value[2] : '') ?>">
                        <?php if (isset($route_data_label_value[2]) && $route_data_label_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete39"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Route_Page_Data_Label'; ?>', 'themeOption_route_data_label', 'hideDelete39', 'text_colorthemeOption_route_data_label', '<?php echo $route_data_label_value[0]; ?>', '<?php echo $route_data_label_value[1]; ?>', '<?php echo $route_data_label_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Data Content</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="route_data_content_font">
                            <option value="">Select Font...</option>
                            <?php
                            $route_data_content_font = mysql_query($getFonts);
                            while ($routeDataContentfont = mysql_fetch_assoc($route_data_content_font)) {
                                ?>
                                <option style="font-family: <?php echo $routeDataContentfont['TOF_Name']; ?>" <?php echo (($route_data_content_value[0] == $routeDataContentfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $routeDataContentfont['TOF_ID'] ?>"><?php echo $routeDataContentfont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="route_data_content_size" size="50" value="<?php echo $route_data_content_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_route_data_content" style="background-color: <?php echo (($route_data_content_value[2] != "") ? $route_data_content_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_route_data_content" id="text_colorthemeOption_route_data_content" value="<?php echo (($route_data_content_value[2] != "") ? $route_data_content_value[2] : '') ?>">
                        <?php if (isset($route_data_content_value[2]) && $route_data_content_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete40"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_Route_Page_Data_Content'; ?>', 'themeOption_route_data_content', 'hideDelete40', 'text_colorthemeOption_route_data_content', '<?php echo $route_data_content_value[0]; ?>', '<?php echo $route_data_content_value[1]; ?>', '<?php echo $route_data_content_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
            </fieldset>
            <fieldset>
                <legend>General</legend>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Body Copy</label>
                    <div class="form-data theme-options">
                        <select class="theme-options-font" name="general_body_copy_font">
                            <option value="">Select Font...</option>
                            <?php
                            $general_body_copy_font = mysql_query($getFonts);
                            while ($generalbodycopyfont = mysql_fetch_assoc($general_body_copy_font)) {
                                ?>
                                <option style="font-family: <?php echo $generalbodycopyfont['TOF_Name']; ?>" <?php echo (($general_body_copy_value[0] == $generalbodycopyfont['TOF_ID']) ? "selected" : "") ?> value="<?php echo $generalbodycopyfont['TOF_ID'] ?>"><?php echo $generalbodycopyfont['TOF_Name'] ?></option>
                            <?php } ?>
                        </select>
                        <input class="theme-options-size" type="number" name="general_body_copy_size" size="50" value="<?php echo $general_body_copy_value[1]; ?>" placeholder="Font size">
                        <div class="color-box-inner theme-options-color"><div class="color-box" id="themeOption_general_body_copy" style="background-color: <?php echo (($general_body_copy_value[2] != "") ? $general_body_copy_value[2] : '') ?>"></div></div>
                        <input type="hidden" name="themeOption_general_body_copy" id="text_colorthemeOption_general_body_copy" value="<?php echo (($general_body_copy_value[2] != "") ? $general_body_copy_value[2] : '') ?>">
                        <?php if (isset($general_body_copy_value[2]) && $general_body_copy_value[2] !== '') { ?>
                            <a class="margin-left-resize theme_browse_float_view hideVal hideDelete41"  onclick="removeColor(<?php echo $regionID; ?>, '<?php echo 'TO_General_Body_Copy'; ?>', 'themeOption_general_body_copy', 'hideDelete41', 'text_colorthemeOption_general_body_copy', '<?php echo $general_body_copy_value[0]; ?>', '<?php echo $general_body_copy_value[1]; ?>', '<?php echo $general_body_copy_value[2]; ?>')">Remove</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Body Copy Line Spacing</label>
                    <div class="form-data theme-options">    
                        <input class="theme-options-size" type="text" name="general_body_copy_space" size="50" value="<?php echo $Region_theme['TO_General_Body_Copy_Line_Spacing']; ?>" placeholder="Enter line space">
                    </div>
                </div>          
            </fieldset>
            <fieldset>
                <legend>Logo</legend>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Logo</label>
                    <div class="form-data div_image_library cat-region-width">
                        <label for="photo0" class="daily_browse with-no-library">Browse</label>
                        <div class="cropit-image-preview aboutus-photo-perview">
                            <img class="preview-img preview-img-script0" style="display: none;" src="">    
                            <?php if ($Region_theme['TO_Desktop_Logo'] != '') { ?>
                                <img class="existing-img existing_imgs0" src="<?php echo IMG_LOC_REL . $Region_theme['TO_Desktop_Logo'] ?>" >
                            <?php } ?>
                        </div>
                        <input type="file" onchange="show_file_name(0, this)" name="pic[]" id="photo0" style="display: none;">

                        <input class="region-logo-prvw margin-theme-logo" type="text" placeholder="Alt name" name="desktop_logo_alt" value="<?php echo $Region_theme['TO_Desktop_Logo_Alt'] ?>">
                        <?php if ($Region_theme['TO_Desktop_Logo'] != '') { ?>
                            <a class="deletePhoto delete-region-cat-photo" style="margin-top: 10px;" onclick="return confirm('Are you sure you want to delete photo?');" href="theme-options-mobile.php?rid=<?php echo $regionID ?>&flag=desktop_logo&op=del">Delete Photo</a>
                        <?php } ?>
                    </div>
                </div>
            </fieldset>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="save_color" id="submit_button" value="Submit" />
                </div>
            </div>

        </form>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>
<script>
    function removeColorImage(id, colorVal, backgrontColorsCahnge, hideDelete, divEmpty, font_size, font_color) {
        var remove = confirm("Are you sure this action can not be undone!");
        if (remove == true) {
            var imagedirest = 'imagelink';
            if (font_size != '') {
                var value = font_size + '>';
            }
            var tblname = 'tbl_Theme_Options_Mobile';
            $.ajax({
                type: "POST",
                url: "remove-colors.php",
                data: {
                    regionID_Check: id,
                    themOptionField: colorVal,
                    tablename: tblname,
                    value: value,
                    images_check: imagedirest
                }
            }).done(function (msg) {
                if (msg == 1) {
                    $("#" + divEmpty).val('');
                    $('.' + hideDelete).hide();
                    $('#' + backgrontColorsCahnge).css({'background-color': '#ffff'});
                    swal("Data Deleted", "Data has been deleted successfully.", "success");
                }
            });
        }
    }
    function removeColor(id, colorVal, backgrontColorsCahnge, hideDelete, divEmpty, font_family, font_size, font_color) {
        var remove = confirm("Are you sure this action can not be undone!");
        if (remove == true) {
            //// Table name set  tbl_Theme_Options_Mobile
            var tblname = 'tbl_Theme_Options_Mobile';
            if (font_family != '' && font_size != '') {
                var value = font_family + '-' + font_size + '-';
            }
            $.ajax({
                type: "POST",
                url: "remove-colors.php",
                data: {
                    regionID: id,
                    themOptionField: colorVal,
                    tablename: tblname,
                    value: value
                }
            }).done(function (msg) {
                if (msg == 1) {
                    $("#" + divEmpty).val('');
                    $('.' + hideDelete).hide();
                    $('#' + backgrontColorsCahnge).css({'background-color': '#ffff'});
                    swal("Data Deleted", "Data has been deleted successfully.", "success");
                }
            });
        }
    }
    $(window).load(function () {
        var regionID = '<?php echo $regionID ?>';
        if (typeof regionID != 'undefined' && regionID > 0) {
            $(".color-box").click(function () {
                var div_id = $(this).attr('id');
                var savedColor = $('#' + div_id).attr('style').split("#");
                $('.color-box').colpickSetColor(savedColor[1]);
            });
        }
    });
    $(document).ready(function () {
        //*****************************************color box function*************************
        $('.color-box').colpick({
            colorScheme: 'dark',
            layout: 'rgbhex',
            color: 'ff8800',
            onSubmit: function (hsb, hex, rgb, el) {
                $(el).css('background-color', '#' + hex);
                var div_id = $(el).attr('id');
                //change app background color
                document.getElementById("text_color" + div_id).value = '#' + hex;
                $(el).colpickHide();

            }
        });
    });
</script>
<?PHP
require_once '../include/admin/footer.php';
?>
