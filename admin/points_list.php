<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('manage-listing-points', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if (isset($_POST['edit_button'])) {
    $maxvalues = $_POST['maxvalues'];
    $PC_ID = $_POST['PC_ID'];
    $sum_points = '';
    for ($x = 1; $x < $maxvalues; $x++) {
        $id = $_POST['id' . $x];
        $insert_points = $_POST['points' . $x];
        $insert_updated_date = date("Y-m-d H:i:s");
        $query = "UPDATE points SET points='$insert_points',updated_date='$insert_updated_date' WHERE id='$id'";
        $result = mysql_query($query);
        $sum_points += $insert_points;
    }
    $PC_ID_UPDATE = "UPDATE tbl_Points_Category SET PC_Points='$sum_points' WHERE pc_id='$PC_ID'";
    $category_result = mysql_query($PC_ID_UPDATE);
    if ($result && $category_result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id  = $PC_ID ;
        Track_Data_Entry('Listing Points','','Manage Listing Points',$id,'Update','super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    //update listing for all records
    $new_listing = 0;
    update_pointsin_business_tbl($new_listing);
}
if (isset($_GET['pc_id'])) {
    $PC_ID = $_GET['pc_id'];
}

$query1 = "SELECT name, id, points FROM points WHERE P_PC_ID = '$PC_ID' ORDER BY  ordering ASC ";
$result1 = mysql_query($query1) or die(mysql_error());
$i = 1;
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Listing Points</div>
        <div class="link"></div>
    </div>
    <div class="left">
        <?PHP require 'nav-B-mypage.php'; ?>
    </div>
    <div class="right">
        <form  name="form1" method="post" action="/admin/points_list.php?pc_id=<?php echo $PC_ID; ?>">
            <div class="content-sub-header">
                <div class="data-column padding-none spl-other">Serial Number</div>
                <div class="data-column padding-none spl-name">Name</div>
                <div class="data-column padding-none spl-other">Points</div>
            </div>
            <?php while ($row = mysql_fetch_array($result1)) { ?>
                <div class="data-content">
                    <div class="data-column spl-other"><?php echo$i; ?> </div>
                    <div class="data-column spl-name"><?php echo $row['name']; ?></div>
                    <div class="data-column padding-none spl-other padding-points"><input name="points<?php echo $i; ?>" type="text" value="<?php echo $row['points']; ?>" size="10" required> <input type="hidden" name="id<?php echo $i; ?>" value="<?php echo $row['id']; ?>"> </div>

                </div>
                <?php
                $i++;
            }
            ?>
            <input type="hidden" name="maxvalues" value="<?php echo $i; ?>">
            <div class="form-inside-div  width-data-content"> 
                <div class="button">
                    <input type="hidden" name="PC_ID" value="<?php echo $PC_ID; ?>">
                    <input type="submit" name="edit_button" id="button2" value="Update" />
                </div>
            </div>
        </form>
    </div>
</div>

<?PHP
require_once '../include/admin/footer.php';
?>