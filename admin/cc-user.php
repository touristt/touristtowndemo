<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
if (!in_array('manage-users', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$sql = "SELECT BG_ID, BG_Name FROM tbl_Buying_Group WHERE BG_ID = " . $_REQUEST['bg_id'] . "";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$BG_row = mysql_fetch_assoc($result);

if ($_REQUEST['bg_id'] <> $BG_row['BG_ID']) {
    header("Location: /admin/cc-list.php");
    exit();
}


if ($_REQUEST['id'] > 0) {
    $sql = "SELECT * FROM tbl_Buying_Group_User WHERE BGU_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
}

if ($_POST['op'] == 'save') {
    $sql = "tbl_Buying_Group_User SET 
            BGU_L_Name = '" . encode_strings($_REQUEST['lname'], $db) . "', 
            BGU_F_Name = '" . encode_strings($_REQUEST['fname'], $db) . "', 
            BGU_Email = '" . encode_strings($_REQUEST['email'], $db) . "', 
            BGU_Username = '" . encode_strings($_REQUEST['uname'], $db) . "', 
            BGU_Title = '" . encode_strings($_REQUEST['title'], $db) . "', ";
    if ($_REQUEST['password']) {
        $sql .= "BGU_Password = MD5('" . encode_strings($_REQUEST['password'], $db) . "'), ";
    }
    $sql .= "BGU_Super_User = '" . encode_strings($_REQUEST['admin'], $db) . "'";

    if ($row['BGU_ID'] > 0) {
        $sql = "UPDATE " . $sql . " WHERE BGU_ID = '" . $row['BGU_ID'] . "'";
    } else {
        $sql = "INSERT " . $sql . ", BGU_Buying_Group_ID = '" . $BG_row['BG_ID'] . "'";
    }
    $result = mysql_query($sql, $db);
     if ($result) {
        $_SESSION['success'] = 1;
    }else{
        $_SESSION['error'] = 1;
    }

    header("Location: cc-users.php?bg_id=" . $BG_row['BG_ID']);
    exit();
} elseif ($_GET['op'] == 'del') {

    $sql = "DELETE tbl_Buying_Group_User FROM tbl_Buying_Group_User WHERE BGU_ID = '" . $row['BGU_ID'] . "'";
    $result = mysql_query($sql, $db);
     if ($result) {
        $_SESSION['delete'] = 1;
    }else{
        $_SESSION['delete_error'] = 1;
    }
    header("Location: cc-users.php?bg_id=" . $BG_row['BG_ID']);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo  $BG_row['BG_Name'] ?>: Manage Users</div>
        <div class="link">
            <a href="/admin/cc-user.php?bg_id=<?php echo  $BG_row['BG_ID'] ?>">+Add User</a>
        </div>
    </div>
    <div class="left">
        <?php require_once('../include/nav-home-sub.php'); ?>
    </div>
    <div class="right">
        <form name="form1" method="post" action="cc-user.php">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="id" value="<?php echo  $row['BGU_ID'] ?>">
            <input type="hidden" name="bg_id" value="<?php echo  $BG_row['BG_ID'] ?>">
            <div class="content-header">User Details</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>First Name</label>
                <div class="form-data">
                    <input name="fname" type="text" id="fname" value="<?php echo  $row['BGU_F_Name'] ?>" size="40" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Last Name</label>
                <div class="form-data">
                    <input name="lname" type="text" id="lname" value="<?php echo  $row['BGU_L_Name'] ?>" size="40" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Title</label>
                <div class="form-data">
                    <input name="title" type="text" id="title" value="<?php echo  $row['BGU_Title'] ?>" size="40">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Email Address</label>
                <div class="form-data">
                    <input name="email" type="text" id="email" value="<?php echo  $row['BGU_Email'] ?>" size="40" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Password</label>
                <div class="form-data">
                    <input name="password" type="text" id="password" size="40" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Admin</label>
                <div class="form-data">
                    <input name="admin" type="checkbox" id="admin" value="1" <?php echo  $row['BGU_Super_User'] ? 'checked="checked"' : '' ?> >
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="button5" id="button52" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>