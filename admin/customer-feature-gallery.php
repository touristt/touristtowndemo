<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('listing-tools', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
}

$BL_ID = $_REQUEST['bl_id'];
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
} else {
    header("Location:customers.php");
    exit();
}
if (isset($_POST['op']) && $_POST['op'] == 'save') {
    require '../include/picUpload.inc.php';
    if ($_POST['image_bank'] == "") {
        $ids = '';
        foreach ($_FILES['pic']['name'] as $key => $val) {
            // last @param 1 = Gallery Images
            $pic = Upload_Pic($key, 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 1, 'Listing', $BL_ID);
            if (is_array($pic)) {
                $sqlMax = "SELECT MAX(BFP_Order) FROM tbl_Business_Feature_Photo WHERE BFP_BL_ID = '$BL_ID'";
                $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
                $rowMax = mysql_fetch_row($resultMax);
                $sql = "INSERT tbl_Business_Feature_Photo SET BFP_Title = '" . encode_strings($_REQUEST['title'], $db) . "',
                        BFP_Photo_710X440 = '" . encode_strings($pic['0']['0'], $db) . "',
                        BFP_Photo_195X195 = '" . encode_strings($pic['0']['1'], $db) . "',
                        BFP_Photo_1000X600 = '" . encode_strings($pic['0']['2'], $db) . "',
                        BFP_Photo_285X210 = '" . encode_strings($pic['0']['3'], $db) . "',
                        BFP_Photo_710X440_Mobile = '" . encode_strings($pic['0']['4'], $db) . "',
                        BFP_BL_ID = '$BL_ID', BFP_Order = '" . ($rowMax[0] + 1) . "'";
                $pic_id = $pic['1'];
            }
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $id = mysql_insert_id();
            $ids = $pic_id . ',' . $ids;
            if ($result) {
                $_SESSION['success'] = 1;
                if ($pic_id > 0) {
                    //Image usage from image bank
                    imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Photo_Gallery', $id);
                }
            } else {
                $_SESSION['error'] = 1;
            }
        }
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $BL_ID, 'Photo Gallery', $ids, 'Add Image', 'super admin');
    } else {

        $ids = encode_strings($_POST['image_bank'], $db);
        $pic_ids = explode(",", $ids);
        foreach ($pic_ids as $pic_id) {
            $pic = Upload_Pic_Library($pic_id, 1);
            if (is_array($pic)) {
                Update_Image_Bank_Listings($pic_id, $BL_ID);
                $sqlMax = "SELECT MAX(BFP_Order) FROM tbl_Business_Feature_Photo WHERE BFP_BL_ID = '$BL_ID'";
                $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
                $rowMax = mysql_fetch_row($resultMax);
                $sql = "INSERT tbl_Business_Feature_Photo SET BFP_Title = '" . encode_strings($_REQUEST['title'], $db) . "',
                        BFP_Photo_710X440 = '" . encode_strings($pic['0'], $db) . "',
                        BFP_Photo_195X195 = '" . encode_strings($pic['1'], $db) . "',
                        BFP_Photo_1000X600 = '" . encode_strings($pic['2'], $db) . "',
                        BFP_Photo_285X210 = '" . encode_strings($pic['3'], $db) . "',
                        BFP_Photo_710X440_Mobile = '" . encode_strings($pic['4'], $db) . "',
                        BFP_BL_ID = '$BL_ID', BFP_Order = '" . ($rowMax[0] + 1) . "'";
            }
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $id = mysql_insert_id();
            if ($result) {
                $_SESSION['success'] = 1;
                if ($pic_id > 0) {
                    //Image usage from image bank
                    imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Photo_Gallery', $id);
                }
            } else {
                $_SESSION['error'] = 1;
            }
        }
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $BL_ID, 'Photo Gallery', $ids, 'Add Image', 'super admin');
    }
    //update points only for listing
    update_pointsin_business_tbl($BL_ID);
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);   
    } else {
        header("Location: /admin/customer-feature-gallery.php?bl_id=" . $BL_ID);
    }

    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'edit') {
    $BFP_ID = $_POST['bfp_id'];
    $text = $_POST['description' . $BFP_ID];
    $sql = "UPDATE tbl_Business_Feature_Photo SET BFP_Title = '$text' WHERE BFP_ID = '$BFP_ID'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Photo Gallery', '', 'Edit Description', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);   
    } else {
        header("Location: /admin/customer-feature-gallery.php?bl_id=" . $BL_ID);
    }

    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left">
    <?php require_once '../include/top-nav-listing.php'; ?>
    <div class="title-link">
        <div class="title">Add Ons - <?php echo $rowListing['BL_Listing_Title'] ?></div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div>         
    </div>

    <div class="left">
        <?PHP
        require '../include/nav-listing-admin.php';
        ?>
    </div>

    <div class="right">
        <form name="form1" method="post" onSubmit="return check_img_size(1, 10000000)"  enctype="multipart/form-data" action="" id="gallery-form" style="display:none;float:left;width:100%;margin-bottom: 10px;">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="image_bank" class="image_bank" id="image_bank1" value="">
            <div class="content-header">Add Photo</div>
            <div class="form-inside-div">
                <label>Title</label>
                <div class="form-data">
                    <input name="title" type="text" id="title" size="50" required/>
                </div>
            </div>

            <div class="form-inside-div">
                <label>Photo</label>
                <div class="form-data form-input-text">
                    <div class="div_image_library">
                        <span onchange="show_file_name(1, this, 0)" onclick="show_image_library(1, null,<?php echo $numRowsGal ?>)">Select File</span>
                        <input onchange="show_text()"  accept="image/*" id="photo1" type="file" name="pic[]" style="display: none;" multiple />
                    </div>
                    <input style="width:170px;" id="uploadFile1" class="uploadFileName" disabled>
                    <div class="form-inside-div add-about-us-item-image margin-none" id="preview" style="display: none;">
                        <div class="cropit-image-preview aboutus-photo-perview">
                            <img class="preview-img preview-img-script1" src="">    
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-inside-div">
                <div class="button">
                    <input type="submit" name="button" id="button" value="Save Photo" />
                </div>
            </div>

            <div class="form-inside-div border-none">
                <div class="form-data">
                    Photo file limit size is 10MBs. If you're files are large they may take 10 seconds to load once you click "Save Photo".
                </div>
            </div>
        </form>

        <div class="content-header">
            <div class="title">Photo Gallery</div>
            <div class="link">
                <?PHP
                $counter = 1;
                if ($numRowsGal < 30) {
                    echo '<div class="add-link"><a onclick="show_form()">+Add Photo</a></div>';
                }
                $Gal = show_addon_points(2);
                if ($numRowsGal > 0) {
                    echo '<div class="points"><div class="points-com">' . $Gal . ' pts</div></div>';
                } else {
                    echo '<div class="points"><div class="points-uncom">' . $Gal . ' pts</div></div>';
                }
                ?>
            </div>
            <script>
                function show_form() {
                    $('#gallery-form').show();
                }
            </script>
        </div>

        <?php
        $help_text = show_help_text('Photo Gallery');
        if ($help_text != '') {
            echo '<div class="form-inside-div">' . $help_text . '</div>';
        }
        ?>
        <div class="photo-gallery-limit">
            <span>Photos <?php echo((mysql_num_rows($resultGal) == "") ? "0" : mysql_num_rows($resultGal)) ?> of 30</span>
        </div>
        <div class="form-inside-div border-none margin-none" id="listing-gallery">
            <ul class="gallery">
                <?PHP
                while ($data = mysql_fetch_assoc($resultGal)) {
                    ?>
                    <li class="image" id="recordsArray_<?php echo $data['BFP_ID'] ?>">
                        <?php if ($data['BFP_Photo_285X210'] != "") { ?>
                            <div class="listing-image"><img class="max-width-thumbnail" src="http://<?php echo DOMAIN . IMG_LOC_REL . $data['BFP_Photo_285X210'] ?>" alt="" /></div>
                        <?php } else { ?>
                            <div class="listing-image"><img class="max-width-thumbnail"  src="http://<?php echo DOMAIN . IMG_LOC_REL . $data['BFP_Photo'] ?>" alt="" width="285" height="210"/></div>
                        <?php } ?>
                        <div class="count"><?php echo $counter ?></div>
                        <div class="cross"><a onClick="return confirm('Are you sure?');" href="customer-feature-gallery-photo-delete.php?op=del&id=<?php echo $data['BFP_ID'] ?>&bl_id=<?php echo $data['BFP_BL_ID'] ?>&go_to_preview=<?php echo $_REQUEST['go_to_preview'] ?>">x</a></div>
                        <div class="desc" onclick="show_description(<?php echo $data['BFP_ID'] ?>);"><?php echo ($data['BFP_Title'] != '') ? $data['BFP_Title'] : '+ Add Description'; ?></div> 

                        <form action="" method="post" name="form" id="gallery-description-form<?php echo $data['BFP_ID'] ?>" style="display:none;">
                            <div style="float:left; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                <input type="hidden" name="op" value="edit">
                                <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
                                <input type="hidden" name="bfp_id" value="<?php echo $data['BFP_ID'] ?>">
                                <textarea name="description<?php echo $data['BFP_ID'] ?>" style="width:420px !important;" size="50" required><?php echo $data['BFP_Title'] ?></textarea>
                            </div>
                            <div style="float:left;text-align: center; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                <div class="form-data">
                                    <input type="submit" name="button" value="Save Now"/>
                                </div>
                            </div> 
                        </form>
                    </li>
                    <?PHP
                    $counter++;
                }
                ?>
            </ul>
        </div>

        <?php
        $sql1 = "SELECT BLF_BL_ID, BLF_ID, BLF_Active FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = 2";
        $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
        $row1 = mysql_fetch_assoc($result1);
        if ($row1['BLF_BL_ID'] != '') {
            ?>
            <div class="form-inside-div border-none text-align-center ">
                <a href="customer-feature-add-ons.php?id=<?php echo $row1['BLF_ID'] ?>&bl_id=<?php echo $BL_ID ?>"  onclick="return confirm('Are you sure you want to <?php echo $row1['BLF_Active'] == 1 ? "deactivate" : "activate" ?> this Add on?');"><?php echo $row1['BLF_Active'] == 1 ? "Deactivate" : "Activate" ?> This Add on</a>
            </div>
            <?PHP
        }
        ?>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>

<script>
    $(function () {
        $("#listing-gallery ul.gallery").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Business_Feature_Photo&field=BFP_Order&id=BFP_ID';
                $.post("reorder.php", order, function (theResponse) {
                    swal("success", "Re-order scuccessfully", "success");
                });
            }
        });
    });

    //FUNCTION TO ALERT IF MORE THAN 20 PHOTOS ARE SELECTED
    $("input[type='file']").on("change", function () {
        var numFiles = $(this).get(0).files.length;
        var numf = numFiles + <?php echo mysql_num_rows($resultGal) ?>;
        if (numf > 30) {
            swal("", "You can not upload more than 30 photos in photo gallery.", "warning");
            $("#photo1").val([]);
            document.getElementById("uploadFile1").value = "";
        } else if (numFiles > 20) {
            swal("", "You can upload only 20 photos.", "warning");
            $("#photo1").val([]);
            document.getElementById("uploadFile1").value = "";
        }
    });
    function show_text() {
        $('#uploadFile1').show();
        $('input[type="file"]').change(function () {
            var test = $(this).val(); //alert(test);
            var imageVal = $(this)[0].files.length;
            if (imageVal == 1) {
                document.getElementById("uploadFile1").value = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '');
                $('#preview').hide();
                $('#image_bank1').val('');
                return false;
            }
            document.getElementById("uploadFile1").value = "Multiple Images Selected";
            $('#preview').hide();
            $('#image_bank1').val('');
            return false;
        });

    }
</script>
<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>