<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('manage-support', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Support</div>
        <div class="addlisting"><a href="support-type-section.php?id=<?php echo $_REQUEST['id'] ?>">+Add Item</a></div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-support.php';   ?>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name">Name</div>
            <div class="data-column padding-none spl-other">Edit</div>
            <div class="data-column padding-none spl-other">Delete</div>
        </div>
        <?PHP
        if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
            $sql = "SELECT S_Name, S_ID FROM tbl_Support WHERE S_SS_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_array($result)) {
               // print_r($row );
                ?>
                <div class="data-content">
                    <div class="data-column spl-name"><?php echo $row['S_Name'] ?></div>
                    <div class="data-column spl-other">
                        <a href="support-type-section.php?id=<?php echo $_REQUEST['id'] ?>&sid=<?php echo $row['S_ID'] ?>">Edit</a>
                    </div>
                    <div class="data-column spl-other">    
                        <a href="support-type-delete.php?id=<?php echo $_REQUEST['id'] ?>&sid=<?php echo $row['S_ID'] ?>" onclick="return confirm('Are you sure you want to delete this recommendation?');">Delete</a>
                    </div>
                </div>
                <?PHP
            }
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>