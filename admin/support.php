<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('manage-support', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['id'] > 0) {
    $sql = "SELECT SS_ID, SS_Name FROM tbl_Support_Section WHERE SS_ID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $sql = "tbl_Support_Section SET SS_Name = '" . encode_strings($_REQUEST['name'], $db) . "'";
    if ($_REQUEST['id'] > 0) {
        $sql = "UPDATE " . $sql . " WHERE SS_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $_REQUEST['id'];
        Track_Data_Entry('Support', '', 'Support', $id, 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Support', '', 'Support', $id, 'Add', 'super admin');
    }
    if ($result) {
        $_SESSION['success'] = 1;
    }else{
        $_SESSION['error'] = 1;
    }

    if ($_REQUEST['id'] > 0) {
        header("Location: support.php?id=" . $row['SS_ID']);
    } else {
        header("Location: support-list.php");
    }
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Support</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
    </div>
    <div class="right">
        <form name="form1" method="post" action="">
            <input type="hidden" name="op" value="save">
            <div class="content-header">Add Section</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Name</label>
                <div class="form-data">
                    <input name="name" type="text" size="50" value="<?php echo $row['SS_Name'] ?>" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="submit" value="Save" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>