<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('listing-tools', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
}
$go_to_preview = $_REQUEST['go_to_preview'];
$BL_ID = $_REQUEST['bl_id'];
$sql_abutus = "SELECT BFAMP_Photo_545X335, BFAMP_Photo from tbl_Business_Feature_About_Main_Photo where BFAMP_BL_ID= '$BL_ID'";
$result_abutus = mysql_query($sql_abutus, $db) or die("Invalid query: $sql_abutus -- " . mysql_error());
$row_aboutus = mysql_fetch_assoc($result_abutus);
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_ID, BL_Listing_Title FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
} else {
    header("Location:customers.php");
    exit();
}
// ADDED NEW CODE TO SAVE INSERT AND UPDATE IN THE SAME FILE
if (isset($_REQUEST['updateTrue']) && $_REQUEST['updateTrue'] == 'UpdateValue') {
    $BFA_TITLE_Update = $_REQUEST['titleCategoryAccordion'];
    $BFA_DESC_Update = $_REQUEST['titleCategorytextareaAccordion'];
    $BFA_BL_ID_Update = $_REQUEST['BFA_ID_UPDATE'];
    $BFA_BL_ID = $_REQUEST['BL_ID'];
    $update = "UPDATE tbl_Business_Feature_About SET BFA_Title = '$BFA_TITLE_Update', BFA_Description = '$BFA_DESC_Update'
    WHERE BFA_ID = '$BFA_BL_ID_Update'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'About Us', $_REQUEST['acc'], 'Update Page', 'user admin');
        //update points only for listing
        update_pointsin_business_tbl($BFA_BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /listings-preview.php?bl_id=" . $BL_ID);   
    }
   else
   {
        header("Location: customer-feature-about-us.php?bl_id=" . $BL_ID . "&acc=" . $_REQUEST['acc']);
   }
    
    exit();
} elseif (isset($_REQUEST['InsertTrue']) && $_REQUEST['InsertTrue'] == 'InsertValue') {
    $BFA_TITLE = $_REQUEST['titleCategory'];
    $BFA_DESC = $_REQUEST['titleCategorytextarea'];
    $BFA_BL_ID = $_REQUEST['BL_ID'];
    $sqlMax = "SELECT MAX(BFA_Order) FROM tbl_Business_Feature_About WHERE BFA_BL_ID = '$BFA_BL_ID'";
    $resultMax = mysql_query($sqlMax);
    $rowMax = mysql_fetch_row($resultMax);
    $sql = "INSERT tbl_Business_Feature_About SET 
            BFA_Title = '$BFA_TITLE', 
            BFA_BL_ID = '$BFA_BL_ID', 
            BFA_Description = '$BFA_DESC',
            BFA_Order = '" . ($rowMax[0] + 1) . "'";
    $result = mysql_query($sql);
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'About Us', '', 'Add Page', 'user admin');
        //update points only for listing
        update_pointsin_business_tbl($BFA_BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /listings-preview.php?bl_id=" . $BL_ID);   
    }
   else
   {
        header("Location: customer-feature-about-us.php?bl_id=" . $BL_ID);
   }
    
    exit();
}

if ($_POST['op'] == 'save') {
    $acc = $_POST['acc'];
    require_once '../include/picUpload.inc.php';
    if ($_POST['image_bank'] == "") {
        if ($_POST['main_image'] == "1") {
            // last @param 2 = About Us Main Image
            $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 2);
            if (is_array($pic)) {
                if (mysql_num_rows($result_abutus) > 0) {
                    $sql = "UPDATE tbl_Business_Feature_About_Main_Photo SET
                            BFAMP_Photo_710X440 = '" . encode_strings($pic['0']['0'], $db) . "',
                            BFAMP_Photo_545X335 = '" . encode_strings($pic['0']['1'], $db) . "'
                            WHERE BFAMP_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                } else {
                    $sql = "INSERT tbl_Business_Feature_About_Main_Photo SET  
                            BFAMP_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                            BFAMP_Photo_710X440 = '" . encode_strings($pic['0']['0'], $db) . "',
                            BFAMP_Photo_545X335 = '" . encode_strings($pic['0']['1'], $db) . "'";
                }
                $pic_id = $pic['1'];
            }
        } else {
            // last @param 3 = About Us Accordion Image
            $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 3);
            if (is_array($pic)) {
                $sql = "INSERT tbl_Business_Feature_About_Photo SET  
                        BFAP_Title = '" . encode_strings($_POST['title'], $db) . "',
                        BFAP_BFA_ID = '" . encode_strings($_POST['aid'], $db) . "',
                        BFAP_Photo_470X320 = '" . encode_strings($pic['0']['0'], $db) . "',
                        BFAP_Photo_285X210 = '" . encode_strings($pic['0']['1'], $db) . "'";
                $pic_id = $pic['1'];
            }
        }
    } else {
        $pic_id = $_POST['image_bank'];
        if ($_POST['main_image'] == "1") {
            // last @param 2 = About Us Main Image
            $pic = Upload_Pic_Library($pic_id, 2);
            if (is_array($pic)) {
                if (mysql_num_rows($result_abutus) > 0) {
                    $sql = "UPDATE tbl_Business_Feature_About_Main_Photo SET
                            BFAMP_Photo_710X440 = '" . encode_strings($pic['0'], $db) . "',
                            BFAMP_Photo_545X335 = '" . encode_strings($pic['1'], $db) . "'
                            WHERE BFAMP_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                } else {
                    $sql = "INSERT tbl_Business_Feature_About_Main_Photo SET  
                            BFAMP_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                            BFAMP_Photo_710X440 = '" . encode_strings($pic['0'], $db) . "',
                            BFAMP_Photo_545X335 = '" . encode_strings($pic['1'], $db) . "'";
                }
            }
        } else {
            // last @param 3 = About Us Accordion Image
            $pic = Upload_Pic_Library($pic_id, 3);
            if (is_array($pic)) {
                $sql = "INSERT tbl_Business_Feature_About_Photo SET  
                        BFAP_Title = '" . encode_strings($_POST['title'], $db) . "',
                        BFAP_BFA_ID = '" . encode_strings($_POST['aid'], $db) . "',
                        BFAP_Photo_470X320 = '" . encode_strings($pic['0'], $db) . "',
                        BFAP_Photo_285X210 = '" . encode_strings($pic['1'], $db) . "'";
            }
        }
    }
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $id = mysql_insert_id();
    if ($_POST['main_image'] == "1") {
        $id = "About Us Main Image";
    }
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'About Us', $acc, 'Update Image', 'user admin');
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        if ($pic_id > 0) {
            //Image usage from image bank
            imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_About_Photo', $id);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /listings-preview.php?bl_id=" . $BL_ID);   
    }
   else
   {
        header("Location: customer-feature-about-us.php?bl_id=" . $BL_ID . "&acc=" . $acc);
   }
    
    exit();
}
if ($_POST['op'] == 'edit') {
    $BFAP_ID = $_POST['bfap_id'];
    $acc = $_POST['acc'];
    $text = $_POST['titleName' . $BFAP_ID];
    $sql = "UPDATE tbl_Business_Feature_About_Photo SET BFAP_Title = '$text' WHERE BFAP_ID = '$BFAP_ID'";

    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'About Us', $acc, 'Update Page', 'user admin');
    } else {
        $_SESSION['error'] = 1;
    }
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /listings-preview.php?bl_id=" . $BL_ID);   
    }
   else
   {
        header("Location: customer-feature-about-us.php?bl_id=" . $BL_ID . "&acc=" . $acc);
   }
    
    exit();
}

if ($_REQUEST['op'] == 'del') {
    $bl_id = $_REQUEST['bl_id'];
        
    $sql = "DELETE FROM tbl_Business_Feature_About_Main_Photo WHERE BFAMP_BL_ID = '$bl_id'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['bl_id'];
        Track_Data_Entry('Listing', $id, 'About Us', '', 'Delete Photo', 'user admin');
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_About_Photo', "About Us Main Image");
    } else {
        $_SESSION['delete_error'] = 1;
    }
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /listings-preview.php?bl_id=" . $BL_ID);   
    }
   else
   {
        header("Location: customer-feature-about-us.php?bl_id=" . $bl_id . "&acc=" . $acc);
   }
    
    exit();
}
$activeAccordion = '';
if (isset($_REQUEST['acc']) && $_REQUEST['acc'] != '') {
    $activeAccordion = $_REQUEST['acc'];
} else {
    $activeAccordion = 'false';
}
require_once '../include/admin/header.php';
?>
<div class="content-left">

    <?php require_once '../include/top-nav-listing.php'; ?>

    <div class="title-link">
        <div class="title">Add Ons - <?php echo $rowListing['BL_Listing_Title'] ?></div>

        <div class="link">
            <?PHP require_once('preview-link.php'); ?>
        </div>  

    </div>

    <div class="left">
        <?PHP require '../include/nav-listing-admin.php'; ?>
    </div>
    <div class="right">
        <input type="hidden" id="bidForProduct" value="<?php echo $BL_ID ?>">
        <div class="content-header">
            <div class="title">About Us</div>
            <div class="link">
                <?php
                $aboutUs = show_addon_points(1);
                if ($numRowsAbt > 0) {
                    echo '<div class="points-com">' . $aboutUs . ' pts</div>';
                } else {
                    echo '<div class="points-uncom">' . $aboutUs . ' pts</div>';
                }
                ?>
            </div>
        </div>
        <div class="form-inside-div border-none">
            <?php
            $help_text = show_help_text('About Us');
            if ($help_text != '') {
                echo '<div class="form-inside-div border-none padding-none margin-none">' . $help_text . '</div>';
            }
            ?>
            <div class="addProductAboutUs border-none">
                <a href="#!" id="addProductSection">+ Add Page</a>
            </div>
        </div>
        <form action="" method="POST">
            <div class="addParentCategory" id="on-click-toggle-section" style="display: none">
                <div class="form-inside-div add-product-section">
                    <!-- ADDED NEW HIDDEN VALUE TO TARGET THE PARTICULAR SAVE OPTION -->
                    <input type="hidden" name="InsertTrue" value="InsertValue">
                    <input type="hidden" name="BL_ID" value="<?php echo $BL_ID ?>">
                    <input type="hidden" name="go_to_preview" value="<?php echo $_REQUEST['go_to_preview'] ?>">
                    <div class="form-data add-menu-item-field about-us-text-fields">
                        <label>Name</label>
                        <input id="titleCategory" name="titleCategory" type="text" required="required">
                    </div>
                    <div class="form-data about-us-text-fields wiziwig-about">
                        <label>Description</label>
                        <textarea id="titleCategorytextarea" class="titleCategorytextarea" name="titleCategorytextarea"></textarea>
                    </div>
                    <div class="form-inside-div border-none">
                        <div class="button">
                            <input type="submit"  value="Save">
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="form-inside-div" style="border:none;">
            <div class="content-sub-header">
                <div class="title">Main Image</div>
                <div class="link">
                </div>
            </div>
            <div class="cropped-container1">
                <div class="image-editor1">
                    <div class="cropit-image-preview-container my-container-left">
                        <div class="cropit-image-preview main-preview">
                            <img class="preview-img preview-img-script2" style="display: none;" src="">    
                            <?php if ($row_aboutus['BFAMP_Photo_545X335'] != "") { ?>
                                <img class="existing_imgs2" src="http://<?php echo DOMAIN . IMG_LOC_REL . $row_aboutus['BFAMP_Photo_545X335'] ?>">
                            <?php } else if ($row_aboutus['BFAMP_Photo'] != "") { ?>
                                <img class="existing_imgs2" src="http://<?php echo DOMAIN . IMG_LOC_REL . $row_aboutus['BFAMP_Photo'] ?>">
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <form name="form1" class="form-class-img-preview" method="post" onSubmit="return check_img_size(2, 10000000)"  enctype="multipart/form-data" action="">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="go_to_preview" value="<?php echo $_REQUEST['go_to_preview'] ?>">
            <input type="hidden" name="main_image" value="1">
            <input type="hidden" name="image_bank" class="image_bank" id="image_bank2" value="">
             <div class="form-inside-div margin-none border-none">
                <div class="div_image_library">
                    <span class="daily_browse"  onclick="show_image_library(2)">Select File</span>
                    <input type="file" name="pic[]" onchange="show_file_name(2, this, 0)" id="photo2" style="display: none;">
                    <?php if ($row_aboutus['BFAMP_Photo_545X335'] != "") { ?><div class="data-column delete-main-photo1 padding-none"><a class="deletePhoto margin-left-main" onClick="return confirm('Are you sure?');" href="customer-feature-about-us.php?bl_id=<?php echo $BL_ID ?>&op=del&go_to_preview=<?php echo $go_to_preview ?>">Delete Photo</a></div><?php } ?>
                </div>
            </div>
            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" name="button" id="button" value="Save Image" />
                </div>
            </div>
        </form>
        <div class="menu-items-accodings product-accordion">                         
            <div id="accordion">
                <?PHP
                $acco = 0;
                while ($rowProductResult = mysql_fetch_array($resultAbt)) {
                    $description = $rowProductResult['BFA_Title'];
                    if (strlen($description) > 45) {
                        $description = substr($description, 0, 45) . '...';
                    }
                    ?>
                    <div class="group" id="recordsArray_<?php echo $rowProductResult['BFA_ID']; ?>">
                        <h3 class="accordion-rows about-us-accordion-h3"><span class="about-us-accordion-title"><?php echo $description; ?></span><label class="add-item-accordion"><a onclick="delete_about_us('<?php echo $rowProductResult['BFA_ID']; ?>')">Delete Page</a></label></h3>
                        <div class="sub-accordions accordion-padding">
                            <form method="POST" action="">
                                <div class="addParentCategory">

                                    <input type="hidden" name="updateTrue" value="UpdateValue">
                                    <input type="hidden" name="BFA_ID_UPDATE" value="<?php echo $rowProductResult['BFA_ID']; ?>">
                                    <input type="hidden" name="BL_ID" value="<?php echo $BL_ID ?>">
                                    <input type="hidden" name="go_to_preview" value="<?php echo $_REQUEST['go_to_preview'] ?>">
                                    <input type="hidden" name="acc" value="<?php echo $acco; ?>">

                                    <div class="form-inside-div add-product-section add-product-section-in-accordion">
                                        <div class="form-data add-menu-item-field about-us-text-fields">
                                            <label>Name</label>
                                            <input name="titleCategoryAccordion" id="titleCategoryAccordion_<?php echo $rowProductResult['BFA_ID']; ?>" type="text" value="<?php echo $rowProductResult['BFA_Title']; ?>" required="required">
                                        </div>
                                        <div class="form-data about-us-text-fields wiziwig-about">
                                            <label>Description</label>
                                            <textarea name="titleCategorytextareaAccordion" class="DescriptionHeight titleCategorytextarea" id="titleCategorytextareaAccordion_<?php echo $rowProductResult['BFA_ID']; ?>"><?php echo $rowProductResult['BFA_Description']; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-inside-div form-inside-div1 border-none margin-top-add-photo">
                                    <div class="button text-align-right">
                                        <input type="submit" class="margin-btn-right1"  value="Save & Close"> 
                                    </div>
                                </div>
                            </form>
                            <div class="form-inside-div form-inside-div2 border-none margin-top-add-photo">
                                <div class="button text-align-right">
                                    <input type="submit" id="addProductSectionPhoto<?php echo $rowProductResult['BFA_ID']; ?>" value="+ Add Photo">
                                </div>
                            </div>
                            <div class="addParentCategory" id="on-click-toggle-sectionPhoto<?php echo $rowProductResult['BFA_ID']; ?>" style="display: none">
                                <form name="form1" method="post" onSubmit="return check_img_size(<?php echo $rowProductResult['BFA_ID']; ?>, 10000000)" enctype="multipart/form-data" style="float:left;width:100%;margin-bottom: 10px;">
                                    <input type="hidden" name="id" class="blidforabout" value="<?php echo $BL_ID ?>">
                                    <input type="hidden" name="aid" value="<?php echo $rowProductResult['BFA_ID']; ?>">
                                    <input type="hidden" name="op" value="save">
                                    <input type="hidden" name="go_to_preview" value="<?php echo $_REQUEST['go_to_preview'] ?>">
                                    <input type="hidden" name="acc" value="<?php echo $acco; ?>">
                                    <input type="hidden" name="image_bank" class="image_bank" id="image_bank<?php echo $rowProductResult['BFA_ID'] ?>" value="">
                                    <div class="form-inside-div">
                                        <label>Title</label>
                                        <div class="form-data">
                                            <input name="title" type="text" id="title<?php echo $rowProductResult['BFA_ID']; ?>" size="50"/>
                                        </div>
                                    </div>
                                    <div class="form-inside-div">
                                        <label>Photo</label>
                                        <div class="margin-none form-inside-div div_image_library about-us-cropit border-none">
                                                <span class="daily_browse" onclick="show_image_library(3, <?php echo $rowProductResult['BFA_ID']; ?>)">Select File</span>
                                                <input type="file" style="display :none;" name="pic[]" onChange="show_file_name(3, this, <?php echo $rowProductResult['BFA_ID']; ?>);" id="photo<?php echo $rowProductResult['BFA_ID']; ?>">
                                                <div class="form-inside-div add-about-us-item-image margin-none">
                                                    <div class="cropit-image-preview aboutus-photo-perview">
                                                        <img class="preview-img preview-img-script<?php echo $rowProductResult['BFA_ID']; ?>" style="display: none;" src="">    
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="form-inside-div">
                                        <div class="button">
                                            <input type="submit" name="button" id="button<?php echo $rowProductResult['BFA_ID']; ?>" value="Save Photo" />
                                        </div>
                                    </div>

                                    <div class="form-inside-div border-none">
                                        <div class="form-data">
                                            Photo file limit size is 3MBs. If you're files are large they may take 10 seconds to load once you click "Save Photo".
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <script>
                                CKEDITOR.disableAutoInline = true;
                                $(document).ready(function () {
                                    var wzwig_bfa_ID = <?php echo $rowProductResult['BFA_ID']; ?>;
                                    $('#titleCategorytextareaAccordion_' + wzwig_bfa_ID).ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
                                });
                                $(document).ready(function () {
                                    var BFA_ID = '<?php echo $rowProductResult['BFA_ID']; ?>';
                                    $("#addProductSectionPhoto" + BFA_ID).click(function () {
                                        $("#on-click-toggle-sectionPhoto" + BFA_ID).toggle();
                                    });
                                });
                            </script>
                            <?php
                            $sqlPhoto = "SELECT * FROM tbl_Business_Feature_About_Photo WHERE BFAP_BFA_ID = '" . encode_strings($rowProductResult['BFA_ID'], $db) . "' ORDER BY BFAP_Order";
                            $sqlPhotoResult = mysql_query($sqlPhoto) or die(mysql_error());
                            $counter = 1;
                            ?>
                            <div class="form-inside-div border-none margin-none" id="listing-gallery">
                                <ul class="gallery">
                                    <?php
                                    while ($sqlPhotoResultData = mysql_fetch_array($sqlPhotoResult)) {
                                        if (mysql_num_rows($sqlPhotoResult) > 0) {
                                            ?>
                                            <li class="image" id="recordsArray_<?php echo $sqlPhotoResultData['BFAP_ID'] ?>">
                                                <div class="listing-image">
                                                    <?php if ($sqlPhotoResultData['BFAP_Photo_285X210'] != "") { ?>
                                                        <img class="max-width-thumbnail" src="http://<?php echo DOMAIN . IMG_LOC_REL . $sqlPhotoResultData['BFAP_Photo_285X210'] ?>" alt="" width="285" height="210"/>
                                                    <?php } else { ?>
                                                        <img class="max-width-thumbnail" src="http://<?php echo DOMAIN . IMG_LOC_REL . $sqlPhotoResultData['BFAP_Photo'] ?>" alt=""/>
                                                    <?php } ?>
                                                </div>
                                                <div class="count"><?php echo $counter ?></div>
                                                <div class="cross"><a onClick="return confirm('Are you sure?');" href="customer-feature-about-us-photo-delete.php?op=del&amp;id=<?php echo $sqlPhotoResultData['BFAP_ID'] ?>&bl_id=<?php echo $BL_ID ?>&go_to_preview=<?php echo $go_to_preview ?>">x</a></div>
                                                <div class="desc" onclick="show_description_about_us(<?php echo $sqlPhotoResultData['BFAP_ID'] ?>);"><?php echo ($sqlPhotoResultData['BFAP_Title'] != '') ? $sqlPhotoResultData['BFAP_Title'] : '+ Add Description'; ?></div> 
                                            </li>
                                            <form action="customer-feature-about-us.php" method="post" name="form" id="gallery-description-form-<?php echo $sqlPhotoResultData['BFAP_ID'] ?>" style="display:none;">
                                                <div style="float:left; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                                    <input type="hidden" name="op" value="edit">
                                                    <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
                                                    <input type="hidden" name="go_to_preview" value="<?php echo $_REQUEST['go_to_preview'] ?>">
                                                    <input type="hidden" name="acc" value="<?php echo $acco; ?>">
                                                    <input type="hidden" name="bfap_id" value="<?php echo $sqlPhotoResultData['BFAP_ID'] ?>">
                                                    <input type="text" value="<?php echo $sqlPhotoResultData['BFAP_Title'] ?>" name="titleName<?php echo $sqlPhotoResultData['BFAP_ID'] ?>" style="width:420px !important;padding: 6px 10px !important;">
                                                </div>
                                                <div style="float:left;text-align: center; width:445px; padding:14px 0; border-bottom:1px solid #eeeeee; margin:0 7px 0 18px; font-size:16px;">
                                                    <div class="form-data">
                                                        <input type="submit" name="button" value="Save Now"/>
                                                    </div>
                                                </div> 
                                            </form>
                                            <?php
                                        }
                                        $counter++;
                                    }
                                    ?>
                                </ul>
                            </div>
                            <div class="accordion-separator product-separator"></div>
                        </div>    
                    </div>    
                    <?PHP
                    $acco++;
                }
                ?>
            </div> 
        </div>

        <?php
        $sql1 = "SELECT * FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = 1";
        $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
        $row1 = mysql_fetch_assoc($result1);
        if ($row1['BLF_BL_ID'] != '') {
            ?>
            <div class="form-inside-div border-none text-align-center">
                <a href="customer-feature-add-ons.php?id=<?php echo $row1['BLF_ID'] ?>&bl_id=<?php echo $BL_ID ?>"  onclick="return confirm('Are you sure you want to <?php echo $row1['BLF_Active'] == 1 ? "deactivate" : "activate" ?> this Add on?');"><?php echo $row1['BLF_Active'] == 1 ? "Deactivate" : "Activate" ?> This Add on</a>
            </div>
            <?PHP
        }
        ?>
    </div>
     <div id="image-library" style="display:none;"></div>
</div>
<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>
<script>
    function show_description_about_us(bfp_id) {
        $("#gallery-description-form-" + bfp_id).attr("title", "Edit Description").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 500
        });
    }
    $(document).ready(function () {
        $("#addProductSection").click(function () {
            $("#on-click-toggle-section").toggle();
        });
    });
    $(function () {
        $('#accordion .add-item-accordion a').click(function () {
            return false;
        });
        $("#accordion")
                .accordion({
                    collapsible: true,
                    heightStyle: 'content',
                    active: <?php echo $activeAccordion; ?>,
                    header: "> div > h3"
                })
                .sortable({
                    axis: "y",
                    handle: "h3",
                    update: function () {
                        var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Business_Feature_About&field=BFA_Order&id=BFA_ID';
                        $.post("reorder.php", order, function (theResponse) {
                            swal("Pages Re-ordered", "Pages Re-ordered scuccessfully!", "success");
                        });
                    },
                    stop: function (event, ui) {
                        // IE doesn't register the blur when sorting
                        // so trigger focusout handlers to remove .ui-state-focus
                        ui.item.children("h3").triggerHandler("focusout");

                        // Refresh accordion to handle new order
                        $(this).accordion("refresh");
                    }
                });
    });
    function delete_about_us(bfa_id) {
        var BL_ID = $('#bidForProduct').val();
        var go_to_preview = '<?php echo $go_to_preview ?>';
        if (confirm("Are you sure?")) {
            $.post("customer-feature-about-us-delete.php", {
                bfa_id: bfa_id,
                BL_ID: BL_ID
            }, function (done) {                
                if (done == 1 && (go_to_preview == '' || go_to_preview == null)) {
                    location.reload();
                }else{
                    $(location).attr('href', 'listings-preview.php?bl_id=<?php echo $BL_ID ?>')
                }
            });
        } else {
            return false;
        }
    }
    CKEDITOR.disableAutoInline = true;
    $(document).ready(function () {
        $('#titleCategorytextarea').ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
    });
    function addSection() {
        var addSection = $('#titleCategory').val();
        var addTextArea = $('#titleCategorytextarea').val();
        var BL_ID = $('#bidForProduct').val();
        if (addSection != "") {
            $.post("customer-feature-add-about-us.php", {
                addSection: addSection,
                addTextArea: addTextArea,
                BL_ID: BL_ID,
                InsertTrue: "InsertValue"
            }, function (done) {
                if (done == 1) {
                    location.reload();
                }
            });
        }
    }
    function UpdateSection(BFA_ID, ACTIVE_ACC) {
        var addSectionUpdate = $('#titleCategoryAccordion_' + BFA_ID).val();
        var addTextAreaUpdate = $('#titleCategorytextareaAccordion_' + BFA_ID).val();
        var BL_ID = $('#bidForProduct').val();
        if (addSectionUpdate != "") {
            $.post("customer-feature-add-about-us.php", {
                addSectionUpdate: addSectionUpdate,
                addTextAreaUpdate: addTextAreaUpdate,
                BFA_ID_UPDATE: BFA_ID,
                BL_ID: BL_ID,
                updateTrue: "UpdateValue"
            }, function (done) {
                if (done == 1) {
                    window.location.href = "customer-feature-about-us.php?bl_id=" + BL_ID + "&acc=" + ACTIVE_ACC;
                }
            });
        }
    }
    $(function () {
        $("#listing-gallery ul.gallery").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Business_Feature_About_Photo&field=BFAP_Order&id=BFAP_ID';
                $.post("reorder.php", order, function (theResponse) {
                    swal("Photos Re-ordered", "Photos Re-ordered scuccessfully!", "success");
                });
            }
        });
    });
    function deletePhoto(BL_ID) {
        if (confirm("Are you sure?")) {
            $.post("aboutus-photo-delete.php", {
                BL_ID: BL_ID
            }, function (result) {
                if (result == 1) {
                    location.reload();
                }
            });
        } else {
            return false;
        }
    }
</script>