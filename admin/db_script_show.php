<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">DB Scripts</div>
    </div> 
    <div class="left">
        <?PHP
        require '../include/nav-mobile-admin.php';
        ?>
    </div>
    <div class="right">
        <div class="content-sub-header">
            Script
        </div>
        <div class="data-content">
            <textarea style="width:100%;height:auto" name="text" wrap="soft" disabled="disabled"><?php echo $_REQUEST['qry'] ?> </textarea>
        </div>
    </div>
</div>
<?php
require_once '../include/admin/footer.php';
?>