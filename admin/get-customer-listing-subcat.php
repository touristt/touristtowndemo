<?php
include '../include/config.inc.php';
require_once '../include/login.inc.php';

$cat_id = $_GET['cat_id'];
if ($cat_id > 0) {
    ?>
    <select name="subcategory" id="recommendation-subcat" onChange="$('#form').submit();">
        <option value="">Select Sub Category</option>
        <?PHP
        $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '" . encode_strings($cat_id, $db) . "' ORDER BY C_Name";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            ?>
            <option value="<?php echo $row['C_ID'] ?>"><?php echo $row['C_Name'] ?></option>
            <?PHP
        }
        ?>
    </select>
<?php } else { ?>
    <select name = "subcategory" id = "recommendation-subcat" onChange = "$('#form').submit();">
        <option value = "">Select Sub Category</option>
        <?PHP
        if (isset($subcategory)) {
            $subcat_sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '$category' ORDER BY C_Name";
            $subcat_result = mysql_query($subcat_sql, $db) or die("Invalid query: $subcat_sql -- " . mysql_error());
            while ($subcat_row = mysql_fetch_assoc($subcat_result)) {
                ?>
                <option value="<?php echo $subcat_row['C_ID'] ?>" <?php echo ($subcategory == $subcat_row['C_ID']) ? 'selected' : ''; ?>><?php echo $subcat_row['C_Name'] ?></option>
                <?PHP
            }
        }
        ?>
    </select>
<?php } ?>