<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('customers', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['id'] > 0) {
    $sql = "SELECT B_ID FROM tbl_Business WHERE B_ID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowBUS = mysql_fetch_assoc($result);
    $BID = $rowBUS['B_ID'];
}


if ($_POST['op'] == 'save') {
    if (strlen(trim($_POST['newNote'])) > 3) {
        $sql = "INSERT tbl_Business_Note SET 
                BN_Date =  NOW(),
                BN_B_ID = '" . $BID . "', 
                BN_Author = '" . $_SESSION['USER_ID'] . "', 
                BN_Note = '" . encode_strings($_POST['newNote'], $db) . "'";
        $result = mysql_query($sql, $db);
        if ($result) {
            $_SESSION['success'] = 1;
            // TRACK DATA ENTRY
            $id = $BID;
            Track_Data_Entry('Listing',$id,'Add Note','','Add','super admin');
            
        } else {
            $_SESSION['error'] = 1;
        }
    } else {
        $_SESSION['note_warning'] = 1;
    }
    header("Location: customer-notes.php?id=" . $BID);
    exit();
}
if ($_REQUEST['op'] == 'del') {
    $sql = "DELETE FROM tbl_Business_Note WHERE BN_ID = '" . encode_strings($_REQUEST['nid'], $db) . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $BID;
        Track_Data_Entry('Listing',$id,'Add Note','','Delete','super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: customer-notes.php?id=" . $BID);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Login Details</div>
        <div class="link"></div>
    </div>
    <div class="left">
        <?PHP
        if ($rowBUS['B_ID'] > 0) {
            require '../include/nav-businessprofile.php';
        } else {
            echo "&nbsp;";
        }
        ?>
    </div>
    <div class="right">

        <form action="customer-notes.php?id=<?php echo $BID ?>" method="post">
            <input type="hidden" name="op" value="save">
            <div class="content-header">Add Note</div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Note</label>
                <div class="form-data">
                    <textarea name="newNote" cols="65" rows="8" wrap="VIRTUAL" style="width:auto;"></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin" style="margin-bottom: 15px;">
                <div class="button">
                    <input type="submit" name="button2" value="Save Now"/>
                </div>
            </div> 
        </form>
        <div class="content-header">View Notes</div>
        <div class="content-sub-header">
            <div class="data-column note-date padding-none">Date</div>
            <div class="data-column note-author padding-none">Author</div>
            <div class="data-column notes padding-none">Note</div>
            <div class="data-column note-delete padding-none">Delete</div>
        </div>
        <?PHP
        $sql = "SELECT U_F_Name, U_Username, BN_ID, BN_Note, DATE_FORMAT(BN_Date,'%m/%d/%Y') as notedate FROM tbl_Business_Note 
                LEFT JOIN tbl_User ON U_ID = BN_Author WHERE BN_B_ID = '" . encode_strings($rowBUS['B_ID'], $db) . "' ORDER BY BN_Date DESC";
        $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($resultTMP)) {
            ?>
            <div class="data-content">
                <div class="data-column note-date"><?php echo $row['notedate'] ?></div>
                <div class="data-column note-author">
                    <?php echo $row['U_F_Name'] ?>
                    <br>
                    (
                    <?php echo $row['U_Username'] ?>
                    )
                </div>
                <div class="data-column notes"><?php echo nl2br($row['BN_Note']) ?></div>
                <div class="data-column note-delete">
                    <a href="/admin/customer-notes.php?id=<?php echo $BID ?>&nid=<?php echo $row['BN_ID'] ?>&op=del" onclick="return confirm('Are you sure you want to delete?');">Delete</a>
                </div>
            </div>
            <?PHP
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>