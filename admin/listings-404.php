<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';
if (!in_array('regions', $_SESSION['USER_PERMISSIONS']) && !in_array('manage-county-region', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
    $sql = "SELECT R_Type, R_ID, R_Name, R_Type, R_Include_Free_Listings FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);

    $sortby = (isset($_REQUEST['sortby'])) ? $_REQUEST['sortby'] : '';
    $bycoupon = (isset($_REQUEST['bycoupon'])) ? $_REQUEST['bycoupon'] : '';
    $subcategory = (isset($_REQUEST['subcategory'])) ? $_REQUEST['subcategory'] : '';
    $subCategories = (isset($_REQUEST['subCategories'])) ? $_REQUEST['subCategories'] : '';
    $sql_org_listings = "SELECT BL_ID FROM  tbl_404_Listing WHERE R_ID = '" . encode_strings($regionID, $db) . "'";
    $result_org_listings = mysql_query($sql_org_listings, $db) or die("Invalid query: $sql_org_listings -- " . mysql_error());
    while ($row_org_listings = mysql_fetch_assoc($result_org_listings)) {
        $childbl_id[] = $row_org_listings['BL_ID'];
    }
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - 404 Listings</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">

        <div class="content-sub-header link-header region-header-padding">
            <div class="data-column spl-name padding-none">Select Listings for 404 <?php echo $activeRegion['R_Name'] ?>
            </div>
        </div>
        <form name="form1" method="GET" id="org_listings_form">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <div class="content-sub-header link-header region-header-padding">
                <div class="data-column spl-org-listngs padding-org-listing"><span class="addlisting font_weight_normal" >Sort:<?php //print_r($_REQUEST['rid']);          ?> </span>
                    <?php if ($activeRegion['R_Type'] == 6) {
                        ?>
                        <select name="bycoupon" id="categorydd" onchange="$('#org_listings_form').submit()">
                            <option value="">Sort by:</option>                            
                            <?PHP
                            $sql = "SELECT C_ID, RC_Name, C_Name_SEO, C_Name FROM tbl_Category
                                 LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID
                                 WHERE C_Is_Product_Web = 1 AND RC_Status = 0  AND RC_R_ID = '" . encode_strings($regionID, $db) . "'";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($row = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo $row['C_ID'] ?>" <?php echo ($row['C_ID'] == $bycoupon) ? 'selected' : '' ?> ><?php echo $row['C_Name'] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <?php if (isset($_REQUEST['bycoupon']) && $_REQUEST['bycoupon'] !== '') { ?>
                            <select class="sort-listing-org-width" name="subCategories" id="advert-subcats" onchange="$('#org_listings_form').submit()">
                                <option value="">Select Sub Category</option>
                                <?PHP
                                if ($bycoupon > 0) {
                                    $sql = "SELECT C_ID, C_Name_SEO, C_Name FROM tbl_Category
                                                   WHERE C_Parent = (SELECT C_ID FROM tbl_Category WHERE C_Is_Product_Web = 1) 
                                                   ORDER BY C_Name";
                                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                    while ($row = mysql_fetch_assoc($result)) {
                                        ?>
                                        <option value="<?php echo $row['C_ID'] ?>" <?php echo ($row['C_ID'] == $subCategories) ? 'selected' : '' ?>><?php echo $row['C_Name'] ?></option>
                                        <?PHP
                                    }
                                }
                                ?>
                            </select>
                            <?php
                        }
                    } else {
                        ?>
                        <select name="sortby" id="categorydd" onchange="check_cat_subcat()">
                            <option value="">Sort by:</option>
                            <?PHP
                            $sql = "SELECT C_ID, C_Name FROM tbl_Category 
                                    LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID
                                    WHERE C_Parent = 0 AND C_Is_Blog = 0 AND  C_Is_Product_Web=0 GROUP BY RC_C_ID ORDER BY C_Name";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($row = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo $row['C_ID'] ?>" <?php echo ($row['C_ID'] == $sortby) ? 'selected' : '' ?> ><?php echo $row['C_Name'] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <select class="sort-listing-org-width" name="subcategory" id="advert-subcats" onchange="$('#org_listings_form').submit()">
                            <option value="">Select Sub Category</option>
                            <?PHP
                            if ($sortby > 0) {
                                $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '" . encode_strings($sortby, $db) . "' ORDER BY C_Name";
                                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                while ($row = mysql_fetch_assoc($result)) {
                                    ?>
                                    <option value="<?php echo $row['C_ID'] ?>" <?php echo ($row['C_ID'] == $subcategory) ? 'selected' : '' ?>><?php echo $row['C_Name'] ?></option>
                                    <?PHP
                                }
                            }
                            ?>
                        </select>
                    <?php } ?>
                </div>
            </div>
            <?PHP
            if ($activeRegion['R_Type'] == 1) {
                //Parent Regions
                $sql = "SELECT tbl_Business_Listing.BL_ID, tbl_Business_Listing.BL_Listing_Title FROM tbl_Business_Listing
                        INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID
                        LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                        INNER JOIN tbl_Region_Category ON RC_C_ID = BLC_C_ID AND RC_R_ID='" . encode_strings($regionID, $db) . "'
                        WHERE hide_show_listing = '1' AND RC_Status = 0 AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY))
                        AND BLCR_BLC_R_ID IN (SELECT R_ID from tbl_Region LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID where RM_Parent = '" . encode_strings($regionID, $db) . "')";
            } else {
                if ($activeRegion['R_Type'] == 6) {
                    $sql = "SELECT tbl_Business_Listing.BL_ID, tbl_Business_Listing.BL_Listing_Title 
                            FROM tbl_Business_Feature_Coupon 
                            LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
                            LEFT JOIN tbl_Category ON C_ID = BFCCM_C_ID 
                            LEFT JOIN tbl_Region_Category ON RC_C_ID = C_ID  AND RC_C_ID ='" . encode_strings($regionID, $db) . "'
                            LEFT JOIN tbl_Business_Listing ON BL_ID = BFC_BL_ID           
                            WHERE BFC_Status = 1 AND (BFC_Expiry_Date >= CURDATE() OR BFC_Expiry_Date = 0000-00-00) AND hide_show_listing = '1'";
                } else {
                    $where = "";
                    if ($activeRegion['R_Include_Free_Listings'] == 0) {
                        $where = "AND BL_Listing_Type > 1";
                    }
                    $sql = "SELECT tbl_Business_Listing.BL_ID, tbl_Business_Listing.BL_Listing_Title FROM tbl_Business_Listing 
                            INNER join tbl_Business_Listing_Category_Region on BL_ID = BLCR_BL_ID
                            LEFT join tbl_Business_Listing_Category on BL_ID = BLC_BL_ID
                            INNER JOIN tbl_Region_Category ON RC_C_ID = BLC_M_C_ID
                            WHERE hide_show_listing = 1 AND RC_Status = 0 AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) 
                            AND BLCR_BLC_R_ID = '" . encode_strings($regionID, $db) . "' $where";
                }
            }

            if (isset($sortby) && $sortby != 0) {
                $sql .= " AND BLC_M_C_ID = $sortby";
            }
            if (isset($subcategory) && $subcategory != 0 && $sortby != 0) {
                $sql .= " AND BLC_C_ID = $subcategory";
            }
            if (isset($subCategories) && $subCategories != 0 && $bycoupon != 0) {
                $sql .= " AND BFCCM_C_ID = $subCategories";
            }
            $sql .= " GROUP BY BL_ID ORDER BY BL_Listing_Title";

            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $pages = new Paginate(mysql_num_rows($result), 30);
            $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                ?>
                <div class="data-content">
                    <div class="data-column spl-org-listngs ">
                        <input type="checkbox" id="check_function_<?php echo $row['BL_ID'] ?>" class="org-check-align" name="organization_lisiting[]" value="<?php echo $row['BL_ID'] ?>" <?php echo (in_array($row['BL_ID'], $childbl_id)) ? 'checked' : '' ?> onclick="<?php echo (in_array($row['BL_ID'], $childbl_id)) ? 'remove_listings(' . $row['BL_ID'] . ',' . $activeRegion['R_ID'] . ')' : 'add_listings(' . $row['BL_ID'] . ',' . $activeRegion['R_ID'] . ')' ?>" ><?php echo $row['BL_Listing_Title'] ?></div>
                </div>
                <?php
            }
            if (isset($pages)) {
                echo $pages->paginate();
            }
            ?>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script>
                            function remove_listings(en_id, r_id)
                            {
                                $("#check_function_" + en_id).attr("onclick", 'add_listings(' + en_id + ',' + r_id + ')');
                                $.ajax({
                                    type: "GET",
                                    url: "remove_404_listings.php",
                                    data: {
                                        en_id: en_id,
                                        r_id: r_id
                                    }
                                })
                                        .done(function (msg) {
                                            swal("Listing Removed", "Listing has been removed from 404.", "success");
                                        });
                            }
                            function add_listings(en_id, r_id)
                            { //
                                $("#check_function_" + en_id).attr("onclick", 'remove_listings(' + en_id + ',' + r_id + ')');
                                $.ajax({

                                    type: "GET",
                                    url: "add_404_listings.php",
                                    data: {
                                        en_id: en_id,
                                        r_id: r_id
                                    }
                                })
                                        .done(function (msg) {
                                            swal("Listing Added", "Listing has been added to 404.", "success");
                                        });
                            }
                            function check_cat_subcat()
                            {
                                $("#advert-subcats").empty();
                                $("#org_listings_form").submit();
                            }
</script>