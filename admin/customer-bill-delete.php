<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/ranking.inc.php';

if (!in_array('delete-invoices', $_SESSION['USER_PERMISSIONS']) && !in_array('superadmin', $_SESSION['USER_ROLES'])) {
    header("Location: /admin/");
    exit();
}

$OP = isset($_GET['op']) ? $_GET['op'] : '';
$cc_refund = isset($_GET['cc_refund']) ? $_GET['cc_refund'] : '';
$ID = isset($_GET['id']) ? $_GET['id'] : '';
$BL_ID = isset($_GET['bl_id']) ? $_GET['bl_id'] : '';


if (isset($_POST['op']) && $_POST['op'] == 'del') {
    $OP = isset($_POST['op']) ? $_POST['op'] : '';
    $ID = isset($_POST['id']) ? $_POST['id'] : '';
    $BL_ID = isset($_POST['bl_id']) ? $_POST['bl_id'] : '';
    $REASON = isset($_POST['reason']) ? $_POST['reason'] : '';

    $sql = "UPDATE tbl_Business_Billing SET BB_Deleted = true, 
            BB_Deleted_Date = NOW(), 
            BB_Deleted_By = '" . encode_strings($_SESSION['USER_EM'] . '-' . $_SESSION['user_online'] . '-' . $_SESSION['USER_ID'], $db) . "',
            BB_Deleted_Reason = '" . encode_strings($REASON, $db) . "'
            WHERE BB_ID = '" . encode_strings($ID, $db) . "'";

    mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    header("Location:customer-billing.php?bl_id=" . $BL_ID);
    exit();
}
if (isset($_POST['op']) && $_POST['op'] == 'refund') {
    $OP = isset($_POST['op']) ? $_POST['op'] : '';
    $cc_refund = isset($_POST['cc_refund']) ? $_POST['cc_refund'] : '';
    $ID = isset($_POST['id']) ? $_POST['id'] : '';
    $BL_ID = isset($_POST['bl_id']) ? $_POST['bl_id'] : '';
    $REASON = isset($_POST['reason']) ? $_POST['reason'] : '';

    if ($cc_refund == 1) {
        $sql_BB = "SELECT BB_Transaction_ID, BB_B_ID, BB_Total, BB_Profile_Id, BB_Payment_Profile FROM tbl_Business_Billing WHERE BB_ID = '" . $ID . "'";
        $BB = mysql_fetch_assoc(mysql_query($sql_BB));
        $sql_profile = "SELECT * FROM payment_profiles WHERE business_id = '" . $BB['BB_B_ID'] . "' ORDER BY created_time DESC LIMIT 1";
        $profile = mysql_fetch_assoc(mysql_query($sql_profile));
        $amount = $BB['BB_Total'];

        //get listing billing cycle
        $sqlListing = "SELECT BL_ID, BL_Listing_Title, BL_Billing_Type FROM tbl_Business_Listing WHERE BL_ID = '$BL_ID'";
        $resListing = mysql_query($sqlListing);
        $rowListing = mysql_fetch_assoc($resListing);

        // get authorize.net information of the specified region
        $sql_region = "SELECT * FROM tbl_Region WHERE R_ID = " . $profile['region'] . " LIMIT 1";
        $res_region = mysql_query($sql_region);
        $payment_region = mysql_fetch_assoc($res_region);
        require_once 'payment/config_payment.php';
        // Get Transaction Details
        $td_request = new AuthorizeNetTD;
        $transaction_details = $td_request->getTransactionDetails($BB['BB_Transaction_ID']);
        if ($transaction_details->xml->transaction->transactionStatus == 'settledSuccessfully') {
            $request = new AuthorizeNetCIM;
            $transaction = new AuthorizeNetTransaction;
            $transaction->amount = $amount;
            $transaction->customerProfileId = $BB['BB_Profile_Id'];
            $transaction->customerPaymentProfileId = $BB['BB_Payment_Profile'];
            $transaction->transId = $BB['BB_Transaction_ID']; // original transaction ID

            $response = $request->createCustomerProfileTransaction("Refund", $transaction);
            $transactionResponse = $response->getTransactionResponse();
            if ($transactionResponse->approved) {
                $delete = "UPDATE tbl_Business_Billing SET 
                        BB_Refund_Deleted = 1, 
			BB_Refund_Deleted_Date = NOW(), 
			BB_Refund_Deleted_By = '" . encode_strings($_SESSION['USER_EM'] . '-' . $_SESSION['user_online'] . '-' . $_SESSION['USER_ID'], $db) . "',
                        BB_Refund_Deleted_Reason = '" . encode_strings($REASON, $db) . "'
			WHERE BB_ID = '" . encode_strings($ID, $db) . "'";
                mysql_query($delete);

                if ($rowListing['BL_Billing_Type'] == -1) {
                    $sql_update_renewal = "UPDATE tbl_Business_Listing SET BL_Active_Date = DATE_SUB(BL_Active_Date,INTERVAL 1 YEAR), BL_Renewal_Date = DATE_SUB(BL_Renewal_Date,INTERVAL 1 YEAR) WHERE BL_ID = '$BL_ID'";
                } else {
                    $sql_update_renewal = "UPDATE tbl_Business_Listing SET BL_Active_Date = DATE_SUB(BL_Active_Date,INTERVAL 1 MONTH), BL_Renewal_Date = DATE_SUB(BL_Renewal_Date,INTERVAL 1 MONTH) WHERE BL_ID = '$BL_ID'";
                }
                $log = "Amount: '" . $amount . "' has been refunded to '" . $rowListing['BL_Listing_Title'] . "' BL_ID = '" . $rowListing['BL_ID'] . "' on '" . date("Y-m-d") . "'";
                $result = mysql_query($sql_update_renewal);
                file_put_contents('refund_amounts.log', $log . PHP_EOL, FILE_APPEND);
                $_SESSION['transaction_success'] = 2;
                header("Location: customer-billing.php?bl_id=" . $BL_ID);
                exit();
            } else {
                $encoded = json_encode($response);
                $log = date('Y-m-d H:i:s') . ':  Division Billing: ' . $DB['DB_ID'] . '. Error: ' . $encoded . PHP_EOL;
                file_put_contents('refund_amounts.log', $log . PHP_EOL, FILE_APPEND);
                $_SESSION['transaction_error'] = 1;
                if ($transactionResponse->response_reason_text != '') {
                    $_SESSION['transaction_msg'] = $transactionResponse->response_reason_text;
                } elseif (current($response->xml->messages->message->text) != '') {
                    $_SESSION['transaction_msg'] = current($response->xml->messages->message->text);
                } else {
                    $_SESSION['transaction_msg'] = 'Unknown error';
                }
                header("Location: customer-bill.php?bl_id=" . $BL_ID . "&id=" . $ID);
                exit();
            }
        } elseif ($transaction_details->xml->transaction->transactionStatus == 'capturedPendingSettlement') {
            $void = new AuthorizeNetAIM;
            $void_response = $void->void($BB['BB_Transaction_ID']);
            if ($void_response->approved) {
                $delete = "UPDATE tbl_Business_Billing SET 
                        BB_Refund_Deleted = 1, 
                        BB_Refund_Deleted_Date = NOW(), 
                        BB_Refund_Deleted_By = '" . encode_strings($_SESSION['USER_EM'] . '-' . $_SESSION['user_online'] . '-' . $_SESSION['USER_ID'], $db) . "',
                        BB_Refund_Deleted_Reason = '" . encode_strings($REASON, $db) . "'
                        WHERE BB_ID = '" . encode_strings($ID, $db) . "'";
                mysql_query($delete);
                if ($rowListing['BL_Billing_Type'] == -1) {
                    $sql_update_renewal = "UPDATE tbl_Business_Listing SET BL_Active_Date = DATE_SUB(BL_Active_Date,INTERVAL 1 YEAR), BL_Renewal_Date = DATE_SUB(BL_Renewal_Date,INTERVAL 1 YEAR) WHERE BL_ID = '$BL_ID'";
                } else {
                    $sql_update_renewal = "UPDATE tbl_Business_Listing SET BL_Active_Date = DATE_SUB(BL_Active_Date,INTERVAL 1 MONTH), BL_Renewal_Date = DATE_SUB(BL_Renewal_Date,INTERVAL 1 MONTH) WHERE BL_ID = '$BL_ID'";
                }
                $log = "Amount: '" . $amount . "' has been voided to '" . $rowListing['BL_Listing_Title'] . "' BL_ID = '" . $rowListing['BL_ID'] . "' on '" . date("Y-m-d") . "'";
                $result = mysql_query($sql_update_renewal);
                file_put_contents('refund_amounts.log', $log . PHP_EOL, FILE_APPEND);
                $_SESSION['transaction_success'] = 2;
                header("Location: customer-billing.php?bl_id=" . $BL_ID);
                exit();
            } else {
                $_SESSION['transaction_error'] = 1;
                if ($void_response->response_reason_text != '') {
                    $_SESSION['transaction_msg'] = $void_response->response_reason_text;
                } else {
                    $_SESSION['transaction_msg'] = 'Unknown error';
                }
                header("Location: customer-bill.php?bl_id=" . $BL_ID . "&id=" . $ID);
                exit();
            }
        } else {
            $_SESSION['transaction_error'] = 1;
            $_SESSION['transaction_msg'] = 'Transaction was not found.';
            header("Location: customer-bill.php?bl_id=" . $BL_ID . "&id=" . $ID);
            exit();
        }
    } else {
        $delete = "UPDATE tbl_Business_Billing SET 
                    BB_Refund_Deleted = 1, 
                    BB_Refund_Deleted_Date = NOW(), 
                    BB_Refund_Deleted_By = '" . encode_strings($_SESSION['USER_EM'] . '-' . $_SESSION['user_online'] . '-' . $_SESSION['USER_ID'], $db) . "',
                    BB_Refund_Deleted_Reason = '" . encode_strings($REASON, $db) . "'
                    WHERE BB_ID = '" . encode_strings($ID, $db) . "'";
        mysql_query($delete);

        if ($rowListing['BL_Billing_Type'] == -1) {
            $sql_update_renewal = "UPDATE tbl_Business_Listing SET BL_Active_Date = DATE_SUB(BL_Active_Date,INTERVAL 1 YEAR), BL_Renewal_Date = DATE_SUB(BL_Renewal_Date,INTERVAL 1 YEAR) WHERE BL_ID = '$BL_ID'";
        } else {
            $sql_update_renewal = "UPDATE tbl_Business_Listing SET BL_Active_Date = DATE_SUB(BL_Active_Date,INTERVAL 1 MONTH), BL_Renewal_Date = DATE_SUB(BL_Renewal_Date,INTERVAL 1 MONTH) WHERE BL_ID = '$BL_ID'";
        }
        $log = "Amount: '" . $amount . "' has been refunded to '" . $rowListing['BL_Listing_Title'] . "' BL_ID = '" . $rowListing['BL_ID'] . "' on '" . date("Y-m-d") . "'";
        $result = mysql_query($sql_update_renewal);
        file_put_contents('refund_amounts.log', $log . PHP_EOL, FILE_APPEND);
        $_SESSION['transaction_success'] = 2;
        header("Location: customer-billing.php?bl_id=" . $BL_ID);
        exit();
    }
}
require_once '../include/admin/header.php';
?>
<div class="content-left">

<?php require_once '../include/top-nav-listing.php'; ?>
    <div class="title-link">
        <div class="title"><?php echo ($OP == 'del') ? 'Delete Invoice # ' . $ID . '?' : 'Refund Invoice # ' . $ID . '?'; ?></div>
        <div class="link">
            <a class="invoices" href="customer-bill.php?bl_id=<?php echo $BL_ID ?>&id=<?php echo $ID ?>">Cancel</a></td>
        </div>
    </div>
    <div class="left">
    </div>
    <div class="right">
        <form method="POST" action="">
            <input type="hidden" name="op" value="<?php echo $OP ?>">
            <input type="hidden" name="cc_refund" value="<?php echo $cc_refund ?>">
            <input type="hidden" name="id" value="<?php echo $ID ?>">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Reason</label>
                <div class="form-data">
                    <textarea name="reason" cols="50" required></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin border-none"> 
                <div class="button">
                    <input type="submit" name="submit" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>

<?PHP
require_once '../include/admin/footer.php';
?>