<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/login.inc.mobile.php';
require_once '../../include/adminFunctions.inc.php';
require_once '../../include/track-data-entry.php';

if (!in_array('business-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/mobile/");
    exit();
}

$BL_ID = $_REQUEST['bl_id'];
$points_taken = 0;

if ($BL_ID > 0) {
    $sql = "SELECT BL_B_ID, BL_Listing_Title, BL_Name_SEO, BL_Contact, BL_Phone, BL_Website, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, BL_Street, BL_Town, 
            BL_Province, BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_SEO_Title, BL_SEO_Description, BL_SEO_Keywords, BL_Search_Words, 
            BL_Listing_Type FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['BL_B_ID'];
} else {
    header("Location:customers.php");
    exit();
}

$sqlRegions = "SELECT BLCR_BLC_R_ID FROM tbl_Business_Listing_Category_Region WHERE BLCR_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
$resultRegions = mysql_query($sqlRegions, $db) or die("Invalid query: $sqlRegions -- " . mysql_error());
$temp = 0;
while ($rowRegions = mysql_fetch_assoc($resultRegions)) {
    $sqlRegion = "SELECT R_Show_Hide_Email FROM tbl_Region WHERE R_ID = '" . encode_strings($rowRegions['BLCR_BLC_R_ID'], $db) . "'";
    $resultRegion = mysql_query($sqlRegion, $db) or die("Invalid query: $sqlRegion -- " . mysql_error());
    $rowRegion = mysql_fetch_assoc($resultRegion);
    if ($rowRegion['R_Show_Hide_Email'] == 1) {
        $temp = 1;
    }
}

if ($_POST['op'] == 'save') {
    $sql = "tbl_Business_Listing SET 
            BL_Listing_Title = '" . encode_strings($_REQUEST['name'], $db) . "', 
            BL_Name_SEO = '" . encode_strings($_REQUEST['seoName'], $db) . "', 
            BL_Contact = '" . encode_strings($_REQUEST['contact'], $db) . "', 
            BL_Phone = '" . encode_strings($_REQUEST['phone'], $db) . "', 
            BL_Toll_Free = '" . encode_strings($_REQUEST['tollFree'], $db) . "', 
            BL_Cell = '" . encode_strings($_REQUEST['cell'], $db) . "', 
            BL_Fax = '" . encode_strings($_REQUEST['fax'], $db) . "', 
            BL_Email = '" . encode_strings($_REQUEST['email'], $db) . "', 
            BL_Website = '" . encode_strings($_REQUEST['website'], $db) . "'";


    if ($BL_ID > 0) {
        $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Contact Details', '', 'Update', 'super admin mobile');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Listing', $id, 'Contact Details', '', 'Add', 'super admin mobile');
    }
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: /admin/mobile/customer-listing-contact-details.php?bl_id=" . $BL_ID);
    exit();
}

require_once '../../include/admin/mobile/header.php';
?>

<div class="content-left">
    <div class="left">
        <?php require_once '../../include/top-nav-listing.php'; ?>
        <?PHP require_once '../../include/nav-mypage.php'; ?>
    </div>

    <div class="right">
        <form name="form1" method="post" action="">
            <script>
                function makeSEO(myVar) {
                    myVar = myVar.toLowerCase();
                    myVar = myVar.replace(/[^a-z0-9\s-]/gi, '');
                    myVar = myVar.replace(/"/g, '');
                    myVar = myVar.replace(/'/g, '');
                    myVar = myVar.replace(/&/g, '');
                    myVar = myVar.replace(/\//g, '');
                    myVar = myVar.replace(/,/g, '');
                    myVar = myVar.replace(/  /g, ' ');
                    myVar = myVar.replace(/ /g, '-');

                    $('#SEOname').val(myVar);
                    return false;
                }
            </script>
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="op" value="save">

            <div class="content-header">
                Contact Details - <?php echo $rowListing['BL_Listing_Title'] ?>
                <div class="instruction">
                    Fields with this background<span></span>will show on free listings profile.
                </div>
            </div>

            <div class="form-inside-div">
                <label>Listing Title</label>
                <div class="form-data">
                    <input class="instructional-background" name="name" type="text" id="BLname" value="<?php echo $rowListing['BL_Listing_Title'] ?>" size="50" onKeyUp="makeSEO(this.value)" required/>
                </div>
                <?php
                $listing = show_field_points('Listing Title');
                if ($rowListing['BL_Listing_Title']) {
                    echo '<div class="points-com">' . $listing . ' pts</div>';
                    $points_taken += $listing;
                } else {
                    echo '<div class="points-uncom">' . $listing . ' pts</div>';
                }
                ?>
            </div>

            <div class="form-inside-div">
                <label>SEO Name</label>
                <div class="form-data">
                    <input name="seoName" id="SEOname" type="text" value="<?php echo $rowListing['BL_Name_SEO'] ?>" onKeyUp="makeSEO(this.value)" size="50" required/>
                </div>
            </div>

            <div class="form-inside-div">
                <label>Contact Name</label>
                <div class="form-data">
                    <input name="contact" type="text" value="<?php echo $rowListing['BL_Contact'] ?>" size="50" />
                </div>
                <?php
                $contact = show_field_points('Contact Name');
                if ($rowListing['BL_Contact']) {
                    echo '<div class="points-com">' . $contact . ' pts</div>';
                    $points_taken += $contact;
                } else {
                    echo '<div class="points-uncom">' . $contact . ' pts</div>';
                }
                ?>
            </div>

            <div class="form-inside-div">
                <label>Phone</label>
                <div class="form-data">
                    <input class="instructional-background" name="phone" class="phone_number" type="text" value="<?php echo $rowListing['BL_Phone'] ?>" size="50" />
                </div>
                <?php
                $phone = show_field_points('Phone');
                if ($rowListing['BL_Phone']) {
                    echo '<div class="points-com">' . $phone . ' pts</div>';
                    $points_taken += $phone;
                } else {
                    echo '<div class="points-uncom">' . $phone . ' pts</div>';
                }
                ?>
            </div>

            <div class="form-inside-div">
                <label>Toll Free</label>
                <div class="form-data">
                    <input name="tollFree" class="phone_toll" type="text" value="<?php echo $rowListing['BL_Toll_Free'] ?>" size="50" />
                </div>
                <?php
                $toll = show_field_points('Toll Free');
                if ($rowListing['BL_Toll_Free']) {
                    echo '<div class="points-com">' . $toll . ' pts</div>';
                    $points_taken += $toll;
                } else {
                    echo '<div class="points-uncom">' . $toll . ' pts</div>';
                }
                ?>
            </div>

            <div class="form-inside-div">
                <label>Cell</label>
                <div class="form-data">
                    <input name="cell" class="phone_number" type="text" value="<?php echo $rowListing['BL_Cell'] ?>" size="50" />
                </div>
            </div>

            <div class="form-inside-div">
                <label>Fax</label>
                <div class="form-data">
                    <input name="fax" class="phone_number" type="text" value="<?php echo $rowListing['BL_Fax'] ?>" size="50" />
                </div>
            </div>

            <div class="form-inside-div">
                <label>Email</label>
                <div class="form-data">
                    <input class="<?php echo ($temp == 1) ? 'instructional-background' : ''; ?>" name="email" type="email" value="<?php echo $rowListing['BL_Email'] ?>" size="50" />
                </div>
                <?php
                $Email = show_field_points('Email');
                if ($rowListing['BL_Email']) {
                    echo '<div class="points-com">' . $Email . ' pts</div>';
                    $points_taken += $Email;
                } else {
                    echo '<div class="points-uncom">' . $Email . ' pts</div>';
                }
                ?>
            </div>

            <div class="form-inside-div">
                <label>Website</label>
                <div class="form-data">
                    <input name="website" type="text" value="<?php echo $rowListing['BL_Website'] ?>" size="50" />
                </div>
                <?php
                $website = show_field_points('Website');
                if ($rowListing['BL_Website']) {
                    echo '<div class="points-com">' . $website . ' pts</div>';
                    $points_taken += $website;
                } else {
                    echo '<div class="points-uncom">' . $website . ' pts</div>';
                }
                ?>
            </div>

            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" name="button2" value="Save Now"/>
                </div>
            </div>

            <div class="form-inside-div listing-ranking border-none">
                Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_cd_points ?> points
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../../include/admin/mobile/footer.php';
?>