<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/login.inc.mobile.php';
require_once '../../include/PHPMailer/class.phpmailer.php';
require_once '../../include/track-data-entry.php';

if (!in_array('billing-history', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/mobile/");
    exit();
}

$BL_ID = $_REQUEST['bl_id'];
if (isset($_REQUEST['id'])) {
    $sql = "SELECT B_ID, B_Email FROM tbl_Business WHERE B_ID = " . $_REQUEST['id'] . " LIMIT 1";
    $result = mysql_query($sql);
    $rowBus = mysql_fetch_assoc($result);
    $BID = $rowBus['B_ID'];

    // get regions of the listings in this business
    $sql = "SELECT BL_ID FROM tbl_Business_Listing WHERE BL_B_ID = '$BID'";
    $result = mysql_query($sql);
    while ($rowListing = mysql_fetch_assoc($result)) {
        $sql_region = "SELECT BLCR_BLC_R_ID, RM_Parent FROM tbl_Business_Listing
                        INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID 
                        Left JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                        INNER JOIN tbl_Region_Multiple ON RM_Child = R_ID
                        WHERE BLCR_BL_ID = '" . $rowListing['BL_ID'] . "'";
        $res_region = mysql_query($sql_region);
        $regions = array();
        while ($r = mysql_fetch_assoc($res_region)) {
            // add region parent as well if available
            if ($r['RM_Parent'] != 0 && !in_array($r['RM_Parent'], $regions)) {
                $regions[] = $r['RM_Parent'];
            }
            // add region
            if ($r['BLCR_BLC_R_ID'] != '' && !in_array($r['BLCR_BLC_R_ID'], $regions)) {
                $regions[] = $r['BLCR_BLC_R_ID'];
            }
        }
    }
} else {
    header("Location:customers.php");
    exit();
}

if (isset($_POST['postback'])) {
    $card_number = $_POST['card_number'];
    $exp_date = $_POST['exp_month'] . '/' . $_POST['exp_year'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $address = str_replace(',', '', $_POST['address']);
    $zip = $_POST['zip'];
    $business_id = $_POST['business_id'];
    $bl_id = $_POST['bl_id'];
    $email = $_POST['email'];
    $region = $_POST['region'];

    // get authorize.net information of the specified region
    $sql_region = "SELECT R_API_Login_ID, R_Transaction_Key FROM tbl_Region WHERE R_ID = $region LIMIT 1";
    $res_region = mysql_query($sql_region);
    $payment_region = mysql_fetch_assoc($res_region);
    require_once '../payment/config_payment.php';

    // Create new customer profile
    $request = new AuthorizeNetCIM;
    $customerProfile = new AuthorizeNetCustomer;
    $customerProfile->merchantCustomerId = $business_id . time();
    $customerProfile->email = $email;
    // Add payment profile.
    $paymentProfile = new AuthorizeNetPaymentProfile;
    $paymentProfile->customerType = "individual";
    $paymentProfile->payment->creditCard->cardNumber = $card_number;
    $paymentProfile->payment->creditCard->expirationDate = $exp_date;
    $paymentProfile->billTo->firstName = $first_name;
    $paymentProfile->billTo->lastName = $last_name;
    $paymentProfile->billTo->address = $address;
    $paymentProfile->billTo->zip = $zip;
    $customerProfile->paymentProfiles[] = $paymentProfile;
    $profile_response = $request->createCustomerProfile($customerProfile, 'testMode');
    $parsedresponse = simplexml_load_string($profile_response->response, "SimpleXMLElement", LIBXML_NOWARNING);
    $profile_id = $parsedresponse->customerProfileId;
    $validationResponses = $profile_response->getValidationResponses();
    foreach ($validationResponses as $vr) {
        $validate = $vr->approved;
    }
    //save profile if created
    if ($profile_id != '' && $validate == 1) {
        $customer_profile = $request->getCustomerProfile($profile_id);
        $parsedprofile = simplexml_load_string($customer_profile->response, "SimpleXMLElement", LIBXML_NOWARNING);
        $profile_object = $parsedprofile->profile;
        $payment_profile_id = $profile_object->paymentProfiles->customerPaymentProfileId;
        $card_num = $profile_object->paymentProfiles->payment->creditCard->cardNumber;
        $expiry_date = $profile_object->paymentProfiles->payment->creditCard->expirationDate;
        $profile_email = $profile_object->email;
        $created_date = date('Y-m-d H:i:s');
        // delete existing profiles for this listing
        $sql_delete = sprintf("DELETE FROM payment_profiles WHERE business_id > 0 AND business_id = '%d'", $business_id);
        mysql_query($sql_delete);
        // insert the newly created profile
        $lastdate = date('t', strtotime($_POST['exp_year'] . '-' . $_POST['exp_month'] . '-01'));
        $expiration_card = $_POST['exp_year'] . '-' . $_POST['exp_month'] . '-' . $lastdate;
        $sql_profile = sprintf("INSERT INTO payment_profiles (profile_id, payment_profile_id, email, card_number, expiry_date, first_name, last_name, created_time, card_expiration, business_id, region, address, zip) VALUES('%d', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%d', '%s', '%s')", $profile_id, $payment_profile_id, $profile_email, $card_num, $expiry_date, mysql_real_escape_string($first_name), mysql_real_escape_string($last_name), $created_date, $expiration_card, $business_id, $region, mysql_real_escape_string($address), $zip);
        $result_profile = mysql_query($sql_profile);
        if ($result_profile) {
            $_SESSION['success'] = 1;
            // TRACK DATA ENTRY
            Track_Data_Entry('Listing', $business_id, 'Credit Card Details', '', 'Update Card', 'super admin mobile');
        } else {
            $_SESSION['error'] = 1;
        }
    } else {
        $file = fopen('authorize.log', 'a');
        $message = $profile_response;
        fwrite($file, $message);
        fclose($file);
        $_SESSION['transaction_processing_error'] = 1;
    }

    header("Location: /admin/mobile/customer-credit-card-details.php?id=" . $business_id . "&bl_id=" . $bl_id);
    exit();
}
require_once '../../include/admin/mobile/header.php';
?>

<div class="content-left">
    <div class="left">
        <?PHP
        require '../../include/nav-businessprofile.php';
        ?>
    </div>

    <div class="right">
        <form name="form1" method="post">
            <div class="content-header">
                Credit Card Details
                <div class="instruction">
                    Fields with this background<span></span>will show on free listings profile.
                </div>
            </div>
            <?php
            $sql_card = "SELECT card_number, DATE(created_time) AS created_date FROM payment_profiles WHERE business_id = '$BID' ORDER BY created_time DESC LIMIT 1";
            $res_card = mysql_query($sql_card);
            $card = mysql_fetch_assoc($res_card);
            if ($card['card_number']) {
                $cardNo = explode("XXXX", $card['card_number']);
                $cardNo = "#### #### #### " . $cardNo[1];
                ?>
                <div class="form-inside-div form-inside-div-width-admin">
                    <label>Card on File</label>
                    <div class="form-data margin-top-credit-card-text">
                        <?php echo $cardNo ?>
                    </div>
                </div>
                <div class="form-inside-div  form-inside-div-width-admin">
                    <label>Created Date</label>
                    <div class="form-data margin-top-credit-card-text">
                        <?php echo $card['created_date'] ?>
                    </div>
                </div>
                <?php
            }
            ?>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Card Number</label>
                <div class="form-data">
                    <input type="number" placeholder="Enter Card Number" name="card_number" id="card_number" required />
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin">
                <label>First Name</label>
                <div class="form-data">
                    <input type="text" placeholder="Enter First Name" name="first_name" id="first_name" required />
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin">
                <label>Last Name</label>
                <div class="form-data">
                    <input type="text" placeholder="Enter Last Name" name="last_name" id="last_name" required />
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin">
                <label>Expiration Date</label>
                <div class="form-data">
                    <select name="exp_month" id="exp_month" required>
                        <option value="">Month</option>
                        <option value="01">01</option>
                        <option value="02">02</option>
                        <option value="03">03</option>
                        <option value="04">04</option>
                        <option value="05">05</option>
                        <option value="06">06</option>
                        <option value="07">07</option>
                        <option value="08">08</option>
                        <option value="09">09</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                    </select>
                    <select name="exp_year" id="exp_year" required>
                        <option value="">Year</option>
                        <?php
                        $startingYear = date('Y');
                        for ($index = 0; $index <= 5; $index++) {
                            echo ("<option value=" . $startingYear . ">" . $startingYear . "</option>");
                            $startingYear ++;
                        }
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin">
                <label>Address</label>
                <div class="form-data">
                    <input type="text" placeholder="Enter Address" name="address" required />
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin">
                <label>Postal Code</label>
                <div class="form-data">
                    <input type="text" placeholder="Enter Postal Code" name="zip" required />
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin">
                <label>Region</label>
                <div class="form-data">
                    <select name="region" required>
                        <option value="">Select Region</option>
                        <?php
                        $sql = "SELECT * FROM tbl_Region WHERE R_API_Login_ID != '' AND R_Transaction_Key != ''";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($region = mysql_fetch_assoc($result)) {
                            print '<option value="' . $region['R_ID'] . '" >' . $region['R_Name'] . '</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-inside-div border-none form-inside-div-width-admin">
                <div class="button">
                    <input type="hidden" name="email" value="<?php echo $rowBus['B_Email'] ?>" />
                    <input type="hidden" name="business_id" value="<?php echo $BID ?>" />
                    <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>" />
                    <input type="hidden" name="postback" value="1" />
                    <input type="submit" name="button2" value="Save Now"/>
                </div>
            </div>
        </form>
    </div>
</div>

<?PHP
require_once '../../include/admin/mobile/footer.php';
?>