<?php
require_once '../../include/config.inc.php';
define("DOMAIN", 'touristtowndemo.com');
if (isset($_POST['pages'])) {
    $lowerLimit = $_POST['lowLimit'];
    $upperLimit = $_POST['upLimit'];
    $currentPage = $_POST['current'];
    $sql = "SELECT IB_ID, IB_Path, IB_Thumbnail_Path, IB_Dimension FROM tbl_Image_Bank LIMIT $lowerLimit, $upperLimit";
} else {
    require_once('../../include/top-nav-show-image-bank.php');
    $currentPage = '1';
    $lowerLimit = '0';
    $upperLimit = '100';
    $sql = "SELECT IB_ID, IB_Path, IB_Thumbnail_Path, IB_Dimension FROM tbl_Image_Bank ORDER BY IB_ID DESC LIMIT $lowerLimit, $upperLimit";
}
$result = mysql_query($sql);
$count = mysql_num_rows($result);

$sql_page = "SELECT COUNT(*) AS IB_Total_IMG FROM tbl_Image_Bank ORDER BY IB_ID DESC";
$res_page = mysql_query($sql_page);
$count_row = mysql_fetch_assoc($res_page);
$count_page = $count_row['IB_Total_IMG'];
?>
<div class="image-bank-container image-bank-container-on-dialog" id="image-bank-container">
    <?php
    if ($count > 0) {
        while ($row = mysql_fetch_array($result)) {
            ?>
            <div id="bgcolor_<?php echo $row['IB_ID'] ?>" onclick="select_the_image('<?php echo $row['IB_Path'] ?>','<?php echo $row['IB_ID'] ?>')" class="image-hover-bank image-bank-image-section" style="border: 1px solid #fff;cursor: default;">
                <a style="top: 0px;right: 0px;color: #FFF;"  class="image-section-delete" onclick="return Delete_Images(<?php echo $row['IB_ID'] ?>)">X</a>
                <div class="image-section-image-align">
                    <img src="http://<?php echo DOMAIN . IMG_BANK_REL . $row['IB_Thumbnail_Path'] ?>">
                </div>
                <div style="text-align:center; margin-top: 2px;">
                    <?php echo $row['IB_ID'] . ' - ' . $row['IB_Dimension']; ?>
                </div>
            </div>
            <?php
        }
    } else {
        echo '<div class="no-image-found">No images found in image bank.</div>';
    }
    if ($count_page > 100) {
        $pages = ceil($count_page / 100);
        ?>
        <div class="image-bank-pagination">
            <div class="pagination clearfix">
                <?php
                $i = 1;
                for ($x = 1; $x <= $pages; $x++) {
                    if ($i == 1) {
                        ?>
                        <div class="pull-left">Page <?php echo $currentPage ?> of <?php echo $pages ?></div>
                        <div class="pull-right">
                            <?php
                            $lowerLimit = 0;
                            $upperLimit = 100;
                        } else {
                            $lowerLimit = $upperLimit;
                            $upperLimit = $upperLimit + 100;
                        }
                        $i++;
                        if ($currentPage == $x) {
                            ?>
                            <a onclick="pagination(<?php echo $lowerLimit ?>, 100, <?php echo $x ?>, '<?php echo (isset($_REQUEST['image_id']) ? $_REQUEST['image_id'] : "") ?>', '<?php echo $_REQUEST['iteration'] ?>', 'show_image_bank');" class="page page-active" style="cursor: pointer"><?php echo $x ?></a>
                        <?php } else { ?>
                            <a onclick="pagination(<?php echo $lowerLimit ?>, 100, <?php echo $x ?>, '<?php echo (isset($_REQUEST['image_id']) ? $_REQUEST['image_id'] : "") ?>', '<?php echo $_REQUEST['iteration'] ?>', 'show_image_bank');" class="page" style="cursor: pointer"><?php echo $x ?></a>
                        <?php } ?>
                        <?php if ($i == $pages + 1) {
                            ?>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
    <?php } ?>
    <div id="autocomplete_data" style="display: none;"><?php include '../../include/autocomplete-mobile.php' ?></div>
    <script id="script_auto">
        $(function () {
            //autocomplete for lisitngs
            var search = $('#autocomplete_data').text();
            var searched_list = "";
            var json_data = [];
            if (search != "") {
                searched_list = search.split("%");
                for (var i = 0; i < searched_list.length - 1; i++) {
                    json_data.push({"name": searched_list[i]});
                }
            }
            $("#autocomplete").tokenInput(json_data, {
                onResult: function (item) {
                    if ($.isEmptyObject(item)) {
                        return [{id: '0', name: $("tester").text()}]
                    } else {
                        item.unshift({"name": $("tester").text()});
                        var lookup = {};
                        var result = [];

                        for (var temp, i = 0; temp = item[i++]; ) {
                            var name = temp.name;

                            if (!(name in lookup)) {
                                lookup[name] = 1;
                                result.push({"name": name});
                            }
                        }
                        return result;
                    }

                },
                onAdd: function (item) {
                    var value = $('#keywords-searched').val();
                    if (value != '') {
                        $('#keywords-searched').val(value + "," + item.name);
                        $('#auto_complete_form_submit').submit();
                    } else {
                        $('#keywords-searched').val(item.name);
                        $('#auto_complete_form_submit').submit();
                    }
                },
                onDelete: function (item) {
                    var value = $('#keywords-searched').val().split(",");
                    $('#keywords-searched').empty();
                    var data = "";
                    $.each(value, function (key, value) {
                        if (value != item.name) {
                            if (data != '') {
                                data = data + "," + value;
                            } else {
                                data = value;
                            }
                        }
                    });
                    $('#keywords-searched').val(data);
                }, resultsLimit: 10
            }
            )
            $('.token-input-dropdown').css("width", "200px");
        });
    </script>
</div>