<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/login.inc.mobile.php';
require_once '../../include/adminFunctions.inc.php';
require_once '../../include/track-data-entry.php';

if (!in_array('business-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['bl_id'] > 0) {
    $sql = "SELECT BL_ID, BL_B_ID, BL_Listing_Title, BL_Contact, BL_Phone, BL_Website, BL_Toll_Free, BL_Email, BL_Street, BL_Town, BL_Province, BL_PostalCode, 
            BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_SEO_Title, BL_SEO_Description, BL_SEO_Keywords, BL_Search_Words, 
            BL_Listing_Type, BL_Billing_Type, BL_CoC_Dis_2, BL_Line_Item_Title, BL_Line_Item_Price
            FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['BL_B_ID'];
    $BL_ID = $rowListing['BL_ID'];
} else {
    header("Location: customer.php");
    exit();
}

if ($_POST['op'] == 'save') {
    if ($_POST['newCat'] > 0) {
        mysql_query("DELETE tbl_Business_Listing_Category FROM tbl_Business_Listing_Category WHERE BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BLC_M_C_ID ='" . encode_strings($_POST['newCat_mutiple_id'], $db) . "'");
    }
    if (is_array($_POST['subcat'])) {
        foreach ($_POST['subcat'] as $key => $val) {
            if (substr($key, 0, 1) == 'c') {
                $mykey = substr($key, 1);
                $sql = "UPDATE tbl_Business_Listing_Category SET 
                        BLC_C_ID = '" . encode_strings($val, $db) . "' 
                        WHERE BLC_ID = '" . encode_strings($mykey, $db) . "'";
                $res1 = mysql_query($sql, $db);
            } else {
                if ($val > 0) {
                    $sql = "SELECT COUNT(*) FROM tbl_Business_Listing_Category WHERE 
                            BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    $row = mysql_fetch_row($result);
                    if ($row[0] == 0) {
                        $sql = "INSERT tbl_Business_Listing_Category SET 
                                BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                                BLC_M_C_ID = '" . encode_strings($_POST['category'], $db) . "',
                                BLC_C_ID = '" . encode_strings($val, $db) . "'";
                        if ($row[0] >= 1) {
                            $sql .= ", BLC_Price = '19.00'";
                        }
                    } else {
                        $sql = "INSERT tbl_Business_Listing_Category SET 
                                BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                                BLC_M_C_ID = '" . encode_strings($_POST['category'], $db) . "',    
                                BLC_C_ID = '" . encode_strings($val, $db) . "'";
                        if ($row[0] >= 1) {
                            $sql .= ", BLC_Price = '19.00'";
                        }
                    }
                    $res1 = mysql_query($sql, $db);
                }
            }
        }
    }

    listing_billing($rowListing['BL_Billing_Type'], $BL_ID, $rowListing['BL_CoC_Dis_2'], $rowListing['BL_Line_Item_Title'], $rowListing['BL_Line_Item_Price']);
    if (!$_REQUEST['newSubCat']) {
        if ($res1) {
            $_SESSION['success'] = 1;
            // TRACK DATA ENTRY
            $id = $BL_ID;
            Track_Data_Entry('Listing', $id, 'Category', '', 'Update Category', 'super admin mobile');
        } else {
            $_SESSION['error'] = 1;
        }
    }

    header("Location: customer-listing-category.php?bl_id=" . $BL_ID);
    exit();
}
if ($_POST['op'] == 'save_new') {
    foreach ($_POST['subcat'] as $val) {
        $sql = "SELECT COUNT(*) FROM tbl_Business_Listing_Category WHERE 
                BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $row = mysql_fetch_row($result);
        if ($row[0] == 0) {
            $sql = "INSERT tbl_Business_Listing_Category SET 
                    BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                    BLC_M_C_ID = '" . encode_strings($_POST['category'], $db) . "',
                    BLC_C_ID = '" . encode_strings($val, $db) . "'";
            if ($row[0] >= 1) {
                $sql .= ", BLC_Price = '19.00'";
            }
        } else {
            $sql = "INSERT tbl_Business_Listing_Category SET 
                    BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                    BLC_M_C_ID = '" . encode_strings($_POST['category'], $db) . "',    
                    BLC_C_ID = '" . encode_strings($val, $db) . "'";
            if ($row[0] >= 1) {
                $sql .= ", BLC_Price = '19.00'";
            }
        }
    }
    $res1 = mysql_query($sql, $db);
    if ($res1) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Category', '', 'Add Category', 'super admin mobile');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: customer-listing-category.php?bl_id=" . $BL_ID);
    exit();
}
if ($_POST['op'] == 'save_type') {
    $sql = "tbl_Business_Listing SET 
            BL_Listing_Type = '" . encode_strings($_POST['type'], $db) . "'";

    if ($_POST['type'] <> $rowListing['BL_Listing_Type']) {
        $sqlTMP = "SELECT * FROM tbl_Listing_Type WHERE LT_ID = '" . encode_strings($_POST['type'], $db) . "'";
        $result = mysql_query($sqlTMP, $db) or die("Invalid query: $sqlTMP -- " . mysql_error());
        $row = mysql_fetch_assoc($result);

        $sql .= ", BL_Price = '" . encode_strings($row['LT_Cost'], $db) . "'";
        if ($_POST['type'] == 1) {
            $sql .= ", BL_Basic_Points = '" . encode_strings(0, $db) . "',
                        BL_Addon_Points = '" . encode_strings(0, $db) . "',
                        BL_Points = '" . encode_strings(0, $db) . "'";
        }
    }
    $sql = "UPDATE " . $sql . " WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $res = mysql_query($sql, $db) or die(mysql_error() . ' - ' . $sql);
    // TRACK DATA ENTRY
    $id = $BL_ID;
    Track_Data_Entry('Listing', $id, 'Category', '', 'Update Type', 'super admin mobile');
    if ($_POST['type'] == 1) {
        $sql = "UPDATE tbl_Advertisement set A_Status = 4 WHERE A_BL_ID = $BL_ID";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        if ($res) {
            $_SESSION['success'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
    } else if ($_POST['type'] == 4) {
        $sql = "UPDATE tbl_Advertisement set A_Status = 3 WHERE A_BL_ID = $BL_ID";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        if ($res) {
            $_SESSION['enhanced_success'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
    } else {
        $sql = "UPDATE tbl_Advertisement set A_Status = 3 WHERE A_BL_ID = $BL_ID";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        if ($res) {
            $_SESSION['success'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
    }
}
if ($_REQUEST['delSubCat'] > 0) {
    $sql = "DELETE FROM tbl_Business_Listing_Category WHERE BLC_ID = '" . encode_strings($_REQUEST['delSubCat'], $db) . "' AND BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BLC_M_C_ID = '" . encode_strings($_REQUEST['del_cat'], $db) . "' ";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Category', '', 'Delete Subcategory', 'super admin mobile');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    listing_billing($rowListing['BL_Billing_Type'], $BL_ID, $rowListing['BL_CoC_Dis_2']);
    header("Location: customer-listing-category.php?bl_id=" . $BL_ID);
    exit();
}
if ($_REQUEST['del_cat'] > 0) {
    $sql = "DELETE FROM tbl_Business_Listing_Category WHERE BLC_M_C_ID = '" . encode_strings($_REQUEST['del_cat'], $db) . "' AND BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "' ";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Category', '', 'Delete Category', 'super admin mobile');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    listing_billing($rowListing['BL_Billing_Type'], $BL_ID, $rowListing['BL_CoC_Dis_2']);
    header("Location: customer-listing-category.php?bl_id=" . $BL_ID);
    exit();
}

require_once '../../include/admin/mobile/header.php';
?>

<div class="content-left">
    <div class="left">
        <?php require_once '../../include/top-nav-listing.php'; ?>
        <?PHP
        require '../../include/nav-mypage.php';
        ?>
    </div>

    <div class="right">
        <div class="content-header">
            <div class="title">Category</div>
            <div class="link">
                <div class="add-link">
                    <a onclick="show_form()">+Add Category</a>
                </div>            
            </div>
            <div class="instruction">
                Fields with this background<span></span>will show on free listings profile.
            </div>
        </div>
        <form name="frmCategory" id="frmCategory" method="post" action="">
            <?php
            $sql_type = "SELECT * FROM tbl_Business_Listing WHERE BL_ID='" . encode_strings($BL_ID, $db) . "'";
            $result_type = mysql_query($sql_type);
            $row_type = mysql_fetch_array($result_type);
            ?>
            <input type="hidden" name="op" value="save_type">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <?php
            if (in_array('bgadmin', $_SESSION['USER_ROLES'])) {
                echo '<input type="hidden" value="' . $row_type['BL_Listing_Type'] . '" name="type">';
            } else {
                ?>
                <div class="form-inside-div">
                    <label>Listing Type</label>
                    <div class="form-data">
                        <?php
                        if (in_array('town', $_SESSION['USER_ROLES'])) {
                            $sql = "SELECT * FROM tbl_Listing_Type WHERE LT_ID IN (1, 5) ORDER BY LT_Order";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        } else {
                            $sql = "SELECT * FROM tbl_Listing_Type ORDER BY LT_Order";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        }
                        ?>
                        <select name="type" id="select" required>
                            <option value="">Select Listing Type</option>
                            <?PHP
                            $js = '';
                            while ($row = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo $row['LT_ID'] ?>" <?php echo $row_type['BL_Listing_Type'] == $row['LT_ID'] ? 'selected' : '' ?>><?php echo $row['LT_Name'] ?></option>
                                <?PHP
                            }
                            ?>
                        </select>

                    </div>
                </div>
                <div class="form-inside-div" style="border:none;">
                    <div class="button">
                        <input type="submit" name="button2" value="Save" class="cat-button"/>
                    </div>
                </div>
            <?php } ?>
        </form>            
        <form name="frm_new_Category" id="new_main_Category" method="post" action="" style="display: none;">
            <input type="hidden" name="op" value="save_new">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <div class="form-inside-div">
                <label>Main Category</label>
                <div class="form-data">
                    <select name="category" id="category_0" onChange="get_sub_category(0)" required>
                        <option value="">Select Main Category</option>
                        <?PHP
                        $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = 0 ORDER BY C_Name";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        $js = '';
                        while ($row = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo $row['C_ID'] ?>"><?php echo $row['C_Name'] ?></option>
                            <?PHP
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="form-inside-div">
                <label>Sub-Category 1</label>
                <div class="form-data listings_subcategory_0">
                    <select class="instructional-background" name="" >
                        <option>Select Sub Category</option>
                    </select>
                </div>
            </div>
            <div class="form-inside-div" style="border:none;">
                <div class="button">
                    <input type="submit" name="button2" value="Save" class="cat-button"/>
                </div>
            </div>
        </form>
        <?php
        $sql_new = "SELECT BLC_M_C_ID FROM tbl_Business_Listing_Category
                    LEFT JOIN tbl_Business_Listing ON BL_ID = BLC_BL_ID
                    WHERE BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "' GROUP BY  BLC_M_C_ID";
        $result_get_details_new = mysql_query($sql_new, $db) or die("Invalid query: $sql -- " . mysql_error());
        $main_cat_count = 1;
        while ($rowListing_new = mysql_fetch_assoc($result_get_details_new)) {
            ?>
            <div class="content-header">
                <?php
                $sql_get_name = "SELECT C_Name FROM tbl_Category WHERE C_ID ='" . encode_strings($rowListing_new['BLC_M_C_ID'], $db) . "'";
                $result_get_name = mysql_query($sql_get_name, $db) or die("Invalid query: $sql_get_name -- " . mysql_error());
                $row_get_name = mysql_fetch_assoc($result_get_name);
                ?>
                <div class="title"><?php echo $row_get_name['C_Name'] ?></div>
                <div class="link">
                    <div class="add-link">
                        <a onclick="return confirm('Are you sure?');" href="customer-listing-category.php?bl_id=<?php echo $BL_ID ?>&del_cat=<?php echo $rowListing_new['BLC_M_C_ID']; ?>">Delete</a>
                    </div>            
                </div>
            </div>
            <form name="frmCategory" id="frmCategory" method="post" action="">
                <input type="hidden" name="op" value="save">
                <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
                <input type="hidden" name="delSubCat" id="delSubCat" value="0">
                <input type="hidden" name="newSubCat" id="newSubCat" value="0">
                <input type="hidden" name="newCat" id="newCat_<?php echo $main_cat_count ?>" value="0">
                <input type="hidden" name="newCat_mutiple_id"  value="<?php echo $rowListing_new['BLC_M_C_ID'] ?>">
                <div class="form-inside-div">
                    <label>Main Category</label>
                    <div class="form-data">
                        <select required name="category" id="category_<?php echo $main_cat_count ?>" onChange="get_sub_category(<?php echo $main_cat_count ?>)">

                            <option value="">Select Main Category</option>
                            <?PHP
                            $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = 0 ORDER BY C_Name";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            $js = '';
                            while ($row = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo $row['C_ID'] ?>" <?php echo $rowListing_new['BLC_M_C_ID'] == $row['C_ID'] ? 'selected' : '' ?>><?php echo $row['C_Name'] ?></option>
                                <?PHP
                            }
                            ?>

                        </select>
                        <script type="text/javascript">
                            function confirmCat()
                            {
                                if (confirm('Are you sure, this can not be changed!')) {
                                    newCat();
                                } else {
                                    $('#category option').first().attr('selected', 'selected');
                                    return false;
                                }
                            }
                            function newCat()
                            {
                                $("#newCat").val("1");
                                $('#frmCategory').submit();
                                return false;
                            }

                        </script> 
                    </div>
                </div>

                <?PHP
                if ($rowListing_new['BLC_M_C_ID']) {
                    $sql = "SELECT *, concat('c',BLC_ID) as arrKey FROM tbl_Business_Listing_Category
                            WHERE BLC_BL_ID = '" . encode_strings($BL_ID, $db) . "' and BLC_M_C_ID= '" . encode_strings($rowListing_new['BLC_M_C_ID'], $db) . "' ORDER BY BLC_ID";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    $subCount = mysql_num_rows($result);

                    if ($_REQUEST['newSubCat']) {
                        $subCount++;
                    }

                    $subs = array();
                    while ($rowSubs = mysql_fetch_assoc($result)) {
                        $subs[] = $rowSubs;
                    }
                    ?>

                    <div class="form-inside-div">
                        <label>Sub-Category 1</label>
                        <div class="form-data listings_subcategory_<?php echo $main_cat_count ?>">
                            <?php
                            $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '" . encode_strings($rowListing_new['BLC_M_C_ID'], $db) . "' ORDER BY C_Name";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            ?>
                            <select class="instructional-background" required name="subcat[<?php echo $subs[0]['arrKey'] ?>]" id="subcat-<?php echo $subs[0]['arrKey'] ?>" >
                                <option value="">Select Sub Category</option>
                                <?PHP
                                while ($row = mysql_fetch_assoc($result)) {
                                    ?>
                                    <option value="<?php echo $row['C_ID'] ?>" <?php echo $subs[0]['BLC_C_ID'] == $row['C_ID'] ? 'selected' : '' ?>><?php echo $row['C_Name'] ?></option>
                                    <?PHP
                                }
                                ?>

                            </select>
                        </div>
                    </div>
                    <div id="sub_category_childs_<?php echo $main_cat_count ?>">
                        <?PHP
                        if ($subCount > 1) {
                            for ($i = 1; $i < $subCount; $i++) {
                                ?>
                                <div class="form-inside-div">
                                    <label>Sub-Category <?php echo $i + 1 ?></label>
                                    <div class="form-data">
                                        <select class="instructional-background" name="subcat[<?php echo $subs[$i]['arrKey'] ?>]" id="subcat-<?php echo $subs[$i]['arrKey'] ?>">
                                            <option>Select Sub Category</option>
                                            <?PHP
                                            $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '" . encode_strings($rowListing_new['BLC_M_C_ID'], $db) . "' ORDER BY C_Name";
                                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                                            while ($row = mysql_fetch_assoc($result)) {
                                                ?>
                                                <option value="<?php echo $row['C_ID'] ?>" <?php echo $subs[$i]['BLC_C_ID'] == $row['C_ID'] ? 'selected' : '' ?>><?php echo $row['C_Name'] ?></option>
                                                <?PHP
                                            }
                                            ?>
                                        </select>
                                        <?PHP if ($subs[$i]['BLC_ID']) { ?>
                                            <a class="delete_region" onClick="return delSubCat();" href="customer-listing-category.php?bl_id=<?php echo $rowListing['BL_ID'] ?>&delSubCat=<?php echo $subs[$i]['BLC_ID'] ?>&del_cat=<?php echo $rowListing_new['BLC_M_C_ID']; ?>">Delete</a>
                                            <?PHP
                                        } else {
                                            echo "&nbsp;";
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?PHP
                            }
                        }
                        ?>
                    </div>
                    <div class="form-inside-div" id="sub_cat_new_<?php echo $main_cat_count ?>" style="display:none;">
                        <label>Sub-Category <?php echo $subCount + 1 ?></label>
                        <div class="form-data">
                            <?php
                            $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = '" . encode_strings($rowListing_new['BLC_M_C_ID'], $db) . "' ORDER BY C_Name";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            ?>
                            <select class="instructional-background" name="subcat[]" id="subcat-<?php echo $subs[0]['arrKey'] ?>" class="listings_subcategory">
                                <option>Select Sub Category</option>
                                <?PHP
                                while ($row = mysql_fetch_assoc($result)) {
                                    ?>
                                    <option value="<?php echo $row['C_ID'] ?>"><?php echo $row['C_Name'] ?></option>
                                    <?PHP
                                }
                                ?>

                            </select>
                        </div>
                    </div>
                    <div class="form-inside-div" style="border:none;">
                        <div class="button">
                            <?php if ($subCount == 0) { ?>
                                <input type="submit" name="button2" value="Save Now" class="cat-button"/>
                            <?php } elseif ($_REQUEST['newSubCat']) { ?>
                                <input type="button" name="button2" value="Save Now" onClick='form_submit()' class="cat-button"/>
                                <?php
                            } else {
                                if ($rowListing['BL_Listing_Type'] > 1) {
                                    ?>
                                    <a onClick="return newSubCat(<?php echo $main_cat_count ?>);" id="add_new_sub_cat_<?php echo $main_cat_count ?>" href="#">+Add Sub-Category</a>
                                <?php } ?>
                                <input type="submit" name="button2" value="Save Now"/>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
                <script type="text/javascript">
                    function newSubCat(id) {
                        $("#sub_cat_new_" + id).show();
                        return false;
                    }
                    function form_submit() {
                        $('#frmCategory').submit();
                    }
                    function delSubCat() {
                        if (confirm('Are you sure you want to delete this sub-category?')) {
                            return true;
                        }
                        return false;
                    }
                </script>
            </form>
            <?php
            $main_cat_count++;
        }
        ?>

    </div>
</div>
<script>
    function get_sub_category(count_id) {
        var cat_id = $('#category_' + count_id).val();
        $.ajax({
            type: "GET",
            url: "get-customer-listings-subcat-new.php",
            data: {
                cat_id: cat_id
            }
        })
                .done(function (msg) {
                    $("#newCat_" + count_id).val("1");
                    $("#sub_category_childs_" + count_id).remove();
                    $("#add_new_sub_cat_" + count_id).remove();
                    $(".listings_subcategory_" + count_id).empty();
                    $(".listings_subcategory_" + count_id).html(msg);
                });
    }
    function show_form() {
        $("#new_main_Category").show();
    }
</script>
<?PHP
require_once '../../include/admin/mobile/footer.php';
?>