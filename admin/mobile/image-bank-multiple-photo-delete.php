<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/login.inc.php';
require_once '../../include/image-bank-usage-function.php';
require_once '../../include/adminFunctions.inc.php';
require_once '../../include/track-data-entry.php';

if (isset($_REQUEST['ids']) && $_REQUEST['ids'] != '') {
    require '../../include/picUpload.inc.php';
    $ids = encode_strings($_REQUEST['ids'], $db);
    $pic_ids = explode("," , $ids);
  
    foreach($pic_ids as $pic_id){
       $msg = Delete_Pic_Library($pic_id); 
    }
    
    if ($msg == 1) {
    $_SESSION['delete'] = 1;
    Track_Data_Entry('Imagas','','Image Bank',$ids,'Multiple Delete','super admin');
    echo '1'; 
    } else {
    $_SESSION['delete_error'] = 1;
    echo '2'; 
    }
}