<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/login.inc.mobile.php';
require_once '../../include/admin/mobile/header.php';
?>
<div class="content-left">
    <div class="right">
        <div class="form-inside-div">
            <div class="menu-item-text-align-inside">
                <a href="login-details.php" role="menuitem" tabindex="0">+Add Customer</a>
            </div>
        </div>
        <div class="form-inside-div">
            <div class="menu-item-text-align-inside">
                <a href="customer.php" role="menuitem" tabindex="0">Search by Email</a>
            </div>
        </div>
        <div class="form-inside-div">
            <div class="menu-item-text-align-inside">
                <a href="listings.php" role="menuitem" tabindex="0">Search Listings</a>
            </div>
        </div>
        <div class="form-inside-div">
            <div class="menu-item-text-align-inside">
                <a href="add-image-bank.php" role="menuitem" tabindex="0">Upload photo  to Image Bank</a>
            </div>
        </div>
        <div class="form-inside-div">
            <div class="menu-item-text-align-inside">
                <?php if ($_SESSION['USER_ID']) { ?>
                    <a href="/admin/mobile/?logop=logout">Logout</a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?PHP
require_once '../../include/admin/mobile/footer.php';
?>