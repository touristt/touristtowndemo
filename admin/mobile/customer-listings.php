<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/login.inc.mobile.php';
require_once '../../include/class.Pagination.php';

if (!in_array('business-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['id'] > 0) {
    $sql = "SELECT B_ID FROM tbl_Business WHERE B_ID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowBUS = mysql_fetch_assoc($result);
}

require_once '../../include/admin/mobile/header.php';
?>
<div class="content-left full-width">
    <div class="left">
    </div>
    <div class="right">
        <div class="form-inside-div border-top-listings ">
            <div class="menu-item-text-align-inside"><a href="customer-listing-regions.php?bid=<?php echo $rowBUS['B_ID'] ?>&op=new">+Add Listing</a></a>
            </div>
        </div>
        <?PHP
        $sql = "SELECT BL_ID, BL_Listing_Title FROM tbl_Business_Listing
                WHERE BL_B_ID = '" . encode_strings($rowBUS['B_ID'], $db) . "' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY))";
        $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($resultTMP)) {
            ?>
            <div class="form-inside-div">
                <div class="menu-item-text-align-inside"><a href="customer-listing-mobile-menu.php?bl_id=<?php echo $row['BL_ID'] ?>"><?php echo $row['BL_Listing_Title'] ?></a>
                </div>
            </div>
            <?PHP
        }
        ?>
    </div>
</div>
<?PHP
require_once '../../include/admin/mobile/footer.php';
?>
<script language="javascript">
    function hide_show_listings(business_listing_id){
        var value=document.getElementById('display'+business_listing_id).checked;
          
           
        xmlHttp=GetXmlHttpObject();
        if (xmlHttp==null)
        {
            alert ("Your browser does not support AJAX!");
            return;
        }
        // alert(cid);
        var url="show_hide_listing.php?BL_ID="+business_listing_id+"&show_hide="+value;
        url = url+"&sid="+Math.random();
        xmlHttp.open("GET",url,true);
        xmlHttp.send(null);
        xmlHttp.onreadystatechange=stateWaiting;
        function stateWaiting()
        {
            if(xmlHttp.readyState==4)
            {
                var data = xmlHttp.responseText;
                alert(data);
            }
        }

        function GetXmlHttpObject()
        {
            var xmlHttp = null;
            try{
                xmlHttp = new XMLHttpRequest();
            }
            catch (e){
                try{
                    xmlHttp = new ActiveXObject("Msxm12.XMLHTTP");
                }
                catch (e)
                {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
            }
            return xmlHttp;
        }
    }
</script>
