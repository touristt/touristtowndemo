<?PHP
ob_start();
require_once '../../include/config.inc.php';
require_once '../../include/login.inc.mobile.php';
require_once '../../include/image-bank-usage-function.php';
require_once '../../include/adminFunctions.inc.php';
require_once '../../include/track-data-entry.php';

if (!in_array('business-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$BL_ID = $_REQUEST['bl_id'];
$points_taken = '';

if ($BL_ID > 0) {
    $sql = "SELECT BL_B_ID, BL_Listing_Title, BL_Contact, BL_Phone, BL_Website, BL_Toll_Free, BL_Email, BL_Street, BL_Town, BL_Province, BL_PostalCode, 
            BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Photo, BL_SEO_Title, BL_SEO_Description, BL_SEO_Keywords, BL_Search_Words, 
            BL_Listing_Type FROM tbl_Business_Listing WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing1 = mysql_fetch_assoc($result);
    $BID = $rowListing1['BL_B_ID'];
} else {
    header("Location:customers.php");
    exit();
}

//TO GET THE CORRESPONDING REGIONS OF CURRENT LISTING
if (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
    $regionLimit = array();
    $limits = explode(',', $_SESSION['USER_LIMIT']);
    foreach ($limits as $limit) {
        $sql = "SELECT R_ID, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $region = mysql_fetch_assoc($result);
        if (isset($region['R_Type']) && $region['R_Type'] == 1) {
            $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($region['R_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                if ($row['R_ID'] != '' && !in_array($row['R_ID'], $regionLimit)) {
                    $regionLimit[] = $row['R_ID'];
                }
            }
        } else {
            if ($region['R_ID'] != '' && !in_array($region['R_ID'], $regionLimit)) {
                $regionLimit[] = $region['R_ID'];
            }
        }
    }
}
$regionLimitCommaSeparated = rtrim(implode(',', $regionLimit), ',');

if (isset($BL_ID) && $BL_ID > 0) {
    $sql = "SELECT BLCR_BLC_R_ID, BLCR_ID FROM tbl_Business_Listing_Category_Region  WHERE BLCR_BL_ID = " . encode_strings($BL_ID, $db);
    $sql .= $regionLimitCommaSeparated ? (" AND BLCR_BLC_R_ID IN(" . $regionLimitCommaSeparated . ") ") : "";
    $resultRegion = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($rowRegion = mysql_fetch_assoc($resultRegion)) {
        ?>
        <?php
        $sql = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Type NOT IN(1,6)";
        $sql .= $regionLimitCommaSeparated ? (" AND R_ID IN(" . $regionLimitCommaSeparated . ") ") : "";
        $sql .= " ORDER BY R_Name";
        $resultRegionList = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        ?>                                      
        <?PHP
        while ($rowRList = mysql_fetch_assoc($resultRegionList)) {
            if ($rowRList['R_ID'] == $rowRegion['BLCR_BLC_R_ID']) {
                $regs[] = $rowRList['R_ID'] . ',' . $rowRList['R_Name'];
            }
        }
    }
}

if ($_POST['op'] == 'save') {
    $BL_ID = $_REQUEST['bl_id'];
    $R_ID = $_REQUEST['r_id'];
    $sql = "SELECT * FROM tbl_Business_Listing_Photo WHERE BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BLP_R_ID = '" . encode_strings($R_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $val = mysql_fetch_assoc($result);


    require '../../include/picUpload.inc.php';
    //Thumbnail Image
    if ($_POST['thumb_photo'] == "") {
        // last @param 21 = Listing Thumbnail Image
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 21, 'Listing', $BL_ID);
        if (is_array($pic)) {
            if ($val['BLP_BL_ID'] > 0 && $val['BLP_R_ID'] > 0) {
                $sql = "UPDATE tbl_Business_Listing_Photo SET BLP_Photo = '" . encode_strings($pic['0']['0'], $db) . "', BLP_Mobile_Photo = '" . encode_strings($pic['0']['1'], $db) . "' WHERE BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BLP_R_ID = '" . encode_strings($R_ID, $db) . "'";
                $result = mysql_query($sql, $db);
            } else {
                $sql = "INSERT tbl_Business_Listing_Photo SET BLP_Photo = '" . encode_strings($pic['0']['0'], $db) . "',
                    BLP_Mobile_Photo = '" . encode_strings($pic['0']['1'], $db) . "',
                    BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                    BLP_R_ID = '" . encode_strings($R_ID, $db) . "' ";
                $result = mysql_query($sql, $db);
            }
        }
        $pic_id = $pic['1'];
    } else {
        $pic_id = $_POST['thumb_photo'];
        // last @param 21 = Listing Thumbnail Image
        $pic_response = Upload_Pic_Library($pic_id, 21);
        if ($pic_response) {

            if ($val['BLP_BL_ID'] > 0 && $val['BLP_R_ID'] > 0) {
                $sql = "UPDATE tbl_Business_Listing_Photo SET BLP_Photo = '" . encode_strings($pic_response['0'], $db) . "', BLP_Mobile_Photo = '" . encode_strings($pic_response['1'], $db) . "' WHERE BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BLP_R_ID = '" . encode_strings($R_ID, $db) . "'";
                $result = mysql_query($sql, $db);
            } else {
                $sql = "INSERT tbl_Business_Listing_Photo SET BLP_Photo = '" . encode_strings($pic_response['0'], $db) . "',
                   BLP_Mobile_Photo = '" . encode_strings($pic_response['1'], $db) . "',
                    BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "',
                    BLP_R_ID = '" . encode_strings($R_ID, $db) . "'";
                $result = mysql_query($sql, $db);
            }
        }
    }
    if ($result && $pic_id > 0) {
        //Image usage from image bank.
        imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Main_Photo', 'Listing Thumbnail Image', 'IBU_R_ID', $R_ID);
    }
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Thumbnail Photos / Video', '', 'Update', 'super admin mobile');
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: customer-listing-thumbnail-photo.php?bl_id=" . $BL_ID);
    exit();
} elseif ($_GET['op'] == 'del') {
    $BL_ID = $_REQUEST['bl_id'];
    $R_ID = $_REQUEST['r_id'];
    $del = "BLP_Photo = ''";
    $photo_type = 'Listing Thumbnail Image';
    $sql = "UPDATE tbl_Business_Listing_Photo SET $del WHERE BLP_BL_ID = '$BL_ID' AND BLP_R_ID = '$R_ID'";

    $result = mysql_query($sql);
    if ($result) {
        $_SESSION['delete'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Main_Photo', $photo_type, 'IBU_R_ID', $R_ID);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Thumbnail Photos / Video', '', 'Delete Photo', 'super admin mobile');
    } else {
        $_SESSION['delete_error'] = 1;
    }

    header("Location: customer-listing-thumbnail-photo.php?bl_id=" . $BL_ID);
    exit();
}

require_once '../../include/admin/mobile/header.php';
?>
<div class="content-left">
    <div class="left">
        <?php require_once '../../include/top-nav-listing.php'; ?>
        <?PHP require_once '../../include/nav-mypage.php'; ?>
    </div>
    <div class="right">
        <div class="listing-inside-div-tittle">
        </div>

        <?php if (sizeof($regs) == 0) { ?>
            <div class="form-inside-div border-none main-photos">
                <div class="content-sub-header">
                    <div class="title">No Website Selected</div>
                </div>
                <div class="form-inside-div margin-none border-none">Kindly select a website to upload Main Header Photo</div>

            </div> 

            <?php
        } else {
            foreach ($regs as $key => $value) {
                $reg = explode(",", $value);
                ?>
                <!--<form onSubmit="return false" id="MyUploadForm" name="form1" id="progress-form" method="post" enctype="multipart/form-data" action="">-->
                <form name="form1" method="post" enctype="multipart/form-data" action="">
                    <input type="hidden" name="op" value="save">
                    <input type="hidden" id="image_page_mobile_redirect" value="1">
                    <input type="hidden" name="bl_id" id="bl_id" value="<?php echo $BL_ID ?>">
                    <input type="hidden" name="r_id" value="<?php echo $reg[0] ?>">
                    <input type="hidden" id="listing_type" value="<?php echo $rowListing1['BL_Listing_Type'] ?>">
                    <div class="content-header">
                        Thumbnail Photos (<?php echo $reg[1] ?>)
                        <div class="instruction">
                            Fields with this background<span></span>will show on free listings profile.
                        </div>
                    </div>
                    <?PHP
                    $sql = "SELECT * FROM tbl_Business_Listing_Photo WHERE BLP_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BLP_R_ID = '" . encode_strings($reg[0], $db) . "' LIMIT 1";
                    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    $rowListing = mysql_fetch_assoc($result);
                    ?>
                    <div class="form-inside-div" style="border:none;">
                        <div class="content-sub-header">
                            <div class="title">Thumbnail</div>
                            <div class="link">
                                <?php
                                $thumbnail = show_field_points('Thumbnail');
                                if ($rowListing['BLP_Photo']) {
                                    echo '<div class="points-com">' . $thumbnail . ' pts</div>';
                                    $points_taken = $thumbnail;
                                } else {
                                    echo '<div class="points-uncom">' . $thumbnail . ' pts</div>';
                                }
                                ?>
                            </div>
                        </div>

                        <div class="form-inside-div margin-none border-none">
                            <div class="div_image_library">
                                <span class="daily_browse thumbnail-button" onclick="show_image_library(21<?php echo $key ?>)">Select File</span>
                                <input class="uploadFileName" id="image_name_bank21<?php echo $key ?>" style="display: none;" disabled>
                                <?php if ($rowListing['BLP_Photo'] != "") { ?>
                                    <a class="view-image bottomName" onclick="show_main_image(1<?php echo $key ?>)" >View</a>
                                    <div class="image-pop-up1<?php echo $key ?>" style="display: none;">
                                        <img src="http://<?php echo DOMAIN . IMG_LOC_REL . $rowListing['BLP_Photo'] ?>" width="100%">
                                    </div>
                                <?php } ?>
                                <input type="hidden" name="thumb_photo" id="image_bank21<?php echo $key ?>" value="">
                                <input type="file" class="setting-name" onchange="show_file_name(21<?php echo $key ?>, this)" name="pic[]" id="photo21<?php echo $key ?>" style="display: none;">
                                <a class="deletePhoto margin-left-main" onClick="return confirm('Are you sure?');"  href="customer-listing-thumbnail-photo.php?bl_id=<?php echo $BL_ID ?>&op=del&r_id=<?php echo $reg[0] ?>">Delete Photo</a>
                            </div>
                        </div>
                    </div>
                    <div class="form-inside-div border-none">
                        <div class="button">
                            <input type="submit" name="button" onclick="progress_bar(<?php echo $ib_id ?>)" id="button" value="Save Image" />
                        </div>
                    </div>


                </form>    
                <?php
            }
        }
        ?>
        <div class="form-inside-div border-none main-photos">
            <div class="content-sub-header">
                <div class="title">Image Uploading Tips</div>
            </div>
            <?php
            $help_text = show_help_text('Main Photos');
            if ($help_text != '') {
                echo '<div class="form-inside-div margin-none border-none">' . $help_text . '</div>';
            }
            ?>
        </div>
        <div class="progress-container-main" style="display:none;">
            <div id="progress-container">
                <p class="progress-title">Upload in Progress</p>
                <p>Please leave this page open while your image is uploading</p>
                <div id="progressbox" style="display:none;">
                    <div id="progressbar"></div>
                    <div id="statustxt">0%</div>
                </div>
            </div>
        </div>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>

<?PHP
require_once '../../include/admin/mobile/footer.php';
?>