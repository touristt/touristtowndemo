<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/login.inc.mobile.php';
require_once '../../include/class.Pagination.php';
if (!in_array('customers', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/mobile/");
    exit();
}
if (isset($_REQUEST['button'])) {
    $sql = "SELECT DISTINCT BL_ID, BL_Listing_Title FROM tbl_Business_Listing 
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID WHERE BL_Listing_Title LIKE '%" . encode_strings($_REQUEST['strSearch'], $db) . "%'
            AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY))
            GROUP BY BL_ID ORDER BY BL_Listing_Title";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $count = mysql_num_rows($result);
    $pages = new Paginate(mysql_num_rows($result), 20);
    $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
}
require_once '../../include/admin/mobile/header.php';
?>
<div class="content-left full-width">
    <div class="left">
        <?PHP require '../../include/nav-managecustomers.php'; ?>
    </div>
    <div class="right">
        <form method="get" enctype="multipart/form-data" action="">
            <div class="listing-search-form">
                <div class="label-for-search-form">
                    Search for Listing
                </div>
                <div>
                    <input type="text" type="text" name="strSearch" required>
                </div>
                <div class="button-for-search-form" >
                    <input type="submit" name="button" value="SEARCH NOW">
                </div>
            </div>
        </form>
        <?PHP
        $i = 0;
        if ($count > 0) {
            while ($row = mysql_fetch_assoc($result)) {
                ?>
                <div class="form-inside-div <?php echo ($i == 0) ? 'border-top-listings' : ''; ?>">
                    <div class="menu-item-text-align-inside"><a href="customer-listing-mobile-menu.php?bl_id=<?php echo $row['BL_ID'] ?>"><?php echo $row['BL_Listing_Title'] ?></a>
                    </div>
                </div>

                <?PHP
                $i++;
            }
        }
        ?> 

        <?php
        if (isset($pages)) {
            echo $pages->paginate();
        }
        ?>
    </div>
</div>
<?PHP
require_once '../../include/admin/mobile/footer.php';
?>