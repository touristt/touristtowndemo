<?php
require_once '../../include/config.inc.php';

$title = "Login Screen";

require_once '../../include/admin/mobile/header.php';
?>
<div class="login-outer-body">
    <div class="login-outer-container">
        <div class="login-container">
            <form name="form1" method="post" action="http://<?php echo DOMAIN . INPUT_DIR; ?>mobile/">
                <input name="logop" type="hidden" id="logop" value="login">
                <div class="login-container-inside-div">
                    <?php
                    if (count($_COOKIE) > 0) {
                        $user_email_id = $_COOKIE["admin_email_id"];
                        $user_pas = $_COOKIE["admin_password"];
                    }
                    ?>
                    <div class="login-detail-title-responsive">
                        <div class="signin-title-responsive-page">
                            Sign In
                        </div>
                    </div>
                    <div class="login-from-inside-div margin-top-login-from">
                        <div class="from-inside-div-feild login-user-font-size" >
                            <input name="LOGIN_USER" id="LOGIN_USER" type="text" size="35" value="<?php echo $user_email_id; ?>"/>
                        </div>
                    </div>
                    <div class="login-from-inside-div margin-top-login-from">
                        <div class="from-inside-div-feild login-user-font-size">
                            <input name="LOGIN_PASS" id="LOGIN_PASS" type="password" size="35" value="<?php echo $user_pas; ?>"/>
                        </div>
                    </div>
                    <div class="login-from-inside-div margin-top-login-from">
                        <div class="from-inside-div-feild"><input class="remember_me" name="remember_me"  type="checkbox" /><span class="remeber_text" >Remember Me</span></div>
                    </div>
                    <?php if (isset($_SESSION['login_error']) && $_SESSION['login_error'] == 1) { ?>
                        <div class="login-from-inside-div" >
                            <div class="login_text" colspan="2" style="display:block;" >Your email or password doesn't match what we have on file. Please try again or contact <a href="mailto:info@touristtown.ca">Tourist Town</a> for help.</div>
                        <?php
                    }
                    unset($_SESSION['login_error']);
                    ?>
                    <div class="login-from-inside-div">
                        <div class="from-inside-div-button">
                            <input name="Submit" type="submit" value="Login Now" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
require_once '../../include/admin/mobile/footer.php';
?>