<?PHP
require_once '../../include/config.inc.php';
define("DOMAIN", 'touristtowndemo.com');
echo '<script type="text/javascript">maintain_selection()</script>';
$join = '';
$where = 'WHERE 1=1';
$search_text = '';
$regionFilter = '';
$tempregionFilter = '';
$seasonFilter = '';
$catFilter = '';
$peopleFilter = '';
$ownerFilter = '';
$campaignFilter = '';
if (isset($_GET['region']) && $_GET['region'] != '') {
    $temp = true;
    $RegionType = "";
    $searchParentImage = "";
    foreach (explode(',', $_GET['region']) as $region) {
        if ($temp) {
            $temp = false;
        } else {
            $regionFilter .= ",";
            $tempregionFilter .= ",";
        }
        $sql_region = "SELECT R_ID, R_Parent FROM tbl_Region WHERE R_ID = '$region'";
        $res_region = mysql_query($sql_region);
        $regionObj = mysql_fetch_assoc($res_region);
        if ($regionObj['R_Parent'] == 0) {
            $sql_child = "SELECT R_ID, R_Parent, RM_Child FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Parent WHERE RM_Parent = " . $regionObj['R_ID'];
            $res_child = mysql_query($sql_child);
            $first = true;
            while ($child = mysql_fetch_assoc($res_child)) {
                $RegionType = $child['R_Parent'];
                $RegionID = $child['R_ID'];
                if ($RegionType == 0) {
                    $searchParentImage = " OR FIND_IN_SET($RegionID, IB_Region)";
                }
                if ($first) {
                    $first = false;
                } else {
                    $regionFilter .= ",";
                }
                $regionFilter .= $child['RM_Child'];
            }
        } else {
            $regionFilter .= $region;
        }
        $tempregionFilter .= $region;
    }
    $regionFilter = array_unique(explode(',', $regionFilter));

    //creating where clause using find_in_set to search value in string
    $i = 1;
    $count = count($regionFilter);
    foreach ($regionFilter as $regions) {
        if ($i == 1) {
            $where .= " AND (";
        } else {
            $where .= " OR ";
        }
        $where .= "FIND_IN_SET($regions, IB_Region)";
        if ($i == $count) {
            $where .= "$searchParentImage)";
        } else {
            $i++;
        }
    }
}
if (isset($_GET['season']) && $_GET['season'] != '') {
    $seasonFilter = $_GET['season'];
    $where .= " AND IB_Season IN ($seasonFilter)";
}
if (isset($_GET['cat']) && $_GET['cat'] != '') {
    $catFilter = $_GET['cat'];
    $where .= " AND IB_Category IN ($catFilter)";
}
if (isset($_GET['people']) && $_GET['people'] != '') {
    $peopleFilter = $_GET['people'];
    $where .= " AND IB_People IN ($peopleFilter)";
}
if (isset($_GET['owner']) && $_GET['owner'] != '') {
    $ownerFilter = $_GET['owner'];
    $where .= " AND IB_Owner IN ($ownerFilter)";
}
if (isset($_GET['campaign']) && $_GET['campaign'] != '') {
    $campaignFilter = $_GET['campaign'];
    $where .= " AND IB_Campaign IN ($campaignFilter)";
}
if (isset($_GET['search_image']) && $_GET['search_image'] != '') {
    $join = 'LEFT JOIN tbl_Business_Listing ON FIND_IN_SET(BL_ID, IB_Listings)';
    $search_text = explode(',', $_GET['search_image']);
    $search_word_len = count($search_text);
    foreach ($search_text as $key => $temp) {
        if ($key == 0) {
            $operation = 'AND';
        } else {
            $operation = 'OR';
        }
        if ($count > 0) {
            if ($key == 0) {
                $where .= " AND (";
                $OR = "";
            } else {
                $OR = " OR ";
    }
            $where .= " $OR IB_Keyword LIKE '%" . mysql_real_escape_string($temp) . "%' OR IB_Name LIKE '%" . mysql_real_escape_string($temp) . "%' OR BL_Listing_Title LIKE '%" . mysql_real_escape_string($temp) . "%'";
            if($key + 1 == $search_word_len) {
                $where .= ")";
}

        } else {
            $where .= " $operation IB_Keyword LIKE '%" . mysql_real_escape_string($temp) . "%' OR IB_Name LIKE '%" . mysql_real_escape_string($temp) . "%' OR BL_Listing_Title LIKE '%" . mysql_real_escape_string($temp) . "%' ";
        }
    }
}

if (isset($_REQUEST['pages'])) {
    $lowerLimit = $_REQUEST['lowLimit'];
    $upperLimit = $_REQUEST['upLimit'];
    $currentPage = $_REQUEST['current'];
    $sql = "SELECT IB_ID, IB_Path, IB_Thumbnail_Path, IB_Dimension FROM tbl_Image_Bank 
            $join $where GROUP BY IB_ID ORDER BY IB_ID DESC LIMIT $lowerLimit, $upperLimit";
} else {
    $currentPage = '1';
    $lowerLimit = '0';
    $upperLimit = '100';
    $sql = "SELECT IB_ID, IB_Path, IB_Thumbnail_Path, IB_Dimension FROM tbl_Image_Bank 
            $join $where GROUP BY IB_ID ORDER BY IB_ID DESC LIMIT $lowerLimit, $upperLimit";
}
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$count = mysql_num_rows($result);

$sql_page = "SELECT COUNT(*) AS IB_Total_IMG FROM tbl_Image_Bank 
            LEFT JOIN tbl_Business_Listing ON FIND_IN_SET(BL_ID, IB_Listings)
            $where GROUP BY IB_ID ORDER BY IB_ID DESC";
$res_page = mysql_query($sql_page);
$count_row = mysql_num_rows($res_page);

if ($count > 0) {
    while ($row = mysql_fetch_array($result)) {
        $image_id = (isset($_REQUEST['image_id']) ? $_REQUEST['image_id'] : "0");

        $content .= '<div id="bgcolor_'.$row['IB_ID'].'" onclick="select_the_image(`' . $row['IB_Path'] .'`,`'. $row['IB_ID'].'`)" class="image-hover-bank image-bank-image-section" style="border: 1px solid #fff;cursor: default;">
                    <a style="top: 0px;right: 0px;color: #FFF;"  class="image-section-delete" onclick="return Delete_Images(' . $row['IB_ID']. ')">X</a>
                     <div class="image-section-image-align">
                     <img src="http://' . DOMAIN . IMG_BANK_REL . $row['IB_Thumbnail_Path'] . '">
                     </div>
                     <div style="text-align:center;margin-top: 2px;">'. $row['IB_ID'] . ' - ' . $row['IB_Dimension'] . '</div>
                     </div>';
    }
} else {
    $content .= '<div class="no-image-found">No images found in image bank.</div>';
}
if ($count_row > 100) {
    $pages = ceil($count_row / 100);

    $content .= '<div class="image-bank-pagination">
            <div class="pagination clearfix">';
    $i = 1;
    for ($x = 1; $x <= $pages; $x++) {
        if ($i == 1) {

            $content .= '<div class="pull-left">Page ' . $currentPage . ' of ' . $pages . ' </div>
                        <div class="pull-right">';

            $lowerLimit = 0;
            $upperLimit = 100;
        } else {
            $lowerLimit = $upperLimit;
            $upperLimit = $upperLimit + 100;
        }
        $i++;
        if ($currentPage == $x) {
            $content .= '<a onclick="pagination(`' . $lowerLimit . '`, `100`, `' . $x . '`, `' . $image_id . '`, `' . $_REQUEST['iteration'] . '`, `show_image_bank_ajax` )" class="page page-active" style="cursor: pointer">' . $x . '</a>';
        } else {
            $content .= '<a onclick="pagination(`' . $lowerLimit . '`, `100`, `' . $x . '`, `' . $image_id . '`, `' . $_REQUEST['iteration'] . '`, `show_image_bank_ajax` )" class="page" style="cursor: pointer">' . $x . '</a>';
        }
        if ($i == $pages + 1) {
            $content .= '</div>';
        }
    }

    $content .= '</div>
        </div>';
}

print $content;