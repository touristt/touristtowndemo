<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/login.inc.mobile.php';
require_once '../../include/class.Pagination.php';
if (!in_array('customers', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/mobile/");
    exit();
}
if (isset($_REQUEST['button'])) {
    $sql = "SELECT * FROM tbl_Business WHERE B_Email = '" . encode_strings($_REQUEST['email'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $count = mysql_num_rows($result);
}

require_once '../../include/admin/mobile/header.php';
?>
<div class="content-left full-width">
    <div class="left">
        <?PHP
        require '../../include/nav-managecustomers.php';
        ?>
    </div>
    <div class="right">
        <form method="get" enctype="multipart/form-data" action="">
            <div class="listing-search-form">
                <div class="label-for-search-form">
                    Search Customer
                </div>
                <div>
                    <input type="email"  name="email" required>
                </div>
                <div class="button-for-search-form" >
                    <input type="submit" name="button" value="SEARCH NOW">
                </div>
            </div>
        </form>
        <?PHP
        $i = 0;
        if ($count > 0) {
            while ($row = mysql_fetch_assoc($result)) {
                ?>
                <div class="form-inside-div <?php echo ($i == 0) ? 'border-top-listings' : ''; ?>">
                    <div class="menu-item-text-align-inside"><a href="customer-listings.php?id=<?php echo $row['B_ID'] ?>"><?php echo $row['B_Email'] ?></a>
                    </div>
                </div>

                <?PHP
                $i++;
            }
        } elseif($_REQUEST['email'] != '' && $count == 0) {
            ?> 
            <div class="form-inside-div border-top-listings">
                <div class="menu-item-text-align-inside">Email does not exist.</div>
            </div>
            <?php
        }
        if (isset($pages)) {
            echo $pages->paginate();
        }
        ?>
    </div>
</div>
<?PHP
require_once '../../include/admin/mobile/footer.php';
?>