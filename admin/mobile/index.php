<?PHP
require_once '../../include/config.inc.php';
require_once '../../include/login.inc.mobile.php';

if (isset($_SESSION['USER_ID']) && $_SESSION['USER_ID'] > 0) {
    header("Location: " . INPUT_DIR . "mobile/admin-mobile-menu.php");
    exit;
}
?>