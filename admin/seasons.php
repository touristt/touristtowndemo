<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('superadmin', $_SESSION['USER_ROLES'])) {
    header("Location: /admin/");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Seasons</div>
    </div>
    <div class="left">
        <div class="sub-menu">
            <ul>
                <li class="addlisting region-sub"><a href="season.php">+Add Season</a></li>
            </ul>
        </div>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name-store">Season</div>
            <div class="data-column padding-none spl-other"></div>
            <div class="data-column padding-none spl-other">Edit</div>
            <div class="data-column padding-none spl-other">Delete</div>
        </div>
        <?PHP
        $sql = "SELECT * FROM tbl_Seasons ORDER BY S_ID";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        if (mysql_num_rows($result)) {
            while ($row = mysql_fetch_assoc($result)) {
                ?>
                <div class="data-content">
                    <div class="data-column spl-name-store"><?= $row['S_Name'] ?></div>
                    <div class="data-column spl-other"></div>
                    <div class="data-column spl-other"><a href="season.php?id=<?= $row['S_ID'] ?>">Edit</a></div>
                    <div class="data-column spl-other"><a onClick="return confirm('Are you sure?\nThis action CANNOT be undone!');" href="season.php?op=del&amp;id=<?= $row['S_ID'] ?>">Delete</a></div>
                </div>
                <?PHP
            }
        } else {
            ?>
            <div class="data-content">
                <div class="data-column spl-name">You have not added seasons yet.</div>
            </div>
            <?PHP
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>