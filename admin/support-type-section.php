<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if (!in_array('manage-support', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['sid'] > 0) {
    $sql = "SELECT S_ID, S_Name, S_PDF, S_Video, S_Thumbnail, S_Description FROM tbl_Support WHERE S_ID = '" . encode_strings($_REQUEST['sid'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del_thumb') {
    require_once '../include/picUpload.inc.php';
    $update = "UPDATE tbl_Support SET";
    $update .= " S_Thumbnail = ''";
    if ($row['S_Thumbnail']) {
        Delete_Pic(IMG_LOC_ABS . $row['S_Thumbnail']);
    }

    $update .= " WHERE S_ID = '" . $_REQUEST['sid'] . "'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['sid'];
        Track_Data_Entry('Support', '', 'Add Support Item', $id, 'Delete Thumbnail', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: support-type-section.php?id=" . $_REQUEST['support_id'] . "&sid=" . $_REQUEST['sid']);
    exit;
}

// CODE TO DELETE PDF FILE
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $select_pdf = "SELECT S_PDF FROM tbl_Support WHERE S_ID = " . $_REQUEST['delete'] . " ";
    $result_pdf = mysql_query($select_pdf);
    $pdf_row = mysql_fetch_assoc($result_pdf);
    if ($pdf_row['S_PDF'] != "") {
        unlink(PDF_LOC_ABS . $pdf_row['S_PDF']);
    }
    $sql = "UPDATE tbl_Support SET S_PDF = '' WHERE S_ID = '" . $_REQUEST['delete'] . "'";
    $result = mysql_query($sql);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['delete'];
        Track_Data_Entry('Support', '', 'Add Support Item', $id, 'Delete PDF', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: support-type-section.php?id=" . $_REQUEST['support_id'] . "&sid=" . $_REQUEST['delete']);
    exit;
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $sql = "tbl_Support SET 
            S_Name = '" . encode_strings($_REQUEST['name'], $db) . "',
            S_SS_ID = '" . encode_strings($_REQUEST['support_id'], $db) . "',
            S_Video = '" . encode_strings($_REQUEST['video_url'], $db) . "',
            S_Description = '" . encode_strings($_REQUEST['description'], $db) . "'";

    $random_digit = rand(0000, 9999);
    $name = str_replace(' ', '_', $_FILES['file']['name']);
    $new_file_name = $random_digit . $name;
    $filePath = IMG_LOC_ABS . $new_file_name;
    if ($name != "") {
        if ($_FILES["file"]["type"] == "application/pdf") {
            move_uploaded_file($_FILES["file"]["tmp_name"], $filePath);
            $sql .= ", S_PDF = '" . $new_file_name . "'";
        } else {
            $_SESSION['pdferror'] = 1;
            header("Location: support-type-section.php?id=" . $_REQUEST['support_id'] . "&sid=" . $row['S_ID']);
            exit();
        }
    }

    require '../include/picUpload.inc.php';
    if ($_POST['image_bank_pic'] == "") {
        // last @param 16 = Support Type Image
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 16);
        if (is_array($pic)) {
            $sql .= ", S_Thumbnail = '" . encode_strings($pic['0']['0'], $db) . "'";
        }
        $pic_id = $pic['1'];
    } else {
        $pic_id = $_POST['image_bank_pic'];
        // last @param 16 = Support Type Image
        $pic_response = Upload_Pic_Library($pic_id, 16);
        if ($pic_response) {
            $sql .= ", S_Thumbnail = '" . encode_strings($pic_response['0'], $db) . "'";
        }
    }
    if ($row['S_ID'] > 0) {
        $sql = "UPDATE " . $sql . " WHERE S_ID = '" . encode_strings($_REQUEST['sid'], $db) . "'";
        $result = mysql_query($sql, $db);
        $id = $row['S_ID'];
        // TRACK DATA ENTRY
        $id = $_REQUEST['sid'];
        Track_Data_Entry('Support', '', 'Add Support Item', $id, 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Support', '', 'Add Support Item', $id, 'Add', 'super admin');
    }

    if ($result) {
        $_SESSION['success'] = 1;
        if ($pic_id > 0) {
            //Image usage from image bank.
            imageBankUsage($pic_id, 'IBU_S_ID', $id, '', '');
        }
    } else {
        $_SESSION['error'] = 1;
    }

    if ($row['S_ID'] > 0) {
        $id = $_REQUEST['support_id'];
        header("Location: support-type-section.php?id=$id&sid=" . $row['S_ID']);
    } else {
        $id1 = $_REQUEST['id'];
        header("Location: support-type.php?id=" . $id1);
    }
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Support</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-support.php'; ?>
    </div>
    <div class="right">
        <form name="form1" method="post" action="" enctype="multipart/form-data">
            <input type="hidden" name="op" value="save">
            <div class="content-header">Add Support Item</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Name</label>
                <div class="form-data">
                    <input name="name" type="text" size="50" value="<?php echo $row['S_Name'] ?>" required/>
                    <input name="support_id" type="hidden" value="<?php echo $_REQUEST['id']; ?>"/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>PDF Document</label>
                <div class="form-data">
                    <div class="inputWrapper adv-photo float-left">Browse
                        <input class="fileInput" type="file" id="file-name-pdf" name="file" onchange="show_file_name(this.files[0].name, 'uploadFile')"/>
                    </div>
                    <input style="display: none;" id="uploadFile" class="uploadFileName" disabled>
                    <div class="pdf-name-support">
                        <?php
                        if ($row['S_PDF'] != '') {
                            header('Content-type: application/pdf');
                            header('Content-Disposition: attachment; filename="' . $row['S_PDF'] . '"');
                            ?>
                            <a href="http://<?php echo DOMAIN . IMG_LOC_REL . $row['S_PDF'] ?>" target="_blank">View</a>
                        </div>
                        <div class="margin-left-resize theme_browse_float_view_support">
                            <a class="delete-pointer" onClick="return confirm('Are you sure this action can not be undone!');" href="support-type-section.php?delete=<?php echo $row['S_ID'] ?>&op=del&support_id=<?php echo $_REQUEST['id']; ?>">Delete</a> 
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Video URL</label>
                <div class="form-data">
                    <input name="video_url" type="text" size="50" value="<?php echo $row['S_Video'] ?>"/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Video Thumbnail</label>
                <div class="form-data div_image_library cat-region-width">
                    <?php if ($row['S_Thumbnail'] != '') { ?>
                        <a class="deletePhoto delete-region-cat-photo" style="margin-top: 10px;" onclick="return confirm('Are you sure you want to delete photo?');" href="support-type-section.php?sid=<?php echo $row['S_ID'] ?>&support_id=<?php echo $_REQUEST['id']; ?>&op=del_thumb">Delete Photo</a>
                    <?php } ?>
                    <div class="cropit-image-preview aboutus-photo-perview">
                        <img class="preview-img preview-img-script16" style="display: none;" src="">    
                        <?php if ($row['S_Thumbnail'] != '') { ?>
                            <img class="existing-img existing_imgs16" src="<?php echo IMG_LOC_REL . $row['S_Thumbnail'] ?>" >
                        <?php } ?>
                    </div>
                    <span class="daily_browse" onclick="show_image_library(16)">Select File</span>
                    <input type="hidden" name="image_bank_pic" id="image_bank16" value="">
                    <input type="file" onchange="show_file_name(16, this)" name="pic[]" id="photo16" style="display: none;">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Description</label>
                <div class="form-data">
                    <textarea name="description" cols="85" rows="10" class="tt-description"><?php echo $row['S_Description'] ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="Submit" class="export" value="Save"/>
                </div>
            </div>
        </form>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>
<script>
    function show_file_name(val, id) {
        document.getElementById(id).value = val;
        document.getElementById(id).style.display = "block";
    }
</script>
<?PHP
require_once '../include/admin/footer.php';
?>