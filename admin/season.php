<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('superadmin', $_SESSION['USER_ROLES'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['id'] > 0) {
    $sql = "SELECT * FROM tbl_Seasons WHERE S_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
}

if ($_REQUEST['op'] == 'save') {
    $sql = "tbl_Seasons SET 
            S_Name = '" . encode_strings($_REQUEST['name'], $db) . "'";

    if ($row['S_ID'] > 0) {
        $sql = "UPDATE " . $sql . " WHERE S_ID = '" . $row['S_ID'] . "'";
        // TRACK DATA ENTRY
        Track_Data_Entry('Season', '', 'Manage Seasons', '', 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
        // TRACK DATA ENTRY
        Track_Data_Entry('Season', '', 'Manage Seasons', '', 'Add', 'super admin');
    }
    $result = mysql_query($sql, $db);

    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: seasons.php");
    exit();
}
if ($_REQUEST['op'] == 'del') {
    $sql = "DELETE tbl_Seasons FROM tbl_Seasons
            WHERE S_ID = '" . $row['S_ID'] . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Season', '', 'Manage Seasons', '', 'Delete', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }

    header("Location: seasons.php");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Seasons</div>
        <div class="addlisting"></div>
    </div>
    
    <div class="right full-width">
        <form name="form1" method="post" action="season.php">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="id" value="<?php echo $row['S_ID'] ?>">
            <div class="content-header full-width">Season Details</div>
            <div class="form-inside-div form-inside-div-width-admin full-width">
                <label>Season Title</label>
                <div class="form-data">
                    <input name="name" type="text" value="<?php echo $row['S_Name'] ?>" size="40" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin full-width"> 
                <div class="button">
                    <input type="submit" name="button" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>