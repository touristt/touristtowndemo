<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

$regionLimit = '0';
$regionLimitCommaSeparated = '';
if (!in_array('manage-events', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
} elseif (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
    $limits = explode(',', $_SESSION['USER_LIMIT']);
    $regionLimit = array();
    foreach ($limits as $limit) {
        $sql = "SELECT R_ID, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $region = mysql_fetch_assoc($result);
        if (isset($region['R_Type']) && $region['R_Type'] == 1) {
            if ($region['R_ID'] != '' && !in_array($region['R_ID'], $regionLimit)) {
                $regionLimit[] = $region['R_ID'];
            }
        } else {
            $sql = "SELECT RM_Parent FROM tbl_Region_Multiple WHERE RM_Child = '" . encode_strings($region['R_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                if ($row['RM_Parent'] != '' && !in_array($row['RM_Parent'], $regionLimit)) {
                    $regionLimit[] = $row['RM_Parent'];
                }
            }
        }
    }
}
$regionLimitCommaSeparated = rtrim(implode(',', $regionLimit), ',');



if ($_REQUEST['id'] > 0) {
    $sql = "SELECT * FROM Events_Towns WHERE ET_ID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
}

if ($_POST['op'] == 'save') {

    $sql = "Events_Towns SET 
            ET_Region_ID = '" . encode_strings($_REQUEST['rID'], $db) . "', 
            ET_Town = '" . encode_strings($_REQUEST['name'], $db) . "'";

    if ($_REQUEST['id'] > 0) {
        $sql = "UPDATE " . $sql . " WHERE ET_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    } else {
        $sql = "INSERT " . $sql;
    }
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: events-communities.php");
    exit();
} elseif ($_GET['op'] == 'del') {

    $sql = "DELETE Events_Towns FROM Events_Towns WHERE ET_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }

    header("Location: events-communities.php");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Events Communities</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manageevents.php'; ?>
    </div>
    <div class="right">
        <form name="form1" method="post" action="/admin/events-community.php">

            <input type="hidden" name="id" value="<?php echo $row['ET_ID'] ?>">
            <input type="hidden" name="op" value="save">
            <?PHP if ($regionLimitCommaSeparated != '') { ?><input type="hidden" name="rID" value="<?php echo $regionLimitCommaSeparated ?>"><?PHP } ?>
            <div class="content-header">Community Information</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Community Name</label>
                <div class="form-data">
                    <input name="name" type="text" id="Bname" value="<?php echo $row['ET_Town'] ?>" size="50" required/>
                </div>
            </div>
            <?PHP if ($regionLimit == 0) { ?>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Region</label>
                    <div class="form-data">
                        <select name="rID" id="rID"><option value="0">Select One</option>
                            <?PHP
                            $sql = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Parent = 0 ORDER BY R_Name";
                            $resultRegionList = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($rowRList = mysql_fetch_assoc($resultRegionList)) {
                                echo "<option value='" . $rowRList['R_ID'] . "'";
                                echo $rowRList['R_ID'] == $row['ET_Region_ID'] ? 'selected' : '';
                                echo ">" . $rowRList['R_Name'] . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                </div>
            <?PHP } ?>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="button2" id="button2" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>