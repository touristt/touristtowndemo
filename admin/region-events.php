<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';
if (!in_array('regions', $_SESSION['USER_PERMISSIONS']) && !in_array('manage-county-region', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
    $sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
    $sql = "SELECT EventID FROM  Events_master where FIND_IN_SET($regionID, E_Region_ID)";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($row = mysql_fetch_assoc($result)) {
        $events[] = $row['EventID'];
    }
}

if (isset($_REQUEST['del']) && $_REQUEST['del'] == 'true') {
    $r_id = $_REQUEST['r_id'];
    if (isset($_REQUEST['event_id']) && isset($_REQUEST['r_id']) && isset($_REQUEST['bit'])) {
        $sql = "SELECT E_Region_ID FROM Events_master WHERE EventID = '" . $_REQUEST['event_id'] . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $Event = mysql_fetch_assoc($result);
        $array1 = array($_REQUEST['r_id']);
        $array2 = explode(',', $Event['E_Region_ID']);
        $array3 = array_diff($array2, $array1);
        $regions = implode(',', $array3);
        $sql = "UPDATE Events_master SET E_Region_ID = '" . $regions . "' WHERE EventID = '" . encode_strings($_REQUEST['event_id'], $db) . "'";
        $result = mysql_query($sql) or die(mysql_error());
        $_SESSION['event_delete'] = 1;
        header("Location: /admin/region-events.php?rid=" . $r_id);
        exit();
    }
}

if ($_REQUEST['rid'] > 0 && $_REQUEST['events'] > 0) {
    $r_id = $_REQUEST['rid'];
    $sql = "SELECT E_Region_ID FROM Events_master WHERE EventID = '" . $_REQUEST['events'] . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $Event = mysql_fetch_assoc($result);
    $regions = $Event['E_Region_ID'] . "," . $_REQUEST['rid'];

    $return['success'] = 1;
    $sql = "UPDATE Events_master SET E_Region_ID = '" . $regions . "' WHERE EventID = '" . encode_strings($_REQUEST['events'], $db) . "'";
    $result = mysql_query($sql) or die(mysql_error());

    $_SESSION['event_success'] = 1;
    if ($page > 0) {
        header("Location: /admin/region-events.php?rid=" . $r_id . "&page=" . $page);
    } else {
        header("Location: /admin/region-events.php?rid=" . $r_id);
    }

    exit();
}

if (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
    $limits = explode(',', $_SESSION['USER_LIMIT']);
    $regionLimit = array();
    foreach ($limits as $limit) {
        $sql = "SELECT R_ID FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            if ($row['R_ID'] != '' && !in_array($row['R_ID'], $regionLimit)) {
                $regionLimit[] = $row['R_ID'];
            }
        }
    }
}
if ($activeRegion['R_Type'] != 4 && !in_array($regionID, $regionLimit)) {
    header("Location: /admin/regions.php");
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Events</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <div class="content-sub-header link-header region-header-padding">
            <div class="data-column spl-name padding-none">Select Events for <?php echo $activeRegion['R_Name'] ?>
            </div>
        </div>
        <form name="form1" method="GET" id="events_form">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <div class="content-sub-header link-header region-header-padding">
                <div class="data-column spl-org-listngs padding-org-listing"><span class="addlisting font_weight_normal" >Add New Event: </span><br>
                    <input style="width: 125px;" class="datepicker event-search-date-input" onchange="filter_events(this.value, $('#enddate').val(), $('#sortby').val(), $('#sortbyregion').val())" name="startdate" type="text" placeholder="Start date" id="startdate" value="<?php echo $_REQUEST['startdate'] ?>" size="50" />
                    <input style="width: 125px;" class="datepicker event-search-date-input" onchange="filter_events($('#startdate').val(), this.value, $('#sortby').val(), $('#sortbyregion').val())" name="enddate" type="text" id="enddate" placeholder="End date" value="<?php echo $_REQUEST['enddate'] ?>" size="50" />
                    <select style="width: 125px;" name="sortby" id="sortby" onchange="filter_events($('#startdate').val(), $('#enddate').val(), this.value, $('#sortbyregion').val())"> 
                        <option  selected>Sort by:</option>
                        <option value="oldest" <?php echo ($_REQUEST['sortby'] == "oldest") ? 'selected' : ''; ?>>Date Oldest First</option>
                        <option value="newest" <?php echo ($_REQUEST['sortby'] == "newest") ? 'selected' : ''; ?>>Date Newest First</option>
                    </select>
                    <select style="width: 125px;" name="sortbyregion" id="sortbyregion" onchange="filter_events($('#startdate').val(), $('#enddate').val(), $('#sortby').val(), this.value)">
                        <option selected>Select Region:</option>
                        <?PHP
                        $sqlCom = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Parent != 0 ORDER BY R_Name";
                        $resultRegionList = mysql_query($sqlCom, $db) or die("Invalid query: $sqlCom -- " . mysql_error());
                        while ($rowRList = mysql_fetch_assoc($resultRegionList)) {
                            ?>
                            <option value="<?php echo $rowRList['R_ID'] ?>" <?php echo ($_REQUEST['sortbyregion'] == $rowRList['R_ID']) ? 'selected' : ''; ?>><?php echo $rowRList['R_Name'] ?></option>
                        <?php } ?>
                    </select>
                    <select style="width: 160px;" class="sort-listing-org-width" name="events" id="events_select">
                        <option value="">Select Events</option>
                    </select>
                    <input type="submit" value="+Add" class="stories-add-button" />
                </div>
            </div>
        </form>
        <div class="data-content" >
            <div class="content-sub-header">
                <div class="data-column spl-org-listngs">
                    <div class="select-listings-nearby-title events-name">Name</div>
                    <div class="select-listings-nearby-title events">
                        Community
                    </div>
                </div>
            </div>
        </div>
        <div id="reorder-category">

            <?PHP
            $sql = "SELECT DISTINCT Title, EventID FROM Events_master WHERE Pending = 0 AND EventDateEnd >= CURDATE() And FIND_IN_SET (" . encode_strings($_REQUEST['rid'], $db) . ",E_Region_ID)";
            if (strlen($_REQUEST['strSearch']) > 3) {
                $sql .= " And Title LIKE '%" . encode_strings($_REQUEST['strSearch'], $db) . "%' ";
            }
            // show latest 30 entries to county admin
            if ($_SESSION['USER_LIMIT'] != '') {
                $sql .= "ORDER BY EventID DESC LIMIT 30";
            } else {
                if ($_REQUEST['sortby'] == 'oldest') {
                    $sql .= "ORDER BY EventDateStart ASC";
                } else if ($_REQUEST['sortby'] == 'newest') {
                    $sql .= "ORDER BY EventDateStart DESC";
                } else {
                    $sql .= "ORDER BY EventDateStart DESC";
                }
            }
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            // show latest 30 entries to county admin
            if ($_SESSION['USER_LIMIT'] == '') {
                $pages = new Paginate(mysql_num_rows($result), 30);
                $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
            }
            ?>

            <?php
            while ($row = mysql_fetch_assoc($result)) {
                ?>
                <div class="data-content" >
                    <div class="data-column spl-org-listngs">
                        <div class="select-listings-nearby-title events">
                            <?php echo $row['Title'] ?>
                        </div>
                        <div class="select-listings-nearby-title events">
                            <?php
                            $query = mysql_query("SELECT * FROM Events_master WHERE EventID = '" . $row['EventID'] . "'");
                            $row_check = mysql_fetch_assoc($query);
                            $regions = explode(',', $row_check['E_Region_ID']);
                            $counts = count($regions);
                            $i = 1;
                            foreach ($regions as $key => $region) {
                                $query = mysql_query("SELECT R_Name FROM tbl_Region WHERE R_ID = '" . $region . "'");
                                $rows = mysql_fetch_assoc($query);
                                if ($i !== $counts) {
                                    echo $rows['R_Name'] . ', ';
                                } else {
                                    echo $rows['R_Name'];
                                }
                                $i++;
                            }
                            ?>
                            
                        </div>
                        <div class="remove-link"><a href="region-events.php?r_id=<?php echo $regionID; ?>&event_id=<?php echo $row['EventID']; ?>&del=true&bit=1" onclick="return confirm('Are you sure you want to perform this action?');">Remove</a></div>

                    </div>
                </div>
                <?PHP
            }
            if (isset($pages)) {
                echo $pages->paginate();
            }
            ?>
        </div>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script>
    function filter_events(startdate, enddate, sortby, region) {
        var c_region = '<?php echo $regionID ?>';
        $.ajax({
            type: "POST",
            url: 'filterEvents.php',
            dataType: 'json',
            data: {
                startdate: startdate,
                enddate: enddate,
                sortby: sortby,
                region: region,
                c_region: c_region
            },
            success: function (response) {
                $('#events_select').html(response.event);

            }

        });

    }
</script>