<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';

if (!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS']) || $_SESSION['USER_SHOW_BUSINESSES'] != 0) {
    header("Location: /admin/");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Requests - Completed Requests</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?php require_once '../include/nav-manage-request.php'; ?>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column rc-titles padding-none">Listing Name</div>
            <div class="data-column rc-titles padding-none">Submitted by</div>
            <div class="data-column rc-date padding-none">Date</div>
            <div class="data-column rc-other padding-none">View</div>
        </div>
        <?PHP
        $sql = "SELECT BL_Listing_Title, RCC_ID, B_Email, RCC_Created_Date, U_Email FROM tbl_Request_Content_Change INNER JOIN tbl_Business_Listing ON BL_ID = RCC_BL_ID
                LEFT JOIN tbl_Business ON RCC_User_ID = B_ID 
                LEFT JOIN tbl_User ON RCC_User_ID = U_ID WHERE RCC_Status = 1";
        if(!in_array('superadmin', $_SESSION['USER_ROLES']) && in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS']))
        {
            $sql .= " and RCC_User_ID = " .$_SESSION['USER_ID'];
        }
        $sql .=" order by RCC_ID ASC";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $pages = new Paginate(mysql_num_rows($result), 100);
        $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            ?>
            <div class="data-content">
                <div class="data-column rc-titles">
                    <?php echo $row['BL_Listing_Title'] ?>
                </div>
                <div class="data-column rc-titles">
                    <?php echo ($row['B_Email'] != '') ? $row['B_Email'] : $row['U_Email'] ?>
                </div>
                <div class="data-column rc-date">
                    <?php echo ($row['RCC_Created_Date'] != '0000-00-00') ? $row['RCC_Created_Date'] : 'N/A' ?>
                </div>
                <div class="data-column rc-other">
                    <a href="request_content_detail.php?id=<?php echo $row['RCC_ID'] ?>">View</a>
                </div>
            </div>
            <?PHP
        }
        ?>
        <?php
        if (isset($pages)) {
            echo $pages->paginate();
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>