<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';
require_once '../include/track-data-entry.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS']) && !in_array('manage-county-region', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $S_ID = $_REQUEST['id'];
    $sql = "DELETE FROM tbl_Story_Region WHERE SR_S_ID = '" . $S_ID . "' AND SR_R_ID = '" . $regionID . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Stories', '', 'Manage Stories', $S_ID, 'Delete Story', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: region-stories.php?rid=" . $regionID);
    exit();
}
//Get Active Region Information
$sql = "SELECT R_Type, R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">

    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Manage Stories</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">
            <div class="title">Manage Stories</div>
            <div class="link">
                <div class="add-link"><a href="story.php" target="_blank">+Add Story</a></div>
            </div>
        </div>
        <form name="form1" method="GET" id="feature_form">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <input type="hidden" name="type" value="overall_story">
            <input type="hidden" name="op" value="save">
            <div class="content-sub-header link-header region-header-padding">
                <div class="data-column spl-org-listngs padding-org-listing">
                    <select name="category" id="feature_category" class="stories-cat" onchange="filterStories('feature_form');" required>
                        <option value="">Select Category</option>
                        <?PHP
                        $sql = "SELECT C_ID FROM tbl_Region_Category 
                                LEFT JOIN tbl_Category ON C_ID = RC_C_ID
                                WHERE C_Parent = 0 AND C_Is_Blog = 1 AND RC_R_ID = '" . encode_strings($regionID, $db) . "'";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($row = mysql_fetch_assoc($result)) {
                            $sql1 = "SELECT C_ID, C_Name FROM tbl_Region_Category
                                     LEFT JOIN tbl_Category ON RC_C_ID = C_ID AND RC_R_ID = '" . encode_strings($regionID, $db) . "'
                                     WHERE C_Parent = '" . $row['C_ID'] . "' AND RC_Status=0 ORDER BY RC_Name ASC";
                            $result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
                            while ($row1 = mysql_fetch_assoc($result1)) {
                                ?>
                                <option value="<?php echo $row1['C_ID'] ?>" <?php echo ($row1['C_ID'] == $feature_category) ? 'selected' : '' ?>><?php echo $row1['C_Name'] ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                    <select name="story" id="overall_story" class="stories" required>
                        <option value="">Select Story</option>
                        <?PHP
                        if (isset($feature_category) && $feature_category > 0) {
                            $sql = "SELECT S_ID, S_Title  FROM tbl_Story WHERE S_Category = '" . encode_strings($feature_category, $db) . "'  
                                    AND S_ID NOT IN (SELECT SHC_S_ID FROM tbl_Story_Homepage_Category WHERE SHC_R_ID = '" . encode_strings($regionID, $db) . "') ORDER BY S_Title";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($row = mysql_fetch_assoc($result)) {
                                ?>
                                <option value="<?php echo $row['S_ID'] ?>"><?php echo $row['S_Title'] ?></option>
                                <?PHP
                            }
                        }
                        ?>
                    </select>
                    <input type="submit" value="+Add" class="stories-add-button" onclick="return saveStory('feature_form');" />
                </div>
            </div>
        </form>
        <div class="reorder-category overall_story">
            <?PHP
            $sql = "SELECT DISTINCT tbl_Story.* FROM tbl_Story LEFT JOIN tbl_Story_Region ON S_ID = SR_S_ID WHERE SR_R_ID = '" . $regionID . "'  ORDER BY S_Title ASC";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                 if (isset($row['S_Active']) && $row['S_Active'] == 1) {
                    $checked = "checked";
                } else {
                    $checked = "";
                }
                ?>
                <div class="data-content" id="recordsArray_<?php echo $row['_ID'] ?>">
                    <div class="data-column spl-org-listngs ">
                        <div class="story-title"><?php echo $row['S_Title'] ?></div>
                        
                        <div class="remove-link">
                            <a style="margin-right: 15px;" href="story.php?id=<?php echo $row['S_ID'] ?>" target="_blank">Edit</a>
                            <a onClick="return confirm('Are you sure?\nThis action CANNOT be undone!');" href="region-stories.php?op=del&amp;id=<?php echo $row['S_ID'] ?>&rid=<?php echo $regionID ?>">Remove</a>
                        </div>
                        <div class="remove-link">
                            <input type="checkbox" name="display" id="display<?php echo $row['S_ID']; ?>" <?php echo $checked; ?> onclick=" return hide_show_stories(<?php echo $row['S_ID']; ?>);">
                        </div>
                    </div>
                </div>
                <?PHP
            }
            ?>
        </div>
    </div>
</div>
<script>
    ///Avtive and InActive function
     function hide_show_stories(id) {
        var value = document.getElementById('display' + id).checked;
        $.ajax({
            type: "POST",
            url: 'show_hide_story.php',
            dataType: 'json',
            data: {
                S_ID: id,
                show_hide: value
            },
            success: function (response) {
                if (response == 0) {
                    swal("Story Deactivated", "Story has been deactivated successfully.", "success");
                } else if(response == 1) {
                    swal("Story Activated", "Story has been activated successfully.", "success");
                }else{
                    swal("Error", "Error saving data. Please try again.", "error");
                }
            }
        });
    }
    //filter listing
    function filterStories(form) {
        var formdata = $('#' + form).serialize();
        $.ajax({
            type: "POST",
            url: 'filterStories.php',
            dataType: 'json',
            data: formdata,
            success: function (response) {
                var type = response.type;
                if (response.stories && response.stories !== '') {
                    $('#' + type).html(response.stories);
                }
            }
        });
    }
    //save listing against route
    function saveStory(form) {
        var formdata = $('#' + form).serialize();
        $.ajax({
            type: "POST",
            url: 'saveStory.php',
            dataType: 'json',
            data: formdata,
            success: function (response) {
                filterStories(form);
                if (response.success && response.success == 1) {
                    swal("Story Added", "Story has been added successfully.", "success");
                }
                if (response.error && response.error == 1) {
                    swal("Error", "Error adding stories. Please try again.", "error");
                }
                if (response.stories && response.stories !== '') {
                    $('.reorder-category.' + response.type).html(response.stories);
                }

                //clear selection of dropdowns
                $('#overall_story').val('');
            }
        });
        return false;
    }
</script>
<?PHP
require_once '../include/admin/footer.php';
?>
