<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
if (isset($_REQUEST['rid']) && $_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
    $sql = "SELECT * FROM tbl_Region 
        WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion1 = mysql_fetch_assoc($result);

    $sql = "SELECT * FROM tbl_Homepage_Paintings
        WHERE HP_R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
    $count = mysql_num_rows($result);
} else {
    $regionID = 0;
}
if (isset($_POST['op']) && $_POST['op'] == 'save') {
    require_once '../include/picUpload.inc.php';
    if ($count > 0) {
        //////////English Data Update
        $sql_Eng = "tbl_Homepage_Paintings SET 
        HP_Link= '" . encode_strings($_REQUEST['link'], $db) . "',
        HP_Title= '" . encode_strings($_REQUEST['title'], $db) . "',
        HP_Description= '" . encode_strings($_REQUEST['description'], $db) . "'";
        $pic1 = Upload_Pic('0', 'pic1', 0, 0, true, IMG_LOC_ABS, 10000000, true, 36, 'Homepage Painting');
        if (is_array($pic1)) {
            $sql_Eng .= ", HP_Homepage_Painting = '" . encode_strings($pic1['0']['0'], $db) . "',
                    HP_Homepage_Painting_Mobile = '" . encode_strings($pic1['0']['1'], $db) . "'";
        }
        $sql_Eng = "UPDATE " . $sql_Eng . " WHERE HP_R_ID = '" . encode_strings($regionID, $db) . "'";
        $result = mysql_query($sql_Eng, $db) or die("Invalid query: $sql_Eng -- " . mysql_error());
    } else {
        //////////English Data
        $sql_Eng_Insert = "tbl_Homepage_Paintings SET 
        HP_Link= '" . encode_strings($_REQUEST['link'], $db) . "',
        HP_Title= '" . encode_strings($_REQUEST['title'], $db) . "',
        HP_Description= '" . encode_strings($_REQUEST['description'], $db) . "',
            HP_R_ID= '" . encode_strings($regionID, $db) . "'";
        $pic1 = Upload_Pic('0', 'pic1', 0, 0, true, IMG_LOC_ABS, 10000000, true, 36, 'Homepage Painting');
        if (is_array($pic1)) {
            $sql_Eng_Insert .= ", HP_Homepage_Painting = '" . encode_strings($pic1['0']['0'], $db) . "',
                    HP_Homepage_Painting_Mobile = '" . encode_strings($pic1['0']['1'], $db) . "'";
        }
        $sql_Eng_Insert = "INSERT " . $sql_Eng_Insert;
        $result = mysql_query($sql_Eng_Insert, $db) or die("Invalid query: $sql_Eng_Insert -- " . mysql_error());
    }
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: homepage-paintings.php?rid=" . $regionID);
    exit();
}
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    require_once '../include/picUpload.inc.php';
    $update = "UPDATE tbl_Homepage_Paintings SET";
    if ($_REQUEST['flag'] == 'hp') {
        $update .= " HP_Homepage_Painting = '', HP_Homepage_Painting_Mobile = '' ";
        if ($activeRegion['HP_Homepage_Painting']) {
            Delete_Pic(IMG_ICON_ABS . $activeRegion['HP_Homepage_Painting']);
            Delete_Pic(IMG_ICON_ABS . $activeRegion['HP_Homepage_Painting_Mobile']);
        }
    }
    $update .= " WHERE HP_R_ID = '" . encode_strings($regionID, $db) . "'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header('location: homepage-paintings.php?rid=' . $_REQUEST['rid']);
    exit;
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo isset($activeRegion1['R_Name']) ? $activeRegion1['R_Name'] : '' ?> - Manage Homepage Image</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">Homepage Image</div>
        <form name="form2" method="post" action="" enctype="multipart/form-data" class="story-content-update-form">

            <input type="hidden" name="op" value="save">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Title</label>
                <div class="form-data">
                    <input name="title" type="text" value="<?php echo (isset($activeRegion['HP_Title']) ? $activeRegion['HP_Title'] : '') ?>" size="50"/> 
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Description</label>
                <div class="form-data"> 
                    <textarea name="description" cols="85" rows="10" class="tt-description"><?php echo (isset($activeRegion['HP_Description']) ? $activeRegion['HP_Description'] : '') ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Link</label>
                <div class="form-data">
                    <input name="link" type="text" id="textfield2" value="<?php echo (isset($activeRegion['HP_Link']) ? $activeRegion['HP_Link'] : '') ?>" size="50"/> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Homepage Painting</label>
                <div class="form-data div_image_library cat-region-width">
                    <label for="photo1" class="daily_browse with-no-library">Browse</label>
                    <div class="cropit-image-preview aboutus-photo-perview">
                        <img class="preview-img preview-img-script1" style="display: none;" src="">    
                        <?php if ($activeRegion['HP_Homepage_Painting'] != '') { ?>
                            <img class="existing-img existing_imgs1" src="<?php echo IMG_LOC_REL . $activeRegion['HP_Homepage_Painting'] ?>" >
                        <?php } ?>
                    </div>
                    <input type="file" onchange="show_file_name(1, this)" name="pic1[]" id="photo1" style="display: none;">

                    <?php if ($activeRegion['HP_Homepage_Painting'] != '') { ?>
                        <a class="deletePhoto delete-region-cat-photo" style="margin-top: 10px;" onclick="return confirm('Are you sure you want to delete photo?');" href="homepage-paintings.php?rid=<?php echo $regionID ?>&flag=hp&op=del&lang=1">Delete Photo</a>
                    <?php } ?>
                </div>
            </div>
            <div class="form-inside-div  width-data-content border-none"> 
                <div class="button"><input type="submit" name="button2" id="button22" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>

