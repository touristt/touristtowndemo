<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('manage-users', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['id'] > 0) {
    $sql = "SELECT * FROM tbl_Buying_Group WHERE BG_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
}

if ($_POST['op'] == 'save') {
    $sql = "tbl_Buying_Group SET 
            BG_Name = '" . encode_strings($_REQUEST['name'], $db) . "', 
            BG_Email = '" . encode_strings($_REQUEST['email'], $db) . "', 
            BG_Notes = '" . encode_strings($_REQUEST['notes'], $db) . "', ";
    if (is_array($_POST['regions'])) {
        $sql .= "BG_Business_In = '" . implode(',', $_POST['regions']) . "' ";
    } else {
        $sql .= "BG_Business_In = '' ";
    }

    if ($row['BG_ID'] > 0) {
        $sql = "UPDATE " . $sql . " WHERE BG_ID = '" . $row['BG_ID'] . "'";
    } else {
        $sql = "INSERT " . $sql;
    }
    $result = mysql_query($sql, $db);
     if ($result) {
        $_SESSION['success'] = 1;
    }else{
        $_SESSION['error'] = 1;
    }

    header("Location: cc-list.php");
    exit();
} elseif ($_GET['op'] == 'del') {

    $sql = "DELETE tbl_Buying_Group, tbl_Buying_Group_User FROM tbl_Buying_Group LEFT JOIN tbl_Buying_Group_User ON BG_ID = BGU_Buying_Group_ID 
            WHERE BG_ID = '" . $row['BG_ID'] . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
    }else{
        $_SESSION['delete_error'] = 1;
    }
    header("Location: cc-list.php");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Chamber of Commerce</div>
        <div class="link">
            <a href="cc-info.php">+Add Chamber</a>
        </div>
    </div>
    <div class="left">
        <?php require_once('../include/nav-home-sub.php'); ?>
    </div>
    <div class="right">
        <form name="form1" method="post" action="cc-info.php">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="id" value="<?php echo  $row['BG_ID'] ?>">
            <div class="content-header">Chamber Details</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Name</label>
                <div class="form-data">
                    <input name="name" type="text" id="name" value="<?php echo  $row['BG_Name'] ?>" size="40" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Email</label>
                <div class="form-data">
                    <input name="email" type="text" id="email" value="<?php echo  $row['BG_Email'] ?>" size="40" required/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Notes</label>
                <div class="form-data">
                    <textarea name="notes" cols="40" wrap="VIRTUAL" id="notes"><?php echo  $row['BG_Notes'] ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Regions</label>
                <div class="form-data">
                    <?PHP
                    $sql = "SELECT R_ID, R_Name FROM tbl_Region ORDER BY R_Name";
                    $resultRegions = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                    $myRegions = explode(',', $row['BG_Business_In']);
                    while ($rowRegions = mysql_fetch_assoc($resultRegions)) {
                        echo '<input name="regions[]" type="checkbox" value="' . $rowRegions['R_ID'] . '"';
                        echo in_array($rowRegions['R_ID'], $myRegions) ? ' checked' : '';
                        echo '>' . $rowRegions['R_Name'] . '<br>';
                    }
                    ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="button5" id="button52" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>