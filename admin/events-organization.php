<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';
if (!in_array('manage-events', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $sql = "SELECT EO_ID, EO_Name, EO_Region_ID, EO_Description FROM Events_Organization WHERE EO_ID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $community = implode(',', $_REQUEST['rID']);
    $sql = "Events_Organization SET 
            EO_Region_ID = '" . encode_strings($community, $db) . "', 
            EO_Name = '" . encode_strings($_REQUEST['name'], $db) . "', 
            EO_Description = '" . encode_strings($_REQUEST['description'], $db) . "'";

    if ($_REQUEST['id'] > 0) {
        $sql = "UPDATE " . $sql . " WHERE EO_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
        $result = mysql_query($sql, $db);
        // TRACK DATA ENTRY
        $id = $_REQUEST['id'];
        Track_Data_Entry('Event','','Manage Organizations',$id,'Update','super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db);
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Event','','Manage Organizations',$id,'Add','super admin');
    }
    
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: events-organizations.php");
    exit();
} elseif (isset($_GET['op']) && $_GET['op'] == 'del') {

    $sql = "DELETE Events_Organization
            FROM Events_Organization 
            WHERE EO_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($sql, $db);
    // TRACK DATA ENTRY
    $id = $_REQUEST['id'];
    Track_Data_Entry('Event','','Manage Organizations',$id,'Delete','super admin');
    if ($result) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }

    header("Location: events-organizations.php");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Events - Manage Organizations</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manageevents.php'; ?></div>
    <div class="right">
        <form name="form1" method="post" action="/admin/events-organization.php">

            <input type="hidden" name="id" value="<?php echo $row['EO_ID'] ?>">
            <input type="hidden" name="op" value="save">
            <div class="content-header">Organization Information</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Organization Name</label>
                <div class="form-data">
                    <input name="name" type="text" id="Bname" value="<?php echo $row['EO_Name'] ?>" size="50" required />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Community</label>
                <div class="form-data">
                    <div id="childRegion"> 
                        <?PHP
                        $sqlCom = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Parent != 0 ORDER BY R_Name";
                        $resultRegionList = mysql_query($sqlCom, $db) or die("Invalid query: $sqlCom -- " . mysql_error());
                        $region_list = explode(',', $row['EO_Region_ID']);
                        while ($rowRList = mysql_fetch_assoc($resultRegionList)) {
                            echo '<div class="childRegionCheckbox"><input type="checkbox" name="rID[]" value="' . $rowRList['R_ID'] . '"';
                            echo (in_array($rowRList['R_ID'], $region_list)) ? 'checked' : '';
                            echo '>' . $rowRList['R_Name'] . "</div>";
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Description</label>
                <div class="form-data">
                    <textarea name="description"  class="description-ckeditor" cols="50" wrap="VIRTUAL" id="description"><?php echo $row['EO_Description'] ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="button2" id="button2" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>