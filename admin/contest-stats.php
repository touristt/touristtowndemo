<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('manage-contests', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['id'] > 0) {
    $sql = "SELECT C_ID, C_Name, C_Views FROM tbl_Contests WHERE C_ID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
}

require_once '../include/admin/header.php';

$sql = "SELECT COUNT(*) FROM tbl_Contests_Entry WHERE CE_C_ID = '" . encode_strings($row['C_ID'], $db) . "'";
$resultCount = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$entryCount = mysql_fetch_row($resultCount);
$numEntries = $entryCount[0];

$sql = "SELECT SUM(CR_Views) FROM tbl_Contests_Redirect WHERE CR_C_ID = '" . encode_strings($row['C_ID'], $db) . "'";
$resultCount = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$redirectCount = mysql_fetch_row($resultCount);
$numRedirects = $redirectCount[0];
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo  $row['C_Name'] ?> - Stats</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <div class="sub-menu">
            <ul>
                <li><a href="/admin/contests.php">Manage Contests</a></li>
                <li><a href="/admin/contest.php">+Add Contest</a></li>
            </ul>
        </div>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name-loc">Contest Stats</div>
        </div>
        <div class="data-content">
            <div class="data-column spl-name-loc"># Views</div>
            <div class="data-column spl-other">
                <?php echo  $row['C_Views'] ?>
            </div>
        </div>
        <div class="data-content">
            <div class="data-column spl-name-loc"># Entries</div>
            <div class="data-column spl-other">
                <?php echo  $numEntries ?>
            </div>
        </div>
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name-loc">Redirect Stats</div>
        </div>
        <div class="data-content">
            <div class="data-column spl-name-loc"># Redirects - Total</div>
            <div class="data-column spl-other">
                <?php echo  $numRedirects ?>
            </div>
        </div>
        <?PHP
        $sql = "SELECT CR_Title, CR_Views FROM tbl_Contests_Redirect WHERE CR_C_ID = '" . encode_strings($row['C_ID'], $db) . "' ORDER BY CR_Title";
        $resultCount = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());

        while ($redirectCount = mysql_fetch_assoc($resultCount)) {
            ?>
            <div class="data-content">
                <div class="data-column spl-name-loc"># <?php echo  $redirectCount['CR_Title'] ?></div>
                <div class="data-column spl-other">
                    <?php echo  $redirectCount['CR_Views'] ?>
                </div>
            </div>
            <?PHP
        }
        ?>
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name-loc">Entries</div>
            <div class="data-column padding-none spl-other"><a href="contest-download.php?id=<?php echo  $row['C_ID'] ?>">Download</a></div>
        </div>

        <?PHP
        $sql = "SELECT CE_Date, CE_Email FROM tbl_Contests_Entry WHERE CE_C_ID = '" . encode_strings($row['C_ID'], $db) . "' ORDER BY CE_Date";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());

        while ($row = mysql_fetch_assoc($result)) {
            ?>
            <div class="data-content">
                <div class="data-column spl-name-loc"> <?php echo  $row['CE_Email'] ?></div>
                <div class="data-column spl-other">
                    <?php echo  $row['CE_Date'] ?>
                </div>
            </div>
            <?PHP
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>