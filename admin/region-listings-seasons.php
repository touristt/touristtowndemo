<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';
if (!in_array('regions', $_SESSION['USER_PERMISSIONS']) && !in_array('manage-county-region', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
    $sql = "SELECT R_ID, R_Name, R_Type, R_Parent FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
    $sortby = (isset($_REQUEST['sortby'])) ? $_REQUEST['sortby'] : " ";
    $subcategory = (isset($_REQUEST['subcategory'])) ? $_REQUEST['subcategory'] : " ";
    $sql_org_listings = "SELECT BLCR_BL_ID FROM  tbl_Business_Listing_Category_Region 
                         WHERE BLCR_BLC_R_ID = '" . encode_strings($regionID, $db) . "' and BLCR_Home_status=1";
    $result_org_listings = mysql_query($sql_org_listings, $db) or die("Invalid query: $sql_org_listings -- " . mysql_error());
    while ($row_org_listings = mysql_fetch_assoc($result_org_listings)) {
        $childbl_id[] = $row_org_listings['BLCR_BL_ID'];
    }
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $rid = $_POST['rid'];
    // GETTING ALL THE SEASONS POSTED BY THE FORM WITH CHECKED BOXES
    $seasonsArray = $_POST['season'];
    // GETTING ALL THE LISTINGS (EVEN THE LISTINGS IGNORED BY THE $seasonsArray )
    $listingArray = $_POST['listing'];

    //Traverse all listings
    foreach ($listingArray as $key) {
        $delete = "DELETE FROM tbl_Business_Listing_Season WHERE BLS_BL_ID = '" . encode_strings($key, $db) . "'";
        $res = mysql_query($delete, $db);
        //Traverse all seasons checked
        foreach ($seasonsArray[$key] as $season) {
            $sql_season = "INSERT tbl_Business_Listing_Season SET BLS_S_ID = '" . $season . "', BLS_BL_ID = '" . encode_strings($key, $db) . "'";
            $res_season = mysql_query($sql_season, $db);
        }
    }

    if ($res_season) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    if (isset($_GET['page']) && $_GET['page'] > 0) {
        $p = "&page=" . $_GET['page'];
    }
    header("Location: region-listings-seasons.php?rid=" . $rid . $p);
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Listing Seasons</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP
        if ($regionID > 0) {
            require '../include/nav-manage-region.php';
        } else {
            echo " & nbsp;";
        }
        ?>
    </div>
    <div class="right">
        <div class="content-header">Apply Seasons to Listings - <?php echo $activeRegion['R_Name'] ?>
        </div>
        <form name="listing-seasons" method="POST" action="">
            <?PHP
            $sql = "SELECT BL_ID, BL_Listing_Title FROM tbl_Business_Listing 
                    LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                    INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID
                    WHERE (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = 1";
            if ($activeRegion['R_Parent'] == 0 && $activeRegion['R_ID'] != 26) {
                $sql .= " AND BLCR_BLC_R_ID IN (SELECT R_ID from tbl_Region LEFT JOIN tbl_Region_Multiple ON RM_Child = R_ID where RM_Parent = '" . encode_strings($activeRegion['R_ID'], $db) . "')";
            } else {
                $sql .= " AND BLCR_BLC_R_ID IN (" . $activeRegion['R_ID'] . ")";
            }
            if (isset($sortby) && $sortby != 0) {
                $sql .= " AND BLC_M_C_ID = $sortby";
            }
            if (isset($subcategory) && $subcategory != 0 && $sortby != 0) {
                $sql .= " AND BLC_C_ID = $subcategory";
            }
           $sql .= " GROUP BY BL_ID ORDER BY BL_Listing_Title";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $pages = new Paginate(mysql_num_rows($result), 100);
            $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                //get listing seasons
                $sqlLS = "SELECT BLS_S_ID FROM tbl_Business_Listing_Season WHERE BLS_BL_ID = '" . encode_strings($row['BL_ID'], $db) . "'";
                $resultLS = mysql_query($sqlLS, $db) or die("Invalid query: $sqlLS -- " . mysql_error());
                $listingSeasons = array();
                while ($rowLS = mysql_fetch_array($resultLS)) {
                    $listingSeasons[] = $rowLS['BLS_S_ID'];
                }
                ?>
                <div class="data-content">
                    <div class="data-column spl-org-listngs">
                        <input type="hidden" name="listing[<?php echo $row['BL_ID'] ?>]" value="<?php echo $row['BL_ID'] ?>">
                        <div class="listing-title"><?php echo $row['BL_Listing_Title'] ?></div>

                        <?PHP
                        $sqlSeasons = "SELECT * FROM tbl_Seasons";
                        $resultSeasons = mysql_query($sqlSeasons, $db) or die("Invalid query: $sqlSeasons -- " . mysql_error());
                        while ($rowSeasons = mysql_fetch_array($resultSeasons)) {
                            print '<div class="form-inside-div"><input name="season[' . $row['BL_ID'] . '][' . $rowSeasons['S_ID'] . ']" type="checkbox" value="' . $rowSeasons['S_ID'] . '" ' . (in_array($rowSeasons['S_ID'], $listingSeasons) ? 'checked' : '') . ' />' . $rowSeasons['S_Name'] . '</div>';
                        }
                        ?>
                    </div>
                </div>
                <?php
            }
            if (isset($pages)) {
                echo $pages->paginate();
            }
            ?>
            <div class="form-inside-div border-none">
                <div class="button">
                    <input type="submit" value="Save Now">
                </div>
            </div>
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <input type="hidden" name="op" value="save">
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
