<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';

if (!in_array('manage-county-region', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0 && $_REQUEST['rid'] == $_SESSION['USER_LIMIT']) {
    $regionID = $_REQUEST['rid'];
    $sql = "SELECT R_ID, R_Name, R_Type, R_Description, R_Mobile_Description FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
} else {
    header("Location: /admin/");
    exit();
}

if ($_POST['op'] == 'save') {
    $sql = " UPDATE tbl_Region SET 
              R_Description = '" . encode_strings(trim(str_replace('<p>', '', str_replace('</p>', '', $_REQUEST['description']))), $db) . "',
              R_Mobile_Description = '" . encode_strings(trim(str_replace('<p>', '', str_replace('</p>', '', $_REQUEST['mobile_description']))), $db) . "'
              WHERE R_ID = '" . encode_strings($regionID, $db) . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }

    header("Location: county-region.php?rid=" . $regionID);
    exit();
}

if ($_REQUEST['op'] == 'slider_save') {

    if ($_REQUEST['image_bank'] == "") {
        require '../include/picUpload.inc.php';
        // last @param 1 = Gallery Images
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 10, 'region', $regionID);
        if (is_array($pic)) {
            $sqlMax = "SELECT MAX(RTP_Order) FROM tbl_Region_Themes_Photos WHERE RTP_RT_RID = '$regionID'";
            $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
            $rowMax = mysql_fetch_row($resultMax);
            $sql = "INSERT tbl_Region_Themes_Photos SET RTP_Photo = '" . encode_strings($pic['0']['0'], $db) . "', 
                    RTP_Mobile_Photo = '" . encode_strings($pic['0']['1'], $db) . "',
                    RTP_RT_RID = '$regionID',
                    RTP_Order = '" . ($rowMax[0] + 1) . "'";
            $pic_id = $pic['1'];
        }
    } else {

        $pic_id = $_POST['image_bank'];
        require '../include/picUpload.inc.php';
        // last @param 1 = Gallery Images
        $pic = Upload_Pic_Library($pic_id, 10);
        if (is_array($pic)) {
            $sqlMax = "SELECT MAX(RTP_Order) FROM tbl_Region_Themes_Photos WHERE RTP_RT_RID = '$regionID'";
            $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
            $rowMax = mysql_fetch_row($resultMax);
            $sql = "INSERT tbl_Region_Themes_Photos SET RTP_Photo = '" . encode_strings($pic['0'], $db) . "', 
                    RTP_Mobile_Photo = '" . encode_strings($pic['1'], $db) . "', 
                    RTP_RT_RID = '$regionID',                
                    RTP_Order = '" . ($rowMax[0] + 1) . "'";
        }
    }
    $result = mysql_query($sql, $db);
    $id = mysql_insert_id();
    if ($result) {
        $_SESSION['success'] = 1;
        if ($pic_id > 0) {
            //Image usage from image bank
            imageBankUsage($pic_id, 'IBU_R_ID', $regionID, 'IBU_Region_Theme', $id);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: county-region.php?rid=" . $regionID);
    exit();
}
if ($_REQUEST['op'] == 'del') {
    $update = "DELETE FROM tbl_Region_Themes_Photos WHERE RTP_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['delete'] = 1;
        imageBankUsageDelete('IBU_R_ID', $regionID, 'IBU_Region_Theme', $_REQUEST['id']);
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header('location: county-region.php?rid=' . $_REQUEST['rid']);
    exit;
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Manage Homepage</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">
            <div class="title">Homepage Description</div>
        </div>
        <form action="" method="post" name="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Description</label>
                <div class="form-data"> 
                    <textarea name="description" cols="85" rows="10" class="tt-description"><?php echo (isset($activeRegion['R_Description']) ? $activeRegion['R_Description'] : '') ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Mobile Description</label>
                <div class="form-data">
                    <textarea name="mobile_description" cols="85" rows="10" class="tt-description"><?php echo (isset($activeRegion['R_Mobile_Description']) ? $activeRegion['R_Mobile_Description'] : '') ?></textarea>
                </div>
            </div>
            <div class="form-inside-div  width-data-content border-none"> 
                <div class="button">
                    <input type="submit" name="button2" value="Submit" />
                </div>
            </div>
        </form>

        <div class="content-header">
            <div class="title">Homepage Slider</div>
            <div class="link">
                <?PHP
                $select = "SELECT RTP_ID, RTP_Photo FROM tbl_Region_Themes_Photos WHERE RTP_RT_RID = '" . $regionID . "' ORDER BY RTP_Order";
                $result = mysql_query($select, $db);
                $numRows = mysql_num_rows($result);
                if ($numRows < 10) {
                    echo '<div class="add-link"><a onclick="show_form()">+Add Photo</a></div>';
                }
                ?>
            </div>
            <script>
                function show_form() {
                    $('#gallery-form').show();   
                }    
            </script>
        </div>
        <form name="form1" method="post" onSubmit="return check_img_size(10, 10000000)"  enctype="multipart/form-data" action="" id="gallery-form" style="display:none;float:left;width:100%;margin-bottom: 10px;">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <input type="hidden" name="op" value="slider_save">
            <input type="hidden" name="image_bank" class="image_bank" id="image_bank10" value="">
            <div class="form-inside-div form-inside-div-width-admin">
                <label class="slider">Photo</label>
                <div class="form-data form-input-text">
                    <div class="div_image_library">
                        <span class="daily_browse" onclick="show_image_library(10)">Select File</span>
<!--                        <div class="dialog-close dialog10 dialog-bank" style="display: none;">
                            <label for="photo10" class="daily_browse">Upload File</label>
                            <a onclick="show_image_library(10)">Add from Library</a>
                        </div>-->
                        <input onchange="show_file_name(10, this, 0)" id="photo10" type="file" name="pic[]" style="display: none;"/>
                    </div>
                    <div class="form-inside-div add-about-us-item-image margin-none">
                        <div class="cropit-image-preview aboutus-photo-perview">
                            <img class="preview-img preview-img-script10" style="display: none;" src="">    
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <div class="button">
                    <input type="submit" name="button" id="button" value="Save Photo" />
                </div>
            </div>
        </form>
        <div class="photo-gallery-limit">
            <span>Photos <?php echo (($numRows == "") ? "0" : $numRows) ?> of 10</span>
        </div>
        <div class="form-inside-div border-none margin-none region-slider-images" id="region-slider-list">
            <ul class="gallery">
                <?PHP
                $count = 1;
                while ($data = mysql_fetch_assoc($result)) {
                    ?>
                    <li class="image" id="recordsArray_<?php echo $data['RTP_ID'] ?>">
                        <label>Photo <?php echo $count ?></label>
                        <div class="form-data div_image_library cat-region-width">
                            <a class="deletePhoto delete-region-cat-photo" onclick="return confirm('Are you sure you want to delete photo?');" href="county-region.php?id=<?php echo $data['RTP_ID'] ?>&rid=<?php echo $regionID ?>&op=del">Delete Photo</a>
                            <div class="cropit-image-preview aboutus-photo-perview">  
                                <?php if ($data['RTP_Photo'] != "") { ?>
                                    <img class="existing-img existing_imgs4" src="http://<?php echo DOMAIN . IMG_LOC_REL . $data['RTP_Photo'] ?>">
                                <?php } ?>
                            </div>
                        </div>
                    </li>
                    <?PHP
                    $count++;
                }
                ?>
            </ul>
        </div>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>

<script>
    $(function () {
        $("#region-slider-list ul.gallery").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Region_Themes_Photos&field=RTP_Order&id=RTP_ID';
                $.post("reorder.php", order, function (theResponse) {
                    swal("Sliders Re-ordered!", "Sliders Re-ordered successfully.", "success");
                });
            }
        });
    });
</script>


<?PHP
require_once '../include/admin/footer.php';
?>