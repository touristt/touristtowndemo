<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';

if (!in_array('business-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
if ($_REQUEST['email'] != '') {
    $sql = "SELECT B_ID, B_Email FROM tbl_Business WHERE B_Email = '" . encode_strings($_REQUEST['email'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowBUS = mysql_fetch_assoc($result);
    if ($rowBUS['B_ID']) {
        $sql = "SELECT COUNT(*) FROM tbl_Business_Listing WHERE BL_B_ID = '" . encode_strings($rowBUS['B_ID'], $db) . "'";
        $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $rowListings = mysql_fetch_row($resultTMP);
    } else {
        $_SESSION['error'] = 1;
        header("Location: listings.php");
        exit();
    }
} else {
    $sql = "SELECT B_ID, B_Email FROM tbl_Business";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowBUS = mysql_fetch_assoc($result);
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Deactivate Listings</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP
        require '../include/nav-managecustomers.php';
        ?>
    </div>
    <?php if (!isset($_REQUEST['email'])) { ?>
        <div class="right">
            <form name="listing_form" method="post" action="#">
                <div class="content-sub-header">
                    <div class="data-column mp-listing-title padding-none">Listing Title</div>
                    <div class="data-column mp-listing-other-2 padding-none">Active</div>
                    <div class="data-column mp-listing-cat padding-none">Category</div>
                    <div class="data-column mp-listing-other padding-none">Type</div>
                </div>
                <?PHP
                $sql = "SELECT BL_ID, BL_Listing_Title, hide_show_listing, LT_Name FROM tbl_Business_Listing
                        LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
                        LEFT JOIN tbl_Listing_Type ON BL_Listing_Type = LT_ID ";
                if (strlen($_REQUEST['strSearch']) > 3) {
                    $where = " WHERE BL_Listing_Title LIKE '%" . encode_strings($_REQUEST['strSearch'], $db) . "%' AND hide_show_listing = 0 " . $LTname;
                    $groupBy = " GROUP BY BL_ID";
                    $orderBy = " ORDER BY BL_Listing_Title";
                } else if (strlen($_REQUEST['strSearch_contact']) > 3) {
                    $where = " WHERE BL_Contact LIKE '%" . encode_strings($_REQUEST['strSearch_contact'], $db) . "%' AND hide_show_listing = 0 " . $LTname;
                    $groupBy = " GROUP BY BL_ID";
                    $orderBy = " ORDER BY BL_Listing_Title";
                } else {
                    $where = " WHERE hide_show_listing = 0 ";
                    $groupBy = " GROUP BY BL_ID";
                    $orderBy = " ORDER BY BL_Listing_Title";
                }
                $sql .= $where . $groupBy . $orderBy;
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                $pages = new Paginate(mysql_num_rows($result), 100);
                $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
                while ($row = mysql_fetch_assoc($result)) {
                    ?>
                    <div class="data-content">
                        <div class="data-column mp-listing-title">
                            <?php echo $row['BL_Listing_Title']; ?>
                        </div>
                        <?php
                        if (isset($row['hide_show_listing']) && $row['hide_show_listing'] == 1) {
                            $checked = "checked";
                        } else {
                            $checked = "";
                        }
                        ?>
                        <div class="data-column mp-listing-other-2">
                            <input type="checkbox" name="display" id="display<?php echo $row['BL_ID']; ?>" value="<?php echo $row['hide_show_listing']; ?>" <?php echo $checked; ?> onclick=" return hide_show_listings(<?php echo $row['BL_ID']; ?>);">
                        </div>
                        <div class="data-column mp-listing-cat">
                            <?php
                            $sql_sub_categories = "SELECT * FROM tbl_Business_Listing_Category
                        left join tbl_Category on C_ID = BLC_M_C_ID
                        where BLC_BL_ID=" . encode_strings($row['BL_ID'], $db) . " GROUP BY BLC_M_C_ID ";
                            $result_sub_categories = mysql_query($sql_sub_categories, $db) or die("Invalid query: $sql_sub_categories -- " . mysql_error());
                            while ($row_sub_categories = mysql_fetch_array($result_sub_categories)) {
                                ?>
                                <div>
                                    <?php
                                    echo $row_sub_categories['C_Name'];
                                    ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="data-column mp-listing-other">
                            <?php echo $row['LT_Name'] ?>
                        </div>
                    </div>
                    <?PHP
                }
                ?>
                <?php
                if (isset($pages)) {
                    echo $pages->paginate();
                }
                ?>
        </div>
    <?php } else { ?>
        <div class="right">
            <div class="content-sub-header">
                <div class="data-column spl-name-events padding-none">Email</div>
                <div class="data-column spl-other-cc-list padding-none"></div>
                <div class="data-column spl-other-cc-list padding-none">#Listings</div>
                <div class="data-column spl-other-cc-list padding-none">Edit</div>
                <div class="data-column mp-listing-other-2 padding-none">Active</div>
            </div>
            <div class="data-content">
                <div class="data-column spl-name-events"><a href="customer-listings.php?id=<?php echo $rowBUS['B_ID'] ?>"><?php echo $rowBUS['B_Email'] ?></a></div>
                <div class="data-column spl-other-cc-list"></div>
                <div class="data-column spl-other-cc-list"><?php echo $rowListings[0] ?></div>
            <div class="data-column spl-other-cc-list"><a href="customer-listings.php?id=<?php echo $rowBUS['B_ID'] ?>">Edit</a></div>
            </div>
        </div>
    <?php } ?>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script language="javascript">
    function hide_show_listings(business_listing_id) {
        var value = document.getElementById('display' + business_listing_id).checked;


        xmlHttp = GetXmlHttpObject();
        if (xmlHttp == null)
        {
            alert("Your browser does not support AJAX!");
            return;
        }
        // alert(cid);
        var url = "show_hide_listing.php?BL_ID=" + business_listing_id + "&show_hide=" + value;
        url = url + "&sid=" + Math.random();
        xmlHttp.open("GET", url, true);
        xmlHttp.send(null);
        xmlHttp.onreadystatechange = stateWaiting;
        function stateWaiting()
        {
            if (xmlHttp.readyState == 4)
            {
                var data = xmlHttp.responseText;
                alert(data);
            }
        }

        function GetXmlHttpObject()
        {
            var xmlHttp = null;
            try {
                xmlHttp = new XMLHttpRequest();
            } catch (e) {
                try {
                    xmlHttp = new ActiveXObject("Msxm12.XMLHTTP");
                } catch (e)
                {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
            }
            return xmlHttp;
        }
    }
</script>
