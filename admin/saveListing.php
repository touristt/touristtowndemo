<?php
require_once '../include/track-data-entry.php';
require_once '../include/config.inc.php';

$return = array();
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'save') {
    $regionID = $_REQUEST['rid'];
    $category = $_REQUEST['cat'];
    $subcategory = $_REQUEST['subcat'];
    $listing = $_REQUEST['listings'];
    $sql = "SELECT R_Whats_NearBy FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
    if ($listing != '') {
        $sql = "SELECT BLC_BL_ID FROM tbl_Business_Listing_Category WHERE BLC_C_ID = '" . $subcategory . "' AND BLC_BL_ID = '" . $listing . "'";
        $result = mysql_query($sql) or die(mysql_error());
        $subcatCount = mysql_num_rows($result);
        $sql = "SELECT BLCR_BL_ID FROM tbl_Business_Listing_Category_Region WHERE BLCR_BLC_R_ID = '" . $regionID . "' AND BLCR_BL_ID = '" . $listing . "'";
        $result = mysql_query($sql) or die(mysql_error());
        $regionCount = mysql_num_rows($result);
        $sql = "SELECT BLO_Order FROM tbl_Business_Listing_Order WHERE BLO_R_ID = '" . $regionID . "' AND BLO_M_C_ID = '" . $category . "' 
                AND BLO_S_C_ID = '" . $subcategory . "' ORDER BY BLO_Order DESC LIMIT 1";
        $result = mysql_query($sql) or die(mysql_error());
        $orderCount = mysql_num_rows($result);
        $rowMax = mysql_fetch_row($result);
        if ($regionCount == 0) {
            $sql = "INSERT tbl_Business_Listing_Category_Region SET BLCR_BLC_R_ID = '" . $regionID . "', BLCR_BL_ID = '" . $listing . "'";
            mysql_query($sql) or die(mysql_error());
        }
        if ($subcatCount == 0) {
            $sql = "INSERT tbl_Business_Listing_Category SET BLC_C_ID = '" . $subcategory . "', BLC_M_C_ID = '" . $category . "', 
            BLC_BL_ID = '" . $listing . "'";
            mysql_query($sql) or die(mysql_error());
        }
        if ($orderCount == 0) {
            $sql = "INSERT tbl_Business_Listing_Order SET BLO_BL_ID = '" . $listing . "', BLO_M_C_ID = '" . $category . "', 
                    BLO_S_C_ID = '" . $subcategory . "', BLO_R_ID = '" . $regionID . "', BLO_Order = '1'";
            mysql_query($sql) or die(mysql_error());
        }else{
            $sql = "INSERT tbl_Business_Listing_Order SET BLO_BL_ID = '" . $listing . "', BLO_M_C_ID = '" . $category . "', 
                    BLO_S_C_ID = '" . $subcategory . "', BLO_R_ID = '" . $regionID . "', BLO_Order = '" . ($rowMax[0] + 1) . "'";
            mysql_query($sql) or die(mysql_error());
        }
        $return['success'] = 1;
        //return
        $listings = '';
        $sql = "SELECT DISTINCT BL_ID, BL_Listing_Title, BLCR_BLC_R_ID, BLO_ID FROM tbl_Business_Listing 
                LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID
                LEFT JOIN tbl_Business_Listing_Order ON BL_ID = BLO_BL_ID AND BLO_R_ID = '" . $regionID . "' AND BLO_M_C_ID = '" . $category . "' 
                AND BLO_S_C_ID = '" . $subcategory . "'
                WHERE BL_Free_Listing_status = 0 AND hide_show_listing = 1 AND BLC_C_ID = '" . $subcategory . "' 
                AND BLCR_BLC_R_ID = '" . encode_strings($regionID, $db) . "' GROUP BY BL_ID ORDER BY BLO_Order ASC";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($rowListings = mysql_fetch_assoc($result)) {
            $listings .= '<div class="data-content" id="recordsArray_' . $rowListings['BLO_ID'] . '">
                          <div class="data-column spl-org-listngs"  id="whats_near_' . $rowListings['BL_ID'] . '">
                          <div class="select-listings-nearby-title check11">' . $rowListings['BL_Listing_Title'] . '</div>';
            $listings .= '<div class="remove-link"><a href="region-listings.php?rid=' . $regionID . '&cat=' . $category . '&subcat=' . $subcategory . '&bl_id=' . $rowListings['BL_ID'] . '&del=true" onclick="return confirm(\'Are you sure you want to perform this action?\');">Remove</a></div>';
            if ($activeRegion['R_Whats_NearBy'] == 1) {
                $listings .= '<div class="select-listings-nearby" id="remove_near_' . $rowListings['BL_ID'] . '"><a href="region-listings-nearby.php?bl_id=' . $rowListings['BL_ID'] . '&rid=' . $regionID . '">What`s Nearby?</a></div>';
             }
            
            $listings .= '</div>
                </div>';
        }
        $return['Listings'] = $listings;
    }
}
// TRACK DATA ENTRY
Track_Data_Entry('Websites', $regionID, 'Listings', $listing, 'Add Listing', 'super admin');
print json_encode($return);
exit;
?>