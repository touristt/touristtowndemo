<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/ranking.inc.php';

$BL_ID = $_REQUEST['bl_id'];
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_ID, BL_Listing_Title, BL_Listing_Type, BL_Name_SEO, BL_Contact, BL_Phone, BL_Toll_Free, BL_Cell, BL_Fax, BL_Email, 
            BL_Website, BL_Street, BL_Town, BL_Province, BL_PostalCode, BL_Lat, BL_Long, BL_Description, BL_Trip_Advisor, BL_Header_Image, BL_Photo, 
            BL_SEO_Title, BL_SEO_Title, BL_SEO_Description, BL_SEO_Keywords, BL_Search_Words, BL_Basic_Points, BL_Line_Item_Title, 
            BL_Line_Item_Price, BL_CoC_Dis_2, BL_SubTotal, BL_Tax, BL_Total, BL_Points, BL_Renewal_Date
            FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID  WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
    $sql_region = " SELECT BLC_M_C_ID, BLC_C_ID, BLCR_BLC_R_ID FROM tbl_Business_Listing
                    LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                    INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID 
                    INNER JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                    WHERE BLC_BL_ID = '$BL_ID'";
    $res_region = mysql_query($sql_region);
    $regions = array();
    $category = array();
    $subcategories = array();
    while ($r = mysql_fetch_assoc($res_region)) {
        if ($r['BLCR_BLC_R_ID'] != '' && !in_array($r['BLCR_BLC_R_ID'], $regions)) {
            $regions[] = $r['BLCR_BLC_R_ID'];
        }
        if ($r['BLC_M_C_ID'] != '' && !in_array($r['BLC_M_C_ID'], $category)) {
            $category[] = $r['BLC_M_C_ID'];
        }
        if ($r['BLC_C_ID'] != '' && !in_array($r['BLC_C_ID'], $subcategories)) {
            $subcategories[] = $r['BLC_C_ID'];
        }
    }
} else {
    header('Location: index.php');
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {

    $sql = "DELETE tbl_Business_Listing, tbl_Business_Listing_Ammenity, tbl_Business_Listing_Category, tbl_Business_Listing_Category_Region,
            tbl_Business_Social, tbl_BL_Feature, payment_profiles, tbl_Business_Feature_About, tbl_Business_Feature_About_Photo,
            tbl_Business_Feature_About_Main_Photo, tbl_Business_Feature_Daily_Specials, tbl_Business_Feature_Daily_Specials_Description, 
            tbl_Business_Feature_Entertainment_Acts, tbl_Business_Feature_Entertainment_Description, tbl_Business_Feature_Guest_Book, 
            tbl_Business_Feature_Guest_Book_Description, tbl_Business_Feature_Menu, tbl_Business_Feature_Photo, tbl_Business_Feature_Product, 
            tbl_Business_Feature_Product_Photo, tbl_Business_Feature_Coupon, tbl_Business_Feature_Coupon_Category_Multiple, tbl_Image_Bank_Usage, 
            tbl_Advertisement, tbl_Advertisement_Billing, tbl_Advertisement_Change_Request, tbl_Advertisement_Photo, tbl_Advertisement_Statistics
            FROM tbl_Business_Listing 
            LEFT JOIN tbl_Business_Listing_Ammenity ON BLA_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Listing_Category_Region ON BLCR_BLC_ID = BLC_ID 
            LEFT JOIN tbl_Business_Social ON BS_BL_ID = BL_ID
            LEFT JOIN tbl_BL_Feature ON BLF_BL_ID = BL_ID
            LEFT JOIN payment_profiles ON listing_id = BL_ID
            LEFT JOIN tbl_Business_Feature_About ON BFA_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_About_Photo ON BFAP_BFA_ID = BFA_ID
            LEFT JOIN tbl_Business_Feature_About_Main_Photo ON BFAMP_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Daily_Specials ON BFDS_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Daily_Specials_Description ON BFDSD_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Entertainment_Acts ON BFEA_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Entertainment_Description ON BFED_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Guest_Book ON BFGB_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Guest_Book_Description ON tbl_Business_Feature_Guest_Book_Description.BFGB_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Menu ON BFM_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Photo ON BFP_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Product ON tbl_Business_Feature_Product.BFP_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Product_Photo ON BFPP_BFP_ID = tbl_Business_Feature_Product.BFP_ID
            LEFT JOIN tbl_Business_Feature_Coupon ON tbl_Business_Feature_Coupon.BFC_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
            LEFT JOIN tbl_Image_Bank_Usage ON IBU_BL_ID = BL_ID
            LEFT JOIN tbl_Advertisement ON BL_ID = A_BL_ID 
            LEFT JOIN tbl_Advertisement_Billing ON A_ID = AB_A_ID 
            LEFT JOIN tbl_Advertisement_Change_Request ON A_ID = ACR_A_AID 
            LEFT JOIN tbl_Advertisement_Photo ON A_ID = AP_A_ID 
            LEFT JOIN tbl_Advertisement_Statistics ON A_ID = AS_A_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "'";
    $result = mysql_query($sql, $db);

    if ($result) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
    // send back to pending listings if came from there
    if (isset($_REQUEST['return']) && $_REQUEST['return'] == 'pending') {
        header("Location: /admin/pending-listings.php");
        exit();
    } else {
        header("Location: /admin/customer-listings.php?id=" . $rowListing['B_ID']);
        exit();
    }
}

if (isset($_GET['rid'])) {
    $RID = $_GET['rid'];
    mysql_query("UPDATE tbl_Recommendations SET R_Seen = 1 WHERE R_ID = '$RID'");
    header("Location: /admin/customer-listing-recommendations.php?bl_id=$BL_ID");
    exit();
}

// getting the default region
foreach ($regions as $key => $r) {
    if ($key == 0) {
        $default_region = $r;
    }
}
// See if a region is already selected
if (isset($_SESSION['ranking_region' . $BL_ID]) && $_SESSION['ranking_region' . $BL_ID] > 0) {
    $ranking_region = $_SESSION['ranking_region' . $BL_ID];
} else {
    if (isset($default_region)) {
        $ranking_region = $default_region;
    }
}
if (isset($ranking_region)) {
    $_SESSION[$BL_ID]['ranking_region'] = $ranking_region; // This session ranking ID to be used in right overview panel   
}

require_once '../include/admin/header.php';
?>
<div class="content-left">

    <?php require_once '../include/top-nav-listing.php'; ?>

    <div class="title-link">
        <div class="title">Overview - <?php echo $rowListing['BL_Listing_Title'] ?></div>
    </div>
    <?php
    if (isset($rowListing['BL_Listing_Type']) && $rowListing['BL_Listing_Type'] == 1) {
        echo '<div class="overview">';
        echo '<div class="data notifications">There is no Ranking, Points and Notifications for free listings. To get Rankings and Points please upgrade your listing type.</div>';
        echo '</div>';
    } else {
        ?>

        <!--Notification Section-->
        <div class="overview">
            <div class="content-header">Notifications</div>
            <!--        Recommendation-->
            <?PHP
            $recom = "SELECT R_ID, R_Recommends, BL_ID, BL_Listing_Title, BL_Name_SEO FROM tbl_Business_Listing 
                    LEFT JOIN tbl_Business ON BL_B_ID = B_ID
                    LEFT JOIN tbl_Recommendations ON R_Recommends = BL_ID 
                    WHERE R_Recommended = '" . encode_strings($BL_ID, $db) . "'
                    AND R_SEEN = 0";
            $recom_result = mysql_query($recom, $db) or die("Invalid query: $recom -- " . mysql_error());
            $notification_count = mysql_num_rows($recom_result);
            if ($notification_count > 0) {
                while ($recom_row = mysql_fetch_array($recom_result)) {
                    ?>
                    <div class="data notifications">
                        <div class="title">Recommendations</div>
                        <div class="info">
                            <?PHP
                            $link_sql = "SELECT R_Domain, C_Name_SEO FROM tbl_Business_Listing_Category
                                        INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BLC_BL_ID 
                                        LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                                        LEFT JOIN tbl_Category ON C_ID = BLC_M_C_ID
                                        WHERE BLC_BL_ID = '" . $recom_row['R_Recommends'] . "' 
                                        LIMIT 1";

                            $link_result = mysql_query($link_sql, $db) or die("Invalid query: $link_sql -- " . mysql_error());
                            $link_row = mysql_fetch_assoc($link_result);
                            ?>
                            Recommended by the <a href="http://<?php echo $link_row['R_Domain'] ?>/profile/<?php echo $recom_row['BL_Name_SEO'] ?>/<?php echo $recom_row['BL_ID'] ?>/" target="_blank"><?php echo $recom_row['BL_Listing_Title'] ?></a>
                        </div>
                        <div class="view"><a href="customer-listing-overview.php?rid=<?php echo $recom_row['R_ID'] ?>&bl_id=<?php echo $BL_ID ?>">View</a></div>
                    </div>
                    <?php
                }
            }

//        Guest Book
            $guest = "SELECT BFGB_ID FROM tbl_Business_Feature_Guest_Book  
                    WHERE BFGB_BL_ID = '" . encode_strings($BL_ID, $db) . "'
                    AND BFGB_Status = 0";
            $guest_result = mysql_query($guest, $db) or die("Invalid query: $guest -- " . mysql_error());
            $guest_count = mysql_num_rows($guest_result);
            if ($guest_count > 0) {
                ?>
                <div class="data notifications">
                    <div class="title">Guest Book</div>
                    <div class="info">
                        You have a new comment. <a href="customer-feature-guest-book.php?bl_id=<?php echo $BL_ID ?>">View Comment</a>
                    </div>
                    <div class="view"><a href="customer-feature-guest-book.php?bl_id=<?php echo $BL_ID ?>">View Comment</a></div>
                </div>
                <?php
            }
            if ($notification_count == 0 && $guest_count == 0) {
                ?>
                <div class="data notifications">
                    You don't currently have any notifications.
                </div>
                <?php
            }
            ?>

        </div>

        <!--Billing Section-->
        <?php
        if (isset($rowListing['BL_Listing_Type']) && $rowListing['BL_Listing_Type'] != 5) {
            ?>
            <div class="overview">
                <div class="content-header">Billing</div>
                <?PHP
                $billing = "SELECT card_number, DATE_FORMAT(card_expiration, '%Y-%m') AS expiry, BL_Billing_Type, BL_Renewal_Date, BL_Total FROM tbl_Business_Listing 
                            LEFT JOIN tbl_Business ON BL_B_ID = B_ID
                            LEFT JOIN payment_profiles ON B_ID = business_id 
                            WHERE B_ID = '" . encode_strings($BID, $db) . "' AND BL_ID = '" . encode_strings($BL_ID, $db) . "'  LIMIT 1";
                $billing_result = mysql_query($billing, $db) or die("Invalid query: $billing -- " . mysql_error());
                $billing_row = mysql_fetch_array($billing_result);
                /* Check whether monthly or yearly to calculate price accordingly */
                if ($billing_row['BL_Billing_Type'] == -1) {
                    $multiplier = 12;
                } else {
                    $multiplier = 1;
                }
                if ($billing_row['card_number'] != '') {
                    $cardNo = explode("XXXX", $billing_row['card_number']);
                    $cardNo = "#### #### #### " . $cardNo[1] . " (Exp: " . $billing_row['expiry'] . ")";
                } else {
                    $cardNo = 'Not Set';
                }
                if ($billing_row['BL_Billing_Type'] == '-1') {
                    $billing_type = 'Yearly';
                } else if ($billing_row['BL_Billing_Type'] == '1') {
                    $billing_type = 'Monthly';
                } else if ($billing_row['BL_Billing_Type'] == '0') {
                    $billing_type = 'Not Set';
                }
                if ($billing_row['BL_Renewal_Date'] != '0000-00-00') {
                    $date = date('F j, Y', strtotime($billing_row['BL_Renewal_Date']));
                } else {
                    $date = 'Not Set';
                }
                //Advertisements
                $sql = " SELECT A_Title, A_Active_Date, A_Total, AT_Name FROM tbl_Advertisement LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID
                         WHERE A_BL_ID = '" . encode_strings($rowListing['BL_ID'], $db) . "' AND A_Status = 3 AND A_Total > 0
                         AND A_Is_Deleted = 0 AND A_Is_Townasset != 1 AND (A_End_Date > CURDATE() OR A_End_Date = '0000-00-00')";
                $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                $ads_total = 0;
                while ($row = mysql_fetch_assoc($resultTMP)) {
                    $renewalDate = strtotime('-1 month', strtotime($rowListing['BL_Renewal_Date']));
                    $renewalDate = date("Y-m-d", $renewalDate);
                    if ($renewalDate >= $row['A_Active_Date']) {
                        $ad_total = ($multiplier * $row['A_Total']);
                        $ads_total += $ad_total;
                    } else {
                        $datetime1 = strtotime(date('Y-m-d', strtotime($row['A_Active_Date'])));
                        $datetime2 = strtotime(date('Y-m-d', strtotime($rowListing['BL_Renewal_Date'])));

                        $secs = $datetime2 - $datetime1; // == <seconds between the two times>
                        $days = $secs / 86400;
                        $ad_total = ((($multiplier * $row['A_Total']) / 30) * $days);
                        if ($ad_total < 0) {
                            $ad_total = 0;
                        }
                        $ads_total += $ad_total;
                    }
                }
                $subtotal = $rowListing['BL_SubTotal'] + $ads_total;
                $subtotal2 = $subtotal - $rowListing['BL_CoC_Dis_2'];
                $tax = round($subtotal2 * .13, 2);
                $total = $subtotal2 + $tax;
                ?>
                <div class="data billing">
                    <div class="title">Credit Card Number</div>
                    <div class="info"><?php echo $cardNo ?></div>
                    <div class="view"><a href="customer-credit-card-details.php?id=<?php echo $BID ?>">Change Credit Card Details</a></div>
                </div>
                <div class="data billing">
                    <div class="title">Billing Type</div>
                    <div class="info"><?php echo $billing_type ?></div>
                </div>
                <div class="data billing">
                    <div class="title">Renewal Date</div>
                    <div class="info"><?php echo $date ?></div>
                </div>
                <div class="data billing">
                    <div class="title">Billing Total</div>
                    <div class="info">$<?php echo number_format(round($total, 2), 2); ?></div>
                </div>

            </div>
        <?php } ?>

        <!--Category Section-->
        <div class="overview">
            <div class="content-header">Categories</div>
            <?php
            foreach ($category as $key => $cat) {
                $sql_category = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_ID = '$cat'";
                $res_category = mysql_query($sql_category);
                $c = mysql_fetch_assoc($res_category);
                $category_rank = calculate_ranking($BL_ID, 'category', $cat, $ranking_region);
                ?>
                <div class="data cat">
                    <div class="title">Category</div>
                    <div class="cat-name"><?php echo $c['C_Name'] ?></div>
                    <div class="reg-name"></div>
                    <div class="rank"><?php echo $category_rank['rank'] . ' of ' . $category_rank['total'] ?></div>
                </div>
                <?php
                $i = 1;
                foreach ($subcategories as $key => $subcat) {
                    $sql_subcat = "SELECT C_ID, C_Parent, C_Name FROM tbl_Category  WHERE C_ID = '" . $subcat . "'AND  C_Parent= '" . $c['C_ID'] . "'"; //  exit();
                    $res_subcat = mysql_query($sql_subcat);
                    if ($res_subcat) {
                        while ($s = mysql_fetch_assoc($res_subcat)) {
                            $subcat_rank = calculate_ranking($BL_ID, 'subcategory', $subcat, $ranking_region);
                            print '<div class="data cat">
                            <div class="title">Sub-Category ' . $i . '</div>
                            <div class="cat-name">' . $s['C_Name'] . '</div>
                            <div class="reg-name"></div>
                            <div class="points">' . $subcat_rank['rank'] . ' of ' . $subcat_rank['total'] . '</div>
                            </div>';
                            $i++;
                        }
                    }
                }
            }
            ?>
        </div>


        <!--My Cart Section-->
        <?php
        if ($rowListing['BL_Listing_Type'] != 5) {
            ?>
            <div class="overview">
                <div class="content-header">My Cart</div>
                <?php
                $sql = "SELECT LT_Name, LT_Cost FROM tbl_Listing_Type WHERE LT_ID = '" . encode_strings($rowListing['BL_Listing_Type'], $db) . "'";
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                $rowLT = mysql_fetch_assoc($result);
                ?>
                <div class="data cart">
                    <div class="title">
                        <?php echo $rowLT['LT_Name'] . ' Listing'; ?>
                    </div>
                    <div class="price">$<?php echo number_format(round($multiplier * $rowLT['LT_Cost'], 2), 2) ?></div>
                    <div class="points"><?php echo $rowListing['BL_Basic_Points']; ?> Points</div>
                </div>

                <?php
                $sql = "SELECT F_File, F_Name, F_Price, F_Points FROM tbl_BL_Feature LEFT JOIN tbl_Feature ON F_ID = BLF_F_ID
                        WHERE BLF_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BLF_Active = 1 AND F_ID NOT IN (3, 12)";
                $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                while ($row = mysql_fetch_assoc($resultTMP)) {
                    ?>
                    <div class="data cart">
                        <div class="title">
                            <a href="<?php echo $row['F_File'] . "?bl_id=" . $BL_ID ?>"><?php echo $row['F_Name'] ?></a>
                        </div>
                        <div class="price">$<?php echo number_format(round($multiplier * $row['F_Price'], 2), 2) ?></div>
                        <?php
                        if ($row['F_Name'] == 'About Us') {
                            $sqlAbt = "SELECT BFA_ID FROM tbl_Business_Feature_About WHERE BFA_BL_ID = '$BL_ID' ORDER BY BFA_Order";
                            $resultAbt = mysql_query($sqlAbt) or die(mysql_error());
                            $numRowsAbt = mysql_num_rows($resultAbt);
                            if ($numRowsAbt > 0) {
                                $class = 'points-com';
                            } else {
                                $class = 'points-uncom';
                            }
                        }
                        //Photo Gallery
                        if ($row['F_Name'] == 'Photo Gallery') {
                            $sqlGal = "SELECT BFP_ID FROM tbl_Business_Feature_Photo	WHERE BFP_BL_ID = '" . encode_strings($BL_ID, $db) . "' ORDER BY BFP_Order";
                            $resultGal = mysql_query($sqlGal, $db) or die("Invalid query: $sqlGal -- " . mysql_error());
                            $numRowsGal = mysql_num_rows($resultGal);
                            if ($numRowsGal > 0) {
                                $class = 'points-com';
                            } else {
                                $class = 'points-uncom';
                            }
                        }
                        //Our Products
                        if ($row['F_Name'] == 'Our Products') {
                            $sqlProd = "SELECT  BFP_ID FROM tbl_Business_Feature_Product WHERE BFP_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                            $resultProd = mysql_query($sqlProd) or die(mysql_error());
                            $numRowsProd = mysql_num_rows($resultProd);
                            if ($numRowsProd > 0) {
                                $class = 'points-com';
                            } else {
                                $class = 'points-uncom';
                            }
                        }
                        //Menu
                        if ($row['F_Name'] == 'Menu') {
                            $sqlMenu = "SELECT BFM_ID FROM tbl_Business_Feature_Menu WHERE BFM_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                            $resultMenu = mysql_query($sqlMenu, $db) or die("Invalid query: $sqlMenu -- " . mysql_error());
                            $numRowsMenu = mysql_num_rows($resultMenu);
                            if ($numRowsMenu > 0) {
                                $class = 'points-com';
                            } else {
                                $class = 'points-uncom';
                            }
                        }
                        //Daily Specials
                        if ($row['F_Name'] == 'Daily Features') {
                            $sqlDS = "SELECT BFDS_ID FROM tbl_Business_Feature_Daily_Specials WHERE BFDS_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                            $resultDS = mysql_query($sqlDS, $db) or die("Invalid query: $sqlDS -- " . mysql_error());
                            $numRowsDS = mysql_num_rows($resultDS);
                            if ($numRowsDS > 0) {
                                $class = 'points-com';
                            } else {
                                $class = 'points-uncom';
                            }
                        }
                        //Entertainment
                        if ($row['F_Name'] == 'Entertainment') {
                            $sqlEnt = "SELECT BFEA_ID FROM tbl_Business_Feature_Entertainment_Acts WHERE BFEA_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                            $resultEnt = mysql_query($sqlEnt) or die(mysql_error());
                            $numRowsEnt = mysql_num_rows($resultEnt);
                            if ($numRowsEnt > 0) {
                                $class = 'points-com';
                            } else {
                                $class = 'points-uncom';
                            }
                        }
                        //Guest Book
                        if ($row['F_Name'] == 'Guest Book') {
                            $sqlGB = "SELECT BFGB_ID FROM tbl_Business_Feature_Guest_Book WHERE BFGB_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BFGB_Status = 1";
                            $resultGB = mysql_query($sqlGB) or die(mysql_error());
                            $numRowsGB = mysql_num_rows($resultGB);
                            if ($numRowsGB > 0) {
                                $class = 'points-com';
                            } else {
                                $class = 'points-uncom';
                            }
                        }
                        //Power Ranking
                        if ($row['F_Name'] == 'Power Ranking') {
                            $class = 'points-com';
                        }
                        //Agendas & Minutes
                        if ($row['F_Name'] == 'Agendas & Minutes') {
                            $sqlAM = "SELECT BFAM_ID FROM tbl_Business_Feature_Agendas_Minutes WHERE BFAM_BL_ID = '" . encode_strings($BL_ID, $db) . "'";
                            $resultAM = mysql_query($sqlAM) or die(mysql_error());
                            $numRowsAM = mysql_num_rows($resultAM);
                            if ($numRowsAM > 0) {
                                $class = 'points-com';
                            } else {
                                $class = 'points-uncom';
                            }
                        }
                        //Manage Coupon
                        if ($row['F_Name'] == 'Manage Coupon') {
                            $sqlMC = "SELECT BFC_ID FROM tbl_Business_Feature_Coupon WHERE BFC_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BFC_Status = 1";
                            $resultMC = mysql_query($sqlMC) or die(mysql_error());
                            $numRowsMC = mysql_num_rows($resultMC);
                            if ($numRowsMC > 0) {
                                $class = 'points-com';
                            } else {
                                $class = 'points-uncom';
                            }
                        }
                        ?>
                        <div class="<?php echo $class ?>"><?php echo $row['F_Points'] . " points" ?></div>
                    </div>
                    <?php
                    $class = '';
                }

                //Advertisements
                $sql = " SELECT A_Title, A_Active_Date, A_Total, AT_Name FROM tbl_Advertisement LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID
                         WHERE A_BL_ID = '" . encode_strings($rowListing['BL_ID'], $db) . "' AND A_Status = 3 AND A_Total > 0
                         AND A_Is_Deleted = 0 AND A_Is_Townasset != 1 AND (A_End_Date > CURDATE() OR A_End_Date = '0000-00-00')";
                $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                $ads_total = 0;
                while ($row = mysql_fetch_assoc($resultTMP)) {
                    $renewalDate = strtotime('-1 month', strtotime($rowListing['BL_Renewal_Date']));
                    $renewalDate = date("Y-m-d", $renewalDate);
                    if ($renewalDate >= $row['A_Active_Date']) {
                        $ad_total = ($multiplier * $row['A_Total']);
                        $ads_total += $ad_total;
                    } else {
                        $datetime1 = strtotime(date('Y-m-d', strtotime($row['A_Active_Date'])));
                        $datetime2 = strtotime(date('Y-m-d', strtotime($rowListing['BL_Renewal_Date'])));

                        $secs = $datetime2 - $datetime1; // == <seconds between the two times>
                        $days = $secs / 86400;
                        $ad_total = ((($multiplier * $row['A_Total']) / 30) * $days);
                        if ($ad_total < 0) {
                            $ad_total = 0;
                        }
                        $ads_total += $ad_total;
                    }
                    if ($ad_total > 0) {
                        ?>
                        <div class="data cart">
                            <div class="title">
                                <?php echo $row['A_Title'] . " - " . $row['AT_Name'] ?>
                            </div>
                            <div class="price">$<?php echo number_format($ad_total, 2) ?></div>
                            <div class="points"></div>
                        </div>
                        <?PHP
                    }
                }

                //Line Item
                if ($rowListing['BL_Line_Item_Title'] != '' && $rowListing['BL_Line_Item_Price'] > 0) {
                    ?>
                    <div class="data cart">
                        <div class="title">
                            <?php echo 'Line item (' . $rowListing['BL_Line_Item_Title'] . ')'; ?>
                        </div>
                        <div class="price">$<?php echo number_format(round($rowListing['BL_Line_Item_Price'], 2), 2); ?></div>
                        <div class="points"></div>
                    </div>
                    <?php
                }
                $subtotal = $rowListing['BL_SubTotal'] + $ads_total;
                $subtotal2 = $subtotal - $rowListing['BL_CoC_Dis_2'];
                $tax = round($subtotal2 * .13, 2);
                $total = $subtotal2 + $tax;
                ?>

                <!--Discount, Taxes and totals-->
                <div class="data cart">
                    <div class="title">
                        Discounts
                    </div>
                    <div class="price">$<?php echo number_format(round($rowListing['BL_CoC_Dis_2'], 2), 2); ?></div>
                    <div class="points"></div>
                </div>
                <div class="data cart">
                    <div class="title">
                        Taxes (13%)
                    </div>
                    <div class="price">$<?php echo number_format(round($tax, 2), 2); ?></div>
                    <div class="points"></div>
                </div>
                <div class="data cart">
                    <div class="title">
                        Totals
                    </div>
                    <div class="price">$<?php echo number_format(round($total, 2), 2); ?></div>
                    <div class="points"><?php echo $rowListing['BL_Points'] . ' points' ?></div>
                </div>
            </div>
            <?php
        }
    }
    ?>

</div>
<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>