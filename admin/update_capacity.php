<?php
include '../include/config.inc.php';
require_once '../include/track-data-entry.php';

if (isset($_POST)) {
    $accom_id = $_POST['accom_id'];
    $closed = isset($_POST['closed']) ? '1' : 0;
    $general = isset($_POST['capacity']) ? $_POST['capacity'] : 0;

    $a1x1 = isset($_POST['accom_1x1']) ? $_POST['accom_1x1'] : 0;
    $a2x2 = isset($_POST['accom_2x2']) ? $_POST['accom_2x2'] : 0;
    $a3x3 = isset($_POST['accom_3x3']) ? $_POST['accom_3x3'] : 0;
    $a4x4 = isset($_POST['accom_4x4']) ? $_POST['accom_4x4'] : 0;
    $a5x5 = isset($_POST['accom_5x5']) ? $_POST['accom_5x5'] : 0;
    $a6x6 = isset($_POST['accom_6x6']) ? $_POST['accom_6x6'] : 0;
    $a7x7 = isset($_POST['accom_7x7']) ? $_POST['accom_7x7'] : 0;
    $a8x8 = isset($_POST['accom_8x8']) ? $_POST['accom_8x8'] : 0;

    $update_query = '';
    $insert_query = '';
    $select_query = "SELECT * FROM `acc_accommodator_capacity` WHERE accommodator_id=$accom_id";
    $exist = mysql_num_rows(mysql_query($select_query));
    if ($exist > 0) {
        $update_query = "UPDATE `acc_accommodator_capacity` SET `general`=$general,`closed`=$closed,`1x1`=$a1x1,`2x2`=$a2x2,`3x3`=$a3x3,`4x4`=$a4x4,`5x5`=$a5x5,`6x6`=$a6x6,`7x7`=$a7x7,`8x8`=$a8x8 WHERE accommodator_id=$accom_id";
        $result = mysql_query($update_query);
        if ($result) {
            // TRACK DATA ENTRY
            Track_Data_Entry('Accomodation', '', 'Accommodator Capacity', $accom_id, 'Update', 'super admin');
            echo 'Capacity Successfully Updated';
        } else {
            echo 'There Occured Some Error, Try Again Please';
        }
    } else {
        $insert_query = "INSERT INTO `acc_accommodator_capacity`(`accommodator_id`, `closed`,general, `1x1`, `2x2`, `3x3`, `4x4`, `5x5`, `6x6`, `7x7`, `8x8`) VALUES ($accom_id,$closed,$general,$a1x1,$a2x2,$a3x3,$a4x4,$a5x5,$a6x6,$a7x7,$a8x8)";
        $result = mysql_query($insert_query);
        if ($result) {
            // TRACK DATA ENTRY
            Track_Data_Entry('Accomodation', '', 'Accommodator Capacity', $accom_id, 'Add', 'super admin');
            echo 'Capacity Successfully Updated';
        } else {
            echo 'There Occured Some Error, Try Again Please!';
        }
    }
}
?>