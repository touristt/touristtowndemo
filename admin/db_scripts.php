<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/admin/header.php';
?>
<?php
$dir = "/home/demotourist/public_html/DB_Scripts/";
// Open a directory, and read its contents
$files = array_diff(scandir($dir), array('.', '..'));


if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'exe' && $_REQUEST['qry'] != '') {
    $sql = $_REQUEST['qry'];
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location:db_scripts.php");
    exit();
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'show') {
   echo $sql = $_REQUEST['qry'];die;
}

?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">DB Scripts</div>
    </div> 
    <div class="left">
        <?PHP
        require '../include/nav-mobile-admin.php';
        ?>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div style="width:50%;" class="data-column spl-name-events padding-none">Script Name</div>
            <div class="data-column spl-other-cc-list padding-none">Execute</div>
        </div>
        <?PHP
        foreach ($files as $file) {
            ?>
            <div class="data-content">
                <?php $qry = file_get_contents('/home/demotourist/public_html/DB_Scripts/' . $file); ?>
                <div style="width:50%;" class="data-column spl-name-events"><a href="db_script_show.php?op=show&amp;qry=<?php echo $qry ?>"><?php echo $file; ?></a></div>      
                <div class="data-column spl-other-cc-list">
                    <a onClick="return confirm('Ar you sure this action can not be undone!');" href="db_scripts.php?op=exe&amp;qry=<?php echo $qry ?>">Execute</a>
                </div>
            </div>
            <?PHP
        }
        ?> 
    </div>
</div>
<?php
require_once '../include/admin/footer.php';
?>