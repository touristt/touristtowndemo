<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
if (!in_array('manage-events', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
    }
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Events - Manage Organizations</div>
        <div class="link">
            <a href="events-organization.php">+ Add Organization</a>
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manageevents.php'; ?>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name-loc">Name</div>
            <div class="data-column padding-none spl-other">Delete</div>
        </div>
        <?PHP
        $sql = "SELECT EO_ID, EO_Name FROM Events_Organization ORDER BY EO_Name";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            ?>
            <div class="data-content">
                <div class="data-column spl-name-loc"><a href="events-organization.php?id=<?php echo $row['EO_ID'] ?>">
                        <?php echo $row['EO_Name'] ?>
                    </a></div>
                <div class="data-column spl-other"><a onClick="return confirm('Are you sure this action can not be undone!');" href="events-organization.php?id=<?php echo $row['EO_ID'] ?>&amp;op=del">Delete</a></div>
            </div>
            <?PHP
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>