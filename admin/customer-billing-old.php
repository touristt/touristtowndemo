<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

$BID = $_SESSION['BUSINESS_ID'];
$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
} else {
    header('Location: index.php');
}

require_once '../include/admin/header.php';
?>
<div class="content-left">

    <?php require_once '../include/top-nav-listing.php'; ?>

    <div class="title-link">
        <div class="title">Invoices - <?php echo  $rowListing['BL_Listing_Title'] ?></div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-billing-old.php'; ?>
    </div>
    <div class="right">
        <div class="billing-inside-div-">
        </div>
        <div class="data-header"> 
            <div class="data-column">Invoice #</div>
            <div class="data-column">Date Paid</div>
            <div class="data-column">Sub Total</div>
            <div class="data-column">Tax</div>
            <div class="data-column">Total</div>
        </div>                             

        <?PHP
        //AND BB_Date >= 2015-06-12 Temporary check.
        $sql = "SELECT * FROM tbl_Business_Billing WHERE BB_BL_ID = '" . encode_strings($BL_ID, $db) . "' AND BB_Deleted = false AND BB_Date < '2015-06-12'
                ORDER BY BB_ID DESC";
        $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $gtotal = $tax = $subTotal = $ch1discount = $bulkdiscount = $discount3 = 0;
        while ($row = mysql_fetch_assoc($resultTMP)) {
            $gtotal += $row['BB_Total'];
            $tax += $row['BB_Tax'];
            $subTotal += $row['BB_SubTotal3'];
            $ch1discount += $row['BB_CoC_Dis_1'];
            $bulkdiscount += $row['BB_Bulk_Dis'];
            $discount3 += $row['BB_CoC_Dis_2'];
            ?>                                  
            <div class="data-content">
                <div class="data-column">#<?php echo  $row['BB_Invoice_Num'] ?></div>
                <div class="data-column"><?php echo  $row['BB_Date'] ?></div>
                <div class="data-column">$<?php echo  $row['BB_SubTotal3'] ?></div>
                <div class="data-column">$<?php echo  $row['BB_Tax'] ?></div>
                <div class="data-column">$<?php echo  $row['BB_Total'] ?></div>
            </div> 
            <?PHP
        }
        ?>        
        <div class="data-header">
            <div class="data-column">Customer Total</div>
            <div class="data-column">&nbsp;</div>
            <div class="data-column">$<?php echo  $subTotal ?></div>
            <div class="data-column">$<?php echo  $tax ?></div>
            <div class="data-column">$<?php echo  $gtotal ?></div>
        </div>
        <div class="data-content">                           
            <div class="data-column">&nbsp;</div>
            <div class="data-column">&nbsp;</div>
            <div class="data-column">&nbsp;</div>
            <div class="data-column">&nbsp;</div>
            <div class="data-column">&nbsp;</div>
        </div>
        <div class="data-content">  
            <div class="data-column">Discount</div>
            <div class="data-column">&nbsp;</div>
            <div class="data-column">&nbsp;</div>
            <div class="data-column">&nbsp;</div>
            <div class="data-column">$<?php echo  $discount3 ?></div>
        </div>
    </div>
</div>

<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>