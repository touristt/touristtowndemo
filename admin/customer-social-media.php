<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('business-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
}

$BL_ID = $_REQUEST['bl_id'];
$points_taken = 0;
$Coupons_direction = $_REQUEST['coupons'];
$bfc_id = $_REQUEST['bfc_id'];
if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
} else {
    header("Location:customers.php");
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    if ($_POST['counter'] > 0) {
        $sql = "UPDATE tbl_Business_Social SET 
                BS_FB_Link = '" . encode_strings($_POST['fb_link'], $db) . "',
                BS_T_Link = '" . encode_strings($_POST['t_link'], $db) . "',
                BS_I_Link = '" . encode_strings($_POST['i_link'], $db) . "'
                WHERE BS_BL_ID = '$BL_ID'";
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Social Media','','Update','super admin');
    } else {
        $sql = "INSERT INTO tbl_Business_Social(BS_BL_ID, BS_FB_Link, BS_T_Link, BS_I_Link) VALUES('" . encode_strings($BL_ID, $db) . "', '" . encode_strings($_POST['fb_link'], $db) . "', '" . encode_strings($_POST['t_link'], $db) . "', '" . encode_strings($_POST['i_link'], $db) . "')";
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing',$id,'Social Media','','Add','super admin');
    }
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
    } else {
        $_SESSION['error'] = 1;
    }
    
    if(isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true){
     header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);   
    }else if(isset($Coupons_direction) && $Coupons_direction == 'editcoupon'){
        
        header("Location:customer-feature-add-coupons.php?bl_id=" . $BL_ID . "&bfc_id=" . $bfc_id . "&re_dir=1");
        exit();
    }
    else
    {
        header("Location: /admin/customer-social-media.php?bl_id=" . $BL_ID);;
    }
    
    exit();
}

require_once '../include/admin/header.php';
?>

<div class="content-left">

    <?php require_once '../include/top-nav-listing.php'; ?>

    <div class="title-link">
        <div class="title">My Page - <?php echo $rowListing['BL_Listing_Title'] ?></div>
        <div class="link">
            <?PHP
            require_once('preview-link.php');
            ?>
        </div> 
        <div class="instruction">
            Fields with this background<span></span>will show on free listings profile.
        </div>
    </div>

    <div class="left">
        <?PHP
        require '../include/nav-mypage.php';
        ?>
    </div>

    <div class="right">

        <form name="form1" method="post" action="">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="bl_id" value="<?php echo $BL_ID ?>">
            <input type="hidden" name="coupons" value="<?php echo $Coupons_direction; ?>">
            <input type="hidden" name="bfc_id" value="<?php echo $bfc_id; ?>">
            <div class="content-header">
                <div class="title">Social Media</div>
                <div class="link">
                    <?php
                    $SM = show_field_points('Hours');
                    if ($rowsSM['BS_FB_Link'] != '' || $rowsSM['BS_T_Link'] != '' || $rowsSM['BS_Y_Link'] || $rowsSM['BS_I_Link']) {
                        echo '<div class="points-com des-heading-point">' . $SM . ' pts</div>';
                        $points_taken = $SM;
                    } else {
                        echo '<div class="points-uncom des-heading-point">' . $SM . ' pts</div>';
                    }
                    ?>
                </div>
            </div>

            <?php
            $help_text = show_help_text('Social Media');
            if ($help_text != '') {
                echo '<div class="form-inside-div" style="border-bottom:3px solid #eeeeee;">' . $help_text . '</div>';
            }
            ?>

            <?PHP
            $sql = "SELECT BS_FB_Link, BS_T_Link, BS_I_Link FROM tbl_Business_Social WHERE BS_BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $count = mysql_num_rows($result);
            $row = mysql_fetch_assoc($result);
            ?> 

            <div class="form-inside-div">
                <label>Facebook Link</label>
                <div class="form-data">
                    <input name="fb_link" type="text" value="<?php echo $row['BS_FB_Link'] ?>" size="50" />
                </div>
            </div>

            <div class="form-inside-div">
                <label>Twitter Link</label>
                <div class="form-data">
                    <input name="t_link" type="text"  value="<?php echo $row['BS_T_Link'] ?>" size="50" />
                </div>
            </div>

            <div class="form-inside-div">
                <label>Instagram Link</label>
                <div class="form-data">
                    <input name="i_link" type="text"  value="<?php echo $row['BS_I_Link'] ?>" size="50" />
                </div>
            </div>

            <div class="form-inside-div">
                <div class="button">
                    <input type="hidden" name="counter" value="<?php echo $count; ?>">
                    <input type="submit" name="button" value="Submit" />
                </div>
            </div>
        </form>

        <div class="form-inside-div listing-ranking border-none">
            Ranking Points: <?php echo $points_taken ?> points out of <?php echo $total_sm_points ?> points
        </div>
    </div>
</div>


<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>