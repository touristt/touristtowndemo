<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';

if (!in_array('business-listings', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) || $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
}

// Filter customers by region
$temp = false;
$regionJoin = '';
$regionWhere = '';
$region_limit = '';
$LTname = '';
$LTjoin = '';
$regionLimit = array();

$regionWhere .= " AND BL_Listing_Type IN (";
if (in_array('free-listings', $_SESSION['USER_PERMISSIONS'])) {
    $regionWhere .= "1";
    $temp = true;
}
if (in_array('business-listings', $_SESSION['USER_PERMISSIONS'])) {
    if ($temp == true) {
        $regionWhere .= ", ";
    }
    $regionWhere .= "4";
    $temp = true;
}
if (in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) {
    if ($temp == true) {
        $regionWhere .= ", ";
    }
    $regionWhere .= "5";
}
$regionWhere .= ")";

if (isset($_REQUEST['type']) && $_REQUEST['type'] != '') {
    $LTname = " AND LT_Name = '" . encode_strings($_REQUEST['type'], $db) . "'";
    $LTjoin = " LEFT JOIN tbl_Listing_Type ON BL_Listing_Type = LT_ID";
}
if ((isset($_REQUEST['sortregion']) && $_REQUEST['sortregion'] != '' && $_REQUEST['sortregion'] != '-1') || $_SESSION['USER_LIMIT'] > 0) {
    if (isset($_SESSION['USER_LIMIT']) && $_SESSION['USER_LIMIT'] != '') {
        $limits = explode(',', $_SESSION['USER_LIMIT']);
    } else {
        $limits = explode(',', $_REQUEST['sortregion']);
    }
    foreach ($limits as $limit) {
        $sql = "SELECT R_ID, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $region = mysql_fetch_assoc($result);
        if (isset($region['R_Type']) && $region['R_Type'] == 1) {
            $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($region['R_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                if ($row['R_ID'] != '' && !in_array($row['R_ID'], $regionLimit)) {
                    $regionLimit[] = $row['R_ID'];
                }
            }
        } else {
            if ($region['R_ID'] != '' && !in_array($region['R_ID'], $regionLimit)) {
                $regionLimit[] = $region['R_ID'];
            }
        }
    }
    $regionLimitCommaSeparated = rtrim(implode(',', $regionLimit), ',');
    $regionJoin = " LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
                    INNER JOIN tbl_Business_Listing_Category_Region ON BL_ID = BLCR_BL_ID";
    $regionWhere .= " AND BLCR_BLC_R_ID IN (" . encode_strings($regionLimitCommaSeparated, $db) . ")";
    if (in_array('bgadmin', $_SESSION['USER_ROLES'])) {
        if (isset($_REQUEST['type']) && $_REQUEST['type'] != 'Free') {
            $regionWhere .= " AND BL_ChamberMember > 0";
        }
        $regionWhere .= " AND BL_Listing_Type IN (1, 4)";
    }
}
if (isset($_GET['op']) && $_GET['op'] == 'del') {

    $sql = "DELETE tbl_Business_Listing, tbl_Business_Listing_Ammenity, 
            tbl_Business_Listing_Category, tbl_Business_Listing_Category_Region,
            tbl_Business_Social, tbl_BL_Feature, payment_profiles,
            tbl_Business_Feature_About, tbl_Business_Feature_About_Photo,
            tbl_Business_Feature_About_Main_Photo, tbl_Business_Feature_Daily_Specials,
            tbl_Business_Feature_Daily_Specials_Description, tbl_Business_Feature_Entertainment_Acts,
            tbl_Business_Feature_Entertainment_Description, tbl_Business_Feature_Guest_Book, tbl_Business_Feature_Guest_Book_Description,
            tbl_Business_Feature_Menu, tbl_Business_Feature_Photo, tbl_Business_Feature_Product, tbl_Business_Feature_Product_Photo,
            tbl_Business_Feature_Coupon, tbl_Business_Feature_Coupon_Category_Multiple, tbl_Image_Bank_Usage
            FROM tbl_Business_Listing 
            LEFT JOIN tbl_Business_Listing_Ammenity ON BLA_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Listing_Category_Region ON BLCR_BLC_ID = BLC_ID 
            LEFT JOIN tbl_Business_Social ON BS_BL_ID = BL_ID
            LEFT JOIN tbl_BL_Feature ON BLF_BL_ID = BL_ID
            LEFT JOIN payment_profiles ON listing_id = BL_ID
            LEFT JOIN tbl_Business_Feature_About ON BFA_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_About_Photo ON BFAP_BFA_ID = BFA_ID
            LEFT JOIN tbl_Business_Feature_About_Main_Photo ON BFAMP_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Daily_Specials ON BFDS_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Daily_Specials_Description ON BFDSD_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Entertainment_Acts ON BFEA_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Entertainment_Description ON BFED_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Guest_Book ON BFGB_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Guest_Book_Description ON tbl_Business_Feature_Guest_Book_Description.BFGB_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Menu ON BFM_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Photo ON BFP_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Product ON tbl_Business_Feature_Product.BFP_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Product_Photo ON BFPP_BFP_ID = tbl_Business_Feature_Product.BFP_ID
            LEFT JOIN tbl_Business_Feature_Coupon ON tbl_Business_Feature_Coupon.BFC_BL_ID = BL_ID
            LEFT JOIN tbl_Business_Feature_Coupon_Category_Multiple ON BFCCM_BFC_ID = BFC_ID
            LEFT JOIN tbl_Image_Bank_Usage ON IBU_BL_ID = BL_ID
            WHERE BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());

    if ($result) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location:" . $_SERVER['HTTP_REFERER']);
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Listings 
            <?php
            if (isset($_REQUEST['type'])) {
                echo " - " . $_REQUEST['type'];
            }
            if (isset($_REQUEST['listings_status']) && $_REQUEST['listings_status'] == 0) {
                echo " - Inactive";
            }
            ?>  </div> 


        <div class="link">
            <form id="frmSort" name="form1" method="GET" action="">
                <?php
                if (isset($_REQUEST['type'])) {
                    $type = $_REQUEST['type'];
                    $name = "type";
                } elseif (isset($_REQUEST['listings_status'])) {
                    $name = "listings_status";
                    $type = $_REQUEST['listings_status'];
                }
                ?>
                <input type="hidden" value="<?php echo $type; ?>" name="<?php echo $name; ?>">
                <label>
                    <span class="addlisting">Filter Listings:</span>
                    <select name="filter_listings" id="filter_listings" onChange="getSubOptions(this.value)">
                        <option value="" <?php echo ($_REQUEST['filter_listings'] == '') ? 'selected' : ''; ?>>Select</option>
                        <option value="no_image" <?php echo ($_REQUEST['filter_listings'] == 'no_image') ? 'selected' : ''; ?>>No Main Image</option>
                        <option value="no_image_thumbnail" <?php echo ($_REQUEST['filter_listings'] == 'no_image_thumbnail') ? 'selected' : ''; ?>>No Thumbnail Image</option>
                        <option value="inactive_listings" <?php echo ($_REQUEST['filter_listings'] == 'inactive_listings') ? 'selected' : ''; ?>>Inactive Listings</option>
                        <option value="no_map" <?php echo ($_REQUEST['filter_listings'] == 'no_map') ? 'selected' : ''; ?>>No Mapping Co-ordinates</option>
                        <option value="zero_billing" <?php echo ($_REQUEST['filter_listings'] == 'zero_billing') ? 'selected' : ''; ?>>Listings With 0 Billing</option>
                        <option value="annual_billing" <?php echo ($_REQUEST['filter_listings'] == 'annual_billing') ? 'selected' : ''; ?>>Listings With Annual Billing</option>
                    </select>
                </label>
                <?php
                if (!isset($_SESSION['USER_LIMIT']) || $_SESSION['USER_LIMIT'] == 0) {
                    $sortregion = (isset($_REQUEST['sortregion'])) ? $_REQUEST['sortregion'] : '';
                    ?>
                    <label>
                        <span class="addlisting">Websites:</span>
                        <select name="sortregion" id="sortregion" onChange="getSubOptions(this.value)">
                            <option value="">Select Website</option>
                            <?php
                            $getRegions = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Domain != ''";
                            $resRegion = mysql_query($getRegions, $db) or die("Invalid query: $getRegions -- " . mysql_error());
                            while ($rowRegion = mysql_fetch_array($resRegion)) {
                                ?>
                                <option value="<?php echo $rowRegion['R_ID'] ?>" <?php echo ($sortregion == $rowRegion['R_ID']) ? 'selected="selected"' : ''; ?>><?php echo $rowRegion['R_Name'] ?></option>
                            <?php } ?>
                            <option value="-1">All</option>
                        </select>
                    </label>
                <?php } ?>
                <label>
                    <span class="addlisting">Sort:</span>
                    <select name="sortby" id="sortby" onChange="getSubOptions(this.value)">
                        <option value="" selected>Sort by:</option>
                        <?PHP
                        $sortby = isset($_REQUEST['sortby']) ? $_REQUEST['sortby'] : '';
                        $sql = "SELECT C_ID, C_Name FROM tbl_Category WHERE C_Parent = 0 AND C_Is_Blog != 1   AND C_Is_Product_Web != 1 ORDER BY C_Name";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        $js = '';
                        while ($row = mysql_fetch_assoc($result)) {
                            $sql = "SELECT C_ID, C_Name FROM tbl_Region_Category 
                                    LEFT JOIN tbl_Category ON RC_C_ID = C_ID
                                    WHERE C_Parent = '" . encode_strings($row['C_ID'], $db) . "'";
                            $sql .= $regionLimitCommaSeparated ? " AND RC_R_ID IN ($regionLimitCommaSeparated)" : "";
                            $sql .= " GROUP BY C_ID ORDER BY C_Order";
                            $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            $js .= "subSort[" . $row['C_ID'] . "] = \"<option value='' selected>Sort by:</option>";
                            while ($rowTMP = mysql_fetch_assoc($resultTMP)) {
                                $selected = (($_REQUEST['sortbySub'] == $rowTMP['C_ID']) ? 'selected=\"selected\"' : '');
                                $js .= "<option value='" . $rowTMP['C_ID'] . "' " . $selected . ">" . $rowTMP['C_Name'] . "</option>";
                            }
                            $js .= "<option value='-1'>List All</option>";
                            $js .= "\"\n";
                            ?>
                            <option value="<?php echo $row['C_ID'] ?>" <?php echo ($sortby == $row['C_ID']) ? 'selected="selected"' : ''; ?>><?php echo $row['C_Name'] ?></option>
                            <?PHP
                        }
                        ?>
                        <option value="upcoming" <?php echo ($sortby == 'upcoming') ? 'selected="selected"' : ''; ?>>Upcoming Renewals</option>
                        <option value="points" <?php echo ($sortby == 'points') ? 'selected="selected"' : ''; ?>>Points</option>
                    </select>
                </label>
                <select name="sortbySub" id="sortbySub" onChange="$('#frmSort').submit();" style="display:none;">
                    <option value="" selected>Sort by:</option>
                </select>

                <script type="text/javascript">
                    var subSort = new Array();
<?php echo $js ?>
                    var selectionArray = new Array('upcoming', 'points', 'noncoc', 'Basic', 'Enhanced', 'Town Asset', 1, 3, 4, 6, 8, '-1');
                    $(document).ready(function () {
                        if ($('#sortby').val() != '' && !isNaN($('#sortby').val())) {
                            $('#sortbySub').html(subSort[$('#sortby').val()]);
                            $('#sortbySub').show();
                        }
                    });
                    function getSubOptions(myVal) {
                        $('#frmSort').submit();
                    }
                    function getSubmitForm(myVal) {
                        $('#formSrchLstng').submit();
                    }
                    function inArray(needle, haystack) {
                        var length = haystack.length;
                        for (var i = 0; i < length; i++) {
                            if (haystack[i] == needle)
                                return true;
                        }
                        return false;
                    }
                </script>
            </form>
            <form class="export_form_width" method="post" name="export_form" action="export_listings-data.php">
                <input type="hidden" name="filter_listings" value="<?php echo $_REQUEST['filter_listings'] ?>">
                <input type="hidden" name="sortregion" value="<?php echo $_REQUEST['sortregion'] ?>">
                <input type="hidden" name="sortby" value="<?php echo $_REQUEST['sortby'] ?>">
                <input type="hidden" name="sortbySub" value="<?php echo $_REQUEST['sortbySub'] ?>">
                <input type="hidden" name="strSearch" value="<?php echo $_REQUEST['strSearch'] ?>">
                <input type="hidden" name="strSearch_contact" value="<?php echo $_REQUEST['strSearch_contact'] ?>">
                <input type="hidden" value="<?php echo $type; ?>" name="<?php echo $name; ?>">
                <input type="submit" name="export_stats" value="Export"/>
            </form>
        </div>
    </div>
    <div class="left">
        <?PHP
        require '../include/nav-managecustomers.php';
        ?>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column spl-name-events padding-none">Name</div>
            <div class="data-column spl-other-cc-list padding-none">Type</div>
            <div class="data-column spl-other-cc-list padding-none">Points</div>
            <div class="data-column spl-other-cc-list padding-none">Renewal</div>
            <div class="data-column spl-other-cc-list padding-none">Edit</div>
            <div class="data-column spl-other-cc-list padding-none">Delete</div>
        </div>
        <?PHP
        $sql = "SELECT DISTINCT BL_ID, BL_Listing_Title, BLP_Photo, BLP_Header_Image, BL_Listing_Type, BL_Points, DATEDIFF(BL_Renewal_Date, CURDATE()) as timeRemaining 
                FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
                LEFT JOIN tbl_Business_Listing_Photo ON BLP_BL_ID = BL_ID
                ";
        if (strlen($_REQUEST['strSearch']) > 3) {
            $where = " WHERE BL_Listing_Title LIKE '%" . encode_strings($_REQUEST['strSearch'], $db) . "%' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) " . $LTname;
            $groupBy = " GROUP BY BL_ID";
            $orderBy = " ORDER BY TRIM(BL_Listing_Title)";
        } else if (strlen($_REQUEST['strSearch_contact']) > 3) {
            $where = " WHERE BL_Contact LIKE '%" . encode_strings($_REQUEST['strSearch_contact'], $db) . "%' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) " . $LTname;
            $groupBy = " GROUP BY BL_ID";
            $orderBy = " ORDER BY TRIM(BL_Listing_Title)";
        } elseif ($_REQUEST['sortby'] == 'upcoming') {
            $where = " WHERE DATEDIFF(B_Renewal_Date, CURDATE()) < 90 AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) " . $LTname;
            $groupBy = " GROUP BY BL_ID";
            $orderBy = " ORDER BY timeRemaining DESC";
        } elseif ($_REQUEST['sortby'] == 'points') {
            if (isset($_REQUEST['type']) && $_REQUEST['type'] != '') {
                $where = " WHERE LT_Name = '" . encode_strings($_REQUEST['type'], $db) . "' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY))";
            } else {
                $where = " WHERE (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = 1 ";
            }
            $groupBy = " GROUP BY BL_ID";
            $orderBy = " ORDER BY BL_Points DESC";
        } elseif ($_REQUEST['sortby'] == 'noncoc') {
            $where = " WHERE BL_ChamberMember != 1 AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY))" . $LTname;
            $groupBy = " GROUP BY BL_ID";
            $orderBy = " ORDER BY BL_Points DESC";
        } elseif (is_numeric($_REQUEST['sortby']) && $_REQUEST['sortby'] > 0) {
            if ($_REQUEST['sortbySub'] == -1 || $_REQUEST['sortbySub'] == '') {
                if ($regionJoin == '') {
                    $regionJoin = " LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID";
                }
                $where = " WHERE BLC_M_C_ID = '" . encode_strings($_REQUEST['sortby'], $db) . "' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY))" . $LTname;
                $groupBy = " GROUP BY BL_ID";
                $orderBy = " ORDER BY TRIM(BL_Listing_Title)";
            } else {
                //check if there is already region selected to avoid multiple times joinging with tbl_Business_Listing_Category
                if ($regionJoin == '') {
                    $regionJoin = " LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID";
                }
                $where = " WHERE BLC_C_ID = '" . encode_strings($_REQUEST['sortbySub'], $db) . "' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY))" . $LTname;
                $groupBy = " GROUP BY BL_ID";
                $orderBy = " ORDER BY TRIM(BL_Listing_Title)";
            }
        } else {
            if (isset($_REQUEST['type']) && $_REQUEST['type'] != '') {
                $where = " WHERE LT_Name = '" . encode_strings($_REQUEST['type'], $db) . "' AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY))";
            } else {
                $where = " WHERE (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY))";
            }
            $groupBy = " GROUP BY BL_ID";
            $orderBy = " ORDER BY TRIM(BL_Listing_Title)";
        }
        // For listings with no thumbnail images
        if (isset($_REQUEST['filter_listings']) && $_REQUEST['filter_listings'] == 'no_image_thumbnail') {
            $where .= " AND (BLP_Photo IS NULL OR BLP_Photo = '')";
        }
        // For listings with no  main images
        if (isset($_REQUEST['filter_listings']) && $_REQUEST['filter_listings'] == 'no_image') {
            $where .= " AND (BLP_Header_Image IS NULL OR BLP_Header_Image = '')";
        }
        // For listings with no map details
        if (isset($_REQUEST['filter_listings']) && $_REQUEST['filter_listings'] == 'no_map') {
            $where .= " AND (BL_Lat = '' OR BL_Long = '')";
        }
        // For listings with no map details
        if (isset($_REQUEST['filter_listings']) && $_REQUEST['filter_listings'] == 'zero_billing') {
            $where .= " AND BL_Total = '0.00'";
        }
        // For listings with no map details
        if (isset($_REQUEST['filter_listings']) && $_REQUEST['filter_listings'] == 'annual_billing') {
            $where .= " AND BL_Billing_Type = -1";
        }
        // For listings Inactive
        if (isset($_REQUEST['filter_listings']) && $_REQUEST['filter_listings'] == "inactive_listings") {
            $where .= " AND hide_show_listing = 0";
        } else {
            if (!isset($_REQUEST['strSearch']) && !isset($_REQUEST['strSearch_contact'])) {
                $where .= " AND hide_show_listing = 1";
            }
        }
        if (isset($_REQUEST['map'])) {
            $where .= " AND (BL_Lat = '' OR BL_Long = '')";
        }
       $sql .= $regionJoin . $LTjoin . $where . $regionWhere . $groupBy . $orderBy;
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $pages = new Paginate(mysql_num_rows($result), 100); //print_r($sql);
        $result = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            ?>
            <div class="data-content">
                <div class="data-column spl-name-events"><a href="customer-listing-overview.php?bl_id=<?php echo $row['BL_ID'] ?>"><?php echo $row['BL_Listing_Title'] ?></a></div>
                <div class="data-column spl-other-cc-list">
                    <?php
                    $sql_list_type = "SELECT * FROM tbl_Listing_Type ORDER BY LT_Order";
                    $result_list_type = mysql_query($sql_list_type, $db) or die("Invalid query: $sql_list_type -- " . mysql_error());
                    while ($row_list_type = mysql_fetch_assoc($result_list_type)) {
                        if ($row['BL_Listing_Type'] == $row_list_type['LT_ID']) {
                            echo $row_list_type['LT_Name'];
                        }
                    }
                    ?></div>
                <div class="data-column spl-other-cc-list"><?php echo $row['BL_Points'] ?></div>
                <div class="data-column spl-other-cc-list"><?php echo $row['timeRemaining'] ?></div>
                <div class="data-column spl-other-cc-list"><a href="customer-listing-overview.php?bl_id=<?php echo $row['BL_ID'] ?>">Edit</a></div>
                <div class="data-column spl-other-cc-list">
                    <a onClick="return confirm('Ar you sure this action can not be undone!');" href="listings.php?op=del&amp;bl_id=<?php echo $row['BL_ID'] ?>">Delete</a>
                </div>
            </div>
            <?PHP
        }
        ?> 

        <?php
        if (isset($pages)) {
            echo $pages->paginate();
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>