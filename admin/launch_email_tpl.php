<?php
include '../include/config.inc.php';
include '../include/accommodation_functions.php';
require_once '../include/login.inc.php';
require_once '../include/admin/header.php';
include '../include/accommodation_header.php';
/* 1 for activation email and 2 for launch */
$result = activation_and_launch_emails(2);
?>
<?php if (isset($_SESSION['message'])) { ?>

    <div class="message">
        <?php
        echo $_SESSION['message'];
        unset($_SESSION['message']);
        ?>
    </div>

<?php } ?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">
            Launch Email Template
        </div>
    </div>
    <div class="left">
        <?PHP
        require '../include/nav-B-admin-email.php';
        ?>
    </div>
    <div class="right">
        <form name="launch_email_tpl" method="post" action="launch_email_tpl_action.php">
            <!--email image-->
            <div class="form-inside-div form-inside-div-width-admin-accommodation"> 
                <label>Body Copy</label>
                <div class="form-data"><textarea class="ckeditor" name="body_txt"><?php
        if (isset($result->footer_txt)) {
            echo $result->body_txt;
        }
        ?></textarea>
                </div>
            </div>
            <!--email footer-->
            <div class="form-inside-div form-inside-div-width-admin-accommodation"> 
                <label>Footer</label>
                <div class="form-data"><textarea class="ckeditor" name="footer_txt"><?php
                        if (isset($result->footer_txt)) {
                            echo $result->footer_txt;
                        }
        ?></textarea></div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin-accommodation ">
                <div class="button">
                    <input type="hidden" name="template_id" value="<?php
                        if (isset($result->id)) {
                            echo $result->id;
                        }
        ?>"/>
                    <input type="submit" name="launch_email_btn" value="Submit"/>
                </div>
            </div>
        </form>
    </div>
</div>
<!--pending blocks ends-->

<?php include 'include/footer.php'; ?>