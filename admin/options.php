<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('manage-cart-items', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['op'] == 'del') {
    $id = $_REQUEST['id'];
    $sql = sprintf("DELETE FROM tbl_Listing_Option WHERE LO_ID = '%d'", $id);
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
    }else{
        $_SESSION['delete_error'] = 1;
    }
    header('Location: options.php');
    exit();
}

// saving option
if ($_POST['op'] == 'save') {
    $item_name = $_POST['item_name'];
    $item_price = $_POST['item_price'];
    $item_id = $_POST['item_id'];
    if ($item_id > 0) {
        $sql = sprintf("UPDATE tbl_Listing_Option SET LO_Name='%s', LO_Cost='%s' WHERE LO_ID = '%d'", $item_name, $item_price, $item_id);
    } else {
        $sql = sprintf("INSERT INTO tbl_Listing_Option (LO_Name, LO_Cost) VALUES('%s', '%d')", $item_name, $item_price);
    }
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
    }else{
        $_SESSION['error'] = 1;
    }
    header('Location: options.php');
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Store - Listing Options</div>
        <div class="addlisting"><a href="options.php?newItem=1" class="table-boldtext">+Add Listing Option</a></div>
        <div class="link">
        </div>
    </div>        
    <div class="left"><?PHP require '../include/nav-cart.php'; ?></div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name-store">Item Name</div>
            <div class="data-column padding-none spl-other">Price</div>
            <div class="data-column padding-none spl-other">Save</div>
            <div class="data-column padding-none spl-other">Delete</div>
        </div>
        <ul class="brief">
            <?PHP
            $sql_options = "SELECT * FROM tbl_Listing_Option ORDER by LO_Order ASC";
            $result_options = mysql_query($sql_options, $db) or die("Invalid query: $sql_options -- " . mysql_error());
            while ($option = mysql_fetch_assoc($result_options)) {
                ?>
                <li class="option-width" id="recordsArray_<?php echo $option['LO_ID'] ?>">
                    <form action="" method="POST">
                        <div class="data-content">
                            <div class="data-column spl-name-store"><input type="text" name="item_name" size="40" value="<?= $option['LO_Name'] ?>" /></div>
                            <div class="data-column spl-other">$<input type="text" name="item_price" size="10" value="<?= $option['LO_Cost'] ?>" /></div>
                            <div class="data-column spl-other padding-none">
                                <input type="hidden" name="item_id" value="<?= $option['LO_ID'] ?>" />
                                <input type="hidden" name="op" value="save" />
                                <input type="submit" name="save_item" value="Save" />
                            </div>
                            <div class="data-column spl-other">
                                <a onClick="return confirm('Are you sure this action can not be undone!');" href="options.php?op=del&id=<?= $option['LO_ID'] ?>">Delete</a>
                            </div>
                        </div>
                    </form>
                </li>
                <?PHP
            }
            ?>
        </ul>
        <?php
        if (isset($_REQUEST['newItem']) && $_REQUEST['newItem'] == 1) {
            ?>
            <form action="" method="POST">
                <div class="data-content">
                    <div class="data-column spl-name-store"><input type="text" name="item_name" size="40" value="" /></div>
                    <div class="data-column spl-other">$<input type="text" name="item_price" size="10" value="" /></div>
                    <div class="data-column spl-other">
                        <input type="hidden" name="item_id" value="0" />
                        <input type="hidden" name="op" value="save" />
                        <input type="submit" name="save_item" value="Save" />
                    </div>
                    <div class="data-column spl-other"></div>
                </div>
            </form>
            <?PHP
        }
        ?>
    </div>
    <div id="dialog-message"></div>
</div>
<script type="text/javascript">
    $(function() {
        $("ul.brief").sortable({opacity: 0.9, cursor: 'move', update: function() {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Listing_Option&field=LO_Order&id=LO_ID';
                $.post("reorder.php", order, function(theResponse) {
                    $("#dialog-message").html("Options Re-Ordered");
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            Ok: function() {
                                $(this).dialog("close");
                            }
                        }
                    });
                });
            }
        });
    });
</script>
<?PHP
require_once '../include/admin/footer.php';
?>