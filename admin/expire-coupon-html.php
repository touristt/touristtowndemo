<?php
require_once '../include/config.inc.php';
require_once '../include/PHPMailer/class.phpmailer.php';

$NewDate = date('Y-m-d');

$sql = "SELECT BL_ID, BFC_ID, BL_Contact, R_Domain, B_Email, BFC_Expiry_Date, BFC_Title, BL_Email, BL_Listing_Title FROM tbl_Business_Feature_Coupon
        LEFT JOIN tbl_Business_Listing ON BFC_BL_ID = BL_ID
        LEFT JOIN tbl_Business ON BL_B_ID = B_ID
        LEFT JOIN tbl_Business_Listing_Category_Region ON BL_ID = BLCR_BL_ID
        LEFT JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
        WHERE BFC_Status = 1 AND BFC_Expiry_Date > CURDATE() AND BFC_Expiry_Date != '0000-00-00'";
$result = mysql_query($sql);
//while ($row = mysql_fetch_array($result)) {
$row = mysql_fetch_array($result);
    $no_of_days = strtotime($NewDate);
    $cardExpire = strtotime($row['BFC_Expiry_Date']);
    $total_days = $cardExpire - $no_of_days;
    $days = $total_days / 86400;
    $message = '    
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Expiring Coupon Notification</title>
            <body>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center"><table width="600" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td bgcolor="#FFFFFF">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="color: #858585;">
                                            <tr>
                                                <td colspan="2" style="border-bottom: 5px solid #f6f6f6; width: 100%; padding-bottom: 25px; ">
                                                    <span style="float: left;"><img src="http://' . DOMAIN . '/images/TouristTown-Logo.gif" width="195" height="43" /></span>
                                                    <span style="float: right; margin-top: 9px;">Expiring Coupon Notification</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                <p> Your coupon(' . $row['BFC_Title'] . ') on <a target="_blank" href="http://couponcountry.ca">CouponCountry.ca</a> is about to expire. If you would like to continue your coupon <a target="_blank" href="http://mycoupon.touristtown.ca/login.php">log In</a>. <br> <br>If you choose to not continue your coupon, it will be stored in your listing account and not be shown on <a target="_blank" href="http://couponcountry.ca">CouponCountry.ca </a> unless reactivated.</p>
                                                
                                                </td>
                                            </tr>
                                            <tr>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="middle" style="border-bottom: 5px solid #f6f6f6; padding-bottom: 25px; "></td>
                                                <td align="right" valign="middle" style="border-bottom: 5px solid #f6f6f6; padding-right: 10px; font-size: 15px;"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="font-size: 12px;">
                                                    
                                                    <p>&nbsp;</p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                             </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table></td>
                    </tr>
                </table>
            </body>
    </html>';
     ob_start();
   include 'email-inlined.php';
    $html = ob_get_contents(); print_r($message); exit();
    ob_clean();
    $emailTo = $row['B_Email'];
    if ($days == 3) {
        $mail = new PHPMailer();
        $mail->From = MAIN_CONTACT_EMAIL;
        $mail->AddReplyTo(MAIN_CONTACT_EMAIL, 'Tourist Town');
        $mail->FromName = MAIN_CONTACT_NAME;
        $mail->IsHTML(true);
        $mail->AddAddress($emailTo);
        $mail->Subject = 'Coupon Expiry Notification';
        $mail->MsgHTML($html);
        $mail->CharSet = 'UTF-8';
//        $mail->Send();
    }
//}
?>