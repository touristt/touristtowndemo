<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
//$regionLimit = '0';
if (!in_array('manage-events', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
if (!in_array('superadmin', $_SESSION['USER_ROLES'])) {
    if ($_SESSION['USER_LIMIT'] > 0) {
        $sql = "SELECT R_Parent, R_ID FROM tbl_Region WHERE R_ID = '" . encode_strings($_SESSION['USER_LIMIT'], $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $region = mysql_fetch_assoc($result);
        if ($region['R_Parent'] == 0) {
            $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($region['R_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $first = true;
            $regionLimit .= "IN (";
            while ($row = mysql_fetch_assoc($result)) {
                if ($first) {
                    $first = false;
                } else {
                    $regionLimit .= ",";
                }
                $regionLimit .= $row['R_ID'];
            }
            $regionLimit .= ")";
        } else {
            $regionLimit = " = " . $region['R_ID'];
        }
    }
}
if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $sql = "SELECT Recuring, EventID,E_Region_ID,Title, E_Region_ID, EventType, EventDateStart, EventDateEnd, EventTimeAllDay, EventStartTime, EventID, EventTimeAllDay, EventEndTime, Event_Facebook_link, ShortDesc, Content, LocationID, LocationList, StreetAddress, OrganizationID, Organization, ContactName, ContactPhone, ContactPhoneTollFree, Email, WebSiteLink, Pending FROM Events_master WHERE EventID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
    $recuring = explode(",", $row['Recuring']);
}
if ($_POST['op'] == 'save') {
    if ($_REQUEST['pending'] == 2) {
        if ($_REQUEST['id'] > 0) {
            $sql = "UPDATE Events_master SET Pending = 2
						WHERE EventID = '" . encode_strings($_REQUEST['id'], $db) . "'";
            $result = mysql_query($sql, $db);
        }
        if ($result) {
            $_SESSION['delete'] = 1;
        } else {
            $_SESSION['delete_error'] = 1;
        }
        header("Location: events.php");
        exit();
    } else {
        $weekdays = array();
        $weekdays[] .= ($_REQUEST['weekdays-mon'] > 0) ? $_REQUEST['weekdays-mon'] : "0";
        $weekdays[] .= ($_REQUEST['weekdays-tue'] > 0) ? $_REQUEST['weekdays-tue'] : "0";
        $weekdays[] .= ($_REQUEST['weekdays-wed'] > 0) ? $_REQUEST['weekdays-wed'] : "0";
        $weekdays[] .= ($_REQUEST['weekdays-thu'] > 0) ? $_REQUEST['weekdays-thu'] : "0";
        $weekdays[] .= ($_REQUEST['weekdays-fri'] > 0) ? $_REQUEST['weekdays-fri'] : "0";
        $weekdays[] .= ($_REQUEST['weekdays-sat'] > 0) ? $_REQUEST['weekdays-sat'] : "0";
        $weekdays[] .= ($_REQUEST['weekdays-sun'] > 0) ? $_REQUEST['weekdays-sun'] : "0";
        $weeks = implode(',', $weekdays);
        $community = implode(',', $_REQUEST['community']);
        if ($_REQUEST['all_day'] == '1') {
            $timing = "EventTimeAllDay = '" . encode_strings("1", $db) . "', ";
        } else {
            $timing = "
                EventTimeAllDay = '" . encode_strings("0", $db) . "', 
                EventStartTime = '" . encode_strings($_REQUEST['starttime'], $db) . "', 
                EventEndTime = '" . encode_strings($_REQUEST['endtime'], $db) . "', 
            ";
        }
        $sql = "Events_master SET 
					E_Region_ID = '" . encode_strings($community, $db) . "',
					Pending = '" . encode_strings($_REQUEST['pending'], $db) . "', 
					Title = '" . encode_strings($_REQUEST['title'], $db) . "', 
					EventType = '" . encode_strings($_REQUEST['type'], $db) . "', 
                    $timing
					ShortDesc = '" . encode_strings(substr($_REQUEST['shortdesc'], 0, 95), $db) . "', 
					Content = '" . encode_strings(substr($_REQUEST['content'], 0, 700), $db) . "', 
					EventDateStart = '" . encode_strings($_REQUEST['startdate'], $db) . "', 
					EventDateEnd = '" . encode_strings($_REQUEST['enddate'], $db) . "', 
					LocationID = '" . encode_strings($_REQUEST['setLocationID'], $db) . "', 
					LocationList = '" . encode_strings($_REQUEST['location'], $db) . "', 
					StreetAddress = '" . encode_strings($_REQUEST['address'], $db) . "', 
					OrganizationID = '" . encode_strings($_REQUEST['setOrganizationID'], $db) . "', 
					Organization = '" . encode_strings($_REQUEST['organization'], $db) . "', 
                    Recuring = '" . encode_strings($weeks, $db) . "', 
					ContactName = '" . encode_strings($_REQUEST['contact'], $db) . "', 
					ContactPhone = '" . encode_strings($_REQUEST['phone'], $db) . "', 
					ContactPhoneTollFree = '" . encode_strings($_REQUEST['tollfree'], $db) . "', 
					Email = '" . encode_strings($_REQUEST['email'], $db) . "', 
					Event_Facebook_link = '" . encode_strings($_REQUEST['facebook_link'], $db) . "', 
					WebSiteLink = '" . encode_strings($_REQUEST['website'], $db) . "'";
//        require '../include/picUpload.inc.php';
//    if ($_POST['image_bank_pic'] == "") {
//        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 25);
//        if (is_array($pic)) {
//            $sql .= ", Event_Image = '" . encode_strings($pic['0']['0'], $db) . "'";
//        }
//        $pic_id = $pic['1'];
//    } else {
//        $pic_id = $_POST['image_bank_pic'];
//        // last @param 14 = Category Icon Images
//        $pic_response = Upload_Pic_Library($pic_id, 25);
//        if ($pic_response) {
//            $sql .= ", Event_Image = '" . encode_strings($pic_response['0'], $db) . "'";
//        }
//    }


        if ($_REQUEST['id'] > 0) {
            $sql = "UPDATE " . $sql . " WHERE EventID = '" . encode_strings($_REQUEST['id'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $id = $_REQUEST['id'];
        } else {
            $sql = "INSERT " . $sql;
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $id = mysql_insert_id();
        }
        //Check event exist
//        $sqlEvent = "SELECT * FROM Events_master";
//        $resultEvent = mysql_query($sqlEvent, $db)
//                or die("Invalid query: $sqlEvent -- " . mysql_error());
//        while ($rowEvent = mysql_fetch_assoc($resultEvent)) {
//            if ($rowEvent['Title'] == $_REQUEST['title']) {
//                $_SESSION['error'] = 2;
//                if ($_REQUEST['id'] > 0) {
//                    header("Location: event.php?id=".$_REQUEST['id']);
//                    exit();
//                } else {
//                    header("Location: event.php");
//                    exit();
//                }
//            }
//        }
        if ($result) {
            $_SESSION['success'] = 1;
//            if ($pic_id > 0) {
//            //Image usage from image bank.
//            imageBankUsage($pic_id, 'IBU_Event_ID', $id, '', '');
//        }
        } else {
            $_SESSION['error'] = 1;
        }

        if ($_REQUEST['id'] > 0) {
            $id = $_REQUEST['id'];
        } else {
            $id = mysql_insert_id($db);
        }


        header("Location: events.php");
        exit();
    }
}
if ($_GET['op'] == 'del') {
    $event_id = $_REQUEST['event_id'];
    $update = "UPDATE Events_master SET Event_Image = '' WHERE EventID = '" . encode_strings($_REQUEST['event_id'], $db) . "'";
    $result = mysql_query($update);
    if ($result) {
        imageBankUsageDelete('IBU_Event_ID', $event_id, '', '');
    } else {
        
    }
    header('location: event.php?id=' . $event_id);
    exit;
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Events<?php echo ($row['Title'] != '') ? " - " . $row['Title'] : ""; ?></div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manageevents.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">Event Information</div>
        <form onSubmit="return validateEventForm();" enctype="multipart/form-data" name="form1" method="post" action="/admin/event.php">
            <input type="hidden" name="id" value="<?php echo $row['EventID'] ?>">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="regionID" value="<?php echo $row['E_Region_ID'] ?>">
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Event Title</label>
                <div class="form-data">
                    <input name="title" type="text" id="title" value="<?php echo $row['Title'] ?>" size="50"  />
                </div>
            </div>

            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Community</label>
                <div class="form-data">
                    <div id="childRegion"> 
                        <?PHP
                        if (!in_array('superadmin', $_SESSION['USER_ROLES'])) {
                            $sqlCom = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Parent != 0 and R_ID " . encode_strings($regionLimit, $db) . "  ORDER BY R_Name";
                        } else {
                            $sqlCom = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Parent != 0 ORDER BY R_Name";
                        }
                        $resultRegionList = mysql_query($sqlCom, $db) or die("Invalid query: $sqlCom -- " . mysql_error());
                        $region_list = explode(',', $row['E_Region_ID']);
                        while ($rowRList = mysql_fetch_assoc($resultRegionList)) {
                            echo '<div class="childRegionCheckbox"><input type="checkbox" class="community_select" name="community[]" value="' . $rowRList['R_ID'] . '"';
                            echo (in_array($rowRList['R_ID'], $region_list)) ? 'checked' : '';
                            echo '>' . $rowRList['R_Name'] . "</div>";
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Event Type</label>
                <div class="form-data">
                    <select name="type" id="eventtype">
                        <option value="0">Select One</option>
                        <option value="3" <?php echo '3' == $row['EventType'] ? 'selected' : '' ?>>Weekly Events</option>
                        <option value="5" <?php echo '5' == $row['EventType'] ? 'selected' : '' ?>>Special Events</option>
                    </select>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Upload PDF</label>
                <div class="form-data image-bank-browse image_bank_width_browse">
                    <div class="inputWrapper adv-photo float-left">Browse
                        <input id="imageInput filename" class="setting-name fileInput file_ext0 required_title0" accept="application/pdf"  type="file" name="file"  onchange="show_file_name(this.files[0].name, 1)" multiple>
                    </div>
                    <input id="uploadFile1" class="uploadFileName" disabled>
                </div>
            </div>
            <!--  -->
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Start Date</label>
                <div class="form-data">
                    <input class="datepicker" name="startdate" type="text" id="startdate" value="<?php echo $row['EventDateStart'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>End Date</label>
                <div class="form-data">
                    <input class="datepicker" name="enddate" type="text" id="enddate" value="<?php echo $row['EventDateEnd'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Recuring</label>
                <div class="weekDays-selector">
                    <input type="checkbox" id="weekday-mon" name="weekdays-mon" value="<?php echo ($recuring[0] == 1) ? 1 : 0 ?>" <?php echo ($recuring[0] == 1) ? 'checked' : '' ?> class="weekday" />
                    <label class="weekday-mon" for="weekday-mon">M</label>
                    <input type="checkbox" id="weekday-tue" name="weekdays-tue" value="<?php echo ($recuring[1] == 1) ? 1 : 0 ?>" <?php echo ($recuring[1] == 1) ? 'checked' : '' ?> class="weekday" />
                    <label class="weekday-tue" for="weekday-tue">T</label>
                    <input type="checkbox" id="weekday-wed" name="weekdays-wed" value="<?php echo ($recuring[2] == 1) ? 1 : 0 ?>" <?php echo ($recuring[2] == 1) ? 'checked' : '' ?> class="weekday" />
                    <label class="weekday-wed" for="weekday-wed">W</label>
                    <input type="checkbox" id="weekday-thu" name="weekdays-thu" value="<?php echo ($recuring[3] == 1) ? 1 : 0 ?>" <?php echo ($recuring[3] == 1) ? 'checked' : '' ?> class="weekday" />
                    <label class="weekday-thu" for="weekday-thu">T</label>
                    <input type="checkbox" id="weekday-fri" name="weekdays-fri" value="<?php echo ($recuring[4] == 1) ? 1 : 0 ?>" <?php echo ($recuring[4] == 1) ? 'checked' : '' ?> class="weekday" />
                    <label class="weekday-fri" for="weekday-fri">F</label>
                    <input type="checkbox" id="weekday-sat" name="weekdays-sat" value="<?php echo ($recuring[5] == 1) ? 1 : 0 ?>" <?php echo ($recuring[5] == 1) ? 'checked' : '' ?> class="weekday" />
                    <label class="weekday-sat" for="weekday-sat">S</label>
                    <input type="checkbox" id="weekday-sun" name="weekdays-sun" value="<?php echo ($recuring[6] == 1) ? 1 : 0 ?>" <?php echo ($recuring[6] == 1) ? 'checked' : '' ?> class="weekday" />
                    <label class="weekday-sun" for="weekday-sun">S</label>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Timing</label>
                <div class="form-data">
                    <select name="starttime" id="starttime" style="float: left; width: 100px; <?php echo ($row['EventTimeAllDay'] == 1) ? 'display:none' : '' ?>">
                        <option value="">Start Time</option>
                        <option value="00:00:00" <?php echo ($row['EventStartTime'] == '00:00:00' && $row['EventID']) ? 'selected' : '' ?>>N/A</option>
                        <option value="00:00:01" <?php echo $row['EventStartTime'] == '00:00:01' ? 'selected' : '' ?>>Dusk</option>
                        <?PHP
                        $theTime = substr($row['EventStartTime'], 0, 1) == 0 ? substr($row['EventStartTime'], 1) : $row['EventStartTime'];
                        for ($i = 1; $i < 24; $i++) {
                            ?>
                            <option value="<?php echo $i ?>:00:00" <?php echo $theTime == $i . ':00:00' ? 'selected' : '' ?>><?php echo $i > 12 ? $i - 12 : $i ?>:00 <?php echo $i > 11 ? 'pm' : 'am' ?></option>
                            <option value="<?php echo $i ?>:15:00" <?php echo $theTime == $i . ':15:00' ? 'selected' : '' ?>><?php echo $i > 12 ? $i - 12 : $i ?>:15 <?php echo $i > 11 ? 'pm' : 'am' ?></option>
                            <option value="<?php echo $i ?>:30:00" <?php echo $theTime == $i . ':30:00' ? 'selected' : '' ?>><?php echo $i > 12 ? $i - 12 : $i ?>:30 <?php echo $i > 11 ? 'pm' : 'am' ?></option>
                            <option value="<?php echo $i ?>:45:00" <?php echo $theTime == $i . ':45:00' ? 'selected' : '' ?>><?php echo $i > 12 ? $i - 12 : $i ?>:45 <?php echo $i > 11 ? 'pm' : 'am' ?></option>
                            <?PHP
                        }
                        ?>
                    </select>
                    <select name="endtime" id="endtime" style="float: left; width: 100px; margin-left: 15px; <?php echo ($row['EventTimeAllDay'] == 1) ? 'display:none' : '' ?>">
                        <option value="">End Time</option>
                        <option value="00:00:00" <?php echo ($row['EventEndTime'] == '00:00:00' && $row['EventID']) ? 'selected' : '' ?>>N/A</option>
                        <option value="00:00:01" <?php echo $row['EventEndTime'] == '00:00:01' ? 'selected' : '' ?>>Dusk</option>
                        <?PHP
                        $theTime = substr($row['EventEndTime'], 0, 1) == 0 ? substr($row['EventEndTime'], 1) : $row['EventEndTime'];
                        for ($i = 1; $i < 24; $i++) {
                            ?>
                            <option value="<?php echo $i ?>:00:00" <?php echo $theTime == $i . ':00:00' ? 'selected' : '' ?>><?php echo $i > 12 ? $i - 12 : $i ?>:00 <?php echo $i > 11 ? 'pm' : 'am' ?></option>
                            <option value="<?php echo $i ?>:15:00" <?php echo $theTime == $i . ':15:00' ? 'selected' : '' ?>><?php echo $i > 12 ? $i - 12 : $i ?>:15 <?php echo $i > 11 ? 'pm' : 'am' ?></option>
                            <option value="<?php echo $i ?>:30:00" <?php echo $theTime == $i . ':30:00' ? 'selected' : '' ?>><?php echo $i > 12 ? $i - 12 : $i ?>:30 <?php echo $i > 11 ? 'pm' : 'am' ?></option>
                            <option value="<?php echo $i ?>:45:00" <?php echo $theTime == $i . ':45:00' ? 'selected' : '' ?>><?php echo $i > 12 ? $i - 12 : $i ?>:45 <?php echo $i > 11 ? 'pm' : 'am' ?></option>
                            <?PHP
                        }
                        ?>
                    </select>
                    <input type="checkbox" id="all_day" <?php echo ($row['EventTimeAllDay'] == 1) ? 'checked' : '' ?> name="all_day" value="<?php echo ($row['EventTimeAllDay'] == 1) ? 1 : 0 ?>" style="float: left; margin-top: 15px; margin-left: 15px;"/>
                    <span style="margin-top: 12px; margin-left: 2px; float: left;">All day</span>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Facebook</label>
                <div class="form-data">
                    <input name="facebook_link" type="text" id="address" value="<?php echo $row['Event_Facebook_link'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Short Description<br>
                    200 characters</label>
                <div class="form-data">

                    <textarea name="shortdesc"  onblur="limitTextArea('shortdesc', 200)" onkeyup="limitTextArea('shortdesc', 200)" cols="50" wrap="VIRTUAL" id="shortdesc"><?php echo $row['ShortDesc'] ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Full Description<br>
                    700 Characters </label>
                <div class="form-data">

                    <textarea name="content"  onblur="limitTextArea('content', 700)" onkeyup="limitTextArea('content', 700)" cols="50" rows="10" wrap="VIRTUAL" id="content"><?php echo $row['Content'] ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Location</label>
                <div class="form-data">
                    <div id="ajax-location">
                        <select name="setLocationID" id="setLocationID">
                            <option value="0">Select One or Use Alternate</option>
                            <?PHP
                            if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
                                $queryconcat = "";
                                $region_locations = explode(",", $row['E_Region_ID']);
                                foreach ($region_locations as $key => $value) {
                                    if ($key == 0) {
                                        $queryconcat .= " FIND_IN_SET (" . $value . ",EL_Region_ID)";
                                    } else {
                                        $queryconcat .= " OR FIND_IN_SET (" . $value . ",EL_Region_ID)";
                                    }
                                }
                                $sql = "SELECT EL_ID, EL_Name  FROM Events_Location WHERE $queryconcat ORDER BY EL_Name";

                                $result = mysql_query($sql, $db);
                                while ($rowTMP = mysql_fetch_assoc($result)) {
                                    ?>
                                    <option value="<?php echo $rowTMP['EL_ID'] ?>" <?php echo $row['LocationID'] == $rowTMP['EL_ID'] ? 'selected' : '' ?>><?php echo $rowTMP['EL_Name'] ?></option>
                                    <?PHP
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <br>
                    <input name="location" type="text" id="location" value="<?php echo $row['LocationList'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Address</label>
                <div class="form-data">
                    <input name="address" type="text" id="address" value="<?php echo $row['StreetAddress'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Organization</label>
                <div class="form-data">
                    <div id="ajax-organization">
                        <select name="setOrganizationID" id="setOrganizationID">
                            <option value="0">Select One or Use Alternate</option>
                            <?PHP
                            if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
                                $queryconcat = "";
                                foreach ($region_locations as $key => $value) {
                                    if ($key == 0) {
                                        $queryconcat .= " FIND_IN_SET (" . $value . ",EO_Region_ID)";
                                    } else {
                                        $queryconcat .= " OR FIND_IN_SET (" . $value . ",EO_Region_ID)";
                                    }
                                }
                                $sql = "SELECT EO_ID, EO_Name FROM Events_Organization WHERE $queryconcat ORDER BY EO_Name";
                                $result = mysql_query($sql, $db);
                                while ($rowTMP = mysql_fetch_assoc($result)) {
                                    ?>
                                    <option value="<?php echo $rowTMP['EO_ID'] ?>" <?php echo $row['OrganizationID'] == $rowTMP['EO_ID'] ? 'selected' : '' ?>><?php echo $rowTMP['EO_Name'] ?></option>
                                    <?PHP
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <br>
                    <input name="organization" type="text" id="organization" value="<?php echo $row['Organization'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Contact Name</label>
                <div class="form-data">
                    <input name="contact" type="text" id="contact" value="<?php echo $row['ContactName'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Contact Phone</label>
                <div class="form-data">
                    <input name="phone" class="phone_number" type="text" id="phone" value="<?php echo $row['ContactPhone'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Additional Phone</label>
                <div class="form-data">
                    <input name="tollfree" class="phone_number" type="text" id="tollfree" value="<?php echo $row['ContactPhoneTollFree'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Email</label>
                <div class="form-data">
                    <input name="email" type="text" id="email" value="<?php echo $row['Email'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Website</label>
                <div class="form-data">
                    <input name="website" type="text" id="website" value="<?php echo $row['WebSiteLink'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Status</label>
                <div class="form-data">
                    <select name="pending">
                        <option value="">Select One</option>
                        <option value="1" <?php echo $row['Pending'] == 1 ? 'selected' : '' ?>>Pending</option>
                        <option value="0" <?php echo ($row['Pending'] == 0 && $row['EventID']) ? 'selected' : '' ?>>Approved</option>
                        <option value="2" <?php echo $row['Pending'] == 2 ? 'selected' : '' ?>>Delete</option>
                    </select>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <input type="submit" name="button2" id="button2" value="Submit" /> 
                </div>
            </div>
        </form>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>