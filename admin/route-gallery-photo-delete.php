<?PHP
require_once '../include/config.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if ($_REQUEST['op'] == 'del') {
  $IR_ID = $_REQUEST['ir_id'];
  $IRG_ID = $_REQUEST['id'];
  $R_ID = $_REQUEST['rid'];
  $sql = "DELETE FROM tbl_Individual_Route_Gallery WHERE IRG_ID = '" . encode_strings($IRG_ID, $db) . "' AND IRG_IR_ID = '" . encode_strings($IR_ID, $db) . "'";
  $result = mysql_query($sql, $db);
  if ($result) {
    $_SESSION['delete'] = 1;
    // TRACK DATA ENTRY
    Track_Data_Entry('Websites', $R_ID, 'Manage Individual Route', $IR_ID, 'Delete Gallery Photo', 'super admin');
    //Delete from image usage when image is deleted
    imageBankUsageDelete('IBU_Individual_Route_Gallery', $IRG_ID);
  } else {
    $_SESSION['delete_error'] = 1;
  }

  header("Location: route.php?rid=" . $R_ID . "&id=" . $IR_ID);
  exit();
}
?>

