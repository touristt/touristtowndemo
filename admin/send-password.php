<?php
$headers = make_mail_headers(MAIN_CONTACT_EMAIL, COMPANY_NAME);

$new_pw = makeRandomPassword();

$sub = INPUT_DIR . " Password Reset";

$msg = "Good day,\n\n"
        . "Your password for " . DOMAIN . INPUT_DIR . " has been reset.\n\n"
        . "Your new password is: " . $new_pw . "\n\n"
        . "After you login please change this password using the \"Change my Password\" "
        . "on the Main Maintenance Page.\n\n"
        . "If you didn't request this please notify the office immediately.\n\n"
        . "Thank you,\n" . COMPANY_NAME . " \n\n";



$sql = "UPDATE tbl_User SET User_Password = '" . md5($new_pw) . "' "
        . "WHERE User_ID = '" . $_SESSION['send_password_uid'] . "' LIMIT 1 ";

mysql_query($sql, $db);
require_once '../include/admin/header.php';
?>
<table width='500' border='0' align='center' cellpadding='0' cellspacing='0'>
    <tr>
        <td align='center'>
            <?php
            if (isset($_SESSION['error_PW']) && $_SESSION['error_PW'] == 1) {
                ?>
        <tr>
            <td colspan="2" >Your request produced an error, please contact the office.</td>
        </tr>
        <?php
        unset($_SESSION['error_PW']);
    }
    if (mail($_SESSION['send_password_email'], $sub, $msg, $headers, '-f' . MAIN_CONTACT_EMAIL)) {
        echo "Your new password has been emailed to you.";
    } else {
        echo "There was an Error sending you new password contact the office.";
    }
    unset($_SESSION['send_password_uid']);
    unset($_SESSION['send_password_email']);
    ?>
</td>
</tr>
</table>
<?php
require_once '..include/admin/footer.php';
?>