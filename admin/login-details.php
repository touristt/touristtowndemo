<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/adminFunctions.inc.php';

if (!in_array('customers', $_SESSION['USER_PERMISSIONS']) && !in_array('free-listings', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
}

$BID = $_REQUEST['id'];

if ($BID > 0) {
    $sql = "SELECT B_ID, B_Email, B_Subscription FROM tbl_Business WHERE B_ID = '$BID' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
    $BID = $row['B_ID'];
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $checkEmail = "SELECT B_ID FROM tbl_Business WHERE B_Email = '" . encode_strings($_REQUEST['email'], $db) . "'";
    $resultEmail = mysql_query($checkEmail, $db) or die("Invalid query: $checkEmail -- " . mysql_error());
    $count = mysql_num_rows($resultEmail);
    $rowEmail = mysql_fetch_assoc($resultEmail);
    $subsciption = isset($_REQUEST['subscription']) ? 1 : 0;
    $sql = "tbl_Business SET B_Email = '" . encode_strings($_REQUEST['email'], $db) . "', B_Subscription = '" . encode_strings($subsciption, $db) . "'";
    if (strlen($_REQUEST['pass1']) > 4) {
        $sql .= ", B_Password = MD5('" . encode_strings($_REQUEST['pass1'], $db) . "')";
    }
    if ($BID > 0) {
        $id = $BID;
        if (($BID == $rowEmail['B_ID'] && $count > 0) || $count == 0) {
            $sql = "UPDATE " . $sql . " WHERE B_ID = '" . encode_strings($BID, $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            if ($result) {
                $_SESSION['success'] = 1;
            } else {
                $_SESSION['error'] = 1;
            }
        } else {
            $_SESSION['WARNING_LOGIN_DETAILS'] = 1;
        }
        header("Location: login-details.php?id=$id");
        exit();
    } else {
        $sql = "INSERT " . $sql;
        if ($count > 0) {
            $_SESSION['WARNING_LOGIN_DETAILS'] = 1;
            header("Location: login-details.php");
            exit();
        } else {
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $id = mysql_insert_id();
            if ($result) {
                $_SESSION['success'] = 1;
            } else {
                $_SESSION['error'] = 1;
            }
            header("Location: login-details.php?id=$id");
            exit();
        }
    }
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Login Details</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP
        if ($row['B_ID'] > 0) {
            require '../include/nav-businessprofile.php';
        } else {
            echo "&nbsp;";
        }
        ?>
    </div>
    <div class="right">
        <form onSubmit="return validatePassword($('#password').val(), true, $('#password1').val())" action="" method="post" name="form1">
            <input type="hidden" name="op" value="save">
            <div class="content-header">Login Details</div>
            <?php
            $help_text = show_help_text('Username');
            if ($help_text != '') {
                echo '<div class="form-inside-div form-inside-div-width-admin">' . $help_text . '</div>';
            }
            ?>
            <div class="form-inside-div form-inside-div-width-admin" style="margin-bottom:35px;">
                <label>Email</label>
                <div class="form-data"><input required name="email" type="email" size="50" value="<?php echo $row['B_Email'] ?>" /> 
                </div>
            </div>
            <?php
            $help_text1 = show_help_text('Password');
            if ($help_text1 != '') {
                echo '<div class="form-inside-div form-inside-div-width-admin">' . $help_text1 . '</div>';
            }
            ?>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Password</label>
                <div class="form-data">
                    <input id="password" name="pass1" type="password" size="50"/> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Confirm Password</label>
                <div class="form-data">
                    <input id="password1" name="pass2" type="password" size="50"/> 
                </div>
            </div> 
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Subscribe</label>
                <div class="form-data region-check-margin">
                    <?php if (isset($row['B_ID']) && $row['B_ID'] > 0) { ?>
                        <input type="checkbox" name="subscription" value="1" <?php echo ($row['B_Subscription'] == 1) ? 'checked' : ''; ?>/>
                    <?php } else { ?>
                        <input type="checkbox" name="subscription" value="1" checked />
                    <?php } ?>
                </div>
            </div> 


            <div class="form-inside-div form-inside-div-width-admin" style="margin-bottom:35px;">
                <div class="button">
                    <input type="submit" name="button2" value="Save Now"/>
                </div>
            </div> 

            <div class="form-inside-div form-inside-div-width-admin">
                <label></label>
                <div class="form-data">
                    <strong>Password must meet the following requirements:</strong>
                    <ul>
                        <li>At Least 6 Characters</li>
                        <li>Contain one Number</li>
                        <li>Contain one lowercase letter</li>
                        <li>Contain one uppercase letter</li>
                    </ul>
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>