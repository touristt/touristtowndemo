<?php

include '../include/config.inc.php';

if (isset($_POST['activation_email_btn'])) {
    $body_txt = isset($_POST['body_txt']) ? addslashes($_POST['body_txt']) : '';
    $footer_txt = isset($_POST['body_txt']) ? addslashes($_POST['footer_txt']) : '';
    $update_insert = isset($_POST['template_id']) ? $_POST['template_id'] : 0;

    //upload  image
    $allowedExts = array("gif", "jpeg", "jpg", "png");
    if (isset($_FILES["img"]["name"]) && $_FILES["img"]["name"] != "") {
        $temp = explode(".", $_FILES["img"]["name"]);
        $extension = end($temp);
        if ((($_FILES["img"]["type"] == "image/gif")
                || ($_FILES["img"]["type"] == "image/jpeg")
                || ($_FILES["img"]["type"] == "image/jpg")
                || ($_FILES["img"]["type"] == "image/pjpeg")
                || ($_FILES["img"]["type"] == "image/x-png")
                || ($_FILES["img"]["type"] == "image/png"))
                && in_array($extension, $allowedExts)) {
            if ($_FILES["img"]["error"] > 0) {
                echo "Return Code: " . $_FILES["img"]["error"] . "<br>";
                exit();
            } else {
                $maxwidth = '640';
                $maxheight = '240';
                list($width, $height) = getimagesize($_FILES['img']['tmp_name']);
                if ($width != $maxwidth || $height != $maxheight) {
                    $_SESSION['message'] = '<div class="error-message">The image should be 640x240 resolution.</div>';
                    header("location:activation_email_tpl.php");
                    exit;
                }

                $path_parts = pathinfo($_FILES["img"]["name"]);
                $image_name = $path_parts['filename'] . '_' . time() . '.' . $path_parts['extension'];
                $image_path = "images/email_tpl/" . $image_name;
                move_uploaded_file($_FILES["img"]["tmp_name"], "$image_path");
                //unlink the old photo
                if (isset($_POST["old_path"]) && $_POST["old_path"] != "") {
                    unlink("../" . $_POST["old_path"]);
                }
            }
        } else {
            echo "Invalid file";
            exit();
        }
    } else if (isset($_POST["old_path"]) && $_POST["old_path"] != "") {
        $image_path = addslashes($_POST["old_path"]);
    } else {
        $image_path = "";
    }


    if ($update_insert) {
        $query = "UPDATE acc_emails_tpl SET body_txt = '$body_txt', footer_txt = '$footer_txt', img = '$image_path' WHERE id = '$update_insert'";
    } else {
        $query = "INSERT INTO acc_emails_tpl(id,body_txt,footer_txt,img,activation_launch) values('','$body_txt','$footer_txt','$image_path','1')";
    }

    $result = mysql_query($query) or die(mysql_error());
    if ($result) {
        $_SESSION['message'] = '<div class="success-message">Activation Email Template hase been saved.</div>';
    } else {
        $_SESSION['message'] = '<div class="error-message">Activation Template is not saved! try latter.</div>';
    }
    header("location:activation_email_tpl.php");
}
?>