<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('superadmin', $_SESSION['USER_ROLES'])) {
    header("Location: /admin/");
    exit();
}

$sql = "SELECT * FROM tbl_Client_Contacts";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$count = mysql_num_rows($result);

if (isset($_GET['op']) && $_GET['op'] == 'del') {
    $sql = "DELETE tbl_Client_Contacts FROM tbl_Client_Contacts WHERE CL_ID = '" . $_REQUEST['id'] . "'";
    $res = mysql_query($sql, $db);
    if ($res) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: client-contacts.php");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Client Contacts</div>
        <div class="link">
            <a href="client-contact-add.php">+Add Contact</a>
            <?php if ($count > 0) { ?>
                <form class="export_form_width clients-contact" method="post" name="export_form" action="export-client-contacts.php">
                    <input type="submit" name="export_contacts" value="Export">
                </form>
            <?php } ?>
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-client-contacts.php'; ?>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column spl-name padding-none">Name</div>
            <div class="data-column spl-other padding-none">Edit</div>
            <div class="data-column spl-other padding-none">Delete</div>
        </div>
        <div class="reorder-category">
            <?PHP
            while ($row = mysql_fetch_assoc($result)) {
                ?>
                <div class="data-content">
                    <div class="data-content">
                        <div class="data-column spl-name">
                            <?php echo $row['CL_Name'] ?>
                        </div>
                        <div class="data-column spl-other">
                            <a href="client-contact-add.php?id=<?php echo $row['CL_ID'] ?>">Edit</a>
                        </div>
                        <div class="data-column spl-other"><a onClick="return confirm('Are you sure? This action can not be undone!')" href="client-contacts.php?id=<?php echo $row['CL_ID'] ?>&op=del">Delete</a></div>
                    </div>
                </div>
                <?PHP
            }
            ?>
        </div>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>