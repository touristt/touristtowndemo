<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('manage-events', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
} elseif ($_SESSION['USER_LIMIT'] > 0) {
    $limits = explode(',', $_SESSION['USER_LIMIT']);
    $regionLimit = array();
    foreach ($limits as $limit) {
        $sql = "SELECT R_ID, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($limit, $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $region = mysql_fetch_assoc($result);
        if (isset($region['R_Type']) && $region['R_Type'] == 1) {
            if ($region['R_ID'] != '' && !in_array($region['R_ID'], $regionLimit)) {
                $regionLimit[] = $region['R_ID'];
            }
        } else {
            $sql = "SELECT RM_Parent FROM tbl_Region_Multiple WHERE RM_Child = '" . encode_strings($region['R_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($row = mysql_fetch_assoc($result)) {
                if ($row['RM_Parent'] != '' && !in_array($row['RM_Parent'], $regionLimit)) {
                    $regionLimit[] = $row['RM_Parent'];
                }
            }
        }
    }
}
$regionLimitCommaSeparated = rtrim(implode(',', $regionLimit), ',');

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Events Communities</div>
        <div class="link">
            <a href="events-community.php">+ Add Community</a>
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manageevents.php'; ?>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name-loc">Name</div>
            <div class="data-column padding-none spl-other">Delete</div>
        </div>
        <?PHP
        $sql = "SELECT * FROM Events_Towns ";
        $sql .= $regionLimitCommaSeparated ? ("AND ET_Region_ID = " . $regionLimitCommaSeparated . " ") : "LEFT JOIN tbl_Region ON R_ID = ET_Region_ID ";
        $sql .= "ORDER BY ET_Town";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            ?>
            <div class="data-content">
                <div class="data-column spl-name-loc"><a href="events-community.php?id=<?php echo $row['ET_ID'] ?>">
                        <?php echo $row['ET_Town'] ?> <?php echo $row['R_Name'] ? '(' . $row['R_Name'] . ')' : '' ?>
                    </a></div>
                <div class="data-column spl-other"><a onClick="return confirm('Are you sure this action can not be undone!');" href="events-community.php?id=<?php echo $row['ET_ID'] ?>&amp;op=del">Delete</a>
                </div>
            </div>
            <?PHP
        }
        ?>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>