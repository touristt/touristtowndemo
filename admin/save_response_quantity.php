<?php

include '../include/config.inc.php';
include '../include/accommodation_functions.php';
require_once '../include/track-data-entry.php';

$accom_id = isset($_GET['accom_id']) ? $_GET['accom_id'] : '';
$block_id = isset($_GET['block_id']) ? $_GET['block_id'] : 0;
$accom_general = $_GET['accom_general'];
$cap_id = isset($_GET['cap_id']) ? $_GET['cap_id'] : 0;
$block = block_info($block_id);

$bed_1 = isset($_GET['bed_1']) && $_GET['bed_1'] != '' ? $_GET['bed_1'] : 0;
$bed_2 = isset($_GET['bed_2']) && $_GET['bed_2'] != '' ? $_GET['bed_2'] : 0;
$bed_3 = isset($_GET['bed_3']) && $_GET['bed_3'] != '' ? $_GET['bed_3'] : 0;
$bed_4 = isset($_GET['bed_4']) && $_GET['bed_4'] != '' ? $_GET['bed_4'] : 0;
$bed_5 = isset($_GET['bed_5']) && $_GET['bed_5'] != '' ? $_GET['bed_5'] : 0;
$bed_6 = isset($_GET['bed_6']) && $_GET['bed_6'] != '' ? $_GET['bed_6'] : 0;
$bed_7 = isset($_GET['bed_7']) && $_GET['bed_7'] != '' ? $_GET['bed_7'] : 0;
$bed_8 = isset($_GET['bed_8']) && $_GET['bed_8'] != '' ? $_GET['bed_8'] : 0;
$sat = isset($_GET['sat']) && $_GET['sat'] != '' ? $_GET['sat'] : 0;
$sun = isset($_GET['sun']) && $_GET['sun'] != '' ? $_GET['sun'] : 0;
$mon = isset($_GET['mon']) && $_GET['mon'] != '' ? $_GET['mon'] : 0;
$tue = isset($_GET['tue']) && $_GET['tue'] != '' ? $_GET['tue'] : 0;
$wed = isset($_GET['wed']) && $_GET['wed'] != '' ? $_GET['wed'] : 0;
$thur = isset($_GET['thur']) && $_GET['thur'] != '' ? $_GET['thur'] : 0;
$fri = isset($_GET['fri']) && $_GET['fri'] != '' ? $_GET['fri'] : 0;

if ($_GET['type'] == 0) {
    $total_rooms_availability = $sat + $sun + $mon + $tue + $wed + $thur + $fri;
    $total_capacity = $accom_general;
    $start_date = $block['start_date'];
    $end_date = $block['end_date'];
    $num_days = floor((strtotime($end_date) - strtotime($start_date)) / (60 * 60 * 24)) + 1;
    $block_aval = $total_capacity * $num_days;
    if ($block_aval) {
        $percent = round(($total_rooms_availability / $block_aval) * 100);
    } else {
        $percent = 0;
    }
} else {
    $total_rooms_availability = $bed_1 + $bed_2 + $bed_3 + $bed_4 + $bed_5 + $bed_6 + $bed_7 + $bed_8;
    $total_capacity = $accom_general;
    $block_aval = $total_capacity;
    if ($block_aval) {
        $percent = round(($total_rooms_availability / $block_aval) * 100);
    } else {
        $percent = 0;
    }
}

$already_exit = "Select * From acc_capacity_response where accommodator_id = $accom_id And block_id = $block_id";
$exist_check = mysql_query($already_exit);
if (mysql_num_rows($exist_check) > 0) {
    $update = "UPDATE  acc_capacity_response  SET  bed_1 =$bed_1, bed_2 =$bed_2, bed_3 =$bed_3, bed_4 =$bed_4, bed_5 =$bed_5, bed_6 =$bed_6, bed_7 =$bed_7, bed_8 =$bed_8, sat =$sat, sun =$sun, mon =$mon, tue =$tue, wed =$wed, thur =$thur, fri =$fri WHERE accommodator_id = $accom_id And block_id = $block_id";
    $result = mysql_query($update);
    if ($result) {
        // TRACK DATA ENTRY
        Track_Data_Entry('Accomodation', '', 'Block - Hotels & Motels', $block_id, 'Update', 'super admin');
        echo $percent;
    } else {
        echo 'There Seems Some Error Please Try Again';
    }
} else {
    $insert = "INSERT INTO acc_capacity_response (block_id, accommodator_id, bed_1, bed_2, bed_3, bed_4, bed_5, bed_6, bed_7, bed_8, sat, sun, mon, tue, wed, thur, fri) VALUES ($block_id,$accom_id,$bed_1,$bed_2,$bed_3,$bed_4,$bed_5,$bed_6,$bed_7,$bed_8,$sat,$sun,$mon,$tue,$wed,$thur,$fri)";
    $result = mysql_query($insert);
    if ($result) {
        // TRACK DATA ENTRY
        Track_Data_Entry('Accomodation', '', 'Block - Hotels & Motels', $block_id, 'Add', 'super admin');
        echo $percent;
    } else {
        echo 'There Seems Some Error Please Try Again';
    }
}
?>
