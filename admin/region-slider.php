<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if (isset($_REQUEST['rid']) && $_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
    $sql = "SELECT R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
} else {
    header("Location: /admin/regions.php");
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    if ($_POST['image_bank'] == "") {
        require '../include/picUpload.inc.php';
        // last @param 1 = Gallery Images
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 4, 'region', $regionID);
        if (is_array($pic)) {
            $sqlMax = "SELECT MAX(RTP_Order) FROM tbl_Region_Themes_Photos WHERE RTP_RT_RID = '$regionID'";
            $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
            $rowMax = mysql_fetch_row($resultMax);
            $sql = "INSERT tbl_Region_Themes_Photos SET 
                    RTP_Title = '" . encode_strings($_REQUEST['sliderTitle'], $db) . "', 
                    RTP_Video_Link = '" . encode_strings($_REQUEST['video'], $db) . "', 
                    RTP_Description = '" . encode_strings($_REQUEST['description'], $db) . "',
                    RTP_Photo = '" . encode_strings($pic['0']['0'], $db) . "', 
                    RTP_Mobile_Photo = '" . encode_strings($pic['0']['2'], $db) . "', 
                    RTP_RT_RID = '$regionID',
                    RTP_Order = '" . ($rowMax[0] + 1) . "'";
            $pic_id = $pic['1'];
        }
    } else {
        $pic_id = $_POST['image_bank'];
        require '../include/picUpload.inc.php';
        // last @param 1 = Gallery Images
        $pic = Upload_Pic_Library($pic_id, 4); 
        if (is_array($pic)) {
            $sqlMax = "SELECT MAX(RTP_Order) FROM tbl_Region_Themes_Photos WHERE RTP_RT_RID = '$regionID'";
            $resultMax = mysql_query($sqlMax, $db) or die("Invalid query: $sqlMax -- " . mysql_error());
            $rowMax = mysql_fetch_row($resultMax);
            $sql = "INSERT tbl_Region_Themes_Photos SET 
                RTP_Title = '" . encode_strings($_REQUEST['sliderTitle'], $db) . "', 
                RTP_Video_Link = '" . encode_strings($_REQUEST['video'], $db) . "', 
                RTP_Description = '" . encode_strings($_REQUEST['description'], $db) . "', 
                RTP_Photo = '" . encode_strings($pic['0'], $db) . "', 
                RTP_Mobile_Photo = '" . encode_strings($pic['2'], $db) . "', 
                RTP_RT_RID = '$regionID',                
                RTP_Order = '" . ($rowMax[0] + 1) . "'";
        }
    }
    $result = mysql_query($sql, $db);
    $id = mysql_insert_id();
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $regionID;
        Track_Data_Entry('Websites', $id, 'Homepage Slider', '', 'Add', 'super admin');
        if ($pic_id > 0) {
            //Image usage from image bank
            imageBankUsage($pic_id, 'IBU_R_ID', $regionID, 'IBU_Region_Theme', $id);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: region-slider.php?rid=" . $regionID);
    exit();
}
if (isset($_POST['op']) && $_POST['op'] == 'update') {
    $sql = "Update tbl_Region_Themes_Photos SET 
            RTP_Title = '" . encode_strings($_REQUEST['sliderTitle'], $db) . "', 
            RTP_Video_Link = '" . encode_strings($_REQUEST['video'], $db) . "', 
            RTP_Description = '" . encode_strings($_REQUEST['description'], $db) . "'  
            where RTP_ID = '" . encode_strings($_REQUEST['image_id'], $db) . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $regionID;
        Track_Data_Entry('Websites', $id, 'Homepage Slider', '', 'Update', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: region-slider.php?rid=" . $regionID);
    exit();
}
if (isset($_GET['op']) && $_GET['op'] == 'del') {
    $update = "DELETE FROM tbl_Region_Themes_Photos WHERE RTP_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $regionID;
        Track_Data_Entry('Websites', $id, 'Homepage Slider', '', 'Delete', 'super admin');
        imageBankUsageDelete('IBU_R_ID', $regionID, 'IBU_Region_Theme', $_REQUEST['id']);
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header('location: region-slider.php?rid=' . $_REQUEST['rid']);
    exit;
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Manage Home Slider</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP
        if ($regionID > 0) {
            require '../include/nav-manage-region.php';
        } else {
            echo "&nbsp;";
        }
        ?>
    </div>
    <div class="right">
        <form name="form1" method="post" onSubmit="return check_img_size(10, 10000000)"  enctype="multipart/form-data" action="" id="gallery-form" style="display:none;float:left;width:100%;margin-bottom: 10px;">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="image_bank" class="image_bank" id="image_bank10" value="">
            <div class="form-inside-div form-inside-div-width-admin">
                <label class="slider">Photo</label>
                <div class="form-data form-input-text">
                    <div class="div_image_library">
                        <span class="daily_browse" onclick="show_image_library(10)">Select File</span>
                        <input onchange="show_file_name(10, this, 0)" id="photo10" type="file" name="pic[]" style="display: none;"/>
                    </div>
                    <div class="form-inside-div add-about-us-item-image margin-none">
                        <div class="cropit-image-preview aboutus-photo-perview">
                            <img class="preview-img preview-img-script10" style="display: none;" src="">    
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Slider Title</label>
                <div class="form-data">
                    <input name="sliderTitle" type="text" value="" size="50" maxlength="255"/> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Video Link</label>
                <div class="form-data">
                    <input name="video" type="text" value="" size="255"/> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Description</label>
                <div class="form-data"> 
                    <textarea name="description" cols="85" rows="10" class="tt-description"></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <div class="button">
                    <input type="submit" name="button" id="button" value="Save Photo"/>
                </div>
            </div>
        </form>
        <div class="content-header">
            <div class="title">Homepage Slider</div>
            <div class="link">
                <?PHP
                $select = " SELECT RTP_ID, RTP_Photo, RTP_Title, RTP_Video_Link, RTP_Description FROM tbl_Region_Themes_Photos 
                            WHERE RTP_RT_RID = '" . $regionID . "' ORDER BY RTP_Order";
                $result = mysql_query($select, $db);
                $numRows = mysql_num_rows($result);
                if ($numRows < 10) {
                    echo '<div class="add-link"><a onclick="show_form()">+Add Photo</a></div>';
                }
                ?>
            </div>
            <script>
                function show_form() {
                    $('#gallery-form').show();
                }
            </script>
        </div>
        <div class="photo-gallery-limit">
            <span>Photos <?php echo (($numRows == "") ? "0" : $numRows) ?> of 10</span>
        </div>
        <div class="form-inside-div border-none margin-none region-slider-images" id="region-slider-list">
            <ul class="gallery">
                <?PHP
                $count = 1;
                while ($data = mysql_fetch_assoc($result)) {
                    ?>
                    <li class="image" id="recordsArray_<?php echo $data['RTP_ID'] ?>">
                        <div class="form-inside-div form-inside-div-width-admin"> 
                            <label>Photo <?php echo $count ?></label>
                            <div class="form-data div_image_library cat-region-width">
                                <a class="deletePhoto delete-region-cat-photo" onclick="return confirm('Are you sure you want to delete this section? This will include photo,title and description.');" href="region-slider.php?id=<?php echo $data['RTP_ID'] ?>&rid=<?php echo $regionID ?>&op=del">Delete</a>
                                <div class="cropit-image-preview aboutus-photo-perview">  
                                    <?php if ($data['RTP_Photo'] != "") { ?>
                                        <img class="existing-img existing_imgs4" src="http://<?php echo DOMAIN . IMG_LOC_REL . $data['RTP_Photo'] ?>">
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <form name="form1" method="post" action=""> 
                            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
                            <input type="hidden" name="image_id" value="<?php echo $data['RTP_ID'] ?>">
                            <input type="hidden" name="op" value="update">
                            <div class="form-inside-div form-inside-div-width-admin"> 
                                <label>Slider Title</label>
                                <div class="form-data">
                                    <input name="sliderTitle" type="text"  value="<?php echo $data['RTP_Title'] ?>" size="50" maxlength="255"/> 
                                </div>
                            </div>
                            <div class="form-inside-div form-inside-div-width-admin"> 
                                <label>Video Link</label>
                                <div class="form-data">
                                    <input name="video" type="text"  value="<?php echo $data['RTP_Video_Link'] ?>" size="50" /> 
                                </div>
                            </div>
                            <div class="form-inside-div form-inside-div-width-admin"> 
                                <label>Description</label>
                                <div class="form-data"> 
                                    <textarea id="description_<?php echo $data['RTP_ID'] ?>" name="description" cols="85" rows="10" class="tt-description"><?php echo $data['RTP_Description'] ?></textarea>
                                </div>
                            </div>
                            <div class="form-inside-div  width-data-content border-none"> 
                                <div class="button"><input type="submit" name="update" id="button22" value="Submit" />
                                </div>
                            </div>
                        </form>   
                    </li>
                    <?PHP
                    $count++;
                }
                ?>
            </ul>
        </div>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>

<script>
    $(function () {
        $("#region-slider-list ul.gallery")
                .sortable({
                    opacity: 0.9,
                    cursor: 'move',
                    start: function (event, ui)
                    {
                        var id_textarea = ui.item.find(".tt-description").attr("id");
                        CKEDITOR.instances[id_textarea].destroy();
                    },
                    update: function () {
                        var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Region_Themes_Photos&field=RTP_Order&id=RTP_ID';
                        $.post("reorder.php", order, function (theResponse) {
                            swal("Sliders Re-ordered!", "Sliders Re-ordered successfully.", "success");
                        });
                    },
                    stop: function (event, ui)
                    {
                        var id_textarea = ui.item.find(".tt-description").attr("id");
                        CKEDITOR.replace(id_textarea);
                    }
                });
    });
</script>


<?PHP
require_once '../include/admin/footer.php';
?>