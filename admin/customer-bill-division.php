<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/ranking.inc.php';

if (!in_array('division-billing', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$BL_ID = $_REQUEST['bl_id'];
$DB_ID = $_REQUEST['id'];

if ($BL_ID > 0) {
    $sql = "SELECT B_ID, card_number FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            LEFT JOIN payment_profiles ON B_ID = business_id
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $sql = "SELECT R_Clientname, R_Street, R_Town, R_Province, R_PostalCode FROM tbl_Business_Listing_Category_Region 
            LEFT JOIN tbl_Region ON R_ID = BLCR_BLC_R_ID WHERE BLCR_BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowReg = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
} else {
    header('Location: customers.php');
}

if ($DB_ID > 0) {
    $sql = "SELECT tbl_Division_Billing.*, BL_Listing_Title, BL_Street, BL_Address, BL_Town, BL_Province, BL_PostalCode FROM tbl_Division_Billing 
            LEFT JOIN tbl_Business_Listing ON DB_BL_ID = BL_ID 
            LEFT JOIN payment_profiles ON DB_Description = division_id
            LEFT JOIN tbl_Payment_Type ON DB_PT_ID = PT_ID
            WHERE DB_ID = '" . encode_strings($DB_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowDB = mysql_fetch_assoc($result);
} else {
    header("Location:customer-billing.php?bl_id=" . $BL_ID);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left">
    <?php require_once '../include/top-nav-listing.php'; ?>

    <div class="title-link">
        <div class="title">Invoice</div>
        <div class="link">
            <?php if (in_array('delete-invoices', $_SESSION['USER_PERMISSIONS'])) { ?>
                <a class="invoices" href="/admin/division-billing-delete.php?db_id=<?php echo $rowDB['DB_ID'] ?>&bl_id=<?php echo $rowDB['DB_BL_ID'] ?>&op=del">Delete Invoice</a></td>
                <?php
            }
            if ($rowDB['DB_Transaction_ID'] > 0 && $rowDB['DB_Refund_Deleted'] != 1) {
                if (in_array('superadmin', $_SESSION['USER_ROLES'])) {
                    ?>
                    <a class="invoices" href="/admin/division-billing-delete.php?db_id=<?php echo $rowDB['DB_ID'] ?>&bl_id=<?php echo $rowDB['DB_BL_ID'] ?>&op=refund">Refund Invoice</a></td>
                    <?php
                }
            }
            else if($rowDB['DB_Transaction_ID'] > 0) {
              echo '<span class="invoices-text">Refunded</span>';
            }
            ?>
        </div>
    </div>

    <div class="left">
        <?PHP require '../include/nav-billing.php'; ?>
    </div>

    <div class="right">
        <div class="page-print">
            <div class="billing-inside-div-tittle">
                <div class="data-content advert-bill-invoice-detail">
                    <div class="data-column invoice-heading">Invoice Number : </div>
                    <div class="data-column invoice-no"><?= $rowDB['DB_ID'] ?></div>
                    <div class="data-column invoice-date">Date Paid : </div>
                    <div class="data-column invoice-date-bb"><?= $rowDB['DB_Date'] ?></div>
                </div>
            </div>
            <div class="invoice-address">
                <div class="data-left  inviced-left">
                    <div class="data-header-inv bold inviced-head">Invoiced To</div>
                    <div class="data-content-address margine-inviced-bottom">
                        <?php echo $rowDB['BL_Listing_Title'] != '' ? trim($rowDB['BL_Listing_Title']) : ''; ?>
                        <?php echo $rowDB['BL_Street'] != '' ? '<br>' . trim($rowDB['BL_Street']) : ''; ?>
                        <?php echo $rowDB['BL_Address'] != '' ? '<br>' . trim($rowDB['BL_Address']) : ''; ?>
                        <?php
                        echo $rowDB['BL_Town'] != '' ? '<br>' . trim($rowDB['BL_Town']) : '';
                        echo $rowDB['BL_Province'] != '' ? ', ' . trim($rowDB['BL_Province']) : '';
                        echo $rowDB['BL_PostalCode'] != '' ? ', ' . trim($rowDB['BL_PostalCode']) : '';
                        ?>
                        <br> Canada
                    </div>
                </div>
                <div class="data-right">
                    <div class="data-header-inv bold pay-head">Pay To</div>
                    <div class="data-content-address margine-inviced-bottom">
                        <?php
                        echo $rowReg['R_Clientname'] != '' ? trim($rowReg['R_Clientname']) : '';
                        echo $rowReg['R_Street'] != '' ? '<br>' . trim($rowReg['R_Street']) : '';
                        echo $rowReg['R_Town'] != '' ? '<br>' . trim($rowReg['R_Town']) : '';
                        echo $rowReg['R_Province'] != '' ? ', ' . trim($rowReg['R_Province']) : '';
                        echo $rowReg['R_PostalCode'] != '' ? ', ' . trim($rowReg['R_PostalCode']) : '';
                        ?>
                        <br>Canada

                    </div>
                </div>
            </div>
            <div class="invoice-pay-main">
                <div class="data-column bold payment-text type-width">Payment Type :</div>
                <div class="data-column card-name"><?php echo $rowDB['PT_Name'] ?></div>
            </div>
            <div class="card-main">
                <div class="data-column bold last-card-digits">Last four digits of card :</div>
                <?php
                    $cardno = explode("XXXX", $rowListing['card_number']);
                ?>
                <div class="data-column last-card-digits-no"><?php echo $cardno[1] ?></div>
            </div>
            <div class="data-header bold inviced-detail inviced-order-heading">Order Summary</div>
            <div class="data-content">
                <div class="data-column inviced-tools-margin ">
                    <?php
                    if ($BL_ID > 0) {
                        echo $rowDB['DB_ID'] . " - " . $rowDB['BL_Listing_Title'];
                    } else {
                        echo $rowDB['DB_ID'] . " - " . $rowDB['DB_Customer_Name'];
                    }
                    ?>
                </div>
                <div class="data-column">$<?= $rowDB['DB_Amount'] ?> </div>
            </div>
            <?php if($rowDB['DB_Description'] != '') { ?>
              <div class="data-content">
                  <div class="data-column inviced-tools-margin ">
                      <?php
                        echo $rowDB['DB_Description'];
                      ?>
                  </div>
              </div>
            <?php } ?>
            <div class="data-header sub-total-color">
                <div class="data-column bold inviced-sub-margin ">Sub-Total 1</div>
                <div class="data-column sub-total-color">$<?= $rowDB['DB_Amount'] ?> </div>
            </div>
            <div class="data-content">
                <div class="data-column bold inviced-sub-margin">Discount</div>
                <div class="data-column">$<?= $rowDB['DB_Discount'] ?></div>
            </div>
            <div class="data-header sub-total-color">
                <div class="data-column bold inviced-sub-margin ">Sub-Total 2</div>
                <div class="data-column sub-total-color">$<?= $rowDB['DB_Subtotal'] ?> </div>
            </div>
            <div class="data-content ">
                <div class="data-column bold inviced-sub-margin">Taxes (13%)</div>
                <div class="data-column">$<?= $rowDB['DB_Tax'] ?></div>
            </div>
            <div class="data-header total-invice-color ">
                <div class="data-column bold inviced-sub-margin total-invice-color">Total</div>
                <div class="data-column total-invice-color">$<?= $rowDB['DB_Total'] ?></div>
            </div>

        </div>
        <div class="data-content print-link">
            <div class="print-button">
                <a href="#" onclick="window.print();">Print Invoice</a>
            </div>
        </div>

        <?php
        // get regions and categories of this listing
        $sql_region1 = "SELECT BL_C_ID, BLC_C_ID, BLC_BL_ID, BLCR_BLC_R_ID, BL_Points, R_Parent FROM tbl_Business_Listing
                        LEFT JOIN tbl_Business_Listing_Category  ON BLC_BL_ID = BL_ID
                        INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID 
                        INNER JOIN tbl_Region ON BLCR_BLC_R_ID = R_ID
                        WHERE BLC_BL_ID = '$BL_ID'";
        $res_region1 = mysql_query($sql_region1);
        $regions = array();
        $category = '';
        $subcategories = array();
        while ($r = mysql_fetch_assoc($res_region1)) {
            if ($r['BLCR_BLC_R_ID'] != '' && !in_array($r['BLCR_BLC_R_ID'], $regions)) {
                $regions[] = $r['BLCR_BLC_R_ID'];
            }
            if ($r['BL_C_ID'] != '' && $r['BL_C_ID'] != $category) {
                $category = $r['BL_C_ID'];
            }
            if ($r['BLC_C_ID'] != '' && !in_array($r['BLC_C_ID'], $subcategories)) {
                $subcategories[] = $r['BLC_C_ID'];
            }
        }
        foreach ($regions as $key => $r) {
            if ($key == 0) {
                $default_region = $r;
            }
        }
        ?>
        <?php if (isset($BL_ID)) { ?>
            <div class="inv-ranking-top-margin">
                <div class="data-header bold inviced-detail inviced-order-heading">Ranking Overview</div>  
                <div class="rank">
                    <div class="rank-container">
                        <div class="ranking-row bottomBorderNone inv-ranking-bottom-line">
                            <div class="ranking-title inv-ranking-margin">Current Points:</div>
                            <div class="ranking-points"><?php echo calculate_listing_points($BL_ID, 'basic'); ?></div>
                        </div>
                        <?php
                        if (isset($_SESSION['ranking_region' . $BL_ID]) && $_SESSION['ranking_region' . $BL_ID] > 0) {
                            $ranking_region = $_SESSION['ranking_region' . $BL_ID];
                        } else {
                            $ranking_region = $default_region;
                        }
                        $region_rank = calculate_ranking($BL_ID, 'region', $ranking_region, $ranking_region);
                        ?>
                        <div class="ranking-row bottomBorderNone inv-ranking-bottom-line">
                            <div class="ranking-title inv-ranking-margin">Overall Ranking:</div>
                            <div class="ranking-points"><?php echo $region_rank['rank'] . ' of ' . $region_rank['total'] ?></div>
                        </div>
                        <?php
                        $sql_category = "SELECT * FROM tbl_Category WHERE C_ID = '$category'";
                        $res_category = mysql_query($sql_category);
                        $c = mysql_fetch_assoc($res_category);
                        $category_rank = calculate_ranking($BL_ID, 'category', $category, $ranking_region);
                        ?>
                        <div class="ranking-row bottomBorderNone inv-ranking-bottom-line">
                            <div class="ranking-title inv-ranking-margin">Category Ranking: (<?php echo $c['C_Name'] ?>)</div>
                            <div class="ranking-points"><?php echo $category_rank['rank'] . ' of ' . $category_rank['total'] ?></div>
                        </div>
                        <?php
                        $i = 1;
                        foreach ($subcategories as $key => $subcat) {
                            $sql_subcat = "SELECT * FROM tbl_Category WHERE C_ID = '$subcat'";
                            $res_subcat = mysql_query($sql_subcat);
                            $s = mysql_fetch_assoc($res_subcat);
                            $subcat_rank = calculate_ranking($BL_ID, 'subcategory', $subcat, $ranking_region);
                            print '<div class="ranking-row bottomBorderNone inv-ranking-bottom-line">
                  <div class="ranking-title">Sub-Category ' . $i . ' Ranking (' . $s['C_Name'] . ')</div>
                  <div class="ranking-points">' . $subcat_rank['rank'] . ' of ' . $subcat_rank['total'] . '</div>
                </div>
                <div class="ranking-row bottomBorderNone inv-ranking-bottom-line">
                  <div class="ranking-title">Page you appear within ' . $s['C_Name'] . '</div>
                  <div class="ranking-points">' . ceil($subcat_rank['rank'] / 10) . ' of ' . ceil($subcat_rank['total'] / 10) . '</div>
                </div>';
                            $i++;
                        }
                        ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>