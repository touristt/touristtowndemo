<?php

require_once '../include/config.inc.php';
require '../include/PHPMailer/class.phpmailer.php';
$NewDate = date('Y-m-d');
$sql = "SELECT * FROM `payment_profiles` WHERE card_expiration >= CURDATE( ) AND card_expiration <= DATE_ADD( CURDATE( ) , INTERVAL 15 DAY )";
$result = mysql_query($sql);
while ($row = mysql_fetch_array($result)) {
    $no_of_days = strtotime($NewDate);
    $cardExpire = strtotime($row['card_expiration']);
    $total_days = $cardExpire - $no_of_days;
    $days = $total_days / 86400;
    $cardNo = explode("XXXX", $row['card_number']);
    $message = '<html>
<head>
 <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
  <title></title>
</head>
<body>
<div style="font-family:tahoma,sans-serif;font-size:small">
<p style="padding-bottom: 25px;border-bottom: 5px solid #f6f6f6;font-family:arial,sans-serif;font-size:12.8px">
    <span><img src="http://' . DOMAIN . '/images/TouristTown-Logo.gif" width="195" height="43"/></span>
</p>
<p style="padding-top: 25px;font-family:arial,sans-serif;font-size:12.8px">
<span style="font-size:12.8px">Hi ' . $row['first_name'] . ' ' . $row['last_name'] . '
</span>
<br>
</p>
<p style="font-family:arial,sans-serif;font-size:12.8px">
Your credit card on file, ending ' . $cardNo[1] . ', is due to expire on
<span>
<span>
' . date('dS M, Y', strtotime($row['card_expiration'])) . '
</span>
</span>
</p>
<p style="font-family:arial,sans-serif;font-size:12.8px">
Please update your credit card details to continue uninterrupted access to TouristTown.
</p>
<p style="margin:3em 0">
	<a href="http://my.dev.touristtown.ca/customer-credit-card-details.php" style="color:#009cfc;background:#fff;border-radius:3px;padding:1em;line-height:42px;min-height:42px;font:bold 1em/1.25em Myriad Pro,Lucida Grande,sans-serif;text-decoration:none;border:1px solid #009cfc">Update My Credit Card Details</a>
</p>
    <p style="font-family:arial,sans-serif;font-size:12.8px">
    If you have any queries, reply to this email, and we will be in touch.
    </p>
    <span style="font-weight:bold;font-family:arial,sans-serif;font-size:12.8px">
    The TouristTown Team</span><br style="font-family:arial,sans-serif;font-size:12.8px">
   <div class="yj6qo ajU"><div id=":1el" class="ajR" role="button" tabindex="0" data-tooltip="Show trimmed content" aria-label="Show trimmed content"><img class="ajT" src="//ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif">
    </div>
    </div>
    <span class="">
    <font color="#888888">
    <br clear="all"></font></span></div>
</body>
</html>';
//print_r($message);

    $mail = new PHPMailer();
    $mail->From = MAIN_CONTACT_EMAIL;
    $mail->AddReplyTo(MAIN_CONTACT_EMAIL, 'Tourist Town');
    $mail->FromName = MAIN_CONTACT_NAME;
    $mail->IsHTML(true);

    $mail->AddAddress($row['email']);
    if ($days == 0) {
        $mail->Subject = "Your credit card ending $cardNo[1] is expiring today";
    } else {
        $mail->Subject = "Your credit card ending $cardNo[1] is expiring in next $days days";
    }
    $mail->MsgHTML($message);
//    $mail->Send();
}
?>

