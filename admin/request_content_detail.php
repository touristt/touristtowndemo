<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS']) || $_SESSION['USER_SHOW_BUSINESSES'] != 0) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['id'] > 0) {
    $RCC_ID = $_REQUEST['id'];
    $sql = "SELECT * FROM tbl_Request_Content_Change LEFT JOIN tbl_Business_Listing ON BL_ID = RCC_BL_ID WHERE RCC_ID = " . $RCC_ID;
    if (!in_array('superadmin', $_SESSION['USER_ROLES']) && in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) {
        $sql .= " AND RCC_User_ID = " . $_SESSION['USER_ID'];
    }
    $result = mysql_query($sql, $db);
    $row = mysql_fetch_assoc($result);
}
if (isset($_POST['request'])) {
    $sql = "UPDATE tbl_Request_Content_Change SET RCC_Status = '" . encode_strings($_REQUEST['completed'], $db) . "' WHERE RCC_ID='" . encode_strings($_REQUEST['rcc_id'], $db) . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['rcc_id'];
        Track_Data_Entry('Change Requests', '', 'Manage Requests - Request Details', $id, 'Update', 'super admin');
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: request-change-completed.php");
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Requests - Request Details</div>
        <div class="link">
        </div>
    </div>

    <div class="left">
        <?php require_once '../include/nav-manage-request.php'; ?>
    </div>

    <div class="right">
        <!--Step One-->
        <form name="form1" method="post" action="" enctype="multipart/form-data" >
            <input type="hidden" name="rcc_id" value="<?php echo $row['RCC_ID'] ?>">
            <div class="content-header">Request Details</div>
            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Listings</label>
                <div class="form-data request-change-detail-margin-top" >
                    <?php if (in_array('superadmin', $_SESSION['USER_ROLES'])) { ?>
                        <a href="customer-listing-contact-details.php?bl_id=<?php echo $row['BL_ID'] ?>" target="_blank"><?php echo $row['BL_Listing_Title'] ?></a>
                        <?php
                    } else {
                        echo $row['BL_Listing_Title'];
                    }
                    ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Description</label>
                <div class="form-data request-change-detail-margin-top" >
                    <?php echo $row['RCC_Description'] ?>
                </div>
            </div>
            <?php if ($row['RCC_Image'] != '') { ?>
                <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                    <label>Photo</label>
                    <div class="form-data" >
                        <div class="approved-photo-div">
                            <img id="uploadFile" src="<?php echo ($row['RCC_Image'] != '') ? 'http://' . DOMAIN . IMG_LOC_REL . $row['RCC_Image'] : '' ?>">          
                        </div>
                    </div>
                    <?php
                    if (in_array('superadmin', $_SESSION['USER_ROLES']) && $row['RCC_Image'] != '') {
                        ?>
                        <div class="download-photo-req">
                            <a href="http://<?php echo DOMAIN . IMG_LOC_REL . $row['RCC_Image'] ?>" download="<?php echo $row['RCC_Image']; ?>">Download this photo</a>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
            }
            if (in_array('superadmin', $_SESSION['USER_ROLES'])) {
                ?>
                <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                    <label>Mark Done</label>
                    <div class="form-data margin-top-req-check-box">
                        <input type="checkbox" name="completed" value="1" <?php echo ($row['RCC_Status'] == 1) ? 'checked' : '' ?> <?php echo ($_REQUEST['done'] == 1) ? 'disabled' : '' ?> >
                    </div>
                </div>
                <?php if ($_REQUEST['done'] != 1) {
                    ?>
                    <div class="form-inside-div border-none margin-bottom-26 margin-top-11 form-inside-div-width-admin">
                        <div class="button">
                            <input type="submit" name="request" value="Save"/>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </form>
    </div>
</div>
<script>
    $(function ()
    {
        //autocomplete_listings
        $("#autocomplete_lisitngs").tokenInput(<?php include '../include/autocomplete_lisitngs_townasset.php'; ?>, {
            onAdd: function (item_lisitngs) {
                var list_val = $('#listing_search').val();
                if (list_val != '') {
                    $('#listing_search').val(list_val + "," + item_lisitngs.id);
                } else {
                    $('#listing_search').val(item_lisitngs.id);
                }
            },
            onDelete: function (item_lisitngs) {
                var value_listings = $('#listing_search').val().split(",");
                $('#listing_search').empty();
                var data_listings = "";
                $.each(value_listings, function (key, value_listings) {
                    if (value_listings != item_lisitngs.id) {
                        if (data_listings != '') {
                            data_listings = data_listings + "," + value_listings;
                        } else {
                            data_listings = value_listings;
                        }
                    }
                });
                $('#listing_search').val(data_listings);
            },
            resultsLimit: 10,
            preventDuplicates: true,
            tokenLimit: 1
        }
        );
        $('.token-input-dropdown').css("width", "320px");
    })
    function show_file(input) {
        if (input.files && input.files[0]) {
            $('#uploadFile').show();
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#uploadFile')
                        .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<?PHP
require_once '../include/admin/footer.php';
?>