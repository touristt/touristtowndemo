<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
if (!in_array('manage-accounting', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
require_once '../include/admin/header.php';
if (isset($_POST['accounting_btn'])) {
    $join = '';
    $region = $_POST['region'];
    $year = $_POST['year'];
    $month = $_POST['month'];
    if (in_array('superadmin', $_SESSION['USER_ROLES'])) {
        $sales_user = $_POST['sales_user'];
    } else {
        $sales_user = $_SESSION['USER_ID'];
    }
    if ($year == "" && $month == "" && $sales_user == "") {
        $date = date('Y-m');
        $display_date = date('F') . ' ' . date('Y');
        $where_clause = "AND DATE_FORMAT(BB_Date,'%Y-%m') = '$date'";
        $where_clause_refund = "AND DATE_FORMAT(BB_Refund_Deleted_Date,'%Y-%m') = '$date'";
        $where_clause_advert = " AND DATE_FORMAT(AB_Active_Date,'%Y-%m') = '$date'";
        $where_clause_division_billing = " AND DATE_FORMAT(DB_Date,'%Y-%m') = '$date'";
        $where_clause_division_billing_refund = " AND DATE_FORMAT(DB_Refund_Deleted_Date,'%Y-%m') = '$date'";
    } else if ($year == "" && $sales_user == "") {
        $where_clause = "AND DATE_FORMAT(BB_Date,'%m') = '$month'";
        $where_clause_refund = "AND DATE_FORMAT(BB_Refund_Deleted_Date,'%m') = '$month'";
        $where_clause_advert = " AND DATE_FORMAT(AB_Active_Date,'%m') = '$month'";
        $where_clause_division_billing = " AND DATE_FORMAT(DB_Date,'%m') = '$month'";
        $where_clause_division_billing_refund = " AND DATE_FORMAT(DB_Refund_Deleted_Date,'%m') = '$month'";
    } else if ($month == "" && $sales_user == "") {
        $where_clause = "AND DATE_FORMAT(BB_Date,'%Y') = '$year'";
        $where_clause_refund = "AND DATE_FORMAT(BB_Refund_Deleted_Date,'%Y') = '$year'";
        $where_clause_advert = " AND DATE_FORMAT(AB_Active_Date,'%Y') = '$year'";
        $where_clause_division_billing = " AND DATE_FORMAT(DB_Date,'%Y') = '$year'";
        $where_clause_division_billing_refund = " AND DATE_FORMAT(DB_Refund_Deleted_Date,'%Y') = '$year'";
    } else if ($month == "" && $year == "") {
        $date = date('Y-m');
        $where_clause = "AND BB_Account_Manager = '$sales_user'";
        $where_clause_refund = "AND BB_Account_Manager = '$sales_user'";
    } else if ($sales_user == "") {
        $date = $year . "-" . $month;
        $where_clause = "AND DATE_FORMAT(BB_Date,'%Y-%m') = '$date'";
        $where_clause_refund = "AND DATE_FORMAT(BB_Refund_Deleted_Date,'%Y-%m') = '$date'";
        $where_clause_advert = " AND DATE_FORMAT(AB_Active_Date,'%Y-%m') = '$date'";
        $where_clause_division_billing = " AND DATE_FORMAT(DB_Date,'%Y-%m') = '$date'";
        $where_clause_division_billing_refund = " AND DATE_FORMAT(DB_Refund_Deleted_Date,'%Y-%m') = '$date'";
        
    } else if ($year == "") {
        $where_clause = "AND DATE_FORMAT(BB_Date,'%m') = '$month' AND BB_Account_Manager = '$sales_user'";
        $where_clause_refund = "AND DATE_FORMAT(BB_Refund_Deleted_Date,'%m') = '$month' AND BB_Account_Manager = '$sales_user'";
        $where_clause_advert = " AND DATE_FORMAT(AB_Active_Date,'%m') = '$month'";
        $where_clause_division_billing = " AND DATE_FORMAT(DB_Date,'%m') = '$month'";
        $where_clause_division_billing_refund = " AND DATE_FORMAT(DB_Refund_Deleted_Date,'%m') = '$month'";
    } else if ($month == "") {
        $where_clause = "AND DATE_FORMAT(BB_Date,'%Y') = '$year' AND BB_Account_Manager = '$sales_user'";
        $where_clause_refund = "AND DATE_FORMAT(BB_Refund_Deleted_Date,'%Y') = '$year' AND BB_Account_Manager = '$sales_user'";
        $where_clause_advert = " AND DATE_FORMAT(AB_Active_Date,'%Y') = '$year'";
        $where_clause_division_billing = " AND DATE_FORMAT(DB_Date,'%Y') = '$year'";
        $where_clause_division_billing_refund = " AND DATE_FORMAT(DB_Refund_Deleted_Date,'%Y') = '$year'";
    } else if ($month != "" && $year != "" && $sales_user != "") {
        $date = $year . "-" . $month;
        $where_clause = "AND DATE_FORMAT(BB_Date,'%Y-%m') = '$date' AND BB_Account_Manager = '$sales_user'";
        $where_clause_refund = "AND DATE_FORMAT(BB_Refund_Deleted_Date,'%Y-%m') = '$date' AND BB_Account_Manager = '$sales_user'";
        $where_clause_advert = " AND DATE_FORMAT(AB_Active_Date,'%Y-%m') = '$date'";
        $where_clause_division_billing = " AND DATE_FORMAT(DB_Date,'%Y-%m') = '$date'";
        $where_clause_division_billing_refund = " AND DATE_FORMAT(DB_Refund_Deleted_Date,'%Y-%m') = '$date'";
    }

// check region
    if ($region != '') {
        $sql = "SELECT R_ID, R_Parent FROM tbl_Region WHERE R_ID = " . $region;
        $result = mysql_query($sql);
        $r = mysql_fetch_assoc($result);
        $regionList = '';
        if ($r['R_Parent'] == 0) {
            $sql = "SELECT RM_Child FROM tbl_Region_Multiple WHERE RM_Parent = '" . $r['R_ID'] . "'";
            $result = mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
            $first = true;
            $regionList .= "(";
            while ($row = mysql_fetch_assoc($result)) {
                if ($first) {
                    $first = false;
                } else {
                    $regionList .= ",";
                }
                $regionList .= $row['RM_Child'];
            }
            $regionList .= ")";
        } else {
            $regionList = '(' . $r['R_ID'] . ')';
        }
        $join .= " INNER JOIN tbl_Business_Listing_Category_Region ON BL_ID = BLCR_BL_ID ";
        $where_clause .= " AND BLCR_BLC_R_ID IN $regionList";
        $where_clause_refund .= " AND BLCR_BLC_R_ID IN $regionList";
        $where_clause_advert .= " AND BLCR_BLC_R_ID IN $regionList";
    }

    if ($month != "") {
        $display_date = date('F', strtotime("2000-$month-01")) . ' ' . $year;
    } else if ($year != "") {
        $display_date = $year;
    } else if ($sales_user != "") {
        $display_date = "";
    }
} else {
    if (in_array('superadmin', $_SESSION['USER_ROLES'])) {
        $display_date = date('F') . ' ' . date('Y');
        $date = date('Y-m');
        $where_clause = "AND DATE_FORMAT(BB_Date,'%Y-%m') = '$date'";
        $where_clause_refund = "AND DATE_FORMAT(BB_Refund_Deleted_Date,'%Y-%m') = '$date'";
        $where_clause_advert = " AND DATE_FORMAT(AB_Active_Date,'%Y-%m') = '$date'";
        $where_clause_division_billing = " AND DATE_FORMAT(DB_Date,'%Y-%m') = '$date'";
        $where_clause_division_billing_refund = " AND DATE_FORMAT(DB_Refund_Deleted_Date,'%Y-%m') = '$date'";
    } else {
        $sales_user = $_SESSION['USER_ID'];
        $where_clause = "AND BB_Account_Manager = '$sales_user'";
        $where_clause_refund = "AND BB_Account_Manager = '$sales_user'";
    }
}
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Accounting</div>
        <div class="link acconting filter">
            <form class="export_form_width acconting_export" method="post" name="export_form" action="export_accounting.php">
                <input type="hidden" name="month_filter" value="<?php echo $_REQUEST['month'] ?>">
                <input type="hidden" name="year_filter" value="<?php echo $_REQUEST['year'] ?>">
                <input type="hidden" name="region_filter" value="<?php echo $_REQUEST['region'] ?>">
                <input type="hidden" name="manager_filter" value="<?php echo $_REQUEST['sales_user'] ?>">
                <input type="submit" name="export_accounting" value="Export"/>
            </form>
            <form class="export_form_width acconting" method="post" name="form" action="accounting.php">
                <?php
                if (in_array('superadmin', $_SESSION['USER_ROLES'])) {
                    $sortregion = (isset($_REQUEST['region'])) ? $_REQUEST['region'] : '';
                    ?>
                    <div class="accounting-dd select-dropdown">
                        <select name="region">
                            <option value="">Select Region</option>
                            <?php
                            $getRegions = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Domain != ''";
                            $resRegion = mysql_query($getRegions, $db) or die("Invalid query: $getRegions -- " . mysql_error());
                            while ($rowRegion = mysql_fetch_array($resRegion)) {
                                ?>
                                <option value="<?php echo $rowRegion['R_ID'] ?>" <?php echo ($sortregion == $rowRegion['R_ID']) ? 'selected="selected"' : ''; ?>><?php echo $rowRegion['R_Name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                <?php } ?>
                <div class="accounting-dd select-dropdown">
                    <select name="month">
                        <option value="">Month</option>
                        <option value="01" <?php echo ($month == 1) ? "selected" : ""; ?>>January</option>
                        <option value="02" <?php echo ($month == 2) ? "selected" : ""; ?>>February</option>
                        <option value="03" <?php echo ($month == 3) ? "selected" : ""; ?>>March</option>
                        <option value="04" <?php echo ($month == 4) ? "selected" : ""; ?>>April</option>
                        <option value="05" <?php echo ($month == 5) ? "selected" : ""; ?>>May</option>
                        <option value="06" <?php echo ($month == 6) ? "selected" : ""; ?>>June</option>
                        <option value="07" <?php echo ($month == 7) ? "selected" : ""; ?>>July</option>
                        <option value="08" <?php echo ($month == 8) ? "selected" : ""; ?>>August</option>
                        <option value="09" <?php echo ($month == 9) ? "selected" : ""; ?>>September</option>
                        <option value="10" <?php echo ($month == 10) ? "selected" : ""; ?>>October</option>
                        <option value="11" <?php echo ($month == 11) ? "selected" : ""; ?>>November</option>
                        <option value="12" <?php echo ($month == 12) ? "selected" : ""; ?>>December</option>
                    </select>
                </div>
                <div class="accounting-dd select-dropdown">
                    <?php $years = range(2014, date('Y')); ?>
                    <select name="year">
                        <option value="">Year</option>
                        <?php
                        foreach ($years as $y) {
                            ?>
                            <option value="<?php echo $y ?>" <?php echo ($year == $y) ? "selected" : ""; ?>><?php echo $y ?></option>
                        <?php } ?>
                    </select>
                </div>
                <?php
                if (in_array('superadmin', $_SESSION['USER_ROLES'])) {
                    ?>
                    <div class="accounting-dd select-dropdown">
                        <select name="sales_user">
                            <option value="">Account Manager</option>
                            <?php
                            $sql = "SELECT U_ID, U_F_Name, U_L_Name FROM tbl_User WHERE U_Commission != 0 ORDER BY U_ID ";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($row = mysql_fetch_array($result)) {
                                $selected = ($sales_user == $row['U_ID']) ? "selected='selected'" : "";
                                ?>
                                <option value="<?php echo $row['U_ID']; ?>" <?php echo $selected ?>><?= $row['U_F_Name'] ?> <?= $row['U_L_Name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                <?php } ?>
                <div class="accounting-dd">
                    <input type="submit" name="accounting_btn" value="Filter"/>
                </div>
            </form>
        </div>
    </div>

    <div class="right full-width">
        <!--For credit cards-->
        <div class="content-header full-width">
            <?php echo 'Listing Sales (Credit Card) - ' . $display_date ?>
        </div>
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name-accounting">Business Name</div>
            <div class="data-column padding-none spl-name-accounting">Card Holder</div>
            <div class="data-column padding-none spl-card-type-accounting">Card Type</div>
            <div class="data-column padding-none spl-card-type-accounting">Date</div>
            <div class="data-column padding-none spl-other-accounting">Inv. #</div>
            <div class="data-column padding-none spl-other-accounting">Sub-Total</div>
            <div class="data-column padding-none spl-other-accounting">Tax</div>
            <div class="data-column padding-none spl-other-accounting">Total</div>
        </div>
        <?PHP
        $listing_billings = array();
        $listing_billings_refund = array();
        $all_listing_billings = array();
        //listing billings without refunds
        $sql = "SELECT BL_ID, BL_Listing_Title, BB_ID, BB_Date, BB_Refund_Deleted_Date, BB_SubTotal3, BB_Tax, BB_Total, BB_Card_Type, BB_Card_Number, BB_First_Name, BB_Last_Name, BB_Invoice_Num,
                U_Commission, pp.first_name, pp.last_name, cd.first_name as fname, cd.last_name as lname FROM tbl_Business_Billing
                LEFT JOIN tbl_Business ON B_ID = BB_B_ID
                LEFT JOIN tbl_Business_Listing ON BL_ID = BB_BL_ID
                LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID 
                $join
                LEFT JOIN cards_details cd ON B_ID = cd.business_id
                LEFT JOIN payment_profiles pp ON pp.business_id = B_ID
                LEFT JOIN tbl_User on BB_Account_Manager = U_ID
                WHERE BB_Deleted = 0 AND BB_Payment_Type = 4 AND BB_Total > 0 $where_clause GROUP BY BB_ID ORDER BY BB_ID";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $i = 0;
        while ($row = mysql_fetch_assoc($result)) {
            $listing_billings[$i] = $row;
            $listing_billings[$i]['date'] = $row['BB_Date'];
            $i++;
        }
        //listing billings with refunds
        $sql = "SELECT BL_ID, BL_Listing_Title, BB_ID, BB_Date, BB_Refund_Deleted_Date, BB_SubTotal3, BB_Tax, BB_Total, BB_Card_Type, BB_Card_Number, BB_First_Name, BB_Last_Name, BB_Invoice_Num,
                U_Commission, BB_Refund_Deleted, pp.first_name, pp.last_name, cd.first_name as fname, cd.last_name as lname FROM tbl_Business_Billing
                LEFT JOIN tbl_Business ON B_ID = BB_B_ID
                LEFT JOIN tbl_Business_Listing ON BL_ID = BB_BL_ID
                LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID 
                $join
                LEFT JOIN cards_details cd ON B_ID = cd.business_id
                LEFT JOIN payment_profiles pp ON pp.business_id = B_ID
                LEFT JOIN tbl_User on BB_Account_Manager = U_ID
                WHERE BB_Deleted = 0 AND BB_Payment_Type = 4 AND BB_Total > 0 AND BB_Refund_Deleted = 1 $where_clause_refund GROUP BY BB_ID ORDER BY BB_ID";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $i = 0;
        while ($row = mysql_fetch_assoc($result)) {
            $listing_billings_refund[$i] = $row;
            $listing_billings_refund[$i]['date'] = $row['BB_Refund_Deleted_Date'];
            $i++;
        }
        $all_listing_billings = array_merge($listing_billings, $listing_billings_refund);

        function sortListing($a, $b) {
            return strtotime($a['date']) - strtotime($b['date']);
        }

        usort($all_listing_billings, "sortListing");

        $subtotal = $tax = $total = $refund_subtotal = $refund_tax = $refund_total = 0;
        
        $overall_refund_subtotal = $overall_refund_tax = $overall_refund_total = 0;
        $overall_listing_subtotal = $overall_listing_tax = $overall_listing_total = 0;
        $overall_subtotal = $overall_tax = $overall_total = 0;
        
        foreach ($all_listing_billings as $row) {  //echo '<pre>';  print_r($row);
            $commission_percent = $row['U_Commission'];
            //show refunded columns in red
            $refund_class = "";
            if (isset($row['BB_Refund_Deleted']) && $row['BB_Refund_Deleted'] == 1) {
                $date = date("m/d/Y", strtotime($row['BB_Refund_Deleted_Date']));
                $refund_class = " refund";
                $refund_subtotal += $row['BB_SubTotal3'];
                $refund_tax += $row['BB_Tax'];
                $refund_total += $row['BB_Total'];
            } else {
                $date = date("m/d/Y", strtotime($row['BB_Date']));
                $subtotal = $subtotal + $row['BB_SubTotal3'];
                $tax = $tax + $row['BB_Tax'];
                $total = $total + $row['BB_Total'];
            }
            ?>
            <div class="data-content<?php echo $refund_class; ?>">
                <div class="data-column spl-name-acc-font spl-name-accounting"><a href="customer-bill.php?bl_id=<?php echo $row['BL_ID'] ?>&id=<?php echo $row['BB_ID'] ?>"><?php echo $row['BL_Listing_Title'] ?></a></div>
                <div class="data-column spl-name-acc-font spl-name-accounting">
                    <?php
                    if (isset($row['BB_First_Name'])) {
                        echo $row['BB_First_Name'] . ' ' . $row['BB_Last_Name'];
                    } else {
                        echo $row['fname'] . ' ' . $row['lname'];
                    }
                    ?>
                </div>
                <div class="data-column spl-name-acc-font spl-card-type-accounting">
                    <?php
                    if ($row['BB_Card_Type'] != '') {
                        if ($row['BB_Card_Type'] == 'Visa') {
                            echo 'V';
                        } else if ($row['BB_Card_Type'] == 'MasterCard') {
                            echo 'MC';
                        } else if ($row['BB_Card_Type'] == 'American Express') {
                            echo 'A';
                        } else {
                            echo 'N/A';
                        }
                    } else {
                        echo 'N/A';
                    }
                    ?>
                </div>
                <div class="data-column spl-name-acc-font spl-card-type-accounting"><?php echo $date ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo $row['BB_Invoice_Num'] ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($refund_class == '') ? "$" . $row['BB_SubTotal3'] : "($" . $row['BB_SubTotal3'] . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($refund_class == '') ? "$" . $row['BB_Tax'] : "($" . $row['BB_Tax'] . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($refund_class == '') ? "$" . $row['BB_Total'] : "($" . $row['BB_Total'] . ")" ?></div>
            </div>   
            <?PHP
        }
        // variables to calculate monthly subtotal, tax and total for listings 
        $overall_listing_subtotal = $subtotal - $refund_subtotal; 
        $overall_listing_tax = $tax - $refund_tax; 
        $overall_listing_total = $total - $refund_total;
        // variables to calculate overall subtotal, tax and total for listings 
        $overall_subtotal += $subtotal;
        $overall_tax += $tax;
        $overall_total += $total;
        $overall_refund_subtotal += $refund_subtotal;
        $overall_refund_tax += $refund_tax;
        $overall_refund_total += $refund_total;
        $commission = $subtotal / 100;
        $commission = round($commission * $commission_percent, 2);
        $grand_sub_total = $grand_sub_total + $subtotal;
        $grand_tax_total = $grand_tax_total + $tax;
        $grand_total = $grand_total + $total;
        ?>
        <div class="data-content">
            <div class="data-column spl-name-acc-font spl-total-accounting">Subtotal</div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($subtotal >= 0) ? "$" . $subtotal : "($" . $subtotal . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($tax >= 0) ? "$" . $tax : "($" . $tax . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($total >= 0) ? "$" . $total : "($" . $total . ")" ?></div>
        </div>
        <!--Refunds -->
        <div class="data-content refund-color">
            <div class="data-column spl-name-acc-font spl-total-accounting">Refunds</div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "($" . $refund_subtotal . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "($" . $refund_tax . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "($" . $refund_total . ")" ?></div>
        </div>
        
        <div class="data-content">
            <div class="data-column spl-name-acc-font spl-total-accounting">Total</div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($subtotal - $refund_subtotal >= 0) ? "$" . ($subtotal - $refund_subtotal) : "($" . ($subtotal - $refund_subtotal) . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($tax - $refund_tax >= 0) ? "$" . ($tax - $refund_tax) : "($" . ($tax - $refund_tax) . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($total - $refund_total >= 0) ? "$" . ($total - $refund_total) : "($" . ($total - $refund_total) . ")" ?></div>
        </div>
        <?php if ($sales_user != "") { ?>
            <div class="data-content">
                <div class="data-column spl-name-acc-font spl-com-acccounting">Commission <?php echo "(" . $commission_percent . "%)"; ?> </div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "$" . $commission ?></div>
            </div>
        <?php } ?>

        <!--For Listing Cheques-->
        <div class="content-header full-width">
            <?php echo 'Listing Sales (Cheque/Cash/E-transfer) - ' . $display_date ?>
        </div>
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name-accounting">Business Name</div>
            <div class="data-column padding-none spl-name-accounting"></div>
            <div class="data-column padding-none spl-card-type-accounting">Payment Method</div>
            <div class="data-column padding-none spl-card-type-accounting">Date</div>
            <div class="data-column padding-none spl-other-accounting">Inv. #</div>
            <div class="data-column padding-none spl-other-accounting">Sub-Total</div>
            <div class="data-column padding-none spl-other-accounting">Tax</div>
            <div class="data-column padding-none spl-other-accounting">Total</div>
        </div>
        <?PHP
        $listing_billings_nonCC = array();
        $listing_billings_nonCC_refund = array();
        $all_listing_billings_nonCC = array();
        $sql = "SELECT BL_ID, BL_Listing_Title, BB_ID, BB_Date, BB_Refund_Deleted_Date, BB_SubTotal3, BB_Tax, BB_Total, BB_Invoice_Num,
                U_Commission, PT_Name FROM tbl_Business_Billing bb 
                LEFT JOIN tbl_Business b ON b.B_ID = bb.BB_B_ID 
                LEFT JOIN tbl_Business_Listing bl ON bl.BL_ID = bb.BB_BL_ID 
                LEFT JOIN tbl_Business_Listing_Category blc ON bl.BL_ID = blc.BLC_BL_ID
                LEFT JOIN tbl_Payment_Type ON BB_Payment_Type = PT_ID
                $join
                LEFT JOIN cards_details cd ON b.B_ID = cd.business_id
                LEFT JOIN payment_profiles pp ON pp.business_id = b.B_ID
                LEFT JOIN tbl_User u on bb.BB_Account_Manager = u.U_ID
                WHERE BB_Deleted = 0 AND BB_Payment_Type != 4 AND BB_Total > 0 $where_clause 
                GROUP BY BB_ID ORDER BY BB_ID";
        $i = 0;
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($row = mysql_fetch_assoc($result)) {
            $listing_billings_nonCC[$i] = $row;
            $listing_billings_nonCC[$i]['date'] = $row['BB_Date'];
            $i++;
        }
        //listing billings with refunds
        $sql = "SELECT BL_ID, BL_Listing_Title, BB_ID, BB_Date, BB_SubTotal3, BB_Tax, BB_Total, BB_Invoice_Num,
                U_Commission, PT_Name, BB_Refund_Deleted_Date, BB_Refund_Deleted
                FROM tbl_Business_Billing
                LEFT JOIN tbl_Business ON B_ID = BB_B_ID
                LEFT JOIN tbl_Business_Listing ON BL_ID = BB_BL_ID
                LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID 
                LEFT JOIN tbl_Payment_Type ON BB_Payment_Type = PT_ID
                $join
                LEFT JOIN cards_details cd ON B_ID = cd.business_id
                LEFT JOIN payment_profiles pp ON pp.business_id = B_ID
                LEFT JOIN tbl_User on BB_Account_Manager = U_ID
                WHERE BB_Deleted = 0 AND BB_Payment_Type != 4 AND BB_Total > 0 AND BB_Refund_Deleted = 1 
                $where_clause_refund GROUP BY BB_ID ORDER BY BB_ID";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $i = 0;
        while ($row = mysql_fetch_assoc($result)) {
            $listing_billings_nonCC_refund[$i] = $row;
            $listing_billings_nonCC_refund[$i]['date'] = $row['BB_Refund_Deleted_Date'];
            $i++;
        }
        $all_listing_billings_nonCC = array_merge($listing_billings_nonCC, $listing_billings_nonCC_refund);

        function sortListingNonCC($a, $b) {
            return strtotime($a['date']) - strtotime($b['date']);
        }

        usort($all_listing_billings_nonCC, "sortListingNonCC");

        $subtotal = $tax = $total = $refund_subtotal = $refund_tax = $refund_total = 0;

        foreach ($all_listing_billings_nonCC as $row) {
            $commission_percent = $row['U_Commission'];
            //show refunded columns in red
            $refund_class = "";
            if (isset($row['BB_Refund_Deleted']) && $row['BB_Refund_Deleted'] == 1) {
                $date = date("m/d/Y", strtotime($row['BB_Refund_Deleted_Date']));
                $refund_class = " refund";
                $refund_subtotal += $row['BB_SubTotal3'];
                $refund_tax += $row['BB_Tax'];
                $refund_total += $row['BB_Total'];
            } else {
            $date = date("m/d/Y", strtotime($row['BB_Date']));
            $subtotal = $subtotal + $row['BB_SubTotal3'];
            $tax = $tax + $row['BB_Tax'];
            $total = $total + $row['BB_Total'];
            }
            ?>
            <div class="data-content<?php echo $refund_class; ?>">
                <div class="data-column spl-name-acc-font spl-name-accounting"><a href="customer-bill.php?bl_id=<?php echo $row['BL_ID'] ?>&id=<?php echo $row['BB_ID'] ?>"><?php echo $row['BL_Listing_Title'] ?></a></div>
                <div class="data-column spl-name-acc-font spl-name-accounting"></div>
                <div class="data-column spl-name-acc-font spl-card-type-accounting"><?php echo $row['PT_Name'] ?></div>
                <div class="data-column spl-name-acc-font spl-card-type-accounting"><?php echo $date ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo $row['BB_Invoice_Num'] ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($refund_class == '') ? "$" . $row['BB_SubTotal3'] : "($" . $row['BB_SubTotal3'] . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($refund_class == '') ? "$" . $row['BB_Tax'] : "($" . $row['BB_Tax'] . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($refund_class == '') ? "$" . $row['BB_Total'] : "($" . $row['BB_Total'] . ")" ?></div>
            </div>   
            <?PHP
        }
        $overall_listing_subtotal += $subtotal - $refund_subtotal;
        $overall_listing_tax += $tax - $refund_tax;
        $overall_listing_total += $total - $refund_total;
        
        $overall_subtotal += $subtotal;
        $overall_tax += $tax;
        $overall_total += $total;
        $overall_refund_subtotal += $refund_subtotal;
        $overall_refund_tax += $refund_tax;
        $overall_refund_total += $refund_total;
        $commission = $subtotal / 100;
        $commission = round($commission * $commission_percent, 2);
        $grand_sub_total = $grand_sub_total + $subtotal;
        $grand_tax_total = $grand_tax_total + $tax;
        $grand_total = $grand_total + $total;
        ?>
        <div class="data-content">
            <div class="data-column spl-name-acc-font spl-total-accounting">Subtotal</div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($subtotal >= 0) ? "$" . $subtotal : "($" . $subtotal . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($tax >= 0) ? "$" . $tax : "($" . $tax . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($total >= 0) ? "$" . $total : "($" . $total . ")" ?></div>
        </div>
        
        <!--Refunds -->
        <div class="data-content refund-color">
            <div class="data-column spl-name-acc-font spl-total-accounting">Refunds</div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "($" . $refund_subtotal . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "($" . $refund_tax . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "($" . $refund_total . ")" ?></div>
        </div>
        
        <div class="data-content">
            <div class="data-column spl-name-acc-font spl-total-accounting">Total</div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($subtotal - $refund_subtotal >= 0) ? "$" . ($subtotal - $refund_subtotal) : "($" . ($subtotal - $refund_subtotal) . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($tax - $refund_tax >= 0) ? "$" . ($tax - $refund_tax) : "($" . ($tax - $refund_tax) . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($total - $refund_total >= 0) ? "$" . ($total - $refund_total) : "($" . ($total - $refund_total) . ")" ?></div>
        </div>
        
        <div class="data-content bold">
            <div class="data-column spl-name-acc-font spl-total-accounting">Monthly Listing Sales Total:</div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($overall_listing_subtotal >= 0) ? "$" . $overall_listing_subtotal : "($" . $overall_listing_subtotal . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($overall_listing_tax >= 0) ? "$" . $overall_listing_tax : "($" . $overall_listing_tax . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($overall_listing_total >= 0) ? "$" . $overall_listing_total : "($" . $overall_listing_total . ")" ?></div>
        </div>
        
        <?php if ($sales_user != "") { ?>
            <div class="data-content">
                <div class="data-column spl-name-acc-font spl-com-acccounting">Commission <?php echo "(" . $commission_percent . "%)"; ?> </div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "$" . $commission ?></div>
            </div>
        <?php } ?>

        <!--For Advertisement Credit Cards-->
        <?php if (strtotime($date) < strtotime('2017-02')) { ?>
            <div class="content-header full-width">
                <?php echo 'Advertising Sales (Credit Card) - ' . $display_date ?>
            </div>
            <div class="content-sub-header">
                <div class="data-column padding-none spl-name-accounting">Business Name</div>
                <div class="data-column padding-none spl-name-accounting">Card Holder</div>
                <div class="data-column padding-none spl-card-type-accounting">Card Type</div>
                <div class="data-column padding-none spl-card-type-accounting">Date</div>
                <div class="data-column padding-none spl-other-accounting">Inv. #</div>
                <div class="data-column padding-none spl-other-accounting">Sub-Total</div>
                <div class="data-column padding-none spl-other-accounting">Tax</div>
                <div class="data-column padding-none spl-other-accounting">Total</div>
            </div>
            <?PHP
            $sql_lisitng = "SELECT AB_ID, A_B_ID, AB_Active_Date, AB_SubTotal, AB_Tax, AB_Total, AB_Card_Type, AB_Invoice_Num, BL_Listing_Title, 
                            pp.first_name, pp.last_name, cd.first_name as fname, cd.last_name as lname FROM tbl_Advertisement_Billing 
                            LEFT JOIN tbl_Advertisement ON AB_A_ID = A_ID 
                            LEFT JOIN cards_details cd ON A_B_ID = cd.business_id
                            LEFT JOIN payment_profiles pp ON pp.business_id = A_B_ID  
                            LEFT JOIN tbl_Business_Listing  ON A_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID 
                            $join
                            WHERE AB_Deleted = 0 AND AB_Refund_Deleted = 0 AND AB_Payment_Type = 4 AND AB_Total > 0 $where_clause_advert GROUP BY AB_Invoice_Num";
            $result_lisitng = mysql_query($sql_lisitng, $db) or die("Invalid query: $sql_lisitng -- " . mysql_error());
            $subtotal = $tax = $total = $refund_subtotal = $refund_tax = $refund_total = 0;
            $overall_advert_subtotal = $overall_advert_tax = $overall_advert_total = 0;
            while ($row_lisitng = mysql_fetch_assoc($result_lisitng)) {
                $date_advert = date("m/d/Y", strtotime($row_lisitng['AB_Active_Date']));
                $subtotal = $subtotal + $row_lisitng['AB_SubTotal'];
                $tax = $tax + $row_lisitng['AB_Tax'];
                $total = $total + $row_lisitng['AB_Total'];
                ?> 
                <div class="data-content">
                    <div class="data-column spl-name-acc-font spl-name-accounting"><a class="support-nav-b" href="advert-bill.php?a_id=<?php echo $row_lisitng['AB_ID']; ?>&id=<?php echo $row_lisitng['A_B_ID']; ?>"><?php echo $row_lisitng['BL_Listing_Title'] ?></a></div>
                    <div class="data-column spl-name-acc-font spl-name-accounting">
                        <?php
                        if (isset($row_lisitng['first_name'])) {
                            echo $row_lisitng['first_name'] . ' ' . $row_lisitng['last_name'];
                        } else {
                            echo $row_lisitng['fname'] . ' ' . $row_lisitng['lname'];
                        }
                        ?>
                    </div>
                    <div class="data-column spl-name-acc-font spl-card-type-accounting">
                        <?php
                        if ($row_lisitng['AB_Card_Type'] != '') {
                            if ($row_lisitng['AB_Card_Type'] == 'Visa') {
                                echo 'V';
                            } else if ($row_lisitng['AB_Card_Type'] == 'MasterCard') {
                                echo 'MC';
                            } else if ($row_lisitng['AB_Card_Type'] == 'American Express') {
                                echo 'A';
                            } else {
                                echo 'N/A';
                            }
                        } else {
                            echo 'N/A';
                        }
                        ?>
                    </div>
                    <div class="data-column spl-name-acc-font spl-card-type-accounting"><?php echo $date_advert ?></div>
                    <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo $row_lisitng['AB_Invoice_Num'] ?></div>
                    <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "$" . $row_lisitng['AB_SubTotal'] ?></div>
                    <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "$" . $row_lisitng['AB_Tax'] ?></div>
                    <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "$" . $row_lisitng['AB_Total'] ?></div>
                </div>  
                <?PHP
            }
            // variables to calculate monthly subtotal, tax and total for division advertisment
            $overall_advert_subtotal += $subtotal; 
            $overall_advert_tax += $tax; 
            $overall_advert_total += $total;
            
            $overall_subtotal += $subtotal;
            $overall_tax += $tax;
            $overall_total += $total;
            $grand_sub_total = $grand_sub_total + $subtotal;
            $grand_tax_total = $grand_tax_total + $tax;
            $grand_total = $grand_total + $total;
            ?>
            <div class="data-content">
                <div class="data-column spl-name-acc-font spl-total-accounting">Subtotal</div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($subtotal >= 0) ? "$" . $subtotal : "($" . $subtotal . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($tax >= 0) ? "$" . $tax : "($" . $tax . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($total >= 0) ? "$" . $total : "($" . $total . ")" ?></div>
            </div>

            <!--Refunds -->
            <div class="data-content refund-color">
                <div class="data-column spl-name-acc-font spl-total-accounting">Refunds</div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "($" . $refund_subtotal . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "($" . $refund_tax . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "($" . $refund_total . ")" ?></div>
            </div>
            
            <div class="data-content">
                <div class="data-column spl-name-acc-font spl-total-accounting">Total</div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($subtotal - $refund_subtotal >= 0) ? "$" . ($subtotal - $refund_subtotal) : "($" . ($subtotal - $refund_subtotal) . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($tax - $refund_tax >= 0) ? "$" . ($tax - $refund_tax) : "($" . ($tax - $refund_tax) . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($total - $refund_total >= 0) ? "$" . ($total - $refund_total) : "($" . ($total - $refund_total) . ")" ?></div>
            </div>

            <!--For Advertisement Cheques-->
            <div class="content-header full-width">
                <?php echo 'Advertising Sales (Cheque/Cash/E-transfer) - ' . $display_date ?>
            </div>
            <div class="content-sub-header">
                <div class="data-column padding-none spl-name-accounting">Business Name</div>
                <div class="data-column padding-none spl-name-accounting"></div>
                <div class="data-column padding-none spl-card-type-accounting">Payment Method</div>
                <div class="data-column padding-none spl-card-type-accounting">Date</div>
                <div class="data-column padding-none spl-other-accounting">Inv. #</div>
                <div class="data-column padding-none spl-other-accounting">Sub-Total</div>
                <div class="data-column padding-none spl-other-accounting">Tax</div>
                <div class="data-column padding-none spl-other-accounting">Total</div>
            </div>
            <?PHP
            $sql_lisitng = "SELECT AB_ID, A_B_ID, AB_Active_Date, AB_SubTotal, AB_Tax, AB_Total, AB_Card_Type, AB_Invoice_Num, BL_Listing_Title, 
                            pp.first_name, pp.last_name, cd.first_name as fname, cd.last_name as lname, PT_Name FROM tbl_Advertisement_Billing 
                            LEFT JOIN tbl_Advertisement ON AB_A_ID = A_ID 
                            LEFT JOIN cards_details cd ON A_B_ID = cd.business_id
                            LEFT JOIN payment_profiles pp ON pp.business_id = A_B_ID  
                            LEFT JOIN tbl_Business_Listing  ON A_BL_ID = BL_ID
                            LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID 
                            LEFT JOIN tbl_Payment_Type ON AB_Payment_Type = PT_ID
                            $join
                            WHERE AB_Deleted = 0 AND (AB_Payment_Type = 1 OR AB_Payment_Type = 3 OR AB_Payment_Type = 5) AND AB_Total > 0 $where_clause_advert GROUP BY AB_Invoice_Num";
            $result_lisitng = mysql_query($sql_lisitng, $db) or die("Invalid query: $sql_lisitng -- " . mysql_error());
            $subtotal = $tax = $total = $refund_subtotal = $refund_tax = $refund_total = 0;
            while ($row_lisitng = mysql_fetch_assoc($result_lisitng)) {
                $date_advert = date("m/d/Y", strtotime($row_lisitng['AB_Active_Date']));
                $subtotal = $subtotal + $row_lisitng['AB_SubTotal'];
                $tax = $tax + $row_lisitng['AB_Tax'];
                $total = $total + $row_lisitng['AB_Total'];
                ?> 
                <div class="data-content">
                    <div class="data-column spl-name-acc-font spl-name-accounting"><a class="support-nav-b" href="advert-bill.php?a_id=<?php echo $row_lisitng['AB_ID']; ?>&id=<?php echo $row_lisitng['A_B_ID']; ?>"><?php echo $row_lisitng['BL_Listing_Title'] ?></a></div>
                    <div class="data-column spl-name-acc-font spl-name-accounting"></div>
                    <div class="data-column spl-name-acc-font spl-card-type-accounting"><?php echo $row_lisitng['PT_Name'] ?></div>
                    <div class="data-column spl-name-acc-font spl-card-type-accounting"><?php echo $date_advert ?></div>
                    <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo $row_lisitng['AB_Invoice_Num'] ?></div>
                    <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "$" . $row_lisitng['AB_SubTotal'] ?></div>
                    <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "$" . $row_lisitng['AB_Tax'] ?></div>
                    <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "$" . $row_lisitng['AB_Total'] ?></div>
                </div>  
                <?PHP
            }
            $overall_advert_subtotal += $subtotal; 
            $overall_advert_tax += $tax; 
            $overall_advert_total += $total;
            
            $overall_subtotal += $subtotal;
            $overall_tax += $tax;
            $overall_total += $total;
            $grand_sub_total = $grand_sub_total + $subtotal;
            $grand_tax_total = $grand_tax_total + $tax;
            $grand_total = $grand_total + $total;
            ?>
            <div class="data-content">
                <div class="data-column spl-name-acc-font spl-total-accounting">Subtotal</div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($subtotal >= 0) ? "$" . $subtotal : "($" . $subtotal . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($tax >= 0) ? "$" . $tax : "($" . $tax . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($total >= 0) ? "$" . $total : "($" . $total . ")" ?></div>
            </div>
            <!--Refunds -->
            <div class="data-content refund-color">
                <div class="data-column spl-name-acc-font spl-total-accounting">Refunds</div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "($" . $refund_subtotal . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "($" . $refund_tax . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "($" . $refund_total . ")" ?></div>
            </div>
            <div class="data-content">
                <div class="data-column spl-name-acc-font spl-total-accounting">Total</div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($subtotal >= 0) ? "$" . $subtotal : "($" . $subtotal . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($tax >= 0) ? "$" . $tax : "($" . $tax . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($total >= 0) ? "$" . $total : "($" . $total . ")" ?></div>
            </div>
            
            <div class="data-content bold">
            <div class="data-column spl-name-acc-font spl-total-accounting">Monthly Advertising Sales Total:</div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($overall_advert_subtotal >= 0) ? "$" . $overall_advert_subtotal : "($" . $overall_advert_subtotal . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($overall_advert_tax >= 0) ? "$" . $overall_advert_tax : "($" . $overall_advert_tax . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($overall_advert_total >= 0) ? "$" . $overall_advert_total : "($" . $overall_advert_total . ")" ?></div>
        </div>
        <?php } ?>
        <!--For Division Billing Credit Cards-->
        <div class="content-header full-width">
            <?php echo 'Division Sales (Credit Card) - ' . $display_date ?>
        </div>
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name-accounting">Business Name</div>
            <div class="data-column padding-none spl-name-accounting">Card Holder</div>
            <div class="data-column padding-none spl-card-type-accounting">Card Type</div>
            <div class="data-column padding-none spl-card-type-accounting">Date</div>
            <div class="data-column padding-none spl-other-accounting">Inv. #</div>
            <div class="data-column padding-none spl-other-accounting">Sub-Total</div>
            <div class="data-column padding-none spl-other-accounting">Tax</div>
            <div class="data-column padding-none spl-other-accounting">Total</div>
        </div>
        <?PHP
        $division_billings = array();
        $division_billings_refund = array();
        $all_division_billings = array();
        //division billings without refunds
        $sql_db = "SELECT DB_ID, DB_Customer_Name, DB_Date, DB_Subtotal, DB_Tax, DB_Total, DB_Refund_Deleted_Date, DB_Card_Type, BL_ID, BL_Listing_Title,
                   DB_First_Name, DB_Last_Name FROM tbl_Division_Billing LEFT JOIN tbl_Business_Listing ON DB_BL_ID = BL_ID
                   LEFT JOIN payment_profiles ON business_id = BL_B_ID WHERE DB_PT_ID = 4 AND DB_Total > 0 AND DB_Deleted = 0 $where_clause_division_billing ORDER BY DB_ID";
        $result_db = mysql_query($sql_db, $db) or die("Invalid query: $sql_db -- " . mysql_error());
        $i = 0;
        while ($row_db = mysql_fetch_assoc($result_db)) {
            $division_billings[$i] = $row_db;
            $division_billings[$i]['date'] = $row_db['DB_Date'];
            $i++;
        }
        $sql_db = "SELECT DB_ID, DB_Customer_Name, DB_Date, DB_Subtotal, DB_Tax, DB_Total, DB_Refund_Deleted, DB_Refund_Deleted_Date, DB_Card_Type, BL_ID, BL_Listing_Title,
                   DB_First_Name, DB_Last_Name FROM tbl_Division_Billing LEFT JOIN tbl_Business_Listing ON DB_BL_ID = BL_ID
                   LEFT JOIN payment_profiles ON business_id = BL_B_ID WHERE DB_PT_ID = 4 AND DB_Total > 0 AND DB_Deleted = 0 AND DB_Refund_Deleted = 1 $where_clause_division_billing_refund ORDER BY DB_ID";
        $result_db = mysql_query($sql_db, $db) or die("Invalid query: $sql_db -- " . mysql_error());
        $i = 0;
        while ($row_db = mysql_fetch_assoc($result_db)) {
            $division_billings_refund[$i] = $row_db;
            $division_billings_refund[$i]['date'] = $row_db['DB_Refund_Deleted_Date'];
            $i++;
        }
        $all_division_billings = array_merge($division_billings, $division_billings_refund);

        function sortDivision($a, $b) {
            return strtotime($a['date']) - strtotime($b['date']);
        }

        usort($all_division_billings, "sortDivision");

        $subtotal = $tax = $total = $refund_subtotal = $refund_tax = $refund_total = 0;
        $overall_billing_subtotal = $overall_billing_tax = $overall_billing_total = 0;
        foreach ($all_division_billings as $row_db) {
            //show refunded columns in red
            $refund_class = "";
            if (isset($row_db['DB_Refund_Deleted']) && $row_db['DB_Refund_Deleted'] == 1) {
                $date_db = date("m/d/Y", strtotime($row_db['DB_Refund_Deleted_Date']));
                $refund_class = " refund";
                $refund_subtotal += $row_db['DB_Subtotal'];
                $refund_tax += $row_db['DB_Tax'];
                $refund_total += $row_db['DB_Total'];
            } else {
                $date_db = date("m/d/Y", strtotime($row_db['DB_Date']));
                $subtotal = $subtotal + $row_db['DB_Subtotal'];
                $tax = $tax + $row_db['DB_Tax'];
                $total = $total + $row_db['DB_Total'];
            }
            ?> 
            <div class="data-content<?php echo $refund_class; ?>">
                <div class="data-column spl-name-acc-font spl-name-accounting">
                    <a class="support-nav-b" href="division-billing-invoice.php?db_id=<?php echo $row_db['DB_ID']; ?>">
                        <?php
                        if ($row_db['BL_ID'] > 0) {
                            echo $row_db['BL_Listing_Title'];
                        } else {
                            echo $row_db['DB_Customer_Name'];
                        }
                        ?>
                    </a>
                </div>
                <div class="data-column spl-name-acc-font spl-name-accounting">
                    <?php
                    if (isset($row_db['DB_First_Name'])) {
                        echo $row_db['DB_First_Name'] . ' ' . $row_db['DB_Last_Name'];
                    }
                    ?>
                </div>
                <div class="data-column spl-name-acc-font spl-card-type-accounting">
                    <?php
                    if ($row_db['DB_Card_Type'] != '') {
                        if ($row_db['DB_Card_Type'] == 'Visa') {
                            echo 'V';
                        } else if ($row_db['DB_Card_Type'] == 'MasterCard') {
                            echo 'MC';
                        } else if ($row_db['DB_Card_Type'] == 'American Express') {
                            echo 'A';
                        } else {
                            echo 'N/A';
                        }
                    } else {
                        echo 'N/A';
                    }
                    ?>
                </div>
                <div class="data-column spl-name-acc-font spl-card-type-accounting"><?php echo $date_db ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo $row_db['DB_ID'] ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($refund_class == '') ? "$" . $row_db['DB_Subtotal'] : "($" . $row_db['DB_Subtotal'] . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($refund_class == '') ? "$" . $row_db['DB_Tax'] : "($" . $row_db['DB_Tax'] . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($refund_class == '') ? "$" . $row_db['DB_Total'] : "($" . $row_db['DB_Total'] . ")" ?></div>
            </div>  
            <?PHP
        }
        $overall_billing_subtotal = $subtotal - $refund_subtotal; 
        $overall_billing_tax = $tax - $refund_tax; 
        $overall_billing_total = $total - $refund_total;
        $overall_subtotal += $subtotal;
        $overall_tax += $tax;
        $overall_total += $total;
        $overall_refund_subtotal += $refund_subtotal;
        $overall_refund_tax += $refund_tax;
        $overall_refund_total += $refund_total;
        $grand_division_sub_total = $grand_division_sub_total + $subtotal;
        $grand_division_tax_total = $grand_division_tax_total + $tax;
        $grand_division_total = $grand_division_total + $total;
        ?>
        <div class="data-content">
            <div class="data-column spl-name-acc-font spl-total-accounting">Subtotal</div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($subtotal >= 0) ? "$" . $subtotal : "($" . $subtotal . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($tax >= 0) ? "$" . $tax : "($" . $tax . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($total >= 0) ? "$" . $total : "($" . $total . ")" ?></div>
        </div>
        <!--Refunds-->
        <div class="data-content refund-color">
            <div class="data-column spl-name-acc-font spl-total-accounting">Refunds</div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "($" . $refund_subtotal . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "($" . $refund_tax . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "($" . $refund_total . ")" ?></div>
        </div>
        
        <div class="data-content">
            <div class="data-column spl-name-acc-font spl-total-accounting">Total</div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($subtotal - $refund_subtotal >= 0) ? "$" . ($subtotal - $refund_subtotal) : "($" . ($subtotal - $refund_subtotal) . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($tax - $refund_tax >= 0) ? "$" . ($tax - $refund_tax) : "($" . ($tax - $refund_tax) . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($total - $refund_total >= 0) ? "$" . ($total - $refund_total) : "($" . ($total - $refund_total) . ")" ?></div>
        </div>
        
        <!--For Division Billing Cheques-->
        <div class="content-header full-width">
            <?php echo 'Division Sales (Cheque/Cash/E-transfer) - ' . $display_date ?>
        </div>
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name-accounting">Business Name</div>
            <div class="data-column padding-none spl-name-accounting"></div>
            <div class="data-column padding-none spl-card-type-accounting">Payment Method</div>
            <div class="data-column padding-none spl-card-type-accounting">Date</div>
            <div class="data-column padding-none spl-other-accounting">Inv. #</div>
            <div class="data-column padding-none spl-other-accounting">Sub-Total</div>
            <div class="data-column padding-none spl-other-accounting">Tax</div>
            <div class="data-column padding-none spl-other-accounting">Total</div>
        </div>
        <?PHP
        $division_billings_nonCC = array();
        $division_billings_nonCC_refund = array();
        $all_division_nonCC_billings = array();
        $sql_db = "SELECT DB_ID, DB_Customer_Name, DB_Date, DB_Subtotal, DB_Tax, DB_Total, BL_ID, BL_Listing_Title, PT_Name 
                   FROM tbl_Division_Billing LEFT JOIN tbl_Business_Listing ON DB_BL_ID = BL_ID
                   LEFT JOIN payment_profiles ON business_id = BL_B_ID
                   LEFT JOIN tbl_Payment_Type ON DB_PT_ID = PT_ID
                   WHERE DB_PT_ID != 4 AND DB_Total > 0 AND DB_Deleted = 0 $where_clause_division_billing 
                   ORDER BY DB_ID";
        $result_db = mysql_query($sql_db, $db) or die("Invalid query: $sql_db -- " . mysql_error());
        $i = 0;
        while ($row_db = mysql_fetch_assoc($result_db)) {
            $division_billings_nonCC[$i] = $row_db;
            $division_billings_nonCC[$i]['date'] = $row_db['DB_Date'];
            $i++;
        }
        $sql_db = "SELECT DB_ID, DB_Customer_Name, DB_Date, DB_Subtotal, DB_Tax, DB_Total, BL_ID, BL_Listing_Title, PT_Name, 
                   DB_Refund_Deleted, DB_Refund_Deleted_Date
                   FROM tbl_Division_Billing LEFT JOIN tbl_Business_Listing ON DB_BL_ID = BL_ID
                   LEFT JOIN payment_profiles ON business_id = BL_B_ID 
                   LEFT JOIN tbl_Payment_Type ON DB_PT_ID = PT_ID
                   WHERE DB_PT_ID != 4 AND DB_Total > 0 AND DB_Deleted = 0 AND 
                   DB_Refund_Deleted = 1 $where_clause_division_billing_refund ORDER BY DB_ID";
        $result_db = mysql_query($sql_db, $db) or die("Invalid query: $sql_db -- " . mysql_error());
        $i = 0;
        while ($row_db = mysql_fetch_assoc($result_db)) {
            $division_billings_nonCC_refund[$i] = $row_db;
            $division_billings_nonCC_refund[$i]['date'] = $row_db['DB_Refund_Deleted_Date'];
            $i++;
        }
        $all_division_nonCC_billings = array_merge($division_billings_nonCC, $division_billings_nonCC_refund);

        function sortDivisionNonCC($a, $b) {
            return strtotime($a['date']) - strtotime($b['date']);
        }

        usort($all_division_nonCC_billings, "sortDivisionNonCC");

        $subtotal = $tax = $total = $refund_subtotal = $refund_tax = $refund_total = 0;
        foreach ($all_division_nonCC_billings as $row_db) {
            //show refunded columns in red
            $refund_class = "";
            if (isset($row_db['DB_Refund_Deleted']) && $row_db['DB_Refund_Deleted'] == 1) {
                $date_db = date("m/d/Y", strtotime($row_db['DB_Refund_Deleted_Date']));
                $refund_class = " refund";
                $refund_subtotal += $row_db['DB_Subtotal'];
                $refund_tax += $row_db['DB_Tax'];
                $refund_total += $row_db['DB_Total'];
            } else {
            $date_db = date("m/d/Y", strtotime($row_db['DB_Date']));
            $subtotal = $subtotal + $row_db['DB_Subtotal'];
            $tax = $tax + $row_db['DB_Tax'];
            $total = $total + $row_db['DB_Total'];
            }
            ?> 
            <div class="data-content<?php echo $refund_class; ?>">
                <div class="data-column spl-name-acc-font spl-name-accounting">
                    <a class="support-nav-b" href="division-billing-invoice.php?db_id=<?php echo $row_db['DB_ID']; ?>">
                        <?php
                        if ($row_db['BL_ID'] > 0) {
                            echo $row_db['BL_Listing_Title'];
                        } else {
                            echo $row_db['DB_Customer_Name'];
                        }
                        ?>
                    </a>
                </div>
                <div class="data-column spl-name-acc-font spl-name-accounting"></div>
                <div class="data-column spl-name-acc-font spl-card-type-accounting"><?php echo $row_db['PT_Name'] ?></div>
                <div class="data-column spl-name-acc-font spl-card-type-accounting"><?php echo $date_db ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo $row_db['DB_ID'] ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($refund_class == '') ? "$" . $row_db['DB_Subtotal'] : "($" . $row_db['DB_Subtotal'] . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($refund_class == '') ? "$" . $row_db['DB_Tax'] : "($" . $row_db['DB_Tax'] . ")" ?></div>
                <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($refund_class == '') ? "$" . $row_db['DB_Total'] : "($" . $row_db['DB_Total'] . ")" ?></div>
            </div>  
            <?PHP
        }
        $overall_billing_subtotal += $subtotal - $refund_subtotal;
        $overall_billing_tax += $tax - $refund_tax;
        $overall_billing_total += $total - $refund_total;
        $overall_subtotal += $subtotal;
        $overall_tax += $tax;
        $overall_total += $total;
        $overall_refund_subtotal += $refund_subtotal;
        $overall_refund_tax += $refund_tax;
        $overall_refund_total += $refund_total;
        $grand_division_sub_total = $grand_division_sub_total + $subtotal;
        $grand_division_tax_total = $grand_division_tax_total + $tax;
        $grand_division_total = $grand_division_total + $total;
        ?>
        <div class="data-content">
            <div class="data-column spl-name-acc-font spl-total-accounting">Subtotal</div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($subtotal >= 0) ? "$" . $subtotal : "($" . $subtotal . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($tax >= 0) ? "$" . $tax : "($" . $tax . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($total >= 0) ? "$" . $total : "($" . $total . ")" ?></div>
        </div>
        <!--Refunds -->
        <div class="data-content refund-color">
            <div class="data-column spl-name-acc-font spl-total-accounting">Refunds</div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "($" . $refund_subtotal . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "($" . $refund_tax . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo "($" . $refund_total . ")" ?></div>
        </div>
        <div class="data-content">
            <div class="data-column spl-name-acc-font spl-total-accounting">Total</div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($subtotal - $refund_subtotal >= 0) ? "$" . ($subtotal - $refund_subtotal) : "($" . ($subtotal - $refund_subtotal) . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($tax - $refund_tax >= 0) ? "$" . ($tax - $refund_tax) : "($" . ($tax - $refund_tax) . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($total - $refund_total >= 0) ? "$" . ($total - $refund_total) : "($" . ($total - $refund_total) . ")" ?></div>
        </div>
        <div class="data-content bold">
            <div class="data-column spl-name-acc-font spl-total-accounting">Monthly Division Sales Total:</div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($overall_billing_subtotal >= 0) ? "$" . $overall_billing_subtotal : "($" . $overall_billing_subtotal . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($overall_billing_tax >= 0) ? "$" . $overall_billing_tax : "($" . $overall_billing_tax . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($overall_billing_total >= 0) ? "$" . $overall_billing_total : "($" . $overall_billing_total . ")" ?></div>
        </div>
        <!--Grand Total-->
        <div class="content-sub-header">
            <div class="data-column spl-name-acc-font spl-total-accounting">
                Monthly Grand Total 
            </div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($overall_subtotal - $overall_refund_subtotal >= 0) ? "$" . ($overall_subtotal - $overall_refund_subtotal) : "($" . ($overall_subtotal - $overall_refund_subtotal) . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($overall_tax - $overall_refund_tax >= 0) ? "$" . ($overall_tax - $overall_refund_tax) : "($" . ($overall_tax - $overall_refund_tax) . ")" ?></div>
            <div class="data-column spl-name-acc-font spl-other-accounting"><?php echo ($overall_total - $overall_refund_total >= 0) ? "$" . ($overall_total - $overall_refund_total) : "($" . ($overall_total - $overall_refund_total) . ")" ?></div>
        </div>
        <div class="form-inside-div form-inside-div-width-admin full-width accounting_page_print ">
            <div class="button">
                <input type="button" onclick="window.print();" name="print_accounting" value="Print">

            </div>
        </div>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>