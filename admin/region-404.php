<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
$rpid = $_REQUEST['rpid'];
$regionID = $_REQUEST['rid'];
if (isset($_REQUEST['rid']) && $_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
    $rpid = $_REQUEST['rpid'];
    $sql = "SELECT R_ID, R_Name, R_Type, RP_ID, RP_Photo, RP_Photo_Title, RP_Title, RP_Description FROM tbl_Region LEFT JOIN tbl_Region_404 ON R_ID = RP_RID 
            WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
    
} else {
    header("Location: /admin/regions.php");
    exit();
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $coupons .= "tbl_Region_404 SET
                RP_Photo_Title = '" . encode_strings($_REQUEST['sliderPhotoTitle'], $db) . "', 
                RP_Title  = '" . encode_strings($_REQUEST['descriptionTitle'], $db) . "', 
                RP_Description = '" . encode_strings($_REQUEST['description'], $db) . "'";
    if ($_POST['image_bank'] == "") {

        require '../include/picUpload.inc.php';
        // last @param 1 = Gallery Images
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 10, 'region', $regionID);

        if (is_array($pic)) {
            $coupons .= ", RP_Photo = '" . encode_strings($pic['0']['0'], $db) . "'
                         , RP_Photo_Mobile = '" . encode_strings($pic['0']['1'], $db) . "'";
            $pic_id = $pic['1'];
        }
    } else {
        $pic_id = $_POST['image_bank'];
        require '../include/picUpload.inc.php';
        // last @param 1 = Gallery Images
        $pic = Upload_Pic_Library($pic_id, 10);

        if (is_array($pic)) {
            Update_Image_Bank_Listings($pic_id, $regionID);
            $coupons .= ", RP_Photo = '" . encode_strings($pic['0'], $db) . "'
                         , RP_Photo_Mobile = '" . encode_strings($pic['1'], $db) . "'";
        }
    }
    if ($rpid > 0) {
        $couponQuery = "UPDATE $coupons WHERE RP_ID = '" . encode_strings($rpid, $db) . "'";
        $resCoupon = mysql_query($couponQuery, $db) or die("Invalid query: $couponQuery -- " . mysql_error());
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $regionID, 'Manage 404 Page', '', 'Update', 'super admin');
    } else {
        $coupons .= ", RP_RID = '" . encode_strings($regionID, $db) . "'";
        $couponQuery = "INSERT INTO  $coupons";
        $resCoupon = mysql_query($couponQuery, $db) or die("Invalid query: $couponQuery -- " . mysql_error());
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $regionID, 'Manage 404 Page', '', 'Add', 'super admin');
    }

    if ($resCoupon) {
        $_SESSION['success'] = 1;
        if (isset($pic_id) && $pic_id > 0) {
            //Image usage from image bank
            imageBankUsage($pic_id, 'IBU_R_ID', $regionID, 'IBU_Region_Theme', 'Page Not Found');
        }
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: region-404.php?rid=" . $regionID . "&type=" . $activeRegion['R_Type']);
    exit();
}

if (isset($_REQUEST['option']) && $_REQUEST['option'] == 'delet') {
    require_once '../include/picUpload.inc.php';
    $update = "UPDATE tbl_Region_404 SET";
    if ($_REQUEST['flag'] == 'slider_logo') {
        $update .= " RP_Photo = '', RP_Photo_Mobile= ''";
        if ($activeRegion['RP_Photo']) {
            Delete_Pic(IMG_LOC_ABS . $activeRegion['RP_Photo']);
        }
    }
    $update .=" WHERE RP_RID = '" . encode_strings($regionID, $db) . "'";
    $result = mysql_query($update);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $regionID, 'Manage 404 Page', '', 'Delete Photo', 'super admin');
        imageBankUsageDelete('IBU_R_ID', $regionID, 'IBU_Region_Theme', 'Page Not Found');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header('location:region-404.php?rid=' . $regionID . "&type=" . $activeRegion['R_Type']);
    exit;
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Manage 404 Page</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <form name="form1" method="post" enctype="multipart/form-data" action="" id="gallery-form" style="float:left;width:100%;margin-bottom: 10px;">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <input type="hidden" name="rpid" value="<?php echo $activeRegion['RP_ID'] ?>">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="image_bank" class="image_bank" id="image_bank10" value="">
            <div class="form-inside-div form-inside-div-width-admin">
                <label class="slider">Slider Photo</label>
                <div class="form-data form-input-text">
                    <div class="div_image_library">
                        <span class="daily_browse" onclick="show_image_library(10)">Select File</span>
                        <input onchange="show_file_name(10, this, 0)" id="photo10" type="file" name="pic[]" style="display: none;"/>
                    </div>
                    <div class="form-inside-div add-about-us-item-image margin-none">
                        <div class="cropit-image-preview aboutus-photo-perview">
                            <img class="preview-img preview-img-script10" style="display: none;" src="">    
                            <?php if (isset($activeRegion['RP_Photo']) && $activeRegion['RP_Photo'] != '') { ?>
                                <img class="existing-img existing_imgs10" src="<?php echo IMG_LOC_REL . $activeRegion['RP_Photo'] ?>" >
                            <?php } ?>

                        </div>
                        <?php if ($activeRegion['RP_Photo'] != '') { ?>
                            <a class="deletePhoto delete-region-cat-photo" style="margin-top: 10px;margin-left: 0;float: left;" onclick="return confirm('Are you sure you want to delete photo?');" href="region-404.php?rid=<?php echo $regionID ?>&flag=slider_logo&option=delet">Delete Photo</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <label>Slider Title</label>
                <div class="form-data">
                    <input name="sliderPhotoTitle" type="text"  value="<?php echo $activeRegion['RP_Photo_Title'] ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Description Title</label>
                <div class="form-data">
                    <input name="descriptionTitle" type="text"  value="<?php echo $activeRegion['RP_Title'] ?>" size="255" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Description</label>
                <div class="form-data"> 
                    <textarea name="description" cols="85" rows="10" class="tt-description"><?php echo $activeRegion['RP_Description'] ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin">
                <div class="button">
                    <input type="submit" name="button" id="button" value="Submit" />
                </div>
            </div>
        </form>
        <div id="image-library" style="display:none;"></div>
    </div>
</div>

<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>