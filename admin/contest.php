<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('manage-contests', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['id'] > 0) {
    $sql = "SELECT * FROM tbl_Contests WHERE C_ID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
}

if ($_POST['op'] == 'save') {
    $sql = "tbl_Contests SET 
            C_Name = '" . encode_strings($_REQUEST['name'], $db) . "', 
            C_SEO_Name = '" . encode_strings($_REQUEST['seoName'], $db) . "', 
            C_Text = '" . encode_strings($_REQUEST['text'], $db) . "', 
            C_Code = '" . encode_strings($_REQUEST['code'], $db) . "'";

    require_once '../include/picUpload.inc.php';
    $pic = Upload_Pic('0', 'pic', 760, 310, true, IMG_LOC_ABS);
    if ($pic) {
        $sql .= ", C_Header = '" . encode_strings($pic, $db) . "'";

        if ($row['C_Header']) {
            Delete_Pic(IMG_LOC_ABS . $row['C_Header']);
        }
    }

    if ($_REQUEST['id'] > 0) {
        $sql = "UPDATE " . $sql . " WHERE C_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    } else {
        $sql = "INSERT " . $sql;
    }
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    
    if ($_REQUEST['id'] > 0) {
        $id = $_REQUEST['id'];
    } else {
        $id = mysql_insert_id($db);
    }

    $sql = "DELETE FROM tbl_Contests_Regions WHERE CR_C_ID = '" . $id . "'";
    mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    if (is_array($_POST['regions'])) {
        foreach ($_POST['regions'] as $val) {
            $sql = "INSERT tbl_Contests_Regions SET CR_C_ID = '" . $id . "', CR_R_ID = '" . $val . "'";
            mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        }
    }

    if ($_POST['newBusiness'] > 0) {
        $sql = "INSERT tbl_Contests_Business SET 
                CB_C_ID = '" . $id . "',
                CB_B_ID = '" . $_POST['newBusiness'] . "',
                CB_Prize = '" . $_POST['newPrize'] . "',
                CB_Order = '99'";
        mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    }

    if (is_array($_POST['prize'])) {
        foreach ($_POST['prize'] as $key => $val) {
            if ($_POST['del'][$key]) {
                $sql = "DELETE FROM tbl_Contests_Business WHERE CB_ID = '" . $key . "'";
                mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            } else {
                $sql = "UPDATE tbl_Contests_Business SET 
                        CB_Prize = '" . $val . "',
                        CB_Order = '" . $_POST['order'][$key] . "' 
                        WHERE CB_ID = '" . $key . "'";
                mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            }
        }
    }

    if (is_array($_POST['delRedirect'])) {
        foreach ($_POST['delRedirect'] as $val) {
            $sql = "DELETE FROM tbl_Contests_Redirect WHERE CR_ID = '" . $val . "'";
            mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        }
    }
    if ($_POST['newRedirectTitle'] && $_POST['newRedirectPage']) {
        $sql = "INSERT tbl_Contests_Redirect SET 
                CR_C_ID = '" . $id . "',
                CR_Title = '" . $_POST['newRedirectTitle'] . "',
                CR_Page = '" . $_POST['newRedirectPage'] . "'";
        mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
         
    }

    header("Location: contest.php?id=" . $id);
    exit();
} elseif ($_GET['op'] == 'del') {

    require_once '../include/picUpload.inc.php';
    if ($row['C_Header']) {
        Delete_Pic(IMG_LOC_ABS . $row['C_Header']);
    }

    $sql = "DELETE tbl_Contests, tbl_Contests_Business, tbl_Contests_Entry, 
            tbl_Contests_Redirect, tbl_Contests_Regions 
            FROM tbl_Contests 
            LEFT JOIN tbl_Contests_Business ON CB_C_ID = C_ID 
            LEFT JOIN tbl_Contests_Entry ON CE_C_ID = C_ID
            LEFT JOIN tbl_Contests_Redirect ON tbl_Contests_Redirect.CR_C_ID = C_ID 
            LEFT JOIN tbl_Contests_Regions ON tbl_Contests_Regions.CR_C_ID = C_ID 
            WHERE C_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $_SESSION['delete'] = 1;
    header("Location: contests.php");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo  $row['C_Name'] ?></div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <div class="sub-menu">
            <ul>
                <li><a href="/admin/contests.php">Manage Contests</a></li>
                <li><a href="/admin/contest.php">+Add Contest</a></li>
            </ul>
        </div>
    </div>
    <div class="right">
        <form name="form1" method="post" action="" enctype="multipart/form-data">
            <input type="hidden" name="id" value="<?php echo  $row['C_ID'] ?>">
            <input type="hidden" name="op" value="save">
            <div class="content-header">Contest Information</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Contest Name</label>
                <div class="form-data">
                    <input name="name" type="text" id="Bname" value="<?php echo  $row['C_Name'] ?>" size="50"  <?PHP if (!$row['B_ID']) { ?>onKeyUp="makeSEO(this.value)" onChange="makeSEO(this.value)"<?PHP } ?> />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>SEO Name</label>
                <div class="form-data">
                    <input name="seoName" type="text" id="seoName" value="<?php echo  $row['C_SEO_Name'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Code</label>
                <div class="form-data">
                    <input name="code" type="text" id="code" value="<?php echo  $row['C_Code'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Text</label>
                <div class="form-data">
                    <?php
                    include_once "../ckeditor/ckeditor.php";
                    $CKEditor = new CKEditor();
                    $CKEditor->basePath = 'http://' . DOMAIN . '/ckeditor/';
                    $CKEditor->config['width'] = 430;
                    $CKEditor->config['height'] = 250;
                    $CKEditor->config['toolbar'] = 'Basic';
                    $CKEditor->editor("text", $row['C_Text']);
                    ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Header</label>
                <div class="form-data">
                    <span class="inputWrapper">&nbsp;&nbsp;Browse 
                        <input class="fileInput" type="file" name="pic[]" id="photo-<?php echo  $activeRegion['RI_ID'] ?>"/>
                    </span>
                    <?PHP
                    if ($row['C_Header']) {
                        echo '<a href="' . IMG_LOC_REL . $row['C_Header'] . '" target="_blank">View</a>';
                    } else {
                        echo "&nbsp;";
                    }
                    ?>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin border-none"> 
                <div class="button">
                    <input type="submit" name="button2" id="button2" value="Submit" />
                </div>
            </div>
            <div class="content-header">Regions</div>
            <?PHP
            $sql = "SELECT CR_R_ID FROM tbl_Contests_Regions WHERE CR_C_ID = '" . encode_strings($row['C_ID'], $db) . "'";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $contestRegions = array();
            while ($rowR = mysql_fetch_row($result)) {
                $contestRegions[] = $rowR[0];
            }

            $sql = "SELECT R_ID, R_Name FROM tbl_Region ORDER BY R_Name";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $counter = 0;

            while ($rowR = mysql_fetch_assoc($result)) {
                if ($counter == 0) {
                    echo '<div class="data-content">';
                }
                ?>
                <div class="data-column data-column-checkbox">
                    <input type="checkbox" name="regions[<?php echo  $rowR['R_ID'] ?>]" value="<?php echo  $rowR['R_ID'] ?>" <?php echo  in_array($rowR['R_ID'], $contestRegions) ? 'checked' : '' ?>>
                </div>
                <div class="data-column spl-name-users"><?php echo  $rowR['R_Name'] ?></div>
                <?PHP
                $counter++;
                if ($counter == 3) {
                    echo "</div>";
                    $counter = 0;
                }
            }

            if ($counter > 1) {
                echo "</div>";
            } elseif ($counter > 0) {
                echo "</div>";
            }
            ?>
            <div class="form-inside-div form-inside-div-width-admin border-none"> 
                <div class="button">
                    <input type="submit" name="button2" id="button2" value="Submit" />
                </div>
            </div>
            <div class="content-header">Buisnesses</div>
            <div class="content-sub-header">
                <div class="data-column padding-none spl-name-users">Business</div>
                <div class="data-column padding-none spl-other-email">Prize</div>
                <div class="data-column padding-none spl-other">Order</div>
                <div class="data-column padding-none spl-other">Delete</div>
            </div>
            <?PHP
            $sql = "SELECT BL_Listing_Title, C_Name, CB_ID, CB_Prize
                    FROM tbl_Contests_Business 
                    LEFT JOIN tbl_Business_Listing ON CB_B_ID = BL_ID 
                    LEFT JOIN tbl_Category ON BL_C_ID = C_ID
                    WHERE CB_C_ID = '" . encode_strings($row['C_ID'], $db) . "' 
                    ORDER BY CB_Order";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $numRows = mysql_num_rows($result);
            $counter = 0;
            while ($rowB = mysql_fetch_assoc($result)) {
                ?>
                <div class="data-content"> 
                    <div class="data-column spl-name-users">
                        <?php echo  $rowB['BL_Listing_Title'] ?>: <?php echo  $rowB['C_Name'] ?>
                    </div>
                    <div class="data-column spl-name-users">
                        <input type="text" name="prize[<?php echo  $rowB['CB_ID'] ?>]" value="<?php echo  $rowB['CB_Prize'] ?>">
                    </div>
                    <div class="data-column spl-other">
                        <select name="order[<?php echo  $rowB['CB_ID'] ?>]">
                            <?PHP for ($i = 0; $i < $numRows; $i++) { ?>
                                <option value="<?php echo  $i ?>"<?php echo  $i == $counter ? 'selected' : '' ?>><?php echo  $i + 1 ?></option>
                            <?PHP } ?>
                        </select>
                    </div>
                    <div class="data-column spl-other">
                        <input type="checkbox" name="del[<?php echo  $rowB['CB_ID'] ?>]" value="1">
                    </div>
                </div>
                <?PHP
                $counter++;
            }
            ?>
            <div class="data-content"> 
                <div class="data-column spl-name-users">
                    <select name="newBusiness" style="width: 200px;">
                        <option value="0">Select Business To Add</option>
                        <?PHP
                        $sql = "SELECT DISTINCT BL_Listing_Title, C_Name, BL_ID FROM tbl_Business_Listing
                                LEFT JOIN tbl_Business_Listing_Category ON BLC_M_C_ID = BL_ID 
                                LEFT JOIN tbl_Category ON BLC_M_C_ID = C_ID ORDER BY BL_Listing_Title";
                        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                        while ($rowB = mysql_fetch_assoc($result)) {
                            ?>
                            <option value="<?php echo  $rowB['BL_ID'] ?>"><?php echo  $rowB['BL_Listing_Title'] ?>: <?php echo  $rowB['C_Name'] ?></option>
                            <?PHP
                        }
                        ?>
                    </select>
                </div>
                <div class="data-column spl-name-users"><input type="text" name="newPrize"></div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin border-none"> 
                <div class="button">
                    <input type="submit" name="button2" id="button2" value="Submit" />
                </div>
            </div>
            <div class="content-header">Redirects</div>
            <div class="content-sub-header">
                <div class="data-column padding-none spl-name-users">Title</div>
                <div class="data-column padding-none spl-other-email">Address</div>
                <div class="data-column padding-none spl-other">Delete</div>
            </div>
            <?PHP
            $sql = "SELECT * FROM tbl_Contests_Redirect WHERE CR_C_ID = '" . encode_strings($row['C_ID'], $db) . "' ORDER BY CR_Title";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            while ($rowB = mysql_fetch_assoc($result)) {
                ?>
                <div class="data-content">
                    <div class="data-column spl-name-users"><?php echo  $rowB['CR_Title'] ?></div>
                    <div class="data-column spl-other-email"><?php echo  $rowB['CR_Page'] ?></div>
                    <div class="data-column spl-other"><input type="checkbox" name="delRedirect[]" value="<?php echo  $rowB['CR_ID'] ?>"></div>
                </div>
                <?PHP
            }
            ?>
            <div class="data-content">
                <div class="data-column spl-name-users"><input type="text" name="newRedirectTitle"></div>
                <div class="data-column spl-name-users"><input type="text" name="newRedirectPage"></div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin border-none">
                <div class="button">
                    <input type="submit" name="button2" id="button2" value="Submit" />
                </div>
            </div>
        </form>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>