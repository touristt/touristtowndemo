<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('categories', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
    $activeID = $_REQUEST['id'];
} else {
    $sql = "SELECT C_ID FROM tbl_Category WHERE C_Parent = 0 ORDER BY C_Order LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
    $activeID = $row['C_ID'];
}

$sql = "SELECT C_ID, C_Name, C_Parent FROM tbl_Category WHERE C_ID = '" . encode_strings($activeID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeCat = mysql_fetch_assoc($result);

if (isset($_GET['op']) && $_GET['op'] == 'del') {
    $sql = "DELETE tbl_Category FROM tbl_Category WHERE C_ID = '" . $_REQUEST['subcat'] . "'";
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['delete'] = 1;
        // TRACK DATA ENTRY
        $id = $_REQUEST['subcat'];
        Track_Data_Entry('Categories','','Manage Categories',$id,'Delete Sub-Category','super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    header("Location: category.php?id=" . $_REQUEST['id']);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Categories<?php echo isset($activeCat['C_Name']) ? ' - ' . $activeCat['C_Name'] : '' ?></div>
        <div class="link">
            <a href="category-add.php?parent=<?php echo $activeID ?>">+Add Sub-Category</a>
            <a href="category-add.php">+Add Category</a>
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-managecategories.php'; ?>
    </div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column spl-name padding-none"><?php echo $activeCat['C_Name'] ?></div>
            <div class="data-column spl-other padding-none">Edit</div>
            <div class="data-column spl-other padding-none">Delete</div>
        </div>
        <div class="reorder-category">
            <?PHP
            $sql = "SELECT C_ID, C_Name, C_Parent FROM tbl_Category WHERE C_Parent = '" . encode_strings($activeID, $db) . "' ORDER BY C_Order";
            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $counter = 0;
            while ($row = mysql_fetch_assoc($result)) {
                $counter++;
                ?>
                <div class="data-content" id="recordsArray_<?php echo $row['C_ID'] ?>">
                    <form id="form3" name="form3" enctype="multipart/form-data" method="post" action="category.php">
                        <input type="hidden" name="op" value="save">
                        <div class="data-content">
                            <div class="data-column spl-name">
                                <?php echo $row['C_Name'] ?>
                            </div>
                            <div class="data-column spl-other">
                                <a href="category-add.php?id=<?php echo $row['C_ID'] ?>">Edit</a>
                            </div>
                            <div class="data-column spl-other"><a onClick="return confirm('Are you sure?')" href="category.php?id=<?php echo $row['C_Parent'] ?>&subcat=<?php echo $row['C_ID'] ?>&op=del">Delete</a></div>
                        </div>
                    </form>
                </div>
                <?PHP
            }
            ?>
            <div id="image-library" style="display:none;"></div>
        </div>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script>
    $(function () {
        $(".reorder-category").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Category&field=C_Order&id=C_ID';
                $.post("reorder.php", order, function (theResponse) {
                    swal("Categories Re-Ordered", "Categories Re-Ordered successfully.", "success");
                });
            }
        });
    });
</script>