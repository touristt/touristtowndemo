<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/track-data-entry.php';

if (!in_array('manage-routes', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
} else {
    header("Location: /admin/regions.php");
    exit();
}

if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $RC_ID = $_REQUEST['id'];
    $RID = $_REQUEST['rid'];
    $sql = "DELETE tbl_Route_Category, tbl_Individual_Route_Category
            FROM tbl_Route_Category
            LEFT JOIN tbl_Individual_Route_Category ON RC_ID = IRC_RC_ID
            WHERE RC_ID = '" . $RC_ID . "'";
    $result = mysql_query($sql) or die(mysql_error());
    if($result) {
      $_SESSION['delete'] = 1;
      // TRACK DATA ENTRY
        Track_Data_Entry('Websites', $RID, 'Manage Route Categories', $RC_ID, 'Delete Category Route', 'super admin');
      imageBankUsageDelete('IBU_Route_Category', $RC_ID);
    }
    else {
      $_SESSION['delete_error'] = 1; 
    }
    header("Location: routes-categories.php?rid=" . $RID);
    exit();
}
//Get Active Region Information
$sql = "SELECT R_Type, R_ID, R_Name, R_Type FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$activeRegion = mysql_fetch_assoc($result);

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo $activeRegion['R_Name'] ?> - Manage Routes</div>
        <div class="link"><a href="routes.php?rid=<?php echo $regionID ?>">Individual Routes</a></div>
        <div class="link"><a href="route-category.php?rid=<?php echo $regionID ?>">+ Add Category</a></div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">
          Manage Route Categories
       </div>
        <div class="content-sub-header">
            <div class="data-column mup-name padding-none">Category</div>
            <div class="data-column spl-other width padding-none">Active</div>
            <div class="data-column spl-other width padding-none">Edit</div>
            <div class="data-column spl-other width padding-none">Routes</div>
            <div class="data-column spl-other width padding-none">Delete</div>
        </div>
        <div class="reorder-category">
            <?PHP
            $sqlCat = "SELECT RC_ID, RC_Name, RC_Status, RC_R_ID FROM tbl_Route_Category WHERE RC_R_ID = '". $activeRegion['R_ID'] ."' ORDER BY RC_Order";

            $resCat = mysql_query($sqlCat, $db) or die("Invalid query: $sqlCat -- " . mysql_error());
            while ($rowCat = mysql_fetch_assoc($resCat)) {
                ?>
                <div class="data-content" id="recordsArray_<?php echo $rowCat['RC_ID'] ?>">
                    <div class="data-column mup-name">
                      <?php echo $rowCat['RC_Name']; ?>
                    </div>
                    <div class="data-column spl-other width">
                        <?php
                        if (isset($rowCat['RC_Status']) && $rowCat['RC_Status'] == 1) {
                            $checked = "checked";
                        } else {
                            $checked = "";
                        }
                        ?>
                        <input type="checkbox" name="display" id="display<?php echo $rowCat['RC_ID']; ?>" value="<?php echo $rowCat['RC_Status']; ?>" <?php echo $checked; ?> onclick=" return hide_show_listings(<?php echo $rowCat['RC_ID']; ?>);">
                    </div>
                    <div class="data-column spl-other width">
                      <a href="route-category.php?rid=<?php echo $rowCat['RC_R_ID'] ?>&id=<?php echo $rowCat['RC_ID'] ?>">Edit</a>
                    </div>
                    <div class="data-column spl-other width">
                      <a href="route-category-routes.php?rid=<?php echo $rowCat['RC_R_ID'] ?>&rcid=<?php echo $rowCat['RC_ID'] ?>">Manage Routes</a>
                    </div>
                    <div class="data-column spl-other width">
                      <a onclick="return confirm('Are you sure?')" href="routes-categories.php?op=del&rid=<?php echo $rowCat['RC_R_ID'] ?>&id=<?php echo $rowCat['RC_ID'] ?>">Delete</a>
                    </div>
                </div>
                <?PHP
            }
            ?>
        </div>
    </div>
</div>
<div id="dialog-message"></div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script>
    function hide_show_listings(route_Category_id) { 
        var value = document.getElementById('display' + route_Category_id).checked; 
        $.ajax({
            type: "POST",
            url: 'show_hide_route_category.php',
            dataType: 'json',
            data: {
                RC_ID: route_Category_id,
                show_hide: value
            },
            success: function (response) { 
                if (response == 0) {
                    swal("Route Category", "Route Category has been deactivated successfully.", "success");
                } else if (response == 1) {
                    swal("Route Category", "Route Category has been activated successfully.", "success");
                } else {
                    swal("Error", "Error saving data. Please try again.", "error");
                }
            }
        });
    }
    $(function() {
        $(".reorder-category").sortable({opacity: 0.9, cursor: 'move', update: function() {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Route_Category&field=RC_Order&id=RC_ID&r_field_name=RC_R_ID&region_id=<?php echo $activeRegion['R_ID'] ?>';
                $.post("reorder.php", order, function(theResponse) {
                  swal("Route Categories Re-Ordered!", "Route Categories Re-Ordered successfully.", "success");
                });
            }
        });
    });
</script>