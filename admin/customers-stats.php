<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
$regionLimit_id = '';
if (!in_array('customers', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$JOIN = '';
$WHERE = '';
if (isset($_POST['filer-region-id']) && $_POST['filer-region-id'] > 0) { 
    $regionLimit_id = $_POST['filer-region-id'];
    $sql = "SELECT R_ID, R_Parent, R_Type FROM tbl_Region WHERE R_ID='" . encode_strings($regionLimit_id, $db) . "'";
    $result = query($sql, $db);
    $REGION = mysql_fetch_assoc($result);
    $regionList = '';
    if ($REGION['R_Type'] == 1) {
        $sql = "SELECT R_ID FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Child WHERE RM_Parent = '" . encode_strings($REGION['R_ID'], $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $first = true;
        $regionList .= "(";
        while ($row = mysql_fetch_assoc($result)) {
            if ($first) {
                $first = false;
            } else {
                $regionList .= ",";
            }
            $regionList .= $row['R_ID'];
        }
        $regionList .= ")";
    } else {
        $regionList = '(' . $REGION['R_ID'] . ')';
    }
    $JOIN = 'INNER JOIN tbl_Business_Listing_Category_Region ON BL_ID = BLCR_BL_ID'; 
    $WHERE = 'AND BLCR_BLC_R_ID IN ' . encode_strings($regionList, $db);
     
}

$sql_regin_filter = "SELECT R_ID, R_Name FROM tbl_Region";
$result_regin_filter = mysql_query($sql_regin_filter);
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Listings - Listings Statistics</div>
        <div class="link customer-export">

            <form class="link_form_width" id="region-filter" method="post">
                <label>
                    <span class="addlisting">Sort:</span>
                </label>  
                <select name="filer-region-id" onchange="filter_region()">
                    <?php
                    while ($row_filter = mysql_fetch_assoc($result_regin_filter)) {
                        $selected = 'selected';
                        ?>
                        <option value=<?php echo $row_filter['R_ID']; ?> <?php echo ($regionLimit_id == $row_filter['R_ID']) ? $selected : '' ?>><?php echo $row_filter['R_Name']; ?></option>   
                        <?php
                    }
                    ?>
                    <option value="0" <?php echo ($regionLimit_id > 0) ? ' ' : 'selected' ?>>All</option>
                </select>
            </form>
            <form class="export_form_width" method="post" name="export_form" action="export_customer_stats.php">
                <input type="hidden" name="region_filter" value="<?php echo (isset($_REQUEST['filer-region-id'])) ? $_REQUEST['filer-region-id'] : $_SESSION['USER_LIMIT'] ?>">
                <input type="submit" name="export_stats" value="Export"/>
            </form>
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-managecustomers.php'; ?>
    </div>

    <div class="right">
        <form name="listing_form" method="post" action="#">

            <div class="content-sub-header">
                <div class="data-column mup-name padding-none">Type</div>
                <div class="data-column spl-other padding-none">Total Active</div>
                <div class="data-column spl-other padding-none">Total Inactive</div>
                <div class="data-column spl-other padding-none">Total</div>
            </div>
            <?php
            $grand_total_active = $grand_total_inactive = $grand_total_all = 0;
            // SELECTING ALL THE LISTINGS TYPES FROM DATABASE
            $sql1 = "SELECT LT_ID, LT_Name FROM tbl_Listing_Type";
            $result = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
            while ($type = mysql_fetch_assoc($result)) {

                // SELECTING ACTIVE LISTINGS .W.R.T. LISTING TYPE 
                $query = "SELECT DISTINCT BL_ID FROM tbl_Business_Listing $JOIN WHERE BL_Listing_Type = '" . $type['LT_ID'] . "' AND hide_show_listing = 1 $WHERE GROUP BY BL_ID";
                $result1 = mysql_query($query, $db) or die("Invalid query: $query -- " . mysql_error());
                $count1 = mysql_num_rows($result1);
                $grand_total_active += $count1;
                //print_r($count1);

                // SELECTING INACTIVE LISTINGS W.R.T. LISTING TYPE
                $query = "SELECT DISTINCT BL_ID FROM tbl_Business_Listing $JOIN WHERE BL_Listing_Type = '" . $type['LT_ID'] . "' AND hide_show_listing = 0 $WHERE GROUP BY BL_ID";
                $result2 = mysql_query($query, $db) or die("Invalid query: $query -- " . mysql_error());
                $count2 = mysql_num_rows($result2);
                $grand_total_inactive += $count2;
                //print_r($total_counts);
                ?>
                <div class="data-content">
                    <div class="data-column mup-name">
                        <?php echo $type['LT_Name'] . " Listings"; ?>
                    </div>

                    <div class="data-column spl-other">
                        <?php echo $count1; ?>
                    </div>
                    <div class="data-column spl-other">
                        <?php echo $count2; ?>
                    </div>
                    <div class="data-column spl-other">
                        <?php
                        echo $total_counts = $count1 + $count2;
                        ?>
                    </div>
                </div>
                <br>
                <?php
                
            }
            ?>
            <div class="content-sub-header total">
                <div class="data-column mup-name padding-none">Total</div>
                <div class="data-column spl-other padding-none">
                    <?php echo $grand_total_active; ?>
                </div>
                <div class="data-column spl-other padding-none">
                    <?php echo $grand_total_inactive; ?>
                </div>
                <div class="data-column spl-other padding-none">
                    <?php echo $total_All = $grand_total_active + $grand_total_inactive; ?>
                </div>
            </div>       

    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>