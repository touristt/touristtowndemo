<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('division-billing', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$DB_ID = $_REQUEST['db_id'];

if ($DB_ID > 0) {
    $sql = "SELECT tbl_Division_Billing.*, BL_ID, BL_Listing_Title, BL_Street, BL_Address, BL_Town, BL_Province, BL_PostalCode, PT_Name
            FROM tbl_Division_Billing 
            LEFT JOIN tbl_Business_Listing ON DB_BL_ID = BL_ID 
            LEFT JOIN tbl_Payment_Type ON DB_PT_ID = PT_ID 
            WHERE DB_ID = '" . encode_strings($DB_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowDB = mysql_fetch_assoc($result);
    if ($rowDB['BL_ID'] > 0) {
        $sql = "SELECT R_Clientname, R_Street, R_Town, R_Province, R_PostalCode FROM tbl_Business_Listing LEFT JOIN tbl_Business_Listing_Category_Region ON BL_ID = BLCR_BL_ID
                LEFT JOIN tbl_Region ON R_ID = BLCR_BLC_R_ID
                WHERE BLCR_BL_ID = '" . encode_strings($rowDB['BL_ID'], $db) . "' LIMIT 1";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $rowReg = mysql_fetch_assoc($result);
    }
} else {
    header('Location: division-billing-invoices.php');
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">

    <div class="title-link">
        <div class="title">Invoice</div>
        <div class="link">
            <?php if (in_array('delete-invoices', $_SESSION['USER_PERMISSIONS'])) { ?>
                <a class="invoices" href="/admin/division-billing-delete.php?db_id=<?php echo $rowDB['DB_ID'] ?>&bl_id=<?php echo $rowDB['DB_BL_ID'] ?>&op=del">Delete Invoice</a></td>
                <?php
            }
            if ($rowDB['DB_Refund_Deleted'] != 1) {
                $cc_refund = 0;
                if ($rowDB['DB_Transaction_ID'] > 0) {
                    $cc_refund = 1;
                }
                if (in_array('superadmin', $_SESSION['USER_ROLES'])) {
                    $current_date = strtotime(date('Y-m-d'));
                    $billing_date = strtotime($rowDB['DB_Date']);
                    $secs = $current_date - $billing_date; // == <seconds between the two times>
                    $days = $secs / 86400;
                    if ($days < 121) {
                        ?>
                        <a class="invoices" href="/admin/division-billing-delete.php?db_id=<?php echo $rowDB['DB_ID'] ?>&bl_id=<?php echo $rowDB['DB_BL_ID'] ?>&op=refund&cc_refund=<?php echo $cc_refund; ?>">Refund Invoice</a></td>
                        <?php
                    }
                }
            } else {
                echo '<span class="invoices-text">Refunded</span>';
            }
            ?>
        </div>
    </div>

    <div class="left">
        <?php require_once('../include/nav-home-sub.php'); ?>
    </div>

    <div class="right">
        <div class="page-print">
            <div class="billing-inside-div-tittle">
                <div class="data-content advert-bill-invoice-detail">
                    <div class="data-column invoice-heading">Invoice Number : </div>
                    <div class="data-column invoice-no"><?php echo $rowDB['DB_ID'] ?></div>
                    <div class="data-column invoice-date">Date Paid : </div>
                    <div class="data-column invoice-date-bb"><?php echo $rowDB['DB_Date'] ?></div>
                    <?php if ($rowDB['DB_Refund_Deleted'] == 1) { ?>
                        <div class="data-column invoice-heading"></div>
                        <div class="data-column invoice-no"></div>
                        <div class="data-column invoice-date">Date Refund : </div>
                        <div class="data-column invoice-date-bb"><?php echo date('Y-m-d', strtotime($rowDB['DB_Refund_Deleted_Date'])) ?></div>
                    <?php } ?>
                </div>
            </div>
            <?php
            if ($rowReg) {
                ?>
                <div class="invoice-address advert-bill-header">
                    <div class="data-left  inviced-left">
                        <div class="data-header-inv bold inviced-head">Invoiced To</div>
                        <div class="data-content-address margine-inviced-bottom">
                            <?php echo $rowDB['BL_Listing_Title'] != '' ? trim($rowDB['BL_Listing_Title']) : ''; ?>
                            <?php echo $rowDB['BL_Street'] != '' ? '<br>' . trim($rowDB['BL_Street']) : ''; ?>
                            <?php echo $rowDB['BL_Address'] != '' ? '<br>' . trim($rowDB['BL_Address']) : ''; ?>
                            <?php
                            echo $rowDB['BL_Town'] != '' ? '<br>' . trim($rowDB['BL_Town']) : '';
                            echo $rowDB['BL_Province'] != '' ? ', ' . trim($rowDB['BL_Province']) : '';
                            echo $rowDB['BL_PostalCode'] != '' ? ', ' . trim($rowDB['BL_PostalCode']) : '';
                            ?>
                            <br> Canada
                        </div>
                    </div>
                    <div class="data-right">
                        <div class="data-header-inv bold pay-head">Pay To</div>
                        <div class="data-content-address margine-inviced-bottom">
                            <?php
                            echo $rowReg['R_Clientname'] != '' ? trim($rowReg['R_Clientname']) : '';
                            echo $rowReg['R_Street'] != '' ? '<br>' . trim($rowReg['R_Street']) : '';
                            echo $rowReg['R_Town'] != '' ? '<br>' . trim($rowReg['R_Town']) : '';
                            echo $rowReg['R_Province'] != '' ? ', ' . trim($rowReg['R_Province']) : '';
                            echo $rowReg['R_PostalCode'] != '' ? ', ' . trim($rowReg['R_PostalCode']) : '';
                            ?>
                            <br>Canada

                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
            <div class="invoice-pay-main" style="width: 475px;">
                <div class="data-column bold payment-text type-width">Payment Type :</div>
                <div class="data-column card-name"><?php echo $rowDB['PT_Name'] ?></div>
            </div>
            <div class="card-main">
                <div class="data-column bold last-card-digits">Last four digits of card :</div>
                <?php
                $cardno = explode("XXXX", $rowDB['DB_Card_Number']);
                ?>
                <div class="data-column last-card-digits-no"><?php echo ($cardno[1] != '') ? $cardno[1] : 'N/A' ?></div>
            </div>
            <div class="data-header bold inviced-detail inviced-order-heading">Order Summary</div>
            <div class="data-content">
                <div class="data-column inviced-tools-margin ">
                    <?php
                    if ($rowDB['BL_ID'] > 0) {
                        echo $rowDB['DB_ID'] . " - " . $rowDB['BL_Listing_Title'];
                    } else {
                        echo $rowDB['DB_ID'] . " - " . $rowDB['DB_Customer_Name'];
                    }
                    ?>
                </div>
                <div class="data-column">$<?php echo $rowDB['DB_Amount'] ?> </div>
            </div>
            <?php if ($rowDB['DB_Description'] != '') { ?>
                <div class="data-content">
                    <div class="data-column inviced-tools-margin ">
                        <?php
                        echo $rowDB['DB_Description'];
                        ?>
                    </div>
                </div>
            <?php } ?>
            <div class="data-header sub-total-color">
                <div class="data-column bold inviced-sub-margin">Sub-Total 1</div>
                <div class="data-column">$<?php echo $rowDB['DB_Amount'] ?></div>
            </div>
            <div class="data-content">
                <div class="data-column bold inviced-sub-margin">Discount</div>
                <div class="data-column">$<?php echo $rowDB['DB_Discount'] ?></div>
            </div>
            <div class="data-header sub-total-color">
                <div class="data-column bold inviced-sub-margin ">Sub-Total 2</div>
                <div class="data-column">$<?php echo $rowDB['DB_Subtotal'] ?> </div>
            </div>
            <div class="data-content ">
                <div class="data-column bold inviced-sub-margin">Taxes (13%)</div>
                <div class="data-column">$<?php echo $rowDB['DB_Tax'] ?></div>
            </div>
            <div class="data-header total-invice-color ">
                <div class="data-column bold inviced-sub-margin total-invice-color">Total</div>
                <div class="data-column total-invice-color">$<?php echo $rowDB['DB_Total'] ?></div>
            </div>

        </div>
        <div class="data-content print-link">
            <div class="print-button">
                <a href="#" onclick="window.print();">Print Invoice</a>
            </div>
        </div>
    </div>
</div>
</div>

<?PHP
require_once '../include/admin/footer.php';
?>