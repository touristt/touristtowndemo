<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';
require_once '../include/track-data-entry.php';

if (!in_array('superadmin', $_SESSION['USER_ROLES'])) {
    header("Location: /admin/");
    exit();
}
$adTpSelect = $_REQUEST['adType'];
$sortregion = $_REQUEST['sortregion'];
$categorydd = $_REQUEST['categorydd'];
$subcategorydd = $_REQUEST['subcategorydd'];
$whereClause = '';
$campaign = '';
if (isset($_REQUEST['adType']) && $_REQUEST['adType'] != '') {
    $adTpSelect = $_REQUEST['adType'];
    $whereClause .= ' AND A_AT_ID = ' . $adTpSelect;
}
if (isset($_REQUEST['sortregion']) && $_REQUEST['sortregion'] != '') {
    $sortregion = $_REQUEST['sortregion'];
    $whereClause .= ' AND A_Website = ' . $sortregion;
}
if (isset($_REQUEST['categorydd']) && $_REQUEST['categorydd'] != '') {
    $categorydd = $_REQUEST['categorydd'];
    $whereClause .= ' AND A_C_ID = ' . $categorydd;
}
if (isset($_REQUEST['subcategorydd']) && $_REQUEST['subcategorydd'] != '') {
    $subcategorydd = $_REQUEST['subcategorydd'];
    $whereClause .= ' AND A_SC_ID = ' . $subcategorydd;
}
if (isset($_REQUEST['campaign']) && $_REQUEST['campaign'] != '') {
    $campaign = $_REQUEST['campaign'];
    $whereClause .= ' AND A_Campaign = ' . $campaign;
}
if (isset($_REQUEST['status']) && $_REQUEST['status'] != '') {
    $status = $_REQUEST['status'];
} else {
    $status = 3;
}
if (isset($_REQUEST['srchByLstng']) && $_REQUEST['srchByLstng'] != '') {
    $srchByLstng = $_REQUEST['srchByLstng'];
    $whereClause .= ' AND (BL_Listing_Title LIKE "%' . $srchByLstng . '%" AND hide_show_listing = 1)';
}
if (isset($_REQUEST['del_req']) && $_REQUEST['del_req'] > 0) {
    $sql = "UPDATE tbl_Advertisement SET A_Status = 4 WHERE A_ID = '" . encode_strings($_REQUEST['del_req'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $_SESSION['success'] = 1;
    // TRACK DATA ENTRY
    $id = $_REQUEST['del_req'];
    Track_Data_Entry('Campaign','','Active Campaigns',$id,'Inactive','super admin'); 
    header("Location:advertisement.php?status=$status");
    exit();
}
//delete advert completed
if (isset($_REQUEST['op']) && $_REQUEST['op'] == 'del') {
    $sql = "UPDATE tbl_Advertisement SET A_Is_Deleted = 1 WHERE A_ID = '" . encode_strings($_REQUEST['aid'], $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $_SESSION['delete'] = 1;
    // TRACK DATA ENTRY
    $id = $_REQUEST['aid'];
    Track_Data_Entry('Campaign','','Active Campaigns',$id,'Delete','super admin'); 
    header("Location:advertisement.php?status=$status");
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Campaigns</div>
        <div class="link">
        </div>
    </div>
    <div class="left advert-left-nav">
        <?php require_once '../include/nav-B-advertisement-admin.php'; ?>
    </div>
    <div class="right">
        <div class="content-header content-header-search">
            <?php
            if ($status == 2) {
                echo '<span>Pending Approval</span>';
            } else if ($status == 3) {
                echo '<span>Active Campaigns</span>';
            } else {
                echo '<span>Requested Campaigns</span>';
            }
            ?>
            <form id="formSrchLstng" name="formSrchLstng" method="GET" action="advertisement.php">
                <input type="text" name="srchByLstng" placeholder="Search by listing" value="<?php echo (isset($srchByLstng) != '') ? $srchByLstng : '' ?>">
                <input type="hidden" name="status" value="<?php echo $status ?>">
                <input type="button" onClick="getSubmitForm(this.value)" value="Go">
            </form>
            <?php if ($status == 3) { ?>
                <form class="export_form_width" method="post" name="export_form" action="export_active_ad.php">
                    <input type="hidden" name="type_filter" value="<?php echo$_REQUEST['adType'] ?>">
                    <input type="hidden" name="region_filter" value="<?php echo$_REQUEST['sortregion'] ?>">
                    <input type="hidden" name="cat_filter" value="<?php echo$_REQUEST['categorydd'] ?>">
                    <input type="hidden" name="sub_cat_filter" value="<?php echo$_REQUEST['subcategorydd'] ?>">
                    <input type="hidden" name="srchByLstng_filter" value="<?php echo$_REQUEST['srchByLstng'] ?>">
                    <input type="submit" name="export_active" value="Export"/>
                </form>
            <?php } ?>
        </div>
        <div class="link-header">

            <form id="frmSort" name="form1" method="GET" action="">
                <input type="hidden" name="status" value="<?php echo $status ?>">
                <?php
                $adTypeDD = "SELECT AT_ID, AT_Name FROM tbl_Advertisement_Type WHERE AT_ID NOT IN(3,4)";
                $resType = mysql_query($adTypeDD, $db) or die("Invalid query: $adTypeDD -- " . mysql_error());
                ?>
                <label>
                    <select name="adType" id="adType" onChange="getSubOptions(this.value)">
                        <option value="">Select Type</option>
                        <?php
                        while ($DDType = mysql_fetch_array($resType)) {
                            ?>
                            <option value="<?php echo $DDType['AT_ID'] ?>" <?php echo ($DDType['AT_ID'] == $adTpSelect) ? 'selected="selected"' : ''; ?>><?php echo $DDType['AT_Name'] ?></option>
                        <?php }
                        ?>
                    </select>
                </label>
                <label class="margin-left-label-select">
                    <?php
                    $ddRegion = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Domain != ''";
                    $ddResReg = mysql_query($ddRegion, $db) or die("Invalid query: $ddRegion -- " . mysql_error());
                    ?>
                    <select name="sortregion" id="sortregion" onChange="getSubOptions(this.value)">
                        <option value="">Select Region</option>
                        <?php
                        while ($rowDDRegion = mysql_fetch_assoc($ddResReg)) {
                            ?>
                            <option value="<?php echo $rowDDRegion['R_ID'] ?>" <?php echo ($rowDDRegion['R_ID'] == $sortregion) ? 'selected' : ''; ?>><?php echo $rowDDRegion['R_Name'] ?></option>
                        <?php }
                        ?>
                    </select>
                </label>
                <label class="margin-left-label-select">
                    <select name="categorydd" id="categorydd" onChange="getSubOptions(this.value)">
                        <option value="" selected>Category:</option>
                        <?PHP
                        if ($sortregion != '') {
                            $sqlCat = " SELECT C_ID, RC_Name, C_Name FROM `tbl_Category` 
                                        LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID
                                        LEFT JOIN tbl_Region ON RC_R_ID = R_ID
                                        WHERE R_ID = '$sortregion' AND C_Parent = 0 ORDER BY RC_Order ASC";
                        } else {
                            $sqlCat = " SELECT C_ID, C_Name FROM `tbl_Category` WHERE C_Parent = 0 AND C_Is_Blog != 1 AND C_Is_Product_Web != 1  
                                        ORDER BY C_Order ASC";
                        }
                        $resultCat = mysql_query($sqlCat, $db) or die("Invalid query: $sqlCat -- " . mysql_error());
                        while ($rowMainCat = mysql_fetch_assoc($resultCat)) {
                            ?>
                            <option value="<?php echo $rowMainCat['C_ID'] ?>" <?php echo ($rowMainCat['C_ID'] == $categorydd) ? 'selected' : ''; ?>><?php echo (isset($rowMainCat['RC_Name']) != '') ? $rowMainCat['RC_Name'] : $rowMainCat['C_Name'] ?></option>
                            <?PHP
                        }
                        ?>
                    </select>
                </label>
                <?php if ($_REQUEST['adType'] == 1) { ?>
                    <label class="margin-left-label-select">
                        <select name="subcategorydd" id="subcategorydd" onChange="getSubOptions(this.value)">
                            <option value="" selected>Sub Category:</option>
                            <?PHP
                            $subcatJoin = '';
                            $subcatWhere = '';
                            if ($categorydd != '' && $sortregion != '') {
                                $subcatJoin = "LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID
                                           LEFT JOIN tbl_Region ON RC_R_ID = R_ID";
                                $subcatWhere = "WHERE R_ID = '$sortregion' AND C_Parent = '$categorydd'";
                            }
                            if ($categorydd != '' && $sortregion == '') {
                                $subcatWhere = "WHERE C_Parent = '$categorydd'";
                            }
                            if ($categorydd == '' && $sortregion != '') {
                                $subcatJoin = "LEFT JOIN tbl_Region_Category ON C_ID = RC_C_ID
                                           LEFT JOIN tbl_Region ON RC_R_ID = R_ID";
                                $subcatWhere = "WHERE R_ID = '$sortregion'";
                            }
                            if ($sortregion == '' && $categorydd == '') {
                                $subcatWhere = "WHERE C_Parent != 0";
                            }
                            $sqlSubCat = "SELECT C_ID, C_Name FROM `tbl_Category` $subcatJoin $subcatWhere ORDER BY C_Name ASC";
                            $resultSubCat = mysql_query($sqlSubCat, $db) or die("Invalid query: $sqlSubCat -- " . mysql_error());
                            while ($rowSubCat = mysql_fetch_assoc($resultSubCat)) {
                                ?>
                                <option value="<?php echo $rowSubCat['C_ID'] ?>" <?php echo ($rowSubCat['C_ID'] == $subcategorydd) ? 'selected' : ''; ?>><?php echo $rowSubCat['C_Name'] ?></option>
                                <?PHP
                            }
                            ?>
                        </select>
                    <?php } ?>
                </label>
                <label class="margin-left-label-select">
                    <select name="campaign" onChange="getSubOptions(this.value)">
                        <option value="" <?php echo ($campaign == '') ? 'selected' : ''; ?>>Sub Campaign:</option>
                        <option value="1" <?php echo ($campaign == '1') ? 'selected' : ''; ?>>Yes</option>
                        <option value="0" <?php echo ($campaign == '0') ? 'selected' : ''; ?>>No</option>
                    </select>
                </label>
                <script type="text/javascript">
                    function getSubOptions(myVal) {
                        $('#frmSort').submit();
                    }
                    function getSubmitForm(myVal) {
                        $('#formSrchLstng').submit();
                    }
                </script>
            </form>
        </div>

        <div class="content-sub-header">
            <div class="data-column advert-listing-title padding-none">Listing</div>
            <div class="data-column advert-listing-title padding-none">Title</div>
            <div class="data-column  padding-none"></div>
            <div class="data-column advert-listing-options padding-none">Edit</div>
            <div class="data-column advert-listing-options padding-none">Delete</div>
            <div class="data-column advert-listing-options padding-none">Inactive</div>
        </div>
        <?php
        $sql = "SELECT A_ID, A_Title, A_Status, A_Third_Party, BL_Listing_Title FROM tbl_Advertisement LEFT JOIN tbl_Business_Listing ON A_BL_ID = BL_ID
                WHERE A_Status = $status AND A_Is_Deleted = 0 AND A_AT_ID NOT IN(3,4) AND (A_End_Date >= CURDATE() OR A_End_Date = 0000-00-00) $whereClause";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        while ($rowadvert = mysql_fetch_assoc($result)) {
            ?>
            <div class="data-content">
                <?php if ($rowadvert['A_Third_Party'] != "") { ?>
                    <div class="data-column advert-listing-title "><a target="_blank" href="<?php echo 'http://' . str_replace(array('http://', 'https://'), '', $rowadvert['A_Third_Party']); ?>">External Link</a></div>
                <?php } else { ?>
                    <div class="data-column advert-listing-title "><a href="buy-an-advertisement.php?advert_id=<?php echo $rowadvert['A_ID']; ?>"><?php echo $rowadvert['BL_Listing_Title']; ?></a></div>
                <?php } ?>
                <div class="data-column advert-listing-title "><?php echo $rowadvert['A_Title']; ?></div>
                <?php
                $sql_req = "SELECT ACR_Description FROM tbl_Advertisement_Change_Request
                            WHERE ACR_A_AID = '" . encode_strings($rowadvert['A_ID'], $db) . "' ORDER BY ACR_ID DESC LIMIT 1";
                $result_req = mysql_query($sql_req, $db) or die("Invalid query: $sql_req -- " . mysql_error());
                $row_req = mysql_fetch_assoc($result_req);
                if ($rowadvert['A_Status'] == 1) {
                    if ($row_req['ACR_Description'] != '') {
                        ?>
                        <div class="data-column "><a  class="avert_req" onclick="show_req(<?php echo $rowadvert['A_ID'] ?>);">Change Request</a></div>
                        <div id="request<?php echo $rowadvert['A_ID']; ?>" style=" display:none;">            
                            <div style="float:left; width:445px; padding:14px 0; margin:0 7px 0 18px; font-size:16px;">
                                <?php echo $row_req['ACR_Description']; ?> 
                            </div>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="data-column "></div>
                        <?php
                    }
                } else {
                    ?>
                    <div class="data-column "></div>
                    <?php
                }
                ?>

                <div class="data-column advert-listing-options spl-other"><a href="buy-an-advertisement.php?advert_id=<?php echo $rowadvert['A_ID']; ?>&status=<?php echo $status; ?>">Edit</a></div>
                <div class="data-column advert-listing-options spl-other"><a onClick="return confirm('Are you sure?');" href="advertisement.php?aid=<?php echo $rowadvert['A_ID']; ?>&op=del">Delete</a></div>
                <div class="data-column advert-listing-options spl-other"><a onClick="return confirm('Are you sure?');" href="advertisement.php?del_req=<?php echo $rowadvert['A_ID']; ?>&status=<?php echo $status ?>">Inactive</a></div>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<script>
    function show_req(A_ID) {
        $("#request" + A_ID).attr("title", "Request Change Description").dialog({
            modal: true,
            draggable: false,
            resizable: false,
            width: 500
        });
    }
</script>
<?PHP
require_once '../include/admin/footer.php';
?>