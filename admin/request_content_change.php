<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';

if (!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS']) || $_SESSION['USER_SHOW_BUSINESSES'] != 0) {
    header("Location: /admin/");
    exit();
}

if (isset($_POST['request'])) {
    $sql = "INSERT tbl_Request_Content_Change SET 
            RCC_BL_ID = '" . encode_strings($_REQUEST['listings'], $db) . "',
            RCC_Description = '" . encode_strings($_REQUEST['request_description'], $db) . "', 
            RCC_User_ID = '" . encode_strings($_SESSION['USER_ID'], $db) . "', 
            RCC_Status = '" . encode_strings(0, $db) . "',
            RCC_Created_Date = CURDATE()";
    require_once '../include/picUpload.inc.php';
    $pic = Upload_Pic_Normal('0', 'approve', 0, 0, true, IMG_LOC_ABS, 0);
    if ($pic) {
        $sql .= ", RCC_Image = '" . encode_strings($pic, $db) . "'";
    }
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    header("Location: request-change-pending.php");
    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Requests - Add Request</div>
        <div class="link">
        </div>
    </div>

    <div class="left">
        <?php require_once '../include/nav-manage-request.php'; ?>
    </div>

    <div class="right">
        <!--Step One-->
        <form name="form1" method="post" action="" onSubmit="return check_req_img_size(10000000)" enctype="multipart/form-data" >
            <div class="content-header">Request Details</div>
            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Listings</label>
                <div class="form-data listings_count">
                    <input id="autocomplete_lisitngs" type="text"/>
                    <input type="hidden" id="listing_search" name="listings" value="">
                </div>
            </div>
            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Description</label>
                <div class="form-data" >
                    <textarea name="request_description"></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-adv form-inside-div-width-admin">
                <label>Photo</label>
                <div class="form-data from-inside-div-text">
                    <div class="approved-photo-div">
                        <div class="inputWrapper adv-photo float-left">Browse
                            <input class="fileInput" id="approved-photo" type="file" name="approve[]" onchange="show_file(this)">
                        </div>
                    </div>
                    <div class="approved-photo-div">
                        <img id="uploadFile" style="display: none;"   src="">          
                    </div>
                </div>
            </div>
            <div class="form-inside-div border-none margin-bottom-26 margin-top-11 form-inside-div-width-admin">
                <div class="button">
                    <input type="submit" name="request" value="Save"/>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function ()
    {
        //autocomplete_listings
        $("#autocomplete_lisitngs").tokenInput(<?php include '../include/autocomplete_lisitngs_townasset.php'; ?>, {
            onAdd: function (item_lisitngs) {
                var list_val = $('#listing_search').val();
                if (list_val != '') {
                    $('#listing_search').val(list_val + "," + item_lisitngs.id);
                } else {
                    $('#listing_search').val(item_lisitngs.id);
                }
            },
            onDelete: function (item_lisitngs) {
                var value_listings = $('#listing_search').val().split(",");
                $('#listing_search').empty();
                var data_listings = "";
                $.each(value_listings, function (key, value_listings) {
                    if (value_listings != item_lisitngs.id) {
                        if (data_listings != '') {
                            data_listings = data_listings + "," + value_listings;
                        } else {
                            data_listings = value_listings;
                        }
                    }
                });
                $('#listing_search').val(data_listings);
            },
            resultsLimit: 10,
            preventDuplicates: true,
            tokenLimit: 1
        }
        );
        $('.token-input-dropdown').css("width", "320px");
    })
    function show_file(input) {
        if (input.files && input.files[0]) {
            $('#uploadFile').show();
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#uploadFile')
                        .attr('src', e.target.result);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    function check_req_img_size(image_size) {
        var image_file = $("#approved-photo").val();
        if (image_file != '') {
            var extesion_image = image_file.substr(image_file.lastIndexOf('.') + 1);
            var ext_check_image = extesion_image.toLowerCase();
            if (ext_check_image != 'jpg' && ext_check_image != 'jpeg' && ext_check_image != 'png' && ext_check_image != 'gif') {
                swal("Image!", "File Must be JPG, PNG, GIF!", "warning");
                $("#approved-photo").val('');
                $("#uploadFile").hide('');
                return false;
            }
        }       
    if (($("#approved-photo"))[0].files.length > 0) {
            var img_size = ($("#approved-photo"))[0].files[0].size;
            if (img_size > image_size) {
                    swal("Image!", "File Size must be less then 10MB!", "warning");
                $("#approved-photo").val('');
                $("#uploadFile").hide('');
                return false;
            }
        }
        }
</script>
<?PHP
require_once '../include/admin/footer.php';
?>