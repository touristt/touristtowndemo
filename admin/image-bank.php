<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';

if (!in_array('image-bank', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$join = '';
$where = 'WHERE 1=1 ';
$search_text = '';
$records = 0;
if (isset($_GET['region']) && $_GET['region'] != '') {
    $temp = true;
    $RegionType = "";
    $searchParentImage = "";
    foreach (explode(',', $_GET['region']) as $region) {
        if ($temp) {
            $temp = false;
        } else {
            $regionFilter .= ",";
            $tempregionFilter .= ",";
        }
        $sql_region = "SELECT R_ID, R_Parent FROM tbl_Region WHERE R_ID = '$region'";
        $res_region = mysql_query($sql_region);
        $regionObj = mysql_fetch_assoc($res_region);
        if ($regionObj['R_Parent'] == 0) {
            $sql_child = "SELECT R_ID, R_Parent, RM_Child FROM tbl_Region LEFT JOIN tbl_Region_Multiple ON R_ID = RM_Parent WHERE RM_Parent = " . $regionObj['R_ID'];
            $res_child = mysql_query($sql_child);
            $first = true;
            while ($child = mysql_fetch_assoc($res_child)) {
                $RegionType = $child['R_Parent'];
                $RegionID = $child['R_ID'];
                if ($RegionType == 0) {
                    $searchParentImage = " OR FIND_IN_SET($RegionID, IB_Region)";
                }
                if ($first) {
                    $first = false;
                } else {
                    $regionFilter .= ",";
                }
                $regionFilter .= $child['RM_Child'];
            }
        } else {
            $regionFilter .= $region;
        }
        $tempregionFilter .= $region;
    }
    $regionFilter = array_unique(explode(',', $regionFilter));

    //creating where clause using find_in_set to search value in string
    $i = 1;
    $count = count($regionFilter);
    foreach ($regionFilter as $regions) {
        if ($i == 1) {
            $where .= " AND (";
        } else {
            $where .= " OR ";
        }
        $where .= "FIND_IN_SET($regions, IB_Region)";
        if ($i == $count) {
            $where .= "$searchParentImage)";
        } else {
            $i++;
        }
    }
}
if (in_array('county', $_SESSION['USER_ROLES'])) {
    $sql = "SELECT PO_ID FROM tbl_Photographer_Owner LEFT JOIN tbl_User ON PO_ID = U_Owner where U_ID = '" . $_SESSION['USER_ID'] . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowCounty = mysql_fetch_assoc($result);
    $where .= " AND IB_Owner = " . $rowCounty['PO_ID'];
}
if (isset($_GET['season']) && $_GET['season'] != '') {
    $seasonFilter = $_GET['season'];
    $where .= " AND IB_Season IN ($seasonFilter)";
}

if (isset($_GET['img_filter']) && $_GET['img_filter'] != '') {
    $imageFilter = $_GET['img_filter'];
    if($imageFilter == 1){
       $imageFilter = "SUBSTRING_INDEX(`IB_Dimension`, 'X', 1) < 500 AND SUBSTRING_INDEX(`IB_Dimension`, 'X', -1) < 500";
    }else if($imageFilter == 2){
       $imageFilter = "(SUBSTRING_INDEX(`IB_Dimension`, 'X', 1) > 500 AND SUBSTRING_INDEX(`IB_Dimension`, 'X', -1) > 500) AND (SUBSTRING_INDEX(`IB_Dimension`, 'X', 1) < 1500 AND SUBSTRING_INDEX(`IB_Dimension`, 'X', -1) < 1500)";   
    }else if($imageFilter == 3){
       $imageFilter = "(SUBSTRING_INDEX(`IB_Dimension`, 'X', 1) > 1500 AND SUBSTRING_INDEX(`IB_Dimension`, 'X', -1) > 1500) AND (SUBSTRING_INDEX(`IB_Dimension`, 'X', 1) < 2500 AND SUBSTRING_INDEX(`IB_Dimension`, 'X', -1) < 2500)";    
    }else if($imageFilter == 4){
       $imageFilter = "SUBSTRING_INDEX(`IB_Dimension`, 'X', 1) > 2500 AND SUBSTRING_INDEX(`IB_Dimension`, 'X', -1) > 2500";  
    }
    $where .= " AND $imageFilter";
}

if (isset($_GET['cat']) && $_GET['cat'] != '') {
    $catFilter = $_GET['cat'];
    $where .= " AND IB_Category IN ($catFilter)";
}
if (isset($_GET['people']) && $_GET['people'] != '') {

    $peopleFilter = $_GET['people'];
    $where .= " AND IB_People IN ($peopleFilter)";
}
if (isset($_GET['owner']) && $_GET['owner'] != '') {

    $ownerFilter = $_GET['owner'];
    $where .= " AND IB_Owner IN ($ownerFilter)";
}
if (isset($_GET['campaign']) && $_GET['campaign'] != '') {

    $campaignFilter = $_GET['campaign'];
    $where .= " AND IB_Campaign IN ($campaignFilter)";
}
if (isset($_GET['search_image']) && $_GET['search_image'] != '') {
    $join = 'LEFT JOIN tbl_Business_Listing ON FIND_IN_SET(BL_ID, IB_Listings)';
    $search_text = explode(',', $_GET['search_image']);
    $search_word_len = count($search_text);
    foreach ($search_text as $key => $temp) {
        if ($key == 0) {
            $operation = 'AND';
        } else {
            $operation = 'OR';
        }
        if ($count > 0) {
            if ($key == 0) {
                $where .= " AND (";
                $OR = "";
            } else {
                $OR = " OR ";
            }
            $where .= " $OR (IB_Keyword LIKE '%" . mysql_real_escape_string($temp) . "%' OR IB_Name LIKE '%" . mysql_real_escape_string($temp) . "%' OR BL_Listing_Title LIKE '%" . mysql_real_escape_string($temp) . "%')";
            if ($key + 1 == $search_word_len) {
                $where .= ")";
            }
        } else {
            $where .= " $operation (IB_Keyword LIKE '%" . mysql_real_escape_string($temp) . "%' OR IB_Name LIKE '%" . mysql_real_escape_string($temp) . "%' OR BL_Listing_Title LIKE '%" . mysql_real_escape_string($temp) . "%') ";
        }
    }
}
$order = "DESC";
if (isset($_REQUEST['img_sort'])) {
    $order = $_REQUEST['img_sort'];
}
if (isset($_REQUEST['img_filter'])) {
    $limit = $_REQUEST['img_filter'];
}
if (in_array('county', $_SESSION['USER_ROLES'])) {
    $sql = "SELECT PO_ID FROM tbl_Photographer_Owner LEFT JOIN tbl_User ON PO_ID = U_Owner where U_ID = '" . $_SESSION['USER_ID'] . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowCounty = mysql_fetch_assoc($result);
    $where .= " AND IB_Owner = " . $rowCounty['PO_ID'];
}
$sql = "SELECT IB_ID, IB_Path, IB_Thumbnail_Path, IB_Dimension FROM tbl_Image_Bank
        $join $where GROUP BY IB_ID ORDER BY IB_ID $order";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$pages = new Paginate(mysql_num_rows($result), 100);
$IMResult = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
$count = mysql_num_rows($result);

if (isset($_GET['page'])) {
    $page = $_GET['page'];
    $records = 100 * ($page - 1);
}
require_once '../include/admin/header.php';
?>

<div class="content-left full-width">
    <div class="image-nav">
        <?php require_once('../include/top-nav-image.php'); ?>
    </div>
    <div class="image-bank-container">
        <div class="image-bank-container-image-gallery">
            <?php
            if ($count > 0) {
                // create page query string
                $query_string = $_SERVER['QUERY_STRING'];
                if ($query_string != '') {
                    $query_string = $query_string . '&';
                }

                $counter = 1;
                $i = 1;
                while ($row = mysql_fetch_array($IMResult)) {
                    //check if image is used anywhere
                    $sql_count_usage = "SELECT COUNT(*) as count FROM tbl_Image_Bank_Usage WHERE IBU_IB_ID = " . $row['IB_ID'];
                    $res_count_usage = mysql_query($sql_count_usage);
                    $count_usage = mysql_fetch_assoc($res_count_usage);
                    $usage_class = '';
                    if ($count_usage['count'] > 0 && in_array('superadmin', $_SESSION['USER_ROLES'])) {
                        $usage_class = ' usage';
                    }
                    if ($i == 1) {
                        ?>
                        <div class="image-section-container <?php echo ($count - $counter < 4) ? 'image-bank-image-section-outer-last' : ''; ?>">
                        <?php } ?>
                        <div id="bgcolor_<?php echo $row['IB_ID'] ?>" onclick="select_the_image('<?php echo $row['IB_Path'] ?>','<?php echo $row['IB_ID'] ?>')" class="image-hover-bank <?php
                        echo ($i == 4) ? 'image-bank-image-section-last' : '';
                        echo $usage_class;
                        ?>">
                            <a class="image-bank-image-section">
                                <div  class="image-section-image-align">
                                    <img src="http://<?php echo DOMAIN . IMG_BANK_REL . $row['IB_Thumbnail_Path'] ?>">
                                </div>
                                <div class="image-section-text-align">
                                    <?php echo $row['IB_ID'] . " - " . $row['IB_Dimension']; ?>
                                </div>
                            </a>
                            <input type="hidden" name="deleteVal" value="<?php echo $row['IB_ID'] ?>">
                            <a  class="image-section-delete" onclick="return confirm('Are you sure you want to delete this image?')"   href="image-bank-photo-delete.php?<?php echo $_SERVER['QUERY_STRING'] ?>&ib_id=<?php echo $row['IB_ID'] ?>&">X</a>
                            <a class="image-section-edit" href="add-image-bank.php?ib_id=<?php echo $row['IB_ID'] ?>">Edit</a>
                            <a class="image-section-detail" href="image-bank-photo-detail.php?<?php echo preg_replace('/(&)(i)(d)(s)(=)((\d)*)|((\d)*(,)(\d)*)/', '', $query_string); ?>id=<?php echo $row['IB_ID'] ?>">Detail</a>
                        </div>
                        <?php
                        if ($i == 4 || $count - ($records + $counter) == 0) { 
                            ?>
                        </div>
                        <?php
                    }
                    $i++;
                    $counter++;
                    if ($i == 5) {
                        $i = 1;
                    }
                }
            } else {
                echo '<div class="no-image-found">No images found in image bank.</div>';
            }
            ?>

        </div>
        <div class="image-bank-pagination">
            <?php
            if (isset($pages)) {
                echo $pages->paginate();
            }
            ?>
        </div>
    </div>

</div>
<div id="autocomplete_data" style="display: none;"><?php include '../include/autocomplete.php' ?></div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script>
    $(function () {
        var search_text = $('#keywords-searched').val();
        var searched_items = "";
        if (search_text != "") {
            searched_items = search_text.split(",");
            var json = [];
            for (var i = 0; i < searched_items.length; i++) {
                json.push({"name": searched_items[i]});
            }
        }
        //autocomplete for lisitngs
        var search = $('#autocomplete_data').text();
        var searched_list = "";
        var json_data = [];
        if (search != "") {
            searched_list = search.split("%");
            for (var i = 0; i < searched_list.length - 1; i++) {
                json_data.push({"name": searched_list[i]});
            }
        }
        $("#autocomplete").tokenInput(json_data, {
            onResult: function (item) {
                if ($.isEmptyObject(item)) {
                    // return [{id:'0',name: $("tester").text()}]
                } else {
                    item.unshift({"name": $("tester").text()});
                    var lookup = {};
                    var result = [];

                    for (var temp, i = 0; temp = item[i++]; ) {
                        var name = temp.name;

                        if (!(name in lookup)) {
                            lookup[name] = 1;
                            result.push({"name": name});
                        }
                    }
                    return result;
                }

            },
            onAdd: function (item) {
                var value = $('#keywords-searched').val();
                if (value != '') {
                    $('#keywords-searched').val(value + "," + item.name);
                    $('#auto_complete_form_submit').submit();
                } else {
                    $('#keywords-searched').val(item.name);
                    $('#auto_complete_form_submit').submit();
                }
            },
            onDelete: function (item) {
                var value = $('#keywords-searched').val().split(",");
                $('#keywords-searched').empty();
                var data = "";
                $.each(value, function (key, value) {
                    if (value != item.name) {
                        if (data != '') {
                            data = data + "," + value;
                        } else {
                            data = value;
                        }
                    }
                });
                $('#keywords-searched').val(data);
            },
            resultsLimit: 10,
            prePopulate: json
        }
        )
    });
    $("a.page").each(function() {
        var $this = $(this);                           
        $this.attr('onclick', 'retain_ids();');
     });
</script>