<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/adminFunctions.inc.php';

if (!in_array('manage-cart-items', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if ($_POST['op'] == 'save') {
    //update listing for all records
    $new_listing = 0;
    $sql = "SELECT BL_ID, BL_Billing_Type, BL_CoC_Dis_2, BL_Line_Item_Title, BL_Line_Item_Price FROM tbl_Business_Listing 
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID LEFT JOIN tbl_Category ON C_ID = BL_C_ID WHERE BL_Listing_Type != '1' GROUP BY BL_ID";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($rowListing = mysql_fetch_array($result)) {
        listing_billing($rowListing['BL_Billing_Type'], $rowListing['BL_ID'], $rowListing['BL_CoC_Dis_2'], $rowListing['BL_Line_Item_Title'], $rowListing['BL_Line_Item_Price']);
    }
    if (isset($_SESSION['success']) || isset($_SESSION['error'])) {
        unset($_SESSION['success']);
        unset($_SESSION['error']);
    }
    $item_price = $_POST['item_price'];
    $sql = sprintf("UPDATE tbl_Listing_SubCategory SET LS_Cost='%s'", $item_price, $item_id);
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
}
require_once '../include/admin/header.php';
?>

<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Store - Listing Sub-Category</div>
        <div class="link">
        </div>
    </div> 
    <div class="left"><?PHP require '../include/nav-cart.php'; ?></div>
    <div class="right">
        <div class="content-sub-header">
            <div class="data-column padding-none spl-name">Name</div>
            <div class="data-column padding-none spl-other">Price</div>
            <div class="data-column padding-none spl-other">Save</div>
        </div>
        <ul class="brief">
            <?PHP
            $sql_types = "SELECT LS_ID, LS_Cost FROM tbl_Listing_SubCategory";
            $result_types = mysql_query($sql_types, $db) or die("Invalid query: $sql_types -- " . mysql_error());
            while ($type = mysql_fetch_assoc($result_types)) {
                ?>
                <li class="option-width" id="recordsArray_<?php echo $type['LS_ID'] ?>">
                    <form action="" method="POST">
                        <div class="data-content">
                            <div class="data-column spl-name">Additional Sub-Category</div>
                            <div class="data-column spl-other">$<input type="text" name="item_price" size="10" value="<?php echo $type['LS_Cost'] ?>" /></div>
                            <div class="data-column spl-other padding-none">
                                <input type="hidden" name="item_id" value="<?php echo $type['LS_ID'] ?>" />
                                <input type="hidden" name="op" value="save" />
                                <input type="submit" name="save_item" value="Save" />
                            </div>
                        </div>
                    </form>
                </li>
                <?PHP
            }
            ?>
        </ul>
    </div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>