<?PHP

require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/class.Pagination.php';

if (!in_array('image-bank', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

$where = 'WHERE 1=1';
if (isset($_REQUEST['img_sort'])) {
     $order = $_REQUEST['img_sort'];
} else {
     $order = 'DESC';
 }
if (in_array('county', $_SESSION['USER_ROLES'])) {
    $sql = "SELECT PO_ID FROM tbl_Photographer_Owner LEFT JOIN tbl_User ON PO_ID = U_Owner where U_ID = '" . $_SESSION['USER_ID'] . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowCounty = mysql_fetch_assoc($result);
    $where .= " AND IB_Owner = " . $rowCounty['PO_ID'];
}
$search_text = "";
if ($_GET['search_image']) {
    $first = true;
    $search_text = explode(',', $_GET['search_image']);
    $for_autocomplete = "";
    foreach ($search_text as $temp) {
        $for_autocomplete = $temp;
        if ($first == true) {
            $first = false;
            $where .= " AND (";
        } else {
            $where .= " OR ";
        }
        $where .= "IB_Keyword LIKE '%" . mysql_real_escape_string($temp) . "%' OR IB_Name LIKE '%" . mysql_real_escape_string($temp) . "%' OR (BL_Listing_Title LIKE '%" . mysql_real_escape_string($temp) . "%' AND hide_show_listing = 1)";
    }
    $where .= ")";
}
$sql = "SELECT IB_ID, IB_Keyword, IB_Path, IB_Region,IB_Thumbnail_Path, IB_Category, IB_Season, IB_People, IB_Photographer, IB_Location, IB_Owner, IB_Dimension
        FROM tbl_Image_Bank LEFT JOIN tbl_Business_Listing ON BL_ID = IB_Listings AND FIND_IN_SET(BL_ID, IB_Listings) AND hide_show_listing = 1
        $where AND (IB_Name = ''
        OR IB_Keyword = ''
        OR IB_Path = ''
        OR IB_Region = ''
        OR IB_Category = '0'
        OR IB_Season = '0'
        OR IB_People = '0'
        OR IB_Photographer = ''
        OR IB_Location = ''
        OR IB_Owner = '0') GROUP BY IB_ID ORDER BY IB_ID $order ";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$pages = new Paginate(mysql_num_rows($result), 100);
$IMResult = mysql_query($sql . $pages->generateSql(), $db) or die("Invalid query: $sql -- " . mysql_error());
$count = mysql_num_rows($result);

if (isset($_GET['page'])) {
    $page = $_GET['page'];
    $records = 100 * ($page - 1);
}
$content .= '<div class="image-bank-container-image-gallery">';

if ($count > 0) {
    $counter = 1;
    $i = 1;
    while ($row = mysql_fetch_array($IMResult)) {
        $last = ($count - $counter < 4) ? ' image-bank-image-section-outer-last' : '';
        if ($i == 1) {
            $content .= '<div class="image-section-container ' . $last . '">';
        }
        $last_class = ($i == 4) ? ' image-bank-image-section-last' : '';
        $content .= '<div class="image-hover-bank ' . $last_class . '">
                     <a href="add-image-bank.php?ib_id=' . $row['IB_ID'] . '&pendVal=pendingVal" class="image-bank-image-section">
                     <div class="image-section-image-align">
                     <img src="http://' . DOMAIN . IMG_BANK_REL . $row["IB_Thumbnail_Path"] . '">
                     </div>
                     <div class="image-section-text-align">' . $row['IB_ID'] . ' - ' . $row['IB_Dimension'] . '</div>
                     </a>
                     <a class="image-section-delete" onclick="return confirm(`Are you sure you want to delete this image?`)" href="image-bank-photo-delete.php?' . $_SERVER['QUERY_STRING'] . '&ib_id=' . $row['IB_ID'] . '&pending=1 ">X</a>
                     <a class="image-section-edit" href="add-image-bank.php?ib_id=' .$row['IB_ID'] .'&pendVal=pendingVal">Edit</a>
                     </div>';

        if ($i == 4 || $count - ($records + $counter) == 0) {
            $content .= '</div>';
        }
        $i++;
        $counter++;
        if ($i == 5) {
            $i = 1;
        }
    }
} else {
    $content .= '<div class="no-image-found">No images found in image bank.</div>';
}
$content .= '</div>
        <div class="image-bank-pagination">';

if (isset($pages)) {
    $content .= $pages->paginate();
}
$content .= '</div>';
print $content . '@' . ($count ? $count : "");