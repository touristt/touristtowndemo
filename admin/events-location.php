<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';
if (!in_array('manage-events', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
    }
if (isset($_REQUEST['id'] ) && $_REQUEST['id'] > 0) {
    $sql = "SELECT EL_ID, EL_Name, EL_Region_ID, EL_Street, EL_Town, EL_Province, EL_PostalCode, EL_Use_GPS, EL_Lat, EL_Long  FROM Events_Location 
            WHERE EL_ID = '" . encode_strings($_REQUEST['id'], $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $row = mysql_fetch_assoc($result);
}

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    if(isset($_REQUEST['useGPS'])){
        $useGPS = $_REQUEST['useGPS'];
    }else{
        $useGPS = 0;
    }
    $community = implode(',', $_REQUEST['rID']);
    $sql = "Events_Location SET 
            EL_Region_ID = '" . encode_strings($community, $db) . "', 
            EL_Name = '" . encode_strings($_REQUEST['name'], $db) . "', 
            EL_Street = '" . encode_strings($_REQUEST['address'], $db) . "', 
            EL_Town = '" . encode_strings($_REQUEST['town'], $db) . "', 
            EL_Province = '" . encode_strings($_REQUEST['province'], $db) . "', 
            EL_PostalCode = '" . encode_strings($_REQUEST['postalcode'], $db) . "', 
            EL_Use_GPS = '" . encode_strings($useGPS, $db) . "', 
            EL_Lat = '" . encode_strings($_REQUEST['latitude'], $db) . "', 
            EL_Long = '" . encode_strings($_REQUEST['longitude'], $db) . "'";

    if (isset($_REQUEST['id']) && $_REQUEST['id'] > 0) {
        $sql = "UPDATE " . $sql . " WHERE EL_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
        $result = mysql_query($sql, $db) or die(mysql_error());
        // TRACK DATA ENTRY
        $id = $_REQUEST['id'];
        Track_Data_Entry('Event','','Manage Locations',$id,'Update','super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db) or die(mysql_error());
        $id = mysql_insert_id();
        // TRACK DATA ENTRY
        Track_Data_Entry('Event','','Manage Locations',$id,'Add','super admin');
    }
    
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }


    header("Location: events-locations.php");
    exit();
} elseif (isset($_GET['op']) && $_GET['op'] == 'del') {

    $sql = "DELETE Events_Location
            FROM Events_Location 
            WHERE EL_ID = '" . encode_strings($_REQUEST['id'], $db) . "'";
    $result = mysql_query($sql, $db);
    // TRACK DATA ENTRY
    $id = $_REQUEST['id'];
    Track_Data_Entry('Event','','Manage Locations',$id,'Delete','super admin');
    if ($result) {
        $_SESSION['delete'] = 1;
    } else {
        $_SESSION['delete_error'] = 1;
    }
    $_SESSION['delete'] = 1;

    header("Location: events-locations.php");
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Manage Events - Manage Locations</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manageevents.php'; ?>
    </div>
    <div class="right">
        <form name="form1" method="post" action="/admin/events-location.php">

            <input type="hidden" name="id" value="<?php echo $row['EL_ID'] ?>">
            <input type="hidden" name="op" value="save">
            <div class="content-header">Location Information</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Location Name</label>
                <div class="form-data">
                    <input name="name" type="text" id="Bname" value="<?php echo $row['EL_Name'] ?>" size="50" required/>
                </div>
            </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Community</label>
                    <div class="form-data">
                    <div id="childRegion"> 
                            <?PHP
                        $sqlCom = "SELECT R_ID, R_Name FROM tbl_Region WHERE R_Parent != 0 ORDER BY R_Name";
                        $resultRegionList = mysql_query($sqlCom, $db) or die("Invalid query: $sqlCom -- " . mysql_error());
                        $region_list = explode(',', $row['EL_Region_ID']);
                            while ($rowRList = mysql_fetch_assoc($resultRegionList)) {
                            echo '<div class="childRegionCheckbox"><input type="checkbox" name="rID[]" value="' . $rowRList['R_ID'] . '"';
                            echo (in_array($rowRList['R_ID'], $region_list)) ? 'checked' : '';
                            echo '>' . $rowRList['R_Name'] . "</div>";
                            }
                        ?>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Street Address</label>
                <div class="form-data">
                    <input name="address" type="text" id="address" value="<?php echo $row['EL_Street'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Town</label>
                <div class="form-data">
                    <input name="town" type="text" id="town" value="<?php echo $row['EL_Town'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Province</label>
                <div class="form-data">
                    <input name="province" type="text" id="province" value="<?php echo $row['EL_Province'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Postal Code</label>
                <div class="form-data">
                    <input name="postalcode" type="text" id="postalcode" value="<?php echo $row['EL_PostalCode'] ?>" size="50" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin border-none"> 
                <label>Use GPS for Maps</label>
                <div class="form-data">
                    <input class="checkbox-margin-top" name="useGPS" type="checkbox" value="1" <?php echo $row['EL_Use_GPS'] ? 'checked' : '' ?>>
                </div>
            </div>
            <div class="content-sub-header"><div class="data-column padding-none spl-name">GPS
                </div>
                </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Latitude</label>
                <div class="form-data">
                    <input name="latitude" type="text" id="latitude" value="<?php echo $row['EL_Lat'] ?>" size="10" />
                    </div>
            </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Longitude</label>
                <div class="form-data">
                    
                    <input name="longitude" type="text" id="longitude" value="<?php echo $row['EL_Long'] ?>" size="10" />
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <div class="button">
                    <td align="left" class="table-boldtext"><input type="submit" name="button2" id="button2" value="Submit" /> </td>
                </div>
            </div>
        </form></div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>