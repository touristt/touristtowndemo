<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('regions', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}

if (isset($_REQUEST['rid']) && $_REQUEST['rid'] > 0) {
    $regionID = $_REQUEST['rid'];
    $sql = "SELECT * FROM tbl_Region WHERE R_ID = '" . encode_strings($regionID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $activeRegion = mysql_fetch_assoc($result);
    $sql = "SELECT RM_Child FROM tbl_Region_Multiple WHERE RM_Parent = '" . encode_strings($regionID, $db) . "'";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    while ($row = mysql_fetch_assoc($result)) {
        $childRegion[] = $row['RM_Child'];
    }
} else {
    $regionID = 0;
}

$regionType = (isset($_REQUEST['type']) && $_REQUEST['type'] != '') ? $_REQUEST['type'] : $activeRegion['R_Type'];

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $hide_show = isset($_REQUEST['hide_show_phone']) ? 1 : 0;
    $hide_show_homepage_image = isset($_REQUEST['hide_show_homepage_image']) ? 1 : 0;
    $hide_show_email = isset($_REQUEST['hide_show_email']) ? 1 : 0;
    $hide_show_nearby = isset($_REQUEST['hide_show_nearby']) ? 1 : 0;
    $hide_show_subcategory_title = isset($_REQUEST['hide_show_subcategory_title']) ? 1 : 0;
    $include_free_listings = isset($_REQUEST['include_free_listings']) ? 1 : 0;
    $include_free_listings_on_map = isset($_REQUEST['include_free_listings_on_map']) ? 1 : 0;
    $order_listings_manually = isset($_REQUEST['order_listings_manually']) ? 1 : 0;
    $r_dropdown_on_off = isset($_REQUEST['R_Dropdown_On_Off']) ? 1 : 0;
    $r_website_title = isset($_REQUEST['R_Website_Title']) ? 1 : 0;
    $r_Whats_NearBy = isset($_REQUEST['R_Whats_NearBy']) ? 1 : 0;
    $r_Stories = isset($_REQUEST['R_Stories']) ? 1 : 0;
    $day_trip = isset($_REQUEST['day_trip']) ? 1 : 0;
    $seasons = isset($_REQUEST['seasons']) ? 1 : 0;
    $hide_show_story_author = isset($_REQUEST['hide_show_story_author']) ? 1 : 0;
    $hide_show_story_search = isset($_REQUEST['hide_show_story_search']) ? 1 : 0;
    $hide_show_event = isset($_REQUEST['hide_show_event']) ? 1 : 0;
    $hide_show_town = isset($_REQUEST['hide_show_town']) ? 1 : 0;
    $hide_show_footer_menu = isset($_REQUEST['hide_show_footer_menu']) ? 1 : 0;
    $thirdpartyevents = isset($_REQUEST['thirdpartyevents']) ? 1 : 0;
    if (isset($_REQUEST['mytrip'])) {
        $mytrip = 1;
    } else {
        $mytrip = 0;
    }
    if (isset($_REQUEST['myroute'])) {
        $myroute = 1;
    } else {
        $myroute = 0;
    }

    $sql = "tbl_Region SET 
            R_Name = '" . encode_strings($_REQUEST['name'], $db) . "',
            R_Parent = '" . encode_strings($_REQUEST['prid'], $db) . "',
            R_SEO_Title = '" . encode_strings($_REQUEST['seoTitle'], $db) . "', 
            R_SEO_Keywords = '" . encode_strings($_REQUEST['seoKeywords'], $db) . "', 
            R_SEO_Description = '" . encode_strings($_REQUEST['seoDescription'], $db) . "', 
            R_Topnav_Title = '" . encode_strings($_REQUEST['nav_title'], $db) . "',
            R_Trip_Planner_Title  = '" . encode_strings($_REQUEST['trip_title'], $db) . "',
            R_Route_Title  = '" . encode_strings($_REQUEST['route_title'], $db) . "',
            R_Homepage_Title  = '" . encode_strings($_REQUEST['homepage_title'], $db) . "',
            R_Homepage_Stories_Title  = '" . encode_strings($_REQUEST['homepage_stories_title'], $db) . "',
            R_Homepage_Text_Above_Events  = '" . encode_strings($_REQUEST['homepage_text_above_events'], $db) . "',
            R_Homepage_Events_Title  = '" . encode_strings($_REQUEST['homepage_events_title'], $db) . "',
            R_Homepage_Event_Register_Text  = '" . encode_strings($_REQUEST['homepage_event_register_text'], $db) . "',
            R_Homepage_Event_See_All_Text  = '" . encode_strings($_REQUEST['homepage_event_see_all_text'], $db) . "',
            R_Listings_Title  = '" . encode_strings($_REQUEST['listings_title'], $db) . "',
            R_Description = '" . encode_strings(trim($_REQUEST['description']), $db) . "',
            R_Mobile_Description = '" . encode_strings(trim($_REQUEST['mobile_description']), $db) . "',
            R_Clientname = '" . encode_strings($_REQUEST['R_cname'], $db) . "',
            R_Street = '" . encode_strings($_REQUEST['R_cstreet'], $db) . "',
            R_Town = '" . encode_strings($_REQUEST['R_ctown'], $db) . "',
            R_Province = '" . encode_strings($_REQUEST['R_cprovince'], $db) . "',
            R_PostalCode = '" . encode_strings($_REQUEST['R_cpostalcode'], $db) . "',
            R_API_Login_ID = '" . encode_strings($_REQUEST['api_login_id'], $db) . "',
            R_Transaction_Key = '" . encode_strings($_REQUEST['transaction_key'], $db) . "',
            R_My_Trip = '" . $mytrip . "',
            R_My_Route = '" . $myroute . "',
            R_Domain = '" . encode_strings($_REQUEST['domain'], $db) . "',
            R_Radius = '" . encode_strings($_REQUEST['radius'], $db) . "',
            R_Domain_Alternates = '" . encode_strings($_REQUEST['domain_alternates'], $db) . "',
            R_Event_Email = '" . encode_strings($_REQUEST['event_email'], $db) . "',
            R_Show_Hide_Phone  = '" . encode_strings($hide_show, $db) . "',
            R_Show_Hide_Homepage_Image  = '" . encode_strings($hide_show_homepage_image, $db) . "',
            R_Show_Hide_Email  = '" . encode_strings($hide_show_email, $db) . "',
            R_Show_Hide_Story_Author  = '" . encode_strings($hide_show_story_author, $db) . "',
            R_Show_Hide_Story_Search  = '" . encode_strings($hide_show_story_search, $db) . "',
            R_Status  = '" . encode_strings((($activeRegion['R_Status'] > 0) ? $activeRegion['R_Status'] : 0), $db) . "',
            R_CO_Community  = '" . encode_strings((($activeRegion['R_CO_Community'] > 0) ? $activeRegion['R_CO_Community'] : 0), $db) . "',
            R_Show_Same_Sub_Cat  = '" . encode_strings($hide_show_nearby, $db) . "',
            R_Show_Hide_Sub_Category_Title  = '" . encode_strings($hide_show_subcategory_title, $db) . "',
            R_Show_Hide_Event  = '" . encode_strings($hide_show_event, $db) . "',
            R_Show_Hide_Town  = '" . encode_strings($hide_show_town, $db) . "',
            R_Show_Hide_Footer_Menu  = '" . encode_strings($hide_show_footer_menu, $db) . "',
            R_Include_Free_Listings  = '" . encode_strings($include_free_listings, $db) . "',
            R_Include_Free_Listings_On_Map  = '" . encode_strings($include_free_listings_on_map, $db) . "',
            R_Dropdown_On_Off      = '" . encode_strings($r_dropdown_on_off, $db) . "',
            R_Whats_NearBy      = '" . encode_strings($r_Whats_NearBy, $db) . "',
            R_Website_Title     = '" . encode_strings($r_website_title, $db) . "',
            R_Stories      = '" . encode_strings($r_Stories, $db) . "',
            R_Day_Trip      = '" . encode_strings($day_trip, $db) . "',
            R_Seasons      = '" . encode_strings($seasons, $db) . "',
            R_Sidebar_Stories  = '" . encode_strings((($_REQUEST['sidebar_stories'] != '') ? $_REQUEST['sidebar_stories'] : 0), $db) . "',
            R_Homepage_Listings  = '" . encode_strings((($_REQUEST['homepage_listings'] != '') ? $_REQUEST['homepage_listings'] : 0), $db) . "',
            R_Tracking_ID = '" . encode_strings($_REQUEST['tracking_id'], $db) . "',
            R_Type = '" . encode_strings($regionType, $db) . "',
            R_Article_Map_Links = '" . encode_strings($_REQUEST['article_map_link'], $db) . "',
            R_Homepage_Carousals  = '" . encode_strings($_REQUEST['homepage_carousals'], $db) . "',
            R_Story_Share_Text  = '" . encode_strings($_REQUEST['story_share_text'], $db) . "',
            R_Story_See_All_Text  = '" . encode_strings($_REQUEST['story_see_all_text'], $db) . "',
            R_Order_Listings_Manually  = '" . encode_strings($order_listings_manually, $db) . "'";
    if ($regionID > 0) {
        $sql = "UPDATE " . $sql . " WHERE R_ID = '" . encode_strings($regionID, $db) . "'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        // TRACK DATA ENTRY
        $id = $regionID;
        Track_Data_Entry('Websites', $id, 'Manage Website', '', 'Update', 'super admin');
    } else {
        $sql = "INSERT " . $sql;
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        $regionID = mysql_insert_id();
        if ($regionType == 1) {
            $action = 'Add Parent Website';
        } elseif ($regionType == 2) {
            $action = 'Add Community';
        } elseif ($regionType == 3) {
            $action = 'Add Community Organization';
        } elseif ($regionType == 4) {
            $action = 'Add County Website';
        } elseif ($regionType == 6) {
            $action = 'Add Product Website';
        } elseif ($regionType == 7) {
            $action = 'Add Specialty Website';
        }
        // TRACK DATA ENTRY
        $id = $regionID;
        Track_Data_Entry('Websites', $id, 'Manage Website', '', $action, 'super admin');
    }
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    //Insert parent in tbl_Region_Multiple 
    if ($_REQUEST['prid'] == 0) {
        $sql = "DELETE FROM tbl_Region_Multiple WHERE RM_Parent = '$regionID'";
        $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
        if (isset($_REQUEST['childRegion'])) {
            foreach ($_REQUEST['childRegion'] as $child) {
                $sql = "tbl_Region_Multiple SET RM_Parent = '$regionID', RM_Child = '$child'";
                $sql = "INSERT " . $sql;
                $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            }
        }
    }
    header("Location: region.php?rid=" . $regionID);
    exit();
} elseif (isset($_GET['op']) && $_GET['op'] == 'del') {
    header("Location: region.php?rid=" . $regionID);
    exit();
}

require_once '../include/admin/header.php';
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title"><?php echo isset($activeRegion['R_Name']) ? $activeRegion['R_Name'] : '' ?> - Manage Website</div>
        <div class="link">
        </div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-region.php'; ?>
    </div>
    <div class="right">
        <form action="/admin/region.php" method="post" enctype="multipart/form-data" name="form1" id="form1">
            <input type="hidden" name="op" value="save">
            <input type="hidden" name="rid" value="<?php echo $regionID ?>">
            <input type="hidden" name="type" value="<?php echo $regionType ?>">
            <div class="content-header">Website Profile</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Website Name</label>
                <div class="form-data">
                    <input name="name" type="text" id="textfield2" value="<?php echo (isset($activeRegion['R_Name']) ? $activeRegion['R_Name'] : '') ?>" size="50" required/> 
                </div>
            </div>
            <?php if ($regionType != 1 && $regionType != 4) { ?>
                <input type="hidden" name="prid" value="1">
            <?php } else { ?>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Select Community</label>
                    <div class="form-data">
                        <input type="hidden" name="prid" value="0">
                        <div id="childRegion"> 
                            <?PHP
                            $sql = "SELECT * FROM tbl_Region WHERE R_Type = 2 ORDER BY R_Name";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($row = mysql_fetch_assoc($result)) {
                                echo '<div class="childRegionCheckbox"><input type="checkbox" name="childRegion[]" value="' . $row['R_ID'] . '"';
                                echo (in_array($row['R_ID'], $childRegion)) ? 'checked' : '';
                                echo '>' . $row['R_Name'] . "</div>";
                            }
                            ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Feature On/Off</label>
                <div class="form-data">
                    <div id="childfeature">
                        <div class="childfeatureCheckbox">
                            <input onclick="showHideVal('mytrip', 'r_title')" id="mytrip" name="mytrip" type="checkbox" value="<?php echo $activeRegion['R_My_Trip']; ?>" <?php echo ($activeRegion['R_My_Trip'] == 1) ? "checked" : '' ?> size="50"/>My Trip
                        </div>
                        <div class="childfeatureCheckbox">
                            <input onclick="showHideVal('chk_route', 'r_title', 'nav_manage_routes')" id="chk_route" name="myroute" type="checkbox" value="<?php echo $activeRegion['R_My_Route']; ?>" <?php echo ($activeRegion['R_My_Route'] == 1) ? "checked" : '' ?> size="50"/>Routes
                        </div>
                        <div class="childfeatureCheckbox">
                            <input onclick="showHideVal('include_free_listings', 'hide_email', 'hide_phone')" id="include_free_listings" type="checkbox" name="include_free_listings" value="<?php echo $activeRegion['R_Include_Free_Listings']; ?>" <?php echo ($activeRegion['R_Include_Free_Listings'] == 1) ? "checked" : '' ?>/>Include Free Listings
                        </div>
                        <div class="childfeatureCheckbox">
                            <input id="include_free_listings_on_map" type="checkbox" name="include_free_listings_on_map" value="<?php echo $activeRegion['R_Include_Free_Listings_On_Map']; ?>" <?php echo ($activeRegion['R_Include_Free_Listings_On_Map'] == 1) ? "checked" : '' ?>/>Include Free Listings On Map
                        </div>
                        <div class="childfeatureCheckbox">
                            <input type="checkbox" name="order_listings_manually" value="<?php echo $activeRegion['R_Order_Listings_Manually']; ?>" <?php echo ($activeRegion['R_Order_Listings_Manually'] == 1) ? "checked" : '' ?>/>Order Listings Manually
                        </div>

                        <div class="childfeatureCheckbox">
                            <input type="checkbox" id="hide_show_homepage_image" name="hide_show_homepage_image" value="<?php echo $activeRegion['R_Show_Hide_Homepage_Image']; ?>"<?php echo ($activeRegion['R_Show_Hide_Homepage_Image'] == 1) ? "checked" : '' ?>/>Show homepage image?
                        </div>

                        <div class="childfeatureCheckbox" id="hide_phone">
                            <input type="checkbox" id="phoneNumerFreeListing" name="hide_show_phone" value="<?php echo $activeRegion['R_Show_Hide_Phone']; ?>"<?php echo ($activeRegion['R_Show_Hide_Phone'] == 1) ? "checked" : '' ?>/>Show phone number for free listings?
                        </div>
                        <div class="childfeatureCheckbox" id="hide_email">
                            <input type="checkbox" name="hide_show_email" id="hide_show_email" value="<?php echo $activeRegion['R_Show_Hide_Email']; ?>" <?php echo ($activeRegion['R_Show_Hide_Email'] == 1) ? "checked" : '' ?>/>Show Email for free listings?
                        </div>
                        <div class="childfeatureCheckbox">
                            <input type="checkbox" name="hide_show_nearby" id="hide_show_nearby" value="<?php echo $activeRegion['R_Show_Same_Sub_Cat']; ?>" <?php echo ($activeRegion['R_Show_Same_Sub_Cat'] == 1) ? "checked" : '' ?>/>Show Nearby in same sub-category?
                        </div>

                        <div class="childfeatureCheckbox">
                            <input type="checkbox" name="hide_show_subcategory_title" id="hide_show_subcategory_title" value="<?php echo $activeRegion['R_Show_Hide_Sub_Category_Title']; ?>" <?php echo ($activeRegion['R_Show_Hide_Sub_Category_Title'] == 1) ? "checked" : '' ?>/>Show sub-category title on sub-category page?
                        </div>

                        <?php if ($regionType == 1 || $regionType == 4) { ?>
                            <div class="childfeatureCheckbox">
                                <input type="checkbox" name="R_Dropdown_On_Off" id="R_Dropdown_On_Off" value="<?php echo $activeRegion['R_Dropdown_On_Off']; ?>" <?php echo ($activeRegion['R_Dropdown_On_Off'] == 1) ? "checked" : '' ?>/>Select Community (Sub-Category)
                            </div>
                        <?php } ?>
                        <div class="childfeatureCheckbox">
                            <input type="checkbox" name="R_Website_Title" id="R_Website_Title" value="<?php echo $activeRegion['R_Website_Title']; ?>" <?php echo ($activeRegion['R_Website_Title'] == 1) ? "checked" : '' ?>/>Website Title
                        </div>
                        <div class="childfeatureCheckbox">
                            <input onclick="showHideVal('R_Whats_NearBy', 'defaultRadius')" type="checkbox" name="R_Whats_NearBy" id="R_Whats_NearBy" value="<?php echo $activeRegion['R_Whats_NearBy']; ?>" <?php echo ($activeRegion['R_Whats_NearBy'] == 1) ? "checked" : '' ?>/>What's nearby
                        </div>
                        <div class="childfeatureCheckbox">
                            <input onclick="showHideVal('R_Stories', 'sidebar_stories', 'nav_homepage_stories', 'homepage_stories_title', 'nav_map_stories', 'story_share_text', 'story_see_all_text', 'nav_manage_stories')" type="checkbox" name="R_Stories" id="R_Stories" value="<?php echo $activeRegion['R_Stories']; ?>" <?php echo ($activeRegion['R_Stories'] == 1) ? "checked" : '' ?>/>Stories
                        </div>

                        <div class="childfeatureCheckbox">
                            <input onclick="showHideVal('day_trip', 'day_trip_menu')" type="checkbox" name="day_trip" id="day_trip" value="<?php echo $activeRegion['R_Day_Trip']; ?>" <?php echo ($activeRegion['R_Day_Trip'] == 1) ? "checked" : '' ?>/>Day Trip
                        </div>
                        <div class="childfeatureCheckbox">
                            <input  type="checkbox" name="seasons" id="seasons" value="<?php echo $activeRegion['R_Seasons']; ?>" <?php echo ($activeRegion['R_Seasons'] == 1) ? "checked" : '' ?>/>Seasons
                        </div>
                        <div class="childfeatureCheckbox">
                            <input  type="checkbox" name="hide_show_story_author" id="" value="<?php echo $activeRegion['R_Show_Hide_Story_Author']; ?>" <?php echo ($activeRegion['R_Show_Hide_Story_Author'] == 1) ? "checked" : '' ?>/>Show author of stories?
                        </div>
                        <div class="childfeatureCheckbox">
                            <input  type="checkbox" name="hide_show_story_search" id="" value="<?php echo $activeRegion['R_Show_Hide_Story_Search']; ?>" <?php echo ($activeRegion['R_Show_Hide_Story_Search'] == 1) ? "checked" : '' ?>/>Show Stories Search?
                        </div>
                        <?php if ($activeRegion['R_Type'] != '6') { ?>
                            <div class="childfeatureCheckbox">
                                <input  type="checkbox" name="hide_show_event" id="" value="<?php echo $activeRegion['R_Show_Hide_Event']; ?>" <?php echo ($activeRegion['R_Show_Hide_Event'] == 1) ? "checked" : '' ?>/>Show Events
                            </div>
                        <?php }if ($activeRegion['R_Type'] == '7') { ?>
                            <div class="childfeatureCheckbox">
                                <input  type="checkbox" name="hide_show_town" id="" value="<?php echo $activeRegion['R_Show_Hide_Town']; ?>" <?php echo ($activeRegion['R_Show_Hide_Town'] == 1) ? "checked" : '' ?>/>Show Communities
                            </div>
                        <?php } ?>
                        <div class="childfeatureCheckbox">
                            <input  type="checkbox" name="hide_show_footer_menu" id="" value="<?php echo $activeRegion['R_Show_Hide_Footer_Menu']; ?>" <?php echo ($activeRegion['R_Show_Hide_Footer_Menu'] == 1) ? "checked" : '' ?>/>Footer Menu
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Articles/Map/Routes Links </label>
                <div class="form-data">
                    <div class="childfeatureCheckbox">
                        <input type="radio" name="article_map_link" value="1" <?php echo ($activeRegion['R_Article_Map_Links'] == 1) ? "checked" : '' ?>/>Show in Navigation
                    </div>
                    <div class="childfeatureCheckbox">
                        <input type="radio" name="article_map_link" value="0" <?php echo ($activeRegion['R_Article_Map_Links'] == 0) ? "checked" : '' ?>/>Show in Top Right
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Homepage Carousals </label>
                <div class="form-data">
                    <div class="childfeatureCheckbox">
                        <input type="radio" name="homepage_carousals" value="1" <?php echo ($activeRegion['R_Homepage_Carousals'] == 1) ? "checked" : '' ?>/>Sub-Categories
                    </div>
                    <div class="childfeatureCheckbox">
                        <input type="radio" name="homepage_carousals" value="0" <?php echo ($activeRegion['R_Homepage_Carousals'] == 0) ? "checked" : '' ?>/>Main Categories
                    </div>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>SEO Title </label>
                <div class="form-data">
                    <input name="seoTitle" type="text" value="<?php echo (isset($activeRegion['R_SEO_Title']) ? $activeRegion['R_SEO_Title'] : '') ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>SEO Description </label>
                <div class="form-data">
                    <input name="seoDescription" type="text" value="<?php echo (isset($activeRegion['R_SEO_Description']) ? $activeRegion['R_SEO_Description'] : '') ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>SEO Keywords</label>
                <div class="form-data">
                    <input name="seoKeywords" type="text" value="<?php echo (isset($activeRegion['R_SEO_Keywords']) ? $activeRegion['R_SEO_Keywords'] : '') ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Top Navigation Title</label>
                <div class="form-data"> 
                    <input type="text" name="nav_title" value="<?php echo (isset($activeRegion['R_Topnav_Title']) ? $activeRegion['R_Topnav_Title'] : '') ?>" size="50"/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin" id="r_title"> 
                <label>Trip Planner Title</label>
                <div class="form-data"> 
                    <input type="text" name="trip_title" value="<?php echo (isset($activeRegion['R_Trip_Planner_Title']) ? $activeRegion['R_Trip_Planner_Title'] : '') ?>" size="50"/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Routes Title</label>
                <div class="form-data"> 
                    <input type="text" name="route_title" value="<?php echo (isset($activeRegion['R_Route_Title']) ? $activeRegion['R_Route_Title'] : '') ?>" size="50"/>
                </div>
            </div>
            <?php if ($activeRegion['R_Type'] != '6') { ?>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Homepage Title</label>
                    <div class="form-data"> 
                        <input type="text" name="homepage_title" value="<?php echo (isset($activeRegion['R_Homepage_Title']) ? $activeRegion['R_Homepage_Title'] : '') ?>" size="50"/>
                    </div>
                </div>

            <?php } ?>
            <div class="form-inside-div form-inside-div-width-admin" id="homepage_stories_title"> 
                <label>Homepage Stories Title</label>
                <div class="form-data"> 
                    <input type="text" name="homepage_stories_title"  value="<?php echo (isset($activeRegion['R_Homepage_Stories_Title']) ? $activeRegion['R_Homepage_Stories_Title'] : '') ?>" size="50"/>
                </div>
            </div>
            <?php if ($activeRegion['R_Type'] !== '6') { ?>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Homepage Text Above Events</label>
                    <div class="form-data"> 
                        <input type="text" name="homepage_text_above_events" value="<?php echo (isset($activeRegion['R_Homepage_Text_Above_Events']) ? $activeRegion['R_Homepage_Text_Above_Events'] : '') ?>" size="50"/>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Homepage Events Title</label>
                    <div class="form-data"> 
                        <input type="text" name="homepage_events_title" value="<?php echo (isset($activeRegion['R_Homepage_Events_Title']) ? $activeRegion['R_Homepage_Events_Title'] : '') ?>" size="50"/>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Homepage Event Register Text</label>
                    <div class="form-data"> 
                        <input type="text" name="homepage_event_register_text" value="<?php echo (isset($activeRegion['R_Homepage_Event_Register_Text']) ? $activeRegion['R_Homepage_Event_Register_Text'] : '') ?>" size="50"/>
                    </div>
                </div>
                <div class="form-inside-div form-inside-div-width-admin"> 
                    <label>Homepage Event See All Text</label>
                    <div class="form-data"> 
                        <input type="text" name="homepage_event_see_all_text" value="<?php echo (isset($activeRegion['R_Homepage_Event_See_All_Text']) ? $activeRegion['R_Homepage_Event_See_All_Text'] : '') ?>" size="50"/>
                    </div>
                </div>
            <?php } ?>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Listings Title</label>
                <div class="form-data"> 
                    <input type="text" name="listings_title" value="<?php echo ((isset($activeRegion['R_Listings_Title']) && $activeRegion['R_Listings_Title'] != '') ? $activeRegion['R_Listings_Title'] : '') ?>" size="50"/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin" id="story_share_text"> 
                <label>Share Story Text</label>
                <div class="form-data"> 
                    <input type="text" name="story_share_text" value="<?php echo (isset($activeRegion['R_Story_Share_Text']) ? $activeRegion['R_Story_Share_Text'] : '') ?>" size="50"/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin" id="story_see_all_text"> 
                <label>See All Stories Text</label>
                <div class="form-data"> 
                    <input type="text" name="story_see_all_text" value="<?php echo (isset($activeRegion['R_Story_See_All_Text']) ? $activeRegion['R_Story_See_All_Text'] : '') ?>" size="50"/>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Description</label>
                <div class="form-data"> 
                    <textarea name="description" cols="85" rows="10" class="tt-description"><?php echo (isset($activeRegion['R_Description']) ? $activeRegion['R_Description'] : '') ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Mobile Description</label>
                <div class="form-data"> 
                    <textarea name="mobile_description" cols="85" rows="10" class="tt-description"><?php echo (isset($activeRegion['R_Mobile_Description']) ? $activeRegion['R_Mobile_Description'] : '') ?></textarea>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin" id="defaultRadius"> 
                <label>Default Radius</label>
                <div class="form-data">
                    <select name="radius">
                        <option value="1" <?php echo (isset($activeRegion['R_Radius']) && $activeRegion['R_Radius'] == 1) ? 'selected' : '' ?>>1 km</option>
                        <option value="10" <?php echo (isset($activeRegion['R_Radius']) && $activeRegion['R_Radius'] == 10) ? 'selected' : '' ?> >10 km</option>
                        <option value="20" <?php echo (isset($activeRegion['R_Radius']) && $activeRegion['R_Radius'] == 20) ? 'selected' : '' ?> >20 km</option>
                        <option value="30" <?php echo (isset($activeRegion['R_Radius']) && $activeRegion['R_Radius'] == 30) ? 'selected' : '' ?> >30 km</option>
                        <option value="50" <?php echo (isset($activeRegion['R_Radius']) && $activeRegion['R_Radius'] == 50) ? 'selected' : '' ?> >50 km</option>
                    </select>
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Domain</label>
                <div class="form-data">
                    <input name="domain" type="text" value="<?php echo (isset($activeRegion['R_Domain']) ? $activeRegion['R_Domain'] : '') ?>" size="50" required/> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Domain Alternates</label>
                <div class="form-data">
                    <input name="domain_alternates" type="text" value="<?php echo (isset($activeRegion['R_Domain_Alternates']) ? $activeRegion['R_Domain_Alternates'] : '') ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Event Notification Email</label>
                <div class="form-data">
                    <input name="event_email" type="email" value="<?php echo (isset($activeRegion['R_Event_Email']) ? $activeRegion['R_Event_Email'] : '') ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Tracking ID</label>
                <div class="form-data">
                    <input name="tracking_id" type="text" value="<?php echo (isset($activeRegion['R_Tracking_ID']) ? $activeRegion['R_Tracking_ID'] : '') ?>" size="50" /> 
                </div>
                <span style="float:left;padding:17px 0 16px 20px;">e.g. UA-32156009-1</span>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Stories in SideBar?</label>
                <div class="form-data"> 
                    <input name="sidebar_stories" id="sidebar_stories" type="text" value="<?php echo (isset($activeRegion['R_Sidebar_Stories']) ? $activeRegion['R_Sidebar_Stories'] : '0') ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin border-none"> 
                <label>Listings on Homepage per Category?</label>
                <div class="form-data"> 
                    <input name="homepage_listings" type="text" value="<?php echo (isset($activeRegion['R_Homepage_Listings']) ? $activeRegion['R_Homepage_Listings'] : '10') ?>" size="50" /> 
                </div>
            </div>

            <div class="content-header">Invoice Information</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Business Name</label>
                <div class="form-data"> 
                    <input name="R_cname" type="text" value="<?php echo (isset($activeRegion['R_Clientname']) ? $activeRegion['R_Clientname'] : '') ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Street Address</label>
                <div class="form-data"> 
                    <input name="R_cstreet" type="text" value="<?php echo (isset($activeRegion['R_Street']) ? $activeRegion['R_Street'] : '') ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Town</label>
                <div class="form-data"> 
                    <input name="R_ctown" type="text" value="<?php echo (isset($activeRegion['R_Town']) ? $activeRegion['R_Town'] : '') ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Province</label>
                <div class="form-data"> 
                    <input name="R_cprovince" type="text" value="<?php echo (isset($activeRegion['R_Province']) ? $activeRegion['R_Province'] : '') ?>" size="50" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>Postal Code</label>
                <div class="form-data"> 
                    <input name="R_cpostalcode" type="text" value="<?php echo (isset($activeRegion['R_PostalCode']) ? $activeRegion['R_PostalCode'] : '') ?>" size="50" /> 
                </div>
            </div>
            <div class="content-header">Authorize.net Information</div>
            <div class="form-inside-div form-inside-div-width-admin"> 
                <label>API Login ID</label>
                <div class="form-data"> 
                    <input name="api_login_id" type="text" value="<?php echo (isset($activeRegion['R_API_Login_ID']) ? $activeRegion['R_API_Login_ID'] : '') ?>" size="90" /> 
                </div>
            </div>
            <div class="form-inside-div form-inside-div-width-admin border-none"> 
                <label>Transaction Key</label>
                <div class="form-data"> 
                    <input name="transaction_key" type="text" value="<?php echo (isset($activeRegion['R_Transaction_Key']) ? $activeRegion['R_Transaction_Key'] : '') ?>" size="90" /> 
                </div>
            </div>
            <div class="form-inside-div  width-data-content border-none"> 
                <div class="button"><input type="submit" name="button2" id="button22" value="Submit" />
                </div>
            </div>
        </form>
    </div>
    <div id="dialog-message"></div>
</div>
<?PHP
require_once '../include/admin/footer.php';
?>
<script>
    $(function () {
        $(".brief").sortable({opacity: 0.9, cursor: 'move', update: function () {
                var order = $(this).sortable("serialize") + '&action=updateRecordsListings&table=tbl_Region_Icon&field=RI_Order&id=RI_ID';
                $.post("reorder.php", order, function (theResponse) {
                    $("#dialog-message").html("HomePage Links Re-Ordered");
                    $("#dialog-message").dialog({
                        modal: true,
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                });
            }
        });
    });
    $('#zoom_map').change(function () {
        var zoom_map = $("#zoom_map").val();
        if (parseFloat(zoom_map) > 20 || parseFloat(zoom_map) < 8)
        {
            swal("Error", "Zoom must be greater than 8 and less than 20!", "error");
            $("#zoom_map").val(0);
        }
    });

    if ($("#chk_route").is(':checked')) {
        $("#r_title").show();
        $("#nav_manage_routes").show();
    } else {
        $("#r_title").hide();
        $("#nav_manage_routes").hide();
    }
    if ($("#include_free_listings").is(':checked')) {
        $("#hide_phone").show();
        $("#hide_email").show();
    } else {
        $("#hide_phone").hide();
        $("#hide_email").hide();
    }
    if ($("#R_Stories").is(':checked')) {
        $("#sidebar_stories").show();
        $("#homepage_stories_title").show();
        $("#story_share_text").show();
        $("#story_see_all_text").show();
        $("#nav_homepage_stories").show();
        $("#nav_map_stories").show();
    } else {
        $("#sidebar_stories").hide();
        $("#homepage_stories_title").hide();
        $("#story_share_text").hide();
        $("#story_see_all_text").hide();
        $("#nav_homepage_stories").hide();
        $("#nav_map_stories").hide();
    }
    if ($("#R_Whats_NearBy").is(':checked')) {
        $("#defaultRadius").show();
    } else {
        $("#defaultRadius").hide();
    }
    if ($("#mytrip").is(':checked')) {
        $("#r_title").show();
    } else {
        $("#r_title").hide();
    }
    if ($("#day_trip").is(':checked')) {
        $("#day_trip_menu").show();
    } else {
        $("#day_trip_menu").hide();
    }

    function showHideVal(checkbox, hideshowit, hideShowVal, hideShowVal1, hideShowVal2, hideShowVal3, hideShowVal4, hideShowVal5) {
        if (document.getElementById(checkbox).checked) {
            document.getElementById(hideshowit).style.display = "block";
            $("#" + hideShowVal).show();
            $("#" + hideShowVal1).show();
            $("#" + hideShowVal2).show();
            $("#" + hideShowVal3).show();
            $("#" + hideShowVal4).show();
            $("#" + hideShowVal5).show();

        } else {
            document.getElementById(hideshowit).style.display = "none";
            $("#" + hideShowVal).hide();
            $("#" + hideShowVal1).hide();
            $("#" + hideShowVal2).hide();
            $("#" + hideShowVal3).hide();
            $("#" + hideShowVal4).hide();
            $("#" + hideShowVal5).hide();
        }
    }
</script>
