<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
$category_ids = "";
$region_ids = "";
$subcategories_ids = "";
$emptyImage  = "";
$coupons_Val = "";
$unverified_Lisiting = "";
$type = $_POST['listing_type'];
$type = " AND BL_Listing_Type IN ($type)";

 // For listings with no images
if (isset($_REQUEST['no_image']) && $_REQUEST['no_image'] == '8') {
  $emptyImage .= " AND BL_Photo = '' AND BL_Header_Image = ''";
} 
 // For coupon create
if (isset($_REQUEST['coupons']) && $_REQUEST['coupons'] == '9') {
  $coupons_Val .= " INNER JOIN tbl_Business_Feature_Coupon ON BFC_BL_ID = BL_ID";
} 
// For Unverified Listings
if (isset($_REQUEST['uLsitng']) && $_REQUEST['uLsitng'] == '7') {
  $unverified_Lisiting .= "AND BL_Free_Listing_status = 1 AND BL_Listing_Type = 1";
} 
 // For region
if (isset($_REQUEST['region']) && $_REQUEST['region'] > 0) {
  $region_ids = $_REQUEST['region'];
} 
foreach ($_POST as $value) {
    $str = str_replace("[]", "", $value);
    foreach ($str as $i => $ids) {
        $strExplode = explode("-", $ids);
        if ($strExplode[0] == "category") {
            if ($category_ids == "") {
                $category_ids = $strExplode[1];
            } else {
                $category_ids .= "," . $strExplode[1];
            }
        }
//        if ($strExplode[0] == "sortregion") {
//            if ($region_ids == "") {
//                $region_ids = $strExplode[1];
//            } else {
//                $region_ids .= "," . $strExplode[1];
//            }
//        }
        $sub_cat = explode("[", $strExplode[0]);
        if ($sub_cat[0] == "subcategory") {
            if ($subcategories_ids == "") {
                $subcategories_ids = $strExplode[1];
            } else {
                $subcategories_ids .= "," . $strExplode[1];
            }
        }
    }
}
if ($region_ids != "" && $category_ids != "" && $subcategories_ids != "") {
    $select = " SELECT BL_ID, BL_Listing_Title FROM tbl_Business_Listing 
                LEFT JOIN tbl_Business ON BL_B_ID = B_ID $unverified_Lisiting
                LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
                INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID $coupons_Val 
                WHERE (BLC_C_ID IN ($subcategories_ids) AND BLC_M_C_ID IN ($category_ids)) AND BLCR_BLC_R_ID IN ($region_ids) AND (B_Email != '' OR BL_Email != '') AND B_Subscription = 1 $type $emptyImage 
                AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = 1 GROUP BY BL_ID ORDER BY BL_Listing_Title";
} else if ($region_ids != "" && $category_ids != "") {
    $select = " SELECT BL_ID, BL_Listing_Title FROM tbl_Business_Listing 
                LEFT JOIN tbl_Business ON BL_B_ID = B_ID $unverified_Lisiting
                LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
                INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID $coupons_Val 
                WHERE BLC_M_C_ID IN ($category_ids) AND BLCR_BLC_R_ID IN ($region_ids) AND (B_Email != '' OR BL_Email != '') AND B_Subscription = 1 $type $emptyImage 
                AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = 1 GROUP BY BL_ID ORDER BY BL_Listing_Title";
} else if ($subcategories_ids != "" && $category_ids != "") {
    $select = " SELECT BL_ID, BL_Listing_Title FROM tbl_Business_Listing 
                LEFT JOIN tbl_Business ON BL_B_ID = B_ID  $unverified_Lisiting
                LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID $un_Lisiting_Val 
                WHERE (BLC_C_ID IN ($subcategories_ids) AND BLC_M_C_ID IN ($category_ids)) AND (B_Email != '' OR BL_Email != '') AND B_Subscription = 1 $type $emptyImage 
                AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = 1 GROUP BY BL_ID ORDER BY BL_Listing_Title";
} else if ($region_ids != "") {
    $select = " SELECT BL_ID, BL_Listing_Title FROM tbl_Business_Listing 
                LEFT JOIN tbl_Business ON BL_B_ID = B_ID $unverified_Lisiting
                LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
                INNER JOIN tbl_Business_Listing_Category_Region ON BLC_BL_ID = BLCR_BL_ID $coupons_Val 
                WHERE BLCR_BLC_R_ID IN ($region_ids) AND (B_Email != '' OR BL_Email != '') AND B_Subscription = 1 $type $emptyImage 
                AND (BL_Free_Listing_status = 0 OR CURDATE() < DATE_ADD(BL_Free_Listing_status_Date,INTERVAL 7 DAY)) AND hide_show_listing = 1 GROUP BY BL_ID ORDER BY BL_Listing_Title";
}
//echo $select;
$listings = mysql_query($select, $db);
if (mysql_num_rows($listings) > 0) {
    ?>
    <div class="content-header content-header-search">
        <span>Listings</span>
    </div>
    <div class="form-inside-div form-inside-div-width-admin">
        <div class="emailColumn" style="width: 100%;">
            <div class="">
                <input type="checkbox" id="select_all_listings" name="all_listings" style="float: left;"><strong style="float: left; margin-bottom: 15px;">Select All Listings</strong>
                <div class="subcatCheckbox" id="listings_siblings" style="width: 100%; float: left;">
                    <?php while ($list_row = mysql_fetch_array($listings)) { ?>
                        <div class="subcatCheckboxInner" style="width: 33%; float: left;">
                            <input type="checkbox" name="selected_listings[]" class="selected_listings" value="<?php echo $list_row['BL_ID'] ?>"><?php echo $list_row['BL_Listing_Title'] ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>