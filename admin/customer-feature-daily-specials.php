<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
require_once '../include/image-bank-usage-function.php';
require_once '../include/adminFunctions.inc.php';
require_once '../include/track-data-entry.php';

if (!in_array('listing-tools', $_SESSION['USER_PERMISSIONS']) && ((!in_array('town-assets-listings', $_SESSION['USER_PERMISSIONS'])) && $_SESSION['USER_SHOW_BUSINESSES'] != 1)) {
    header("Location: /admin/");
    exit();
}

$BL_ID = $_REQUEST['bl_id'];

if ($BL_ID > 0) {
    $sql = "SELECT B_ID, BL_Listing_Title FROM tbl_Business_Listing LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            WHERE BL_ID = '" . encode_strings($BL_ID, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
} else {
    header("Location:customers.php");
    exit();
}
if (isset($_POST['button_description'])) {

    $daily_description = $_POST['daily-special-description'];
    $bl_id_description = $_POST['bl_id'];
    $sql_daily_des = mysql_query("SELECT BFDSD_ID from tbl_Business_Feature_Daily_Specials_Description WHERE BFDSD_BL_ID = '$BL_ID'");
    $count = mysql_num_rows($sql_daily_des);
    if ($count > 0) {
        $sql_daily_des_update = "UPDATE tbl_Business_Feature_Daily_Specials_Description SET BFDSD_Description = '$daily_description' WHERE BFDSD_BL_ID = '$bl_id_description'";
        $result = mysql_query($sql_daily_des_update);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Daily Features', '', 'Update Description', 'super admin');
    } else {
        $sql_daily_des_insert = "INSERT tbl_Business_Feature_Daily_Specials_Description SET BFDSD_BL_ID= '$bl_id_description',BFDSD_Description = '$daily_description'";
        $result = mysql_query($sql_daily_des_insert);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Daily Features', '', 'Add Description', 'super admin');
    }
    $result = mysql_query($sql, $db);
    if ($result) {
        $_SESSION['success'] = 1;
    } else {
        $_SESSION['error'] = 1;
    }
    if (isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true) {
        header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    } else {
        header("Location: customer-feature-daily-specials.php?bl_id=" . $BL_ID);
    }

    exit;
}
$sql_description_daily = mysql_query("SELECT BFDSD_ID,BFDSD_Description from tbl_Business_Feature_Daily_Specials_Description WHERE BFDSD_BL_ID = '$BL_ID'");
$data_description = mysql_fetch_assoc($sql_description_daily);

if (isset($_POST['op']) && $_POST['op'] == 'save') {
    $select = "SELECT BFDS_ID FROM tbl_Business_Feature_Daily_Specials 
                WHERE BFDS_BL_ID = '" . encode_strings($_POST['bl_id'], $db) . "' AND BFDS_Day = '" . encode_strings($_POST['weekno'], $db) . "'";
    $sql_check = mysql_query($select) or die(mysql_error());
    $rowDaily = mysql_fetch_array($sql_check);
    $count = mysql_num_rows($sql_check);
    require_once '../include/picUpload.inc.php';
    if ($_POST['image_bank'] == "") {
        // last @param 19 = Daily Features
        $pic = Upload_Pic('0', 'pic', 0, 0, true, IMG_LOC_ABS, 10000000, true, 19, 'Listing', $BL_ID);
        if ($count > 0) {
            if (is_array($pic)) {
                $sql = "UPDATE tbl_Business_Feature_Daily_Specials SET 
                        BFDS_Title = '" . encode_strings($_POST['title'], $db) . "', 
                        BFDS_Description = '" . encode_strings($_POST['description'], $db) . "', 
                        BFDS_Photo = '" . encode_strings($pic['0']['0'], $db) . "'
                        WHERE BFDS_BL_ID = '" . encode_strings($_POST['bl_id'], $db) . "' AND BFDS_Day = '" . encode_strings($_POST['weekno'], $db) . "'";
                $pic_id = $pic['1'];
            } else {
                $sql = "UPDATE tbl_Business_Feature_Daily_Specials SET 
                        BFDS_Title = '" . encode_strings($_POST['title'], $db) . "', 
                        BFDS_Description = '" . encode_strings($_POST['description'], $db) . "'
                        WHERE BFDS_BL_ID = '" . encode_strings($_POST['bl_id'], $db) . "' AND BFDS_Day = '" . encode_strings($_POST['weekno'], $db) . "'";
            }
            // TRACK DATA ENTRY
            $id = $BL_ID;
            Track_Data_Entry('Listing', $id, 'Daily Features', '', 'Update Item', 'super admin');
        } else {
            $sql = "INSERT tbl_Business_Feature_Daily_Specials SET 
                    BFDS_Title = '" . encode_strings($_POST['title'], $db) . "', 
                    BFDS_Description = '" . encode_strings($_POST['description'], $db) . "', 
                    BFDS_Photo = '" . encode_strings($pic['0']['0'], $db) . "', 
                    BFDS_BL_ID = '" . encode_strings($_POST['bl_id'], $db) . "', 
                    BFDS_Day = '" . encode_strings($_POST['weekno'], $db) . "'";
            $pic_id = $pic['1'];
            // TRACK DATA ENTRY
            $id = $BL_ID;
            Track_Data_Entry('Listing', $id, 'Daily Features', '', 'Add Item', 'super admin');
        }
    } else {
        $pic_id = $_POST['image_bank'];
        // last @param 19 = Daily Features
        $pic = Upload_Pic_Library($pic_id, 19);
        Update_Image_Bank_Listings($pic_id, $BL_ID);
        if ($count > 0) {
            if (is_array($pic)) {
                $sql = "UPDATE tbl_Business_Feature_Daily_Specials SET 
                        BFDS_Title = '" . encode_strings($_POST['title'], $db) . "', 
                        BFDS_Description = '" . encode_strings($_POST['description'], $db) . "', 
                        BFDS_Photo = '" . encode_strings($pic['0'], $db) . "'
                        WHERE BFDS_BL_ID = '" . encode_strings($_POST['bl_id'], $db) . "' AND BFDS_Day = '" . encode_strings($_POST['weekno'], $db) . "'";
            }
            // TRACK DATA ENTRY
            $id = $BL_ID;
            Track_Data_Entry('Listing', $id, 'Daily Features', '', 'Update Item', 'super admin');
        } else {
            $sql = "INSERT tbl_Business_Feature_Daily_Specials SET 
                    BFDS_Title = '" . encode_strings($_POST['title'], $db) . "', 
                    BFDS_Description = '" . encode_strings($_POST['description'], $db) . "', 
                    BFDS_Photo = '" . encode_strings($pic['0'], $db) . "', 
                    BFDS_BL_ID = '" . encode_strings($_POST['bl_id'], $db) . "', 
                    BFDS_Day = '" . encode_strings($_POST['weekno'], $db) . "'";
            // TRACK DATA ENTRY
            $id = $BL_ID;
            Track_Data_Entry('Listing', $id, 'Daily Features', '', 'Add Item', 'super admin');
        }
    }
    $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
    if ($count > 0) {
        $id = $rowDaily['BFDS_ID'];
    } else {
        $id = mysql_insert_id();
    }
    if ($result) {
        $_SESSION['success'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        if ($pic_id > 0) {
            //Image usage from image bank
            imageBankUsage($pic_id, 'IBU_BL_ID', $BL_ID, 'IBU_Daily_Features', $id);
        }
    } else {
        $_SESSION['error'] = 1;
    }
    if (isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true) {
        header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    } else {
        header("Location: customer-feature-daily-specials.php?bl_id=" . $BL_ID);
    }

    exit();
}
if (isset($_GET['op']) && $_GET['op'] == 'del_photo') {
    
    $sql = "UPDATE tbl_Business_Feature_Daily_Specials SET BFDS_Photo = '' 
            WHERE BFDS_BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "' AND BFDS_Day = '" . encode_strings($_REQUEST['bds_id_photo'], $db) . "'";
    $result = mysql_query($sql);
    if ($result) {
        $_SESSION['delete'] = 1;
        $select = "SELECT BFDS_ID FROM tbl_Business_Feature_Daily_Specials 
                    WHERE BFDS_BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "' AND BFDS_Day = '" . encode_strings($_REQUEST['bds_id_photo'], $db) . "'";
        $sql_check = mysql_query($select) or die(mysql_error());
        $rowDaily = mysql_fetch_array($sql_check);
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Daily_Features', $rowDaily['BFDS_ID']);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Daily Features', '', 'Delete Image', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    if (isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true) {
        header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    } else {
        header("Location: customer-feature-daily-specials.php?bl_id=" . $_REQUEST['bl_id']);
    }

    exit();
}
if (isset($_GET['op']) && $_GET['op'] == 'del_item') {
    $sql = "DELETE FROM tbl_Business_Feature_Daily_Specials WHERE BFDS_BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "' AND BFDS_Day = '" . encode_strings($_REQUEST['bds_item_id'], $db) . "'";
    $result = mysql_query($sql);
    if ($result) {
        $_SESSION['delete'] = 1;
        //update points only for listing
        update_pointsin_business_tbl($BL_ID);
        $select = "SELECT BFDS_ID FROM tbl_Business_Feature_Daily_Specials 
                    WHERE BFDS_BL_ID = '" . encode_strings($_REQUEST['bl_id'], $db) . "' AND BFDS_Day = '" . encode_strings($_REQUEST['bds_item_id'], $db) . "'";
        $sql_check = mysql_query($select) or die(mysql_error());
        $rowDaily = mysql_fetch_array($sql_check);
        //Delete from image usage when image is deleted
        imageBankUsageDelete('IBU_BL_ID', $BL_ID, 'IBU_Daily_Features', $rowDaily['BFDS_ID']);
        // TRACK DATA ENTRY
        $id = $BL_ID;
        Track_Data_Entry('Listing', $id, 'Daily Features', '', 'Delete Item', 'super admin');
    } else {
        $_SESSION['delete_error'] = 1;
    }
    if (isset($_REQUEST['go_to_preview']) && $_REQUEST['go_to_preview'] == true) {
        header("Location: /admin/listings-preview.php?bl_id=" . $BL_ID);
    } else {
        header("Location: customer-feature-daily-specials.php?bl_id=" . $_REQUEST['bl_id']);
    }

    exit();
}
require_once '../include/admin/header.php';
?>
<div class="content-left">
<?php require_once '../include/top-nav-listing.php'; //top-nav-listing  ?> 
    <div class="title-link">
        <div class="title">Add Ons - <?php echo $rowListing['BL_Listing_Title'] ?></div>
        <div class="link">
<?PHP
require_once ('preview-link.php');
?>
        </div> 
    </div>

    <div class="left">
            <?PHP
            require '../include/nav-listing-admin.php'; // nav-listing-admin
            ?>
    </div>
    <div class="right">

        <input type="hidden" name="bl_id" id="daily_bl_id" value="<?php echo $BL_ID ?>"> 
        <input type="hidden" name="op" value="save">
        <div class="content-header">
            <div class="title">Daily Features</div>
            <div class="link">
<?php
$DS = show_addon_points(7);
if ($numRowsDS > 0) {
    echo '<div class="points-com">' . $DS . ' pts</div>';
} else {
    echo '<div class="points-uncom">' . $DS . ' pts</div>';
}
?>
            </div>
        </div>

                <?php
                $help_text = show_help_text('Daily Specials');
                if ($help_text != '') {
                    echo '<div class="form-inside-div" style="border-bottom:3px solid #eeeeee;">' . $help_text . '</div>';
                }
                ?>
        <form name="form1" method="post"  action="">
            <div class="daily-special-specifivation">
                <div class="form-inside-div add-menu-item Description-width">
                    <input type="hidden" name="bl_id" id="daily_bl_id" value="<?php echo $BL_ID ?>"> 
                    <label>Description</label>
                    <div class="form-data add-menu-item-field wiziwig-about">
                        <textarea name="daily-special-description" class="description-textarea description-ckeditor" id="daily-description"  cols="60" rows="3" ><?php echo $data_description['BFDSD_Description'] ?></textarea>
                    </div>
                </div>
                <div class="form-inside-div add-menu-item-button">
                    <div class="button">
                        <input type="submit" name="button_description" value="Save" />
                    </div>      
                </div> 
            </div>      
        </form>         
<?PHP
foreach ($days as $key => $val) {
    $sql = "SELECT BFDS_BL_ID, BFDS_Title, BFDS_Description, BFDS_Photo FROM tbl_Business_Feature_Daily_Specials 
                    WHERE BFDS_BL_ID = '$BL_ID' AND BFDS_Day = '" . encode_strings($key, $db) . "' LIMIT 1";
    $result = mysql_query($sql, $db)
            or die("Invalid query: $sql -- " . mysql_error());
    $data = mysql_fetch_assoc($result);
    ?>
            <form name="menu-item" method="post" onSubmit="return check_img_size(<?php echo $key ?>, 10000000)"  enctype="multipart/form-data" action="">
                <input type="hidden" name="bl_id" value="<?php echo ($data['BFDS_BL_ID'] != "") ? $data['BFDS_BL_ID'] : $BL_ID ?>">
                <input type="hidden" name="weekno" value="<?php echo $key ?>">
                <input type="hidden" name="op" value="save">
                <input type="hidden" id="update_<?php echo $key ?>" value="1">
                <input type="hidden" name="image_bank" class="image_bank" id="image_bank<?php echo $key ?>" value="">
                <div class="data-content daily-special-items">
                    <div class="form-inside-div add-menu-item"><label class="dayname-label"><?php echo $val ?></label></div>
                    <div class="add-menu-item-container">
                        <div class="form-inside-div add-menu-item">
                            <label>Name</label>    
                            <div class="form-data add-menu-item-field daily-special-width"> 
                                <input type="text" name="title" id="daily-title<?php echo $key ?>" value="<?php echo $data['BFDS_Title'] ?>" required/>
                            </div>
                        </div>

                        <div class="form-inside-div add-menu-item">
                            <label>Description</label>
                            <div class="form-data add-menu-item-field wizwig_editor_product_items">
                                <textarea name="description" class="description-ckeditor"  id="daily-description<?php echo $key ?>"  cols="40" rows="8" ><?php echo $data['BFDS_Description'] ?></textarea>
                            </div>
                        </div>

                    </div> 
                    <div class="form-inside-div div_image_library add-menu-item-image daily-feature-cropit">
                        <span class="daily_browse" onclick="show_image_library(19,<?php echo $key ?>)">Select File</span>
    <!--                        <div class="dialog-close dialog<?php echo $key ?> dialog-bank" style="display: none;">
                            <label for="photo<?php echo $key ?>" class="daily_browse">Upload New Photo</label>
                            <a onclick="show_image_library(19,<?php echo $key ?>)">Add from Library</a>
                        </div>-->
                        <input type="file" name="pic[]" onchange="show_file_name(19, this, <?php echo $key ?>)" id="photo<?php echo $key ?>" style="display: none;">
                        <div class="cropit-image-preview aboutus-photo-perview">
                            <img class="preview-img preview-img-script<?php echo $key ?> max-width-thumbnail" style="display: none;" src="">    
    <?php if ($data['BFDS_Photo'] != "") { ?>
                                <img class="existing-img existing_imgs<?php echo $key ?> max-width-thumbnail" src="http://<?php echo DOMAIN . IMG_LOC_REL . $data['BFDS_Photo'] ?>">
    <?php } ?>
                        </div>
                    </div>
                    <div class="form-inside-div add-menu-item-image">
    <?php if ($data['BFDS_Photo'] != "") { ?>
                            <div class="data-column daily-del">
                                <a class="deletePhoto" href="customer-feature-daily-specials.php?bl_id=<?php echo $BL_ID ?>&bds_id_photo=<?php echo $key ?>&op=del_photo" onClick="return confirm('Are you sure you want to delete photo?');">Delete Photo</a>
                            </div>
                            <?php } ?>
                        <div class="data-column delete-orange"></div>
                    </div>
                    <div class="form-inside-div border-none button-spaces">
                        <div class="button">
                            <input type="submit" name="button3" class="button<?php echo $key ?>" value="Save Daily Feature" />
                            <a class="btn-anchor" href="customer-feature-daily-specials.php?bl_id=<?php echo $BL_ID ?>&bds_item_id=<?php echo $key ?>&op=del_item&flag=<?php echo $_REQUEST['flag']; ?>" onClick="return confirm('Are you sure you want to delete it?');">Delete Daily Feature</a>
                        </div>      
                    </div> 
                </div>
            </form>
            <script>
                CKEDITOR.disableAutoInline = true;
                $(document).ready(function () {
                    var wzwig_daily_key = <?php echo $key ?>;
                    $('#daily-description' + wzwig_daily_key).ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
                });
            </script>
    <?PHP
}
$sql1 = "SELECT BLF_BL_ID, BLF_ID, BLF_Active FROM tbl_BL_Feature WHERE BLF_BL_ID = $BL_ID AND BLF_F_ID = 7";
$result1 = mysql_query($sql1, $db) or die("Invalid query: $sql1 -- " . mysql_error());
$row1 = mysql_fetch_assoc($result1);
if ($row1['BLF_BL_ID'] != '') {
    ?>
            <div class="form-inside-div border-none text-align-center">
                <a href="customer-feature-add-ons.php?id=<?php echo $row1['BLF_ID'] ?>&bl_id=<?php echo $BL_ID ?>"  onclick="return confirm('Are you sure you want to <?php echo $row1['BLF_Active'] == 1 ? "deactivate" : "activate" ?> this Add on?');"><?php echo $row1['BLF_Active'] == 1 ? "Deactivate" : "Activate" ?> This Add on</a>
            </div>
            <?PHP
        }
        ?>
        <div id="image-library" style="display:none;"></div>
        <input id="image-library-usage" type="hidden" value="">
    </div>
</div>
<script>
    CKEDITOR.disableAutoInline = true;
    $(document).ready(function () {
        $('#daily-description').ckeditor(); // Use CKEDITOR.replace() if element is <textarea>.
    });
</script>
<?PHP
require_once('rank-advertise.php');
require_once '../include/admin/footer.php';
?>