<?php
/**
 * This file contains config info for the sample app.
 */

// Adjust this to point to the Authorize.Net PHP SDK
require_once 'authorize/AuthorizeNet.php';


$METHOD_TO_USE = "AIM";
// $METHOD_TO_USE = "DIRECT_POST";         // Uncomment this line to test DPM

define("AUTHORIZENET_API_LOGIN_ID", $payment_region['R_API_Login_ID']);    // Sandbox info
define("AUTHORIZENET_TRANSACTION_KEY",$payment_region['R_Transaction_Key']); // Sandbox info
//define("AUTHORIZENET_API_LOGIN_ID","5X4B2JwtuF");    // Sandbox info
//define("AUTHORIZENET_TRANSACTION_KEY","6vB99Cr6y8V9qTz4"); // Sandbox info
//define("AUTHORIZENET_API_LOGIN_ID","3E28bSjM");    // Live site info
//define("AUTHORIZENET_TRANSACTION_KEY","5K4k3H6s9u6aT8kS"); // Live site info
//define("AUTHORIZENET_SANDBOX",true);       // Set to false to test against production
define("AUTHORIZENET_SANDBOX",true);       // Set to false to test against production
define("TEST_REQUEST", "FALSE");           // You may want to set to true if testing against production


// You only need to adjust the two variables below if testing DPM
define("AUTHORIZENET_MD5_SETTING","");                // Add your MD5 Setting.
$site_root = "http://dev.touristtown.ca/admin/payment/"; // Add the URL to your site


if (AUTHORIZENET_API_LOGIN_ID == "") {
    die('Enter your merchant credentials in config.php before running the sample app.');
}
