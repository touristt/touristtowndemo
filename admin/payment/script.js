var basepath  = 'http://dev.touristtown.ca/';
$(document).ready(function() {
  // for default when comes to page
  var card_id = $("#saved_card").val();
  if(card_id == 'new'){
    $('#new_card').show();
    $("#card_number").prop('required', true);
    $("#card_type").prop('required', true);
    $("#first_name").prop('required', true);
    $("#last_name").prop('required', true);
    $("#exp_month").prop('required', true);
    $("#exp_year").prop('required', true);
    $("#card_cvv").prop('required', true);
    $("#address").prop('required', true);
    $("#city").prop('required', true);
    $("#zip").prop('required', true);
        $("#email").prop('required', true);
        $("#region").prop('required', true);
  }
  else {
    $('#new_card').hide();
    $("#card_number").removeProp('required');
    $("#card_type").removeProp('required');
    $("#first_name").removeProp('required');
    $("#last_name").removeProp('required');
    $("#exp_month").removeProp('required');
    $("#exp_year").removeProp('required');
    $("#card_cvv").removeProp('required');
    $("#address").removeProp('required');
    $("#city").removeProp('required');
    $("#zip").removeProp('required');
        $("#email").removeProp('required');
        $("#region").removeProp('required');
  }
  
  // on change event
  $("#saved_card").change(function() {
    var card_id = $(this).val();
    if(card_id == 'new'){
      $('#new_card').show();
      $("#card_number").prop('required', true);
      $("#card_type").prop('required', true);
      $("#first_name").prop('required', true);
      $("#last_name").prop('required', true);
      $("#exp_month").prop('required', true);
      $("#exp_year").prop('required', true);
      $("#card_cvv").prop('required', true);
      $("#address").prop('required', true);
      $("#city").prop('required', true);
      $("#zip").prop('required', true);
            $("#email").prop('required', true);
            $("#region").prop('required', true);
    }
    else {
      $('#new_card').hide();
      $("#card_number").removeProp('required');
      $("#card_type").removeProp('required');
      $("#first_name").removeProp('required');
      $("#last_name").removeProp('required');
      $("#exp_month").removeProp('required');
      $("#exp_year").removeProp('required');
      $("#card_cvv").removeProp('required');
      $("#address").removeProp('required');
      $("#city").removeProp('required');
      $("#zip").removeProp('required');
            $("#email").removeProp('required');
            $("#region").removeProp('required');
    }
  });
});