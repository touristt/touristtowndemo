<?php

require_once '../../include/config.inc.php';
require("../../include/PHPMailer/class.phpmailer.php");

function close_order($listing_id, $card_type, $payment_id) {
    $sql = " SELECT * FROM tbl_Business_Listing        
    LEFT JOIN tbl_Business ON BL_B_ID = B_ID         
    LEFT JOIN tbl_Listing_Type ON BL_Listing_Type = LT_ID         
            LEFT JOIN tbl_Business_Listing_Category ON BL_ID = BLC_BL_ID
    INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID=BL_ID
    WHERE BL_ID = " . $listing_id . " LIMIT 1";
    $result = mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);

    // see the billing cycle
    if ($rowListing['BL_Billing_Type'] == -1) {
        $multiplier = 12;
    } else {
        $multiplier = 1;
    }

    //Advertisements
    $sqlAd = " SELECT * FROM tbl_Advertisement LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID
               WHERE A_BL_ID = '" . mysql_real_escape_string($rowListing['BL_ID']) . "' AND A_Status = 3 AND A_Total > 0
               AND A_Is_Deleted = 0 AND A_Is_Townasset != 1  AND (A_End_Date > CURDATE() OR A_End_Date = '0000-00-00')";
    $resultTMP = mysql_query($sqlAd) or die("Invalid query: $sqlAd -- " . mysql_error());
    $ads_total = 0;
    while ($row = mysql_fetch_assoc($resultTMP)) {
        $renewalDate = strtotime('-2 month', strtotime($rowListing['BL_Renewal_Date']));
        $renewalDate = date("Y-m-d", $renewalDate);
        if ($renewalDate >= $row['A_Active_Date']) {
            $ad_total = ($multiplier * $row['A_Total']);
            $ads_total += $ad_total;
        } else {
            $datetime1 = strtotime(date('Y-m-d', strtotime($row['A_Active_Date'])));
            $datetime2 = strtotime('-1 month', strtotime($rowListing['BL_Renewal_Date']));
            $secs = $datetime2 - $datetime1; // == <seconds between the two times>
            $days = $secs / 86400;
            $ad_total = ((($multiplier * $row['A_Total']) / 30) * $days);
            if ($ad_total < 0) {
                $ad_total = 0;
            }
            $ads_total += $ad_total;
        }
    }

    $subtotal = $rowListing['BL_SubTotal'] + $ads_total;
    $subtotal2 = $subtotal - $rowListing['BL_CoC_Dis_2'];
    $tax = round($subtotal2 * .13, 2);
    $total = $subtotal2 + $tax;

    //get Payment Profile information
    $sql_payment_profile = "SELECT * FROM payment_profiles WHERE business_id = '" . $rowListing['BL_B_ID'] . "'";
    $res_payment_profile = mysql_query($sql_payment_profile);
    $payment_payment_profile = mysql_fetch_assoc($res_payment_profile);

    $sql = " INSERT tbl_Business_Billing SET 
            BB_B_ID = '" . mysql_real_escape_string($rowListing['B_ID']) . "', 
            BB_Listing = '" . mysql_real_escape_string($rowListing['BL_Number']) . "', 
            BB_BL_ID = '" . mysql_real_escape_string($rowListing['BL_ID']) . "', 
            BB_R_ID = '" . mysql_real_escape_string($rowListing['BLCR_BLC_R_ID']) . "', 
            BB_Category = '" . mysql_real_escape_string($rowListing['BLC_M_C_ID']) . "', 
            BB_Date = NOW(), 
            BB_Renewal_Date = '" . mysql_real_escape_string($rowListing['BL_Renewal_Date']) . "', 
            BB_Payment_Type = '" . mysql_real_escape_string($rowListing['BL_Payment_Type']) . "', 
            BB_Payment_Status = '" . mysql_real_escape_string($rowListing['BL_Payment_Status']) . "', 
            BB_Account_Manager = '" . mysql_real_escape_string($rowListing['BL_Account_Manager']) . "', 
            BB_SubTotal = '" . mysql_real_escape_string($subtotal) . "',
            BB_CoC_Dis_2 = '" . mysql_real_escape_string($rowListing['BL_CoC_Dis_2']) . "', 
            BB_SubTotal3 = '" . mysql_real_escape_string($subtotal2) . "', 
            BB_Tax = '" . mysql_real_escape_string($tax) . "', 
            BB_Total = '" . mysql_real_escape_string($total) . "',
            BB_Card_Type = '" . mysql_real_escape_string($card_type) . "',
            BB_Transaction_ID = '" . mysql_real_escape_string($payment_id) . "',
            BB_Manual_Payment = 0,
            BB_Card_Number = '" . mysql_real_escape_string($payment_payment_profile['card_number']) . "',
            BB_First_Name = '" . mysql_real_escape_string($payment_payment_profile['first_name']) . "',
            BB_Last_Name = '" . mysql_real_escape_string($payment_payment_profile['last_name']) . "',
            BB_Profile_Id = '" . $payment_payment_profile['profile_id'] . "',
            BB_Payment_Profile = '" . $payment_payment_profile['payment_profile_id'] . "'";
    $result = mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    $bbid = mysql_insert_id();
    $sql = "UPDATE tbl_Business_Billing SET BB_Invoice_Num = '" . mysql_real_escape_string($bbid) . "'
            WHERE BB_ID = '" . mysql_real_escape_string($bbid) . "'";
    mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    $sql_bl = "SELECT ($multiplier * LT_Cost) FROM tbl_Listing_Type WHERE LT_ID = " . $rowListing['BL_Listing_Type']; // listing price
    $listing_price = mysql_result(mysql_query($sql_bl), 0);
    if ($listing_price > 0) {
        $sql = "INSERT tbl_Business_Billing_Lines SET                 
                BBL_BB_ID = '" . mysql_real_escape_string($bbid) . "',                
                BBL_Title = '" . mysql_real_escape_string("Listing Package - " . $rowListing['LT_Name']) . "',                
                BBL_Price = '" . mysql_real_escape_string($listing_price) . "'";
        mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    }
    $sql = "SELECT *         
            FROM tbl_BL_Feature
            LEFT JOIN tbl_Feature ON F_ID = BLF_F_ID
            WHERE BLF_BL_ID = '" . mysql_real_escape_string($rowListing['BL_ID']) . "'";
    $result = mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    while ($row = mysql_fetch_assoc($result)) {
        if ($row['F_Price'] > 0) {
            $sql = "INSERT tbl_Business_Billing_Lines SET
                    BBL_BB_ID = '" . mysql_real_escape_string($bbid) . "', 
                    BBL_Title = '" . mysql_real_escape_string("Listing Tool - " . $row['F_Name']) . "',
                    BBL_Price = '" . mysql_real_escape_string($multiplier * $row['F_Price']) . "'";
            mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
        }
    }
    if ($rowListing['BL_Line_Item_Price'] > 0) {
        $sql = " INSERT tbl_Business_Billing_Lines SET 
                 BBL_BB_ID = '" . mysql_real_escape_string($bbid) . "',
                 BBL_Title = '" . mysql_real_escape_string("Listing Tool - " . $rowListing['BL_Line_Item_Title']) . "',
                 BBL_Price = '" . mysql_real_escape_string($rowListing['BL_Line_Item_Price']) . "'";
        mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    }
    //Advertisements
    $sqlAd = " SELECT * FROM tbl_Advertisement LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID
             WHERE A_BL_ID = '" . mysql_real_escape_string($rowListing['BL_ID']) . "' AND A_Status = 3 AND A_Total > 0
             AND A_Is_Deleted = 0 AND A_Is_Townasset != 1  AND (A_End_Date > CURDATE() OR A_End_Date = '0000-00-00')";
    $resultTMP = mysql_query($sqlAd) or die("Invalid query: $sqlAd -- " . mysql_error());
    while ($row = mysql_fetch_assoc($resultTMP)) {
        $renewalDate = strtotime('-2 month', strtotime($rowListing['BL_Renewal_Date']));
        $renewalDate = date("Y-m-d", $renewalDate);
        if ($renewalDate >= $row['A_Active_Date']) {
            $ad_total = ($multiplier * $row['A_Total']);
        } else {
            $datetime1 = strtotime(date('Y-m-d', strtotime($row['A_Active_Date'])));
            $datetime2 = strtotime('-1 month', strtotime($rowListing['BL_Renewal_Date']));

            $secs = $datetime2 - $datetime1; // == <seconds between the two times>
            $days = $secs / 86400;
            $ad_total = ((($multiplier * $row['A_Total']) / 30) * $days);
        }
        if ($ad_total > 0) {
            $sql = " INSERT tbl_Business_Billing_Lines SET 
                     BBL_BB_ID = '" . mysql_real_escape_string($bbid) . "',
                     BBL_Title = '" . mysql_real_escape_string("Advertisement - " . $row['A_Title']) . "',
                     BBL_Price = '" . mysql_real_escape_string($ad_total) . "'";
            mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
        }
    }
    return $bbid;
}

$date = date('Y-m-d');
$providedDate = date(date('Y') . '-' . date('m') . '-27');
if (date('Y-m-d', strtotime($date)) > date('Y-m-d', strtotime($providedDate))) {
    
} else {
// generating log  
    $log = '';
    $sql = "SELECT bl.*, b.*, blcr.* FROM tbl_Business_Listing bl     
            LEFT JOIN tbl_Business_Listing_Category blc ON blc.BLC_BL_ID = bl.BL_ID     
            INNER JOIN tbl_Business_Listing_Category_Region blcr ON blcr.BLCR_BL_ID = bl.BL_ID     
            LEFT JOIN tbl_Business b ON b.B_ID = bl.BL_B_ID     
            LEFT JOIN tbl_Business_Billing bb ON bb.BB_B_ID = bl.BL_B_ID     
            WHERE bl.BL_Renewal_Date <= CURDATE() AND bl.BL_Renewal_Date != '0000-00-00' AND bl.BL_Payment_Type = 4 AND bl.BL_Listing_Type = 4 AND hide_show_listing = 1 GROUP BY bl.BL_ID";
    $log = $sql . '\n';
    $res = mysql_query($sql);
    while ($bl = mysql_fetch_object($res)) {
        $failed = 0;
        $date = $bl->BL_Renewal_Date;
        $providedDate = date(date('Y') . '-' . date('m') . '-27');
        if (date('Y-m-d', strtotime($date)) > date('Y-m-d', strtotime($providedDate))) {
            //getting 2nd date of next month, if date is greater than 27.
            $renewal = strtotime('+1 month', strtotime($bl->BL_Renewal_Date));
            $renewal = date("Y-m-02", $renewal);
            $sql_update_renewal = " UPDATE tbl_Business_Listing SET BL_Renewal_Date = '" . $renewal . "',
                                BL_Renewal_Date_Last_Update = '' 
                                WHERE BL_ID = " . $bl->BL_ID;
            mysql_query($sql_update_renewal);
        } else {
            //Advertisements
            $sql = " SELECT * FROM tbl_Advertisement LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID
                 WHERE A_BL_ID = " . $bl->BL_ID . " AND A_Status = 3 AND A_Total > 0
                 AND A_Is_Deleted = 0 AND A_Is_Townasset != 1  AND (A_End_Date > CURDATE() OR A_End_Date = '0000-00-00')";
            $resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
            $ads_total = 0;
            if ($bl->BL_Billing_Type == -1) {
                $multiplier = 12;
            } else {
                $multiplier = 1;
            }
            while ($row = mysql_fetch_assoc($resultTMP)) {
                $renewalDate = strtotime('-1 month', strtotime($bl->BL_Renewal_Date));
                $renewalDate = date("Y-m-d", $renewalDate);
                if ($renewalDate >= $row['A_Active_Date']) {
                    $ad_total = ($multiplier * $row['A_Total']);
                    $ads_total += $ad_total;
                } else {
                    $datetime1 = strtotime(date('Y-m-d', strtotime($row['A_Active_Date'])));
                    $datetime2 = strtotime(date('Y-m-d', strtotime($bl->BL_Renewal_Date)));

                    $secs = $datetime2 - $datetime1; // == <seconds between the two times>
                    $days = $secs / 86400;
                    $ad_total = ((($multiplier * $row['A_Total']) / 30) * $days);
                    if ($ad_total < 0) {
                        $ad_total = 0;
                    }
                    $ads_total += $ad_total;
                }
            }
            $ads_total = round($ads_total, 2);
            $subtotal = $bl->BL_SubTotal + $ads_total;
            $subtotal2 = $subtotal - $bl->BL_CoC_Dis_2;
            $tax = round($subtotal2 * .13, 2);
            $listingTotal = $subtotal2 + $tax;
            if ($listingTotal > 0) {
                // get authorize.net information of the specified region
                $sql_profile = "SELECT * FROM payment_profiles WHERE business_id = $bl->B_ID ORDER BY created_time DESC LIMIT 1";
                $log = $sql_profile . '\n';
                $sql_profile_res = mysql_query($sql_profile);
                if (mysql_num_rows($sql_profile_res) > 0) {
                    $profile = mysql_fetch_assoc($sql_profile_res);
                    $sql_region = "SELECT * FROM tbl_Region WHERE R_ID = " . $profile['region'] . " LIMIT 1";
                    $res_region = mysql_query($sql_region);
                    $payment_region = mysql_fetch_assoc($res_region);
                    require_once 'config_payment.php';

                    $request = new AuthorizeNetCIM;
                    // Create Auth & Capture Transaction
                    $transaction = new AuthorizeNetTransaction;
                    $transaction->amount = $listingTotal;
                    $transaction->customerProfileId = $profile['profile_id'];
                    $transaction->customerPaymentProfileId = $profile['payment_profile_id'];

                    $lineItem = new AuthorizeNetLineItem;
                    $lineItem->itemId = $bl->BL_ID;
                    $lineItem->name = "Renewal Fee";
                    $lineItem->description = substr($bl->BL_Listing_Title, 0, 125);
                    $lineItem->quantity = "1";
                    $lineItem->unitPrice = $listingTotal;
                    $lineItem->taxable = "false";
                    $transaction->lineItems[] = $lineItem;
//    $transaction->addLineItem("$bl->BL_ID", 'Renewal Fee', substr($bl->BL_Listing_Title, 0, 125), '1', $bl->BL_Total, 'N');

                    $response = $request->createCustomerProfileTransaction("AuthCapture", $transaction, 'x_duplicate_window=0');
                    $parsedresponse = simplexml_load_string($response->response, "SimpleXMLElement", LIBXML_NOWARNING);
                    $directResponseFields = explode(",", $parsedresponse->directResponse);
                    $responseCode = $directResponseFields[0]; // 1 = Approved 2 = Declined 3 = Error
                    $cardType = $directResponseFields[51]; // Card Type
                    if ($responseCode == 1) {

                        // generate log
                        print "Payment of $" . $listingTotal . " has been processed for " . $bl->BL_Listing_Title . " (BL_ID = " . $bl->BL_ID . ")<hr>";
                        $payment_id = $directResponseFields[6];
                        $status = $responseCode;
                        $payment_date = date('Y-m-d H:i:s');
                        $total = $directResponseFields[9];
                        $payment_method = $directResponseFields[10];
                        $sql = sprintf("INSERT INTO payments (payment_id, status, payment_date, amount, payment_method, business_id, listing_id, card_type) VALUES('%s', '%s', '%s', '%g', '%s', '%d', '%d', '%s')", $payment_id, $status, $payment_date, $total, $payment_method, $bl->B_ID, $bl->BL_ID, $cardType);
                        $log = $sql . '\n';
                        mysql_query($sql);
                        if ($bl->BL_Billing_Type == -1) {
                            $sql_update_renewal = " UPDATE tbl_Business_Listing SET 
                                                BL_Renewal_Date = DATE_ADD('" . $bl->BL_Renewal_Date . "', INTERVAL 1 YEAR), 
                                                BL_Renewal_Date_Last_Update = '',
                                                BL_Payment_Status = 5 WHERE BL_ID = '$bl->BL_ID'";
                        } else {
                            $sql_update_renewal = " UPDATE tbl_Business_Listing SET 
                                                BL_Renewal_Date = DATE_ADD('" . $bl->BL_Renewal_Date . "', INTERVAL 1 MONTH), 
                                                BL_Renewal_Date_Last_Update = '',
                                                BL_Payment_Status = 5 WHERE BL_ID = '$bl->BL_ID'";
                        }
                        $log = $sql_update_renewal . '\n';
                        mysql_query($sql_update_renewal);
                        $bbid = close_order($bl->BL_ID, $cardType, $payment_id);
                        $failed = 0;
                        $file = fopen('json.txt', 'wb');
                        fwrite($file, $log);
                        fclose($file);

                        // send confirmation email
                        ob_start();
                        include '../../include/email-template/payment-confirmation-html.php';
                        $html = ob_get_contents();
                        ob_clean();

                        $mail = new PHPMailer();
                        $mail->From = MAIN_CONTACT_EMAIL;
                        $mail->FromName = MAIN_CONTACT_NAME;
                        $mail->IsHTML(true);
//                        $mail->AddAddress('muhammad.tanweer@app-desk.com');
                        $mail->AddAddress($bl->B_Email);
                        $sql_email_rec = "SELECT * FROM tbl_Email_Recipients where ER_Type=1 and ER_Status=1";
                        $Result_email_rec = mysql_query($sql_email_rec, $db) or die("Invalid query: $sql_email_rec -- " . mysql_error());
                        while ($Row_email_rec = mysql_fetch_assoc($Result_email_rec)) {
                            $mail->AddCC($Row_email_rec['ER_Email']);
                        }
                        $mail->AddReplyTo(MAIN_CONTACT_EMAIL, 'Tourist Town');
                        $mail->Subject = 'Payment confirmation';
                        $mail->MsgHTML($html);
//                        $mail->Send();
                    } else {
                        $encoded = json_encode($response);
                        $file = fopen('authorize.log', 'a');
                        $message = date('Y-m-d H:i:s') . ':  Business Name: ' . $bl->BL_Listing_Title . '. Error: ' . $encoded . PHP_EOL;
                        fwrite($file, $message);
                        fclose($file);
                        $failed = 1;
                        $error_msg = $directResponseFields[3] . PHP_EOL;
                    }
                } else {
                    $failed = 1;
                    $error_msg = "Credit Card information has not been received for this business.";
                }
                if ($failed == 1) {
                    $error_msg_email = "";
                    if (strlen($error_msg) == "1") {
                        $sql_profile_error = "SELECT * FROM payment_profiles WHERE business_id = $bl->B_ID ORDER BY created_time DESC LIMIT 1";
                        $res_error = mysql_query($sql_profile_error);
                        if (mysql_num_rows($res_error) > 0) {
                            $error_msg_email = "Unknown error occur.";
                        } else {
                            $error_msg_email = "Credit Card information has not been received for this business.";
                        }
                    } else {
                        $error_msg_email = $error_msg;
                    }
                    $payment_date = date('Y-m-d H:i:s');
                    $payment_logs = sprintf("INSERT INTO tbl_Payment_Logs (PL_BL_ID, PL_Error_Msg, PL_Response, PL_Date) VALUES('%d', '%s', '%s', '%s')", $bl->BL_ID, $error_msg_email, $encoded, $payment_date);
                    mysql_query($payment_logs);
                    // send transaction email if it fails
                    $BL_ID = $bl->BL_ID;
                    $BL_Title_ID = $bl->BL_Listing_Title;
                    ob_start();
                    include '../../include/email-template/transaction-fail-html.php';
                    $html = ob_get_contents();
                    ob_clean();
                    $Subject_for_mail = str_replace("(Business Name here)", $BL_Title_ID, $rowEmail['ET_Subject']);
                    $mail = new PHPMailer();
                    $mail->From = MAIN_CONTACT_EMAIL;
                    $mail->FromName = MAIN_CONTACT_NAME;
                    $mail->IsHTML(true);
//        $mail->AddAddress('brad@canvasstudios.ca', 'Brad Smith');
//                $mail->AddAddress('sharplogician@gmail.com', 'Danielle Mulasmajic');
                    $mail->AddAddress($bl->B_Email);
                    $sql_email_rec = "SELECT * FROM tbl_Email_Recipients where ER_Type=2 and ER_Status=1";
                    $Result_email_rec = mysql_query($sql_email_rec, $db) or die("Invalid query: $sql_email_rec -- " . mysql_error());
                    while ($Row_email_rec = mysql_fetch_assoc($Result_email_rec)) {
                        $mail->AddCC($Row_email_rec['ER_Email']);
                    }
                    $mail->AddReplyTo(MAIN_CONTACT_EMAIL, 'Tourist Town');
                    $mail->Subject = $Subject_for_mail;
                    $mail->MsgHTML($html);
//                $mail->Send();
                }
            } else {
                if ($bl->BL_Billing_Type == -1) {
                    $sql_update_renewal = " UPDATE tbl_Business_Listing SET 
                                            BL_Renewal_Date = DATE_ADD('" . $bl->BL_Renewal_Date . "',INTERVAL 1 YEAR), 
                                            BL_Renewal_Date_Last_Update = '',
                                            BL_Payment_Status = 5 WHERE BL_ID = '$bl->BL_ID'";
                } else {
                    $sql_update_renewal = " UPDATE tbl_Business_Listing SET 
                                            BL_Renewal_Date = DATE_ADD('" . $bl->BL_Renewal_Date . "', INTERVAL 1 MONTH), 
                                            BL_Renewal_Date_Last_Update = '',
                                            BL_Payment_Status = 5 WHERE BL_ID = '$bl->BL_ID'";
                }
                mysql_query($sql_update_renewal);
            }
        }
    }
}
?>