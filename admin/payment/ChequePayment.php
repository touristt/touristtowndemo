<?php
require_once '../../include/config.inc.php';
require_once '../../include/login.inc.php';
require_once '../../include/PHPMailer/class.phpmailer.php';

function close_order($listing_id) {
  $sql = "SELECT * FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            LEFT JOIN tbl_Listing_Type ON BL_Listing_Type = LT_ID 
            LEFT JOIN tbl_Business_Listing_Category ON BL_ID=BLC_BL_ID
            INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID=BL_ID
            WHERE BL_ID = " . $listing_id . " LIMIT 1";
  $result = mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
  $rowListing = mysql_fetch_assoc($result);

  // see the billing cycle
  if ($rowListing['BL_Billing_Type'] == -1) {
    $multiplier = 12;
  } else {
    $multiplier = 1;
  }
  
  //Advertisements
    $sqlAd = "SELECT * FROM tbl_Advertisement LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID
             WHERE A_BL_ID = '" . mysql_real_escape_string($rowListing['BL_ID']) . "' AND A_Status = 3 AND A_Total > 0
             AND A_Is_Deleted = 0 AND A_Is_Townasset != 1  AND (A_End_Date > CURDATE() OR A_End_Date = '0000-00-00')";
  $resultTMP = mysql_query($sqlAd) or die("Invalid query: $sqlAd -- " . mysql_error());
  $ads_total = 0;
  while ($row = mysql_fetch_assoc($resultTMP)) {
      $renewalDate = strtotime('-2 month', strtotime($rowListing['BL_Renewal_Date']));
      $renewalDate = date("Y-m-d", $renewalDate);
      if ($renewalDate >= $row['A_Active_Date']) {
          $ad_total = ($multiplier * $row['A_Total']);
          $ads_total += $ad_total;
      } else {
          $datetime1 = strtotime(date('Y-m-d', strtotime($row['A_Active_Date'])));
          $datetime2 = strtotime('-1 month', strtotime($rowListing['BL_Renewal_Date']));
          $secs = $datetime2 - $datetime1; // == <seconds between the two times>
          $days = $secs / 86400;
          $ad_total = ((($multiplier * $row['A_Total']) / 30) * $days);
          if ($ad_total < 0) {
              $ad_total = 0;
          }
          $ads_total += $ad_total;
      }
  }

  $subtotal = $rowListing['BL_SubTotal'] + $ads_total;
  $subtotal2 = $subtotal - $rowListing['BL_CoC_Dis_2'];
  $tax = round($subtotal2 * .13, 2);
  $total = $subtotal2 + $tax;
  
    $sql = "INSERT tbl_Business_Billing SET 
            BB_B_ID = '" . mysql_real_escape_string($rowListing['B_ID']) . "', 
            BB_Listing = '" . mysql_real_escape_string($rowListing['BL_Number']) . "', 
            BB_BL_ID = '" . mysql_real_escape_string($rowListing['BL_ID']) . "', 
            BB_R_ID = '" . mysql_real_escape_string($rowListing['BLCR_BLC_R_ID']) . "', 
            BB_Category = '" . mysql_real_escape_string($rowListing['BLC_M_C_ID']) . "', 
            BB_Date = NOW(), 
            BB_Renewal_Date = '" . mysql_real_escape_string($rowListing['BL_Renewal_Date']) . "', 
            BB_Payment_Type = '" . mysql_real_escape_string($rowListing['BL_Payment_Type']) . "', 
            BB_Payment_Status = '" . mysql_real_escape_string($rowListing['BL_Payment_Status']) . "', 
            BB_Account_Manager = '" . mysql_real_escape_string($rowListing['BL_Account_Manager']) . "', 
            BB_SubTotal = '" . mysql_real_escape_string($subtotal) . "',
            BB_CoC_Dis_2 = '" . mysql_real_escape_string($rowListing['BL_CoC_Dis_2']) . "', 
            BB_SubTotal3 = '" . mysql_real_escape_string($subtotal2) . "', 
            BB_Tax = '" . mysql_real_escape_string($tax) . "', 
            BB_Total = '" . mysql_real_escape_string($total) . "'";
  $result = mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
  $bbid = mysql_insert_id();
  $sql = "UPDATE tbl_Business_Billing SET BB_Invoice_Num = '" . mysql_real_escape_string($bbid) . "' 
          WHERE BB_ID = '" . mysql_real_escape_string($bbid) . "'";
  mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
  $sql_bl = "SELECT ($multiplier * LT_Cost) FROM tbl_Listing_Type WHERE LT_ID = " . $rowListing['BL_Listing_Type']; // listing price
  $listing_price = mysql_result(mysql_query($sql_bl), 0);
  if ($listing_price > 0) { 
    $sql = "INSERT tbl_Business_Billing_Lines SET 
              BBL_BB_ID = '" . mysql_real_escape_string($bbid) . "',
              BBL_Title = '" . mysql_real_escape_string("Listing Package - " . $rowListing['LT_Name']) . "',
              BBL_Price = '" . mysql_real_escape_string($listing_price) . "'"; 
    mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
  }
  $sql = "SELECT * 
      FROM tbl_BL_Feature
      LEFT JOIN tbl_Feature ON F_ID = BLF_F_ID
      WHERE BLF_BL_ID = '" . mysql_real_escape_string($rowListing['BL_ID']) . "'";
  $result = mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
  while ($row = mysql_fetch_assoc($result)) {
    if ($row['F_Price'] > 0) { 
      $sql = "INSERT tbl_Business_Billing_Lines SET 
                  BBL_BB_ID = '" . mysql_real_escape_string($bbid) . "',
          BBL_Title = '" . mysql_real_escape_string("Listing Tool - " . $row['F_Name']) . "',
          BBL_Price = '" . mysql_real_escape_string($multiplier * $row['F_Price']) . "'";
      mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    }
  }
  if ($rowListing['BL_Line_Item_Price'] > 0) { 
    $sql = "INSERT tbl_Business_Billing_Lines SET 
                  BBL_BB_ID = '" . mysql_real_escape_string($bbid) . "',
          BBL_Title = '" . mysql_real_escape_string("Listing Tool - " . $rowListing['BL_Line_Item_Title']) . "',
          BBL_Price = '" . mysql_real_escape_string($rowListing['BL_Line_Item_Price']) . "'";
    mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
  }

    //Advertisements
    $sqlAd = " SELECT * FROM tbl_Advertisement LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID
               WHERE A_BL_ID = '" . mysql_real_escape_string($rowListing['BL_ID']) . "' AND A_Status = 3 AND A_Total > 0
               AND A_Is_Deleted = 0 AND A_Is_Townasset != 1  AND (A_End_Date > CURDATE() OR A_End_Date = '0000-00-00')";
    $resultTMP = mysql_query($sqlAd) or die("Invalid query: $sqlAd -- " . mysql_error());
    while ($row = mysql_fetch_assoc($resultTMP)) {
        $renewalDate = strtotime('-2 month', strtotime($rowListing['BL_Renewal_Date']));
        $renewalDate = date("Y-m-d", $renewalDate);
        if ($renewalDate >= $row['A_Active_Date']) {
            $ad_total = ($multiplier * $row['A_Total']);
        } else {
            $datetime1 = strtotime(date('Y-m-d', strtotime($row['A_Active_Date'])));
            $datetime2 = strtotime('-1 month', strtotime($rowListing['BL_Renewal_Date']));

            $secs = $datetime2 - $datetime1; // == <seconds between the two times>
            $days = $secs / 86400;
            $ad_total = ((($multiplier * $row['A_Total']) / 30) * $days);
        }
        if ($ad_total > 0) {
            $sql = " INSERT tbl_Business_Billing_Lines SET 
                     BBL_BB_ID = '" . mysql_real_escape_string($bbid) . "',
                     BBL_Title = '" . mysql_real_escape_string("Advertisement - " . $row['A_Title']) . "',
                     BBL_Price = '" . mysql_real_escape_string($ad_total) . "'";
            mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
        }
    }
  return $bbid;
}

if (isset($_REQUEST['bl_id'])) {
  //business id and listing id
  $listing_id = $_REQUEST['bl_id'];
  $business_id = $_REQUEST['bid'];
  $payment_method = $_REQUEST['payment_method'];
  $sql_listing = "SELECT * FROM tbl_Business_Listing WHERE BL_ID = '$listing_id'";
  $res_listing = mysql_query($sql_listing);
  $rowListing = mysql_fetch_assoc($res_listing);
  $BID = $rowListing['B_ID'];
  $BL_ID = $rowListing['BL_ID'];
}

if (isset($_POST['postback'])) { 
  // ### CreditCard
  // A resource representing a credit card that can be
  // used to fund a payment.
  $status = $_POST['payment_status'];
  $total = $_POST['amount'];
  $business_id = $_POST['business_id'];
  $listing_id = $_POST['listing_id'];
  $billing_type = $_POST['billing_type'];
  $payment_method = $_POST['payment_method'];
  $payment_date = date('Y-m-d H:i:s');

  // ### Create Payment
  $sql = sprintf("INSERT INTO payments (status, payment_date, amount, payment_method, business_id, listing_id) VALUES('%s', '%s', '%g', '%s', '%d', '%d')", $status, $payment_date, $total, $payment_method, $business_id, $listing_id);
  mysql_query($sql);
  if ($billing_type == -1) {
    $sql_update_renewal = "UPDATE tbl_Business_Listing SET BL_Renewal_Date = DATE_ADD(BL_Renewal_Date,INTERVAL 1 YEAR), BL_Payment_Status = 5 WHERE BL_ID = '$listing_id'";
  } else {
    $sql_update_renewal = "UPDATE tbl_Business_Listing SET BL_Renewal_Date = DATE_ADD(BL_Renewal_Date,INTERVAL 1 MONTH), BL_Payment_Status = 5 WHERE BL_ID = '$listing_id'";
  }
  mysql_query($sql_update_renewal);
  $bbid = close_order($listing_id);
  header("Location: ../customer-bill.php?bl_id=" . $listing_id . "&id=" . $bbid);
  exit();
}

$dir = 'payment';
include '../../include/admin/header.php';
?>
<div class="content-left">

  <?php require_once '../../include/top-nav-listing.php'; ?>

  <div class="title-link">
  </div>

  <div class="left">
    <?PHP require '../../include/nav-mypage.php'; ?>
  </div>
  <div class="right">
    <div class="content-header">Payment using <?php echo (($payment_method == 'cheque') ? 'Cheque' : 'E-Transfer'); ?></div>
    <?php if (isset($status)) { ?>
      <pre><?php if ($status == 1) { ?></pre>
        <p>Your Payment has been received. Thank You!</p>
      <?php } else { ?>
        <p>Your transaction failed. Please Try Again.</p>
      <?php } ?>
      <?php
    } else {
      ?>
      <form action="" method="POST">
        <div class="form-inside-div">
          <label>Payment Status: </label>
          <div class="form-data">
            <select name="payment_status" required id="payment_status">
              <option value="1">Approved</option>
              <option value="0">Denied</option>
            </select>
          </div>
        </div>
        <div class="form-inside-div">
          <div class="button">
            <input type="hidden" name="amount" value="<?php echo $rowListing['BL_Total'] ?>" />
            <input type="hidden" name="business_id" value="<?php echo $business_id ?>" />
            <input type="hidden" name="listing_id" value="<?php echo $listing_id ?>" />
            <input type="hidden" name="billing_type" value="<?php echo $rowListing['BL_Billing_Type'] ?>" />
            <input type="hidden" name="payment_method" value="<?php echo $payment_method ?>" />
            <input type="hidden" name="postback" value="1" />
            <input type="submit" value="Confirm Payment" />
          </div>
        </div>
      </form>
    <?php } ?>
  </div>
</div>
<script type="text/javascript" src="jquery-1.9.1.js"></script>
<script type="text/javascript" src="script.js"></script>
<?PHP
include '../../include/admin/footer.php';
?>
