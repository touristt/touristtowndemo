<?php
require_once '../../include/config.inc.php';
require_once '../../include/login.inc.php';
require_once '../../include/PHPMailer/class.phpmailer.php';

function close_order($listing_id, $card_type, $payment_id) {
    $sql = "SELECT * FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            LEFT JOIN tbl_Listing_Type ON BL_Listing_Type = LT_ID 
            LEFT JOIN tbl_Business_Listing_Category ON BL_ID=BLC_BL_ID
            INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID=BL_ID
            WHERE BL_ID = " . $listing_id . " LIMIT 1";
    $result = mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);

    // see the billing cycle
    if ($rowListing['BL_Billing_Type'] == -1) {
        $multiplier = 12;
    } else {
        $multiplier = 1;
    }

    //Advertisements
    $sqlAd = " SELECT * FROM tbl_Advertisement LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID
               WHERE A_BL_ID = '" . mysql_real_escape_string($rowListing['BL_ID']) . "' AND A_Status = 3 AND A_Total > 0
               AND A_Is_Deleted = 0 AND A_Is_Townasset != 1  AND (A_End_Date > CURDATE() OR A_End_Date = '0000-00-00')";
    $resultTMP = mysql_query($sqlAd) or die("Invalid query: $sqlAd -- " . mysql_error());
    $ads_total = 0;
    while ($row = mysql_fetch_assoc($resultTMP)) {
        $renewalDate = strtotime('-2 month', strtotime($rowListing['BL_Renewal_Date']));
        $renewalDate = date("Y-m-d", $renewalDate);
        if ($renewalDate >= $row['A_Active_Date']) {
            $ad_total = ($multiplier * $row['A_Total']);
            $ads_total += $ad_total;
        } else {
            $datetime1 = strtotime(date('Y-m-d', strtotime($row['A_Active_Date'])));
            $datetime2 = strtotime('-1 month', strtotime($rowListing['BL_Renewal_Date']));
            $secs = $datetime2 - $datetime1; // == <seconds between the two times>
            $days = $secs / 86400;
            $ad_total = ((($multiplier * $row['A_Total']) / 30) * $days);
            if ($ad_total < 0) {
                $ad_total = 0;
            }
            $ads_total += $ad_total;
        }
    }

    $subtotal = $rowListing['BL_SubTotal'] + $ads_total;
    $subtotal2 = $subtotal - $rowListing['BL_CoC_Dis_2'];
    $tax = round($subtotal2 * .13, 2);
    $total = $subtotal2 + $tax;

    //get Payment Profile information
    $sql_payment_profile = "SELECT * FROM payment_profiles WHERE business_id = '" . $rowListing['BL_B_ID'] . "'";
    $res_payment_profile = mysql_query($sql_payment_profile);
    $payment_payment_profile = mysql_fetch_assoc($res_payment_profile);

    $sql = " INSERT tbl_Business_Billing SET 
            BB_B_ID = '" . mysql_real_escape_string($rowListing['B_ID']) . "', 
            BB_Listing = '" . mysql_real_escape_string($rowListing['BL_Number']) . "', 
            BB_BL_ID = '" . mysql_real_escape_string($rowListing['BL_ID']) . "', 
            BB_R_ID = '" . mysql_real_escape_string($rowListing['BLCR_BLC_R_ID']) . "', 
            BB_Category = '" . mysql_real_escape_string($rowListing['BLC_M_C_ID']) . "', 
            BB_Date = NOW(),
            BB_Renewal_Date = '" . mysql_real_escape_string($rowListing['BL_Renewal_Date']) . "', 
            BB_Payment_Type = '" . mysql_real_escape_string($rowListing['BL_Payment_Type']) . "', 
            BB_Payment_Status = '" . mysql_real_escape_string($rowListing['BL_Payment_Status']) . "', 
            BB_Account_Manager = '" . mysql_real_escape_string($rowListing['BL_Account_Manager']) . "', 
            BB_SubTotal = '" . mysql_real_escape_string($subtotal) . "',
            BB_CoC_Dis_2 = '" . mysql_real_escape_string($rowListing['BL_CoC_Dis_2']) . "', 
            BB_SubTotal3 = '" . mysql_real_escape_string($subtotal2) . "', 
            BB_Tax = '" . mysql_real_escape_string($tax) . "', 
            BB_Total = '" . mysql_real_escape_string($total) . "',
            BB_Card_Type = '" . mysql_real_escape_string($card_type) . "',
            BB_Transaction_ID = '" . mysql_real_escape_string($payment_id) . "',
            BB_Manual_Payment = 1,
            BB_Card_Number = '" . mysql_real_escape_string($payment_payment_profile['card_number']) . "',
            BB_First_Name = '" . mysql_real_escape_string($payment_payment_profile['first_name']) . "',
            BB_Last_Name = '" . mysql_real_escape_string($payment_payment_profile['last_name']) . "',
            BB_Profile_Id = '" . $payment_payment_profile['profile_id'] . "',
            BB_Payment_Profile = '" . $payment_payment_profile['payment_profile_id'] . "'";
    $result = mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    $bbid = mysql_insert_id();
    $sql = " UPDATE tbl_Business_Billing SET BB_Invoice_Num = '" . mysql_real_escape_string($bbid) . "' 
            WHERE BB_ID = '" . mysql_real_escape_string($bbid) . "'";
    mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    $sql_bl = "SELECT ($multiplier * LT_Cost) FROM tbl_Listing_Type WHERE LT_ID = " . $rowListing['BL_Listing_Type']; // listing price
    $listing_price = mysql_result(mysql_query($sql_bl), 0);
    if ($listing_price > 0) {
        $sql = " INSERT tbl_Business_Billing_Lines SET 
                BBL_BB_ID = '" . mysql_real_escape_string($bbid) . "',
                BBL_Title = '" . mysql_real_escape_string("Listing Package - " . $rowListing['LT_Name']) . "',
                BBL_Price = '" . mysql_real_escape_string($listing_price) . "'";
        mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    }
    $sql = " SELECT * 
            FROM tbl_BL_Feature
            LEFT JOIN tbl_Feature ON F_ID = BLF_F_ID
            WHERE BLF_BL_ID = '" . mysql_real_escape_string($rowListing['BL_ID']) . "'";
    $result = mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    while ($row = mysql_fetch_assoc($result)) {
        if ($row['F_Price'] > 0) {
            $sql = " INSERT tbl_Business_Billing_Lines SET 
                    BBL_BB_ID = '" . mysql_real_escape_string($bbid) . "',
                    BBL_Title = '" . mysql_real_escape_string("Listing Tool - " . $row['F_Name']) . "',
                    BBL_Price = '" . mysql_real_escape_string($multiplier * $row['F_Price']) . "'";
            mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
        }
    }
    if ($rowListing['BL_Line_Item_Price'] > 0) {
        $sql = " INSERT tbl_Business_Billing_Lines SET 
                BBL_BB_ID = '" . mysql_real_escape_string($bbid) . "',
                BBL_Title = '" . mysql_real_escape_string("Listing Tool - " . $rowListing['BL_Line_Item_Title']) . "',
                BBL_Price = '" . mysql_real_escape_string($rowListing['BL_Line_Item_Price']) . "'";
        mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    }

    //Advertisements
    $sqlAd = " SELECT * FROM tbl_Advertisement LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID
               WHERE A_BL_ID = '" . mysql_real_escape_string($rowListing['BL_ID']) . "' AND A_Status = 3 AND A_Total > 0
               AND A_Is_Deleted = 0 AND A_Is_Townasset != 1  AND (A_End_Date > CURDATE() OR A_End_Date = '0000-00-00')";
    $resultTMP = mysql_query($sqlAd) or die("Invalid query: $sqlAd -- " . mysql_error());
    while ($row = mysql_fetch_assoc($resultTMP)) {
        $renewalDate = strtotime('-2 month', strtotime($rowListing['BL_Renewal_Date']));
        $renewalDate = date("Y-m-d", $renewalDate);
        if ($renewalDate >= $row['A_Active_Date']) {
            $ad_total = ($multiplier * $row['A_Total']);
        } else {
            $datetime1 = strtotime(date('Y-m-d', strtotime($row['A_Active_Date'])));
            $datetime2 = strtotime('-1 month', strtotime($rowListing['BL_Renewal_Date']));

            $secs = $datetime2 - $datetime1; // == <seconds between the two times>
            $days = $secs / 86400;
            $ad_total = ((($multiplier * $row['A_Total']) / 30) * $days);
        }
        if ($ad_total > 0) {
            $sql = " INSERT tbl_Business_Billing_Lines SET 
                     BBL_BB_ID = '" . mysql_real_escape_string($bbid) . "',
                     BBL_Title = '" . mysql_real_escape_string("Advertisement - " . $row['A_Title']) . "',
                     BBL_Price = '" . mysql_real_escape_string($ad_total) . "'";
            mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
        }
    }
    return $bbid;
}

if (isset($_REQUEST['bl_id'])) {
    //business id and listing id
    $listing_id = $_REQUEST['bl_id'];
    $business_id = $_REQUEST['bid'];
    $sql = " SELECT * FROM tbl_Business_Listing
            LEFT JOIN tbl_Business ON BL_B_ID = B_ID 
            LEFT JOIN tbl_Listing_Type ON BL_Listing_Type = LT_ID 
            LEFT JOIN tbl_Business_Listing_Category ON BLC_BL_ID = BL_ID 
            INNER JOIN tbl_Business_Listing_Category_Region ON BLCR_BL_ID = BL_ID 
            LEFT JOIN tbl_Category ON BL_C_ID = C_ID 
            WHERE BL_ID = " . $listing_id . " LIMIT 1";
    $result = mysql_query($sql) or die("Invalid query: $sql -- " . mysql_error());
    $rowListing = mysql_fetch_assoc($result);
    $BID = $rowListing['B_ID'];
    $BL_ID = $rowListing['BL_ID'];
}

$flag = 0;
$date = $rowListing['BL_Renewal_Date'];
$providedDate = date(date('Y') . '-' . date('m') . '-27');
if (date('Y-m-d', strtotime($date)) > date('Y-m-d', strtotime($providedDate))) {
    //getting 2nd date of next month, if date is greater than 27.
    $renewal = strtotime('+1 month', strtotime($date));
    $renewal = date("Y-m-02", $renewal);
    $flag = 0;
} else {
    $flag = 1;
}


//Advertisements
$sql = " SELECT * FROM tbl_Advertisement LEFT JOIN tbl_Advertisement_Type ON A_AT_ID = AT_ID
         WHERE A_BL_ID = " . $rowListing['BL_ID'] . " AND A_Status = 3 AND A_Total > 0
         AND A_Is_Deleted = 0 AND A_Is_Townasset != 1  AND (A_End_Date > CURDATE() OR A_End_Date = '0000-00-00')";
$resultTMP = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$ads_total = 0;
if ($rowListing['BL_Billing_Type'] == -1) {
    $multiplier = 12;
} else {
    $multiplier = 1;
}
while ($row = mysql_fetch_assoc($resultTMP)) {
    $renewalDate = strtotime('-1 month', strtotime($rowListing['BL_Renewal_Date']));
    $renewalDate = date("Y-m-d", $renewalDate);
    if ($renewalDate >= $row['A_Active_Date']) {
        $ad_total = ($multiplier * $row['A_Total']);
        $ads_total += $ad_total;
    } else {
        $datetime1 = strtotime(date('Y-m-d', strtotime($row['A_Active_Date'])));
        $datetime2 = strtotime(date('Y-m-d', strtotime($rowListing['BL_Renewal_Date'])));

        $secs = $datetime2 - $datetime1; // == <seconds between the two times>
        $days = $secs / 86400;
        $ad_total = ((($multiplier * $row['A_Total']) / 30) * $days);
        if ($ad_total < 0) {
            $ad_total = 0;
        }
        $ads_total += $ad_total;
    }
}
$ads_total = round($ads_total, 2);
$subtotal = $rowListing['BL_SubTotal'] + $ads_total;
$subtotal2 = $subtotal - $rowListing['BL_CoC_Dis_2'];
$tax = round($subtotal2 * .13, 2);
$listingTotal = $subtotal2 + $tax;
if ($listingTotal > 0) {
    
} else {
    if ($flag == 1) {
        if ($rowListing['BL_Billing_Type'] == -1) {
            $sql_update_renewal = " UPDATE tbl_Business_Listing SET 
                                    BL_Renewal_Date = DATE_ADD('" . $rowListing['BL_Renewal_Date'] . "', INTERVAL 1 YEAR), 
                                    BL_Payment_Status = 5 WHERE BL_ID = " . $rowListing['BL_ID'];
        } else {
            $sql_update_renewal = " UPDATE tbl_Business_Listing SET 
                                    BL_Renewal_Date = DATE_ADD('" . $rowListing['BL_Renewal_Date'] . "',INTERVAL 1 MONTH), 
                                    BL_Payment_Status = 5 WHERE BL_ID = " . $rowListing['BL_ID'];
        }
        mysql_query($sql_update_renewal);
        $_SESSION['transaction_success'] = 1;
        header("Location: ../customer-listing-billing.php?bl_id=" . $listing_id);
        exit();
    } else {
        $sql_update_renewal = " UPDATE tbl_Business_Listing SET BL_Renewal_Date = '" . $renewal . "' WHERE BL_ID = " . $rowListing['BL_ID'];
        $result = mysql_query($sql_update_renewal);
        if ($result) {
            $_SESSION['success'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
        header("Location: ../customer-listing-billing.php?bl_id=" . $listing_id);
        exit();
    }
}

//If clicked from billing section and card information is available
//If existing profile used
if (isset($_REQUEST['bil_sect']) && $_REQUEST['bil_sect'] == 1) {
    // if existing profile being used
    $sql_profile = "SELECT * FROM payment_profiles WHERE business_id = $business_id ORDER BY created_time DESC LIMIT 1";
    $profile = mysql_fetch_assoc(mysql_query($sql_profile));

    // get authorize.net information of the specified region
    $sql_region = "SELECT * FROM tbl_Region WHERE R_ID = " . $profile['region'] . " LIMIT 1";
    $res_region = mysql_query($sql_region);
    $payment_region = mysql_fetch_assoc($res_region);
    require_once 'config_payment.php';
    if ($flag == 1) {
        $request = new AuthorizeNetCIM;
        // Create Auth & Capture Transaction
        $transaction = new AuthorizeNetTransaction;
        $transaction->amount = $listingTotal;
        $transaction->customerProfileId = $profile['profile_id'];
        $transaction->customerPaymentProfileId = $profile['payment_profile_id'];

        $lineItem = new AuthorizeNetLineItem;
        $lineItem->itemId = $listing_id;
        $lineItem->name = "Renewal Fee";
        $lineItem->description = $rowListing['BL_Listing_Title'];
        $lineItem->quantity = "1";
        $lineItem->unitPrice = $listingTotal;
        $lineItem->taxable = "false";
        $transaction->lineItems[] = $lineItem;

        $response = $request->createCustomerProfileTransaction("AuthCapture", $transaction, 'x_duplicate_window=0');
        $parsedresponse = simplexml_load_string($response->response, "SimpleXMLElement", LIBXML_NOWARNING);
        $directResponseFields = explode(",", $parsedresponse->directResponse);
        $responseCode = $directResponseFields[0]; // 1 = Approved 2 = Declined 3 = Error
        $cardType = $directResponseFields[51]; // Card Type

        if ($responseCode == 1) {
            $payment_id = $directResponseFields[6];
            $status = $responseCode;
            $payment_date = date('Y-m-d H:i:s');
            $total = $directResponseFields[9];
            $payment_method = $directResponseFields[10];
            $sql = sprintf("INSERT INTO payments (payment_id, status, payment_date, amount, payment_method, business_id, listing_id, card_type) VALUES('%s', '%s', '%s', '%g', '%s', '%d', '%d', '%s')", $payment_id, $status, $payment_date, $total, $payment_method, $business_id, $listing_id, $cardType);
            mysql_query($sql);
            $sql_update_payment = "UPDATE tbl_Business_Listing SET BL_Payment_Status = 5 WHERE BL_ID = '$listing_id'";
            mysql_query($sql_update_payment);
            if ($rowListing['BL_Billing_Type'] == -1) {
                $sql_update_renewal = " UPDATE tbl_Business_Listing SET 
                                        BL_Renewal_Date = DATE_ADD('" . $rowListing['BL_Renewal_Date'] . "', INTERVAL 1 YEAR), 
                                        BL_Payment_Status = 5 WHERE BL_ID = '$listing_id'";
            } else {
                $sql_update_renewal = " UPDATE tbl_Business_Listing SET 
                                        BL_Renewal_Date = DATE_ADD('" . $rowListing['BL_Renewal_Date'] . "', INTERVAL 1 MONTH), 
                                        BL_Payment_Status = 5 WHERE BL_ID = '$listing_id'";
            }
            mysql_query($sql_update_renewal);
            $bbid = close_order($listing_id, $cardType, $payment_id);

            // send confirmation email
            ob_start();
            include '../../include/email-template/payment-confirmation-html.php';
            $html = ob_get_contents();
            ob_clean();
            $_SESSION['transaction_success'] = 1;
            $mail = new PHPMailer();
            $mail->From = MAIN_CONTACT_EMAIL;
            $mail->FromName = MAIN_CONTACT_NAME;
            $mail->IsHTML(true);
            $mail->AddAddress($rowListing['B_Email']);
            //PAYMENT RECEIPTIONS EMAILS
            $sql_email_rec = "SELECT * FROM tbl_Email_Recipients where ER_Type=3 and ER_Status=1";
            $Result_email_rec = mysql_query($sql_email_rec, $db) or die("Invalid query: $sql_email_rec -- " . mysql_error());
            while ($Row_email_rec = mysql_fetch_assoc($Result_email_rec)) {
                $mail->AddCC($Row_email_rec['ER_Email']);
            }
            $mail->AddReplyTo(MAIN_CONTACT_EMAIL, 'Tourist Town');
            $mail->Subject = 'Payment confirmation';
            $mail->MsgHTML($html);
//            $mail->Send();
            header("Location: ../customer-bill.php?bl_id=" . $listing_id . "&id=" . $bbid);
            exit();
        } else {
            $file = fopen('authorize.log', 'a');
            $encoded = json_encode($response);
            $message = date('Y-m-d H:i:s') . ':  Business Name: ' . $rowListing['BL_Listing_Title'] . '. Error: ' . $encoded . PHP_EOL;
            fwrite($file, $message);
            fclose($file);
            $_SESSION['transaction_error'] = 1;
            $_SESSION['transaction_error_msg'] = $directResponseFields[3];

            // send transaction email if it fails
            $error_msg_email = "";
            if (strlen($directResponseFields[3]) == "1" || $directResponseFields[3] == "") {
                $sql_profile = "SELECT * FROM payment_profiles WHERE business_id = $business_id ORDER BY created_time DESC LIMIT 1";
                $res = mysql_query($sql_profile);
                if (mysql_num_rows($res) > 0) {
                    $error_msg_email = "Unknown error occur.";
                } else {
                    $error_msg_email = "Credit Card information has not been received for this business.";
                }
            } else {
                $error_msg_email = $directResponseFields[3];
            }
            $payment_date = date('Y-m-d H:i:s');
            $payment_logs = sprintf("INSERT INTO tbl_Payment_Logs (PL_BL_ID, PL_Error_Msg, PL_Response, PL_Date) VALUES('%d', '%s', '%s', '%s')", $listing_id, $error_msg_email, $encoded, $payment_date);
            mysql_query($payment_logs);
            $BL_ID = $listing_id;
            ob_start();
            include '../../include/email-template/transaction-fail-html.php';
            $html = ob_get_contents();
            ob_clean();
            $Subject_for_mail = str_replace("(Business Name here)", $rowListing['BL_Listing_Title'], $rowEmail['ET_Subject']);
            $mail = new PHPMailer();
            $mail->From = MAIN_CONTACT_EMAIL;
            $mail->FromName = MAIN_CONTACT_NAME;
            $mail->IsHTML(true);
            $mail->AddAddress($rowListing['B_Email']);
            // Failed Receiptions email
            $sql_email_rec = "SELECT * FROM tbl_Email_Recipients where ER_Type=2 and ER_Status=1";
            $Result_email_rec = mysql_query($sql_email_rec, $db) or die("Invalid query: $sql_email_rec -- " . mysql_error());
            while ($Row_email_rec = mysql_fetch_assoc($Result_email_rec)) {
                $mail->AddCC($Row_email_rec['ER_Email']);
            }
            $mail->AddReplyTo(MAIN_CONTACT_EMAIL, 'Tourist Town');
            $mail->Subject = $Subject_for_mail;
            $mail->MsgHTML($html);
//            $mail->Send();
            header("Location: CreatePayment.php?bl_id=" . $listing_id . "&bid=" . $business_id);
            exit();
        }
    } else {
        $sql_update_renewal = "UPDATE tbl_Business_Listing SET BL_Renewal_Date = '" . $renewal . "' WHERE BL_ID = '$listing_id'";
        $result = mysql_query($sql_update_renewal);
        if ($result) {
            $_SESSION['success'] = 1;
        } else {
            $_SESSION['error'] = 1;
        }
        header("Location: ../customer-listing-billing.php?bl_id=" . $listing_id);
        exit();
    }
}

//If new profile will create it automatically proceed with transaction.
if (isset($_POST['postback'])) {
    // ### Process payment
    $card_number = $_POST['card_number'];
    $exp_date = $_POST['exp_month'] . '/' . $_POST['exp_year'];
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $email = $_POST['email'];
    $item_name = $_POST['item_name'];
    $total_amount = $_POST['amount'];
    $business_id = $_POST['business_id'];
    $billing_type = $_POST['billing_type'];
    $region = $_POST['region'];
    $address = str_replace(',', '', $_POST['address']);
    $zip = $_POST['zip'];
    // get authorize.net information of the specified region
    $sql_region = "SELECT * FROM tbl_Region WHERE R_ID = $region LIMIT 1";
    $res_region = mysql_query($sql_region);
    $payment_region = mysql_fetch_assoc($res_region);
    require_once 'config_payment.php';
    // Create new customer profile
    $request = new AuthorizeNetCIM;
    $customerProfile = new AuthorizeNetCustomer;
    $customerProfile->description = $item_name;
    $customerProfile->merchantCustomerId = $business_id . time();
    $customerProfile->email = $email;
    // Add payment profile.
    $paymentProfile = new AuthorizeNetPaymentProfile;
    $paymentProfile->customerType = "individual";
    $paymentProfile->payment->creditCard->cardNumber = $card_number;
    $paymentProfile->payment->creditCard->expirationDate = $exp_date;
    $paymentProfile->billTo->firstName = $first_name;
    $paymentProfile->billTo->lastName = $last_name;
    $paymentProfile->billTo->address = $address;
    $paymentProfile->billTo->zip = $zip;
    $customerProfile->paymentProfiles[] = $paymentProfile;
    $profile_response = $request->createCustomerProfile($customerProfile, 'testMode');
    $parsedresponse = simplexml_load_string($profile_response->response, "SimpleXMLElement", LIBXML_NOWARNING);
    $profile_id = $parsedresponse->customerProfileId;
    $validationResponses = $profile_response->getValidationResponses();
    foreach ($validationResponses as $vr) {
        $validate = $vr->approved;
    }
    //save profile if created
    if ($profile_id != '' && $validate == 1) {
        $customer_profile = $request->getCustomerProfile($profile_id);
        $parsedprofile = simplexml_load_string($customer_profile->response, "SimpleXMLElement", LIBXML_NOWARNING);
        $profile_object = $parsedprofile->profile;
        $payment_profile_id = $profile_object->paymentProfiles->customerPaymentProfileId;
        $card_num = $profile_object->paymentProfiles->payment->creditCard->cardNumber;
        $expiry_date = $profile_object->paymentProfiles->payment->creditCard->expirationDate;
        $profile_email = $profile_object->email;
        $created_date = date('Y-m-d H:i:s');
        // delete existing profiles for this listing
        $sql_delete = sprintf("DELETE FROM payment_profiles WHERE business_id > 0 AND business_id = '%d'", $business_id);
        mysql_query($sql_delete);
        // insert the newly created profile
        $lastdate = date('t', strtotime($_POST['exp_year'] . '-' . $_POST['exp_month'] . '-01'));
        $expiration_card = $_POST['exp_year'] . '-' . $_POST['exp_month'] . '-' . $lastdate;
        $sql_profile = sprintf("INSERT INTO payment_profiles (profile_id, payment_profile_id, email, card_number, expiry_date, first_name, last_name, created_time, card_expiration, business_id, region, address, zip) VALUES('%d', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%d', '%s', '%s')", $profile_id, $payment_profile_id, $profile_email, $card_num, $expiry_date, mysql_real_escape_string($first_name), mysql_real_escape_string($last_name), $created_date, $expiration_card, $business_id, $region, mysql_real_escape_string($address), $zip);
        $loacl_profile_created = mysql_query($sql_profile);
        if ($loacl_profile_created) {
            if ($flag == 1) {
                $transaction = new AuthorizeNetAIM;
                $transaction->setSandbox(AUTHORIZENET_SANDBOX);
                $transaction->setFields(
                        array(
                            'amount' => $total_amount,
                            'card_num' => $card_number,
                            'exp_date' => $exp_date,
                            'first_name' => $first_name,
                            'last_name' => $last_name,
                            'email' => $email
                        )
                );
                $transaction->addLineItem("$listing_id", 'Renewal Fee', $item_name, '1', $total_amount, 'N');
                $response = $transaction->authorizeAndCapture();
                if ($response->approved) {
                    $payment_id = $response->transaction_id;
                    $status = ($response->approved) ? 1 : 0;
                    $payment_date = date('Y-m-d H:i:s');
                    $total = $response->amount;
                    $payment_method = $response->method;
                    $cardType = $response->card_type; // Card Type
                    $sql = sprintf("INSERT INTO payments (payment_id, status, payment_date, amount, payment_method, business_id, listing_id, card_type) VALUES('%s', '%s', '%s', '%g', '%s', '%d', '%d', '%s')", $payment_id, $status, $payment_date, $total, $payment_method, $business_id, $listing_id, $cardType);
                    mysql_query($sql);
                    if ($billing_type == -1) {
                        $sql_update_renewal = " UPDATE tbl_Business_Listing SET 
                                                BL_Renewal_Date = DATE_ADD('" . $rowListing['BL_Renewal_Date'] . "',INTERVAL 1 YEAR), 
                                                BL_Payment_Status = 5 WHERE BL_ID = '$listing_id'";
                    } else {
                        $sql_update_renewal = " UPDATE tbl_Business_Listing SET 
                                                BL_Renewal_Date = DATE_ADD('" . $rowListing['BL_Renewal_Date'] . "', INTERVAL 1 MONTH), 
                                                BL_Payment_Status = 5 WHERE BL_ID = '$listing_id'";
                    }
                    mysql_query($sql_update_renewal);
                    $bbid = close_order($listing_id, $cardType, $payment_id);
                    // send confirmation email
                    ob_start();
                    include '../../include/email-template/payment-confirmation-html.php';
                    $html = ob_get_contents();
                    ob_clean();
                    $_SESSION['transaction_success'] = 1;
                    $mail = new PHPMailer();
                    $mail->From = MAIN_CONTACT_EMAIL;
                    $mail->FromName = MAIN_CONTACT_NAME;
                    $mail->IsHTML(true);
                    $mail->AddAddress($email);
//PAYMENT RECEIPTIONS EMAILS
                    $sql_email_rec = "SELECT * FROM tbl_Email_Recipients where ER_Type=3 and ER_Status=1";
                    $Result_email_rec = mysql_query($sql_email_rec, $db) or die("Invalid query: $sql_email_rec -- " . mysql_error());
                    while ($Row_email_rec = mysql_fetch_assoc($Result_email_rec)) {
                        $mail->AddCC($Row_email_rec['ER_Email']);
                    }
                    $mail->AddReplyTo(MAIN_CONTACT_EMAIL, 'Tourist Town');
                    $mail->Subject = 'Payment confirmation';
                    $mail->MsgHTML($html);
//                    $mail->Send();
                    header("Location: ../customer-bill.php?bl_id=" . $listing_id . "&id=" . $bbid);
                    exit();
                } else {
                    $file = fopen('authorize.log', 'a');
                    $encoded = json_encode($response);
                    $message = date('Y-m-d H:i:s') . ':  Business Name: ' . $item_name . '. Error: ' . $encoded . PHP_EOL;
                    fwrite($file, $message);
                    fclose($file);
                    $_SESSION['transaction_error'] = 1;
                    $_SESSION['transaction_error_msg'] = $response->response_reason_text;

                    // send transaction email if it fails
                    $error_msg_email = "";
                    if (strlen($response->response_reason_text) == "1" || $response->response_reason_text == "") {
                        $sql_profile = "SELECT * FROM payment_profiles WHERE business_id = $business_id ORDER BY created_time DESC LIMIT 1";
                        $res = mysql_query($sql_profile);
                        if (mysql_num_rows($res) > 0) {
                            $error_msg_email = "Unknown error occur.";
                        } else {
                            $error_msg_email = "Credit Card information has not been received for this business.";
                        }
                    } else {
                        $error_msg_email = $response->response_reason_text;
                    }
                    $payment_date = date('Y-m-d H:i:s');
                    $payment_logs = sprintf("INSERT INTO tbl_Payment_Logs (PL_BL_ID, PL_Error_Msg, PL_Response, PL_Date) VALUES('%d', '%s', '%s', '%s')", $listing_id, $error_msg_email, $encoded, $payment_date);
                    mysql_query($payment_logs);
                    $BL_ID = $listing_id;
                    ob_start();
                    include '../../include/email-template/transaction-fail-html.php';
                    $html = ob_get_contents();
                    ob_clean();
                    $Subject_for_mail = str_replace("(Business Name here)", $item_name, $rowEmail['ET_Subject']);
                    $mail = new PHPMailer();
                    $mail->From = MAIN_CONTACT_EMAIL;
                    $mail->FromName = MAIN_CONTACT_NAME;
                    $mail->IsHTML(true);
                    $mail->AddAddress($email);
// Failed Receiptions email
                    $sql_email_rec = "SELECT * FROM tbl_Email_Recipients where ER_Type=2 and ER_Status=1";
                    $Result_email_rec = mysql_query($sql_email_rec, $db) or die("Invalid query: $sql_email_rec -- " . mysql_error());
                    while ($Row_email_rec = mysql_fetch_assoc($Result_email_rec)) {
                        $mail->AddCC($Row_email_rec['ER_Email']);
                    }
                    $mail->AddReplyTo(MAIN_CONTACT_EMAIL, 'Tourist Town');
                    $mail->Subject = $Subject_for_mail;
                    $mail->MsgHTML($html);
//                    $mail->Send();
                    header("Location: CreatePayment.php?bl_id=" . $listing_id . "&bid=" . $business_id);
                    exit();
                }
            } else {
                $sql_update_renewal = "UPDATE tbl_Business_Listing SET BL_Renewal_Date = '" . $renewal . "' WHERE BL_ID = '$listing_id'";
                $result = mysql_query($sql_update_renewal);
                if ($result) {
                    $_SESSION['success'] = 1;
                } else {
                    $_SESSION['error'] = 1;
                }
                header("Location: ../customer-listing-billing.php?bl_id=" . $listing_id);
                exit();
            }
        } else {
            $file = fopen('authorize.log', 'a');
            $message = date('Y-m-d H:i:s') . ':  Creating profile: CreatePayment.php Response from local sql query: ' . $sql_profile . PHP_EOL;
            fwrite($file, $message);
            fclose($file);
            $_SESSION['local_error'] = 2;
            header("Location: CreatePayment.php?bl_id=" . $listing_id . "&bid=" . $business_id);
            exit();
        }
    } else {
        $file = fopen('authorize.log', 'a');
        $encode_response = json_encode($profile_response);
        $message = date('Y-m-d H:i:s') . ': Creating profile: CreatePayment.php Response from authorize.net: ' . $encode_response . PHP_EOL;
        fwrite($file, $message);
        fclose($file);
        $_SESSION['profile_error'] = 1;
        header("Location: CreatePayment.php?bl_id=" . $listing_id . "&bid=" . $business_id);
        exit();
    }
}

$dir = 'payment';
require_once '../../include/admin/header.php';
?>

<div class="content-left">

    <?php require_once '../../include/top-nav-listing.php'; ?>

    <div class="title-link">
        <div class="title">My Page</div>
    </div>

    <div class="left">
        <?PHP require '../../include/nav-mypage.php'; ?>
    </div>
    <div class="right">
        <div class="content-header">Payment using Credit Card</div>
        <?php
        $sql_card = "SELECT card_number, DATE(created_time) AS created_date FROM payment_profiles WHERE business_id = '$business_id' ORDER BY created_time DESC LIMIT 1";
        $res_card = mysql_query($sql_card);
        $card = mysql_fetch_assoc($res_card);
        if ($card['card_number']) {
            $cardNo = explode("XXXX", $card['card_number']);
            $cardNo = "#### #### #### " . $cardNo[1];
            ?>
            <div class="form-inside-div">
                <label>Card on File</label>
                <div class="form-data"><p><?= $cardNo ?></p></div>
            </div>
            <div class="form-inside-div">
                <label>Created Date</label>
                <div class="form-data"><p><?= $card['created_date'] ?></p></div>
            </div>
            <?php
        }
        ?>
        <form action="" method="POST">
            <div id="new_card">
                <div class="form-inside-div">
                    <label>Card Number: </label>
                    <div class="form-data">
                        <input type="text" placeholder="Enter Card Number" name="card_number" id="card_number" required />
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>First Name: </label>
                    <div class="form-data">
                        <input type="text" placeholder="Enter First Name" name="first_name" id="first_name" required />
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>Last Name: </label>
                    <div class="form-data">
                        <input type="text" placeholder="Enter Last Name" name="last_name" id="last_name" required />
                    </div>
                </div>
                <div class="form-inside-div">
                    <label>Expiration Date: </label>
                    <div class="form-data">
                        <select name="exp_month" id="exp_month" required>
                            <option value="">Month</option>
                            <option value="01">01</option>
                            <option value="02">02</option>
                            <option value="03">03</option>
                            <option value="04">04</option>
                            <option value="05">05</option>
                            <option value="06">06</option>
                            <option value="07">07</option>
                            <option value="08">08</option>
                            <option value="09">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>
                        <select name="exp_year" id="exp_year" required>
                            <option value="">Year</option>
                            <?php
                            $startingYear = date('Y');
                            for ($index = 0; $index <= 5; $index++) {
                                echo ("<option value=" . $startingYear . ">" . $startingYear . "</option>");
                                $startingYear ++;
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <input type="hidden" name="email" id="email" value="<?php echo $rowListing['B_Email'] ?>" />
                <div class="form-inside-div">
                    <label>Address</label>
                    <div class="form-data">
                        <input type="text" placeholder="Enter Address" name="address" required />
                    </div>
                </div>

                <div class="form-inside-div">
                    <label>Postal Code</label>
                    <div class="form-data">
                        <input type="text" placeholder="Enter Postal Code" name="zip" required />
                    </div>
                </div>

                <div class="form-inside-div">
                    <label>Region</label>
                    <div class="form-data">
                        <select name="region" required>
                            <option value="">Select Region</option>
                            <?php
                            $sql = "SELECT * FROM tbl_Region WHERE R_API_Login_ID != '' AND R_Transaction_Key != ''";
                            $result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
                            while ($region = mysql_fetch_assoc($result)) {
                                print '<option value="' . $region['R_ID'] . '" >' . $region['R_Name'] . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-inside-div">
                <div class="button">
                    <input type="hidden" name="email" value="<?php echo $rowListing['B_Email'] ?>" />
                    <input type="hidden" name="item_name" value="<?php echo substr($rowListing['BL_Listing_Title'], 0, 125); ?>" />
                    <input type="hidden" name="amount" value="<?php echo $listingTotal ?>" />
                    <input type="hidden" name="business_id" value="<?php echo $business_id ?>" />
                    <input type="hidden" name="bl_id" value="<?php echo $listing_id ?>" />
                    <input type="hidden" name="billing_type" value="<?php echo $rowListing['BL_Billing_Type'] ?>" />
                    <input type="hidden" name="postback" value="1" />
                    <input type="submit" value="Pay Now" />
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript" src="jquery-1.9.1.js"></script>
<?PHP
if ($_SESSION['WARNING_BILLING'] == 1) {
    print '<script>swal("Credit Card Info", "Please provide credit card info before buying!.", "warning");</script>';
    unset($_SESSION['WARNING_BILLING']);
}
if ($_SESSION['transaction_error'] == 1) {
    if ($_SESSION['transaction_error_msg'] != "" || strlen($_SESSION['transaction_error_msg']) == "1") {
        echo '<script>
            swal({
                    title: "Transaction error",   
                    text: "Payment has not received due to: ' . $_SESSION['transaction_error_msg'] . '",   
                    type: "error",
                    html: true
                })
          </script>';
    } else {
        print '<script>swal("Transaction error", "Payment has not received due to: Unkown error.", "error");</script>';
    }
    unset($_SESSION['transaction_error_msg']);
    unset($_SESSION['transaction_error']);
}
if ($_SESSION['profile_error'] == 1) {
    print '<script>swal("Error", "We were unable to process your credit card. Please carefully review your credit card information and try again.", "error");</script>';
    unset($_SESSION['profile_error']);
}
if ($_SESSION['local_error'] == 2) {
    print '<script>swal("Error", "Error saving data. Please try again.", "error");</script>';
    unset($_SESSION['local_error']);
}
require_once '../../include/admin/footer.php';
?>
