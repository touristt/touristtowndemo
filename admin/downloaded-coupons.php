<?PHP
require_once '../include/config.inc.php';
require_once '../include/login.inc.php';
$regionLimit_id = '';
if (!in_array('customers', $_SESSION['USER_PERMISSIONS'])) {
    header("Location: /admin/");
    exit();
}
require_once '../include/admin/header.php';
$bfc_id = $_REQUEST['bfc_id'];
$sql = "SELECT BL_ID, BFC_ID, BFC_Title, BFC_Expiry_Date, YEAR(BFC_Expiry_Date) AS Expiry_Year, MONTH(BFC_Expiry_Date) AS Expiry_Month, BL_Listing_Title FROM tbl_Business_Feature_Coupon
        LEFT JOIN tbl_Business_Listing ON BFC_BL_ID = BL_ID
        WHERE BFC_ID = '" . $bfc_id . "'";
$result = mysql_query($sql, $db) or die("Invalid query: $sql -- " . mysql_error());
$row = mysql_fetch_assoc($result);
?>
<div class="content-left full-width">
    <div class="title-link">
        <div class="title">Coupon - <?php echo $row['BFC_Title']; ?></div>
    </div>
    <div class="left">
        <?PHP require '../include/nav-manage-coupon.php'; ?>
    </div>
    <div class="right">
        <div class="menu-items-accodings">
            <div id="accordion">
                <?php
                $years = array();
                $months = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
                $expiry_year = $row['Expiry_Year'];
                $expiry_month = $row['Expiry_Month'];
                $month_current = date('m');
                $year_current = date('Y');
                if ($row['BFC_Expiry_Date'] > date('Y-m-d')) {
                    $end_year = $year_current;
                    $end_month = $month_current;
                } else {
                    if ($expiry_year > 0) {
                        $end_year = $expiry_year;
                    } else {
                        $end_year = $year_current;
                    }
                    if ($expiry_month > 0) {
                        $end_month = $expiry_month;
                    } else {
                        $end_month = $month_current;
                    }
                }
                for ($start_year = 2017; $start_year <= $end_year; $start_year++) {
                    $years[] = $start_year;
                }
                $years = array_reverse($years);
                foreach ($years as $year) {
                    ?>
                    <h3 class="accordion-rows" id="ddd"><span class="accordion-title"><?php echo $year; ?></span></h3> 
                    <div class="sub-accordions accordion-padding">
                        <div class="content-sub-header">
                            <div class="data-column spl-other padding-none">Month</div>
                            <div class="data-column spl-other padding-none">Downloads</div>
                        </div>
                        <?php
                        $mailSent = 0;
                        foreach ($months as $month) {
                            ?>
                            <div class="data-content">
                                <div class="data-column spl-other">
                                    <?php
                                    print date('M', strtotime($year . '-' . $month . '-' . '01'));
                                    ?>
                                </div>
                                <div class="data-column spl-other">
                                    <?php
                                    // SELECTING Mail Sending 
                                    $total_Sent_Mail = 0;
                                    $sql_sent = "SELECT CU_ID FROM tbl_Coupon_Usages
                                                 WHERE MONTH(CU_Date) = '" . $month . "' AND YEAR(CU_Date) = '" . $year . "' AND CU_BFC_ID = '" . $bfc_id . "'";
                                    $result_count = mysql_query($sql_sent, $db) or die("Invalid query: $sql_sent -- " . mysql_error());
                                    $total_Sent_Mail = mysql_num_rows($result_count);
                                    $mailSent += $total_Sent_Mail;
                                    echo $total_Sent_Mail;
                                    ?>
                                </div>
                            </div>            
                            <?php
                            if ($year == $end_year && $month == $end_month) {
                                break;
                            }
                        }
                        ?> 
                        <div class="content-sub-header">
                            <div class="data-column spl-other padding-none">Total :</div>                            
                            <div class="data-column spl-other padding-none">
                                <?php
                                echo $mailSent;
                                ?>
                            </div>                            
                        </div>       
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>

    </div>
</div>
<script>
    $(function () {

        $('.accordion-rows a').click(function (event) {
            if ($(this).parent().parent().hasClass("ui-accordion-header")) {
                event.stopPropagation(); // this is
            }
        });
        $("#accordion").accordion({
            collapsible: true,
            heightStyle: 'content',
            active: 0
        });
        $('.token-input-dropdown').css("width", "400px");
    });
</script>
<style>
    .menu-items-accodings{width: 825px !important;}
    .content-wrapper .content .content-left .right .data-column.spl-other{width: 44% !important;}
</style>
<?PHP
require_once '../include/admin/footer.php';
?>