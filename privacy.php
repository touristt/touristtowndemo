<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Privacy Policy</title>
<link href="/include/tt.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF"><table width="100%" border="0" cellspacing="50" cellpadding="0">
          <tr>
            <td width="55%" valign="middle" class="pagetitle">Privacy</td>
            <td width="45%" align="right" valign="middle"><img src="images/TouristTown-Logo.gif" width="195" height="43" align="absmiddle" /></td>
            </tr>
          <tr>
            <td colspan="2"><p><span class="bold">Website Visitors</span><br />
                Like most website operators, Tourist Town collects non-personally-identifying information of the sort that web browsers and servers typically make available, such as the browser type, language preference, referring site, and the date and time of each visitor request. Tourist Town&#8217;s purpose in collecting non-personally identifying information is to better understand how Tourist Town&#8217;s visitors use its website. From time to time, Tourist Town may release non-personally-identifying information in the aggregate, e.g., by publishing a report on trends in the usage of its website.                Tourist Town may also collect potentially personally-identifying information like Internet Protocol (IP) addresses for logged in users. Tourist Town only discloses logged in user and commenter IP addresses under the same circumstances that it uses and discloses personally-identifying information as described below.</p>
              <p> <span class="bold">Gathering of Personally-Identifying Information</span><br />
                Certain visitors to Tourist Town&#8217;s websites choose to interact with Tourist Town in ways that require Tourist Town to gather personally-identifying information. The amount and type of information that Tourist Town gathers depends on the nature of the interaction. For example, we ask members to provide a username and email address. Those who engage in transactions with Tourist Town are asked to provide additional information, including as necessary the personal and financial information required to process those transactions. In each case, Tourist Town collects such information only insofar as is necessary or appropriate to fulfill the purpose of the visitor&#8217;s interaction with Tourist Town. Tourist Town does not disclose personally-identifying information other than as described below. Visitors can always refuse to supply personally-identifying information, with the caveat that it may prevent them from engaging in certain website-related activities.</p>
              <p> <span class="bold">Aggregated Statistics</span><br />
                Tourist Town may collect statistics about the behavior of visitors to its websites. Tourist Town may display this information publicly or provide it to others. However, Tourist Town does not disclose personally-identifying information other than as described below.</p>
              <p> <span class="bold">Protection of Certain Personally-Identifying Information</span><br />
                Tourist Town discloses potentially personally-identifying and personally-identifying information only to those of its employees, contractors and affiliated organizations that (i) need to know that information in order to process it on Tourist Town&#8217;s behalf or to provide services available at Tourist Town&#8217;s websites, and (ii) that have agreed not to disclose it to others. Some of those employees, contractors and affiliated organizations may be located outside of Canada; by using Tourist Town&#8217;s websites, you consent to the transfer of such information to them. Tourist Town will not rent or sell potentially personally-identifying and personally-identifying information to anyone. Other than to its employees, contractors and affiliated organizations, as described above, Tourist Town discloses potentially personally-identifying and personally-identifying information only in response to a subpoena, court order or other governmental request, or when Tourist Town believes in good faith that disclosure is reasonably necessary to protect the property or rights of Tourist Town, third parties or the public at large. If you are a registered user of an Tourist Town website and have supplied your email address, Tourist Town may occasionally send you an email to tell you about new features, solicit your feedback, or just keep you up to date with what&#8217;s going on with Tourist Town and our products. We primarily use our various product blogs to communicate this type of information, so we expect to keep this type of email to a minimum. If you send us a request (for example via a support email or via one of our feedback mechanisms), we reserve the right to publish it in order to help us clarify or respond to your request or to help us support other users. Tourist Town takes all measures reasonably necessary to protect against the unauthorized access, use, alteration or destruction of potentially personally-identifying and personally-identifying information.</p>
              <p> <span class="bold">Cookies</span><br />
                A cookie is a string of information that a website stores on a visitor&#8217;s computer, and that the visitor&#8217;s browser provides to the website each time the visitor returns. Tourist Town uses cookies to help Tourist Town identify and track visitors, their usage of Tourist Town website, and their website access preferences. Tourist Town visitors who do not wish to have cookies placed on their computers should set their browsers to refuse cookies before using Tourist Town&#8217;s websites, with the drawback that certain features of Tourist Town&#8217;s websites may not function properly without the aid of cookies.</p></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</body>
</html>
